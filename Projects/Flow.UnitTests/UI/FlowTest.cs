﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using System.Threading;
using Flow.View;
using System.Windows;
using System.Windows.Threading;
using Flow.Controller;

namespace Flow.UnitTests.UI
{
    [TestFixture]
    public class FlowTest
    {
        Exception _e = null;

        [Test]
        public void Main()
        {
            Thread t = new Thread(() =>
                {
                    Window2 w = new Window2();
                    w.Loaded += new System.Windows.RoutedEventHandler(w_Loaded);
                    w.ShowDialog();
                });
            t.SetApartmentState(ApartmentState.STA);
            t.Start();
            t.Join();

            if (_e != null)
            {
                Assert.Fail(_e.ToString());
            }
        }

        void Current_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            MessageBox.Show("Error: " + e.ToString());
        }

        void w_Loaded(object sender, System.Windows.RoutedEventArgs args)
        {
            Window2 w = sender as Window2;
            try
            {
                ApplicationPanel appPanel = w.AppPanel;
                FlowController flow = appPanel.FlowController;

                // Click on each button and make sure the correct panel is loaded
                flow.Test();
                Assert.AreEqual(Visibility.Visible, appPanel.TestPanel.Visibility);
                flow.Project();
                Assert.IsNotNull(appPanel.MainProjectPanel);
				Assert.AreEqual(Visibility.Visible, appPanel.MainProjectPanel.Visibility);
            }
            catch (Exception e)
            {
                _e = e;
            }


        }
    }
}
