﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Flow.Lib;
using Flow.Controller;
using Flow.View;
using System.Windows;
using System.Threading;
using Flow.Lib.Persister;
using Flow.Model.Settings;
using System.IO;
using Flow.View.Capture;
using Flow.Config;

namespace Flow.UnitTests.UI
{
    [TestFixture]
    public class HotFolderTest
    {
        public class NotifierMock : IShowsNotificationProgress
        {
            public Window Window { get; set; }

            public NotifierMock(Window w)
            {
                Window = w;
            }

            #region IShowsNotificationProgress Members

            public void ShowNotification(NotificationProgressInfo info)
            {
                Window.Title = info.Title + info.Message;
            }

            #endregion
        }

        public class DataPersisterMock : IConfigDataPersister
        {
            public T Load<T>() where T : class, new()
            {
                return default(T);
            }

            public void Save<T>(T data) { }
        }

        public class ConfigRepositoryMock : ConfigRepository
        {
            public ConfigRepositoryMock() : base(new DataPersisterMock()){ }

            public override T Load<T>()
            {
                if (typeof(T) == typeof(ApplicationConfig))
                {
                    string curDir = AppDomain.CurrentDomain.BaseDirectory;

                    ApplicationConfig config = new ApplicationConfig();
                    config.HotfolderDirectory = Path.Combine(curDir, "Hotfolder");
                    config.ProjectsWorkspace = Path.Combine(curDir, "Workspace");

                    // make sure the directories exist
                    if (!Directory.Exists(config.HotfolderDirectory))
                        Directory.CreateDirectory(config.HotfolderDirectory);
                    if (!Directory.Exists(config.ProjectsWorkspace))
                        Directory.CreateDirectory(config.ProjectsWorkspace);

                    return config as T;
                }
                else
                    return base.Load<T>();
            }
        }

        [Test]
        public void MainTest()
        {
            Thread t = new Thread(new ThreadStart((Action)delegate
            {
                FlowFactory.Register(typeof(ConfigRepository), delegate
                {
                    return new ConfigRepositoryMock();
                });

                HotFolderPanel pnl = new HotFolderPanel();

                Window w = new Window();
                w.Content = pnl;
                w.SizeToContent = SizeToContent.WidthAndHeight;

                HotFolderController hotfolder = FlowFactory.Build<HotFolderController>();
                hotfolder.Main(pnl);
                w.ShowDialog();
            }));
            t.SetApartmentState(ApartmentState.STA);
            t.Start();
            t.Join();

        }
    }
}
