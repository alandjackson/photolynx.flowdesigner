﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Flow.Designer.View;
using System.Threading;
using System.Windows;
using Flow.Designer.Controller;
using Flow.Designer.Helpers.Commands;
using Flow.Lib;
using Flow.Config;

namespace Flow.UnitTests.UI
{
    [TestFixture]
    public class DesignerTest
    {
        [Test]
        public void Main()
        {
            Thread t = new Thread(() =>
                {
                    Window w = new Window();
                    w.SizeToContent = SizeToContent.WidthAndHeight;

                    w.Loaded += new RoutedEventHandler(w_Loaded);
                    w.ShowDialog();
                });
            t.SetApartmentState(ApartmentState.STA);
            t.Start();
            t.Join();

        }

        void w_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                Flow.Designer.Controller.DesignerController d =
                    FlowFactory.Build<DesignerController>();
                d.Main(sender as Window);
                string img = @"C:\Users\Public\Pictures\Sample Pictures\Autumn Leaves.jpg";
                d.DoCommand(new AddGraphicFromFileCmd(img));
            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString());

            }
        }
    }
}
