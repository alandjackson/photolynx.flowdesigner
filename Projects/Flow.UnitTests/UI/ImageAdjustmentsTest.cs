﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Windows.Forms;
using NUnit.Framework;
using Flow.View.Edit.ImageAdjustments;
//using System.Windows;
//using System.Windows.Controls;
using System.Threading;
using Flow.Controller.Edit.ImageAdjustments;
using Flow.Schema.LinqModel.DataContext;

namespace Flow.UnitTests.UI
{
    [TestFixture]
    public class ImageAdjustmentsTest
    {
        Exception _e = null;

        [Test]
        public void Main()
        {
            Thread t = new Thread(() =>
            {
                System.Windows.Window w = new System.Windows.Window();
                ImageAdjustmentsController controller = new ImageAdjustmentsController();
                FlowProjectDataContext fpdc = new FlowProjectDataContext(@"C:\ProgramData\Flow\Projects\15190b24-0316-4804-afb1-4b61548b27de\15190b24-0316-4804-afb1-4b61548b27de.sdf");

                //Image i1 = new Image();
                //i1.ImagePath = @"C:\Users\BJ\Desktop\archive\bnl\barkdale2\000001.jpg";
                //i1.Red = 0;
                //i1.Green = 0;
                //i1.Blue = 0;
                //i1.Contrast = 0;
                //i1.Density = 0;
                //i1.ImageID = 1;
                //i1.CropH = 1;
                //i1.CropL = 0;
                //i1.CropT = 0;
                //i1.CropW = 1;
                //fpdc.Images.InsertOnSubmit(i1);
                //fpdc.SubmitChanges();
                controller.ImageListContoller.ImageTable = fpdc.Images;

                w.Content = controller.ImageAdjustmentsPanel;
                w.ShowDialog();
            });
            t.SetApartmentState(ApartmentState.STA);
            t.Start();
            t.Join();
            
        }
    }
}
