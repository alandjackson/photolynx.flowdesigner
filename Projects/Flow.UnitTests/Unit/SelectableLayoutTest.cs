﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Flow.Designer.Model;
using System.ComponentModel;

namespace Flow.UnitTests.Unit
{
    [TestFixture]
    public class SelectableLayoutTest
    {
        [Test]
        public void NotifyTest()
        {
            _lastArgs = new Queue<PropertyChangedEventArgs>();

            SelectableLayout l = new SelectableLayout();
            l.PropertyChanged += new PropertyChangedEventHandler(l_PropertyChanged);

            l.Layout = new Layout();
            Assert.AreEqual("Layout", _lastArgs.Dequeue().PropertyName);
        }

        protected Queue<PropertyChangedEventArgs> _lastArgs;
        void l_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            _lastArgs.Enqueue(e);
        }
    }
}
