﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Flow.Designer.Model;
using Flow.Lib.Persister;

namespace Flow.UnitTests.Unit
{
    [TestFixture]
    public class LayoutTest
    {
        [Test]
        public void SerializeTest()
        {
            Layout l = new Layout();
            l.LayoutItems.Add(new LayoutStaticImage());

            Console.WriteLine(ObjectSerializer.ToXmlString(l));
        }
    }
}
