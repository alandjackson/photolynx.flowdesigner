﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Flow.Lib;
using Flow.Lib.AsyncWorker;
using System.IO;
using Flow.Lib.Helpers;

namespace Flow.UnitTests.Unit
{
    [TestFixture]
    public class HotFolderWatcherTest
    {
        public int _filesCount;

        [Test]
        public void MainTest()
        {
            BackgroundWorker bw = new BackgroundWorker();
            bw.ShouldRun = true;
            List<string> formats = new List<string>();
            formats.Add("jpg");

            string hotfolderDir = @"hotfolder";
            if (!Directory.Exists(hotfolderDir))
                Directory.CreateDirectory(hotfolderDir);
            else
                foreach (string filename in Directory.GetFiles(hotfolderDir))
                    File.Delete(filename);
            _filesCount = 0;

            HotFolderWatcher watcher = new HotFolderWatcher(bw, formats);
            watcher.HotfolderDirectory = hotfolderDir;
            watcher.ImageFileAdded += 
                new EventHandler<EventArgs<string>>(watcher_ImageFileAdded);
            watcher.ImageFileRemoved += 
                new EventHandler<EventArgs<string>>(watcher_ImageFileRemoved);
            watcher.ScanHotfolderForChanges();
            Assert.AreEqual(0, _filesCount);

            // add in a couple of files
            string tempFilename = Path.GetTempFileName();
            File.Move(tempFilename, Path.Combine(hotfolderDir, 
                new FileInfo(tempFilename).Name + ".jpg"));
            tempFilename = Path.GetTempFileName();
            File.Move(tempFilename, Path.Combine(hotfolderDir,
                new FileInfo(tempFilename).Name + ".jpg"));
            watcher.ScanHotfolderForChanges();
            Assert.AreEqual(2, _filesCount);

            File.Delete(new FileInfo(tempFilename).Name + ".jpg");
            watcher.ScanHotfolderForChanges();
            Assert.AreEqual(2, _filesCount);

            foreach (string filename in Directory.GetFiles(hotfolderDir))
                File.Delete(filename);
            Directory.Delete(hotfolderDir);
        }

        void watcher_ImageFileRemoved(object sender, EventArgs<string> e)
        {
            _filesCount--;
        }

        void watcher_ImageFileAdded(object sender, EventArgs<string> e)
        {
            _filesCount++;
        }
    }
}
