﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using System.Data.Linq;
using Subject = Flow.Schema.LinqModel.Subject;
using System.Data.OleDb;

namespace Flow.UnitTests.Unit
{
    [TestFixture]
    public class LinqTest
    {
        [Test]
        public void SqlCeTest()
        {
            DataContext db = new DataContext("SqlCe/FlowProject.sdf");
            Table<Flow.Schema.LinqModel.Subject> Subjects = db.GetTable<Flow.Schema.LinqModel.Subject>();
            var q = from c in Subjects select c;
            foreach (var cust in q)
                Console.WriteLine("Id: {0}, Name: {1} {2}", cust.SubjectId, cust.FirstName, cust.LastName);
        }
        [Test]
        public void AccessTest()
        {
            OleDbConnection connection = new OleDbConnection(@"Provider=Microsoft.Jet.OLEDB.4.0; data source=Access/FlowProject.mdb");
            DataContext db = new DataContext(connection);
            Table<Flow.Schema.LinqModel.Subject> Subjects = db.GetTable<Flow.Schema.LinqModel.Subject>();
            var q = from c in Subjects select c;
            foreach (var cust in q)
                Console.WriteLine("Id: {0}, Name: {1} {2}", cust.SubjectId, cust.FirstName, cust.LastName);
        }
    }
}
