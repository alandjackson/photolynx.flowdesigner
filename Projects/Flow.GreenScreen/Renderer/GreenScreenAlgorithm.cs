﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Flow.GreenScreen.Data;
using Flow.GreenScreen.Panels;
using Flow.GreenScreen.Util;
using PhotoLynx.Logging;
using Image = System.Windows.Controls.Image;
using Bitmap = System.Drawing.Bitmap;
using Size = System.Windows.Size;

namespace Flow.GreenScreen.Renderer
{
    public class GreenScreenAlgorithm
    {
        Bitmap _bitmap;
        double _hueCorrection;
        double _satCorrection;
        double _valCorrection;
        int[] _tableSaturation;
        int[] _tableValue;
        int[] _tableHue;
        byte[] _curveTable;
        BackgroundAndColorConfiguration _configuration;
        ToleranceHSV _tableDefinitions;

        public static byte DefaultGreen
        {
            get { return (byte)191; }
        }

        public static byte DefaultRed
        {
            get { return (byte)86; }
        }

        public static byte DefaultBlue
        {
            get { return (byte)121; }
        }

        public GreenScreenAlgorithm()
        {
            ResetTableDefinitions();

            _satCorrection = 1;
            _valCorrection = 1;
            _hueCorrection = 1;
        }

        public GreenScreenAlgorithm(BackgroundAndColorConfiguration configuration)
            : this(configuration, null)
        {

        }

        public GreenScreenAlgorithm(BackgroundAndColorConfiguration configuration,
                                    byte[] curveTable)
            : this()
        {
            SetConfiguration(configuration);
            if (curveTable == null)
            {
                _curveTable = AlphaCurve.CreateTable(configuration.ControlPoints);
            }
            else
            {
                _curveTable = curveTable;
            }
        }

        private void ResetTableDefinitions()
        {
            _tableDefinitions = new ToleranceHSV();

            _tableDefinitions.Hue.ToZeroBegin = 56;
            _tableDefinitions.Hue.ToZeroEnd = 106;
            _tableDefinitions.Hue.ToFullBegin = 136;
            _tableDefinitions.Hue.ToFullEnd = 176;
            _tableDefinitions.Saturation.ToZeroBegin = 20;
            _tableDefinitions.Saturation.ToZeroEnd = 90;
            _tableDefinitions.Saturation.ToFullBegin = 255;
            _tableDefinitions.Saturation.ToFullEnd = 255;
            _tableDefinitions.Value.ToZeroBegin = 60;
            _tableDefinitions.Value.ToZeroEnd = 200;
            _tableDefinitions.Value.ToFullBegin = 255;
            _tableDefinitions.Value.ToFullEnd = 255;
        }

        public void SetConfiguration(BackgroundAndColorConfiguration configuration)
        {
            SetConfiguration(configuration, false);
        }

        public void SetConfiguration(BackgroundAndColorConfiguration configuration,
                                     bool constructCurveTable)
        {
            _configuration = configuration.CopyDeep();
            SetBackgroundDefinition(_configuration.BackgroundDefinitionRed, _configuration.BackgroundDefinitionGreen, _configuration.BackgroundDefinitionBlue);

            if (constructCurveTable)
            {
                _curveTable = AlphaCurve.CreateTable(_configuration.ControlPoints);
            }

        }

        public System.Drawing.Bitmap RunAlgorithm(System.Drawing.Bitmap bitmap)
        {
            return RunAlgorithm(bitmap, null);
        }

        public System.Drawing.Bitmap RunAlgorithm(Bitmap bitmap,
                                                  ProgressChangedEventHandler progress)
        {
            if (progress != null) progress(this,
              new ProgressChangedEventArgs(0, "Starting Color Dropout"));

            try
            {
                if (!_configuration.ProcessDropout)
                {
                    return bitmap;
                }
                _bitmap = bitmap;
                DateTime start = DateTime.Now;

                ResetTableDefinitions();


                _tableSaturation = new int[256];
                _tableValue = new int[256];
                _tableHue = new int[360];

                //scrutinze the creation of the table defs
                if (_configuration.DropList.Count > 0)
                {
                    DropOutValues dropOutValues = GetDropOutValues(_configuration.DropList);

                    _tableDefinitions.Hue.ToZeroBegin = 70;
                    _tableDefinitions.Hue.ToZeroEnd = dropOutValues.HueAverage - 10;
                    _tableDefinitions.Hue.ToFullBegin = dropOutValues.HueAverage + 10;
                    _tableDefinitions.Hue.ToFullEnd = dropOutValues.HueAverage + 40; //160;
                }

                PopulateTable(_tableDefinitions.Value, _tableValue);
                PopulateTable(_tableDefinitions.Hue, _tableHue);
                PopulateTable(_tableDefinitions.Saturation, _tableSaturation);


                //PrintGraph();

                if (_bitmap.PixelFormat != System.Drawing.Imaging.PixelFormat.Format32bppArgb)
                {
                    Bitmap reformated = new Bitmap(_bitmap.Width, bitmap.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
                    using (Graphics g = Graphics.FromImage(reformated))
                    {
                        g.DrawImage(_bitmap, new Rectangle(0, 0, reformated.Width, reformated.Height));
                    }
                    _bitmap.Dispose();
                    _bitmap = reformated;
                }

                BitmapData bmpData = _bitmap.LockBits(new Rectangle(0, 0, _bitmap.Width, _bitmap.Height), ImageLockMode.ReadWrite, _bitmap.PixelFormat);
                IntPtr ptr = bmpData.Scan0;

                // Declare an array to hold the bytes of the bitmap.
                int bytes = bmpData.Stride * _bitmap.Height;
                byte[] rgbValues = new byte[bytes];

                // Copy the RGB values into the array.
                System.Runtime.InteropServices.Marshal.Copy(ptr, rgbValues, 0, bytes);

                for (int i = 0; i < bytes; i += 4)
                {
                    if (progress != null) progress(this, new ProgressChangedEventArgs((int)((double)i / bytes * 100), "Processing Color Values"));

                    System.Drawing.Color newColor = ProcessPixelColor(rgbValues[i + 2], rgbValues[i + 1], rgbValues[i], rgbValues[i + 3]);

                    rgbValues[i + 3] = newColor.A;
                    rgbValues[i + 2] = newColor.R;
                    rgbValues[i + 1] = newColor.G;
                    rgbValues[i] = newColor.B;
                }

                // Copy the RGB values back to the bitmap
                System.Runtime.InteropServices.Marshal.Copy(rgbValues, 0, ptr, bytes);
                rgbValues = null;
                // Unlock the bits.
                _bitmap.UnlockBits(bmpData);

                bitmap = _bitmap;
                _bitmap = null;
                PhotoLynx.Logging.PlLog.Logger.Info("Dropout completed in " + (DateTime.Now - start).TotalMilliseconds.ToString());
                //if (_configuration.SoftenEdges)
                //{
                //  DefocusCommand cmd = new DefocusCommand(4,1);
                //  bitmap = cmd.Apply(bitmap);
                //  //cmd = new DefocusCommand(2, 1);
                //}
            }
            catch (Exception e)
            {
                PlLog.Logger.Error("GreenScreenAlgorithm:RunAlgorithm: " + e.ToString());
                throw (e);
            }
            return bitmap;
        }

        public static System.Drawing.Color GetAreaAverage(byte[] rgbValues, int pixelIndex, int stride)
        {
            int bpp = 4;

            pixelIndex = pixelIndex - pixelIndex % bpp;

            int sampleLength = 9; //needs to be odd

            int red = 0;
            int green = 0;
            int blue = 0;
            int count = 0;

            for (int i = 0; i < sampleLength; i++)
            {
                int currentRowPixelCenterIndex = pixelIndex + (stride * (((sampleLength - 1) / -2) + i));
                if (currentRowPixelCenterIndex >= 0 && currentRowPixelCenterIndex < rgbValues.Length)
                {
                    System.Drawing.Color aveColor = GetRowAverage(rgbValues, currentRowPixelCenterIndex, stride, sampleLength);
                    red += aveColor.R;
                    green += aveColor.G;
                    blue += aveColor.B;
                    count++;
                }
            }

            return System.Drawing.Color.FromArgb(255, red / count, green / count, blue / count);

        }

        static private System.Drawing.Color GetRowAverage(byte[] rgbValues, int rowPixelIndex, int stride, int sampleLength)
        {
            int bpp = 4;
            int red = 0;
            int green = 0;
            int blue = 0;
            int count = 0;

            double row = Math.Floor(rowPixelIndex / (double)stride);
            //left
            for (int i = 0; i < sampleLength; i++)
            {
                int currentPixelIndex = rowPixelIndex + (bpp * (((sampleLength - 1) / -2) + i));
                if (currentPixelIndex >= 0 && currentPixelIndex < rgbValues.Length && Math.Floor(currentPixelIndex / (double)stride) == row)
                {
                    blue += rgbValues[currentPixelIndex];
                    green += rgbValues[currentPixelIndex + 1];
                    red += rgbValues[currentPixelIndex + 2];
                    count++;
                }
            }
            return System.Drawing.Color.FromArgb(255, red / count, green / count, blue / count);
        }

        private void PrintGraph()
        {
            Bitmap graph = new Bitmap(360, 255);
            using (Graphics g = Graphics.FromImage(graph))
            {
                System.Drawing.Point[] points = new System.Drawing.Point[_tableHue.Count()];
                for (int i = 0; i < _tableHue.Count(); i++)
                {
                    points[i] = new System.Drawing.Point(i, _tableHue[i]);
                }
                g.DrawLines(new System.Drawing.Pen(System.Drawing.Color.Honeydew, 5f), points);


                points = new System.Drawing.Point[_tableSaturation.Count()];
                for (int i = 0; i < _tableSaturation.Count(); i++)
                {
                    points[i] = new System.Drawing.Point(i, _tableSaturation[i]);
                }
                g.DrawLines(new System.Drawing.Pen(System.Drawing.Color.SkyBlue, 5f), points);

                points = new System.Drawing.Point[_tableValue.Count()];
                for (int i = 0; i < _tableValue.Count(); i++)
                {
                    points[i] = new System.Drawing.Point(i, _tableValue[i]);
                }
                g.DrawLines(new System.Drawing.Pen(System.Drawing.Color.Violet, 5f), points);
            }
            //graph.Save(@"C:\colorGraph.jpg");
        }

        //private System.Drawing.Color ProcessPixelColor(byte red, byte green, byte blue, byte alpha)
        //{
        //  return ProcessPixelColor(red, green, blue, alpha, null);
        //}

        private int Clamp(int v, int min, int max)
        {
            return Math.Min(Math.Max(v, min), max);
        }

        private System.Drawing.Color ProcessPixelColor(byte red, byte green, byte blue, byte alpha)
        {

            //Process keep color list
            foreach (GreenScreenBGRA item in _configuration.KeepList)
            {

                if (blue > (item.Blue - _configuration.GetKeepTolerance()) && blue < (item.Blue + _configuration.GetKeepTolerance()) &&
                     green > (item.Green - _configuration.GetKeepTolerance()) && green < (item.Green + _configuration.GetKeepTolerance()) &&
                     red > (item.Red - _configuration.GetKeepTolerance()) && red < (item.Red + _configuration.GetKeepTolerance()))
                {
                    alpha = 255;
                    return System.Drawing.Color.FromArgb(alpha, red, green, blue);
                }
            }

            //Process drop color list
            foreach (GreenScreenBGRA item in _configuration.DropList)
            {
                //within tolerance
                if (blue > (item.Blue - _configuration.GetDropTolerance()) && blue < (item.Blue + _configuration.GetDropTolerance()) &&
                     green > (item.Green - _configuration.GetDropTolerance()) && green < (item.Green + _configuration.GetDropTolerance()) &&
                     red > (item.Red - _configuration.GetDropTolerance()) && red < (item.Red + _configuration.GetDropTolerance()))
                {
                    alpha = 0;
                    //return System.Drawing.Color.FromArgb(alpha, red, green, blue);
                    return System.Drawing.Color.FromArgb(alpha, 255, 255, 255);
                }
            }

            //Spill



            //int modifiedRed = (int)Math.Min(Math.Max(0, red * _greenCorrection * _configuration.BackgroundCorrectionWeight), 255);
            //int modifiedGreen = (int)Math.Min(Math.Max(0, green * _greenCorrection * _configuration.BackgroundCorrectionWeight), 255);
            //int modifiedBlue = (int)Math.Min(Math.Max(0, blue * _greenCorrection * _configuration.BackgroundCorrectionWeight), 255);


            //int modifiedGreen = (int)Math.Max(0, green * _greenCorrection);
            //int modifiedRed = (int)Math.Max(0, red * _greenCorrection);
            //int modifiedBlue = (int)Math.Max(0, blue * _greenCorrection);

            ////int modifiedGreen = (int)Math.Min(green + _greenCorrection, 255);
            ////int modifiedRed = (int)Math.Min(red + _greenCorrection, 255);
            ////int modifiedBlue = (int)Math.Min(blue + _greenCorrection, 255);


            //if (modifiedBlue > 255 || modifiedGreen > 255 || modifiedRed > 255)
            //{
            //  int adjustment = Math.Max(modifiedRed, Math.Max(modifiedGreen, modifiedBlue));
            //  adjustment -= 255;
            //  modifiedBlue -= adjustment;
            //  modifiedGreen -= adjustment;
            //  modifiedRed -= adjustment;
            //}

            GreenScreenHSV mHsv = GreenScreenHSV.FromRGB(red, green, blue, alpha);

            int tableValue;
            int tableHue;
            int tableSat;
            int vNdx = Clamp((int)(mHsv.v * _valCorrection), 0, _tableValue.Length - 1);
            int hNdx = Clamp((int)(mHsv.h * _hueCorrection), 0, _tableHue.Length - 1); // (int)Math.Min((int)(mHsv.h * _hueCorrection), _tableHue.Length - 1);
            int sNdx = Clamp((int)(mHsv.s * _satCorrection), 0, _tableSaturation.Length - 1); // (int)Math.Min((int)(mHsv.s * _satCorrection), _tableSaturation.Length - 1);

            tableValue = _tableValue[vNdx];
            tableHue = _tableHue[hNdx];
            tableSat = _tableSaturation[sNdx];

            if (mHsv.s < 30)
            {
                alpha = 255;
            }
            else if (green < 55 && red < 55 && blue < 55)
            {
                alpha = 255;
            }
            else
            {
                if (tableHue < 180)
                {
                    alpha = (byte)Math.Max(Math.Max(tableSat, tableHue), tableValue);
                }
                else
                {
                    alpha = (byte)Math.Max(tableSat, tableHue);
                }
            }

            alpha = _curveTable[alpha];


            GreenScreenHSV pHsv = GreenScreenHSV.FromRGB(red, green, blue, alpha);
            if (alpha != 0)
            {
                //if (pHsv.h > 40 && pHsv.h < 120)
                if (pHsv.h > 40 && pHsv.h <= 55 && pHsv.v < 160)
                {
                    green = (byte)((red + blue) / 2);
                }
                else if (pHsv.h > 55 && pHsv.h < 120)
                {
                    green = (byte)((red + blue) / 2);
                }
                else if (pHsv.h > 120 && pHsv.h <= 149)
                {
                    green = (byte)(Math.Max(red, blue) + Math.Abs(red - blue));
                }
                else if (pHsv.h == 120)
                {
                    green = (byte)((float)red * (float)0.299 + (float)0.587 * (float)green + (float)0.114 * (float)blue);
                    blue = red = green;
                }
            }

            return System.Drawing.Color.FromArgb(alpha, red, green, blue);
        }

        private int DistanceFromAverage(byte red, byte green, byte blue, System.Drawing.Color areaAverageColor)
        {
            return Math.Max(Math.Abs(areaAverageColor.R - red), Math.Max(areaAverageColor.B - blue, areaAverageColor.G - green));
        }

        private void PopulateTable(ToleranceValues vals, int[] table)
        {
            //int aa;
            int i;
            double gamma = 1.0;
            double value;

            for (i = 0; i <= vals.ToZeroBegin; ++i)
                table[i] = 255;

            for (; i <= vals.ToZeroEnd; ++i)
            {
                if (gamma != 1.0)
                {
                    value = 255 - ((i - vals.ToZeroBegin * 255) / vals.ToZeroEnd - vals.ToZeroBegin);

                    value = (double)(i - vals.ToZeroBegin) / (double)(vals.ToZeroEnd - vals.ToZeroBegin);
                    value = Math.Pow(value, gamma);
                    //aa = table[i] = (int)(255 - (value * 255.0));
                }
                else
                    table[i] = 255 - (((i - vals.ToZeroBegin) * 255) / (vals.ToZeroEnd - vals.ToZeroBegin));
            }

            for (; i <= vals.ToFullBegin; ++i)
                table[i] = 0;

            for (; i <= vals.ToFullEnd; ++i)
                table[i] = ((i - vals.ToFullBegin) * 255) / (vals.ToFullEnd - vals.ToFullBegin);

            for (; i < table.Count(); ++i)
                table[i] = 255;
        }

        private DropOutValues GetDropOutValues(List<GreenScreenBGRA> colors)
        {
            DropOutValues dr = new DropOutValues();

            foreach (GreenScreenBGRA color in colors)
            {
                GreenScreenHSV hsv = GreenScreenHSV.FromRGB(color);
                dr.HueAverage += (int)hsv.h;
                if (hsv.h < dr.huemin)
                    dr.huemin = (int)hsv.h;
                if (hsv.h > dr.huemax)
                    dr.huemax = (int)hsv.h;

                dr.SaturationAverage += (int)hsv.s;
                if (hsv.s < dr.satmin)
                    dr.satmin = (int)hsv.s;
                if (hsv.s > dr.satmax)
                    dr.satmax = (int)hsv.s;

                dr.ValueAverage += (int)hsv.v;
                if (hsv.v < dr.valmin)
                    dr.valmin = (int)hsv.v;
                if (hsv.v > dr.valmax)
                    dr.valmax = (int)hsv.v;
            }

            dr.HueAverage /= colors.Count;
            dr.SaturationAverage /= colors.Count;
            dr.ValueAverage /= colors.Count;

            return dr;
        }

        private static void SaveImageControl(Image image, string filename)
        {
            RenderTargetBitmap rtb = new RenderTargetBitmap((int)image.Width, (int)image.Height, 96, 96, PixelFormats.Pbgra32);
            rtb.Render(image);

            PngBitmapEncoder encoder = new PngBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(rtb));

            MemoryStream ms = new MemoryStream();
            encoder.Save(ms);
            ((Bitmap)Bitmap.FromStream(ms)).Save(filename);
        }

        private static ImageSource GetImageSourceFromBitmap(System.Drawing.Bitmap bitmap)
        {
            MemoryStream ms = new MemoryStream();
            bitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
            ms.Position = 0;

            BitmapImage bi = new BitmapImage();
            bi.BeginInit();
            bi.StreamSource = ms;
            bi.EndInit();

            return bi;
        }

        public static Bitmap ScaleBitmap(Bitmap srcImage, System.Drawing.Size newSize)
        {
            // create the new image
            Bitmap dstImage = new Bitmap(newSize.Width, newSize.Height, srcImage.PixelFormat);
            dstImage.SetResolution(srcImage.HorizontalResolution, srcImage.VerticalResolution);

            // draw the src image at the new size
            Graphics dstGraphics = Graphics.FromImage(dstImage);
            dstGraphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            dstGraphics.DrawImage(srcImage,
                                  new Rectangle(0, 0, newSize.Width, newSize.Height),
                                  new Rectangle(0, 0, srcImage.Width, srcImage.Height),
                                  GraphicsUnit.Pixel);

            srcImage.Dispose();

            return dstImage;
        }

        public static System.Drawing.Bitmap RenderImageToBitmap(System.Drawing.Bitmap sourceBitmap,
                                                                string selectionConfigurationXml,
                                                                string foregroundPath,
                                                                string backgroundPath,
                                                                //System.Drawing.Bitmap dropoutImage,
                                                                System.Drawing.RectangleF cropRectangle,
                                                                float cropRatio)
        {
            return RenderImageToBitmap(sourceBitmap, selectionConfigurationXml, foregroundPath, backgroundPath, //dropoutImage,
              cropRectangle, cropRatio, null);
        }

        public static double GetScale(Bitmap bitmap, double renderWidth, double renderHeight)
        {
            return GetScale(new SizeF((float)bitmap.Width, (float)bitmap.Height),
                            new SizeF((float)renderWidth, (float)renderHeight));
        }

        public static double GetScale(SizeF sourceSize, SizeF renderSize)
        {
            return Math.Max(sourceSize.Width / renderSize.Width, sourceSize.Height / renderSize.Height);
        }

        public static Bitmap RenderDropOutToBitmap(Bitmap sourceBitmap, string selectionConfigurationXml, System.ComponentModel.ProgressChangedEventHandler progress)
        {
            
            if (selectionConfigurationXml != null)
            {
                SelectionConfiguration selectionConfig =
                 SelectionConfiguration.DeserializeFromString(selectionConfigurationXml);

                GreenScreenAlgorithm alg = new GreenScreenAlgorithm(selectionConfig.ColorAndBackgroundConfiguration);
                System.Drawing.Bitmap dropoutImage = alg.RunAlgorithm(new Bitmap(sourceBitmap), progress);
                return dropoutImage;
            }
            return sourceBitmap;

        }
        public static Bitmap RenderImageToBitmap(Bitmap sourceBitmap,
                                  string selectionConfigurationXml,
                                  string foregroundPath,
                                  string backgroundPath,
                                  //System.Drawing.Bitmap dropoutImage,
                                  System.Drawing.RectangleF cropRectangle,
                                  float cropRatio,
                                  System.ComponentModel.ProgressChangedEventHandler progress)
        {

            if (progress != null) progress(null, new ProgressChangedEventArgs(99,
              "Loading Clip Regions"));
            SelectionConfiguration selectionConfig = null;
            if(selectionConfigurationXml == null) 
                selectionConfig = new SelectionConfiguration();
            else
                selectionConfig = SelectionConfiguration.DeserializeFromString(selectionConfigurationXml);

            Bitmap renderedImage = null;

            double uniformScale = GetScale(sourceBitmap, GreenScreenPanel.RenderWidth, GreenScreenPanel.RenderHeight);
            double scaleX = uniformScale;
            double scaleY = uniformScale;

            //check cache settings
            GreenScreenRenderCache cache = new GreenScreenRenderCache(foregroundPath);
            if (cache.IsUpToDate(selectionConfig))
            {
                renderedImage = cache.LoadCachedImage();
            }
            else
            {
                renderedImage = CreateDropoutImage(sourceBitmap, selectionConfig, progress, scaleX, scaleY);

                if (cache.IsGreenScreenCacheEnabled) //saves a fresh copy of selectionConfig incase of modifications during render
                {
                    SelectionConfiguration config=null;
                    if (selectionConfigurationXml == null)
                        config = new SelectionConfiguration();
                    else
                        config = SelectionConfiguration.DeserializeFromString(selectionConfigurationXml);
                    cache.SaveToCache(renderedImage, config);

                }
            }

            //finnaly, crop add background 
            try
            {
                //stretch image and draw background image to fit\
                using (Bitmap backgroundImage = new Bitmap(backgroundPath))
                {
                    int finalWidth = renderedImage.Width;
                    int finalHeight = renderedImage.Height;
                    if (cropRectangle.Height != 0)
                    {
                        finalHeight = (int)(finalHeight * (cropRectangle.Height / 100));
                        finalWidth = (int)(finalHeight * cropRatio);
                    }

                    Bitmap tempBitmap = new Bitmap(backgroundImage, finalWidth, finalHeight);
                    using (Graphics tempGraphics = Graphics.FromImage(tempBitmap))
                    {
                        backgroundImage.Dispose();
                        tempGraphics.DrawImage(renderedImage,
                          new RectangleF((float)(selectionConfig.ForegroundTopLeft.X * uniformScale), (float)(selectionConfig.ForegroundTopLeft.Y * uniformScale),
                            (float)(tempBitmap.Width * selectionConfig.ForegroundScale), (float)(tempBitmap.Height * selectionConfig.ForegroundScale)),
                          new RectangleF(
                            renderedImage.Width * (cropRectangle.X / 100),
                            renderedImage.Height * (cropRectangle.Y / 100),
                            tempBitmap.Width, tempBitmap.Height),
                          GraphicsUnit.Pixel);
                    }
                    //sourceBitmap.Dispose();
                    renderedImage.Dispose();
                    renderedImage = tempBitmap;
                }
            }
            catch (Exception e)
            {
                string message = "ERROR GreenScreenAlgorithm.RenderImageToBitmapMemoryOptimized: Failed drawing background " + backgroundPath + ". " + e.ToString();
                //MessageBox.Show(message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                PhotoLynx.Logging.PlLog.Logger.Info(message);
                throw (new Exception(message));
            }

            //GarbageManager.DeleteGDIObject(hDropoutImage);
            //GarbageManager.DeleteGDIObject(hSourceImage);
            //sourceBitmap.Dispose();
            //dropoutImage.Dispose();

            return renderedImage;
        }

        public static Bitmap CreateDropoutImage(Bitmap sourceBitmap, SelectionConfiguration selectionConfig,
            ProgressChangedEventHandler progress = null, double scaleX = 1.0, double scaleY = 1.0)
        {
            Bitmap renderedImage;

            //Create dropped out image
            GreenScreenAlgorithm alg = new GreenScreenAlgorithm(selectionConfig.ColorAndBackgroundConfiguration);
            System.Drawing.Bitmap dropoutImage = alg.RunAlgorithm(new Bitmap(sourceBitmap), progress);

            if (selectionConfig.RenderedClipNegative.Figures.Count != 0 ||
                selectionConfig.ForegroundClip.Figures.Count != 0 ||
                selectionConfig.ForegroundScale != 1)
            {


                System.Windows.Controls.Image imageRendered = null;
                System.Windows.Controls.Image imageForeground = null;
                try
                {
                    if (progress != null) progress(null, new ProgressChangedEventArgs(99,
                      "Processing Foreground Clip Regions"));
                    imageRendered = new System.Windows.Controls.Image();
                    imageRendered.BeginInit();
                    imageRendered.Width = dropoutImage.Width;
                    imageRendered.Height = dropoutImage.Height;
                    imageRendered.Source = GetImageSourceFromBitmap(dropoutImage);
                    imageRendered.Clip = new CombinedGeometry(GeometryCombineMode.Exclude,
                      new RectangleGeometry(
                        new Rect(0, 0, GreenScreenPanel.RenderWidth, GreenScreenPanel.RenderHeight)),
                      selectionConfig.RenderedClipNegative);
                    imageRendered.Clip.Transform = new ScaleTransform(scaleX, scaleY);
                    imageRendered.EndInit();
                    imageRendered.Measure(new Size(dropoutImage.Width, dropoutImage.Height));
                    imageRendered.Arrange(new Rect(0, 0, dropoutImage.Width, dropoutImage.Height));
                    imageRendered.UpdateLayout();

                    if (progress != null) progress(null, new ProgressChangedEventArgs(99, "Processing Background Clip Regions"));
                    //BitmapSource originalSource = BitmapInterop.ToBitmapSource(hSourceImage, dropoutImage.Width, dropoutImage.Height);
                    imageForeground = new System.Windows.Controls.Image();
                    imageForeground.BeginInit();
                    imageForeground.Width = dropoutImage.Width;
                    imageForeground.Height = dropoutImage.Height;
                    imageForeground.Source = GetImageSourceFromBitmap(sourceBitmap); //originalSource;
                    //imageForeground.Clip = selectionConfig.ForegroundClip.GetOutlinedPathGeometry();
                    imageForeground.Clip = selectionConfig.ForegroundClip;
                    imageForeground.Clip.Transform = new ScaleTransform(scaleX, scaleY);
                    imageForeground.EndInit();
                    imageForeground.Measure(new System.Windows.Size(dropoutImage.Width, dropoutImage.Height));
                    imageForeground.Arrange(new System.Windows.Rect(0, 0, dropoutImage.Width, dropoutImage.Height));
                    imageForeground.UpdateLayout();
                    Size sourceSize = new Size(sourceBitmap.Width, sourceBitmap.Height);
                    sourceBitmap.Dispose();
                    dropoutImage.Dispose();

                    //SaveImageControl(imageRendered, @"c:\img_rendered.png");
                    //SaveImageControl(imageForeground, @"c:\img_foreground.png");

                    if (progress != null) progress(null, new ProgressChangedEventArgs(99, "Combining Image Layers"));
                    RenderTargetBitmap rtb = new RenderTargetBitmap((int)sourceSize.Width, (int)sourceSize.Height, 96, 96, PixelFormats.Pbgra32);
                    rtb.Render(imageRendered);
                    rtb.Render(imageForeground);

                    //System.Windows.Controls.Canvas c = new System.Windows.Controls.Canvas();
                    //c.
                    //c.Children.Add(imageRendered);

                    //c.Children.Add(imageForeground);


                    PngBitmapEncoder encoder = new PngBitmapEncoder();

                    encoder.Frames.Add(BitmapFrame.Create(rtb));

                    using (MemoryStream ms = new MemoryStream())
                    {
                        encoder.Save(ms);
                        renderedImage = (Bitmap)Bitmap.FromStream(ms);
                    }
                    rtb.Clear();

                }
                catch (Exception e)
                {
                    throw e;
                }
                finally
                {
                    if (imageForeground != null)
                        (imageForeground.Source as BitmapImage).StreamSource.Dispose();
                    if (imageRendered != null)
                        (imageRendered.Source as BitmapImage).StreamSource.Dispose();
                }
            }
            else
            {
                renderedImage = dropoutImage.Clone() as Bitmap;
                dropoutImage.Dispose();
            }

            //soften edges
            if (selectionConfig.ColorAndBackgroundConfiguration.SoftenEdges)
            {
                DefocusCommand cmd = new DefocusCommand(4, 0);
                renderedImage = cmd.Apply(renderedImage);
            }
            return renderedImage;
        }

        private static System.Drawing.Region BuildRegion(List<object> shapeDefinition)
        {
            Region region = new Region();
            region.MakeEmpty();

            if (shapeDefinition[0] is System.Windows.Point)
            {//dragged brush
                System.Drawing.Drawing2D.GraphicsPath path = new System.Drawing.Drawing2D.GraphicsPath();
                List<PointF> points = new List<PointF>();
                for (int i = 0; i < shapeDefinition.Count; i++)
                {
                    points.Add(new PointF((float)((System.Windows.Point)shapeDefinition[i]).X,
                      (float)((System.Windows.Point)shapeDefinition[i]).Y));
                    shapeDefinition[i] = null;
                }
                path.AddPolygon(points.ToArray());
                points.Clear();
                region.Union(path);
            }
            else
            {//lasso selection
                for (int i = 0; i < shapeDefinition.Count(); i++)
                {
                    System.Drawing.Drawing2D.GraphicsPath path = new System.Drawing.Drawing2D.GraphicsPath();
                    if (shapeDefinition[i] is System.Windows.Point)
                    {//constuct polygon
                        List<PointF> points = new List<PointF>();
                        for (int j = i; j < i + 4; j++)
                        {
                            points.Add(new System.Drawing.PointF((float)((System.Windows.Point)shapeDefinition[j]).X,
                              (float)((System.Windows.Point)shapeDefinition[j]).Y));
                            shapeDefinition[j] = null;
                        }
                        path.AddPolygon(points.ToArray());
                        points.Clear();

                        i += 3;
                    }
                    else
                    {
                        path.AddEllipse((float)((shapeDefinition[i] as EllipseGeometry).Center.X - (shapeDefinition[i] as EllipseGeometry).RadiusX),
                      (float)((shapeDefinition[i] as EllipseGeometry).Center.Y - (shapeDefinition[i] as EllipseGeometry).RadiusY),
                      (float)((shapeDefinition[i] as EllipseGeometry).RadiusX * 2),
                      (float)((shapeDefinition[i] as EllipseGeometry).RadiusY * 2));
                    }
                    region.Union(path);
                }
            }

            return region;
        }


        internal void SetBackgroundDefinition(byte red, byte green, byte blue)
        {
            //_configuration.BackgroundDefinitionBlue = blue;
            //_configuration.BackgroundDefinitionGreen = green;
            //_configuration.BackgroundDefinitionRed = red;


            GreenScreenHSV averageHsv = GreenScreenHSV.FromRGB((byte)Math.Ceiling(red + _configuration.BackgroundCorrection),
              (byte)Math.Ceiling(green + _configuration.BackgroundCorrection),
              (byte)Math.Ceiling(blue + _configuration.BackgroundCorrection), 255);
            GreenScreenHSV defaultHsv = GreenScreenHSV.FromRGB(DefaultRed, DefaultGreen, DefaultBlue, 255);

            _hueCorrection = defaultHsv.h / averageHsv.h;
            _valCorrection = defaultHsv.v / averageHsv.v;
            _satCorrection = defaultHsv.s / averageHsv.s;
        }
    }


    public class PixelAlphaHistory
    {
        public enum AlphaState
        {
            Normal,
            Dropped,
            Kept
        }

        public PixelAlphaHistory()
        {
            State = AlphaState.Normal;
        }

        public AlphaState State;
    }

    public class ToleranceHSV
    {
        public ToleranceValues Hue;
        public ToleranceValues Saturation;
        public ToleranceValues Value;

        public ToleranceHSV()
        {
            Hue = new ToleranceValues();
            Saturation = new ToleranceValues();
            Value = new ToleranceValues();
        }
    }

    public class ToleranceValues
    {
        public int ToZeroBegin, ToZeroEnd;
        //public int minout1, maxout1;
        public int ToFullBegin, ToFullEnd;
        //public int minout2, maxout2;
        //public int topend;

        public ToleranceValues()
        {

        }
    }

    public class DropOutValues
    {
        public int huemin, huemax, HueAverage;
        public int valmin, valmax, ValueAverage;
        public int satmin, satmax, SaturationAverage;

        public DropOutValues()
        {
            huemin = satmin = valmin = 999;
            huemax = satmax = valmax = -999;
            HueAverage = SaturationAverage = ValueAverage = 0;
        }
    }

    public class GreenScreenHSV
    {
        public float h;
        public float s;
        public float v;

        internal static GreenScreenHSV FromRGB(GreenScreenBGRA color)
        {
            return FromRGB(color.Red, color.Green, color.Blue, color.Alpha);
        }

        internal static GreenScreenHSV FromRGB(System.Drawing.Color color)
        {
            GreenScreenHSV hsv = new GreenScreenHSV();
            hsv.h = color.GetHue();
            hsv.s = color.GetSaturation();
            hsv.v = color.GetBrightness();
            return hsv;
        }

        internal static GreenScreenHSV FromRGB(int red, int green, int blue, int alpha)
        {
            GreenScreenHSV hsv = new GreenScreenHSV();

            float min, max, delta;
            float h;

            min = Math.Min(Math.Min(red, green), blue);
            max = Math.Max(Math.Max(red, green), blue);

            hsv.v = (int)max;
            delta = max - min;

            if (max == min)
            {
                hsv.s = 0;
                hsv.h = 0;
                return hsv;
            }
            else if (max != 0)
                hsv.s = (int)((float)255.0 * delta / max);
            else
            {
                hsv.s = 0;
                hsv.h = 0;
                return hsv;
            }
            if (red == max)
                h = (float)(green - blue) / delta;
            else if (green == max)
                h = (float)2.0 + ((float)(blue - red)) / delta;
            else
                h = (float)4.0 + ((float)(red - green)) / delta;
            hsv.h = (int)(h * 60.0);
            if (hsv.h < 0)
                hsv.h += 360;
            return hsv;
        }
    }

    public class GreenScreenBGRA
    {
        protected char _red;
        protected char _green;
        protected char _blue;
        protected char _alpha;

        public GreenScreenBGRA()
        {
            Red = 0;
            Green = 0;
            Blue = 0;
            Alpha = 255;
        }

        public GreenScreenBGRA(string[] parameters)
            : this()
        {
            Red = Convert.ToInt32(parameters[0]);
            Green = Convert.ToInt32(parameters[1]);
            Blue = Convert.ToInt32(parameters[2]);
        }

        public GreenScreenBGRA(int red, int green, int blue, int alpha)
        {
            Red = red;
            Green = green;
            Blue = blue;
            Alpha = alpha;
        }

        public int Alpha
        {
            get { return (int)_alpha; }
            set { _alpha = (char)value; }
        }

        public int Red
        {
            get { return (int)_red; }
            set { _red = (char)value; }
        }

        public int Green
        {
            get { return (int)_green; }
            set { _green = (char)value; }
        }

        public int Blue
        {
            get { return (int)_blue; }
            set { _blue = (char)value; }
        }

        public bool Equals(GreenScreenBGRA obj)
        {
            if (Red != obj.Red)
            {
                return false;
            }
            if (Blue != obj.Blue)
            {
                return false;
            }
            if (Green != obj.Green)
            {
                return false;
            }
            if (Alpha != obj.Alpha)
            {
                return false;
            }
            return true;
        }

        public override string ToString()
        {
            return Red.ToString() + " " + Green.ToString() + " " + Blue.ToString();
        }
    }

    public class InvalidBackgroundException : ArgumentException
    {
        public InvalidBackgroundException(string message)
            : base(message)
        {

        }
    }
}
