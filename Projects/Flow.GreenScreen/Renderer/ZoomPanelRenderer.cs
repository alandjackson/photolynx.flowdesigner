﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Flow.GreenScreen.Renderer
{/*
  public class ZoomPanelRenderer
  {
    public ZoomPanelRenderer(Rectangle backgroundImageSurface, Rectangle layerGroupSurface, ZoomPanel zoomPanel, BitmapImage currentImage,
      ImageSource displayedImage, Dispatcher dispatcher)
    {
      _backgroundImageSurface = backgroundImageSurface;
      _layerGroupSurface = layerGroupSurface;
      _zoomPanel = zoomPanel;
      _currentImage = currentImage;
      _displayedImage = displayedImage;
      _dispatcher = dispatcher;
    }

    public void BeginRender()
    {
      _dispatcher.BeginInvoke(RenderingPriority, new RenderDelegate(RenderZoomPanelBitmap));
    }

    private void RenderZoomPanelBitmap()
    {
      try
      {
        DrawingVisual drawing = new DrawingVisual();
        using (DrawingContext context = drawing.RenderOpen())
        {
          Rect rect = new Rect(new Size(_currentImage.Width, _currentImage.Height));
          context.PushClip(new RectangleGeometry(rect));
          context.DrawRectangle(_backgroundImageSurface.Fill, null, rect);
          context.PushOpacityMask(new ImageBrush(_displayedImage));
          context.DrawImage(_currentImage, rect);
        }
        RenderTargetBitmap rtb = new RenderTargetBitmap(_currentImage.PixelWidth, _currentImage.PixelHeight,
          _currentImage.DpiX, _currentImage.DpiY, PixelFormats.Pbgra32);
        rtb.Render(drawing);
        rtb.Render(_layerGroupSurface);
        _zoomPanel.SourceImage = rtb;
      }
      catch (Exception e)
      {
        PhotoLynx.Logging.PlLog.Logger.Info("ZoomPanelRenderer.RenderZoomPanelBitmap: " + e.Message);
      }
    }

    protected delegate void RenderDelegate();

    protected DispatcherPriority RenderingPriority { get { return DispatcherPriority.Normal; } }

    protected Rectangle _backgroundImageSurface;
    protected Rectangle _layerGroupSurface;
    protected ZoomPanel _zoomPanel;
    protected BitmapImage _currentImage;
    protected ImageSource _displayedImage;
    protected Dispatcher _dispatcher;
  }*/
}
