﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Flow.GreenScreen.Data;
using Flow.GreenScreen.Panels;
using Flow.GreenScreen.Util;
using PhotoLynx.Logging;
using Image = System.Windows.Controls.Image;
using Bitmap = System.Drawing.Bitmap;
using PixelFormat = System.Drawing.Imaging.PixelFormat;
using Flow.GreenScreen.Managers;
using System.Text;

namespace Flow.GreenScreen.Renderer
{
    public class IsolatedGreenScreenRenderer : MarshalByRefObject
    {
        protected System.Drawing.Bitmap _image;
        protected byte[] _renderedImageData;
        protected string _configurationXml;
        protected string _foregroundPath;
        protected string _backgroundPath;
        //System.Drawing.Bitmap _greenScreenedImage;
        System.Drawing.RectangleF _cropRectangle;
        float _cropRatio;
        System.ComponentModel.ProgressChangedEventHandler _progress = null;

        public System.Drawing.Bitmap Render(System.Drawing.Bitmap image,
                                            string configurationXml,
                                            string foregroundPath,
                                            string backgroundPath,
                                            //System.Drawing.Bitmap greenScreenedImage,
                                            System.Drawing.RectangleF croppedRectangle,
                                            float cropRatio)
        {
            return Render(image,
                          configurationXml,
                          foregroundPath,
                          backgroundPath,
                          //greenScreenedImage,
                          croppedRectangle,
                          cropRatio,
                          null);
        }

        public System.Drawing.Bitmap Render(System.Drawing.Bitmap image,
                                            string configurationXml,
                                            string foregroundPath,
                                            string backgroundPath,
                                            System.Drawing.RectangleF croppedRectangle,
                                            float cropRatio,
                                            ProgressChangedEventHandler progress)
        {
            _cropRatio = cropRatio;
            _image = image;
            _cropRectangle = croppedRectangle;
            _configurationXml = configurationXml;
            _foregroundPath = foregroundPath;
            _backgroundPath = backgroundPath;
            _progress = progress;

            RenderImage();
            return _image;
        }

        protected void RenderImage()
        {
            Bitmap image = GreenScreenAlgorithm.RenderImageToBitmap(_image,
                                                                    _configurationXml,
                                                                    _foregroundPath,
                                                                    _backgroundPath,
                                                                    _cropRectangle,
                                                                    _cropRatio,
                                                                    _progress);
            _image.Dispose();
            _image = image;
        }

        public static Bitmap IsolatedRenderImage(Bitmap originalImage,
                                                 string selectionConfigurationXml,
                                                 string backgroundPath, string foregroundPath,
                                                 ProgressChangedEventHandler progress)
        {
            return IsolatedRenderImage(originalImage,
                                       selectionConfigurationXml,
                                       backgroundPath, foregroundPath,
                                       new RectangleF(),
                                       0,
                                       progress);
        }

        protected static string BuildFilename(string path,
                                              string filename,
                                              string ext)
        {
            return Path.Combine(path, filename + ext);
        }

        public static Bitmap IsolatedRenderImage(Bitmap originalImage,
                                                 string selectionConfigurationXml,
                                                 string backgroundPath, string foregroundPath,
                                                 RectangleF cropRectangle,
                                                 float cropRatio)
        {
            return IsolatedRenderImage(originalImage,
                                       selectionConfigurationXml,
                                       backgroundPath, foregroundPath,
                                       cropRectangle,
                                       cropRatio,
                                       null);
        }

        public static bool VerifyBackgroundPath(string bgPath)
        {

            if (String.IsNullOrEmpty(bgPath))
            {
                if (GreenScreen2InstanceSettings.Instance.ContinueOnInvalidBackground < 0)
                {
                    GreenScreen2InstanceSettings.Instance.VerifyContinueOnInvalidBackground();
                }

                if (GreenScreen2InstanceSettings.Instance.ContinueOnInvalidBackground == 0)
                {
                    string msg = "Green screen background not specified.";
                    throw (new InvalidBackgroundException(msg));
                }
                else
                {
                    return false;
                }
            }
            return true;
        }

        public class PlProfileInfo
        {
            public string Message { get; set; }
            public DateTime Timestamp { get; set; }

            public PlProfileInfo(string msg)
            {
                Message = msg;
                Timestamp = DateTime.Now;
            }

            public static string GetProfilesMsg(List<PlProfileInfo> infos)
            {
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < infos.Count; i++)
                {
                    string ellapsed;
                    if (i < infos.Count - 1)
                        ellapsed = infos[i + 1].Timestamp.Subtract(infos[i].Timestamp).
                                    TotalSeconds.ToString();
                    else
                        ellapsed = null;

                    sb.AppendLine(infos[i].Message + (ellapsed != null ? "..." + ellapsed : ""));
                }
                if (infos.Count > 1)
                {
                    sb.AppendLine("Total Time: " +
                       infos[infos.Count - 1].Timestamp.Subtract(infos[0].Timestamp).
                       TotalSeconds.ToString());
                }

                return sb.ToString();
            }
        }

        public static Bitmap IsolatedRenderImage(Bitmap originalImage,
                                                 string selectionConfigurationXml,
                                                 string backgroundPath,
                                                 string foregroundPath,
                                                 RectangleF cropRectangle,
                                                 float cropRatio,
                                                 ProgressChangedEventHandler progress)
        {
            List<PlProfileInfo> timestamps = new List<PlProfileInfo>();
            timestamps.Add(new PlProfileInfo("IsolatedRenderImage Start"));
            SelectionConfiguration selectionConfig = null;
            if (selectionConfigurationXml == null)
                selectionConfig = new SelectionConfiguration(true);
            else
                selectionConfig = SelectionConfiguration.DeserializeFromString(selectionConfigurationXml);

            if (selectionConfig.RotateFlipType != RotateFlipType.RotateNoneFlipNone)
                originalImage.RotateFlip(selectionConfig.RotateFlipType);

            if (!VerifyBackgroundPath(backgroundPath))
                return (Bitmap)originalImage.Clone();



            //using (Bitmap bgImage = new Bitmap(originalImage))
            //{
                //originalImage.Dispose();

                timestamps.Add(new PlProfileInfo("IsolatedRenderImage GS Alg"));

                //GreenScreenRenderCache cache = new GreenScreenRenderCache(foregroundPath);
                //Bitmap greenScreenedImage = null;
                //if (cache.IsUpToDate(selectionConfig))
                //{
                //    greenScreenedImage = cache.LoadCachedImage();
                //}
                //else
                //{

                    //GreenScreenAlgorithm alg = new GreenScreenAlgorithm(
                    //  selectionConfig.ColorAndBackgroundConfiguration);
                    //Bitmap greenScreenedImage = alg.RunAlgorithm(new Bitmap(originalImage), progress);
                    //if (cache.IsGreenScreenCacheEnabled)
                    //    cache.SaveToCache(greenScreenedImage, selectionConfig);
                //}
                  timestamps.Add(new PlProfileInfo("IsolatedRenderImage Isolated Gs Render"));
                  IsolatedGreenScreenRenderer isoRenderer = new IsolatedGreenScreenRenderer();
                  Bitmap toReturn = isoRenderer.Render(originalImage, selectionConfigurationXml, foregroundPath,
                    backgroundPath, cropRectangle, cropRatio, progress);
                  originalImage.Dispose();

                  timestamps.Add(new PlProfileInfo("IsolatedRenderImage Pixel Format"));
                  if (toReturn.PixelFormat == PixelFormat.Format32bppPArgb)
                  {
                      PlLog.Logger.Info("TRACE IsolatedGreenScreenRenderer.IsolatedRenderImage:" +
                        " Resizing image for mathematics");
                      Bitmap modifiedBitmap = new Bitmap(toReturn.Width, toReturn.Height,
                        PixelFormat.Format24bppRgb);
                      using (Graphics g = Graphics.FromImage((System.Drawing.Image)modifiedBitmap))
                          g.DrawImage(toReturn, 0, 0, toReturn.Width, toReturn.Height);
                      toReturn.Dispose();
                      toReturn = modifiedBitmap;
                  }
                  timestamps.Add(new PlProfileInfo("IsolatedRenderImage Dispose"));

                  selectionConfig.Dispose();

                  timestamps.Add(new PlProfileInfo("IsolatedRenderImage Finish"));

                  PlLog.Logger.Info(PlProfileInfo.GetProfilesMsg(timestamps));
                  //greenScreenedImage.Dispose();
                  return toReturn;      
            //}
        }
    }
}
