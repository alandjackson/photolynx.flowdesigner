﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;

namespace Flow.GreenScreen.Renderer
{
  public class DefocusCommand
  {
    int _radius;

    public DefocusCommand(int radius, double strengthModifier)
    {
      AllowTransparencyIncrease = false;
      StrengthModifier = strengthModifier;
      _radius = radius;
    }

    public bool AllowTransparencyIncrease { get; set; }
    public double StrengthModifier { get; set; }

    public Bitmap Apply(Bitmap bitmap)
    {
      DateTime start = DateTime.Now;
      bitmap = fastblur(bitmap, _radius);
      DateTime end = DateTime.Now;
      PhotoLynx.Logging.PlLog.Logger.Info("focus rendering completed: " + (end - start).TotalMilliseconds.ToString());
      return bitmap;
    }

    Bitmap fastblur(Bitmap img, int radius)
    {
      if (radius < 1)
      {
        return img;
      }
      int bpp = 4;
      int imageWidth = img.Width;
      int imageHeight = img.Height;
      int imageWidthMaxIndex = imageWidth - 1;
      int imageHeightMaxIndex = imageHeight - 1;
      int pixelCount = imageWidth * imageHeight;
      int div = radius + radius + 1;
      //int[] redArray = new int[pixelCount];
      //int[] greenArray = new int[pixelCount];
      //int[] blueArray = new int[pixelCount];
      int[] tranArray = new int[pixelCount];
      int tranSum, t1, t2, currentRadiusRow, imagePixelIndex, imageRowIndexModifier;
      int[] vmin = new int[Math.Max(imageWidth, imageHeight)];
      int[] vmax = new int[Math.Max(imageWidth, imageHeight)];


      BitmapData data = img.LockBits(new Rectangle(0, 0, img.Width, img.Height), ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb);
      byte[] pix = new byte[data.Stride * data.Height];
       System.Runtime.InteropServices.Marshal.Copy(data.Scan0, pix, 0, pix.Count());

      
      int[] divisionTable = new int[256 * div];
      for (int i = 0; i < 256 * div; i++)
      {
        divisionTable[i] = (i / div);
      }

      imageRowIndexModifier = imagePixelIndex = 0;

      for (int row = 0; row < imageHeight; row++)
      {
        tranSum = 0;
        for (int i = -radius; i <= radius; i++)
        {//sum within radius
          tranSum += pix[((imagePixelIndex + Math.Min(imageWidthMaxIndex, Math.Max(i, 0))) * bpp) + 3];
        }
        for (int column = 0; column < imageWidth; column++)
        {
          tranArray[imagePixelIndex] = divisionTable[tranSum];

          if (row == 0)
          {
            vmin[column] = Math.Min(column + radius + 1, imageWidthMaxIndex);
            vmax[column] = Math.Max(column - radius, 0);
          }
          t1 = pix[((imageRowIndexModifier + vmin[column]) * bpp) + 3];
          t2 = pix[((imageRowIndexModifier + vmax[column]) * bpp) + 3];

          tranSum += t1 - t2;

          imagePixelIndex++;
        }
        imageRowIndexModifier += imageWidth;
      }

      for (int column = 0; column < imageWidth; column++)
      {
        tranSum = 0;
        currentRadiusRow = -radius * imageWidth;
        for (int i = -radius; i <= radius; i++)
        {
          imagePixelIndex = Math.Max(0, currentRadiusRow) + column;
          tranSum += tranArray[imagePixelIndex];
          currentRadiusRow += imageWidth;
        }
        imagePixelIndex = column;
        for (int row = 0; row < imageHeight; row++)
        {
          if (divisionTable[tranSum] < pix[(imagePixelIndex * bpp) + 3] || AllowTransparencyIncrease)
          {
            if (StrengthModifier != 0)
            {
              int value = pix[(imagePixelIndex * bpp) + 3];
              int newValue = divisionTable[tranSum];

              pix[(imagePixelIndex * bpp) + 3] = (byte)Math.Max(divisionTable[tranSum] - ((value - newValue) * StrengthModifier),0);
            }
            else
            {
              pix[(imagePixelIndex * bpp) + 3] = (byte)(divisionTable[tranSum]);
            }
          }
          if (column == 0)
          {
            vmin[row] = Math.Min(row + radius + 1, imageHeightMaxIndex) * imageWidth;
            vmax[row] = Math.Max(row - radius, 0) * imageWidth;
          }
          t1 = column + vmin[row];
          t2 = column + vmax[row];

          tranSum += tranArray[t1] - tranArray[t2];

          imagePixelIndex += imageWidth;
        }
      }

      System.Runtime.InteropServices.Marshal.Copy(pix, 0, data.Scan0, pix.Count());
      img.UnlockBits(data);

      return img;
    }
  }
}
