﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Security.Cryptography;
using Flow.Lib.Activation;

namespace Flow.GreenScreen.Renderer
{
    public class GreenScreenCacheData
    {
        public string ForegroundPath { get; set; }
        public string RenderPath { get; set; }
        public int Orientation { get; set; }
        public string BackgroundPath { get; set; }
        public string GreenScreenSettings { get; set; }

        public override string ToString()
        {
            return ForegroundPath + RenderPath
                + Orientation.ToString() + BackgroundPath
                + GreenScreenSettings;
        }

        public string GetSecureHash()
        {
            return SecureHash(ToString());
        }

        private string SecureHash(string inputString)
        {
            var inBytes = UTF8Encoding.UTF8.GetBytes(inputString);
            var outBytes = new MD5CryptoServiceProvider().ComputeHash(inBytes);
            return UTF8Encoding.UTF8.GetString(outBytes);
        }

    }

    public class GreenScreenCache
    {
        public static void Initialize(string ImagePath)
        {
            var fi = new FileInfo(ImagePath);
            string dir = Path.Combine(fi.Directory.FullName, "PlCache");
            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);
            dir = Path.Combine(dir, "GreenScreen");
            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);
        }

        public static string GetCacheDirectory(string ImagePath)
        {
            var fi = new FileInfo(ImagePath);
            string dir = Path.Combine(fi.Directory.FullName, "PlCache");
            dir = Path.Combine(dir, "GreenScreen");
            return dir;
        }

        public static bool CachedImageRenderExists(GreenScreenCacheData cacheData)
        {
            // TODO: also keep some in memory

            // check for a stored hash
            if (!File.Exists(cacheData.RenderPath + ".$data"))
                return false;

            // check if stored hash matches current hash
            string cachedHash = File.ReadAllText(cacheData.RenderPath + ".$data");
            return cachedHash == cacheData.GetSecureHash();
        }

        public static void StoreCachedImageRender(GreenScreenCacheData cacheData)
        {
            // TODO: also keep some in memory

            // write the hash to a file
            File.WriteAllText(cacheData.RenderPath + ".$data", cacheData.GetSecureHash());
    
        }
    }
}
