﻿/* Renders children rendering layers onto a single surface */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Media.Imaging;
using System.Windows.Controls;
using System.Windows;

namespace Flow.GreenScreen.Renderer
{
  public class RenderingLayerGroup
  {
    public RenderingLayerGroup()
    {
      _children = new List<RenderingLayer>();
    }

    protected List<RenderingLayer> _children { get; set; }

    public void AddLayer(RenderingLayer layer)
    {
      _children.Add(layer);
      layer.OnRendered += new RenderingLayer.OnRenderedDelegate(layer_OnRendered);
    }

    void layer_OnRendered(RenderingLayer sender, Geometry region)
    {
      foreach (RenderingLayer child in _children)
      {
        if (child != sender)
        {
          child.ExludeRegion(region);
        }
      }
    }

    internal void SetVisibleOpacity(double opacity)
    {
      foreach (RenderingLayer child in _children)
      {
        child.VisibleOpacity = opacity;
      }
    }

    internal void SetSize(double width, double height)
    {
      foreach (RenderingLayer child in _children)
      {
        child.Width = width;
        child.Height = height;
      }
    }

    internal void Clear()
    {
      foreach (RenderingLayer child in _children)
      {
        child.Clear();
      }
    }
  }
}
