﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;

using System.Threading;
using System.Windows.Threading;

using System.Windows.Shapes;
using System.Windows.Controls;

namespace Flow.GreenScreen.Renderer
{/*
  public class BrushToolRenderer
  {
    public BrushToolRenderer(Point location, Shape cursorShape, Dispatcher dispatcher)
    {
      _location = location;
      _cursorShape = cursorShape;
      _dispatcher = dispatcher;
    }

    public BrushToolRenderer(Point location, Point lastLocation, double radius, bool isBackground, RenderingLayer background,
      RenderingLayer foreground, Geometry newGeometry, Dispatcher dispatcher)
    {
      _newGeometry = newGeometry;
      _location = location;
      _radius = radius;
      _backgroundLayer = background;
      _foregroundLayer = foreground;
      _isBackground = isBackground;
      _dispatcher = dispatcher;
    }

    protected delegate void RenderDelegate();

    protected Dispatcher _dispatcher;
    protected Geometry _brush;
    protected Geometry _newGeometry;
    protected Point _location;
    protected double _radius;
    protected RenderingLayer _backgroundLayer;
    protected RenderingLayer _foregroundLayer;
    protected bool _isBackground;
    protected Shape _cursorShape;

    protected DispatcherPriority RenderingPriority { get { return DispatcherPriority.Normal; } }

    public void BeginRenderMouseMove()
    {
      _dispatcher.BeginInvoke(RenderingPriority, new RenderDelegate(RenderMouseMove));
    }

    private void RenderMouseMove()
    {
      try
      {
        if (_isBackground)
        {
          _backgroundLayer.DrawRegion(_newGeometry, 1);
        }
        else
        {
          _foregroundLayer.DrawRegion(_newGeometry, 1);
        }
      }
      catch (Exception e)
      {
        PhotoLynx.Logging.PlLog.Logger.Info("BrushToolRenderer.RenderMouseMove: " + e.Message);
      }
    }

    internal void BeginRenderMouseDown()
    {
      _dispatcher.BeginInvoke(RenderingPriority, new RenderDelegate(RenderMouseDown));
    }

    private void RenderMouseDown()
    {
      try
      {
        if (_isBackground)
        {
          _backgroundLayer.DrawRegion(_newGeometry, 1);
        }
        else
        {
          _foregroundLayer.DrawRegion(_newGeometry, 1);
        }
      }
      catch (Exception e)
      {
        PhotoLynx.Logging.PlLog.Logger.Info("BrushToolRenderer.RenderMouseDown: " + e.Message);
      }
    }

    private void SetCursorPosition()
    {
      PhotoLynx.Logging.PlLog.Logger.Info("Cursor");
      SetCanvasPosition(_cursorShape, _location.X - (_cursorShape.Width / 2.0), _location.Y - (_cursorShape.Height / 2.0));
    }

    private void SetCanvasPosition(Shape shape, double x, double y)
    {
      Canvas.SetTop(shape, y);
      Canvas.SetLeft(shape, x);
    }

    internal void BeginSetCursorPosition()
    {
      _dispatcher.BeginInvoke(RenderingPriority, new RenderDelegate(SetCursorPosition));
    }
  }*/
}
