﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.GreenScreen.Data;
using System.IO;
using System.Drawing;
using Microsoft.Win32;

namespace Flow.GreenScreen.Renderer
{
    public class GreenScreenRenderCache
    {
        string m_cachePath;

        public GreenScreenRenderCache(string imagePath)
        {
            m_cachePath = Path.GetDirectoryName(imagePath);
            m_cachePath = Path.Combine(m_cachePath, 
                Path.Combine( Path.Combine("PlCache","GreenScreen"),
                Path.GetFileNameWithoutExtension(imagePath)));
            m_cachePath = ConvertNetwork(m_cachePath);
        }

        public string CachedImage
        {
            get { return m_cachePath + ".png"; }
        }

        protected string CachedData
        {
            get { return m_cachePath + ".$data"; }
        }

        public bool IsUpToDate(SelectionConfiguration currentConfig)
        {
            if (!File.Exists(CachedImage) || !File.Exists(CachedData))
                return false;
            try
            {
              SelectionConfiguration savedConfig = (SelectionConfiguration)ObjectSerializer.DeserializeFile(CachedData, typeof(SelectionConfiguration));
              if (savedConfig.Equals(currentConfig))
                return true;
            }
            catch
            {//error in CachedData
              File.Delete(CachedData);
            }
            return false;
        }

        public void SaveToCache(Bitmap image, SelectionConfiguration data)
        {
            if (!Directory.Exists(Path.GetDirectoryName(Path.GetDirectoryName(CachedImage))))
                Directory.CreateDirectory(Path.GetDirectoryName(Path.GetDirectoryName(CachedImage)));

            if (!Directory.Exists(Path.GetDirectoryName(CachedImage)))
                Directory.CreateDirectory(Path.GetDirectoryName(CachedImage));

            image.Save(CachedImage, System.Drawing.Imaging.ImageFormat.Png);
            ObjectSerializer.SerializeFile(data, CachedData);
        }

        public static string ConvertNetwork(string filename)
        {
            if (filename == "")
            {
                return (filename);
            }

            string root;
            try
            {
                root = new System.IO.FileInfo(filename).Directory.Root.FullName;
            }
            catch (Exception e)
            {
                throw (new Exception("Error getting directory root for filename: '" + filename + "'", e));
            }
            string localpath = @"\\" + System.Environment.MachineName + @"\";

            if (root.Length > localpath.Length &&
                root.Substring(0, localpath.Length).ToUpper() == localpath.ToUpper() &&
                root.Substring(localpath.Length).Length == 1)
            {
                return (root.Substring(localpath.Length) + ":" + filename.Substring(root.Length));
            }
            else
            {
                return (filename);
            }
        }

        public Bitmap LoadCachedImage()
        {
            return new Bitmap(CachedImage);
        }

        public bool IsGreenScreenCacheEnabled
        {
            get
            {
                return GreenScreenSettings.Instance.EnableGreenScreenCaching.Value;
            }
        }
    }
}
