﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace Flow.GreenScreen.Renderer
{
  public class AlphaCurve
  {
    public static byte[] CreateTable(Point[] points)
    {
      byte[] table = new byte[256];
      int index = 0;
      double t = 0;

      List<double> xArray = new List<double>();
      List<double> yArray = new List<double>();
      xArray.Add(0);
      yArray.Add(0);
      foreach (Point point in points)
      {
        xArray.Add(point.X);
        yArray.Add(point.Y);
      }
      xArray.Add(255);
      yArray.Add(255);

      while (index < table.Count())
      {
        if (t > 1)
        {
          t = 1;
        }
        //Point point = SolveCubicBezierAtT(t, pointArray);
        double x = SolveParametricBezierAtT(t, xArray.ToArray());
        if (x > 255)
        {
          x = 255;
        }

        if (x >= index)
        {
          double y = SolveParametricBezierAtT(t, yArray.ToArray());
          if (y > 255)
          {
            y = 255;
          }
          else if (y < 0)
          {
            y = 0;
          }
          while (x >= index && index < table.Count())
          {
            table[index] = (byte)y;
            index++;
          }
        }
        if (t >= 1)
        {
          for (; index < table.Count(); index++)
          {
            table[index] = 255;
          }
        }
        t += .001;
      }

      return table;
    }

    protected static double SolveParametricBezierAtT(double t, double[] points)
    {
      return SolveParametricBezierAtTRecusiveWorker(t, points, 0, points.Count() - 1);
    }

    protected static double SolveParametricBezierAtTRecusiveWorker(double t, double[] points, int itteration, int n)
    {
      if (itteration > n)
      {
        return 0;
      }
      double currentItterationValue = Choose(n, itteration) * points[itteration] *
        Math.Pow(1 - t, n - itteration) * Math.Pow(t, itteration);
      return currentItterationValue +
        SolveParametricBezierAtTRecusiveWorker(t, points, itteration + 1, n);
    }

    protected static double Choose(int n, int k)
    {
      if (k == 0)
      {
        return 1;
      }
      return Permute(n, k) / Factorial(k);
    }

    protected static int Factorial(int n)
    {
      int factorial = 1;
      for (int i = 2; i <= n; i++)
      {
        factorial *= i;
      }
      return factorial;
    }

    protected static int Permute(int n, int k)
    {
      int permute = 1;
      for (int i = n; i > n - k; i--)
      {
        permute *= i;
      }
      return permute;
      //return Factorial(n) / Factorial(n - k);
    }
  }
}
