﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows;
using System.Collections;
using System.IO;
using PhotoLynx.ImageCore;
using Flow.GreenScreen.Data;
using System.Drawing.Drawing2D;
using Flow.GreenScreen.Panels;
using System.Diagnostics;
//using System.Drawing;
//using System.Drawing.Drawing2D;

namespace Flow.GreenScreen.Renderer
{
  public class RenderingLayer
  {
    public RenderingLayer(bool garbageCollection)
    {
      InteractiveRendering = true;
      _surface = new System.Windows.Shapes.Rectangle();
      _surface.Height = 0;
      _surface.Width = 0;
      _surface.Fill = new ImageBrush();
      _surface.Opacity = 0;
      VisibleOpacity = .5;
      _surface.OpacityMask = Brushes.Transparent;
      _surface.IsHitTestVisible = false;

      GarbageCollection = garbageCollection;
    }

    public RenderingLayer(DrawingVisual parent, bool gargabeCollection)
      : this(gargabeCollection)
    {
      parent.Children.Add(_surface);
    }

    public RenderingLayer(Panel parent, bool garbageCollection)
      : this(garbageCollection)
    {
      parent.Children.Add(_surface);
    }

    public delegate void OnRenderedDelegate(RenderingLayer sender, Geometry drawing);

    public event OnRenderedDelegate OnRendered;

    protected System.Windows.Shapes.Rectangle _surface;
    public System.Drawing.Region ClipRegion;
    public bool GarbageCollection { get; set; }
    public double DpiX { get { return ((_surface.Fill as ImageBrush).ImageSource as BitmapSource).DpiX; } }
    public double DpiY { get { return ((_surface.Fill as ImageBrush).ImageSource as BitmapSource).DpiY; } }
    public double Height { get { return _surface.Height; } set { _surface.Height = value; } }
    public double Width { get { return _surface.Width; } set { _surface.Width = value; } }
    public bool InteractiveRendering { get; set; }
    public double VisibleOpacity
    {
      get { return _surface.Opacity; }
      set { _surface.Opacity = value; }
    }

    public bool HasClip
    {
      get
      {
        if (_surface.Clip == null)
        {
          return false;
        }
        return true;
      }
    }

    public System.Windows.Media.Brush Brush
    {
      get
      {
        return _surface.Fill.Clone();
      }
    }

    public Geometry Clip
    {
      get { return _surface.Clip; }
      set { _surface.Clip = value; }
    }

    //public void SetFill(System.Windows.Media.Brush brush, IEnumerable<Selection> selections)
    //{
    //  SetFill(brush, selections, GreenScreenPanel.RenderWidth, GreenScreenPanel.RenderHeight);
    //}



    public void SetFill(System.Windows.Media.Brush brush, IEnumerable<Selection> selections, double width, double height)
    {
      _surface.Fill = brush;
      _surface.Width = width;
      _surface.Height = height;
    }
    
    public bool DisplayingCurrentFile(string imageFilePath)
    {
      if (_surface.Fill == null)
      {
        return false;
      }
      if((_surface.Fill is ImageBrush) && (_surface.Fill as ImageBrush).ImageSource == null)
      {
        return false;
      }
      if (_surface.Fill is DrawingBrush)
      {
        return false;
      }

      if (_surface.Fill is ImageBrush)
      {
        ImageBrush brush = _surface.Fill as ImageBrush;
        if (brush.ImageSource is WriteableBitmap)
        {
          return false;
        }

        /*if ((brush.ImageSource is CroppedBitmap) &&
          ((brush.ImageSource as CroppedBitmap).Source as BitmapImage).UriSource.Equals(imageFilePath))
        {
          return true;
        }
        if ((brush.ImageSource is BitmapImage) && 
          (brush.ImageSource as BitmapImage).UriSource.Equals(imageFilePath))
        {
          return true;
        }*/
      }
      else
      {
        return false;
      }
      /*
      string oldPath = ((_surface.Fill as ImageBrush).ImageSource as BitmapImage).UriSource.AbsoluteUri;
      _surface.Fill = null;
      if (GarbageCollection)
      {
        try
        {
          File.Delete(oldPath);
        }
        catch { }
      }*/
      return false;
    }

    public Rect GetRectangle()
    {
      return new Rect(0, 0, _surface.Width, _surface.Height);
    }

    internal void DisplaySelectionMasks(bool isBackground)
    {
      if (isBackground)
      {
        _surface.Fill = Brushes.Red;
      }
      else
      {
        _surface.Fill = Brushes.Blue;
      }
    }

    internal void DisplayRender(ImageSource source)
    {
      _surface.Fill = new ImageBrush(source);
    }

    internal void IncludeRegion(Geometry region)
    {
      try
      {
        if (!region.IsFrozen && InteractiveRendering)
        {
          //region.Freeze();
        }
        if (_surface.OpacityMask != null)
        {
          _surface.OpacityMask = null;
        }

        if (InteractiveRendering)
        {
          if (_surface.Clip == null)
          {
            _surface.Clip = region;
          }
          else
          {
            _surface.Clip = new CombinedGeometry(GeometryCombineMode.Union, _surface.Clip, region);
            //_surface.Clip.Freeze();
          }
        }
        else
        {//riplynx rendering
          if (ClipRegion == null)
          {
            ClipRegion = new System.Drawing.Region(new System.Drawing.RectangleF(0, 0, (float)_surface.Width, (float)_surface.Height));
          }

          GraphicsPath path = new GraphicsPath();

          if (region is EllipseGeometry)
          {
            path.AddEllipse((float)(region as EllipseGeometry).Center.X,
              (float)(region as EllipseGeometry).Center.Y,
              (float)(region as EllipseGeometry).RadiusX * 2,
              (float)(region as EllipseGeometry).RadiusY * 2);
          }


        }
        if (OnRendered != null)
        {
          OnRendered(this, region);
        }
      }
      catch
      {
        PhotoLynx.Logging.PlLog.Logger.Info("LEGACY: There was an error creating a selection region. Probable cause is use of a job opened with an older gs2 version");
      }
    }

    internal void ExludeRegion(Geometry region)
    {
      if (!region.IsFrozen && InteractiveRendering)
      {
        //region.Freeze();
      }
      if (_surface.Clip != null)
      {
        _surface.Clip = new CombinedGeometry(GeometryCombineMode.Exclude, _surface.Clip, region);
        if (InteractiveRendering)
        {
          //_surface.Clip.Freeze();
        }
      }
    }


    internal void Clear()
    {
      _surface.Clip = null;
      _surface.OpacityMask = Brushes.Transparent;
    }
  }
}
