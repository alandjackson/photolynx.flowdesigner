﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.GreenScreen.Data;
using System.Drawing;
using Microsoft.Win32;
using System.IO;
using CPIGreenScreen.Util;
using Flow.GreenScreen.Util;
using System.Windows;
using System.ComponentModel;
using System.Drawing.Imaging;

namespace Flow.GreenScreen.Renderer
{
    public class CPIRenderer
    {
        string _bgPath;
        string _imgPath;
        string _configXml;

        public CPIRenderer(string imgPath, string bgPath, string configXml)
        {
            _bgPath = bgPath;
            _imgPath = imgPath;
            _configXml = configXml;
        }

        public void Render()
        {
            Render(null);
        }

        public void Render(ProgressChangedEventHandler progress)
        {
            using (Bitmap originalImage = new Bitmap(_imgPath))
            {
                string savePath = GetSaveRenderedImagePath();
                RenderToFile(originalImage, _configXml,
                  _bgPath, _imgPath, savePath, progress);
            }
        }

        /// <summary>
        /// Builds the next rendered image path to save to
        /// </summary>
        /// <returns></returns>
        protected string GetSaveRenderedImagePath()
        {
            string savePath = CPISettings.Instance.CompletedFolder;
            int index = 0;
            while (File.Exists(Path.Combine(savePath,
              Path.GetFileNameWithoutExtension(_imgPath) + "_" + index.ToString() +
              Path.GetExtension(_imgPath))))
            {
                index++;
            }
            return Path.Combine(savePath,
              Path.GetFileNameWithoutExtension(_imgPath) + "_" + index.ToString() +
              Path.GetExtension(_imgPath));
        }

        public static void RenderToFile(GreenScreenCacheData gsInfo)
        {

            using (System.Drawing.Bitmap originalImage =
                new System.Drawing.Bitmap(gsInfo.ForegroundPath))
            {
                originalImage.RotateFlip(GetRotateFlip(gsInfo.Orientation));
                CPIRenderer.RenderToFile(originalImage,
                    gsInfo.GreenScreenSettings, gsInfo.BackgroundPath,
                    gsInfo.ForegroundPath, gsInfo.RenderPath, null);
            }
        }

        private static System.Drawing.RotateFlipType GetRotateFlip(int orientation)
        {
            if (orientation == 90)
                return System.Drawing.RotateFlipType.Rotate90FlipNone;
            else if (orientation == 180)
                return System.Drawing.RotateFlipType.Rotate180FlipNone;
            else if (orientation == 270)
                return System.Drawing.RotateFlipType.Rotate270FlipNone;
            else
                return System.Drawing.RotateFlipType.RotateNoneFlipNone;
        }


        /// <summary>
        /// Runs the isolated GS render either in a process or in memory
        /// </summary>
        /// <param name="originalImage"></param>
        /// <param name="configXml"></param>
        /// <param name="bgPath"></param>
        /// <param name="progress"></param>
        /// <returns></returns>
        public static void RenderToFile(Bitmap originalImage,
            string configXml, string bgPath, string foregroundPath, string savePath,
            ProgressChangedEventHandler progress)
        {
            if (string.IsNullOrEmpty(bgPath))
            {
                //MessageBox.Show("Please select a background.", "No Background");
                return;
            }

            using (Bitmap renderedBitmap = RendererToBitmap(
                originalImage, configXml, bgPath, foregroundPath, progress))
            {
                if (savePath.ToUpper().EndsWith("JPG") || savePath.ToUpper().EndsWith("JPEG"))
                {
                    // Set the compression parameter of our encoder
                    EncoderParameters parms = new EncoderParameters(1);
                    parms.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Compression, 95L);

                    renderedBitmap.Save(savePath, GetImageEncoder("JPEG"), parms);
                }
                else
                {
                    renderedBitmap.Save(savePath);
                }
            }
        }

        static ImageCodecInfo GetImageEncoder(string imageType)
        {
            imageType = imageType.ToUpperInvariant();
            foreach (ImageCodecInfo info in ImageCodecInfo.GetImageEncoders())
            {
                if (info.FormatDescription == imageType)
                {
                    return info;
                }
            }
            return null;
        }

        /// <summary>
        /// Runs the isolated GS render in the current process
        /// </summary>
        /// <param name="originalImage"></param>
        /// <param name="configXml"></param>
        /// <param name="bgPath"></param>
        /// <param name="progress"></param>
        /// <returns></returns>
        protected static Bitmap RendererToBitmap(Bitmap originalImage,
          string configXml, string bgPath, string foregroundPath, ProgressChangedEventHandler progress)
        {
            return IsolatedGreenScreenRenderer.IsolatedRenderImage(
              originalImage, configXml, bgPath, foregroundPath, progress);
        }

        /// <summary>
        /// Spawns a new process to run the isolated GS render
        /// </summary>
        /// <param name="originalImage"></param>
        /// <param name="configXml"></param>
        /// <param name="bgPath"></param>
        /// <param name="progress"></param>
        /// <returns></returns>
        protected static void RendererExternally(
            Bitmap originalImage, string configXml, string bgPath, string foregroundPath, string savePath,
            ProgressChangedEventHandler progress)
        {
            ExternalGsRenderData externalData = new ExternalGsRenderData();
            externalData.OriginalImageFilename = Path.GetTempFileName();
            externalData.ConfigXml = configXml;
            externalData.BackgroundPath = bgPath;
            externalData.RenderedImageFilename = savePath;

            string commandFilePath = Path.GetTempFileName();

            try
            {
                ObjectSerializer.SerializeFile(externalData, commandFilePath);

                string exePath = ProcessUtil.GetExePath("CPIGreenScreenRenderer.exe");
                if (string.IsNullOrEmpty(exePath))
                {
                    throw new ApplicationException("Could not find renderer exe: " +
                                                   "CPIGreenScreenRenderer.exe");
                }

                originalImage.Save(externalData.OriginalImageFilename,
                                   System.Drawing.Imaging.ImageFormat.Png);

                ProcessUtil.RunProcess(exePath, '"' + commandFilePath + '"', true);
            }
            finally
            {
                FileUtil.SafeDeleteFile(externalData.OriginalImageFilename);
                FileUtil.SafeDeleteFile(commandFilePath);
            }
        }

    }
}
