﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Flow.GreenScreen.Data;

namespace Flow.GreenScreen.Dialogs
{
    /// <summary>
    /// Interaction logic for EditPresets.xaml
    /// </summary>
    public partial class EditPresets : Window
    {
        public EditPresets()
        {
            InitializeComponent();
        }

        private void uxOk_Click(object sender, RoutedEventArgs e)
        {
            Window.GetWindow(this).DialogResult = true;
        }

        private void uxCancel_Click(object sender, RoutedEventArgs e)
        {
            Window.GetWindow(this).DialogResult = false;
        }

        private void uxNew_Click(object sender, RoutedEventArgs e)
        {
            var vm = ViewModel;
            if (vm == null) return;

            vm.Presets.Add(new ColorDropoutPreset() 
            { 
                Name="New", 
                Blue = vm.DefaultBlue,
                Green = vm.DefaultGreen,
                Red = vm.DefaultRed
            });
            vm.Selected = vm.Presets[vm.Presets.Count - 1];
        }

        private void uxDelete_Click(object sender, RoutedEventArgs e)
        {
            var vm = ViewModel;
            if (vm == null || vm.Selected == null) return;

            vm.Presets.Remove(vm.Selected);
            if (vm.Presets.Count > 0)
                vm.Selected = vm.Presets[0];
        }

        EditPresentsViewModel ViewModel
        {
            get { return DataContext as EditPresentsViewModel; }
        }

    }
}
