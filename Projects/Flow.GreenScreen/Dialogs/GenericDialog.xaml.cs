﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using NUnit.Framework;
using System.Threading;

namespace PhotoLynx.CommonWpfControls.Dialogs
{
  public enum BrowseType
  {
    Folder,
    File
  }

  class FieldBrowsePair
  {
    public TextBox FieldTextBox;
    public Button BrowseButton;

    public FieldBrowsePair(TextBox textBox, Button button)
    {
      FieldTextBox = textBox;
      BrowseButton = button;
    }
  }

  /// <summary>
  /// Interaction logic for GenericDialog.xaml
  /// </summary>
  public partial class GenericDialog : Window
  {
    protected Dictionary<string, object> _optionMappings;
    protected int _dialogOptionCount;

    public GenericDialog(UserControl ownner)
    {
      InitializeComponent();

      this.Owner = Owner;
      _optionMappings = new Dictionary<string, object>();
      //this.SizeChanged += new SizeChangedEventHandler(GenericDialog_SizeChanged);
    }

    static double DefaultTextBoxWidth = 400;
    static double DefaultFieldHeight = 24;
    static double PaddingFactor = 1.2;

    public int Result { get; set; }

    //void GenericDialog_SizeChanged(object sender, SizeChangedEventArgs e)
    //{
    //  rectangle.Width = this.Width;
    //  rectangle.Height = this.Height;
    //}

    public string GetTextFieldValue(string fieldTitle)
    {
      foreach (object value in _optionMappings.Values)
      {
        if (value is FieldBrowsePair)
        {
          if ((value as FieldBrowsePair).FieldTextBox.Name == fieldTitle)
          {
            return (value as FieldBrowsePair).FieldTextBox.Text;
          }
        }
      }

      throw (new ArgumentException("Field title " + fieldTitle + " does not exist in dialog " + this.Title));
    }

    public void SetTextFieldValue(string fieldTitle, string newValue)
    {
      foreach (object value in _optionMappings.Values)
      {
        if (value is FieldBrowsePair)
        {
          if ((value as FieldBrowsePair).FieldTextBox.Name == fieldTitle)
          {
            (value as FieldBrowsePair).FieldTextBox.Text = newValue;
            return;
          }
        }
      }

      throw (new ArgumentException("Field title " + fieldTitle + " does not exist in dialog " + this.Title));
    }

    public void AddDialogButton(string buttonText, int map)
    {
      if (_dialogOptionCount == 0)
      {
        mainGrid.RowDefinitions.Add(new RowDefinition());
        this.Height += DefaultFieldHeight * PaddingFactor;
      }
      
      if(_dialogOptionCount >= mainGrid.ColumnDefinitions.Count)
      {
        mainGrid.ColumnDefinitions.Add(new ColumnDefinition());
      }
      _optionMappings.Add(buttonText, map.ToString());

      Button newButton = CreateButton(buttonText);
      newButton.VerticalAlignment = VerticalAlignment.Top;
      newButton.Click += new RoutedEventHandler(newButton_Click);
      mainGrid.Children.Add(newButton);
      Grid.SetColumn(newButton, _dialogOptionCount);
      Grid.SetRow(newButton, mainGrid.RowDefinitions.Count - 1);

      this.Width += newButton.Width * PaddingFactor;

      _dialogOptionCount++;
    }

    public new bool ShowDialog()
    {
      return base.ShowDialog().Value;
    }

    Button CreateButton(string text)
    {
      Button newButton = new Button();
      newButton.Width = text.Length * 8;
      newButton.Height = DefaultFieldHeight;
      newButton.Content = text;
      return newButton;
    }

    public void AddBrowseField(string defaultPath, string fieldTitle, BrowseType browseType)
    {
      mainGrid.RowDefinitions.Add(new RowDefinition());
      this.Height += DefaultFieldHeight * PaddingFactor;

      while (mainGrid.ColumnDefinitions.Count < 2)
      {
        mainGrid.ColumnDefinitions.Add(new ColumnDefinition());
      }

      TextBox textField = new TextBox();
      textField.HorizontalAlignment = HorizontalAlignment.Right;
      textField.Width = DefaultTextBoxWidth * .9;
      textField.Height = DefaultFieldHeight;
      textField.Name = fieldTitle.Replace(" ","");
      mainGrid.Children.Add(textField);
      Grid.SetColumn(textField, 0);
      Grid.SetRow(textField, mainGrid.RowDefinitions.Count - 1);
      Grid.SetColumnSpan(textField, mainGrid.ColumnDefinitions.Count - 1);
      textField.Text = defaultPath;
      if (mainGrid.ColumnDefinitions[0].MinWidth < DefaultTextBoxWidth)
      {
        mainGrid.ColumnDefinitions[0].MinWidth = DefaultTextBoxWidth;
        this.Width += DefaultTextBoxWidth * PaddingFactor;
      }

      Button browseButton = CreateButton("Browse");
      mainGrid.Children.Add(browseButton);
      Grid.SetColumn(browseButton, mainGrid.RowDefinitions.Count);
      if (mainGrid.ColumnDefinitions[mainGrid.RowDefinitions.Count - 1].MinWidth < browseButton.Width)
      {
        mainGrid.ColumnDefinitions[mainGrid.RowDefinitions.Count - 1].MinWidth = browseButton.Width;
        this.Width += browseButton.MinWidth * PaddingFactor;
      }
      Grid.SetRow(browseButton, mainGrid.RowDefinitions.Count - 1);

      _optionMappings.Add("FieldBrowsePair" + Convert.ToString(mainGrid.RowDefinitions.Count - 1), new FieldBrowsePair(textField, browseButton));

      if (browseType == BrowseType.Folder)
      {
        browseButton.Click += new RoutedEventHandler(folderBrowseButton_Click);

      }
      else if (browseType == BrowseType.File)
      {
        throw (new NotImplementedException("Browse type file not implemented"));
      }
    }

    void folderBrowseButton_Click(object sender, RoutedEventArgs e)
    {
      System.Windows.Forms.FolderBrowserDialog fbd = new System.Windows.Forms.FolderBrowserDialog();
      fbd.ShowNewFolderButton = true;
      fbd.SelectedPath = (_optionMappings["FieldBrowsePair" + Grid.GetRow((Button)sender).ToString()] as FieldBrowsePair).FieldTextBox.Text;
      fbd.Description = "Select folder.";
      if (fbd.ShowDialog() != System.Windows.Forms.DialogResult.OK)
      {
        return;
      }

      (_optionMappings["FieldBrowsePair" + Grid.GetRow((Button)sender).ToString()] as FieldBrowsePair).FieldTextBox.Text = fbd.SelectedPath;
    }

    public new string Title
    {
      get { return LabelTitle.Content.ToString(); }
      set { LabelTitle.Content = value; }
    }

    void newButton_Click(object sender, RoutedEventArgs e)
    {
      Result = Convert.ToInt32(_optionMappings[((Button)sender).Content.ToString()]);
      this.Hide();
    }
  }

  [TestFixture]
  public class GenericDialogTest
  {
    [Test]
    public void DisplayTest()
    {
      Thread runningThread = new Thread(new ThreadStart(RunDisplayTest));
      runningThread.SetApartmentState(ApartmentState.STA);
      runningThread.Start();
      runningThread.Join();
    }

    private void RunDisplayTest()
    {
      GenericDialog gd = new GenericDialog(null);
      gd.Title = "Test Dialog";
      gd.AddBrowseField(@"C:\", "Test Browse Field", BrowseType.Folder);
      gd.AddDialogButton("Okay", 0);
      gd.AddDialogButton("Done2", 1);
      gd.ShowDialog();
    }
  }
}
