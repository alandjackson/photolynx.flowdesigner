﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Flow.GreenScreen.Dialogs
{
  /// <summary>
  /// Interaction logic for BackgroundDialog.xaml
  /// </summary>
  public partial class BackgroundDialog : Window
  {
    new public enum DialogResult
    {
      ApplyToAll,
      AutoAssign,
      Cancel
    }

    public BackgroundDialog()
    {
      InitializeComponent();
      InitializeClass();
      InitializeEvents();
    }

    private void InitializeEvents()
    {
      buttonApplyCurrentToAll.Click += new RoutedEventHandler(buttonApplyCurrentToAll_Click);
      buttonAutoAssign.Click += new RoutedEventHandler(buttonAutoAssign_Click);
      buttonCancel.Click += new RoutedEventHandler(buttonCancel_Click);
    }

    void buttonCancel_Click(object sender, RoutedEventArgs e)
    {
      this.Close();
    }

    void buttonAutoAssign_Click(object sender, RoutedEventArgs e)
    {
      Result = DialogResult.AutoAssign;
      this.Close();
    }

    void buttonApplyCurrentToAll_Click(object sender, RoutedEventArgs e)
    {
      Result = DialogResult.ApplyToAll;
      this.Close();
    }

    private void InitializeClass()
    {
      Result = DialogResult.Cancel;
    }



    public DialogResult Result;
  }
}
