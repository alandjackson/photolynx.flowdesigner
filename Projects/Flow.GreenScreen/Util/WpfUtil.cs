﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace Flow.GreenScreen.Util
{
  public class WpfUtil
  {
    public static void MoveOnCanvas(UIElement e, Point p)
    {
      Canvas.SetTop(e, Canvas.GetTop(e) + p.Y);
      Canvas.SetLeft(e, Canvas.GetLeft(e) + p.X);
    }
    
    public static void SetOnCanvas(UIElement e, Point p)
    {
      Canvas.SetTop(e, p.Y);
      Canvas.SetLeft(e, p.X);
    }
  }
}
