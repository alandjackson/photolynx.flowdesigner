﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Drawing;
using System.Collections;

using PlSize = System.Drawing.Size;
using System.IO;

namespace PhotoLynx.CommonWpfControls.Util
{
  public class ImageFileVirtualizationManager
  {
    public bool Active;

    System.Drawing.Size _zeroSize;
    public System.Drawing.Size ZeroSize { get { return _zeroSize; } }
    System.Drawing.Size _originalSize;
    System.Drawing.Size _currentVirtSize;
    public System.Drawing.Size CurrentVirtSize { get { return _currentVirtSize; } }
    double _widthStep;
    double _startFactor;
    double _divisionStep;

    const double _DIVISIONS = 3;

    public ImageFileVirtualizationManager(System.Windows.Size stageZeroSize)
    {
      Active = true;
      _zeroSize = new System.Drawing.Size((int)stageZeroSize.Width, (int)stageZeroSize.Height);
    }

    public void NewImage(System.Drawing.Size size)
    {
      _originalSize = size;
      _currentVirtSize = new System.Drawing.Size(0, 0);

      if (size.Height < size.Width)
      {
        _startFactor = _zeroSize.Height / (double)size.Height;
      }
      else
      {
        _startFactor = _zeroSize.Width / (double)size.Width;
      }

      _divisionStep = (1 - _startFactor) / _DIVISIONS;
      _widthStep = size.Width * _divisionStep;
    }

    public bool IsCurrentSizeInvalid(System.Windows.Size newRenderSize)
    {
      if (!Active)
      {
        return false;
      }

      if (_currentVirtSize.Width == _originalSize.Width)
      {
        return false;
      } 
      if (newRenderSize.Width >= _currentVirtSize.Width + _widthStep)
      {
        return true;
      }
      return false;
    }

    public Bitmap VirtualizeImageBitmap(Bitmap originalImage, System.Drawing.Size renderSize)
    {
      if (!Active)
      {
        return (Bitmap)originalImage.Clone();
      }

      double factor = ComputeDesiredFactor(originalImage.Size, renderSize);
      if (factor >= 1)
      {
        PhotoLynx.Logging.PlLog.Logger.Info("Virtualized image at original size");
        _currentVirtSize = _originalSize;
        return new Bitmap(originalImage);
      }

      _currentVirtSize = new System.Drawing.Size((int)Math.Ceiling(originalImage.Width * factor), (int)Math.Ceiling(originalImage.Height * factor));
      PhotoLynx.Logging.PlLog.Logger.Info("Virtualized image original size: " + _originalSize.Width + ", " + _originalSize.Height + ". Virt size: " + _currentVirtSize.Width + ", " + _currentVirtSize.Height);
      return new Bitmap(originalImage, _currentVirtSize);
    }

    //protected PlSize SizeToFit(PlSize originalSize, PlSize targetSize)
    //{
    //  PlSize newSize = null;
    //  if (originalSize.Width > originalSize.Height)
    //  {
    //    newSize = new System.Drawing.Size(originalSize.Width * targetSize.Height / originalSize.Height, targetSize.Height);
    //  }
    //  else
    //  {
    //    newSize = new System.Drawing.Size(targetSize.Width, originalSize.Height * targetSize.Width / originalSize.Width);
    //  }
    //  return newSize;
    //}

    protected double ComputeDesiredFactor(PlSize originalSize, PlSize renderSize)
    {
      if (renderSize.Width > originalSize.Width || renderSize.Height > originalSize.Height)
      {
        return 1;
      }

      double factor;
      double currentFactor = _startFactor;

      if (renderSize.Height < renderSize.Width)
      {
        factor = renderSize.Height / (double)originalSize.Height;
      }
      else
      {
        factor = renderSize.Width / (double)originalSize.Width;
      }

      if (factor > _divisionStep + currentFactor)
      {
        while (currentFactor < factor)
        {
          currentFactor += _divisionStep;
        }
      }

      return currentFactor;
    }

    //protected string GetFileSuffix(double factor)
    //{
    //  return factor.ToString("0.0000").Substring(2);
    //}
  }
}
