﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace Flow.GreenScreen.Util
{
    public class InvalidBackgroundException : Exception
    {
        public InvalidBackgroundException()
        {
        }

        public InvalidBackgroundException(string msg)
            : base(msg)
        {
        }
    }

    public class GreenScreen2InstanceSettings
    {
        public GreenScreen2InstanceSettings()
        {
            ContinueOnInvalidBackground = -1;
        }

        public int ContinueOnInvalidBackground;

        protected static GreenScreen2InstanceSettings _instance;

        public static GreenScreen2InstanceSettings Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new GreenScreen2InstanceSettings();
                }
                return _instance;
            }
            set { _instance = value; }
        }

        public static bool ThrowExceptionOnInvalidBackground = false;
        

        public void VerifyContinueOnInvalidBackground()
        {
            if (ThrowExceptionOnInvalidBackground)
            {
                throw new InvalidBackgroundException("Background image not specified");
            }

            if (MessageBox.Show("There are invalid backgrounds. Continue processing anyway?", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
            {
                ContinueOnInvalidBackground = 1;
            }
            else
            {
                ContinueOnInvalidBackground = 0;
            }
        }
    }
}
