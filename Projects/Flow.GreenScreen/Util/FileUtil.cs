﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flow.GreenScreen.Util
{
  public class FileUtil
  {
    public static void SafeDeleteFile(string path)
    {
      try
      {
        File.Delete(path);
      }
      catch (Exception e)
      {
        PhotoLynx.Logging.PlLog.Logger.Info("Error deleting file: " + path + ": " + e.ToString());
      }
    }
  }
}
