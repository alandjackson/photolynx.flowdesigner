﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace Flow.GreenScreen.Util
{
  public class GarbageManager
  {
    [System.Runtime.InteropServices.DllImport("gdi32.dll")]
    protected static extern bool DeleteObject(IntPtr hObject);

    public static bool DeleteGDIObject(IntPtr obj)
    {
      return DeleteObject(obj);
    }

    public static void ForceGC()
    {
      GC.Collect(GC.MaxGeneration, GCCollectionMode.Forced);
      GC.WaitForPendingFinalizers();  
    }
  }

  public class GDICounter : IDisposable
  {
    private static GDICounter _instance;
    public static GDICounter Instance
    {
      get
      {
        if (_instance == null)
        {
          _instance = new GDICounter();
        }
        return _instance;
      }
      set { _instance = value; }
    }

    private const int GR_GDIOBJECTS = 0, GR_USEROBJECTS = 1;
    [DllImport("User32", ExactSpelling = true, CharSet = CharSet.Auto)]
    public static extern int GetGuiResources(IntPtr hProcess, int uiFlags);
    private int initialHandleCount;

    public GDICounter()
    {
      initialHandleCount = 0;

      initialHandleCount = GetGDIObjects();
    }

    public int GetDifference()
    {
      int currentHandleCount = GetGDIObjects();
      int change = 0;
      if (initialHandleCount != currentHandleCount)
      {
        change = currentHandleCount - initialHandleCount;
        initialHandleCount = currentHandleCount;
      }
      return change;
    }

    private int GetGDIObjects()
    {
      IntPtr processHandle = System.Diagnostics.Process.GetCurrentProcess().Handle;

      if (processHandle != IntPtr.Zero)
      {
        return (int)GetGuiResources(processHandle, GR_GDIOBJECTS);
      }

      return 0;
    }

    public void Dispose()
    {
      int currentHandleCount = GetGDIObjects();
      if (initialHandleCount != currentHandleCount)
      {
        int change = currentHandleCount - initialHandleCount;
        if (change > 0)
        {
          Debug.Fail("Handle count changed by: " + change.ToString() + new StackTrace().ToString());
        }
      }
    }
  }
}
