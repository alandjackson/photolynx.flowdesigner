﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Win32;

namespace Flow.GreenScreen.Util
{
    public class GsRegUtil
    {
        public static readonly RegistryKey DefaultRegistry = Registry.CurrentUser;
        public static readonly string GreenScreenSection = @"SOFTWARE\PhotoLynx\GreenScreen";
        public static readonly string ImageMatchSection = @"SOFTWARE\PhotoLynx\ImageMatch\Settings";
        public static readonly string PhotoLynxSection = @"SOFTWARE\PhotoLynx\General\Settings";


        public static bool GetBoolValue(string keyName, bool defaultValue)
        {
            return GetBoolValue(DefaultRegistry, GreenScreenSection, keyName, defaultValue);
        }

        public static bool GetBoolValue(RegistryKey regKey, string sectionName, 
            string keyName, bool defaultValue)
        {
            string strValue = GetStrValue(keyName, defaultValue.ToString());
            bool boolValue;
            if (Boolean.TryParse(strValue, out boolValue)) return boolValue;
            return defaultValue;
        }

        public static int GetIntValue(string keyName, int defaultValue)
        {
            return GetIntValue(DefaultRegistry, GreenScreenSection, keyName, defaultValue);
        }

        public static int GetIntValue(RegistryKey regKey, string sectionName,
            string keyName, int defaultValue)
        {
            string strValue = GetStrValue(keyName, defaultValue.ToString());
            int intValue;

            if (Int32.TryParse(strValue, out intValue)) return intValue;
            return defaultValue;
        }

        public static string GetStrValue(RegistryKey regKey, string sectionName, string keyName)
        {
            return GetStrValue(DefaultRegistry, GreenScreenSection, keyName, "");
        }

        public static string GetStrValue(string keyName)
        {
            return GetStrValue(DefaultRegistry, GreenScreenSection, keyName);
        }

        public static string GetStrValue(string keyName, string defaultValue)
        {
            return GetStrValue(DefaultRegistry, GreenScreenSection, keyName, defaultValue);
        }

        public static string GetStrValue(RegistryKey regKey, string sectionName, 
            string keyName, string defaultValue)
        {
            var reg = regKey.OpenSubKey(sectionName);
            if (reg == null)
                return defaultValue;

            return reg.GetValue(keyName, defaultValue).ToString();
        }

        public static void SetIntValue(string keyName, int value)
        {
            SetIntValue(DefaultRegistry, GreenScreenSection, keyName, value);
        }

        public static void SetIntValue(RegistryKey regKey, string sectionName, 
            string keyName, int value)
        {
            SetStrValue(DefaultRegistry, GreenScreenSection, keyName, value.ToString());
        }

        public static void SetBoolValue(string keyName, bool value)
        {
            SetBoolValue(DefaultRegistry, GreenScreenSection, keyName, value);
        }

        public static void SetBoolValue(RegistryKey regKey, string sectionName, 
            string keyName, bool value)
        {
            SetStrValue(regKey, sectionName, keyName, value.ToString());
        }

        public static void SetStrValue(RegistryKey regKey, string sectionName, 
            string keyName, string value)
        {
            var reg = regKey.OpenSubKey(sectionName, true);
            if (reg == null)
                reg = regKey.CreateSubKey(sectionName);

            reg.SetValue(keyName, value);
        }
    }
}
