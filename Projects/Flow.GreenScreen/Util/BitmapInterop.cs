﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Imaging;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Media;
using System.Windows.Interop;
using System.IO;
using System.Windows;

namespace Flow.GreenScreen.Util
{
  public class BitmapInterop
  {
    public static List<IntPtr> GDIPointers
    {
      get
      {
        if (_hBitmapHolder == null)
        {
          _hBitmapHolder = new List<IntPtr>();
        }
        return _hBitmapHolder;
      }
      set { _hBitmapHolder = value; }
    }
    protected static List<IntPtr> _hBitmapHolder;

    public static BitmapSource CreateBitmapSourceFromHBitmap(IntPtr bitmap, IntPtr palette, Int32Rect rect, BitmapSizeOptions options)
    {
      GDIPointers.Add(bitmap);
      return System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(bitmap, palette, rect, options);
    }

    public static void ClearGDIObjects()
    {
      foreach (IntPtr ptr in GDIPointers)
      {
        try
        {
          GarbageManager.DeleteGDIObject(ptr);
        }
        catch { }
      }
      GDIPointers = null;
    }

    public static BitmapSource ToBitmapSource(Bitmap bitmap, out IntPtr hBitmap)
    {
      hBitmap = bitmap.GetHbitmap();
      return ToBitmapSource(hBitmap, bitmap.Width, bitmap.Height);
    }

    public static BitmapSource ToBitmapSource(IntPtr hBitmap, int width, int height)
    {
      return Imaging.CreateBitmapSourceFromHBitmap(hBitmap, IntPtr.Zero,
              new System.Windows.Int32Rect(0, 0, width, height), BitmapSizeOptions.FromEmptyOptions());
    }

    internal static BitmapImage ToBitmapImage(Bitmap renderedBitmap)
    {
      MemoryStream ms = new MemoryStream();
      renderedBitmap.Save(ms, ImageFormat.Png);
      ms.Position = 0;


      BitmapImage image = new BitmapImage();
      image.BeginInit();
      image.CreateOptions = BitmapCreateOptions.PreservePixelFormat;
      image.StreamSource = ms;
      image.CacheOption = BitmapCacheOption.None;
      image.EndInit();
      return image;
    }

    internal static IntPtr GetHBitmap(Bitmap bitmap)
    {
      IntPtr ptr = bitmap.GetHbitmap();
      GDIPointers.Add(ptr);
      return ptr;
    }
  }
}
