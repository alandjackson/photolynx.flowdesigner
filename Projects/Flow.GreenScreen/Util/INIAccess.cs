﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.IO;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Windows.Shapes;
using System.Windows;



namespace Flow.GreenScreen.Util
{
  public class INIAccess
  {
    [DllImport("kernel32.dll", EntryPoint = "GetPrivateProfileIntA")]
    static extern public int GetPrivateProfileInt(string sectionName, string keyName, int defaultVal, string fileName);
    [DllImport("kernel32.dll", EntryPoint = "GetPrivateProfileStringA", SetLastError = true)]
    static extern public int GetPrivateProfileString(string sectionName, string keyName, string defaultVal, StringBuilder returnVal, int returnSize, string fileName);
    [DllImport("kernel32.dll", EntryPoint = "WritePrivateProfileStringA", SetLastError = true)]
    static extern public int WritePrivateProfileStringA(string sectionName, string keyName, string val, string fileName);
  }
}
