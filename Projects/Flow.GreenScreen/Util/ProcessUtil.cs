﻿using System;
using System.IO;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flow.GreenScreen.Util
{
  public class ProcessUtil
  {
    public static string GetExePath(string exeFilename)
    {
      string testFilename = exeFilename;
      if (!File.Exists(testFilename)) testFilename = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, exeFilename);
      if (!File.Exists(testFilename)) return "";
      return testFilename;
    }
  
    public static int RunProcess(string path, string args, bool wait)
    {
      string output = null;
      return RunProcess(path, args, wait, ref output);
    }

    public static int RunProcess(string path, string args, bool wait, ref string stdOutput)
    {
      ProcessStartInfo psi = new ProcessStartInfo(path, args);
      psi.CreateNoWindow = true;
      psi.UseShellExecute = false;
      if (stdOutput != null) { psi.RedirectStandardOutput = true; }
      Process p = Process.Start(psi);
      if (stdOutput != null) { stdOutput = p.StandardOutput.ReadToEnd(); }
      if (wait)
      {
        p.WaitForExit();
        return (p.ExitCode);
      }
      return (0);
    }
  }
}
