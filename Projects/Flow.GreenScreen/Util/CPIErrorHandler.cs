﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace Flow.GreenScreen.Util
{
  public class CPIErrorHandler
  {
    public static void CatchException(Exception e)
    {
      PhotoLynx.Logging.PlLog.Logger.Info("Application Error: " + e.ToString());
      MessageBox.Show("Application Error: " + e.ToString());
    }
    
    
  }
}
