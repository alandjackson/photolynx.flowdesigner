﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace Panels
{
  public class FloatingPanel : UserControl
  {

    public FloatingPanel()
    {
      Window = new Window();
      Window.WindowStyle = WindowStyle.None;
      Window.ResizeMode = ResizeMode.NoResize;
    }

    public Window Window
    {
      get;
      set;
    }

    public void Show(Window parent)
    {
      Window.Owner = parent;
      StackPanel stack = new StackPanel();
      stack.Children.Add(this);
      Window.Content = stack;
      Window.Width = this.Width;
      Window.Height = this.Height;
      Window.Show();

    }
  }
}
