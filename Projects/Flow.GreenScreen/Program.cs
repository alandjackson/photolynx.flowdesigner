﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Flow.GreenScreen.Data;
using Flow.GreenScreen.Renderer;

namespace Flow.GreenScreen
{
    class Program
    {
        [STAThread]
        static void Main()
        {
            try
            {
                var stdIn = ReadStdIn();
                var gsInfo = ObjectSerializer.DeserializeOrDefault<GreenScreenCacheData>(stdIn);
                CPIRenderer.RenderToFile(gsInfo);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }

        }

        static string ReadStdIn()
        {
            Stream s = Console.OpenStandardInput();
            StreamReader sr = new StreamReader(s);
            return sr.ReadToEnd();
        }
    }
}
