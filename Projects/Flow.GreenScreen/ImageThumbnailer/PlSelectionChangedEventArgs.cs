﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PhotoLynx.ImageThumbnailer
{

  public delegate void PlSelectionChangedEventHandler(object sender, PlSelectionChangedEventArgs args);

  public class PlSelectionChangedEventArgs : EventArgs
  {
    public IList RemovedItems { get; set; }
    public IList AddedItems { get; set; }

    public PlSelectionChangedEventArgs(IList removedItems, IList addedItems)
    {
      RemovedItems = removedItems;
      AddedItems = addedItems;
    }
  }
}
