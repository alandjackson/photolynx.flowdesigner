﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PhotoLynx.ImageThumbnailer
{
  /// <summary>
  /// Interaction logic for PlImageList.xaml
  /// </summary>
  public partial class PlImageList : UserControl
  {
    public PlImageList()
    {
      SelectedListBox = null;
      SelectedItem = null;
      InitializeComponent();
    }

    public static readonly DependencyProperty SelectionChangedProperty = DependencyProperty.Register(
      "SelectionChanged", typeof(SelectionChangedEventHandler), typeof(PlImageList));

    public event PlSelectionChangedEventHandler SelectionChanged;

    protected ListBox SelectedListBox { get; set; }

    protected Object _selectedItem;
    public Object SelectedItem 
    {
      get { return _selectedItem; }
      set
      {
        if (value == _selectedItem) return;
        _selectedItem = value;

        //this.thumbsVirtualizingPanel
        
        if (value != null)
          ListBox_PlSelectionChanged(this, new PlSelectionChangedEventArgs(new ArrayList(), new ArrayList(new object[] { value })));
      }
    }
    public int SelectedIndex
    {
      get 
      { 
        //PhotoLynx.Logging.PlLog.Logger.Info("PlImageList.SelectedIndex_get");  
        return (this.DataContext as IList).IndexOf(SelectedItem); 
      }
      set 
      { 
        //PhotoLynx.Logging.PlLog.Logger.Info("PlImageList.SelectedIndex_set: " + value);
        IList items = this.DataContext as IList;
        if (items != null && value >= 0 && value < items.Count)
          SelectedItem = (this.DataContext as IList)[value]; 
      }
    }

    private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      ListBox_PlSelectionChanged(sender, new PlSelectionChangedEventArgs(e.RemovedItems, e.AddedItems));
    }

    private void ListBox_PlSelectionChanged(object sender, PlSelectionChangedEventArgs e)
    {
      if (e.AddedItems.Count == 0) return;

      //PhotoLynx.Logging.PlLog.Logger.Info("ListBox Selected");
      //foreach (object selected in e.AddedItems)
      //  PhotoLynx.Logging.PlLog.Logger.Info("\tAdded: " + selected.ToString());
      //foreach (object selected in e.RemovedItems)
      //  PhotoLynx.Logging.PlLog.Logger.Info("\tRemoved: " + selected.ToString());

      ArrayList removedItems = new ArrayList();
      ArrayList addedItems = new ArrayList();

      // deselect the previous item
      if (SelectedListBox != null)
      {
        removedItems.Add(SelectedItem);
        SelectedListBox.SelectedItem = null;
      }

      // add the newly selected item
      if (sender is ListBox) SelectedListBox = sender as ListBox;
      object newSelectedItem;
      if (e.AddedItems[0] is ListBoxItem)
        newSelectedItem = GetListBoxObjectDataItem(e.AddedItems[0] as ListBoxItem);
      else
        newSelectedItem = e.AddedItems[0];
      addedItems.Add(newSelectedItem);
      _selectedItem = newSelectedItem;
      
      // throw the change event
      if (SelectionChanged != null) SelectionChanged(this, new PlSelectionChangedEventArgs(removedItems, addedItems));

    }

    protected object GetListBoxObjectDataItem(ListBoxItem lbi)
    {
      StackPanel sp = lbi.Content as StackPanel;
      if (sp == null) return null;
      DataHolderControl dhc = null;
      foreach (UIElement child in sp.Children)
      {
        //PhotoLynx.Logging.PlLog.Logger.Info("\tChild: " + child.GetType().Name);
        if (child is DataHolderControl)
          dhc = child as DataHolderControl;
      }
      if (dhc == null) return null;
      if (dhc == null) throw new ApplicationException("PlImageList.GetListBoxObject: listbox does not contain DataHolderControl");
      return dhc.DataContext;
    }
  }
}
