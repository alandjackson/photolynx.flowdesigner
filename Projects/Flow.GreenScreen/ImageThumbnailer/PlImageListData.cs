﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using PhotoLynx.ImageCore;

namespace PhotoLynx.ImageThumbnailer
{
  public class PlImageListData : ObservableCollection<PlImage>
  {

    public void AddImageDirectory(string dirName, string searchPattern, SearchOption searchOption)
    {
      foreach (FileInfo fi in new DirectoryInfo(dirName).GetFiles(searchPattern, searchOption))
      {
        Add(new PlImage(fi.FullName));
      }
    }

  }
}
