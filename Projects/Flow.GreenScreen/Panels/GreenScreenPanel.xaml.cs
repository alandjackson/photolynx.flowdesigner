using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using PhotoLynx.CommonWpfControls.Util;
using Flow.GreenScreen.Data;
using Flow.GreenScreen.Managers;
using Flow.GreenScreen.Util;
using Flow.GreenScreen.Managers.Panels;
using System.Drawing;

namespace Flow.GreenScreen.Panels
{
    /// <summary>
    /// Interaction logic for GreenScreenPanel.xaml
    /// </summary>
    public partial class GreenScreenPanel : UserControl
    {

        public int StartingIndex = 0;
        PanelData _data;
        public PanelData Data
        {
            get { return _data; }
            set
            {
                _data = value;
                this.DataContext = _data;
            }
        }

        public GreenScreenPanel()
            : this(VariantType.CPI)
        {
        }

        public GreenScreenPanel(VariantType type)
        {
            LoadedAction = null;
            System.IO.Directory.SetCurrentDirectory(AppDomain.CurrentDomain.BaseDirectory);
            InitializeComponent();
            InitializeClass(type);
            SetupEvents();
            InitializeComponentUser();
        }


        public RecordsetInfoPanel recordsetInfoPanel { get; set; }
        public CPIRotatePanel cpiRotatePanel { get; set; }

        /// <summary>
        /// Initialize class - user generated code :)
        /// </summary>
        protected void InitializeComponentUser()
        {
            if (Data.Variant == VariantType.ImageMatch)
            {
                recordsetInfoPanel = new RecordsetInfoPanel();
                this.recordsetInfoParent.Children.Add(recordsetInfoPanel);
            }
            else if (Data.Variant == VariantType.CPI)
            {
                cpiRotatePanel = new CPIRotatePanel();
                this.recordsetInfoParent.Children.Add(cpiRotatePanel);

                BitmapImage startBmp = BitmapInterop.ToBitmapImage(new Bitmap(
                  (int)GreenScreenPanel.RenderWidth, (int)GreenScreenPanel.RenderHeight));

                imageRendered.Source = startBmp;
                imageBackground.Source = startBmp;
                imageForeground.Source = startBmp;
                imageWorking.Source = startBmp;
            }
        }



        public Window Window { get; set; }

        public static int DefaultDPI
        {
            get { return 300; }
        }

        public static double RenderWidth
        {
            get { return 414.72; }
        }

        public static double RenderHeight
        {
            get { return 619.52; }
        }

        public void ShowDialog()
        {
            ShowDialog(true);
        }

        public Action LoadedAction { get; set; }

        public void ShowDialog(bool virtualizeDisplay)
        {
            InitializeWindow();
            Initialize(virtualizeDisplay);
            Window.ShowDialog();
        }

        public Window InitializeWindow()
        {
            // Remove items from a previous window
            if (Window != null)
            {
                (Window.Content as StackPanel).Children.Clear();
            }

            Window = new Window();
            Window.WindowStartupLocation = WindowStartupLocation.CenterScreen;

            StackPanel panel = new StackPanel();
            panel.Children.Add(this);
            Window.Content = panel;
            Window.Title = CPIGreenScreen.Util.CPISettings.Instance.AppDisplayName;
            //Window.Icon = BitmapFrame.Create(new Uri(
            //  Path.Combine(AppDomain.CurrentDomain.BaseDirectory,
            //               @"res\photolynx.generic.ico")));

            //Window.Loaded += new RoutedEventHandler(Window_Loaded);
            return Window;
        }

        public void Initialize()
        {
            Initialize(true);
        }

        public void Initialize(bool virtualizeDisplay)
        {
            ManagerTracker.Initialize(this, Data.Variant);
            ManagerTracker.ImageFileVirtualizationManager.Active = virtualizeDisplay;
        }

        string GetTmpFilename(string ext)
        {
            string tmpFilename = Path.GetTempFileName();
            File.Delete(tmpFilename);
            return Path.ChangeExtension(tmpFilename, ext);
        }

        void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (LoadedAction != null) LoadedAction();
            Window w = sender as Window;

            w.Topmost = true;
            w.WindowState = WindowState.Normal;
            w.Visibility = Visibility.Visible;
            w.Activate();
            w.Topmost = false;

            
            ManagerTracker.DataManager.UpdateSelectedImage(StartingIndex);

            //ManagerTracker.MainDisplayManager.Panel.thumbsImageList.SelectedIndex =
            //  ManagerTracker.ImageListManager.StartingIndex;
            //w.Activate();
        }

        public int SelectedIndex
        {
            set
            {
                ManagerTracker.DataManager.UpdateSelectedImage(value);
            }
        }

        private void InitializeClass(VariantType type)
        {
            //BitmapInterop.GDIPointers.Add(IntPtr.Zero);//original bitmap placeholder

            if (type == VariantType.ImageMatch)
            {
                Data = new PanelData();
            }
            else if (type == VariantType.CPI)
            {
                Data = new CPIPanelData();
            }
            Data.AllowClose = false;
            Data.Variant = type;
            DataContext = Data;
        }

        private void SetupEvents()
        {
        }

        //private void UpdateZoomPosition(Point location)
        //{
        //  double ratio = _data.CurrentImage.PixelHeight / imageDisplay.ActualHeight;
        //  double x = location.X * ratio;
        //  double y = location.Y * ratio;
        //  double zoomCursorXOffset = 0;
        //  double zoomCursorYOffset = 0;
        //  if (x <= _zoomPanel.CursorOffset)
        //  {
        //    zoomCursorXOffset = (x - _zoomPanel.CursorOffset) * (_zoomPanel.canvasZoomPanel.Width / _zoomPanel.ZoomDimension);
        //    x = _zoomPanel.CursorOffset;
        //  }
        //  else if (x > _data.CurrentImage.PixelWidth - _zoomPanel.CursorOffset)
        //  {
        //    zoomCursorXOffset = (x - _data.CurrentImage.PixelWidth + _zoomPanel.CursorOffset) * (_zoomPanel.canvasZoomPanel.Width / _zoomPanel.ZoomDimension);
        //    if (_data.CurrentImage.PixelWidth - _zoomPanel.CursorOffset < _zoomPanel.CursorOffset)
        //    {
        //      x = _zoomPanel.CursorOffset;
        //    }
        //    else
        //    {
        //      x = _data.CurrentImage.PixelWidth - _zoomPanel.CursorOffset;
        //    }
        //  }

        //  if (y < _zoomPanel.CursorOffset)
        //  {
        //    zoomCursorYOffset = (y - _zoomPanel.CursorOffset) * (_zoomPanel.canvasZoomPanel.Height / _zoomPanel.ZoomDimension);
        //    y = _zoomPanel.CursorOffset;
        //  }
        //  else if (y > _data.CurrentImage.PixelHeight - _zoomPanel.CursorOffset)
        //  {
        //    zoomCursorYOffset = (y - _data.CurrentImage.PixelHeight + _zoomPanel.CursorOffset) * (_zoomPanel.canvasZoomPanel.Height / _zoomPanel.ZoomDimension);
        //    if (_data.CurrentImage.PixelHeight - _zoomPanel.CursorOffset < _zoomPanel.CursorOffset)
        //    {
        //      y = _zoomPanel.CursorOffset;
        //    }
        //    else
        //    {
        //      y = _data.CurrentImage.PixelHeight - _zoomPanel.CursorOffset;
        //    }
        //  }

        //  _zoomPanel.ZoomOrigin = new Point(x - _zoomPanel.CursorOffset, y - _zoomPanel.CursorOffset);
        //  if (_zoomPanel.CursorShape is Rectangle)
        //  {
        //    SetCanvasPosition(_zoomPanel.CursorShape, _zoomPanel.canvasZoomPanel.Width / 2 + zoomCursorXOffset,
        //        _zoomPanel.canvasZoomPanel.Height / 2 - _zoomPanel.CursorShape.Height + zoomCursorYOffset);
        //  }
        //  else
        //  {
        //    SetCanvasPosition(_zoomPanel.CursorShape, _zoomPanel.canvasZoomPanel.Width / 2 + zoomCursorXOffset - _zoomPanel.CursorShape.Width / 2,
        //        _zoomPanel.canvasZoomPanel.Height / 2 - _zoomPanel.CursorShape.Width / 2 + zoomCursorYOffset);
        //  }
        //}

        public static void SaveImageToPNG(BitmapSource image, string path)
        {
            FileStream stream = new FileStream(path, FileMode.Create);
            PngBitmapEncoder pngEncoder = new PngBitmapEncoder();
            pngEncoder.Frames.Add(BitmapFrame.Create(image));
            pngEncoder.Save(stream);
            stream.Close();
        }


        //[DllImport(@"C:\Program Files\PhotoLynx\bin\PhotoLynx.GSV2.dll")]
        //public static extern void ApplyAlgorithm(string image, string grn, string back, string backgroundOut, int dpi);
    }
}
