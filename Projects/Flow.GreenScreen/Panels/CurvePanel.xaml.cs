﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Animation;
using Flow.GreenScreen.Util;

namespace Flow.GreenScreen.Panels
{
    /// <summary>
    /// Interaction logic for PanelTracker.CurvePanel.xaml
    /// </summary>
    public partial class CurvePanel : UserControl
    {
        public CurvePanel(Point[] controlPoints)
        {
            InitializeComponent();
            InitializeClass(controlPoints);
            SetupEvents();
        }

        public CurvePanel()
            : this(new Point[] { new Point(76, 1), new Point(105, 87) })
        {
        }

        public delegate void ControlPointsChangedDelegate(Point[] newControlPoints);
        public ControlPointsChangedDelegate ControlPointsChanged;

        private void SetupEvents()
        {
            _iconPoint1.MouseDown += new MouseButtonEventHandler(_iconPoint_MouseDown);
            _iconPoint1.MouseMove += new MouseEventHandler(_iconPoint_MouseMove);
            _iconPoint1.MouseUp += new MouseButtonEventHandler(_iconPoint_MouseUp);

            _iconPoint2.MouseDown += new MouseButtonEventHandler(_iconPoint_MouseDown);
            _iconPoint2.MouseMove += new MouseEventHandler(_iconPoint_MouseMove);
            _iconPoint2.MouseUp += new MouseButtonEventHandler(_iconPoint_MouseUp);
        }

        void _iconPoint_MouseUp(object sender, MouseButtonEventArgs e)
        {
            Point pointLocation = new Point();
            if (sender == _iconPoint1)
            {
                pointLocation = (Point)_bezierSegment.GetValue(BezierSegment.Point1Property);
            }
            else if (sender == _iconPoint2)
            {
                pointLocation = (Point)_bezierSegment.GetValue(BezierSegment.Point2Property);
            }
            (((Ellipse)sender).ToolTip as ToolTip).Content = pointLocation.X.ToString() + ", " + pointLocation.Y.ToString();


            _mouseDown = false;
            ((Ellipse)sender).ReleaseMouseCapture();

            if (ControlPointsChanged != null)
            {
                ControlPointsChanged(new Point[]{
        new Point((double)_iconPoint1.GetValue(Canvas.LeftProperty), (double)_iconPoint1.GetValue(Canvas.TopProperty)),
        new Point((double)_iconPoint2.GetValue(Canvas.LeftProperty), (double)_iconPoint2.GetValue(Canvas.TopProperty))});
            }
        }

        void _iconPoint_MouseMove(object sender, MouseEventArgs e)
        {
            if (_mouseDown)
            {
                Point location = e.GetPosition(canvasGraph);
                SetCanvasPosition((Ellipse)sender, location.X, location.Y);

                if (sender == _iconPoint1)
                {
                    _bezierSegment.SetValue(BezierSegment.Point1Property, location);
                }
                else if (sender == _iconPoint2)
                {
                    _bezierSegment.SetValue(BezierSegment.Point2Property, location);
                }
            }
        }

        void _iconPoint_MouseDown(object sender, MouseButtonEventArgs e)
        {
            _mouseDown = true;
            ((Ellipse)sender).CaptureMouse();
        }

        private void InitializeClass(Point[] controlPoints)
        {
            _mouseDown = false;

            Path path = new Path();
            path.Stroke = Brushes.Black;
            path.StrokeThickness = 1;

            PathFigure figure = new PathFigure();
            figure.StartPoint = new Point(0, 0);
            if (controlPoints.Length >= 2)
            {
                _bezierSegment = new BezierSegment(controlPoints[0], controlPoints[1], new Point(255, 255), true);
                figure.Segments.Add(_bezierSegment);
            }
            else
            {
                _bezierSegment = new BezierSegment();
                figure.Segments.Add(_bezierSegment);
            }

            PathGeometry pathGeo = new PathGeometry();
            pathGeo.Figures.Add(figure);

            path.Data = pathGeo;
            canvasGraph.Children.Add(path);

            _iconPoint1 = new Ellipse();
            _iconPoint1.Width = 10;
            _iconPoint1.Height = 10;
            _iconPoint1.Fill = Brushes.ForestGreen;
            _iconPoint1.ToolTip = new ToolTip();
            if (controlPoints.Length >= 1)
            {
                ((ToolTip)_iconPoint1.ToolTip).Content = controlPoints[0].X.ToString() + ", " + controlPoints[0].Y.ToString(); ;
                SetCanvasPosition(_iconPoint1, controlPoints[0].X, controlPoints[0].Y);
            }
            canvasGraph.Children.Add(_iconPoint1);

            _iconPoint2 = new Ellipse();
            _iconPoint2.Width = 10;
            _iconPoint2.Height = 10;
            _iconPoint2.Fill = Brushes.LightSeaGreen;
            _iconPoint2.ToolTip = new ToolTip();
            if (controlPoints.Length >= 2)
            {
                ((ToolTip)_iconPoint2.ToolTip).Content = controlPoints[1].X.ToString() + ", " + controlPoints[1].Y.ToString(); ;
                SetCanvasPosition(_iconPoint2, controlPoints[1].X, controlPoints[1].Y);
            }
            canvasGraph.Children.Add(_iconPoint2);

            imageGraphGrid.Source = BitmapInterop.CreateBitmapSourceFromHBitmap(Properties.Resources.curveGraphGrid.GetHbitmap(),
              IntPtr.Zero, new Int32Rect(0, 0, Properties.Resources.curveGraphGrid.Width, Properties.Resources.curveGraphGrid.Height), BitmapSizeOptions.FromEmptyOptions());
        }

        private void SetCanvasPosition(Ellipse shape, double x, double y)
        {
            Canvas.SetTop(shape, y - shape.Height / 2);
            Canvas.SetLeft(shape, x - shape.Width / 2);
        }

        public Point[] GetControlPoints()
        {
            return new Point[]{
        new Point((double)_iconPoint1.GetValue(Canvas.LeftProperty) + _iconPoint1.Width / 2, (double)_iconPoint1.GetValue(Canvas.TopProperty) + _iconPoint1.Height / 2),
        new Point((double)_iconPoint2.GetValue(Canvas.LeftProperty) + _iconPoint2.Width / 2, (double)_iconPoint2.GetValue(Canvas.TopProperty) + _iconPoint2.Height / 2)};
        }

        protected bool _mouseDown;
        protected BezierSegment _bezierSegment;
        protected Ellipse _iconPoint1;
        protected Ellipse _iconPoint2;

        internal void SetControlPoints(Point[] point)
        {
            SetCanvasPosition(_iconPoint1, point[0].X, point[0].Y);
            SetCanvasPosition(_iconPoint2, point[1].X, point[1].Y);
            _bezierSegment.SetValue(BezierSegment.Point1Property, point[0]);
            _bezierSegment.SetValue(BezierSegment.Point2Property, point[1]);
        }
    }
}
