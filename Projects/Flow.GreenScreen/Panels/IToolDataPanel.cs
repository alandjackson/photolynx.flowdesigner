﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flow.GreenScreen.Panels
{
  public struct ToolboxButtons
  {

    public const string ForegroundAdjustmentTool = "ForegroundAdjustmentTool";
    public const string HandTool = "HandTool";
    public const string PolygonSelectionTool = "PolygonSelectionTool";
    public const string ColorPickerTool = "ColorPickerTool";
    public const string BrushTool = "BrushTool";
    public const string ZoomTool = "ZoomTool";
    public const string BezierTool = "BezierTool";
    public const string BackgroundDefinitionTool = "BackgroundDefinitionTool";
  }

  public interface IToolDataPanel
  {
    string GetName();//returns name to match to button in toolbox
  }
}
