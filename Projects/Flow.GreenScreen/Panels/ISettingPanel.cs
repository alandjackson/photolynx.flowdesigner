﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;

namespace Flow.GreenScreen.Panels
{
  public interface ISettingsPanel
  {
    Canvas CanvasData { get; }
  }
}
