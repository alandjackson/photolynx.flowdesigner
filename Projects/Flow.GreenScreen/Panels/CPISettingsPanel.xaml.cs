﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Flow.GreenScreen.Panels
{
  /// <summary>
  /// Interaction logic for CPISettingsPanel.xaml
  /// </summary>
  public partial class CPISettingsPanel : UserControl, ISettingsPanel
  {
    Canvas _hiddenCanvasData;

    public CPISettingsPanel()
    {
      _hiddenCanvasData = new Canvas();
      InitializeComponent();
    }

    #region ISettingsPanel Members

    public Canvas CanvasData
    {
      get { return _hiddenCanvasData; }
    }

    #endregion
  }
}
