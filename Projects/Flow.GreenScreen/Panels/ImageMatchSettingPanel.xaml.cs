﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Flow.GreenScreen.Panels
{
  /// <summary>
  /// Interaction logic for ImageMatchSettingPanel.xaml
  /// </summary>
  public partial class ImageMatchSettingPanel : UserControl, ISettingsPanel
  {
    public ImageMatchSettingPanel()
    {
      InitializeComponent();
    }

    #region ISettingPanel Members

    public Canvas CanvasData
    {
      get { return canvasData; }
    }

    #endregion
  }
}

namespace Panels
{
}