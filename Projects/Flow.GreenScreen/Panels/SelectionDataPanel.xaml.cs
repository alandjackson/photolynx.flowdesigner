﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Flow.GreenScreen.Data;

namespace Flow.GreenScreen.Panels
{
  /// <summary>
  /// Interaction logic for PolygonPanelTracker.SelectionDataPanel.xaml
  /// </summary>
  public partial class SelectionDataPanel : UserControl, IToolDataPanel
  {
    public SelectionDataPanel()
    {
      InitializeComponent();
      InitializeClass();
      SetupEvents();
    }

    public SelectionConfiguration SelectionConfiguration { get; set; }
    public Shape CursorShape { get; set; }
    public Rectangle CursorRectangle
    {
      get
      {
        return CursorShape as Rectangle;
      }
      set
      {
        CursorShape = value;
      }
    }
    public Ellipse CursorEllipse
    {
      get
      {
        return CursorShape as Ellipse;
      }
      set
      {
        CursorShape = value;
      }
    }
    public Brush FillBrush { get; set; }
    public bool IsBackgroundMode { get; set; }

    private void SetupEvents()
    {
      sliderBrushSize.ValueChanged += new RoutedPropertyChangedEventHandler<double>(sliderBrushSize_ValueChanged);
      textBoxBrushSize.TextChanged += new TextChangedEventHandler(textBoxBrushSize_TextChanged);

      sliderBrushOpacity.ValueChanged += new RoutedPropertyChangedEventHandler<double>(sliderBrushOpacity_ValueChanged);
      textBoxBrushOpacity.TextChanged += new TextChangedEventHandler(textBoxBrushOpacity_TextChanged);
    }

    void textBoxBrushOpacity_TextChanged(object sender, TextChangedEventArgs e)
    {
      int value;
      try
      {
        value = (int)Math.Ceiling(Convert.ToDouble((sender as TextBox).Text.Replace("%", "")));
        sliderBrushOpacity.ValueChanged -= new RoutedPropertyChangedEventHandler<double>(sliderBrushOpacity_ValueChanged);
        sliderBrushOpacity.Value = value;
        sliderBrushOpacity.ValueChanged += new RoutedPropertyChangedEventHandler<double>(sliderBrushOpacity_ValueChanged);
      }
      catch
      {
        
      }

      if (!(sender as TextBox).Text.Contains("%"))
      {
        textBoxBrushOpacity.TextChanged -= new TextChangedEventHandler(textBoxBrushOpacity_TextChanged);
        textBoxBrushOpacity.Text += "%";
        textBoxBrushOpacity.TextChanged += new TextChangedEventHandler(textBoxBrushOpacity_TextChanged);
      }
    }

    void sliderBrushOpacity_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
    {
      textBoxBrushOpacity.TextChanged -= new TextChangedEventHandler(textBoxBrushOpacity_TextChanged);
      textBoxBrushOpacity.Text = ((int)(e.NewValue * 100)).ToString() + "%";
      textBoxBrushOpacity.TextChanged += new TextChangedEventHandler(textBoxBrushOpacity_TextChanged);
    }

    void textBoxBrushSize_TextChanged(object sender, TextChangedEventArgs e)
    {
      int value;
      try
      {
        value = (int)Math.Ceiling( Convert.ToDouble((sender as TextBox).Text)); 
        sliderBrushSize.ValueChanged -= new RoutedPropertyChangedEventHandler<double>(sliderBrushSize_ValueChanged);
        sliderBrushSize.Value = value;
        sliderBrushSize.ValueChanged += new RoutedPropertyChangedEventHandler<double>(sliderBrushSize_ValueChanged);
        UpdateCursorBrush(value);
      }
      catch
      {
        (textBoxBrushSize as TextBox).Text = ((int)Math.Ceiling(Convert.ToDouble((e.OriginalSource as TextBox).Text))).ToString();
      }
    }

    void UpdateCursorBrush(int size)
    {
      CursorEllipse.Width = size;
      CursorEllipse.Height = size;
    }

    void sliderBrushSize_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
    {
      if (e.NewValue != Math.Ceiling(e.NewValue))
      {
        sliderBrushSize.Value = Math.Ceiling(e.NewValue);
        return;
      }

      textBoxBrushSize.TextChanged -= new TextChangedEventHandler(textBoxBrushSize_TextChanged);
      textBoxBrushSize.Text = e.NewValue.ToString();
      textBoxBrushSize.TextChanged += new TextChangedEventHandler(textBoxBrushSize_TextChanged);

      UpdateCursorBrush((int)e.NewValue);
    }

    private void InitializeClass()
    {
      IsBackgroundMode = true;
      SelectionConfiguration = new SelectionConfiguration();
      sliderBrushSize.Value = 20;
      textBoxBrushSize.Text = sliderBrushSize.Value.ToString();

      sliderBrushOpacity.Value = 1;
      textBoxBrushOpacity.Text = "100%";
    }

    internal void RefreshList()
    {
      //listBoxSelections.ItemsSource = SelectionConfiguration.DisplayedSelections;
      //listBoxSelections.Items.Refresh();
    }

    public Ellipse CreateEllipseBrush(Geometry clip, double scale)
    {
      Ellipse brush = new Ellipse();
      brush.Width = sliderBrushSize.Value * scale;
      brush.Height = sliderBrushSize.Value * scale;
      brush.Stroke = Brushes.Black;
      brush.Fill = Brushes.Transparent;
      brush.StrokeThickness = 1;
      brush.IsHitTestVisible = false;
      brush.Clip = clip;
      return brush;
    }
    public Ellipse CreateEllipseBrush(Geometry clip)
    {
      return CreateEllipseBrush(clip, 1);
    }

    public Rectangle CreateRectangleBrush(ImageSource image, Geometry clip)
    {
      Rectangle brush = new Rectangle();
      brush.IsHitTestVisible = false;
      brush.Width = 20;
      brush.Height = 20;
      brush.Fill = new ImageBrush(image);
      brush.Clip = clip;
      return brush;
    }

    #region IToolDataPanel Members

    public string GetName()
    {
      return "PolygonSelectionTool";
    }

    #endregion

  }
}
