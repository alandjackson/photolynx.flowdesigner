﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Markup;

namespace Flow.GreenScreen.Panels
{
  /// <summary>
  /// Interaction logic for RecordsetInfoPanel.xaml
  /// </summary>
  public partial class RecordsetInfoPanel : UserControl
  {
    public RecordsetInfoPanel()
    {
      InitializeComponent();
    }
  }
}
