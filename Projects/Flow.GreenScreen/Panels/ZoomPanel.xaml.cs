﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Runtime.InteropServices;
using System.ComponentModel;
using System.Windows.Interop;

namespace Flow.GreenScreen.Panels
{
  public partial class ZoomPanel : UserControl
  {
    public ZoomPanel()
    {
      InitializeComponent();

      _brush = new ImageBrush();
      _isZoomActive = true;

      Surface.Fill = _brush;
      _isZoomActive = true;

      ZoomDimension = (int)sliderZoomLevel.Value;

      SetupEvents();

      Surface.Fill = MagnifyBrush;

    }

    public delegate void CursorInvalidatedDelegate();

    public CursorInvalidatedDelegate OnCursorInvalidated;
    public double ZoomFactor
    {
      get
      {
        SetSpotInfo();
        return Math.Floor(Width / ((BitmapSource)CurrentSpotBmpSrc).Width);
      }
    }
    public ImageSource SourceImage
    {
      get
      {
        return _sourceImage;
      }
      set
      {
        _sourceImage = value;
        if (CurrentSpotBmpSrc == null)
        {
          SetSpotInfo();
        }
      }
    }
    public int CursorOffset { get; set; }
    public Shape CursorShape { get; set; }

    protected int _zoomDimension;
    protected ImageSource _sourceImage;
    protected bool _isZoomActive;
    protected Brush _brush;
    protected System.Windows.Point _cursorPoint;
    protected ImageSource _zoomedBitmap;
    protected System.Windows.Media.Color _currentCursorColor;

    private void SetupEvents()
    {
      sliderZoomLevel.ValueChanged += new RoutedPropertyChangedEventHandler<double>(sliderZoomLevel_ValueChanged);
      PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(OnSpotChange);
    }

    void sliderZoomLevel_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
    {
      ZoomDimension = (int)sliderZoomLevel.Value;
      if (OnCursorInvalidated != null)
      {
        OnCursorInvalidated();
      }
    }

    public Brush MagnifyBrush
    {
      get{ return _brush;}
      set { _brush = value; }
    }

    public void Refresh()
    {
      SetSpotInfo();
    }

    public System.Windows.Point ZoomOrigin
    {
      get { return _cursorPoint; }
      set
      {
        if (_cursorPoint != value)
        {
          _cursorPoint = value;
          SetSpotInfo();
        }
      }
    }

    public Rectangle Surface
    {
      get { return MagnifyRect; }
      set { MagnifyRect = value; }
    }

    public int ZoomDimension
    {
      get { return _zoomDimension; }

      set
      {
        if (_zoomDimension != value)
        {
          _zoomDimension = value * 2;
          CursorOffset = value;
        }
      }
    }

    public ImageSource CurrentSpotBmpSrc
    {
      get { return _zoomedBitmap; }
      set
      {
        _zoomedBitmap = value;
        OnPropertyChanged("CurrentSpotBmpSrc");
      }
    }

    public System.Windows.Media.Color CurrentCursorColor
    {
      get { return _currentCursorColor; }
      set
      {
        _currentCursorColor = value;
        OnPropertyChanged("CurrentCursorColor");
      }
    }

    private void OnSpotChange(object sender, System.ComponentModel.PropertyChangedEventArgs args)
    {
      if (_isZoomActive)
      {
        (_brush as ImageBrush).ImageSource = CurrentSpotBmpSrc;
      }
    }

    private void WidthSliderDrag(object sender, RoutedPropertyChangedEventArgs<double> args)
    {
        ZoomDimension = (int)args.NewValue;
    }

    #region ///Imported Screen Methods
    class ImportedMethods
    {

      [DllImport("gdi32.dll", EntryPoint = "DeleteDC")]
      public static extern IntPtr DeleteDC(IntPtr hDc);

      [DllImport("gdi32.dll", EntryPoint = "DeleteObject")]
      public static extern IntPtr DeleteObject(IntPtr hDc);

      [DllImport("gdi32.dll", EntryPoint = "BitBlt")]
      public static extern bool BitBlt(IntPtr hdcDest, int xDest, int yDest, int
      wDest, int hDest, IntPtr hdcSource, int xSrc, int ySrc, int RasterOp);

      [DllImport("gdi32.dll", EntryPoint = "CreateCompatibleBitmap")]
      public static extern IntPtr CreateCompatibleBitmap(IntPtr hdc, int nWidth, int nHeight);

      [DllImport("gdi32.dll", EntryPoint = "CreateCompatibleDC")]
      public static extern IntPtr CreateCompatibleDC(IntPtr hdc);

      [DllImport("gdi32.dll", EntryPoint = "SelectObject")]
      public static extern IntPtr SelectObject(IntPtr hdc, IntPtr bmp);
      [DllImport("user32.dll", EntryPoint = "GetDesktopWindow")]
      public static extern IntPtr GetDesktopWindow();

      [DllImport("user32.dll", EntryPoint = "GetDC")]
      public static extern IntPtr GetDC(IntPtr ptr);

      [DllImport("user32.dll", EntryPoint = "GetSystemMetrics")]
      public static extern int GetSystemMetrics(int abc);

      [DllImport("user32.dll", EntryPoint = "GetWindowDC")]
      public static extern IntPtr GetWindowDC(IntPtr ptr);

      [DllImport("user32.dll", EntryPoint = "ReleaseDC")]
      public static extern IntPtr ReleaseDC(IntPtr hWnd, IntPtr hDc);

    }
    #endregion


    public void SetSpotInfo()
    {
      
      CroppedBitmap cb = new CroppedBitmap(
         (BitmapSource)SourceImage, new Int32Rect((int)_cursorPoint.X, (int)_cursorPoint.Y,
          _zoomDimension, _zoomDimension));
      cb.Freeze();
      CurrentSpotBmpSrc = cb;
    }

    public System.Windows.Media.Color ConvertColor(System.Drawing.Color col)
    {
      System.Windows.Media.Color resCol = new System.Windows.Media.Color();
      resCol.A = col.A;
      resCol.R = col.R;
      resCol.G = col.G;
      resCol.B = col.B;
      return resCol;
    }

    public event PropertyChangedEventHandler PropertyChanged;

    private void OnPropertyChanged(string propName)
    {
      if (PropertyChanged != null)
      {
        PropertyChanged(this, new PropertyChangedEventArgs(propName));
      }
    }
  }
}
