﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Flow.GreenScreen.Util;
using Flow.GreenScreen.Data;
using Flow.GreenScreen.Managers;

namespace Flow.GreenScreen.Panels
{
  /// <summary>
  /// Interaction logic for PanelTracker.ColorPickerDataPanel.xaml
  /// </summary>
  public partial class ColorPickerDataPanel : UserControl, IToolDataPanel
  {
    protected PanelData _data;

    public ColorPickerDataPanel(double backgroundSliderValue, PanelData data)
    {
      _data = data;
      InitializeComponent();
      InitializeClass();

      ToolTip tt = new ToolTip();
      tt.Content = sliderBackgroundStrength.Value.ToString(".00");
      sliderBackgroundStrength.Value = backgroundSliderValue;
    }

    private void InitializeClass()
    {
      InDropColorMode = true;
      PippeteIcon = BitmapInterop.CreateBitmapSourceFromHBitmap(Properties.Resources.colorPickerToolIcon.GetHbitmap(),
        IntPtr.Zero, new Int32Rect(0, 0, Properties.Resources.colorPickerToolIcon.Width, Properties.Resources.colorPickerToolIcon.Height), BitmapSizeOptions.FromEmptyOptions());
    }

    public bool InDropColorMode { get; set; }
    public BitmapSource PippeteIcon;


    public void HideBackgroundSlider()
    {
      labelTolerance.Visibility = Visibility.Visible;
      textBoxTolerance.Visibility = Visibility.Visible;
      sliderBackgroundStrength.Visibility = Visibility.Hidden;
      labelBackgroundStrength.Visibility = Visibility.Hidden;
      checkBoxSoftenEdges.Visibility = Visibility.Hidden;
    }

    public void ShowBackgroundSlider()
    {
      labelTolerance.Visibility = Visibility.Hidden;
      textBoxTolerance.Visibility = Visibility.Hidden;
      checkBoxSoftenEdges.Visibility = Visibility.Visible;
      if (_data.AllowAdvancedTools)
      {
        sliderBackgroundStrength.Visibility = Visibility.Visible;
        labelBackgroundStrength.Visibility = Visibility.Visible;
      }
    }
    #region IToolDataPanel Members

    public string GetName()
    {
      return ToolboxButtons.ColorPickerTool.ToString();
    }

    #endregion
  }
}
