﻿using System;
using System.IO;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Imaging;
using System.Drawing;

namespace PhotoLynx.ImageCore
{

  public class ImageLoader
  {
    public event ImageEventHandler ImageLoaded;

    public static MemoryStream ReadImageToStream(string path)
    {
      byte[] buffer = System.IO.File.ReadAllBytes(path);

      MemoryStream ms1 = new MemoryStream(buffer);
      return ms1;
    }

    public BitmapImage LoadImage(string imagePath)
    {
      return LoadImage(imagePath, null, false);
    }
    
    public BitmapImage LoadImage(string imagePath, Mutex loadMutex, 
                                 bool renderOnCompletion)
    {
      BitmapImage image = null;
      try
      {
        using (Stream ms = ReadImageToStream(imagePath))
        {
          if (loadMutex != null) loadMutex.WaitOne();
          image = new BitmapImage();
          image.BeginInit();
          image.StreamSource = ms;
          image.EndInit();
          image.Freeze();
          if (image != null && ImageLoaded != null) 
            ImageLoaded(this, new ImageEventArgs(image, renderOnCompletion));
          return image;
        }
      }
      catch (Exception e)
      {
        PhotoLynx.Logging.PlLog.Logger.Info("ImageLoader.LoadImage: " + 
                                            imagePath.ToString() + 
                                            " Exception: " + e.ToString());
        return null;
      }
      finally
      {
        if (loadMutex != null) loadMutex.ReleaseMutex();
      }
    }
    
    public BitmapImage LoadImageThumbnail(string imagePath, int thumbSize)
    {
      try
      {
        // Load image as thumb
        MemoryStream ms = new MemoryStream();
        using (Stream sourceMs = ReadImageToStream(imagePath))
        {
          BitmapImage image = new BitmapImage();
          image.BeginInit();
          image.DecodePixelHeight = thumbSize;
          image.StreamSource = sourceMs;
          //image.UriSource = new Uri(imagePath);
          image.EndInit();
          image.Freeze();
          
          // save image to stream
          BitmapEncoder encoder = new PngBitmapEncoder();
          encoder.Frames.Add(BitmapFrame.Create(image));
          encoder.Save(ms);
        }
        ms.Seek(0, SeekOrigin.Begin);
        
       
        // load stream as the real bitmap image
        BitmapImage memImage = new BitmapImage();
        memImage.BeginInit();
        memImage.StreamSource = ms;
        //memImage.DecodePixelHeight = thumbSize;
        memImage.EndInit(); 
        memImage.Freeze();
        
        // dispose of the original image to release file reference
        if (memImage != null && ImageLoaded != null) 
          ImageLoaded(this, new ImageEventArgs(memImage));
        return memImage;
        
      }
      catch (Exception e)
      {
        PhotoLynx.Logging.PlLog.Logger.Info("ImageLoader.LoadImageThumbnail: " + 
          imagePath.ToString() + " Exception: " + e.ToString());
        return null;
      }
    }

    public BitmapImage LoadImageThumbnailA(string imagePath, int thumbSize)
    {
      try
      {
        //using (Stream ms = ReadImageToStream(imagePath))
        //{
        //Stream ms = ReadImageToStream(imagePath);
          BitmapImage image = new BitmapImage();
          
          image.BeginInit();
          //image.DownloadCompleted += delegate
          //{
          //  ms.Dispose();
          //};
          image.DecodePixelHeight = thumbSize;
          image.CacheOption = BitmapCacheOption.OnDemand;
          //image.StreamSource = ms;
          //image.StreamSource = new BufferedStream(File.OpenRead(imagePath), 10240);
          image.UriSource = new Uri(imagePath);
          image.EndInit();
          image.Freeze();

          

          if (image != null && ImageLoaded != null) ImageLoaded(this, new ImageEventArgs(image));
          return image;
        //}
      }
      catch (Exception e)
      {
        PhotoLynx.Logging.PlLog.Logger.Info("ImageLoader.LoadImageThumbnail: " + imagePath.ToString() + " Exception: " + e.ToString());
        return null;
      }
    }
  }
}
