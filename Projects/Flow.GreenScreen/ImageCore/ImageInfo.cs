﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;

namespace PhotoLynx.ImageCore
{
  public class ImageInfo
  {
    public ImageInfo()
    {

    }

    public ImageInfo(int pWidth, int pHeight, double diWidth, double diHeight, double dpi) : this(pWidth, pHeight, diWidth, diHeight, dpi, PixelFormats.Pbgra32) { }

    public ImageInfo(int pWidth, int pHeight, double diWidth, double diHeight, double dpi, PixelFormat format)
    {
      PixelWidth = pWidth;
      PixelHeight = pHeight;
      Width = diWidth;
      Height = diHeight;
      Dpi = dpi;
      Format = format;
    }

    public PixelFormat Format { get; set; }
    public double Dpi { get; set; }
    public int PixelWidth { get; set; }
    public int PixelHeight { get; set; }
    public double Width { get; set; }
    public double Height { get; set; }
  }
}
