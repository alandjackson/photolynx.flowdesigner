﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Imaging;
using System.Threading;

namespace PhotoLynx.ImageCore
{

  public class ImageLoaderPool
  {
    public event ImageEventHandler ImageLoaded;
    protected ImageLoader _currentImageLoader = null;

    protected Mutex _loadingMutex = new Mutex();

    public bool CancelOnNewLoad { get; set; }

    public void BeginLoadImage(string imagePath, bool renderWhenFinished)
    {
      Thread loadThread = new Thread(delegate() { LoadImage(imagePath, renderWhenFinished); });
      loadThread.Priority = ThreadPriority.BelowNormal;
      loadThread.Start();
    }

    public void LoadImage(string imagePath, bool renderWhenFinished)
    {
      if (_currentImageLoader != null) _currentImageLoader.ImageLoaded -= new ImageEventHandler(loader_ImageLoaded);

      _currentImageLoader = new ImageLoader();
      _currentImageLoader.ImageLoaded += new ImageEventHandler(loader_ImageLoaded);
      _currentImageLoader.LoadImage(imagePath, _loadingMutex, renderWhenFinished);
    }

    void loader_ImageLoaded(object sender, ImageEventArgs args)
    {
      if (ImageLoaded != null) ImageLoaded(this, args);
    }

    public void BeginLoadImageThumbnail(string imagePath, int thumbSize)
    {
      Thread loadThread = new Thread(delegate() { LoadImageThumbnail(imagePath, thumbSize); });
      loadThread.Priority = ThreadPriority.Lowest;
      loadThread.Start();
    }

    public void LoadImageThumbnail(string imagePath, int thumbSize)
    {
      _currentImageLoader = new ImageLoader();
      _currentImageLoader.ImageLoaded += new ImageEventHandler(loader_ImageLoaded);
      _currentImageLoader.LoadImageThumbnail(imagePath, thumbSize);
    }

  }
}
