﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Imaging;
using System.Threading;

namespace PhotoLynx.ImageCore
{
  public class ImageEventArgs : EventArgs
  {
    BitmapImage _image = null;

    public bool RenderOnCompletion { get; set; }

    public ImageEventArgs(BitmapImage image) : this(image, false) { }

    public ImageEventArgs(BitmapImage image, bool renderOnCompletion)
    {
      _image = image;
      RenderOnCompletion = renderOnCompletion;
    }

    public BitmapImage BitmapImage
    {
      get { return _image; }
      set { _image = value; }
    }
  }

  public delegate void ImageEventHandler(object sender, ImageEventArgs args);
}