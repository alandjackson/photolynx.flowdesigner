﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Imaging;
using System.ComponentModel;

namespace PhotoLynx.ImageCore
{
  public class PlImage : INotifyPropertyChanged
  {
    protected PhotoLynx.ImageCore.ImageInfo _imageInfo;
    protected string _path = "";

    public PlImage(string path)
    {
      ImagePath = path;
      ThumbSize = 100;
    }

    public int ThumbSize { get; set; }

    public string ImagePath 
    {
      get { return _path; }
      set
      {
        _path = value;
        _imageThumbnail = null;
      }
    }

    public string ImageFileName
    {
      get 
      { 
        PhotoLynx.Logging.PlLog.Logger.Info("Getting image filename: " + new FileInfo(ImagePath).Name);
        return new FileInfo(ImagePath).Name; 
      }
      set { }
    }


    public override string ToString()
    {
      return ("PlImage Path: " + ImagePath);
    }

    public ImageInfo ImageInfo
    {
      get
      {
        if (_imageInfo == null)
        {
          System.Drawing.Bitmap bitmap = new System.Drawing.Bitmap(ImagePath);
          _imageInfo = new ImageInfo(bitmap.Width, bitmap.Height, bitmap.Width, bitmap.Height, bitmap.HorizontalResolution);
        }
        return _imageInfo;
      }
    }

    public long ImageFileSize
    {
      get
      {
        PhotoLynx.Logging.PlLog.Logger.Info("Getting file size for: " + ImagePath);
        if (!File.Exists(ImagePath)) return -1;

        try
        {
          return new FileInfo(ImagePath).Length;
        }
        catch (Exception e)
        {
          PhotoLynx.Logging.PlLog.Logger.Info("Exception getting file size: " + e.ToString());
          return -2;
        }
      }
      set { throw new NotImplementedException(); }
    }

    protected BitmapImage _imageThumbnail = null;
    public BitmapImage ImageThumbnail
    {
      get
      {
        if (_imageThumbnail == null && File.Exists(ImagePath))
        {
          ImageLoaderPool imageLoaderPool = new ImageLoaderPool();
          imageLoaderPool.ImageLoaded += new ImageEventHandler(imageLoader_ImageLoaded);
          imageLoaderPool.BeginLoadImageThumbnail(ImagePath, ThumbSize);
            return null;
        }
        return _imageThumbnail;
      }
      set 
      {
        _imageThumbnail = value;
        if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("ImageThumbnail"));
        
        // Image info changes when it's loaded
        if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("ImageInfo"));
        
      }
    }

    void imageLoader_ImageLoaded(object sender, ImageEventArgs args)
    {
      ImageThumbnail = args.BitmapImage;
    }




    #region INotifyPropertyChanged Members

    public event PropertyChangedEventHandler PropertyChanged;

    #endregion
  }
}
