﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Collections;
using System.Xml.Serialization;

namespace Flow.GreenScreen.Data
{
  [XmlInclude(typeof(ControlPointProfile))]
  public class ControlPointProfileList
  {
    [XmlIgnore]
    public List<ControlPointProfile> List;

    public ControlPointProfile[] Array
    {
      get
      {
        return List.ToArray();
      }
      set
      {
        List = new List<ControlPointProfile>();
        foreach (ControlPointProfile item in value)
        {
          List.Add(item);
        }
      }
    }

    public ControlPointProfileList()
    {
      List = new List<ControlPointProfile>();
    }
  }

  [System.Xml.Serialization.XmlInclude(typeof(Point))]
  public class ControlPointProfile
  {
    public string Name;
    public Point[] Points;

    public ControlPointProfile() : this("", new Point[] { }) { }

    public ControlPointProfile(string name, Point[] points)
    {
      Name = name;
      Points = points;
    }

    public override string ToString()
    {
      return Name;
    }

    public static bool ExistsInArrayList(string profileName, List<ControlPointProfile> list)
    {
      foreach (ControlPointProfile profile in list)
      {
        if (profile.Name.Equals(profileName))
        {
          return true;
        }
      }
      return false;
    }
  }
}
