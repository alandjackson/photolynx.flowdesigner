﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.GreenScreen.Util;
using Microsoft.Win32;

namespace Flow.GreenScreen.Data
{
    public abstract class AbstractGsRegSetting<T>
    {
        public T Value;
        public string Name;
        public RegistryKey RegKey;
        public string Section;

        public AbstractGsRegSetting(RegistryKey regKey, string section)
        {
            RegKey = regKey;
            Section = section;
        }

        public AbstractGsRegSetting()
        {
            RegKey = GsRegUtil.DefaultRegistry;
            Section = GsRegUtil.GreenScreenSection;
        }
    }

    public class GsRegBoolSetting : AbstractGsRegSetting<bool>
    {
        public GsRegBoolSetting(RegistryKey regKey, string section, string name, bool defaultValue)
            : base(regKey, section)
        {
            Name = name;
            Value = GsRegUtil.GetBoolValue(RegKey, Section, name, defaultValue);
        }

        public GsRegBoolSetting(string name, bool defaultValue)
        {
            Name = name;
            Value = GsRegUtil.GetBoolValue(RegKey, Section, name, defaultValue);
        }

        public void SaveValue(bool v)
        {
            Value = v;
            GsRegUtil.SetBoolValue(RegKey, Section, Name, Value);
        }
    }

    public class GsRegIntSetting : AbstractGsRegSetting<int>
    {
        public int DefaultValue;

        public GsRegIntSetting(RegistryKey regKey, string section, string name, int defaultValue)
            : base(regKey, section)
        {
            Name = name;
            Value = GsRegUtil.GetIntValue(RegKey, Section, name, defaultValue);
        }

        public GsRegIntSetting(string name, int defaultValue)
        {
            Name = name;
            DefaultValue = defaultValue;
            Value = GsRegUtil.GetIntValue(RegKey, Section, name, defaultValue);
        }

        public void SaveValue(int v)
        {
            Value = v;
            GsRegUtil.SetIntValue(RegKey, Section, Name, Value);
        }

        public void ResetDefaultValue()
        {
            SaveValue(DefaultValue);
        }
    }

    public class GsRegStringSetting : AbstractGsRegSetting<string>
    {
        public string DefaultValue;

        public GsRegStringSetting(RegistryKey regKey, string section, string name)
            : base(regKey, section)
        {
            Name = name;
            Value = GsRegUtil.GetStrValue(RegKey, Section, name);
        }

        public GsRegStringSetting(string name)
            : this(name, "")
        {
        }

        public GsRegStringSetting(string name, string defaultValue)
        {
            Name = name;
            DefaultValue = defaultValue;
            Value = GsRegUtil.GetStrValue(RegKey, Section, name);
        }

        public void SaveValue(string v)
        {
            Value = v;
            GsRegUtil.SetStrValue(RegKey, Section, Name, Value);
        }

        public void ResetDefaultValue()
        {
            SaveValue(DefaultValue);
        }
    }

    public class GreenScreenSettings
    {
        protected static GreenScreenSettings _instance = null;
        public static GreenScreenSettings Instance { get { if (_instance == null) _instance = new GreenScreenSettings(); return _instance; } }

        public bool ConfirmUnsavedChanges { get; set; }
        public GsRegBoolSetting FilterDeletes = new GsRegBoolSetting("FilterDeletes", true);
        public GsRegBoolSetting FilterDuplicates = new GsRegBoolSetting("FilterDuplicates", true);
        public GsRegBoolSetting ShowCrop = new GsRegBoolSetting("ShowCrop", true);

        public GsRegIntSetting DefaultBackgroundRed = new GsRegIntSetting("DefaultBackgroundRed", 86);
        public GsRegIntSetting DefaultBackgroundBlue = new GsRegIntSetting("DefaultBackgroundBlue", 121);
        public GsRegIntSetting DefaultBackgroundGreen = new GsRegIntSetting("DefaultBackgroundGreen", 191);

        public GsRegBoolSetting AllowAdvancedTools = new GsRegBoolSetting(GsRegUtil.DefaultRegistry,
            GsRegUtil.ImageMatchSection, "AllowAdvancedTools", false);
        public GsRegStringSetting ControlPointProfilePath = new GsRegStringSetting(
            GsRegUtil.DefaultRegistry, GsRegUtil.ImageMatchSection, "ControlPointProfilePath");
        public GsRegBoolSetting EnableGreenScreenCaching = new GsRegBoolSetting(
            GsRegUtil.DefaultRegistry, GsRegUtil.PhotoLynxSection, "EnableGreenScreenCaching", true);

        public GsRegStringSetting ColorPresets = new GsRegStringSetting("ColorPresets");

        protected bool _lassoSelectionIsFreehand;
        public bool LassoSelectionIsFreehand
        {
            get { return _lassoSelectionIsFreehand; }
            set
            {
                if (_lassoSelectionIsFreehand == value) return;
                _lassoSelectionIsFreehand = value;
                GsRegUtil.SetBoolValue("LassoSelectionIsFreehand", value);
            }
        }

        public GreenScreenSettings()
        {
            ConfirmUnsavedChanges = true;
            _lassoSelectionIsFreehand = GsRegUtil.GetBoolValue("LassoSelectionIsFreehand", true);

        }
    }
}

