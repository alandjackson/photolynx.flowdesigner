﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.IO;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Windows.Shapes;
using System.Windows;
using PhotoLynx.ImageCore;
using System.Xml.Serialization;
using System.Xml;
using System.Text.RegularExpressions;
using NUnit.Framework;
using Flow.GreenScreen.Managers;

namespace Flow.GreenScreen.Data
{
  public class ImageData : PlImage, IDisposable
  {
    #region Members

    protected SelectionConfiguration _selectionConfiguration;
    protected string _renderedThumbnailFile;

    public int ImageMatchSortIndex { get; set; }
    public List<string> PackageBackgrounds;
    
    public bool IsDuplicate { get; set; }
    public bool IsDelete { get; set; }

    public PanelData.GetConfigurationXmlDelegate GetConfigurationXml;
    public PanelData.SetConfigurationXmlDelegate SetConfigurationXml; 

    #endregion

    #region Constructors

    public ImageData() 
      : this("", 0)
    {
    }

    public ImageData(string imagePath, int imageMatchSortIndex)
      : this(imagePath, imageMatchSortIndex, new List<string>())
    {

    }

    public ImageData(string imagePath, int imageMatchSortIndex, List<string> packageBackgrounds)
      : base(imagePath)
    {
      ImageMatchSortIndex = imageMatchSortIndex;
      PackageBackgrounds = packageBackgrounds;
      ApplyConfigTo = SaveGsSettingsTo.Image;
    }

    #endregion

    #region Properties

    public override string ToString()
    {
      return Name;
    }

    public SelectionConfiguration CurrentSelectionConfiguration
    {
      get
      {
        if (_selectionConfiguration == null)
        {
          LoadConfiguration();
        }
        return _selectionConfiguration;
      }
      set { _selectionConfiguration = value; } 
    }

    public ImageSource Thumbnail { get { return ImageThumbnail; } }
    public ImageSource RenderedThumbnail
    {
      get;
      set;
    }

    public string FilePath
    {
      get { return ImagePath; }
      set { ImagePath = value; }
    }

    public string Name
    {
      get { return ImageFileName; }
    }

    public SaveGsSettingsTo ApplyConfigTo { get; set; }

    public string SelectionConfigurationXml
    {
      get
      {
        return GetConfigurationXml(FilePath);
      }
      set
      {
        SetConfigurationXml(FilePath, value, ApplyConfigTo);
      }
    }

    #endregion

    #region Methods

    internal bool SaveSelectionData()
    {//save brush and polygon selections
      SelectionConfiguration config = SelectionConfiguration.DeserializeFromString(SelectionConfigurationXml);
      //config.Selections = CurrentSelectionConfiguration.Selections;
      config.RenderedClipNegative = CurrentSelectionConfiguration.RenderedClipNegative;
      config.ForegroundClip = CurrentSelectionConfiguration.ForegroundClip;
      return SaveConfigurationData(config);
    }

    internal bool SaveConfigurationData(SelectionConfiguration config)
    {
      try
      {
        SelectionConfigurationXml = ObjectSerializer.SerializeString(config, true);
      }
      catch (Exception e)
      {
        ErrorManager.Instance.LogError(e, 
          "There was an error saving the configuration data for " + FilePath);
        return false;
      }
      return true;
    }
    
    internal bool SaveBackground()
    {
      return SaveBackground(CurrentSelectionConfiguration.ColorAndBackgroundConfiguration.BackgroundImagePath);
    }

    internal bool SaveBackground(string backgroundPath)
    {
      SelectionConfiguration config = SelectionConfiguration.DeserializeFromString(SelectionConfigurationXml);
      config.ColorAndBackgroundConfiguration.BackgroundImagePath = backgroundPath;
      if (!SaveConfigurationData(config))
      {
        return false;
      }
      config.Dispose();
      return true;
    }

    internal bool SaveColorSettings()
    {
      return SaveColorSettings(CurrentSelectionConfiguration.ColorAndBackgroundConfiguration);
    }

    public void LoadConfiguration()
    {
      object obj = null;
      try
      {
        obj = ObjectSerializer.DeserializeFromString(SelectionConfigurationXml, typeof(SelectionConfiguration));
      }
      catch (Exception e)
      {
        PhotoLynx.Logging.PlLog.Logger.Info("LoadConfiguration exception, xml: " + SelectionConfigurationXml + ": " + e.ToString());
        
        //MessageBox.Show("Error, could not load existing settings.");
      }
      if (obj == null)
      {
        _selectionConfiguration = new SelectionConfiguration();
      }
      else
      {
        _selectionConfiguration = (SelectionConfiguration)obj;
      }
    }

    internal bool SaveColorSettings(BackgroundAndColorConfiguration newConfig)
    {
      SelectionConfiguration config = SelectionConfiguration.DeserializeFromString(SelectionConfigurationXml);
      config.ColorAndBackgroundConfiguration = newConfig;
      if (!SaveConfigurationData(config))
      {
        return false;
      }
      config.Dispose();
      return true;
    }

    #endregion

    #region IDisposable Members

    public void Dispose()
    {
      if (_selectionConfiguration != null)
      {
        _selectionConfiguration.ColorAndBackgroundConfiguration.Dispose();
      }
    }

    #endregion
  }
  
  [TestFixture]
  public class ImageDataTest
  {
    [Test]
    public void DeserializeTest()
    {
      string xml = @"
<SelectionConfiguration xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">
  <ForegroundTopLeft>
    <X>0</X>
    <Y>0</Y>
  </ForegroundTopLeft>
  <ForegroundScale>1</ForegroundScale>
  <SelectionsArray />
  <ColorAndBackgroundConfiguration>
    <BackgroundDefinitionRed>26</BackgroundDefinitionRed>
    <BackgroundDefinitionGreen>87</BackgroundDefinitionGreen>
    <BackgroundDefinitionBlue>43</BackgroundDefinitionBlue>
    <BackgroundCorrection>0</BackgroundCorrection>
    <ControlPoints>
      <Point>
        <X>76</X>
        <Y>1</Y>
      </Point>
      <Point>
        <X>105</X>
        <Y>87</Y>
      </Point>
    </ControlPoints>
    <DropColors>
      <Tolerance>24</Tolerance>
      <ColorsSerializationString />
    </DropColors>
    <KeepColors>
      <Tolerance>24</Tolerance>
      <ColorsSerializationString />
    </KeepColors>
    <BackgroundImagePath>\\Dellserver\Shared\Images\Backgrounds 2008-2009\20082009_Good_backgrounds\Gray.jpg</BackgroundImagePath>
    <ProcessDropout>true</ProcessDropout>
    <SoftenEdges>true</SoftenEdges>
  </ColorAndBackgroundConfiguration>
</SelectionConfiguration>
";
      object obj = ObjectSerializer.DeserializeFromString(xml, typeof(SelectionConfiguration));
    }
  }
}
