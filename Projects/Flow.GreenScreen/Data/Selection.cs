﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using System.Xml.Serialization;
using Flow.GreenScreen.Managers;
using PhotoLynx.Logging;

namespace Flow.GreenScreen.Data
{
  [XmlInclude(typeof(Geometry))]
  [XmlInclude(typeof(EllipseGeometry))]
  [XmlInclude(typeof(StreamGeometry))]
  [XmlInclude(typeof(Point))]
  public class Selection
  {
    public enum SelectionType
    {
      Brush,
      Polygon
    }

    enum ShapeDefinitionType
    {
      Point = 0,
      EllipseGeometry = 1
    }

    public Selection() : this( false, "", 1, SelectionType.Brush)
    {
    }

    public Selection(bool isBackground, string name, double opacity, SelectionType type)
    {
      m_isBackground = isBackground;
      Name = name;
      Opacity = opacity;
      Type = type;
    }

    public delegate void SelectionTypeChangedDelegate();
    public event SelectionTypeChangedDelegate OnSelectionTypeChanged;

    protected bool m_isBackground;
    public string Name { get; set; }
    public double Opacity { get; set; }

    public SelectionType Type { get; set; }
    public PathGeometry Geometry;

    public string ShapeDefinitionsSerializationString
    {
      get
      {
        return "";
      }
      set
      {
        if (string.IsNullOrEmpty(value))
        {
          return;
        }

        string[] values = value.Split(' ');
        value = "";

        Geometry combined = null;
        Geometry deserializedGeo = null;

        int i = 0;
        while (i < values.Count())
        {
          try
          {
            if (Convert.ToInt32(values[i]) == (int)ShapeDefinitionType.Point)
            {
              deserializedGeo = new StreamGeometry();
              StreamGeometry polygon = deserializedGeo as StreamGeometry;
              using (StreamGeometryContext ctx = polygon.Open())
              {
                ctx.BeginFigure(new Point(Convert.ToDouble(values[i + 1]), 
                                          Convert.ToDouble(values[i + 2])), true, true);
                i += 3;
                while (Convert.ToInt32(values[i]) == (int)ShapeDefinitionType.Point)
                {
                  try
                  {
                    ctx.LineTo(new Point(Convert.ToDouble(values[i + 1]), 
                                         Convert.ToDouble(values[i + 2])), true, true);
                    i += 3;
                  }
                  catch (Exception ex)
                  {
                    ErrorManager.Instance.LogError(ex, 
                      "There was a problem reconstructing a lasso selection.");
                  }
                }
              }
              //polygon.Freeze();
            }
            else if (Convert.ToInt32(values[i]) == 
                            (int)ShapeDefinitionType.EllipseGeometry)
            {
              try
              {
                deserializedGeo = new EllipseGeometry(
                  new Point(Convert.ToDouble(values[i + 1]), 
                            Convert.ToDouble(values[i + 2])),
                  Convert.ToDouble(values[i + 3]), Convert.ToDouble(values[i + 4]));
                i += 5;
              }
              catch (Exception ex)
              {
                ErrorManager.Instance.LogError(ex,
                  "There was a problem reconstructing a brush selection.");
              }
            }
            else
            {
              throw (new Exception("Error deserializing shape definitions. " + 
                                   "Definition type was not expected"));
            }

            if (combined == null)
            {
              combined = deserializedGeo;
            }
            else
            {
              combined = new CombinedGeometry(
                GeometryCombineMode.Union, combined, deserializedGeo);
            }
          }
          catch (Exception ex)
          {
            PlLog.Logger.Info(
              "There was a problem reconstructing the selection data\n" + 
              "Data\n" + value + "\n" + ex.ToString());
          }
        }
        if (combined != null) Geometry = combined.GetOutlinedPathGeometry();
      }
    }

    public bool IsBackground
    {
      get { return m_isBackground; }
      set
      {
        m_isBackground = value;
        if (OnSelectionTypeChanged != null)
        {
          OnSelectionTypeChanged();
        }
      }
    }

    public bool IsForeground
    {
      get { return !m_isBackground; }
      set
      {
        m_isBackground = !value;
      }
    }

    public override int GetHashCode()
    {
      return base.GetHashCode();
    }
  }

}