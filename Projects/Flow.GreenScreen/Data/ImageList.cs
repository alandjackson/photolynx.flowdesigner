﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using PhotoLynx.ImageThumbnailer;

namespace Flow.GreenScreen.Data
{
  public class ImageList : ObservableCollection<ImageData>
  {
    public ImageList() 
    {
    }
    
    public ImageList(List<ImageData> list) : base(list)
    {
    }
  }
}
