﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.GreenScreen.Util;

namespace CPIGreenScreen.Util
{
  public class CPISettings
  {
    protected static CPISettings _instance = null;
    public static CPISettings Instance { get { if (_instance == null) _instance = new CPISettings(); return _instance; } }
    
    public string HotFolder { get; set; }
    public string BackgroundFolder { get; set; }
    public string OutFolder { get; set; }
    public string CompletedFolder { get; set; }
    public string AppDisplayName { get; set; }
    public bool RenderExternally { get; set; }
    public int JpegCompression { get; set; }

    public CPISettings()
    {
        BackgroundFolder = GsRegUtil.GetStrValue("CPIBackgroundFolder", @"C:\");
        HotFolder = GsRegUtil.GetStrValue("CPIWatchFolder");
        OutFolder = GsRegUtil.GetStrValue("CPIOutFolder", @"C:\");
        JpegCompression = GsRegUtil.GetIntValue("CPIJpegCompression", 50);
        CompletedFolder = OutFolder;

        RenderExternally = GsRegUtil.GetBoolValue("CPIRenderExternally", true);

        AppDisplayName = "Green Screen Image Import";
    }
  }
}
