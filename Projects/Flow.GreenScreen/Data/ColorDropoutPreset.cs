﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace Flow.GreenScreen.Data
{
    public class ColorDropoutPreset : INotifyPropertyChanged
    {

        public string _name { get; set; }
        public string Name
        {
            get { return _name; }
            set { _name = value; OnPropertyChanged("Name"); }
        }

        public int _red { get; set; }
        public int Red
        {
            get { return _red; }
            set { _red = value; OnPropertyChanged("Red"); }
        }

        public int _green { get; set; }
        public int Green
        {
            get { return _green; }
            set { _green = value; OnPropertyChanged("Green"); }
        }

        public int _blue { get; set; }
        public int Blue
        {
            get { return _blue; }
            set { _blue = value; OnPropertyChanged("Blue"); }
        }

        public int _tolerance { get; set; }
        public int Tolerance
        {
            get { return _tolerance; }
            set { _tolerance = value; OnPropertyChanged("Tolerance"); }
        }

        public override string ToString()
        {
            return Name;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name) { if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(name)); }
    }

    public class EditPresentsViewModel : INotifyPropertyChanged
    {
        protected ObservableCollection<ColorDropoutPreset> _presets = 
            new ObservableCollection<ColorDropoutPreset>();

        public byte DefaultRed;
        public byte DefaultBlue;
        public byte DefaultGreen;

        public ObservableCollection<ColorDropoutPreset> Presets
        {
            get { return _presets; }
            set { _presets = value; OnPropertyChanged("Presets"); }
        }

        protected ColorDropoutPreset _selected;
        public ColorDropoutPreset Selected
        {
            get { return _selected; }
            set { _selected = value; OnPropertyChanged("Selected"); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name) { if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(name)); }

    }
}
