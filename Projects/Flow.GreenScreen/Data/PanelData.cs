﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.IO;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Windows.Shapes;
using System.Windows;

using Microsoft.Win32;

using PhotoLynx.ImageThumbnailer;
using Flow.GreenScreen.Util;
using Flow.GreenScreen.Renderer;
using Flow.GreenScreen.Managers;
using Flow.GreenScreen.Panels;
using Flow.GreenScreen.Managers.Panels;

namespace Flow.GreenScreen.Data
{
    public enum SaveGsSettingsTo
    {
        Image,
        Subject,
        Project,
        Filter
    }

    public class PanelData : IDisposable, INotifyPropertyChanged
    {
        #region Members

        public bool AllowAdvancedTools;
        public BitmapImage CurrentRenderedBitmapSource;
        public BitmapImage CurrentOriginalBitmapSource;
        public System.Windows.Media.ImageSource CurrentBackgroundSource;
        public bool CurrentRenderedBitmapSourceDropoutValid;
        public ImageList Images;
        public String DatabaseFile;
        public Color ColorUnderCursor;
        public System.Drawing.Bitmap CurrentBitmap;
        public ImageData CurrentImage;
        public VariantType Variant;
        public double DisplaySizeRatio;
        public string CurrentTool;
        public Point LastMousePosition;


        #endregion

        #region Constructors

        public PanelData()
        {
            LoadControlPointProfiles();
            Images = new ImageList();
            ColorUnderCursor = new Color();
            CurrentRenderedBitmapSourceDropoutValid = false;
            AllowAdvancedTools = GreenScreenSettings.Instance.AllowAdvancedTools.Value;

            // setup default empty delegates
            GetConfigurationXml = image => ObjectSerializer.SerializeString(new SelectionConfiguration());
            SetConfigurationXml = (image, config, applyTo) => { };
            GetAdjustmentBitmap = image => new System.Drawing.Bitmap(image);
            SaveConfigurationChanges = () => { };
            GetCrop = image => new System.Drawing.RectangleF(0F, 0F, 100F, 100F);

        }

        #endregion

        #region Events and Delegates

        public delegate System.Drawing.Bitmap GetAdjustedBitmapDelegate(string imageFile);
        public delegate string GetConfigurationXmlDelegate(string imageFile);
        public delegate void SetConfigurationXmlDelegate(string imageFile, string configurationXml, SaveGsSettingsTo applyTo);
        public delegate void SaveConfigurationChangesDelegate();
        public delegate System.Drawing.RectangleF GetCropDelegate(string imageFile);

        public event EventHandler SaveJob;
        public void OnSaveJob() { if (SaveJob != null) SaveJob(this, EventArgs.Empty); }

        #endregion

        #region Properties

        public bool AllowClose;
        public List<GreenScreenBGRA> ColorSelections;
        public string ControlPointProfilePath;
        public ControlPointProfileList ControlPointProfiles;
        public GetAdjustedBitmapDelegate GetAdjustmentBitmap;
        public GetConfigurationXmlDelegate GetConfigurationXml;
        public SetConfigurationXmlDelegate SetConfigurationXml;
        public SaveConfigurationChangesDelegate SaveConfigurationChanges;
        public GetCropDelegate GetCrop = null;

        protected ImageList _imagesInFilter = null;
        public ImageList ImagesInFilter
        {
            get
            {
                if (_imagesInFilter == null)
                    return Images;
                return _imagesInFilter;
            }
            set
            {
                if (_imagesInFilter == value) return;
                _imagesInFilter = value;
                OnPropertyChanged("ImagesInFilter");
            }
        }

        public void UpdateFilter(bool filterDeletes, bool filterDuplicates)
        {
            ImagesInFilter = FilterImages(Images, filterDeletes, filterDuplicates);
        }

        protected static ImageList FilterImages(ImageList srcList, bool filterDeletes, bool filterDuplicates)
        {
            return new ImageList(new List<ImageData>(srcList.Where<ImageData>(delegate(ImageData data)
            {
                return !((filterDeletes && data.IsDelete) || (filterDuplicates && data.IsDuplicate));
            })));

        }

        #endregion

        #region Functions

        public void SetSelectedIndex(int ndx)
        {
            ManagerTracker.DataManager.UpdateSelectedImage(ndx);
        }

        public void SetImageFiles(string[] files)
        {
            SetImageFiles(files, null);
        }

        public void SetImageFiles(string[] files, List<ImageData> parameters)
        {
            Images.Clear();
            for (int i = 0; i < files.Count(); i++)
            {
                ImageData image = null;
                if (parameters != null && parameters.Count == files.Length)
                {
                    image = parameters[i];
                }
                else
                {
                    image = new ImageData(files[i], i);
                }

                image.GetConfigurationXml = GetConfigurationXml;
                image.SetConfigurationXml = SetConfigurationXml;
                Images.Add(image);
            }

            if (Images.Count > 0) CurrentImage = Images[0];
        }

        public void LoadControlPointProfiles(string path)
        {

            ControlPointProfiles = new ControlPointProfileList();

            //if (path.Equals("") || !new FileInfo(path).Directory.Exists)
            //{
            //    path = @"C:\Program Files\PhotoLynx\bin\controlProfile.crv";
            //    if (!new FileInfo(path).Directory.Exists)
            //    {
            //        path = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "controlProfile.crv");
            //    }
            //    GreenScreenSettings.Instance.ControlPointProfilePath.SaveValue(path);
            //}
            ControlPointProfilePath = path;
            if (File.Exists(path))
            {
                ControlPointProfiles = (ControlPointProfileList)ObjectSerializer.DeserializeFile(path, typeof(ControlPointProfileList));
                if (ControlPointProfiles == null)
                {
                    ControlPointProfiles = new ControlPointProfileList();
                }
            }
        }

        public void LoadControlPointProfiles()
        {
            LoadControlPointProfiles(
                GreenScreenSettings.Instance.ControlPointProfilePath.Value);
        }

        #endregion

        internal void SaveControlPointProfile()
        {
            ObjectSerializer.SerializeFile(ControlPointProfiles, ControlPointProfilePath);
        }

        #region IDisposable Members

        public void Dispose()
        {
            foreach (ImageData image in Images)
            {
                image.Dispose();
            }

        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propName)
        {
            if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }

        #endregion
    }
}
