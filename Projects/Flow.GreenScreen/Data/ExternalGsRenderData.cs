﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flow.GreenScreen.Data
{
  public class ExternalGsRenderData
  {
    public string OriginalImageFilename { get; set; }
    public string ConfigXml { get; set; }
    public string BackgroundPath { get; set; }
    public string RenderedImageFilename { get; set; }
  }
}
