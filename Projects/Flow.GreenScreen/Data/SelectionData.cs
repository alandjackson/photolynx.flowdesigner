﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Xml.Serialization;
using PhotoLynx.ImageCore;
using System.IO;
using System.Diagnostics;
using System.Drawing;
using Point = System.Windows.Point;
using Flow.GreenScreen.Managers;

namespace Flow.GreenScreen.Data
{
  [XmlInclude(typeof(Selection))]
  [XmlInclude(typeof(EllipseGeometry))]
  [XmlInclude(typeof(StreamGeometry))]
  [XmlInclude(typeof(CombinedGeometry))]
  [XmlInclude(typeof(ImageInfo))]
  [XmlInclude(typeof(BackgroundAndColorConfiguration))]
  [XmlInclude(typeof(BackgroundAndColorConfiguration.PixelColorCollection))]
  [XmlInclude(typeof(PolyBezierSegment))]
  [XmlInclude(typeof(PolyLineSegment))]
  [XmlInclude(typeof(BezierSegment))]
  [XmlInclude(typeof(LineSegment))]
  [XmlInclude(typeof(PathGeometry))]
  public class SelectionConfiguration : IDisposable
  {
    public event Selection.SelectionTypeChangedDelegate OnSelectionTypeChanged;

    public SelectionConfiguration() :this(false)
    {
    }

    public SelectionConfiguration(bool render)
    {
      ForegroundClip = Geometry.Empty.GetOutlinedPathGeometry();
      ForegroundClip.FillRule = FillRule.Nonzero;
      RenderedClipNegative = Geometry.Empty.GetOutlinedPathGeometry();
      RenderedClipNegative.FillRule = FillRule.Nonzero;

      ForegroundScale = 1;
      ForegroundTopLeft = new Point();
      //m_selectionNumber = 0;
      //ImageInfo = new ImageInfo();
      ColorAndBackgroundConfiguration = new BackgroundAndColorConfiguration(render);
      RotateFlipType = RotateFlipType.RotateNoneFlipNone;
    }

    public BackgroundAndColorConfiguration ColorAndBackgroundConfiguration { get; set; }
    public Point ForegroundTopLeft;
    public double ForegroundScale;
    
    public RotateFlipType RotateFlipType { get; set; }

    [XmlIgnore()]
    public Polygon LineFillPolygon { get; set; }
    [XmlIgnore()]
    public Polyline CurrentLine { get; set; }

    bool IsBackground { get; set; }    

    public PathGeometry ForegroundClip;
    public PathGeometry RenderedClipNegative;

    /// <summary>
    /// Legacy brush data support
    /// </summary>
    public Selection[] SelectionsArray
    {
      get
      {
        return null;
      }
      set
      {
        try
        {
          //_Selections = value;
          Geometry foreground = null;
          Geometry background = null;
          foreach (Selection selection in value)
          {
            if (selection.IsBackground)
            {
              if (background == null)
              {
                background = selection.Geometry;
              }
              else
              {
                background = new CombinedGeometry(GeometryCombineMode.Union, 
                                                  background, 
                                                  selection.Geometry);
              }
            }
            else
            {
              if (foreground == null)
              {
                foreground = selection.Geometry;
              }
              else
              {
                foreground = new CombinedGeometry(GeometryCombineMode.Union, 
                                                  foreground, 
                                                  selection.Geometry);
              }
            }
          }
          if (foreground != null)
          {
            ForegroundClip = foreground.GetOutlinedPathGeometry();
          }
          if (background != null)
          {
            RenderedClipNegative = background.GetOutlinedPathGeometry();
          }
        }
        catch (Exception ex)
        {
          ErrorManager.Instance.LogError(ex,
            "This database contains old data settings that could not be" + 
            " supported.\n Saving new green screen settings will overwrite your old " + 
            "data.");
        }
      }
    }

    private Selection SaveCurrentSelection(double opacity, Selection.SelectionType type)
    {
      return null;
    }

    void selection_OnSelectionTypeChanged()
    {
      if (OnSelectionTypeChanged != null)
      {
        OnSelectionTypeChanged();
      }
    }

    private bool PathEquals(PathGeometry p1, PathGeometry p2)
    {
      if (p1.Figures.Count != p2.Figures.Count)
      {
        return false;
      }

      for (int i = 0; i < p1.Figures.Count; i++)
      {
        if (p1.Figures[i].Segments.Count != p2.Figures[i].Segments.Count)
        {
          return false;
        }
      }
      return true;
    }

    internal Selection SavePolygon(double opacity, Selection.SelectionType type)
    {
      StreamGeometry polygon = new StreamGeometry();
      using (StreamGeometryContext ctx = polygon.Open())
      {
        ctx.BeginFigure(CurrentLine.Points[0], true, true);
        ctx.PolyLineTo(CurrentLine.Points, true, true);
      }

      CurrentLine = null;

      return SaveCurrentSelection(opacity, type);
    }

    internal void SaveCurrentBrushSelection(double opacity, Selection.SelectionType type)
    {
      SaveCurrentSelection(opacity, type);
      //CurrentSelection = null;
      }

    public override int GetHashCode()
    {
      return base.GetHashCode();
    }

    public override bool Equals(object obj)
    {
      bool retVal = false;

      SelectionConfiguration operand = (SelectionConfiguration)obj;

      //ignore background path changes
      string backgroundImage = this.ColorAndBackgroundConfiguration.BackgroundImagePath;
      this.ColorAndBackgroundConfiguration.BackgroundImagePath = "";
      string opBackgroundImage = operand.ColorAndBackgroundConfiguration.BackgroundImagePath;
      operand.ColorAndBackgroundConfiguration.BackgroundImagePath = "";

      if (ColorAndBackgroundConfiguration.Equals(operand.ColorAndBackgroundConfiguration)
           == BackgroundAndColorConfiguration.EqualityOutputs.Equal)
      {
        if ((ForegroundClip.Bounds == Rect.Empty &&
             operand.ForegroundClip.Bounds == Rect.Empty &&
             RenderedClipNegative.Bounds == Rect.Empty &&
             operand.RenderedClipNegative.Bounds == Rect.Empty) ||
            (PathEquals(ForegroundClip, operand.ForegroundClip) &&
             PathEquals(RenderedClipNegative, operand.RenderedClipNegative)))
        {
          retVal = true;
        }
      }
      else
      {
        retVal = false;
      }

      this.ColorAndBackgroundConfiguration.BackgroundImagePath = backgroundImage;
      operand.ColorAndBackgroundConfiguration.BackgroundImagePath = opBackgroundImage;

      return retVal;
    }

    /// <summary>
    /// IDisposable implementation
    /// </summary>
    public void Dispose()
    {
      ColorAndBackgroundConfiguration = null;
    }


    public static SelectionConfiguration FromXmlStr(string xml)
    {
      return DeserializeFromString(xml);
    }

    internal static SelectionConfiguration DeserializeFromString(string xml)
      
    {
      object obj = ObjectSerializer.DeserializeFromString(xml, 
                                                          typeof(SelectionConfiguration));
      if (obj == null)
      {
        return new SelectionConfiguration();
      }
      return (SelectionConfiguration)obj;
    }
  }
}
