﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using System.Text.RegularExpressions;
using System.Xml.Schema;
using System.Collections;
using System.Diagnostics;
using System.Windows.Forms;

namespace Flow.GreenScreen.Data
{
    public class ObjectSerializer
    {
        public static T DeserializeOrDefault<T>(string xml)
        {
            try
            {
                xml = (new Regex(@"^.*\<\?xml.*?\?\>\s*")).Replace(xml, "").Replace("\\r", "").Replace("\\n", "");

                var type = typeof(T);
                if (!xml.Equals(""))
                {
                    using (var ms = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(xml)))
                    {
                        XmlSerializer serializer = new XmlSerializer(type);
                        return (T)serializer.Deserialize(ms);
                    }
                }
            }
            catch (Exception)
            {
            }
            return default(T);
        }


        public static object DeserializeFromString(string xml, Type type)
        {
            //if (!string.IsNullOrEmpty(xml) && xml.StartsWith("?"))
            //    xml = xml.Substring(1);
            xml = (new Regex(@"^.*\<\?xml.*?\?\>\s*")).Replace(xml, "").Replace("\\r", "").Replace("\\n", "");

            try
            {
                if (xml.Equals("")) return null;


                using (MemoryStream ms = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(xml)))
                {
                    XmlSerializer serializer = new XmlSerializer(type);
                    return serializer.Deserialize(ms);
                }

            }
            catch (Exception e)
            {
                throw (e);
            }
        }

        public static string SerializeString(object o)
        {
            return SerializeString(o, false);
        }

        public static string SerializeString(object o, bool strip_xml_header)
        {
            Type type = o.GetType();
            string str = "";

            // create new serializer
            XmlSerializer serializer = new XmlSerializer(type);

            // serialize object to a file
            using (MemoryStream ms = new MemoryStream())
            {

                // create xml text writer
                using (var mem_writer = new XmlTextWriter(ms, System.Text.Encoding.UTF8))
                {
                    mem_writer.Formatting = Formatting.Indented;

                    // get the string of xml data
                    serializer.Serialize(mem_writer, o);
                    //str = System.Text.ASCIIEncoding.ASCII.GetString(ms.ToArray(), 0, (int)ms.Length);
                    str = System.Text.UTF8Encoding.UTF8.GetString(ms.ToArray(), 0, (int)ms.Length);

                    // strip out xml header
                    if (strip_xml_header)
                    {
                        str = (new Regex(@"^.*\<\?xml.*?\?\>")).Replace(str, "");
                    }
                }

            }

            return (str);

        }

        public static object DeserializeFile(string path)
        {
            return DeserializeFile(path, typeof(object));
        }

        public static object DeserializeFile(string path, Type type)
        {
            using (TextReader reader = new StreamReader(path))
            {
                object obj = ObjectSerializer.DeserializeFromString(reader.ReadToEnd(), type);
                reader.Close();
                return obj;
            }
        }

        public static void SerializeFile(object obj, string path)
        {
            if (File.Exists(path))
            {
                File.Delete(path);
            }

            using (TextWriter writer = new StreamWriter(path))
            {
                writer.WriteLine(SerializeString(obj, true));
                writer.Close();
            }
        }
    }
}
