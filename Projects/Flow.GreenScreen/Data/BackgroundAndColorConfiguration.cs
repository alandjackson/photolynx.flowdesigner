﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.IO;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Windows.Shapes;
using System.Windows;


using System.Xml.Serialization;
using Flow.GreenScreen.Renderer;
using Flow.GreenScreen.Util;

namespace Flow.GreenScreen.Data
{
  [XmlInclude(typeof(PixelColorCollection))]
  public class BackgroundAndColorConfiguration: IDisposable
  {
    public enum EqualityOutputs
    {
      Equal = 1,
      BackgroundPathInequal = -1,
      ColorConfiguartionInequal = -2
    }

    public BackgroundAndColorConfiguration() : this(false) { }

    public BackgroundAndColorConfiguration(bool isValid) : this(isValid, "") { }

    public BackgroundAndColorConfiguration( bool isValid, string iniFilePath)
    {
      ControlPoints = new Point[] { new Point(76, 1), new Point(105, 87) };

      SoftenEdges = true;

      DropColors = new PixelColorCollection();
      KeepColors = new PixelColorCollection();

      BackgroundImagePath = "";

      ProcessDropout = isValid;

      BackgroundDefinitionGreen = (byte)GreenScreenSettings.Instance.DefaultBackgroundGreen.Value; //GreenScreenAlgorithm.DefaultGreen;
      BackgroundDefinitionBlue = (byte)GreenScreenSettings.Instance.DefaultBackgroundBlue.Value;
      BackgroundDefinitionRed = (byte)GreenScreenSettings.Instance.DefaultBackgroundRed.Value;
      BackgroundCorrection = 0;
    }

    public byte BackgroundDefinitionRed;
    public byte BackgroundDefinitionGreen;
    public byte BackgroundDefinitionBlue;
    public double BackgroundCorrection;

    public PixelColorCollection DropColors { get; set; }
    public PixelColorCollection KeepColors { get; set; }

    public string BackgroundImagePath { get; set; }
    public bool ProcessDropout { get; set; }
    public Point[] ControlPoints;
    public bool SoftenEdges { get; set; }

    [XmlIgnore()]
    public List<GreenScreenBGRA> KeepList { get { return KeepColors.Colors; } set { KeepColors.Colors = value; } }
    [XmlIgnore()]
    public List<GreenScreenBGRA> DropList { get { return DropColors.Colors; } set { DropColors.Colors = value; } }

    public EqualityOutputs Equals(BackgroundAndColorConfiguration operand)
    {
      if (operand.BackgroundCorrection != BackgroundCorrection)
      {
        return EqualityOutputs.ColorConfiguartionInequal;
      }
      if (operand.BackgroundDefinitionGreen != BackgroundDefinitionGreen)
      {
        return EqualityOutputs.ColorConfiguartionInequal;
      }
      if (operand.SoftenEdges != SoftenEdges)
      {
        return EqualityOutputs.ColorConfiguartionInequal;
      }
      if (!operand.BackgroundImagePath.Equals(BackgroundImagePath))
      {
        return EqualityOutputs.BackgroundPathInequal;
      }
      if (!operand.DropColors.Equals(DropColors))
      {
        return EqualityOutputs.ColorConfiguartionInequal;
      }
      if (!operand.KeepColors.Equals(KeepColors))
      {
        return EqualityOutputs.ColorConfiguartionInequal;
      }
      if (!operand.ProcessDropout == ProcessDropout)
      {
        return EqualityOutputs.ColorConfiguartionInequal;
      }
      if (ControlPoints.Count() == operand.ControlPoints.Count())
      {
        for (int i = 0; i < ControlPoints.Count(); i++)
        {
          if (ControlPoints[i].X != operand.ControlPoints[i].X
            || ControlPoints[i].Y != operand.ControlPoints[i].Y)
          {
            return EqualityOutputs.ColorConfiguartionInequal;
          }
        }
      }
      else
      {
        return EqualityOutputs.ColorConfiguartionInequal;
      }

      return EqualityOutputs.Equal;
    }

    public void AddDropColor(List<GreenScreenBGRA> colors)
    {
      foreach (GreenScreenBGRA color in colors)
      {
        if (!IsColorContained(color))
        {
          DropColors.Colors.Add(color);
        }
      }
      
     
    }

    public void AddKeepColor(List<GreenScreenBGRA> colors)
    {
      foreach (GreenScreenBGRA color in colors)
      {
        if (!IsColorContained(color))
        {
          KeepColors.Colors.Add(color);
        }
      }
    
    }

    private bool IsColorContained(GreenScreenBGRA color)
    {
      foreach (GreenScreenBGRA selectionColor in DropColors.Colors)
      {
        if (color.Blue == selectionColor.Blue)
        {
          if (color.Green == selectionColor.Green)
          {
            if (color.Red == selectionColor.Red)
            {
              return true;
            }
          }
        }
      }

      foreach (GreenScreenBGRA selectionColor in KeepColors.Colors)
      {
        if (color.Blue == selectionColor.Blue)
        {
          if (color.Green == selectionColor.Green)
          {
            if (color.Red == selectionColor.Red)
            {
              return true;
            }
          }
        }
      }

      return false;
    }

    public void SetDropTolerance(int tolerance)
    {
      DropColors.Tolerance = tolerance;
      
    }

    public void SetKeepTolerance(int tolerance)
    {
      KeepColors.Tolerance = tolerance;
      
    }

    public int GetDropTolerance()
    {
      return DropColors.Tolerance;
    }

    public int GetKeepTolerance()
    {
      return KeepColors.Tolerance;
    }

    internal int GetDropColorCount()
    {
      return DropColors.Colors.Count;
    }

    public void CopyColorsTo(BackgroundAndColorConfiguration copy)
    {
      copy.DropColors = DropColors.Copy();
      copy.KeepColors = KeepColors.Copy();
    }

    public BackgroundAndColorConfiguration CopyDeep()
    {
      BackgroundAndColorConfiguration copy = new BackgroundAndColorConfiguration();
      CopyColorsTo(copy);
      copy.BackgroundImagePath = BackgroundImagePath;
      for (int i = 0; i < ControlPoints.Count(); i++)
      {
        copy.ControlPoints[i] = ControlPoints[i];
      }
      copy.BackgroundCorrection = BackgroundCorrection;
      copy.BackgroundDefinitionGreen = BackgroundDefinitionGreen;
      copy.BackgroundDefinitionBlue = BackgroundDefinitionBlue;
      copy.BackgroundDefinitionRed = BackgroundDefinitionRed;
      copy.ProcessDropout = ProcessDropout;
      copy.SoftenEdges = SoftenEdges;
      copy.SetDropTolerance(GetDropTolerance());
      copy.SetKeepTolerance(GetKeepTolerance());

      return copy;
    }

    [XmlInclude(typeof(GreenScreenBGRA))]
    public class PixelColorCollection : IDisposable
    {
      public PixelColorCollection()
      {
        Colors = new List<GreenScreenBGRA>();
        Tolerance = 24;
      }

      public int Tolerance { get; set; }

      public string ColorsSerializationString
      {
        get
        {
          string xml = "";
          foreach (GreenScreenBGRA color in Colors)
          {
            if (!xml.Equals(""))
            {
              xml = xml + " ";
            }
            xml = xml + color.ToString();
          }
          return xml;
        }
        set
        {
          Colors = new List<GreenScreenBGRA>();
          if (value.Equals(""))
          {
            return;
          }
          string[] values = ((string)value).Split(' ');
          value = "";
          for (int i = 0; i < values.Count(); i += 3)
          {
            Colors.Add(new GreenScreenBGRA(Convert.ToInt32(values[i]), Convert.ToInt32(values[i + 1]), Convert.ToInt32(values[i + 2]), 255));
          }
        }
      }

      [XmlIgnore()]
      public List<GreenScreenBGRA> Colors { get; set; }
      
      public bool Equals(PixelColorCollection operand)
      {
        if (Colors.Count != operand.Colors.Count)
        {
          return false;
        }

        for (int i = 0; i < Colors.Count; i++)
        {
          if (!Colors[i].Equals(operand.Colors[i]))
          {
            return false;
          }
        }

        if (operand.Tolerance != Tolerance)
        {
          return false;
        }
        return true;
      }

      internal void Save(string iniFilePath, string section)
      {
        if (!File.Exists(iniFilePath))
        {
          File.Create(iniFilePath);
        }

        INIAccess.WritePrivateProfileStringA(section, "cnt", Colors.Count.ToString(), iniFilePath);

        INIAccess.WritePrivateProfileStringA(section, "tol", Tolerance.ToString(), iniFilePath);
        for (int i = 1; i <= Colors.Count; i++)
        {
          INIAccess.WritePrivateProfileStringA(section, "pix" + Convert.ToString(i), Colors[i - 1].ToString(), iniFilePath);
        }
      }

      internal PixelColorCollection Copy()
      {
        PixelColorCollection copy = new PixelColorCollection();
        copy.Colors.AddRange(Colors.ToArray());
        copy.Tolerance = Tolerance;
        return copy;
      }

      #region IDisposable Members

      public void Dispose()
      {
        
      }

      #endregion
    }

    internal void Clear()
    {
      KeepColors.Colors.Clear();
      DropColors.Colors.Clear();
    }

    #region IDisposable Members

    public void Dispose()
    {
      DropColors.Dispose();
      KeepColors.Dispose();
    }

    #endregion
  }
}
