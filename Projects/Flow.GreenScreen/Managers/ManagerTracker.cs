﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.GreenScreen.Managers.Data;
using Flow.GreenScreen.Data;
using Flow.GreenScreen.Managers.Panels;
using Flow.GreenScreen.Managers.Tools;
using System.Windows.Controls;
using PhotoLynx.ImageThumbnailer;
using System.Windows;
using Flow.GreenScreen.Panels;
using PhotoLynx.CommonWpfControls.Util;

namespace Flow.GreenScreen.Managers
{
  public class ManagerTracker : GenericManager
  {
    ImageFileVirtualizationManager _imageFileVirtualizationManager;
    Dictionary<Type, GenericManager> _managerList;
    static ManagerTracker _instance;
    VariantType _variant;

    ManagerTracker(GreenScreenPanel greenScreenPanel, VariantType variant)
      : base(greenScreenPanel.Data)
    {
      _managerList = new Dictionary<Type, GenericManager>();
      _variant = variant;
      PanelTracker.Initialize(greenScreenPanel.Data);

    }

    #region Properties

    //public static CacheManager CacheManager
    //{
    //  get { return _instance._managerList[typeof(CacheManager)] as CacheManager; }
    //}


    public static CPIRotatePanelManager CPIRotatePanelManager
    {
      get { return _instance._managerList[typeof(CPIRotatePanelManager)] as CPIRotatePanelManager; }
    }

    public static DataManager DataManager
    {
      get { return _instance._managerList[typeof(DataManager)] as DataManager; }
    }

    public static RenderManager RenderManager
    {
      get { return _instance._managerList[typeof(RenderManager)] as RenderManager; }
    }

    public static ColorPickerDataPanelManager ColorPickerDataPanelManager
    {
      get { return _instance._managerList[typeof(ColorPickerDataPanelManager)] as ColorPickerDataPanelManager; }
    }

    public static CurvePanelManager CurvePanelManager
    {
      get { return _instance._managerList[typeof(CurvePanelManager)] as CurvePanelManager; }
    }

    public static ImageDisplayManager ImageDisplayManager
    {
      get { return _instance._managerList[typeof(ImageDisplayManager)] as ImageDisplayManager; }
    }

    //public static ImageListManager ImageListManager
    //{
    //  get { return _instance._managerList[typeof(ImageListManager)] as ImageListManager; }
    //}

    public static MainDisplayManager MainDisplayManager
    {
      get { return _instance._managerList[typeof(MainDisplayManager)] as MainDisplayManager; }
    }

    public static SelectionDataPanelManager SelectionDataPanelManager
    {
      get { return _instance._managerList[typeof(SelectionDataPanelManager)] as SelectionDataPanelManager; }
    }

    public static ISettingsPanelManager SettingsPanelManager
    {
      get { return _instance._managerList[typeof(ISettingsPanelManager)] as ISettingsPanelManager; }
    }

    public static DefaultToolbarManager ImageMatchToolbarManager
    {
      get { return _instance._managerList[typeof(DefaultToolbarManager)] as DefaultToolbarManager; }
    }

    public static BackgroundDefinitionToolManager BackgroundDefinitionToolManager
    {
      get { return _instance._managerList[typeof(BackgroundDefinitionToolManager)] as BackgroundDefinitionToolManager; }
    }

    public static BrushToolManager BrushToolManager
    {
      get { return _instance._managerList[typeof(BrushToolManager)] as BrushToolManager; }
    }

    public static BezierToolManager BezierToolManager
    {
      get { return _instance._managerList[typeof(BezierToolManager)] as BezierToolManager; }
    }

    public static ColorPickerToolManager ColorPickerToolManager
    {
      get { return _instance._managerList[typeof(ColorPickerToolManager)] as ColorPickerToolManager; }
    }

    public static HandToolManager HandToolManager
    {
      get { return _instance._managerList[typeof(HandToolManager)] as HandToolManager; }
    }

    public static PolygonSelectionToolManager PolygonSelectionToolManager
    {
      get { return _instance._managerList[typeof(PolygonSelectionToolManager)] as PolygonSelectionToolManager; }
    }

    public static ZoomToolManager ZoomToolManager
    {
      get { return _instance._managerList[typeof(ZoomToolManager)] as ZoomToolManager; }
    }

    //public static ConfigurationManager ConfigurationManager
    //{
    //    get { return _instance._managerList[typeof(ConfigurationManager)] as ConfigurationManager; }
    //}

    public static ImageFileVirtualizationManager ImageFileVirtualizationManager
    {
      get { return _instance._imageFileVirtualizationManager; }
    }

    public static UndoRedoManager<System.Windows.Media.PathGeometry[]> UndoRedoManager
    {
      get { return (UndoRedoManager<System.Windows.Media.PathGeometry[]>) _instance._managerList[typeof(UndoRedoManager<System.Windows.Media.PathGeometry[]>)]; }
    }

    #endregion

    #region Methods

    public static void Initialize(GreenScreenPanel greenScreenPanel, VariantType variant)
    {
      _instance = new ManagerTracker(greenScreenPanel, variant);
      _instance.InitializeManagers(null, 
                                   greenScreenPanel.canvasImageDisplay, 
                                   greenScreenPanel.imageWorking, 
                                   greenScreenPanel.imageRendered, 
                                   greenScreenPanel.imageBackground, 
                                   greenScreenPanel.imageForeground, 
                                   greenScreenPanel.canvasImageDisplayClip, 
                                   greenScreenPanel.Data, 
                                   greenScreenPanel.Window, 
                                   greenScreenPanel);
    }

    private void InitializeManagers(PlImageList imageList, 
                                    Canvas canvasImageDisplay, 
                                    Image imageWorking, 
                                    Image imageRendered, 
                                    Image imageBackground, 
                                    Image imageForeground, 
                                    Canvas canvasImageDisplayClip, 
                                    PanelData data, 
                                    Window mainWindow, 
                                    GreenScreenPanel greenScreenPanel)
    {
      _imageFileVirtualizationManager = new ImageFileVirtualizationManager(
        new Size(Math.Ceiling(GreenScreenPanel.RenderWidth * 4 / 5.0), 
                 Math.Ceiling(GreenScreenPanel.RenderHeight * 4 / 5.0)));

      //_managerList.Add(typeof(CacheManager), new CacheManager(imageRendered, data));
      _managerList.Add(typeof(DataManager), 
        new DataManager(imageList, imageRendered, data));
      if (data.Variant == VariantType.CPI)
      {
        _managerList.Add(typeof(CPIRotatePanelManager),
          new CPIRotatePanelManager(greenScreenPanel.cpiRotatePanel, data));
      }
      _managerList.Add(typeof(RenderManager), 
        new RenderManager(canvasImageDisplay, imageWorking, imageRendered, 
                          imageBackground, imageForeground, data));
      _managerList.Add(typeof(ColorPickerDataPanelManager), 
        new ColorPickerDataPanelManager(data));
      _managerList.Add(typeof(CurvePanelManager), new CurvePanelManager(data));
      _managerList.Add(typeof(ImageDisplayManager), 
        new ImageDisplayManager(data, imageBackground, imageRendered, 
                                canvasImageDisplay, canvasImageDisplayClip));
      //_managerList.Add(typeof(ImageListManager), 
      //  new ImageListManager(mainWindow, imageRendered, imageList, data));
      _managerList.Add(typeof(MainDisplayManager), 
        new MainDisplayManager(greenScreenPanel, data));
      _managerList.Add(typeof(SelectionDataPanelManager), 
        new SelectionDataPanelManager(data));
      
      if (_variant == VariantType.ImageMatch)
        _managerList.Add(typeof(ISettingsPanelManager), new ImageMatchSettingPanelManager(greenScreenPanel, mainWindow, data));
      else if (_variant == VariantType.CPI)
        _managerList.Add(typeof(ISettingsPanelManager), new CPISettingsPanelManager(greenScreenPanel, mainWindow, data));
      
      _managerList.Add(typeof(BackgroundDefinitionToolManager), new BackgroundDefinitionToolManager(data));
      _managerList.Add(typeof(BezierToolManager), new BezierToolManager(imageRendered, imageBackground, PanelTracker.ISettingPanel.CanvasData, data));
      _managerList.Add(typeof(BrushToolManager), new BrushToolManager(canvasImageDisplay, imageForeground, imageBackground, imageRendered, imageWorking, data));
      _managerList.Add(typeof(ColorPickerToolManager), new ColorPickerToolManager(canvasImageDisplay, PanelTracker.ISettingPanel.CanvasData, imageBackground, imageRendered, data));
      _managerList.Add(typeof(HandToolManager), new HandToolManager(canvasImageDisplay, imageForeground, imageBackground, imageRendered, data));
      _managerList.Add(typeof(PolygonSelectionToolManager), new PolygonSelectionToolManager(canvasImageDisplay, PanelTracker.ISettingPanel.CanvasData, imageBackground, imageRendered, data));
      if (_variant == VariantType.ImageMatch)
      {
        _managerList.Add(typeof(DefaultToolbarManager), new DefaultToolbarManager(PanelTracker.ISettingPanel.CanvasData, greenScreenPanel.gridToolBar, data));
      }
      else if (_variant == VariantType.CPI)
      {
        _managerList.Add(typeof(DefaultToolbarManager), new CPIToolbarManager(PanelTracker.ISettingPanel.CanvasData, greenScreenPanel.gridToolBar, data));
      }
      _managerList.Add(typeof(ZoomToolManager), new ZoomToolManager(imageForeground, imageBackground, imageRendered, imageWorking, canvasImageDisplay, data));
      _managerList.Add(typeof(UndoRedoManager<System.Windows.Media.PathGeometry[]>), new UndoRedoManager<System.Windows.Media.PathGeometry[]>());
    }

    public static void AddManager(Type managerType, GenericManager manager)
    {
      _instance._managerList.Add( managerType, manager);
    }

    #endregion
  }
}
