﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using PhotoLynx.Logging;

namespace Flow.GreenScreen.Managers
{
  public class ErrorManager
  {
    protected static ErrorManager _instance = null;
    public static ErrorManager Instance 
    {
      get
      {
        if (_instance == null)
          _instance = new ErrorManager();
        return _instance;
      }
    }

    public delegate void ExceptionDelegate(Exception e, string msg);
    public ExceptionDelegate ErrorLogger { get; set; }

    protected ErrorManager()
    {
      ErrorLogger = new ExceptionDelegate(MessageBoxErrorLogger);
    }    

    public void LogError(Exception e)
    {
      LogError(e, "");
    }
    
    public void LogError(Exception e, string msg)
    {
      if (ErrorLogger != null)
        ErrorLogger(e, msg);
    }
    
    protected void MessageBoxErrorLogger(Exception e, string msg)
    {
      MessageBox.Show(msg + ": " + e.ToString(), "Exception");
      PlLog.Logger.Error(msg + ": " + e.ToString());
    }
    
    
  
  }
}
