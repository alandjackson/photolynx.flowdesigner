﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.GreenScreen.Panels;
using Flow.GreenScreen.Data;
using System.Windows.Controls;
using Flow.GreenScreen.Managers.Panels;
using System.Windows.Media;

namespace Flow.GreenScreen.Managers
{
  public class PanelTracker : GenericManager
  {

    #region Members

    static PanelTracker _instance;
    Dictionary<Type, UserControl> _dataPanels;

    #endregion

    #region Constructors

    PanelTracker(PanelData data)
      : base(data)
    {
      _dataPanels = new Dictionary<Type, UserControl>();
    }

    #endregion


    #region Properties

    public static ImageMatchToolbarPanel ImageMatchToolbarPanel
    {
      get { return _instance._dataPanels[typeof(ImageMatchToolbarPanel)] as ImageMatchToolbarPanel; }
    }

    public static ColorPickerDataPanel ColorPickerDataPanel
    {
      get { return _instance._dataPanels[typeof(ColorPickerDataPanel)] as ColorPickerDataPanel; }
    }

    public static CurvePanel CurvePanel
    {
      get { return _instance._dataPanels[typeof(CurvePanel)] as CurvePanel; }
    }

    public static SelectionDataPanel SelectionDataPanel
    {
      get { return _instance._dataPanels[typeof(SelectionDataPanel)] as SelectionDataPanel; }
    }

    public static ISettingsPanel ISettingPanel
    {
      get { return _instance._dataPanels[typeof(ISettingsPanel)] as ISettingsPanel; }
    }

    #endregion

    #region Methods

    public static void Initialize(PanelData data)
    {
      _instance = new PanelTracker(data);
    }

    public static void AddPanel(UserControl panel, Type panelType)
    {
      _instance._dataPanels.Add(panelType, panel);
    }

    public static System.Windows.Point CanvasToForeground(
      System.Windows.Point src, System.Windows.UIElement canvas, VariantType v)
    {
      // The cpi app moves the foreground when zooming/panning
      // so adjust the selection point
      if (false && v == VariantType.CPI)
      {
        double top = Canvas.GetTop(canvas);
        double left = Canvas.GetLeft(canvas);

        return new System.Windows.Point(src.X - left, src.Y - top);
      }
      return src;
    }


    public static void AppendTransform(Geometry g, Transform t)
    {
      if (g.Transform.Equals(Transform.Identity))
      {
        g.Transform = t;
      }
      else if (g.Transform is TransformGroup)
      {
        (g.Transform as TransformGroup).Children.Add(t);
      }
      else
      {
        TransformGroup grp = new TransformGroup();
        grp.Children.Add(g.Transform);
        grp.Children.Add(t);
        g.Transform = grp;
      }
    }
    #endregion

  }
}
