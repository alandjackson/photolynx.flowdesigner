﻿using System;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.GreenScreen.Panels;
using Flow.GreenScreen.Data;
using System.Windows.Controls;
using System.Windows;
using System.IO;
using System.Windows.Media.Imaging;
using Flow.GreenScreen.Renderer;
using Microsoft.Win32;
using System.Windows.Media.Animation;
using PhotoLynx.Logging;
using System.ComponentModel;
using System.Windows.Media;
using System.Reflection;
using Flow.Lib;

namespace Flow.GreenScreen.Managers.Panels
{
    public class CPISettingsPanelManager : GenericPanelManager<CPISettingsPanel>, ISettingsPanelManager
    {
        Window _mainWindow;
        List<string> _backgroundDirectories;
        string[] _backgroundPaths;
        Image _selectedImage, _imageThumb;
        bool _isRendering = false;

        public CPISettingsPanelManager(GreenScreenPanel panel, Window mainWindow, PanelData data)
            : base(data)
        {
            _mainWindow = mainWindow;
            _selectedImage = null;

            Panel = new CPISettingsPanel();
            panel.gridRightPanel.Children.Add(Panel);

            PanelTracker.AddPanel(Panel, typeof(ISettingsPanel));

            PopulateBackgroundList();

            Panel.comboBoxFolderList.SelectionChanged += new SelectionChangedEventHandler(comboBoxFolderList_SelectionChanged);
            Panel.buttonReset.Click += (s,e) => ResetSettings();
            Panel.buttonSave.Click += (s, e) => SaveSettings(SaveGsSettingsTo.Image);
            Panel.buttonApplyToSubject.Click += (s, e) => SaveSettings(SaveGsSettingsTo.Subject);
            Panel.buttonApplyToProject.Click += (s, e) => SaveSettings(SaveGsSettingsTo.Project);
            Panel.buttonApplyToFilter.Click += (s, e) => SaveSettings(SaveGsSettingsTo.Filter);
            Panel.buttonGsRender.Click += (s, e) => ToggleGsRender();
        }

        void ToggleGsRender()
        {
            if (! RenderEnabled)
            {
                Panel.buttonGsRender.Content = "Show Original Image";
            }
            else
            {
                Panel.buttonGsRender.Content = "Show GS Dropout";
            }
            ManagerTracker.RenderManager.RenderRefresh(true);

        }


        void SaveSettings(SaveGsSettingsTo applyTo)
        {
            if (applyTo == SaveGsSettingsTo.Project)
            {
                MessageBoxResult result = MessageBox.Show("This will apply the current GS settings to all images in your Project, overwriting any previous settings. Are you sure you want to continue?", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result == MessageBoxResult.No) return;
            }
            else if (applyTo == SaveGsSettingsTo.Filter)
            {
                MessageBoxResult result = MessageBox.Show("This will apply the current GS settings to all images in the project that have the current filter applied, overwriting any previous settings. Are you sure you want to continue?", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result == MessageBoxResult.No) return;
            }

            var config = _data.CurrentImage.CurrentSelectionConfiguration;

            // reset the cpi foreground variables because flow doesn't use them.
            config.ForegroundTopLeft = new Point();
            config.ForegroundScale = 1;

            _data.CurrentImage.ApplyConfigTo = applyTo;
            _data.CurrentImage.SelectionConfigurationXml = ObjectSerializer.SerializeString(config);

            NotifyUser("Settings Saved");
        }

        void ResetSettings()
        {
            _data.CurrentImage.SelectionConfigurationXml = "";
            ManagerTracker.DataManager.UpdateSelectedImage(_data.CurrentImage);
            //_data.CurrentImage.LoadConfiguration();
            NotifyUser("Settings Reset");
        }

        void NotifyUser(string message)
        {
            try
            {
                var notification = new NotificationProgressInfo(message, "Green Screen");
                notification.Display();
                notification.Complete("Green Screen");
                notification.Update();
            }
            catch (Exception)
            {
                System.Windows.MessageBox.Show(message);
            }
        }

        void buttonContinueToImport_Click(object sender, RoutedEventArgs e)
        {
            if (_isRendering)
            {
                MessageBox.Show("Please wait until the current image is finished processing before continuing", "Busy");
                return;
            }
            _mainWindow.DialogResult = true;
        }

        CPIPanelData CPIPanelData
        {
            get { return _data as CPIPanelData; }
        }

        void buttonQueueImage_Click(object sender, RoutedEventArgs e)
        {
            if (_isRendering)
            {
                MessageBox.Show("Please wait until the current image is finished processing before continuing", "Busy");
                return;
            }
            BeginRenderCurrentImage();
        }

        void BeginRenderCurrentImage()
        {
            new Thread(new ThreadStart(RenderCurrentImage)).Start();
        }

        void RenderCurrentImage()
        {

            try
            {
                string imgPath = "";
                string bgPath = "";
                string selectionConfigXml = "";
                _isRendering = true;
                ManagerTracker.MainDisplayManager.MainWindow.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, (Action)delegate
                {
                    ManagerTracker.MainDisplayManager.MouseBusy(true);
                    ManagerTracker.MainDisplayManager.SetStatusMessage("Rendering Green Screen Image...");

                    bgPath = BackgroundPath;
                    selectionConfigXml = ObjectSerializer.SerializeString(
                        _data.CurrentImage.CurrentSelectionConfiguration, true);
                    imgPath = _data.CurrentImage.FilePath;

                });

                new CPIRenderer(imgPath, bgPath, selectionConfigXml)
                    .Render(new ProgressChangedEventHandler(RenderProgressChangedHandler));
            }
            catch (Exception e)
            {
                ErrorManager.Instance.LogError(e, "Error rendering image");
            }
            finally
            {
                _isRendering = false;
                ManagerTracker.MainDisplayManager.MainWindow.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, (Action)delegate
                {
                    ManagerTracker.MainDisplayManager.SetStatusMessage("Rendering Green Screen Image...Done");
                    ManagerTracker.MainDisplayManager.MouseIdle(true);
                });
            }
        }

        Dictionary<Image, string> _backgroundThumbsToPath = new Dictionary<Image, string>();

        int _lastPercentage = -1;
        string _lastMessage = "";
        void RenderProgressChangedHandler(object sender, System.ComponentModel.ProgressChangedEventArgs args)
        {
            if (args.UserState == null) return;
            if (_lastPercentage == args.ProgressPercentage && _lastMessage == args.UserState.ToString()) return;
            _lastPercentage = args.ProgressPercentage;
            _lastMessage = args.UserState.ToString();
            ManagerTracker.MainDisplayManager.MainWindow.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, (Action)delegate
            {
                ManagerTracker.MainDisplayManager.SetStatusMessage("Rendering Image - " + args.ProgressPercentage + "% - " + args.UserState.ToString());
            });
        }

        void comboBoxFolderList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            BeginUpdateSelectedBackgroundDirectory(_backgroundDirectories[Panel.comboBoxFolderList.Items.IndexOf(e.AddedItems[0])]);
        }

        public void BeginUpdateSelectedBackgroundDirectory(string newBackgroundDir)
        {
            Thread t = new Thread(new ParameterizedThreadStart(delegate(object s)
            {
                UpdateSelectedBackgroundDirectory((string)s, true);
            }));
            t.SetApartmentState(ApartmentState.STA);
            t.Start(newBackgroundDir);
        }

        public void UpdateSelectedBackgroundDirectory(string newBackgroundDir, bool includeSubDirs)
        {
            try
            {
                List<UIElement> bkChildren = new List<UIElement>();

                _backgroundThumbsToPath = new Dictionary<Image, string>();
                Panel.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, (Action)delegate
                {
                    Panel.gridBackgroundList.Children.Clear();
                });

                List<string> tmpbackgrounds = new List<string>();
                if(includeSubDirs)
                    tmpbackgrounds.AddRange(Directory.GetFiles(newBackgroundDir, "*.jpg", SearchOption.AllDirectories));
                else
                    tmpbackgrounds.AddRange(Directory.GetFiles(newBackgroundDir, "*.jpg", SearchOption.TopDirectoryOnly));

                if (includeSubDirs)
                    tmpbackgrounds.AddRange(Directory.GetFiles(newBackgroundDir, "*.png", SearchOption.AllDirectories));
                else
                    tmpbackgrounds.AddRange(Directory.GetFiles(newBackgroundDir, "*.png", SearchOption.TopDirectoryOnly));

                _backgroundPaths = tmpbackgrounds.ToArray();

                int width, height;

                using (System.Drawing.Bitmap background = new System.Drawing.Bitmap(_backgroundPaths[0]))
                {

                    if (background.Width > background.Height)
                    {
                        width = 200;
                        height = 200 * background.Height / background.Width;
                    }
                    else
                    {
                        height = 200;
                        width = 200 * background.Width / background.Height;
                    }
                }

                if (_imageThumb == null)
                {
                    Panel.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, (Action)delegate
                    {
                        Panel.canvasMain.Children.Remove(_imageThumb);
                    });
                }

                int rowcol = 0;

                Panel.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, (Action)delegate
                {

                    _imageThumb = new Image();
                    _imageThumb.Width = width;
                    _imageThumb.Height = height;
                    _imageThumb.IsHitTestVisible = false;
                    _imageThumb.Visibility = Visibility.Hidden;
                    
                    Panel.canvasMain.Children.Add(_imageThumb);

                    rowcol = (int)Math.Ceiling(Math.Sqrt(_backgroundPaths.Count()));

                    Panel.gridBackgroundList.Rows = rowcol;
                    Panel.gridBackgroundList.Columns = rowcol;
                });

                
                

                //int i = 0;
                foreach (string path in _backgroundPaths)
                {
                    MemoryStream ms = PhotoLynx.ImageCore.ImageLoader.ReadImageToStream(path);

                    {
                        BitmapImage image = null;
                        Panel.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, (Action)delegate
                        {
                            image = new BitmapImage();
                            image.DownloadCompleted += delegate { ms.Dispose(); };
                            image.BeginInit();
                            image.DecodePixelHeight = height;
                            image.DecodePixelWidth = width;
                            image.StreamSource = ms;
                            image.EndInit();

                            BackgroundThumbnail bkgThumb = new BackgroundThumbnail();
                            bkgThumb.imgBackground.Source = image;
                            if (path.ToLower().Contains("premium"))
                                bkgThumb.imgPremiumOverlay.Visibility = Visibility.Visible;
                            else
                                bkgThumb.imgPremiumOverlay.Visibility = Visibility.Collapsed;

                            bkgThumb.MouseEnter += new System.Windows.Input.MouseEventHandler(backgroundThumb_MouseEnter);
                            bkgThumb.MouseLeave += new System.Windows.Input.MouseEventHandler(backgroundThumb_MouseLeave);
                            bkgThumb.MouseLeftButtonUp += new System.Windows.Input.MouseButtonEventHandler(backgroundThumb_MouseLeftButtonUp);

                            bkgThumb.UpdateLayout();
                            Panel.gridBackgroundList.Children.Add(bkgThumb);

                            _backgroundThumbsToPath[bkgThumb.imgBackground] = path;
                        });
                    }
                }

            }
            catch (Exception e)
            {
                ErrorManager.Instance.LogError(e);
            }

        }


        void backgroundThumb_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            ManagerTracker.RenderManager.RestoreBackground();
        }

        void backgroundThumb_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
        }

        void backgroundThumb_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            ManagerTracker.RenderManager.PreviewBackground((sender as BackgroundThumbnail).imgBackground.Source);
        }

        void backgroundThumb_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            UpdateBackgroundFromThumb((BackgroundThumbnail)sender);
        }

        void UpdateBackgroundFromThumb(BackgroundThumbnail sender)
        {
            _selectedImage = sender.imgBackground;
            ManagerTracker.RenderManager.UpdateBackgroundImage(BackgroundPath);
        }

        private void PopulateBackgroundList()
        {
            try
            {
                _backgroundDirectories = new List<string>();

                string bgBaseDir = CPIGreenScreen.Util.CPISettings.Instance.BackgroundFolder;
                if (!Directory.Exists(bgBaseDir)) bgBaseDir = @"c:\";
                string[] directories = Directory.GetDirectories(bgBaseDir);


                // Set the default background directory to the first name
                if (Panel.comboBoxFolderList.Items.Count > 0)
                {
                    Panel.comboBoxFolderList.SelectedIndex = 0;
                    BeginUpdateSelectedBackgroundDirectory(directories[0]);
                }
                else if (Directory.GetFiles(bgBaseDir, "*.jpg").Length > 0)
                {
                    _backgroundDirectories.Add(bgBaseDir);
                    Panel.comboBoxFolderList.Items.Add(Path.GetFileName(bgBaseDir));
                    Panel.comboBoxFolderList.SelectedIndex = 0;
                    BeginUpdateSelectedBackgroundDirectory(bgBaseDir);
                }
            }
            catch (Exception e)
            {
                ErrorManager.Instance.LogError(e);
            }
        }

        public string BackgroundPath
        {
            get
            {
                if (_selectedImage != null && _backgroundThumbsToPath.ContainsKey(_selectedImage))
                {
                    return _backgroundThumbsToPath[_selectedImage];

                    //return (_selectedImage.Source as BitmapImage).UriSource.LocalPath;
                }
                return "";
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    _selectedImage = null;
                    ManagerTracker.RenderManager.UpdateBackgroundImage("");
                    return;
                }

                foreach (BackgroundThumbnail backgroundThumb in Panel.gridBackgroundList.Children)
                {
                    if (_backgroundThumbsToPath[backgroundThumb.imgBackground] == value)
                    {
                        UpdateBackgroundFromThumb(backgroundThumb);
                        //_selectedImage = backgroundThumb.imgBackground;
                        //ManagerTracker.RenderManager.UpdateBackgroundImage(value);
                    }
                }
            }
        }

        public bool RenderEnabled
        {
            get { return (Panel.buttonGsRender.Content as string) == "Show Original Image"; }
            set { }
        }

        public void RecordChanged()
        {

        }

        public bool IsBackgroundValid()
        {
            return true;
        }

        public void LoadBitmaps()
        {
            //no bitmaps to load
        }

    }
}
