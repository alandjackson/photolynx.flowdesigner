﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.GreenScreen.Data;

namespace Flow.GreenScreen.Managers.Panels
{
  public class GenericPanelManager<T_PanelType> : GenericManager
  {
    #region Consructors

    public GenericPanelManager():this(null)
    {

    }

    public GenericPanelManager(PanelData data)
      : base(data)
    {

    }

    #endregion

    #region Properties

    public T_PanelType Panel
    {
      get;
      set;
    }

    #endregion
  }
}
