using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows;
using Flow.GreenScreen.Panels;
using Flow.GreenScreen.Data;
using Microsoft.Win32;
using PhotoLynx.CommonWpfControls.Dialogs;
using Flow.GreenScreen.Dialogs;

namespace Flow.GreenScreen.Managers.Panels
{

  public enum VariantType
  {
    ImageMatch,
    CPI
  }

  public interface ISettingsPanelManager
  {
    string BackgroundPath { get; set; }
    bool RenderEnabled { get; set; }

    void RecordChanged();
    bool IsBackgroundValid();
    void LoadBitmaps();

  }
}