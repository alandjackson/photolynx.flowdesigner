﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Imaging;
using System.Windows;
using System.Windows.Controls;
using Flow.GreenScreen.Data;
using PhotoLynx.ImageThumbnailer;

namespace Flow.GreenScreen.Managers.Panels
{
    public class ImageListManager : GenericManager
    {
        #region Members

        public int StartingIndex;

        Image _imageDisplay;
        PlImageList _imageList;
        public PlImageList ImageList { get { return _imageList; } }
        Window _mainWindow;

        #endregion

        #region Constructors

        public ImageListManager(Window mainWindow, Image imageDisplay, PlImageList imageList, PanelData data)
            : base(data)
        {
            _mainWindow = mainWindow;
            _imageList = imageList;
            _imageDisplay = imageDisplay;

            if (_imageList != null)
            {
                _imageList.DataContext = _data.ImagesInFilter;
                _imageList.SelectionChanged += new PlSelectionChangedEventHandler(thumbsimagelist_selectionchanged);
                _imageList.Loaded += new RoutedEventHandler(thumbsImageList_Loaded);
            }



        }

        #endregion

        #region Methods

        public void thumbsimagelist_selectionchanged(
          object sender, PlSelectionChangedEventArgs e)
        {
            try
            {
                if (e.RemovedItems.Count == 0)
                {
                    e.RemovedItems.Add(_data.ImagesInFilter[0]);
                }
                ManagerTracker.DataManager.RecordSelectionChanged(e.RemovedItems);
            }
            catch (Exception err)
            {
                ErrorManager.Instance.LogError(err, "Error updating selection");
            }
        }

        void thumbsImageList_Loaded(object sender, RoutedEventArgs e)
        {
            if (_data.ImagesInFilter.Count == 0)
            {
                MessageBox.Show("No images have been loaded. Confirm preferences are correct.",
                  "Green screen error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                _data.AllowClose = true;
                _mainWindow.Close();
                return;
            }
            //ManagerTracker.DataManager.ImageList.SelectedIndex = StartingIndex;

            PanelTracker.CurvePanel.comboBoxCurveList.ItemsSource = _data.ControlPointProfiles.List;
            PanelTracker.CurvePanel.comboBoxCurveList.SelectionChanged += new SelectionChangedEventHandler(ManagerTracker.CurvePanelManager.comboBoxCurveList_SelectionChanged);
            PanelTracker.CurvePanel.comboBoxCurveList.SelectedIndex = -1;
            PanelTracker.CurvePanel.buttonSaveCurrentCurve.Click += new RoutedEventHandler(ManagerTracker.CurvePanelManager.buttonSaveCurrentCurve_Click);
            PanelTracker.CurvePanel.buttonRemoveCurve.Click += new RoutedEventHandler(ManagerTracker.CurvePanelManager.buttonRemoveCurve_Click);
            //PanelTracker.CurvePanel.buttonBrowse.Click += new RoutedEventHandler(ManagerTracker.CurvePanelManager.buttonBrowse_Click);

            ManagerTracker.MainDisplayManager.MouseIdle();
        }

        #endregion
    }
}
