﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.GreenScreen.Panels;
using System.Windows;
using System.Windows.Controls;
using Flow.GreenScreen.Data;
using System.Windows.Input;
using Microsoft.Win32;
using PhotoLynx.CommonWpfControls.Dialogs;
using Flow.GreenScreen.Dialogs;
using Flow.GreenScreen.Util;
using System.Windows.Media.Imaging;
using System.Collections.ObjectModel;

namespace Flow.GreenScreen.Managers.Panels
{
    public class ImageMatchSettingPanelManager : GenericPanelManager<ImageMatchSettingPanel>, ISettingsPanelManager
    {
        #region Members

        Window _window;

        #endregion

        #region Constructors

        public ImageMatchSettingPanelManager(GreenScreenPanel gsPanel, Window mainWindow, PanelData data)
            : base(data)
        {
            _window = mainWindow;

            Panel = new ImageMatchSettingPanel();

            Panel.buttonRevertSettings.Click += new RoutedEventHandler(buttonRevertSettings_Click);
            Panel.buttonBackgroundBrowse.Click += new RoutedEventHandler(buttonBackgroundBrowse_Click);
            Panel.buttonOpenBackroundDialog.Click += new RoutedEventHandler(buttonBackroundDialog_Click);
            Panel.buttonApply.Click += new RoutedEventHandler(buttonApply_Click);
            Panel.buttonColorApplyToAll.Click += new RoutedEventHandler(buttonColorApplyToAll_Click);
            Panel.buttonClose.Click += new RoutedEventHandler(buttonClose_Click);

            Panel.comboBoxPackageBackgounds.SelectionChanged += new SelectionChangedEventHandler(comboBoxPackageBackgounds_SelectionChanged);
            Panel.comboBoxPackageBackgounds.KeyUp += new KeyEventHandler(comboBoxPackageBackgounds_KeyUp);

            Panel.checkBoxRender.Click += new RoutedEventHandler(checkBoxRender_Clicked);
            Panel.buttonSettingsRemove.Click += new RoutedEventHandler(buttonSettingsRemove_Click);
            Panel.buttonSettingsDefault.Click += new RoutedEventHandler(buttonSettingsDefault_Click);

            Panel.checkBoxRender.IsChecked = true;
            //Panel.checkBoxRender.Visibility = Visibility.Hidden;

            gsPanel.gridRightPanel.Children.Add(Panel);
            
            LoadPresetsIntoDropdown();
            Panel.comboPresets.SelectionChanged += comboPresets_SelectionChanged;

            PanelTracker.AddPanel(Panel, typeof(ISettingsPanel));
        }

        bool _inComboSelection = false;
        void comboPresets_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (_inComboSelection) return;

            _inComboSelection = true;
            if (Panel.comboItemEditPresets.IsSelected)
            {
                if (EditPresets())
                {
                    LoadPresetsIntoDropdown();
                    Panel.comboPresets.SelectedIndex = 0;
                    LoadNewSelectionConfig(new SelectionConfiguration(false));
                }
            }
            else if (Panel.comboPresets.SelectedIndex == 0)
                LoadNewSelectionConfig(new SelectionConfiguration(false));
            else if (Panel.comboPresets.SelectedItem is ColorDropoutPreset)
                LoadNewPreset(Panel.comboPresets.SelectedItem as ColorDropoutPreset);

            _inComboSelection = false;
        }

        void LoadPresetsIntoDropdown()
        {
            while (Panel.comboPresets.Items.Count > 3)
                Panel.comboPresets.Items.RemoveAt(1);

            foreach (var preset in GetPresetSettings().Reverse())
            {
                Panel.comboPresets.Items.Insert(1, preset);
            }

        }

        void LoadNewPreset(ColorDropoutPreset preset) 
        {

            var newConfig = new SelectionConfiguration(true);
            newConfig.ColorAndBackgroundConfiguration.BackgroundDefinitionBlue = (byte)preset.Blue;
            newConfig.ColorAndBackgroundConfiguration.BackgroundDefinitionRed = (byte)preset.Red;
            newConfig.ColorAndBackgroundConfiguration.BackgroundDefinitionGreen = (byte) preset.Green;

            LoadNewSelectionConfig(newConfig);
        }

        static ColorDropoutPreset[] GetPresetSettings()
        {
            var presetsXml = GreenScreenSettings.Instance.ColorPresets.Value;
            var presets = ObjectSerializer.DeserializeOrDefault<ColorDropoutPreset[]>(presetsXml)
                ?? new ColorDropoutPreset[0];
            return presets;
        }

        bool EditPresets()
        {
            var presets = GetPresetSettings();
            var curColor = _data.CurrentImage.CurrentSelectionConfiguration.ColorAndBackgroundConfiguration;

            var vm = new EditPresentsViewModel();
            vm.Presets = new ObservableCollection<ColorDropoutPreset>(presets.ToList());
            vm.DefaultRed = curColor.BackgroundDefinitionRed;
            vm.DefaultBlue = curColor.BackgroundDefinitionBlue;
            vm.DefaultGreen = curColor.BackgroundDefinitionGreen;

            if (vm.Presets.Count > 0)
                vm.Selected = vm.Presets[0];

            var dlg = new EditPresets();
            dlg.DataContext = vm;
            if (dlg.ShowDialog() == true)
            {
                GreenScreenSettings.Instance.ColorPresets.SaveValue(
                    ObjectSerializer.SerializeString(vm.Presets.ToArray()));
                return true;
            }
            return false;
        }


        #endregion

        #region Properties

        public string BackgroundPath
        {
            get
            {
                return Panel.comboBoxPackageBackgounds.Text;
            }
            set
            {
                Panel.comboBoxPackageBackgounds.Text = value;
            }
        }

        public bool RenderEnabled
        {
            get
            {
                return Panel.checkBoxRender.IsChecked.Value;
            }
            set
            {
                Panel.checkBoxRender.IsChecked = value;
            }
        }

        #endregion

        #region Methods

        void buttonBackgroundBrowse_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            if (Convert.ToBoolean(ofd.ShowDialog(_window)))
            {
                if (!ManagerTracker.DataManager.IsImage(ofd.FileName))
                {
                    return;
                }
                ManagerTracker.MainDisplayManager.MouseBusy();

                Panel.comboBoxPackageBackgounds.SelectedIndex = 0;
                Panel.comboBoxPackageBackgounds.Text = ofd.FileName;
                _data.CurrentImage.CurrentSelectionConfiguration.ColorAndBackgroundConfiguration.BackgroundImagePath = ofd.FileName;
                ManagerTracker.RenderManager.UpdateBackgroundImage();
                ManagerTracker.MainDisplayManager.MouseIdle();
            }
        }

        public void comboBoxPackageBackgounds_TextChanged(object sender, TextChangedEventArgs e)
        {
            ManagerTracker.RenderManager.UpdateBackgroundImage();
        }

        public void buttonBrowse_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Curve profile (.crv)|*.crv";
            ofd.InitialDirectory = System.IO.Path.GetDirectoryName(_data.ControlPointProfilePath);
            if (ofd.ShowDialog().Value)
            {
                _data.LoadControlPointProfiles(ofd.FileName);
                PanelTracker.CurvePanel.comboBoxCurveList.ItemsSource = _data.ControlPointProfiles.List;
                PanelTracker.CurvePanel.comboBoxCurveList.Items.Refresh();
            }
        }


        void buttonApply_Click(object sender, RoutedEventArgs e)
        {//Save current settings to current image

            GenericDialog dg = new GenericDialog(Panel);
            dg.Title = "Apply all settings...";
            dg.AddDialogButton("Apply to current", 0);
            dg.AddDialogButton("Apply to all", 1);
            dg.AddDialogButton("Cancel", 2);
            dg.ShowDialog();

            if (dg.Result == 0)
            {

                if (ManagerTracker.DataManager.IsImage(Panel.comboBoxPackageBackgounds.Text) || Panel.comboBoxPackageBackgounds.Text.Equals(""))
                {
                    _data.CurrentImage.CurrentSelectionConfiguration.ColorAndBackgroundConfiguration.BackgroundImagePath = Panel.comboBoxPackageBackgounds.Text.ToString();
                }
                _data.CurrentImage.CurrentSelectionConfiguration.ColorAndBackgroundConfiguration.ControlPoints = PanelTracker.CurvePanel.GetControlPoints();
                if (!_data.CurrentImage.SaveConfigurationData(_data.CurrentImage.CurrentSelectionConfiguration))
                {
                    MessageBox.Show(_window, "ERROR: Could not save data.", "Cannot save data", MessageBoxButton.OK, MessageBoxImage.Error);
                    _data.AllowClose = true;
                    _window.Close();
                }
                _data.SaveConfigurationChanges();
            }
            if (dg.Result == 1)
            {
                if (ManagerTracker.DataManager.IsImage(Panel.comboBoxPackageBackgounds.Text) || Panel.comboBoxPackageBackgounds.Text.Equals(""))
                {
                    _data.CurrentImage.CurrentSelectionConfiguration.ColorAndBackgroundConfiguration.BackgroundImagePath = Panel.comboBoxPackageBackgounds.Text.ToString();
                }
                _data.CurrentImage.CurrentSelectionConfiguration.ColorAndBackgroundConfiguration.ControlPoints = PanelTracker.CurvePanel.GetControlPoints();


                foreach (ImageData image in _data.ImagesInFilter)
                {
                    if (!image.SaveConfigurationData(_data.CurrentImage.CurrentSelectionConfiguration))
                    {
                        MessageBox.Show(_window, "ERROR: Could not save data.", "Cannot save data", MessageBoxButton.OK, MessageBoxImage.Error);
                        _data.AllowClose = true;
                        _window.Close();
                    }
                }

                _data.SaveConfigurationChanges();
            }
        }


        void buttonRevertSettings_Click(object sender, RoutedEventArgs e)
        {
            SelectionConfiguration newConfig = SelectionConfiguration.DeserializeFromString(_data.CurrentImage.SelectionConfigurationXml);

            if (!newConfig.Equals(_data.CurrentImage.CurrentSelectionConfiguration))
            {
                //_data.CurrentRenderedBitmapSourceValid = false;
                _data.CurrentImage.CurrentSelectionConfiguration = newConfig;
                PanelTracker.CurvePanel.SetControlPoints(_data.CurrentImage.CurrentSelectionConfiguration.ColorAndBackgroundConfiguration.ControlPoints);
                ManagerTracker.CurvePanelManager.CurvePanelControlPointChanged(_data.CurrentImage.CurrentSelectionConfiguration.ColorAndBackgroundConfiguration.ControlPoints, false);
            }

            PanelTracker.CurvePanel.comboBoxCurveList.SelectedIndex = -1;

            ManagerTracker.RenderManager.RenderRefresh();
        }

        void comboBoxPackageBackgounds_KeyUp(object sender, KeyEventArgs e)
        {
            if (ManagerTracker.DataManager.IsImage(Panel.comboBoxPackageBackgounds.Text))
            {
                ManagerTracker.RenderManager.UpdateBackgroundImage();
            }
        }

        void comboBoxPackageBackgounds_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                if (ManagerTracker.DataManager.IsImage((string)e.AddedItems[0]) || e.AddedItems[0].Equals(""))
                {
                    ManagerTracker.RenderManager.UpdateBackgroundImage((string)e.AddedItems[0]);
                }
            }
            else
            {
                ManagerTracker.RenderManager.UpdateBackgroundImage("");
            }
        }

        void comboBoxPackageBackgounds_TextInput(object sender, TextCompositionEventArgs e)
        {
            if (ManagerTracker.DataManager.IsImage(e.Text))
            {
                ManagerTracker.RenderManager.UpdateBackgroundImage();
            }
        }

        void buttonColorApplyToAll_Click(object sender, RoutedEventArgs e)
        {
            DateTime start = DateTime.Now;
            ManagerTracker.MainDisplayManager.MouseBusy();
            _data.CurrentImage.CurrentSelectionConfiguration.ColorAndBackgroundConfiguration.ControlPoints = PanelTracker.CurvePanel.GetControlPoints();
            foreach (ImageData image in _data.ImagesInFilter)
            {
                image.CurrentSelectionConfiguration.ColorAndBackgroundConfiguration = _data.CurrentImage.CurrentSelectionConfiguration.ColorAndBackgroundConfiguration.CopyDeep();
                if (!image.SaveColorSettings(_data.CurrentImage.CurrentSelectionConfiguration.ColorAndBackgroundConfiguration))
                {
                    _window.Close();
                }
                if (image != _data.CurrentImage)
                {
                    image.CurrentSelectionConfiguration = null;
                }
            }

            _data.SaveConfigurationChanges();

            ManagerTracker.MainDisplayManager.MouseIdle();
            DateTime end = DateTime.Now;
            PhotoLynx.Logging.PlLog.Logger.Info("Apply color to all completed in: " + (end - start).TotalMilliseconds.ToString());
        }

        void buttonClose_Click(object sender, RoutedEventArgs e)
        {//Close without saving
            if (!ManagerTracker.DataManager.VerifyClose())
            {
                return;
            }
            _window.Close();
        }


        void buttonBackroundDialog_Click(object sender, RoutedEventArgs e)
        {
            BackgroundDialog dialog = new BackgroundDialog();
            dialog.textBoxCurrentBackground.Text = Panel.comboBoxPackageBackgounds.Text;
            dialog.ShowDialog();
            _data.CurrentImage.CurrentSelectionConfiguration.ColorAndBackgroundConfiguration.BackgroundImagePath = Panel.comboBoxPackageBackgounds.Text;
            switch (dialog.Result)
            {
                case BackgroundDialog.DialogResult.ApplyToAll:
                    ApplyCurrentBackgroundToAllImages();
                    break;
                case BackgroundDialog.DialogResult.AutoAssign:
                    AutoAssignBackgroundToAllImages();
                    _data.CurrentImage.CurrentSelectionConfiguration = null;
                    Panel.comboBoxPackageBackgounds.Text = _data.CurrentImage.CurrentSelectionConfiguration.ColorAndBackgroundConfiguration.BackgroundImagePath;
                    break;
                default:
                    break;
            }
        }

        void sliderBackgroundStrength_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            _data.CurrentImage.CurrentSelectionConfiguration.ColorAndBackgroundConfiguration.BackgroundCorrection = PanelTracker.ColorPickerDataPanel.sliderBackgroundStrength.Value;
            _data.CurrentRenderedBitmapSourceDropoutValid = false;
            ManagerTracker.RenderManager.RenderGreenScreenCurrentSettings(false, false);
        }


        private void AutoAssignBackgroundToAllImages()
        {
            ManagerTracker.MainDisplayManager.MouseBusy();
            foreach (ImageData image in _data.ImagesInFilter)
            {
                if (image.PackageBackgrounds.Count > 0)
                {
                    if (!image.SaveBackground(image.PackageBackgrounds[0]))
                    {
                        _window.Close();
                    }
                    if (image != _data.CurrentImage)
                    {
                        image.CurrentSelectionConfiguration = null;
                    }
                }
            }
            _data.SaveConfigurationChanges();
            ManagerTracker.MainDisplayManager.MouseIdle();
        }


        void buttonSettingsRemove_Click(object sender, RoutedEventArgs e)
        {
            LoadNewSelectionConfig(new SelectionConfiguration(false));
        }

        void LoadNewSelectionConfig(SelectionConfiguration newConfig)
        {
            if (!newConfig.Equals(_data.CurrentImage.CurrentSelectionConfiguration))
            {
                _data.CurrentImage.CurrentSelectionConfiguration.Dispose();
                _data.CurrentImage.CurrentSelectionConfiguration = newConfig;
                PanelTracker.CurvePanel.SetControlPoints(
                    _data.CurrentImage.CurrentSelectionConfiguration.ColorAndBackgroundConfiguration.ControlPoints);

                ManagerTracker.CurvePanelManager.CurvePanelControlPointChanged(
                    _data.CurrentImage.CurrentSelectionConfiguration.ColorAndBackgroundConfiguration.ControlPoints,
                    false);
            }

            PanelTracker.CurvePanel.comboBoxCurveList.SelectedIndex = -1;
            ManagerTracker.RenderManager.RenderRefresh();
            PanelTracker.SelectionDataPanel.RefreshList();
        }

        void buttonSettingsDefault_Click(object sender, RoutedEventArgs e)
        {
            LoadNewSelectionConfig(new SelectionConfiguration(true));
        }


        void checkBoxRender_Clicked(object sender, RoutedEventArgs e)
        {
            ManagerTracker.RenderManager.RenderRefresh(true);
        }

        void ApplyCurrentBackgroundToAllImages()
        {
            ManagerTracker.MainDisplayManager.MouseBusy();

            string background = _data.CurrentImage.CurrentSelectionConfiguration.ColorAndBackgroundConfiguration.BackgroundImagePath;
            foreach (ImageData image in _data.ImagesInFilter)
            {
                if (!image.SaveBackground(background))
                {
                    _window.Close();
                }
                if (image != _data.CurrentImage)
                {
                    image.CurrentSelectionConfiguration = null;
                }

            }
            _data.SaveConfigurationChanges();
            ManagerTracker.MainDisplayManager.MouseIdle();
        }

        public void RecordChanged()
        {
            string currentBackground = _data.CurrentImage.CurrentSelectionConfiguration.ColorAndBackgroundConfiguration.BackgroundImagePath;
            Panel.comboBoxPackageBackgounds.Items.Clear();
            Panel.comboBoxPackageBackgounds.Items.Add("");
            foreach (string item in _data.CurrentImage.PackageBackgrounds)
            {
                Panel.comboBoxPackageBackgounds.Items.Add(item);
            }

            Panel.comboBoxPackageBackgounds.Text = currentBackground;
        }

        public void LoadBitmaps()
        {
            Panel.buttonApplyIcon.Source = BitmapInterop.CreateBitmapSourceFromHBitmap(BitmapInterop.GetHBitmap(Properties.Resources.applyIcon),
             IntPtr.Zero, new Int32Rect(0, 0, Properties.Resources.applyIcon.Width, Properties.Resources.applyIcon.Height), BitmapSizeOptions.FromEmptyOptions());


            Panel.buttonCloseIcon.Source = BitmapInterop.CreateBitmapSourceFromHBitmap(BitmapInterop.GetHBitmap(Properties.Resources.closeIcon),
              IntPtr.Zero, new Int32Rect(0, 0, Properties.Resources.closeIcon.Width, Properties.Resources.closeIcon.Height), BitmapSizeOptions.FromEmptyOptions());

        }

        public bool IsBackgroundValid()
        {
            return ManagerTracker.DataManager.IsImage(Panel.comboBoxPackageBackgounds.Text);

        }
        #endregion


    }
}
