﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.GreenScreen.Panels;
using Flow.GreenScreen.Data;
using System.Windows;
using System.Windows.Input;
using System.ComponentModel;
using System.IO;
using System.Diagnostics;

namespace Flow.GreenScreen.Managers.Panels
{
    public class MainDisplayManager : GenericPanelManager<GreenScreenPanel>
    {

        //Window _mainWindow;
        public Window MainWindow { get { return Window.GetWindow(Panel); } }
        int _mouseWaitCount;

        public MainDisplayManager(GreenScreenPanel panel, PanelData data)
            : base(data)
        {
            //_mainWindow = window;
            Panel = panel;

            //_mainWindow.Width = 1044;
            //_mainWindow.Height = 803;
            //_mainWindow.Left = 0;
            //_mainWindow.Top = 0;
            var window = Window.GetWindow(panel);
            if (window != null)
                window.KeyDown += new KeyEventHandler(Window_KeyDown);
            //_mainWindow.Closing += new CancelEventHandler(Window_Closing);
            //_mainWindow.Closed += new EventHandler(Window_Closed);

            if (data.Variant == VariantType.ImageMatch)
            {
                panel.recordsetInfoPanel.uxShowCrop.IsChecked = GreenScreenSettings.Instance.ShowCrop.Value;
                UpdateCropVisibility();

                panel.recordsetInfoPanel.uxFilterDeletes.IsChecked = GreenScreenSettings.Instance.FilterDeletes.Value;
                panel.recordsetInfoPanel.uxFilterDuplicates.IsChecked = GreenScreenSettings.Instance.FilterDuplicates.Value;

                // update the start index to account for the filter
                ImageData startingData;
                if (Panel.StartingIndex > 0 && Panel.StartingIndex < _data.Images.Count)
                    startingData = _data.Images[Panel.StartingIndex];
                else if (_data.Images.Count > 0)
                    startingData = _data.Images[0];
                else
                    throw new Exception("Error initializing MainDisplayManager, no images");

                _data.UpdateFilter((bool)Panel.recordsetInfoPanel.uxFilterDeletes.IsChecked,
                                   (bool)Panel.recordsetInfoPanel.uxFilterDuplicates.IsChecked);
                //Panel.thumbsImageList.DataContext = _data.ImagesInFilter;

                Panel.StartingIndex = _data.ImagesInFilter.IndexOf(startingData);

                panel.recordsetInfoPanel.uxShowCrop.Click += new RoutedEventHandler(uxShowCrop_Click);
                panel.recordsetInfoPanel.uxSaveJob.Click += new RoutedEventHandler(uxSaveJob_Click);
                panel.recordsetInfoPanel.uxFilterDeletes.Click += new RoutedEventHandler(uxFilterDeletes_Click);
                panel.recordsetInfoPanel.uxFilterDuplicates.Click += new RoutedEventHandler(uxFilterDuplicates_Click);
                data.PropertyChanged += new PropertyChangedEventHandler(data_PropertyChanged);
            }
            else if (data.Variant == VariantType.CPI)
            {
                UpdateCropVisibility(false);
            }


            PanelTracker.AddPanel(Panel, Panel.GetType());
        }

        void uxSaveJob_Click(object sender, RoutedEventArgs e)
        {
            Panel.Data.OnSaveJob();
        }

        void uxShowCrop_Click(object sender, RoutedEventArgs e)
        {
            GreenScreenSettings.Instance.ShowCrop.Value = (bool)Panel.recordsetInfoPanel.uxShowCrop.IsChecked;
            UpdateCropVisibility();
        }

        public void UpdateCropVisibility()
        {
            UpdateCropVisibility(GreenScreenSettings.Instance.ShowCrop.Value);
        }

        public void UpdateCropVisibility(bool visible)
        {
            Panel.cropRectangle.Visibility = (visible ? Visibility.Visible : Visibility.Hidden);
            Panel.invertCropRectangle.Visibility = (visible ? Visibility.Visible : Visibility.Hidden);
        }

        void data_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            //Panel.thumbsImageList.DataContext = _data.ImagesInFilter;
        }

        void uxFilterDuplicates_Click(object sender, RoutedEventArgs e)
        {
            _data.UpdateFilter((bool)Panel.recordsetInfoPanel.uxFilterDeletes.IsChecked,
                               (bool)Panel.recordsetInfoPanel.uxFilterDuplicates.IsChecked);
            GreenScreenSettings.Instance.FilterDuplicates.SaveValue((bool)Panel.recordsetInfoPanel.uxFilterDuplicates.IsChecked);
        }

        void uxFilterDeletes_Click(object sender, RoutedEventArgs e)
        {
            _data.UpdateFilter((bool)Panel.recordsetInfoPanel.uxFilterDeletes.IsChecked,
                               (bool)Panel.recordsetInfoPanel.uxFilterDuplicates.IsChecked);
            GreenScreenSettings.Instance.FilterDeletes.SaveValue((bool)Panel.recordsetInfoPanel.uxFilterDeletes.IsChecked);
        }



        void Window_Closed(object sender, EventArgs e)
        {
            //if (File.Exists(ManagerTracker.RenderManager.TempRenderedImageFilename))
            //{
            //  try
            //  {
            //    File.Delete(ManagerTracker.RenderManager.TempRenderedImageFilename);
            //  }
            //  catch
            //  {
            //    PhotoLynx.Logging.PlLog.Logger.Info("Could not delete temporary file " + ManagerTracker.RenderManager.TempRenderedImageFilename);
            //  }
            //}
            //ManagerTracker.CacheManager.ClearCachedOriginalBitmapImage();
            //ManagerTracker.CacheManager.ClearCachedRenderedBitmapImage();
        }

        void Window_Closing(object sender, CancelEventArgs e)
        {
            if (_data == null)
                return;

            if (!_data.AllowClose && ManagerTracker.DataManager != null)
            {
                e.Cancel = !ManagerTracker.DataManager.VerifyClose();
            }

            if (_data.AllowClose)
            {
                if (_data.CurrentBitmap != null)
                    _data.CurrentBitmap.Dispose();
                if (_data.CurrentImage != null)
                    _data.CurrentImage.Dispose();
            }
        }


        void Window_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Space:
                    ManagerTracker.ImageDisplayManager.DisableImageDisplayPan();
                    break;
                default:
                    break;
            }

        }

        void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (!Panel.IsVisible) return;

            if (Keyboard.IsKeyDown(Key.LeftCtrl))
            {//Left ctrl shortcuts
                switch (e.Key)
                {
                    case Key.Z:
                        ManagerTracker.DataManager.Undo();
                        break;
                    case Key.Y:
                        ManagerTracker.DataManager.Redo();
                        break;
                    default:
                        break;
                }
            }
            else
            {
                //if (Mouse.DirectlyOver.Equals(imageDisplay))
                //{
                switch (e.Key)
                {
                    case Key.Space:
                        //ReadyImageDisplayPan();
                        break;
                    case Key.Up:
                        OffsetMousePosition(0, -1);
                        break;
                    case Key.Down:
                        OffsetMousePosition(0, 1);
                        break;
                    case Key.Left:
                        OffsetMousePosition(-1, 0);
                        break;
                    case Key.Right:
                        OffsetMousePosition(1, 0);
                        break;
                    default:
                        break;
                }
                //}
            }
        }


        private void OffsetMousePosition(int x, int y)
        {
            System.Windows.Forms.Cursor.Position = new System.Drawing.Point(System.Windows.Forms.Cursor.Position.X + x, System.Windows.Forms.Cursor.Position.Y + y);
        }

        public void SetStatusMessage() { SetStatusMessage(""); }

        public void SetStatusMessage(string msg)
        {
            MainWindow.Title = CPIGreenScreen.Util.CPISettings.Instance.AppDisplayName + " - " + msg;
        }

        public void MouseIdle() { MouseIdle(false); }

        public void MouseIdle(bool force)
        {
            _mouseWaitCount--;
            if (_mouseWaitCount == 0 || force)
            {
                MainWindow.Cursor = null;
            }
            if (_mouseWaitCount < 0)
            {
                _mouseWaitCount = 0;
            }
        }

        public void Invoke(Action action)
        {
            Panel.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, action);
        }

        public void MouseBusy() { MouseBusy(false); }

        public void MouseBusy(bool force)
        {
            if (_mouseWaitCount == 0 || force)
            {
                MainWindow.Cursor = Cursors.Wait;
            }
            _mouseWaitCount++;
        }

    }
}
