using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using Flow.GreenScreen.Data;
using System.Windows;
using Flow.GreenScreen.Panels;
using Flow.GreenScreen.Renderer;

namespace Flow.GreenScreen.Managers.Panels
{
    public class CurvePanelManager : GenericPanelManager<CurvePanel>
    {
        #region Members

        byte[] _curveTable;

        #endregion

        #region Constructors

        public CurvePanelManager(PanelData data)
            : base(data)
        {
            Point[] startValue = _data.CurrentImage == null ? new Point[0] :
                _data.CurrentImage.CurrentSelectionConfiguration.ColorAndBackgroundConfiguration.ControlPoints;
            Panel = new CurvePanel(startValue);
            Panel.ControlPointsChanged = new CurvePanel.ControlPointsChangedDelegate(CurvePanelControlPointChanged);

            PanelTracker.AddPanel(Panel, Panel.GetType());
        }

        #endregion

        #region Properties

        public byte[] CurveTable
        {
            get
            {
                if (_curveTable == null)
                {
                    _curveTable = AlphaCurve.CreateTable(PanelTracker.CurvePanel.GetControlPoints());
                }
                return _curveTable;
            }
            set { _curveTable = value; }
        }

        #endregion


        #region Methods

        public void comboBoxCurveList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                Panel.SetControlPoints((e.AddedItems[0] as ControlPointProfile).Points);
                CurvePanelControlPointChanged((e.AddedItems[0] as ControlPointProfile).Points);
            }
        }

        public void CurvePanelControlPointChanged(Point[] newControlPoints)
        {
            CurvePanelControlPointChanged(newControlPoints, true);
        }

        public void CurvePanelControlPointChanged(Point[] newControlPoints, bool render)
        {
            ManagerTracker.MainDisplayManager.MouseBusy();
            _data.CurrentImage.CurrentSelectionConfiguration.ColorAndBackgroundConfiguration.ControlPoints = newControlPoints;
            CurveTable = AlphaCurve.CreateTable(newControlPoints);
            _data.CurrentRenderedBitmapSourceDropoutValid = false;
            if (render)
            {
                ManagerTracker.RenderManager.RenderGreenScreenCurrentSettings();
            }
            ManagerTracker.MainDisplayManager.MouseIdle();
        }

        public void buttonRemoveCurve_Click(object sender, RoutedEventArgs e)
        {
            if (PanelTracker.CurvePanel.comboBoxCurveList.SelectedIndex >= 0)
            {
                _data.ControlPointProfiles.List.RemoveAt(PanelTracker.CurvePanel.comboBoxCurveList.SelectedIndex);
            }
            PanelTracker.CurvePanel.comboBoxCurveList.Items.Refresh();
            PanelTracker.CurvePanel.comboBoxCurveList.SelectedIndex = -1;
            _data.SaveControlPointProfile();
        }


        public void buttonSaveCurrentCurve_Click(object sender, RoutedEventArgs e)
        {
            if (Panel.comboBoxCurveList.Text.Equals(""))
            {
                MessageBox.Show("Enter a name for the current curve", "Curve unnamed", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            if (ControlPointProfile.ExistsInArrayList(Panel.comboBoxCurveList.Text, _data.ControlPointProfiles.List))
            {
                MessageBox.Show("Curve name exists. Rename curve.", "Curve name exists", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }

            _data.ControlPointProfiles.List.Add(new ControlPointProfile(Panel.comboBoxCurveList.Text, Panel.GetControlPoints()));
            _data.SaveControlPointProfile();
            Panel.comboBoxCurveList.Items.Refresh();
        }

        #endregion
    }
}