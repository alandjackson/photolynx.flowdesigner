﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using Flow.GreenScreen.Panels;
using Flow.GreenScreen.Data;
using System.Windows;

namespace Flow.GreenScreen.Managers.Panels
{
    public class ColorPickerDataPanelManager : GenericPanelManager<ColorPickerDataPanel>
    {
        #region Members

        #endregion

        #region Constructors

        public ColorPickerDataPanelManager(PanelData data)
            : base(data)
        {
            double startValue = _data.CurrentImage == null ? 0 :
                _data.CurrentImage.CurrentSelectionConfiguration.ColorAndBackgroundConfiguration.BackgroundCorrection;
            Panel = new ColorPickerDataPanel(startValue, _data);

            Panel.textBoxTolerance.TextChanged += new TextChangedEventHandler(textBoxTolerance_TextChanged);
            Panel.sliderBackgroundStrength.ValueChanged += new RoutedPropertyChangedEventHandler<double>(sliderBackgroundStrength_ValueChanged);

            if (_data.CurrentImage != null)
            {
                Panel.sliderBackgroundStrength.Value = _data.CurrentImage.CurrentSelectionConfiguration.ColorAndBackgroundConfiguration.BackgroundCorrection;
                Panel.checkBoxSoftenEdges.IsChecked = _data.CurrentImage.CurrentSelectionConfiguration.ColorAndBackgroundConfiguration.SoftenEdges;
            }
            Panel.sliderBackgroundStrength.ValueChanged += new RoutedPropertyChangedEventHandler<double>(sliderBackgroundStrength_ValueChanged);
            Panel.sliderBackgroundStrength.MouseUp += new System.Windows.Input.MouseButtonEventHandler(sliderBackgroundStrength_MouseUp);
            Panel.sliderBackgroundStrength.MouseDown += new System.Windows.Input.MouseButtonEventHandler(sliderBackgroundStrength_MouseDown);
            Panel.checkBoxSoftenEdges.Click += new RoutedEventHandler(checkBoxSoftenEdges_Click);
            PanelTracker.AddPanel(Panel, Panel.GetType());
        }

        #endregion

        #region Methods


        void sliderBackgroundStrength_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Panel.sliderBackgroundStrength.ReleaseMouseCapture();
            ManagerTracker.RenderManager.RenderGreenScreenCurrentSettings();
        }

        void sliderBackgroundStrength_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Panel.sliderBackgroundStrength.CaptureMouse();
        }

        void checkBoxSoftenEdges_Click(object sender, RoutedEventArgs e)
        {
            _data.CurrentImage.CurrentSelectionConfiguration.ColorAndBackgroundConfiguration.SoftenEdges = PanelTracker.ColorPickerDataPanel.checkBoxSoftenEdges.IsChecked.Value;
            _data.CurrentRenderedBitmapSourceDropoutValid = false;
            ManagerTracker.RenderManager.RenderGreenScreenCurrentSettings();
        }

        void sliderBackgroundStrength_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            ToolTip tt = new ToolTip();
            tt.Content = Panel.sliderBackgroundStrength.Value.ToString(".00");
            Panel.sliderBackgroundStrength.ToolTip = tt;
        }

        void textBoxTolerance_TextChanged(object sender, TextChangedEventArgs e)
        {
            int newTolerance;
            try
            {
                newTolerance = Convert.ToInt32(Panel.textBoxTolerance.Text);
            }
            catch
            {
                return;
            }
            _data.CurrentRenderedBitmapSourceDropoutValid = false;
            if (Panel.InDropColorMode)
            {
                _data.CurrentImage.CurrentSelectionConfiguration.ColorAndBackgroundConfiguration.SetDropTolerance(newTolerance);
            }
            else
            {
                _data.CurrentImage.CurrentSelectionConfiguration.ColorAndBackgroundConfiguration.SetKeepTolerance(newTolerance);
            }
        }

        #endregion
    }
}
