﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using Flow.GreenScreen.Data;
using Flow.GreenScreen.Panels;

namespace Flow.GreenScreen.Managers.Panels
{
  public class SelectionDataPanelManager : GenericPanelManager<SelectionDataPanel>
  {
    #region Constructors

    public SelectionDataPanelManager(PanelData data):base(data)
    {
      Panel = new SelectionDataPanel();
      Panel.buttonRemoveSelection.Click += new RoutedEventHandler(buttonRemoveSelection_Click);
      Panel.buttonSelectAll.Click += new RoutedEventHandler(buttonSelectAll_Click);
      Panel.buttonSelectNone.Click += new RoutedEventHandler(buttonSelectNone_Click);
      Panel.buttonApplySelected.Click += new RoutedEventHandler(buttonApplySelected_Click);

      PanelTracker.AddPanel(Panel, Panel.GetType());
    }

    #endregion

    #region Methods

    void buttonRemoveSelection_Click(object sender, RoutedEventArgs e)
    {
      //foreach (Selection selection in PanelTracker.SelectionDataPanel.listBoxSelections.SelectedItems)
      //{
      //  _data.CurrentImage.CurrentSelectionConfiguration.Selections.Remove(selection);
      //}
      ////__data.CurrentImage.SaveSelectionData();

      //PanelTracker.SelectionDataPanel.listBoxSelections.SelectedItems.Clear();
      //PanelTracker.SelectionDataPanel.RefreshList();
      //ManagerTracker.RenderManager.RenderGreenScreenCurrentSettings();

      //ManagerTracker.RenderManager.RefreshLayerGroup();
      throw new NotImplementedException();
    }

    public void RemoveLastSelection()
    {
      //_data.CurrentImage.CurrentSelectionConfiguration.Selections.RemoveAt(_data.CurrentImage.CurrentSelectionConfiguration.Selections.Count - 1);
      //PanelTracker.SelectionDataPanel.RefreshList();

      //ManagerTracker.RenderManager.RenderGreenScreenCurrentSettings();
      //ManagerTracker.RenderManager.RefreshLayerGroup();
      throw new NotImplementedException();
    }
    void buttonApplySelected_Click(object sender, RoutedEventArgs e)
    {
      //List<Selection> selected = new List<Selection>();
      //foreach (Selection selection in PanelTracker.SelectionDataPanel.listBoxSelections.SelectedItems)
      //{
      //  selected.Add(selection);
      //}

      //Selection[] selectionArray = selected.ToArray();
      //selected.Clear();

      //foreach (ImageData image in _data.Images)
      //{
      //  if (image != _data.CurrentImage)
      //  {
      //    image.CurrentSelectionConfiguration.Selections.AddRange(selectionArray);
      //  }
      //  image.SaveConfigurationData(image.CurrentSelectionConfiguration);
      //}
      //_data.SaveConfigurationChanges();
      throw new NotImplementedException();
    }

    void buttonSelectNone_Click(object sender, RoutedEventArgs e)
    {
      PanelTracker.SelectionDataPanel.listBoxSelections.SelectedItems.Clear();
    }

    void buttonSelectAll_Click(object sender, RoutedEventArgs e)
    {
      PanelTracker.SelectionDataPanel.listBoxSelections.SelectedItems.Clear();
      foreach (object item in PanelTracker.SelectionDataPanel.listBoxSelections.Items)
      {
        PanelTracker.SelectionDataPanel.listBoxSelections.SelectedItems.Add(item);
      }
    }

    #endregion
  }
}
