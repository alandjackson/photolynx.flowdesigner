using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Windows;
using Flow.GreenScreen.Data;
using System.Windows.Media;
using System.Windows.Controls;
using Flow.GreenScreen.Panels;
using System.Windows.Shapes;

namespace Flow.GreenScreen.Managers.Panels
{
  public class ImageDisplayManager : GenericManager
  {
    #region Members
    public bool MousePressed;
    public double ZoomFactor;

    Image _imageDisplay;
    Image _imageBackground;
    Canvas _canvasImageDisplayClip;
    Canvas _canvasImageDisplay;
    
    public bool IsCapturingMouseEvents { get; set; }

    #endregion

    #region Constructors

    public ImageDisplayManager(PanelData data, Image imageBackground, Image imageDisplay, Canvas canvasImageDisplay, Canvas canvasImageDisplayClip)
      : base(data)
    {
      _imageBackground = imageBackground;
      _imageDisplay = imageDisplay;
      _canvasImageDisplay = canvasImageDisplay;
      _canvasImageDisplayClip = canvasImageDisplayClip;
      ZoomFactor = 1.0;

      _imageBackground.MouseDown += new MouseButtonEventHandler(imageDisplay_MouseDown);
      _imageBackground.MouseUp += new MouseButtonEventHandler(imageDisplay_MouseUp);
      _imageBackground.MouseMove += new MouseEventHandler(imageDisplay_MouseMove);
      _imageBackground.MouseEnter += new MouseEventHandler(imageDisplay_MouseEnter);
      _imageBackground.MouseLeave += new MouseEventHandler(imageDisplay_MouseLeave);
      _imageBackground.SizeChanged += new SizeChangedEventHandler(imageDisplay_SizeChanged);
      
      IsCapturingMouseEvents = true;
    }

    #endregion

    #region Methods

    void imageDisplay_MouseUp(object sender, MouseButtonEventArgs e)
    {
      if (! IsCapturingMouseEvents) return;

      if (!ManagerTracker.ImageDisplayManager.MousePressed)
      {
        return;
      }
      switch (_data.CurrentTool)
      {
        case ToolboxButtons.BackgroundDefinitionTool:
          ManagerTracker.BackgroundDefinitionToolManager.BackgroundDefinitionToolMouseUp((Image)sender);
          break;
        case ToolboxButtons.HandTool:
          ManagerTracker.HandToolManager.HandToolMouseUp((Image)sender);
          break;
        case ToolboxButtons.BrushTool:
          ManagerTracker.BrushToolManager.BrushToolMouseUpHandler((Image)sender);
          break;
        case ToolboxButtons.ColorPickerTool:
          ManagerTracker.ColorPickerToolManager.ColorPickerToolMouseUpHandler((Image)sender);
          break;
        case ToolboxButtons.PolygonSelectionTool:
          ManagerTracker.PolygonSelectionToolManager.MouseUpHandler();
          break;
        default:
          break;
      }
      ManagerTracker.UndoRedoManager.PushHistory(new PathGeometry[] { _data.CurrentImage.CurrentSelectionConfiguration.ForegroundClip, _data.CurrentImage.CurrentSelectionConfiguration.RenderedClipNegative });

    }

    void imageDisplay_MouseDown(object sender, MouseButtonEventArgs e)
    {
      if (!IsCapturingMouseEvents) return;
      Point p = ManagerTracker.DataManager.FormatPoint(e.GetPosition(_canvasImageDisplay));

      switch (_data.CurrentTool)
      {
        case ToolboxButtons.HandTool:
          ManagerTracker.HandToolManager.HandToolMouseDown(
              System.Windows.Forms.Cursor.Position, 
              (Image)sender, e);
          break;
        case ToolboxButtons.BrushTool:
          ManagerTracker.BrushToolManager.BrushToolMouseDownHandler(p, (Image)sender);
          break;
        case ToolboxButtons.BackgroundDefinitionTool:
        case ToolboxButtons.ColorPickerTool:
          ManagerTracker.ColorPickerToolManager.ColorPickerToolMouseDownHandler(p, (Image)sender);
          break;
        case ToolboxButtons.PolygonSelectionTool:
          ManagerTracker.PolygonSelectionToolManager.MouseDownHandler(p);
          break;
        case ToolboxButtons.ZoomTool:
          ManagerTracker.ZoomToolManager.AdjustImageDisplayZoom(e, p);
          break;
        default:
          break;
      }
    }

    void imageDisplay_SizeChanged(object sender, SizeChangedEventArgs e)
    {
      RectangleGeometry clip = new RectangleGeometry(new Rect(new Size(_imageDisplay.ActualWidth, _imageDisplay.ActualHeight)));
      //clip.Freeze();
      _canvasImageDisplayClip.Clip = clip;
      ManagerTracker.RenderManager.ImageDisplaySizeChanged(_imageDisplay.ActualWidth, _imageDisplay.ActualHeight);
    }

    private void ReadyImageDisplayPan()
    {
      _imageBackground.Cursor = Cursors.Hand;
    }

    public void DisableImageDisplayPan()
    {
      _imageBackground.Cursor.Dispose();
      _imageBackground.Cursor = null;
    }


    void imageDisplay_MouseLeave(object sender, MouseEventArgs e)
    {
      //_zoomPanel.canvasZoomPanel.Visibility = Visibility.Hidden;

      if (_canvasImageDisplay.Children.Contains(PanelTracker.SelectionDataPanel.CursorShape))
      {
        _canvasImageDisplay.Children.Remove(PanelTracker.SelectionDataPanel.CursorShape);
      }
    }

    void imageDisplay_MouseEnter(object sender, MouseEventArgs e)
    {
      //RenderZoomPanelBitmap();
      //_zoomPanel.canvasZoomPanel.Visibility = Visibility.Visible;

      Shape cursor = PanelTracker.SelectionDataPanel.CursorShape;
      if (!_canvasImageDisplay.Children.Contains(cursor) && cursor != null)
      {
        _canvasImageDisplay.Children.Add(cursor);
      }
    }


    void imageDisplay_MouseMove(object sender, MouseEventArgs e)
    {
      if (!IsCapturingMouseEvents) return;
      try
      {
        Point lastLocation = _data.LastMousePosition;
        _data.LastMousePosition = 
          ManagerTracker.DataManager.FormatPoint(e.GetPosition(_canvasImageDisplay));
        switch (_data.CurrentTool)
        {
          case ToolboxButtons.HandTool:
            ManagerTracker.HandToolManager.HandToolMouseMove(
              _data.LastMousePosition, lastLocation);
            break;
          case ToolboxButtons.BrushTool:
            ManagerTracker.BrushToolManager.BrushToolMouseMoveHandler(
              _data.LastMousePosition, lastLocation);
            break;
          case ToolboxButtons.BackgroundDefinitionTool:
          case ToolboxButtons.ColorPickerTool:
            ManagerTracker.ColorPickerToolManager.ColorPickerToolMouseMoveHandler(
              _data.LastMousePosition);
            break;
          case ToolboxButtons.PolygonSelectionTool:
            ManagerTracker.PolygonSelectionToolManager.MouseMoveHandler(
              _data.LastMousePosition);
            break;
          case ToolboxButtons.ZoomTool:
            ManagerTracker.ZoomToolManager.ZoomToolMouseMoveHandler(
              _data.LastMousePosition);
            break;
          case ToolboxButtons.BezierTool:
            ManagerTracker.BezierToolManager.BezierToolMouseMoveHandler(
              _data.LastMousePosition);
            break;
          default:
            break;
        }
      }
      catch (Exception err)
      {
        MessageBox.Show("imageDisplay_MouseMove error: " + err.ToString());
      }
    }

    #endregion
  }
}