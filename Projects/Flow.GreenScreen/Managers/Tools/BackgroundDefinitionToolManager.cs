using Flow.GreenScreen.Renderer;
using Flow.GreenScreen.Data;
using System.Windows.Controls;
namespace Flow.GreenScreen.Managers.Tools
{
  public class BackgroundDefinitionToolManager : GenericManager
  {
    #region Constructors

    public BackgroundDefinitionToolManager(PanelData data):base(data)
    {

    }

    #endregion

    #region Methods

    public void BackgroundDefinitionToolMouseUp(Image sender)
    {
      int green = 0;
      int red = 0;
      int blue = 0;

      foreach (GreenScreenBGRA color in _data.ColorSelections)
      {
        red += color.Red;
        green += color.Green;
        blue += color.Blue;
      }

      _data.CurrentImage.CurrentSelectionConfiguration.ColorAndBackgroundConfiguration.BackgroundDefinitionGreen = (byte)(green / (double)_data.ColorSelections.Count);
      _data.CurrentImage.CurrentSelectionConfiguration.ColorAndBackgroundConfiguration.BackgroundDefinitionRed = (byte)(red / (double)_data.ColorSelections.Count);
      _data.CurrentImage.CurrentSelectionConfiguration.ColorAndBackgroundConfiguration.BackgroundDefinitionBlue = (byte)(blue / (double)_data.ColorSelections.Count);

      ManagerTracker.ColorPickerToolManager.ColorPickerToolEnd(sender);
    }

    #endregion
  }
}