using System.Windows;
using System.Windows.Media;
using System.Collections.Generic;
using System.Windows.Controls;
using Flow.GreenScreen.Data;
using System.Windows.Input;
using System;
using Flow.GreenScreen.Managers.Panels;
namespace Flow.GreenScreen.Managers.Tools
{

  public static class ImageExtensions
  {
    public static void SetNewNonzeroGeometry(this Image i, Geometry g)
    {
      i.Clip = new PathGeometry();
      (i.Clip as PathGeometry).FillRule = FillRule.Nonzero;
      (i.Clip as PathGeometry).AddGeometry(g);
    }
    public static void AppendTransform(this Geometry g, Transform t)
    {
      if (g.Transform.Equals(Transform.Identity))
        g.Transform = t;
      else if (g.Transform is TransformGroup)
        (g.Transform as TransformGroup).Children.Add(t);
      else
      {
        TransformGroup grp = new TransformGroup();
        grp.Children.Add(g.Transform);
        grp.Children.Add(t);
        g.Transform = grp;
      }
    }
  }
  public class BrushToolManager : GenericManager
  {
    #region Members

    Image _imageDisplay;
    Image _imageWorking;
    Image _imageBackground;
    Image _imageForeground;
    Canvas _canvasImageDisplay;

    #endregion

    #region Constructors

    public BrushToolManager(Canvas canvasImageDisplay, Image imageForeground, Image imageBackground, Image imageDisplay, Image imageWorking, PanelData data):base(data)
    {
      _imageForeground = imageForeground;
      _imageBackground = imageBackground;
      _imageWorking = imageWorking;
      _canvasImageDisplay = canvasImageDisplay;
      _imageDisplay = imageDisplay;
    }

    #endregion

    #region Members

    public void BrushToolSelected()
    {
      ManagerTracker.PolygonSelectionToolManager.PolygonSelectionToolSelected(false);

      _imageBackground.Cursor = Cursors.Cross;
      PanelTracker.SelectionDataPanel.CursorShape = PanelTracker.SelectionDataPanel.CreateEllipseBrush(ManagerTracker.DataManager.GetImageDisplayGeometry());

    }

    public void BrushToolDeselected()
    {
      _canvasImageDisplay.Children.Remove(PanelTracker.SelectionDataPanel.CursorEllipse);
    }

    public void BrushToolMouseMoveHandler(Point location, Point lastLocation)
    {
      double radius = PanelTracker.SelectionDataPanel.CursorEllipse.Width / 2;
      ManagerTracker.RenderManager.SetCanvasPosition(PanelTracker.SelectionDataPanel.CursorShape, location.X - radius, location.Y - radius);

      if (! ManagerTracker.ImageDisplayManager.MousePressed)
        return;

      Point direction = new Point(location.X - lastLocation.X, location.Y - lastLocation.Y); //direction vector

      EllipseGeometry eBegin = new EllipseGeometry(new Point(lastLocation.X, lastLocation.Y), radius, radius);
      //eBegin.Freeze();
      EllipseGeometry eEnd = new EllipseGeometry(new Point(location.X, location.Y), radius, radius);
      //eEnd.Freeze();

      direction = new Point(direction.Y, -direction.X);//perpendicular
      double magnitude = Math.Sqrt(direction.X * direction.X + direction.Y * direction.Y);
      direction.X = (direction.X / magnitude) * (eBegin.RadiusX);
      direction.Y = (direction.Y / magnitude) * (eBegin.RadiusY);

      Point[] polygonPointArray = OrderForRectangle(
        ManagerTracker.DataManager.FormatPoint(new Point(lastLocation.X - direction.X, lastLocation.Y - direction.Y)),
        ManagerTracker.DataManager.FormatPoint(new Point(lastLocation.X + direction.X, lastLocation.Y + direction.Y)),
        ManagerTracker.DataManager.FormatPoint(new Point(location.X + direction.X, location.Y + direction.Y)),
        ManagerTracker.DataManager.FormatPoint(new Point(location.X - direction.X, location.Y - direction.Y)));

      List<Point> points = new List<Point>();
      points.AddRange(polygonPointArray);

      StreamGeometry polygon = new StreamGeometry();
      using (StreamGeometryContext ctx = polygon.Open())
      {
        ctx.BeginFigure(points[0], true, true);
        ctx.PolyLineTo(points, true, true);
      }
      CombinedGeometry combined = new CombinedGeometry(GeometryCombineMode.Union, eBegin, polygon);
      combined = new CombinedGeometry(GeometryCombineMode.Union, combined, eEnd);
      //combined.Freeze();

      (_imageWorking.Clip as PathGeometry).AddGeometry(combined);
    }

    protected Point CanvasToForeground(Point src)
    {
      return PanelTracker.CanvasToForeground(src, _imageDisplay, _data.Variant);
    }

    private Point[] OrderForRectangle(Point a, Point b, Point c, Point d)
    {
      Point dirA = new Point(a.X - b.X, a.Y - b.Y);
      Point dirB = new Point(c.X - d.X, c.Y - d.Y);

      if (dirA.X * dirB.X + dirA.Y * dirB.Y > 0)
        return new Point[] { a, b, d, c };
      else
        return new Point[] { a, b, c, d };
    }


    public void BrushToolMouseDownHandler(Point location, Image sender)
    {
      ManagerTracker.ImageDisplayManager.MousePressed = true;
      sender.CaptureMouse();

      double radius = PanelTracker.SelectionDataPanel.CursorEllipse.Width / 2;

      EllipseGeometry eEnd = new EllipseGeometry(new Point(location.X, location.Y), radius, radius);

      bool isBackground = PanelTracker.SelectionDataPanel.IsBackgroundMode;
      _imageWorking.Source = (isBackground ? _imageBackground : _imageForeground).Source;
      _imageWorking.SetNewNonzeroGeometry(eEnd);

      _imageWorking.Clip.Transform = Transform.Identity;
      _imageWorking.Clip.AppendTransform(BuildTranslateTransform());
    }

    protected TranslateTransform BuildTranslateTransform()
    {
      Point pt = PanelTracker.CanvasToForeground(new Point(), _imageDisplay, _data.Variant);
      return new TranslateTransform(pt.X, pt.Y);
    }

    protected ScaleTransform BuildScaleTransform()
    {
      double zoom = 1 / ManagerTracker.ImageDisplayManager.ZoomFactor;
      return new ScaleTransform(zoom, zoom);
    }

    public void BrushToolMouseUpHandler(Image sender)
    {
      ManagerTracker.ImageDisplayManager.MousePressed = false;
      sender.ReleaseMouseCapture();

      _imageWorking.Clip.Transform = Transform.Identity;
      _imageWorking.Clip.AppendTransform(BuildTranslateTransform());

      ManagerTracker.RenderManager.AddSelectionGeometry(_imageWorking.Clip, 
        PanelTracker.SelectionDataPanel.IsBackgroundMode);

      _imageWorking.Clip = null;
      _imageWorking.Source = null;

    }
    

    #endregion
  }
}