using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using PhotoLynx.CommonWpfControls.Util;
using Flow.GreenScreen.Data;
using Flow.GreenScreen.Managers.Panels;
using Flow.GreenScreen.Panels;
using Flow.GreenScreen.Util;
using System.Collections.Generic;

namespace Flow.GreenScreen.Managers.Tools
{
  public class ZoomToolManager : GenericManager
  {
    Image _imageRendered;
    Image _imageBackground;
    Image _imageForeground;
    Image _imageWorking;
    List<Image> _zoomPanels;
    Canvas _canvasImageDisplay;
    public ZoomToolManager(Image imageForeground,
                           Image imageBackground,
                           Image imageDisplay,
                           Image imageWorking,
                           Canvas canvasImageDisplay,
                           PanelData data)
      : base(data)
    {
      _imageWorking = imageWorking;
      _imageForeground = imageForeground;
      _imageBackground = imageBackground;
      _imageRendered = imageDisplay;
      // TODO: an exception here gives a generic application error, 
      //       make sure it was logged correctly
      _zoomPanels = new List<Image>();
      _zoomPanels.Add(_imageWorking);
      _zoomPanels.Add(_imageForeground);
      _zoomPanels.Add(_imageRendered);
      _canvasImageDisplay = canvasImageDisplay;
    }


    public void ZoomToolSelected()
    {
      ImageSource src = ManagerTracker.ImageMatchToolbarManager.Panel.zoomToolIcon.Source;
      _imageBackground.Cursor = Cursors.None;
      PanelTracker.SelectionDataPanel.CursorShape =
        PanelTracker.SelectionDataPanel.CreateRectangleBrush(src,
        ManagerTracker.DataManager.GetImageDisplayGeometry());
    }

    public void ZoomToolMouseMoveHandler(Point location)
    {
      ManagerTracker.RenderManager.SetCanvasPosition
      (
        PanelTracker.SelectionDataPanel.CursorShape,
        location.X,
        location.Y - PanelTracker.SelectionDataPanel.CursorRectangle.Height
      );

    }

    public void ResetZoom()
    {
      ManagerTracker.ImageDisplayManager.ZoomFactor = 1;
      double zoomFactor = ManagerTracker.ImageDisplayManager.ZoomFactor;

      if (false && _data.Variant == VariantType.CPI)
      {
        _imageRendered.LayoutTransform = new ScaleTransform(zoomFactor, zoomFactor);
        _imageForeground.LayoutTransform = new ScaleTransform(zoomFactor, zoomFactor);
        //ManagerTracker.RenderManager.SetCanvasPosition(_imageRendered, 0, 0);
        //ManagerTracker.RenderManager.SetCanvasPosition(_imageForeground, 0, 0);
        foreach (Image i in _zoomPanels)
          ManagerTracker.RenderManager.SetCanvasPosition(i, 0, 0);
      }
      else
      {
        _canvasImageDisplay.LayoutTransform = new ScaleTransform(zoomFactor, zoomFactor);
        ManagerTracker.RenderManager.SetCanvasPosition(_canvasImageDisplay, 0, 0);
      }
    }

    private void UpdateCanvasPosition(Point location)
    {
      double zoomFactor = ManagerTracker.ImageDisplayManager.ZoomFactor;
      ManagerTracker.RenderManager.SetCanvasPosition(
        _canvasImageDisplay,
        -location.X * zoomFactor + _imageRendered.ActualWidth / 2,
        -location.Y * zoomFactor + _imageRendered.ActualHeight / 2);
    }

    /// <summary>
    /// Centers the display on the desired point
    /// Used when zomming in or out to center the display
    /// where the user clicked
    /// </summary>
    /// <param name="imgLoc"></param>
    public void CenterImageDisplay(Point imgLoc)
    {
      double zoomFactor = ManagerTracker.ImageDisplayManager.ZoomFactor;
      Canvas parent = _imageRendered.Parent as Canvas;

      Point center = new Point(_imageRendered.Width / 2, _imageRendered.Height / 2);
      Canvas.SetLeft(_imageRendered, center.X - imgLoc.X);
      Canvas.SetTop(_imageRendered, center.Y - imgLoc.Y);

    }

    /// <summary>
    /// Updates the zoom factor on a mouse click
    /// </summary>
    /// <param name="e"></param>
    /// <param name="location"></param>
    protected void SetNewZoomFactor(MouseButtonEventArgs e, Point location)
    {
      switch (e.ChangedButton)
      {
        case MouseButton.Left:
          ManagerTracker.ImageDisplayManager.ZoomFactor *= 1.2;
          break;
        case MouseButton.Right:
          if (ManagerTracker.ImageDisplayManager.ZoomFactor != 1.0)
          {
            ManagerTracker.ImageDisplayManager.ZoomFactor /= 1.2;
            if (ManagerTracker.ImageDisplayManager.ZoomFactor < 1.0)
            {
              ManagerTracker.ImageDisplayManager.ZoomFactor = 1.0;
            }
          }
          break;
        default:
          break;
      }
    }

    public void AdjustImageDisplayZoom(MouseButtonEventArgs e, Point location)
    {
      // save the original zoom level
      double origZoom = ManagerTracker.ImageDisplayManager.ZoomFactor;

      SetNewZoomFactor(e, location);

      // get the current zoom level
      double curZoom = ManagerTracker.ImageDisplayManager.ZoomFactor;


      // In the CPI app, scale the foreground as a crop
      if (false && _data.Variant == VariantType.CPI)
      {
        if (curZoom == 1.0)
        {
          foreach (Image i in _zoomPanels)
            WpfUtil.SetOnCanvas(i, new Point(0, 0));
          
          //WpfUtil.SetOnCanvas(_imageRendered, new Point(0, 0));
          //WpfUtil.SetOnCanvas(_imageForeground, new Point(0, 0));
        }
        else 
          CenterForegroundOnPoint(origZoom, curZoom, location);
         
        foreach (Image i in _zoomPanels)
          i.LayoutTransform = new ScaleTransform(curZoom, curZoom);
        //_imageRendered.LayoutTransform = new ScaleTransform(curZoom, curZoom);
        //_imageForeground.LayoutTransform = new ScaleTransform(curZoom, curZoom);
        //this._image .LayoutTransform = new ScaleTransform(curZoom, curZoom);
      }
      // In the normal app, scale everything as a zoon
      else
      {
        if (curZoom == 1.0)
          ManagerTracker.RenderManager.SetCanvasPosition(_canvasImageDisplay, 0, 0);
        else 
          UpdateCanvasPosition(location);
        _canvasImageDisplay.LayoutTransform = new ScaleTransform(curZoom, curZoom);
      }

      // Check if the zoom level requires a higher quality image
      ImageFileVirtualizationManager vMgr = ManagerTracker.ImageFileVirtualizationManager;
      if (vMgr.IsCurrentSizeInvalid(new Size(GreenScreenPanel.RenderWidth * curZoom,
                                             GreenScreenPanel.RenderHeight * curZoom))
                                    && ManagerTracker.SettingsPanelManager.RenderEnabled)
      {
        ManagerTracker.RenderManager.InvalidateGreenScreenRender();
      }


      // since we are cropping, set the foreground location/scale
      if (false && _data.Variant == VariantType.CPI)
      {

        SelectionConfiguration config = _data.CurrentImage.CurrentSelectionConfiguration;
        config.ForegroundTopLeft = new Point(Canvas.GetLeft(_imageRendered),
                                             Canvas.GetTop(_imageRendered));
        config.ForegroundScale = ManagerTracker.ImageDisplayManager.ZoomFactor;
      }
    }

    /// <summary>
    /// Make sure the point they clicked on is still in the same
    /// location after the zoom
    /// </summary>
    /// <param name="origZoom"></param>
    /// <param name="curZoom"></param>
    /// <param name="location"></param>
    protected void CenterForegroundOnPoint(double origZoom, double curZoom,
                                           Point location)
    {
      // get the amount grown
      double xDelta = GreenScreenPanel.RenderWidth * curZoom -
                        GreenScreenPanel.RenderWidth * origZoom;
      double yDelta = GreenScreenPanel.RenderHeight * curZoom -
                        GreenScreenPanel.RenderHeight * origZoom;

      // calculate how much to offset the display to keep the point on the mouse
      double moveLeft = xDelta * location.X / GreenScreenPanel.RenderWidth * -1;
      double moveUp = yDelta * location.Y / GreenScreenPanel.RenderHeight * -1;

      // Move the display
      foreach (Image i in _zoomPanels)
        WpfUtil.MoveOnCanvas(i, new Point(moveLeft, moveUp));

      //WpfUtil.MoveOnCanvas(_imageRendered, new Point(moveLeft, moveUp));
      //WpfUtil.MoveOnCanvas(_imageForeground, new Point(moveLeft, moveUp));
    }
  }
}