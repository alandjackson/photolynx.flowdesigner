﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.GreenScreen.Panels;
using System.Windows;
using Flow.GreenScreen.Data;
using System.Drawing;

namespace Flow.GreenScreen.Managers.Tools
{
  public class CPIRotatePanelManager : GenericManager
  {
    CPIRotatePanel RotatePanel { get; set; }
    public SelectionConfiguration SelConfig
    { 
      get { return _data.CurrentImage.CurrentSelectionConfiguration; } 
    }
  
    public CPIRotatePanelManager(CPIRotatePanel pnl, PanelData data)
      : base(data)
    {
      RotatePanel = pnl;
      RotatePanel.uxRotateCcw.Click += new RoutedEventHandler(uxRotateCcw_Click);
      RotatePanel.uxRotateCw.Click += new RoutedEventHandler(uxRotateCw_Click);
    }

    void uxRotateCw_Click(object sender, System.Windows.RoutedEventArgs e)
    {
      SelConfig.RotateFlipType = RotateCw(SelConfig.RotateFlipType);
      ManagerTracker.RenderManager.InvalidateGreenScreenRender();
    }

    void uxRotateCcw_Click(object sender, System.Windows.RoutedEventArgs e)
    {
      SelConfig.RotateFlipType = RotateCcw(SelConfig.RotateFlipType);
      ManagerTracker.RenderManager.InvalidateGreenScreenRender();
    }

    static List<RotateFlipType> RotateOrder = new List<RotateFlipType>( 
      new RotateFlipType [] {
        RotateFlipType.RotateNoneFlipNone,
        RotateFlipType.Rotate90FlipNone,
        RotateFlipType.Rotate180FlipNone,
        RotateFlipType.Rotate270FlipNone});
    
    RotateFlipType RotateCw(RotateFlipType r)
    {
      return RotateOrder[(RotateOrder.IndexOf(r) + 1) % RotateOrder.Count];
    }

    RotateFlipType RotateCcw(RotateFlipType r)
    {
      return RotateOrder[
        (RotateOrder.IndexOf(r) - 1 + RotateOrder.Count) % RotateOrder.Count];
    }    
  }
}
