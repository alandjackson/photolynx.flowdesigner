﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.GreenScreen.Data;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Input;
using System.Windows.Shapes;
using Flow.GreenScreen.Managers.Panels;

namespace Flow.GreenScreen.Managers.Tools
{
  public class PolygonSelectionToolManager : GenericManager
  {
    public static readonly int CLOSE_LASSO_TOLERANCE = 6;

    Image _imageDisplay;
    Image _imageBackground;
    Canvas _canvasData;
    Canvas _canvasImageDisplay;
    bool _lastPointIsDisplay = false;
    public bool IsFreehand { get; set; }


    public PolygonSelectionToolManager(Canvas canvasImageDisplay, Canvas canvasData, 
      Image imageBackground, Image imageDisplay, PanelData data)
      : base(data)
    {
      _imageBackground = imageBackground;
      _canvasImageDisplay = canvasImageDisplay;
      _canvasData = canvasData;
      _imageDisplay = imageDisplay;
    }

    public void PolygonSelectionToolSelected(bool createCursor)
    {
      _canvasData.Children.Add(PanelTracker.SelectionDataPanel);
      PanelTracker.SelectionDataPanel.SelectionConfiguration = 
        _data.CurrentImage.CurrentSelectionConfiguration;

      if (createCursor)
      {
        _imageBackground.Cursor = Cursors.Pen;
        PanelTracker.SelectionDataPanel.CursorShape = null;
      }
    }

    public void PolygonSelectionToolDeSelected()
    {
      if (!IsFreehand &&
          _data.CurrentImage.CurrentSelectionConfiguration.CurrentLine != null &&
          _data.CurrentImage.CurrentSelectionConfiguration.CurrentLine.Points.Count > 0)
      {
        EndSelection();
      }
    }

    public void UpdatePolygonSelectionTool()
    {
      PanelTracker.SelectionDataPanel.SelectionConfiguration = _data.CurrentImage.CurrentSelectionConfiguration;
      PanelTracker.SelectionDataPanel.RefreshList();
    }


    public void MouseUpHandler()
    {
      if (IsFreehand) EndSelection();
    }

    public void MouseMoveHandler(Point location)
    {
      Point canvasPt = location;
      Point fgPt = CanvasToForeground(location);
      //location = CanvasToForeground(location);
    
      if (!ManagerTracker.ImageDisplayManager.MousePressed) return;
      if (IsFreehand)
      {
        AddPointToSelection(location);
      }
      else
      {
        if (_lastPointIsDisplay)
        {
          ReplaceLastPointInSelection(location);
        }
        else
        {
          AddPointToSelection(location);
          _lastPointIsDisplay = true;
        }
      }
    }

    public void MouseDownHandler(Point location)
    {
      //location = CanvasToForeground(location);

      if (IsFreehand)
      {
        StartSelection(location);
      }

      // Polygonal tool
      else
      {
        Polyline curLine = _data.CurrentImage.CurrentSelectionConfiguration.CurrentLine;
        
        // start new polygon
        if (!ManagerTracker.ImageDisplayManager.MousePressed ||
            curLine == null ||
            curLine.Points.Count == 0)
        {
          StartSelection(location);
        }
        else
        {
          if (_lastPointIsDisplay &&
              curLine.Points.Count > 1)
          {
            RemoveLastPointInSelection();
            _lastPointIsDisplay = false;
          }

          // check to see if we are updating or closing the tools
          Point startPoint = curLine.Points[0];
          if (Math.Abs(location.X - startPoint.X) < CLOSE_LASSO_TOLERANCE / 2 &&
              Math.Abs(location.Y - startPoint.Y) < CLOSE_LASSO_TOLERANCE / 2)
          {
            EndSelection();
          }
          else
          {
            AddPointToSelection(location);
          }
        }
      }
    }

    protected void RemoveLastPointInSelection()
    {
      PointCollection points = _data.CurrentImage.CurrentSelectionConfiguration.CurrentLine.Points;
      points.RemoveAt(points.Count - 1);
    }

    protected void ReplaceLastPointInSelection(Point location)
    {
      Point displayLocation = DisplayPointToCanvasPoint(location);

      PointCollection points = _data.CurrentImage.CurrentSelectionConfiguration.CurrentLine.Points;
      points[points.Count - 1] = displayLocation;
    }

    protected void AddPointToSelection(Point location)
    {
      Point displayLocation = DisplayPointToCanvasPoint(location);
      _data.CurrentImage.CurrentSelectionConfiguration.CurrentLine.Points.Add(location);
    }

    public void StartSelection(Point location)
    {
      _imageBackground.CaptureMouse();
      ManagerTracker.ImageDisplayManager.MousePressed = true;

      _data.CurrentImage.CurrentSelectionConfiguration.CurrentLine = new Polyline();
      _data.CurrentImage.CurrentSelectionConfiguration.CurrentLine.Clip = ManagerTracker.DataManager.GetImageDisplayGeometry();
      _data.CurrentImage.CurrentSelectionConfiguration.CurrentLine.Stroke = Brushes.Black;
      _data.CurrentImage.CurrentSelectionConfiguration.CurrentLine.StrokeThickness = 1;
      DoubleCollection dashArray = new DoubleCollection();
      dashArray.Add(3);
      dashArray.Add(3);
      _data.CurrentImage.CurrentSelectionConfiguration.CurrentLine.StrokeDashArray = dashArray;
      _data.CurrentImage.CurrentSelectionConfiguration.CurrentLine.Points.Add(location);

      _data.CurrentImage.CurrentSelectionConfiguration.LineFillPolygon = new Polygon();
      _data.CurrentImage.CurrentSelectionConfiguration.LineFillPolygon.Clip = ManagerTracker.DataManager.GetImageDisplayGeometry();
      _data.CurrentImage.CurrentSelectionConfiguration.LineFillPolygon.StrokeThickness = 0;

      if (PanelTracker.SelectionDataPanel.IsBackgroundMode)
      {
        _data.CurrentImage.CurrentSelectionConfiguration.LineFillPolygon.Fill = Brushes.Red;
      }
      else
      {
        _data.CurrentImage.CurrentSelectionConfiguration.LineFillPolygon.Fill = Brushes.Blue;
      }

      _data.CurrentImage.CurrentSelectionConfiguration.LineFillPolygon.Opacity = .5;
      _data.CurrentImage.CurrentSelectionConfiguration.LineFillPolygon.Points = _data.CurrentImage.CurrentSelectionConfiguration.CurrentLine.Points;

      _canvasImageDisplay.Children.Add(_data.CurrentImage.CurrentSelectionConfiguration.CurrentLine);
      _canvasImageDisplay.Children.Add(_data.CurrentImage.CurrentSelectionConfiguration.LineFillPolygon);

      _firstPointRectangle = new Rectangle();
      _firstPointRectangle.Stroke = Brushes.Black;
      _firstPointRectangle.StrokeThickness = 1;
      _firstPointRectangle.Height = CLOSE_LASSO_TOLERANCE;
      _firstPointRectangle.Width = CLOSE_LASSO_TOLERANCE;
      _firstPointRectangle.Margin = new Thickness(location.X - (CLOSE_LASSO_TOLERANCE / 2),
                                                  location.Y - (CLOSE_LASSO_TOLERANCE / 2),
                                                  0, 0);

      _canvasImageDisplay.Children.Add(_firstPointRectangle);


    }
    Rectangle _firstPointRectangle = null;

    public void EndSelection()
    {
      _lastPointIsDisplay = false;
      
      ManagerTracker.MainDisplayManager.MouseBusy();
      _imageBackground.ReleaseMouseCapture();
      _canvasImageDisplay.Children.Remove(_data.CurrentImage.CurrentSelectionConfiguration.CurrentLine);
      _canvasImageDisplay.Children.Remove(_data.CurrentImage.CurrentSelectionConfiguration.LineFillPolygon);
      if (_firstPointRectangle != null)
      {
        _canvasImageDisplay.Children.Remove(_firstPointRectangle);
        _firstPointRectangle = null;
      }

      ManagerTracker.ImageDisplayManager.MousePressed = false;

      if (_data.CurrentImage.CurrentSelectionConfiguration.CurrentLine.Points.Count > 2)
      {
        StreamGeometry polygon = new StreamGeometry();
        using (StreamGeometryContext ctx = polygon.Open())
        {
          Polyline curLine = _data.CurrentImage.CurrentSelectionConfiguration.CurrentLine;
          ctx.BeginFigure(CanvasToForeground(curLine.Points[0]), true, true);
          for (int i = 1; i < curLine.Points.Count; i++)
          {
            ctx.LineTo(CanvasToForeground(curLine.Points[i]), true, true);
          }

        }

        polygon.FillRule = FillRule.Nonzero;

        ManagerTracker.RenderManager.AddSelectionGeometry(polygon, 
          PanelTracker.SelectionDataPanel.IsBackgroundMode);
        _data.CurrentImage.CurrentSelectionConfiguration.CurrentLine = new Polyline();
      }
      ManagerTracker.MainDisplayManager.MouseIdle();
    }
    
    protected Point CanvasToForeground(Point src)
    {
      return PanelTracker.CanvasToForeground(src, _imageDisplay, _data.Variant);
    }

    protected Point DisplayPointToCanvasPoint(Point location)
    {
      GeneralTransform anscestor = _imageDisplay.TransformToAncestor(_canvasImageDisplay);
      Point displayLocation = anscestor.Transform(new Point(0, 0));
      location.X += displayLocation.X;
      location.Y += displayLocation.Y;
      return location;
    }


  }
}
