using System.Windows;
using Flow.GreenScreen.Data;
using System.Windows.Controls;
using System.Windows.Media;
using Flow.GreenScreen.Renderer;
using System;
using System.Windows.Input;
using System.Windows.Shapes;
using System.Collections.Generic;
using System.Diagnostics;
namespace Flow.GreenScreen.Managers.Tools
{
    public class ColorPickerToolManager : GenericManager
    {
        #region Members

        Canvas _canvasData;
        Canvas _canvasImageDisplay;
        Image _imageDisplay;
        Image _imageBackground;

        #endregion

        #region Constructors

        public ColorPickerToolManager(Canvas canvasImageDisplay, Canvas canvasData, Image imageBackground, Image imageDisplay, PanelData data)
            : base(data)
        {
            _canvasImageDisplay = canvasImageDisplay;
            _canvasData = canvasData;
            _imageBackground = imageBackground;
            _imageDisplay = imageDisplay;
        }

        #endregion

        public void ColorPickerTool_Loaded(object sender, RoutedEventArgs e)
        {
            ManagerTracker.ImageMatchToolbarManager.ToolButtonHit(ManagerTracker.ImageMatchToolbarManager.Panel.BackgroundDefinitionTool, null);
        }

        public void ColorPickerToolSelected()
        {
            _canvasData.Children.Add(PanelTracker.ColorPickerDataPanel);

            ManagerTracker.ColorPickerToolManager.UpdateColorPickerDataPanel();

            _imageBackground.Cursor = Cursors.Cross;
            //PanelTracker.SelectionDataPanel.CursorShape = PanelTracker.SelectionDataPanel.CreateRectangleBrush(PanelTracker.ColorPickerDataPanel.PippeteIcon, GetImageDisplayGeometry());
            PanelTracker.SelectionDataPanel.CursorShape = null;

        }

        public void UpdateColorPickerDataPanel()
        {
            if (_data.CurrentImage == null)
                return;

            try
            {
                if (PanelTracker.ColorPickerDataPanel.InDropColorMode)
                {
                    PanelTracker.ColorPickerDataPanel.textBoxTolerance.Text = _data.CurrentImage.CurrentSelectionConfiguration.ColorAndBackgroundConfiguration.GetDropTolerance().ToString();
                }
                else
                {
                    PanelTracker.ColorPickerDataPanel.textBoxTolerance.Text = _data.CurrentImage.CurrentSelectionConfiguration.ColorAndBackgroundConfiguration.GetKeepTolerance().ToString();
                }
            }
            catch
            {

            }
        }


        public void ColorPickerToolEnd(Image sender)
        {
            _data.ColorSelections = null;
            sender.ReleaseMouseCapture();

            _canvasImageDisplay.Children.Remove(_data.CurrentImage.CurrentSelectionConfiguration.CurrentLine);

            RefreshBackgroundDropoutRender();
        }

        public void RefreshBackgroundDropoutRender()
        {
            bool freshRender = false;

            if (!_data.CurrentImage.CurrentSelectionConfiguration.ColorAndBackgroundConfiguration.ProcessDropout)
            {
                _data.CurrentImage.CurrentSelectionConfiguration.ColorAndBackgroundConfiguration.ProcessDropout = freshRender = true;
            }
            ManagerTracker.ImageDisplayManager.MousePressed = false;
            _data.CurrentRenderedBitmapSourceDropoutValid = false;
            if (freshRender)
            {
                ManagerTracker.RenderManager.RenderRefresh();
            }
            else
            {
                ManagerTracker.RenderManager.RenderGreenScreenCurrentSettings(true);
            }
        }


        public void ColorPickerToolMouseDownHandler(Point location, Image sender)
        {
            if (_data.CurrentImage == null)
                return;
            _data.CurrentImage.CurrentSelectionConfiguration.CurrentLine = new Polyline();
            _data.CurrentImage.CurrentSelectionConfiguration.CurrentLine.Clip = ManagerTracker.DataManager.GetImageDisplayGeometry();
            _data.CurrentImage.CurrentSelectionConfiguration.CurrentLine.Stroke = Brushes.Black;
            _data.CurrentImage.CurrentSelectionConfiguration.CurrentLine.StrokeThickness = 1;
            DoubleCollection dashArray = new DoubleCollection();
            dashArray.Add(3);
            dashArray.Add(3);
            _data.CurrentImage.CurrentSelectionConfiguration.CurrentLine.StrokeDashArray = dashArray;
            _data.CurrentImage.CurrentSelectionConfiguration.CurrentLine.Points.Add(location);

            _data.CurrentImage.CurrentSelectionConfiguration.CurrentLine.Points.Add(location);
            _canvasImageDisplay.Children.Add(_data.CurrentImage.CurrentSelectionConfiguration.CurrentLine);
            ManagerTracker.ImageDisplayManager.MousePressed = true;
            sender.CaptureMouse();

            _data.ColorSelections = new List<GreenScreenBGRA>();
            AddCursorColorToSelections();
        }

        private void AddCursorColorToSelections()
        {
            //try
            //{
            _data.ColorSelections.Add(new GreenScreenBGRA(_data.ColorUnderCursor.R, _data.ColorUnderCursor.G,
              _data.ColorUnderCursor.B, 255));
            //}
            //catch
            //{
            //  PhotoLynx.Logging.PlLog.Logger.Info("GreenScreenPanel.AddCursorColorToSelections error at " + location.X.ToString() + ", " +
            //    location.Y.ToString() + ": " + e.Message);
            //}

        }

        public void ColorPickerToolMouseUpHandler(Image sender)
        {

            if (PanelTracker.ColorPickerDataPanel.InDropColorMode)
            {
                _data.CurrentImage.CurrentSelectionConfiguration.ColorAndBackgroundConfiguration.AddDropColor(_data.ColorSelections);
            }
            else
            {
                _data.CurrentImage.CurrentSelectionConfiguration.ColorAndBackgroundConfiguration.AddKeepColor(_data.ColorSelections);
            }

            ColorPickerToolEnd(sender);
        }

        private Color GetImagePixelUnderCursor(Point location)
        {
            System.Drawing.Color color = _data.CurrentBitmap.GetPixel((int)(location.X * _data.DisplaySizeRatio), (int)(location.Y * _data.DisplaySizeRatio));
            return Color.FromArgb(color.A, color.R, color.G, color.B);
        }

        public void ColorPickerToolMouseMoveHandler(Point location)
        {
            if (_data == null || _data.CurrentBitmap == null)
                return;

            try
            {
                if (location.X < 0 || location.X * _data.DisplaySizeRatio > _data.CurrentBitmap.Width ||
                    location.Y < 0 || location.Y * _data.DisplaySizeRatio > _data.CurrentBitmap.Height)
                    return;

                _data.ColorUnderCursor = GetImagePixelUnderCursor(location);
            }
            catch
            {
                PhotoLynx.Logging.PlLog.Logger.Info("Error: ColorPickerToolMouseMoveHandler. Cannot get pixel color at " + location.X.ToString() + ", " + location.Y.ToString());
                return;
            }
            GreenScreenHSV hsv = GreenScreenHSV.FromRGB(_data.ColorUnderCursor.R, _data.ColorUnderCursor.G, _data.ColorUnderCursor.B, _data.ColorUnderCursor.A);
            PanelTracker.ColorPickerDataPanel.labelRedValue.Content = "R: " + _data.ColorUnderCursor.R.ToString() + "\tH: " + hsv.h.ToString();
            PanelTracker.ColorPickerDataPanel.labelGreenValue.Content = "G: " + _data.ColorUnderCursor.G.ToString() + "\tS: " + hsv.s.ToString();
            PanelTracker.ColorPickerDataPanel.labelBlueValue.Content = "B: " + _data.ColorUnderCursor.B.ToString() + "\tV: " + hsv.v.ToString();

            PanelTracker.ColorPickerDataPanel.rectangleCurrentColor.Fill = new SolidColorBrush(_data.ColorUnderCursor);


            if (ManagerTracker.ImageDisplayManager.MousePressed)
            {
                _data.CurrentImage.CurrentSelectionConfiguration.CurrentLine.Points.Add(location);
                AddCursorColorToSelections();
            }
        }
    }
}