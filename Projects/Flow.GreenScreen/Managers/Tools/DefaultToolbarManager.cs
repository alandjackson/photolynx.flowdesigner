﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using Flow.GreenScreen.Data;
using Flow.GreenScreen.Panels;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using Flow.GreenScreen.Util;
using Flow.GreenScreen.Managers.Panels;
using System.Windows.Media;
using Flow.GreenScreen.Renderer;

namespace Flow.GreenScreen.Managers.Tools
{
  public class DefaultToolbarManager : GenericPanelManager<ImageMatchToolbarPanel>
  {
    Brush _defaultBrush;
    Button _currentButton;
    Canvas _canvasData;

    public DefaultToolbarManager(Canvas canvasData, Grid gridToolBar,  PanelData data)
      : base(data)
    {
      _canvasData = canvasData;

      Panel = new ImageMatchToolbarPanel();

      IntPtr ptr = BitmapInterop.GetHBitmap(Properties.Resources.freeSelectionToolBackgroundIcon);
      Panel.polygonSelectionToolBackgroundIcon.Source = BitmapInterop.CreateBitmapSourceFromHBitmap(ptr,
        IntPtr.Zero, new Int32Rect(0, 0, Properties.Resources.freeSelectionToolBackgroundIcon.Width, Properties.Resources.freeSelectionToolBackgroundIcon.Height), BitmapSizeOptions.FromEmptyOptions());

      ptr = BitmapInterop.GetHBitmap(Properties.Resources.freeSelectionToolForegroundIcon);
      Panel.polygonSelectionToolForegroundIcon.Source = BitmapInterop.CreateBitmapSourceFromHBitmap(ptr,
        IntPtr.Zero, new Int32Rect(0, 0, Properties.Resources.freeSelectionToolForegroundIcon.Width, Properties.Resources.freeSelectionToolForegroundIcon.Height), BitmapSizeOptions.FromEmptyOptions());

      ptr = BitmapInterop.GetHBitmap(Properties.Resources.colorPickerToolBackgroundIcon);
      Panel.colorPickerToolBackgroundIcon.Source = BitmapInterop.CreateBitmapSourceFromHBitmap(ptr,
        IntPtr.Zero, new Int32Rect(0, 0, Properties.Resources.colorPickerToolBackgroundIcon.Width, Properties.Resources.colorPickerToolBackgroundIcon.Height), BitmapSizeOptions.FromEmptyOptions());

      ptr = BitmapInterop.GetHBitmap(Properties.Resources.colorPickerToolForegroundIcon);
      Panel.colorPickerToolForegroundIcon.Source = BitmapInterop.CreateBitmapSourceFromHBitmap(ptr,
        IntPtr.Zero, new Int32Rect(0, 0, Properties.Resources.colorPickerToolForegroundIcon.Width, Properties.Resources.colorPickerToolForegroundIcon.Height), BitmapSizeOptions.FromEmptyOptions());

      ptr = BitmapInterop.GetHBitmap(Properties.Resources.brushToolForegroundIcon);
      Panel.brushToolForegroundIcon.Source = BitmapInterop.CreateBitmapSourceFromHBitmap(ptr,
        IntPtr.Zero, new Int32Rect(0, 0, Properties.Resources.brushToolForegroundIcon.Width, Properties.Resources.brushToolForegroundIcon.Height), BitmapSizeOptions.FromEmptyOptions());

      ptr = BitmapInterop.GetHBitmap(Properties.Resources.brushToolBackgroundIcon);
      Panel.brushToolBackgroundIcon.Source = BitmapInterop.CreateBitmapSourceFromHBitmap(ptr,
        IntPtr.Zero, new Int32Rect(0, 0, Properties.Resources.brushToolBackgroundIcon.Width, Properties.Resources.brushToolBackgroundIcon.Height), BitmapSizeOptions.FromEmptyOptions());

      ptr = BitmapInterop.GetHBitmap(Properties.Resources.handToolIcon);
      Panel.handToolIcon.Source = BitmapInterop.CreateBitmapSourceFromHBitmap(ptr,
        IntPtr.Zero, new Int32Rect(0, 0, Properties.Resources.handToolIcon.Width, Properties.Resources.handToolIcon.Height), BitmapSizeOptions.FromEmptyOptions());

      ptr = BitmapInterop.GetHBitmap(Properties.Resources.zoomToolIcon);
      Panel.zoomToolIcon.Source = BitmapInterop.CreateBitmapSourceFromHBitmap(ptr,
        IntPtr.Zero, new Int32Rect(0, 0, Properties.Resources.zoomToolIcon.Width, Properties.Resources.zoomToolIcon.Height), BitmapSizeOptions.FromEmptyOptions());

      ptr = BitmapInterop.GetHBitmap(Properties.Resources.backgroundDefinitionIcon);
      Panel.BackgroundDefinitionToolIcon.Source = BitmapInterop.CreateBitmapSourceFromHBitmap(ptr,
        IntPtr.Zero, new Int32Rect(0, 0, Properties.Resources.backgroundDefinitionIcon.Width, Properties.Resources.backgroundDefinitionIcon.Height), BitmapSizeOptions.FromEmptyOptions());

      ManagerTracker.SettingsPanelManager.LoadBitmaps();

      if (_data.AllowAdvancedTools)
      {
        ptr = BitmapInterop.GetHBitmap(Properties.Resources.curveToolIcon);
        Panel.bezierToolIcon.Source = BitmapInterop.CreateBitmapSourceFromHBitmap(ptr,
          IntPtr.Zero, new Int32Rect(0, 0, Properties.Resources.curveToolIcon.Width, Properties.Resources.curveToolIcon.Height), BitmapSizeOptions.FromEmptyOptions());
      }
      else
      {
        Panel.BezierTool.Visibility = Visibility.Hidden;
        ManagerTracker.ColorPickerDataPanelManager.Panel.checkBoxSoftenEdges.Visibility = Visibility.Hidden;
      }

      Panel.uxSaveBackgroundColor.Click += new RoutedEventHandler(uxSaveBackgroundColor_Click);
      Panel.uxResetBackgroundColor.Click += new RoutedEventHandler(uxResetBackgroundColor_Click);

      Panel.uxBackgroundFreehand.Click += new RoutedEventHandler(FreehandLassoChecked);
      Panel.uxForegroundFreehand.Click += new RoutedEventHandler(FreehandLassoChecked);
      Panel.uxBackgroundPolygonal.Click += new RoutedEventHandler(PolygonalLassoChecked);
      Panel.uxForegroundPolygonal.Click += new RoutedEventHandler(PolygonalLassoChecked);
      UpdateLassoTypeChecks(GreenScreenSettings.Instance.LassoSelectionIsFreehand);
      

      Panel.ColorPickerToolBackground.Loaded += new RoutedEventHandler(ManagerTracker.ColorPickerToolManager.ColorPickerTool_Loaded);

      PanelTracker.AddPanel(Panel, typeof(ImageMatchToolbarPanel));
      gridToolBar.Children.Add(Panel);
    }

    void uxResetBackgroundColor_Click(object sender, RoutedEventArgs e)
    {
      try
      {
        GreenScreenSettings.Instance.DefaultBackgroundBlue.ResetDefaultValue();
        GreenScreenSettings.Instance.DefaultBackgroundGreen.ResetDefaultValue();
        GreenScreenSettings.Instance.DefaultBackgroundRed.ResetDefaultValue();
        
        BackgroundAndColorConfiguration config = _data.CurrentImage.CurrentSelectionConfiguration.ColorAndBackgroundConfiguration;
        
        config.BackgroundDefinitionBlue = GreenScreenAlgorithm.DefaultBlue;
        config.BackgroundDefinitionRed = GreenScreenAlgorithm.DefaultRed;
        config.BackgroundDefinitionGreen = GreenScreenAlgorithm.DefaultGreen;
        
        ManagerTracker.ColorPickerToolManager.RefreshBackgroundDropoutRender();
      }
      catch (Exception err) { ErrorManager.Instance.LogError(err); }
    }

    void uxSaveBackgroundColor_Click(object sender, RoutedEventArgs e)
    {
      try
      {
        BackgroundAndColorConfiguration config = _data.CurrentImage.CurrentSelectionConfiguration.ColorAndBackgroundConfiguration;
        GreenScreenSettings.Instance.DefaultBackgroundBlue.SaveValue((int)config.BackgroundDefinitionBlue);
        GreenScreenSettings.Instance.DefaultBackgroundGreen.SaveValue((int)config.BackgroundDefinitionGreen);
        GreenScreenSettings.Instance.DefaultBackgroundRed.SaveValue((int)config.BackgroundDefinitionRed);
      }
      catch (Exception err) { ErrorManager.Instance.LogError(err); }
    }
    
    bool _suspendUpdateLassoTypes = false;
    void UpdateLassoTypeChecks(bool isFreehand)
    {
      if (_suspendUpdateLassoTypes) return;

      _suspendUpdateLassoTypes = true;
      Panel.uxBackgroundFreehand.IsChecked = isFreehand;
      Panel.uxForegroundFreehand.IsChecked = isFreehand;
      Panel.uxBackgroundPolygonal.IsChecked = !isFreehand;
      Panel.uxForegroundPolygonal.IsChecked = !isFreehand;
      _suspendUpdateLassoTypes = false;
      ManagerTracker.PolygonSelectionToolManager.IsFreehand = isFreehand;
      GreenScreenSettings.Instance.LassoSelectionIsFreehand = isFreehand;
    }

    void FreehandLassoChecked(object sender, RoutedEventArgs e)
    {
      UpdateLassoTypeChecks(true);
    }

    void PolygonalLassoChecked(object sender, RoutedEventArgs e)
    {
      UpdateLassoTypeChecks(false);
    }

    public void ToolButtonHit(object sender, RoutedEventArgs e)
    {
      if (_currentButton == null)
      {
        _defaultBrush = (sender as Button).Background;
      }
      else
      {
        _currentButton.Background = _defaultBrush;
      }

      _currentButton = (Button)sender;
      _currentButton.Background = new LinearGradientBrush(Colors.Aqua, Colors.DarkGray, 90);

      string name = (sender as Button).Name;

      if (sender == Panel.ColorPickerToolBackground)
      {
        name = ToolboxButtons.ColorPickerTool;
        PanelTracker.ColorPickerDataPanel.InDropColorMode = true;
      }
      else if (sender == Panel.ColorPickerToolForeground)
      {
        name = ToolboxButtons.ColorPickerTool;
        PanelTracker.ColorPickerDataPanel.InDropColorMode = false;
      }
      else if (sender == Panel.PolygonSelectionToolBackground)
      {
        name = ToolboxButtons.PolygonSelectionTool;
        PanelTracker.SelectionDataPanel.IsBackgroundMode = true;
      }
      else if (sender == Panel.PolygonSelectionToolForeground)
      {
        name = ToolboxButtons.PolygonSelectionTool;
        PanelTracker.SelectionDataPanel.IsBackgroundMode = false;
      }
      else if (sender == Panel.BrushToolForeground)
      {
        name = ToolboxButtons.BrushTool;
        PanelTracker.SelectionDataPanel.IsBackgroundMode = false;
      }
      else if (sender == Panel.BrushToolBackground)
      {
        name = ToolboxButtons.BrushTool;
        PanelTracker.SelectionDataPanel.IsBackgroundMode = true;
      }

      if (name == _data.CurrentTool)
      {
        return;
      }

      switch (_data.CurrentTool)
      {
        case ToolboxButtons.HandTool:
          break;
        case ToolboxButtons.BrushTool:
          ManagerTracker.BrushToolManager.BrushToolDeselected();
          break;
        case ToolboxButtons.ColorPickerTool:
          break;
        case ToolboxButtons.PolygonSelectionTool:
          ManagerTracker.PolygonSelectionToolManager.PolygonSelectionToolDeSelected();
          break;
        case ToolboxButtons.ZoomTool:
          break;
        case ToolboxButtons.BackgroundDefinitionTool:
          break;
        default:
          break;
      }

      _canvasData.Children.Clear();
      _data.CurrentTool = name;
      switch (name)
      {
        case ToolboxButtons.BezierTool:
          ManagerTracker.BezierToolManager.BezierToolSelected();
          break;
        case ToolboxButtons.HandTool:
          ManagerTracker.HandToolManager.HandToolSelected();
          break;
        case ToolboxButtons.BrushTool:
          ManagerTracker.BrushToolManager.BrushToolSelected();
          PanelTracker.SelectionDataPanel.groupBoxBrushType.Visibility = Visibility.Visible;
          PanelTracker.SelectionDataPanel.groupBoxSelectionOptions.Visibility = Visibility.Hidden;
          PanelTracker.SelectionDataPanel.groupBoxSelectionsList.Visibility = Visibility.Hidden;
          break;
        case ToolboxButtons.ColorPickerTool:
          PanelTracker.ColorPickerDataPanel.HideBackgroundSlider();
          ManagerTracker.ColorPickerToolManager.ColorPickerToolSelected();
          break;
        case ToolboxButtons.PolygonSelectionTool:
          ManagerTracker.PolygonSelectionToolManager.PolygonSelectionToolSelected(true);
          PanelTracker.SelectionDataPanel.groupBoxBrushType.Visibility = Visibility.Hidden;
          //PanelTracker.SelectionDataPanel.groupBoxSelectionOptions.Visibility = Visibility.Visible;
          //PanelTracker.SelectionDataPanel.groupBoxSelectionsList.Visibility = Visibility.Visible;
          break;
        case ToolboxButtons.ZoomTool:
          ManagerTracker.ZoomToolManager.ZoomToolSelected();
          break;
        case ToolboxButtons.BackgroundDefinitionTool:
          PanelTracker.ColorPickerDataPanel.ShowBackgroundSlider();
          ManagerTracker.ColorPickerToolManager.ColorPickerToolSelected();
          break;
        default:
          break;
      }
    }
  }
}
