using System.Windows;
using System.Windows.Controls;
using Flow.GreenScreen.Data;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
namespace Flow.GreenScreen.Managers.Tools
{
  public class BezierToolManager : GenericManager
  {
    #region Members

    Image _imageDisplay;
    Image _imageBackground;
    Canvas _canvasData;

    #endregion

    #region Constructors

    public BezierToolManager(Image imageDisplay, Image imageBackground, Canvas canvasData, PanelData data) : base(data)
    {
      _imageDisplay = imageDisplay;
      _imageBackground = imageBackground;
      _canvasData = canvasData;
      
    }

    #endregion

    #region Methods

    public  void BezierToolSelected()
    {
      _imageBackground.Cursor = Cursors.Cross;
      PanelTracker.SelectionDataPanel.CursorShape = null;
      _canvasData.Children.Add(PanelTracker.CurvePanel);
    }

    public  void BezierToolMouseMoveHandler(Point location)
    {
      int alpha = GetRenderedAlphaBelowCursor(location);

      ManagerTracker.RenderManager.SetCanvasPosition(PanelTracker.CurvePanel.rectangleTransparencyIndicator,
        alpha - (double)PanelTracker.CurvePanel.rectangleTransparencyIndicator.GetValue(Rectangle.WidthProperty) / 2,
        (double)PanelTracker.CurvePanel.rectangleTransparencyIndicator.GetValue(Canvas.TopProperty));
    }

    private int GetRenderedAlphaBelowCursor(Point location)
    {
      if (!(_imageDisplay.Source is BitmapSource))
      {
        return 255;
      }
      CroppedBitmap cb;
      byte[] pixels = new byte[4];
      try
      {
        cb = new CroppedBitmap((BitmapSource)_imageDisplay.Source,
          new Int32Rect((int)(location.X * _data.DisplaySizeRatio), (int)(location.Y * _data.DisplaySizeRatio), 1, 1));
        cb.CopyPixels(pixels, 4, 0);
      }
      catch
      {
        //MessageBox.Show(ex.Message);
      }

      return pixels[3];
    }



    #endregion
  }
}