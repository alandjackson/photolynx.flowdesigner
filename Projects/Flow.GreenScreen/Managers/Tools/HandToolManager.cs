using System.Windows;
using Flow.GreenScreen.Data;
using System.Windows.Controls;
using System.Windows.Input;
using PhotoLynx.Logging;
using Flow.GreenScreen.Managers.Panels;
using Flow.GreenScreen.Util;
namespace Flow.GreenScreen.Managers.Tools
{
  public class HandToolManager : GenericManager
  {
    Image _imageDisplay;
    Image _imageBackground;
    Image _imageForeground;
    Canvas _canvasImageDisplay;

    public HandToolManager(Canvas canvasImageDisplay, 
                           Image imageForeground, 
                           Image imageBackground, 
                           Image imageDispay, 
                           PanelData data)
      : base(data)
    {
      _imageForeground = imageForeground;
      _canvasImageDisplay = canvasImageDisplay;
      _imageBackground = imageBackground;
      _imageDisplay = imageDispay;
    }

    public void HandToolSelected()
    {
      _imageBackground.Cursor = Cursors.Hand;
      PanelTracker.SelectionDataPanel.CursorShape = null;
    }

    public void HandToolMouseDown(System.Drawing.Point location, 
                                  Image sender,
                                  MouseButtonEventArgs e)
    {
      ManagerTracker.ImageDisplayManager.MousePressed = true;
      sender.CaptureMouse();
      sender.Cursor = Cursors.None;
    }

    public void HandToolMouseMove(Point currentLocation, Point lastLocation)
    {
      if (! ManagerTracker.ImageDisplayManager.MousePressed) return;
 
      if (false && _data.Variant == VariantType.CPI)
      {
        double dtop = currentLocation.Y - lastLocation.Y;
        double dleft = currentLocation.X - lastLocation.X;
        WpfUtil.MoveOnCanvas(_imageDisplay, new Point(dleft, dtop));
        WpfUtil.MoveOnCanvas(_imageForeground, new Point(dleft, dtop));
      }
      else
      {
        if (ManagerTracker.ImageDisplayManager.ZoomFactor > 1.0)
        {
          double top = Canvas.GetTop(_canvasImageDisplay) + 
                       currentLocation.Y - lastLocation.Y;
          double left = Canvas.GetLeft(_canvasImageDisplay) + 
                        currentLocation.X - lastLocation.X;

          Canvas.SetTop(_canvasImageDisplay, top);
          Canvas.SetLeft(_canvasImageDisplay, left);

        }
      }
    }

    public void HandToolMouseUp(Image sender)
    {
      sender.Cursor = Cursors.Hand;
      sender.ReleaseMouseCapture();

      if (false && _data.Variant == VariantType.CPI)
      {
        _data.CurrentImage.CurrentSelectionConfiguration.ForegroundTopLeft = 
            new Point(Canvas.GetLeft(_imageDisplay), Canvas.GetTop(_imageDisplay));
      }

      ManagerTracker.ImageDisplayManager.MousePressed = false;
    }

  }
}