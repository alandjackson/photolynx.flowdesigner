﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.GreenScreen.Data;

namespace Flow.GreenScreen.Managers
{
  public class GenericManager
  {
    #region Members

    protected PanelData _data;

    #endregion

    #region Constructors

    public GenericManager()
      : this(null)
    {
    }

    public GenericManager(PanelData data)
    {
      _data = data;
    }

    #endregion

    #region Properties

    #endregion

    #region Methods



    #endregion
  }
}
