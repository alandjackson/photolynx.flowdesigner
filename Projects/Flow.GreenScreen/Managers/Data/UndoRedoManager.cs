﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flow.GreenScreen.Managers.Data
{
  public class UndoRedoManager<T> : GenericManager
  {
    Stack<T> UndoStack = new Stack<T>();
    Stack<T> RedoStack = new Stack<T>();

    public UndoRedoManager()
    {
    }
    
    public void Clear()
    {
      UndoStack = new Stack<T>();
      RedoStack = new Stack<T>();
    }

    public T Undo()
    {
      if (UndoStack.Count <= 1) return default(T);
    
      RedoStack.Push(UndoStack.Pop());
      return UndoStack.Peek();
    }
    
    public T Redo()
    {
      if (RedoStack.Count == 0) return default(T);
      
      T item = RedoStack.Pop();
      UndoStack.Push(item);
      return item;
    }

    public void PushHistory(T data)
    {
      UndoStack.Push(data);
      RedoStack = new Stack<T>();
    }
  } 
}
