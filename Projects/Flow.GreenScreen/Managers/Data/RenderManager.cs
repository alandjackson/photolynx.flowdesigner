﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Shapes;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using Flow.GreenScreen.Panels;
using System.Windows.Controls;
using Flow.GreenScreen.Data;
using Flow.GreenScreen.Renderer;
using Flow.GreenScreen.Util;
using System.Diagnostics;
using System.IO;
using Flow.GreenScreen.Managers.Panels;
using PhotoLynx.Logging;
using System.Drawing;
using Brushes = System.Windows.Media.Brushes;
using Image = System.Windows.Controls.Image;
using Point = System.Windows.Point;
using Pen = System.Windows.Media.Pen;
using Size = System.Drawing.Size;

namespace Flow.GreenScreen.Managers.Data
{
    public class RenderManager : GenericManager
    {

        GreenScreenAlgorithm _greenScreenAlgorithm;
        Canvas _canvasImageDisplay;
        DrawingBrush _transparentBackgroundBrush;
        Image _imageRendered;
        Image _imageBackground;
        Image _imageForeground;
        Image _imageWorking;
        List<Selection> _displayedPolygons;


        #region Constructors

        public RenderManager(Canvas canvasImageDisplay, Image imageWorking,
                             Image imageRendered, Image imageBackground,
                             Image imageForeground, PanelData data)
            : base(data)
        {
            _imageWorking = imageWorking;
            _imageRendered = imageRendered;
            _imageBackground = imageBackground;
            _imageForeground = imageForeground;


            _imageRendered.Width = GreenScreenPanel.RenderWidth;
            _imageRendered.Height = GreenScreenPanel.RenderHeight;
            _imageBackground.Width = GreenScreenPanel.RenderWidth;
            _imageBackground.Height = GreenScreenPanel.RenderHeight;
            _imageForeground.Width = GreenScreenPanel.RenderWidth;
            _imageForeground.Height = GreenScreenPanel.RenderHeight;
            _imageWorking.Width = GreenScreenPanel.RenderWidth;
            _imageWorking.Height = GreenScreenPanel.RenderHeight;

            _canvasImageDisplay = canvasImageDisplay;
            _displayedPolygons = new List<Selection>();

            //_layerGroup = new RenderingLayerGroup();
            //ForegroundLayer = new RenderingLayer(canvasImageDisplay, false);
            //BackgroundLayer = new RenderingLayer(canvasImageDisplay, true);
            //_layerGroup.AddLayer(ForegroundLayer);
            //_layerGroup.AddLayer(BackgroundLayer);

            //_backgroundImage = new Rectangle();
            //canvasImageDisplay.Children.Insert(0, _backgroundImage);

            _imageRendered.Loaded += new RoutedEventHandler(_imageDisplay_Loaded);
        }

        #endregion

        public DrawingBrush TransparentBackgroundBrush
        {
            get
            {
                if (_transparentBackgroundBrush == null)
                {
                    _transparentBackgroundBrush = new DrawingBrush();
                    GeometryDrawing backgroundSquare =
                        new GeometryDrawing(
                            Brushes.White,
                            null,
                            new RectangleGeometry(new Rect(0, 0, 4, 4)));

                    GeometryGroup aGeometryGroup = new GeometryGroup();
                    aGeometryGroup.Children.Add(new RectangleGeometry(new Rect(0, 0, 2, 2)));
                    aGeometryGroup.Children.Add(new RectangleGeometry(new Rect(2, 2, 2, 2)));

                    GeometryDrawing checkers = new GeometryDrawing(
                      Brushes.LightGray, null, aGeometryGroup);

                    DrawingGroup checkersDrawingGroup = new DrawingGroup();
                    checkersDrawingGroup.Children.Add(backgroundSquare);
                    checkersDrawingGroup.Children.Add(checkers);

                    _transparentBackgroundBrush.Drawing = checkersDrawingGroup;
                    _transparentBackgroundBrush.Viewport = new Rect(0, 0, 0.075, 0.075);
                    _transparentBackgroundBrush.TileMode = TileMode.Tile;
                }
                return _transparentBackgroundBrush;
            }
        }

        public void RenderRefresh()
        {
            RenderRefresh(true);
        }

        public void RenderRefresh(bool unrenderedChange)
        {
            RenderGreenScreenCurrentSettings(false, unrenderedChange);
            if (!ManagerTracker.SettingsPanelManager.IsBackgroundValid())
            {
                LoadTransparencyBackground();
            }
            RebindClips();
        }

        public void InvalidateGreenScreenRender()
        {
            _data.CurrentRenderedBitmapSourceDropoutValid = false;
            RenderGreenScreenCurrentSettings();
        }

        public void RenderGreenScreenCurrentSettings()
        {
            RenderGreenScreenCurrentSettings(false);
        }

        public void RenderGreenScreenCurrentSettings(bool renderLayers)
        {
            RenderGreenScreenCurrentSettings(renderLayers, true);
        }
        public void RenderGreenScreenCurrentSettings(bool renderLayers, bool unrenderedChange)
        {
            RenderGreenScreen(_data.CurrentImage,
              _data.CurrentImage.CurrentSelectionConfiguration.ColorAndBackgroundConfiguration,
              renderLayers, unrenderedChange);
        }

        public void RenderGreenScreen(ImageData greenScreenImage,
          BackgroundAndColorConfiguration configuration, bool renderLayers)
        {
            RenderGreenScreen(greenScreenImage, configuration, renderLayers, true);
        }

        public void RenderGreenScreen(ImageData greenScreenImage,
          BackgroundAndColorConfiguration configuration,
          bool renderLayers,
          bool unrenderedChange)
        {
            try
            {
                ManagerTracker.MainDisplayManager.MouseBusy();
                if (greenScreenImage != null)
                {
                    SelectionConfiguration selectionConfig =
                      _data.CurrentImage.CurrentSelectionConfiguration;
                    BackgroundAndColorConfiguration bgConfig =
                      selectionConfig.ColorAndBackgroundConfiguration;

                    if (_data.Variant == VariantType.CPI && !bgConfig.ProcessDropout)
                    {
                        bgConfig.ProcessDropout = true;
                    }
                    if (ManagerTracker.SettingsPanelManager.RenderEnabled &&
                        bgConfig.ProcessDropout)
                    {
                        if (!_data.CurrentRenderedBitmapSourceDropoutValid)
                        {
                            Slider uxBgStrength = ManagerTracker.ColorPickerDataPanelManager.Panel.
                                                  sliderBackgroundStrength;
                            bgConfig.BackgroundCorrection = uxBgStrength.Value;

                            if (_greenScreenAlgorithm == null)
                            {
                                _greenScreenAlgorithm = new GreenScreenAlgorithm(bgConfig,
                                  ManagerTracker.CurvePanelManager.CurveTable);
                            }
                            else
                            {
                                _greenScreenAlgorithm.SetConfiguration(bgConfig, true);
                            }

                            double zoom = ManagerTracker.ImageDisplayManager.ZoomFactor;
                            double rwidth = GreenScreenPanel.RenderWidth;
                            double rheight = GreenScreenPanel.RenderHeight;
                            Size renderSize = new Size((int)Math.Ceiling(rwidth * zoom),
                                                       (int)Math.Ceiling(rheight * zoom));


                            using (Bitmap virtBitmap = ManagerTracker.ImageFileVirtualizationManager.
                              VirtualizeImageBitmap(_data.CurrentBitmap, renderSize))
                            {
                                using (Bitmap renderedBitmap =
                                  _greenScreenAlgorithm.RunAlgorithm(virtBitmap))
                                {
                                    if (selectionConfig.RotateFlipType != RotateFlipType.RotateNoneFlipNone)
                                        renderedBitmap.RotateFlip(selectionConfig.RotateFlipType);

                                    _data.CurrentRenderedBitmapSource =
                                      BitmapInterop.ToBitmapImage(renderedBitmap);
                                }
                            }

                            _data.CurrentRenderedBitmapSource.Freeze();
                            _data.CurrentRenderedBitmapSourceDropoutValid = true;
                        }
                        UpdateImageDisplay(_data.CurrentRenderedBitmapSource);
                    }
                    else
                    {//not checked
                        if (unrenderedChange)
                        {
                            UpdateImageDisplay(_data.CurrentOriginalBitmapSource);
                            _imageRendered.Clip = null;
                            _imageForeground.Clip = Geometry.Empty;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                ErrorManager.Instance.LogError(e);
            }
            finally
            {
                ManagerTracker.MainDisplayManager.MouseIdle();
            }
        }

        public void PreviewBackground(ImageSource background)
        {
            _imageBackground.Source = background;
        }

        public void RestoreBackground()
        {
            _imageBackground.Source = _data.CurrentBackgroundSource;
        }

        public void UpdateBackgroundImage()
        {
            UpdateBackgroundImage(ManagerTracker.SettingsPanelManager.BackgroundPath);
        }

        public void UpdateBackgroundImage(string imagePath)
        {
            try
            {
                ManagerTracker.MainDisplayManager.MouseBusy();

                // image doesn't exist so load the default transparent background
                if (!File.Exists(imagePath) || !ManagerTracker.DataManager.IsImage(imagePath))
                {
                    LoadTransparencyBackground();
                    return;
                }

                // same image is already loaded
                if ((_data.CurrentBackgroundSource is BitmapImage) && (_data.CurrentBackgroundSource as BitmapImage).UriSource.AbsolutePath.Equals(imagePath))
                    return;

                // go ahead and load the background
                if (_imageRendered.ActualHeight > 0)
                    _imageBackground.Height = _imageRendered.ActualHeight;
                SetBackground(imagePath, LoadBackgroundToBitmapImage(imagePath));

            }

            catch (Exception err)
            {
                PhotoLynx.Logging.PlLog.Logger.Info("Rendering Exception: " + err.ToString());
            }
            finally
            {
                ManagerTracker.MainDisplayManager.MouseIdle();
            }
        }

        private BitmapImage LoadBackgroundToBitmapImage(string imagePath)
        {
            BitmapImage background = new BitmapImage();
            //background.Freeze();
            background.BeginInit();
            background.CacheOption = BitmapCacheOption.OnLoad;
            background.DecodePixelHeight = (int)Math.Ceiling(_imageBackground.Height);
            background.DecodePixelWidth = (int)Math.Ceiling(_imageBackground.Width);
            background.UriSource = new Uri(imagePath);
            background.EndInit();
            int q = background.PixelHeight;
            return background;
        }

        private void LoadTransparencyBackground()
        {
            SetBackground("", BuildTransparentImage());
        }

        private ImageSource BuildTransparentImage()
        {
            return new DrawingImage(new GeometryDrawing(TransparentBackgroundBrush, new Pen(),
                new RectangleGeometry(new Rect(0, 0, _imageBackground.Width, _imageBackground.Height))));
        }

        private void SetBackground(string path, ImageSource img)
        {
            _data.CurrentImage.CurrentSelectionConfiguration.ColorAndBackgroundConfiguration.BackgroundImagePath = path;
            _data.CurrentBackgroundSource = img;
            _imageBackground.Source = img;
        }


        private void DrawElipse(Point location, int width, int height)
        {
            Ellipse e = new Ellipse();
            e.Fill = Brushes.Red;
            e.Width = width;
            e.Height = height;
            _canvasImageDisplay.Children.Add(e);
            Canvas.SetTop(e, location.Y - height / 2.0);
            Canvas.SetLeft(e, location.X - width / 2.0);
        }

        public void SetCanvasPosition(FrameworkElement shape, double x, double y)
        {
            Canvas.SetTop(shape, y);
            Canvas.SetLeft(shape, x);
        }


        void _imageDisplay_Loaded(object sender, RoutedEventArgs e)
        {
            UpdateDisplayRatio();
        }


        public void UpdateImageDisplay(BitmapSource image)
        {
            PlLog.Logger.Info("UpdateImageDisplay.start, source: " + image);
            if (_imageRendered.Source == image)
            {
                PlLog.Logger.Info("Source not new, returning");
                return;
            }
            _imageRendered.Source = image;
            UpdateDisplayRatio();

            UpdateBackgroundImage();

            if (_data.Variant == VariantType.ImageMatch)
            {
                double scale = GreenScreenAlgorithm.GetScale(_data.CurrentBitmap,
                                                             GreenScreenPanel.RenderWidth,
                                                             GreenScreenPanel.RenderHeight);
                System.Drawing.RectangleF displayCropRect = DataManager.GetDisplayRect(
                        _data.GetCrop(_data.CurrentImage.ImagePath),
                        new System.Drawing.Size(
                                               (int)(_data.CurrentBitmap.Width / scale),
                                               (int)(_data.CurrentBitmap.Height / scale)));
                UpdateCropRect(displayCropRect);

                PlLog.Logger.Info("Updating Display, Current Bitmap: " + _data.CurrentBitmap.ToString() + _data.CurrentBitmap.Size.ToString());

            }
        }

        private void UpdateCropRect(System.Drawing.RectangleF displayCropRect)
        {
            GreenScreenPanel gsPnl = ManagerTracker.MainDisplayManager.Panel;
            //gsPnl.cropRectangle.Visibility = Visibility.Hidden;
            gsPnl.cropRectangle.Width = displayCropRect.Width + gsPnl.cropRectangle.StrokeThickness * 2;
            gsPnl.cropRectangle.Height = displayCropRect.Height + +gsPnl.cropRectangle.StrokeThickness * 2;
            gsPnl.cropRectangle.Margin = new Thickness(displayCropRect.X - gsPnl.cropRectangle.StrokeThickness,
                                                       displayCropRect.Y - gsPnl.cropRectangle.StrokeThickness, 0, 0);
            gsPnl.invertCropRectangle.Width = GreenScreenPanel.RenderWidth;
            gsPnl.invertCropRectangle.Height = GreenScreenPanel.RenderHeight;
            gsPnl.invertCropRectangle.Margin = new Thickness(0, 0, 0, 0);
            gsPnl.invertCropClipSmall.Rect = new Rect(0, 0, GreenScreenPanel.RenderWidth, GreenScreenPanel.RenderHeight);
            gsPnl.invertCropClipLarge.Rect = new Rect(displayCropRect.X, displayCropRect.Y,
                                                      displayCropRect.Width, displayCropRect.Height);
        }

        private void UpdateDisplayRatio()
        {
            if (_data == null || _data.CurrentBitmap == null) return;

            _data.DisplaySizeRatio = _data.CurrentBitmap.Height / _imageRendered.ActualHeight;
        }

        internal void ImageDisplaySizeChanged(double width, double height)
        {
            //_layerGroup.SetSize(width, height);
        }

        internal void RecordSelectioChanged()
        {

            _imageForeground.Source = _data.CurrentOriginalBitmapSource;
            //_imageBackground.Source = _data.CurrentBackground;

            RebindClips();
        }

        internal void RebindClips()
        {
            _imageForeground.Clip = _data.CurrentImage.CurrentSelectionConfiguration.ForegroundClip;
            _imageRendered.Clip = new CombinedGeometry(GeometryCombineMode.Exclude,
              new RectangleGeometry(new Rect(0, 0, _imageRendered.Width, _imageRendered.Height)), _data.CurrentImage.CurrentSelectionConfiguration.RenderedClipNegative).GetOutlinedPathGeometry();
        }

        internal void AddSelectionGeometry(Geometry geometry, bool isBackground)
        {
            if (isBackground)
            {
                _data.CurrentImage.CurrentSelectionConfiguration.RenderedClipNegative = new CombinedGeometry(GeometryCombineMode.Union, _data.CurrentImage.CurrentSelectionConfiguration.RenderedClipNegative, geometry).GetOutlinedPathGeometry();
                _data.CurrentImage.CurrentSelectionConfiguration.ForegroundClip = new CombinedGeometry(GeometryCombineMode.Exclude, _data.CurrentImage.CurrentSelectionConfiguration.ForegroundClip, geometry).GetOutlinedPathGeometry();
            }
            else
            {
                _data.CurrentImage.CurrentSelectionConfiguration.ForegroundClip = new CombinedGeometry(GeometryCombineMode.Union, _data.CurrentImage.CurrentSelectionConfiguration.ForegroundClip, geometry).GetOutlinedPathGeometry();
            }
            _data.CurrentImage.CurrentSelectionConfiguration.ForegroundClip.FillRule = FillRule.Nonzero;

            RebindClips();
        }


    }
}
