﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;
using System.Windows.Controls;
using Flow.GreenScreen.Data;
using PhotoLynx.ImageThumbnailer;
using System.Windows.Media.Imaging;
using Flow.GreenScreen.Managers.Panels;
using System.Reflection;
using Flow.GreenScreen.Panels;
using Flow.GreenScreen.Util;
using PhotoLynx.Logging;
using System.IO;

namespace Flow.GreenScreen.Managers.Data
{
    public class DataManager : GenericManager
    {
        Image _imageDisplay;

        public DataManager(PlImageList imageList, Image imageDisplay, PanelData data)
            : base(data)
        {
            _imageDisplay = imageDisplay;
        }


        public bool VerifyClose()
        {
            _data.AllowClose = true;
            return true;
        }

        private bool IsConfigurationSaved(ImageData image)
        {
            if (!image.CurrentSelectionConfiguration.Equals(SelectionConfiguration.DeserializeFromString(image.SelectionConfigurationXml)))
            {
                return false;
            }
            return true;
        }

        private bool IsConfigurationSaved()
        {
            return IsConfigurationSaved(_data.CurrentImage);
        }

        public void UpdateClips(PathGeometry[] lastClips)
        {
            if (lastClips == null || lastClips.Length != 2) return;
            _data.CurrentImage.CurrentSelectionConfiguration.ForegroundClip = lastClips[0];
            _data.CurrentImage.CurrentSelectionConfiguration.RenderedClipNegative = lastClips[1];
            _data.CurrentImage.SaveSelectionData();

            ManagerTracker.RenderManager.RebindClips();
        }

        public void Redo()
        {
            UpdateClips(ManagerTracker.UndoRedoManager.Redo());
        }

        public void Undo()
        {
            UpdateClips(ManagerTracker.UndoRedoManager.Undo());
        }


        public bool IsImage(string path)
        {
            try
            {
                BitmapImage image = new BitmapImage();
                image.BeginInit();
                image.DecodePixelHeight = 1;
                image.DecodePixelWidth = 1;
                image.UriSource = new Uri(path);
                image.EndInit();
            }
            catch
            {
                return false;
            }
            return true;
        }

        public Point FormatPoint(Point p)
        {
            return new Point(Math.Round(p.X, 3), Math.Round(p.Y, 3));
        }

        private Point GetTopLeftPoint(PointCollection pointCollection)
        {
            Point topLeft = pointCollection[0];
            for (int i = 1; i < pointCollection.Count; i++)
            {
                if (topLeft.Y < pointCollection[i].Y)
                {
                    topLeft = pointCollection[i];
                }
                if (topLeft.X < pointCollection[i].X)
                {
                    topLeft = pointCollection[i];
                }
            }
            return topLeft;
        }

        public RectangleGeometry GetImageDisplayGeometry()
        {
            return new RectangleGeometry(new Rect(new Size(GreenScreenPanel.RenderWidth, GreenScreenPanel.RenderHeight)));
        }

        private Point ImageDisplayResizeRatioTransform(Point location)
        {
            location.X *= _data.DisplaySizeRatio;
            location.Y *= _data.DisplaySizeRatio;
            return location;
        }

        public void RecordSelectionChanged(System.Collections.IList removedItems)
        {
            if (removedItems != null &&
                removedItems.Count != 0 &&
                !IsConfigurationSaved(removedItems[0] as ImageData) &&
                GreenScreenSettings.Instance.ConfirmUnsavedChanges)
            {
                MessageBoxResult result = MessageBox.Show("There are unsaved changes. Continue?",
                                                          "Unsaved changes",
                                                          MessageBoxButton.YesNo,
                                                          MessageBoxImage.Warning);
                if (result == MessageBoxResult.No)
                {
                    return;
                }
            }

            ManagerTracker.MainDisplayManager.MouseBusy();

            //unload SelectionConfiguration
            if (removedItems != null && removedItems.Count > 0)
            {
                (removedItems[0] as ImageData).CurrentSelectionConfiguration = null;
            }
            try
            {
                IsImageUpdatingEnabled = false;
                ManagerTracker.MainDisplayManager.SetStatusMessage("Loading Image...");

                BeginUpdateSelectedImage();
            }
            finally
            {
            }

            ManagerTracker.MainDisplayManager.MouseIdle();

        }

        public void BeginUpdateSelectedImage()
        {
            // Alan: updating the selected image in a background thread kills the rendered 
            // image control under very special circumstances in some instances, so I am 
            // removing until I know how this works better.
            UpdateSelectedImage();

            //new System.Threading.Thread(new System.Threading.ThreadStart(UpdateSelectedImage)).Start();
        }


        public void UpdateSelectedImage()
        {
            UpdateSelectedImage(0);
            //UpdateSelectedImage(_data.ImagesInFilter[ManagerTracker.MainDisplayManager.Panel.thumbsImageList.SelectedIndex]);
        }

        public void UpdateSelectedImage(string filename)
        {
            UpdateSelectedImage(new ImageData(filename, 0));
        }

        public void UpdateSelectedImage(int ndx)
        {
            if (ndx < _data.ImagesInFilter.Count && ndx >= 0)
                UpdateSelectedImage(_data.ImagesInFilter[ndx]);
        }

        protected void RunOnUIThread(Action a)
        {
            a();
        }

        public void UpdateSelectedImage(ImageData image)
        {
            if (!File.Exists(image.FilePath))
                return;
            try
            {
                RunOnUIThread(() =>
                    {
                        _data.CurrentImage = image;

                        _data.CurrentImage.CurrentSelectionConfiguration = null;
                        _data.CurrentImage.LoadConfiguration();
                        ManagerTracker.SettingsPanelManager.BackgroundPath = 
                            _data.CurrentImage.CurrentSelectionConfiguration.ColorAndBackgroundConfiguration.BackgroundImagePath;

                        PanelTracker.SelectionDataPanel.SelectionConfiguration = 
                            _data.CurrentImage.CurrentSelectionConfiguration;
                        PanelTracker.SelectionDataPanel.RefreshList();

                        _imageDisplay.OpacityMask = null;
                    });

                _data.CurrentRenderedBitmapSourceDropoutValid = false;

                RunOnUIThread(() =>
                    {
                        ManagerTracker.SettingsPanelManager.RecordChanged();
                    });

                if (_data.CurrentBitmap != null)
                {
                    _data.CurrentBitmap.Dispose();
                }


                _data.CurrentBitmap = _data.GetAdjustmentBitmap(_data.CurrentImage.ImagePath);
                ManagerTracker.ImageFileVirtualizationManager.NewImage(_data.CurrentBitmap.Size);

                _data.CurrentOriginalBitmapSource = BitmapInterop.ToBitmapImage(_data.CurrentBitmap);
                _data.CurrentOriginalBitmapSource.Freeze();


                RunOnUIThread(() =>
                {
                    ManagerTracker.RenderManager.RecordSelectioChanged();

                    var cfg = _data.CurrentImage.CurrentSelectionConfiguration.ColorAndBackgroundConfiguration;
                    PanelTracker.ColorPickerDataPanel.checkBoxSoftenEdges.IsChecked = cfg.SoftenEdges;
                    PanelTracker.ColorPickerDataPanel.sliderBackgroundStrength.Value = cfg.BackgroundCorrection;
                    PanelTracker.CurvePanel.SetControlPoints(cfg.ControlPoints);
                    PanelTracker.CurvePanel.comboBoxCurveList.SelectedIndex = -1;

                    UpdateZoom();
                    ManagerTracker.RenderManager.RenderGreenScreenCurrentSettings(false);

                    switch (_data.CurrentTool)
                    {
                        case ToolboxButtons.ColorPickerTool:
                            ManagerTracker.ColorPickerToolManager.UpdateColorPickerDataPanel();
                            break;
                        case ToolboxButtons.BrushTool:
                        case ToolboxButtons.PolygonSelectionTool:
                            ManagerTracker.PolygonSelectionToolManager.UpdatePolygonSelectionTool();
                            break;
                        default:
                            break;
                    }

                    ManagerTracker.UndoRedoManager.Clear();
                    ManagerTracker.UndoRedoManager.PushHistory(new PathGeometry[] { _data.CurrentImage.CurrentSelectionConfiguration.ForegroundClip, _data.CurrentImage.CurrentSelectionConfiguration.RenderedClipNegative });
                    IsImageUpdatingEnabled = true;
                });

                RunOnUIThread(() =>
                {
                    ManagerTracker.MainDisplayManager.SetStatusMessage("Loading Image...Done");
                });

            }
            catch (Exception e)
            {
                ErrorManager.Instance.LogError(e);
            }
        }

        bool IsImageUpdatingEnabled
        {
            set
            {
                ManagerTracker.ImageMatchToolbarManager.Panel.IsEnabled = value;
                ManagerTracker.ImageDisplayManager.IsCapturingMouseEvents = value;
                ManagerTracker.MainDisplayManager.Panel.imageStatus.Visibility = (!value ? Visibility.Visible : Visibility.Hidden);
            }
        }


        private void UpdateZoom()
        {
            ManagerTracker.ZoomToolManager.ResetZoom();
        }

        public static System.Drawing.RectangleF GetDisplayRect(System.Drawing.RectangleF cropRect,
                                                               System.Drawing.Size virtSize)
        {
            return new System.Drawing.RectangleF(
                     (float)(cropRect.X * (double)virtSize.Width / 100),
                     (float)(cropRect.Y * (double)virtSize.Height / 100),
                     (float)cropRect.Width / 100 * virtSize.Width,
                     (float)cropRect.Height / 100 * virtSize.Height
                     );
        }

        public static void UpdateDisplayRectFromCropRect(System.Drawing.RectangleF cropRect,
                                                   System.Windows.Shapes.Rectangle displayRect,
                                                   System.Drawing.Size virtSize
                                                   )
        {

            displayRect.Visibility = (cropRect.IsEmpty ? Visibility.Hidden : Visibility.Visible);


            displayRect.Width = (double)cropRect.Width / 100 * virtSize.Width;
            displayRect.Height = (double)cropRect.Height / 100 * virtSize.Height;
            displayRect.Margin = new Thickness(cropRect.X * (double)virtSize.Width / 100,
                                               cropRect.Y * (double)virtSize.Height / 100, 0, 0);

            displayRect.Width += displayRect.StrokeThickness;
            displayRect.Height += displayRect.StrokeThickness;
            displayRect.Margin = new Thickness(displayRect.Margin.Left - (displayRect.StrokeThickness / 2),
                                               displayRect.Margin.Top - (displayRect.StrokeThickness / 2), 0, 0);


        }

    }
}
