﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Flow.Designer.Model;

namespace Flow.Designer.Tests
{
    /// <summary>
    /// Summary description for SuspendableNotifyPropertyChangedTests
    /// </summary>
    [TestClass]
    public class SuspendableNotifyPropertyChangedTests
    {
        public SuspendableNotifyPropertyChangedTests()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        class MyPropClass : SuspendableNotifyPropertyChanged
        {
            public void ThrowPropChanged(string propname)
            {
                OnPropertyChanged(propname);
            }
        }

        [TestMethod]
        public void SuspendableNotifyPropertyChanged_Initially_Throws_Events()
        {
            MyPropClass pc = new MyPropClass();
            pc.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(pc_PropertyChanged);
            string propname = "MyProperty";
            pc_PropertyChanged_count = 0;
            pc.ThrowPropChanged(propname);
            Assert.AreEqual(1, pc_PropertyChanged_count);
        }

        [TestMethod]
        public void SuspendableNotifyPropertyChanged_Suspend_Blocks_Notify()
        {
            MyPropClass pc = new MyPropClass();
            pc.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(pc_PropertyChanged);
            string propname = "MyProperty";
            pc_PropertyChanged_count = 0;
            pc.SuspendPropertyNotifying(propname);
            pc.ThrowPropChanged(propname);
            Assert.AreEqual(0, pc_PropertyChanged_count);
        }

        [TestMethod]
        public void SuspendableNotifyPropertyChanged_Resume_Throws_Event_And_Stops_Block()
        {
            MyPropClass pc = new MyPropClass();
            pc.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(pc_PropertyChanged);
            string propname = "MyProperty";
            pc_PropertyChanged_count = 0;
            pc.SuspendPropertyNotifying(propname);
            pc.ResumePropertyNotifying(propname, true);
            Assert.AreEqual(1, pc_PropertyChanged_count);
            pc.ThrowPropChanged(propname);
            Assert.AreEqual(2, pc_PropertyChanged_count);
        }

        int pc_PropertyChanged_count = 0;
        void pc_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            pc_PropertyChanged_count++;
        }
    }
}
