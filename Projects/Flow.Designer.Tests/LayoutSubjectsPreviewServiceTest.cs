﻿//using System;
//using System.Text;
//using System.Collections.Generic;
//using System.Linq;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using Flow.Designer.Lib.ProjectInterop;
//using StructureMap;
//using StructureMap.Attributes;

//namespace Flow.Designer.Tests
//{
//    /// <summary>
//    /// Summary description for LayoutSubjectsPreviewServiceTest
//    /// </summary>
//    [TestClass]
//    public class LayoutSubjectsPreviewServiceTest
//    {
//        public LayoutSubjectsPreviewServiceTest()
//        {
//            StructureMapConfiguration
//                .ForRequestedType<ISubjectsRepository>()
//                .TheDefaultIsConcreteType<TestSubjectsRepository>()
//                .CacheBy(InstanceScope.Singleton);
//        }

//        private TestContext testContextInstance;

//        /// <summary>
//        ///Gets or sets the test context which provides
//        ///information about and functionality for the current test run.
//        ///</summary>
//        public TestContext TestContext
//        {
//            get
//            {
//                return testContextInstance;
//            }
//            set
//            {
//                testContextInstance = value;
//            }
//        }

//        #region Additional test attributes
//        //
//        // You can use the following additional attributes as you write your tests:
//        //
//        // Use ClassInitialize to run code before running the first test in the class
//        // [ClassInitialize()]
//        // public static void MyClassInitialize(TestContext testContext) { }
//        //
//        // Use ClassCleanup to run code after all tests in a class have run
//        // [ClassCleanup()]
//        // public static void MyClassCleanup() { }
//        //
//        // Use TestInitialize to run code before running each test 
//        // [TestInitialize()]
//        // public void MyTestInitialize() { }
//        //
//        // Use TestCleanup to run code after each test has run
//        // [TestCleanup()]
//        // public void MyTestCleanup() { }
//        //
//        #endregion

//        [TestMethod]
//        public void LayoutSubjectsPreviewService_ChangeNdx_Throws_SubjectChanged()
//        {
//            LayoutSubjectsPreviewService service = new LayoutSubjectsPreviewService();
//            service.SubjectChanged += new EventHandler(service_SubjectChanged);
//            service_SubjectChanged_count = 0;
//            service.MoveNext();
//            Assert.AreEqual(1, service_SubjectChanged_count);
//        }

//        int service_SubjectChanged_count = 0;
//        void service_SubjectChanged(object sender, EventArgs e)
//        {
//            service_SubjectChanged_count++;
//        }
//    }
//}
