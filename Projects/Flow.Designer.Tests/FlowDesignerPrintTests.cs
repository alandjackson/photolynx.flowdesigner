﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Flow.Designer.Model.Print;
using Flow.Lib.Persister;
using System.Diagnostics;
using System.IO;
using Flow.Designer.Controller;
using System.Data;

namespace Flow.Designer.Tests
{
    [TestClass]
    public class FlowDesignerPrintTests
    {
        [TestMethod]
        public void FlowRenderInfo_CanRenderSubjectImage()
        {
            Program.InitializeTypes();
            var db = StructureMap.ObjectFactory.GetInstance<FlowDesignerDataContext>();
            db.LayoutPreferences.LayoutsDirectory = @"C:\ProgramData\PhotoLynx\LayoutDesigner\Layouts";

            var ds = new DataSet();
            ds.Tables.Add(new DataTable());
            ds.Tables[0].Columns.Add("First Name");
            ds.Tables[0].Columns.Add("Last Name");
            ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
            ds.Tables[0].Rows[0]["First Name"] = "Alan";
            ds.Tables[0].Rows[0]["Last Name"] = "Jackson";

            var renderInfo = new FlowRenderInfo()
            {
                Dpi = 300,
                FlowLayoutFilename = @"subject-image.lyt",
                OutputFilename = @"C:\ProgramData\PhotoLynx\LayoutDesigner\Output\FlowRenderInfo_CanRenderSubjectImage.png",
                SubjectImageFilename = @"Z:\Documents\PhotoLynx\Archive\2016-05-13 RL Flow Layout\PrintInfo\temp-image.jpg",
                DataSet = ds

            };
            if (File.Exists(renderInfo.OutputFilename)) File.Delete(renderInfo.OutputFilename);

            renderInfo.RenderToImage();
            Assert.IsTrue(File.Exists(renderInfo.OutputFilename), "Output file not found: " + renderInfo.SubjectImageFilename);
        }

        [TestMethod]
        public void FlowRenderInfo_CanRenderFromLayoutDirectory()
        {
            Program.InitializeTypes();

            var db = StructureMap.ObjectFactory.GetInstance<FlowDesignerDataContext>();
            db.LayoutPreferences.LayoutsDirectory = @"Z:\Documents\PhotoLynx\Archive\2016-05-13 RL Flow Layout\Layouts\";

            var renderInfo = new FlowRenderInfo()
            {
                Dpi = 300,
                FlowLayoutFilename = @"--layout.lyt",
                OutputFilename = @"Z:\Documents\PhotoLynx\Archive\2016-05-13 RL Flow Layout\PrintInfo\temp-output.png",
                SubjectImageFilename = @"Z:\Documents\PhotoLynx\Archive\2016-05-13 RL Flow Layout\PrintInfo\temp-image"

            };
            if (File.Exists(renderInfo.OutputFilename)) File.Delete(renderInfo.OutputFilename);

            renderInfo.RenderToImage();
            Assert.IsTrue(File.Exists(renderInfo.OutputFilename), "Output file not found: " + renderInfo.SubjectImageFilename);
        }

        [TestMethod]
        public void FlowRenderInfo_CanRender()
        {
            Program.InitializeTypes();
            var renderInfo = new FlowRenderInfo()
            {
                Dpi = 300,
                FlowLayoutFilename = @"Z:\Documents\PhotoLynx\Archive\2016-05-13 RL Flow Layout\Layouts\--layout.lyt",
                OutputFilename = @"Z:\Documents\PhotoLynx\Archive\2016-05-13 RL Flow Layout\PrintInfo\temp-output.png",
                SubjectImageFilename = @"Z:\Documents\PhotoLynx\Archive\2016-05-13 RL Flow Layout\PrintInfo\temp-image"

            };
            if (File.Exists(renderInfo.OutputFilename)) File.Delete(renderInfo.OutputFilename);

            renderInfo.RenderToImage();
            Assert.IsTrue(File.Exists(renderInfo.OutputFilename), "Output file not found: " + renderInfo.SubjectImageFilename);
        }

        [TestMethod]
        public void Program_CanPrint()
        {
            var inputFilename = @"Z:\Documents\PhotoLynx\Active\2016-05-13 RL Flow Layout\PrintInfo\temp-layout-data.txt";
            var outputFilename = @"Z:\Documents\PhotoLynx\Active\2016-05-13 RL Flow Layout\PrintInfo\temp-output.jpg";

            Program.Main(new string[] { "PRINT", inputFilename, outputFilename });
        }

        [TestMethod]
        public void Program_CanPrint2()
        {
            //var inputFilename = @"Z:\Documents\PhotoLynx\Active\2016-05-13 RL Flow Layout\PrintInfo\temp-layout-data.txt";
            var inputFilename = @"Z:\Downloads\tmp20A6.tmp";
            var outputFilename = @"Z:\Documents\PhotoLynx\Active\2016-05-13 RL Flow Layout\PrintInfo\temp-output.jpg";
            Flow.Designer.Program.Main(new string[] { "PRINT", inputFilename, outputFilename });
        }

        [TestMethod]
        public void Program_CanDeserialize()
        {
            var inputFilename = @"Z:\Downloads\tmp20A6.tmp";
            var renderInfo = ObjectSerializer.FromXmlFile<FlowRenderInfo>(inputFilename);
        }

        //[TestMethod]
        //public void Program_CanRunExe()
        //{
        //    int MaxRetry = 1;
        //    string exeFullPath = @"C:\Users\user\Source\flow-designer\Projects\Flow.Designer\bin\Debug\Flow.Designer.exe";
        //    string renderDataFilename = @"z:\Downloads\tmp20A6.tmp";
        //    //string renderDataFilename = System.IO.Path.GetTempFileName();
        //    //ObjectSerializer.SerializeFile(renderDataFilename, this);

        //    string stdout = "FIRST_RUN";
        //    int failCount = 0;
        //    while (!string.IsNullOrEmpty(stdout) && failCount < MaxRetry)
        //    {
        //        RunProcess(exeFullPath, "PRINT \"" + renderDataFilename + "\"", true, ref stdout);
        //        failCount++;
        //    }

        //    // stdout is assumed to be an error
        //    if (!string.IsNullOrEmpty(stdout))
        //        throw new ApplicationException(stdout);

        //    System.IO.File.Delete(renderDataFilename);
        //}

        //public static int RunProcess(string path, string args, bool wait, ref string stdOutput)
        //{
        //    Trace.WriteLine("Running process, path: " + path + ", args: " + args + ", wait: " + wait);
        //    ProcessStartInfo psi = new ProcessStartInfo(path, args);
        //    psi.CreateNoWindow = true;
        //    psi.UseShellExecute = false;
        //    if (stdOutput != null) { psi.RedirectStandardOutput = true; }
        //    Process p = Process.Start(psi);
        //    if (stdOutput != null) { stdOutput = p.StandardOutput.ReadToEnd(); }
        //    if (wait)
        //    {
        //        p.WaitForExit();
        //        return (p.ExitCode);
        //    }
        //    return (0);
        //}

    }
}
