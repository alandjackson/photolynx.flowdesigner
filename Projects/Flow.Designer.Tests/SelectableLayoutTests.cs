﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Flow.Designer.Model;
using System.Collections.ObjectModel;

namespace Flow.Designer.Tests
{
    /// <summary>
    /// Summary description for SelectableLayoutTests
    /// </summary>
    [TestClass]
    public class SelectableLayoutTests
    {
        SelectableLayout sl;

        public SelectableLayoutTests()
        {
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
         [TestInitialize()]
         public void MyTestInitialize() 
         {
             sl = new SelectableLayout();
             sl.Layout.LayoutItems.Add(new LayoutImage());
             sl.Layout.LayoutItems.Add(new LayoutText());
             sl.Layout.LayoutItems.Add(new LayoutText());
             sl.Layout.LayoutItems.Add(new LayoutText());
         }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void SelectableLayout_MoveSelectedToBack_Moves_One_Item_To_Back()
        {
            ILayoutItem li = sl.Layout.LayoutItems[0];
            sl.SelectedItem = li;
            sl.MoveSelectedToBack();
            Assert.AreEqual(li, sl.Layout.LayoutItems[sl.Layout.LayoutItems.Count - 1]);
        }

        [TestMethod]
        public void SelectableLayout_MoveSelectedToBack_Moves_Two_Item_To_Back()
        {
            ILayoutItem li1 = sl.Layout.LayoutItems[0];
            ILayoutItem li2 = sl.Layout.LayoutItems[2];
            sl.SelectedItems = new ObservableCollection<ILayoutItem>(new List<ILayoutItem>(new ILayoutItem[] { li1, li2 }));
            sl.MoveSelectedToBack();
            Assert.AreEqual(li1, sl.Layout.LayoutItems[sl.Layout.LayoutItems.Count - 2]);
            Assert.AreEqual(li2, sl.Layout.LayoutItems[sl.Layout.LayoutItems.Count - 1]);
        }

        [TestMethod]
        public void SelectableLayout_MoveSelectedToFront_Moves_One_Item_To_Front()
        {
            ILayoutItem li = sl.Layout.LayoutItems[3];
            sl.SelectedItem = li;
            sl.MoveSelectedToFront();
            Assert.AreEqual(li, sl.Layout.LayoutItems[0]);
        }

        [TestMethod]
        public void SelectableLayout_MoveSelectedToFront_Moves_Two_Item_To_Front_In_Order()
        {
            ILayoutItem li1 = sl.Layout.LayoutItems[2];
            ILayoutItem li2 = sl.Layout.LayoutItems[3];
            sl.SelectedItems = new ObservableCollection<ILayoutItem>(new List<ILayoutItem>(new ILayoutItem[] { li1, li2 }));
            sl.MoveSelectedToFront();
            Assert.AreEqual(li1, sl.Layout.LayoutItems[0]);
            Assert.AreEqual(li2, sl.Layout.LayoutItems[1]);
        }

        [TestMethod]
        public void SelectableLayout_MoveSelectedToFront_Maintains_Selection()
        {
            ILayoutItem li = sl.Layout.LayoutItems[3];
            sl.SelectedItem = li;
            sl.MoveSelectedToFront();
            Assert.AreEqual(li, sl.SelectedItem);
        }

    }
}
