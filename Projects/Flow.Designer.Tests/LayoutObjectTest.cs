﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Flow.Designer.Model;
using System.Reflection;
using Flow.Designer.Lib.Service;
using StructureMap;
using StructureMap.Attributes;
using Flow.Designer.Lib.ProjectRepository;

namespace Flow.Designer.Tests
{
    /// <summary>
    /// Summary description for LayoutObjectTest
    /// </summary>
    [TestClass]
    public class LayoutObjectTest
    {
        public LayoutObjectTest()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        [TestInitialize()]
        public void MyTestInitialize() 
        {
            //StructureMapConfiguration
            //    .ForRequestedType<ILayoutSubjectService>()
            //    .TheDefaultIsConcreteType<NullLayoutSubjectService>()
            //    .CacheBy(InstanceScope.Singleton);
        }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void LayoutObject_copy_returns_copy_not_self()
        {
            ILayoutItem ale1 = new LayoutImage();
            ILayoutItem ale2 = ale1.Copy();

            ale1.ItemName = "Changed";
            Assert.AreNotEqual(ale1.ItemName, ale2.ItemName);
        }

        public class NullLayoutSubjectService : ILayoutSubjectService
        {
            #region ILayoutSubjectService Members

            public event EventHandler SubjectChanged;
            protected void OnSubjectChanged() { if (SubjectChanged != null) SubjectChanged(this, EventArgs.Empty); }

            public ICurrentSubjectRepository SubjectRepository
            {
                get
                {
                    throw new NotImplementedException();
                }
                set
                {
                    throw new NotImplementedException();
                }
            }

            public Flow.Designer.Model.Project.Subject GetSubject()
            {
                throw new NotImplementedException();
            }

            #endregion
        }

        [TestMethod]
        public void Layout_copy_does_not_throw_exception()
        {
            Layout l = (Layout) new Layout().Copy();
        }

        [TestMethod]
        public void LayoutImage_copied_object_properties_match()
        {

            LayoutImage li = new LayoutImage();
            li.BlurEnabled = true;
            li.DropShadowEnabled = true;
            li.Location = new System.Windows.Rect(2, 2, 2, 2);

            LayoutImage li2 = (LayoutImage) li.Copy();
            LayoutObject_copied_object_properties_match(li, li2);
        }

        [TestMethod]
        public void LayoutText_copied_object_properties_match()
        {
            //StructureMapConfiguration
            //    .ForRequestedType<ILayoutSubjectService>()
            //    .TheDefaultIsConcreteType<NullLayoutSubjectService>()
            //    .CacheBy(InstanceScope.Singleton);

            LayoutText li = new LayoutText();
            li.Text = "asdf";
            li.Location = new System.Windows.Rect(2, 2, 2, 2);

            LayoutText li2 = (LayoutText)li.Copy();
            LayoutObject_copied_object_properties_match(li, li2);
        }

        protected void LayoutObject_copied_object_properties_match(
            ILayoutItem ale1, ILayoutItem ale2)
        {
            foreach (PropertyInfo pi in 
                typeof(AbstractLayoutElement).GetProperties())
            {
                // ignore services
                if (pi.PropertyType.FullName.StartsWith("Flow.Designer.Lib.Service")) continue;

                object v1 = pi.GetValue(ale1, null);
                object v2 = pi.GetValue(ale2, null);
                Assert.AreEqual(v1, v2);
            }

        }

        [TestMethod]
        public void Translate_moves_x_and_y()
        {
            LayoutImage li1 = new LayoutImage();
            li1.Location = new System.Windows.Rect(0, 0, 0, 0);
            LayoutImage li2 = (LayoutImage) li1.Translate(5, 5);
            Assert.AreEqual(0, li1.Location.X);
            Assert.AreEqual(0, li1.Location.Y);
            Assert.AreEqual(5, li2.Location.X);
            Assert.AreEqual(5, li2.Location.Y);
        }
    }
}
