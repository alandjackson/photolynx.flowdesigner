﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Flow.Designer.View;
using Flow.Designer.Model.Designer;
using Flow.Designer.Model;

namespace Flow.Designer.Tests
{
    [TestClass]
    public class SurfaceTests
    {
        [TestMethod]
        public void Surface_bind_model()
        {
            var limage = new LayoutImage();
            limage.ImageFilename = @"c:\users\ajackson\harbinger2k911280.jpg";
            limage.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(limage_PropertyChanged);

            var model = new DesignerModel();
            model.SelectableLayout.Layout.LayoutItems.Add(limage);

            var surface = new Surface();
            surface.Model = model;
            Console.WriteLine("Render Image Property Count: " + renderImageCount);

            surface.Model = model;
            Console.WriteLine("Render Image Property Count: " + renderImageCount);

            model.SelectableLayout.Layout = model.SelectableLayout.Layout;
            Console.WriteLine("Render Image Property Count: " + renderImageCount);
        }

        protected int renderImageCount = 0;
        void limage_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "RenderImageFilename")
                renderImageCount++;
        }

    }
}
