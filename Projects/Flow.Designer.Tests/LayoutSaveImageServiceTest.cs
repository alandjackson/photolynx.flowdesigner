﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Flow.Designer.Lib.Service;

namespace Flow.Designer.Tests
{
    /// <summary>
    /// Summary description for LayoutSaveImageServiceTest
    /// </summary>
    [TestClass]
    public class LayoutSaveImageServiceTest
    {
        public LayoutSaveImageServiceTest()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void LayoutSaveImageService_BuildOutputFilename_builds_correct_filename()
        {
            LayoutSaveImageService service = new LayoutSaveImageService();
            Assert.AreEqual(@"c:\img0001.jpg", service.BuildOutputFilename(
                "c:\\", "img", 1, 4, ".jpg"));
            //
            // TODO: Add test logic	here
            //
        }
    }
}
