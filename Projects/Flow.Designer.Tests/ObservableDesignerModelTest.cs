﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Flow.Designer.Model.Designer;
using Flow.Designer.Model;

namespace Flow.Designer.Tests
{
    /// <summary>
    /// Summary description for ObservableDesignerModelTest
    /// </summary>
    [TestClass]
    public class ObservableDesignerModelTest
    {
        public ObservableDesignerModelTest()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void ObservableDesignerModel_Add_Layout_Item_Throws_LayoutChanged()
        {
            ObservableDesignerModel omodel = new ObservableDesignerModel(new DesignerModel());
            omodel.LayoutChanged += new EventHandler<Flow.Lib.Helpers.EventArgs<Flow.Designer.Model.Layout>>(omodel_LayoutChanged);
            omodel_LayoutChanged_count = 0;
            omodel.Layout = new Flow.Designer.Model.Layout();
            Assert.AreEqual(1, omodel_LayoutChanged_count);
            Assert.AreEqual(omodel.SelectableLayout.Layout, omodel.Layout);
            omodel.Layout.LayoutItems.Add(new LayoutText());
            Assert.AreEqual(2, omodel_LayoutChanged_count);
        }

        int omodel_LayoutChanged_count = 0;
        void omodel_LayoutChanged(object sender, Flow.Lib.Helpers.EventArgs<Flow.Designer.Model.Layout> e)
        {
            omodel_LayoutChanged_count++;
        }

        [TestMethod]
        public void ObservableDesignerModel_Set_SelectableLayout_Sets_Model_SelectableLayout()
        {
            ObservableDesignerModel omodel = new ObservableDesignerModel(new DesignerModel());
            omodel.SelectableLayout = new SelectableLayout();
            Assert.AreEqual(omodel.Model.SelectableLayout, omodel.SelectableLayout);
        }

        [TestMethod]
        public void ObservableDesignerModel_Set_Selectable_Layout_Sets_Layout()
        {
            ObservableDesignerModel omodel = new ObservableDesignerModel(new DesignerModel());
            omodel.SelectableLayout.Layout = new Layout();
            Assert.AreEqual(omodel.SelectableLayout.Layout, omodel.Layout);
        }


        [TestMethod]
        public void ObservableDesignerModel_Set_Selectable_Layout_Sets_Layout_Property_Notifier()
        {
            ObservableDesignerModel omodel = new ObservableDesignerModel(new DesignerModel());
            omodel.LayoutChanged += new EventHandler<Flow.Lib.Helpers.EventArgs<Layout>>(omodel_LayoutChanged);
            omodel.SelectableLayout.Layout = new Layout();
            omodel_LayoutChanged_count = 0;
            omodel.SelectableLayout.Layout.LayoutItems.Add(new LayoutText());
            Assert.AreEqual(1, omodel_LayoutChanged_count);
        }
    }
}
