﻿//using System;
//using System.Text;
//using System.Collections.Generic;
//using System.Linq;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using Flow.Designer.Model;
//using System.IO;
//using Flow.Designer.Lib.Service;
//using System.Windows.Media;
//using System.Windows.Controls;
//using System.Windows.Media.Imaging;
//using System.Windows;
//using Flow.Config;
//using System.ComponentModel;
//using System.Threading;
//using Flow.Designer.Model.Settings;

//namespace Flow.Designer.Tests
//{
//    /// <summary>
//    /// Summary description for LayoutRenderTest
//    /// </summary>
//    [TestClass]
//    public class LayoutRenderTest
//    {
//        public LayoutRenderTest()
//        {
//            //
//            // TODO: Add constructor logic here
//            //
//        }

//        private TestContext testContextInstance;

//        /// <summary>
//        ///Gets or sets the test context which provides
//        ///information about and functionality for the current test run.
//        ///</summary>
//        public TestContext TestContext
//        {
//            get
//            {
//                return testContextInstance;
//            }
//            set
//            {
//                testContextInstance = value;
//            }
//        }

//        #region Additional test attributes
//        //
//        // You can use the following additional attributes as you write your tests:
//        //
//        // Use ClassInitialize to run code before running the first test in the class
//        // [ClassInitialize()]
//        // public static void MyClassInitialize(TestContext testContext) { }
//        //
//        // Use ClassCleanup to run code after all tests in a class have run
//        // [ClassCleanup()]
//        // public static void MyClassCleanup() { }
//        //
//        // Use TestInitialize to run code before running each test 
//        // [TestInitialize()]
//        // public void MyTestInitialize() { }
//        //
//        // Use TestCleanup to run code after each test has run
//        // [TestCleanup()]
//        // public void MyTestCleanup() { }
//        //
//        #endregion

//        //[TestMethod]
//        public void GenerateImageFile_outputs_image_from_image()
//        {
//            Assert.Inconclusive();
//            BitmapImage bi = new BitmapImage();
//            bi.BeginInit();
//            bi.UriSource = new Uri(@"C:\Users\Public\Pictures\Sample Pictures\Autumn Leaves.jpg");
//            bi.EndInit();
//            bi.Freeze();

//            Image i = new Image();
//            i.Width = 500;
//            i.Height = 500;
//            i.Source = bi;
//            i.Measure(new Size(i.Width, i.Height));
//            i.Arrange(new Rect(0, 0, i.Width, i.Height));
//            i.GenerateImageFile(@"c:\img.jpg");
            
//        }

//        public class MockPrintSettingsRepository : IPrintSettingsRepository
//        {
//            #region IPrintSettingsRepository Members

//            public PrintSettings Load(Flow.Schema.LinqModel.DataContext.FlowMasterDataContext fmdc)
//            {
//                return new PrintSettings();
//            }

//            public PrintSettings Load()
//            {
//                return new PrintSettings();
//            }

//            public void Save(PrintSettings ps)
//            {
//            }

//            #endregion
//        }

//        [TestMethod]
//        public void GenerateImageFile_outputs_image_file()
//        {
//            Bootstrapper.ConfigureStructureMap();
//            StructureMap.ObjectFactory.Inject<IPrintSettingsRepository>
//                (new MockPrintSettingsRepository());

//            string filename = Path.Combine(
//                AppDomain.CurrentDomain.BaseDirectory, "img.jpg");
//            if (File.Exists(filename))
//                File.Delete(filename);

//            Layout l = new Layout();
//            l.LayoutWidth = 5;
//            l.LayoutHeight = 5;
//            l.BackgroundColor = Colors.Yellow;

//            LayoutImage li = new LayoutImage();
//            li.IsSepia = true;
//            li.Location = new System.Windows.Rect(0, 0, 200, 200);
//            li.ImageFilename = @"Chrysanthemum.jpg";
//            l.LayoutItems.Add(li);

//            LayoutText lt = new LayoutText();
//            lt.Text = "This is some text";
//            lt.FontFamilyName = "Arial";
//            lt.FontSize = 40;
//            lt.Location = new Rect(200, 200, 200, 200);
//            l.LayoutItems.Add(lt);

//            l.GenerateImageFile(96, filename);
//        }


//        [TestMethod]
//        public void GenerateImageFile_outputs_image_file_with_sepia()
//        {
//            Bootstrapper.ConfigureStructureMap();
//            StructureMap.ObjectFactory.Inject<IPrintSettingsRepository>
//                (new MockPrintSettingsRepository());

//            string filename = Path.Combine(
//                AppDomain.CurrentDomain.BaseDirectory, "img.jpg");
//            if (File.Exists(filename))
//                File.Delete(filename);

//            Layout l = new Layout();
//            l.LayoutWidth = 5;
//            l.LayoutHeight = 5;
//            l.BackgroundColor = Colors.Yellow;

//            LayoutImage li = new LayoutImage();
//            li.IsSepia = true;
//            li.Location = new System.Windows.Rect(0, 0, 200, 200);
//            li.ImageFilename = @"Chrysanthemum.jpg";
//            l.LayoutItems.Add(li);

//            LayoutText lt = new LayoutText();
//            lt.Text = "This is some text";
//            lt.FontFamilyName = "Arial";
//            lt.FontSize = 40;
//            lt.Location = new Rect(200, 200, 200, 200);
//            l.LayoutItems.Add(lt);
//            ThreadSafeLayout tsl = new ThreadSafeLayout(l);

//            Exception err = null;
//            Thread t = new Thread(new ThreadStart(() =>
//                {
//                    try
//                    {
//                        Layout lw = tsl.ToLayout();
//                        lw.GenerateImageFile(96, filename);
//                    }
//                    catch (Exception e)
//                    {
//                        err = e;
//                    }
//                }));
//            t.SetApartmentState(ApartmentState.STA);
//            t.Start();
//            t.Join();

//            if (err != null)
//                throw err;

//        }

//    }
//}
