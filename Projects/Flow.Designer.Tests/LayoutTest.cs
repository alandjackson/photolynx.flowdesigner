﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Flow.Designer.Model;
using System.ComponentModel;

namespace Flow.Designer.Tests
{
    /// <summary>
    /// Summary description for LayoutTest
    /// </summary>
    [TestClass]
    public class LayoutTest
    {
        public LayoutTest()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void Change_child_property_throws_changed_event()
        {

            LayoutImage li = new LayoutImage();

            Layout l = new Layout();
            l.LayoutItems.Add(li);
            l.LayoutItemPropertyChaged += new EventHandler(l_PropertyChanged);

            l_PropertyChangedCount = 0;
            Assert.AreEqual(0, l_PropertyChangedCount);
            li.ImageFilename = "asdf";
            Assert.IsTrue(l_PropertyChangedCount > 0);
        }

        protected int l_PropertyChangedCount = 0;
        void l_PropertyChanged(object sender, EventArgs e)
        {
            l_PropertyChangedCount++;
        }
    }
}
