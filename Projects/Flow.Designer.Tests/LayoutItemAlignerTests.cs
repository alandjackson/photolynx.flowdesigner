﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Flow.Designer.Model;
using Flow.Designer.Lib.SurfaceUi;

namespace Flow.Designer.Tests
{
    /// <summary>
    /// Summary description for LayoutItemAlignerTests
    /// </summary>
    [TestClass]
    public class LayoutItemAlignerTests
    {
        public LayoutItemAlignerTests()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void LayoutItemAligner_AlignLefts_Makes_Y_Positions_The_Same()
        {
            ILayoutItem[] items = new ILayoutItem[] {
                new LayoutText(),new LayoutText(),new LayoutText()};
            items[0].Location = new System.Windows.Rect(5, 5, 5, 5);
            items[1].Location = new System.Windows.Rect(6, 6, 6, 6);
            items[2].Location = new System.Windows.Rect(7, 7, 7, 7);
            new LayoutItemAligner().AlignLefts(items);

            Assert.AreEqual(items[0].Location.X, items[1].Location.X);
            Assert.AreEqual(items[2].Location.X, items[1].Location.X);
            Assert.AreEqual(7, items[0].Location.X);
            Assert.AreEqual(new System.Windows.Rect(7, 5, 5, 5), items[0].Location);
        }

        [TestMethod]
        public void LayoutItemAligner_AlignRights_Makes_Right_Edges_The_Same()
        {
            ILayoutItem[] items = new ILayoutItem[] {
                new LayoutText(),new LayoutText(),new LayoutText()};
            items[0].Location = new System.Windows.Rect(2, 2, 2, 2);
            items[1].Location = new System.Windows.Rect(4, 4, 4, 4);
            items[2].Location = new System.Windows.Rect(8, 8, 8, 8);
            new LayoutItemAligner().AlignRights(items);

            Assert.AreEqual(new System.Windows.Rect(14, 2, 2, 2), items[0].Location);
            Assert.AreEqual(new System.Windows.Rect(12, 4, 4, 4), items[1].Location);
            Assert.AreEqual(new System.Windows.Rect(8, 8, 8, 8), items[2].Location);
        }

        [TestMethod]
        public void LayoutItemAligner_AlignHMiddles_Makes_Middles_The_Same()
        {
            ILayoutItem[] items = new ILayoutItem[] {
                new LayoutText(),new LayoutText(),new LayoutText()};
            items[0].Location = new System.Windows.Rect(2, 2, 2, 2);
            items[1].Location = new System.Windows.Rect(4, 4, 4, 4);
            items[2].Location = new System.Windows.Rect(8, 8, 8, 8);
            new LayoutItemAligner().AlignHMiddles(items);

            Assert.AreEqual(new System.Windows.Rect(11, 2, 2, 2), items[0].Location);
            Assert.AreEqual(new System.Windows.Rect(10, 4, 4, 4), items[1].Location);
            Assert.AreEqual(new System.Windows.Rect(8, 8, 8, 8), items[2].Location);
        }

        [TestMethod]
        public void LayoutItemAligner_AlignTops_Makes_Top_The_Same()
        {
            ILayoutItem[] items = new ILayoutItem[] {
                new LayoutText(),new LayoutText(),new LayoutText()};
            items[0].Location = new System.Windows.Rect(2, 2, 2, 2);
            items[1].Location = new System.Windows.Rect(4, 4, 4, 4);
            items[2].Location = new System.Windows.Rect(8, 8, 8, 8);
            new LayoutItemAligner().AlignTops(items);

            Assert.AreEqual(new System.Windows.Rect(2, 8, 2, 2), items[0].Location);
            Assert.AreEqual(new System.Windows.Rect(4, 8, 4, 4), items[1].Location);
            Assert.AreEqual(new System.Windows.Rect(8, 8, 8, 8), items[2].Location);
        }

        [TestMethod]
        public void LayoutItemAligner_AlignBottoms_Makes_Bottoms_The_Same()
        {
            ILayoutItem[] items = new ILayoutItem[] {
                new LayoutText(),new LayoutText(),new LayoutText()};
            items[0].Location = new System.Windows.Rect(2, 2, 2, 2);
            items[1].Location = new System.Windows.Rect(4, 4, 4, 4);
            items[2].Location = new System.Windows.Rect(8, 8, 8, 8);
            new LayoutItemAligner().AlignBottoms(items);

            Assert.AreEqual(new System.Windows.Rect(2, 14, 2, 2), items[0].Location);
            Assert.AreEqual(new System.Windows.Rect(4, 12, 4, 4), items[1].Location);
            Assert.AreEqual(new System.Windows.Rect(8, 8, 8, 8), items[2].Location);
        }

        [TestMethod]
        public void LayoutItemAligner_AlignVMiddles_Makes_Vertical_Middles_The_Same()
        {
            ILayoutItem[] items = new ILayoutItem[] {
                new LayoutText(),new LayoutText(),new LayoutText()};
            items[0].Location = new System.Windows.Rect(2, 2, 2, 2);
            items[1].Location = new System.Windows.Rect(4, 4, 4, 4);
            items[2].Location = new System.Windows.Rect(8, 8, 8, 8);
            new LayoutItemAligner().AlignVMiddles(items);

            Assert.AreEqual(new System.Windows.Rect(2, 11, 2, 2), items[0].Location);
            Assert.AreEqual(new System.Windows.Rect(4, 10, 4, 4), items[1].Location);
            Assert.AreEqual(new System.Windows.Rect(8, 8, 8, 8), items[2].Location);
        }

    }
}
