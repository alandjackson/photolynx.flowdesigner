﻿//using System;
//using System.Text;
//using System.Collections.Generic;
//using System.Linq;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using Flow.Designer.Model;
//using Flow.Designer.Lib.Service;
//using System.Threading;
//using System.Windows.Threading;
//using Flow.Lib.Persister;
//using Flow.Designer.Model.Settings;
//using StructureMap;

//namespace Flow.Designer.Tests.TestLayout
//{
//    /// <summary>
//    /// Summary description for AnsycToCanvasTest
//    /// </summary>
//    [TestClass]
//    public class AnsycToCanvasTest
//    {
//        public AnsycToCanvasTest()
//        {
//            //
//            // TODO: Add constructor logic here
//            //
//        }

//        private TestContext testContextInstance;

//        /// <summary>
//        ///Gets or sets the test context which provides
//        ///information about and functionality for the current test run.
//        ///</summary>
//        public TestContext TestContext
//        {
//            get
//            {
//                return testContextInstance;
//            }
//            set
//            {
//                testContextInstance = value;
//            }
//        }

//        #region Additional test attributes
//        //
//        // You can use the following additional attributes as you write your tests:
//        //
//        // Use ClassInitialize to run code before running the first test in the class
//         [ClassInitialize()]
//         public static void MyClassInitialize(TestContext testContext) 
//         {
//             ObjectFactory.Inject<IPrintSettingsRepository>(new MockPrintSettingsRepository());
//             //DesignerRegistry
//             //.ForRequestedType<IPrintSettingsRepository>()
//             //   .TheDefaultIsConcreteType<PrintSettingsRepository>();

//         }
//        //
//        // Use ClassCleanup to run code after all tests in a class have run
//        // [ClassCleanup()]
//        // public static void MyClassCleanup() { }
//        //
//        // Use TestInitialize to run code before running each test 
//        // [TestInitialize()]
//        // public void MyTestInitialize() { }
//        //
//        // Use TestCleanup to run code after each test has run
//        // [TestCleanup()]
//        // public void MyTestCleanup() { }
//        //
//        #endregion

//        public class MockPrintSettingsRepository : IPrintSettingsRepository
//        {
//            #region IPrintSettingsRepository Members

//            public PrintSettings Load(Flow.Schema.LinqModel.DataContext.FlowMasterDataContext fmdc)
//            {
//                return Load();
//            }

//            public PrintSettings Load()
//            {
//                return new PrintSettings()
//                {
//                     ImageDPI = 96, 
//                     ImageFormat = "Jpg", 
//                     ImageOutputFolderPath = "Images", 
//                     PrintDestination = PrintSettings.PrintDestiations.Image, 
//                     UseSubjectImageNameInFileName = false
//                };
//            }

//            public void Save(PrintSettings ps)
//            {
//                throw new NotImplementedException();
//            }

//            #endregion
//        }

//        [TestMethod]
//        public void GenerateImageFile_works_from_thread()
//        {

//            Layout l = CreateTestLayout();
//            ThreadSafeLayout tsl = new ThreadSafeLayout(l);

//            Exception exception = null;
//            Thread t = new Thread(new ThreadStart(() =>
//            {
//                try
//                {
//                    tsl.ToLayout().GenerateImageFile(96, "img.jpg");
//                }
//                catch (Exception err)
//                {
//                    exception = err;
//                }
//            }));
//            t.SetApartmentState(ApartmentState.STA);
//            t.Start();
//            t.Join();

//            if (exception != null)
//                throw exception;
//        }

//        protected Layout CreateTestLayout()
//        {
//            LayoutImage li = new LayoutImage();
//            li.DropShadowEnabled = true;

//            Layout l = new Layout();
//            l.LayoutItems.Add(li);
//            return l;
//        }

//    }
//}
