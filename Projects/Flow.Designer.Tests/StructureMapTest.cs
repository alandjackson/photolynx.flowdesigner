﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StructureMap.Configuration.DSL;
using StructureMap;

namespace Flow.Designer.Tests
{
    /// <summary>
    /// Summary description for StructureMapTest
    /// </summary>
    [TestClass]
    public class StructureMapTest
    {
        public StructureMapTest()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion


        public interface ITestType 
        {
            ITestSubType1 ITestSubType1 { get; set; }
            ITestSubType2 ITestSubType2 { get; set; }
        }
        public interface ITestSubType1 { }
        public interface ITestSubType2 { }

        public class TestType : ITestType
        {
            public ITestSubType1 ITestSubType1 { get; set; }
            public ITestSubType2 ITestSubType2 { get; set; } 
            public TestType() { }
            public TestType(ITestSubType1 st1, ITestSubType2 st2) 
            {
                ITestSubType1 = st1;
                ITestSubType2 = st2;
            }
        }
        public class TestSubType1 : ITestSubType1
        {
            public TestSubType1() { }
        }
        public class TestSubType2 : ITestSubType2
        {
            public TestSubType2() { }
        }

        public class TestRegistry : Registry
        {
            public TestRegistry()
            {
                ForRequestedType<ITestType>().TheDefaultIsConcreteType<TestType>();
                ForRequestedType<ITestSubType1>().TheDefaultIsConcreteType<TestSubType1>();
                ForRequestedType<ITestSubType2>().TheDefaultIsConcreteType<TestSubType2>();
            }
        }

        [TestMethod]
        public void Registry_fills_dependency_parameters()
        {
            ObjectFactory.Initialize(x => { x.AddRegistry<TestRegistry>(); });

            ITestType tt = ObjectFactory.GetInstance<ITestType>();
            Assert.AreEqual(tt.GetType(), typeof(TestType));
            Assert.IsNotNull(tt.ITestSubType1);
            Assert.IsNotNull(tt.ITestSubType2);
            Assert.IsTrue(tt.ITestSubType1 is ITestSubType1);
            Assert.IsTrue(tt.ITestSubType2 is ITestSubType2);

        }
    }
}
