﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Flow.Designer.Lib.Service.SnapToService;
using System.Windows.Controls;

namespace Flow.Designer.Tests
{
    /// <summary>
    /// Summary description for CanvasEdgeMoveSnapToSurfaceTest
    /// </summary>
    [TestClass]
    public class CanvasEdgeMoveSnapToSurfaceTest
    {
        public CanvasEdgeMoveSnapToSurfaceTest()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void CanvasEdgeMoveSnapToSurface_continuous_move_snap_x()
        {
            Canvas c = new Canvas();

            ISnapToService snap = new SnapToService(5, new CanvasSnapLocationProvider(c));
            snap.Start();
            double x = snap.SnapX(6);
            Assert.AreEqual(6, x);
            for (int i = 0; i < 20; i++)
            {
                x = snap.SnapX(x - 1);
                if (i <= 10)
                    Assert.AreEqual(0, x, "i: " + i);
                else
                    Assert.AreEqual(5 - i, x, "i: " + i);
            }

            snap.Start();
            c.Width = 100;
            x = snap.SnapX(94);
            Assert.AreEqual(94, x);
            for (int i = 0; i < 20; i++)
            {
                x = snap.SnapX(x + 1);
                if (i <= 10)
                    Assert.AreEqual(100, x, "i: " + i);
                else
                    Assert.AreEqual(95 + i, x, "i: " + i);
            }

        }

        [TestMethod]
        public void CanvasEdgeMoveSnapToSurface_continuous_move_snap_y()
        {
            Canvas c = new Canvas();

            ISnapToService snap = new SnapToService(5, new CanvasSnapLocationProvider(c));
            double x;

            snap.Start();
            x = snap.SnapY(6);
            Assert.AreEqual(6, x);
            for (int i = 0; i < 20; i++)
            {
                x = snap.SnapY(x - 1);
                if (i <= 10)
                    Assert.AreEqual(0, x, "i: " + i);
                else
                    Assert.AreEqual(5 - i, x, "i: " + i);
            }

            snap.Start();
            c.Height = 200;
            x = snap.SnapY(194);
            Assert.AreEqual(194, x);
            for (int i = 0; i < 20; i++)
            {
                x = snap.SnapY(x + 1);
                if (i <= 10)
                    Assert.AreEqual(200, x, "i: " + i);
                else
                    Assert.AreEqual(195 + i, x, "i: " + i);
            }

        }

        [TestMethod]
        public void CanvasEdgeMoveSnapToSurface_snaps_to_canvas_edges()
        {
            Canvas c = new Canvas();

            ISnapToService snap = new SnapToService(5, new CanvasSnapLocationProvider(c));

            // test left edge
            Assert.AreEqual(0, snap.SnapX(4));
            Assert.AreEqual(0, snap.SnapX(-4));
            Assert.AreEqual(5.1, snap.SnapX(5.1));
            Assert.AreEqual(-5.1, snap.SnapX(-5.1));

            // test right edge
            c.Width = 100;
            Assert.AreEqual(100, snap.SnapX(104));
            Assert.AreEqual(100, snap.SnapX(96));
            Assert.AreEqual(105.1, snap.SnapX(105.1));
            Assert.AreEqual(94.1, snap.SnapX(94.1));

            // test top edge
            Assert.AreEqual(0, snap.SnapY(4));
            Assert.AreEqual(0, snap.SnapY(-4));
            Assert.AreEqual(5.1, snap.SnapY(5.1));
            Assert.AreEqual(-5.1, snap.SnapY(-5.1));

            // test bottom edge
            c.Height = 200;
            Assert.AreEqual(200, snap.SnapY(204));
            Assert.AreEqual(200, snap.SnapY(196));
            Assert.AreEqual(205.1, snap.SnapY(205.1));
            Assert.AreEqual(194.1, snap.SnapY(194.1));
        }


        public class TestSnapLocationProvider : ISnapLocationProvider
        {
            public double[] XSnapLocations
            {
                get { return new double[] { 40, 50, 60 }; }
            }

            public double[] YSnapLocations
            {
                get { return new double[] { 40, 50, 60 }; }
            }
        }

        [TestMethod]
        public void Snap_picks_nearest_snap_location()
        {
            ISnapToService snap = new SnapToService(100, new TestSnapLocationProvider());
            Assert.AreEqual(60, snap.SnapX(60));
            Assert.AreEqual(60, snap.SnapY(60));

        }
    }
}
