﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.Designer.Lib.Service;
using Flow.Designer.Tests.TestModels;
using Flow.Designer.Model.Project;
using Flow.Designer.Lib.ProjectRepository;

namespace Flow.Designer.Tests.TestServices
{
    public class TestSubject : Subject
    {
        public TestSubject()
        {
            Fields = new Dictionary<string, string>();
            Fields["First Name"] = "Alan";
        }
        
    }

    public class TestLayoutSubjectService : ILayoutSubjectService
    {
        Subject Subject { get; set; }

        public TestLayoutSubjectService()
        {
            Subject = new Subject();
            Subject.Fields = new Dictionary<string, string>();
            Subject.Fields["First Name"] = "Alan";
        }

        #region ILayoutSubjectService Members

        public ICurrentSubjectRepository SubjectRepository { get; set; }
        public Subject GetSubject()
        {
            return Subject;
        }

        #endregion

        #region ILayoutSubjectService Members

        public event EventHandler SubjectChanged;
        protected void OnSubjectChanged(){if (SubjectChanged != null) SubjectChanged(this, EventArgs.Empty); }
        #endregion
    }
}
