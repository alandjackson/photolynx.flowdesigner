﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Flow.Designer.Lib.Collections;
using System.ComponentModel;
using Flow.Designer.Model;

namespace Flow.Designer.Tests
{
    /// <summary>
    /// Summary description for UndoRedoStackTests
    /// </summary>
    [TestClass]
    public class UndoRedoStackTests
    {
        public UndoRedoStackTests()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        class CloneableDouble : AbstractNotifyPropertyChanged, ICloneable
        {
            protected double _value;
            public double Value
            {
                get { return _value; }
                set { _value = value; }
            }

            public CloneableDouble() : this(0) { }

            public CloneableDouble(double v)
            {
                Value = v;
            }

            #region ICloneable Members

            public object Clone()
            {
                return new CloneableDouble(Value); 
            }

            #endregion
        }

        [TestMethod]
        public void Starts_empty()
        {
            UndoRedoStack<CloneableDouble> history = new UndoRedoStack<CloneableDouble>();
            Assert.AreEqual(0, history.Items.Count);
            Assert.IsNull(history.Top);
        }

        [TestMethod]
        public void Push_add_to_items_and_top()
        {
            UndoRedoStack<CloneableDouble> history = new UndoRedoStack<CloneableDouble>();
            history.Push(new CloneableDouble(12));
            Assert.AreEqual(1, history.Items.Count);
            Assert.AreEqual(12, history.Items[0].Value);
            Assert.AreEqual(12, history.Top.Value);
        }

        [TestMethod]
        public void Undo_removes_top_item()
        {
            UndoRedoStack<CloneableDouble> history = new UndoRedoStack<CloneableDouble>();
            history.Push(new CloneableDouble(12));
            history.Push(new CloneableDouble(13));
            Assert.AreEqual(13, history.Top.Value);
            history.Undo();
            Assert.AreEqual(12, history.Top.Value);
        }

        [TestMethod]
        public void Redo_adds_back_last_item_removed()
        {
            UndoRedoStack<CloneableDouble> history = new UndoRedoStack<CloneableDouble>();
            history.Push(new CloneableDouble(12));
            history.Push(new CloneableDouble(13));
            history.Undo();
            history.Redo();
            Assert.AreEqual(13, history.Top.Value);
        }
    }
}
