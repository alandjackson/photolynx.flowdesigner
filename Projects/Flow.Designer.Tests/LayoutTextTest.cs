﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Flow.Designer.Model;
using Flow.Designer.Tests.TestServices;

namespace Flow.Designer.Tests
{
    /// <summary>
    /// Summary description for LayoutTextTest
    /// </summary>
    [TestClass]
    public class LayoutTextTest
    {
        public LayoutTextTest()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void LayoutText_Set_Static_Text()
        {
            LayoutText lt = new LayoutText();
            lt.Subject = new TestSubject();
            lt.Text = "School Pictures 2009";
            Assert.AreEqual("School Pictures 2009", lt.RenderText);
        }

        [TestMethod]
        public void LayoutText_Set_Dynamic_Text_First_Name()
        {
            LayoutText lt = new LayoutText();
            lt.Subject = new TestSubject();
            lt.Text = "[First Name]";
            Assert.AreEqual("Alan", lt.RenderText);
        }
    }
}
