﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Flow.Controls.Edit
{
	public class IntegerTextBox : ConstrainedTextBox<int?>
	{
		public IntegerTextBox() : base()
		{
			base.Constraint = @"^\d*$";
			base.ExcludeSpace = true;
		}
	}
}
