﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Flow.Controls.Edit
{
	
	/// <summary>
	/// 
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class ConstrainedTextBox<T> : BaseTextBox
	{
		//static ConstrainedTextBox()
		//{
		//    DefaultStyleKeyProperty.OverrideMetadata(typeof(ConstrainedTextBox<T>), new FrameworkPropertyMetadata(typeof(ConstrainedTextBox<T>)));
		//}

		private string _constraint = null;
		protected string Constraint
		{
			get { return _constraint; }
			set
			{
				_constraint = value;
				SetConstraintRegex();
			}
		}

		private Regex _constraintRegex = null;

		private ConstraintVector _constraintVector = ConstraintVector.Inclusion;
		protected ConstraintVector ConstraintVector
		{
			get { return _constraintVector; }
			set
			{
				_constraintVector = value;
				SetConstraintRegex();
			}
		}

		protected bool ExcludeSpace { get; set; }


		private T _typedValue;
		public T TypedValue
		{
			get { return _typedValue; }
			set { _typedValue = value; }
		}

		public ConstrainedTextBox() : base() { }

		protected override void OnPreviewKeyDown(KeyEventArgs e)
		{
			if (e.Key == Key.Space && this.ExcludeSpace)
				e.Handled = true;
	
			base.OnPreviewKeyDown(e);
		}

		//protected override void OnPreviewTextInput(TextCompositionEventArgs e)
		//{
		//    if(!_constraintRegex.IsMatch(e.Text))
		//        e.Handled = true;

		//    base.OnPreviewTextInput(e);
		//}

		protected override void OnTextInput(TextCompositionEventArgs e)
		{
			TextBox source = e.Source as TextBox;

			string provisionalText = source.Text
				.Remove(source.SelectionStart, source.SelectionLength)
				.Insert(source.SelectionStart, e.Text);

			if (!_constraintRegex.IsMatch(provisionalText))
				e.Handled = true;

			base.OnTextInput(e);
		}


		private void SetConstraintRegex()
		{
			string _constraintRegexString = "";
	
			switch (_constraintVector)
			{
				case ConstraintVector.Exclusion:
					_constraintRegexString = "[" + _constraint + "]";
					break;
				default:
					_constraintRegexString = _constraint;
					break;
			}

			_constraintRegex = new Regex(_constraintRegexString);
		}

	} // END class


	public enum ConstraintVector
	{
		Inclusion,
		Exclusion
	}

}	// END namespace
