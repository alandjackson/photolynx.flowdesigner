﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Flow.Controls.Edit
{
	public class DecimalTextBox : ConstrainedTextBox<decimal?>
	{
		public DecimalTextBox()	: base()
		{
			base.Constraint = @"^-?\d*\.?\d{0,2}$";
			base.ExcludeSpace = true;
		}

		protected override void OnLostFocus(RoutedEventArgs e)
		{
			decimal result;

			if(decimal.TryParse(this.Text, out result))
				this.Text = result.ToString("0.00");
			else
				this.Text = null;

			base.OnLostFocus(e);
		}
	}
}
