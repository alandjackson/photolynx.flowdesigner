﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;

namespace Flow.Controls.Extension
{
	public static class ComboBoxExtension
	{
		public static string GetTagString(this ComboBox comboBox)
		{
			ComboBoxItem item = comboBox.SelectedItem as ComboBoxItem;

			if (item != null && item.Tag.GetType() == typeof(string) && item.Tag != null)
				return item.Tag.ToString();
			else
				return null;
		}
	}
}
