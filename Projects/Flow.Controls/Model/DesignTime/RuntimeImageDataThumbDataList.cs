﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flow.Controls.Model.DesignTime
{
    public class RuntimeImageDataThumbDataList : List<ImageDataThumbData>
    {
        public RuntimeImageDataThumbDataList()
        {
            for (int i = 0; i < 20; i++)
            {
                this.Add(new RuntimeImageDataThumbData());
            }
        }
    }
}
