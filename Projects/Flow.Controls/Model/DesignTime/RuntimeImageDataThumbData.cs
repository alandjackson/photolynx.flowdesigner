﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flow.Controls.Model.DesignTime
{
    public class RuntimeImageDataThumbData : ImageDataThumbData
    {
        public RuntimeImageDataThumbData()
        {
            this.ImageFilename = @"C:\Users\ajackson\Development\photolynx\flow\" +
                @"Flow.Controls\Images\DesignTime\000002.JPG";
            PrimaryFieldValue = "Alan Jackson";
        }
    }
}
