﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flow.Controls.Model.DesignTime
{
    public class RuntimeKeyFieldsGridLayoutData : KeyFieldsGridLayoutData
    {
        public RuntimeKeyFieldsGridLayoutData()
        {
            this.Add(new FieldData("Code", "TEST"));
            this.Add(new FieldData("Job Type", "Middle"));
            this.Add(new FieldData("Job Number", "10-15-00"));
            this.Add(new FieldData("Sequence Number", "1"));
            this.Add(new FieldData("Record Number", "10001"));
            this.Add(new FieldData("First Name", "Mark"));
            this.Add(new FieldData("Last Name", "Allington"));
            this.Add(new FieldData("Title", "Superintendent"));
            this.Add(new FieldData("Teacher", "Staff"));
            this.Add(new FieldData("Grade", "Staff"));
        }

        public void Add(FieldData data)
        {
            EditableFieldData edata = new EditableFieldData();
            edata.FieldData = data;
            edata.IsEditing = false;
            this.Add(edata);
        }
    }

    public class RuntimeLeftKeyFieldsGridLayoutData : KeyFieldsGridLayoutData
    {
        public RuntimeLeftKeyFieldsGridLayoutData()
        {
            RuntimeKeyFieldsGridLayoutData data = new RuntimeKeyFieldsGridLayoutData();
            for (int i = 0; i < data.Count; i+=2)
                this.Add(data[i]);
        }
    }
    public class RuntimeRightKeyFieldsGridLayoutData : KeyFieldsGridLayoutData
    {
        public RuntimeRightKeyFieldsGridLayoutData()
        {
            RuntimeKeyFieldsGridLayoutData data = new RuntimeKeyFieldsGridLayoutData();
            for (int i = 1; i < data.Count; i += 2)
                this.Add(data[i]);
        }
    }

}
