﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace Flow.Controls.Model.DesignTimeModels
{
    public class RuntimeImageList : ObservableCollection<string>
    {
        public RuntimeImageList()
        {
            this.Add(@"C:\Users\ajackson\Development\photolynx\flow\Flow.Controls\Images\DesignTime\000001.JPG");
            this.Add(@"C:\Users\ajackson\Development\photolynx\flow\Flow.Controls\Images\DesignTime\000002.JPG");
            this.Add(@"C:\Users\ajackson\Development\photolynx\flow\Flow.Controls\Images\DesignTime\000003.JPG");
            this.Add(@"C:\Users\ajackson\Development\photolynx\flow\Flow.Controls\Images\DesignTime\000004.JPG");
            this.Add(@"C:\Users\ajackson\Development\photolynx\flow\Flow.Controls\Images\DesignTime\000005.JPG");
        }
    }
}
