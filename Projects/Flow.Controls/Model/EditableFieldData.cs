﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.ComponentModel;

namespace Flow.Controls.Model
{
    public class EditableFieldData : INotifyPropertyChanged
    {
        public EditableFieldData()
        {
            this.PropertyChanged += 
                new PropertyChangedEventHandler(EditableFieldData_PropertyChanged);
        }

        void EditableFieldData_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "IsEditing")
            {
                OnPropertyChanged("EditingVisibleProp");
                OnPropertyChanged("EditingNotVisibleProp");
            }
        }

        protected FieldData _fieldData;
        public FieldData FieldData
        {
            get { return (_fieldData); }
            set { _fieldData = value; OnPropertyChanged("FieldData"); }
        }
        protected bool _isEditing;
        public bool IsEditing 
        {
            get { return _isEditing; }
            set { _isEditing = value; OnPropertyChanged("IsEditing"); }
        }

        public Visibility EditingVisibleProp
        {
            get { return BoolToVisibility(IsEditing); }
            set { }
        }
        public Visibility EditingNotVisibleProp
        {
            get { return BoolToVisibility(!IsEditing); }
            set { }
        }

        public Visibility BoolToVisibility(bool v)
        {
            return v ? Visibility.Visible : Visibility.Hidden;
        }



        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(name));
        }

        #endregion
    }
}
