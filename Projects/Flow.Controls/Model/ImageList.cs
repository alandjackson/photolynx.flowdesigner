﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flow.Controls.Model
{
    public class ImageList
    {
        public ObservableCollection<string> ImageFilenames { get; set; }

        public ImageList()
        {
            ImageFilenames = new ObservableCollection<string>();
        }
    }
}
