﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flow.Controls.Model
{
    public class ImageDataThumbData
    {
        public string ImageFilename { get; set; }
        public List<FieldData> Fields { get; set; }
        public string PrimaryFieldValue { get; set; }
        public string MinorField1 { get; set; }
        public string MinorField2 { get; set; }
        public string MinorField3 { get; set; }
        public List<string> MinorFields
        {
            get
            {
                return new List<string>(new string[] {
                    MinorField1, MinorField2, MinorField3});
            }
        }

        public ImageDataThumbData()
        {
            ImageFilename = "";
            Fields = new List<FieldData>();
        }

    }
}
