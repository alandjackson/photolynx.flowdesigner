﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flow.Controls.Model
{
    public class FieldData
    {
        public string FieldName { get; set; }
        public string FieldStringValue { get; set; }

        public FieldData(string name, string value)
        {
            FieldName = name;
            FieldStringValue = value;
        }
    }
}
