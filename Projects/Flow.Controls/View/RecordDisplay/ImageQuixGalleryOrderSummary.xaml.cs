﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Flow.Schema.LinqModel.DataContext.FlowMaster;
using Flow.Lib.Helpers;
using Flow.Schema.LinqModel.DataContext;

namespace Flow.Controls.View.RecordDisplay
{
    /// <summary>
    /// Interaction logic for ImageQuixGalleryOrderSummary.xaml
    /// </summary>
    public partial class ImageQuixGalleryOrderSummary : UserControl
    {

        public event EventHandler<EventArgs<ProjectGallery>> LoadOrdersInvoked = delegate { };

        public ImageQuixGalleryOrderSummary()
        {
            InitializeComponent();
        }

        private void btnImportOrders_Click(object sender, RoutedEventArgs e)
        {
            ProjectGallery gallery = (ProjectGallery)this.DataContext;
            this.LoadOrdersInvoked(this, new EventArgs<ProjectGallery>(gallery));
        }

      

    }
}
