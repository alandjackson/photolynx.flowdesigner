﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Controls.View.RecordDisplay;
using Flow.Lib.Helpers;
using Flow.Schema.LinqModel.DataContext;

namespace Flow.Controls.View.RecordDisplay
{
	/// <summary>
	/// Interaction logic for OrderProductImageListDisplay.xaml
	/// </summary>
	public partial class OrderProductNodeListDisplay : UserControl
	{
		public event EventHandler<EventArgs<OrderProductNode>> AssignPreviousImageRequested = delegate { };
		public event EventHandler<EventArgs<OrderProductNode>> AssignNextImageRequested = delegate { };
		public event EventHandler<EventArgs<OrderProductNode>> AssignNullImageRequested = delegate { };
		
		public OrderProductNodeListDisplay()
		{
			InitializeComponent();
		}


		private void AssignPreviousImageButton_Click(object sender, RoutedEventArgs e)
		{
			Button source = sender as Button;
			if (source != null)
			{
				OrderProductNode orderProductNode = source.DataContext as OrderProductNode;
				if (orderProductNode != null)
					this.AssignPreviousImageRequested(this, new EventArgs<OrderProductNode>(orderProductNode));
			}
		}

		private void AssignNullImageButton_Click(object sender, RoutedEventArgs e)
		{
			Button source = sender as Button;
			if (source != null)
			{
				OrderProductNode orderProductNode = source.DataContext as OrderProductNode;
				if (orderProductNode != null)
					this.AssignNullImageRequested(this, new EventArgs<OrderProductNode>(orderProductNode));
			}
		}

		private void AssignNextImageButton_Click(object sender, RoutedEventArgs e)
		{
			Button source = sender as Button;
			if (source != null)
			{
				OrderProductNode orderProductNode = source.DataContext as OrderProductNode;
				if (orderProductNode != null)
					this.AssignNextImageRequested(this, new EventArgs<OrderProductNode>(orderProductNode));
			}
		}	
	}

	/// <summary>
	/// 
	/// </summary>
	public class ImageQuixProductNodeTypeTemplateSelector : DataTemplateSelector
	{
		public DataTemplate ImageDataTemplate { get; set; }
		public DataTemplate TextDataTemplate { get; set; }
		public DataTemplate NoneDataTemplate { get; set; }

		public override DataTemplate SelectTemplate(object item, DependencyObject container)
		{
			if (item != null && item is OrderProductNode)
			{
				OrderProductNode node = item as OrderProductNode;

				if (node.ImageQuixProductNodeType == 1)
					return this.ImageDataTemplate;

				if (node.ImageQuixProductNodeType == 2)
					return this.TextDataTemplate;
			}

			return this.NoneDataTemplate;
		}
	}



}
