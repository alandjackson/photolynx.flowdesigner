﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Lib.Helpers;
using Flow.Schema.LinqModel.DataContext;

namespace Flow.Controls.View.RecordDisplay
{
    /// <summary>
    /// Interaction logic for ImageDataThumb.xaml
    /// </summary>
    public partial class CatalogOptionRecordDisplay : UserControl
    {
        public event EventHandler<EventArgs<FlowCatalogOption>> AddCatalogOptionInvoked = delegate { };
        //public event EventHandler<EventArgs<ProductPackage>> AddCatalogOptionToAllInvoked = delegate { };
        //public event EventHandler<EventArgs<ProductPackage>> UnassignItemInvoked = delegate { };
        public event EventHandler<EventArgs> SaveChangesInvoked = delegate { };

        public event EventHandler<EventArgs<ProductPackage>> EditCatalogItemInvoked = delegate { };
        //public event EventHandler<EventArgs<ProductPackage>> DeleteItemInvoked = delegate { };


        protected static DependencyProperty CatalogOptionDisplayContextProperty = DependencyProperty.Register(
            "CatalogOptionDisplayContext", typeof(CatalogOptionItemDisplayContext), typeof(CatalogOptionRecordDisplay),
            new PropertyMetadata(CatalogOptionItemDisplayContext.CatalogAssignment)
        );

        public CatalogOptionItemDisplayContext CatalogOptionDisplayContext
        {
            get { return (CatalogOptionItemDisplayContext)this.GetValue(CatalogOptionDisplayContextProperty); }
            set { this.SetValue(CatalogOptionDisplayContextProperty, value); }
        }

        //protected static DependencyProperty AddToAllConfirmPanelVisibilityProperty = DependencyProperty.Register(
        //    "AddToAllConfirmPanelVisibility", typeof(Visibility), typeof(ProductPackageRecordDisplay),
        //    new PropertyMetadata(Visibility.Collapsed)
        //);

        //public Visibility AddToAllConfirmPanelVisibility
        //{
        //    get { return (Visibility)this.GetValue(AddToAllConfirmPanelVisibilityProperty); }
        //    set { this.SetValue(AddToAllConfirmPanelVisibilityProperty, value); }
        //}



        //public ProductPackage AssignedProductPackage
        //{
        //    get { return (this.DataContext as ProductPackage); }
        //}


        public CatalogOptionRecordDisplay()
        {
            InitializeComponent();
            //((FlowCatalogOption)this.DataContext).ImageQuixCatalogOption
        }

        private void AddCatalogPackageButton_Click(object sender, RoutedEventArgs e)
        {
            this.AddCatalogOptionInvoked(this, new EventArgs<FlowCatalogOption>((FlowCatalogOption)this.DataContext));
        }

        //private void AddOrderPackageToAllButton_Click(object sender, RoutedEventArgs e)
        //{
        //    this.AddToAllConfirmPanelVisibility = Visibility.Visible;
        //}

        //private void ConfirmAddToAllButton_Click(object sender, RoutedEventArgs e)
        //{
        //    this.AddToAllConfirmPanelVisibility = Visibility.Collapsed;
        //    this.AddOrderPackageToAllInvoked(this, new EventArgs<ProductPackage>(this.AssignedProductPackage));
        //}

        //private void CancelAddToAllButton_Click(object sender, RoutedEventArgs e)
        //{
        //    this.AddToAllConfirmPanelVisibility = Visibility.Collapsed;
        //}
        string oldMap = "";
        string oldPrice = "";
        bool oldTaxed = false;

        private void EditCatalogOptionItemButton_Click(object sender, RoutedEventArgs e)
        {
            this.EditCatalogOptionItemButton.Visibility = Visibility.Collapsed;
            this.CancelCatalogOptionItemButton.Visibility = Visibility.Visible;
            this.SaveCatalogOptionItemButton.Visibility = Visibility.Visible;
            this.txtMap.IsEnabled = true;
            this.txtPrice.IsEnabled = true;
            this.cmbTaxed.IsEnabled = true;

            oldMap = txtMap.Text;
            oldPrice = txtPrice.Text;
            oldTaxed = (bool)cmbTaxed.IsChecked;
        }

        private void SaveCatalogOptionItemButton_Click(object sender, RoutedEventArgs e)
        {
            this.EditCatalogOptionItemButton.Visibility = Visibility.Visible;
            this.CancelCatalogOptionItemButton.Visibility = Visibility.Collapsed;
            this.SaveCatalogOptionItemButton.Visibility = Visibility.Collapsed;
            this.txtMap.IsEnabled = false;
            this.txtPrice.IsEnabled = false;
            this.cmbTaxed.IsEnabled = false;
            this.SaveChangesInvoked(this, null);
        }

        private void CancelCatalogOptionItemButton_Click(object sender, RoutedEventArgs e)
        {
            this.txtMap.Text = oldMap;
            this.txtPrice.Text = oldPrice;
            this.cmbTaxed.IsChecked = oldTaxed;

            this.EditCatalogOptionItemButton.Visibility = Visibility.Visible;
            this.CancelCatalogOptionItemButton.Visibility = Visibility.Collapsed;
            this.SaveCatalogOptionItemButton.Visibility = Visibility.Collapsed;
            this.txtMap.IsEnabled = false;
            this.txtPrice.IsEnabled = false;
            this.cmbTaxed.IsEnabled = false;
        }

        //private void DeleteItemButton_Click(object sender, RoutedEventArgs e)
        //{
        //    this.DeleteItemInvoked(this, new EventArgs<ProductPackage>(this.AssignedProductPackage));
        //}

        //private void AddProductButton_Click(object sender, RoutedEventArgs e)
        //{
        //    Button source = sender as Button;

        //    ProductPackageComposition composition = source.CommandParameter as ProductPackageComposition;

        //    composition++;
        //}

        //private void DeleteProductButton_Click(object sender, RoutedEventArgs e)
        //{
        //    Button source = sender as Button;

        //    ProductPackageComposition composition = source.CommandParameter as ProductPackageComposition;

        //    if ((--composition).Quantity == 0)
        //        this.AssignedProductPackage.DeleteComposition(composition);
        //}


        private void ControlButton_Click(object sender, RoutedEventArgs e)
        {
            //this.AssignedProductPackage.Select();
        }

        
	}


    public enum CatalogOptionItemDisplayContext
    {
        CatalogAssignment,
        ProductAssignment,
        OrderEntry
    }

}
