﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Lib.Helpers;
using Flow.Schema.LinqModel.DataContext;

namespace Flow.Controls.View.RecordDisplay
{
    /// <summary>
    /// Interaction logic for ImageDataThumb.xaml
    /// </summary>
    public partial class OrderItemDisplay : UserControl
    {
		public event EventHandler<OrderChangeOperationEventArgs> OrderChangeOperationInvoked = delegate { };

		public OrderItem AssignedOrderItem
		{
			get { return (this.DataContext as OrderItem); }
		}

		public OrderItemDisplay()
        {
            InitializeComponent();
        }

		private void IncrementItemButton_Click(object sender, RoutedEventArgs e)
		{
			this.OrderChangeOperationInvoked(this,
				new OrderChangeOperationEventArgs(OrderChangeOperation.Increment, this.AssignedOrderItem.ProductPackage));
		}

		private void DecrementItemButton_Click(object sender, RoutedEventArgs e)
		{
			this.OrderChangeOperationInvoked(this,
				new OrderChangeOperationEventArgs(OrderChangeOperation.Decrement, this.AssignedOrderItem.ProductPackage));
		}

		private void ClearItemsButton_Click(object sender, RoutedEventArgs e)
		{
			this.OrderChangeOperationInvoked(this,
				new OrderChangeOperationEventArgs(OrderChangeOperation.Clear, this.AssignedOrderItem.ProductPackage));
		}
	}

	public class OrderChangeOperationEventArgs : EventArgs
	{
		public OrderChangeOperation Operation { get; private set; }
		public ProductPackage Item { get; private set; }

		public OrderChangeOperationEventArgs(OrderChangeOperation operation, ProductPackage item)
		{
			this.Operation = operation;
			this.Item = item;
		}
	}

	public enum OrderChangeOperation
	{
		Increment,
		Decrement,
		Clear
	}


}
