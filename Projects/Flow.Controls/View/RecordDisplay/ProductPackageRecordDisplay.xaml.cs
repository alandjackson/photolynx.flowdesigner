﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Lib.Helpers;
using Flow.Schema.LinqModel.DataContext;
using Flow.Controls.View.Dialogs;
using System.IO;
using System.Windows.Controls.Primitives;

namespace Flow.Controls.View.RecordDisplay
{
    /// <summary>
    /// Interaction logic for ImageDataThumb.xaml
    /// </summary>
    public partial class ProductPackageRecordDisplay : UserControl
    {
		public event EventHandler<EventArgs<ProductPackage>> AddOrderPackageInvoked = delegate { };
        public event EventHandler<EventArgs<KeyValuePair<ProductPackage, string>>> AddOrderPackageToAllInvoked = delegate { };
		//public event EventHandler<EventArgs<ProductPackage>> UnassignItemInvoked = delegate { };

		public event EventHandler<EventArgs<ProductPackage>> EditItemInvoked = delegate { };
		public event EventHandler<EventArgs<ProductPackage>> DeleteItemInvoked = delegate { };

        //public FlowProjectDataContext FlowProjectDataContext { get; set; }



        protected static DependencyProperty FlowProjectDataContextProperty = DependencyProperty.Register(
            "FlowProjectDataContext", typeof(FlowProjectDataContext), typeof(ProductPackageRecordDisplay));
        public FlowProjectDataContext FlowProjectDataContext
        {
            get { return (FlowProjectDataContext)this.GetValue(FlowProjectDataContextProperty); }
            set { this.SetValue(FlowProjectDataContextProperty, value); }
        }

		protected static DependencyProperty DisplayContextProperty = DependencyProperty.Register(
			"DisplayContext", typeof(ProductCatalogItemDisplayContext), typeof(ProductPackageRecordDisplay),
			new PropertyMetadata(ProductCatalogItemDisplayContext.CatalogAssignment)
		);

		public ProductCatalogItemDisplayContext DisplayContext
		{
			get { return (ProductCatalogItemDisplayContext)this.GetValue(DisplayContextProperty); }
			set { this.SetValue(DisplayContextProperty, value); }
		}

		protected static DependencyProperty AddToAllConfirmPanelVisibilityProperty = DependencyProperty.Register(
			"AddToAllConfirmPanelVisibility", typeof(Visibility), typeof(ProductPackageRecordDisplay),
			new PropertyMetadata(Visibility.Collapsed)
		);

		public Visibility AddToAllConfirmPanelVisibility
		{
			get { return (Visibility)this.GetValue(AddToAllConfirmPanelVisibilityProperty); }
			set { this.SetValue(AddToAllConfirmPanelVisibilityProperty, value); }
		}



		public ProductPackage AssignedProductPackage
		{
			get { return (this.DataContext as ProductPackage); }
		}


		public ProductPackageRecordDisplay()
        {
            InitializeComponent();
            cmbDigitalDownload.Items.Add("None");
            cmbDigitalDownload.Items.Add("400px");
            cmbDigitalDownload.Items.Add("800px");
            cmbDigitalDownload.Items.Add("1200px");
        }

		private void AddOrderPackageButton_Click(object sender, RoutedEventArgs e)
		{
			this.AddOrderPackageInvoked(this, new EventArgs<ProductPackage>(this.AssignedProductPackage));
		}

		private void AddOrderPackageToAllButton_Click(object sender, RoutedEventArgs e)
		{
            //if(this.AssignedProductPackage.FlowCatalog.FlowMasterDataContext.FlowProjectList.CurrentItem.FlowProjectDataContext.GroupImages.Count() > 0)
            if (this.FlowProjectDataContext.GroupImages.Count() > 0)
                this.AddToAllConfirmPanelVisibility = Visibility.Visible;
            else
            {
                AssignPackageToAll("Primary");
            }
		}

		private void ConfirmAddToAllButton_Click(object sender, RoutedEventArgs e)
		{
            this.AddToAllConfirmPanelVisibility = Visibility.Collapsed;
            bool useGroupImage = (bool)this.rdoUseGroupImage.IsChecked;
            bool usePrimaryImage = (bool)this.rdoUsePrimaryImage.IsChecked;
            if(useGroupImage)
                AssignPackageToAll("Group");
            else
                AssignPackageToAll("Primary");
        }

        private void AssignPackageToAll(string PackageImageType)
        {
            //int subsWithImages = ((ProductPackage)this.AssignedProductPackage).FlowCatalog.FlowMasterDataContext.FlowProjectList.CurrentItem.SubjectList.Where(s=>s.SubjectImages != null && s.SubjectImages.Count>0 && ).Count();
            //int subs = ((ProductPackage)this.AssignedProductPackage).FlowCatalog.FlowMasterDataContext.FlowProjectList.CurrentItem.SubjectList.View.Count;
            string message = "\nYou are about to add this Package to all Subjects that have already been asigned an image.\n\n This is an irreversible operation!";
            //
            FlowMessageDialog msg = new FlowMessageDialog("Confirm Bulk Package Add", message);
            msg.buttonOkay.Content = "Confirm";
            if (msg.ShowDialog() == true)
            {
                this.AddOrderPackageToAllInvoked(this, new EventArgs<KeyValuePair<ProductPackage, string>>(new KeyValuePair<ProductPackage, string>(this.AssignedProductPackage, PackageImageType)));
            }
        }
		private void CancelAddToAllButton_Click(object sender, RoutedEventArgs e)
		{
			this.AddToAllConfirmPanelVisibility = Visibility.Collapsed;
		}

		private void EditItemButton_Click(object sender, RoutedEventArgs e)
		{
			this.AssignedProductPackage.Select();
			this.EditItemInvoked(this, new EventArgs<ProductPackage>(this.AssignedProductPackage)); 
		}

		private void DeleteItemButton_Click(object sender, RoutedEventArgs e)
		{
			this.DeleteItemInvoked(this, new EventArgs<ProductPackage>(this.AssignedProductPackage));
		}

		private void AddProductButton_Click(object sender, RoutedEventArgs e)
		{
			Button source = sender as Button;

			ProductPackageComposition composition = source.CommandParameter as ProductPackageComposition;

			composition++;
		}

		private void DeleteProductButton_Click(object sender, RoutedEventArgs e)
		{
			Button source = sender as Button;

			ProductPackageComposition composition = source.CommandParameter as ProductPackageComposition;

			if ((--composition).Quantity == 0)
				this.AssignedProductPackage.DeleteComposition(composition);
		}
	

		private void ControlButton_Click(object sender, RoutedEventArgs e)
		{
            if(this.AssignedProductPackage != null)
			    this.AssignedProductPackage.Select();
		}

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((sender as ListBox).SelectedIndex < 0)
                return;

            ProductPackage pp = this.DataContext as ProductPackage;

            ListBox cb = sender as ListBox;
            ProductPackageComposition ppc = (cb.DataContext as ProductPackageComposition);
            if (cb.SelectedItem.ToString() == "None")
                cb.SelectedItem = "";

            if (cb.SelectedItem.ToString().StartsWith("-----"))
            {
                cb.SelectedIndex = -1;
                ppc.UpdateDisplayLabel();
                ppc.ExpandLayouts = true;
                //cb.IsDropDownOpen = true;
                return;
            }
            else if (cb.SelectedItem.ToString().StartsWith(".."))
            {
                pp.FlowCatalog.FlowMasterDataContext.TemplayoutFolder = (new DirectoryInfo(pp.FlowCatalog.FlowMasterDataContext.TemplayoutFolder)).Parent.FullName;
                pp.FlowCatalog.FlowMasterDataContext.UpdateLayouts();
                cb.SelectedIndex = -1;
                ppc.UpdateDisplayLabel();
                ppc.ExpandLayouts = true;
                //cb.IsDropDownOpen = true;
                return;
            }
            else if (cb.SelectedItem.ToString().StartsWith("["))
            {
                pp.FlowCatalog.FlowMasterDataContext.TemplayoutFolder = System.IO.Path.Combine(pp.FlowCatalog.FlowMasterDataContext.TemplayoutFolder, cb.SelectedItem.ToString().Trim('[', ']'));
                pp.FlowCatalog.FlowMasterDataContext.UpdateLayouts();
                cb.SelectedIndex = -1;
                ppc.UpdateDisplayLabel();
                ppc.ExpandLayouts = true;
               // cb.IsDropDownOpen = true;
                return;
            }

            ppc.Layout = cb.SelectedItem.ToString();
            ppc.UpdateDisplayLabel();
            pp.FlowCatalog.FlowMasterDataContext.SubmitChanges();
            //cb.IsDropDownOpen = false;
            ppc.ExpandLayouts = false;
            
        }

        private void chkShowInGallery_Checked(object sender, RoutedEventArgs e)
        {
             ProductPackage pp = this.DataContext as ProductPackage;
            if(pp.FlowCatalog.FlowMasterDataContext != null)
                pp.FlowCatalog.FlowMasterDataContext.SubmitChanges();
        }

        private void chkShowInGallery_Unchecked(object sender, RoutedEventArgs e)
        {
            ProductPackage pp = this.DataContext as ProductPackage;
            pp.FlowCatalog.FlowMasterDataContext.SubmitChanges();
        }

        private void cmbDigitalDownload_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ProductPackage pp = this.DataContext as ProductPackage;
           
            if (pp.FlowCatalog != null  && pp.FlowCatalog.FlowMasterDataContext != null)
                pp.FlowCatalog.FlowMasterDataContext.SubmitChanges();

            lblDigitalDownloadMessage.Content = pp.DigitalDownloadMessage;
            lblDigitalDownloadMessage.UpdateLayout();
        }

        private void uxLoadbtn_Click(object sender, RoutedEventArgs e)
        {
            (sender as Button).ContextMenu.IsOpen = true;
            //this.uxLoadBtnMenu.IsOpen = true;
        }

        private void btnShowPopup_Click(object sender, RoutedEventArgs e)
        {

            ProductPackageComposition ppc = ((sender as Button).DataContext as ProductPackageComposition);
            if (ppc.ExpandLayouts == true)
                ppc.ExpandLayouts = false;
            else
                ppc.ExpandLayouts = true;
        }

        private void btnCancelPopup_Click(object sender, RoutedEventArgs e)
        {
            ProductPackageComposition ppc = ((sender as Button).DataContext as ProductPackageComposition);
            ppc.ExpandLayouts = false;
        }

        private void layoutPopup_Opened(object sender, EventArgs e)
        {
            Popup p = (Popup)sender;
            Grid sp = (Grid)p.Child;
            ListBox lb = (ListBox)sp.FindName("lstLayouts");
            lb.Focus();
        }

        private void layoutPopup_LostFocus(object sender, RoutedEventArgs e)
        {
            //ProductPackageComposition ppc = ((sender as Popup).DataContext as ProductPackageComposition);
            //pp.ExpandLayouts = false;
        }

        private void layoutPopup_MouseLeave(object sender, MouseEventArgs e)
        {
            ProductPackageComposition ppc = ((sender as Popup).DataContext as ProductPackageComposition);
            ppc.ExpandLayouts = false;
        }

        private void chkIsAnyXPackage_Checked(object sender, RoutedEventArgs e)
        {
            ProductPackage pp = this.DataContext as ProductPackage;
            if (pp.ProductPackageCompositions.Count > 0)
            {
                pp.IsAnyXPackage = false;
                pp.MaxProductsAllowed = 0;
            }

            if (pp.FlowCatalog.FlowMasterDataContext != null)
                pp.FlowCatalog.FlowMasterDataContext.SubmitChanges();
        }

        private void chkIsAnyXPackage_Unchecked(object sender, RoutedEventArgs e)
        {
            ProductPackage pp = this.DataContext as ProductPackage;
            if (pp.FlowCatalog.FlowMasterDataContext != null)
                pp.FlowCatalog.FlowMasterDataContext.SubmitChanges();
        }

       
	}


	public enum ProductCatalogItemDisplayContext
	{
		CatalogAssignment,
		ProductAssignment,
		OrderEntry
	}

}
