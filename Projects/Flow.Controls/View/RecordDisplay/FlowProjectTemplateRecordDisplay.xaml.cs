﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Flow.Schema.LinqModel.DataContext;

using Flow.Lib.Helpers;

namespace Flow.Controls.View.RecordDisplay
{
    /// <summary>
    /// Interaction logic for ImageDataThumb.xaml
    /// </summary>
    public partial class FlowProjectTemplateRecordDisplay : UserControl
    {

        public event EventHandler<EventArgs<ProjectTemplate>> DeleteProjectTemplateInvoked = delegate { };
        
		public FlowProjectTemplateRecordDisplay()
        {
            InitializeComponent();
        }

        public ProjectTemplate FlowProjectTemplate
        {
            get { return (this.DataContext as ProjectTemplate); }
        }

        private void DeleteProjectTemplate_Click(object sender, RoutedEventArgs e)
        {
            this.DeleteProjectTemplateInvoked(this, new EventArgs<ProjectTemplate>(this.FlowProjectTemplate));
        }
	}
}
