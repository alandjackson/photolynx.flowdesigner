﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Schema.LinqModel.DataContext;

namespace Flow.Controls.View.RecordDisplay
{
	/// <summary>
	/// Interaction logic for SubjectDatumControl.xaml
	/// </summary>
	public partial class SubjectDatumDatePickerControl : UserControl
	{
		public static DependencyProperty IsEditableProperty =
			DependencyProperty.Register("IsEditable", typeof(bool), typeof(SubjectDatumDatePickerControl), null);

		public bool IsEditable
		{
			get { return (bool)this.GetValue(IsEditableProperty); }
			set { this.SetValue(IsEditableProperty, value); }
		}

		public SubjectDatumDatePickerControl()
		{
			InitializeComponent();
		}
	}
}
