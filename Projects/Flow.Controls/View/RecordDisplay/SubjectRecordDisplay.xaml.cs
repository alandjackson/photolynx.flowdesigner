﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Lib;
using Flow.Lib.Helpers;
using Flow.Schema.LinqModel.DataContext;

using Xceed.Wpf.DataGrid;
using Flow.Controls.View.ImageDisplay;
using NLog;

namespace Flow.Controls.View.RecordDisplay
{
	/// <summary>
	/// Interaction logic for XceedSubjectRecordDisplay.xaml
	/// </summary>
	public partial class SubjectRecordDisplay : UserControl
	{

       

		public event EventHandler<EventArgs<IEnumerable<SubjectImage>>> ImageUnassigned = delegate { };
        public event EventHandler<EventArgs<SubjectImage>> ToggleGreenscreenInvoked = delegate { };
        public event EventHandler<EventArgs<SubjectImage>> ToggleFlagImageInvoked = delegate { };
        public event EventHandler<EventArgs<SubjectImage>> ToggleRemoteImageServiceInvoked = delegate { };

        public event EventHandler ShowOrderEntry = delegate { };
        public event EventHandler ShowSubmitOrders = delegate { };
        public event EventHandler ShowAdjustImages  = delegate { };

		protected static DependencyProperty CollectionNavigatorVisibilityProperty =
			DependencyProperty.Register("CollectionNavigatorVisibility", typeof(Visibility), typeof(SubjectRecordDisplay),
				new PropertyMetadata(Visibility.Visible)
			);

		public Visibility CollectionNavigatorVisibility
		{
			get { return (Visibility)this.GetValue(CollectionNavigatorVisibilityProperty); }
			set { this.SetValue(CollectionNavigatorVisibilityProperty, value); }
		}


		public SubjectRecordDisplay()
		{
			InitializeComponent();
		}

        private static Logger logger = LogManager.GetCurrentClassLogger();

		private void uxSubjectImages_Drop(object sender, DragEventArgs e)
		{
			//if (e.Data is BitmapImage)
			//    return;
		}

		private void uxSubjectImageList_ImageDoubleClick(object sender, EventArgs e)
		{
            IEnumerable<SubjectImage> selectedImages =
                this.SubjectImageList.GetSelectedItems<SubjectImage>();

            this.ImageUnassigned(this, new EventArgs<IEnumerable<SubjectImage>>(selectedImages));
		}

        private void CollectionNavigator_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void SubjectImageList_MouseEnter(object sender, MouseEventArgs e)
        {
            // Check everything for null before doing anything
            if (this.DataContext == null) return;
            FlowMasterDataContext fmdc = ((FlowProject)this.DataContext).FlowMasterDataContext;
            FlowImageListBoxSubjectRecordTemp imageListBox = (FlowImageListBoxSubjectRecordTemp)sender;
            if (fmdc == null || imageListBox == null) return;

            try
            {
                // Show UnAssign and Flag As buttons (if permitted)
                imageListBox.UnAssignButtonVisibility = fmdc.PreferenceManager.Permissions.CanUnassignImagesFromSubjects ? Visibility.Visible : Visibility.Hidden;
                imageListBox.FlagAsButtonVisibility = fmdc.PreferenceManager.Permissions.CanFlagImageAs ? Visibility.Visible : Visibility.Hidden;
            }
            catch (NullReferenceException exception)
            {
                // Ignore any NullReferenceExceptions
                logger.WarnException("Ignoring an exception: ", exception);
            }
        }

        private void CheckBox_Click(object sender, RoutedEventArgs e)
        {
            FlowProject proj = (FlowProject)this.DataContext;
            proj.FlowProjectDataContext.SubmitChanges();

        }

        private void lblPackages_Click(object sender, MouseButtonEventArgs e)
        {
            this.ShowOrderEntry(this, e);
        }

        private void AddPackageButton_Click(object sender, RoutedEventArgs e)
        {
            this.ShowOrderEntry(this, e);
        }

        private void SubmitOrdersButton_Click(object sender, RoutedEventArgs e)
        {
            this.ShowSubmitOrders(this, e);
        }

        private void SubjectImageList_ToggleGreenscreenInvoked(object sender, EventArgs<SubjectImage> e)
        {

            if (e.Data != null)
            {
                SubjectImage item = e.Data;

                if (item != null)
                {
                    this.ToggleGreenscreenInvoked(this, new EventArgs<SubjectImage>(item));
                }
            }
        }

        private void SubjectImageList_ToggleFlagImageInvoked(object sender, EventArgs<SubjectImage> e)
        {
            if (e.Data != null)
            {
                SubjectImage item = e.Data;

                if (item != null)
                {
                    this.ToggleFlagImageInvoked(this, new EventArgs<SubjectImage>(item));
                }
            }
        }


        private void SubjectImageList_ToggleRemoteImageServiceInvoked(object sender, EventArgs<SubjectImage> e)
        {
            if (e.Data != null)
            {
                SubjectImage item = e.Data;

                if (item != null)
                {
                    this.ToggleRemoteImageServiceInvoked(this, new EventArgs<SubjectImage>(item));
                }
            }
        }

        private void SubjectRecordWrapDisplay_BailBailBail(object sender, EventArgs<bool> e)
        {
            //CollectionNavigator.CancelEdit();
            CollectionNavigator.SaveRecord();
        }

        private void CheckBox_Click_1(object sender, RoutedEventArgs e)
        {

        }

        private void chkIsReady_Click(object sender, RoutedEventArgs e)
        {
            //FlowMasterDataContext fmdc = ((FlowProject)this.DataContext).FlowMasterDataContext;
            ((FlowProject)this.DataContext).FlowProjectDataContext.SubmitChanges();
        }


        private void btnCrop_Click(object sender, RoutedEventArgs e)
        {
           
                this.ShowAdjustImages(this, e);
        }

        private void SubjectRecordWrapDisplay_CheckChanged(object sender, EventArgs<bool> e)
        {
            ((FlowProject)this.DataContext).FlowProjectDataContext.SubmitChanges();
        }
	}





}
