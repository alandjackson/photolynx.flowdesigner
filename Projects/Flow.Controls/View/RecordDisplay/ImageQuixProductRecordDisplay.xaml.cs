﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Lib.Helpers;
using Flow.Schema.LinqModel.DataContext;

namespace Flow.Controls.View.RecordDisplay
{
    /// <summary>
    /// Interaction logic for ImageDataThumb.xaml
    /// </summary>
    public partial class ImageQuixProductRecordDisplay : UserControl
    {
		public event EventHandler<EventArgs<ImageQuixProduct>> AddItemInvoked = delegate { };
		public event EventHandler<EventArgs<ImageQuixProduct>> DeleteItemInvoked = delegate { };
        public event EventHandler<EventArgs<ImageQuixProduct>> AddALaCarteInvoked = delegate { };
        

		protected static DependencyProperty AssignmentButtonsVisibilityProperty = DependencyProperty.Register(
			"AssignmentButtonsVisibility", typeof(Visibility), typeof(ImageQuixProductRecordDisplay)
		);

		public Visibility AssignmentButtonsVisibility
		{
			get { return (Visibility)this.GetValue(AssignmentButtonsVisibilityProperty); }
			set { this.SetValue(AssignmentButtonsVisibilityProperty, value); }
		}


		public ImageQuixProduct AssignedImageQuixProduct
		{
			get { return (this.DataContext as ImageQuixProduct); }
		}


		public ImageQuixProductRecordDisplay()
        {
            InitializeComponent();
        }

        bool ItemAddOrRemoved = false;
		private void AddItemButton_Click(object sender, RoutedEventArgs e)
		{
            ItemAddOrRemoved = true;
			this.AddItemInvoked(this, new EventArgs<ImageQuixProduct>(this.AssignedImageQuixProduct));
		}

		private void DeleteItemButton_Click(object sender, RoutedEventArgs e)
		{
            ItemAddOrRemoved = true;
			this.DeleteItemInvoked(this, new EventArgs<ImageQuixProduct>(this.AssignedImageQuixProduct));
		}


        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (ItemAddOrRemoved)
                ItemAddOrRemoved = false;
            else
            {
                if (this.productDetails.Visibility == Visibility.Visible)
                    this.productDetails.Visibility = Visibility.Collapsed;
                else
                    this.productDetails.Visibility = Visibility.Visible;
            }
        }

        private void AddALaCarteButton_Click(object sender, RoutedEventArgs e)
        {
            this.AddALaCarteInvoked(this, new EventArgs<ImageQuixProduct>(this.AssignedImageQuixProduct));
        }

       


		//private void AssignItemButton_Click(object sender, RoutedEventArgs e)
		//{
		//    this.AssignItemInvoked(this, new EventArgs<ProductPackage>(this.AssignedProductPackage));
		//}

		//private void EditItemButton_Click(object sender, RoutedEventArgs e)
		//{
		//    this.EditItemInvoked(this, new EventArgs<ProductPackage>(this.AssignedProductPackage)); 
		//}

		//private void DeleteItemButton_Click(object sender, RoutedEventArgs e)
		//{
		//    this.DeleteItemInvoked(this, new EventArgs<ProductPackage>(this.AssignedProductPackage));
		//}
	}
}
