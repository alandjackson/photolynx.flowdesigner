﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Schema.LinqModel.DataContext;

namespace Flow.Controls.View.RecordDisplay
{
	/// <summary>
	/// Interaction logic for SubjectDatumControl.xaml
	/// </summary>
	public partial class SubjectDatumControl : UserControl
	{
		public static DependencyProperty IsReadOnlyProperty =
            DependencyProperty.Register("IsReadOnly", typeof(bool), typeof(SubjectDatumControl), new PropertyMetadata(new PropertyChangedCallback(OnPropertyChanged)));

        public event EventHandler<Flow.Lib.Helpers.EventArgs<bool>> BailBailBail = delegate { };


        static void OnPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            (obj as SubjectDatumControl).OnPropertyChanged(e);
           
           

        }

        
        void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            if (DataContext == null) return;

            if (IsReadOnly == false && ((SubjectDatum)DataContext).Field == "Last Name")
            {
                SetFocus();
            }
            if (IsReadOnly == false)
                this.SubjectDatumTextBox.SelectAll();
            CheckFieldLength();

            if (PropertyChanged == null) return;
            PropertyChanged(this, e);

            
        }

        public event DependencyPropertyChangedEventHandler PropertyChanged;

		public bool IsReadOnly
		{
			get {
                    return (bool)this.GetValue(IsReadOnlyProperty); 
            }
			set { 

                    this.SetValue(IsReadOnlyProperty, value); 
            }
		}


        //public static DependencyProperty IsFocusProperty =
        //    DependencyProperty.Register("IsFocus", typeof(bool), typeof(SubjectDatumControl), null);

        //public bool IsFocus
        //{
        //    get { return (bool)this.GetValue(IsFocusProperty); }
        //    set { this.SetValue(IsFocusProperty, value); this.SubjectDatumTextBox.Focus(); }
        //}

		public SubjectDatumControl()
		{
			InitializeComponent();
            
		}

        public void SetFocus()
        {
            this.SubjectDatumTextBox.Focus();
        }

        public bool ExceedsFieldLengthLimit
        {
            get
            {
                if (this.DataContext == null) return false;
                SubjectDatum subjectDatum = (this.DataContext as SubjectDatum);
                if (subjectDatum == null) return false;
                if (subjectDatum.Value == null) return false;

                int length = subjectDatum.Value.ToString().Length;
                int max = subjectDatum.FieldLengthLimit;

                if (max < 1) return false;

                return (bool)(length > max);
            }
        }

        private void CheckFieldLength()
        {
            if (this.SubjectDatumTextBox.Text.Contains(".nst."))
            {
                //bail
                //this.SubjectDatumTextBox.Text = this.SubjectDatumTextBox.Text.Split(".nst.")[0];
                
                this.SubjectDatumTextBox.Text = previousValue;
                this.BailBailBail(this, new Flow.Lib.Helpers.EventArgs<bool>(true));
            }

            this.fieldLengthWarning.Visibility = this.ExceedsFieldLengthLimit ? Visibility.Visible : Visibility.Collapsed;

            if (this.ExceedsFieldLengthLimit)
                this.fieldLengthWarning.ToolTip = String.Format("Field length may be truncated when sending to the lab ({0} character limit)", (this.DataContext as SubjectDatum).FieldLengthLimit);
        }

        private void SubjectDatumTextBox_KeyUp(object sender, KeyEventArgs e) { CheckFieldLength(); }
        private void SubjectDatumTextBox_LostFocus(object sender, RoutedEventArgs e) { CheckFieldLength(); }
        private void _this_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) { CheckFieldLength(); }
        string previousValue = "";
        private void SubjectDatumTextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.OemPeriod && !this.SubjectDatumTextBox.Text.Contains(".nst"))
                previousValue = this.SubjectDatumTextBox.Text;

        }

        private void SubjectDatumTextBox_GotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            if (IsReadOnly == false)
                this.SubjectDatumTextBox.SelectAll();
        }

       
	}

    public class BoolBoolLogicConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (
                values == null ||
                !(values[0] is Boolean) ||
                !(values[1] is Boolean) ||
                values[1] == null
            )
                return false;

            bool localReadOnly = (bool)values[0];
            bool FieldIsLocked = (bool)values[1];

            if (localReadOnly || FieldIsLocked)
                return true;

            return false;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }

    }

    public class BoolBoolReverseLogicConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (
                values == null ||
                !(values[0] is Boolean) ||
                !(values[1] is Boolean) ||
                values[1] == null
            )
                return !false;

            bool localReadOnly = (bool)values[0];
            bool FieldIsLocked = (bool)values[1];

            if (localReadOnly || FieldIsLocked)
                return !true;

            return !false;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }

    }
}
