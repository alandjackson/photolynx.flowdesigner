﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Lib.Helpers;
using Flow.Schema.LinqModel.DataContext;

namespace Flow.Controls.View.RecordDisplay
{

	/// <summary>
	/// Interaction logic for CollectionNavigator.xaml
	/// </summary>
	public partial class CollectionEditButtonStrip : UserControl
	{
		public event EventHandler<EditStateChangedEventArgs> EditStateChanged = delegate { };

		public event EventHandler<SearchInvokedEventArgs> SearchInvoked = delegate { };

		protected static DependencyProperty IsEditingDisabledProperty =
			DependencyProperty.Register("IsEditingDisabled", typeof(bool), typeof(CollectionEditButtonStrip));

		public bool IsEditingDisabled
		{
			get { return (bool)GetValue(IsEditingDisabledProperty); }
			set
			{
				SetValue(IsEditingDisabledProperty, value);
				if (value)
					this.EnterEditStateButtonVisibility = Visibility.Collapsed;
				else
					this.EnterEditStateButtonVisibility = Visibility.Visible;

				this.EditState = EditState.View;
			}
		}

		protected static DependencyProperty IsSaveEnabledProperty =
			DependencyProperty.Register("IsSaveEnabled", typeof(bool), typeof(CollectionEditButtonStrip), new PropertyMetadata(true));

		public bool IsSaveEnabled
		{
			get { return (bool)GetValue(IsSaveEnabledProperty); }
			set { SetValue(IsSaveEnabledProperty, value); }
		}

		protected static DependencyProperty IsDeleteEnabledProperty =
			DependencyProperty.Register("IsDeleteEnabled", typeof(bool), typeof(CollectionEditButtonStrip), new PropertyMetadata(true));

		public bool IsDeleteEnabled
		{
			get { return (bool)GetValue(IsDeleteEnabledProperty); }
			set { SetValue(IsDeleteEnabledProperty, value); }
		}

		protected static DependencyProperty EnterEditStateButtonVisibilityProperty =
			DependencyProperty.Register("EnterEditStateButtonVisibility", typeof(Visibility), typeof(CollectionEditButtonStrip));

		public Visibility EnterEditStateButtonVisibility
		{
			get { return (Visibility)GetValue(EnterEditStateButtonVisibilityProperty); }
			set { SetValue(EnterEditStateButtonVisibilityProperty, value); }
		}


		protected static DependencyProperty ExitEditStateButtonVisibilityProperty =
			DependencyProperty.Register("ExitEditStateButtonVisibility", typeof(Visibility), typeof(CollectionEditButtonStrip));

		public Visibility ExitEditStateButtonVisibility
		{
			get { return (Visibility)GetValue(ExitEditStateButtonVisibilityProperty); }
			set { SetValue(ExitEditStateButtonVisibilityProperty, value); }
		}

		protected static DependencyProperty AddItemButtonVisibilityProperty =
			DependencyProperty.Register("AddItemButtonVisibility", typeof(Visibility), typeof(CollectionEditButtonStrip));

		public Visibility AddItemButtonVisibility
		{
			get { return (Visibility)GetValue(AddItemButtonVisibilityProperty); }
			set { SetValue(AddItemButtonVisibilityProperty, value); }
		}


		protected static DependencyProperty EditItemButtonVisibilityProperty =
			DependencyProperty.Register("EditItemButtonVisibility", typeof(Visibility), typeof(CollectionEditButtonStrip));

		public Visibility EditItemButtonVisibility
		{
			get { return (Visibility)GetValue(EditItemButtonVisibilityProperty); }
			set { SetValue(EditItemButtonVisibilityProperty, value); }
		}

		private bool _deletionInvoked = false;

		private INavigable _sourceCollection;


		private EditState _editState;
		public EditState EditState
		{
			get { return _editState; }
			set
			{
				if (_editState != value)
				{
					EditState oldEditState = _editState;

					switch (value)
					{
						case EditState.ReadOnly:
							this.IsEnabled = true;
							//this.grdNavigationControls.IsEnabled = true;
							//uxNavigationSlider.IsEnabled = true;
							break;

						case EditState.View:
							if (
								oldEditState == EditState.Add	||
								oldEditState == EditState.Edit
							)
							{
								this.EditState = EditState.Cancel;
								return;
							}
							
							//this.grdNavigationControls.IsEnabled = true;
							//uxNavigationSlider.IsEnabled = true;

							if(this.IsEditingDisabled)
								this.EnterEditStateButtonVisibility = Visibility.Collapsed;
							else
								this.EnterEditStateButtonVisibility = Visibility.Visible;

							this.ExitEditStateButtonVisibility = Visibility.Collapsed;
							this.IsEnabled = true;
							break;

						case EditState.Delete:
							this.IsEnabled = false;
							break;

						case EditState.Edit:
						case EditState.Add:
							//this.grdNavigationControls.IsEnabled = false;
							//uxNavigationSlider.IsEnabled = false;
							this.EnterEditStateButtonVisibility = Visibility.Collapsed;
							this.ExitEditStateButtonVisibility = Visibility.Visible;
							this.IsEnabled = true;
							break;
					}

					_editState = value;

					this.EditStateChanged(this, new EditStateChangedEventArgs(oldEditState, _editState));
				}
			}
		}



		/// <summary>
		/// Constructor
		/// </summary>
		public CollectionEditButtonStrip()
		{
			InitializeComponent();
		}


		public void ResetEditState(EditState editState)
		{
			_editState = EditState.Unknown;
			this.EditState = editState;
		}


		#region Editing Button Click Event Handlers

		void AddItemButton_Click(object sender, RoutedEventArgs e)
		{
			this.EditState = EditState.Add;
		}

		void EditItemButton_Click(object sender, RoutedEventArgs e)
		{
			this.EditState = EditState.Edit;
		}

		void SaveItemButton_Click(object sender, RoutedEventArgs e)
		{
			Keyboard.Focus(sender as IInputElement);
			this.EditState = EditState.Save;
		}

		void DeleteItemButton_Click(object sender, RoutedEventArgs e)
		{
			this.EditState = EditState.Delete;
		}

		void CancelItemButton_Click(object sender, RoutedEventArgs e)
		{
			this.EditState = EditState.Cancel;
		}

		#endregion


		public void SetSourceCollection<T>(FlowObservableCollection<T> source)
			where T : class, INotifyPropertyChanged, INotifyPropertyChanging, new()
		{
			this.DataContext = source;

			if (source == null)
			{
				_sourceCollection = null;
				this.EditState = EditState.Unknown;
			}

			else
			{
				_sourceCollection = source;

				this.EditState = EditState.View;
			}
		}


	}		// END class


}		// END namespace
