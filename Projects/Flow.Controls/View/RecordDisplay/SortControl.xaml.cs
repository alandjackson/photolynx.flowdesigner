﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Flow.Controls.View.RecordDisplay
{
	/// <summary>
	/// Interaction logic for SortControl.xaml
	/// </summary>
	public partial class SortControl : UserControl
	{
		/// <summary>
		/// 
		/// </summary>
		protected static readonly DependencyProperty DisplayMemberPathProperty =
			DependencyProperty.Register("DisplayMemberPath", typeof(string), typeof(SortControl));

		public string DisplayMemberPath
		{
			get { return (string)this.GetValue(DisplayMemberPathProperty); }
			set { this.SetValue(DisplayMemberPathProperty, value); }
		}


		/// <summary>
		/// 
		/// </summary>
		protected static readonly DependencyProperty SelectedSortFieldProperty =
			DependencyProperty.Register("SelectedSortField", typeof(object), typeof(SortControl));

		public object SelectedSortField
		{
			get { return this.GetValue(SelectedSortFieldProperty); }
			set { this.SetValue(SelectedSortFieldProperty, value); }
		}


		/// <summary>
		/// 
		/// </summary>
		protected static readonly DependencyProperty OmissionItemProperty =
			DependencyProperty.Register("OmissionItem", typeof(object), typeof(SortControl),
			new PropertyMetadata(new PropertyChangedCallback(OnOmissionItemChanged))
		);

		protected static void OnOmissionItemChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
		{
			SortControl source = sender as SortControl;

			if (source != null)
			{
				ItemCollection items = source.FieldListComboBox.Items;
				
				ComboBoxItem oldItem =
					(ComboBoxItem)source.FieldListComboBox.ItemContainerGenerator.ContainerFromItem(e.OldValue);

				if(oldItem != null)
					oldItem.Visibility = Visibility.Visible;

				ComboBoxItem newItem =
					(ComboBoxItem)source.FieldListComboBox.ItemContainerGenerator.ContainerFromItem(e.NewValue);

				if (newItem != null)
				{
					if (newItem.IsSelected)
					{
						int itemIndex = items.IndexOf(e.NewValue);

						newItem.Visibility = Visibility.Collapsed;

						itemIndex++;

						if (itemIndex == items.Count)
							itemIndex -= 2;

						source.SelectedIndex = itemIndex;
					}
					else
						newItem.Visibility = Visibility.Collapsed;
				}
			}
		}

		public object OmissionItem
		{
			get { return this.GetValue(OmissionItemProperty); }
			set { this.SetValue(OmissionItemProperty, value); }
		}


		/// <summary>
		/// 
		/// </summary>
		protected static readonly DependencyProperty SelectedIndexProperty =
			DependencyProperty.Register("SelectedIndex", typeof(int), typeof(SortControl));

		public int SelectedIndex
		{
			get { return (int)this.GetValue(SelectedIndexProperty); }
			set { this.SetValue(SelectedIndexProperty, value); }
		}


		/// <summary>
		/// 
		/// </summary>
		protected static readonly DependencyProperty DisplayOmitSortProperty =
			DependencyProperty.Register("DisplayOmitSort", typeof(bool), typeof(SortControl), new PropertyMetadata(true));

		public bool DisplayOmitSort
		{
			get { return (bool)this.GetValue(DisplayOmitSortProperty); }
			set { this.SetValue(DisplayOmitSortProperty, value); }
		}

		/// <summary>
		/// 
		/// </summary>
		protected static readonly DependencyProperty SortOrderProperty =
			DependencyProperty.Register("SortOrder", typeof(string), typeof(SortControl), new PropertyMetadata("None"));

		public string SortOrder
		{
			get { return (string)this.GetValue(SortOrderProperty); }
			set { this.SetValue(SortOrderProperty, value); }
		}

		/// <summary>
		/// 
		/// </summary>
		public SortControl()
		{
			InitializeComponent();

			this.FieldListComboBox.ItemContainerGenerator.StatusChanged += new EventHandler(ItemContainerGenerator_StatusChanged);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		void ItemContainerGenerator_StatusChanged(object sender, EventArgs e)
		{
			if (this.FieldListComboBox.ItemContainerGenerator.Status == System.Windows.Controls.Primitives.GeneratorStatus.ContainersGenerated)
			{
				OnOmissionItemChanged(this, new DependencyPropertyChangedEventArgs(OmissionItemProperty, this.OmissionItem, this.OmissionItem));
			}
		}

		public SortDescription SortDescription
		{
			get
			{
				SortDescription retVal = new SortDescription();

				if (!this.OmitSortComboBoxItem.IsSelected)
				{
					retVal.PropertyName = this.FieldListComboBox.Text;
					retVal.Direction = ((string)this.SortOrderComboBox.SelectedValue == "Asc")
						? ListSortDirection.Ascending
						: ListSortDirection.Descending
					;
				}

				return retVal;
			}
		}
	
	}
}
