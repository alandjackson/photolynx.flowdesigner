﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Lib.Helpers;
using Flow.Schema.LinqModel.DataContext;
using System.Windows.Threading;
using NLog;

namespace Flow.Controls.View.RecordDisplay
{

	/// <summary>
	/// Interaction logic for CollectionNavigator.xaml
	/// </summary>
	public partial class CollectionNavigator : UserControl
	{
        private static Logger logger = LogManager.GetCurrentClassLogger();

		public event EventHandler<EditStateChangedEventArgs> EditStateChanged = delegate { };

		public event EventHandler<SearchInvokedEventArgs> SearchInvoked = delegate { };

		//public static DependencyProperty CurrentPositionProperty =
		//    DependencyProperty.Register("CurrentPosition", typeof(int), typeof(CollectionNavigator));

		//public static DependencyProperty RecordCountProperty =
		//    DependencyProperty.Register("RecordCount", typeof(int), typeof(CollectionNavigator));

		//protected static DependencyProperty SearchHasFocusProperty =
		//    DependencyProperty.Register("SearchHasFocus", typeof(bool), typeof(CollectionNavigator));

		//public bool SearchHasFocus
		//{
		//    get { return (bool)GetValue(SearchHasFocusProperty); }
		//    set { this.SetValue(SearchHasFocusProperty, value); }
		//}

		protected static DependencyProperty IsEditingDisabledProperty =
			DependencyProperty.Register("IsEditingDisabled", typeof(bool), typeof(CollectionNavigator));


		public bool IsEditingDisabled
		{
			get { return (bool)GetValue(IsEditingDisabledProperty); }
			set
			{
				SetValue(IsEditingDisabledProperty, value);
				if (value)
					this.EnterEditStateButtonVisibility = Visibility.Collapsed;
				else
					this.EnterEditStateButtonVisibility = Visibility.Visible;

				this.EditState = EditState.View;
			}
		}

		protected static DependencyProperty EnterEditStateButtonVisibilityProperty =
			DependencyProperty.Register("EnterEditStateButtonVisibility", typeof(Visibility), typeof(CollectionNavigator));

		public Visibility EnterEditStateButtonVisibility
		{
			get { return (Visibility)GetValue(EnterEditStateButtonVisibilityProperty); }
			set { SetValue(EnterEditStateButtonVisibilityProperty, value); }
		}


		protected static DependencyProperty ExitEditStateButtonVisibilityProperty =
			DependencyProperty.Register("ExitEditStateButtonVisibility", typeof(Visibility), typeof(CollectionNavigator));

		public Visibility ExitEditStateButtonVisibility
		{
			get { return (Visibility)GetValue(ExitEditStateButtonVisibilityProperty); }
			set { SetValue(ExitEditStateButtonVisibilityProperty, value); }
		}


		protected static DependencyProperty AddItemButtonVisibilityProperty =
			DependencyProperty.Register("AddItemButtonVisibility", typeof(Visibility), typeof(CollectionNavigator));

		public Visibility AddItemButtonVisibility
		{
			get { return (Visibility)GetValue(AddItemButtonVisibilityProperty); }
			set { SetValue(AddItemButtonVisibilityProperty, value); }
		}

        protected static DependencyProperty DeleteItemButtonVisibilityProperty =
            DependencyProperty.Register("DeleteItemButtonVisibility", typeof(Visibility), typeof(CollectionNavigator));

        public Visibility DeleteItemButtonVisibility
        {
            get { return (Visibility)GetValue(DeleteItemButtonVisibilityProperty); }
            set { SetValue(DeleteItemButtonVisibilityProperty, value); }
        }

		protected static DependencyProperty EditItemButtonVisibilityProperty =
			DependencyProperty.Register("EditItemButtonVisibility", typeof(Visibility), typeof(CollectionNavigator));

		public Visibility EditItemButtonVisibility
		{
			get { return (Visibility)GetValue(EditItemButtonVisibilityProperty); }
			set { SetValue(EditItemButtonVisibilityProperty, value); }
		}

		protected static DependencyProperty ClearFilterButtonVisibilityProperty =
			DependencyProperty.Register("ClearFilterButtonVisibility", typeof(Visibility), typeof(CollectionNavigator));

		public Visibility ClearFilterButtonVisibility
		{
			get { return (Visibility)GetValue(ClearFilterButtonVisibilityProperty); }
			set { SetValue(ClearFilterButtonVisibilityProperty, value); }
		}

		protected static DependencyProperty DataIsValidProperty =
			DependencyProperty.Register("DataIsValid", typeof(bool), typeof(CollectionNavigator), new PropertyMetadata(true));

		public bool DataIsValid
		{
			get { return (bool)GetValue(DataIsValidProperty); }
			set { SetValue(DataIsValidProperty, value); }
		}

		//protected static DependencyProperty EditSaveButtonSymbolProperty =
		//    DependencyProperty.Register("EditSaveButtonSymbol", typeof(string), typeof(CollectionNavigator));

		//protected static DependencyProperty DeleteCancelButtonSymbolProperty =
		//    DependencyProperty.Register("DeleteCancelButtonSymbol", typeof(string), typeof(CollectionNavigator));


		private bool _deletionInvoked = false;

		private bool _scanInvoked = false;

		private INavigable SourceCollection
		{
			get { return this.DataContext as INavigable; }
		}

		//public INavigable _sourceCollection
		//{
		//    get { return _sourceCollection; }
		//}


		//public event EventHandler CurrentChanged
		//{
		//    add { _collectionView.CurrentChanged += value; }
		//    remove { _collectionView.CurrentChanged -= value; }
		//}

		//public event RoutedEventHandler AddButtonClick
		//{
		//    add { btnAddItem.Click += value; }
		//    remove { btnAddItem.Click -= value; }
		//}

		//public event RoutedEventHandler EditButtonClick
		//{
		//    add { btnEditItem.Click += value; }
		//    remove { btnEditItem.Click -= value; }
		//}

		//public event RoutedEventHandler SaveButtonClick
		//{
		//    add { btnSaveItem.Click += value; }
		//    remove { btnSaveItem.Click -= value; }
		//}

		//public event RoutedEventHandler DeleteButtonClick
		//{
		//    add { btnDeleteItem.Click += value; }
		//    remove { btnDeleteItem.Click -= value; }
		//}

		//public event RoutedEventHandler CancelButtonClick
		//{
		//    add { btnCancelItem.Click += value; }
		//    remove { btnCancelItem.Click -= value; }
		//}

		//public int CurrentPosition
		//{
		//    get { return (int)GetValue(CurrentPositionProperty); }
		//    set { SetValue(CurrentPositionProperty, value + 1); }
		//}

		//public int RecordCount
		//{
		//    get { return (int)GetValue(RecordCountProperty); }
		//    set { SetValue(RecordCountProperty, value); }
		//}

		private EditState _editState;
		public EditState EditState
		{
			get { return _editState; }
			set
			{
                
                if (_editState != value)
                {
                    EditState oldEditState = _editState;

                    switch (value)
                    {
                        case EditState.ReadOnly:
                            this.IsEnabled = true;
                            this.grdNavigationControls.IsEnabled = true;
                            uxNavigationSlider.IsEnabled = true;
                            break;

                        case EditState.View:
                            if (
                                oldEditState == EditState.Add ||
                                oldEditState == EditState.Edit
                            )
                            {
                                this.EditState = EditState.Cancel;
                                return;
                            }

                            this.grdNavigationControls.IsEnabled = true;
                            uxNavigationSlider.IsEnabled = true;

                            if (this.IsEditingDisabled)
                                this.EnterEditStateButtonVisibility = Visibility.Collapsed;
                            else
                                this.EnterEditStateButtonVisibility = Visibility.Visible;

                            this.ExitEditStateButtonVisibility = Visibility.Collapsed;
                            this.IsEnabled = true;
                            break;

                        case EditState.Delete:
                            this.IsEnabled = false;
                            break;

                        case EditState.Edit:
                        case EditState.Add:
                            this.grdNavigationControls.IsEnabled = false;
                            uxNavigationSlider.IsEnabled = false;
                            this.EnterEditStateButtonVisibility = Visibility.Collapsed;
                            this.ExitEditStateButtonVisibility = Visibility.Visible;
                            this.IsEnabled = true;
                            break;
                    }

                    _editState = value;

                    logger.Info("Switched edit state from {0} to {1}", oldEditState.ToString(), value.ToString());

                    this.EditStateChanged(this, new EditStateChangedEventArgs(oldEditState, _editState));

                }
            
			}
		}



		//public string EditSaveButtonSymbol
		//{
		//    get { return (string)GetValue(EditSaveButtonSymbolProperty); }
		//    set { SetValue(EditSaveButtonSymbolProperty, value); }
		//}

		//public string DeleteCancelButtonSymbol
		//{
		//    get { return (string)GetValue(DeleteCancelButtonSymbolProperty); }
		//    set { SetValue(DeleteCancelButtonSymbolProperty, value); }
		//}

		//public object CurrentItem
		//{
		//    get { return _collectionView.CurrentItem; }
		//}


		/// <summary>
		/// Constructor
		/// </summary>
		public CollectionNavigator()
		{
			InitializeComponent();
			this.Loaded += new RoutedEventHandler(CollectionNavigator_Loaded);

			//this.btnAddItem.Click += new RoutedEventHandler(btnAddItem_Click);
			//this.btnEditItem.Click += new RoutedEventHandler(btnEditItem_Click);
			//this.btnSaveItem.Click += new RoutedEventHandler(btnSaveItem_Click);
			//this.btnDeleteItem.Click += new RoutedEventHandler(btnDeleteItem_Click);
			//this.btnCancelItem.Click += new RoutedEventHandler(btnCancelItem_Click);

			//this.uxNavigationSlider.ValueChanged += new RoutedPropertyChangedEventHandler<double>(uxNavigationSlider_ValueChanged);
		}

		void CollectionNavigator_Loaded(object sender, RoutedEventArgs e)
		{
			this.SetSearchFocus();
		}


		public void ResetEditState(EditState editState)
		{
			_editState = EditState.Unknown;
			this.EditState = editState;
		}


		#region Editing Button Click Event Handlers

		void AddItemButton_Click(object sender, RoutedEventArgs e)
		{
			this.EditState = EditState.Add;
		}

		void EditItemButton_Click(object sender, RoutedEventArgs e)
		{
			this.EditState = EditState.Edit;
		}

        public void SaveRecord()
        {
            this.EditState = EditState.Save;
        }
		void SaveItemButton_Click(object sender, RoutedEventArgs e)
		{
			this.EditState = EditState.Save;
		}

		void DeleteItemButton_Click(object sender, RoutedEventArgs e)
		{
			this.EditState = EditState.Delete;
		}

        public void CancelEdit()
        {
            this.EditState = EditState.Cancel;
        }
		void CancelItemButton_Click(object sender, RoutedEventArgs e)
		{
			this.EditState = EditState.Cancel;
		}

		#endregion


		//void _collectionView_CurrentChanged(object sender, EventArgs e)
		//{
		//    this.CurrentPosition = _collectionView.CurrentPosition;
		//}

		//void _collectionView_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
		//{
		//    this.RecordCount = _collectionView.Cast<object>().Count();
		//    //			_collectionView.MoveCurrentToFirst();
		//}

		//public void SetSourceCollection<T>(FlowObservableCollection<T> source)
		//    where T : class, INotifyPropertyChanged, INotifyPropertyChanging, new()
		//{
		//    //			bool collectionViewNull = _collectionView == null;
		//    //this.Dispatcher.Invoke((Action)delegate
		//    //{
		//    //    this.DataContext = source;
		//    //}, DispatcherPriority.DataBind);

		//    this.DataContext = source;

		//    if (source == null)
		//    {
		//        _sourceCollection = null;
		//        this.EditState = EditState.Unknown;
		//    }

		//    else
		//    {
		//        _sourceCollection = source;

		//        //_collectionView.CurrentChanged += new EventHandler(_collectionView_CurrentChanged);
		//        //_collectionView.CollectionChanged += new NotifyCollectionChangedEventHandler(_collectionView_CollectionChanged);

		//        //this.CurrentPosition = _collectionView.CurrentPosition;
		//        //this.RecordCount = source.Count;

		//        this.EditState = EditState.View;
		//    }
		//}

		private void btnMoveFirst_Click(object sender, RoutedEventArgs e)
		{
            logger.Info("Clicked Move to First Subject button");

			this.SourceCollection.MoveCurrentToFirst();

			//if (!_sourceCollection.IsEmpty)
			//        _sourceCollection.MoveCurrentToFirst();
		}

		private void btnMovePrevious_Click(object sender, RoutedEventArgs e)
		{
            logger.Info("Clicked Move to Previus Subject button");

			this.SourceCollection.MoveCurrentToPrevious();

			//if (!_sourceCollection.IsEmpty)
			//{
			//    if (_sourceCollection.CurrentPosition > 0)
			//        _sourceCollection.MoveCurrentToPrevious();
			//    else
			//        _sourceCollection.MoveCurrentToLast();
			//}
		}

		private void btnMoveNext_Click(object sender, RoutedEventArgs e)
		{
            logger.Info("Clicked Move to Next Subject button");

			this.SourceCollection.MoveCurrentToNext();

			//if (!_collectionView.IsEmpty)
			//{
			//    if (_collectionView.CurrentPosition < _collectionView.)
			//        _collectionView.MoveCurrentToNext();
			//    else
			//        _collectionView.MoveCurrentToFirst();
			//}
		}

		private void btnMoveLast_Click(object sender, RoutedEventArgs e)
		{
            logger.Info("Clicked Move to Last Subject button");

			this.SourceCollection.MoveCurrentToLast();

			//if (this.CurrentPosition < this.RecordCount)
			//    _sourceCollection.MoveCurrentToLast();
		}


		#region RECORD POSITION TEXTBOX EVENT HANDLERS

		private void txtRecordPosition_PreviewKeyDown(object sender, KeyEventArgs e)
		{
			if (e.Key == Key.Delete || e.Key == Key.Back)
				_deletionInvoked = true;
		}

		private void txtRecordPosition_KeyUp(object sender, KeyEventArgs e)
		{
			_deletionInvoked = false;
		}

		private void txtRecordPosition_TextChanged(object sender, TextChangedEventArgs e)
		{
			if (_deletionInvoked)
				ValidateCurrentPositionTextBoxInput(txtRecordPosition.Text);
		}

		private void txtRecordPosition_PreviewTextInput(object sender, TextCompositionEventArgs e)
		{
			int selectionStart = txtRecordPosition.SelectionStart;

			string provisionalRecordPositionText = txtRecordPosition.Text
				.Remove(selectionStart, txtRecordPosition.SelectionLength)
				.Insert(selectionStart, e.Text)
			;

			if (ValidateCurrentPositionTextBoxInput(provisionalRecordPositionText))
			{
				if (txtRecordPosition.Text == "" && provisionalRecordPositionText == this.SourceCollection.CurrentOrdinalPosition.ToString())
					txtRecordPosition.Text = provisionalRecordPositionText;

				txtRecordPosition.SelectionStart = selectionStart + e.Text.Length;
			}

			e.Handled = true;
		}

		private bool ValidateCurrentPositionTextBoxInput(string provisionalRecordPositionText)
		{
			int provisionalRecordPosition = 0;

			if (Int32.TryParse(provisionalRecordPositionText, out provisionalRecordPosition))
			{
				if (provisionalRecordPosition > 0 && provisionalRecordPosition <= this.SourceCollection.RecordCount)
				{
					this.SourceCollection.MoveCurrentToPosition(provisionalRecordPosition - 1);

					return true;
				}
			}

			return false;
		}

		private void txtRecordPosition_LostFocus(object sender, RoutedEventArgs e)
		{
			if (txtRecordPosition.Text == "")
				txtRecordPosition.Text = this.SourceCollection.CurrentOrdinalPosition.ToString();
		}

		#endregion

		private void SearchButton_Click(object sender, RoutedEventArgs e)
		{
            logger.Info("Clicked Search button");

			this.SearchInvoked(this, new SearchInvokedEventArgs(false, SearchTextBox.Text, cmbSearchField.SelectedValue as string));
		}

		private void SearchTextBox_GotFocus(object sender, RoutedEventArgs e)
		{
            logger.Info("Search text box focused");

			// NOTE: this is not operating as expected
			this.SearchTextBox.SelectAll();
		}

		private void SearchTextBox_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			//if (this.IsFocused)
			//{

			//    this.SearchTextBox.SelectAll();

			//    Keyboard.Focus(this.SearchTextBox);

			//    this.SearchTextBox.Focus();

			//    e.Handled = true;
			//}
		}

		private void SearchTextBox_PreviewKeyUp(object sender, KeyEventArgs e)
		{
			if (e.Key == Key.Enter)
			{
                if (SearchTextBox.Text.ToLower().StartsWith(".nst."))
                {
                    SearchTextBox.Text = "";
                    return;
                }
                this.SearchInvoked(this, new SearchInvokedEventArgs(_scanInvoked, SearchTextBox.Text, cmbSearchField.SelectedValue as string));
				_scanInvoked = false;
			}
		}

		private void ClearFilterButton_Click(object sender, RoutedEventArgs e)
		{
            logger.Info("Clicked Clear Filter button");

            this.SearchTextBox.Text = "";
			this.SearchInvoked(this, new SearchInvokedEventArgs(false, null, null));
		}

		public void SetSearchFocus()
		{
			Keyboard.Focus(this.SearchTextBox);
			this.SearchTextBox.SelectAll();
		}

        public void SetSearchString(string text)
        {
            if (text == null || text == "")
                logger.Info("Clearing search string");
            else
                logger.Info("Setting search string to '{0}'", text);

            this.SearchTextBox.Text = text;
        }

		public void InvokeScan(string scanText, bool endsWithCarriageReturn)
		{
			this.SearchTextBox.Text = scanText;
			this.SetSearchFocus();

			if (endsWithCarriageReturn)
				_scanInvoked = true;
			else
                this.SearchInvoked(this, new SearchInvokedEventArgs(true, scanText, cmbSearchField.SelectedValue as string));
		}

        private void cmbSearchField_DropDownOpened(object sender, EventArgs e)
        {
            if( ((sender as ComboBox).DataContext as SubjectObservableCollection).HasItems)
            {
                 ((sender as ComboBox).DataContext as SubjectObservableCollection)[0].FlowProjectDataContext.FlowProject.UpdateProjectSubjectFieldListForSearch();
                 //cmbSearchField.ItemsSource = ((sender as ComboBox).DataContext as SubjectObservableCollection)[0].FlowProjectDataContext.FlowProject.ProjectSubjectFieldListForSearch;
                }
        }

	}		// END class


	public enum EditState
	{
		Unknown,
		ReadOnly,
		View,
		Add,
		Edit,
		Save,
		Delete,
		Cancel
	}

	public class EditStateChangedEventArgs : EventArgs
	{
		public readonly EditState OldEditState;
		public readonly EditState NewEditState;

		public EditStateChangedEventArgs(EditState oldEditState, EditState newEditState)
		{
			OldEditState = oldEditState;
			NewEditState = newEditState;
		}
	}

	public class SearchInvokedEventArgs : EventArgs
	{
		public readonly bool IsDefiniteBarcodeScan;
		public readonly string Criterion;
        public readonly string FieldDesc;

		public SearchInvokedEventArgs(bool isDefiniteBarcodeScan, string criterion, string fieldDesc)
		{
			IsDefiniteBarcodeScan = isDefiniteBarcodeScan;
			Criterion = (criterion == null) ? null : criterion.Trim();
            FieldDesc = (fieldDesc == null) ? null : fieldDesc.Trim();
		}
	}

   
    
}		// END namespace
