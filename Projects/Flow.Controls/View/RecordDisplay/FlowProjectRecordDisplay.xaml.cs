﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Lib.Helpers;
using Flow.Schema.LinqModel.DataContext;

namespace Flow.Controls.View.RecordDisplay
{
    /// <summary>
    /// Interaction logic for ImageDataThumb.xaml
    /// </summary>
    public partial class FlowProjectRecordDisplay : UserControl
    {
		public event EventHandler<EventArgs<FlowProject>> EditProjectButton_Click = delegate { };
		public event EventHandler<EventArgs<FlowProject>> EditProjectPreferencesButton_Click = delegate { };
		public event EventHandler<EventArgs<FlowProject>> ArchiveProjectButton_Click = delegate { };
        public event EventHandler<EventArgs<FlowProject>> SubmitOrdersButton_Click = delegate { };
		public event EventHandler<EventArgs<FlowProject>> UploadProjectButton_Click = delegate { };
        public event EventHandler<EventArgs<FlowProject>> SubmitOnlineOrderDataButton_Click = delegate { };
        public event EventHandler<EventArgs<FlowProject>> DeleteProject_Click = delegate { };
        public event EventHandler<EventArgs<FlowProject>> ArchiveDeleteProject_Click = delegate { };
		
		public FlowProject AssignedFlowProject
		{
			get { return (this.DataContext as FlowProject); }
		}

        public bool BtnProjectPreferences
        {
            set { 
                if(value)
                    this.btnProjectPreferences.Visibility = Visibility.Visible;
                else
                    this.btnProjectPreferences.Visibility = Visibility.Collapsed;
            }
        }
        public bool BtnExport
        {
            set
            {
                if (value)
                    this.btnExport.Visibility = Visibility.Visible;
                else
                    this.btnExport.Visibility = Visibility.Collapsed;
            }
        }

        //public bool BtnUpload
        //{
        //    set
        //    {
        //        if (value)
        //            this.btnUpload.Visibility = Visibility.Visible;
        //        else
        //            this.btnUpload.Visibility = Visibility.Hidden;
        //    }
        //}

		public FlowProjectRecordDisplay()
        {
            InitializeComponent();
        }

		private void EditProject_Click(object sender, RoutedEventArgs e)
		{
            this.AssignedFlowProject.inProgress = true;
			this.EditProjectButton_Click(this, new EventArgs<FlowProject>(this.AssignedFlowProject));
            this.AssignedFlowProject.inProgress = false;
		}

		private void EditProjectPreferences_Click(object sender, RoutedEventArgs e)
		{
            this.AssignedFlowProject.inProgress = true;
			this.EditProjectPreferencesButton_Click(this, new EventArgs<FlowProject>(this.AssignedFlowProject));
            this.AssignedFlowProject.inProgress = false;
		}

        

		private void ArchiveProject_Click(object sender, RoutedEventArgs e)
		{
            this.AssignedFlowProject.inProgress = true;
			this.ArchiveProjectButton_Click(this, new EventArgs<FlowProject>(this.AssignedFlowProject));
            if (this.AssignedFlowProject != null) this.AssignedFlowProject.inProgress = false;
		}

        private void SubmitOrders_Click(object sender, RoutedEventArgs e)
        {
            this.AssignedFlowProject.inProgress = true;
            this.SubmitOrdersButton_Click(this, new EventArgs<FlowProject>(this.AssignedFlowProject));
            this.AssignedFlowProject.inProgress = false;
        }

		private void UploadProject_Click(object sender, RoutedEventArgs e)
		{
            this.AssignedFlowProject.inProgress = true;
			this.UploadProjectButton_Click(this, new EventArgs<FlowProject>(this.AssignedFlowProject));
            this.AssignedFlowProject.inProgress = false;
		}

        private void SubmitOnlineOrderData_Click(object sender, RoutedEventArgs e)
        {
            this.AssignedFlowProject.inProgress = true;
            this.SubmitOnlineOrderDataButton_Click(this, new EventArgs<FlowProject>(this.AssignedFlowProject));
            this.AssignedFlowProject.inProgress = false;
        }

        private void btnDeleteProject_Click(object sender, RoutedEventArgs e)
        {
            this.AssignedFlowProject.inProgress = true;
            this.DeleteProject_Click(this, new EventArgs<FlowProject>(this.AssignedFlowProject));
            if (this.AssignedFlowProject != null) this.AssignedFlowProject.inProgress = false;
        }

        private void btnArchiveProject_Click(object sender, RoutedEventArgs e)
        {
            this.AssignedFlowProject.inProgress = true;
            this.ArchiveDeleteProject_Click(this, new EventArgs<FlowProject>(this.AssignedFlowProject));
            if (this.AssignedFlowProject != null) this.AssignedFlowProject.inProgress = false;
        }

        private void btnEdit_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {

        }


	}
}
