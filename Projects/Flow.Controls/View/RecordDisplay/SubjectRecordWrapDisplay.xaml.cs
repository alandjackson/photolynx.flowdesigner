﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


using Flow.Lib.Helpers;
using Flow.Schema.LinqModel.DataContext;

namespace Flow.Controls.View.RecordDisplay
{
	/// <summary>
	/// Interaction logic for SubjectRecordWrapDisplay.xaml
	/// </summary>
	public partial class SubjectRecordWrapDisplay : UserControl, System.ComponentModel.INotifyPropertyChanged
	{

        #region INotifyPropertyChanged Members

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        protected void SendPropertyChanged(string propName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propName));
        }
        #endregion


		public event EventHandler<EventArgs<bool>> ValidationInvoked = delegate { };

        public event EventHandler<Flow.Lib.Helpers.EventArgs<bool>> BailBailBail = delegate { };

        

        public event EventHandler<Flow.Lib.Helpers.EventArgs<bool>> CheckChanged = delegate { };

		public static DependencyProperty CurrentSubjectProperty =
			DependencyProperty.Register("CurrentSubject", typeof(Subject), typeof(SubjectRecordWrapDisplay), null);

		public Subject CurrentSubject
		{
			get { return (Subject)this.GetValue(CurrentSubjectProperty); }
			set { this.SetValue(CurrentSubjectProperty, value); }
		}

       
		//public static DependencyProperty ProjectSubjectFieldListProperty =
		//    DependencyProperty.Register("ProjectSubjectFieldList", typeof(FlowObservableCollection<ProjectSubjectField>), typeof(SubjectRecordWrapDisplay), null);

		//public FlowObservableCollection<ProjectSubjectField> ProjectSubjectFieldList
		//{
		//    get { return (FlowObservableCollection<ProjectSubjectField>)this.GetValue(CurrentSubjectProperty); }
		//    set { this.SetValue(CurrentSubjectProperty, value); }
		//}
	
		public static DependencyProperty IsReadOnlyProperty =
		    DependencyProperty.Register("IsReadOnly", typeof(bool), typeof(SubjectRecordWrapDisplay), null);

		public bool IsReadOnly
		{
			get { return (bool)this.GetValue(IsReadOnlyProperty); }
			set
			{
                this.SetValue(IsReadOnlyProperty, value);
                this.IsEditable = !value;
			}
		}

        public static DependencyProperty PickListProperty =
            DependencyProperty.Register("PickList", typeof(List<string>), typeof(SubjectRecordWrapDisplay), null);
        public List<string> PickList
        {
            get { return (List<string>)this.GetValue(PickListProperty); }
            set
            {
                this.SetValue(PickListProperty, value);
            }
        }


        public static DependencyProperty TeacherListProperty =
            DependencyProperty.Register("TeacherList", typeof(List<string>), typeof(SubjectRecordWrapDisplay), null);
        public List<string> TeacherList
        {
            get { return (List<string>)this.GetValue(TeacherListProperty); }
            set
            {
                this.SetValue(TeacherListProperty, value);
            }
        }
        public static DependencyProperty ClassListProperty =
             DependencyProperty.Register("ClassList", typeof(List<string>), typeof(SubjectRecordWrapDisplay), null);
        public List<string> ClassList
        {
            get { return (List<string>)this.GetValue(ClassListProperty); }
            set
            {
                this.SetValue(ClassListProperty, value);
            }
        }
        public static DependencyProperty GradeListProperty =
            DependencyProperty.Register("GradeList", typeof(List<string>), typeof(SubjectRecordWrapDisplay), null);
        public List<string> GradeList
        {
            get { return (List<string>)this.GetValue(GradeListProperty); }
            set
            {
                this.SetValue(GradeListProperty, value);
            }
        }

        public void UpdatePickLists()
        {
            SendPropertyChanged("TeacherList");
            SendPropertyChanged("ClassList");
            SendPropertyChanged("GradeList");
        }


        public static DependencyProperty IsNewAddProperty =
            DependencyProperty.Register("IsNewAdd", typeof(bool), typeof(SubjectRecordWrapDisplay), null);

        public bool IsNewAdd
        {
            get { return (bool)this.GetValue(IsNewAddProperty); }
            set
            {
                this.SetValue(IsNewAddProperty, value);
            }
        }

		public static DependencyProperty IsEditableProperty =
			DependencyProperty.Register("IsEditable", typeof(bool), typeof(SubjectRecordWrapDisplay), null);

		public bool IsEditable
		{
			get { return (bool)this.GetValue(IsEditableProperty); }
			set { this.SetValue(IsEditableProperty, value); }
		}


		public static DependencyProperty SuppressNonProminentFieldsProperty =
			DependencyProperty.Register("SuppressNonProminentFields", typeof(bool), typeof(SubjectRecordWrapDisplay), null);

		public bool SuppressNonProminentFields
		{
			get { return (bool)this.GetValue(SuppressNonProminentFieldsProperty); }
			set { this.SetValue(SuppressNonProminentFieldsProperty, value); }
		}


		public SubjectRecordWrapDisplay()
		{
			InitializeComponent();
		}

        private void TextBox_KeyUp(object sender, KeyEventArgs e)
        {
            //if(this.Validate())
            //    this.ValidationInvoked(this, new EventArgs<bool>(true));
            this.ValidationInvoked(this, new EventArgs<bool>(this.Validate()));
        }

		private void TextBox_LostFocus(object sender, RoutedEventArgs e)
		{
			//if (!this.IsReadOnly)
			//{
			//    TextBox source = e.OriginalSource as TextBox;
	
			//    string fieldName = source.Tag.ToString();

			//    this.CurrentSubject.SubjectData[fieldName].Value = source.Text;
			//}

			//this.ValidationInvoked(this, new EventArgs<bool>(this.Validate()));
		}

		public bool Validate()
		{

           

			return IsValid(this.ProjectSubjectFieldsItemsControl);
           
		}

		public static bool IsValid(DependencyObject parent)
		{
			if (parent == null)
				return true;
			
			// Validate all the bindings on the parent
			bool valid = true;

			LocalValueEnumerator localValues;

			try
			{
				localValues = parent.GetLocalValueEnumerator();
			}
			catch(Exception exc)
			{
				throw exc;

				//return valid;
			}

            while (localValues.MoveNext()) 
            {
                LocalValueEntry entry = localValues.Current;
                if (valid == true && BindingOperations.IsDataBound(parent, entry.Property) && entry.Property.Name == "Text")
                {
                    if (!((SubjectDatum)((BindingExpression)entry.Value).DataItem).IsValid)
                        valid = false;

                    //Binding binding = BindingOperations.GetBinding(parent, entry.Property);
                    //foreach (ValidationRule rule in binding.ValidationRules)
                    //{

                    //    BindingExpression expression = BindingOperations.GetBindingExpression(parent, entry.Property);

                    //    SubjectDatum item = expression.DataItem as SubjectDatum;

                    //    ValidationResult result;

                    //    if (item != null && !item.IsValid)
                    //    {
                    //        result = new ValidationResult(false, item["Value"]);
                    //    }
                    //    else
                    //    {
                    //        result = rule.Validate(parent.GetValue(entry.Property), null);
                    //    }

                    //    if (!result.IsValid)
                    //    {
                    //        System.Windows.Controls.Validation.MarkInvalid(expression, new ValidationError(rule, expression, result.ErrorContent, null));
                    //        valid = false;
                    //    }
                    //}
                }
            }

			// Validate all the bindings on the children
			for (int i = 0; i != VisualTreeHelper.GetChildrenCount(parent); ++i)
			{
				DependencyObject child = VisualTreeHelper.GetChild(parent, i);
				valid = valid && (IsValid(child));
			}

            
			return valid;
		}
        public void FocusFirstField()
        {
           
            //this.ProjectSubjectFieldsItemsControl.Focus();
        }

        public void CheckRememberme()
        {
            FlowProject fp = (ProjectSubjectFieldsItemsControl as ItemsControl).DataContext as FlowProject;
            foreach (ProjectSubjectField field in fp.ProjectSubjectFieldList)
            {
                
                if(field.SubjectFieldDesc != "FirstName" && 
                    field.SubjectFieldDesc != "LastName" &&
                    field.SubjectFieldDesc != "TicketCode" &&
                    field.SubjectFieldDesc != "PackageSummary"
                    )
                {
                    if (field != null && field.RememberMe)
                        DoRememberMe(field);
                }
            }
        }

        private void SubjectDatumControl_BailBailBail(object sender, EventArgs<bool> e)
        {
            this.BailBailBail(this, new Flow.Lib.Helpers.EventArgs<bool>(true));

        }

        private void SubjectDatumBooleanControl_CheckChanged(object sender, EventArgs<bool> e)
        {
            this.CheckChanged(this, new Flow.Lib.Helpers.EventArgs<bool>(true));

        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            if (this.IsEditable)
            {
                ProjectSubjectField field = (sender as CheckBox).DataContext as ProjectSubjectField;
                DoRememberMe(field);
            }
        }

        private void DoRememberMe(ProjectSubjectField field)
        {
            FlowProject proj = (FlowProject)this.DataContext;
            //int targetInd = proj.SubjectList.Count - 1;
            if (proj.LastSubjectViewed != null &&
                (proj.SubjectList.CurrentItem.SubjectData[field.ProjectSubjectFieldDisplayName].Value == null ||
                String.IsNullOrWhiteSpace(proj.SubjectList.CurrentItem.SubjectData[field.ProjectSubjectFieldDisplayName].Value.ToString())))
                proj.SubjectList.CurrentItem.SubjectData[field.ProjectSubjectFieldDisplayName].Value = proj.LastSubjectViewed.SubjectData[field.ProjectSubjectFieldDisplayName].Value;

        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            //commenting out because we decided to not ever clear out the value on uncheck
            //if (this.IsEditable)
            //{
            //    ProjectSubjectField field = (sender as CheckBox).DataContext as ProjectSubjectField;
            //    FlowProject proj = (FlowProject)this.DataContext;
            //    proj.SubjectList.CurrentItem.SubjectData[field.ProjectSubjectFieldDisplayName].Value = "";
            //}
        }

	}



	/// <summary>
	/// 
	/// </summary>
	public class SubjectDataConverter : IMultiValueConverter
	{
		public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			if(
				values == null							||
				!(values[0] is string)					||
				!(values[1] is SubjectDataCollection)	||
				values[1] == null
			)
				return null;

			string field = (string)values[0];

			SubjectDataCollection subjectData = values[1] as SubjectDataCollection;

			return subjectData[field];
		}

		public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
		{
			return null;
		}

	}	// END class


	/// <summary>
	/// 
	/// </summary>
	public class NonProminentFieldValueConverter : IMultiValueConverter
	{
		public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			if (
				values == null ||
				!(values[0] is Boolean) ||
				!(values[1] is Boolean) ||
				values[1] == null
			)
				return Visibility.Visible;

			bool suppress = (bool)values[0];
			bool isProminent = (bool)values[1];

			if (suppress && !isProminent)
				return Visibility.Collapsed;

			return Visibility.Visible;
		}

		public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
		{
			return null;
		}

	}	// END class

   
	/// <summary>
	/// 
	/// </summary>
	public class DataTypeTemplateSelector : DataTemplateSelector
	{
		public DataTemplate TextDataTemplate { get; set; }
		public DataTemplate IntegerDataTemplate { get; set; }
		public DataTemplate DecimalDataTemplate { get; set; }
		public DataTemplate DateDataTemplate { get; set; }
        public DataTemplate BooleanDataTemplate { get; set; }
        public DataTemplate ComboDataTemplate { get; set; }

		public override DataTemplate SelectTemplate(object item, DependencyObject container)
		{
			if (item != null && item is ProjectSubjectField)
			{
				ProjectSubjectField field = item as ProjectSubjectField;

				if (field.SubjectFieldDataType.NativeType == typeof(int))
					return this.IntegerDataTemplate;

				if (field.SubjectFieldDataType.NativeType == typeof(decimal))
					return this.DecimalDataTemplate;

                if (field.SubjectFieldDataType.NativeType == typeof(bool))
                    return this.BooleanDataTemplate;

                if (field.IsPickListField)
                    return this.ComboDataTemplate;

				if (field.SubjectFieldDataType.NativeType == typeof(DateTime))
					return this.DateDataTemplate;

				return this.TextDataTemplate;
			}

			return null;
		}

	}

    class MultiBooleanToVisibilityConverter : IMultiValueConverter
    {
        public object Convert(object[] values,
                                Type targetType,
                                object parameter,
                                System.Globalization.CultureInfo culture)
        {
            bool visible = true;
            foreach (object value in values)
                if (value is bool)
                    visible = visible && (bool)value;

            if (visible)
                return System.Windows.Visibility.Visible;
            else
                return System.Windows.Visibility.Hidden;
        }

        public object[] ConvertBack(object value,
                                    Type[] targetTypes,
                                    object parameter,
                                    System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}	// END namespace
