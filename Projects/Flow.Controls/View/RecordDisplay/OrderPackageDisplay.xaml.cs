﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Lib.Helpers;
using Flow.Schema.LinqModel.DataContext;

namespace Flow.Controls.View.RecordDisplay
{
    /// <summary>
    /// Interaction logic for ImageDataThumb.xaml
    /// </summary>
    public partial class OrderPackageDisplay : UserControl
    {
		//public event EventHandler<OrderChangeOperationEventArgs> OrderChangeOperationInvoked = delegate { };
		public event EventHandler<EventArgs<OrderPackage>> DuplicateOrderPackageInvoked = delegate { };
		public event EventHandler<EventArgs<OrderPackage>> RemoveOrderPackageInvoked = delegate { };

		public event EventHandler<EventArgs<OrderProductNode>> AssignPreviousImageRequested = delegate { };
		public event EventHandler<EventArgs<OrderProductNode>> AssignNullImageRequested = delegate { };
		public event EventHandler<EventArgs<OrderProductNode>> AssignNextImageRequested = delegate { };
	
		public event EventHandler<EventArgs<OrderPackage>> OrderPackageSelected = delegate { };
		public event SelectionChangedEventHandler OptionListSelectionChanged = delegate { };
        public event SelectionChangedEventHandler OptionListMasterSelectionChanged = delegate { };


		protected static DependencyProperty OptionGroupsProperty = DependencyProperty.Register(
			"OptionGroups", typeof(List<ImageQuixOptionGroup>), typeof(OrderPackageDisplay)
		);

		public List<ImageQuixOptionGroup> OptionGroups
		{
			get { return (List<ImageQuixOptionGroup>)this.GetValue(OptionGroupsProperty); }
			set { this.SetValue(OptionGroupsProperty, value); }
		}


		public OrderPackage AssignedOrderPackage
		{
			get { return (this.DataContext as OrderPackage); }
		}

		public OrderPackageDisplay()
        {
            InitializeComponent();
            
        }

		private void DuplicateOrderPackageButton_Click(object sender, RoutedEventArgs e)
		{
			this.DuplicateOrderPackageInvoked(this, new EventArgs<OrderPackage>(this.AssignedOrderPackage));
		}

		private void RemoveOrderPackageButton_Click(object sender, RoutedEventArgs e)
		{
			this.RemoveOrderPackageInvoked(this, new EventArgs<OrderPackage>(this.AssignedOrderPackage));
		}

		private void OrderProductImageListDisplay_AssignNextImageRequested(object sender, EventArgs<OrderProductNode> e)
		{
			this.AssignNextImageRequested(sender, e);
		}

		private void OrderProductImageListDisplay_AssignNullImageRequested(object sender, EventArgs<OrderProductNode> e)
		{
			this.AssignNullImageRequested(sender, e);
		}

		private void OrderProductImageListDisplay_AssignPreviousImageRequested(object sender, EventArgs<OrderProductNode> e)
		{
			this.AssignPreviousImageRequested(sender, e);
		}

		private void OrderPackageDisplay_Click(object sender, RoutedEventArgs e)
		{
			this.OrderPackageSelected(this, new EventArgs<OrderPackage>(this.AssignedOrderPackage));
		}

		private void OrderProductOptionListDisplay_OptionListSelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			this.OptionListSelectionChanged(sender, e);
		}

        private void OrderProductOptionListDisplayMaster_OptionListSelectionChanged(object sender, SelectionChangedEventArgs e)
		{

			this.OptionListMasterSelectionChanged(sender, e);
		}
        private void cmbPackageGreenScreen_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
        }

        //bool initSelChng = true;
        private void lbxPackageGreenScreen_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.DataContext == null)
                return;
            if ((sender as ListBox).SelectedItem == null)
                return;
            //if (initSelChng == false) { initSelChng = true; return; }

            OrderPackage thisOP = this.DataContext as OrderPackage;
            string gsBack = ((sender as ListBox).SelectedItem as Flow.Schema.LinqModel.DataContext.FlowMaster.GreenScreenBackground).FullPath;
            SetOrderPackageBackground(thisOP, gsBack);
        }

        private void SetOrderPackageBackground(OrderPackage thisOP, string gsBack)
        {
            foreach (OrderProduct oProd in thisOP.OrderProducts)
            {
                oProd.GreenScreenBackground = gsBack;
                oProd.GreenScreenBackgroundObjectChanged();
            }
            thisOP.DefaultBackgroundChanged();
            thisOP.SubjectOrder.Subject.FlowProjectDataContext.SubmitChanges();
            DefaultBackgroundThumbnail.UpdateLayout();
            thisOP.ReCalculatePrice();
            thisOP.SubjectOrder.ReCalculateGrandTotal();
        }

        private void lbxPackageGreenScreen2_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.DataContext == null)
                return;
            //if (initSelChng == false) { initSelChng = true; return; }

            if ((sender as ListBox).SelectedItem == null)
                return;

            OrderProduct oProd = (sender as ListBox).DataContext as OrderProduct;
            oProd.GreenScreenBackground = ((sender as ListBox).SelectedItem as Flow.Schema.LinqModel.DataContext.FlowMaster.GreenScreenBackground).FullPath;
            oProd.GreenScreenBackgroundObjectChanged();
            oProd.OrderPackage.DefaultBackgroundChanged();
            oProd.OrderPackage.SubjectOrder.Subject.FlowProjectDataContext.SubmitChanges();
            oProd.OrderPackage.ReCalculatePrice();
            oProd.OrderPackage.SubjectOrder.ReCalculateGrandTotal();
        }

        public void ExpandBackgrounds()
        {
            cmbPackageGreenScreen.IsDropDownOpen = true;
        }

        private void _this_Loaded(object sender, RoutedEventArgs e)
        {
            OrderPackage thisOP = this.DataContext as OrderPackage;

            if (thisOP == null) return;

            if (thisOP.SubjectOrder == null)
            {
                this.RemoveOrderPackageInvoked(this, new EventArgs<OrderPackage>(this.AssignedOrderPackage));
                return;
            }

            if (thisOP.IsOnlineOrder)
            {
                //_this.Background = Brushes.Green;
                //gridAll.Background = Brushes.DarkGreen;
                //Brush b = new SolidColorBrush(Color.FromArgb(64, 245, 245, 245));
                Brush b = new SolidColorBrush(Color.FromArgb(64, 0, 100, 0));

                gridAll.Background = b;
                ActionGlyphContainer2.Visibility = Visibility.Collapsed;
            }

            if (thisOP.DefaultBackgound == null && (bool)thisOP.SubjectOrder.Subject.FlowProjectDataContext.FlowProject.ShowGreenScreen)
            {
                if (thisOP.SubjectOrder.OrderPackages != null && thisOP.SubjectOrder.OrderPackages.Count > 0 && thisOP.SubjectOrder.OrderPackages[0].DefaultBackgound != null)
                {
                    SetOrderPackageBackground(thisOP, thisOP.SubjectOrder.OrderPackages[0].DefaultBackgound.FullPath);
                    
                }
                else
                    ExpandBackgrounds();
            }

            foreach (OrderProduct oprod in thisOP.OrderProducts)
                if (string.IsNullOrEmpty(oprod.GreenScreenBackground))
                    oprod.GreenScreenBackgroundObject = thisOP.DefaultBackgound;
        }

        public event EventHandler<EventArgs<OrderPackage>> AssignMainPreviousImageRequested = delegate { };
        public event EventHandler<EventArgs<OrderPackage>> AssignMainNextImageRequested = delegate { };
        public event EventHandler<EventArgs<OrderPackage>> AssignMainNullImageRequested = delegate { };

       

        private void AssignPreviousImageButton_Click(object sender, RoutedEventArgs e)
        {
            Button source = sender as Button;
            if (source != null)
            {
                OrderPackage orderPackage = source.DataContext as OrderPackage;
                if (orderPackage != null)
                {
                    //this.AssignMainPreviousImageRequested(this, new EventArgs<OrderPackage>(orderPackage));
                    foreach (OrderProduct op in orderPackage.OrderProducts)
                    {
                        foreach (OrderProductNode orderProductNode in op.OrderProductNodes)
                        {

                            if (orderProductNode != null && orderProductNode.SubjectImage != null)
                                this.AssignPreviousImageRequested(this, new EventArgs<OrderProductNode>(orderProductNode));
                        }
                    }
                }
                orderPackage.UpdateFirstImage();
            }
        }

        private void AssignNullImageButton_Click(object sender, RoutedEventArgs e)
        {
            Button source = sender as Button;
            if (source != null)
            {
                OrderPackage orderPackage = source.DataContext as OrderPackage;
                if (orderPackage != null)
                {
                    //this.AssignMainNullImageRequested(this, new EventArgs<OrderPackage>(orderPackage));
                    foreach (OrderProduct op in orderPackage.OrderProducts)
                    {
                        foreach (OrderProductNode orderProductNode in op.OrderProductNodes)
                        {

                            if (orderProductNode != null && orderProductNode.SubjectImage != null)
                                this.AssignNullImageRequested(this, new EventArgs<OrderProductNode>(orderProductNode));
                        }
                    }
                }
                orderPackage.UpdateFirstImage();
            }
        }

        private void AssignNextImageButton_Click(object sender, RoutedEventArgs e)
        {
            Button source = sender as Button;
            if (source != null)
            {
                OrderPackage orderPackage = source.DataContext as OrderPackage;
                if (orderPackage != null)
                {
                    //this.AssignMainNextImageRequested(this, new EventArgs<OrderPackage>(orderPackage));
                    foreach (OrderProduct op in orderPackage.OrderProducts)
                    {
                        foreach (OrderProductNode orderProductNode in op.OrderProductNodes)
                        {

                            if (orderProductNode != null && orderProductNode.SubjectImage != null)
                                this.AssignNextImageRequested(this, new EventArgs<OrderProductNode>(orderProductNode));
                        }
                    }

                }
                orderPackage.UpdateFirstImage();
            }
        }
	}
}
