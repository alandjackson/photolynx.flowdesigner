﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Lib;
using Flow.Lib.Helpers;
using Flow.Schema.LinqModel.DataContext;

using Xceed.Wpf.DataGrid;

namespace Flow.Controls.View.RecordDisplay
{
	/// <summary>
	/// Interaction logic for XceedSubjectRecordDisplay.xaml
	/// </summary>
	public partial class XceedSubjectRecordDisplay : UserControl
	{
		public event EventHandler<EventArgs<string>> ImageUnassigned = delegate { };
	
		private FlowProject _currentFlowProject = null;
		private Subject _tempSubject = null;
		DataGridCollectionView _dataGridCollectionView = null;

		public static readonly DependencyProperty CurrentSubjectProperty
			= DependencyProperty.Register(
				"CurrentSubject", typeof(Subject), typeof(XceedSubjectRecordDisplay), null
			);

		public Subject CurrentSubject
		{
			get { return (Subject)this.GetValue(CurrentSubjectProperty); }
			set
			{
				if (value == null)
				{
					this.DataContext = null;
					this.SetValue(CurrentSubjectProperty, null);
					this.IsEnabled = false;
				}
				else
				{
					this.SetValue(CurrentSubjectProperty, value);
					this.DataContext = this.CurrentSubject;

					if (_dataGridCollectionView != null)
					{
						System.Data.DataRow row = _currentFlowProject.SubjectList.FindItemByID(this.CurrentSubject.SubjectID);

						_dataGridCollectionView.MoveCurrentTo(
							row
						);
					}

					uxSubjectImageList.ItemsSource = this.CurrentSubject.ImageList;

					this.IsEnabled = true;
				}
			}
		}

		public CollectionNavigator Navigator
		{
			get { return uxCollectionNavigator; }
		}

		public XceedSubjectRecordDisplay()
		{
			InitializeComponent();
		}

		public void SetDataContext(FlowProject currentFlowProject)
		{
			_currentFlowProject = currentFlowProject;

			uxCollectionNavigator.SetSourceCollection<Subject>(_currentFlowProject.SubjectList);

			this.CurrentSubject = currentFlowProject.SubjectList.CurrentItem as Subject;

			_dataGridCollectionView = new DataGridCollectionView(currentFlowProject.SubjectList.Table);

			_dataGridCollectionView.ItemProperties.Remove(_dataGridCollectionView.ItemProperties["SubjectID"]);
//			_dataGridCollectionView.ItemProperties["FormalName"].IsReadOnly = true;
//			_dataGridCollectionView.ItemProperties.Remove(_dataGridCollectionView.ItemProperties["FormalName"]);

			xcdSubjectCardView.ItemsSource = _dataGridCollectionView;

			//			uxSubjectExtended.ItemsSource = _currentFlowProject.ProjectSubjectFields;

			uxCollectionNavigator.ResetEditState(EditState.View);

			_currentFlowProject.SubjectList.CurrentChanged += new EventHandler<EventArgs<Subject>>(SubjectList_CurrentChanged);
		}

		void SubjectList_CurrentChanged(object sender, EventArgs<Subject> e)
		{
			if (this.CurrentSubject.IsDirty && this.CurrentSubject != _tempSubject)
				this.CurrentSubject.Save();

			this.CurrentSubject = e.Data;
		}

		//void uxCollectionNavigator_CurrentChanged(object sender, EventArgs e)
		//{
		//    if(this.CurrentSubject.IsDirty && this.CurrentSubject != _tempSubject)
		//        this.CurrentSubject.Save();

		//    this.CurrentSubject = uxCollectionNavigator.CurrentItem as Subject;
		//}

		private FlowImageCollection<FlowImage> GetImageList()
		{
			return
				FlowImageCollection<FlowImage>.GetFlowImageCollectionFromList(
					(from i in this.CurrentSubject.ImageList
					 select i.ImagePath
					).AsEnumerable<string>()
				);
		}

		private void SubjectExtendedField_TextChanged(object sender, TextChangedEventArgs e)
		{
			//if (this.CurrentSubject != null)
			//{
			//    TextBox source = e.Source as TextBox;
			//    if (source != null)
			//    {
			//        string columnName = source.Tag as string;

			//        this.CurrentSubject[columnName] = source.Text;
			//    }
			//}
		}

		private void uxSubjectImages_Drop(object sender, DragEventArgs e)
		{
			//if (e.Data is BitmapImage)
			//    return;
		}

		private void uxCollectionNavigator_EditStateChanged(object sender, EditStateChangedEventArgs e)
		{
			switch (e.OldEditState)
			{
				case EditState.View:

					switch (e.NewEditState)
					{
						case EditState.Add:
							_tempSubject = Subject.CreateNewSubject(_currentFlowProject);
							this.CurrentSubject = _tempSubject;
							_dataGridCollectionView.AddNew();
							_dataGridCollectionView.MoveCurrentTo(_dataGridCollectionView.CurrentAddItem);
							break;

						case EditState.Edit:
							break;

						case EditState.Delete:
							grdDeleteConfirmation.Visibility = Visibility.Visible;
							break;
					}
					break;


				case EditState.Add:

					switch (e.NewEditState)
					{
						case EditState.Save:
							_tempSubject.Save();
							uxCollectionNavigator.EditState = EditState.View;
							_tempSubject = null;

							break;

						case EditState.Cancel:
							_tempSubject = null;
							this.CurrentSubject = _currentFlowProject.SubjectList.CurrentItem;
							uxCollectionNavigator.EditState = EditState.View;
							break;
					}
					break;


				case EditState.Edit:
					switch (e.NewEditState)
					{
						case EditState.Save:
							uxCollectionNavigator.EditState = EditState.View;
							break;

						case EditState.Cancel:
							uxCollectionNavigator.EditState = EditState.View;
							break;
					}
					break;

				
				case EditState.Delete:
					grdDeleteConfirmation.Visibility = Visibility.Collapsed;
					break;

			}
		}

		private void btnConfirmDelete_Click(object sender, RoutedEventArgs e)
		{
			_currentFlowProject.SubjectList.DeleteCurrent();
			uxCollectionNavigator.EditState = EditState.View;
		}

		private void btnCancelDelete_Click(object sender, RoutedEventArgs e)
		{
			uxCollectionNavigator.EditState = EditState.View;
		}

		//private void uxSubjectImageList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
		//{
		//    System.Windows.Controls.Image selectedImage = e.OriginalSource as System.Windows.Controls.Image;

		//    if (selectedImage != null)
		//    {
		//        string imageFileName = selectedImage.Tag.ToString();
		//        this.CurrentSubject.DetachImage(imageFileName);	
		//        ImageUnassigned(this, new EventArgs<string>(imageFileName));
		//    }
		//}

		private void uxSubjectImageList_ImageDoubleClick(object sender, EventArgs<System.Windows.Controls.Image> e)
		{
			if(e.Data != null)
			{
				string imageFileName = e.Data.Tag.ToString();
				this.CurrentSubject.DetachImage(imageFileName);
				ImageUnassigned(this, new EventArgs<string>(imageFileName));
			}
		}

		private void uxSubjectImageList_ImageDoubleClick(object sender, EventArgs e)
		{
			var selectedImageFileNames = (
				from i in uxSubjectImageList.GetSelectedItems<Flow.Schema.LinqModel.DataContext.Image>()
				select i.ImagePath
			).ToList();

			foreach(string imageFileName in selectedImageFileNames)
			{
				this.CurrentSubject.DetachImage(imageFileName);
				ImageUnassigned(this, new EventArgs<string>(imageFileName));
			}
		
		}	}
}
