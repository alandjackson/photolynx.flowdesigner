﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Schema.LinqModel.DataContext;
using Flow.Lib.Helpers;

namespace Flow.Controls.View.RecordDisplay
{
    /// <summary>
    /// </summary>
    public partial class FlowCatalogRecordDisplay : UserControl
    {

        public event EventHandler<EventArgs<FlowCatalog>> DuplicateCatalog_Click = delegate { };

		public FlowCatalog AssignedFlowCatalog
		{
			get { return (this.DataContext as FlowCatalog); }
		}


		public FlowCatalogRecordDisplay()
        {
            InitializeComponent();
        }

        private void ControlButton_Click(object sender, RoutedEventArgs e)
        {
            if(this.AssignedFlowCatalog != null)
                this.AssignedFlowCatalog.Select();
        }


        private void btnDuplicateCatalog_Click(object sender, RoutedEventArgs e)
        {
            this.DuplicateCatalog_Click(this, new EventArgs<FlowCatalog>((this.DataContext as FlowCatalog)));
        }
	}
}
