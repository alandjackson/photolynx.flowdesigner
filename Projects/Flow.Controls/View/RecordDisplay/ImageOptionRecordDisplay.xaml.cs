﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Lib.Helpers;
using Flow.Schema.LinqModel.DataContext;

namespace Flow.Controls.View.RecordDisplay
{
    /// <summary>
    /// Interaction logic for ImageDataThumb.xaml
    /// </summary>
    public partial class ImageOptionRecordDisplay : UserControl
    {
        public event EventHandler<EventArgs<OrderImageOption>> DeleteImageOptionInvoked = delegate { };
        //public event EventHandler<EventArgs<ProductPackage>> AddImageOptionToAllInvoked = delegate { };
        //public event EventHandler<EventArgs<ProductPackage>> UnassignItemInvoked = delegate { };

      
        protected static DependencyProperty ImageOptionDisplayContextProperty = DependencyProperty.Register(
            "ImageOptionDisplayContext", typeof(ImageOptionItemDisplayContext), typeof(ImageOptionRecordDisplay),
            new PropertyMetadata(ImageOptionItemDisplayContext.ImageAssignment)
        );

        public ImageOptionItemDisplayContext ImageOptionDisplayContext
        {
            get { return (ImageOptionItemDisplayContext)this.GetValue(ImageOptionDisplayContextProperty); }
            set { this.SetValue(ImageOptionDisplayContextProperty, value); }
        }

        //protected static DependencyProperty AddToAllConfirmPanelVisibilityProperty = DependencyProperty.Register(
        //    "AddToAllConfirmPanelVisibility", typeof(Visibility), typeof(ProductPackageRecordDisplay),
        //    new PropertyMetadata(Visibility.Collapsed)
        //);

        //public Visibility AddToAllConfirmPanelVisibility
        //{
        //    get { return (Visibility)this.GetValue(AddToAllConfirmPanelVisibilityProperty); }
        //    set { this.SetValue(AddToAllConfirmPanelVisibilityProperty, value); }
        //}



        //public ProductPackage AssignedProductPackage
        //{
        //    get { return (this.DataContext as ProductPackage); }
        //}


        public ImageOptionRecordDisplay()
        {
            InitializeComponent();
            //((FlowImageOption)this.DataContext).ImageQuixImageOption
        }

        private void DeleteImageOptionButton_Click(object sender, RoutedEventArgs e)
        {
            this.DeleteImageOptionInvoked(this, new EventArgs<OrderImageOption>((OrderImageOption)this.DataContext));
        }

        //private void AddOrderPackageToAllButton_Click(object sender, RoutedEventArgs e)
        //{
        //    this.AddToAllConfirmPanelVisibility = Visibility.Visible;
        //}

        //private void ConfirmAddToAllButton_Click(object sender, RoutedEventArgs e)
        //{
        //    this.AddToAllConfirmPanelVisibility = Visibility.Collapsed;
        //    this.AddOrderPackageToAllInvoked(this, new EventArgs<ProductPackage>(this.AssignedProductPackage));
        //}

        //private void CancelAddToAllButton_Click(object sender, RoutedEventArgs e)
        //{
        //    this.AddToAllConfirmPanelVisibility = Visibility.Collapsed;
        //}

        private void EditImageOptionItemButton_Click(object sender, RoutedEventArgs e)
        {
            //this.AssignedProductPackage.Select();
            //this.EditItemInvoked(this, new EventArgs<ProductPackage>(this.AssignedProductPackage));
        }

        //private void DeleteItemButton_Click(object sender, RoutedEventArgs e)
        //{
        //    this.DeleteItemInvoked(this, new EventArgs<ProductPackage>(this.AssignedProductPackage));
        //}

        //private void AddProductButton_Click(object sender, RoutedEventArgs e)
        //{
        //    Button source = sender as Button;

        //    ProductPackageComposition composition = source.CommandParameter as ProductPackageComposition;

        //    composition++;
        //}

        //private void DeleteProductButton_Click(object sender, RoutedEventArgs e)
        //{
        //    Button source = sender as Button;

        //    ProductPackageComposition composition = source.CommandParameter as ProductPackageComposition;

        //    if ((--composition).Quantity == 0)
        //        this.AssignedProductPackage.DeleteComposition(composition);
        //}


        private void ControlButton_Click(object sender, RoutedEventArgs e)
        {
            //this.AssignedProductPackage.Select();
        }

        private void _this_Loaded(object sender, RoutedEventArgs e)
        {
            OrderImageOption thisOP = this.DataContext as OrderImageOption;

            if (thisOP == null) return;

            if (thisOP.isOnlineOrder)
            {
                //_this.Background = Brushes.Green;
                //gridAll.Background = Brushes.DarkGreen;
                //Brush b = new SolidColorBrush(Color.FromArgb(64, 245, 245, 245));
                Brush b = new SolidColorBrush(Color.FromArgb(64, 0, 100, 0));

                gridAll.Background = b;
            }
        }
	}


    public enum ImageOptionItemDisplayContext
    {
        ImageAssignment,
        ProductAssignment,
        OrderEntry
    }

}
