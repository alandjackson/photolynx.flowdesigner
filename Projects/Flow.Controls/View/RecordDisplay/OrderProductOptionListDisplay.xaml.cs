﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Schema.LinqModel.DataContext;


namespace Flow.Controls.View.RecordDisplay
{
	/// <summary>
	/// Interaction logic for OrderProductOptionListDisplay.xaml
	/// </summary>
	public partial class OrderProductOptionListDisplay : UserControl
	{
		public event SelectionChangedEventHandler OptionListSelectionChanged = delegate { };

		protected static DependencyProperty OptionGroupsProperty = DependencyProperty.Register(
			"OptionGroups", typeof(List<ImageQuixOptionGroup>), typeof(OrderProductOptionListDisplay)
		);

		public List<ImageQuixOptionGroup> OptionGroups
		{
			get { return (List<ImageQuixOptionGroup>)this.GetValue(OptionGroupsProperty); }
			set { this.SetValue(OptionGroupsProperty, value); }
		}

		protected static DependencyProperty OptionsProperty = DependencyProperty.Register(
			"Options", typeof(FlowObservableCollection<ImageQuixOption>), typeof(OrderProductOptionListDisplay)
		);

		public FlowObservableCollection<ImageQuixOption> Options
		{
			get { return (FlowObservableCollection<ImageQuixOption>)this.GetValue(OptionsProperty); }
			set { this.SetValue(OptionsProperty, value); }
		}	

		public OrderProductOptionListDisplay()
		{
			InitializeComponent();
		}

		private void OptionListComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			this.OptionListSelectionChanged(sender, e);
		}
	}


	/// <summary>
	/// 
	/// </summary>
	public class ImageQuixOptionListMultiConverter : IMultiValueConverter
	{

		#region IMultiValueConverter Members

		public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			if(
				values == null	||
				values.Count() != 2 ||
				!(values[0] is int) ||
				!(values[1] is List<ImageQuixOptionGroup>)
			)
				return null;

			int imageQuixOptionGroupPk = (int)values[0];
			List<ImageQuixOptionGroup> optionGroups = values[1] as List<ImageQuixOptionGroup>;

			ImageQuixOptionGroup optionGroup = optionGroups.FirstOrDefault(g => g.PrimaryKey == imageQuixOptionGroupPk);

			if (optionGroup == null)
				return null;
			
			return optionGroup.OptionDisplayList;
		}

		public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
		{
			throw new NotImplementedException();
		}

		#endregion

	}

}
