﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Schema.LinqModel.DataContext;
using Flow.Schema.Reports;


namespace Flow.Controls.View.RecordDisplay
{
	/// <summary>
	/// Interaction logic for ReportSubjectFieldSelectionControl.xaml
	/// </summary>
	public partial class ReportSubjectFieldSelectionControl : UserControl
	{
		private FlowReportView AssignedReportView
		{
			get { return this.DataContext as FlowReportView; }
		}
	
		public ReportSubjectFieldSelectionControl()
		{
			InitializeComponent();
		}

		private void AddFieldButton_Click(object sender, RoutedEventArgs e)
		{
			string selectedField = (sender as Button).CommandParameter as string;

			this.AssignedReportView.Add(selectedField);
		}	
		
		private void RemoveFieldButton_Click(object sender, RoutedEventArgs e)
		{
			ProjectSubjectField selectedField = (sender as Button).CommandParameter as ProjectSubjectField;

			selectedField.IsGroupField = false;

			this.AssignedReportView.Remove(selectedField);
		}

	}	// END class

}	// END namespace
