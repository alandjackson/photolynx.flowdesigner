﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Lib.Helpers;
using Flow.Schema.LinqModel.DataContext;

namespace Flow.Controls.View.RecordDisplay
{
    /// <summary>
    /// Interaction logic for ImageDataThumb.xaml
    /// </summary>
    public partial class ProductCatalogItemDisplay : UserControl
    {
		public event EventHandler<EventArgs<ProductPackage>> DeleteItemInvoked = delegate { };
		public event EventHandler<EventArgs<ProductPackage>> AssignItemInvoked = delegate { };

		protected static DependencyProperty DisplayContextProperty = DependencyProperty.Register(
			"DisplayContext", typeof(ProductCatalogItemDisplayContext), typeof(ProductCatalogItemDisplay)
		);

		public ProductCatalogItemDisplayContext DisplayContext
		{
			get { return (ProductCatalogItemDisplayContext)this.GetValue(DisplayContextProperty); }
			set { this.SetValue(DisplayContextProperty, value); }
		}
		
		public ProductPackage AssignedProductPackage
		{
			get { return (this.DataContext as ProductPackage); }
		}

		public ProductCatalogItemDisplay()
        {
            InitializeComponent();
        }

		private void DeleteItemButton_Click(object sender, RoutedEventArgs e)
		{
			this.DeleteItemInvoked(this, new EventArgs<ProductPackage>(this.AssignedProductPackage));
		}

		private void AssignItemButton_Click(object sender, RoutedEventArgs e)
		{
			this.AssignItemInvoked(this, new EventArgs<ProductPackage>(this.AssignedProductPackage));
		}

	}

	public enum ProductCatalogItemDisplayContext
	{
		CatalogAssignment,
		OrderEntry
	}



}
