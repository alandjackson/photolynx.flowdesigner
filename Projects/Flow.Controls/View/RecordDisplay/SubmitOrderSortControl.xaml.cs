﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Flow.Controls.View.RecordDisplay
{
	/// <summary>
	/// Interaction logic for SubmitOrderSortControl.xaml
	/// </summary>
	public partial class SubmitOrderSortControl : UserControl
	{
		protected static DependencyProperty SortField1Property = DependencyProperty.Register(
			"SortField1", typeof(string), typeof(SubmitOrderSortControl), new PropertyMetadata("[None]")
		);

		public string SortField1
		{
			get { return (string)this.GetValue(SortField1Property); }
			set { this.SetValue(SortField1Property, value); }
		}

		protected static DependencyProperty SortField2Property = DependencyProperty.Register(
			"SortField2", typeof(string), typeof(SubmitOrderSortControl), new PropertyMetadata("[None]")
		);

		public string SortField2
		{
			get { return (string)this.GetValue(SortField2Property); }
			set { this.SetValue(SortField2Property, value); }
		}


		public SubmitOrderSortControl()
		{
			InitializeComponent();
		}
	}
}
