﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Xceed.Wpf.Controls;
using System.ComponentModel;

namespace Flow.Controls.View.RecordDisplay
{
    /// <summary>
    /// Interaction logic for ImageFilter.xaml
    /// </summary>
    public partial class ImageFilter : UserControl
    {

        public event EventHandler<SelectionChangedEventArgs> ImageFilterChanged = delegate { };
        public event EventHandler<EventArgs> ImageFilterBeginDateChanged = delegate { };
        public event EventHandler<EventArgs> ImageFilterEndDateChanged = delegate { };
        public event EventHandler<EventArgs> ImageSourceChanged = delegate { };
       

        public ImageFilter()
        {
            InitializeComponent();

            DependencyPropertyDescriptor selectedDateChangedDescriptor =
            DependencyPropertyDescriptor.FromProperty(Xceed.Wpf.Controls.DatePicker.SelectedDateProperty, typeof(Xceed.Wpf.Controls.DatePicker));
            selectedDateChangedDescriptor.AddValueChanged(this.FilterBeginSearchDate, new EventHandler(FilterBeginDate_Changed));
            selectedDateChangedDescriptor.AddValueChanged(this.FilterEndSearchDate, new EventHandler(FilterEndDate_Changed));
        }

        private void cmbImageFilter_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {   
            this.ImageFilterChanged(sender, e);
        }

        void FilterBeginDate_Changed(object sender, EventArgs e)
        {
            this.ImageFilterBeginDateChanged(sender, e);
        }
        void FilterEndDate_Changed(object sender, EventArgs e)
        {
            this.ImageFilterEndDateChanged(sender, e);
        }

        private void cmbImageSource_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //this.ImageSourceChanged(sender, e);
        }
    }
}
