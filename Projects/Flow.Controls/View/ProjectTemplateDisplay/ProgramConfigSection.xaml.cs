﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Schema.LinqModel.FlowMaster;

namespace Flow.Controls.View.ProjectTemplateDisplay
{
	/// <summary>
	/// Interaction logic for ProgramConfigSection.xaml
	/// </summary>
	public partial class ProgramConfigSection : ProjectTemplateDisplayBase
	{
		public ProgramConfigSection()
		{
			InitializeComponent();
		}

		public override void SetDataContext(Flow.Schema.LinqModel.FlowMaster.FlowMasterDataContext dataContext)
		{
			base.SetDataContext(dataContext);

			cmbPackagePrograms.DisplayMemberPath = "ProductPackageDesc";
			cmbPackagePrograms.SelectedValuePath = "ProductPackageID";
			cmbPackagePrograms.ItemsSource =
				from p in this.FlowMasterDataContext.ProductPackages
				select p;
		}
	}
}
