﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Schema.LinqModel.DataContext;

namespace Flow.Controls.View.ProjectTemplateDisplay.EventTriggerControls
{
	/// <summary>
	/// Interaction logic for EventTriggerTypeGroupBox.xaml
	/// </summary>
	public partial class EventTriggerTypeGroupBox : UserControl
	{
		private int _eventTriggerTypeID;
	
		public EventTriggerTypeGroupBox()
		{
			InitializeComponent();
		}

		private void uxEventTrigger_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
			_eventTriggerTypeID = (int)(this.DataContext as EventTriggerType).EventTriggerTypeID;
		}

		public void SetEventTriggers(FlowMasterDataContext flowMasterDataContext)
		{
			uxEventTrigger.ItemsSource =
				from et in flowMasterDataContext.EventTriggers
				where et.EventTriggerTypeID == _eventTriggerTypeID
				select et;
		}

	
	}
}
