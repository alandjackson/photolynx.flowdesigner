﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Lib;
using Flow.Lib.Helpers;
using Flow.Schema.LinqModel.DataContext;

namespace Flow.Controls.View.RecordDisplay
{
	
	/// <summary>
	/// Interaction logic for SubjectRecordDisplay.xaml
	/// </summary>
	public partial class SubjectRecordDisplay : UserControl
	{
		public event EventHandler<EventArgs<string>> ImageUnassigned = delegate { };
	
		private FlowProjectDataContext _flowProjectDataContext = null;
		private Subject _tempSubject = null;

		public static readonly DependencyProperty CurrentSubjectProperty
			= DependencyProperty.Register(
				"CurrentSubject", typeof(Subject), typeof(SubjectRecordDisplay), null
			);

		public Subject CurrentSubject
		{
			get { return (Subject)this.GetValue(CurrentSubjectProperty); }
			set
			{
				if (value == null)
				{
					this.DataContext = null;
					this.SetValue(CurrentSubjectProperty, null);
					this.IsEnabled = false;
				}
				else
				{
					this.SetValue(CurrentSubjectProperty, value);
					this.DataContext = this.CurrentSubject;

					uxSubjectImageList.ItemsSource = this.CurrentSubject.ImageList;

					this.IsEnabled = true;
				}
			}
		}

		public CollectionNavigator Navigator
		{
			get { return uxCollectionNavigator; }
		}
	
		public SubjectRecordDisplay()
		{
			InitializeComponent();
		}

		public void SetDataContext(FlowProjectDataContext flowProjectDataContext)
		{
			_flowProjectDataContext = flowProjectDataContext;

			uxCollectionNavigator.SetSourceCollection<Subject>(_flowProjectDataContext.SubjectList);
			uxCollectionNavigator.CurrentChanged += new EventHandler(uxCollectionNavigator_CurrentChanged);

			this.CurrentSubject = uxCollectionNavigator.CurrentItem as Subject;

			uxSubjectExtended.ItemsSource = flowProjectDataContext.ProjectSubjectFields;

			uxCollectionNavigator.ResetEditState(EditState.View);
		}

		void uxCollectionNavigator_CurrentChanged(object sender, EventArgs e)
		{
			if(this.CurrentSubject.IsDirty && this.CurrentSubject != _tempSubject)
				this.CurrentSubject.Save();

			this.CurrentSubject = uxCollectionNavigator.CurrentItem as Subject;
		}

		private FlowImageCollection<FlowImage> GetImageList()
		{
			return
				FlowImageCollection<FlowImage>.GetFlowImageCollectionFromList(
					(from i in this.CurrentSubject.ImageList
					 select i.ImagePath
					).AsEnumerable<string>()
				);
		}

		private void SubjectExtendedField_TextChanged(object sender, TextChangedEventArgs e)
		{
			if (this.CurrentSubject != null)
			{
				TextBox source = e.Source as TextBox;
				if (source != null)
				{
					string columnName = source.Tag as string;

					this.CurrentSubject[columnName] = source.Text;
				}
			}
		}

		private void uxSubjectImages_Drop(object sender, DragEventArgs e)
		{
			//if (e.Data is BitmapImage)
			//    return;
		}

		private void uxCollectionNavigator_EditStateChanged(object sender, EditStateChangedEventArgs e)
		{
			switch (e.OldEditState)
			{
				case EditState.View:

					switch (e.NewEditState)
					{
						case EditState.Add:
							_tempSubject = Subject.CreateNewSubject(_flowProjectDataContext);
							this.CurrentSubject = _tempSubject;
							break;

						case EditState.Edit:
							break;

						case EditState.Delete:
							grdDeleteConfirmation.Visibility = Visibility.Visible;
							break;
					}
					break;


				case EditState.Add:

					switch (e.NewEditState)
					{
						case EditState.Save:
							_tempSubject.Save();
							uxCollectionNavigator.EditState = EditState.View;
							_tempSubject = null;

							break;

						case EditState.Cancel:
							_tempSubject = null;
							this.CurrentSubject = uxCollectionNavigator.CurrentItem as Subject;
							uxCollectionNavigator.EditState = EditState.View;
							break;
					}
					break;


				case EditState.Edit:
					switch (e.NewEditState)
					{
						case EditState.Save:
							uxCollectionNavigator.EditState = EditState.View;
							break;

						case EditState.Cancel:
							uxCollectionNavigator.EditState = EditState.View;
							break;
					}
					break;

				
				case EditState.Delete:
					grdDeleteConfirmation.Visibility = Visibility.Collapsed;
					break;

			}
		}

		private void btnConfirmDelete_Click(object sender, RoutedEventArgs e)
		{
			_flowProjectDataContext.DeleteSubject2(CurrentSubject);
			uxCollectionNavigator.EditState = EditState.View;
		}

		private void btnCancelDelete_Click(object sender, RoutedEventArgs e)
		{
			uxCollectionNavigator.EditState = EditState.View;
		}

		private void uxSubjectImageList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
		{
			System.Windows.Controls.Image selectedImage = e.OriginalSource as System.Windows.Controls.Image;

			if (selectedImage != null)
			{
				string imageFileName = selectedImage.Tag.ToString();
				this.CurrentSubject.DetachImage(imageFileName);	
				ImageUnassigned(this, new EventArgs<string>(imageFileName));
			}
		}

	}
}
