﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections;

using Flow.Lib.Helpers;

namespace Flow.Controls.ProgressNotificationControl
{
    /// <summary>
    /// Class ProgressNotificationControlContainer
    /// </summary>
    public partial class ProgressNotificationControlContainer : UserControl
    {
        #region Private objects
        private List<ProgressNotificationControlData> _notificationCollection;
        private double _top;
        private double _left;
        private double _topOffset;
        private double _leftOffset;
        private const double _separation = 4;
        private const string _childNamePrefix = "pnc_child_";
        private bool _isMouseDown;
        private TranslateTransform _translate = new TranslateTransform();
        #endregion

        #region Contructor & Destructor
        public ProgressNotificationControlContainer()
        {
            InitializeComponent();
            this._notificationCollection = new List<ProgressNotificationControlData>();
            TransformGroup _tg = new TransformGroup();
            _tg.Children.Add(_translate);
            this.RenderTransform = _tg;
        }

        ~ProgressNotificationControlContainer()
        {
            this._notificationCollection = null;
            this._translate = null;
        }
        #endregion

        #region Public properties
        public Visibility ContainerVisibility
        {
            get
            {
                return this.Visibility;
            }

            set
            {
                this.Visibility = value;
            }
        }
        #endregion

        #region Public methods
        /// <summary>
        /// Creates a new notification in the container using a ProgressNotificationInfo object
        /// </summary>
        /// <param name="pnInfo">ProgressNotificationInfo object</param>
        public void CreateNotification(ProgressNotificationControlData pnInfo)
        {
            ProgressNotificationControlChild child = null;
            try
            {
                if (this._notificationCollection.Contains(pnInfo, new ProgressNotificationInfoComparer()))
                {
                    this.UpdateNotification(pnInfo);
                }
                else
                {
                    child = new ProgressNotificationControlChild(pnInfo);
                    child.Name = _childNamePrefix + pnInfo.Id.ToString("N");
                    this.gridMain.RegisterName(_childNamePrefix + pnInfo.Id.ToString("N"), child);
					this._notificationCollection.Add(pnInfo);
					child.Completed += new EventHandler<EventArgs<ProgressNotificationControlData>>(child_Completed);
					addChildInGrid(child);
                }
            }
            finally
            {
                child = null;
            }
        }

		void child_Completed(object sender, Flow.Lib.Helpers.EventArgs<ProgressNotificationControlData> e)
		{
			this.RemoveNotification(e.Data);
		}


        /// <summary>
        /// Updates a notification in the notifications' container
        /// </summary>
        /// <param name="pnInfo">ProgressNotificationInfo object</param>
        public void UpdateNotification(ProgressNotificationControlData pnInfo)
        {
            if (this._notificationCollection.Contains(pnInfo))
            {
                this._notificationCollection[this._notificationCollection.IndexOf(pnInfo)] = pnInfo;
                this.updateChildInGrid(pnInfo);
            }
        }

        /// <summary>
        /// Removes a notification from the notifications' container
        /// </summary>
        /// <param name="pnInfo">ProgressNotificationInfo object</param>
        public void RemoveNotification(ProgressNotificationControlData pnInfo)
        {
            if (this._notificationCollection.Contains(pnInfo))
            {
                this._notificationCollection.Remove(pnInfo);
                removeChildFromGrid(pnInfo);
            }
        }

        /// <summary>
        /// Removes all notifications from the container
        /// </summary>
        public void RemoveAllNotifications()
        {
            this._notificationCollection.Clear();
            removeAllChildrenFromGrid();
        }
        #endregion

        #region Private methods
        /// <summary>
        /// Adds a ProgressNotificationControlChild object to the the container's grid,
        /// calculates its position in it and updates container's visual
        /// </summary>
        /// <param name="child">ProgressNotificationControlChild object</param>
        private void addChildInGrid(ProgressNotificationControlChild child)
        {
            child.VerticalAlignment = VerticalAlignment.Top;
            child.HorizontalAlignment = HorizontalAlignment.Left;
            this.HorizontalContentAlignment = HorizontalAlignment.Left;
            this.VerticalContentAlignment = VerticalAlignment.Top;

            double _top = (this._notificationCollection.Count - 1) * (child.Height + _separation);
            child.Margin = new Thickness(_separation, _top + _separation, 0, _separation);
            this.Height += (this._notificationCollection.Count <= 1 ? 0 : child.Height + _separation);
            this.gridMain.Height += (this._notificationCollection.Count <= 1 ? 0 : child.Height + _separation);

            this.gridMain.Children.Add(child);
            this.gridMain.UpdateLayout();
            this.UpdateLayout();
        }

        /// <summary>
        /// Updates a ProgressNotificationControlChild object in the container's grid
        /// using a ProgressNotificationInfo object with the updated data
        /// </summary>
        /// <param name="pnInfo">ProgressNotificationInfo object</param>
        private void updateChildInGrid(ProgressNotificationControlData pnInfo)
        {
            ProgressNotificationControlChild child;
            try
            {
                child = ((ProgressNotificationControlChild)this.gridMain.FindName(_childNamePrefix + pnInfo.Id.ToString("N")));
                //child.pnInfo = pnInfo;
                child.UpdatePnInfo(pnInfo);

                //if (pnInfo.ProgressBarValue >= 100)
                //    this.RemoveNotification(pnInfo);
            }
            finally
            {
                child = null;
            }
        }

        /// <summary>
        /// Removes a ProgressNotificationControlChild object from the container's grid
        /// and updates the container's visual
        /// </summary>
        /// <param name="pnInfo">ProgressNotificationInfo object</param>
        private void removeChildFromGrid(ProgressNotificationControlData pnInfo)
        {
            ProgressNotificationControlChild child;
            try
            {
                child = ((ProgressNotificationControlChild)this.gridMain.FindName(_childNamePrefix + pnInfo.Id.ToString("N")));
                this.gridMain.Children.Remove(child);

                this.Height = this.MinHeight;
                this.gridMain.Height = this.gridMain.MinHeight;
                for (int i = 0; i < this.gridMain.Children.Count; i++)
                {
                    child = ((ProgressNotificationControlChild)this.gridMain.Children[i]);
                    double _top = i * (child.Height + _separation);
                    child.Margin = new Thickness(_separation, _top + _separation, 0, _separation);

                    this.Height += (i < 1 ? 0 : child.Height + _separation);
                    this.gridMain.Height += (i < 1 ? 0 : child.Height + _separation);
                }
                this.gridMain.UpdateLayout();
                this.UpdateLayout();
            }
            finally
            {
                child = null;
            }

            if (this._notificationCollection.Count == 0)
                this.Visibility = Visibility.Collapsed;
        }

        /// <summary>
        /// Removes all ProgressNotificationControlChild objects from the container's grid
        /// </summary>
        private void removeAllChildrenFromGrid()
        {
            this.gridMain.Children.Clear();
            this.Height = this.MinHeight;
            this.gridMain.Height = this.gridMain.MinHeight;
            this.gridMain.UpdateLayout();
            this.UpdateLayout();
            if (this._notificationCollection.Count == 0)
                this.Visibility = Visibility.Collapsed;
        }
        #endregion

        #region Mouse Events
        protected void OnMouseMove(object sender, MouseEventArgs e)
        {
            if (_isMouseDown)
            {
                this._left = this._left + (e.GetPosition(this).X - this._leftOffset);
                this._top = this._top + (e.GetPosition(this).Y - this._topOffset);
                this._translate.Y = this._top;
            }
        }

        protected void OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            this._leftOffset = e.GetPosition(this).X;
            this._topOffset = e.GetPosition(this).Y;
            this._isMouseDown = true;
        }

        protected void OnMouseUp(object sender, MouseButtonEventArgs e)
        {
            this._isMouseDown = false;
        }
        #endregion

        #region Button Events
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.ContainerVisibility = Visibility.Collapsed;
        }
        #endregion
    }

    /// <summary>
    /// Sub Class ProgressNotificationInfoComparer
    /// Used to create a custom comparisson method for ProgressNotificationInfo objects
    /// </summary>
    class ProgressNotificationInfoComparer : IEqualityComparer<ProgressNotificationControlData>
    {
        public bool Equals(ProgressNotificationControlData x, ProgressNotificationControlData y)
        {
            if (Object.ReferenceEquals(x, y)) 
                return true;
            if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                return false;
            return x.Id == y.Id;
        }

        public int GetHashCode(ProgressNotificationControlData obj)
        {
            if (Object.ReferenceEquals(obj, null)) 
                return 0;
            int hashId = obj.Id == null ? 0 : obj.Id.GetHashCode();
            return hashId;
        }
    }
}
