﻿using System;
using System.ComponentModel;
using System.Windows;

using Flow.Lib;


namespace Flow.Controls.ProgressNotificationControl
{
    /// <summary>
    /// Class ProgressNotificationControlData
    /// Defines the data model for the control
    /// </summary>
    public class ProgressNotificationControlData : NotifyPropertyBase
    {
		public ProgressNotificationControlContainer ControlContainer { get; set; }
		

		private bool _isComplete = false;
		public bool IsComplete
		{
			get { return _isComplete; }
			set
			{
				_isComplete = value;
				this.SendPropertyChanged("IsComplete");
			}
		}


		public bool IsCanceled { get; set; }


		private bool _allowCancel = false;
		public bool AllowCancel
		{
			get { return _allowCancel; }
			set
			{
				_allowCancel = value;
				this.SendPropertyChanged("AllowCancel");
			}
		}

        private bool _doneButtonOn = false;
        public bool DoneButtonOn
        {
            get { return _doneButtonOn; }
            set
            {
                _doneButtonOn = value;
                this.SendPropertyChanged("DoneButtonOn");
            }
        }


		Guid _id;
        /// <summary>
        /// Notification's unique identifier
        /// </summary>
        public Guid Id
        {
			get { return this._id; }
			set
			{
				_id = value;
				this.SendPropertyChanged("Id");
			}
        }



        string _statusTextTop = String.Empty;
        /// <summary>
        /// Notification's top text or title
        /// </summary>
        public string StatusTextTop
        {
            get { return this._statusTextTop; }
            set 
            {
				_statusTextTop = value;
				this.SendPropertyChanged("StatusTextTop");
				this.SendPropertyChanged("StatusTextTopVisible");
            }
        }

        /// <summary>
        /// Top text visibility
        /// </summary>
        public Visibility StatusTextTopVisibility
        {
            get
			{
				return
					(String.IsNullOrEmpty(this.StatusTextTop) || this.ProgressBarVisibility == Visibility.Visible)
					? Visibility.Collapsed
					: Visibility.Visible;
			}
        }

        string _statusTextBottom = String.Empty;
        /// <summary>
        /// Notification's bottom text or subtitle
        /// </summary>
        public string StatusTextBottom
        {
			get { return _statusTextBottom; }
			set
			{
				_statusTextBottom = value;
				this.SendPropertyChanged("StatusTextBottom");
			}
        }

        System.Windows.Controls.Image _iconImage;
        /// <summary>
        /// Notification's icon image which appears in the left side of the box
        /// </summary>
        public System.Windows.Controls.Image IconImage
        {
            get {  return this._iconImage; }
            set 
            {
                _iconImage = value;
				this.SendPropertyChanged("IconImage");
            }
        }

        double _progressBarValue = 0.0;
        /// <summary>
        /// Notification's progress bar value
        /// </summary>
        public double ProgressBarValue
        {
            get { return this._progressBarValue; }
            set 
            {
                _progressBarValue = value;
				this.SendPropertyChanged("ProgressBarValue");

                this.ProgressBarVisibility = (_progressBarValue < 0) ? Visibility.Collapsed : Visibility.Visible;

				this.SendPropertyChanged("SpinnerVisibility");
			}
        }

		Visibility _progressBarVisible;
        /// <summary>
        /// Notification progress bar visibility
        /// </summary>
        public Visibility ProgressBarVisibility
        {
            get { return _progressBarVisible; }
            set 
            {
                _progressBarVisible = value;
				this.SendPropertyChanged("StatusTextTopVisibility");
				this.SendPropertyChanged("ProgressBarVisibility");
            }
        }

		/// <summary>
		/// 
		/// </summary>
		public Visibility SpinnerVisibility
		{
			get { return this.ProgressBarValue < 0 ? Visibility.Hidden : Visibility.Visible; }
		}


        System.Windows.Controls.Image _thumbImage;
        /// <summary>
        /// Notification's thumb image which appears in the right side of the box
        /// </summary>
        public System.Windows.Controls.Image ThumbsImage
        {
            get { return this._thumbImage; }
            set 
            {
                _thumbImage = value;
				this.SendPropertyChanged("ThumbsImage");
            }
        }

        /// <summary>
        /// Creates a ProgressNotificationInfo object and 
        /// automatically generates its unique identifier
        /// </summary>
        public ProgressNotificationControlData()
        {
            this.Id = Guid.NewGuid();
        }
	}
}