﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Lib.Helpers;

namespace Flow.Controls.ProgressNotificationControl
{
    /// <summary>
    /// Class ProgressNotificationControlChild
    /// </summary>
    partial class ProgressNotificationControlChild : UserControl
    {
		public event EventHandler<EventArgs<ProgressNotificationControlData>> Completed = delegate { };
		
		#region Private objects
		
		protected static DependencyProperty NotificationDataProperty = DependencyProperty.Register(
			"NotificationData", typeof(ProgressNotificationControlData), typeof(ProgressNotificationControlChild)
		);

		public ProgressNotificationControlData NotificationData
		{
			get { return (ProgressNotificationControlData)this.GetValue(NotificationDataProperty); }
			set { this.SetValue(NotificationDataProperty, value); }
		}

        #endregion

        #region Constructors
        public ProgressNotificationControlChild()
        {
            InitializeComponent();
//            this.progressBar.Visibility = Visibility.Collapsed;
        }

        public ProgressNotificationControlChild(ProgressNotificationControlData pnInfo)
        {
            InitializeComponent();
//            this.progressBar.Visibility = Visibility.Collapsed;
			this.NotificationData = pnInfo;
        }
        #endregion

        #region Public properties
		//public ProgressNotificationControlData pnInfo
		//{
		//    get
		//    {
		//        return this.NotificationData;
		//    }
		//    set
		//    {
		//        this.NotificationDataProperty = value;
		//        updateInfo();
		//    }
		//}

        public void UpdatePnInfo(ProgressNotificationControlData p)
        {
			this.NotificationData = p;
        }

		//public new double Height
		//{
		//    get 
		//    { 
		//        return gridNotification.Height; 
		//    }
		//}
        #endregion

        #region Private methods

		protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
		{
			base.OnPropertyChanged(e);

			if(e.Property.Name == "Opacity" && (double)e.NewValue == 0)
				this.Completed(this, new EventArgs<ProgressNotificationControlData>(this.NotificationData));
		}

		//private void updateInfo()
		//{
		//    //this.txtStatusTop.Text = this.NotificationData.StatusTextTop;
		//    //this.txtStatusTop.Visibility = this.NotificationData.StatusTextTopVisible ? Visibility.Visible : Visibility.Collapsed;
		//    //this.txtStatusBottom.Text = this.NotificationData.StatusTextBottom;
		//    //this.progressBar.Value = this.NotificationData.ProgressBarValue;
		//    //this.progressBar.Visibility = this.NotificationData.ProgressBarVisible ? Visibility.Visible : Visibility.Collapsed;

		//    //if (this.NotificationData.ProgressBarValue < 0)
		//    //{
		//    //    this.spinner.Visibility = Visibility.Hidden;
		//    //}
		//    //else
		//    //{
		//    //    this.spinner.Visibility = Visibility.Visible;
		//    //}

		//    //if (this.NotificationData.IconImage != null)
		//    //{
		//    //    this.imgIcon.BeginInit();
		//    //    this.imgIcon.Source = this.NotificationData.IconImage.Source;
		//    //    this.imgIcon.EndInit();
		//    //}

		//    //if (this.NotificationData.ThumbsImage != null)
		//    //{
		//    //    this.imgThumb.BeginInit();
		//    //    this.imgThumb.Source = this.NotificationData.ThumbsImage.Source;
		//    //    this.imgThumb.EndInit();
		//    //}
		//}
        #endregion

        private void uxPause_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
			this.Completed(this, new EventArgs<ProgressNotificationControlData>(this.NotificationData));
		}

        private void uxCancel_Click(object sender, RoutedEventArgs e)
        {

        }

		private void DoubleAnimation_Completed(object sender, EventArgs e)
		{
			this.Completed(this, new EventArgs<ProgressNotificationControlData>(this.NotificationData));
		}

		private void btnCancel_Click(object sender, RoutedEventArgs e)
		{
			this.NotificationData.IsCanceled = true;
		}

		//private void Clear()
		//{
		//    NotificationData.ControlContainer.RemoveNotification(NotificationData);
		//    NotificationData.ControlContainer = null;
		//}
	}
}
