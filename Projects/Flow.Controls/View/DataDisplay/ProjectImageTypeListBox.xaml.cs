﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Lib.Helpers;
using Flow.Schema.LinqModel.DataContext;

namespace Flow.Controls.View.DataDisplay
{
	/// <summary>
	/// Interaction logic for ImageTypeListBox.xaml
	/// </summary>
	public partial class ProjectImageTypeListBox : DragDropListBox<ProjectImageType>
	{
		public event EventHandler<EventArgs<ProjectImageType>> ProjectImageTypeRemove = delegate { };
	
		public ProjectImageTypeListBox()
		{
			InitializeComponent();
		}

		private void Control_DoubleClick(object sender, MouseButtonEventArgs e)
		{
			//FrameworkElement control = sender as FrameworkElement;

			//if(control != null)
			//    this.ProjectImageTypeRemove(this, new EventArgs<ProjectImageType>(control.DataContext as ProjectImageType));
		}
	}
}
