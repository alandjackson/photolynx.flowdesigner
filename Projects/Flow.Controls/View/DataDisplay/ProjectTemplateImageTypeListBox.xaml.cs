﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Lib.Helpers;
using Flow.Schema.LinqModel.DataContext;

namespace Flow.Controls.View.DataDisplay
{
	/// <summary>
	/// Interaction logic for ImageTypeListBox.xaml
	/// </summary>
	public partial class ProjectTemplateImageTypeListBox : DragDropListBox<ProjectTemplateImageType>
	{
		public event EventHandler<EventArgs<ProjectTemplateImageType>> ProjectTemplateImageTypeRemove = delegate { };
	
		public ProjectTemplateImageTypeListBox()
		{
			InitializeComponent();
		}

		private void Control_DoubleClick(object sender, MouseButtonEventArgs e)
		{
			FrameworkElement control = sender as FrameworkElement;

			if(control != null)
				this.ProjectTemplateImageTypeRemove(this, new EventArgs<ProjectTemplateImageType>(control.DataContext as ProjectTemplateImageType));
		}
	}
}
