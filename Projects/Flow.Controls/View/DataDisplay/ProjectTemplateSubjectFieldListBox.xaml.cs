﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Schema.LinqModel.DataContext;

namespace Flow.Controls.View.DataDisplay
{
	/// <summary>
	/// Interaction logic for SubjectFieldListBox.xaml
	/// </summary>
	public partial class ProjectTemplateSubjectFieldListBox : DragDropListBox<ProjectTemplateSubjectField>
	{
		public ProjectTemplateSubjectFieldListBox()
		{
			InitializeComponent();
		}

		protected new void OnDrop(DragEventArgs e)
		{
			//EntitySet<ProjectTemplateSubjectField> collection = (EntitySet<ProjectTemplateSubjectField>)this.ItemsSource;
			//if (e.Data.GetDataPresent(DataFormats.UnicodeText, true))
			//{
			//    // NOTE: An explicit cast operator (to ProjectTemplateSubjectField) exists for SubjectField
			//    // See SubjectField class def in FlowMasterDataContext.cs (Flow.Schema.LinqModel.DataContext)
			//    collection.Add((SubjectField)e.Data.GetData(DataFormats.UnicodeText, true));

			//    e.Effects =	((e.KeyStates & DragDropKeyStates.ControlKey) != 0)
			//        ? DragDropEffects.Copy : DragDropEffects.Move;

			    e.Handled = true;
			//}
		}	
	}
}
