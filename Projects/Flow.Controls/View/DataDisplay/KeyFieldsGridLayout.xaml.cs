﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Flow.Controls.Model;

namespace Flow.Controls.DataDisplay
{
    /// <summary>
    /// Interaction logic for KeyFieldsGridLayout.xaml
    /// </summary>
    public partial class KeyFieldsGridLayout : UserControl
    {
        public KeyFieldsGridLayout()
        {
            InitializeComponent();
        }

        public void EditCurrentRecord()
        {
            KeyFieldsGridLayoutData data;
            
            data = uxLeftDataCol.ItemsSource as KeyFieldsGridLayoutData;
            foreach (EditableFieldData edata in data)
                edata.IsEditing = true;
            uxLeftDataCol.ItemsSource = data;

            data = uxRightDataCol.ItemsSource as KeyFieldsGridLayoutData;
            foreach (EditableFieldData edata in data)
                edata.IsEditing = true;
            uxRightDataCol.ItemsSource = data;

        }


    }
}
