﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Data.Linq;

namespace Flow.Controls.View.DataDisplay
{
	/// <summary>
	/// Follow steps 1a or 1b and then 2 to use this custom control in a XAML file.
	///
	/// Step 1a) Using this custom control in a XAML file that exists in the current project.
	/// Add this XmlNamespace attribute to the root element of the markup file where it is 
	/// to be used:
	///
	///     xmlns:MyNamespace="clr-namespace:Flow.Controls.View.DataDisplay"
	///
	///
	/// Step 1b) Using this custom control in a XAML file that exists in a different project.
	/// Add this XmlNamespace attribute to the root element of the markup file where it is 
	/// to be used:
	///
	///     xmlns:MyNamespace="clr-namespace:Flow.Controls.View.DataDisplay;assembly=Flow.Controls.View.DataDisplay"
	///
	/// You will also need to add a project reference from the project where the XAML file lives
	/// to this project and Rebuild to avoid compilation errors:
	///
	///     Right click on the target project in the Solution Explorer and
	///     "Add Reference"->"Projects"->[Browse to and select this project]
	///
	///
	/// Step 2)
	/// Go ahead and use your control in the XAML file.
	///
	///     <MyNamespace:DragDropListBox/>
	///
	/// </summary>
	public class DragDropListBox<T> : ListBox, IDragDropControl<T> where T : class, new()
	{
		//static DragDropListBox()
		//{
		//    DefaultStyleKeyProperty.OverrideMetadata(typeof(DragDropListBox<T>), new FrameworkPropertyMetadata(typeof(DragDropListBox<T>)));
		//}

		private static DependencyProperty IsDownProperty = DependencyProperty.RegisterAttached(
			"IsDown", typeof(bool), typeof(DragDropListBox<T>), new FrameworkPropertyMetadata(false));
		private static DependencyProperty DraggingElementProperty = DependencyProperty.RegisterAttached(
			"DraggingElement", typeof(T), typeof(DragDropListBox<T>), new FrameworkPropertyMetadata(null));
		private static DependencyProperty StartPointProperty = DependencyProperty.RegisterAttached(
			"StartPoint", typeof(Point), typeof(DragDropListBox<T>), new FrameworkPropertyMetadata(default(Point)));


		public T GetSelectedItem(Point clickPoint)
		{
			UIElement element = this.InputHitTest(clickPoint) as UIElement;
			while (element != null)
			{
				if (element == this)
				{
					return null;
				}

				object item = this.ItemContainerGenerator.ItemFromContainer(element);

				bool itemFound = !object.ReferenceEquals(item, DependencyProperty.UnsetValue);

				if (itemFound)
				{
					return item as T;
				}
				else
				{
					element = VisualTreeHelper.GetParent(element) as UIElement;
				}
			}

			return null;
		}

		protected override void OnPreviewMouseLeftButtonDown(MouseButtonEventArgs e)
		{
			base.OnPreviewMouseLeftButtonDown(e);

			T item = GetSelectedItem(e.GetPosition(this));
			if (item != null)
			{
				this.SetValue(IsDownProperty, true);
				this.SetValue(DraggingElementProperty, item);
				this.SetValue(StartPointProperty, e.GetPosition(this));
			}	
		}

		protected override void OnPreviewMouseMove(MouseEventArgs e)
		{
			base.OnPreviewMouseMove(e);

			bool isDown = (bool)this.GetValue(IsDownProperty);
			if (!isDown)
			{
				return;
			}

			Point startPoint = (Point)this.GetValue(StartPointProperty);

			if (Math.Abs(e.GetPosition(this).X - startPoint.X) > SystemParameters.MinimumHorizontalDragDistance ||
				Math.Abs(e.GetPosition(this).Y - startPoint.Y) > SystemParameters.MinimumVerticalDragDistance)
			{
				DragStarted();
			}
		}
	
		protected override void OnPreviewMouseLeftButtonUp(MouseButtonEventArgs e)
		{
			base.OnPreviewMouseLeftButtonUp(e);

			this.ClearValue(IsDownProperty);
			this.ClearValue(DraggingElementProperty);
			this.ClearValue(StartPointProperty);
		}

		private void DragStarted()
		{
			this.ClearValue(IsDownProperty); // SetValue to false would also work.

			// Add the bound item, available as DraggingElement, to a DataObject, however we see fit.
			T draggingElement = this.GetValue(DraggingElementProperty) as T;
			DataObject d = new DataObject(DataFormats.UnicodeText, draggingElement);
			DragDropEffects effects = DragDrop.DoDragDrop(this, d, DragDropEffects.Copy | DragDropEffects.Move);
			if ((effects & DragDropEffects.Move) != 0)
			{
				// Move rather than copy, so we should remove from bound list.
				//ObservableCollection<T> collection = (ObservableCollection<T>)this.ItemsSource;
				//collection.Remove(draggingElement);
			}
		}


		protected override void OnDrop(DragEventArgs e)
		{
			//Table<T> collection = (Table<T>)this.ItemsSource;
			//if (e.Data.GetDataPresent(DataFormats.UnicodeText, true))
			//{
			//    collection.InsertOnSubmit((T)e.Data.GetData(DataFormats.UnicodeText, true));
			//    e.Effects =
			//        ((e.KeyStates & DragDropKeyStates.ControlKey) != 0) ?
			//        DragDropEffects.Copy : DragDropEffects.Move;
			//    e.Handled = true;
			//}
		}

	}
}
