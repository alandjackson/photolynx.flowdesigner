﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Lib.Helpers;
using Flow.Schema.LinqModel.DataContext;

namespace Flow.Controls.View.DataDisplay
{
	/// <summary>
	/// Interaction logic for ImageTypeListBox.xaml
	/// </summary>
	public partial class ImageTypeListBox : DragDropListBox<ImageType>
	{
		public event EventHandler<EventArgs<ImageType>> ImageTypeSelectionChanged = delegate { };
	
		public ImageTypeListBox()
		{
			InitializeComponent();
		}

		private void Control_Click(object sender, MouseButtonEventArgs e)
		{
			FrameworkElement control = sender as FrameworkElement;

			if (control != null)
			{
				this.ImageTypeSelectionChanged(this, new EventArgs<ImageType>(control.DataContext as ImageType));
			}
		}
	}
}