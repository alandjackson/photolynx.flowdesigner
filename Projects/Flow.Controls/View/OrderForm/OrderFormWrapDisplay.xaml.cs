﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Flow.Schema.LinqModel.DataContext;



namespace Flow.Controls.View.OrderForm
{
    /// <summary>
    /// Interaction logic for OrderFormWrapDisplay.xaml
    /// </summary>
    public partial class OrderFormWrapDisplay : UserControl
    {
        public OrderFormWrapDisplay()
        {
            InitializeComponent();
        }

        public static DependencyProperty OrderFormFieldListProperty =
            DependencyProperty.Register("OrderFormFieldList", typeof(FlowObservableCollection<OrderFormFieldLocal>), typeof(OrderFormWrapDisplay), null);

        public FlowObservableCollection<OrderFormFieldLocal> OrderFormFieldList
        {
            get { return (FlowObservableCollection<OrderFormFieldLocal>)this.GetValue(OrderFormFieldListProperty); }
            set { this.SetValue(OrderFormFieldListProperty, value); }
        }

        public static DependencyProperty IsReadOnlyProperty =
            DependencyProperty.Register("IsReadOnly", typeof(bool), typeof(OrderFormWrapDisplay), null);

        public bool IsReadOnly
        {
            get { return (bool)this.GetValue(IsReadOnlyProperty); }
            set
            {
                this.SetValue(IsReadOnlyProperty, value);
                this.IsEditable = !value;
            }
        }

        public static DependencyProperty IsEditableProperty =
            DependencyProperty.Register("IsEditable", typeof(bool), typeof(OrderFormWrapDisplay), null);

        public bool IsEditable
        {
            get { return (bool)this.GetValue(IsEditableProperty); }
            set { this.SetValue(IsEditableProperty, value); }
        }

    }



    public class BooleanTextConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is string)
            {
                string sValue = (string)value;
                string[] possibleValues = {"yes", "true", "y", "t"};
                if(possibleValues.Contains(sValue.ToLower()))
                    return true;
                else
                    return false;
            }
            else
            {
                throw new ArgumentException("PickList Converter: Value Must Be String Type");
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is Boolean)
            {
                Boolean bValue = (Boolean)value;
                if (bValue)
                    return "YES";
                else
                    return "NO";
            }
            else
            {
                throw new ArgumentException("PickList Converter: Value Must Be Boolean Type");
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class PickListStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is string)
            {
                List<string> rList = new List<string>();
                rList.Add("");
                rList.AddRange(((string)value).Split(',').ToList<string>());
                return rList;
            }
            else
            {
                throw new ArgumentException("PickList Converter: Value Must Be String Type");
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class DataTypeTemplateSelector : DataTemplateSelector
    {
        public DataTemplate TextDataTemplate { get; set; }
        public DataTemplate BooleanDataTemplate { get; set; }
        public DataTemplate PickListDataTemplate { get; set; }
        public DataTemplate TextBlockDataTemplate { get; set; }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            if (item != null)
            {
                OrderFormFieldLocal field = item as OrderFormFieldLocal;

                if (field.FieldType == 1)
                    return this.TextDataTemplate;
                if (field.FieldType == 2)
                    return this.BooleanDataTemplate;
                if (field.FieldType == 3)
                {
                   return this.PickListDataTemplate;
                }
                if (field.FieldType == 4)
                    return this.TextBlockDataTemplate;

               

                return this.TextDataTemplate;
            }

            return null;
        }

    }
}
