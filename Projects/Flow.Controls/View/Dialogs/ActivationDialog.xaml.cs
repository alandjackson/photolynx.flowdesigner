﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Forms;
using Flow.Schema.LinqModel.DataContext;

namespace Flow.Controls.View.Dialogs
{
    /// <summary>
    /// Interaction logic for ImagePrintSetup.xaml
    /// </summary>
    public partial class ActivationDialog : System.Windows.Controls.UserControl
    {
        public FlowMasterDataContext FlowMasterDataContext { get; set; }
        
        public ActivationDialog()
        {
            InitializeComponent();
        }

        private void uxOk_Click(object sender, RoutedEventArgs e)
        {
            Window w = Window.GetWindow(this);
            if (w == null)
                return;
            w.DialogResult = true;
        }

    }
}

