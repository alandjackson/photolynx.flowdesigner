﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Flow.Controls.View.Dialogs
{
    /// <summary>
    /// Interaction logic for FlowMessageDialog.xaml
    /// </summary>
    public partial class FlowMessageDialog : Window
    {
        public FlowMessageDialog(string title, string message)
        {
            InitializeComponent();
            this.Title = title;
            lblTitle.Content = title;
            txtMessage.Text = message;
        }

        public bool CancelButtonVisible
        {
            get { return buttonCancel.Visibility == Visibility.Visible; }
            set
            {
                if (value)
                    buttonCancel.Visibility = Visibility.Visible;
                else
                    buttonCancel.Visibility = Visibility.Hidden;
            }
        }

        private void buttonOkay_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void buttonCancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }


    }
}
