﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Lib;
using Flow.Lib.Helpers;
using Flow.Schema.LinqModel.DataContext;

namespace Flow.Controls.View.ImageDisplay
{
	/// <summary>
	/// Interaction logic for FlowImageListBox.xaml
	/// </summary>
	public partial class FlowImageListBoxEditTemp : UserControl
	{
//		public event EventHandler ImageDoubleClick = delegate { };

		public event EventHandler<EventArgs<SubjectImage>> OpenAdjustmentsInvoked = delegate { };
		public event EventHandler<EventArgs<SubjectImage>> UnassignImageInvoked = delegate { };
		public event EventHandler<EventArgs<SubjectImage>> DeleteImageInvoked = delegate { };
        public event EventHandler<EventArgs<SubjectImage>> OpenExternalEditorInvoked = delegate { };
        public event EventHandler<EventArgs<SubjectImage>> RefreshImageInvoked = delegate { };
        public event EventHandler<EventArgs<SubjectImage>> ToggleGreenscreenInvoked = delegate { };
        public event EventHandler<EventArgs<SubjectImage>> ToggleFlagImageInvoked = delegate { };
        public event EventHandler<EventArgs<SubjectImage>> ToggleRemoteImageServiceInvoked = delegate { };

		public event SelectionChangedEventHandler ListBoxSelectionChanged
		{
			add { lstImageListBox.SelectionChanged += value; }
			remove { lstImageListBox.SelectionChanged -= value; }
		}


		public static DependencyProperty OrientationProperty =
			DependencyProperty.Register("Orientation", typeof(Orientation), typeof(FlowImageListBoxEditTemp));
	
		public Orientation Orientation
		{
			get { return (Orientation)GetValue(OrientationProperty); }
			set { SetValue(OrientationProperty, value); }
		}


		public static DependencyProperty HorizontalScrollBarVisibilityProperty =
			DependencyProperty.Register("HorizontalScrollBarVisibility", typeof(ScrollBarVisibility), typeof(FlowImageListBoxEditTemp));

		public ScrollBarVisibility HorizontalScrollBarVisibility
		{
			get { return (ScrollBarVisibility)GetValue(HorizontalScrollBarVisibilityProperty); }
			set { SetValue(HorizontalScrollBarVisibilityProperty, value); }
		}


		public static DependencyProperty VerticalScrollBarVisibilityProperty =
			DependencyProperty.Register("VerticalScrollBarVisibility", typeof(ScrollBarVisibility), typeof(FlowImageListBoxEditTemp));

		public ScrollBarVisibility VerticalScrollbarVisibility
		{
			get { return (ScrollBarVisibility)GetValue(VerticalScrollBarVisibilityProperty); }
			set { SetValue(VerticalScrollBarVisibilityProperty, value); }
		}


		public static DependencyProperty ItemsSourceProperty =
			DependencyProperty.Register("ItemsSource", typeof(IEnumerable), typeof(FlowImageListBoxEditTemp));

		public IEnumerable ItemsSource
		{
			get { return (IEnumerable)this.GetValue(ItemsSourceProperty); }
			set
			{
				this.SetValue(ItemsSourceProperty, value);
				lstImageListBox.ItemsSource = this.ItemsSource;
			}
		}

        public static DependencyProperty UnAssignButtonVisibilityProperty =
         DependencyProperty.Register("UnAssignButtonVisibility", typeof(Visibility), typeof(FlowImageListBoxEditTemp), new PropertyMetadata(Visibility.Visible));

        public Visibility UnAssignButtonVisibility
        {
            get { return (Visibility)GetValue(UnAssignButtonVisibilityProperty); }
            set { SetValue(UnAssignButtonVisibilityProperty, value); }
        }

        public static DependencyProperty DeleteButtonVisibilityProperty =
         DependencyProperty.Register("DeleteButtonVisibility", typeof(Visibility), typeof(FlowImageListBoxEditTemp), new PropertyMetadata(Visibility.Visible));

        public Visibility DeleteButtonVisibility
        {
            get { return (Visibility)GetValue(DeleteButtonVisibilityProperty); }
            set { SetValue(DeleteButtonVisibilityProperty, value); }
        }

        public static DependencyProperty RefreshButtonVisibilityProperty =
 DependencyProperty.Register("RefreshButtonVisibility", typeof(Visibility), typeof(FlowImageListBoxEditTemp), new PropertyMetadata(Visibility.Visible));

        public Visibility RefreshButtonVisibility
        {
            get { return (Visibility)GetValue(RefreshButtonVisibilityProperty); }
            set { SetValue(RefreshButtonVisibilityProperty, value); }
        }

        public static DependencyProperty GreenscreenButtonVisibilityProperty =
DependencyProperty.Register("GreenscreenButtonVisibility", typeof(Visibility), typeof(FlowImageListBoxEditTemp), new PropertyMetadata(Visibility.Visible));

        public Visibility GreenscreenButtonVisibility
        {
            get { return (Visibility)GetValue(GreenscreenButtonVisibilityProperty); }
            set { SetValue(GreenscreenButtonVisibilityProperty, value); }
        }

        public static DependencyProperty OpenExternallyButtonVisibilityProperty =
 DependencyProperty.Register("OpenExternallyButtonVisibility", typeof(Visibility), typeof(FlowImageListBoxEditTemp), new PropertyMetadata(Visibility.Visible));

        public Visibility OpenExternallyButtonVisibility
        {
            get { return (Visibility)GetValue(OpenExternallyButtonVisibilityProperty); }
            set { SetValue(OpenExternallyButtonVisibilityProperty, value); }
        }

        public static DependencyProperty FlagAsButtonVisibilityProperty =
           DependencyProperty.Register("FlagAsButtonVisibility", typeof(Visibility), typeof(FlowImageListBoxEditTemp), new PropertyMetadata(Visibility.Visible));

        public Visibility FlagAsButtonVisibility
        {
            get { return (Visibility)GetValue(FlagAsButtonVisibilityProperty); }
            set { SetValue(FlagAsButtonVisibilityProperty, value); }
        }

        public static DependencyProperty AdjustButtonVisibilityProperty =
          DependencyProperty.Register("AdjustButtonVisibility", typeof(Visibility), typeof(FlowImageListBoxEditTemp), new PropertyMetadata(Visibility.Visible));

        public Visibility AdjustButtonVisibility
        {
            get { return (Visibility)GetValue(AdjustButtonVisibilityProperty); }
            set { SetValue(AdjustButtonVisibilityProperty, value); }
        }

		//public static DependencyProperty ActionGlyphContainerContentProperty =
		//    DependencyProperty.RegisterAttached("ActionGlyphContainerContent", this


		public IEnumerable<T> GetSelectedItems<T>()
		{
			return lstImageListBox.SelectedItems.Cast<T>();
		}

		public int SelectedIndex
		{
			get { return lstImageListBox.SelectedIndex; }
			set { lstImageListBox.SelectedIndex = 0; }
		}


		public FlowImageListBoxEditTemp()
		{
			InitializeComponent();
		}

		public void SelectAllItems()
		{
			lstImageListBox.SelectAll();
		}

		public void HideItem<T>(T item)
		{
			ListBoxItem visualItem = lstImageListBox.ItemContainerGenerator.ContainerFromItem(item) as ListBoxItem;

			if (visualItem != null)
				visualItem.Visibility = Visibility.Collapsed;
		}

		private void UserControl_SizeChanged(object sender, SizeChangedEventArgs e)
		{
			if (e.NewSize.Width > e.NewSize.Height)
			{
				this.Orientation = Orientation.Horizontal;
				this.HorizontalScrollBarVisibility = ScrollBarVisibility.Auto;
				this.VerticalScrollbarVisibility = ScrollBarVisibility.Disabled;
			}
			else
			{
				this.Orientation = Orientation.Vertical;
				this.HorizontalScrollBarVisibility = ScrollBarVisibility.Disabled;
				this.VerticalScrollbarVisibility = ScrollBarVisibility.Auto;
			}
		}


		private void lstImageListBox_PreviewMouseRightButtonDown(object sender, MouseButtonEventArgs e)
		{
			e.Handled = true;
		}

		// OBSOLETE; CONISDER REMOVAL
		private void lstImageListBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
		{
//			ImageDoubleClick(this, new EventArgs());
		}

		private void OpenAdjustmentsButton_Click(object sender, RoutedEventArgs e)
		{
			Button source = e.OriginalSource as Button;

			if (source != null)
			{
				SubjectImage item = source.CommandParameter as SubjectImage;

				if (item != null)
				{
					this.OpenAdjustmentsInvoked(this, new EventArgs<SubjectImage>(item));
				}
			}
		}

		private void ReturnToHotfolderButton_Click(object sender, RoutedEventArgs e)
		{
			Button source = e.OriginalSource as Button;

			if (source != null)
			{
				SubjectImage item = source.CommandParameter as SubjectImage;

				if (item != null)
				{
					this.UnassignImageInvoked(this, new EventArgs<SubjectImage>(item));
				}
			}
		}

		private void DeleteImage_Click(object sender, RoutedEventArgs e)
		{
			Button source = sender as Button;

			if (source != null)
			{
                SubjectImage image = source.DataContext as SubjectImage;
                if (image.IsGroupImage)
                {
                    MessageBox.Show("You can't delete a group image, you can only un-assign a group image", "Delete Image");
                    return;
                }

				StackPanel panel = source.CommandParameter as StackPanel;
				if (panel != null)
				{
					panel.Visibility = Visibility.Visible;
				}
			}
		}

		private void ConfirmDeleteImage_Click(object sender, RoutedEventArgs e)
		{
			Button source = sender as Button;

			if (source != null)
			{
				SubjectImage image = source.DataContext as SubjectImage;
                

				if (image != null)
				{
					this.DeleteImageInvoked(this, new EventArgs<SubjectImage>(image));
				}
				
				StackPanel panel = source.CommandParameter as StackPanel;
				if (panel != null)
				{
					panel.Visibility = Visibility.Collapsed;
				}
			}
		}

		private void CancelDeleteImage_Click(object sender, RoutedEventArgs e)
		{
			Button source = sender as Button;

			if (source != null)
			{
				StackPanel panel = source.CommandParameter as StackPanel;
				if (panel != null)
				{
					panel.Visibility = Visibility.Collapsed;
				}
			}
		}

        private void OpenExternalEditor_Click(object sender, RoutedEventArgs e)
        {
            Button source = e.OriginalSource as Button;

            if (source != null)
            {
                SubjectImage item = source.CommandParameter as SubjectImage;

                if (item != null)
                {
                    this.OpenExternalEditorInvoked(this, new EventArgs<SubjectImage>(item));
                }
            }
        }

        private void RefreshImage_Click(object sender, RoutedEventArgs e)
        {
            Button source = e.OriginalSource as Button;
            
            if (source != null)
            {
                SubjectImage item = source.CommandParameter as SubjectImage;
                if (item != null)
                {
                    this.RefreshImageInvoked(this, new EventArgs<SubjectImage>(item));
                    
                }
            }

        }

		private void OpenContextMenuButton_Click(object sender, RoutedEventArgs e)
		{
			Button source = sender as Button;

			source.ContextMenu.PlacementTarget = source;
			source.ContextMenu.Placement = PlacementMode.Right;

			source.ContextMenu.IsOpen = true;
		}

        private void IsGreenscreenButton_Click(object sender, RoutedEventArgs e)
        {
            Button source = e.OriginalSource as Button;

            if (source != null)
            {
                SubjectImage item = source.CommandParameter as SubjectImage;

                if (item != null)
                {
                    this.ToggleGreenscreenInvoked(this, new EventArgs<SubjectImage>(item));
                }
            }
        }

        private void IsFlagged_Click(object sender, RoutedEventArgs e)
        {
            Button source = e.OriginalSource as Button;

            if (source != null)
            {
                SubjectImage item = source.CommandParameter as SubjectImage;

                if (item != null)
                {
                    this.ToggleFlagImageInvoked(this, new EventArgs<SubjectImage>(item));
                }
            }
        }

        private void IsRemoteImageService_Click(object sender, RoutedEventArgs e)
        {
            Button source = e.OriginalSource as Button;

            if (source != null)
            {
                SubjectImage item = source.CommandParameter as SubjectImage;

                if (item != null)
                {
                    this.ToggleRemoteImageServiceInvoked(this, new EventArgs<SubjectImage>(item));
                }
            }
        }

	}
}