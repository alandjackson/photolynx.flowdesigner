﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Lib;
using Flow.Lib.Helpers;
using Flow.Schema.LinqModel.DataContext;

namespace Flow.Controls.View.ImageDisplay
{
	/// <summary>
	/// Interaction logic for FlowImageListBox.xaml
	/// </summary>
	public partial class FlowImageListBox : UserControl
	{

		public event EventHandler ImageDoubleClick = delegate { };
		
		public event SelectionChangedEventHandler ListBoxSelectionChanged
		{
			add { lstImageListBox.SelectionChanged += value; }
			remove { lstImageListBox.SelectionChanged -= value; }
		}


		public static DependencyProperty OrientationProperty =
			DependencyProperty.Register("Orientation", typeof(Orientation), typeof(FlowImageListBox));
	
		public Orientation Orientation
		{
			get { return (Orientation)GetValue(OrientationProperty); }
			set { SetValue(OrientationProperty, value); }
		}


		public static DependencyProperty HorizontalScrollBarVisibilityProperty =
			DependencyProperty.Register("HorizontalScrollBarVisibility", typeof(ScrollBarVisibility), typeof(FlowImageListBox));

		public ScrollBarVisibility HorizontalScrollBarVisibility
		{
			get { return (ScrollBarVisibility)GetValue(HorizontalScrollBarVisibilityProperty); }
			set { SetValue(HorizontalScrollBarVisibilityProperty, value); }
		}


		public static DependencyProperty VerticalScrollBarVisibilityProperty =
			DependencyProperty.Register("VerticalScrollBarVisibility", typeof(ScrollBarVisibility), typeof(FlowImageListBox));

		public ScrollBarVisibility VerticalScrollbarVisibility
		{
			get { return (ScrollBarVisibility)GetValue(VerticalScrollBarVisibilityProperty); }
			set { SetValue(VerticalScrollBarVisibilityProperty, value); }
		}


		public static DependencyProperty ItemsSourceProperty =
			DependencyProperty.Register("ItemsSource", typeof(IEnumerable), typeof(FlowImageListBox));

		public IEnumerable ItemsSource
		{
			get { return (IEnumerable)this.GetValue(ItemsSourceProperty); }
			set
			{
				this.SetValue(ItemsSourceProperty, value);
				lstImageListBox.ItemsSource = this.ItemsSource;
			}
		}


		//public static DependencyProperty ActionGlyphContainerContentProperty =
		//    DependencyProperty.RegisterAttached("ActionGlyphContainerContent", this


		public IEnumerable<T> GetSelectedItems<T>()
		{
			return lstImageListBox.SelectedItems.Cast<T>();
		}

		public int SelectedIndex
		{
			get { return lstImageListBox.SelectedIndex; }
			set { lstImageListBox.SelectedIndex = 0; }
		}


		public FlowImageListBox()
		{
			InitializeComponent();
		}

		public void SelectAllItems()
		{
			lstImageListBox.SelectAll();
		}

		public void HideItem<T>(T item)
		{
			ListBoxItem visualItem = lstImageListBox.ItemContainerGenerator.ContainerFromItem(item) as ListBoxItem;

			if (visualItem != null)
				visualItem.Visibility = Visibility.Collapsed;
		}

		private void UserControl_SizeChanged(object sender, SizeChangedEventArgs e)
		{
			if (e.NewSize.Width > e.NewSize.Height)
			{
				this.Orientation = Orientation.Horizontal;
				this.HorizontalScrollBarVisibility = ScrollBarVisibility.Auto;
				this.VerticalScrollbarVisibility = ScrollBarVisibility.Disabled;
			}
			else
			{
				this.Orientation = Orientation.Vertical;
				this.HorizontalScrollBarVisibility = ScrollBarVisibility.Disabled;
				this.VerticalScrollbarVisibility = ScrollBarVisibility.Auto;
			}
		}


		private void lstImageListBox_PreviewMouseRightButtonDown(object sender, MouseButtonEventArgs e)
		{
			e.Handled = true;
		}

		private void lstImageListBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
		{
			ImageDoubleClick(this, new EventArgs());
		}
	}
}