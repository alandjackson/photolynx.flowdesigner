﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Flow.Schema.LinqModel.DataContext.FlowMaster;

namespace Flow.Controls.ImageDisplay
{
    /// <summary>
    /// Interaction logic for GSBackgroundThumbnail.xaml
    /// </summary>
    public partial class GSBackgroundThumbnail : UserControl
    {
        public GSBackgroundThumbnail()
        { 
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {

        }
    }
}
