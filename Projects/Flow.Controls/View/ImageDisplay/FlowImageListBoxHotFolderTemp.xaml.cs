﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Lib;
using Flow.Lib.Helpers;
using Flow.Schema.LinqModel.DataContext;
using Flow.Controls.View.Dialogs;

using NLog;

namespace Flow.Controls.View.ImageDisplay
{
	/// <summary>
	/// Interaction logic for FlowImageListBox.xaml
	/// </summary>
	public partial class FlowImageListBoxHotFolderTemp : UserControl
	{
        private static Logger logger = LogManager.GetCurrentClassLogger();

		public event EventHandler ImageDoubleClick = delegate { };
        public event EventHandler GroupImageClick = delegate { };
        public event EventHandler DeleteImageClick = delegate { };

        public event EventHandler<EventArgs<FlowImage>>
         UpdateMainImage = delegate { };

        //private bool deletingImage = false;
//		public event EventHandler<EventArgs<Flow.Schema.LinqModel.DataContext.Image>> AssignImageInvoked = delegate { };

		
		public event SelectionChangedEventHandler ListBoxSelectionChanged
		{
			add { lstImageListBox.SelectionChanged += value; }
			remove { lstImageListBox.SelectionChanged -= value; }
		}


		public static DependencyProperty OrientationProperty =
			DependencyProperty.Register("Orientation", typeof(Orientation), typeof(FlowImageListBoxHotFolderTemp));
	
		public Orientation Orientation
		{
			get { return (Orientation)GetValue(OrientationProperty); }
			set { SetValue(OrientationProperty, value); }
		}


		public static DependencyProperty HorizontalScrollBarVisibilityProperty =
			DependencyProperty.Register("HorizontalScrollBarVisibility", typeof(ScrollBarVisibility), typeof(FlowImageListBoxHotFolderTemp));

		public ScrollBarVisibility HorizontalScrollBarVisibility
		{
			get { return (ScrollBarVisibility)GetValue(HorizontalScrollBarVisibilityProperty); }
			set { SetValue(HorizontalScrollBarVisibilityProperty, value); }
		}


        public static DependencyProperty GroupAssignVisibilityProperty =
            DependencyProperty.Register("GroupAssignVisibility", typeof(Visibility), typeof(FlowImageListBoxHotFolderTemp), new PropertyMetadata(Visibility.Visible));
        public Visibility GroupAssignVisibility
        {
            get { return (Visibility)GetValue(GroupAssignVisibilityProperty); }
            set { SetValue(GroupAssignVisibilityProperty, value); }
        }


        public static DependencyProperty AssignButtonVisibilityProperty =
            DependencyProperty.Register("AssignButtonVisibility", typeof(Visibility), typeof(FlowImageListBoxHotFolderTemp), new PropertyMetadata(Visibility.Visible));

        public Visibility AssignButtonVisibility
        {
            get { return (Visibility)GetValue(AssignButtonVisibilityProperty); }
            set { SetValue(AssignButtonVisibilityProperty, value); }
        }

        public static DependencyProperty ShowOverlayVisibilityProperty =
            DependencyProperty.Register("ShowOverlayVisibility", typeof(Visibility), typeof(FlowImageListBoxHotFolderTemp), new PropertyMetadata(Visibility.Hidden));

        public Visibility ShowOverlayVisibility
        {
            get { return (Visibility)GetValue(ShowOverlayVisibilityProperty); }
            set { SetValue(ShowOverlayVisibilityProperty, value); }
        }

		public static DependencyProperty VerticalScrollBarVisibilityProperty =
			DependencyProperty.Register("VerticalScrollBarVisibility", typeof(ScrollBarVisibility), typeof(FlowImageListBoxHotFolderTemp));

		public ScrollBarVisibility VerticalScrollbarVisibility
		{
			get { return (ScrollBarVisibility)GetValue(VerticalScrollBarVisibilityProperty); }
			set { SetValue(VerticalScrollBarVisibilityProperty, value); }
		}


		public static DependencyProperty ItemsSourceProperty =
			DependencyProperty.Register("ItemsSource", typeof(IEnumerable), typeof(FlowImageListBoxHotFolderTemp));

		public IEnumerable ItemsSource
		{
			get { return (IEnumerable)this.GetValue(ItemsSourceProperty); }
			set
			{
				this.SetValue(ItemsSourceProperty, value);
				lstImageListBox.ItemsSource = this.ItemsSource;
			}
		}

        //public static DependencyProperty ImageOverlayProperty =
        //    DependencyProperty.Register("ImageOverlay", typeof(string), typeof(FlowImageListBoxHotFolderTemp));

        //public string ImageOverlay
        //{
        //    get { return (string)this.GetValue(ImageOverlayProperty); }
        //    set
        //    {
        //        this.SetValue(ImageOverlayProperty, value);
        //    }
        //}


		//public static DependencyProperty ActionGlyphContainerContentProperty =
		//    DependencyProperty.RegisterAttached("ActionGlyphContainerContent", this


		public IEnumerable<T> GetSelectedItems<T>()
		{
			return lstImageListBox.SelectedItems.Cast<T>();
		}

		public int SelectedIndex
		{
			get { return lstImageListBox.SelectedIndex; }
			set { lstImageListBox.SelectedIndex = value; }
		}


		public FlowImageListBoxHotFolderTemp()
		{
			InitializeComponent();
            //AssignButtonVisibility = Visibility.Hidden;
            
		}

		public void SelectAllItems()
		{
			lstImageListBox.SelectAll();
		}

		public void HideItem<T>(T item)
		{
			ListBoxItem visualItem = lstImageListBox.ItemContainerGenerator.ContainerFromItem(item) as ListBoxItem;
            
			if (visualItem != null)
				visualItem.Visibility = Visibility.Collapsed;
		}

		private void UserControl_SizeChanged(object sender, SizeChangedEventArgs e)
		{
			if (e.NewSize.Width > e.NewSize.Height)
			{
				this.Orientation = Orientation.Horizontal;
				this.HorizontalScrollBarVisibility = ScrollBarVisibility.Auto;
				this.VerticalScrollbarVisibility = ScrollBarVisibility.Disabled;
			}
			else
			{
				this.Orientation = Orientation.Vertical;
				this.HorizontalScrollBarVisibility = ScrollBarVisibility.Disabled;
				this.VerticalScrollbarVisibility = ScrollBarVisibility.Auto;
			}
		}

		private void lstImageListBox_PreviewMouseRightButtonDown(object sender, MouseButtonEventArgs e)
		{
			e.Handled = true;
		}

		private void lstImageListBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
		{
            // Figure out which control was double-clicked
            DependencyObject source = (DependencyObject)(e.OriginalSource);
            while (!(source is Control))
                source = VisualTreeHelper.GetParent(source);

            // Only respond if the item itself was double-clicked (not the scrollbar)
            if (source.GetType().Name == "ListBoxItem")
            {
                ImageDoubleClick(this, new EventArgs());
                logger.Info("Hotfolder image double-clicked");
            }
		}

		private void AssignImageButton_Click(object sender, RoutedEventArgs e)
		{
			Button source = sender as Button;

			if (source != null)
			{
				lstImageListBox.SelectedItem = source.CommandParameter;
			}

			ImageDoubleClick(this, new EventArgs());
            logger.Info("Hotfolder image assign clicked");
		}

        private void DeleteImageButton_Click(object sender, RoutedEventArgs e)
        {
            FlowMessageDialog msg = new FlowMessageDialog("Delete Image", "Do you really want to delete these images?");
            if ((bool)msg.ShowDialog())
            {
                //deletingImage = true;
                Button source = sender as Button;

                //if (source != null)
                //{
                //    lstImageListBox.SelectedItem = source.CommandParameter;
                //}
                
                DeleteImageClick(source, new EventArgs());
                logger.Info("Hotfolder image delete clicked");
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void AssignGroupImageButton_Click(object sender, RoutedEventArgs e)
        {
            Button source = sender as Button;
            lstImageListBox.SelectedItem = source.DataContext as FlowImage;
            if (source != null)
            {
                lstImageListBox.SelectedItem = source.CommandParameter;
            }
            GroupImageClick(this, new EventArgs());

            logger.Info("Hotfolder assign group image clicked");
        }

        private void lstImageListBox_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            logger.Info("Hotfolder image clicked");
            UpdateMainImage(this, new EventArgs<FlowImage>((e.Source as ListBox).SelectedItem as FlowImage));
        }
       
        
	}

    
}