﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Lib;
using Flow.Lib.Helpers;
using Flow.Schema.LinqModel.DataContext;
using Flow.Controls.View.Dialogs;

using NLog;

namespace Flow.Controls.View.ImageDisplay
{
	/// <summary>
	/// Interaction logic for FlowImageListBox.xaml
	/// </summary>
	public partial class FlowImageListBoxGroupGroupPhotoTemp : UserControl
	{
        private static Logger logger = LogManager.GetCurrentClassLogger();

		public event EventHandler ImageDoubleClick = delegate { };
        public event EventHandler GroupImageClick = delegate { };
        public event EventHandler DeleteImageClick = delegate { };
        public event EventHandler SaveProjectDataContextInvoked = delegate { };

        public event EventHandler<EventArgs<GroupImage>>
        UpdateMainImage = delegate { };

        public event EventHandler<EventArgs<GroupImage>>
        AssignGroupImage = delegate { };

        public event EventHandler<EventArgs<GroupImage>>
        AssignAllGroupImage = delegate { };

        public event EventHandler<EventArgs<GroupImage>>
        RemoveGroupImage = delegate { };

        //private bool deletingImage = false;
//		public event EventHandler<EventArgs<Flow.Schema.LinqModel.DataContext.Image>> AssignImageInvoked = delegate { };

		
		public event SelectionChangedEventHandler ListBoxSelectionChanged
		{
			add { lstImageListBox.SelectionChanged += value; }
			remove { lstImageListBox.SelectionChanged -= value; }
		}


		public static DependencyProperty OrientationProperty =
			DependencyProperty.Register("Orientation", typeof(Orientation), typeof(FlowImageListBoxGroupGroupPhotoTemp));
	
		public Orientation Orientation
		{
			get { return (Orientation)GetValue(OrientationProperty); }
			set { SetValue(OrientationProperty, value); }
		}


		public static DependencyProperty HorizontalScrollBarVisibilityProperty =
			DependencyProperty.Register("HorizontalScrollBarVisibility", typeof(ScrollBarVisibility), typeof(FlowImageListBoxGroupGroupPhotoTemp));

		public ScrollBarVisibility HorizontalScrollBarVisibility
		{
			get { return (ScrollBarVisibility)GetValue(HorizontalScrollBarVisibilityProperty); }
			set { SetValue(HorizontalScrollBarVisibilityProperty, value); }
		}

        public static DependencyProperty AssignButtonVisibilityProperty =
            DependencyProperty.Register("AssignButtonVisibility", typeof(Visibility), typeof(FlowImageListBoxGroupGroupPhotoTemp), new PropertyMetadata(Visibility.Visible));

        public Visibility AssignButtonVisibility
        {
            get { return (Visibility)GetValue(AssignButtonVisibilityProperty); }
            set { SetValue(AssignButtonVisibilityProperty, value); }
        }

        public static DependencyProperty ShowOverlayVisibilityProperty =
            DependencyProperty.Register("ShowOverlayVisibility", typeof(Visibility), typeof(FlowImageListBoxGroupGroupPhotoTemp), new PropertyMetadata(Visibility.Hidden));

        public Visibility ShowOverlayVisibility
        {
            get { return (Visibility)GetValue(ShowOverlayVisibilityProperty); }
            set { SetValue(ShowOverlayVisibilityProperty, value); }
        }

		public static DependencyProperty VerticalScrollBarVisibilityProperty =
			DependencyProperty.Register("VerticalScrollBarVisibility", typeof(ScrollBarVisibility), typeof(FlowImageListBoxGroupGroupPhotoTemp));

		public ScrollBarVisibility VerticalScrollbarVisibility
		{
			get { return (ScrollBarVisibility)GetValue(VerticalScrollBarVisibilityProperty); }
			set { SetValue(VerticalScrollBarVisibilityProperty, value); }
		}


		public static DependencyProperty ItemsSourceProperty =
			DependencyProperty.Register("ItemsSource", typeof(IEnumerable), typeof(FlowImageListBoxGroupGroupPhotoTemp));

		public IEnumerable ItemsSource
		{
			get { return (IEnumerable)this.GetValue(ItemsSourceProperty); }
			set
			{
				this.SetValue(ItemsSourceProperty, value);
				lstImageListBox.ItemsSource = this.ItemsSource;
			}
		}

        //public static DependencyProperty ImageOverlayProperty =
        //    DependencyProperty.Register("ImageOverlay", typeof(string), typeof(FlowImageListBoxGroupGroupPhotoTemp));

        //public string ImageOverlay
        //{
        //    get { return (string)this.GetValue(ImageOverlayProperty); }
        //    set
        //    {
        //        this.SetValue(ImageOverlayProperty, value);
        //    }
        //}


		//public static DependencyProperty ActionGlyphContainerContentProperty =
		//    DependencyProperty.RegisterAttached("ActionGlyphContainerContent", this


		public IEnumerable<T> GetSelectedItems<T>()
		{
			return lstImageListBox.SelectedItems.Cast<T>();
		}

		public int SelectedIndex
		{
			get { return lstImageListBox.SelectedIndex; }
			set { lstImageListBox.SelectedIndex = value; }
		}


		public FlowImageListBoxGroupGroupPhotoTemp()
		{
			InitializeComponent();
            //AssignButtonVisibility = Visibility.Hidden;
            
		}

		public void SelectAllItems()
		{
			lstImageListBox.SelectAll();
		}

		public void HideItem<T>(T item)
		{
			ListBoxItem visualItem = lstImageListBox.ItemContainerGenerator.ContainerFromItem(item) as ListBoxItem;
            
			if (visualItem != null)
				visualItem.Visibility = Visibility.Collapsed;
		}

		private void UserControl_SizeChanged(object sender, SizeChangedEventArgs e)
		{
			if (e.NewSize.Width > e.NewSize.Height)
			{
				this.Orientation = Orientation.Horizontal;
				this.HorizontalScrollBarVisibility = ScrollBarVisibility.Auto;
				this.VerticalScrollbarVisibility = ScrollBarVisibility.Disabled;
			}
			else
			{
				this.Orientation = Orientation.Vertical;
				this.HorizontalScrollBarVisibility = ScrollBarVisibility.Disabled;
				this.VerticalScrollbarVisibility = ScrollBarVisibility.Auto;
			}
		}

		private void lstImageListBox_PreviewMouseRightButtonDown(object sender, MouseButtonEventArgs e)
		{
			e.Handled = true;
		}

		private void lstImageListBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
		{
			//ImageDoubleClick(this, new EventArgs());
            //AssignGroupImage(this, new EventArgs<GroupImage>(lstImageListBox.SelectedItem as GroupImage));
		}

		private void AssignImageButton_Click(object sender, RoutedEventArgs e)
		{
            logger.Info("Group Images assign button clicked");

			Button source = sender as Button;

			if (source != null)
			{
				lstImageListBox.SelectedItem = source.CommandParameter;
			}

			//ImageDoubleClick(this, new EventArgs());
            AssignGroupImage(this, new EventArgs<GroupImage>(lstImageListBox.SelectedItem as GroupImage));
		}

        private void DeleteImageButton_Click(object sender, RoutedEventArgs e)
        {
            logger.Info("Group Images delete image button clicked");

            //FlowMessageDialog msg = new FlowMessageDialog("Delete Image", "Do you really want to delete this image?");
            //if ((bool)msg.ShowDialog())
            //{
                //deletingImage = true;
                Button source = sender as Button;

                //DeleteImageClick(source, new EventArgs());
                RemoveGroupImage(this, new EventArgs<GroupImage>(source.DataContext as GroupImage));
            //}
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void _this_Loaded(object sender, RoutedEventArgs e)
        {

        }


        private void lstImageListBox_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            logger.Info("Group Images image clicked");
            UpdateMainImage(this, new EventArgs<GroupImage>((e.Source as ListBox).SelectedItem as GroupImage));
        }

        private void AssignImageToAllButton_Click(object sender, RoutedEventArgs e)
        {
            logger.Info("Group Images assign to all button clicked");

            Button source = sender as Button;

            if (source != null)
            {
                lstImageListBox.SelectedItem = source.CommandParameter;
            }

            //ImageDoubleClick(this, new EventArgs());
            AssignAllGroupImage(this, new EventArgs<GroupImage>(lstImageListBox.SelectedItem as GroupImage));
        }

        private void txtGroupImageDesc_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            logger.Info("Group images description double-clicked");

            (sender as TextBox).SelectAll();
            (sender as TextBox).Focusable = true;
            (sender as TextBox).IsReadOnly = false;
        }


        private void txtGroupImageDesc_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {

                (sender as TextBox).Focusable = false;
                (sender as TextBox).IsReadOnly = true;
                //this.SaveProjectDataContextInvoked(sender, e);
            }
        }

        private void txtGroupImageDesc_LostFocus(object sender, RoutedEventArgs e)
        {
            (sender as TextBox).Focusable = false;
            (sender as TextBox).IsReadOnly = true;
            this.SaveProjectDataContextInvoked(sender, e);
        }

       
       
        
	}

    
}