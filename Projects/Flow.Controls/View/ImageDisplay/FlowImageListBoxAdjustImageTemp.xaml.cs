﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Lib;
using Flow.Lib.Helpers;
using Flow.Schema.LinqModel.DataContext;

namespace Flow.Controls.View.ImageDisplay
{
	/// <summary>
	/// Interaction logic for FlowImageListBox.xaml
	/// </summary>
	public partial class FlowImageListBoxAdjustImageTemp : UserControl
	{
		public event EventHandler ImageDoubleClick = delegate { };

//		public event EventHandler<EventArgs<Flow.Schema.LinqModel.DataContext.Image>> AssignImageInvoked = delegate { };

		
		public event SelectionChangedEventHandler ListBoxSelectionChanged
		{
			add { lstImageListBox.SelectionChanged += value; }
			remove { lstImageListBox.SelectionChanged -= value; }
		}


		public static DependencyProperty OrientationProperty =
			DependencyProperty.Register("Orientation", typeof(Orientation), typeof(FlowImageListBoxAdjustImageTemp));
	
		public Orientation Orientation
		{
			get { return (Orientation)GetValue(OrientationProperty); }
			set { SetValue(OrientationProperty, value); }
		}


		public static DependencyProperty HorizontalScrollBarVisibilityProperty =
			DependencyProperty.Register("HorizontalScrollBarVisibility", typeof(ScrollBarVisibility), typeof(FlowImageListBoxAdjustImageTemp));

		public ScrollBarVisibility HorizontalScrollBarVisibility
		{
			get { return (ScrollBarVisibility)GetValue(HorizontalScrollBarVisibilityProperty); }
			set { SetValue(HorizontalScrollBarVisibilityProperty, value); }
		}

        public static DependencyProperty AssignButtonVisibilityProperty =
            DependencyProperty.Register("AssignButtonVisibility", typeof(Visibility), typeof(FlowImageListBoxAdjustImageTemp), new PropertyMetadata(Visibility.Visible));

        public Visibility AssignButtonVisibility
        {
            get { return (Visibility)GetValue(AssignButtonVisibilityProperty); }
            set { SetValue(AssignButtonVisibilityProperty, value); }
        }

        public static DependencyProperty ShowOverlayVisibilityProperty =
            DependencyProperty.Register("ShowOverlayVisibility", typeof(Visibility), typeof(FlowImageListBoxAdjustImageTemp), new PropertyMetadata(Visibility.Hidden));

        public Visibility ShowOverlayVisibility
        {
            get { return (Visibility)GetValue(ShowOverlayVisibilityProperty); }
            set { SetValue(ShowOverlayVisibilityProperty, value); }
        }

		public static DependencyProperty VerticalScrollBarVisibilityProperty =
			DependencyProperty.Register("VerticalScrollBarVisibility", typeof(ScrollBarVisibility), typeof(FlowImageListBoxAdjustImageTemp));

		public ScrollBarVisibility VerticalScrollbarVisibility
		{
			get { return (ScrollBarVisibility)GetValue(VerticalScrollBarVisibilityProperty); }
			set { SetValue(VerticalScrollBarVisibilityProperty, value); }
		}


		public static DependencyProperty ItemsSourceProperty =
			DependencyProperty.Register("ItemsSource", typeof(IEnumerable), typeof(FlowImageListBoxAdjustImageTemp));

		public IEnumerable ItemsSource
		{
			get { return (IEnumerable)this.GetValue(ItemsSourceProperty); }
			set
			{
				this.SetValue(ItemsSourceProperty, value);
				lstImageListBox.ItemsSource = this.ItemsSource;
			}
		}

        //public static DependencyProperty ImageOverlayProperty =
        //    DependencyProperty.Register("ImageOverlay", typeof(string), typeof(FlowImageListBoxAdjustImageTemp));

        //public string ImageOverlay
        //{
        //    get { return (string)this.GetValue(ImageOverlayProperty); }
        //    set
        //    {
        //        this.SetValue(ImageOverlayProperty, value);
        //    }
        //}


		//public static DependencyProperty ActionGlyphContainerContentProperty =
		//    DependencyProperty.RegisterAttached("ActionGlyphContainerContent", this


		public IEnumerable<T> GetSelectedItems<T>()
		{
			return lstImageListBox.SelectedItems.Cast<T>();
		}

		public int SelectedIndex
		{
			get { return lstImageListBox.SelectedIndex; }
			set { 
                lstImageListBox.SelectedIndex = value;
                FocusSelectedItem();
            }
		}

        private void FocusSelectedItem()
        {
            if (lstImageListBox.SelectedIndex >= 0)
            {
                ScrollViewer scroll = FindVisualChild<ScrollViewer>(lstImageListBox);
                if (scroll != null)
                {
                    double scrollPos = lstImageListBox.SelectedIndex;
                    scroll.ScrollToHorizontalOffset(scrollPos);
                }
            }
        }

        private childItem FindVisualChild<childItem>(DependencyObject obj) where childItem : DependencyObject
        {
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(obj, i);
                if (child != null && child is childItem)
                {
                    return (childItem)child;
                }
                else
                {
                    childItem childOfChild = FindVisualChild<childItem>(child);
                    if (childOfChild != null)
                    {
                        return childOfChild;
                    }
                }
            }
            return null;
        }


        public FlowImageListBoxAdjustImageTemp()
		{
			InitializeComponent();
            //AssignButtonVisibility = Visibility.Hidden;
            
		}

		public void SelectAllItems()
		{
			lstImageListBox.SelectAll();
		}

		public void HideItem<T>(T item)
		{
			ListBoxItem visualItem = lstImageListBox.ItemContainerGenerator.ContainerFromItem(item) as ListBoxItem;
            
			if (visualItem != null)
				visualItem.Visibility = Visibility.Collapsed;
		}

		private void UserControl_SizeChanged(object sender, SizeChangedEventArgs e)
		{
			if (e.NewSize.Width > e.NewSize.Height)
			{
				this.Orientation = Orientation.Horizontal;
				this.HorizontalScrollBarVisibility = ScrollBarVisibility.Auto;
				this.VerticalScrollbarVisibility = ScrollBarVisibility.Disabled;
			}
			else
			{
				this.Orientation = Orientation.Vertical;
				this.HorizontalScrollBarVisibility = ScrollBarVisibility.Disabled;
				this.VerticalScrollbarVisibility = ScrollBarVisibility.Auto;
			}
		}

		private void lstImageListBox_PreviewMouseRightButtonDown(object sender, MouseButtonEventArgs e)
		{
			e.Handled = true;
		}

		private void lstImageListBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
		{
			ImageDoubleClick(this, new EventArgs());
		}

		private void AssignImageButton_Click(object sender, RoutedEventArgs e)
		{
			Button source = sender as Button;

			if (source != null)
			{
				lstImageListBox.SelectedItem = source.CommandParameter;
			}

			ImageDoubleClick(this, new EventArgs());
		}
       
        
	}

    
}