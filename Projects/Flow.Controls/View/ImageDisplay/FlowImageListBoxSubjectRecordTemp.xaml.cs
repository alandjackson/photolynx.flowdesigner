﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Lib;
using Flow.Lib.Helpers;
using Flow.Schema.LinqModel.DataContext;

namespace Flow.Controls.View.ImageDisplay
{
	/// <summary>
	/// Interaction logic for FlowImageListBox.xaml
	/// </summary>
	public partial class FlowImageListBoxSubjectRecordTemp : UserControl
	{
		public event EventHandler ImageDoubleClick = delegate { };
        public event EventHandler<EventArgs<SubjectImage>> ToggleGreenscreenInvoked = delegate { };
        public event EventHandler<EventArgs<SubjectImage>> ToggleFlagImageInvoked = delegate { };
        public event EventHandler<EventArgs<SubjectImage>> ToggleRemoteImageServiceInvoked = delegate { };

        public event EventHandler<EventArgs<SubjectImage>>
         UpdateMainImage = delegate { };

//		public event EventHandler<EventArgs<Flow.Schema.LinqModel.DataContext.Image>> AssignImageInvoked = delegate { };

		
		public event SelectionChangedEventHandler ListBoxSelectionChanged
		{
			add { lstImageListBox.SelectionChanged += value; }
			remove { lstImageListBox.SelectionChanged -= value; }
		}


		public static DependencyProperty OrientationProperty =
			DependencyProperty.Register("Orientation", typeof(Orientation), typeof(FlowImageListBoxSubjectRecordTemp));
	
		public Orientation Orientation
		{
			get { return (Orientation)GetValue(OrientationProperty); }
			set { SetValue(OrientationProperty, value); }
		}


		public static DependencyProperty HorizontalScrollBarVisibilityProperty =
			DependencyProperty.Register("HorizontalScrollBarVisibility", typeof(ScrollBarVisibility), typeof(FlowImageListBoxSubjectRecordTemp));

		public ScrollBarVisibility HorizontalScrollBarVisibility
		{
			get { return (ScrollBarVisibility)GetValue(HorizontalScrollBarVisibilityProperty); }
			set { SetValue(HorizontalScrollBarVisibilityProperty, value); }
		}


		public static DependencyProperty VerticalScrollBarVisibilityProperty =
			DependencyProperty.Register("VerticalScrollBarVisibility", typeof(ScrollBarVisibility), typeof(FlowImageListBoxSubjectRecordTemp));

		public ScrollBarVisibility VerticalScrollbarVisibility
		{
			get { return (ScrollBarVisibility)GetValue(VerticalScrollBarVisibilityProperty); }
			set { SetValue(VerticalScrollBarVisibilityProperty, value); }
		}


		public static DependencyProperty ItemsSourceProperty =
			DependencyProperty.Register("ItemsSource", typeof(IEnumerable), typeof(FlowImageListBoxSubjectRecordTemp));

		public IEnumerable ItemsSource
		{
			get { return (IEnumerable)this.GetValue(ItemsSourceProperty); }
			set
			{
				this.SetValue(ItemsSourceProperty, value);
				lstImageListBox.ItemsSource = this.ItemsSource;
			}
		}

        public static DependencyProperty UnAssignButtonVisibilityProperty =
           DependencyProperty.Register("UnAssignButtonVisibility", typeof(Visibility), typeof(FlowImageListBoxSubjectRecordTemp), new PropertyMetadata(Visibility.Visible));

        public Visibility UnAssignButtonVisibility
        {
            get { return (Visibility)GetValue(UnAssignButtonVisibilityProperty); }
            set { SetValue(UnAssignButtonVisibilityProperty, value); }
        }

        public static DependencyProperty FlagAsButtonVisibilityProperty =
           DependencyProperty.Register("FlagAsButtonVisibility", typeof(Visibility), typeof(FlowImageListBoxSubjectRecordTemp), new PropertyMetadata(Visibility.Visible));

        public Visibility FlagAsButtonVisibility
        {
            get { return (Visibility)GetValue(FlagAsButtonVisibilityProperty); }
            set { SetValue(FlagAsButtonVisibilityProperty, value); }
        }

		//public static DependencyProperty ActionGlyphContainerContentProperty =
		//    DependencyProperty.RegisterAttached("ActionGlyphContainerContent", this


		public IEnumerable<T> GetSelectedItems<T>()
		{
			return lstImageListBox.SelectedItems.Cast<T>();
		}

		public int SelectedIndex
		{
			get { return lstImageListBox.SelectedIndex; }
			set { lstImageListBox.SelectedIndex = 0; }
		}


		public FlowImageListBoxSubjectRecordTemp()
		{
			InitializeComponent();
		}

		public void SelectAllItems()
		{
			lstImageListBox.SelectAll();
		}

		public void HideItem<T>(T item)
		{
			ListBoxItem visualItem = lstImageListBox.ItemContainerGenerator.ContainerFromItem(item) as ListBoxItem;

			if (visualItem != null)
				visualItem.Visibility = Visibility.Collapsed;
		}

		private void UserControl_SizeChanged(object sender, SizeChangedEventArgs e)
		{
			if (e.NewSize.Width > e.NewSize.Height)
			{
				this.Orientation = Orientation.Horizontal;
				this.HorizontalScrollBarVisibility = ScrollBarVisibility.Auto;
				this.VerticalScrollbarVisibility = ScrollBarVisibility.Disabled;
			}
			else
			{
				this.Orientation = Orientation.Vertical;
				this.HorizontalScrollBarVisibility = ScrollBarVisibility.Disabled;
				this.VerticalScrollbarVisibility = ScrollBarVisibility.Auto;
			}
		}


		private void lstImageListBox_PreviewMouseRightButtonDown(object sender, MouseButtonEventArgs e)
		{
			e.Handled = true;
		}

		private void lstImageListBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
		{
			//ImageDoubleClick(this, new EventArgs());
		}

		private void UnassignImageButton_Click(object sender, RoutedEventArgs e)
		{
			Button source = e.OriginalSource as Button;

			if (source != null)
			{
				SubjectImage item = source.CommandParameter as SubjectImage;

				if (item != null)
				{
					lstImageListBox.SelectedItem = item;
				}
			}
	
			ImageDoubleClick(this, new EventArgs());
		}

        private void IsGreenscreenButton_Click(object sender, RoutedEventArgs e)
        {
            Button source = e.OriginalSource as Button;

            if (source != null)
            {
                SubjectImage item = source.CommandParameter as SubjectImage;

                if (item != null)
                {
                    this.ToggleGreenscreenInvoked(this, new EventArgs<SubjectImage>(item));
                }
            }
        }

		private void OpenContextMenuButton_Click(object sender, RoutedEventArgs e)
		{
			Button source = sender as Button;

			source.ContextMenu.PlacementTarget = source;
			source.ContextMenu.Placement = PlacementMode.Right;

			source.ContextMenu.IsOpen = true;
		}

        //private void lstImageListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    SelectedImageChanged(this, new EventArgs<SubjectImage>((e.Source as ListBox).SelectedItem as SubjectImage));
        //}

        private void lstImageListBox_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            UpdateMainImage(this, new EventArgs<SubjectImage>((e.Source as ListBox).SelectedItem as SubjectImage));
        }

        private void _this_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ImageDoubleClick(this, new EventArgs());
        }

        private void IsFlagged_Click(object sender, RoutedEventArgs e)
        {
            Button source = e.OriginalSource as Button;

            if (source != null)
            {
                SubjectImage item = source.CommandParameter as SubjectImage;

                if (item != null)
                {
                    this.ToggleFlagImageInvoked(this, new EventArgs<SubjectImage>(item));
                }
            }
        }

        private void IsRemoteImageService_Click(object sender, RoutedEventArgs e)
        {
            Button source = e.OriginalSource as Button;

            if (source != null)
            {
                SubjectImage item = source.CommandParameter as SubjectImage;

                if (item != null)
                {
                    this.ToggleRemoteImageServiceInvoked(this, new EventArgs<SubjectImage>(item));
                }
            }

        }
	}
}