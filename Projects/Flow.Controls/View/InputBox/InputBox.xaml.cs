﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Flow.Controls.View.InputBox
{
    /// <summary>
    /// InputBox Class
    /// A simple InputBox.Show method made in WPF and C#
    /// 
    /// <author>Carlos Eugênio Torres</author>
    /// <date>2009/01/08</date>
    /// </summary>
    public partial class InputBox : Window
    {
        private static InputBox _newInputBox;
        private static string _returnValue;

        public InputBox()
        {
            InitializeComponent();
        }

        public static string Show(string inputBoxTitle)
        {
            _newInputBox = new InputBox();
            _newInputBox.Title = inputBoxTitle;
            _newInputBox.txtInput.Text = String.Empty;
            _newInputBox.ShowDialog();
            return _returnValue;
        }

        private void butOk_Click(object sender, RoutedEventArgs e)
        {
            confirm();
        }

        private void butCancel_Click(object sender, RoutedEventArgs e)
        {
            cancel();
        }

        private void txtInput_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                confirm();
            }
            else if (e.Key == Key.Escape)
            {
                cancel();
            }
        }

        private void confirm()
        {
            _returnValue = txtInput.Text;
            _newInputBox.Close();
            _newInputBox = null;
        }

        private void cancel()
        {
            _returnValue = String.Empty;
            _newInputBox.Close();
            _newInputBox = null;
        }
    }
}
