﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Lib;
using Flow.Lib.Helpers;
using Flow.Schema;
using Flow.Schema.LinqModel.DataContext;

namespace Flow.Controls.View.ImportDisplay
{
	/// <summary>
	/// Interaction logic for ColumnPropertiesControl.xaml
	/// </summary>
	public partial class ColumnPropertiesControl : UserControl
	{
		public event EventHandler<EventArgs<object>> ColumnMappingContextMenuOpened = delegate { };

		public event EventHandler<EventArgs<ISubjectField>> SubjectFieldSelected = delegate { };

		//protected static readonly DependencyProperty OmitColumnImageVisibilityProperty =
		//    DependencyProperty.Register("OmitColumnImageVisibility", typeof(Visibility), typeof(ColumnPropertiesControl));

		//public Visibility OmitColumnImageVisibility
		//{
		//    get { return (Visibility)this.GetValue(OmitColumnImageVisibilityProperty); }
		//    set { this.SetValue(OmitColumnImageVisibilityProperty, value); }
		//}


		//protected static readonly DependencyProperty UseSourceColumnImageVisibilityProperty =
		//    DependencyProperty.Register("UseSourceColumnImageVisibility", typeof(Visibility), typeof(ColumnPropertiesControl));

		//public Visibility UseSourceColumnImageVisibility
		//{
		//    get { return (Visibility)this.GetValue(UseSourceColumnImageVisibilityProperty); }
		//    set { this.SetValue(UseSourceColumnImageVisibilityProperty, value); }
		//}


		public ColumnPropertiesControl()
		{
			InitializeComponent();
		}

		//private void FieldNameMenuButton_MouseEnter(object sender, MouseEventArgs e)
		//{
		//    this.ContextMenu.PlacementTarget = sender as Button;
		//    this.ContextMenu.Placement = PlacementMode.Right;

		//    this.ContextMenu.IsOpen = true;
		//}

		private void ColumnMappingButton_Click(object sender, RoutedEventArgs e)
		{
			Button sourceButton = sender as Button;

			this.ContextMenu.PlacementTarget = sourceButton;
			this.ContextMenu.Placement = PlacementMode.Right;

			this.ContextMenu.IsOpen = true;

			this.ColumnMappingContextMenuOpened(this, new EventArgs<object>(this.DataContext));
		}

		private void SubjectFieldMenuItem_Clicked(object sender, RoutedEventArgs e)
		{
			MenuItem selectedItem = e.OriginalSource as MenuItem;

			ISubjectField selectedSubjectField = (ISubjectField)selectedItem.Header; 
	
			this.SubjectFieldSelected(this, new EventArgs<ISubjectField>(selectedSubjectField));
		}

		private void OmitColumnButton_Click(object sender, RoutedEventArgs e)
		{
			ImportColumnPair<ProjectSubjectField> item = this.DataContext as ImportColumnPair<ProjectSubjectField>;

			if(item.OmitColumn)
				this.SubjectFieldSelected(this, new EventArgs<ISubjectField>(item.SourceField));
			else
				this.SubjectFieldSelected(this, new EventArgs<ISubjectField>(null));

//			this.ColumnMappingContextMenu.IsOpen = false;
		}

	}
}
