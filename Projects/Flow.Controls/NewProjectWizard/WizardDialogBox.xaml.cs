using System;
using System.Windows;
using System.Windows.Navigation;

namespace Flow.NewProjectWizard
{
    public partial class WizardDialogBox : NavigationWindow
    {
        ProjectSettings projectSettings;

        public ProjectSettings ProjectSettings
        {
            get { return this.projectSettings; }
        }

        public WizardDialogBox()
        {
            InitializeComponent();

            // Launch the wizard
            WizardLauncher wizardLauncher = new WizardLauncher();
            wizardLauncher.WizardReturn += new WizardReturnEventHandler(wizardLauncher_WizardReturn);
            this.Navigate(wizardLauncher);
        }

        void wizardLauncher_WizardReturn(object sender, WizardReturnEventArgs e)
        {
            // Handle wizard return
            this.projectSettings = e.Data as ProjectSettings;
            if (this.DialogResult == null)
            {
                this.DialogResult = (e.Result == WizardResult.Finished);
            }
        }
    }
}