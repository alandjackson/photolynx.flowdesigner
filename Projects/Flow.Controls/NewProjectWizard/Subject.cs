﻿using System;
using System.Collections.Generic;

namespace Flow
{
    public class Subject
    {
        #region Attributes
        Guid _id;
        int _order;
        string _name;
        Subject _parent;
        List<Subject> _childSubjects;
        #endregion

        #region Properties
        public Guid Id
        {
            get
            {
                return this._id;
            }
        }

        public string Name
        {
            get
            {
                return this._name;
            }
            set
            {
                this._name = value;
            }
        }

        public int Order
        {
            get
            {
                return this._order;
            }
            set
            {
                this._order = value;
            }
        }

        public Subject Parent
        {
            get
            {
                return this._parent;
            }
        }

        public List<Subject> ChildSubjects
        {
            get
            {
                return this._childSubjects;
            }
        }

        public bool HasChildren
        {
            get
            {
                return (this._childSubjects != null && this._childSubjects.Count > 0);
            }
        }
        #endregion

        #region Constructors
        public Subject(string name)
        {
            this._id = Guid.NewGuid();
            this._name = name;
            this._order = 0;
            this._childSubjects = new List<Subject>();
        }

        public Subject(Subject parent, string name)
        {
            this._id = Guid.NewGuid();
            this._name = name;
            this._order = 0;
            this._childSubjects = new List<Subject>();
            this._parent = parent;
        }

        public Subject(string name, int order)
        {
            this._id = Guid.NewGuid();
            this._name = name;
            this._order = order;
            this._childSubjects = new List<Subject>();
        }

        public Subject(Subject parent, string name, int order)
        {
            this._id = Guid.NewGuid();
            this._name = name;
            this._order = order;
            this._childSubjects = new List<Subject>();
            this._parent = parent;
        }
        #endregion

        #region Public methods
        public void AddChildSubject(string name)
        {
            this._childSubjects.Add(new Subject(this, name));
        }

        public void RemoveChildSubject(Subject childSubject)
        {
            this._childSubjects.Remove(childSubject);
        }

        public void RemoveChildSubject(Guid id)
        {
            foreach (Subject s in this._childSubjects)
            {
                if (s.Id == id)
                {
                    this._childSubjects.Remove(s);
                    break;
                }
            }
        }

        public void SetChildSubjectName(Guid id, string name)
        {
            foreach (Subject s in this._childSubjects)
            {
                if (s.Id == id)
                {
                    s.Name = name;
                    break;
                }
            }
        }

        public void SetChildSubjectOrder(Guid id, int order)
        {
            foreach (Subject s in this._childSubjects)
            {
                if (s.Id == id)
                {
                    s.Order = order;
                    break;
                }
            }
        }
        #endregion
    }
}
