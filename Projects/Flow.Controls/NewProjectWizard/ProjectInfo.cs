﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flow.NewProjectWizard
{
    public class ProjectInfo
    {
        #region Attributes
        string _fieldLabel;
        string _fieldName;
        string _fieldSize;
        string _fieldValue;
        #endregion

        #region Properties
        public string FieldLabel
        {
            get 
            {
                return this._fieldLabel;
            }
            set
            {
                this._fieldLabel = value;
            }
        }

        public string FieldName
        {
            get
            {
                return this._fieldName;
            }
            set
            {
                this._fieldName = value;
            }
        }

        public string FieldSize
        {
            get
            {
                return this._fieldSize;
            }
            set
            {
                this._fieldSize = value;
            }
        }

        public string FieldValue
        {
            get
            {
                return this._fieldValue;
            }
            set
            {
                this._fieldValue = value;
            }
        }
        #endregion

        #region Contructors
        public ProjectInfo()
        { 
        
        }

        public ProjectInfo(string fieldLabel, string fieldName, string fieldSize)
        {
            this._fieldLabel = fieldLabel;
            this._fieldName = fieldName;
            this._fieldSize = fieldSize;
        }
        #endregion
    }
}
