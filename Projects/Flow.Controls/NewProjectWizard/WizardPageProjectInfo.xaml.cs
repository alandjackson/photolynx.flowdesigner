namespace Flow.NewProjectWizard
{
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Navigation;

    public partial class WizardPageProjectInfo : PageFunction<WizardResult>
    {
        public WizardPageProjectInfo(ProjectSettings projectSettings)
        {
            InitializeComponent();
            
            // Bind wizard state to UI
            this.DataContext = projectSettings;
        }

        void nextButton_Click(object sender, RoutedEventArgs e)
        {
            // Go to next wizard page
            WizardPageSubjects wizardPageSubjects = new WizardPageSubjects((ProjectSettings)this.DataContext);
            wizardPageSubjects.Return += new ReturnEventHandler<WizardResult>(wizardPage_Return);
            this.NavigationService.Navigate(wizardPageSubjects);
        }

        void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            // Cancel the wizard and don't return any data
            OnReturn(new ReturnEventArgs<WizardResult>(WizardResult.Canceled));
        }

        public void wizardPage_Return(object sender, ReturnEventArgs<WizardResult> e)
        {
            // If returning, wizard was completed (finished or canceled),
            // so continue returning to calling page
            OnReturn(e);
        }
    }
}