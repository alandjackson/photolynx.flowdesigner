namespace Flow.NewProjectWizard
{
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Navigation;

    public partial class WizardPageEventsActions : PageFunction<WizardResult>
    {
        public WizardPageEventsActions(ProjectSettings projectSettings)
        {
            InitializeComponent();

            // Bind wizard state to UI
            this.DataContext = projectSettings;
        }

        void backButton_Click(object sender, RoutedEventArgs e)
        {
            // Go to previous wizard page
            this.NavigationService.GoBack();
        }

        void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            // Cancel the wizard and don't return any data
            OnReturn(new ReturnEventArgs<WizardResult>(WizardResult.Canceled));
        }

        void finishButton_Click(object sender, RoutedEventArgs e)
        {
            // Finish the wizard and return bound data to calling page
            OnReturn(new ReturnEventArgs<WizardResult>(WizardResult.Finished));
        }
    }
}