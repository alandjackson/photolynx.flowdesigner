﻿using System;
using System.Xml;
using System.Xml.XPath;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Flow.NewProjectWizard;

namespace Flow
{
    public class ProjectSettings
    {
        #region Attributes
        List<ProjectInfo> _projectInfoList;
        List<Subject> _subjectsList;
        #endregion

        #region Properties
        public List<ProjectInfo> ProjectInfoList
        {
            get
            {
                return this._projectInfoList;
            }
        }

        public List<Subject> SubjectsList
        {
            get
            {
                return this._subjectsList;
            }
            set
            {
                this._subjectsList = value;
            }
        }
        #endregion

        #region Contructor & destructor
        public ProjectSettings()
        {
            LoadProjectInfoFromXml();
            this._subjectsList = new List<Subject>();
        }
        ~ProjectSettings()
        {
            this._projectInfoList = null;
        }
        #endregion
       
        #region Public methods
        public void SetProjectInfo(string fieldName, string fieldValue)
        {
            foreach (ProjectInfo pi in this._projectInfoList)
            {
                if (pi.FieldName == fieldName)
                {
                    pi.FieldValue = fieldValue;
                    break;
                }
            }
        }
        #endregion

        #region Private methods
        void LoadProjectInfoFromXml()
        {
            XPathNavigator nav;
            XPathDocument docNav;
            XPathNodeIterator nodeIter;
            try
            {
                docNav = new XPathDocument(@"Config/ProjectInfoFields.xml");
                nav = docNav.CreateNavigator();
                this._projectInfoList = new List<ProjectInfo>();

                nodeIter = nav.Select("/ProjectInfo/Fields/Field");
                while (nodeIter.MoveNext())
                {
                    this._projectInfoList.Add(new ProjectInfo(nodeIter.Current.GetAttribute("Label", ""), nodeIter.Current.GetAttribute("Name", ""), nodeIter.Current.GetAttribute("Size", "")));
                }
            }
            finally
            {
                nav = null;
                docNav = null;
                nodeIter = null;
            }
        }
        #endregion
    }
}
