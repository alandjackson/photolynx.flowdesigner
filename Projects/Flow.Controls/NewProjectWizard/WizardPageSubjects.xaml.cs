namespace Flow.NewProjectWizard
{
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Navigation;
    using System.Globalization;
    using System.Windows.Data;
    using System.Windows.Media.Imaging;
    using System.Windows.Controls.Primitives;
    using System.Linq;
    using System.Windows.Media;

    public partial class WizardPageSubjects : PageFunction<WizardResult>
    {
        public WizardPageSubjects(ProjectSettings projectSettings)
        {
            InitializeComponent();

            // Bind wizard state to UI
            this.DataContext = projectSettings;
        }

        void backButton_Click(object sender, RoutedEventArgs e)
        {
            // Go to previous wizard page
            this.NavigationService.GoBack();
        }

        void nextButton_Click(object sender, RoutedEventArgs e)
        {
            // Go to next wizard page
            WizardPageEventsActions wizardPageEventsActions = new WizardPageEventsActions((ProjectSettings)this.DataContext);
            wizardPageEventsActions.Return += new ReturnEventHandler<WizardResult>(wizardPage_Return);
            this.NavigationService.Navigate(wizardPageEventsActions);
        }

        void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            // Cancel the wizard and don't return any data
            OnReturn(new ReturnEventArgs<WizardResult>(WizardResult.Canceled));
        }

        public void wizardPage_Return(object sender, ReturnEventArgs<WizardResult> e)
        {
            // If returning, wizard was completed (finished or canceled),
            // so continue returning to calling page
            OnReturn(e);
        }

        private void mnuAddSubject_Click(object sender, RoutedEventArgs e)
        {
            string subjectName = Flow.Controls.View.InputBox.InputBox.Show("Enter Subject Name");
            if (!subjectName.Trim().Equals(String.Empty))
            {
                ((ProjectSettings)this.DataContext).SubjectsList.Add(new Subject(subjectName));
                tvSubjects.Items.Refresh();
            }
            e.Handled = true;
        }

        private void mnuAddSubSubject_Click(object sender, RoutedEventArgs e)
        {
            if (tvSubjects.SelectedItem != null)
            {
                string subjectName = Flow.Controls.View.InputBox.InputBox.Show("Enter Subject Name");
                if (!subjectName.Trim().Equals(String.Empty))
                {
                    Subject s = (Subject)tvSubjects.SelectedItem;
                    s.AddChildSubject(subjectName);
                    tvSubjects.Items.Refresh();
                    tvSubjects.SelectedValuePath += "subjectName";
                }
                e.Handled = true;
            }
        }

        private void mnuRemoveSubject_Click(object sender, RoutedEventArgs e)
        {
            if (tvSubjects.SelectedItem != null)
            {
                Subject s = (Subject)tvSubjects.SelectedItem;
                if (((ProjectSettings)this.DataContext).SubjectsList.Contains(s))
                {
                    ((ProjectSettings)this.DataContext).SubjectsList.Remove((Subject)tvSubjects.SelectedItem);
                }
                else
                {
                    s.Parent.RemoveChildSubject(s);
                }
                tvSubjects.Items.Refresh();
                e.Handled = true;
            }
        }

        private void tvSubjects_Expanded(object sender, RoutedEventArgs e)
        {
            TreeViewItem item = (TreeViewItem)e.OriginalSource;
            if (item != null)
            {
                Subject s = (Subject)item.DataContext;
                if (s != null && s.HasChildren)
                    item.ItemsSource = s.ChildSubjects;
            }
        }

        //public void SetSelectedNodeByPosition(TreeView tv,
        //                                   int mouseX,
        //                                   int mouseY)
        //{
        //    TreeViewItem node = null;
        //    try
        //    {
        //        Point pt = new Point(mouseX, mouseY);
        //        tv.PointToScreen(pt);
        //        node = tv.Items.(pt);
        //        tv.SelectedNode = node;
        //        if (node == null) return;
        //        if (!node.Bounds.Contains(pt)) { return; }
        //    }
        //    catch { }
        //}
        /*
        int GetCurrentIndex(GetPositionDelegate getPosition)
        {
            int index = -1;
            for (int i = 0; i < this.tvSubjects.Items.Count; ++i)
            {
                TreeViewItem item = GetTreeViewItem(i);
                if (this.IsMouseOverTarget(item, getPosition))
                {
                    index = i;
                    break;
                }
            }
            return index;
        }

        bool IsMouseOverTarget(Visual target, GetPositionDelegate getPosition)
        {
            Rect bounds = VisualTreeHelper.GetDescendantBounds(target);
            Point mousePos = getPosition((IInputElement)target);
            return bounds.Contains(mousePos);
        }

        TreeViewItem GetTreeViewItem(int index)
        {
            if (tvSubjects.ItemContainerGenerator.Status != GeneratorStatus.ContainersGenerated)
                return null;

            return tvSubjects.ItemContainerGenerator.ContainerFromIndex(index) as TreeViewItem;
        }
        */
    }

    delegate Point GetPositionDelegate(IInputElement element);

    [ValueConversion(typeof(string), typeof(bool))]
    public class HeaderToImageConverter : IValueConverter
    {
        public static HeaderToImageConverter Instance = new HeaderToImageConverter();

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (((Subject)value).HasChildren)
            {
                Uri uri = new Uri("pack://application:,,,/images/wizard/group.png");
                BitmapImage source = new BitmapImage(uri);
                return source;
            }
            else
            {
                Uri uri = new Uri("pack://application:,,,/images/wizard/user.png");
                BitmapImage source = new BitmapImage(uri);
                return source;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException("Cannot convert back");
        }
    }
}