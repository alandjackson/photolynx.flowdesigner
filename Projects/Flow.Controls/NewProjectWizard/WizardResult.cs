namespace Flow.NewProjectWizard
{
    using System;

    public enum WizardResult
    {
        Finished,
        Canceled
    }
}
