﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Data;

namespace Flow.Controls.ValueConverters
{
        public class FileInfoToUriConverter : IValueConverter
        {
            public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
            {
                if (value is FileInfo)
                {
                    return new Uri(((FileInfo)value).FullName);
                }
                else
                {
                    throw new ArgumentException("Value Must Be FileInfo Type");
                }
            }
            public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
            {
                throw new NotImplementedException();
            }
        }
}
