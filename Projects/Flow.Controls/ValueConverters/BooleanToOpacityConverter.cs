﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;

using Flow.Schema;

namespace Flow.Controls.ValueConverters
{
	public class BooleanToOpacityConverter : IValueConverter
	{

		#region IValueConverter Members

		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			Nullable<bool> retVal = value as Nullable<bool>;

			return ((retVal == null || !retVal.Value) ? 0 : 1);
		}

		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			throw new NotImplementedException();
		}

		#endregion
	}
}
