﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.Objects;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Input;

using Flow.Controller.Capture;
using Flow.Controller.Catalog;
using Flow.Lib;
using Flow.Lib.Helpers;
using Flow.Lib.Helpers.BarcodeScan;
using Flow.Schema;
using Flow.Schema.LinqModel.DataContext;
using Flow.View;
using Flow.ViewModel.Capture;
using Flow.View.Dialogs;
using System.Windows.Controls;
using Flow.Model;
using NLog;
using System.Windows.Media.Imaging;
using Flow.Lib.Preferences;
using ExifLib;

namespace Flow.Controller
{
    public class CaptureController
    {
        public CaptureDockPanel CaptureDockPanel { get; set; }

		public IShowsNotificationProgress Notifier { get; set; }

		CurrentImageController CurrentImageController { get; set; }
        GrayCardController GrayCardController { get; set; }
		public HotFolderController HotfolderController { get; set; }
		public SubjectRecordController SubjectRecordController { get; set; }
		public OrderEntryController OrderEntryController { get; set; }
        public LayoutPreviewController LayoutPreviewController { get; set; }
        public AutoPrintController AutoPrintController { get; set; }

		public BarcodeScanDetector BarcodeScanDetector { get; private set; }

        public bool SaveLayoutOnClose { get; set; }

        private static Logger logger = LogManager.GetCurrentClassLogger();

		private FlowController _flowController = null;
		public FlowController FlowController
		{
			get { return _flowController; }
			set
			{
				_flowController = value;
				CaptureDockPanel.FlowController = _flowController;
                LayoutPreviewController.FlowController = _flowController;
            }
		}

		private bool _orderEntryInitialized = false;


        public CaptureController(
			HotFolderController hfc,
			CurrentImageController cic,
			SubjectRecordController src,
			OrderEntryController oec,
            LayoutPreviewController lpc
		)
        {

            SaveLayoutOnClose = true;

            Notifier = null;

            LayoutPreviewController = lpc;

			CurrentImageController = cic;
			HotfolderController = hfc;
			SubjectRecordController = src;
			OrderEntryController = oec;

			HotfolderController.SelectedHotfolderImageChanged += 
                new EventHandler<EventArgs<Flow.Model.HotfolderImage>>(HotfolderController_SelectedHotfolderImageChanged);

            HotfolderController.NewHotfolderImageAdded +=new EventHandler<EventArgs<HotfolderImage>>(HotfolderController_NewHotfolderImageAdded);
			HotfolderController.HotfolderImageAssigned +=
				new EventHandler<EventArgs<Collection<string>>>(HotfolderController_HotfolderImageAssigned);

            HotfolderController.HotfolderGroupImageAssigned +=
                new EventHandler<EventArgs<Collection<string>>>(HotfolderController_HotfolderGroupImageAssigned);

            HotfolderController.HotfolderImageDeleted += new EventHandler<EventArgs<Collection<string>>>(HotfolderController_HotfolderImageDeleted);

            
           

        }

        public event EventHandler<EventArgs<Subject>> ShowOrderEntryRequested = delegate { };
        public event EventHandler<EventArgs<string>> AutoPrint = delegate { };


        public GrayCardViewModel GrayCardViewModel { get; set; }
        public void Main(CaptureDockPanel pnl, FlowController flowController)
        {
            //this.FlowController = flowController;
			this.HotfolderController.FlowMasterDataContext =
				flowController.ProjectController.FlowMasterDataContext;
            this.HotfolderController.FlowController = flowController;

			// Assign the currently open Flow project to the SubjectRecordController
			this.SubjectRecordController.ProjectController = flowController.ProjectController;
			this.OrderEntryController.ProjectController = flowController.ProjectController;

            GrayCardViewModel = new GrayCardViewModel(pnl.GrayCardPanel);
            pnl.GrayCardPanel.DataContext = GrayCardViewModel;

			flowController.ProjectController.PropertyChanged +=
				new PropertyChangedEventHandler(this.SubjectRecordController.ProjectController_PropertyChanged);

			flowController.OpenOrderEntryInvoked += new EventHandler(ProjectController_OpenOrderEntryInvoked);

			CaptureDockPanel = pnl;
			CaptureDockPanel.CaptureController = this;
            this.CurrentImageController.SetFlowController(flowController);
			CaptureDockPanel.CurrentImageController = this.CurrentImageController;
			CaptureDockPanel.HotFolderController = this.HotfolderController;
			CaptureDockPanel.SubjectRecordController = this.SubjectRecordController;

            CaptureDockPanel.pnlGroupPhotos.DataContext = flowController.ProjectController.CurrentFlowProject.FlowProjectDataContext.GroupImageList;
            CaptureDockPanel.pnlGroupPhotos.FlowController = flowController;

           

			// Set initial focus state
			CaptureDockPanel.SelectedImageDocumentContent.SetAsActive();
			CaptureDockPanel.SelectedImageDocumentContent.Focus();
			this.SetSearchFocus();

            this.SubjectRecordController.ToggleGreenscreenInvoked +=
              new EventHandler<EventArgs<SubjectImage>>(SubjectRecordController_ToggleGreenscreenInvoked);

            this.SubjectRecordController.ToggleFlagImageInvoked +=
              new EventHandler<EventArgs<SubjectImage>>(SubjectRecordController_ToggleFlagImageInvoked);

            this.SubjectRecordController.ToggleRemoteImageServiceInvoked +=
              new EventHandler<EventArgs<SubjectImage>>(SubjectRecordController_ToggleRemoteImageServiceInvoked);
  
			this.SubjectRecordController.ImageUnassigned +=
				new EventHandler<EventArgs<IEnumerable<SubjectImage>>>(SubjectRecordController_ImageUnassigned);

			this.SubjectRecordController.HotfolderAutoAssignRequested +=
				new EventHandler<EventArgs<Subject>>(SubjectRecordController_HotfolderAutoAssignRequested);

            this.CaptureDockPanel.Loaded += new RoutedEventHandler(CaptureDockPanel_Loaded);

            this.SubjectRecordController.ShowOrderEntryRequested +=
                new EventHandler<EventArgs<Subject>>(SubjectRecordController_ShowOrderEntryRequested);

            this.SubjectRecordController.AutoPrint +=
                new EventHandler<EventArgs<string>>(SubjectRecordController_AutoPrint);

            //this.HotfolderController.HotfolderAutoAssignLatestRequested +=
            //    new EventHandler<EventArgs<string>>(HotfolderController_HotfolderAutoAssignLatestRequested);

            this.SubjectRecordController.ShowOrderEntry += new EventHandler(ShowOrderEntry);
            this.SubjectRecordController.ShowSubmitOrders += new EventHandler(ShowSubmitOrders);
            this.SubjectRecordController.ShowAdjustImages += new EventHandler(ShowAdjustImages); 
            this.OrderEntryController.CloseOrderEntryRequested += new EventHandler<RoutedEventArgs>(OrderEntryController_CloseOrderEntryRequested);

			// Instantiate the barcode detector from the project barcode pattern descriptors
			FlowProject currentFlowProject = flowController.ProjectController.CurrentFlowProject;
			this.BarcodeScanDetector = new BarcodeScanDetector(
				currentFlowProject.BarcodeScanInitSequence,
				currentFlowProject.BarcodeScanTermSequence,
				currentFlowProject.BarcodeScanValueLength
			);

            this.AutoPrintController = new AutoPrintController( pnl.Dispatcher, flowController.ProjectController);

            this.AutoPrintController.AutoPrintStopped += new EventHandler<RunWorkerCompletedEventArgs>(AutoPrintController_AutoPrintStopped);

            pnl.HotfolderPanel.HotfolderListbox.UpdateMainImage +=
                new EventHandler<EventArgs<FlowImage>>
                    (HotFolder_UpdateMainImage);
            pnl.pnlGroupPhotos.GroupPhotoListbox.UpdateMainImage +=
                new EventHandler<EventArgs<GroupImage>>
                    (GroupPhotos_UpdateMainImage);
            pnl.SubjectRecordPanel.SubjectRecordDisplay.SubjectImageList.UpdateMainImage +=
                 new EventHandler<EventArgs<SubjectImage>>
                    (SubjectRecord_UpdateMainImage);


            pnl.pnlGroupPhotos.GroupPhotoListbox.AssignGroupImage +=
               new EventHandler<EventArgs<GroupImage>>
                   (GroupPhotos_AssignGroupImage);

            pnl.pnlGroupPhotos.GroupPhotoListbox.AssignAllGroupImage +=
              new EventHandler<EventArgs<GroupImage>>
                  (GroupPhotos_AssignAllGroupImage);

            pnl.pnlGroupPhotos.GroupPhotoListbox.RemoveGroupImage +=
               new EventHandler<EventArgs<GroupImage>>
                   (GroupPhotos_RemoveGroupImage);

            pnl.SubjectRecordPanel.SubjectRecordDisplay.SubjectImageList.ListBoxSelectionChanged += new SelectionChangedEventHandler(SubjectImageList_ListBoxSelectionChanged);
            //flowController.ProjectController.CurrentFlowProject.FlowProjectDataContext.GroupImageList.CollectionChanged +=
            //     new System.Collections.Specialized.NotifyCollectionChangedEventHandler(GroupImageList_CollectionChanged);

            //if (flowController.ProjectController.CurrentFlowProject.FlowProjectDataContext.GroupImageList.HasItems)
            //    CaptureDockPanel.paneGroupPhoto.Visibility = Visibility.Visible;
            //else
            //    CaptureDockPanel.paneGroupPhoto.Visibility = Visibility.Collapsed;

            pnl.pnlGroupPhotos.GroupPhotoListbox.SaveProjectDataContextInvoked += new EventHandler(GroupPhotoListbox_SaveProjectDataContextInvoked);

            LayoutPreviewController.FlowController = flowController;
            LayoutPreviewController.Main(pnl.uxTemplatePreview);


            if (flowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.IsBasic)
                this.CaptureDockPanel.GrayCardDocumentContent.Visibility = Visibility.Collapsed;
        }

        void CaptureDockPanel_Loaded(object sender, RoutedEventArgs e)
        {
            
        }

        void GroupPhotoListbox_SaveProjectDataContextInvoked(object sender, EventArgs e)
        {
            this.FlowController.ProjectController.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();
        }

        void SubjectImageList_ListBoxSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SubjectImage selectedSubjectImage = (e == null) ? null : (e.Source as ListBox).SelectedItem as SubjectImage;

            if (selectedSubjectImage == null)
                logger.Info("Current subject image selection changed and is now null");
            else
                logger.Info("Current subject image selection changed to '{0}'", selectedSubjectImage.ImageFileName);

            CurrentImageController.DisplaySelectedSubjectImage(selectedSubjectImage, false);

            if (this.SubjectRecordController.CurrentSubject != null &&
                this.SubjectRecordController.CurrentSubject.SubjectImages != null)
            {
                //
                // Every time the selected item changes in the hot folder, we need to clear the bitmaps from memory on the images that are not in view
                // To do this we will clear 100 images before the current image - 5 and 100 images after the current image + 5
                // so we should not be storing more than 10 images at a time in memory
                //

                //what is the index of the currently selected hotfolder image
                int i = this.CaptureDockPanel.SubjectRecordPanel.SubjectRecordDisplay.SubjectImageList.SelectedIndex;
                if (i >= 0)
                {
                    //dispose all images 5-15 before the image, and 5-15 after the image
                    int start = i - 0;
                    int delCount = 5;
                    int loop = 0;
                    while ((start - loop) > 0 && delCount-- > 0)
                    {
                        SubjectImage image = this.SubjectRecordController.CurrentSubject.SubjectImages[start - loop];
                        image.FlowImage.Clear();
                        loop++;
                    }


                    int itemCount = this.SubjectRecordController.CurrentSubject.SubjectImages.Count();
                    start = i + 0;
                    delCount = 100;
                    loop = 0;
                    while ((start + loop) < itemCount && delCount-- > 0)
                    {
                        SubjectImage image = this.SubjectRecordController.CurrentSubject.SubjectImages[start + loop];
                        image.FlowImage.Clear();
                        loop++;
                    }
                }
            }

            //this.CaptureDockPanel.SubjectRecordPanel.SubjectRecordDisplay.SubjectImageList.ItemsSource = null;
            //this.CaptureDockPanel.SubjectRecordPanel.SubjectRecordDisplay.SubjectImageList.ItemsSource = this.SubjectRecordController.CurrentSubject.SubjectImages;
            //this.CaptureDockPanel.SubjectRecordPanel.SubjectRecordDisplay.SubjectImageList.UpdateLayout();
        }

        //void GroupImageList_CollectionChanged(object sender, EventArgs e)
        //{
        //    if (this.FlowController.ProjectController.CurrentFlowProject.FlowProjectDataContext.GroupImageList.HasItems)
        //        CaptureDockPanel.paneGroupPhoto.Visibility = Visibility.Visible;
        //    else
        //        CaptureDockPanel.paneGroupPhoto.Visibility = Visibility.Collapsed;
        //}

        void HotFolder_UpdateMainImage(object sender,
           EventArgs<FlowImage> e)
        {
            CurrentImageController.DisplaySelectedFlowImage(
                (e == null) ? null : e.Data, true
            );
            
        }

        void GroupPhotos_UpdateMainImage(object sender,
            EventArgs<GroupImage> e)
        {
            CurrentImageController.DisplaySelectedGroupImage(
                (e == null) ? null : e.Data, true
            );
        }


        void SubjectRecord_UpdateMainImage(object sender,
            EventArgs<SubjectImage> e)
        {

                CurrentImageController.DisplaySelectedSubjectImage(
                    (e == null) ? null : e.Data, true
                );
        }


        void GroupPhotos_AssignGroupImage(object sender,
            EventArgs<GroupImage> e)
        {
            GroupImage img = e.Data as GroupImage;

            if (!this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Permissions.CanAssignImagesToSubjects)
                return;


            if (this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.UseGroupObject)
            {
                if (this.FlowController.ProjectController.CurrentFlowProject.CurrentGroup == null)
                {
                    FlowMessageBox msg = new FlowMessageBox("Select Group Image", "You must first select a group Image");
                    msg.CancelButtonVisible = false;
                    msg.ShowDialog();

                    return;
                }
                this.FlowController.ProjectController.CurrentFlowProject.CurrentGroup.AssignGroupImage(img);
                return;
            }
			// If there is a subject current of the project SubjectList...
			if (SubjectRecordController.CurrentSubject != null  && !SubjectRecordController.CurrentSubject.IsNew)
			{
				FileInfo imageFile = new FileInfo(img.ImagePath);

                if (!imageFile.Exists)
                {
                    logger.Warn("Image file didn't exist when trying to assign group image: fileName='{0}'", img.ImagePath);
                    return;
                }

				// Assign the image to the subject
				//SubjectRecordController.AssignHotfolderImage(imageFile.Name);
                string returnString = SubjectRecordController.AssignGroupImage(img);

                // this.FlowController.ProjectController.CurrentFlowProject.RefreshHasMissingImages();

                if (this.FlowController.ProjectController.CurrentFlowProject.GetEventTriggerSetting(8))
                    this.ShowOrderEntryRequested(this, new EventArgs<Subject>(null));
                if (this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.AutoPrintLayout)
                    this.AutoPrint(this, new EventArgs<string>(returnString));
			}
        }

        void GroupPhotos_AssignAllGroupImage(object sender,
            EventArgs<GroupImage> e)
        {

            int subs = this.FlowController.ProjectController.CurrentFlowProject.SubjectList.View.Count;
            string message = "\nYou are about to add this Image to all Subjects in the current filter.\n\nThis Image will be added to " + subs + " subjects.\n\n This is an irreversible operation!";
			//this.AddToAllConfirmPanelVisibility = Visibility.Visible;
            FlowMessageBox msg = new FlowMessageBox("Confirm Bulk Image Add", message);
            msg.buttonOkay.Content = "Confirm";
            if (msg.ShowDialog() == true)
            {

                GroupImage img = e.Data as GroupImage;

                if (!this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Permissions.CanAssignImagesToSubjects)
                    return;

                // If there is a subject current of the project SubjectList...
                if (SubjectRecordController.CurrentSubject != null && !SubjectRecordController.CurrentSubject.IsNew)
                {

                    FileInfo imageFile = new FileInfo(img.ImagePath);

                    if (!imageFile.Exists)
                    {
                        logger.Warn("Image file didn't exist when trying to assign group image: fileName='{0}'", img.ImagePath);
                        return;
                    }

                    // Assign the image to the subject
                    //SubjectRecordController.AssignHotfolderImage(imageFile.Name);
                    SubjectRecordController.AssignAllGroupImage(img);

                    //this.FlowController.ProjectController.CurrentFlowProject.RefreshHasMissingImages();

                }
            }


        }

        void GroupPhotos_RemoveGroupImage(object sender,
           EventArgs<GroupImage> e)
        {
            GroupImage img = e.Data as GroupImage;
            if (img.GetSubjectImageList.Count > 0)
            {
                FlowMessageBox msg = new FlowMessageBox("Remove Group Image", "You may not remove a group image when it is still assigned to a subject");
                msg.CancelButtonVisible = false;
                msg.ShowDialog();
                return;
            }

            if (!this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Permissions.CanUnassignImagesFromSubjects)
            {
                logger.Warn("Tried to remove group image without CanUnassignImagesFromSubjects permission");
                return;
            }

            img.DisposeImages();
            
            this.FlowController.ProjectController.CurrentFlowProject.FlowProjectDataContext.GroupImageList.Remove(img);
            this.FlowController.ProjectController.CurrentFlowProject.FlowProjectDataContext.GroupImages.DeleteOnSubmit(img);
            this.FlowController.ProjectController.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();

            if (!this.FlowController.ProjectController.CurrentFlowProject.FlowProjectDataContext.DeletedSubjectImages.Any(ds => ds.SubjectImageGuid == img.GroupImageGuid))
            {
                DeletedSubjectImage dsi = new DeletedSubjectImage();
                dsi.DeleteDate = DateTime.Now;
                dsi.SubjectImageGuid = img.GroupImageGuid;
                this.FlowController.ProjectController.CurrentFlowProject.FlowProjectDataContext.DeletedSubjectImages.InsertOnSubmit(dsi);
            }
            //only move it to hot folder if its not a group image

            string newImageFilePath = Path.Combine(
                this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Capture.HotFolderDirectory,
                img.ImageOriginalFileName
            );

            this.FlowController.ProjectController.MoveFile(img.ImagePath, newImageFilePath);

            logger.Info("Removed group image: fileName='{0}'", img.ImagePath);
        }

        void OrderEntryController_CloseOrderEntryRequested(object sender, RoutedEventArgs e)
        {
            CaptureDockPanel.SelectedImageDocumentContent.SetAsActive();
            CaptureDockPanel.OrderEntryDocumentContent.Visibility = Visibility.Collapsed;
        }

        private void AutoPrintController_AutoPrintStopped(object sender, RunWorkerCompletedEventArgs e)
        {
            LayoutPreviewController.StopAutoPrint();
        }


		//public void EditCurrentRecord()
		//{
		//    CaptureDockPanel.EditCurrentRecord();
		//}




        void HotfolderController_NewHotfolderImageAdded(object sender, 
            EventArgs<Flow.Model.HotfolderImage> e)
        {
            CurrentImageController.DisplaySelectedImage(
                (e == null) ? null : e.Data, true
            );
        }


        void HotfolderController_SelectedHotfolderImageChanged(object sender, 
            EventArgs<Flow.Model.HotfolderImage> e)
        {
            CurrentImageController.DisplaySelectedImage(
                (e == null) ? null : e.Data, false
            );

            if (this.HotfolderController.HotfolderImages != null)
            {
                //
                // Every time the selected item changes in the hot folder, we need to clear the bitmaps from memory on the images that are not in view
                // To do this we will clear 100 images before the current image - 5 and 100 images after the current image + 5
                // so we should not be storing more than 10 images at a time in memory
                //
               

                //what is the index of the currently selected hotfolder image
                int i = this.CaptureDockPanel.HotfolderPanel.HotfolderListbox.SelectedIndex;
                if (i >= 0)
                {
                    //dispose all images 5-15 before the image, and 5-15 after the image
                    int start = i - 0;
                    int delCount = 5;
                    int loop = 0;
                    while ((start - loop) > 0 && delCount-- > 0)
                    {
                        HotfolderImage image = this.HotfolderController.HotfolderImages[start - loop];
                        image.Clear();
                        loop++;
                    }


                    int itemCount = this.HotfolderController.HotfolderImages.Count();
                    start = i + 0;
                    delCount = 100;
                    loop = 0;
                    while ((start + loop) < itemCount && delCount-- > 0)
                    {
                        HotfolderImage image = this.HotfolderController.HotfolderImages[start + loop];
                        image.Clear();
                        loop++;
                    }
                }
            }

           
        }

		/// <summary>
		/// Moves the specified hotfolder image to the target project image directory and
		/// assigns the image to the current Subject record
		/// </summary>
		void HotfolderController_HotfolderImageAssigned(object sender, EventArgs<Collection<string>> e)
		{
            
            SubjectImage imageToRemove = null;
            if (!this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Permissions.CanAssignImagesToSubjects)
                return;
            if (this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Capture.OneImagePerSubject == true && SubjectRecordController.CurrentSubject != null && SubjectRecordController.CurrentSubject.SubjectImages.Count > 0)
            {
                string newImageFileName = e.Data[0];
                if (!File.Exists(newImageFileName))
                    return;
                BitmapImage oldImage = SubjectRecordController.CurrentSubject.SubjectImages.Last().ProcessingImage;
                BitmapImage newImage = new BitmapImage(new Uri(newImageFileName));
                PickAnImage pick = new PickAnImage(newImage.Clone(),oldImage);
                bool? keepNew = pick.ShowDialog();
                newImage = null;
                if (keepNew == true)
                    imageToRemove = SubjectRecordController.CurrentSubject.SubjectImages.Last();
                else
                {
                    ShowCurrentSubjectSelectedImage();
                    return; //do nothing, we are keeping the old image
                }

                //FlowMessageBox msg = new FlowMessageBox("Only 1 Image Allowed", "Only 1 image is allowed per subject.\n\nThe current subject already has an Image");
                //msg.CancelButtonVisible = false;
                //msg.ShowDialog();
                //return;
            }

            //int i = this.CaptureDockPanel.HotfolderPanel.HotfolderListbox.SelectedIndex;
            //HotfolderImage image = this.HotfolderController.HotfolderImages[i];
            //this.HotfolderController.HotfolderImages.Remove(image);
            //image.Clear();

			// If there is a subject current of the project SubjectList...
            if (SubjectRecordController != null && SubjectRecordController.CurrentSubject != null && !SubjectRecordController.CurrentSubject.IsNew)
			{
				// Iterate through the list of image filepaths to assign
				foreach (string imageFileName in e.Data)
				{
					FileInfo imageFile = new FileInfo(imageFileName);

					if (!imageFile.Exists)
						continue;

                    string newImageFileName = "";
					// Assign the image to the subject
                    try
                    {
                        logger.Info("About to assign Image" + imageFile.Name);
                        newImageFileName = SubjectRecordController.AssignHotfolderImage(imageFile.Name, FlowController.ProjectController.CustomSettings.Force8x10Crop);
                    }
                    catch (Exception ee)
                    {
                        logger.Info("ERROR - " + ee.Message);
                            FlowMessageBox msg = new FlowMessageBox("Error Assigning Image", "There was an error assigning images, please try again");
                            msg.CancelButtonVisible = false;
                            msg.ShowDialog();

                            continue;
                    }

					// Create the destination filename
					string newImageFilePath = Path.Combine(_flowController.ProjectController.CurrentFlowProject.ImageDirPath, newImageFileName);

					// Copy the image to the project's image directory
					this.FlowController.ProjectController.MoveFile(imageFileName, newImageFilePath);

                    if (FlowController.ProjectController.CustomSettings.Force8x10Crop)
                    {
                        SubjectImage newsi = SubjectRecordController.CurrentSubject.SubjectImages.FirstOrDefault(si => si.ImagePathFullRes == newImageFilePath);
                        if (newsi != null)
                            newsi.AutoCrop();

                    }

                    this.FlowController.ProjectController.CurrentFlowProject.RefreshHasMissingImages();

                    if (this.FlowController.ProjectController.CurrentFlowProject.GetEventTriggerSetting(8))
                        this.ShowOrderEntryRequested(SubjectRecordController, new EventArgs<Subject>(null));
                    if (this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.AutoPrintLayout)
                        this.AutoPrint(this, new EventArgs<string>(newImageFileName));
                    //this.LayoutPreviewController.UpdateLayoutPreview();
				}
			}
            
            this.CaptureDockPanel.SubjectRecordPanel.SubjectRecordDisplay.SubjectImageList.UpdateLayout();

            if (imageToRemove != null)
            {
                List<SubjectImage> selectedImages = new List<SubjectImage>();
                selectedImages.Add(imageToRemove);
                SubjectRecordController_ImageUnassigned(this, new EventArgs<IEnumerable<SubjectImage>>(selectedImages));
                //SubjectRecordController.CurrentSubject.DetachImage(imageToRemove);
            }

            ShowCurrentSubjectSelectedImage();
		}
        private int GroupPhotoNumber = 0;
        void HotfolderController_HotfolderGroupImageAssigned(object sender, EventArgs<Collection<string>> e)
        {
            if (!this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Permissions.CanAssignImagesToSubjects)
                return;

            //int i = this.CaptureDockPanel.HotfolderPanel.HotfolderListbox.SelectedIndex;
            //HotfolderImage tmpimage = this.HotfolderController.HotfolderImages[i];
            //this.HotfolderController.HotfolderImages.Remove(tmpimage);
            //tmpimage.Clear();

                // Iterate through the list of image filepaths to assign
                foreach (string imageFileName in e.Data)
                {
                    FileInfo imageFile = new FileInfo(imageFileName);

                    if (!imageFile.Exists)
                        continue;
                    
                    // Assign the image as a group image
                    string newImageFileName = "GroupPhoto_" + (++GroupPhotoNumber).ToString("00000") + ".jpg";
                    string destFile = Path.Combine(_flowController.ProjectController.CurrentFlowProject.ImageDirPath, newImageFileName);
                    while (File.Exists(destFile) || _flowController.ProjectController.CurrentFlowProject.FlowProjectDataContext.GroupImages.Any(gi => gi.ImageFileName == newImageFileName))
                    {
                        newImageFileName = "GroupPhoto_" + (++GroupPhotoNumber).ToString("00000") + ".jpg";
                        destFile = Path.Combine(_flowController.ProjectController.CurrentFlowProject.ImageDirPath, newImageFileName);
                        if (GroupPhotoNumber > 99999) return;
                    }
                   

                    GroupImage image = new GroupImage(this.FlowController.ProjectController.CurrentFlowProject.FlowProjectDataContext);

                    image.FlowProjectGuid = this.FlowController.ProjectController.CurrentFlowProject.FlowProjectDataContext.FlowProject.FlowProjectGuid;
                    image.ImageFileName = newImageFileName;
                    image.ImageOriginalFileName = imageFileName;
                    image.DateAssigned = DateTime.Now;
                    image.ImagePath = destFile;
                    

                    if (this.FlowController.ProjectController.CurrentFlowProject.SelectedGroupImageFlags.Count > 1)
                        image.SelectedFlag = this.FlowController.ProjectController.CurrentFlowProject.SelectedGroupImageFlags[1];

                    // Due to a limitation of LINQ-to-SQL, items of child collections in related entities
                    // must also be added to the entity exposed by the managing datacontext

                    // Add to the datacontext entity
                    if(!this.FlowController.ProjectController.CurrentFlowProject.FlowProjectDataContext.GroupImageList.Contains(image))
                        this.FlowController.ProjectController.CurrentFlowProject.FlowProjectDataContext.GroupImageList.Add(image);
                    this.FlowController.ProjectController.CurrentFlowProject.FlowProjectDataContext.GroupImages.InsertOnSubmit(image);
                    this.FlowController.ProjectController.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();
                    


                   

                    // Create the destination filename
                    string newImageFilePath = Path.Combine(_flowController.ProjectController.CurrentFlowProject.ImageDirPath, newImageFileName);

                    // Copy the image to the project's image directory
                    this.FlowController.ProjectController.MoveFile(imageFileName, newImageFilePath);

                    if (FlowController.ProjectController.CustomSettings.Force8x10Crop)
                        image.AutoCrop();

                    //this.FlowController.ProjectController.CurrentFlowProject.RefreshHasMissingImages();
                }

        }

        void HotfolderController_HotfolderImageDeleted(object sender, EventArgs<Collection<string>> e)
        {
            //int i = this.CaptureDockPanel.HotfolderPanel.HotfolderListbox.SelectedIndex;
            //HotfolderImage image = this.HotfolderController.HotfolderImages[i];

            bool isDeleted = false;
                foreach (string imageFileName in e.Data)
                {
                    try
                    {
                        File.Delete(imageFileName);
                        isDeleted = true;
                    }
                    catch
                    {
                        int x = 0;
                        while (x < 15)
                        {
                            x++;
                            Thread.Sleep(300);
                            try
                            {
                                File.Delete(imageFileName);
                                isDeleted = true;
                            }
                            catch(Exception ex)
                            {
                                if (x == 15)
                                {
                                    FlowMessageBox msg = new FlowMessageBox("Can Not Delete File", "Unable to Delete File from Hotfolder:\n\n" + ex.Message);
                                    msg.CancelButtonVisible = false;
                                    msg.ShowDialog();
                                    logger.ErrorException("Unable to Delete File from Hotfolder", ex);
                                    isDeleted = false;
                                }
                            }
                        }
                    }
                    
                }
                if (isDeleted)
                {
                    //this.HotfolderController.HotfolderImages.Remove(image);

                    //image.Clear();
                    CaptureDockPanel.CurrentImagePanel.DisplaySelectedImage(null);
                }
        }

        void ShowOrderEntry(object sender, EventArgs e)
        {
            DisplayOrderEntry(false);
        }

        void ShowSubmitOrders(object sender, EventArgs e)
        {
            this.FlowController.ApplicationPanel.ShowMainProjectPanel();
            this.FlowController.ProjectController.SubmitOrders(this.FlowController.ProjectController.CurrentFlowProject);
        }

        void ShowAdjustImages(object sender, EventArgs e)
        {

            if (this.SubjectRecordController.CurrentSubject != null && this.SubjectRecordController.CurrentSubject.HasAssignedImages)
            {
                this.FlowController.Edit();
                this.FlowController.ImageAdjustmentsCurrentRecord(this.SubjectRecordController.CurrentSubject.ImageList.CurrentItem);
            }
            else
            {
                FlowMessageBox msg = new FlowMessageBox("Crop Image", "You must assign an image to a subject before you can crop it");
                msg.CancelButtonVisible = false;
                msg.ShowDialog();
            }
            
        }
        void SubjectRecordController_ToggleFlagImageInvoked(object sender, EventArgs<SubjectImage> e)
        {
            SubjectImage image = e.Data;
            if (image.ImageFlagged == true)
                image.ImageFlagged = false;
            else
                image.ImageFlagged = true;
            this.FlowController.ProjectController.CurrentFlowProject.FlowProjectDataContext.UpdateHoldImageList();
        }

        void SubjectRecordController_ToggleRemoteImageServiceInvoked(object sender, EventArgs<SubjectImage> e)
        {
            SubjectImage image = e.Data;
            if (image.RemoteImageService == true && (image.RemoteImageServiceStatus == null || image.RemoteImageServiceStatus == "new"))
                image.RemoteImageService = false;
            else
                image.RemoteImageService = true;
            this.FlowController.ProjectController.CurrentFlowProject.FlowProjectDataContext.UpdateHoldImageList();
        }

        void SubjectRecordController_ToggleGreenscreenInvoked(object sender, EventArgs<SubjectImage> e)
        {
            SubjectImage image = e.Data;

            if (string.IsNullOrEmpty(image.GreenScreenSettings))
            {
                image.GreenScreenSettings = this.FlowController.ProjectController.CurrentFlowProject.DefaultGSxml;
                logger.Info("Enabled Green Screen for subject image: subjectName='{0}' imageFileName='{1}'",
                    image.Subject.FormalFullName, image.ImageFileName);
            }
            else
            {
                image.GreenScreenSettings = null;
                logger.Info("Disabled Green Screen for subject image: subjectName='{0}' imageFileName='{1}'",
                    image.Subject.FormalFullName, image.ImageFileName);
            }
            image.Subject.FlagForMerge = true;
            image.UpdateGS();

            if (image.IsGroupImage)
            {
                foreach (SubjectImage si in image.GroupImage.GetSubjectImageList)
                {
                    si.GreenScreenSettings = image.GreenScreenSettings;
                    si.Subject.FlagForMerge = true;
                }
            }

            this.FlowController.ProjectController.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();
        }

		/// <summary>
		/// Unassigns the selected image from the current Subject record and returns
		/// the image to the Hotfolder
		/// </summary>
		public void SubjectRecordController_ImageUnassigned(object sender, EventArgs<IEnumerable<SubjectImage>> e)
		{
            Subject theSubject = e.Data.FirstOrDefault().Subject;
            if (theSubject.ImageList.Count <= 0)
                return;

            if (!this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Permissions.CanUnassignImagesFromSubjects)
                return;

            if (theSubject.SubjectOrder != null)
            {
                if (((IEnumerable<SubjectImage>)e.Data).First().OrderProductNodes.Count > 0)
                {
                    if (theSubject.SubjectOrder.OrderPackages.Count > 0 && theSubject.SubjectOrder.OrderPackageList.Count == 0)
                    {
                        FlowMessageBox msg = new FlowMessageBox("Remove Image", "You may not remove an image when there is an ONLINE order attached to the image.");
                        msg.CancelButtonVisible = false;
                        msg.ShowDialog();
                    }
                    else
                    {
                        FlowMessageBox msg = new FlowMessageBox("Remove Image", "You may not remove an image when there is still an order attached to the image.\nYou must first delete the order");
                        msg.CancelButtonVisible = false;
                        msg.ShowDialog();
                    }
                    return;
                }
            }

            if (theSubject.ImageList.CurrentItem.OrderImageOptions.Count > 0)
                {
                    FlowMessageBox msg = new FlowMessageBox("Remove Image", "You may not remove an image when there is still an Image Option attached to the image.\nYou must first delete the Image Option in Order Entry");
                    msg.CancelButtonVisible = false;
                    msg.ShowDialog();
                    return;
                }

			var imageFileNameList = (
				from i in e.Data
				select i.ImageFileName
			).ToList();

			var imageFileMoveList = (
				from i in e.Data.Where(img => img.GroupImageGuid == null)
				select new
				{
                    PossibleThumb = i.ImagePath,
					ImagePath = Path.Combine(this._flowController.ProjectController.CurrentFlowProject.ImageDirPath,i.ImageFileName),
					ImageOriginalFileName = i.ImageOriginalFileName
				}
			).ToList();


            theSubject.DetachImage(imageFileNameList);


            foreach (var image in imageFileMoveList)
            {

                if (image.PossibleThumb.Contains("thumbnails"))
                {
                    FileInfo possibleThumb = new FileInfo(image.PossibleThumb);
                    if (possibleThumb.Exists)
                    {
                        try
                        {
                            possibleThumb.Delete();
                        }
                        catch (Exception exc)
                        {
                            logger.Error("Failed to delete thumbnail: " + possibleThumb.FullName);
                            //ignore, move on
                        }
                    }
                }
                FileInfo imageFile = new FileInfo(image.ImagePath);

                string newImageFilePath = Path.Combine(
                    this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Capture.HotFolderDirectory,
                    image.ImageOriginalFileName
                );
                this.HotfolderController.LastUnassignedImage = newImageFilePath;
                this.FlowController.ProjectController.MoveFile(image.ImagePath, newImageFilePath);

            }
		}

		/// <summary>
		/// Invokes assignment of all images in the HotFolder to the current subject record
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		void SubjectRecordController_HotfolderAutoAssignRequested(object sender, EventArgs<Subject> e)
		{
            HotFolderAutoAssignRequest();
		}
        public void HotFolderAutoAssignRequest()
        {
            if (!this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Permissions.CanAssignImagesToSubjects)
                return;

            this.HotfolderController.AssignAllImages();
        }

        void SubjectRecordController_ShowOrderEntryRequested(object sender, EventArgs<Subject> e)
        {
            DisplayOrderEntry(false);
        }

       
        void SubjectRecordController_AutoPrint(object sender, EventArgs<string> e)
        {
            SubjectImage si = null;
            foreach (SubjectImage simg in this.FlowController.ProjectController.CurrentFlowProject.SubjectList.CurrentItem.SubjectImages)
            {
                if (simg.ImagePath == e.Data)
                    si = simg;
            }

            if (si != null)
            {
                if(si.Subject.SubjectImages.Count() == this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.AutoPrintLayoutImageCount)
                    AutoPrintController.ImageQueue.Enqueue(si.Subject);
            }
        }

        //void HotfolderController_HotfolderAutoAssignLatestRequested(object sender, EventArgs<string> e)
        //{
        //    if (!this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Permissions.CanAssignImagesToSubjects)
        //        return;
        //    FlowMessageBox msg = new FlowMessageBox("Auto Assign Image", "Do you want to assign the current Image to the current Subject");
        //    bool result = (bool)msg.ShowDialog();
        //    if(result)
        //    {
        //        this.SubjectRecordController.AssignHotfolderImage(this.HotfolderController.CurrentHotfolderImage.ImageFilename);
        //    }
        //}

		/// <summary>
		/// Sets keyboard focus to the search textbox in the SubjectRecordDisplay (CollectionNavigator)
		/// </summary>
		internal void SetSearchFocus()
		{
			this.SubjectRecordController.SetSearchFocus();
		}

        public void SetSearchString(string text)
        {
            this.SubjectRecordController.SetSearchString(text);
        }
		/// <summary>
		/// Detects if the input text matches the pattern of the barcode scan key,
		/// and invokes a barcode lookup against the SubjectList if so
		/// </summary>
		/// <param name="scanText"></param>
		/// <param name="endsWithCarriageReturn"></param>
		internal bool DetectScan(string scanText, bool endsWithCarriageReturn)
		{
			// Check the pattern of the input text and store the embedded data
			// if the text matches the barcode scan pattern
			string scanResult = this.BarcodeScanDetector.DetectScan(scanText);

			bool scanInvoked = false;

			if (! string.IsNullOrEmpty(scanResult))
			{
				// Invoke the barcode lookup if a scan was detected
				this.SubjectRecordController.InvokeScan(scanResult, endsWithCarriageReturn);

				scanInvoked = true;
			}

			return scanInvoked;
		}

		/// <summary>
		/// 
		/// </summary>
		internal void DisplayOrderEntry()
		{
			this.DisplayOrderEntry(false);
		}

        internal void DisplayMrGray()
        {
            this.DisplayMrGray(false);
        }

       
        internal void AddNewSubject()
        {
            this.SubjectRecordController.EnableAddNewSubjectMode();
        }

        internal void EditSubject()
        {
            this.SubjectRecordController.EnableEditSubjectMode();
        }

        internal void DeleteSubject()
        {
            this.SubjectRecordController.EnableDeleteSubjectMode();
        }

        /// <summary>
        /// Toggles the OrderEntryPanel, initializing the panel on first invocation
        /// </summary>
        /// <param name="orderEntryInitialized"></param>
        internal void DisplayMrGray(bool toggle)
        {


            // Toggle visibility of the OrderEntryPanel based on the current visibility
            if (toggle && CaptureDockPanel.GrayCardDocumentContent.Visibility == Visibility.Visible)
            {
                CaptureDockPanel.SelectedImageDocumentContent.SetAsActive();
                CaptureDockPanel.GrayCardDocumentContent.Visibility = Visibility.Collapsed;
            }
            else
            {
                CaptureDockPanel.GrayCardDocumentContent.Visibility = Visibility.Visible;
                CaptureDockPanel.GrayCardDocumentContent.SetAsActive();
            }

        }

        internal void DisplaySelectedImage()
        {
            if (CaptureDockPanel.OrderEntryDocumentContent.Visibility == Visibility.Visible)
            {
                CaptureDockPanel.SelectedImageDocumentContent.SetAsActive();
                CaptureDockPanel.OrderEntryDocumentContent.Visibility = Visibility.Collapsed;
            }
        }

		/// <summary>
		/// Toggles the OrderEntryPanel, initializing the panel on first invocation
		/// </summary>
		/// <param name="orderEntryInitialized"></param>
		internal void DisplayOrderEntry(bool toggle)
		{
			// OrderEntry has not yet been invoked, initialize it
			if (!_orderEntryInitialized)
			{
				CaptureDockPanel.OrderEntryController = this.OrderEntryController;
				_orderEntryInitialized = true;
			}

			// Toggle visibility of the OrderEntryPanel based on the current visibility
			if (toggle && CaptureDockPanel.OrderEntryDocumentContent.Visibility == Visibility.Visible)
			{
				CaptureDockPanel.SelectedImageDocumentContent.SetAsActive();
				CaptureDockPanel.OrderEntryDocumentContent.Visibility = Visibility.Collapsed;
			}
			else
			{
				CaptureDockPanel.OrderEntryDocumentContent.Visibility = Visibility.Visible;
				CaptureDockPanel.OrderEntryDocumentContent.SetAsActive();
                OrderEntryController.SetDefaultFocus();
			}

		}

		void ProjectController_OpenOrderEntryInvoked(object sender, EventArgs e)
		{
			this.DisplayOrderEntry(false);
		}

        internal void PrintOneLayout()
        {
            this.LayoutPreviewController.PrintOneLayout();
        }

        internal void ShowCurrentSubjectSelectedImage()
        {
            if (SubjectRecordController.CurrentSubject != null && SubjectRecordController.CurrentSubject.SubjectImages.Count > 0)
                this.CurrentImageController.DisplaySelectedSubjectImage(SubjectRecordController.CurrentSubject.SubjectImages.Last(), true);
        }




        internal void ArchiveHotFolder()
        {
            string hotfolderArchiveRoot = Path.Combine(FlowContext.FlowAppDataDirPath, "HotFolderArchieve");
            string hotfolderArchiveDir = Path.Combine(hotfolderArchiveRoot, this.FlowController.ProjectController.CurrentFlowProject.FileSafeProjectName);
            string hotfolderDir = this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Capture.HotFolderDirectory;

            if (!Directory.Exists(hotfolderArchiveRoot)) Directory.CreateDirectory(hotfolderArchiveRoot);
            if (!Directory.Exists(hotfolderArchiveDir)) Directory.CreateDirectory(hotfolderArchiveDir);
            foreach(string file in Directory.GetFiles(hotfolderDir))
            {
                if (file.ToLower().EndsWith("jpg") || file.ToLower().EndsWith("png"))
                {
                    string destFile =  Path.Combine(hotfolderArchiveDir, new FileInfo(file).Name);
                    int tryCount = 0;
                    while (File.Exists(destFile))
                    {
                        destFile = Path.Combine(hotfolderArchiveDir, TicketGenerator.GenerateTicketString(4) + "_" + new FileInfo(file).Name);
                        if (tryCount++ > 500) break;
                    }
                    try
                    {
                        File.Move(file, destFile);
                    }
                    catch (Exception ex)
                    {
                        //don't bail, if we cant move the file, lets just ignore it and not error.
                    }
                }
            }
        }
    }
}