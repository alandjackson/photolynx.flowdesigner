﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Flow.Schema.LinqModel.DataContext;
using Flow.View.Dialogs;
using Flow.Lib;
using StructureMap;

namespace Flow.Controller
{
    public class ScanController
    {

        public FlowController FlowController { get; set; }

        private string barPrefix = "";
        private string barContentRaw = "";
        private string barContent
        {
            get
            {
                return barContentRaw.ToLower();
            }
        }
        private string lastKey = "";
        public bool addKeyStroke(Key key)
        {
            this.FlowController.ProjectController.specialBarCodeHandled = false;
             string text = key.ToString().ToLower();
            string rawText = key.ToString();

            if(lastKey == "LeftShift")
                rawText = rawText.ToUpper();

            lastKey = key.ToString();

            if (text.Contains("shift"))
                return false;
            if (text.Contains("period"))
            {
                text = ".";
                rawText = ".";
            }
            if (text.Length == 2 && text.StartsWith("d"))
            {
                text = text.Substring(1);
                rawText = rawText.Substring(1);
            }

            if ((barPrefix.Length == 4 || barPrefix.Length == 5) && barPrefix.EndsWith(".") && barPrefix.StartsWith("."))
            {
                string[] input = { ".bar.", ".pkg.", ".sub.", ".nst.", ".pko."};
                List<string> validPrefixes = new List<string>(input);
                if (validPrefixes.Contains(barPrefix))
                {
                    //its good, valid barcode scan
                }
                else if ((barPrefix.Length >= 4 && (barPrefix.Substring(barPrefix.Length - 4).StartsWith(".f")) || (barPrefix.Length >= 5 && barPrefix.Substring(barPrefix.Length - 5).StartsWith(".f"))) && barPrefix.EndsWith("."))
                {
                    //its good, valid barcode scan
                }
                else
                {
                    //coincience... some typed .###.
                    barPrefix = "";
                    barContentRaw = "";
                    return false;
                }
            }


            if (text == "return")
            {
                if (barPrefix.EndsWith(".bar.") || barPrefix.EndsWith(".pkg."))
                {
                     barcodeAddPackage();
                     this.FlowController.ProjectController.specialBarCodeHandled = true;
                }
                else if (barPrefix.EndsWith(".sub."))
                {
                    barcodeSearchSubject();
                    this.FlowController.ProjectController.specialBarCodeHandled = true;
                    if (this.FlowController.ProjectController.CurrentFlowProject.GetEventTriggerSetting(8))
                        this.FlowController.CaptureController.DisplayOrderEntry(false);
                }
                else if (barPrefix.EndsWith(".nst."))
                {
                    subjectTicketScanned();
                    this.FlowController.ProjectController.specialBarCodeHandled = true;
                    barPrefix = "";
                    
                }
                else if (barPrefix.EndsWith(".pko."))
                {
                    barcodeAddCatalogOption();
                    this.FlowController.ProjectController.specialBarCodeHandled = true;
                }
                else if ((barPrefix.Length >= 4 && (barPrefix.Substring(barPrefix.Length - 4).StartsWith(".f")) || (barPrefix.Length >= 5 && barPrefix.Substring(barPrefix.Length - 5).StartsWith(".f"))) && barPrefix.EndsWith("."))
                {
                    string indString = barPrefix.Trim('.').Trim('f');
                    int indInt = -1;
                    if (Int32.TryParse(indString, out indInt))
                        barcodeCustomScan(indInt);
                    else
                    {
                        FlowMessageBox msg = new FlowMessageBox("Invalid Barcode", "The barcode you scanned was invalid");
                        msg.CancelButtonVisible = false;
                        msg.ShowDialog();
                        
                    }
                    this.FlowController.ProjectController.specialBarCodeHandled = true;
                }
                
            }
            
            //does the prefix match .***. where * can be any char
            if (((barPrefix.Length == 4 || barPrefix.Length == 5)  && barPrefix.EndsWith(".")))
            {
                barContentRaw += rawText;
                //need to set focus on search, in case focus is on another button that will hijack the return key
                if(this.FlowController.CaptureController != null)
                    this.FlowController.CaptureController.SetSearchFocus();
                if (this.FlowController.EditController != null)
                    this.FlowController.EditController.SetFocus();
                return true;
            }


            if ((text == ".") || (barPrefix.StartsWith(".")))
            {
                barPrefix += text;
                string[] input2 = { ".bar.", ".pkg.", ".sub.", ".nst.", ".pko.", ".f" };
                List<string> validPrefixes2 = new List<string>(input2);
                if (validPrefixes2.Any(s => s.StartsWith(barPrefix)))
                {
                    //its good, valid barcode scan
                }
                else if (barPrefix.Length > 2 && barPrefix.StartsWith(".f") && (barPrefix.EndsWith("0") || barPrefix.EndsWith("1") || barPrefix.EndsWith("2") || barPrefix.EndsWith("3") || barPrefix.EndsWith("4") || barPrefix.EndsWith("5") || barPrefix.EndsWith("6") || barPrefix.EndsWith("7") || barPrefix.EndsWith("8") || barPrefix.EndsWith("9") || barPrefix.EndsWith(".")))
                {
                    //its good, valid barcode scan
                }
                else
                {
                    //coincience... some typed .###.
                    barPrefix = "";
                    barContentRaw = "";

                    if ((text == "."))
                        barPrefix += text;


                }
            }
            else
                barPrefix = "";

            if (barPrefix.Length > 5)
            {
                barPrefix = "";
                barContentRaw = "";
            }
           
            return this.FlowController.ProjectController.specialBarCodeHandled;
        }

        private void barcodeCustomScan(int fieldIndex)
        {
            
            Subject s = this.FlowController.ProjectController.CurrentFlowProject.SubjectList.CurrentItem;
            s.SubjectData[fieldIndex].Value = barContentRaw;
            s.Save();
            NotificationProgressInfo _notificationInfo = new NotificationProgressInfo("Adding Package", "Adding Custom Value ...");
            IShowsNotificationProgress progressNotification = ObjectFactory.GetInstance<IShowsNotificationProgress>();
            _notificationInfo.NotificationProgress = progressNotification;
            progressNotification.ShowNotification(_notificationInfo);
            _notificationInfo.Complete("Finished Adding Custom Value: " + barContentRaw + " TO " + s.SubjectData[fieldIndex].Field);
            barPrefix = "";
            barContentRaw = "";

        }

        private void subjectTicketScanned()
        {

            // Get the event trigger for the auto-assign of images to current subject record
            if (this.FlowController.ProjectController.CurrentFlowProject.GetEventTriggerSetting(6))
            {
                if (this.FlowController.CaptureController != null)
                    this.FlowController.CaptureController.HotFolderAutoAssignRequest();
            }

            if (this.FlowController.ProjectController.CurrentFlowProject.GetEventTriggerSetting(7))
            {
                if (this.FlowController.CaptureController != null)
                    this.FlowController.CaptureController.DisplayOrderEntry();
            }

            if (this.FlowController.CaptureController.CaptureDockPanel.chkAutoClearHotfolder.IsChecked == true)
                this.FlowController.CaptureController.ArchiveHotFolder();

            if (this.FlowController.CaptureController != null)
                this.FlowController.CaptureController.SubjectRecordController.SetSearchString("");

            if (this.FlowController.EditController != null)
                this.FlowController.EditController.SetSearchString("");

            if (barContent.Length == 8)
            {
                string ticketCode = barContent.ToUpper();
                int foundSubjectId = 0;
                //if (!this.FlowController.ProjectController.SubjectDataInitialized)
                //{
                //    this.FlowController.ProjectController.initializeSubjectData();
                //}
                foreach (Subject sub in this.FlowController.ProjectController.CurrentFlowProject.SubjectList)
                {
                    if (sub.TicketCode.ToUpper() == ticketCode)
                        foundSubjectId = sub.SubjectID;
                }
                if (foundSubjectId > 0)
                {
                    barcodeSearchSubject(foundSubjectId);
                }
                else
                {
                    //if (this.FlowController.CaptureController != null)
                    //{
                    //    FlowMessageBox msg = new FlowMessageBox("Add New Subject", "New subjects can only be added in Capture, not in Edit");
                    //    msg.CancelButtonVisible = false;
                    //    msg.ShowDialog();
                    //}
                    //lets create a new subject with bogus data
                    Subject sub = new Subject(this.FlowController.ProjectController.CurrentFlowProject.FlowProjectDataContext);
                    sub.FlowProjectGuid = this.FlowController.ProjectController.CurrentFlowProject.FlowProjectGuid;
                    
                    if(sub.SubjectGuid == null) sub.SubjectGuid = Guid.NewGuid();

                    foreach (SubjectDatum data in sub.SubjectData)
                    {
                        //if (data.IsRequiredField && (data.FieldDesc == "FirstName" || data.FieldDesc == "LastName"))
                        if (data.IsRequiredField)
                        {
                            if (data.FieldType == typeof(string))
                                data.Value = "No Data";
                            if (data.FieldType == typeof(int))
                                data.Value = 0;
                        }
                    }
                    //lets save the ticket number and subjectid
                    sub.TicketCode = ticketCode;

                    //add this subject to the subjectlist
                    this.FlowController.ProjectController.CurrentFlowProject.FlowProjectDataContext.Subjects.InsertOnSubmit(sub);
                    sub.Save();
                    this.FlowController.ProjectController.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();

                   

                    //make sure the record is selected in edit
                    barcodeSearchSubject(sub.SubjectID);
                    //check for "require edit" trigger

                    if (this.FlowController.ProjectController.CurrentFlowProject.GetEventTriggerSetting(10))
                    {
                        if (this.FlowController.CaptureController != null)
                            this.FlowController.CaptureController.SubjectRecordController.EnableEditSubjectMode();

                        foreach (SubjectDatum data in sub.SubjectData)
                        {
                            //if (data.IsRequiredField && (data.FieldDesc == "FirstName" || data.FieldDesc == "LastName"))
                            if (data.IsRequiredField)
                            {
                                if (data.IsRequiredField && (data.FieldDesc == "FirstName" || data.FieldDesc == "LastName"))
                                {
                                    if (data.FieldType == typeof(string))
                                        data.Value = "No Data";
                                }
                                else if (data.IsRequiredField)
                                {
                                    if (data.FieldType == typeof(string))
                                        data.Value = "";
                                }
                                if (data.FieldType == typeof(int))
                                    data.Value = 0;
                            }
                        }
                    }

                }

                //make sure selected image is shown
                if (this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Capture.NewTicketScanAction == "Show Order Entry")
                    this.FlowController.CaptureController.DisplayOrderEntry();
                else if(this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Capture.NewTicketScanAction == "Show Selected Image")
                    this.FlowController.CaptureController.DisplaySelectedImage();
            }
            barPrefix = "";
            barContentRaw = "";
        }

        private void barcodeSearchSubject()
        {
            barcodeSearchSubject(Convert.ToInt16(barContent));
        }
        private void barcodeSearchSubject(int subjectId)
        {
            if (this.FlowController.CaptureController != null)
                this.FlowController.CaptureController.SubjectRecordController.SetSearchString("");
            if (this.FlowController.EditController != null)
                this.FlowController.EditController.SetSearchString("");
            if (barContent.Length > 0)
            {
                IEnumerable<Subject> sub = this.FlowController.ProjectController.CurrentFlowProject.SubjectList.Where(s => s.SubjectID == subjectId);
                this.FlowController.ProjectController.CurrentFlowProject.SubjectList.CurrentItem = sub.First();
            }
            barPrefix = "";
            barContentRaw = "";
        }


        private void barcodeAddPackage()
        {
            if (this.FlowController.CaptureController == null)
                return;
            this.FlowController.CaptureController.SubjectRecordController.SetSearchString("");
            if (barContent.Length > 0)
            {
                if (this.FlowController.ProjectController.CurrentFlowProject.FlowCatalog == null)
                {
                    FlowMessageBox msg = new FlowMessageBox("No Catalog Assigned", "There is no catalog assigned to this project.\n To assign a catalog open up order entry by pressing F8");
                    msg.CancelButtonVisible = false;
                    msg.ShowDialog();
                    return;
                }
                //process barContent as a package
                foreach (ProductPackage thisPackage in this.FlowController.ProjectController.CurrentFlowProject.FlowCatalog.ProductPackages)
                {
                    if (thisPackage.Map.ToLower() == barContent)
                    {
                        //Houston, we have a match
                        if (this.FlowController.ProjectController.CurrentFlowProject.SubjectList.CurrentItem.HasAssignedImages)
                        {
                            //let put up a progressbar so we know we added the package...
                            NotificationProgressInfo _notificationInfo = new NotificationProgressInfo("Adding Package", "Adding Package: " + thisPackage.ProductPackageDesc + " ...");
                            IShowsNotificationProgress progressNotification = ObjectFactory.GetInstance<IShowsNotificationProgress>();
                            _notificationInfo.NotificationProgress = progressNotification;
                            progressNotification.ShowNotification(_notificationInfo);
                            Subject subject = this.FlowController.ProjectController.CurrentFlowProject.SubjectList.CurrentItem;
                            subject.AddOrderPackage(thisPackage);
                            subject.ForcePropertyChange("PackageSummary");
                            _notificationInfo.Complete("Finished Adding Package: " + thisPackage.ProductPackageDesc);
                        }
                        else
                        {
                            FlowMessageBox msg = new FlowMessageBox("No Subject Images", "You must first assign an image to a subject before you assign a package");
                            msg.CancelButtonVisible = false;
                            msg.ShowDialog();
                        }
                    }
                }
            }
            barPrefix = "";
            barContentRaw = "";

        }

        private void barcodeAddCatalogOption()
        {
            if (this.FlowController.CaptureController == null)
                return;
            this.FlowController.CaptureController.SubjectRecordController.SetSearchString("");
            if (barContent.Length > 0)
            {
                if (this.FlowController.ProjectController.CurrentFlowProject.FlowCatalog == null)
                {
                    FlowMessageBox msg = new FlowMessageBox("No Catalog Assigned", "There is no catalog assigned to this project.\n To assign a catalog open up order entry by pressing F8");
                    msg.CancelButtonVisible = false;
                    msg.ShowDialog();
                    return;
                }
                //process barContent as a catalog option
                foreach (FlowCatalogOption thisOption in this.FlowController.ProjectController.CurrentFlowProject.FlowCatalog.FlowCatalogOptions)
                {
                    if (thisOption.Map.ToLower() == barContent)
                    {
                        //Houston, we have a match
                        if (this.FlowController.ProjectController.CurrentFlowProject.SubjectList.CurrentItem.HasAssignedImages)
                        {
                            //let put up a progressbar so we know we added the catalog option...
                            NotificationProgressInfo _notificationInfo = new NotificationProgressInfo("Adding Catalog Option", "Adding Catalog Option: " + thisOption.ImageQuixCatalogOption.Label + " ...");
                            IShowsNotificationProgress progressNotification = ObjectFactory.GetInstance<IShowsNotificationProgress>();
                            _notificationInfo.NotificationProgress = progressNotification;
                            progressNotification.ShowNotification(_notificationInfo);
                            

                            Subject subject = this.FlowController.ProjectController.CurrentFlowProject.SubjectList.CurrentItem;
                            
                            if(this.FlowController.ProjectController.FlowMasterDataContext.IsPUDConfiguration)
                                this.FlowController.CaptureController.OrderEntryController.AddCatalogOption(thisOption);
                            else
                                subject.AddCatalogOption(thisOption);
                            
                            subject.ForcePropertyChange("PackageSummary");
                            _notificationInfo.Complete("Finished Adding Catalog Option: " + thisOption.ImageQuixCatalogOption.Label);
                        }
                        else
                        {
                            FlowMessageBox msg = new FlowMessageBox("No Subject Images", "You must first assign an image to a subject before you assign a package");
                            msg.CancelButtonVisible = false;
                            msg.ShowDialog();
                        }
                    }
                }
            }
            barPrefix = "";
            barContentRaw = "";

        }
    }
}
