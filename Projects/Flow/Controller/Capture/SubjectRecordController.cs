﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;

using Flow.Controller.Project;
using Flow.Controls;
using Flow.Controls.DataDisplay;
using Flow.Controls.ImageDisplay;
using Flow.Controls.View;
using Flow.Controls.View.DataDisplay;
using Flow.Controls.View.ImageDisplay;
using Flow.Controls.View.RecordDisplay;
using Flow.Lib.Helpers;
using Flow.Model;
using Flow.Model.Capture;
using Flow.Model.Settings;
using Flow.Schema.LinqModel.DataContext;
using Flow.View;
using Flow.View.Capture;
using System.Windows.Threading;
using System.Windows.Data;
using NLog;


namespace Flow.Lib
{
    public class SubjectRecordController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

		public event EventHandler<EventArgs<IEnumerable<SubjectImage>>> ImageUnassigned
		{
			add { SubjectRecordPanel.ImageUnassigned += value; }
			remove { SubjectRecordPanel.ImageUnassigned -= value; }
		}

        public event EventHandler<EventArgs<SubjectImage>> ToggleGreenscreenInvoked
        {
            add { SubjectRecordPanel.ToggleGreenscreenInvoked += value; }
            remove { SubjectRecordPanel.ToggleGreenscreenInvoked -= value; }
        }

        public event EventHandler<EventArgs<SubjectImage>> ToggleFlagImageInvoked
        {
            add { SubjectRecordPanel.ToggleFlagImageInvoked += value; }
            remove { SubjectRecordPanel.ToggleFlagImageInvoked -= value; }
        }

        public event EventHandler<EventArgs<SubjectImage>> ToggleRemoteImageServiceInvoked
        {
            add { SubjectRecordPanel.ToggleRemoteImageServiceInvoked += value; }
            remove { SubjectRecordPanel.ToggleRemoteImageServiceInvoked -= value; }
        }

        public event EventHandler ShowOrderEntry 
        { 
            add { this.SubjectRecordDisplay.ShowOrderEntry += value; }
            remove { this.SubjectRecordDisplay.ShowOrderEntry -= value; }
        }

        public event EventHandler ShowSubmitOrders
        {
            add { this.SubjectRecordDisplay.ShowSubmitOrders += value; }
            remove { this.SubjectRecordDisplay.ShowSubmitOrders -= value; }
        }

        public event EventHandler ShowAdjustImages
        {
            add { this.SubjectRecordDisplay.ShowAdjustImages += value; }
            remove { this.SubjectRecordDisplay.ShowAdjustImages -= value; }
        }
		public event EventHandler<EventArgs<Subject>> HotfolderAutoAssignRequested = delegate { };
        public event EventHandler<EventArgs<Subject>> ShowOrderEntryRequested = delegate { };
        public event EventHandler<EventArgs<string>> AutoPrint = delegate { };

		public ProjectController ProjectController { get; set; }
		public FlowProject CurrentFlowProject
		{ get { return (this.ProjectController == null) ? null : this.ProjectController.CurrentFlowProject; } }

		SubjectRecordPanel SubjectRecordPanel { get; set; }

		SubjectRecordDisplay SubjectRecordDisplay { get { return this.SubjectRecordPanel.SubjectRecordDisplay; } }

		CollectionNavigator CollectionNavigator { get { return this.SubjectRecordDisplay.CollectionNavigator; } }
		FlowImageListBoxSubjectRecordTemp SubjectImageList { get { return this.SubjectRecordDisplay.SubjectImageList; } }
		SubjectRecordWrapDisplay SubjectRecordWrapDisplay { get { return this.SubjectRecordDisplay.SubjectRecordWrapDisplay; } }

		public Subject CurrentSubject
		{
			get
			{
                return (this.CurrentFlowProject == null || this.CurrentFlowProject.SubjectList == null) ? null : this.CurrentFlowProject.SubjectList.CurrentItem;
			}
		}

		public void Main(SubjectRecordPanel pnl)
		{
			SubjectRecordPanel = pnl;

			SetDependencies();

			this.CollectionNavigator.EditStateChanged += new EventHandler<EditStateChangedEventArgs>(CollectionNavigator_EditStateChanged);
			this.CollectionNavigator.SearchInvoked += new EventHandler<SearchInvokedEventArgs>(CollectionNavigator_SearchInvoked);

			this.SubjectRecordDisplay.ConfirmDeleteButton.Click += new RoutedEventHandler(ConfirmDeleteButton_Click);
			this.SubjectRecordDisplay.CancelDeleteButton.Click += new RoutedEventHandler(CancelDeleteButton_Click);

			this.SubjectRecordWrapDisplay.ValidationInvoked += new EventHandler<EventArgs<bool>>(SubjectRecordWrapDisplay_ValidationInvoked);
		}

		void SubjectRecordWrapDisplay_ValidationInvoked(object sender, EventArgs<bool> e)
		{
			this.CollectionNavigator.DataIsValid = e.Data;
		}


		/// <summary>
		/// Handles updates of datacontexts when a project is loaded
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		internal void ProjectController_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			if (e.PropertyName == "CurrentFlowProject")
				SetDependencies();
		}

		//void CurrentFlowProject_PropertyChanged(object sender, PropertyChangedEventArgs e)
		//{
		//    if (e.PropertyName == "SubjectList")
		//        this.SetDependencies();
		//}

		/// <summary>
		/// Sets datacontexts when a project is loaded
		/// </summary>
		private void SetDependencies()
		{
//			this.CollectionNavigator.DataContext = this.CurrentFlowProject.SubjectList;
			this.CollectionNavigator.EditState = EditState.View;

			//this.CollectionNavigator.SetSourceCollection<Subject>(this.CurrentFlowProject.SubjectList);

            //this.SubjectRecordPanel.Dispatcher.Invoke((Action)delegate
            //{
            this.SubjectRecordPanel.DataContext = this.CurrentFlowProject;
			//if (this.CurrentFlowProject.SubjectList.CurrentItem != null)
			//    this.CurrentFlowProject.SubjectList.CurrentItem.SubjectData.Refresh();

//			this.CurrentFlowProject.PropertyChanged += new PropertyChangedEventHandler(CurrentFlowProject_PropertyChanged);

            // NOTE: this is a workaround for the "zero subjects" synchronization issue
            this.SubjectRecordDisplay.SubjectRecordWrapDisplay.GetBindingExpression(SubjectRecordWrapDisplay.CurrentSubjectProperty).UpdateTarget();
            //});

            this.SubjectRecordWrapDisplay.TeacherList = this.CurrentFlowProject.UniqueTeachers;
            this.SubjectRecordWrapDisplay.GradeList = this.CurrentFlowProject.UniqueGrades;
            this.SubjectRecordWrapDisplay.ClassList = this.CurrentFlowProject.UniqueClasses;
		}

		/// <summary>
		/// Assigns the image specified by filename to the Subject
		/// </summary>
		/// <param name="imageFileName"></param>
		public string AssignHotfolderImage(string imageFileName, bool auto8x10)
		{
            logger.Info("Assigning hotfolder image: fileName='{0}'", imageFileName);

            string returnString = this.CurrentSubject.AssignImage(imageFileName, this.ProjectController.FlowMasterDataContext.LoginID, auto8x10);
            if (this.CurrentFlowProject.GetEventTriggerSetting(8))
                this.ShowOrderEntryRequested(this, new EventArgs<Subject>(null));
            if (this.ProjectController.FlowMasterDataContext.PreferenceManager.Application.AutoPrintLayout)
                this.AutoPrint(this, new EventArgs<string>(returnString));


            return returnString;
		}

        public string AssignGroupImage(GroupImage image)
        {
            logger.Info("Assigning group image: fileName='{0}'", image.ImageFileName);

            string returnString = this.CurrentSubject.AssignGroupImage(image, this.ProjectController.FlowMasterDataContext.LoginID);

           

            return returnString;
        }

        public string AssignAllGroupImage(GroupImage image)
        {
            logger.Info("Assigning group image to all: fileName='{0}'", image.ImageFileName);

            //will always be the same return string, because the path to the image is always the same
            string returnString = "";
            
            //we will modify the collection, so lets generate a simple list of the subjects in the collection.
            List<Subject> subList = new List<Subject>();
            foreach (Subject s in this.ProjectController.CurrentFlowProject.SubjectList.View)
                subList.Add(s);

            foreach (Subject sub in subList)
            {
                if (sub.ImageList.Count(i => i.GroupImageGuid == image.GroupImageGuid) == 0)
                {
                    returnString = sub.AssignGroupImage(image, this.ProjectController.FlowMasterDataContext.LoginID);
                    sub.FlagForMerge = true;
                }
                
            }
            this.ProjectController.CurrentFlowProject.SubjectList.ApplyFilter();
            return returnString;
        }

        public void EnableAddNewSubjectMode()
        {
            this.CollectionNavigator.EditState = EditState.Add;
            this.SubjectRecordDisplay.SubjectRecordWrapDisplay.FocusFirstField();
            logger.Info("Enabled Add New Subject Mode");
        }

        public void EnableEditSubjectMode()
        {
            this.CollectionNavigator.EditState = EditState.Edit;
            this.SubjectRecordDisplay.SubjectRecordWrapDisplay.FocusFirstField();
            logger.Info("Enabled Edit Subject Mode");
        }

        public void EnableDeleteSubjectMode()
        {
            if (this.CollectionNavigator.EditState != EditState.Add)
            {
                this.CollectionNavigator.EditState = EditState.Delete;
                this.SubjectRecordDisplay.ConfirmDeleteButton.Focus();
                logger.Info("Enabled Delete Subject Mode");
            }
            else
                logger.Warn("Cannot enable Delete Subject Mode while subject is in Add Mode");
        }
















































































































































































































































































































































		/// <summary>
		/// Manages the state of the SubjectRecordDisplay and its child controls when the EditState
		/// of the CollectionNavigator changes
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		void CollectionNavigator_EditStateChanged(object sender, EditStateChangedEventArgs e)
		{
            this.CurrentFlowProject.LastSubjectViewed = this.CurrentSubject;
			this.SubjectRecordWrapDisplay.IsReadOnly = (e.NewEditState == EditState.View);
            this.SubjectRecordWrapDisplay.TeacherList = this.CurrentFlowProject.UniqueTeachers;
            this.SubjectRecordWrapDisplay.GradeList = this.CurrentFlowProject.UniqueGrades;
            this.SubjectRecordWrapDisplay.ClassList = this.CurrentFlowProject.UniqueClasses;

			switch (e.OldEditState)
			{
				case EditState.View:
                   
//					this.SubjectRecordWrapDisplay.IsReadOnly = false;
				
					switch (e.NewEditState)
					{
						case EditState.Add:
							this.CurrentFlowProject.SubjectList.ClearFilter();

                            this.CurrentFlowProject.SubjectList.CreateNew();

                            this.SubjectRecordWrapDisplay.IsNewAdd = true;
							//this.CollectionNavigator.DataIsValid = 
							//    this.SubjectRecordDisplay.SubjectRecordWrapDisplay.Validate();
                            this.SubjectRecordDisplay.SubjectRecordWrapDisplay.CheckRememberme();

							this.CollectionNavigator.DataIsValid = false;
                            this.SubjectRecordDisplay.SubjectRecordWrapDisplay.FocusFirstField();

							break;

						case EditState.Edit:
                            if (this.CurrentSubject == null)
                            {
                                this.CollectionNavigator.EditState = EditState.View;
                            }
                            else
                            {
                                this.CollectionNavigator.DataIsValid =
                                    this.SubjectRecordDisplay.SubjectRecordWrapDisplay.Validate();

                                this.SubjectRecordDisplay.SubjectRecordWrapDisplay.FocusFirstField();

                            }
							break;

						case EditState.Delete:
                            if (this.CurrentSubject == null)
                            {
                                this.CollectionNavigator.EditState = EditState.View;
                            }
                            else
                            {
                                this.SubjectRecordDisplay.DeleteConfirmationPrompt.Visibility = Visibility.Visible;
                                this.SubjectRecordDisplay.CancelDeleteButton.Focus();
                            }
							break;
					}
					break;


				case EditState.Add:
                   
                    this.SubjectRecordWrapDisplay.IsNewAdd = false;
					switch (e.NewEditState)
					{
						case EditState.Save:

							// Flag if the project has no subject records
							bool emptySubjectList = !this.CurrentFlowProject.HasSubjects;

                            this.CurrentSubject.TicketCode = TicketGenerator.GenerateTicketString(8);
							this.CurrentSubject.Save();


							// If this is the first subject record, invoke SetDependencies to avoid the "zero subjects" synch issue
							if(emptySubjectList)
								SetDependencies();

							this.CollectionNavigator.EditState = EditState.View;
                            
							break;

						case EditState.Cancel:
                            if(this.CurrentFlowProject.SubjectList != null)
							    this.CurrentFlowProject.SubjectList.CancelNew();
							this.CollectionNavigator.EditState = EditState.View;
							break;
					}
					break;


				case EditState.Edit:
					switch (e.NewEditState)
					{
						case EditState.Save:
							this.CurrentSubject.Save();
							this.CollectionNavigator.EditState = EditState.View;
                           
							break;

						case EditState.Cancel:
                            if(this.CurrentSubject != null)
							    this.CurrentSubject.Revert();

							this.CollectionNavigator.EditState = EditState.View;
							break;
					}
		
					break;


				case EditState.Delete:
					this.SubjectRecordDisplay.DeleteConfirmationPrompt.Visibility = Visibility.Collapsed;
					break;

			}

            if (e.NewEditState == EditState.Save)
            {
                if (this.CurrentFlowProject.GetEventTriggerSetting(3))
                {
                    //autoprint
                    //this.FlowController.DataImport();
                }
                else if (this.CurrentFlowProject.GetEventTriggerSetting(4))
                {
                    //require package entry
                    this.ProjectController.FlowController.CaptureController.DisplayOrderEntry();
                }
                this.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();
                this.SubjectRecordWrapDisplay.UpdatePickLists();
            }
		}

		/// <summary>
		/// Commits deletion of the current subject after the user has confirmed the delete
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		void ConfirmDeleteButton_Click(object sender, RoutedEventArgs e)
		{
            logger.Info("Clicked Confirm Delete button on current subject");
            if(this.CurrentFlowProject.SubjectList.Count > 0)
            {
                //if (this.CurrentFlowProject.SubjectList.CurrentItem.SubjectTicket != null)
                //{
                //    this.CurrentFlowProject.FlowProjectDataContext.SubjectTickets.DeleteOnSubmit(this.CurrentFlowProject.SubjectList.CurrentItem.SubjectTicket);
                //    this.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();
                //}
                this.CurrentFlowProject.SubjectList.CurrentItem.DeleteOrders();
                while (this.CurrentFlowProject.SubjectList.CurrentItem.SubjectImages.Count > 0)
                {
                    SubjectImage thisImage = this.CurrentFlowProject.SubjectList.CurrentItem.SubjectImages[0];

                    if (thisImage.OrderImageOptions.Count > 0)
                    {
                        foreach (OrderImageOption o in thisImage.OrderImageOptionList)
                        {
                            this.CurrentFlowProject.SubjectList.CurrentItem.RemoveCatalogOption(o);
                        }
                    }

                    this.CurrentFlowProject.SubjectList.CurrentItem.DetachImage(thisImage.ImageFileName);
                    if (thisImage.ImagePath != null)
                    {
                        FileInfo imageFile = new FileInfo(thisImage.ImagePath);

                        string newImageFilePath = Path.Combine(
                            this.ProjectController.FlowMasterDataContext.PreferenceManager.Capture.HotFolderDirectory,
                            thisImage.ImageOriginalFileName
                        );

                        this.ProjectController.FlowController.CaptureController.HotfolderController.LastUnassignedImage = newImageFilePath;
                        this.ProjectController.MoveFile(thisImage.ImagePath, newImageFilePath);
                    }
                }
                this.CurrentFlowProject.SubjectList.DeleteCurrent();
			    this.CollectionNavigator.EditState = EditState.View;
            }
		}

		/// <summary>
		/// Cancels the deletion invocation
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		void CancelDeleteButton_Click(object sender, RoutedEventArgs e)
		{
            logger.Info("Clicked Cancel Delete button");
			this.CollectionNavigator.EditState = EditState.View;
		}

		/// <summary>
		/// Responds to a request for a search or a barcode lookup; this can be induced by the
		/// user hitting enter or clicking the search button in the CollectionNavigator control,
		/// or by the barcode scan key pattern being detected
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		void CollectionNavigator_SearchInvoked(object sender, SearchInvokedEventArgs e)
		{
            logger.Info("Subject record search invoked: criterion='{0}' field='{1}'",
                (e.Criterion) == null ? "<null>" : e.Criterion,
                (e.FieldDesc) == null ? "<null>" : e.FieldDesc);

            if (this.ProjectController.specialBarCodeHandled)
                return;

			// Get the event trigger for the auto-assign of images to current subject record
			if (this.CurrentFlowProject.GetEventTriggerSetting(6))
				this.HotfolderAutoAssignRequested(this, new EventArgs<Subject>(null));

            if (this.CurrentFlowProject.GetEventTriggerSetting(7))
            {
                if (this.ProjectController.FlowController.CaptureController != null)
                    this.ProjectController.FlowController.CaptureController.DisplayOrderEntry();
            }

            this.CurrentFlowProject.SubjectList.ApplyFilter(false, false, null, null, null);
            this.CurrentFlowProject.SubjectList.ApplyFilter(null, null);

			IEnumerable<Subject> queryResult = null;

			this.SubjectRecordPanel.SearchResultsList.ItemsSource = queryResult;
			
			if(e.IsDefiniteBarcodeScan)
				queryResult = this.CurrentFlowProject.SubjectList.PerformBarcodeLookup(e.Criterion);
			else
				queryResult	= this.CurrentFlowProject.SubjectList.GetQueryResults(e.Criterion, true, e.FieldDesc);

			int queryResultCount = (queryResult == null) ? 0 : queryResult.Count();

			if (queryResultCount == 0)
			{
                logger.Info("Subject record search returned zero results");

                this.SetSearchFocus();
                this.CurrentFlowProject.SubjectList.ApplyFilter(false, false, null, null, null);
                this.CurrentFlowProject.SubjectList.ApplyFilter(null, null);
			}
			else if (queryResultCount == 1)
            {
                logger.Info("Subject record search returned one result");

                this.CurrentFlowProject.SubjectList.ApplyFilter(e.Criterion, e.FieldDesc);
				this.CurrentFlowProject.SubjectList.MoveCurrentToFirst();

				this.SubjectRecordPanel.SearchResultsPanel.Visibility = Visibility.Collapsed;
                this.CurrentFlowProject.SubjectList.ApplyFilter(false, false, null, null, null);
                this.CurrentFlowProject.SubjectList.ApplyFilter(null, null);
			}
			else
			{
                logger.Info("Subject record search returned multiple results");

                this.CurrentFlowProject.SubjectList.ApplyFilter(e.Criterion, e.FieldDesc);
				this.SubjectRecordPanel.SearchResultsList.ItemsSource = queryResult;
                this.CurrentFlowProject.SubjectList.MoveCurrentToFirst();

				this.SubjectRecordPanel.SearchResultsPanel.Visibility = Visibility.Visible;
			}

            // Notify the user if the search returned no results
            this.SubjectRecordPanel.NoSearchResultsLabel.Visibility = queryResultCount == 0 ? Visibility.Visible : Visibility.Collapsed; 
		}

		public void SetSearchFocus()
		{
			this.SubjectRecordPanel.SetSearchFocus();
		}
        public void SetSearchString(string text)
        {
            this.SubjectRecordPanel.SetSearchString(text);
        }
		public void InvokeScan(string scanText, bool endsWithCarriageReturn)
		{
			this.CollectionNavigator.InvokeScan(scanText, endsWithCarriageReturn);
		}

	}
}
