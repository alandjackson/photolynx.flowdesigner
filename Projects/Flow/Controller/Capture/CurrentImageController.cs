﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Imaging;

using Flow.View.Capture;
using Flow.Model;
using Flow.Schema.LinqModel.DataContext;
using Flow.Lib;

namespace Flow.Controller.Capture
{
    public class CurrentImageController
    {
        CurrentImagePanel CurrentImagePanel { get; set; }
        FlowController FlowController { get; set; }

        string lastImageSource = "";

        public void Main(CurrentImagePanel pnl)
        {
            CurrentImagePanel = pnl;
            CurrentImagePanel.FlowController = FlowController;
			CurrentImagePanel.DataContext = FlowController.ProjectController.FlowMasterDataContext;
        }

        public void DisplaySelectedImage(HotfolderImage image, bool forceChange)
        {
            //hotfolder selection change

            if ((lastImageSource == "hotfolder" || lastImageSource == "" || forceChange) && image != null)
            {
                if (CurrentImagePanel != null)
                    CurrentImagePanel.DisplaySelectedImage(image);

                if (this.FlowController.CaptureController.CaptureDockPanel.GrayCardPanel.IsVisible)
                    this.FlowController.CaptureController.GrayCardViewModel.MainImage = (image == null) ? null : image;

                lastImageSource = "hotfolder";
            }
		}

        public void DisplaySelectedFlowImage(FlowImage image, bool forceChange)
        {
            //hotfolder image clicked
            if ((lastImageSource == "hotfolder" || forceChange) && image != null)
            {
                if (CurrentImagePanel != null)
                    CurrentImagePanel.DisplaySelectedImage(image);

                if (this.FlowController.CaptureController.CaptureDockPanel.GrayCardPanel.IsVisible)
                    this.FlowController.CaptureController.GrayCardViewModel.MainImage = (image == null) ? null : image;

                lastImageSource = "hotfolder";
            }
        }

        public void DisplaySelectedGroupImage(GroupImage image, bool forceChange)
        {
            if ((lastImageSource == "groupPhotos" || forceChange) && image != null)
            {
                if (CurrentImagePanel != null)
                    CurrentImagePanel.DisplaySelectedGroupImage(image);

                if (this.FlowController.CaptureController.CaptureDockPanel.GrayCardPanel.IsVisible)
                    this.FlowController.CaptureController.GrayCardViewModel.MainImage = (image == null) ? null : image.FlowImage;

                lastImageSource = "groupPhotos";
            }
        }

        public void DisplaySelectedSubjectImage(SubjectImage image, bool forceChange)
        {
            if ((lastImageSource == "subjectRecord" || forceChange) && image != null)
            {
                if (CurrentImagePanel != null)
                    CurrentImagePanel.DisplaySelectedSubjectImage(image);

                if (this.FlowController.CaptureController.CaptureDockPanel.GrayCardPanel.IsVisible)
                    this.FlowController.CaptureController.GrayCardViewModel.MainImage = (image == null) ? null : image.FlowImage;

                lastImageSource = "subjectRecord";
            }
        }

        public void SetFlowController(FlowController fc)
        {
            FlowController = fc;
        }
    }
}
