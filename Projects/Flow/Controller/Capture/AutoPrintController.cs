﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.View.Dialogs;
using System.Windows.Controls;
using Flow.Schema.LinqModel.DataContext;
using Flow.Lib.AsyncWorker;
using System.Threading;
using Flow.Lib.Log;
using System.Windows.Threading;
using Flow.Lib;
using System.ComponentModel;
using Flow.Lib.Persister;
using Flow.Controller.Project;
using System.Windows.Media;
using Flow.Designer.Lib.Service;
using Flow.Designer.Model;
using Flow.Service;
using System.Windows;
using System.Windows.Media.Imaging;
using System.IO;
using Flow.Lib.Helpers;
using NLog;
using System.Printing;

namespace Flow.Controller.Capture
{
    public class AutoPrintController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        private  bool confirmedFirstPrint = false;
        private bool keepRunning = true;

        private PrintDialog pDialog { get; set; }
        private PrintDialog tempDialog { get; set; }
        private  PrintServer pserver = null;
        private string pserverName = "";
        private int copyCount = 1;
        private PrintTicket pTicket = null;
        private Dispatcher dispatcher { get; set; }

        public Queue<Subject> ImageQueue { get; set; }
        public Queue<Layout> LayoutsToPrint { get; set; }

        public DisplayableFile displayableFile { get; set; }

        private ProjectController ProjectController { get; set; }
        //FlowBackgroundWorker Worker;

        public event EventHandler<RunWorkerCompletedEventArgs> AutoPrintStopped = delegate { };

        private bool doOneOnly = false;

        public AutoPrintController(Dispatcher disp, ProjectController progController)
        {
            ProjectController = progController;
            this.dispatcher = disp;
            
           
            
            //BackgroundWorker.DoWork = (Action)delegate { AutoPrintEngine(); };
            

        }

        /// <summary>
        /// Starts the scan process
        /// </summary>
        public void Start(DisplayableFile dfile)
        {
            displayableFile = dfile;
            keepRunning = true;
            //Worker = FlowBackgroundWorkerManager.RunWorker(AutoPrintEngine, AutoPrintEngine_Stopped);

            Thread t = new Thread(new ThreadStart(() =>
            {
                ImageQueue = new Queue<Subject>();
                LayoutsToPrint = new Queue<Layout>();
                Thread.Sleep(100);
                AutoPrintEngine();
                AutoPrintEngine_Stopped();
            }));
            t.SetApartmentState(ApartmentState.STA);
            t.IsBackground = true;
            t.Start();



            logger.Info("Auto Print scan process started");
        }

        //public void ConfigPrinter()
        //{
        //     Thread t = new Thread(new ThreadStart(() =>
        //    {
        //        pDialog = null;
        //        confirmedFirstPrint = false;
        //        AutoPrintEngine();
        //        AutoPrintEngine_Stopped();
                
                
        //    }));
        //     t.SetApartmentState(ApartmentState.STA);
        //     t.IsBackground = true;
        //     t.Start();
        //}

        public void StartOne(DisplayableFile dfile)
        {
            if (doOneOnly)
                return;
            doOneOnly = true;
            displayableFile = dfile;
            keepRunning = true;
            //Worker = FlowBackgroundWorkerManager.RunWorker(AutoPrintEngine, AutoPrintEngine_Stopped);

            Thread t = new Thread(new ThreadStart(() =>
            {
                ImageQueue = new Queue<Subject>();
                LayoutsToPrint = new Queue<Layout>();
                Thread.Sleep(100);
                AutoPrintEngine();
                AutoPrintEngine_Stopped();
            }));
            t.SetApartmentState(ApartmentState.STA);
            t.IsBackground = true;
            t.Start();



            logger.Info("Auto Print scan process started (one only)");
        }

        /// <summary>
        /// Stops the scan process
        /// </summary>
        public void Stop()
        {
            keepRunning = false;
        }

        void AutoPrintEngine_Stopped()
        {
            if (!doOneOnly)
            {
                pDialog = null;
                tempDialog = null;
                confirmedFirstPrint = false;
            }
            doOneOnly = false;
            keepRunning = false;
            this.AutoPrintStopped(this, null);

            logger.Info("Auto Print scan process stopped");
        }

        void AutoPrintEngine()
        {
            try
            {
                 bool result = true;
                   

                   
                          //FlowMessageBox msg = new FlowMessageBox("Confirm Auto Print", "Turning on Auto Print means that every time an image is assigned to a subject\nthe selected layout will be sent to the printer.");
                          //result = (bool)msg.ShowDialog();

                        if (result == true && pDialog == null)
                        {

                            if (tempDialog == null)
                            {

                                this.dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                               {
                                   tempDialog = new PrintDialog();
                                   result = (bool)tempDialog.ShowDialog();
                                   pserver = tempDialog.PrintQueue.HostingPrintServer;
                                   pserverName = tempDialog.PrintQueue.Name;
                                   copyCount = (int)tempDialog.PrintTicket.CopyCount;
                                   pTicket = tempDialog.PrintTicket;
                               }));
                            }

                            
                                pDialog = new PrintDialog();
                                pDialog.PrintQueue = new System.Printing.PrintQueue(pserver, pserverName);
                                pDialog.PrintTicket = pTicket;
                                pDialog.PrintTicket.CopyCount = copyCount;
                               
                        }
                    
                    if (!result)
                        return;

                //if we made it this far, we are good to print.
                    while (keepRunning)
                {

                    GenerateLayout();
                    PrintLayout();
                    Thread.Sleep(1000);
                }
            }
            catch (Exception ex)
            {
                logger.ErrorException("Exception occured.", ex);
            }

        }

        public void PrintLayout()
        {


            if (this.LayoutsToPrint.Count > 0)
            {
                logger.Info("Print Layout");
                SendToPrinter();
            }
        }

        public void SendToPrinter()
        {
            logger.Info("Sending layout to printer");

            bool PrintQueueBusy = false;

            logger.Info("about to get print queue status");
            //this.dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
            //{
                pDialog.PrintQueue.Refresh();
                if (pDialog.PrintQueue.NumberOfJobs == 0)
                {
                    logger.Info("print queue is not busy");
                    PrintQueueBusy = false;
                }
                else
                {
                    logger.Info("print queue IS busy");
                    PrintQueueBusy = true;
                }
            //}));

            //there is currently a job in the printers queue, so do nothing. bail for now
            if (PrintQueueBusy)
                return;

            logger.Info("getting next print job from list of layouts to print (" + LayoutsToPrint.Count + " layout in the list)");
            var thisLayout = LayoutsToPrint.Dequeue();

            
                try
                {
                    logger.Info("Sending to printer: " + thisLayout.ItemName);
                    this.dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
            {
                        thisLayout.SendToPrinter(tempDialog);
                    }));
                    logger.Info("Layout was sent to printer: " + thisLayout.ItemName);

                }
                catch (Exception ex)
                {
                    
                    if (!ex.Message.ToLower().Contains("printing was canceled"))
                    {
                        this.dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                        {
                            FlowMessageBox msg = new FlowMessageBox("Print Error", "There was a problem printing.\n\n" + ex.Message);
                            msg.CancelButtonVisible = false;
                            msg.ShowDialog();
                            logger.ErrorException("There was a problem printing", ex);
                        }));
                        
                    }
                    //ignore the error
                }

                        
            //wait one sec before deleting the image
            Thread.Sleep(1000);
            //if (File.Exists(thisLayout))
            //    File.Delete(thisLayout);

            if (doOneOnly && LayoutsToPrint.Count == 0)
            {
                pDialog = null;
                keepRunning = false;
            }
        }


        public void GenerateLayout()
        {
            

            //get a Job and print it!
            Subject sub=null;
            //this.dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
            //   {
                   if(this.ImageQueue.Count > 0)
                       sub = this.ImageQueue.Dequeue();
            //   }));
            if (sub == null || sub.IsNew)
                return;
            logger.Info("Generating layout");
            LoadLayout(displayableFile, sub);
        }

        public static Layout BuildLayoutFromFile(string xmlFile, ProjectController controller, 
            Flow.Designer.Model.Project.Subject sub = null)
        {
            Layout layout = ObjectSerializer.FromXmlFile<Layout>(xmlFile);
            layout.ImageTypes = Flow.Designer.Model.Project.ImageTypes.Build(controller.FlowMasterDataContext);
            layout.DesignerDynamicFields = Flow.Designer.Model.Project.DesignerDynamicFields.Build(controller.CurrentFlowProject);

            if (sub != null)
                layout.Subject = sub;

            return layout;
        }

        protected void LoadLayout(DisplayableFile layoutFile, Subject sub)
        {
            if (layoutFile == null)
                return;

            logger.Info("Loading layout - {0}", layoutFile.ToString());

            byte[] pngBytes = new byte[0];
            Layout layout = null;



           
                  layout = BuildLayoutFromFile(layoutFile.FileInfo.FullName, ProjectController, sub.ToDesignerSubject());

                  if (!confirmedFirstPrint)
                  {
                     this.dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                      {
                          Canvas canvas = layout.ToCanvas(300, LayoutRenderQuality.High);
                          pngBytes = canvas.ToPngBytes();

                          PreviewLayout wnd = new PreviewLayout(pngBytes);
                          if (!(bool)wnd.ShowDialog())
                          {
                              confirmedFirstPrint = false;
                              keepRunning = false;
                          }
                          }));
                  }
                  if (keepRunning)
                  {
                      confirmedFirstPrint = true;

                      LayoutsToPrint.Enqueue(layout);
                  }
             
        }

    }
}
