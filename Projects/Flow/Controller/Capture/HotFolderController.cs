﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;

using Flow.Lib.Helpers;
using Flow.Model;
using Flow.Model.Capture;
using Flow.Model.Settings;
using Flow.View;
using Flow.View.Capture;
using Flow.Properties;
using System.Data.Linq;
using Flow.Schema.LinqModel.DataContext;
using Flow.View.Dialogs;
using Flow.Controller;
using NLog;
using ExifLib;
using Flow.Lib.Preferences;
using System.Windows.Forms;
using System.Globalization;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Drawing.Imaging;
using System.Drawing;
using System.Threading;

namespace Flow.Lib
{
    public class HotFolderController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

//        private FlowMasterDataContext _flowMasterDataContext = null;
		internal FlowMasterDataContext FlowMasterDataContext { get; set; }
        internal FlowController FlowController { get; set; }

        public event EventHandler<EventArgs<HotfolderImage>>
            SelectedHotfolderImageChanged = null;

        public event EventHandler<EventArgs<HotfolderImage>>
           NewHotfolderImageAdded = null;

		public event EventHandler<EventArgs<Collection<string>>>
			HotfolderImageAssigned = delegate { };

        public event EventHandler<EventArgs<Collection<string>>>
            HotfolderGroupImageAssigned = delegate { };

        public event EventHandler<EventArgs<Collection<string>>>
            HotfolderImageDeleted = delegate { };


        HotFolderPanel HotFolderPanel { get; set; }
        HotFolderWatcher HotFolderWatcher { get; set; }
        ConfigRepository ConfigRepository { get; set; }

        public HotfolderImages HotfolderImages { get; set; }

        public HotfolderImage CurrentHotfolderImage{ get; set; }

        public string LastUnassignedImage = "";

        public HotFolderController(HotFolderWatcher w, ConfigRepository c,
            HotfolderImages hi)
        {
            HotFolderWatcher = w;

            HotFolderWatcher.ImageFileAdded += 
                new EventHandler<EventArgs<string>>(HotFolderWatcher_ImageFileAdded);

			HotFolderWatcher.ImageFileRemoved += 
                new EventHandler<EventArgs<string>>(HotFolderWatcher_ImageFileRemoved);

			HotFolderWatcher.ImageFilesAdded +=
				new EventHandler<EventArgs<IEnumerable<string>>>(HotFolderWatcher_ImageFilesAdded);

			HotFolderWatcher.ImageFilesRemoved +=
				new EventHandler<EventArgs<IEnumerable<string>>>(HotFolderWatcher_ImageFilesRemoved);

			ConfigRepository = c;
            HotfolderImages = hi;
        }


        public void Main(HotFolderPanel pnl)
        {

            HotFolderPanel = pnl;
            pnl.ImageImport += new EventHandler(ImageImport);
			pnl.SelectedHotfolderImageChanged +=
                new EventHandler<EventArgs<HotfolderImage>>
                    (pnl_SelectedHotfolderImageChanged);

			pnl.HotfolderImageAssigned +=
				new EventHandler<EventArgs<Collection<string>>>
					(pnl_HotfolderImageAssigned);

            pnl.HotfolderGroupImageAssigned +=
                new EventHandler<EventArgs<Collection<string>>>
                    (pnl_HotfolderGroupImageAssigned);

            pnl.HotfolderImageDeleted +=
                new EventHandler<EventArgs<Collection<string>>>
                    (pnl_HotfolderImageDeleted);

			pnl.SetDataContext(HotfolderImages);

			StartHotfolderWatcher();
        }

        public void ChangeHotfolderWatcher(String hotFolderDir)
        {
            //HotFolderWatcher.Stop();
            if (HotFolderWatcher != null)
            {
                logger.Info("Hotfolder watcher directory changed from '{0}' to '{1}'", HotFolderWatcher.HotfolderDirectory, hotFolderDir);
                HotFolderWatcher.HotfolderDirectory = hotFolderDir;
            }
            //HotFolderWatcher.Start();

        }
        protected void StartHotfolderWatcher()
        {
//            this.FlowMasterDataContext = new FlowMasterDataContext();

            string prefCaptureHotFolder = this.FlowMasterDataContext.PreferenceManager.Capture.HotFolderDirectory;

            if (prefCaptureHotFolder == null)
            {
				prefCaptureHotFolder = FlowContext.FlowHotFolderDirPath;
				this.FlowMasterDataContext.PreferenceManager.Capture.HotFolderDirectory = prefCaptureHotFolder;
				this.FlowMasterDataContext.SubmitChanges();
            }
 
            //ApplicationConfig appConfig = ConfigRepository.Load<ApplicationConfig>();
            //ValidateHotfolderDirectory(appConfig);

            HotFolderWatcher.HotfolderDirectory = prefCaptureHotFolder;
            HotFolderWatcher.Start();

            logger.Info("Started Hotfolder watcher: directory='{0}'", HotFolderWatcher.HotfolderDirectory);
        }

        protected void ValidateHotfolderDirectory(ApplicationConfig appConfig)
        {
			if (appConfig.HotfolderDirectory != FlowContext.FlowHotFolderDirPath)
			{
				appConfig.HotfolderDirectory = FlowContext.FlowHotFolderDirPath;

				if (!Directory.Exists(appConfig.HotfolderDirectory))
					Directory.CreateDirectory(appConfig.HotfolderDirectory);

				ConfigRepository.Save<ApplicationConfig>(appConfig);
			}
		}

		/// <summary>
		/// Passes request for assignment of all hotfolder images (to current subject record) to
		/// the hotfolder panel
		/// </summary>
		internal void AssignAllImages()
		{
			this.HotFolderPanel.AssignAllImages();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
        void pnl_SelectedHotfolderImageChanged(object sender, EventArgs<HotfolderImage> e)
        {
            this.CurrentHotfolderImage = e.Data;
			OnSelectedHotfolderImageChanged(
				(e == null) ? null : e.Data
			);
        }

		protected void OnSelectedHotfolderImageChanged(HotfolderImage image)
        {
            if (image != null)
            {
                logger.Info("Selected Hotfolder image changed to '{0}'", (image.ImageFilename == null) ? "<null>" : image.ImageFilename);
                //            if (SelectedHotfolderImageChanged != null)
                SelectedHotfolderImageChanged(this, new EventArgs<HotfolderImage>(image));
            }
        }

        void HotFolderWatcher_ImageFileAdded(object sender, EventArgs<string> e)
        {
            HotFolderPanel.Dispatcher.Invoke((Action)delegate
				{
					// Insert the new image as the first element (displays at top of hotfolder)
                    HotfolderImage himage = new HotfolderImage(e.Data);
                    HotfolderImages.Insert(0, himage);
					HotFolderPanel.HotfolderListbox.SelectedIndex = 0;
                    NewHotfolderImageAdded(this, new EventArgs<HotfolderImage>(himage));
				}
			);

        }

        void HotFolderWatcher_ImageFileRemoved(object sender, EventArgs<string> e)
        {
            HotFolderPanel.Dispatcher.Invoke((Action)delegate
            {
				// Only attempt to remove image if moved/deleted directly from HF directory
				if(HotfolderImages.Any(i => i.ImageFilename == e.Data))
					HotfolderImages.Remove(new HotfolderImage(e.Data));

				// If the removed file had been the selected file, select the first image in the listbox
				if (this.HotFolderPanel.HotfolderListbox.SelectedIndex == -1)
					this.HotFolderPanel.HotfolderListbox.SelectedIndex = 0;

                
            });
        }


		void HotFolderWatcher_ImageFilesAdded(object sender, EventArgs<IEnumerable<string>> e)
		{
			HotFolderPanel.Dispatcher.Invoke((Action)delegate
				{
                    logger.Info("Hotfolder watcher detected new files added");

                    if (this.HotfolderImages.Count > 0)
                    {
                        foreach (string fileName in e.Data)
                        {
                            logger.Info("Adding image to hotfolder: fileName='{0}'", fileName);
                            HotfolderImage himage = new HotfolderImage(fileName);
                            HotfolderImages.Insert(0, himage);
                            NewHotfolderImageAdded(this, new EventArgs<HotfolderImage>(himage));
                            if (e.Data.Count() == 1) AutoAssign(fileName); //normal autoassign on single image adds (teathered workflow)
                            //AutoAssignBatch(fileName); //this is for metadata match, done in batch

                        }
                    }
                    else
                    {
                        foreach (string fileName in e.Data)
                        {
                            logger.Info("Adding image to hotfolder: fileName='{0}'", fileName);
                            HotfolderImage himage = new HotfolderImage(fileName);
                            HotfolderImages.Add(himage);
                            NewHotfolderImageAdded(this, new EventArgs<HotfolderImage>(himage));
                            if (e.Data.Count() == 1) AutoAssign(fileName); //normal autoassign on single image adds (teathered workflow)
                            //AutoAssignBatch(fileName); //this is for metadata match, done in batch
                        }
					}

					HotFolderPanel.HotfolderListbox.SelectedIndex = 0;
				}
			);
            
		}




        void AutoAssign(string fileName)
        {
            if (this.FlowController.ProjectController.CurrentFlowProject == null)
                return;
            
            if (this.FlowController.ProjectController.CurrentFlowProject.GetEventTriggerSetting(9))
            {
                if (!this.FlowMasterDataContext.PreferenceManager.Permissions.CanAssignImagesToSubjects)
                    return;
                if (fileName == this.LastUnassignedImage)
                    return;

                Collection<string> imageFileNames = new Collection<string>();
                imageFileNames.Add(fileName);
                this.HotfolderImageAssigned(this, new EventArgs<Collection<string>>(imageFileNames));

                this.FlowController.CaptureController.ShowCurrentSubjectSelectedImage();

            }
        }

		void HotFolderWatcher_ImageFilesRemoved(object sender, EventArgs<IEnumerable<string>> e)
		{
			HotFolderPanel.Dispatcher.Invoke((Action)delegate
				{
                    logger.Info("Hotfolder watcher detected files removed");

                    foreach (string fileName in e.Data)
                    {
                        logger.Info("Removing deleted image from Hotfolder: fileName='{0}'", fileName);

                        //HotfolderImage img = new HotfolderImage(fileName);
                        //HotfolderImage ExistingImage = HotfolderImages.Where(h => h.ImageFilename == img.ImageFilename).FirstOrDefault();
                        HotfolderImage img = HotfolderImages.Where(h => h.ImageFilename == fileName).FirstOrDefault();
                        HotfolderImages.Remove(img);

                        //if (ExistingImage != null)
                        //{
                        //    ExistingImage.Clear();
                        //}
                        img.Clear();

                        //this.HotFolderPanel.HotfolderListbox.ItemsSource = null;
                        //this.HotFolderPanel.HotfolderListbox.ItemsSource = HotfolderImages;
                        //this.HotFolderPanel.HotfolderListbox.UpdateLayout();
                    }
					// If the removed file had been the selected file, select the first image in the listbox
					if (this.HotFolderPanel.HotfolderListbox.SelectedIndex == -1)
						this.HotFolderPanel.HotfolderListbox.SelectedIndex = 0;

				}
			);
		}




		/// <summary>
		/// Hotfolder image assignment event handler; raised from HotFolderPanel
		/// </summary>
		void pnl_HotfolderImageAssigned(object sender, EventArgs<Collection<string>> e)
		{
			OnHotfolderImageAssigned(e.Data);
		}

        void pnl_HotfolderGroupImageAssigned(object sender, EventArgs<Collection<string>> e)
        {
            OnHotfolderGroupImageAssigned(e.Data);
        }

        void pnl_HotfolderImageDeleted(object sender, EventArgs<Collection<string>> e)
        {
            OnHotfolderImageDeleted(e.Data);
        }
		/// <summary>
		/// Hotfolder image assignment event invoker
		/// </summary>
		protected void OnHotfolderImageAssigned(Collection<string> imageFileNames)
		{
			this.HotfolderImageAssigned(this, new EventArgs<Collection<string>>(imageFileNames));
		}

        protected void OnHotfolderGroupImageAssigned(Collection<string> imageFileNames)
        {
            this.HotfolderGroupImageAssigned(this, new EventArgs<Collection<string>>(imageFileNames));
        }

        protected void OnHotfolderImageDeleted(Collection<string> imageFileNames)
        {
            this.HotfolderImageDeleted(this, new EventArgs<Collection<string>>(imageFileNames));
        }



        internal void ImageImport(object sender, EventArgs e)
        {
            
            bool DebugImageSeqAssign = false;
            if (FlowController.ImportImagesAutoAssign == true)
            {
                DebugImageSeqAssign = true;
            }
            //prompt for a directory
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            DialogResult result = fbd.ShowDialog();
            if (result != DialogResult.OK)
                return;

            List<String> files = Directory.GetFiles(fbd.SelectedPath).ToList();

            NotificationProgressInfo prog = new NotificationProgressInfo("Image Importer", "Importing Images", 0, files.Count, 1);
            prog.Display();


             BackgroundWorker bw = new BackgroundWorker();

        // this allows our worker to report progress during work
        bw.WorkerReportsProgress = true;

        // what to do in the background thread
        bw.DoWork += new DoWorkEventHandler(
        delegate(object o, DoWorkEventArgs args)
        {

            BackgroundWorker b = o as BackgroundWorker;

            //opticon
            //determine import type (data file or image metadata)
            bool timeStampDataFile = false;
            bool imageMetadata = false;
            string pathToDataFile = "";
            if (files.Any(f => f.ToLower().EndsWith(".txt")))
            {
                foreach (string f in files.Where(f => f.ToLower().EndsWith(".txt")))
                {
                    string dateString = File.ReadAllLines(f)[0].Split(",")[2];
                    DateTime dresult = new DateTime();
                    if (DateTime.TryParse(dateString, out dresult))
                    {
                        pathToDataFile = f;
                        timeStampDataFile = true;
                        break;
                    }

                }
            }
            if (!timeStampDataFile)
            {
                //Assume its imageMetadata
                imageMetadata = true;
            }
            //if image metdata
            if (imageMetadata)
            {
                //foreach image
                int cnt = 0;
                foreach (string filePath in files.Where(f => f.ToLower().EndsWith(".jpg")))
                {
                    //b.ReportProgress(cnt++);
                    this.FlowController.ProjectController.CurrentFlowProject.MainDispatcher.Invoke((Action)delegate
               {
                   prog.Step();
                   prog.Update("Loaded " + cnt++ + "/" + files.Count);
               
                    AutoAssignBasedOnMetadata(filePath);
                    });
                    Thread.Sleep(100);

                    if (DebugImageSeqAssign && File.Exists(filePath))
                    {
                        AssignImage(this.FlowController.ProjectController.CurrentFlowProject.SubjectList.CurrentItem.TicketCode, filePath);
                        //next subject
                         this.FlowController.ProjectController.CurrentFlowProject.MainDispatcher.Invoke((Action)delegate
                                {
                                    this.FlowController.ProjectController.CurrentFlowProject.SubjectList.MoveCurrentToNext();
                                });
                    }
                }
                
            }
            //if data file
            if (timeStampDataFile)
            {
                Dictionary<DateTime, string> TimeCodes = new Dictionary<DateTime, string>();
                List<string> allLines = File.ReadAllLines(pathToDataFile).ToList();
                foreach (string line in allLines)
                {
                    string[] fields = line.Split(",");
                    string barcodeValue = fields[0];
                    string timestamp = fields[1];
                    string datestamp = fields[2];
                    if (barcodeValue.ToLower().Contains(".nst.") || barcodeValue.Trim().Length > 8)
                        barcodeValue = barcodeValue.Replace(".nst.", "").Replace(".NST.", "").Substring(0, 8);
                    
                    DateTime dt = DateTime.ParseExact(datestamp + " " + timestamp, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                    TimeCodes.Add(dt, barcodeValue);
                }

                DateTime firstScanTime = TimeCodes.First().Key;
                double secondsDiff = 0;
                int cnt = 0;
                foreach (string filePath in files.Where(f => f.ToLower().EndsWith(".jpg")))
                {
                    //b.ReportProgress(cnt++);
                    this.FlowController.ProjectController.CurrentFlowProject.MainDispatcher.Invoke((Action)delegate
                    {
                       prog.Step();
                       prog.Update("Loaded " + cnt++ + "/" + files.Count);
               

                        DateTime fileTimeStamp = DateTime.Now;
                        try
                        {
                            fileTimeStamp = GetDateTakenFromImage(filePath);
                        }
                        catch (Exception exc)
                        {
                            fileTimeStamp = new FileInfo(filePath).CreationTime;
                        }

                        if (secondsDiff == 0)
                        {
                            DateTime firstImageTime = fileTimeStamp;
                            secondsDiff = (firstScanTime - firstImageTime).TotalSeconds;
                        }
                        KeyValuePair<DateTime, string> kvp = TimeCodes.Reverse().FirstOrDefault(k => k.Key <= (fileTimeStamp.AddSeconds(secondsDiff)));
                        if (kvp.Value != null && kvp.Value.Length == 8)
                        {
                            AssignImage(kvp.Value, filePath);
                        }
                    });
                    Thread.Sleep(100);
                }
            }

        });

        // what to do when worker completes its task (notify the user)
        bw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(
        delegate(object o, RunWorkerCompletedEventArgs args)
        {
            prog.Complete("Done Importing Images");
        });

        bw.RunWorkerAsync();


            

        }

        private void AssignImage(string ticketCode, string filePath)
        {
            this.FlowController.ProjectController.GotoTicketID(ticketCode);
            

            //there are no subjects
            if (this.FlowController.ProjectController.CurrentFlowProject.SubjectList.Count == 0)
                return;

            //make sure the current subject is a match
            if (this.FlowController.ProjectController.CurrentFlowProject.SubjectList.CurrentItem.TicketCode.ToLower() != ticketCode.ToLower())
                return;

            if (!this.FlowMasterDataContext.PreferenceManager.Permissions.CanAssignImagesToSubjects)
                return;
            if (filePath == this.LastUnassignedImage)
                return;

            Collection<string> imageFileNames = new Collection<string>();
            imageFileNames.Add(filePath);

            this.FlowController.ProjectController.CurrentFlowProject.MainDispatcher.Invoke((Action)delegate
                {
                    this.HotfolderImageAssigned(this, new EventArgs<Collection<string>>(imageFileNames));
                    this.FlowController.CaptureController.ShowCurrentSubjectSelectedImage();
                });

            
        }

        private void AutoAssignBasedOnMetadata(string filePath)
        {
            if (this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Capture.MetaMatchAutoAssign &&
                this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Capture.MetaMatchSourceField != null &&
                 this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Capture.MetaMatchDestinationField != null)
            {
                //get MetaFieldValue
                ExifField ef = this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Capture.MetaMatchSourceField;
                string destField = this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Capture.MetaMatchDestinationField;

                string metaValue = "";
                Byte[] metaBytes;
                try
                {
                    using (var reader = new ExifReader(filePath))
                    {
                        reader.GetTagValue<Byte[]>(ushort.Parse(ef.Id), out metaBytes);
                        metaValue = System.Text.Encoding.ASCII.GetString(metaBytes as Byte[]).Replace("ASCII", "").Trim('\0');
                    }
                }
                catch (Exception e)
                {
                    //ignore it
                }

                if (string.IsNullOrEmpty(metaValue)) return;

                if (metaValue.ToLower().StartsWith(".nst.")) metaValue = metaValue.Replace(".nst.", "").Replace(".NST.", "");
                metaValue = metaValue.Substring(0, 8);
                //pull up the right subject
                AssignImage(metaValue, filePath);

            }
        }


        //we init this once so that if the function is repeatedly called
        //it isn't stressing the garbage man
        private static Regex r = new Regex(":");

        //retrieves the datetime WITHOUT loading the whole image (date taken)
        public static DateTime GetDateTakenFromImage(string path)
        {
            using (FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read))
            using (Image myImage = Image.FromStream(fs, false, false))
            {
                PropertyItem propItem = myImage.GetPropertyItem(36867);
                string dateTaken = r.Replace(Encoding.UTF8.GetString(propItem.Value), "-", 2);
                return DateTime.Parse(dateTaken);
            }
        }
    }
}
