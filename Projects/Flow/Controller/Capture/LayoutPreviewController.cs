﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.View.Capture;
using Flow.Lib;
using System.IO;
using System.Windows.Controls;
using Flow.Designer.Lib.Service;
using Flow.Designer.Model;
using Flow.Lib.Persister;
using Flow.Schema.LinqModel.DataContext;
using Flow.Service;
using System.Windows.Media;
using Flow.View.Dialogs;
using System.Windows.Media.Imaging;
using System.Threading;
using System.Windows.Threading;
using System.Data.Linq;
using System.Windows;
using NLog;
using System.Windows.Input;
using Flow.Controller.Project;
using System.Threading.Tasks;

namespace Flow.Controller.Capture
{
    public class LayoutPreviewController
    {
        TemplatePreviewPanel TemplatePreviewPanel { get; set; }
        public FlowController FlowController { get; set; }
        TextBlock _workingTextBlock { get; set; }

        private static Logger logger = LogManager.GetCurrentClassLogger();

        public void Main(TemplatePreviewPanel pnl)
        {
            _workingTextBlock = new TextBlock() { FontSize = 30, FontWeight = FontWeights.Bold, HorizontalAlignment = HorizontalAlignment.Center, VerticalAlignment = VerticalAlignment.Center, Text = "Refreshing..." };
            TemplatePreviewPanel = pnl;
            //pnl.uxLayouts.DropDownOpened += new EventHandler(uxLayouts_DropDownOpened);
            pnl.btnLayouts.Click +=new RoutedEventHandler(btnLayouts_Click);
            //pnl.uxLayouts.SelectionChanged += new SelectionChangedEventHandler(uxLayouts_SelectionChanged);
            pnl.uxLayouts.MouseLeftButtonUp += new System.Windows.Input.MouseButtonEventHandler(uxLayouts_MouseLeftButtonUp);
            pnl.chkAutoPrint.Click += new System.Windows.RoutedEventHandler(chkAutoPrint_Click);
            pnl.chkPreviewOn.Click += new RoutedEventHandler(chkPreviewOn_Click);
            pnl.chkAutoPrint.IsChecked = false;
            pnl.btnPrint.Click += new System.Windows.RoutedEventHandler(btnPrint_Click);
            //pnl.btnPrintConfig.Click += new System.Windows.RoutedEventHandler(btnPrintConfig_Click);
            pnl.txtAutoPrintImageCount.TextChanged += new TextChangedEventHandler(txtAutoPrintImageCount_TextChanged);
            pnl.txtAutoPrintImageCount.Text = this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.AutoPrintLayoutImageCount.ToString();

            if (this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.AutoPrintLayout == true)
            { 
                this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.AutoPrintLayout = false;
            }

            if (!string.IsNullOrEmpty(this.FlowController.ProjectController.CustomSettings.DefaultCaptureLayout))
            {
                FileInfo lyt = new FileInfo(Path.Combine(FlowContext.FlowLayoutsDirPath, this.FlowController.ProjectController.CustomSettings.DefaultCaptureLayout));
                if (lyt.Exists)
                {
                    LoadLayoutsMenu();

                    
                    SelectedFile = new Flow.Designer.Lib.Service.DisplayableFile(lyt);

                   

                    TemplatePreviewPanel.chkPreviewOn.IsChecked = true;
                    LoadLayout(new Flow.Designer.Lib.Service.DisplayableFile(lyt));
                    TemplatePreviewPanel.lblSelectedFile.Content = SelectedFile.FileInfo.Name;

                    //TemplatePreviewPanel.uxLayouts.SelectedIndex = TemplatePreviewPanel.uxLayouts.Items.Count - 1; //select last layout
                    int x = -1;
                    foreach (DisplayableFile i in TemplatePreviewPanel.uxLayouts.Items.SourceCollection.Cast<DisplayableFile>())
                    {
                        x++;
                        if (i.FileInfo.Name == lyt.Name)
                            break;
                    }

                    TemplatePreviewPanel.uxLayouts.SelectedIndex = x;
                    //UpdateLayout();
                }
            }
        }

        private void uxLayouts_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {

            if ((sender as ListBox).SelectedIndex < 0)
                return;
            if (TemplatePreviewPanel.uxLayouts.SelectedItem == null)
                return;


            ListBox mi = TemplatePreviewPanel.uxLayouts;
            if (TemplatePreviewPanel.uxLayouts.SelectedItem.GetType() != typeof(DisplayableFile))
                return;

            FileInfo selectedFile = (TemplatePreviewPanel.uxLayouts.SelectedItem as DisplayableFile).FileInfo;
            if (selectedFile.Name.StartsWith(".."))
            {
                TempLayoutDir = Directory.GetParent(TempLayoutDir).FullName;
                LoadLayoutsMenu();
                mi.SelectedIndex = -1;
                mi.Focus();

            }
            else if (selectedFile.Name.StartsWith("-----"))
            {
                LoadLayoutsMenu();
                mi.SelectedIndex = -1;
                mi.Focus();

            }
            else if (selectedFile.Name.StartsWith("["))
            {

                TempLayoutDir = System.IO.Path.Combine(selectedFile.DirectoryName, selectedFile.Name.Substring(1, selectedFile.Name.Length - 2));
                LoadLayoutsMenu();
                mi.SelectedIndex = -1;
                mi.Focus();

            }
            else
            {
                SelectedFile = mi.SelectedItem as DisplayableFile;
                TemplatePreviewPanel.lblSelectedFile.Content = SelectedFile.FileInfo.Name;
                UpdateLayout();
                TemplatePreviewPanel.layoutPopup.IsOpen = false;
                this.FlowController.CaptureController.SetSearchFocus();
            }
        }

        void txtAutoPrintImageCount_TextChanged(object sender, TextChangedEventArgs e)
        {
            string svalue = TemplatePreviewPanel.txtAutoPrintImageCount.Text;
            short ivalue = 0;
            if (Int16.TryParse(svalue, out ivalue))
            {
                this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.AutoPrintLayoutImageCount = Convert.ToInt16(ivalue);
                this.FlowController.ProjectController.FlowMasterDataContext.SavePreferences();
            }
            else
            {
                TemplatePreviewPanel.txtAutoPrintImageCount.Text = this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.AutoPrintLayoutImageCount.ToString();
            }

            logger.Info("Auto Print image count set to {0}", this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.AutoPrintLayoutImageCount);
        }

        void btnLayouts_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            LoadLayoutsMenu();
            this.FlowController.CaptureController.SetSearchFocus();
        }

        //void btnPrintConfig_Click(object sender, System.Windows.RoutedEventArgs e)
        //{
        //    logger.Info("Clicked Layout Preview Print button");
        //    this.FlowController.CaptureController.AutoPrintController.ConfigPrinter();
        //}

        void btnPrint_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            logger.Info("Clicked Layout Preview Print button");
            PrintOneLayout(); 
        }

        public void PrintOneLayout()
        {
            try
            {
                if (this.SelectedFile == null && (TemplatePreviewPanel.uxLayouts.SelectedItem == null ||
                        TemplatePreviewPanel.uxLayouts.SelectedItem.GetType() == typeof(ComboBoxItem)) &&
                        ((ComboBoxItem)TemplatePreviewPanel.uxLayouts.SelectedItem).Content.ToString() == "Select Layout")
                {
                    FlowMessageBox msg = new FlowMessageBox("Select A Layout", "Please select a layout before you Print.");
                    msg.CancelButtonVisible = false;
                    msg.ShowDialog();
                    return;
                }
                if (TemplatePreviewPanel.uxLayouts.SelectedItem == null)
                    this.FlowController.CaptureController.AutoPrintController.StartOne(this.SelectedFile);
                else
                    this.FlowController.CaptureController.AutoPrintController.StartOne(TemplatePreviewPanel.uxLayouts.SelectedItem as DisplayableFile);
                Thread.Sleep(2000);
                this.FlowController.CaptureController.AutoPrintController.ImageQueue.Enqueue(this.FlowController.ProjectController.CurrentFlowProject.SubjectList.CurrentItem);
            }
            catch (Exception e)
            {
                FlowMessageBox msg = new FlowMessageBox("Select A Layout", "Please select a layout before you Print.");
                msg.CancelButtonVisible = false;
                msg.ShowDialog();
                return;
            }
        }

        void chkAutoPrint_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            logger.Info("Clicked Layout Preview Auto Print checkbox");

            if (((CheckBox)e.Source).IsChecked == true)
            {
                TurnOnAutoPrint();
            }
            else
            {
                TurnOffAutoPrint();
            }

            this.FlowController.ProjectController.FlowMasterDataContext.SavePreferences();
        }

        public void TurnOffLayoutPreview()
        {
            TemplatePreviewPanel.chkPreviewOn.IsChecked = false;
        }
        public void TurnOffAutoPrint()
        {
            
            TemplatePreviewPanel.chkAutoPrint.IsChecked = false;

            logger.Info("Auto Print disabled");
            this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.AutoPrintLayout = false;
            this.FlowController.CaptureController.AutoPrintController.Stop();
        }

        public void TurnOnAutoPrint()
        {
            //TemplatePreviewPanel.chkPreviewOn.IsChecked = true;
            TemplatePreviewPanel.chkAutoPrint.IsChecked = true;

            if (TemplatePreviewPanel.uxLayouts.SelectedItem == null ||
                    TemplatePreviewPanel.uxLayouts.SelectedItem.GetType() == typeof(ComboBoxItem) &&
                    ((ComboBoxItem)TemplatePreviewPanel.uxLayouts.SelectedItem).Content.ToString() == "Select Layout")
            {
                FlowMessageBox msg = new FlowMessageBox("Select A Layout", "Please select a layout before you turn on Auto Print.");
                msg.CancelButtonVisible = false;
                msg.ShowDialog();
                TemplatePreviewPanel.chkAutoPrint.IsChecked = false;
                this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.AutoPrintLayout = false;
                this.FlowController.CaptureController.AutoPrintController.Stop();

            }
            else
            {
                logger.Info("Auto Print enabled");
                this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.AutoPrintLayout = true;
                this.FlowController.CaptureController.AutoPrintController.Start(TemplatePreviewPanel.uxLayouts.SelectedItem as DisplayableFile);
            }
        }

        void chkPreviewOn_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            UpdateLayout();
        }

        public DisplayableFile SelectedFile { get; set; }

        void uxLayouts_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ListBox mi = (ListBox)e.Source;
            mi.Focus();
            if ((sender as ListBox).SelectedIndex < 0)
                return;

            if (TemplatePreviewPanel.uxLayouts.SelectedItem == null)
                return;
            
            
            //ListBox mi = (ListBox)e.Source;
            FileInfo selectedFile = (TemplatePreviewPanel.uxLayouts.SelectedItem as DisplayableFile).FileInfo;
            if (selectedFile.Name.StartsWith(".."))
            {
                TempLayoutDir = Directory.GetParent(TempLayoutDir).FullName;
                mi.SelectedIndex = -1;
                LoadLayoutsMenu();
                mi.Focus();
               
            }
            else if (selectedFile.Name.StartsWith("-----"))
            {
                mi.SelectedIndex = -1;
                LoadLayoutsMenu();
                mi.Focus();
                
            }
            else if (selectedFile.Name.StartsWith("["))
            {

                TempLayoutDir = System.IO.Path.Combine(selectedFile.DirectoryName, selectedFile.Name.Substring(1, selectedFile.Name.Length - 2));
                mi.SelectedIndex = -1;
                LoadLayoutsMenu();
                mi.Focus();

            }
            else
            {
                SelectedFile = mi.SelectedItem as DisplayableFile;
                TemplatePreviewPanel.lblSelectedFile.Content = SelectedFile.FileInfo.Name;
                UpdateLayout();
                TemplatePreviewPanel.layoutPopup.IsOpen = false;
            }
        }

        private void UpdateLayout()
        {
            if (this.TemplatePreviewPanel.chkPreviewOn.IsChecked == true)
            {
                if (this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.AutoPrintLayout == true)
                {
                    this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.AutoPrintLayout = false;
                    this.FlowController.CaptureController.AutoPrintController.Stop();
                }
                LoadLayout(TemplatePreviewPanel.uxLayouts.SelectedItem as DisplayableFile);
                if (TemplatePreviewPanel.chkAutoPrint.IsChecked == false && this.FlowController.ProjectController.CurrentFlowProject.GetEventTriggerSetting(3))
                {
                    //autoprint event trigger
                    TurnOnAutoPrint();
                }
            }
        }

        void uxLayouts_DropDownOpened(object sender, EventArgs e)
        {
            LoadLayoutsMenu();
        }

        public string TempLayoutDir { get; set; }

        protected void LoadLayoutsMenu()
        {
            if (String.IsNullOrEmpty(TempLayoutDir))
                TempLayoutDir = this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Layout.LayoutsDirectory;

            if(TempLayoutDir.Length <  this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Layout.LayoutsDirectory.Length)
                TempLayoutDir = this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Layout.LayoutsDirectory;

            new LoadLayoutsListService()
                .ItemsToRemoveAre(TemplatePreviewPanel.uxLayouts.Items)
                .WhenNoFilesFound(() =>
                {
                    TemplatePreviewPanel.uxLayouts.Items.Add("No Layouts Found");
                })
                .ForEachFileFound((FileInfo layoutFile) =>
                {
                    TemplatePreviewPanel.uxLayouts.Items.Add(
                        new DisplayableFile(layoutFile));
                })
               .LoadLayouts(TempLayoutDir);
        }

        FlowProject appliedLayoutEventHandlers = null;
        DisplayableFile _currentLayoutFile = null;
        protected void LoadLayout(DisplayableFile layoutFile)
        {
            if (layoutFile == null)
                return;

            _currentLayoutFile = layoutFile;

            FlowProject proj = FlowController.ProjectController.CurrentFlowProject;
            if (appliedLayoutEventHandlers == null || proj.FlowProjectGuid != appliedLayoutEventHandlers.FlowProjectGuid)
            {
                if (proj.SubjectList != null)
                {
                    proj.SubjectList.CurrentChanged += (s, e) => UpdateLayoutPreview();

                    this.TemplatePreviewPanel.chkPreviewOn.Checked += (s, e) => UpdateLayoutPreview();
                }
            }
            appliedLayoutEventHandlers = proj;

            UpdateLayoutPreview();
        }

        public void UpdateLayoutPreview()
        {
            if (this.TemplatePreviewPanel.chkPreviewOn.IsChecked == true)
            {

                //Layout layout = BuildLayoutFromFile(_currentLayoutFile.FileInfo.FullName, this.FlowController.ProjectController, this.FlowController.ProjectController.CurrentFlowProject.SubjectList.CurrentItem.ToDesignerSubject());
                //Canvas canvas = layout.ToCanvas(300, LayoutRenderQuality.High);
                //TemplatePreviewPanel.uxPreviewSurface.Children.Clear();
                //TemplatePreviewPanel.uxPreviewSurface.Children.Add(canvas);


                TemplatePreviewPanel.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                (Action)(() =>
                {
                    TemplatePreviewPanel.uxPreviewSurface.Children.Clear();
                    TemplatePreviewPanel.uxPreviewSurface.Children.Add(_workingTextBlock);
                }));

                //DoEvents();
                //MemoryStream s = RenderLayoutPreviewToMs();
                //LoadLayoutPreview(s);


           //     _ui = TaskScheduler.FromCurrentSynchronizationContext();

           //     Task.Factory.StartNew(
           //() => { LoadLayoutPreview( RenderLayoutPreviewToMs()); },
           // CancellationToken.None,
           // TaskCreationOptions.None,
           // _ui);


                logger.Info("About to run Renderlayout to ms in backgrond");
                RunInBackground<MemoryStream>(
                    () => { return RenderLayoutPreviewToMs(); },
                    (s) => LoadLayoutPreview(s));
            }
        }

        public static Layout BuildLayoutFromFile(string xmlFile, ProjectController controller,
           Flow.Designer.Model.Project.Subject sub = null)
        {
            Layout layout = ObjectSerializer.FromXmlFile<Layout>(xmlFile);
            layout.ImageTypes = Flow.Designer.Model.Project.ImageTypes.Build(controller.FlowMasterDataContext);
            layout.DesignerDynamicFields = Flow.Designer.Model.Project.DesignerDynamicFields.Build(controller.CurrentFlowProject);

            if (sub != null)
                layout.Subject = sub;

            return layout;
        }
        static void DoEvents()
        {
            System.Windows.Forms.Application.DoEvents();
            //DispatcherFrame frame = new DispatcherFrame(true);
            //Dispatcher.CurrentDispatcher.BeginInvoke
            //(
            //DispatcherPriority.Background,
            //(SendOrPostCallback)delegate(object arg)
            //{
            //    var f = arg as DispatcherFrame;
            //    f.Continue = false;
            //},
            //frame
            //);
            //Dispatcher.PushFrame(frame);
        }

        Object _generatePreviewLock = new Object();
        public void RunInBackground<T>(Func<T> action, Action<T> onSuccess)
        {
            Thread t = new Thread(new ThreadStart(() =>
            {
                Thread.Sleep(100);
                lock (_generatePreviewLock)
                {
                    try
                    {

                        T result = action();
                        TemplatePreviewPanel.uxPreviewSurface.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                            (Action)(() => { onSuccess(result); }));
                    }
                    catch (Exception e)
                    {
                        TemplatePreviewPanel.uxPreviewSurface.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                            (Action)(() => { System.Windows.MessageBox.Show("Error: " + e.ToString()); }));
                        logger.ErrorException("Exception occured", e);
                    }
                }
            }));
            t.SetApartmentState(ApartmentState.STA);
            t.IsBackground = true;
            t.Start();
        }

        MemoryStream RenderLayoutPreviewToMs()
        {
            //Thread.Sleep(2000);
            if (_currentLayoutFile == null) return null;
            try
            {
                Layout layout = ObjectSerializer.FromXmlFile<Layout>(_currentLayoutFile.FileInfo.FullName);
                layout.ImageTypes = Flow.Designer.Model.Project.ImageTypes.Build(FlowController.ProjectController.FlowMasterDataContext);
                layout.DesignerDynamicFields = Flow.Designer.Model.Project.DesignerDynamicFields.Build(FlowController.ProjectController.CurrentFlowProject);

                

                FlowProject proj = FlowController.ProjectController.CurrentFlowProject;
                if (proj.SubjectCount > 0 && proj.SubjectList.CurrentItem != null)
                {
                    var curSub = proj.SubjectList.CurrentItem;
                    var curImages = curSub.SubjectImages;

                    // setup an image collection event handler
                    if (curImages != _watchedImageList)
                    {
                        // remove previous event handler
                        if (_watchedImageList != null)
                            _watchedImageList.ListChanged -= SubjectImages_ListChanged;

                        // attach new event handler
                        curImages.ListChanged += SubjectImages_ListChanged;
                        _watchedImageList = curImages;
                    }
                    layout.Subject = curSub.ToDesignerSubject();
                }

                Canvas canvas = layout.ToCanvas(96, LayoutRenderQuality.Medium);
                
                logger.Info("We have out Canvas, starting memory stream");
                MemoryStream ms = new MemoryStream();
                new FrameworkElementRenderService().RenderToJpegStream(canvas, 96, ms);
                logger.Info("gonna return the memory stream");
                canvas.Children.Clear();
                return ms;
            }
            catch (Exception e)
            {
                System.Windows.MessageBox.Show("Error: " + e.ToString());
                logger.ErrorException("Exception occured", e);
                return null;
            }
        }

        EntitySet<SubjectImage> _watchedImageList = null;
        private TaskScheduler _ui;
        void SubjectImages_ListChanged(object sender, System.ComponentModel.ListChangedEventArgs e)
        {
            //slight delay

            UpdateLayoutPreview();
        }

        

        void LoadLayoutPreview(MemoryStream ms)
        {
            if (ms == null) return;
            try
            {
                ms.Position = 0;

                BitmapImage source = new BitmapImage();
                source.BeginInit();
                source.StreamSource = ms;
                source.EndInit();
                Image image = new Image();
                image.Source = source;
                TemplatePreviewPanel.uxPreviewSurface.Children.Clear();
                TemplatePreviewPanel.uxPreviewSurface.Children.Add(image);
                //ms.Dispose();
                
            }
            catch (Exception e)
            {
                System.Windows.MessageBox.Show("Error: " + e.ToString());
                logger.ErrorException("Exception occurred", e);
            }
        }

        public void StopAutoPrint()
        {
            this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.AutoPrintLayout = false;
            //TemplatePreviewPanel.chkAutoPrint.IsChecked = false;
            this.FlowController.ProjectController.FlowMasterDataContext.SavePreferences();
        }
    }


}

