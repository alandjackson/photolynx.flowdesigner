﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.Objects;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;

using Flow.Controller.Capture;
using Flow.Controller.Catalog;
using Flow.Lib;
using Flow.Lib.Helpers;
using Flow.Lib.Helpers.BarcodeScan;
using Flow.Schema;
using Flow.Schema.LinqModel.DataContext;
using Flow.View;
using Flow.View.Preferences;
using Flow.Controller.Project;
using Flow.Controls.View.RecordDisplay;

namespace Flow.Controller.Preferences
{
    public class PreferencesController
    {
        public IShowsNotificationProgress Notifier { get; set; }

        PreferenceSectionsPanel PreferenceSectionsPanel;

        public ProjectController ProjectController { get; set; }

        public FlowProject CurrentFlowProject
        { get { return (this.ProjectController == null) ? null : this.ProjectController.CurrentFlowProject; } }

        public FlowController FlowController { get; set; }
        //UserPanel UserPanel { get; set; }

       
        
        public PreferencesController()
        {
            
        }

        public void Main(PreferenceSectionsPanel pnl, FlowController flowController)
        {
            PreferenceSectionsPanel = pnl;
            PreferenceSectionsPanel.FlowController = flowController;
            this.FlowController = flowController;

			this.PreferenceSectionsPanel.DataContext = flowController.ProjectController.FlowMasterDataContext;
        }


    }
}
