﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.Controller.Project;
using Flow.Lib.AsyncWorker;
using Flow.Lib;
using System.ComponentModel;
using Flow.Schema.LinqModel.DataContext;
using System.IO;
using System.Windows.Threading;
using System.Threading;
using StructureMap;
using Flow.Schema;
using NLog;
using Flow.Lib.Helpers;
using Flow.Lib.Net;
using Flow.Controls.View.Dialogs;
using System.Xml.Serialization;
using System.Net;
using System.Drawing;
using NetServ.Net.Json;
using System.Security.Cryptography;
using System.Runtime.InteropServices;
using Flow.Lib.Preferences;
using Flow.Lib.RIS;
using Flow.Lib.Activation;

namespace Flow.Controller
{
    public class LabOrderUploader : NotifyPropertyBase
    {


        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        static extern EXECUTION_STATE SetThreadExecutionState(EXECUTION_STATE esFlags);

        [FlagsAttribute]
        public enum EXECUTION_STATE : uint
        {
            ES_AWAYMODE_REQUIRED = 0x00000040,
            ES_CONTINUOUS = 0x80000000,
            ES_DISPLAY_REQUIRED = 0x00000002,
            ES_SYSTEM_REQUIRED = 0x00000001
        }

        //no longer prevent the system from sleeping
        void AllowSleep()
        {
            SetThreadExecutionState(EXECUTION_STATE.ES_CONTINUOUS);
        }

        //prevents the system from sleeping untill the app closes or AllowSleep is called
        void PreventSleep()
        {
            // Prevent Idle-to-Sleep (monitor not affected)
            SetThreadExecutionState(EXECUTION_STATE.ES_CONTINUOUS | EXECUTION_STATE.ES_AWAYMODE_REQUIRED);
        }

        //just resets the idle clock, must keep calling this periodically to keep awake
        void KeepSystemAwake()
        {
            SetThreadExecutionState(EXECUTION_STATE.ES_SYSTEM_REQUIRED);
        }



        FlowController FlowController { get; set; }

        FlowBackgroundWorker bWorker;

        private static Logger logger = LogManager.GetCurrentClassLogger();

        bool keepRunning { get; set; }


        private BindingList<string> _uploadQueue { get; set; }
        

        private string _currentOrder { get; set; }
        private long _currentOrderTotalFiles { get; set; }
        private long _currentOrderUploadedFiles { get; set; }
        private OrderUploadHistory _currentOrderUploadHistory { get; set; }
        private bool _skipCurrentOrder { get; set; }

        public int ProgressBarValue
        {
            get
            {
                if (_currentOrderTotalFiles > 0)
                    return (int)(((double)(double)_currentOrderUploadedFiles / (double)_currentOrderTotalFiles) * (double)100);
                else
                    return 0;
            }
        }

        public BindingList<string> UploadQueue
        {
            get
            {
                return _uploadQueue;
            }
            set
            {
                _uploadQueue = value;
                SendPropertyChanged("UploadQueue");

            }
        }

        private Boolean _uploadInProgress { get; set; }
        public Boolean UploadInProgress
        {
            get
            {
                return _uploadInProgress;
            }
            set
            {
                _uploadInProgress = value;
                SendPropertyChanged("UploadInProgress");

            }
        }

        public string CurrentOrder
        {
            get
            {
                return _currentOrder;
            }
            set
            {
                _currentOrder = value;
                SendPropertyChanged("CurrentOrder");

            }
        }
        public long CurrentOrderTotalFiles
        {
            get
            {
                return _currentOrderTotalFiles;
            }
            set
            {
                _currentOrderTotalFiles = value;
                SendPropertyChanged("CurrentOrderTotalFiles");
                SendPropertyChanged("ProgressBarValue");

            }
        }
        public long CurrentOrderUploadedFiles
        {
            get
            {
                return _currentOrderUploadedFiles;
            }
            set
            {
                _currentOrderUploadedFiles = value;
                SendPropertyChanged("CurrentOrderUploadedFiles");
                SendPropertyChanged("ProgressBarValue");

            }
        }

        public OrderUploadHistory CurrentOrderUploadHistory
        {
            get { return _currentOrderUploadHistory; }
        }

        private Dispatcher dispatcher { get; set; }


        public LabOrderUploader(FlowController flowController, Dispatcher disp)
        {
            this.FlowController = flowController;
            this.dispatcher = disp;

            if (UploadQueue == null)
                UploadQueue = new BindingList<string>();
            this.UploadQueue.AddingNew += new AddingNewEventHandler(UploadQueue_AddingNew);
        }

        void UploadQueue_AddingNew(object sender, AddingNewEventArgs e)
        {
            SendPropertyChanged("UploadQueue");
        }

        public void Begin()
        {
            //start a thread that will be looking for files to upload to lab
            keepRunning = true;
            bWorker = new FlowBackgroundWorker();
            bWorker = FlowBackgroundWorkerManager.RunWorker(UploaderEngine, UploaderEngine_Stopped);
            //bWorker.RunWorkerAsync();
        }

        /// <summary>
        /// Cancel an order upload job and remove it from the queue.
        /// </summary>
        /// <param name="orderUploadToCancel"></param>
        public void CancelOrder(OrderUploadHistory orderUploadToCancel)
        {
            FlowMasterDataContext fmdc = this.FlowController.ProjectController.FlowMasterDataContext;

            // Stop the order if it is currently uploading
            if (orderUploadToCancel == this.CurrentOrderUploadHistory)
            {
                logger.Info("Signaling order upload engine to skip the current upload job.");
                this._skipCurrentOrder = true;
            }

            // Remove the order from the upload history list
            fmdc.OrderUploadHistories.DeleteOnSubmit(orderUploadToCancel);
            fmdc.SubmitChanges();
            fmdc.RefreshOrderUploadHistoryList();

            logger.Info("Done removing upload job from queue.");
        }

        void UploaderEngine_Stopped(object sender, RunWorkerCompletedEventArgs e)
        {

        }

        void UploaderEngine(object sender, DoWorkEventArgs e)
        {
            string LabOrderQueueFolder = this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.LabOrderQueueDirectory;

            CurrentOrder = "None In Progress";
            CurrentOrderTotalFiles = 0;
            CurrentOrderUploadedFiles = 0;

            Thread.Sleep(5000);
            while (keepRunning)
            {
                try
                {
                    //while (this.FlowController.ApplicationPanel.IsEnabled == false)
                    //{
                    //    //if the main ui is disabled, pause the uploader, other stuff is going on
                    //    Thread.Sleep(5000);
                    //}

                    Thread.Sleep(15000);
                    string dots = "";
                    UpdateQueue(LabOrderQueueFolder);
                    UploadInProgress = false;

                    List<OrderUploadHistory> MainList = new List<OrderUploadHistory>();


                    this.dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                                 {
                                     try
                                     {
                                         if (this.FlowController.ProjectController.FlowMasterDataContext.UploadQueueHasItems)
                                         {
                                             
                                             MainList = new List<OrderUploadHistory>(this.FlowController.ProjectController.FlowMasterDataContext.OrderUploadHistoryQueueList);
                                         }
                                     }
                                     catch
                                     {
                                         //do nothing, we will loop around and try again
                                     }
                                 }));

                    foreach (OrderUploadHistory hist in MainList)
                    {
                        PreventSleep();
                        this._currentOrderUploadHistory = hist;

                        bool isLabOrder = false;
                        bool isIMJob = false;
                        bool isProject = false;
                        bool isRemoteImageService = false;
                        bool isImageUpload = false;

                        string uploadJob = null;
                        if (hist.IsLabOrder)
                        {
                            uploadJob = Path.Combine(LabOrderQueueFolder, "order_" + hist.LabOrderId);
                            isLabOrder = true;
                        }
                        else if (hist.IsImageMatchExport == true)
                        {
                            uploadJob = Path.Combine(LabOrderQueueFolder, "ImageMatch_exp_" + hist.OrderUploadHistoryID);
                            isIMJob = true;
                        }
                        else if (hist.IsProjectExport == true)
                        {
                            uploadJob = Path.Combine(LabOrderQueueFolder, "Project_exp_" + hist.OrderUploadHistoryID);
                            isProject = true;
                        }
                        else if (hist.IsRemoteImageServiceUpload == true)
                        {
                            uploadJob = Path.Combine(LabOrderQueueFolder, "RemoteImageService_exp_" + hist.OrderUploadHistoryID);
                            isRemoteImageService = true;
                        }
                        else if (hist.IsImageUpload == true)
                        {
                            uploadJob = Path.Combine(LabOrderQueueFolder, "ImageUpload_" + hist.OrderUploadHistoryID);
                            isImageUpload = true;
                        }

                        if (!Directory.Exists(uploadJob))
                            continue;

                        UploadInProgress = true;
                        UpdateQueue(LabOrderQueueFolder);




                        string jobDirName = new DirectoryInfo(uploadJob).Name;


                        string orderFormFile = "";

                        string mailFile = "";

                        if (isImageUpload)
                        {
                            string DataFile = System.IO.Path.Combine(uploadJob, "JobInfo.xml");
                            if (!File.Exists(DataFile))
                                continue;

                            ImageUploadJob imageUploadJob = null;

                            using (var stream = System.IO.File.OpenRead(DataFile))
                            {
                                var serializer = new XmlSerializer(typeof(ImageUploadJob));
                                imageUploadJob = serializer.Deserialize(stream) as ImageUploadJob;
                            }

                            CurrentOrderTotalFiles = imageUploadJob.images.Count;

                            int counter = 0;
                            foreach (ImageUploadItem item in imageUploadJob.images)
                            {
                                if(!File.Exists(Path.Combine(uploadJob, item.ImageName)))
                                {
                                    //assume it does not exist because its already done, so we can skip it.
                                    continue;
                                }
                                CurrentOrderUploadedFiles = counter++;
                                logger.Info("uploading image for: " + item.ImageName);

                                try
                                {
                                    HttpStatusCode code = UploadImage(item, uploadJob);


                                    if (code == HttpStatusCode.OK)
                                    {
                                        File.Delete(Path.Combine(uploadJob, item.ImageName));
                                    }
                                    else
                                    {
                                        //Error, did not upload
                                        throw new Exception("FAILED TO UPLOAD IMAGE full size");
                                    }
                                }
                                catch (Exception ex)
                                {
                                    logger.Error("Faile uploading image for: " + item.ImageName);
                                    logger.Error("Error: " + ex.Message);
                                    throw (ex);
                                }

                            }

                        }
                        else if (isRemoteImageService)
                        {

                            //string ftpAddress = this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.ProjectTransfer.HostAddress;
                            //int ftpPort = this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.ProjectTransfer.HostPort;
                            //string ftpUserName = this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.ProjectTransfer.UserName;
                            //string ftpPassword = this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.ProjectTransfer.Password;
                            //string ftpDirectory = this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.ProjectTransfer.RemoteImageMatchDirectory;
                            //bool useSftp = this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.ProjectTransfer.UseSftp;
                            

                            //string ftpAddress = "113.108.248.106";
                            
                            //string ftpAddress = "ftp.photoretouchonline.com";
                            //int ftpPort = 21;
                            //string ftpUserName = "FlowBridge@photoretouchonline.com";
                            //string ftpPassword = "4c1665a5";
                            //bool useSftp = false;
                            //string ftpRoot = "";

                            //string ftpAddress = "sb.jaleatech.com";
                            //int ftpPort = 21;
                            //string ftpUserName = "Photolynx";
                            //string ftpPassword = "589305";
                            //bool useSftp = false;
                            //string ftpRoot = "";


                           



                            string serviceName = this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.RemoteImageServiceFtp;
                            RemoteImageService ris = this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.RemoteImageServices.FirstOrDefault(r => r.Name == serviceName);

                            string ftpAddress = ris.Ftp;
                            int ftpPort = Int32.Parse(ris.Port);

                            string ftpUserName = this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.RemoteImageServiceUser;
                            string ftpPassword = this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.RemoteImageServicePW;
                            bool useSftp = false;
                            string ftpRoot = "";
                            if (ftpAddress.Contains("sftp") || ftpPort == 22)
                            {
                                ftpPort = 22;
                                useSftp = true;
                            }


                            //string ftpAddress = "69.162.106.166";
                            //int ftpPort = 21;
                            //string ftpUserName = "photolynx";
                            //string ftpPassword = "p@wprint$";
                            //bool useSftp = false;

                            //string ftpRoot = "/RemoteImageService";

                            string studioName = "";
                            string projectName = "";
                            string projectGuid = "";



                            string dataFile = Path.Combine(uploadJob, "JobInfo.txt");
                            foreach (string line in File.ReadAllLines(dataFile))
                            {
                                if(line.Contains(':'))
                                {
                                    string item = line.Split(':')[0];
                                    string value = line.Split(':')[1];
                                    if (item.StartsWith("Studio"))
                                        studioName = value;
                                    if (item.StartsWith("Project"))
                                        projectName = value;
                                    if (item.StartsWith("Guid"))
                                        projectGuid = value;

                                }
                            }

                            string batchDate = DateTime.Now.ToString("yyyy_MM_dd_hh_mm_ss");
                            string randStr = TicketGenerator.GenerateTicketString(5);
                            ftpRoot = ftpRoot + "/" + "IN" + "/";
                            string ftpDirectory = ftpRoot + projectGuid + "_" + randStr + "/";

                           


                            //establish an ftp connection
                            FileTransfer ft = new FileTransfer(ftpAddress, ftpPort, ftpUserName, ftpPassword, useSftp, null);
                            ft.Connect();
                           
                            

                            long iterCount = 0;
                            //iterate thru all the files,
                            int imgCount = 0;
                            foreach (string upfile in Directory.GetFiles(uploadJob))
                            {
                                if (upfile.ToLower().EndsWith("jpg") || (upfile.ToLower().EndsWith("png")) || (upfile.ToLower().EndsWith("jpeg")))
                                    imgCount++;

                                if (this._skipCurrentOrder)
                                {
                                    ft.Disconnect();
                                    File.Delete(upfile);
                                    break;
                                }

                                CurrentOrderUploadedFiles = ++iterCount;

                                UpdateQueue(LabOrderQueueFolder);

                                string upfilename = new FileInfo(upfile).Name;

                                //skip over done.txt and email until the end
                                if (upfilename == "done.txt")
                                    continue;

                                if (upfilename == "mail.txt")
                                    continue;

                                if (useSftp)
                                {
                                    if (!ft.sftpClient.DirectoryExists(ftpRoot))
                                    {
                                        ft.sftpClient.CreateDirectory(ftpRoot);

                                    }
                                    if (!ft.sftpClient.DirectoryExists(ftpDirectory))
                                    {
                                        ft.sftpClient.CreateDirectory(ftpDirectory);

                                    }
                                    if ( !ft.sftpClient.FileExists(ftpDirectory + "/" + upfilename) || (ft.sftpClient.GetFileLength(ftpDirectory + "/" + upfilename) < (new FileInfo(upfile).Length)))
                                    {
                                        //NotificationInfo.Update("Sending File " + si.SubjectImage.ImageFileName);
                                        ft.SendFile(upfile, ftpDirectory + "/" + upfilename);
                                        if ( !ft.sftpClient.FileExists(ftpDirectory + "/" + upfilename) || (ft.sftpClient.GetFileLength(ftpDirectory + "/" + upfilename) < (new FileInfo(upfile).Length)))
                                        {
                                            //NotificationInfo.Update("Sending File FAILED, trying again" + si.SubjectImage.ImageFileName);
                                            ft.SendFile(upfile, ftpDirectory + "/" + upfilename);
                                            if ( !ft.sftpClient.FileExists(ftpDirectory + "/" + upfilename) || (ft.sftpClient.GetFileLength(ftpDirectory + "/" + upfilename) < (new FileInfo(upfile).Length)))
                                            {
                                                logger.Error("Sending Order - Sending File FAILED 2 times: {0}", upfilename);
                                                FlowMessageDialog msg = new FlowMessageDialog("Upload Error", "There seems to be an error uploading files. Please restart Flow to try again.\n\nIf the problem keeps happening, please contact your lab.");
                                                msg.CancelButtonVisible = false;
                                                msg.ShowDialog();
                                                this.FlowController.ProjectController.IsUploading = false;
                                                keepRunning = false;
                                                return;
                                            }
                                        }
                                        //NotificationInfo.Step();
                                    }

                                    //if the file is up, delete it
                                    if (!upfilename.StartsWith("OrderForm_") && ft.sftpClient.FileExists(ftpDirectory + "/" + upfilename) && ft.sftpClient.GetFileLength(ftpDirectory + "/" + upfilename) == (new FileInfo(upfile).Length))
                                        File.Delete(upfile);

                                    if (upfilename.StartsWith("OrderForm_"))
                                        orderFormFile = upfile;
                                }
                                else
                                {

                                    if (!ft.ftpClient.DirectoryExists(ftpRoot))
                                    {
                                        ft.ftpClient.CreateDirectory(ftpRoot);

                                    }
                                    if (!ft.ftpClient.DirectoryExists(ftpDirectory))
                                    {
                                        ft.ftpClient.CreateDirectory(ftpDirectory);

                                    }
                                    if ( !ft.ftpClient.FileExists(ftpDirectory + "/" + upfilename) || (ft.ftpClient.GetFileLength(ftpDirectory + "/" + upfilename) < (new FileInfo(upfile).Length)))
                                    {
                                        //NotificationInfo.Update("Sending File " + si.SubjectImage.ImageFileName);
                                        ft.SendFile(upfile, ftpDirectory + "/" + upfilename);
                                        if ( !ft.ftpClient.FileExists(ftpDirectory + "/" + upfilename) || (ft.ftpClient.GetFileLength(ftpDirectory + "/" + upfilename) < (new FileInfo(upfile).Length)))
                                        {
                                            //NotificationInfo.Update("Sending File FAILED, trying again" + si.SubjectImage.ImageFileName);
                                            ft.SendFile(upfile, ftpDirectory + "/" + upfilename);
                                            if ( !ft.ftpClient.FileExists(ftpDirectory + "/" + upfilename) || (ft.ftpClient.GetFileLength(ftpDirectory + "/" + upfilename) < (new FileInfo(upfile).Length)))
                                            {
                                                logger.Error("Sending Order - Sending File FAILED 2 times: {0}", upfilename);
                                                FlowMessageDialog msg = new FlowMessageDialog("Upload Error", "There seems to be an error uploading files, Please restart Flow to try again.\n\nIf the problem keeps happening, please contact your lab.");
                                                msg.CancelButtonVisible = false;
                                                msg.ShowDialog();
                                                this.FlowController.ProjectController.IsUploading = false;
                                                keepRunning = false;
                                                return;
                                            }
                                        }
                                        //NotificationInfo.Step();
                                    }

                                    //if the file is up, delete it
                                    if (!upfilename.StartsWith("OrderForm_") && ft.ftpClient.FileExists(ftpDirectory + "/" + upfilename) && ft.ftpClient.GetFileLength(ftpDirectory + "/" + upfilename) == (new FileInfo(upfile).Length))
                                    {
                                        bool tryDelete = true;
                                        int tryCount = 0;
                                        while (tryDelete)
                                        {
                                            tryCount++;
                                            try
                                            {

                                                File.Delete(upfile);
                                                tryDelete = false;
                                            }
                                            catch (Exception ex)
                                            {
                                                logger.Error("Sending Order - ERROR Deleteing a file we already uploaded: {0}", upfilename);
                                                Thread.Sleep(1000);
                                                if (tryCount > 3)
                                                {
                                                    logger.Error("Done Trying");
                                                    tryDelete = false;
                                                }
                                                else
                                                {
                                                    logger.Error("Trying Again");
                                                }

                                            }
                                        }
                                    }

                                    if (upfilename.StartsWith("OrderForm_"))
                                        orderFormFile = upfile;
                                }

                            }
                            string doneFile = Path.Combine(uploadJob, "uploadComplete.txt");
                           // if (!File.Exists(doneFile))
                           //     File.Create(doneFile);
                           // Thread.Sleep(1000);
                            File.WriteAllText(doneFile, "Upload Complete: " + projectGuid + "_" + randStr);

                            //send done.txt
                            ft.SendFile(doneFile, ftpDirectory + "/uploadComplete.txt");
                            File.Delete(doneFile);

                            //close ftp connection
                            ft.Disconnect();

                            mailFile = Path.Combine(uploadJob, "mail.txt");



                             //
                            //UPLOAD STATS
                            //
                            RISStore store = new RISStore();
                            
                            ActivationValidator activation = new ActivationValidator(this.FlowController.ActivationKeyFile);
                            ActivationStore activationStore = activation.Repository.Load();

                            store.Application = "Flow";
                            store.CustomerName = studioName;
                            store.ServiceFTP = ftpAddress;
                            store.ServiceUserName = ftpUserName;
                            store.RemoteFolderName = projectGuid + "_" + randStr;
                            store.UpImageCount  = imgCount; 
                            store.ActivationKey = activationStore.ActivationKey;
                            store.ProjectName = projectName;
                            store.ApplicationVersion = FlowContext.GetFlowVersion();
                            store.ServicesOrdered = "";
                            RISPost RISPost = new RISPost();
                            string returnMsg = RISPost.Post(store);
                            //
                            //END UPLOAD STATS
                            //



                        }
                        else if (isIMJob || isProject)
                        {
                            string uploadFilePath = GetZipOrPaf(uploadJob);

                            FlowProject tempFlowProject = this.FlowController.ProjectController.FlowMasterDataContext.FlowProjects.FirstOrDefault(p => p.FlowProjectGuid.ToString() == hist.ProjectGuid);

                            string ftpAddress = this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.ProjectTransfer.HostAddress;
                            int ftpPort = this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.ProjectTransfer.HostPort;
                            string ftpUserName = this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.ProjectTransfer.UserName;
                            string ftpPassword = this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.ProjectTransfer.Password;
                            string ftpDirectory = this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.ProjectTransfer.RemoteImageMatchDirectory;
                            if (isProject)
                                ftpDirectory = this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.ProjectTransfer.RemoteDirectory;
                            if (ftpDirectory == null) ftpDirectory = "";
                            if (tempFlowProject != null)
                            {
                                ftpDirectory = ftpDirectory + "/" + tempFlowProject.Studio.OrganizationName;
                                if (tempFlowProject.Studio.LabCustomerID != null && tempFlowProject.Studio.LabCustomerID != "")
                                    ftpDirectory = ftpDirectory + " (" + tempFlowProject.Studio.LabCustomerID + ")";

                            }
                            bool useSftp = this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.ProjectTransfer.UseSftp;

                            FileInfo uploadFile = new FileInfo(uploadFilePath);
                            CurrentOrder = uploadFile.Name;
                            CurrentOrderTotalFiles = uploadFile.Length;

                            if (uploadFilePath.EndsWith(".zip"))
                                mailFile = uploadFilePath.Split(".zip")[0] + "_mail.txt";
                            else if (uploadFilePath.EndsWith(".paf"))
                                mailFile = uploadFilePath.Split(".paf")[0] + "_mail.txt";

                            orderFormFile = mailFile.Split("_mail.txt")[0] + "_OrderForm.pdf"; ;



                            this.FlowController.ProjectController.IsUploading = true;


                            FileTransfer ft = new FileTransfer(ftpAddress, ftpPort, ftpUserName, ftpPassword, useSftp, null);

                            string destFile = ftpDirectory + "/" + uploadFile.Name;
                            destFile = destFile.TrimStart('/');

                            try
                            {
                                ft.Connect();
                                int cnt = 1;

                                if (useSftp)
                                {
                                    while (ft.sftpClient.FileExists(destFile))
                                    {
                                        destFile = ftpDirectory + "/" + uploadFile.Name.Replace(uploadFile.Extension, "_" + cnt++ + uploadFile.Extension);
                                        destFile = destFile.TrimStart('/');
                                        if (cnt > 50)
                                            break;
                                    }
                                }
                                else
                                {
                                    while (ft.ftpClient.FileExists(destFile))
                                    {
                                        destFile = ftpDirectory + "/" + uploadFile.Name.Replace(uploadFile.Extension, "_" + cnt++ + uploadFile.Extension);
                                        destFile = destFile.TrimStart('/');
                                        if (cnt > 50)
                                            break;
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                logger.ErrorException("There was an error connecting to the ftp server: ", ex);
                                this.dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                                {
                                    FlowMessageDialog msg = new FlowMessageDialog("FTP Error", "There was an error connecting to the ftp server.\n\nCheck your FTP settings\n\nFTP FAILED\n\n" + ex.Message);
                                    msg.CancelButtonVisible = false;
                                    msg.ShowDialog();

                                }));
                                this.FlowController.ApplicationPanel.IsEnabled = true;
                                return;
                            }

                            if (!this._skipCurrentOrder)
                            {
                                ft.SentBytesUpdated += new EventHandler<EventArgs<long>>(ft_SentBytesUpdated);
                                ft.SendFile(uploadFilePath, destFile);
                            }

                            ft.Disconnect();
                            File.Delete(uploadFilePath);
                        }
                        else if (isLabOrder)
                        {
                            string ftpAddress = this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.OrderSubmission.ImageQuixFtpAddress.Trim();
                            int ftpPort = this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.OrderSubmission.ImageQuixFtpPort;
                            string ftpUserName = this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.OrderSubmission.ImageQuixFtpUserName.Trim();
                            string ftpPassword = this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.OrderSubmission.ImageQuixFtpPassword.Trim();
                            string ftpDirectory = this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.OrderSubmission.ImageQuixFtpDirectory.Trim();
                            bool useSftp = this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.OrderSubmission.ImageQuixUseSftp;



                            string uploadDirName = new DirectoryInfo(uploadJob).Name;
                            CurrentOrder = uploadDirName;
                            CurrentOrderTotalFiles = Directory.GetFiles(uploadJob).Count();

                            //make suer there is a done.txt file for lab orders
                            if (isLabOrder && !File.Exists(Path.Combine(uploadJob, "done.txt")))
                                continue;

                            this.FlowController.ProjectController.IsUploading = true;

                            ftpDirectory = this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.OrderSubmission.ImageQuixFtpDirectory.Trim();

                            ftpDirectory = ftpDirectory + "/" + uploadDirName;
                            if (ftpDirectory.StartsWith("//"))
                                ftpDirectory = ftpDirectory.Substring(1);
                            if (!ftpDirectory.StartsWith("/"))
                                ftpDirectory = "/" + ftpDirectory;

                            ftpDirectory = "." + ftpDirectory;

                            //establish an ftp connection
                            FileTransfer ft = new FileTransfer(ftpAddress, ftpPort, ftpUserName, ftpPassword, useSftp, null);
                            ft.Connect();

                            long iterCount = 0;
                            //iterate thru all the files,
                            foreach (string upfile in Directory.GetFiles(uploadJob))
                            {
                                if (this._skipCurrentOrder)
                                {
                                    ft.Disconnect();
                                    File.Delete(upfile);
                                    break;
                                }

                                CurrentOrderUploadedFiles = ++iterCount;

                                UpdateQueue(LabOrderQueueFolder);

                                string upfilename = new FileInfo(upfile).Name;

                                //skip over done.txt and email until the end
                                if (upfilename == "done.txt")
                                    continue;

                                if (upfilename == "mail.txt")
                                    continue;

                                if (useSftp)
                                {
                                    logger.Info("about to upload a file: " + upfilename);
                                    if ( !ft.sftpClient.FileExists(ftpDirectory + "/" + upfilename) || (ft.sftpClient.GetFileLength(ftpDirectory + "/" + upfilename) < (new FileInfo(upfile).Length)))
                                    {
                                        //NotificationInfo.Update("Sending File " + si.SubjectImage.ImageFileName);
                                        ft.SendFile(upfile, ftpDirectory + "/" + upfilename);
                                        if ( !ft.sftpClient.FileExists(ftpDirectory + "/" + upfilename) || (ft.sftpClient.GetFileLength(ftpDirectory + "/" + upfilename) < (new FileInfo(upfile).Length)))
                                        {
                                            logger.Info("upload file, failed, trying again 2: " + upfilename);
                                            //NotificationInfo.Update("Sending File FAILED, trying again" + si.SubjectImage.ImageFileName);
                                            ft.SendFile(upfile, ftpDirectory + "/" + upfilename);
                                            if ( !ft.sftpClient.FileExists(ftpDirectory + "/" + upfilename) || (ft.sftpClient.GetFileLength(ftpDirectory + "/" + upfilename) < (new FileInfo(upfile).Length)))
                                            {
                                                logger.Error("Sending Order - Sending File FAILED 2 times: {0}", upfilename);
                                                FlowMessageDialog msg = new FlowMessageDialog("Upload Error", "There seems to be an error uploading files, Please restart Flow to try again.\n\nIf the problem keeps happening, please contact your lab.");
                                                msg.CancelButtonVisible = false;
                                                msg.ShowDialog();
                                                this.FlowController.ProjectController.IsUploading = false;
                                                keepRunning = false;
                                                return;
                                            }
                                        }
                                        //NotificationInfo.Step();
                                    }

                                    //if the file is up, delete it
                                    if (!upfilename.StartsWith("OrderForm_") && ft.sftpClient.FileExists(ftpDirectory + "/" + upfilename) && ft.sftpClient.GetFileLength(ftpDirectory + "/" + upfilename) == (new FileInfo(upfile).Length))
                                        File.Delete(upfile);

                                    if (upfilename.StartsWith("OrderForm_"))
                                        orderFormFile = upfile;
                                }
                                else
                                {
                                    logger.Info("about to upload a file: " + upfilename);
                                    if ( !ft.ftpClient.FileExists(ftpDirectory + "/" + upfilename) || (ft.ftpClient.GetFileLength(ftpDirectory + "/" + upfilename) < (new FileInfo(upfile).Length)))
                                    {
                                        //NotificationInfo.Update("Sending File " + si.SubjectImage.ImageFileName);
                                        ft.SendFile(upfile, ftpDirectory + "/" + upfilename);
                                        if ( !ft.ftpClient.FileExists(ftpDirectory + "/" + upfilename) || (ft.ftpClient.GetFileLength(ftpDirectory + "/" + upfilename) < (new FileInfo(upfile).Length)))
                                        {
                                            logger.Info("upload file, failed, trying again 2: " + upfilename);
                                            //NotificationInfo.Update("Sending File FAILED, trying again" + si.SubjectImage.ImageFileName);
                                            ft.SendFile(upfile, ftpDirectory + "/" + upfilename);
                                            if ( !ft.ftpClient.FileExists(ftpDirectory + "/" + upfilename) || (ft.ftpClient.GetFileLength(ftpDirectory + "/" + upfilename) < (new FileInfo(upfile).Length)))
                                            {
                                                logger.Error("Sending Order - Sending File FAILED 2 times: {0}", upfilename);
                                                FlowMessageDialog msg = new FlowMessageDialog("Upload Error", "There seems to be an error uploading files, Please restart Flow to try again.\n\nIf the problem keeps happening, please contact your lab.");
                                                msg.CancelButtonVisible = false;
                                                msg.ShowDialog();
                                                this.FlowController.ProjectController.IsUploading = false;
                                                keepRunning = false;
                                                return;
                                            }
                                        }
                                        //NotificationInfo.Step();
                                    }

                                    //if the file is up, delete it
                                    if (!upfilename.StartsWith("OrderForm_") && ft.ftpClient.FileExists(ftpDirectory + "/" + upfilename) && ft.ftpClient.GetFileLength(ftpDirectory + "/" + upfilename) == (new FileInfo(upfile).Length))
                                    {
                                        bool tryDelete = true;
                                        int tryCount = 0;
                                        while (tryDelete)
                                        {
                                            tryCount++;
                                            try
                                            {

                                                File.Delete(upfile);
                                                tryDelete = false;
                                            }
                                            catch (Exception ex)
                                            {
                                                logger.Error("Sending Order - ERROR Deleteing a file we already uploaded: {0}", upfilename);
                                                Thread.Sleep(1000);
                                                if (tryCount > 3)
                                                {
                                                    logger.Error("Done Trying");
                                                    tryDelete = false;
                                                }
                                                else
                                                {
                                                    logger.Error("Trying Again");
                                                }

                                            }
                                        }
                                    }

                                    if (upfilename.StartsWith("OrderForm_"))
                                        orderFormFile = upfile;
                                }

                            }

                            //send done.txt
                            ft.SendFile(Path.Combine(uploadJob, "done.txt"), ftpDirectory + "/done.txt");
                            File.Delete(Path.Combine(uploadJob, "done.txt"));

                            //close ftp connection
                            ft.Disconnect();

                            mailFile = Path.Combine(uploadJob, "mail.txt");
                        }

                        string orderHistoryID = null;
                        //send email
                        if (File.Exists(mailFile))
                        {
                            Emailer mail = null;
                            try
                            {
                                string mail_to = "";
                                string mail_from = "";
                                string mail_cc = "";
                                string mail_subject = "";
                                string mail_attachment = "";
                                string mail_body = "";

                                using (StreamReader sr = File.OpenText(mailFile))
                                {
                                    string s = "";
                                    while ((s = sr.ReadLine()) != null)
                                    {
                                        if (s.StartsWith("To: "))
                                            mail_to = s.Replace("To: ", "");
                                        else if (s.StartsWith("From: "))
                                            mail_from = s.Replace("From: ", "");
                                        else if (s.StartsWith("CC: "))
                                            mail_cc = s.Replace("CC: ", "");
                                        else if (s.StartsWith("Subject: "))
                                            mail_subject = s.Replace("Subject: ", "");
                                        else if (s.StartsWith("Attatchment: "))
                                            mail_attachment = s.Replace("Attatchment: ", "");
                                        else if (s.StartsWith("HistoryID: "))
                                            orderHistoryID = s.Replace("HistoryID: ", "");
                                        else
                                            mail_body = mail_body + s + "\n";
                                    }
                                }
                                if (mail_to.Length < 2)
                                    mail_to = mail_cc;
                                if (mail_to.Length > 2)
                                {
                                    mail = new Emailer(mail_subject, mail_body, false, mail_to, mail_from, mail_cc);
                                    mail.AddAttachment(mail_attachment);
                                    mail.Send();
                                    mail.Close();

                                }

                            }
                            catch (Exception ex)
                            {
                                logger.Error("Mail Error: {0}", ex.Message);
                            }

                            File.Delete(mailFile);
                        }


                        if (File.Exists(orderFormFile))
                        {
                            bool deleted = false;
                            int cnt = 0;
                            while (!deleted)
                            {
                                try
                                {
                                    File.Delete(orderFormFile);
                                    deleted = true;
                                }
                                catch
                                {
                                    if (++cnt == 10)
                                        break;
                                    Thread.Sleep(500);
                                }
                            }
                        }

                        if (isImageUpload && Directory.GetFiles(uploadJob).Count() == 1 && File.Exists(Path.Combine(uploadJob, "JobInfo.xml")))
                            File.Delete(Path.Combine(uploadJob, "JobInfo.xml"));
                        if (Directory.GetFiles(uploadJob).Count() == 0)
                            Directory.Delete(uploadJob);
                        else if (this._skipCurrentOrder)
                            Directory.Delete(uploadJob, true);

                        string labOrderID = "0";
                        if (isLabOrder)
                            labOrderID = new FileInfo(uploadJob).Name.Split("_")[1];

                        this.dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                        {
                            //OrderUploadHistory hist = null;
                            //if(isLabOrder)
                            //    hist =  this.FlowController.ProjectController.FlowMasterDataContext.OrderUploadHistories.FirstOrDefault(o => o.LabOrderId == labOrderID);
                            //else if(orderHistoryID != null)
                            //    hist = this.FlowController.ProjectController.FlowMasterDataContext.OrderUploadHistories.FirstOrDefault(o => o.OrderUploadHistoryID == Int32.Parse(orderHistoryID));


                            if (hist != null && !Directory.Exists(uploadJob))
                            {
                                hist.UploadDate = DateTime.Now;
                                this.FlowController.ProjectController.FlowMasterDataContext.SubmitChanges();
                                this.FlowController.ProjectController.FlowMasterDataContext.RefreshOrderUploadHistoryList();
                            }
                        }));

                        CurrentOrder = "None In Progress";
                        CurrentOrderTotalFiles = 0;
                        CurrentOrderUploadedFiles = 0;
                        UploadQueue.Remove(uploadJob);

                        this.FlowController.ProjectController.IsUploading = false;

                        if (this._skipCurrentOrder)
                            this._skipCurrentOrder = false;

                        //Thread.Sleep(100);
                        AllowSleep();
                    }
                    //while (!this.FlowController.ProjectController.FlowServerIsOn)
                    //{
                    //    this.FlowController.ProjectController.FlowServerStatus = "Server is Off";
                    //    Thread.Sleep(3000);
                    //}

                    if (this._skipCurrentOrder)
                        this._skipCurrentOrder = false;
                }
                catch (Exception ex)
                {
                    AllowSleep();
                    //there was an error
                   this.dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                        {
                            FlowMessageDialog msg = new FlowMessageDialog("Upload Error", "There was a problem uploading to the lab's ftp server.\nThe uploader will try again after you restart Flow.\n\nIf the problem keeps happening, please contact your lab.");
                    msg.CancelButtonVisible = false;
                    msg.ShowDialog();
                        }));
                    keepRunning = false;
                    this.FlowController.ProjectController.IsUploading = false;
                }
            }
        }

        private HttpStatusCode UploadImage(ImageUploadItem item, string uploadJob)
        {

            HttpWebRequest rq = (HttpWebRequest)WebRequest.Create(item.Url);

           
            foreach(ImageUploadHeader h in item.HttpHeaders)
            {
                rq.Headers.Add(h.header, h.value);
            }

            System.Drawing.Image bmp = new Bitmap(Path.Combine(uploadJob, item.ImageName));
            Stream ms = new MemoryStream();
            if (item.ImageName.ToLower().EndsWith("png"))
            {
                rq.ContentType = "image/png";
                bmp.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
            }
            else
            {
                rq.ContentType = "image/jpg";
                bmp.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
            }

            byte[] buffer = ReadToEnd(ms);

            rq.Timeout = ((60 * 5) * 1000);//try for 5 minutes (60 seconds * 5 minutes * 1000 = miliseconds)
            rq.Method = item.Method;

            if (item.Url.ToLower().Contains("imagequix"))
            {
                string md5hash;
                using (var md5 = MD5.Create())
                {
                    md5hash = BitConverter.ToString(md5.ComputeHash(buffer)).Replace("-", "").ToLower();
                }
                rq.Headers.Add("x-iq-etag", md5hash);
                rq.Credentials = new NetworkCredential(item.Username, item.Password);
                rq.ContentLength = ms.Length;
                Stream requestStream = rq.GetRequestStream();
                requestStream.Write(buffer, 0, (int)ms.Length);
                requestStream.Flush();
            }
            else
            {
                string md5hash;
                using (var md5 = MD5.Create())
                {
                    md5hash = Convert.ToBase64String(md5.ComputeHash(buffer));
                }

                rq.ContentType = "application/json";

                JsonObject PostDataJson = new JsonObject();
                PostDataJson.Add("ImageType", 1);
                PostDataJson.Add("GalleryGuid", item.GalleryGuid);
                PostDataJson.Add("SubjectPassword", item.SubjectPassword);
                PostDataJson.Add("OriginalImageFilename", item.ImageName);
                PostDataJson.Add("ThumbnailBase64", Convert.ToBase64String(buffer));
                PostDataJson.Add("ThumbnailMd5", md5hash);


                JsonWriter _writerPS = new JsonWriter();
                PostDataJson.Write(_writerPS);
                string postData = _writerPS.ToString().Replace("\\/", "/");

                rq.ContentLength = postData.Length;
                rq.Timeout = (120000);//try for 120 seconds

                StreamWriter stOut = new
                   StreamWriter(rq.GetRequestStream(),
                   System.Text.Encoding.ASCII);
                stOut.Write(postData);
                stOut.Close();

            }

            
            
            HttpWebResponse rs;
            try
            {
               rs = (HttpWebResponse)rq.GetResponse();
            }
            catch (Exception e)
            {

                throw e;
            }

            logger.Info("image upload response status: " + rs.StatusCode + " - " + rs.StatusDescription);
            //Stream s = rs.GetResponseStream();
            //NotificationInfo.Update(currentMessage + " reading response");
            StreamReader reader = new StreamReader(rs.GetResponseStream());
            string jsonText = reader.ReadToEnd();
            logger.Info("image upload response text: " + jsonText);

            //NotificationInfo.Update(currentMessage + " closing reader");
            reader.Close();
            bmp.Dispose();
            //NotificationInfo.Update(currentMessage + " done uploading");

            HttpStatusCode code = rs.StatusCode;
            rs.Close();
            return code;

        }

        public static byte[] ReadToEnd(System.IO.Stream stream)
        {
            long originalPosition = stream.Position;
            stream.Position = 0;

            try
            {
                byte[] readBuffer = new byte[4096];

                int totalBytesRead = 0;
                int bytesRead;

                while ((bytesRead = stream.Read(readBuffer, totalBytesRead, readBuffer.Length - totalBytesRead)) > 0)
                {
                    totalBytesRead += bytesRead;

                    if (totalBytesRead == readBuffer.Length)
                    {
                        int nextByte = stream.ReadByte();
                        if (nextByte != -1)
                        {
                            byte[] temp = new byte[readBuffer.Length * 2];
                            Buffer.BlockCopy(readBuffer, 0, temp, 0, readBuffer.Length);
                            Buffer.SetByte(temp, totalBytesRead, (byte)nextByte);
                            readBuffer = temp;
                            totalBytesRead++;
                        }
                    }
                }

                byte[] buffer = readBuffer;
                if (readBuffer.Length != totalBytesRead)
                {
                    buffer = new byte[totalBytesRead];
                    Buffer.BlockCopy(readBuffer, 0, buffer, 0, totalBytesRead);
                }
                return buffer;
            }
            finally
            {
                stream.Position = originalPosition;
            }
        }

        private string GetZipOrPaf(string uploadJob)
        {
            foreach (string file in Directory.GetFiles(uploadJob))
            {
                if (file.EndsWith(".zip") || file.EndsWith(".paf"))
                    return file;
            }
            return null;
        }

        void ft_SentBytesUpdated(object sender, EventArgs<long> e)
        {
            FileTransfer ft = sender as FileTransfer;

            if (this._skipCurrentOrder)
                ft.AbortTransfer();

            CurrentOrderUploadedFiles = e.Data;
        }

        private void UpdateQueue(string LabOrderQueueFolder)
        {
           

            foreach (string folder in Directory.GetDirectories(LabOrderQueueFolder))
            {   
                if(!UploadQueue.Contains(folder))
                    UploadQueue.Add(folder);
            }

            //foreach (string file in Directory.GetFiles(LabOrderQueueFolder))
            //{
            //    if (file.EndsWith(".zip") || file.EndsWith(".paf"))
            //        if (!UploadQueue.Contains(file))
            //            UploadQueue.Add(file);
            //}

        }



    }

    public class ImageUploadJob
    {
        public List<ImageUploadItem> images { get; set; }
        public ImageUploadJob()
        {
            images = new List<ImageUploadItem>();
        }
    }

    public class ImageUploadItem
    {
        public string Url { get; set; }
        public string ImageName { get; set; }
        public List<ImageUploadHeader> HttpHeaders { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Method { get; set; }

        public string GalleryGuid { get; set; }
        public string SubjectPassword { get; set; }
        public string ThumbnailMd5 { get; set; }

        public ImageUploadItem()
        {
            HttpHeaders = new List<ImageUploadHeader>();
        }
    }
    public class ImageUploadHeader
    {
        public string header { get; set; }
        public string value { get; set; }

        public ImageUploadHeader()
        {

        }

        public ImageUploadHeader(string h, string v)
        {
            header = h;
            value = v;
        }
    }
}
