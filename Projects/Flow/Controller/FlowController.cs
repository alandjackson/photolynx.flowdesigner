﻿using System;
using System.IO;
using System.Windows;

using Flow.Config;
using Flow.Controller.Catalog;
using Flow.Controller.Edit;
using Flow.Controller.Edit.ImageAdjustments;
using Flow.Controller.Preferences;
using Flow.Controller.Project;
using Flow.Designer;
using Flow.Designer.Controller;
using Flow.Designer.Lib.ProjectInterop;
using Flow.Helpers.FlowController;
using Flow.Lib;
using Flow.Lib.Activation;
using Flow.Lib.Helpers;
using Flow.Model.Settings;
using Flow.Schema.LinqModel.DataContext;
using Flow.View;
using Flow.View.Catalog;
using Flow.View.Edit;
using Flow.View.Edit.ImageAdjustments;
using Flow.View.Preferences;
using Flow.View.Reports;

//using Flow.Schema.LinqModel.FlowMaster;
using StructureMap;
using Flow.Controls.View.Dialogs;
using Flow.Helpers;
using System.Data;
using System.Xml;
using System.Collections.Generic;
using Flow.Lib.Net;
using Flow.Lib.Preferences;
using Flow.View.Dialogs;

using System.ComponentModel;
using System.Data.Linq;
using System.Linq;
using System.Threading;

using NLog;
using System.Windows.Threading;

namespace Flow.Controller
{
    public class FlowNotificationService : IShowsNotificationProgress
    {
        #region IShowsNotificationProgress Members

        public void ShowNotification(NotificationProgressInfo info)
        {
            ObjectFactory.GetInstance<FlowController>().ShowNotification(info);
        }
        public void UpdateNotification(NotificationProgressInfo info)
        {
            ObjectFactory.GetInstance<FlowController>().UpdateNotification(info);
        }


        #endregion
    }

    /// <summary>
    /// Controller for the entire flow application.
    /// </summary>
    public class FlowController : IShowsNotificationProgress
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

		public event EventHandler OpenOrderEntryInvoked = delegate { };

        public bool IsDayPass { get; set; }

        public bool HideNewFeatures { get; set; }

        public string ActivationKeyFile = Path.Combine(FlowContext.FlowCommonAppDataDirPath, "Activation.dat");

        public string FlowMasterFile = Path.Combine(FlowContext.FlowConfigDirPath, "FlowMaster.sdf");

		protected ConfigRepository ConfigRepository { get; set; }
		protected ProjectWorkspaceValidator WorkspaceValidator { get; set; }

		public ProjectController ProjectController { get; private set; }

        public ScanController ScanController { get; set; }

        private string _activationKey { get; set; }
        public string ActivationKey { 
            get{
                 return _activationKey;
            }
            set { _activationKey = value; }
        }

		//protected CaptureController _capture = null;
        public CaptureController CaptureController { get; private set; }

        public EditController EditController = null;

		protected DataImportController _dataImport = null;

		protected ImageAdjustmentsController _imageAdjustments = null;

		protected DesignerController _designer = null;

		protected FlowCatalogController _flowCatalog = null;

		protected ReportsController _reports = null;

		protected ImageQuixCatalogController _imageQuixCatalog = null;
		protected ProductPackageController _product = null;

        protected PreferencesController _preferences = null;

		public ApplicationPanel ApplicationPanel { get; set; }

        public FlowServer FlowServer { get; set; }
        public FlowServerCrawler FlowServerCrawler { get; set; }

        public Thumbnailer Thumbnailer { get; set; }

        public LabOrderUploader LabOrderUploader { get; set; }


        public Boolean ForceFlowMaster = false;
        public Boolean ForceFlowServer = false;
        public Boolean ForceChooseConfig = false;
        public Boolean ForcePUDOrders = false;
        public Boolean ImportImagesAutoAssign = false;

        public Boolean JustActivated = false;
		/// <summary>
		/// Constructor
		/// </summary>
		public FlowController(ConfigRepository cr)
        {
            if (File.Exists(Path.Combine(FlowContext.FlowCommonAppDataDirPath, "choose.txt")))
            {
                ForceChooseConfig = true;
            }
            if (File.Exists(Path.Combine(FlowContext.FlowCommonAppDataDirPath, "settings.txt")))
            {
                string line;
                System.IO.StreamReader file =
                new System.IO.StreamReader(Path.Combine(FlowContext.FlowCommonAppDataDirPath, "settings.txt"));
                while ((line = file.ReadLine()) != null)
                {
                    line = line.Trim().ToLower();
                    if (line.StartsWith("forcechooseconfig") && line.EndsWith("=1"))
                        ForceChooseConfig = true;
                    if (line.StartsWith("forceflowmaster") && line.EndsWith("=1"))
                        ForceFlowMaster = true;
                    if (line.StartsWith("forceflowserver") && line.EndsWith("=1"))
                        ForceFlowServer = true;
                    if (line.StartsWith("forcepudorder") && line.EndsWith("=1"))
                        ForcePUDOrders = true;
                    if (line.StartsWith("importimagesautoassign") && line.EndsWith("=1"))
                        ImportImagesAutoAssign = true;
                }

                file.Close();
            }

            logger.Info("------------------------------------------------------------");
            logger.Info("Updating FlowMaster and FlowProjectTemplate databases");

            string installedDB = Path.Combine(Path.Combine(FlowContext.FlowDbInstallDirPath, "New"), "FlowMaster.sdf");
            string installedProjectDB = Path.Combine(Path.Combine(FlowContext.FlowDbInstallDirPath, "New"), "FlowProjectTemplate.sdf");

            string destDB = Path.Combine(FlowContext.FlowDbInstallDirPath, "FlowMaster.sdf");
            string destProjectDB = Path.Combine(FlowContext.FlowDbInstallDirPath, "FlowProjectTemplate.sdf");
            
            if(File.Exists(installedDB))
            {
                if(File.Exists(destDB))
                    File.Delete(destDB);
                File.Move(installedDB, destDB);
            }
            if (File.Exists(installedProjectDB))
            {
                if (File.Exists(destProjectDB))
                    File.Delete(destProjectDB);
                File.Move(installedProjectDB, destProjectDB);
            }



            if(Directory.Exists(Path.Combine(FlowContext.FlowDbInstallDirPath, "New")))
                Directory.Delete(Path.Combine(FlowContext.FlowDbInstallDirPath, "New"), true);

            ConfigRepository = cr;
            //ConfigRepository = ObjectFactory.GetInstance<ConfigRepository>();
            WorkspaceValidator = new ProjectWorkspaceValidator();

            //lets the user choose which activation to use
            // if there is a choose.txt File in the folder
            if (ForceChooseConfig)
            {
                List<string> keys = new List<string>();
                foreach (string key in Directory.GetFiles(FlowContext.FlowCommonAppDataDirPath))
                {
                    string fileName = new FileInfo(key).Name;
                    if (fileName.ToLower().StartsWith("activation") && fileName.ToLower().EndsWith(".dat"))
                        keys.Add(fileName);
                }
                if (keys.Count == 1)
                {
                    ActivationKeyFile = Path.Combine(FlowContext.FlowCommonAppDataDirPath, keys[0]);

                }

                else if (keys.Count > 1)
                {
                    SelectActivationDialog dlg = new SelectActivationDialog(keys);
                    dlg.ShowDialog();
                    ActivationKeyFile = Path.Combine(FlowContext.FlowCommonAppDataDirPath, dlg.selectedFileName);
                }
                string uniqueText = "";

                if ((new FileInfo(ActivationKeyFile)).Name.Contains("ctivation_"))
                    uniqueText = (new FileInfo(ActivationKeyFile)).Name.Split("ctivation_")[1].Replace(".dat", "");

                if (uniqueText.Length > 0)
                {
                    string tmpFM = Path.Combine(FlowContext.FlowConfigDirPath, "FlowMaster_" + uniqueText + ".sdf");
                    if (File.Exists(tmpFM))
                    {
                        FlowMasterFile = tmpFM;

                    }
                }
            }
            AppDomain.CurrentDomain.SetData("PathToFlowMaster", FlowMasterFile);
            
			ProjectController = new ProjectController(this);

           

			ProjectController.OpenCaptureInvoked += new EventHandler(ProjectController_OpenCaptureInvoked);
		}

        public void ResetControllers(){

            CaptureController = null;
            EditController = null;
		    _dataImport = null;
		    _imageAdjustments = null;
		    _designer = null;
		    _flowCatalog = null;
		    _reports = null;
		    _imageQuixCatalog = null;
		    _product = null;
             _preferences = null;

        }
        public void ShowNotification(NotificationProgressInfo info)
        {
            ApplicationPanel.ShowNotification(info);
        }

        public void UpdateNotification(NotificationProgressInfo info)
        {
            ApplicationPanel.UpdateNotification(info);
        }


        public static readonly bool ActivationEnabled = true;

        /// <summary>
        /// Main attaches the controller to the application panel
        /// </summary>
        /// <param name="pnl"></param>
        public bool Main(ApplicationPanel pnl)
        {

            this.ApplicationPanel = pnl;
            this.ApplicationPanel.FlowController = this;

            if (ActivationEnabled && !CheckActivation())
            {
                Application.Current.Shutdown();
                Window.GetWindow(pnl).Close();
                return false;
            }

            ActivationValidator activation = new ActivationValidator(this.ActivationKeyFile);
            ActivationStore activationStore = activation.Repository.Load();

            this.ActivationKey = activationStore.ActivationKey;

           


            

            this.ScanController = new ScanController();
            ScanController.FlowController = this;

            // make sure the workspace folder is valid and exists
            ApplicationConfig appConfig = ConfigRepository.Load<ApplicationConfig>();

            if (!Directory.Exists(appConfig.ProjectsWorkspace))
            {
				//string appData =
				//    Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);

				//appConfig.ProjectsWorkspace = 
				//    IOUtil.CombinePaths(appData, "Flow", "Projects");

                appConfig.ProjectsWorkspace = this.ProjectController.FlowMasterDataContext.PreferenceManager.Application.ProjectsDirectory;

				if (!Directory.Exists(appConfig.ProjectsWorkspace))
                    Directory.CreateDirectory(appConfig.ProjectsWorkspace);

				ConfigRepository.Save<ApplicationConfig>(appConfig);
            }

            if (this.ProjectController.FlowMasterDataContext.PreferenceManager.Capture.HotFolderDirectory != null &&
                !Directory.Exists(this.ProjectController.FlowMasterDataContext.PreferenceManager.Capture.HotFolderDirectory))
            {
                Directory.CreateDirectory(this.ProjectController.FlowMasterDataContext.PreferenceManager.Capture.HotFolderDirectory);
            }



            FlowContext.ShowNewFeatures = true;

            //if (_activationKey.ToUpper() == "J52SX89QF3" || _activationKey.ToUpper() == "K39AQ86TW2" || activationStore.CustomerName.Contains("LTPD"))
            //    FlowContext.ShowNewFeatures = true;

            //Load customSettings File
            ProjectController.LoadCustomSettings();

            //if (!WorkspaceValidator.Validate(appConfig.ProjectsWorkspace))
            //{
            //    appConfig.ProjectsWorkspace = WorkspaceValidator.CreateNewWorkspace();
            //    ConfigRepository.Save<ApplicationConfig>(appConfig);
            //}


            FlowServer = new FlowServer(this, this.ApplicationPanel.Dispatcher);
            FlowServerCrawler = new FlowServerCrawler(this, this.ApplicationPanel.Dispatcher);
            if (this.ProjectController.FlowMasterDataContext.PreferenceManager.Application.IsFlowServer)
            {
                //this.ProjectController.FlowServerIsOn = false;
                FlowServerCrawler.Begin();
                FlowServer.Begin();

            }

            Thumbnailer = new Thumbnailer(this);
            if (this.ProjectController.FlowMasterDataContext.PreferenceManager.Application.CreateThumbnails && this.ProjectController.FlowMasterDataContext.PreferenceManager.Application.IsFlowServer)
            {
                Thumbnailer.Begin();
            }

            LabOrderUploader = new LabOrderUploader(this, this.ApplicationPanel.Dispatcher);

            if (ForcePUDOrders)
                this.ProjectController.FlowMasterDataContext.PreferenceManager.Application.ForcePUDOrders = true;
            else
                this.ProjectController.FlowMasterDataContext.PreferenceManager.Application.ForcePUDOrders = false;



            


            return true;
        }

        private bool uploaderstarted = false;
        public void StartUploader()
        {
            if (uploaderstarted == false && !this.ProjectController.FlowMasterDataContext.PreferenceManager.Application.IsFlowServer)
            {
                
                LabOrderUploader.Begin();
            }
            uploaderstarted = true;
        }
       



        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
		public bool CheckActivation()
        {
            logger.Info("Checking activation");

            IsDayPass = false;

            #if DEBUG
            // return true;
            #endif

            // Check to see if we can generate a machine ID
            bool doFingerPrint = false;
            MachineInfo mi = new MachineInfo();
            try
            {
                mi.GenerateMachineId();
            }
            catch (NullReferenceException e)
            {
                try
                {
                    string fingerPrint = FingerPrint.Value();
                    doFingerPrint = true;
                }
                catch
                {
                    string message = "Because of performance limitaions regarding hardware acceleration, Flow doesn't support running within a Virtual Machine. Contact Support for more details.";
                    logger.Fatal(message);
                    System.Windows.MessageBox.Show(message);
                    return false;
                }
            }

            
            


            //if its activated, return true
            ActivationValidator activation = new ActivationValidator(ActivationKeyFile);
            if (activation.FeatureIsEnabled("Flow 1 Beta"))
            {
                DateTime activationEnd = activation.Repository.Load().ActivatedEnd;
                TimeSpan timeRemaining = (activationEnd - DateTime.Now);
                int daysRemaining = (int)Math.Round(timeRemaining.TotalDays);

                // If this activation expires in 30 days or less
                if (daysRemaining <= 30)
                {
                    // Remind the client how many days are left
                    FlowMessageBox msgReminder = new FlowMessageBox("Internet Activation - Reminder", String.Format("Your activation expires on {0}.\n\nYou have approximately {1} {2} remaining.", activationEnd, daysRemaining, (daysRemaining == 1 ? "day" : "days")));
                    msgReminder.CancelButtonVisible = false;
                    msgReminder.ShowDialog();
                }

                return true;
            }

           
            //not activated, so lets ask for activation key
            this.ActivationKey = activation.Repository.Load().ActivationKey;
            ActivationDialog activationDialog = new ActivationDialog();
            activationDialog.DataContext = this;
            Window w = UIUtil.WrapInWindow(activationDialog);
            w.Title = "Flow Activation";
            if (w.ShowDialog() != true)
                return false;

            if (this.ActivationKey == null)
            {
                FlowMessageBox msgTryAgain = new FlowMessageBox("Internet Activation - Failed", "The activation key was left blank. Please supply an activation key and try again.");
                msgTryAgain.CancelButtonVisible = false;
                msgTryAgain.ShowDialog();
                return CheckActivation();
            }

            //is the ActivationKey a Day Pass?
            if (this.ActivationKey.Trim() == activation.DayPassValue().Trim())
            {
                activation.GrantActivation("Flow 1 Beta", 1);
                IsDayPass = true;
                FlowMessageBox msgb = new FlowMessageBox("Internet Activation - Success", "1 Day Activation is Successful");
                msgb.CancelButtonVisible = false;
                if (msgb.ShowDialog() == false)
                    return false;
                JustActivated = true;
                //MessageBox.Show("1 Day Activation is Successful", "Success");
                return true;
            }

            //is the ActivationKey a 14 Multi Day Pass?
            //if (this.ActivationKey.Trim() == activation.MultiDayPassValue(14).Trim())
            //{
            //    activation.GrantActivation("Flow 1 Beta", 14);
            //    MessageBox.Show("14 Day Activation is Successful", "Success");
            //    return true;
            //}

            //this is not a day pass, so lets activate over the internet...
            //if (MessageBox.Show("Flow Will Activate Automatically Over " +
            //    "The Internet?", "Internet Activation",
            //    MessageBoxButton.OKCancel) == MessageBoxResult.Cancel)
            //    return false;

            FlowMessageBox msg = new FlowMessageBox("Internet Activation", "Flow Will Activate Automatically Over The Internet");
            if (msg.ShowDialog() == false)
                return false;


            WebServiceActivator activator = new WebServiceActivator();
            ActivationStore store = activation.Repository.Load();
            if(doFingerPrint)
                store.ComputerId = FingerPrint.Value();
            else
                store.ComputerId = activation.MachineInfo.GenerateMachineId();

            store.FingerPrint = FingerPrint.Value();
            store.ActivationKey = this.ActivationKey.Trim();
            store.ComputerName = System.Environment.MachineName;

            try
            {
                activation.Repository.Save(activator.Activate(store));
            }
            catch (System.Net.WebException e)
            {
                FlowMessageBox msgFailure = new FlowMessageBox("Internet Activation - Failed", "Activation failed because there is no active internet connection.");
                msgFailure.CancelButtonVisible = false;
                msgFailure.ShowDialog();
                logger.ErrorException("Activation failed because there is no active internet connection.", e);
                return false;
            }

            store = activation.Repository.Load();
            DataSet ds = store.ConfigData;
            if (ds != null)
            {
                this.ProjectController.FlowMasterDataContext.PreferenceManager.Load(ds);
                this.ProjectController.FlowMasterDataContext.SavePreferences();
            }

            //DataSet dsOrderForm = store.OrderFormData;
            //if (dsOrderForm != null)
            //{
            //    LoadOrderFormFields(dsOrderForm);
            //}

            if (store.CustomerId != null && store.CustomerName != null)
            {
                FlowObservableCollection<Organization> OrgList = this.ProjectController.FlowMasterDataContext.OrgList;
                Boolean isNew = true;
                foreach (Organization org in OrgList)
                {
                    if (org.LabCustomerID == store.CustomerId)
                        isNew = false;
                }
                if (isNew)
                {
                    OrgList.CreateNew();
                    OrgList.CurrentItem.OrganizationName = store.CustomerName;
                    OrgList.CurrentItem.LabCustomerID = store.CustomerId;
                    OrgList.CurrentItem.OrganizationTypeID = 4;

                    if (store.CustomerEmail != null)
                        OrgList.CurrentItem.OrganizationContactEmail = store.CustomerEmail;
                    if (store.Address != null)
                        OrgList.CurrentItem.OrganizationAddressLine1 = store.Address;
                    if (store.City != null)
                        OrgList.CurrentItem.OrganizationCity = store.City;
                    if (store.State != null && store.State.Length >= 2)
                        OrgList.CurrentItem.OrganizationStateCode = store.State.Substring(0, 2);
                    if (store.Zip != null)
                        OrgList.CurrentItem.OrganizationZipCode = store.Zip;
                    if (store.ShippingAddress != null)
                        OrgList.CurrentItem.OrganizationShippingAddressLine1 = store.ShippingAddress;
                    if (store.ShippingCity != null)
                        OrgList.CurrentItem.OrganizationShippingCity = store.ShippingCity;
                    if (store.ShippingState != null && store.ShippingState.Length >= 2)
                        OrgList.CurrentItem.OrganizationShippingStateCode = store.ShippingState.Substring(0,2);
                    if (store.ShippingZip != null)
                        OrgList.CurrentItem.OrganizationShippingZipCode = store.ShippingZip;
                    if (store.ContactName != null)
                        OrgList.CurrentItem.OrganizationContactName = store.ContactName;
                    if (store.CustomerPhone != null)
                        OrgList.CurrentItem.OrganizationContactPhone = store.CustomerPhone;
                   
                    OrgList.CurrentItem.OrganizationGuid = Guid.NewGuid();
                    OrgList.CurrentItem.DateCreated = DateTime.Now;
                    OrgList.CurrentItem.DateModified = DateTime.Now;
                    OrgList.CommitNew();

                    this.ProjectController.FlowMasterDataContext.SubmitChanges();
                    this.ProjectController.FlowMasterDataContext.RefreshOrgList();
                }
            }

            if (activation.FeatureIsEnabled("Flow 1 Beta"))
            {

                FlowMessageBox msgb = new FlowMessageBox("Internet Activation - Success", "Activation Successful\n\nThis Activation Expires on " + (store).ActivatedEnd.ToShortDateString());
                msgb.CancelButtonVisible = false;
                if (msgb.ShowDialog() == false)
                    return false;
                JustActivated = true;
                return true;
            }

            FlowMessageBox msgF = new FlowMessageBox("Internet Activation - Failed", "Activation Failed, please call support for assistance");
            msgF.CancelButtonVisible = false;
            msgF.ShowDialog();
            return false;
        }

        public void LoadOrderFormFields(List<DataSet> orderForms)
        {
            this.ProjectController.FlowMasterDataContext.OrderFormFields.DeleteAllOnSubmit(this.ProjectController.FlowMasterDataContext.OrderFormFields);
            this.ProjectController.FlowMasterDataContext.SubmitChanges();

            foreach (DataSet ds in orderForms)
            {
                DataTable dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    //IEnumerable<OrderFormField> fields = this.ProjectController.FlowMasterDataContext.OrderFormFieldList.Where(o => o.FieldID == Convert.ToInt32(dr["FieldId"]));
                    //OrderFormField field;
                    //if (fields.Count() > 0)
                    //{
                    //    field = fields.First();
                    //}
                    //else
                    //{
                    OrderFormField field = new OrderFormField();
                    this.ProjectController.FlowMasterDataContext.OrderFormFields.InsertOnSubmit(field);
                    //}

                    field.FieldID = ConvertDbValueToInt32(dr["FieldId"]);
                    field.ConfigFileID = ConvertDbValueToInt32(dr["ConfigFileId"]);
                    field.FieldName = ConvertDbValueToString(dr["FieldName"]);
                    field.FieldType = ConvertDbValueToInt32(dr["FieldType"]);
                    field.FieldValue = ConvertDbValueToString(dr["FieldValue"]);
                    field.IsRequired = ConvertDbValueToBoolean(dr["IsRequired"]);
                    field.SortOrder = ConvertDbValueToInt32(dr["SortOrder"]);
                    field.ProjectTemplateID = ConvertDbValueToInt32(dr["ProjectTemplateID"]);
                    field.ProjectTemplateName = ConvertDbValueToString(dr["ProjectTemplateName"]);
                }
                this.ProjectController.FlowMasterDataContext.SubmitChanges();
            }
        }

        int ConvertDbValueToInt32(object value)
        {
            return value == DBNull.Value ? 0 : Convert.ToInt32(value);
        }

        string ConvertDbValueToString(object value)
        {
            return value == DBNull.Value ? "" : Convert.ToString(value);
        }

        bool ConvertDbValueToBoolean(object value)
        {
            return value == DBNull.Value ? false : Convert.ToBoolean(value);
        }


        /// <summary>
        ///  Displays a list of available Flow projects
        /// </summary>
        public void Project()
        {
            ApplicationPanel.ShowMainProjectPanel();
			this.ApplicationPanel.FramingDockPanel.DataContext = this.ProjectController;
        }

        public void FlowServerPanel()
        {
            ApplicationPanel.ShowFlowServerPanel();
            
        }
        /// <summary>
        /// Shows the capture view
        /// </summary>
        public void Capture()
        {
            CaptureDockPanel pnl = ApplicationPanel.ShowCaptureDockPanel();

            // build the capture controller on demand
            if (CaptureController == null)
            {
                CaptureController = ObjectFactory.GetInstance<CaptureController>();
                CaptureController.Notifier = ApplicationPanel;
                CaptureController.Main(pnl, this);
			}

            CaptureController.FlowController = this;
            this.ProjectController.CurrentFlowProject.RefreshPropertyBinding("SubjectList");
            pnl.Focus();

            var subjList = ProjectController.CurrentFlowProject.SubjectList;
            if (subjList != null && subjList.Count > 0 && subjList.CurrentItem != null)
            {
                foreach (SubjectImage si in subjList.CurrentItem.ImageList)
                {
                    if (si.Subject.FlowProjectDataContext == null)
                        si.Subject.FlowProjectDataContext = this.ProjectController.CurrentFlowProject.FlowProjectDataContext;

                    if (si.ImagePath != null && File.Exists(si.ImagePath))
                        si.RefreshImage();
                }
            }

            if (this.ProjectController.CurrentFlowProject.GetEventTriggerSetting(3))
            {
                //autoprint
                this.CaptureController.LayoutPreviewController.TurnOnAutoPrint();
               
            }

            if(this.CaptureController.SubjectRecordController.CurrentSubject != null)
                this.CaptureController.SubjectRecordController.CurrentSubject.RefreshImageList();
		}

		void ProjectController_OpenCaptureInvoked(object sender, EventArgs e)
		{
			this.Capture();
			//this.OpenOrderEntryInvoked(sender, e);
		}

        /// <summary>
        /// Shows the edit view
        /// </summary>
        public void Edit()
        {
			EditDockPanel pnl = ApplicationPanel.ShowEditDockPanel();
            
            if (EditController == null)
			{
                EditController = ObjectFactory.GetInstance<EditController>();
                EditController.Main(pnl, this);
			}
            this.ProjectController.CurrentFlowProject.SubjectList.MoveCurrentToNext();
            this.ProjectController.CurrentFlowProject.SubjectList.MoveCurrentToPrevious();
		}

		/// <summary>
		/// Shows the data import view
		/// </summary>
		public void DataImport()
		{
			DataImportPanel pnl = ApplicationPanel.ShowDataImportPanel();

			if (_dataImport == null)
			{
				_dataImport = ObjectFactory.GetInstance<DataImportController>();
				_dataImport.Main(pnl, this);
			}
		}

        /// <summary>
        /// 
        /// </summary>
		private void InitializeImageAdjustments()
        {
            logger.Info("Initializing Image Adjustments");

            ImageAdjustmentsPanel pnl = ApplicationPanel.ShowImageAdjustmentsPanel();

            if (_imageAdjustments == null)
            {
                _imageAdjustments = ObjectFactory.GetInstance<ImageAdjustmentsController>();
                _imageAdjustments.Main(pnl, this);
                _imageAdjustments.FlowController = this;
                
            }

            _imageAdjustments.ToolController.CropToolPanel.CropStackPanel.IsEnabled = this.ProjectController.FlowMasterDataContext.PreferenceManager.Permissions.CanCropImages;
           

            ApplicationPanel.uxEdit.IsEnabled = true;
        }

        /// <summary>
        /// 
        /// </summary>
		public void ImageAdjustmentsCurrentView()
        {
            InitializeImageAdjustments();

            int position = this.ProjectController.CurrentFlowProject.SubjectList.View.CurrentPosition;
            //this.ProjectController.CurrentFlowProject.SubjectList.ApplyTemporaryFilter();

            List<SubjectImage> subjectImagesList = new List<SubjectImage>();
            //if (this.ProjectController.CurrentFlowProject.SubjectList.IsFiltered)
           // {

            BackgroundWorker wrker = new BackgroundWorker();
            wrker.DoWork += delegate
            {
                if (this.ProjectController.CurrentFlowProject.SubjectList.IsFiltered == false && this.ProjectController.CurrentFlowProject.SubjectList.IsSorted == false)
                {
                    subjectImagesList = new List<SubjectImage>(this.ProjectController.CurrentFlowProject.ImageList);
                }
                else
                {
                    subjectImagesList = new List<SubjectImage>();

                    while (this.ProjectController.CurrentFlowProject.initingSubjects)
                        Thread.Sleep(1000);
                    int cnt = 0;
                    foreach (Subject s in this.ProjectController.CurrentFlowProject.SubjectList.View)
                    {
                        foreach (SubjectImage si in s.ImageList.Where(i => i.IsSuppressed != true))
                        {
                            subjectImagesList.Add(si);
                            //this.EditController.
                            //cnt++;
                            //if (cnt  == 9999999)
                            //{
                            //    IEnumerable<SubjectImage> nonGroupImagesList = subjectImagesList.Where(i => !i.IsGroupImage);
                            //    IEnumerable<SubjectImage> groupImagesList = subjectImagesList.Where(i => i.IsGroupImage).GroupBy(i => i.GroupImageGuid).Select(g => g.First());
                            //    FlowObservableCollection<SubjectImage> adjustImagesList = new FlowObservableCollection<SubjectImage>(nonGroupImagesList.Concat(groupImagesList));

                            //    this.ProjectController.CurrentFlowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                            //    {
                            //        _imageAdjustments.ImageListContoller.ProjectImageList = adjustImagesList;
                            //    }));
                            

                            //    try
                            //    {
                            //        this.ProjectController.CurrentFlowProject.SubjectList.View.MoveCurrentToPosition(position);
                            //    }
                            //    catch (ArgumentOutOfRangeException e)
                            //    {
                            //        // The remembered position has somehow become out of range
                            //        // Move to the first position as a fallback
                            //        this.ProjectController.CurrentFlowProject.SubjectList.View.MoveCurrentToFirst();
                            //    }
                            //    this.ProjectController.CurrentFlowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                            //    {

                            //        _imageAdjustments.ImageListContoller.ProjectImageList = adjustImagesList;
                            //        _imageAdjustments.ImageListContoller.ImageListPanel.ImageListbox.SelectedIndex = 0;

                            //        _imageAdjustments.ToolController.ShowMask();
                            //    }));
                            //}
                        }
                    }
                }
            };
            
            wrker.RunWorkerCompleted += delegate
            {
                IEnumerable<SubjectImage> nonGroupImagesList = subjectImagesList.Where(i => !i.IsGroupImage);
                IEnumerable<SubjectImage> groupImagesList = subjectImagesList.Where(i => i.IsGroupImage).GroupBy(i => i.GroupImageGuid).Select(g => g.First());
                FlowObservableCollection<SubjectImage> adjustImagesList = new FlowObservableCollection<SubjectImage>(nonGroupImagesList.Concat(groupImagesList));

                this.ProjectController.CurrentFlowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                {
                    _imageAdjustments.ImageListContoller.ProjectImageList = adjustImagesList;
                }));

                try
                {
                    this.ProjectController.CurrentFlowProject.SubjectList.View.MoveCurrentToPosition(position);
                }
                catch (ArgumentOutOfRangeException e)
                {
                    // The remembered position has somehow become out of range
                    // Move to the first position as a fallback
                    this.ProjectController.CurrentFlowProject.SubjectList.View.MoveCurrentToFirst();
                }


                this.ProjectController.CurrentFlowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                {
                    _imageAdjustments.ImageListContoller.ProjectImageList = adjustImagesList;
                    _imageAdjustments.ImageListContoller.ImageListPanel.ImageListbox.SelectedIndex = 0;

                    _imageAdjustments.ToolController.ShowMask();
                }));
               
            };

            wrker.RunWorkerAsync();
           // } else
           //     subjectImagesList = new List<SubjectImage>(this.ProjectController.CurrentFlowProject.ImageList);


           

            //this.ProjectController.CurrentFlowProject.SubjectList.RestoreFilter();

            //try
            //{
            //    this.ProjectController.CurrentFlowProject.SubjectList.View.MoveCurrentToPosition(position);
            //}
            //catch (ArgumentOutOfRangeException e)
            //{
            //    // The remembered position has somehow become out of range
            //    // Move to the first position as a fallback
            //    this.ProjectController.CurrentFlowProject.SubjectList.View.MoveCurrentToFirst();
            //}

            //_imageAdjustments.ImageListContoller.ProjectImageList = adjustImagesList;
            //_imageAdjustments.ImageListContoller.ImageListPanel.ImageListbox.SelectedIndex = 0;

            //_imageAdjustments.ToolController.ShowMask();
        }


		/// <summary>
		/// Shoes the image adjustments view
		/// </summary>
		public void ImageAdjustmentsCurrentRecord(SubjectImage selectedImage)
		{
            if (selectedImage.CropH < 0 || double.IsNaN(selectedImage.CropH))
                selectedImage.CropH = 0;

            InitializeImageAdjustments();

            _imageAdjustments.ImageListContoller.ProjectImageList = this.ProjectController.CurrentFlowProject.SubjectList.CurrentItem.ImageList;
            
            _imageAdjustments.ImageListContoller.ImageListPanel.ImageListbox.SelectedIndex = _imageAdjustments.ImageListContoller.ProjectImageList.IndexOf(selectedImage);

            _imageAdjustments.ToolController.ShowMask();
        }


        /// <summary>
        /// Shows the layout view
        /// </summary>
        public LayoutPanel layoutPanel = null;
        private bool layoutPanelExists = false;
        public void Layout()
        {
            layoutPanel = ApplicationPanel.ShowLayoutPanel();
            if(layoutPanelExists == false)
                layoutPanel.IsVisibleChanged += new DependencyPropertyChangedEventHandler(pnl_IsVisibleChanged);
            layoutPanelExists = true;

            if (_designer == null)
            {
                _designer = ObjectFactory.GetInstance<DesignerController>();
                
            }
            _designer.Main(ProjectController.CurrentFlowProject, ProjectController.FlowMasterDataContext, layoutPanel);
            _designer.CanCreateEditLayouts = ProjectController.FlowMasterDataContext.PreferenceManager.Permissions.CanCreateEditLayouts;

            _designer.Activate();
            
        }

        void pnl_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue == false && (bool)e.OldValue == true)
                _designer.Deactivate();
        }

		/// <summary>
		/// 
		/// </summary>
		public FlowCatalogController Catalog()
		{
			CatalogDockPanel pnl = ApplicationPanel.ShowFlowCatalogEditor();

			if (_flowCatalog == null)
			{
				_flowCatalog = ObjectFactory.GetInstance<FlowCatalogController>();
				_flowCatalog.Main(this, pnl);
			}
            return _flowCatalog;
		}

		/// <summary>
		/// 
		/// </summary>
		public void Reports()
		{
			ReportsDockPanel pnl = ApplicationPanel.ShowReports();

			if (_reports == null)
			{
				_reports = ObjectFactory.GetInstance<ReportsController>();
				_reports.Main(this, pnl);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public void ImageQuixCatalog()
		{
			ImageQuixCatalogPanel pnl = ApplicationPanel.ShowImageQuixCatalogPanel();
			if (_imageQuixCatalog == null)
			{
				_imageQuixCatalog = ObjectFactory.GetInstance<ImageQuixCatalogController>();
				_imageQuixCatalog.Main(this,pnl);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public void Product()
		{
			ProductPackageEditor pnl = ApplicationPanel.ShowProductPackageEditor();

			if (_product == null)
			{
				_product = ObjectFactory.GetInstance<ProductPackageController>();
				_product.Main(this, pnl);
			}
		}

        /// <summary>
        /// Shows the test view (for development tests)
        /// </summary>
        public void Test()
        {
            ApplicationPanel.ShowTestPanel();
        }

        /// <summary>
        /// Shows the preferences view
        /// </summary>
        //public void Preferences()
        //{
        //    ApplicationPanel.ShowPreferencesPanel();
        //}
        public void Preferences()
        {
            PreferenceSectionsPanel pnl = ApplicationPanel.ShowPreferencesPanel();

            // build the capture controller on demand
            if (_preferences == null)
            {
                _preferences = ObjectFactory.GetInstance<PreferencesController>();
                _preferences.Notifier = ApplicationPanel;
                _preferences.Main(pnl, this);
            }

            _preferences.FlowController = this;
        }

        public DateTime FlowStartTime { get; set; }
        public string FlowSessionID { get; set; }

        public int ErrorCount { get; set; }

        public bool PullingOrders = false;



        internal void UpdateDatabase(string p)
        {
            
        }
    }
}
