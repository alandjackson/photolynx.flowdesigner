﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.Controller.Project;
using Flow.Lib.AsyncWorker;
using Flow.Lib;
using System.ComponentModel;
using Flow.Schema.LinqModel.DataContext;
using System.IO;
using System.Windows.Threading;
using System.Threading;
using StructureMap;
using Flow.Schema;
using NLog;
using Flow.Lib.Helpers;
using Flow.View.Dialogs;

namespace Flow.Controller
{
    public class FlowServer
    {
        FlowController FlowController { get; set; }
        
        FlowBackgroundWorker bWorker;

        private static Logger logger = LogManager.GetCurrentClassLogger();
        
        bool keepRunning { get; set; }

        private Dispatcher dispatcher { get; set; }


        public FlowServer(FlowController flowController, Dispatcher disp)
        {
            this.FlowController = flowController;
            this.dispatcher = disp;
        }

        public void Begin()
        {
            //return;
            //start a thread that will be looking for new incoming projects to merge
            keepRunning = true;
            bWorker = new FlowBackgroundWorker();
            bWorker = FlowBackgroundWorkerManager.RunWorker(ServerEngine, ServerEngine_Stopped);


            //Thread t = new Thread(delegate()
            //{
            //    StartServerEngine();
            //});
            //t.SetApartmentState(ApartmentState.STA);
            //t.Start();


        }

        void ServerEngine_Stopped(object sender, RunWorkerCompletedEventArgs e)
        {
          
        }

        void ServerEngine(object sender, DoWorkEventArgs e)
        {
            try
            {
                StartServerEngine();
            }
            catch (Exception ex)
            {
                logger.Error("Error in FlowServer: " + ex.Message);
                throw ex;
            }
        }

        void StartServerEngine()
        {
            string pafIncomingFolder = Path.Combine(this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.ProjectsDirectory, "incoming");

            bool firstIteration = false;
            FlowProject IterationFlowProject = null;
            bool waitForLoad = true;
            bool errorsFound = false;

            this.FlowController.ProjectController.PropertyChanged += new PropertyChangedEventHandler(
                                      delegate(object sender2, PropertyChangedEventArgs e2)
                                      {
                                          if (e2.PropertyName == "MergeComplete")
                                          {
                                              if (IterationFlowProject.FlowProjectDataContext == null)
                                                  IterationFlowProject.Connect(this.FlowController.ProjectController.FlowMasterDataContext);
                                              DbVersionManager.SynchFlowProject(IterationFlowProject);
                                              IterationFlowProject.PrepareForExport();
                                              waitForLoad = false;
                                              logger.Info("Flow Server: Merge Complete for {0}", IterationFlowProject.FlowProjectName);
                                          }

                                          if (e2.PropertyName == "ProjectImportComplete")
                                          {
                                              waitForLoad = false;
                                          }
                                      }
                                      );

            Thread.Sleep(5000);
            string dots = "";
            bool skipAll = true;
            while (keepRunning)
            {

                while (!this.FlowController.ProjectController.FlowServerIsOn)
                {
                    this.FlowController.ProjectController.FlowServerStatus = "Server is Off";
                    firstIteration = true;
                    Thread.Sleep(3000);
                }

                    if (firstIteration && this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.ServerPorjectsNeedInitialized == true)
                    {

                        NotificationProgressInfo NotificationInfo = new NotificationProgressInfo("Initializing Projects", "Please Wait while we initialize the server projects", 0, this.FlowController.ProjectController.FlowMasterDataContext.FlowProjectList.Count, 1);
                        IShowsNotificationProgress progressNotification = ObjectFactory.GetInstance<IShowsNotificationProgress>();
                        NotificationInfo.NotificationProgress = progressNotification;
                        progressNotification.ShowNotification(NotificationInfo);

                        firstIteration = false;
                        foreach (FlowProject fp in this.FlowController.ProjectController.FlowMasterDataContext.FlowProjectList)
                        {
                            if (!this.FlowController.ProjectController.FlowServerIsOn)
                                break;
                            this.FlowController.ProjectController.FlowServerStatus = "Initializing projects: " + fp.FlowProjectName;
                            NotificationInfo.Step();

                            waitForLoad = true;

                            //this.dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                            //{

                                this.FlowController.ProjectController.PrepareServerProject(fp);

                                //fp.FlowProjectDataContext.SubjectDataLoaded += delegate
                                //{
                                //    fp.PrepareForExport();
                                //    fp.FlowProjectDataContext.SubmitChanges();
                                //    waitForLoad = false;
                                //};
                            //}));

                            //while (waitForLoad)
                            //{
                            //    Thread.Sleep(1000);

                            //}
                            Thread.Sleep(1000);
                        }

                        NotificationInfo.Complete("Done Initializing Projects");
                        this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.ServerPorjectsNeedInitialized = false;
                        this.FlowController.ProjectController.FlowMasterDataContext.SavePreferences();
                    }


                    firstIteration = false;

                    Thread.Sleep(1000);

                    this.FlowController.ProjectController.FlowServerStatus = "Waiting for projects to merge " + (dots += ".");
                    if (dots == "..........................")
                        dots = "";

                    UpdateLockedFileStatus();
                    //foreach (FlowProject fp in this.FlowController.ProjectController.FlowMasterDataContext.FlowProjectList)
                    while (this.FlowController.ProjectController.IncomingClientProjects.Where(p => p.IsLocked == false).Count() > 0)
                    {
                        IncomingProjectFile pFile = this.FlowController.ProjectController.IncomingClientProjects.FirstOrDefault(p => p.IsLocked == false);
                        if (pFile == null) continue;

                        this.FlowController.FlowServerCrawler.Stop();
                        if (File.Exists(pFile.FilePath) && IOUtil.IsFileInUse(pFile.FilePath))
                        {
                            pFile.IsLocked = true;
                            logger.Info("Incoming server file is in use, skipping for now: " + pFile.FilePath);
                            Thread.Sleep(1000);
                            continue;
                        }


                        UpdateLockedFileStatus();


                        if (pFile.FileType == ".sdf")
                        {
                            FlowProject fp = this.FlowController.ProjectController.FlowMasterDataContext.FlowProjectList.FirstOrDefault(p => p.FlowProjectGuid == pFile.ProjectGuid);

                            if (!this.FlowController.ProjectController.FlowServerIsOn)
                                break;

                            // this.FlowController.ProjectController.FlowServerStatus = "Scanning for projects to merge " + (dots += ".");

                            IterationFlowProject = fp;
                            fp.FlowMasterDataContext = this.FlowController.ProjectController.FlowMasterDataContext;

                            string incomingFolder = Path.Combine(fp.DirPath, "incoming");
                            string inProgressFolder = Path.Combine(fp.DirPath, "inProgress");
                            string qcFolder = Path.Combine(fp.DirPath, "qc");

                            if (!Directory.Exists(inProgressFolder))
                                Directory.CreateDirectory(inProgressFolder);

                            string inProgressFile = Path.Combine(incomingFolder, "inProgress.txt");
                            if (File.Exists(pFile.FilePath))
                            {

                                if (!this.FlowController.ProjectController.FlowServerIsOn)
                                    break;
                                this.FlowController.ProjectController.FlowServerStatus = "Merging project (" + fp.FlowProjectName + ")";

                                string tempDbCopy = Path.Combine(inProgressFolder, (new FileInfo(pFile.FilePath)).Name);
                                if (File.Exists(tempDbCopy))
                                    File.Move(tempDbCopy, tempDbCopy.Replace(".sdf", "_" + TicketGenerator.GenerateTicketString(4) + ".sdf"));
                                errorsFound = false;
                                waitForLoad = true;
                                this.dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                                   {
                                       try
                                       {
                                           this.FlowController.ErrorCount = 0;
                                           logger.Info("Found Project To Merge!");
                                           File.Copy(pFile.FilePath, tempDbCopy);
                                           this.FlowController.ProjectController.MergeProject(pFile.FilePath, fp, false, this.FlowController);
                                           File.Create(inProgressFile);
                                           File.Delete(pFile.FilePath);
                                           this.FlowController.ProjectController.IncomingClientProjects.RemoveAt(0);
                                           //File.Copy(dbToMerge, tempDbCopy);
                                       }
                                       catch
                                       {
                                           //ignore
                                           waitForLoad = false;
                                           errorsFound = true;
                                       }

                                   }));
                                while (waitForLoad)
                                {

                                    Thread.Sleep(1000);
                                }
                                logger.Info("Done with merge, keep looking for new stuff to merge");
                                if (File.Exists(inProgressFile))
                                {
                                    Thread.Sleep(2000);
                                    int deleteattempts = 0;
                                    while (deleteattempts++ < 100)
                                    {
                                        try
                                        {
                                            File.Delete(inProgressFile);
                                            deleteattempts = 101;//exit loop, it was successfull
                                        }
                                        catch (Exception ex)
                                        {
                                            if (deleteattempts > 10)
                                            {
                                                logger.Error("Failed to delete the inProgress.txt file more than 10 times");
                                                throw ex;
                                            }
                                        }
                                        Thread.Sleep(2000);
                                    }

                                }
                                this.FlowController.ProjectController.FlowServerStatus = "Done Merging project (" + fp.FlowProjectName + ")";

                                if ((errorsFound || this.FlowController.ErrorCount > 0) && (File.Exists(tempDbCopy) || File.Exists(pFile.FilePath)))
                                {
                                    logger.Error("Errors Occurred while merging " + pFile.DisplayName);
                                    string tempQcCopy = Path.Combine(qcFolder, (new FileInfo(pFile.FilePath)).Name);
                                    if (File.Exists(tempQcCopy))
                                        File.Move(tempQcCopy, tempQcCopy.Replace(".sdf", "_" + TicketGenerator.GenerateTicketString(4) + ".sdf"));

                                    if (!Directory.Exists(qcFolder))
                                        Directory.CreateDirectory(qcFolder);

                                    if (File.Exists(tempDbCopy))
                                    {
                                        File.Move(tempDbCopy, tempQcCopy);
                                        File.Delete(pFile.FilePath);
                                    }
                                    else if (File.Exists(pFile.FilePath))
                                        File.Move(pFile.FilePath, tempQcCopy);
                                }
                                else
                                {
                                    File.Delete(tempDbCopy);
                                }
                                Thread.Sleep(1000);
                            }
                        }
                        else if (pFile.FileType == ".paf" || pFile.FileType == "")
                        {

                            logger.Info("Incoming merge is a paf file: " + pFile.FilePath);
                            FlowProject RootProject = null;
                            try
                            {
                                RootProject = this.FlowController.ProjectController.FlowMasterDataContext.GetExistingProject(pFile.FilePath);
                                logger.Info("found a matching project in FlowMaster for " + pFile.FileName);
                            }
                            catch
                            {
                                //continue;
                            }
                            waitForLoad = true;

                            bool deleteFile = true;
                            if (RootProject == null)
                            {
                                logger.Info("Failed to find a match in FlowMaster for " + pFile.FileName);
                                logger.Info("Importing as new project ");
                                IterationFlowProject = null;
                                this.FlowController.ProjectController.FlowServerStatus = "Importing new project from paf - " + new FileInfo(pFile.FilePath).Name;
                                //if RootProject is null, its a new project
                                this.dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                                {
                                    try
                                    {
                                        if (pFile.FileType == ".paf")
                                        {
                                            if (File.Exists(pFile.FilePath))
                                                this.FlowController.ProjectController.AttachProject(pFile.FilePath, false, this.FlowController);
                                        }
                                        else
                                            this.FlowController.ProjectController.AttachProjectUncompressed(pFile.FilePath, false, this.FlowController);
                                        logger.Info("Finished attactching new project ");
                                        deleteFile = true;
                                        this.FlowController.ProjectController.IncomingClientProjects.RemoveAt(0);
                                    }
                                    catch (Exception exp)
                                    {
                                        logger.Error("FAILED to attatch new project. MSG: " + exp.Message);
                                        //ignore
                                        deleteFile = false;
                                        waitForLoad = false;
                                    }
                                }));
                            }
                            else
                            {
                                logger.Info("Begin Merge of project: " + pFile.FileName);
                                this.FlowController.ProjectController.FlowServerStatus = "merging paf file - " + new FileInfo(pFile.FilePath).Name;
                                IterationFlowProject = RootProject;
                                this.dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                                {
                                    //if RootProject is not null, it needs merged
                                    try
                                    {
                                        if (pFile.FileType == ".paf")
                                            this.FlowController.ProjectController.MergeProject(
                                                pFile.FilePath,
                                                RootProject,
                                                false,
                                                this.FlowController
                                            );
                                        else
                                            this.FlowController.ProjectController.MergeProjectUncompressed(
                                                pFile.FilePath,
                                                RootProject,
                                                false,
                                                this.FlowController
                                            );
                                        this.FlowController.ProjectController.IncomingClientProjects.RemoveAt(0);
                                        logger.Info("Finished merging project ");
                                        deleteFile = true;
                                    }
                                    catch (Exception exp)
                                    {
                                        logger.Error("FAILED to merge paf project. MSG: " + exp.Message);
                                        //ignore
                                        deleteFile = false;
                                        waitForLoad = false;
                                    }
                                }));




                            }

                            while (waitForLoad)
                            {

                                Thread.Sleep(1000);
                            }
                            if (deleteFile)
                            {
                                logger.Info("paf has been processed with no errors, so deleting file: " + pFile.FilePath);
                                if(File.Exists(pFile.FilePath))
                                    File.Delete(pFile.FilePath);
                                if (Directory.Exists(pFile.FilePath))
                                    Directory.Delete(pFile.FilePath,true);
                                Thread.Sleep(2000);
                            }

                        }
                        Thread.Sleep(1000);
                        this.FlowController.FlowServerCrawler.Begin();
                        Thread.Sleep(2000);
                    }
                    Thread.Sleep(100);
          
               

                
                //this.FlowController.ProjectController.FlowServerStatus = "Server is On";
            }
        }

        private void UpdateLockedFileStatus()
        {
            foreach (IncomingProjectFile pp in this.FlowController.ProjectController.IncomingClientProjects.Where(p => p.IsLocked))
            {
                if (File.Exists(pp.FilePath) && IOUtil.IsFileInUse(pp.FilePath))
                {
                    pp.IsLocked = true;
                }
                else
                {
                    pp.IsLocked = false;
                }
            }
            //this.dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
            //{
            //    this.FlowController.ProjectController.UpdateServerIncoming();
            //}));
        }

    }
}
