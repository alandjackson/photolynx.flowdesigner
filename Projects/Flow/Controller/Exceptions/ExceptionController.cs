﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Permissions;
using System.Windows.Forms;
using Flow.Properties;
using Flow.Lib.Log;
using System.Drawing;
using Flow.View.Exceptions;
using System.IO;
using System.Net.Mail;
using System.Drawing.Imaging;
using System.Net.Mime;
using Flow.Lib;
using StructureMap;
using Flow.Lib.Net;
using Flow.Controller.Project;
using Flow.Schema.LinqModel.DataContext;
using System.ComponentModel;
using System.Security.Principal;
using Flow.Lib.Exceptions;
using System.Threading;
using System.Windows.Threading;
using Flow.View.Dialogs;
using System.Data;
using System.Runtime.InteropServices;
using NLog;
using NLog.Targets;
using NLog.Config;
using Flow.Lib.FlowError;
using Flow.Lib.Activation;
using Flow.Lib.Persister;

namespace Flow.Controller.Exceptions
{

    public class ExceptionController : IExceptionHandler
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        ProjectController _projectController = null;
        public ProjectController ProjectController
        {
            get
            {
                return _projectController ?? (_projectController = ObjectFactory.GetInstance<FlowController>().ProjectController);
            }
        }


        [SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.ControlAppDomain)]
        public ExceptionController()
        {
            AppDomain currentDomain = AppDomain.CurrentDomain;
            currentDomain.UnhandledException += new UnhandledExceptionEventHandler(UnhandledExceptionHandler);

            Application.ThreadException += new System.Threading.ThreadExceptionEventHandler(Application_ThreadException);
            Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);

            System.Windows.Application.Current.DispatcherUnhandledException += new System.Windows.Threading.DispatcherUnhandledExceptionEventHandler(Current_DispatcherUnhandledException);
        }

        static Bitmap ScreenShot()
        {
            Bitmap bmpScreenshot = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            Graphics gfxScreenshot = Graphics.FromImage(bmpScreenshot);
            gfxScreenshot.CopyFromScreen(Screen.PrimaryScreen.Bounds.X, 
                Screen.PrimaryScreen.Bounds.Y, 0, 0, Screen.PrimaryScreen.Bounds.Size, 
                CopyPixelOperation.SourceCopy);

            return bmpScreenshot;
            //bmpScreenshot.Save("ss.png", System.Drawing.Imaging.ImageFormat.Png);
        }

        public void HandleException(object sender, Exception e)
        {
            HandleException(sender, e, ProjectController);
        }

        static ulong GetTotalMemoryInBytes()
        {
            return new Microsoft.VisualBasic.Devices.ComputerInfo().TotalPhysicalMemory;
        }

        static void HandleException(object sender, Exception e, ProjectController projectController)
        {
            if (projectController.FlowController.ErrorCount == null)
                projectController.FlowController.ErrorCount = 1;
            else
                projectController.FlowController.ErrorCount++;
 
            if (Settings.Default.ExceptionContext != "DEBUG")
            {
                logger.ErrorException("Exception occured.", e);
            }

            Bitmap ss = ScreenShot();

            if (e is IPhotoLynxException)
            {
                (e as IPhotoLynxException).Handle();
                return;
            }

            ExceptionNotification notififcation = new ExceptionNotification(e);
            if (notififcation.ShowDialog().Value)
            {
                Organization Studio = null;
                if(projectController.CurrentFlowProject != null)
                    Studio = (Organization)(projectController.FlowMasterDataContext.StudioList.Where(o => o.OrganizationID == projectController.CurrentFlowProject.StudioID).First<Organization>());
                else
                    Studio = (Organization)(projectController.FlowMasterDataContext.StudioList[0]);
                
                long workingSet = System.Diagnostics.Process.GetCurrentProcess().WorkingSet64;
               
                ulong available = GetTotalMemoryInBytes();

                string flowEdition = "Client Edition";
                if (projectController.FlowMasterDataContext.PreferenceManager.Application.IsFlowServer)
                    flowEdition = String.Format("Server Edition ({0})", projectController.FlowServerIsOn ? "On" : "Off");

                string flowPlatform = Environment.Is64BitProcess ? "x64" : "x86";

                string memoryString = (workingSet /1024)/1024 + " MB (" + (available / 1024)/1024 + " MB total available)";
                string body = String.Format("Computer ID: {0}\nSupportURL: {10}\nActivation Key: {15}\nProject Name: {1}\nFlow Version: {2}\nFlow Edition: {13}\nFlow Platform: {14}\nMemory: {3}\nFlow Up Time: {11} minutes\nSessionID: {12}\nStudio: {4}\nContact: {5}\nEmail: {6}\nPhone: {7}\nUser Message:\n{8}\n\nStack Trace:\n{9}",
                    WindowsIdentity.GetCurrent().Name, 
                    projectController.CurrentFlowProject == null ? "None loaded" : projectController.CurrentFlowProject.FileSafeProjectName,
                    FlowContext.GetFlowVersion(),
                    memoryString,
                    Studio.OrganizationName + " (" + Studio.LabCustomerID + ")",
                    Studio.OrganizationContactName,
                    Studio.OrganizationContactEmail,
                    Studio.OrganizationContactPhone,
                    notififcation.textBoxDescription.Text, 
                    e.ToString(),
                    projectController.FlowMasterDataContext.PreferenceManager.Application.SupportURL,
                    (DateTime.Now - projectController.FlowController.FlowStartTime).TotalMinutes,
                    projectController.FlowController.FlowSessionID,
                    flowEdition,
                    flowPlatform,
                    projectController.FlowController.ActivationKey);
                string subject = String.Format("Flow Bug Notification - ID {0}", notififcation.ErrorID);
                ProjectEmailer mailer = new ProjectEmailer(subject, body);

                string ssPath = null;
                FileInfo textPath = null;
                FlowErrorStore store = new FlowErrorStore();
                try
                {
                    

                    
                    //string textPath = FlowContext.FlowCommonAppDataDirPath + "/Logs/" + DateTime.Now.ToString("yyyy-MM-dd__HH.mm.ss") + ".txt"
                    LoggingConfiguration config = LogManager.Configuration;
                    FileTarget standardTarget = config.FindTargetByName("logfile") as FileTarget;

                    if (standardTarget != null)
                    {
                        string realName = NLog.Layouts.SimpleLayout.Evaluate(standardTarget.FileName.ToString());
                        realName = realName.Replace('/', Path.DirectorySeparatorChar);
                        realName = realName.Replace("'", "");
                        realName = realName.Replace("" + Path.DirectorySeparatorChar + Path.DirectorySeparatorChar,
                                                                    "" + Path.DirectorySeparatorChar);
                        textPath = new FileInfo(realName);
                    }

                    //TextWriter tw = new StreamWriter(textPath.FullName);
                    //tw.Write(body);
                    //tw.Close();
                    if(textPath != null)
                        mailer.AddAttachment(textPath.FullName);
                                        

                    if (notififcation.SendSS)
                    {
                        ssPath = Path.Combine(Path.GetTempPath(), "screenshot_" + notififcation.ErrorID + ".jpeg");
                        ss.Save(ssPath, ImageFormat.Jpeg);
                        mailer.AddAttachment(ssPath);
                    }


                    //////////////////////////////////////////////////

                    ActivationValidator activation = new ActivationValidator(projectController.FlowController.ActivationKeyFile);
                    ActivationStore activationStore = activation.Repository.Load();

                    store.ComputerId = WindowsIdentity.GetCurrent().Name;
                    store.SupportURL = projectController.FlowMasterDataContext.PreferenceManager.Application.SupportURL;
                    store.ProjectName = projectController.CurrentFlowProject == null ? "None loaded" : projectController.CurrentFlowProject.FileSafeProjectName;
                    store.FlowVersion = FlowContext.GetFlowVersion();
                    store.FlowEdition = flowEdition;
                    store.Memory = memoryString;
                    store.FlowUpTime = (DateTime.Now - projectController.FlowController.FlowStartTime).TotalMinutes.ToString();
                    store.SessionID = projectController.FlowController.FlowSessionID;
                    store.Studio = Studio.OrganizationName + " (" + Studio.LabCustomerID + ")";
                    store.Contact = Studio.OrganizationContactName;
                    store.Email = Studio.OrganizationContactEmail;
                    store.Phone = Studio.OrganizationContactPhone;
                    store.UserMessage = notififcation.textBoxDescription.Text.Substring(0, Math.Min(notififcation.textBoxDescription.Text.Length, 4000));
                    store.ActivationKey = activationStore.ActivationKey;
                    store.ErrorDate = DateTime.Now;
                    store.StackTrace = e.ToString().Substring(0, Math.Min(e.ToString().Length, 4000));
                    store.ErrorID = notififcation.ErrorID;

                   


                    ///////////////////////////////////////////////

                    if (notififcation.SendProject && projectController.CurrentFlowProject != null)
                    {
                        mailer.ProjectController = projectController;
                        FlowBackgroundWorkerManager.RunWorker(
                            Worker,
                            WorkerCompleted,
                            mailer);
                    }
                    else
                    {
                        mailer.AsyncSend();
                    }

                    //log error to flowadmin:
                    FlowErrorPost FEPost = new FlowErrorPost();
                    string returnMsg = FEPost.Post(store);
                    FEPost.SendFile(returnMsg, ssPath, store.ErrorID.ToString());
                    FEPost.SendFile(returnMsg, textPath.FullName, store.ErrorID.ToString());
                   

                    logger.Info("Sent error report ID {0}", notififcation.ErrorID);
                }
                catch (Exception ex)
                {
                    //FlowMessageBox msg = new FlowMessageBox("Error Mailer", "Sorry, Failed to Send Error Message\n\nCheck Your Internet Connection");
                    //msg.CancelButtonVisible = false;
                    //msg.ShowDialog();
                    mailer.ProgressInfo.Cancel();

                    DataTable dt = projectController.FlowMasterDataContext.PreferenceManager.Application.UnsentErrors;
                    DataRow dr = dt.NewRow();
                    dr["subject"] = subject;
                    dr["body"] = body;
                    dr["screenShot"] = ssPath;
                    dr["logFile"] = textPath.FullName;
                    dr["ErrorStore"] = ObjectSerializer.ToXmlString(new WebServiceFlowErrorMessage() { FlowErrorStore = store });
                    dt.Rows.Add(dr);
                    projectController.FlowMasterDataContext.SavePreferences();

                    logger.Warn("Failed to send error report ID {0}", notififcation.ErrorID);
                }
            }
        }

        


        static private void Worker(object sender, DoWorkEventArgs e)
        {
            ProjectUploader uploader = new ProjectUploader(
                (e.Argument as ProjectEmailer).ProjectController.FlowController,
                (e.Argument as ProjectEmailer).ProjectController.FlowMasterDataContext,
                (e.Argument as ProjectEmailer).ProjectController.CurrentFlowProject, false, true, null,"100");

            ExceptionWorkerArguements args = new ExceptionWorkerArguements(e.Argument as Emailer);

            uploader.ProgressInfo = (e.Argument as ProjectEmailer).ProgressInfo;
            args.AsyncAttatchment = uploader.PrepareUpload();

            (sender as Flow.Lib.AsyncWorker.FlowBackgroundWorker).RunParameter = args;
        }

        static private void WorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            ExceptionWorkerArguements args = (sender as Flow.Lib.AsyncWorker.FlowBackgroundWorker).RunParameter as ExceptionWorkerArguements;
            args.Emailer.AddAttachment(args.AsyncAttatchment);
            args.Emailer.AsyncSend();
        }

        void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            HandleException(sender, e.Exception);
        }


        void UnhandledExceptionHandler(object sender, UnhandledExceptionEventArgs e)
        {
            HandleException(sender, e.ExceptionObject as Exception);
        }

        void Current_DispatcherUnhandledException(object sender,
            System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            e.Handled = true;
            HandleException(sender, e.Exception as Exception);
        }
    }
}
