﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.Lib.Net;

namespace Flow.Controller.Exceptions
{
    public class ExceptionWorkerArguements
    {
        public ExceptionWorkerArguements(Emailer emailer)
        {
            Emailer = emailer;
        }

        public Emailer Emailer { get; set; }

        public string AsyncAttatchment { get; set; }
    }
}
