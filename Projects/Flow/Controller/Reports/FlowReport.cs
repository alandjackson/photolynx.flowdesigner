﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Xml;

using C1.C1Report;

using Flow.Schema.Reports;
using Flow.Schema.LinqModel.DataContext;
using C1.C1Preview;
using Flow.Lib.Helpers;
using System.Drawing.Printing;


namespace Flow.Controller.Reports
{
	public class FlowReport : C1Report
	{
		public FlowReportView FlowReportView { get; set; }
	
		private double _groupIndent = 0;

        public List<OrderPackage> ApplicablePackages = null;
        public List<Subject> ApplicableSubjects = null;
        public bool DoGroupSort { get { return this.FlowReportView.DoGroupSort; } }
        public bool ShowGroupByValue { get { return this.FlowReportView.ShowGroupByValue; } }
        public bool ShowReportName { get { return this.FlowReportView.ShowReportName; } }
        public bool ShowProjectName { get { return this.FlowReportView.ShowProjectName; } }
        public string OrderNumber = "None";
        public string SortString = "LastName, FirstName";
        public Organization ShippingOrganization { get { return this.FlowReportView.ShippingOrganization; } }
		public FlowReport()
		{
		}

		public FlowReport(FlowReportView reportView)
			: this()
		{
			this.FlowReportView = reportView;
            //this.C1Document.PageLayout.PageSettings.UsePrinterPaperSize = false;
            this.C1Document.PageLayout.PageSettings.UsePrinterPaperSize = false;
            this.C1Document.PageLayout.PageSettings.PaperKind = PaperKind.Letter;
            //this.C1Document.PageLayout.PageSettings.Width = "216mm";
            //this.C1Document.PageLayout.PageSettings.Height = "279mm";
            //this.C1Document.PageLayout.PageSettings.TopMargin = "10mm";

			// initialize control
			this.Clear();                // clear any existing fields
			this.Font.Name = "Tahoma";   // set default font for all controls
			this.Font.Size = 9;

            //this.C1Document.PageLayout.PageSettings.UsePrinterPaperSize = false;
            //this.C1Document.PageLayout.PageSettings.Width = "216mm";
           // this.C1Document.PageLayout.PageSettings.Height = "279mm";

            //this.Document.DefaultPageSettings.PaperSize.Width = 850;
            //this.Document.DefaultPageSettings.PaperSize.Height = 1100;
           
			// initialize Layout
			Layout l = this.Layout;
            
			l.Orientation = reportView.Orientation;
			l.Width = ((l.Orientation == OrientationEnum.Portrait) ? 8.5 : 11) * 1440;
            
            l.MarginTop = 720;
            l.MarginLeft = 720;
            l.MarginBottom = 720;
            l.MarginRight = 720;



			this.StartPage += new ReportEventHandler(FlowReport_StartPage);
            this.EndPage += new ReportEventHandler(FlowReport_EndPage);
		}

		void FlowReport_StartPage(object sender, ReportEventArgs e)
		{
			C1.C1Report.Section s = this.Sections[SectionTypeEnum.PageHeader];

			bool pageHeaderVisible = true;
			s.Visible = pageHeaderVisible;
		}

        void FlowReport_EndPage(object sender, ReportEventArgs e)
        {
           
        }
        

		private void CreateReportHeader(DataTable dataSource)
		{
			// create a report header
			Field fld;
			C1.C1Report.Section s = this.Sections[SectionTypeEnum.Header];

			s.Height = 720;
			

            //if (ShowReportName)
            //{
            //    fld = s.Fields.Add("FldTitle", this.FlowReportView.ReportTitle, 0, 0, 8000, 720);
            //    fld.Font.Size = 12;
            //    fld.Font.Bold = true;
            //    fld.Align = FieldAlignEnum.LeftTop;
            //}
            //if (ShowProjectName)
            //{
            //    fld = s.Fields.Add("FldProject", this.FlowReportView.FlowProject.FullProjectDisplayName, 0, 360, 8000, 720
            //    );
            //    fld.Font.Bold = true;
            //    fld.Align = FieldAlignEnum.LeftTop;
            //}

            //foreach (DataColumn column in dataSource.Columns)
            //{


            //    if (column.ExtendedProperties.ContainsKey("IsPageSubTitle"))
            //    {
            //        fld = s.Fields.Add(
            //            "GH" + column.ColumnName,
            //            "\"This Order Packaged For: \"",
            //            0, 720, 2800, 720
            //        );
            //        fld.Calculated = true;
            //        fld.CanGrow = true;
            //        fld.Font.Bold = false;
            //        fld.Font.Size = 12;
            //        fld.Align = FieldAlignEnum.LeftBottom;

            //        fld = s.Fields.Add(
            //            "GH2" + column.ColumnName,
            //            "\"\" & " + column.ColumnName,
            //            2800, 720, 8000, 720
            //        );
            //        fld.Calculated = true;
            //        fld.CanGrow = true;
            //        fld.Font.Bold = true;
            //        fld.Font.Size = 13;
            //        fld.Align = FieldAlignEnum.LeftBottom;

            //    }

            //}

            //fld = s.Fields.Add("FldStudio", this.FlowReportView.FlowProject.Customer.OrganizationName, this.Layout.Width - 3 * 1440, 0, 3 * 1440, 240);
            //fld.Align = FieldAlignEnum.RightTop;

            //fld = s.Fields.Add("FldOrganization", this.FlowReportView.FlowProject.Organization.OrganizationName, this.Layout.Width - 3 * 1440, 240, 3 * 1440, 240);
            //fld.Align = FieldAlignEnum.RightTop;
            

			double left = 0;

			if (dataSource != null && !dataSource.ExtendedProperties.ContainsKey("HasGroupings"))
			{
				foreach (DataColumn column in dataSource.Columns)
				{

                        double columnWidthInTwips = this.GetColumnWidthInTwips(column, 6, false);

                        fld = s.Fields.Add("H" + column.Ordinal.ToString(), column.Caption, left, 1720, columnWidthInTwips, 250);
                        fld.Font.Underline = true;
                        fld.Align = FieldAlignEnum.LeftTop;

                        if (column.ExtendedProperties.ContainsKey("IsNumeric"))
                            fld.Align = FieldAlignEnum.RightTop;

                        left += columnWidthInTwips;
				}
			}
		}


		private void CreateReportFooter(DataTable dataSource)
		{
			if (dataSource == null)
				return;
	
			Field fld;
			C1.C1Report.Section reportFooter = this.Sections[SectionTypeEnum.Footer];
			reportFooter.Height = 500;
			reportFooter.Visible = true;

			double left = _groupIndent;

			fld = reportFooter.Fields.Add("RptTotals", "Report Totals: ", 0, 120, 3 * 1440, 250);
			fld.Calculated = true;
			fld.Font.Bold = true;
			fld.Align = FieldAlignEnum.LeftTop;

			// Section separator
			fld = reportFooter.Fields.Add("RptFooterLine", "", 0, 120, this.Layout.Width, 20);
			fld.LineSlant = LineSlantEnum.NoSlant;
			fld.BorderStyle = BorderStyleEnum.Solid;
			fld.BorderColor = Colors.Black;

			foreach (DataColumn column in dataSource.Columns)
			{
				double columnWidthInTwips = this.GetColumnWidthInTwips(column, 6, false);

                if (!column.ExtendedProperties.ContainsKey("IsGroupField") && !column.ExtendedProperties.ContainsKey("HideColumn"))
				{
					if (column.ExtendedProperties.ContainsKey("CalculationExpression"))
					{
						string calculationExpression = column.ExtendedProperties["CalculationExpression"].ToString();

						fld = reportFooter.Fields.Add("RF", calculationExpression, left, 120, columnWidthInTwips, 250);
						fld.Calculated = true;
						fld.Font.Bold = true;
						fld.Align = FieldAlignEnum.RightTop;

						if (column.ExtendedProperties.ContainsKey("Format"))
							fld.Format = column.ExtendedProperties["Format"].ToString();
					}

					left += columnWidthInTwips;
				}
			}
		}


		private void CreatePageHeader(DataTable dataSource)
		{
			// create a page header with field labels
			Field fld;

			C1.C1Report.Section s = this.Sections[SectionTypeEnum.PageHeader];
			s.Height = 1020;
			s.Visible = true;

            if (ShowReportName)
            {
                fld = s.Fields.Add("PHTitle", this.FlowReportView.ReportTitle, 0, 0, 8000, 720);
                fld.Font.Bold = true;
                fld.Align = FieldAlignEnum.LeftTop;
            }



            if (ShowProjectName)
            {
                fld = s.Fields.Add("PHProject", this.FlowReportView.FlowProject.FullProjectDisplayName, 0, 240, 8000, 720);
                fld.Font.Bold = true;
                fld.Align = FieldAlignEnum.LeftTop;
            }

            if (this.FlowReportView.ReportName == "Subject Orders Summary")
            {
                if (dataSource.Columns.Contains("IQOrderID"))
                {
                    string OrderSource = "Order Source: Flow";
                    if (!string.IsNullOrEmpty(dataSource.Rows[0]["IQOrderID"].ToString()))
                        OrderSource = "Order Source: ImageQuix";
                    fld = s.Fields.Add(
                        "HIQOrderID",
                        OrderSource,
                        0, 480, 8000, 720
                    );

                    fld.Font.Bold = false;
                    fld.Align = FieldAlignEnum.LeftTop;
                }
                if (dataSource.Columns.Contains("WebOrderID"))
                {
                    string OrderSource = "Order Source: Flow";
                    if (!string.IsNullOrEmpty(dataSource.Rows[0]["WebOrderID"].ToString()))
                        OrderSource = "Order Source: Web";
                    fld = s.Fields.Add(
                        "HWebOrderID",
                        OrderSource,
                        0, 480, 8000, 720
                    );

                    fld.Font.Bold = false;
                    fld.Align = FieldAlignEnum.LeftTop;
                }
            }


			fld = s.Fields.Add("PHStudio", this.FlowReportView.FlowProject.Customer.OrganizationName, this.Layout.Width - 3 * 1440, 0, 3 * 1440, 240);
			fld.Align = FieldAlignEnum.RightTop;

			fld = s.Fields.Add("PHOrganization", this.FlowReportView.FlowProject.Organization.OrganizationName, this.Layout.Width - 3 * 1440, 240, 3 * 1440, 240);
			fld.Align = FieldAlignEnum.RightTop;			

			double left = 0;

            if (dataSource != null)
            {
                if (!dataSource.ExtendedProperties.ContainsKey("HasGroupings"))
                {
                    foreach (DataColumn column in dataSource.Columns)
                    {
                        if (column.ExtendedProperties.ContainsKey("HideColumn"))
                            continue;

                        double columnWidthInTwips = this.GetColumnWidthInTwips(column, 6, false);

                        fld = s.Fields.Add("H" + column.Ordinal.ToString(), column.Caption, left, 720, columnWidthInTwips, 840);
                        fld.Font.Underline = true;
                        fld.Align = FieldAlignEnum.LeftBottom;

                        if (column.ExtendedProperties.ContainsKey("IsNumeric"))
                            fld.Align = FieldAlignEnum.RightBottom;

                        left += columnWidthInTwips;
                    }
                }
            }
		}

        private void CreatePageFooter()
        {
            CreatePageFooter(0, 250, true);
        }
		private void CreatePageFooter( double leftPad, double bottomPad, bool showDivider)
		{
			// create a page footer
			Field f;
			C1.C1Report.Section s = this.Sections[SectionTypeEnum.PageFooter];

            s.Height = bottomPad;
			s.Visible = true;

			// Date/time (left)
            f = s.Fields.Add("FldFtrLeft", "Now", 0 + leftPad, 0, 4000, bottomPad);
			f.Calculated = true;
			f.Align = FieldAlignEnum.LeftBottom;
			f.Width = 4 * 1440;

			// Project name (center)
            f = s.Fields.Add("FldFtrCenter", this.FlowReportView.FlowProject.FlowProjectName, 0 + leftPad, 0, this.Layout.Width, bottomPad);
			f.Calculated = true;
			f.Align = FieldAlignEnum.CenterBottom;
			f.Width = this.Layout.Width - f.Left;

			// Page counter
            f = s.Fields.Add("FldFtrRight", @"""Page "" & Page & "" of "" & Pages", 4000 + leftPad, 0, 4000, bottomPad);
			f.Calculated = true;
			f.Align = FieldAlignEnum.RightBottom;
			f.Width = this.Layout.Width - f.Left;

            if (showDivider)
            {
                // Section separator
                f = s.Fields.Add("FldLine", "", 0 + leftPad, 0, this.Layout.Width, 20);
                f.LineSlant = LineSlantEnum.NoSlant;
                f.BorderStyle = BorderStyleEnum.Solid;
                f.BorderColor = Colors.Black;
            }
		}

        private void CreateDetail(FlowProject flowProject)
        {
           
            this.CreatePageHeader(null);


            Field fld;
            C1.C1Report.Section s;

            double left = _groupIndent;

            s = this.Sections[SectionTypeEnum.Detail];
            s.Height = 720;
            s.Visible = true;

            double top = 0;

            Organization studio = flowProject.FlowMasterDataContext.Organizations.Where(o => o.OrganizationID == (int)flowProject.StudioID).First();
                
            // Studio Name:
            fld = s.Fields.Add("UnderLine", "", 0, top, 11000, 720);
            fld.Visible = true;
            fld.CanGrow = true;
            fld.Font.Size = 10;
            fld.Font.Bold = false;
            fld.Height = 311;
            fld.BackColor = System.Windows.Media.Colors.LightGray;
            fld.Align = FieldAlignEnum.LeftBottom;

            fld = s.Fields.Add("FieldName", "Studio Name:", 0, top, 4000, 720);
            fld.Visible = true;
            fld.CanGrow = true;
            fld.Font.Size = 10;
            fld.Font.Bold = true;
            fld.Align = FieldAlignEnum.LeftTop;

            fld = s.Fields.Add("FieldValue", studio.OrganizationName, 4500, top, 6000, 720);
            fld.Visible = true;
            fld.CanGrow = true;
            fld.Font.Size = 10;
            fld.Font.Bold = false;
            fld.Align = FieldAlignEnum.LeftTop;

           

            top += 350;

            // Studio Account Number:
            fld = s.Fields.Add("FieldName", "Account Number:", 0, top, 4000, 720);
            fld.Visible = true;
            fld.CanGrow = true;
            fld.Font.Size = 10;
            fld.Font.Bold = true;
            fld.Align = FieldAlignEnum.LeftTop;

            fld = s.Fields.Add("FieldValue", studio.LabCustomerID, 4500, top, 6000, 720);
            fld.Visible = true;
            fld.CanGrow = true;
            fld.Font.Size = 10;
            fld.Font.Bold = false;
            fld.Align = FieldAlignEnum.LeftTop;

            top += 350;

            // Contact Name:
            fld = s.Fields.Add("UnderLine", "", 0, top, 11000, 720);
            fld.Visible = true;
            fld.CanGrow = true;
            fld.Font.Size = 10;
            fld.Font.Bold = false;
            fld.Height = 311;
            fld.BackColor = System.Windows.Media.Colors.LightGray;
            fld.Align = FieldAlignEnum.LeftBottom;

            fld = s.Fields.Add("FieldName", "Contact Name:", 0, top, 4000, 720);
            fld.Visible = true;
            fld.CanGrow = true;
            fld.Font.Size = 10;
            fld.Font.Bold = true;
            fld.Align = FieldAlignEnum.LeftTop;

            fld = s.Fields.Add("FieldValue", studio.OrganizationContactName, 4500, top, 6000, 720);
            fld.Visible = true;
            fld.CanGrow = true;
            fld.Font.Size = 10;
            fld.Font.Bold = false;
            fld.Align = FieldAlignEnum.LeftTop;

            top += 350;


            // Contact Phone:
            fld = s.Fields.Add("FieldName", "Contact Phone:", 0, top, 4000, 720);
            fld.Visible = true;
            fld.CanGrow = true;
            fld.Font.Size = 10;
            fld.Font.Bold = true;
            fld.Align = FieldAlignEnum.LeftTop;

            fld = s.Fields.Add("FieldValue", studio.OrganizationContactPhone, 4500, top, 6000, 720);
            fld.Visible = true;
            fld.CanGrow = true;
            fld.Font.Size = 10;
            fld.Font.Bold = false;
            fld.Align = FieldAlignEnum.LeftTop;

            top += 350;

            // Contact Email:
            fld = s.Fields.Add("UnderLine", "", 0, top, 11000, 720);
            fld.Visible = true;
            fld.CanGrow = true;
            fld.Font.Size = 10;
            fld.Font.Bold = false;
            fld.Height = 311;
            fld.BackColor = System.Windows.Media.Colors.LightGray;
            fld.Align = FieldAlignEnum.LeftBottom;

            fld = s.Fields.Add("FieldName", "Contact Email:", 0, top, 4000, 720);
            fld.Visible = true;
            fld.CanGrow = true;
            fld.Font.Size = 10;
            fld.Font.Bold = true;
            fld.Align = FieldAlignEnum.LeftTop;

            fld = s.Fields.Add("FieldValue", studio.OrganizationContactEmail, 4500, top, 6000, 720);
            fld.Visible = true;
            fld.CanGrow = true;
            fld.Font.Size = 10;
            fld.Font.Bold = false;
            fld.Align = FieldAlignEnum.LeftTop;

            top += 350;

            // Order Number:
            fld = s.Fields.Add("FieldName", "Order Number:", 0, top, 4000, 720);
            fld.Visible = true;
            fld.CanGrow = true;
            fld.Font.Size = 10;
            fld.Font.Bold = true;
            fld.Align = FieldAlignEnum.LeftTop;

            fld = s.Fields.Add("FieldValue", this.OrderNumber, 4500, top, 6000, 720);
            fld.Visible = true;
            fld.CanGrow = true;
            fld.Font.Size = 10;
            fld.Font.Bold = false;
            fld.Align = FieldAlignEnum.LeftTop;

            top += 350;

            // Sort String:
            fld = s.Fields.Add("UnderLine", "", 0, top, 11000, 720);
            fld.Visible = true;
            fld.CanGrow = true;
            fld.Font.Size = 10;
            fld.Font.Bold = false;
            fld.Height = 311;
            fld.BackColor = System.Windows.Media.Colors.LightGray;
            fld.Align = FieldAlignEnum.LeftBottom;

            fld = s.Fields.Add("FieldName", "Sort Order:", 0, top, 4000, 720);
            fld.Visible = true;
            fld.CanGrow = true;
            fld.Font.Size = 10;
            fld.Font.Bold = true;
            fld.Align = FieldAlignEnum.LeftTop;

            fld = s.Fields.Add("FieldValue", this.SortString, 4500, top, 6000, 720);
            fld.Visible = true;
            fld.CanGrow = true;
            fld.Font.Size = 10;
            fld.Font.Bold = false;
            fld.Align = FieldAlignEnum.LeftTop;

            top += 350;

            // Shipping Info:
            fld = s.Fields.Add("FieldName", "Ship To:", 0, top, 4000, 720);
            fld.Visible = true;
            fld.CanGrow = true;
            fld.Font.Size = 10;
            fld.Font.Bold = true;
            fld.Align = FieldAlignEnum.LeftTop;

            if (ShippingOrganization != null)
            {
                fld = s.Fields.Add("FieldValue", this.ShippingOrganization.OrganizationName, 4500, top, 6000, 720);
                fld.Visible = true;
                fld.CanGrow = true;
                fld.Font.Size = 10;
                fld.Font.Bold = false;
                fld.Align = FieldAlignEnum.LeftTop;

                top += 350;
                fld = s.Fields.Add("FieldValue", this.ShippingOrganization.OrganizationShippingAddressLine1, 4500, top, 6000, 720);
                fld.Visible = true;
                fld.CanGrow = true;
                fld.Font.Size = 10;
                fld.Font.Bold = false;
                fld.Align = FieldAlignEnum.LeftTop;

                top += 350;
                fld = s.Fields.Add("FieldValue", this.ShippingOrganization.OrganizationShippingCity + ", " + this.ShippingOrganization.OrganizationShippingStateCode + " " + this.ShippingOrganization.OrganizationShippingZipCode, 4500, top, 6000, 720);
                fld.Visible = true;
                fld.CanGrow = true;
                fld.Font.Size = 10;
                fld.Font.Bold = false;
                fld.Align = FieldAlignEnum.LeftTop;

                top += 350;
                //end shipping info
            }
            else
            {
                top += 350;
            }


            int rowCnt = 0;
            foreach (OrderFormFieldLocal field in flowProject.FlowProjectDataContext.OrderFormFieldLocalList.OrderBy(o => o.SortOrder))
            {
                //double valueWidth = 6000;
                double valueWidth = 6000;
                double stringW = GetStringWidthInTwips(field.SelectedValue, "Arial", 10, false);
                int lines = Convert.ToInt32(Math.Ceiling((double)(stringW / valueWidth)));
                if (lines < 1)
                    lines = 1;

                if (rowCnt++ % 2 == 0)
                {
                    fld = s.Fields.Add("UnderLine", "", 0, top, 11000, 720);
                    fld.Visible = true;
                    fld.CanGrow = true;
                    fld.Font.Size = 10;
                    fld.Font.Bold = false;
                    fld.Height = 311 * lines;
                    fld.BackColor = System.Windows.Media.Colors.LightGray;
                    fld.Align = FieldAlignEnum.LeftBottom;
                }


                s.Visible = true;
                s.KeepTogether = true;

                fld = s.Fields.Add("FieldName", field.FieldName, 0, top, 4000, 720);
                fld.Visible = true;
                fld.CanGrow = true;
                fld.Font.Size = 10;
                fld.Font.Bold = true;
                fld.Align = FieldAlignEnum.LeftTop;


                fld = s.Fields.Add("FieldValue", field.SelectedValue, 4500, top, 6000, 720);
                fld.Visible = true;
                fld.CanGrow = true;
                fld.Font.Size = 10;
                fld.Font.Bold = false;
                fld.Align = FieldAlignEnum.LeftTop;

                top += (350 * lines);
            }

        }

        //
        //This Method is no longer used.
        //This report is now done in the CustomReports Class
        //
        //private void CreateCatalogPackages(FlowProject flowProject)
        //{

        //    this.CreatePageHeader(null);


        //    Field fld;
        //    C1.C1Report.Section s;

        //    double left = _groupIndent;

        //    s = this.Sections[SectionTypeEnum.Detail];
        //    s.Height = 720;
        //    s.Visible = true;

        //    double top = 0;

        //    // Catalog Name

        //    fld = s.Fields.Add("FieldName", "Catalog Name:", 0, top, 4000, 720);
        //    fld.Visible = true;
        //    fld.CanGrow = true;
        //    fld.Font.Size = 11;
        //    fld.Font.Bold = true;
        //    fld.Align = FieldAlignEnum.LeftTop;

        //    fld = s.Fields.Add("FieldValue", flowProject.FlowCatalog.FlowCatalogDesc, 1700, top, 6000, 720);
        //    fld.Visible = true;
        //    fld.CanGrow = true;
        //    fld.Font.Size = 12;
        //    fld.Font.Bold = true;
        //    fld.Align = FieldAlignEnum.LeftTop;


        //    top += 350;
        //    top += 350;
        //    int rowCnt = 0;
        //    double startTop = top;
        //    double lastRow = 0;
        //    foreach (ProductPackage pp in flowProject.FlowCatalog.ProductPackages)
        //    {
                
        //        rowCnt++;
        //        int leftSide = 0;
        //        if (rowCnt % 2 == 0)
        //        {
        //            leftSide = 5000;
        //            lastRow = top;
        //            top = startTop;
        //        }
        //        else
        //        {
                   
        //            leftSide = 0;
        //            if (top > lastRow)
        //                startTop = top;
        //            else
        //                startTop = lastRow;
        //            top = startTop;
        //        }
        //            //fld = s.Fields.Add("FieldName", "Package:", 0, top, 4000, 720);
        //            //fld.Visible = true;
        //            //fld.CanGrow = true;
        //            //fld.Font.Size = 10;
        //            //fld.Font.Bold = true;
        //            //fld.Align = FieldAlignEnum.LeftTop;


        //            fld = s.Fields.Add("FieldValue", pp.ProductPackageDesc, leftSide + 0, top, 6000, 720);
        //        fld.Visible = true;
        //        fld.CanGrow = true;
        //        fld.Font.Size = 10;
        //        fld.Font.Bold = true;
        //        fld.Align = FieldAlignEnum.LeftTop;

        //        top += 300;
                
        //        foreach (ProductPackageComposition ppc in pp.ProductPackageCompositions)
        //        {
                    

        //            fld = s.Fields.Add("FieldName", "(" + ppc.Quantity + ")", leftSide + 350, top, 4000, 720);
        //            fld.Visible = true;
        //            fld.CanGrow = true;
        //            fld.Font.Size = 10;
        //            fld.Font.Bold = false;
        //            fld.Align = FieldAlignEnum.LeftTop;

        //            fld = s.Fields.Add("FieldName", ppc.DisplayLabel, leftSide + 650, top, 4000, 720);
        //            fld.Visible = true;
        //            fld.CanGrow = true;
        //            fld.Font.Size = 10;
        //            fld.Font.Bold = false;
        //            fld.Align = FieldAlignEnum.LeftTop;

        //            top += 300;
        //        }
        //        top += 350;
        //    }

        //}


        private void CreateDetail(FlowProject flowProject, IEnumerable<ProjectSubjectField> subjectFields)
        {
            if (this.ApplicableSubjects != null && this.ApplicablePackages != null)
            {
                CreateDetail(flowProject, subjectFields, this.ApplicablePackages, this.ApplicableSubjects);
                return;
            }
            string orderFilter = flowProject.SelectedReportOrderFilter;
            string orderFilterID = "ALL";
            if (orderFilter == "New Orders" || orderFilter == "New Online Orders")
            {
                orderFilterID = null;
            }
            if (orderFilter.Contains(" - "))
            {
                orderFilterID = orderFilter.Split(" - ".ToCharArray())[0];
            }

            List<OrderPackage> applicableOrderPackages = new List<OrderPackage>();
            List<Subject> applicableSubjects = new List<Subject>();
            foreach (Subject thisSubject in flowProject.SubjectList.OrderBy(o => o.FormalFullName))
            {
                foreach (SubjectOrder so in thisSubject.SubjectOrders)
                {
                    if (!so.IsMarkedForDeletion)
                    {
                        foreach (OrderPackage op in so.OrderPackages)
                        {
                            if (orderFilterID == null)
                            {
                                if (orderFilter == "New Online Orders")
                                {
                                    if ((op.IsOnlineOrder) && (op.LabOrderID == null))
                                    {
                                        applicableOrderPackages.Add(op);
                                        if (!applicableSubjects.Contains(thisSubject))
                                            applicableSubjects.Add(thisSubject);
                                    }
                                }
                                else
                                {
                                    if ((op.OrderHistories == null) || (op.OrderHistories.Count < 1))
                                    {
                                        applicableOrderPackages.Add(op);
                                        if (!applicableSubjects.Contains(thisSubject))
                                            applicableSubjects.Add(thisSubject);
                                    }
                                }
                            }
                            else if (orderFilterID == "ALL")
                            {
                                applicableOrderPackages.Add(op);
                                if (!applicableSubjects.Contains(thisSubject))
                                    applicableSubjects.Add(thisSubject);
                            }
                            else if (orderFilterID != null && orderFilterID.Length > 1)
                            {
                                //applicable packages are those part of an old orderId
                                foreach (OrderHistory oh in op.OrderHistories)
                                {
                                    if (oh.OrderID == orderFilterID)
                                    {
                                        applicableOrderPackages.Add(op);
                                        if (!applicableSubjects.Contains(thisSubject))
                                            applicableSubjects.Add(thisSubject);
                                    }
                                }
                            }
                            else
                            {
                                applicableOrderPackages.Add(op);
                                if (!applicableSubjects.Contains(thisSubject))
                                    applicableSubjects.Add(thisSubject);
                            }
                        }
                    }
                }
            }

            CreateDetail(flowProject, subjectFields, applicableOrderPackages, applicableSubjects);
        }


        private void CreateDetail(FlowProject flowProject, IEnumerable<ProjectSubjectField> subjectFields, List<OrderPackage> applicableOrderPackages, List<Subject> applicableSubjects)
        {
            Field fld;
            Field fldSub;
            Field fldHead;
            C1.C1Report.Section s;

            bool showPrice = flowProject.ReportShowPrice;
            double left = _groupIndent;

            s = this.Sections[SectionTypeEnum.Detail];
            s.Height = 720;
            s.Visible = true;


            foreach (Subject subject in applicableSubjects)
            {
                if (subject.SubjectOrder.OrderPackageList.Where(op => applicableOrderPackages.Contains(op)).Count() > 0)
                {
                    s.Visible = true;
                    s.KeepTogether = true;
                    fld = s.Fields.Add("Summary", "Order Details", 0, 0, 8000, 720);
                    fld.Visible = true;
                    fld.CanGrow = true;
                    fld.Font.Size = 14;
                    fld.Font.Bold = true;
                    fld.Align = FieldAlignEnum.LeftTop;
                    

                    String orderPackages = "";

                    C1Report MySubReport = new C1Report();
                    C1.C1Report.Section headerSection = MySubReport.Sections[SectionTypeEnum.Header];
                    C1.C1Report.Section summarySection = MySubReport.Sections[SectionTypeEnum.Detail];
                    C1.C1Report.Section footerSection = MySubReport.Sections[SectionTypeEnum.Footer];
                    summarySection.Visible = true;
                    summarySection.KeepTogether = true;
                    footerSection.Visible = true;
                    footerSection.KeepTogether = true;
                    headerSection.Visible = true;
                    headerSection.KeepTogether = true;

                    MySubReport.ReportName = "Summary";





                    fldSub = summarySection.Fields.Add("", "Orders for: " + subject.FormalFullName, 0, 720, 8000, 720);
                    fldSub.Visible = true;
                    fldSub.CanGrow = true;
                    fldSub.Font.Size = 14;
                    fldSub.Font.Bold = true;
                    fldSub.Align = FieldAlignEnum.LeftTop;

                    decimal sum = 0;
                    foreach (OrderPackage op in subject.SubjectOrder.OrderPackageList.Where(op => applicableOrderPackages.Contains(op)))
                    {

                        orderPackages = orderPackages + op.ProductPackageDesc;
                        if (showPrice)
                            orderPackages = orderPackages + " - " + op.ExpandedPriceDisplayText;
                        orderPackages = orderPackages + "\n";

                        sum = sum + op.ExpandedPrice;
                        foreach (OrderProduct product in op.OrderProductList)
                        {
                            orderPackages = orderPackages + "\t" + product.Quantity + " : " + product.OrderProductLabel + "\n";
                        }
                        orderPackages = orderPackages + "\n";

                    }

                    fldSub = summarySection.Fields.Add("", orderPackages, 150, 1200, 8000, 720);
                    fldSub.Visible = true;
                    fldSub.CanGrow = true;
                    fldSub.Font.Size = 10;
                    fldSub.Font.Bold = false;
                    fldSub.Align = FieldAlignEnum.LeftTop;

                    if (showPrice)
                    {
                        fldSub = footerSection.Fields.Add("", "Report Total: $" + sum, 50, 0, 8000, 720);
                        fldSub.Visible = true;
                        fldSub.CanGrow = true;
                        fldSub.Font.Size = 11;
                        fldSub.Font.Bold = true;
                        fldSub.Align = FieldAlignEnum.LeftTop;
                    }

                    fldHead = headerSection.Fields.Add("FldTitle", this.FlowReportView.ReportTitle, 0, 0, 8000, 720);
                    fldHead.Font.Size = 12;
                    fldHead.Font.Bold = true;
                    fldHead.Align = FieldAlignEnum.LeftTop;

                    fldHead = headerSection.Fields.Add("FldProject", this.FlowReportView.FlowProject.FullProjectDisplayName, 0, 360, 8000, 720
                    );
                    fldHead.Font.Bold = true;
                    fldHead.Align = FieldAlignEnum.LeftTop;

                    fldHead = headerSection.Fields.Add("FldStudio", this.FlowReportView.FlowProject.Customer.OrganizationName, this.Layout.Width - 3 * 1440, 0, 3 * 1440, 240);
                    fldHead.Align = FieldAlignEnum.RightTop;

                    fldHead = headerSection.Fields.Add("FldOrganization", this.FlowReportView.FlowProject.Organization.OrganizationName, this.Layout.Width - 3 * 1440, 240, 3 * 1440, 240);
                    fldHead.Align = FieldAlignEnum.RightTop;

                    fld.Subreport = MySubReport;
                    fld.ForcePageBreak = ForcePageBreakEnum.After;
                    //fldSub.ForcePageBreak = ForcePageBreakEnum.Before;
                }
                
            }
            
        }

        private void CreateDetail(DataTable dataSource, bool doGroupSort, bool showGroupByValue, bool showPrice)
		{
			if (dataSource != null)
			{
				this.CreatePageHeader(dataSource);

				if (dataSource.ExtendedProperties.ContainsKey("HasGroupings"))
                    this.CreateGroups(dataSource, doGroupSort, showGroupByValue, showPrice);
			}

			Field fld;
            Field fldH;
            bool doneOrderSource = false;
			C1.C1Report.Section s = this.Sections[SectionTypeEnum.Detail];
            
			s.Height = 500;
			s.Visible = true;
            s.CanGrow = true;
			double left = _groupIndent;

			if (dataSource == null)
			{
				fld = s.Fields.Add("FNoData", "No report data.", left, 0, 1440, 250);
				fld.Align = FieldAlignEnum.LeftTop;
			}
			else
			{
               
                
                //string previousSubjectID = "0";
				foreach (DataColumn column in dataSource.Columns)
				{

                    if (this.Groups.Count > 0)
                    {


                        if (column.ExtendedProperties.ContainsKey("IsPageSubTitle"))
                        {
                            C1.C1Report.Section groupHeader = this.Groups[0].SectionHeader;
                            groupHeader.Height = 40;


                            //fld = groupHeader.Fields.Add(
                            //   "GH" + column.ColumnName,
                            //   "\"Order For: \"",
                            //   0, 0, 1250, 10
                            // );
                            //fld.Calculated = true;
                            //fld.CanGrow = true;
                            //fld.Font.Bold = false;
                            //fld.Font.Size = 11;
                            //fld.Align = FieldAlignEnum.LeftBottom;

                            fld = groupHeader.Fields.Add(
                                "GH2" + column.ColumnName,
                                "\"\" & " + column.ColumnName,
                                1550, 0, 8000, 10
                            );
                            fld.Calculated = true;
                            fld.CanGrow = true;
                            fld.Font.Bold = true;
                            fld.Font.Size = 12;
                            fld.Align = FieldAlignEnum.LeftTop;
                        }

                        if (column.ColumnName.Contains("TicketID") && !doGroupSort)
                        {
                            C1.C1Report.Section groupHeader = this.Groups[0].SectionHeader;
                            groupHeader.Height = 40;

                            fld = groupHeader.Fields.Add(
                                "GH2" + column.ColumnName,
                                "\"\" & " + column.ColumnName,
                                 0, 0, 1550, 10
                            );
                            fld.Calculated = true;
                            fld.CanGrow = true;
                            fld.Font.Bold = false;
                            fld.Font.Size = 11;
                            fld.Align = FieldAlignEnum.LeftTop;
                        }

                        if (column.ColumnName.Contains("ShipName"))
                        {
                            C1.C1Report.Section groupHeader = this.Groups[0].SectionHeader;
                            groupHeader.Height = 40;
                            //C1.C1Report.Section groupHeader = this.Groups[0].SectionHeader;
                            //C1.C1Report.Section pageHeader = this.Sections[SectionTypeEnum.PageHeader];

                            //groupHeader.Height = 10;
                            //groupHeader.Visible = true;
                            //groupHeader.Repeat = true;

                            fld = groupHeader.Fields.Add(
                               "GH" + column.ColumnName,
                               "\"Ship To: \"",
                               5300, 0, 1250, 10
                             );
                            fld.Calculated = true;
                            fld.CanGrow = true;
                            fld.Font.Bold = false;
                            fld.Font.Size = 10;
                            fld.Align = FieldAlignEnum.LeftTop;

                            fld = groupHeader.Fields.Add(
                                "GH2" + column.ColumnName,
                                "\"\" & " + column.ColumnName,
                                6250, 0, 8000, 10
                            );
                            fld.Calculated = true;
                            fld.CanGrow = true;
                            fld.Font.Bold = false;
                            fld.Font.Size = 10;
                            fld.Align = FieldAlignEnum.LeftBottom;
                        }
                        if (column.ColumnName.Contains("ShipAddress"))
                        {
                            C1.C1Report.Section groupHeader = this.Groups[0].SectionHeader;
                            groupHeader.Height = 40;
                            //C1.C1Report.Section groupHeader = this.Groups[0].SectionHeader;
                            //C1.C1Report.Section pageHeader = this.Sections[SectionTypeEnum.PageHeader];

                            //groupHeader.Height = 10;
                            //groupHeader.Visible = true;
                            //groupHeader.Repeat = true;


                            fld = groupHeader.Fields.Add(
                                "GH2" + column.ColumnName,
                                "\"\" & " + column.ColumnName,
                                6250, 20, 8000, 10
                            );
                            fld.Calculated = true;
                            fld.CanGrow = true;
                            fld.Font.Bold = false;
                            fld.Font.Size = 10;
                            fld.Align = FieldAlignEnum.LeftBottom;
                        }
                        if (column.ColumnName.Contains("ShipCityState"))
                        {
                            C1.C1Report.Section groupHeader = this.Groups[0].SectionHeader;
                            groupHeader.Height = 40;
                            //C1.C1Report.Section groupHeader = this.Groups[0].SectionHeader;
                            //C1.C1Report.Section pageHeader = this.Sections[SectionTypeEnum.PageHeader];

                            //groupHeader.Height = 10;
                            //groupHeader.Visible = true;
                            //groupHeader.Repeat = true;


                            fld = groupHeader.Fields.Add(
                                "GH2" + column.ColumnName,
                                "\"\" & " + column.ColumnName,
                                6250, 40, 8000, 10
                            );
                            fld.Calculated = true;
                            fld.CanGrow = true;
                            fld.Font.Bold = false;
                            fld.Font.Size = 10;
                            fld.Align = FieldAlignEnum.LeftBottom;
                        }

                        if (column.ColumnName.Contains("IQID"))
                        {
                            doneOrderSource = true;
                            C1.C1Report.Section groupHeader = this.Groups[0].SectionHeader;
                            groupHeader.Height = 40;
                            //C1.C1Report.Section groupHeader = this.Groups[0].SectionHeader;
                            //C1.C1Report.Section pageHeader = this.Sections[SectionTypeEnum.PageHeader];

                            //groupHeader.Height = 10;
                            //groupHeader.Visible = true;
                            //groupHeader.Repeat = true;


                            string OrderSource = "Order Source: Flow";
                            if (!string.IsNullOrEmpty(dataSource.Rows[0]["IQID"].ToString()))
                                OrderSource = "Order Source: ImageQuix - ";
                            fld = groupHeader.Fields.Add(
                                    "GH2" + OrderSource,
                                    OrderSource,
                                    0, 40, 2400, 10
                            );
                            fld.Calculated = true;
                            fld.CanGrow = true;
                            fld.Font.Bold = false;
                            fld.Font.Size = 10;
                            fld.Align = FieldAlignEnum.LeftBottom;




                            if (!string.IsNullOrEmpty(dataSource.Rows[0]["IQID"].ToString()))
                            {
                               
                                
                                 fld = groupHeader.Fields.Add(
                                "GH2" + column.ColumnName,
                                "\"\" & " + column.ColumnName,
                                2400, 40, 2000, 10
                                );
                                fld.Calculated = true;
                                fld.CanGrow = true;
                                fld.Font.Bold = false;
                                fld.Font.Size = 10;
                                fld.Align = FieldAlignEnum.LeftBottom;
                            }

                        }


                    }


                   

                    bool isBold = false;
                    if (column.ExtendedProperties.Contains("IsBold"))
                        isBold = true;

					double columnWidthInTwips = this.GetColumnWidthInTwips(column, 6, isBold);



					if (column.ExtendedProperties.Contains("IsGroupField"))
						continue;
                    if (column.ExtendedProperties.Contains("HideColumn"))
                        continue;
                    
					fld = s.Fields.Add("F" + column.Ordinal.ToString(), column.ColumnName, left, 0, columnWidthInTwips, 150);
					fld.Calculated = true;
					fld.Align = FieldAlignEnum.LeftTop;
                    fld.CanGrow = true;
                    //if (column.ExtendedProperties.Contains("IsPageBreakField"))
                    //{
                    //    if (fld.Value.ToString() != previousSubjectID)
                    //        fld.ForcePageBreak = ForcePageBreakEnum.Before;

                    //    previousSubjectID = fld.Value.ToString();
                    //}

                    if (column.ExtendedProperties.Contains("HideDuplicates"))
                        fld.HideDuplicates = true;

                    if (isBold)
                        fld.Font.Bold = true;

                    if (column.ExtendedProperties.Contains("fontSize"))
                        fld.Font.Size = (float)column.ExtendedProperties["fontSize"];

					if (column.ColumnName == "SubjectName")
						fld.HideDuplicates = true;

					if (column.ExtendedProperties.ContainsKey("IsNumeric"))
						fld.Align = FieldAlignEnum.RightTop;

					if (column.ExtendedProperties.ContainsKey("Format"))
						fld.Format = column.ExtendedProperties["Format"].ToString();

                    

					left += columnWidthInTwips;
				}
			}
		}


        //private void CreateNewTicketBarcodes(FlowProject flowProject)
        //{
            
        //    this.RecordsPerPage = 0; //this isnt true, but we need to lie about it ;-)
        //    string layout = flowProject.SelectedReportSubjectBarcodesLayout;
        //    int ticketCount = flowProject.NewTicketBatchSize;
            
        //    //////////////////////////////////////////////
        //    // All Configurable Setting are here /////////
        //    //////////////////////////////////////////////

        //    //Default Values are for 30-up labels
        //    double marginLeft = 400;
        //    double marginTop = 720;
        //    double marginRight = 0;
        //    double marginBottom = 720;

        //    double NameTopMargin = 100;

        //    double barCodeStringLeftPadding = 2580;
        //    double spaceBetweenItems = 50;

        //    double pageW = 12240 - (marginLeft + marginRight);
        //    double pageH = 15840 - (marginTop + marginBottom);

        //    double pageRows = 10;
        //    double pageCols = 3;

        //    float NameFontSize = 10;
        //    float BarcodeFontSize = 18;
        //    float otherFontSize = 8;

        //    double NameFieldHeight = 250;
        //    double BarcodeFieldHeight = 300;
        //    double smallFontFieldHeight = 170;

        //    string barcodeFont = "BC C39 2 to 1 Medium";

        //    int itemsPerPage = 30;
            
        //    if(layout.ToLower().Contains("8-up"))
        //    {
        //        marginLeft = 720;
        //        marginTop = 720;
        //        marginRight = 720;
        //        marginBottom = 720;

        //        NameTopMargin = 100;

        //        barCodeStringLeftPadding = 3420;
        //        spaceBetweenItems = 100;

        //        pageW = 9540;
        //        pageH = 15840 - marginTop;

        //        pageRows = 4;
        //        pageCols = 2;

        //        NameFontSize = 14;
        //        BarcodeFontSize = 22;
        //        otherFontSize = 9;

        //        NameFieldHeight = 400;
        //        BarcodeFieldHeight = 400;
        //        smallFontFieldHeight = 200;

        //        barcodeFont = "BC C39 2 to 1 Medium";

        //        itemsPerPage = 8;
        //    }
        //    if (layout.ToLower().Contains("10-up"))
        //    {
        //        marginLeft = 720;
        //        marginTop = 0;
        //        marginRight = 720;
        //        marginBottom = 0;

        //        NameTopMargin = 350;

        //        barCodeStringLeftPadding = 3420;
        //        spaceBetweenItems = 1340;

        //        pageW = 12240 - (marginLeft + marginRight + spaceBetweenItems);
        //        pageH = 15840 - (marginTop );

        //        pageRows = 5;
        //        pageCols = 2;

        //        NameFontSize = 14;
        //        BarcodeFontSize = 22;
        //        otherFontSize = 9;

        //        NameFieldHeight = 400;
        //        BarcodeFieldHeight = 400;
        //        smallFontFieldHeight = 200;

        //        barcodeFont = "BC C39 2 to 1 Medium";

        //        this.RecordsPerPage = 0;

        //        itemsPerPage = 10;
        //    }

        //    ///////////////////////////////////////////////
        //    ///////////////////////////////////////////////

        //    this.Layout.MarginBottom = 350;
        //    this.Layout.MarginLeft = marginLeft;
        //    this.Layout.MarginRight = marginRight;
        //    this.Layout.MarginTop = marginTop;
           


        //    //double left = _groupIndent;
        //    double hardLeft = 0;
        //    double hardTop = 0;
        //    double rowCnt=0;
        //    double colCnt=0;

        //    double cardWidth = pageW / pageCols;
        //    double cardHeight = pageH / pageRows;

            
        //    double BarcodeTopMargin = NameTopMargin + NameFieldHeight;
        //    double OrganizaionTopMargin = BarcodeTopMargin + BarcodeFieldHeight;
        //    double OtherTopMargin = OrganizaionTopMargin;

           
        //    C1.C1Report.Section sh = this.Sections[SectionTypeEnum.Detail];
        //    sh.Visible = true;
        //    sh.CanGrow = true;
        //    sh.Repeat = false;


           
        //    C1.C1Report.Section s = null; 
        //        int allrowCnt = 0;
        //        int pageCount = 0;
        //        for (int i = 0; i < ticketCount;i++ )
        //        {

        //            allrowCnt++;
        //            hardLeft = colCnt * cardWidth;
        //            hardTop = rowCnt * cardHeight;

        //            if (rowCnt == 0 && colCnt == 0)
        //            {
        //                pageCount++;


        //                Field fldm = sh.Fields.Add("FSummary " + pageCount, "Camera Cards", 0, 0, 1, 1);
        //                fldm.ZOrder = pageCount;
        //                fldm.Visible = true;
        //                fldm.CanGrow = true;
        //                fldm.Font.Bold = true;
        //                fldm.Align = FieldAlignEnum.LeftTop;

        //                C1Report MySubReport = new C1Report();
        //                s = MySubReport.Sections[SectionTypeEnum.Detail];
        //                s.Repeat = false;
        //                s.Visible = true;

        //                fldm.Subreport = MySubReport;
        //                fldm.ForcePageBreak = ForcePageBreakEnum.After;

        //                MySubReport.ReportName = "Camera Cards " + pageCount;
        //            }


        //                Field fld;

        //                //
        //                //SubjectName
        //                //
        //                string subjectName = "Name: _________________";
        //                double nameTextWidth = GetStringWidthInTwips(subjectName, "Tacoma", NameFontSize, false);
        //                double leftSide = hardLeft;
        //                if (colCnt > 0) leftSide += spaceBetweenItems;
        //                fld = s.Fields.Add("F" + allrowCnt + "subname", subjectName, leftSide, hardTop + NameTopMargin, cardWidth, 360);
        //                fld.Align = FieldAlignEnum.LeftTop;
        //                fld.CanGrow = true;
        //                fld.Font.Size = NameFontSize;
        //                fld.HideDuplicates = true;


        //                //
        //                //Barcode Stuff
        //                //
        //                string subjectID = TicketGenerator.GenerateTicketString(8);

        //                // Text Barcode is to the right of the SubjectName
        //                Field fld2;
        //                double barCodeStringTextWidth = GetStringWidthInTwips(subjectID, "Tacoma", otherFontSize, false);
        //                leftSide = hardLeft + barCodeStringLeftPadding;
        //                if (colCnt > 0) leftSide += spaceBetweenItems;
        //                fld2 = s.Fields.Add("Ftxt" + allrowCnt + "subIdView", subjectID, leftSide, hardTop + NameTopMargin, cardWidth, 360);
        //                fld2.Align = FieldAlignEnum.LeftTop;
        //                fld2.CanGrow = true;
        //                fld2.Font.Size = otherFontSize;
        //                fld2.HideDuplicates = true;

        //                //Actual Barcode takes up an entire row
        //                double barCodeTextWidth = GetStringWidthInTwips("*.NST." + subjectID + "*", barcodeFont, BarcodeFontSize, false);
        //                leftSide = hardLeft;
        //                if (colCnt > 0) leftSide += spaceBetweenItems;
        //                fld = s.Fields.Add("Fbar" + allrowCnt + "subId", "*.NST." + subjectID + "*", leftSide, hardTop + BarcodeTopMargin, cardWidth, 360);
        //                fld.Align = FieldAlignEnum.LeftTop;
        //                fld.Font.Name = barcodeFont;
        //                fld.CanGrow = true;
        //                fld.Font.Size = BarcodeFontSize;
        //                fld.HideDuplicates = true;

                        
        //                //
        //                // Organization Name
        //                //
        //                string orgName = flowProject.Organization.OrganizationName;

        //                double textWidth = GetStringWidthInTwips(orgName, "Tacoma", otherFontSize, false);
        //                leftSide = hardLeft;
        //                if (colCnt > 0) leftSide += spaceBetweenItems;
        //                fld = s.Fields.Add("F" + allrowCnt + "OrgName", orgName, leftSide, hardTop + BarcodeTopMargin + BarcodeFieldHeight, cardWidth, 360);
        //                fld.Align = FieldAlignEnum.LeftTop;
        //                fld.Font.Bold = true;
        //                fld.CanGrow = true;
        //                fld.Font.Size = otherFontSize;
        //                fld.HideDuplicates = true;

        //                //
        //                //Other Fields
        //                //
        //                int otherFieldCnt = 1;
        //                foreach (ProjectSubjectField psf in this.FlowReportView.SubjectFieldList)
        //                {
        //                    Field afld;
        //                    string FieldValue = "____________________________";

        //                    double aleftSide = (hardLeft);
        //                    if (colCnt > 0) aleftSide += spaceBetweenItems;
        //                    double atopSide = (hardTop + OtherTopMargin) + (smallFontFieldHeight * otherFieldCnt);
        //                    afld = s.Fields.Add("F" + allrowCnt + "_" + otherFieldCnt, psf.SubjectFieldDisplayName + ": " + FieldValue, aleftSide, atopSide, cardWidth, 360);
        //                    afld.Align = FieldAlignEnum.LeftTop;
        //                    afld.CanGrow = true;
        //                    afld.Font.Size = otherFontSize;
        //                    afld.HideDuplicates = true;
        //                    otherFieldCnt++;
        //                }


        //            //}
        //            if (colCnt < (pageCols - 1))
        //                colCnt++;
        //            else
        //            {
        //                colCnt = 0;
        //                rowCnt++;
        //            }
        //            if (rowCnt > (pageRows - 1))
        //            {

        //                rowCnt = 0;
        //            }
        //        }
        //    //}
        //}



        private void CreateBarcodeLayouts(FlowProject flowProject, bool newCardsOnly)
        {

           

            this.RecordsPerPage = 0; //this isnt true, but we need to lie about it ;-)
            string layout = flowProject.SelectedReportSubjectBarcodesLayout;
            bool doStackSort = flowProject.IsReportStackSorted;
            
            int ticketCount = flowProject.NewTicketBatchSize;

            //////////////////////////////////////////////
            // All Configurable Setting are here /////////
            //////////////////////////////////////////////

            //Default Values are for 30-up labels
            double marginLeft = 190;// 250;
            double marginTop = 440;
            double marginRight = 0;
            double marginBottom = 400;// 460;

            double NameTopMargin = 100;

            double barCodeStringLeftPadding = 2580;
            double spaceBetweenItems = 0;

            double pageW = 12740 - (marginLeft + marginRight);
            double pageH = 15800 - (marginTop + marginBottom);

            double pageRows = 10;
            double pageCols = 3;

            float NameFontSize = 10;
            float BarcodeFontSize = 18;
            float otherFontSize = 8;

            double NameFieldHeight = 250;
            double BarcodeFieldHeight = 370;// 300;
            double smallFontFieldHeight = 170;

            string barcodeFont = "BC C39 2 to 1 Medium";
            //string barcodeFont = "BC C39 2 to 1 Narrow";

            int itemsPerPage = 30;

            if (layout.ToLower().Contains("8-up"))
            {
                marginLeft = 720;
                marginTop = 720;
                marginRight = 720;
                marginBottom = 600;

                NameTopMargin = 100;

                barCodeStringLeftPadding = 3000;
                spaceBetweenItems = 100;

                pageW = 9540;
                pageH = 15840 - marginTop - marginBottom;
                
                pageRows = 4;
                pageCols = 2;

                NameFontSize = 14;
                //BarcodeFontSize = 22;
                BarcodeFontSize = 40;
                otherFontSize = 9;

                NameFieldHeight = 350;
                BarcodeFieldHeight = 400;
                smallFontFieldHeight = 200;

                //barcodeFont = "BC C39 2 to 1 Medium";
                barcodeFont = "BC C39 2 to 1 Narrow";

                itemsPerPage = 8;
            }
            if (layout.ToLower().Contains("10-up"))
            {
                marginLeft = 1220;
                marginTop = 0;
                marginRight = 220;
                marginBottom = 1200;

                NameTopMargin = 350;

                barCodeStringLeftPadding = 3420;
                spaceBetweenItems = 1340;

                pageW = 12240 - (marginLeft + marginRight + spaceBetweenItems);
                pageH = 16240 - marginTop - marginBottom;

                pageRows = 5;
                pageCols = 2;

                NameFontSize = 14;
                BarcodeFontSize = 40;
                otherFontSize = 9;

                NameFieldHeight = 350;
                BarcodeFieldHeight = 400;
                smallFontFieldHeight = 200;

                //barcodeFont = "BC C39 2 to 1 Medium";
                barcodeFont = "BC C39 2 to 1 Narrow";

                this.RecordsPerPage = 0;

                itemsPerPage = 10;
            }

            ///////////////////////////////////////////////
            ///////////////////////////////////////////////

            this.Layout.MarginBottom = 150;
            this.Layout.MarginLeft = marginLeft;
            this.Layout.MarginRight = marginRight;
            this.Layout.MarginTop = marginTop;



            //double left = _groupIndent;
            double hardLeft = 0;
            double hardTop = 0;
            double rowCnt = 0;
            double colCnt = 0;

            double cardWidth = pageW / pageCols;
            double cardHeight = pageH / pageRows;


            double BarcodeTopMargin = NameTopMargin + NameFieldHeight;
            double OrganizaionTopMargin = BarcodeTopMargin + BarcodeFieldHeight;
            double OtherTopMargin = OrganizaionTopMargin;

           
            C1.C1Report.Section sh = this.Sections[SectionTypeEnum.Detail];
            sh.Visible = true;
            sh.CanGrow = true;
            sh.Repeat = false;

            List<Subject> origSubjectList = new List<Subject>();
            for (int x = 0; x < (flowProject.SubjectList.View.Count); x++)
            {
                for (int q = 0; q < flowProject.SubjectCopies; q++)
                    origSubjectList.Add((Subject)flowProject.SubjectList.View.GetItemAt(x));
            }

            List<Subject> subjectList = new List<Subject>();
            int loopsToDo = 0;
            if (newCardsOnly)
            {
                loopsToDo = ticketCount;
            }
            else
            {
                if (doStackSort)
                {
                    int itemsInList = origSubjectList.Count;
                    int iterationsNeeded = Convert.ToInt32(Math.Ceiling((double)itemsInList / itemsPerPage));
                    for (int x = 0; x < iterationsNeeded; x++)
                    {
                        for (int j = 0; j < itemsPerPage; j++)
                        {
                            int targetIndex = x + (j * (iterationsNeeded));
                            if (targetIndex < itemsInList)
                                subjectList.Add((Subject)origSubjectList[targetIndex]);
                            else
                                subjectList.Add(null);
                        }
                    }

                }
                else
                {
                    subjectList = origSubjectList;
                }
                loopsToDo = subjectList.Count();
            }

            C1.C1Report.Section s = null; 
                int allrowCnt = 0;
                int pageCount = 0;
                string lastBreakFieldValue = "";
                for (int i = 0; i < loopsToDo; i++)
                {
                    
                    string subjectName = "Name: _________________";
                    string ticketCode = TicketGenerator.GenerateTicketString(8);
                    Subject sub = null;
                    if (!newCardsOnly)
                    {
                        sub = subjectList[i];

                       

                        if (sub != null)
                        {
                            subjectName = sub.FormalFullName;

                            if (sub.TicketCode == null || sub.TicketCode.Length == 0)
                            {
                                sub.TicketCode = ticketCode;
                                flowProject.FlowProjectDataContext.SubmitChanges();
                            }
                            else
                            {
                                ticketCode = sub.TicketCode;
                            }
                        }
                    }

                    bool forcePageBreak = false;
                    if (flowProject.SelectedPageBreakField != null && !flowProject.IsReportStackSorted)
                    {
                        //string value = typeof(Subject).GetProperty(flowProject.SelectedPageBreakField.ProjectSubjectFieldDisplayName).GetValue(sub,null).ToString();

                        SubjectDatum result = (sub == null) ? null : sub.SubjectData.FirstOrDefault(sd => sd.FieldDesc == flowProject.SelectedPageBreakField.ProjectSubjectFieldDesc);
                        string value = (result == null || result.Value == null) ? null : result.Value.ToString();

                        if (i == 0)
                            lastBreakFieldValue = value;

                        if (i > 0 && value != lastBreakFieldValue)
                        {
                            //page break
                            forcePageBreak = true;
                            lastBreakFieldValue = value;
                        }

                    }

                       

                       
                        if (rowCnt == 0 && colCnt == 0 || forcePageBreak == true)
                        {
                            pageCount++;
                            
                            
                            Field fldm = sh.Fields.Add("FSummary " + pageCount, "Camera Cards", 0, 0, 1, 1);
                            if (forcePageBreak == true && (rowCnt > 0 || colCnt > 0))
                            {
                                fldm.ForcePageBreak = ForcePageBreakEnum.Before;
                                rowCnt = 0;
                                colCnt = 0;
                            }
                            fldm.ZOrder = pageCount;
                            fldm.Visible = true;
                            fldm.CanGrow = true;
                            fldm.Font.Bold = true;
                            fldm.Align = FieldAlignEnum.LeftTop;

                            C1Report MySubReport = new C1Report();
                            s = MySubReport.Sections[SectionTypeEnum.Detail];
                            s.Repeat = false;
                            s.Visible = true;

                            fldm.Subreport = MySubReport;
                            fldm.ForcePageBreak = ForcePageBreakEnum.After;

                            MySubReport.ReportName = "Camera Cards " + pageCount;
                            
                        }


                        allrowCnt++;
                        hardLeft = colCnt * cardWidth;
                        hardTop = rowCnt * (cardHeight - 0);


                        if (sub != null || newCardsOnly)
                        {
                            Field fld;

                            //
                            //SubjectName
                            //
                            //string subjectName = "Name: _________________";
                            //string subjectName = sub.FormalFullName;
                            double nameTextWidth = GetStringWidthInTwips(subjectName, "Tacoma", NameFontSize, false);
                            double leftSide = hardLeft;
                            if (colCnt > 0) leftSide += spaceBetweenItems;
                            fld = s.Fields.Add("F" + allrowCnt + "subname", subjectName, leftSide, hardTop + NameTopMargin, cardWidth, 360);
                            fld.Align = FieldAlignEnum.LeftTop;
                            fld.CanGrow = true;
                            fld.Font.Size = NameFontSize;
                            fld.HideDuplicates = true;


                            //
                            //Barcode Stuff
                            //
                            //string subjectID = TicketGenerator.GenerateTicketString(8);
                            //string subjectID = sub.SubjectID.ToString();
                            // Text Barcode is to the right of the SubjectName
                            Field fld2;
                            double barCodeStringTextWidth = GetStringWidthInTwips(ticketCode, "Tacoma", otherFontSize, false);
                            leftSide = hardLeft + barCodeStringLeftPadding;
                            if (colCnt > 0) leftSide += spaceBetweenItems;
                            fld2 = s.Fields.Add("Ftxt" + allrowCnt + "subIdView", ticketCode, leftSide, hardTop + NameTopMargin, cardWidth, 360);
                            fld2.Align = FieldAlignEnum.LeftTop;
                            fld2.CanGrow = true;
                            fld2.Font.Size = otherFontSize;
                            fld2.HideDuplicates = true;

                            //Actual Barcode takes up an entire row
                            double barCodeTextWidth = GetStringWidthInTwips("*.NST." + ticketCode + "*", barcodeFont, BarcodeFontSize, false);
                            leftSide = hardLeft;
                            if (colCnt > 0) leftSide += spaceBetweenItems;
                            fld = s.Fields.Add("Fbar" + allrowCnt + "subId", "*.NST." + ticketCode + "*", leftSide, hardTop + BarcodeTopMargin, cardWidth, 360);
                            fld.Align = FieldAlignEnum.LeftTop;
                            fld.Font.Name = barcodeFont;
                            fld.CanGrow = true;
                            fld.Font.Size = BarcodeFontSize;
                            fld.HideDuplicates = true;


                            //
                            // Organization Name
                            //
                            string orgName = flowProject.Organization.OrganizationName;
                            double textWidth = GetStringWidthInTwips(orgName, "Tacoma", otherFontSize, false);
                            leftSide = hardLeft;
                            if (colCnt > 0) leftSide += spaceBetweenItems;
                            fld = s.Fields.Add("F" + allrowCnt + "OrgName", orgName, leftSide, hardTop + BarcodeTopMargin + BarcodeFieldHeight, cardWidth, 360);
                            fld.Align = FieldAlignEnum.LeftTop;
                            fld.Font.Bold = true;
                            fld.CanGrow = true;
                            fld.Font.Size = otherFontSize;
                            fld.HideDuplicates = true;

                            //
                            //Other Fields
                            //
                            int otherFieldCnt = 1;
                            foreach (ProjectSubjectField psf in this.FlowReportView.SubjectFieldList)
                            {
                                Field afld;
                                string FieldValue = "____________________________";
                                if (!newCardsOnly)
                                {
                                    if (sub.SubjectData[psf.SubjectFieldDisplayName] != null && sub.SubjectData[psf.SubjectFieldDisplayName].Value != null)
                                        FieldValue = sub.SubjectData[psf.SubjectFieldDisplayName].Value.ToString();
                                    else
                                        FieldValue = "";
                                }
                                double aleftSide = (hardLeft);
                                if (colCnt > 0) aleftSide += spaceBetweenItems;
                                double atopSide = (hardTop + OtherTopMargin) + (smallFontFieldHeight * otherFieldCnt);
                                afld = s.Fields.Add("F" + allrowCnt + "_" + otherFieldCnt, psf.SubjectFieldDisplayName + ": " + FieldValue, aleftSide, atopSide, cardWidth, 360);
                                afld.Align = FieldAlignEnum.LeftTop;
                                afld.CanGrow = true;
                                afld.Font.Size = otherFontSize;
                                afld.HideDuplicates = true;
                                otherFieldCnt++;
                            }
                        }
                           
                       
                    //}
                    if (colCnt < (pageCols - 1))
                        colCnt++;
                    else
                    {
                        colCnt = 0;
                        rowCnt++;
                    }
                    if (rowCnt > (pageRows - 1))
                    {
                        
                        rowCnt = 0;
                        
                    }
                }
            //}
        }



        private void CreateGroups(DataTable dataSource, bool doGroupSort, bool showGroupByValue, bool showPrice)
		{
			IEnumerable<DataColumn> groupFields = dataSource.Columns.Cast<DataColumn>().Where(c =>
				c.ExtendedProperties.ContainsKey("IsGroupField")
			);

			int groupFieldCount = groupFields.Count();

			_groupIndent = groupFieldCount * 200;

			for (int i = 0; i < groupFieldCount; i++)
			{
				DataColumn field = groupFields.ElementAt(i);

				bool isLastGroupLevel = (i == groupFieldCount - 1);

                this.CreateGroup(i + 1, field, dataSource, isLastGroupLevel, doGroupSort, showGroupByValue, showPrice);
			}
		}

        private void CreateGroup(int level, DataColumn field, DataTable dataSource, bool isLastGroupLevel, bool doGroupSort, bool showGroupByValue, bool showPrice)
		{

            int topSpacing = 0;
            SortEnum sortEnum = SortEnum.NoSort;
            if (doGroupSort)
                sortEnum = SortEnum.Ascending;

			Field fld;
            C1.C1Report.Group group = this.Groups.Add("G" + field.ColumnName, field.ColumnName, sortEnum);
			C1.C1Report.Section groupHeader = group.SectionHeader;
			C1.C1Report.Section groupFooter = group.SectionFooter;
			
			groupHeader.Height = 375;
			groupHeader.Visible = true;
			groupHeader.Repeat = true;

			groupFooter.Height = 500;
			groupFooter.Visible = true;
            groupFooter.ForcePageBreak = ForcePageBreakEnum.After;

            if (showGroupByValue)
            {
                fld = groupHeader.Fields.Add(
                    "GH" + field.ColumnName,
                    "\"" + field.ColumnName + ": \" & " + field.ColumnName,
                    level * 200, topSpacing, 4 * 1440, 250
                );
            }
            else
            {
                fld = groupHeader.Fields.Add(
                   "GH" + field.ColumnName,
                   "",
                   level * 200, topSpacing, 4 * 1440, 250
               );
            }
            fld.Calculated = true;
            fld.Font.Bold = true;
            fld.Align = FieldAlignEnum.LeftTop;
            

			double left = _groupIndent;

			foreach (DataColumn column in dataSource.Columns)
			{
                if (column.ExtendedProperties.ContainsKey("HideColumn"))
                    continue;

				double columnWidthInTwips = this.GetColumnWidthInTwips(column, 6, false);

                if (!column.ExtendedProperties.ContainsKey("IsGroupField") && !column.ExtendedProperties.ContainsKey("HideColumn"))
				{
					if (isLastGroupLevel)
					{
                        fld = groupFooter.Fields.Add("FldLine", "", 0, topSpacing, this.Layout.Width, 20);
						fld.LineSlant = LineSlantEnum.NoSlant;
						fld.BorderStyle = BorderStyleEnum.Solid;
						fld.BorderColor = Colors.Black;

						fld = groupHeader.Fields.Add("GH" + level.ToString() + "F" + column.Ordinal.ToString(), column.Caption, left, 250, columnWidthInTwips, 250);
						fld.Font.Underline = true;

						if (column.ExtendedProperties.ContainsKey("IsNumeric"))
							fld.Align = FieldAlignEnum.RightTop;
					}

					if (showPrice && column.ExtendedProperties.ContainsKey("CalculationExpression"))
					{
						string calculationExpression = column.ExtendedProperties["CalculationExpression"].ToString();

                        if (showGroupByValue)
                        {
                            fld = groupFooter.Fields.Add(
                                "GF" + field.ColumnName,
                                "\"Totals for " + field.ColumnName + ": \" & " + field.ColumnName,
                                level * 200, topSpacing, 4 * 1440, 250
                            );
                        }
                        else
                        {
                            fld = groupFooter.Fields.Add(
                                "GF" + field.ColumnName,
                                "Total: ",
                                level * 200, topSpacing, 4 * 1440, 250
                            );
                        }
						fld.Calculated = true;
						fld.Font.Bold = true;
						fld.Align = FieldAlignEnum.LeftTop;

                        fld = groupFooter.Fields.Add("GF" + level.ToString() + "F" + column.Ordinal.ToString(), calculationExpression, left, topSpacing, columnWidthInTwips, 250);
						fld.Calculated = true;
						fld.Font.Bold = true;
						fld.Align = FieldAlignEnum.RightTop;

						if (column.ExtendedProperties.ContainsKey("Format"))
							fld.Format = column.ExtendedProperties["Format"].ToString();
					}

					left += columnWidthInTwips;
				}
			}
            
		}

		public C1Report Generate()
		{
            this.Clear();
			DataTable dataSource = this.FlowReportView.DataSource;

            bool showFooter = this.FlowReportView.FlowProject.IsReportShowFooter;

            if (dataSource != null)
				this.DataSource.Recordset = dataSource.DefaultView.ToTable();

			//this.CreateReportHeader(dataSource);

            //Catalog Packages
            if (this.FlowReportView.ReportName == "Order Form")
            {
                if(showFooter) this.CreatePageFooter();
                this.CreateDetail(this.FlowReportView.FlowProject);
                if (showFooter) this.CreateReportFooter(dataSource);
            }
            //else if (this.FlowReportView.ReportName == "Catalog Packages")
            //{
            //    this.CreatePageFooter();
            //    this.CreateCatalogPackages(this.FlowReportView.FlowProject);
            //    this.CreateReportFooter(dataSource);

            //}
            else if (this.FlowReportView.ReportName == "Subject Barcodes")
            {
                
                if (showFooter) this.CreatePageFooter(600, 180, false);
                this.CreateBarcodeLayouts(this.FlowReportView.FlowProject, false);
                
            }
            else if (this.FlowReportView.ReportName == "New Ticket Barcodes")
            {
                if (showFooter) this.CreatePageFooter(600, 180, false);
                this.CreateBarcodeLayouts(this.FlowReportView.FlowProject, true);

            }
            else
            {
                if (showFooter) this.CreatePageFooter();
                this.CreateReportHeader(dataSource);
                this.CreateDetail(dataSource, DoGroupSort, ShowGroupByValue, this.FlowReportView.FlowProject.ReportShowPrice);
                if (showFooter) this.CreateReportFooter(dataSource);
            }

			

			return this;
		}

        private double GetStringWidthInTwips(string text, string font, float size, bool isBold)
        {

                string buffer = new string('X', text.Length);

                Typeface type = new Typeface("font");

                FormattedText f = new FormattedText(buffer, CultureInfo.InvariantCulture, FlowDirection.LeftToRight, type, size, System.Windows.Media.Brushes.Black);
                if (isBold)
                    f.SetFontWeight(FontWeights.Bold);

                double widthInTwips = f.WidthIncludingTrailingWhitespace * (1440d / 96d);

                return widthInTwips;
        }


		private double GetColumnWidthInTwips(DataColumn column, int padding, bool isBold)
		{
			if (!column.ExtendedProperties.ContainsKey("WidthInTwips"))
			{
                int maxColumnLength = padding;
                if (column.ExtendedProperties["MaxTextLength"] != null)
                {
                    maxColumnLength = ((int)column.ExtendedProperties["MaxTextLength"]) + padding;
                } 
                string buffer = new string('X', maxColumnLength);

                Typeface type = new Typeface("Tahoma");
               
                


                FormattedText f = new FormattedText(buffer, CultureInfo.InvariantCulture, FlowDirection.LeftToRight, type, 9, System.Windows.Media.Brushes.Black);
                if (isBold)
                    f.SetFontWeight(FontWeights.Bold);

                double widthInTwips = f.WidthIncludingTrailingWhitespace * (1440d / 96d);

                column.ExtendedProperties.Add("WidthInTwips", widthInTwips);

               
			}

			return (double)column.ExtendedProperties["WidthInTwips"];
		}

	}	// END class

}	// END namespace
