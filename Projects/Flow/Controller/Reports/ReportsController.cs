﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Xps.Packaging;

using Flow.Controller.Reports;
using Flow.Lib;
using Flow.Schema.LinqModel.DataContext;
using Flow.Schema.Reports;
using Flow.View.Reports;
using Flow.View.Dialogs;
using System.Windows.Documents;

using NLog;
using System.Collections;
using System.Drawing.Printing;
using C1.C1Preview;
using C1.C1Report;

namespace Flow.Controller
{
	public class ReportsController
	{
        private static Logger logger = LogManager.GetCurrentClassLogger();

		protected FlowController FlowController { get; set; }
		protected ReportsDockPanel ReportsDockPanel { get; set; }

        public C1PrintDocument thisDoc { get; set; }
		//public FlowReportView FlowReportView { get; protected set; }

		private FlowProject CurrentFlowProject
		{
			get { return this.ReportsDockPanel.DataContext as FlowProject; }
		}

		internal void Main(FlowController flowController, ReportsDockPanel pnl)
		{
			this.FlowController = flowController;
			this.ReportsDockPanel = pnl;

			this.ReportsDockPanel.DataContext = this.FlowController.ProjectController.CurrentFlowProject;

			this.ReportsDockPanel.ReportComboBox.SelectionChanged += new SelectionChangedEventHandler(ReportComboBox_SelectionChanged);
			this.ReportsDockPanel.ReportViewComboBox.SelectionChanged += new SelectionChangedEventHandler(ReportViewComboBox_SelectionChanged);

			this.ReportsDockPanel.SaveViewAsButton.Click += new RoutedEventHandler(SaveViewAsButton_Click);
			this.ReportsDockPanel.ConfirmSaveReportViewButton.Click += new RoutedEventHandler(ConfirmSaveReportViewButton_Click);
			this.ReportsDockPanel.CancelSaveReportViewButton.Click += new RoutedEventHandler(CancelSaveReportViewButton_Click);

			this.ReportsDockPanel.ReportPreviewButton.Click += new RoutedEventHandler(ReportPreviewButton_Click);
			//this.ReportsDockPanel.ReportExportButton.Click += new RoutedEventHandler(ReportExportButton_Click);

			this.ReportsDockPanel.ReportViewComboBox.SelectedIndex = 0;

			this.FlowController.ProjectController.PropertyChanged += new PropertyChangedEventHandler(ProjectController_PropertyChanged);

            this.ReportsDockPanel.PrintButtonInvoked += new EventHandler(ReportsDockPanel_PrintButtonInvoked);
            this.ReportsDockPanel.PdfButtonInvoked += new EventHandler(ReportsDockPanel_PdfButtonInvoked);
            this.ReportsDockPanel.CsvButtonInvoked += new EventHandler(ReportsDockPanel_CsvButtonInvoked);
            
		}

        void ReportsDockPanel_PrintButtonInvoked(object sender, object e)
        {
            if (thisDoc == null)
            {
                FlowMessageBox msg = new FlowMessageBox("Print", "Preview your report before you try and print it");
                msg.CancelButtonVisible = false;
                msg.ShowDialog();
                return;
            }
            this.thisDoc.PrintDialog();
        }


		void ProjectController_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			if (e.PropertyName == "CurrentFlowProject")
			{
				this.ReportsDockPanel.ReportDocumentViewer.Document = null;
				
				this.ReportsDockPanel.DataContext = this.FlowController.ProjectController.CurrentFlowProject;

				this.ReportsDockPanel.ReportViewComboBox.SelectedIndex = 0;
			}
		}

		void ReportComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			this.ReportsDockPanel.SaveViewDockPanel.Visibility = Visibility.Collapsed;
			this.ReportsDockPanel.ReportViewComboBox.SelectedIndex = 0;
		}

		void ReportViewComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			this.ReportsDockPanel.SaveViewDockPanel.Visibility = Visibility.Collapsed;
		}
		
		void SaveViewAsButton_Click(object sender, RoutedEventArgs e)
		{
            logger.Info("Save View As button clicked");

			this.ReportsDockPanel.SaveViewDockPanel.Visibility = Visibility.Visible;
			this.ReportsDockPanel.ReportViewNameTextBox.Focus();
		}

		void ConfirmSaveReportViewButton_Click(object sender, RoutedEventArgs e)
		{
            logger.Info("Confirm Save Report View button clicked");

			if (this.ReportsDockPanel.ReportViewComboBox.SelectedIndex == 0)
			{
				this.CurrentFlowProject.SaveReportView(this.ReportsDockPanel.SelectedReportView);
				this.ReportsDockPanel.ReportViewComboBox.SelectedIndex = this.ReportsDockPanel.ReportViewComboBox.Items.Count - 1;
			}
			else
				this.CurrentFlowProject.SaveReportViews();
	
			this.ReportsDockPanel.SaveViewDockPanel.Visibility = Visibility.Collapsed;
		}

		void CancelSaveReportViewButton_Click(object sender, RoutedEventArgs e)
		{
            logger.Info("Cancel Save Report View button clicked");

			this.ReportsDockPanel.SaveViewDockPanel.Visibility = Visibility.Collapsed;
		}

		void ReportPreviewButton_Click(object sender, RoutedEventArgs e)
		{
            logger.Info("Report Preview button clicked");

            if (this.ReportsDockPanel.SelectedReportView == null)
            {
                this.ReportsDockPanel.ReportViewComboBox.SelectedIndex = 0;
                if (this.ReportsDockPanel.SelectedReportView == null)
                {
                    FlowMessageBox msg = new FlowMessageBox("Report View", "Please select a report to view");
                    msg.CancelButtonVisible = false;
                    msg.ShowDialog();
                    return;
                }
            }
            if (this.CurrentFlowProject.SelectedReportName == "Catalog Packages" && this.CurrentFlowProject.FlowCatalog == null)
            {
                FlowMessageBox msg = new FlowMessageBox("No Catalog", "There is no catalog assigned to this project");
                msg.CancelButtonVisible = false;
                msg.ShowDialog();
                return;
            }

            if (this.CurrentFlowProject.SelectedReportName == "Catalog Packages" && this.CurrentFlowProject.FlowCatalog != null)
            {
                thisDoc = CustomReports.GetCatalogReport(this.CurrentFlowProject.FlowCatalog, this.CurrentFlowProject.ReportShowBarCodes, this.CurrentFlowProject.ReportShowProducts, this.CurrentFlowProject.ReportShowImageOptions);
                this.ReportsDockPanel.ReportDocumentViewer.Document = thisDoc.FixedDocumentSequence;
                return;
            }

            if (this.CurrentFlowProject.SelectedReportName == "Lab Billing Report" && this.CurrentFlowProject.FlowCatalog != null)
            {
                Dictionary<string, Dictionary<string, List<object>>> AllUnitCatagories = null;
                thisDoc = CustomReports.GetBillingReport(this.CurrentFlowProject, this.CurrentFlowProject.SelectedReportOrderFilter, null,  null, null, out AllUnitCatagories);
                
                this.ReportsDockPanel.ReportDocumentViewer.Document = thisDoc.FixedDocumentSequence;
                return;
            }

            if (this.CurrentFlowProject.SelectedReportName == "Slate Sheet 3up")
            {
                C1Report thisReport = CustomReports.GetSlateReport(this.CurrentFlowProject);
                thisDoc = thisReport.C1Document;
                this.ReportsDockPanel.ReportDocumentViewer.Document = thisDoc.FixedDocumentSequence;
                return;
            }

            if (this.CurrentFlowProject.SelectedReportName.StartsWith(">"))
            {
                C1Report thisReport = CustomReports.GetCustomReport(this.CurrentFlowProject, this.CurrentFlowProject.SelectedReportName.Substring(2));
                thisDoc = thisReport.C1Document;
                this.ReportsDockPanel.ReportDocumentViewer.Document = thisDoc.FixedDocumentSequence;
                return;
            }
			FlowProject currentFlowProject = this.FlowController.ProjectController.CurrentFlowProject;

			this.ReportsDockPanel.ReportDocumentViewer.Cursor = Cursors.Wait;

			foreach (Subject s in currentFlowProject.SubjectList)
			{
                try
                {
                    s.SetProductOrder(currentFlowProject.FlowCatalog);
                }
                catch (Exception ex)
                {
                    logger.Error("Error occured while setting product order for subject '{0}'", s.FormalFullName);
                    throw;
                }
			}

			this.ReportsDockPanel.SelectedReportView.ReportName = this.CurrentFlowProject.SelectedReportName;

            int tryCnt = 0;
            while (tryCnt++ < 5)
            {
                try
                {

                    ReportDataDefinitions.GetReportData(this.ReportsDockPanel.SelectedReportView, null);
                    //ReportDataDefinitions.GetReportData(this.ReportsDockPanel.SelectedReportView);


                    C1Report newReport = new FlowReport(this.ReportsDockPanel.SelectedReportView).Generate();
                    thisDoc = newReport.C1Document;
                    this.ReportsDockPanel.ReportDocumentViewer.Document = thisDoc.FixedDocumentSequence;
                    //this.ReportsDockPanel.ReportDocumentViewer.Document = thisDoc.FixedDocumentSequence; // thisDoc.FixedDocumentSequence;

                    this.ReportsDockPanel.ReportDocumentViewer.Cursor = Cursors.Arrow;
                    tryCnt = 100;
                }
                catch
                {

                }
            }

           
            
		}

		void ReportExportButton_Click(object sender, RoutedEventArgs e)
		{
            logger.Info("Report Export button clicked");

            if (this.ReportsDockPanel.SelectedReportView == null)
            {
                this.ReportsDockPanel.ReportViewComboBox.SelectedIndex = 0;
                if (this.ReportsDockPanel.SelectedReportView == null)
                {
                    FlowMessageBox msg = new FlowMessageBox("Report View", "Please select a report to view");
                    msg.CancelButtonVisible = false;
                    msg.ShowDialog();
                    return;
                }
            }

			FlowProject currentFlowProject = this.FlowController.ProjectController.CurrentFlowProject;

            if (this.CurrentFlowProject.SelectedReportName == "Catalog Packages" && this.CurrentFlowProject.FlowCatalog != null)
            {
                C1PrintDocument thisReport = CustomReports.GetCatalogReport(this.CurrentFlowProject.FlowCatalog, this.CurrentFlowProject.ReportShowBarCodes, this.CurrentFlowProject.ReportShowProducts, this.CurrentFlowProject.ReportShowImageOptions);
                ReportDataDefinitions.ExportReport(
                    this.ReportsDockPanel.SelectedReportView.ReportTitle + " - " + currentFlowProject.FlowProjectName,
                    thisReport,
                    this.ReportsDockPanel.SelectedExportFormat,
                    null, true
                );
                return;
            }

            if (this.CurrentFlowProject.SelectedReportName == "Lab Billing Report" && this.CurrentFlowProject.FlowCatalog != null)
            {
                Dictionary<string, Dictionary<string, List<object>>> AllUnitCatagories = null;
                C1PrintDocument thisReport = CustomReports.GetBillingReport(this.CurrentFlowProject, this.CurrentFlowProject.SelectedReportOrderFilter, null, null, null, out AllUnitCatagories);
                ReportDataDefinitions.ExportReport(
                    this.ReportsDockPanel.SelectedReportView.ReportTitle + " - " + currentFlowProject.FlowProjectName,
                    thisReport,
                    this.ReportsDockPanel.SelectedExportFormat,
                    null, true
                );
                return;
            }

			foreach (Subject s in currentFlowProject.SubjectList)
			{
				s.SetProductOrder(currentFlowProject.FlowCatalog);
			}

			this.ReportsDockPanel.SelectedReportView.ReportName = this.CurrentFlowProject.SelectedReportName;
            int tryCnt = 0;
            while (tryCnt++ < 5)
            {
                try
                {

                
            ReportDataDefinitions.GetReportData(this.ReportsDockPanel.SelectedReportView, FlowController.ProjectController.CurrentFlowProject.FlowProjectDataContext.FilteredOrderPackages.ToList());

			FlowReport report = new FlowReport(this.ReportsDockPanel.SelectedReportView);
			report.Generate();

			ReportDataDefinitions.ExportReport(
				this.ReportsDockPanel.SelectedReportView.ReportTitle + " - " + currentFlowProject.FlowProjectName,
                report.C1Document,
				this.ReportsDockPanel.SelectedExportFormat,
                null, true
			);
            tryCnt = 100;
                }
                catch
                {

                }
            }

		}

        void ReportsDockPanel_PdfButtonInvoked(object sender, object e)
        {

            KeyValuePair<string, string> exportFormat = new KeyValuePair<string, string>("PDF", "Adobe PDF (*.pdf)|*.pdf");

            logger.Info("PDF Report Export button clicked");

            if (this.ReportsDockPanel.SelectedReportView == null)
            {
                this.ReportsDockPanel.ReportViewComboBox.SelectedIndex = 0;
                if (this.ReportsDockPanel.SelectedReportView == null)
                {
                    FlowMessageBox msg = new FlowMessageBox("Report View", "Please select a report to view");
                    msg.CancelButtonVisible = false;
                    msg.ShowDialog();
                    return;
                }
            }

            FlowProject currentFlowProject = this.FlowController.ProjectController.CurrentFlowProject;

            if (this.CurrentFlowProject.SelectedReportName == "Catalog Packages" && this.CurrentFlowProject.FlowCatalog != null)
            {
                C1PrintDocument thisReport = CustomReports.GetCatalogReport(this.CurrentFlowProject.FlowCatalog, this.CurrentFlowProject.ReportShowBarCodes, this.CurrentFlowProject.ReportShowProducts, this.CurrentFlowProject.ReportShowImageOptions);
                ReportDataDefinitions.ExportReport(
                    this.ReportsDockPanel.SelectedReportView.ReportTitle + " - " + currentFlowProject.FlowProjectName,
                    thisReport,
                    exportFormat,
                    null, true
                );
                return;
            }

            if (this.CurrentFlowProject.SelectedReportName == "Lab Billing Report" && this.CurrentFlowProject.FlowCatalog != null)
            {
                Dictionary<string, Dictionary<string, List<object>>> AllUnitCatagories = null;
                C1PrintDocument thisReport = CustomReports.GetBillingReport(this.CurrentFlowProject, this.CurrentFlowProject.SelectedReportOrderFilter, null, null, null, out AllUnitCatagories);
                ReportDataDefinitions.ExportReport(
                    this.ReportsDockPanel.SelectedReportView.ReportTitle + " - " + currentFlowProject.FlowProjectName,
                    thisReport,
                    exportFormat,
                    null, true
                );
                return;
            }


            if (this.CurrentFlowProject.SelectedReportName.StartsWith(">"))
            {
                C1Report thisReport = CustomReports.GetCustomReport(this.CurrentFlowProject, this.CurrentFlowProject.SelectedReportName.Substring(2));
                thisDoc = thisReport.C1Document;
                this.ReportsDockPanel.ReportDocumentViewer.Document = thisDoc.FixedDocumentSequence;
                ReportDataDefinitions.ExportReport(
                    this.ReportsDockPanel.SelectedReportView.ReportTitle + " - " + currentFlowProject.FlowProjectName,
                    thisReport.C1Document,
                    exportFormat,
                    null, true
                );
                return;
            }
            foreach (Subject s in currentFlowProject.SubjectList)
            {
                s.SetProductOrder(currentFlowProject.FlowCatalog);
            }

            int tryCnt = 0;
            while (tryCnt++ < 5)
            {
                try
                {

                
            this.ReportsDockPanel.SelectedReportView.ReportName = this.CurrentFlowProject.SelectedReportName;

            ReportDataDefinitions.GetReportData(this.ReportsDockPanel.SelectedReportView, FlowController.ProjectController.CurrentFlowProject.FlowProjectDataContext.FilteredOrderPackages.ToList());

            FlowReport report = new FlowReport(this.ReportsDockPanel.SelectedReportView);
            report.Generate();

            ReportDataDefinitions.ExportReport(
                this.ReportsDockPanel.SelectedReportView.ReportTitle + " - " + currentFlowProject.FlowProjectName,
                report.C1Document,
                exportFormat,
                null, true
            );
            tryCnt = 100;
                }
                catch
                {

                }
            }
        }

        void ReportsDockPanel_CsvButtonInvoked(object sender, object e)
        {

            if (thisDoc == null)
            {
                FlowMessageBox msg = new FlowMessageBox("Print", "Preview your report before you try and export a csv");
                msg.CancelButtonVisible = false;
                msg.ShowDialog();
                return;
            }

            KeyValuePair<string, string> exportFormat = new KeyValuePair<string, string>("CSV", "(*.csv)|*.csv");

            logger.Info("CSV Report Export button clicked");

            object obj = this.thisDoc.DataSchema;

            if (this.ReportsDockPanel.SelectedReportView == null)
            {
                this.ReportsDockPanel.ReportViewComboBox.SelectedIndex = 0;
                if (this.ReportsDockPanel.SelectedReportView == null)
                {
                    FlowMessageBox msg = new FlowMessageBox("Report View", "Please select a report to view");
                    msg.CancelButtonVisible = false;
                    msg.ShowDialog();
                    return;
                }
            }

            FlowProject currentFlowProject = this.FlowController.ProjectController.CurrentFlowProject;


            
            if (this.CurrentFlowProject.SelectedReportName == "Lab Billing Report" && this.CurrentFlowProject.FlowCatalog != null)
            {
                Dictionary<string, Dictionary<string, List<object>>> AllUnitCatagories = null;
                C1PrintDocument thisReport = CustomReports.GetBillingReport(this.CurrentFlowProject, this.CurrentFlowProject.SelectedReportOrderFilter, null, null, null, out AllUnitCatagories);
                ReportDataDefinitions.ExportCSVReport(
                    this.ReportsDockPanel.SelectedReportView.ReportTitle + " - " + currentFlowProject.FlowProjectName,
                    AllUnitCatagories,
                    exportFormat,
                    null, true
                );
                return;
            }

            foreach (Subject s in currentFlowProject.SubjectList)
            {
                s.SetProductOrder(currentFlowProject.FlowCatalog);
            }

            this.ReportsDockPanel.SelectedReportView.ReportName = this.CurrentFlowProject.SelectedReportName;
            int tryCnt = 0;
            while (tryCnt++ < 5)
            {
                try
                {

                
            ReportDataDefinitions.GetReportData(this.ReportsDockPanel.SelectedReportView, FlowController.ProjectController.CurrentFlowProject.FlowProjectDataContext.FilteredOrderPackages.ToList());

            FlowReport report = new FlowReport(this.ReportsDockPanel.SelectedReportView);
            //report.Generate();
            if (report.FlowReportView.DataSource == null)
            {
                FlowMessageBox msg = new FlowMessageBox("Report View", "The Report you selected can not be exported as csv.");
                msg.CancelButtonVisible = false;
                msg.ShowDialog();
                return;
            }
            ReportDataDefinitions.ExportCSVReport(
                this.ReportsDockPanel.SelectedReportView.ReportTitle + " - " + currentFlowProject.FlowProjectName,
                report.FlowReportView.DataSource,
                exportFormat,
                null, true
            );
            tryCnt = 100;
                }
                catch
                {

                }
            }
        }

	}
}
