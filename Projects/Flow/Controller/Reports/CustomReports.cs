﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.Schema.LinqModel.DataContext;
using C1.C1Preview;
using C1.C1Report;
using Flow.Lib.Helpers;
using System.Drawing.Drawing2D;
using System.Xml;
using Flow.Controller.Project;
using System.Data;
using System.IO;
using Flow.Lib;
using System.Drawing;
using System.Drawing.Printing;

namespace Flow.Controller.Reports
{
    public class CustomReports
    {

        public static XmlDocument GetXMLBillingReport(FlowProject flowProject, string orderID, List<OrderPackage> CurrentOrderPackages, List<SubjectImagePlus> OrderSubjectImages, Organization shipOrg, XmlElement OrderFormXML)
        {
            //lets first build our datasource
            //if (string.IsNullOrEmpty(orderID))
            //    orderID = orderFilter;

            //string orderFilterString = "ALL";
            //bool onlineOrders = false;
            //if (orderFilter == "New Online Orders")
            //{
            //    onlineOrders = true;
            //    orderFilterString = null;
            //}
            //if (orderFilter == "New Orders")
            //{
            //    orderFilterString = null;
            //}
            //if (orderFilter.Contains(" - "))
            //{
            //    orderFilterString = orderFilter.Split(" - ")[0];
            //}


            Dictionary<string, Dictionary<string, List<object>>> AllUnitCatagories = new Dictionary<string, Dictionary<string, List<object>>>();
            List<ImageQuixProduct> allProducts = new List<ImageQuixProduct>();

            List<string> allIQOrderIDs = new List<string>();
            List<string> allWebOrderIDs = new List<string>();
            List<string> allWebAuthTokens = new List<string>();
            List<string> allNotes = new List<string>();

            foreach (OrderPackage op in CurrentOrderPackages)
            {
                if (op.SubjectOrder.IsMarkedForDeletion)
                    continue;

                if (op.IQOrderID != null && op.IQOrderID > 0)
                    allIQOrderIDs.Add(op.IQOrderID.ToString());

                if (op.WebOrderID != null && op.WebOrderID > 0)
                    allWebOrderIDs.Add(op.WebOrderID.ToString());

                if (op.WebOrderAuthorizationToken != null && !string.IsNullOrEmpty(op.WebOrderAuthorizationToken))
                    allWebAuthTokens.Add(op.WebOrderAuthorizationToken);


                if (!string.IsNullOrEmpty(op.Notes))
                    allNotes.Add(op.Notes);

                foreach (OrderProduct prod in op.OrderProducts)
                {
                    for (int j = 0; j < op.Qty; j++)
                    {
                        for (int i = 0; i < prod.Quantity; i++)
                        {
                            ImageQuixProduct iqProd = flowProject.FlowMasterDataContext.ImageQuixProducts.FirstOrDefault(iqp => iqp.PrimaryKey == prod.ImageQuixProductPk);
                            if (iqProd == null)
                            {
                                continue;

                            }
                            allProducts.Add(iqProd);
                            if (!AllUnitCatagories.ContainsKey(iqProd.ImageQuixProductGroup.Label))
                            {
                                AllUnitCatagories.Add(iqProd.ImageQuixProductGroup.Label, new Dictionary<string, List<object>>());
                            }

                            if (!AllUnitCatagories[iqProd.ImageQuixProductGroup.Label].ContainsKey(iqProd.Label))
                            {
                                AllUnitCatagories[iqProd.ImageQuixProductGroup.Label].Add(iqProd.Label, new List<object>());
                                AllUnitCatagories[iqProd.ImageQuixProductGroup.Label][iqProd.Label].Add(0);
                                if (string.IsNullOrEmpty(iqProd.SKU))
                                    AllUnitCatagories[iqProd.ImageQuixProductGroup.Label][iqProd.Label].Add("");
                                else
                                    AllUnitCatagories[iqProd.ImageQuixProductGroup.Label][iqProd.Label].Add(iqProd.SKU);

                                //if (string.IsNullOrEmpty(iqProd.Map))
                                //    AllUnitCatagories[iqProd.ImageQuixProductGroup.Label][iqProd.Label].Add("");
                                //else
                                //    AllUnitCatagories[iqProd.ImageQuixProductGroup.Label][iqProd.Label].Add(iqProd.Map);
                                AllUnitCatagories[iqProd.ImageQuixProductGroup.Label][iqProd.Label].Add(iqProd.Price);
                            }

                            AllUnitCatagories[iqProd.ImageQuixProductGroup.Label][iqProd.Label][0] = ((int)AllUnitCatagories[iqProd.ImageQuixProductGroup.Label][iqProd.Label][0]) + 1;
                        }
                    }
                }
                
            }

             foreach (SubjectImagePlus si in OrderSubjectImages)
            {
                foreach (OrderImageOption oio in si.SubjectImage.OrderImageOptions)
                {

                    //foreach (OrderProduct prod in op.OrderProducts)
                    //{
                    if (flowProject.FlowMasterDataContext.ImageQuixCatalogOptions.Any(iqo => iqo.ResourceURL == oio.ResourceURL))
                    {
                        ImageQuixCatalogOption iqOpt = flowProject.FlowMasterDataContext.ImageQuixCatalogOptions.First(iqo => iqo.ResourceURL == oio.ResourceURL);

                        //allProducts.Add(iqProd);
                        if (!AllUnitCatagories.ContainsKey(iqOpt.ImageQuixCatalogOptionGroup.Label))
                        {
                            AllUnitCatagories.Add(iqOpt.ImageQuixCatalogOptionGroup.Label, new Dictionary<string, List<object>>());
                        }

                        if (!AllUnitCatagories[iqOpt.ImageQuixCatalogOptionGroup.Label].ContainsKey(iqOpt.Label))
                        {
                            AllUnitCatagories[iqOpt.ImageQuixCatalogOptionGroup.Label].Add(iqOpt.Label, new List<object>());
                            AllUnitCatagories[iqOpt.ImageQuixCatalogOptionGroup.Label][iqOpt.Label].Add(0);

                            //Need SKU
                            if (string.IsNullOrEmpty(iqOpt.SKU))
                                AllUnitCatagories[iqOpt.ImageQuixCatalogOptionGroup.Label][iqOpt.Label].Add("");
                            else
                                AllUnitCatagories[iqOpt.ImageQuixCatalogOptionGroup.Label][iqOpt.Label].Add(iqOpt.SKU);



                            AllUnitCatagories[iqOpt.ImageQuixCatalogOptionGroup.Label][iqOpt.Label].Add(iqOpt.Price);
                        }

                        AllUnitCatagories[iqOpt.ImageQuixCatalogOptionGroup.Label][iqOpt.Label][0] = ((int)AllUnitCatagories[iqOpt.ImageQuixCatalogOptionGroup.Label][iqOpt.Label][0]) + 1;
                    }
 
                }   
            }

            XmlDocument doc = new XmlDocument();
            XmlDeclaration dec = doc.CreateXmlDeclaration("1.0", null, null);
            doc.AppendChild(dec);// Create the root element
            XmlElement orderReport = doc.CreateElement("OrderReport");
            doc.AppendChild(orderReport);
            {
                XmlElement orderHeader = doc.CreateElement("OrderHeader");
                orderReport.AppendChild(orderHeader);
                {
                    XmlElement organizationName = doc.CreateElement("OrganizationName");
                    organizationName.InnerText = flowProject.Studio.OrganizationName;
                    orderHeader.AppendChild(organizationName);

                    XmlElement labCustomerID = doc.CreateElement("LabCustomerID");
                    labCustomerID.InnerText = flowProject.Studio.LabCustomerID;
                    orderHeader.AppendChild(labCustomerID);

                    XmlElement projectName = doc.CreateElement("ProjectName");
                    projectName.InnerText = flowProject.FlowProjectName;
                    orderHeader.AppendChild(projectName);

                    XmlElement xmlorderID = doc.CreateElement("OrderID");
                    xmlorderID.InnerText = orderID;
                    orderHeader.AppendChild(xmlorderID);

                    if (allIQOrderIDs.Count > 0)
                    {
                        XmlElement xmliqorderIDs = doc.CreateElement("IQOrderIDs");
                        xmliqorderIDs.InnerText = string.Join(", ", allIQOrderIDs);
                        orderHeader.AppendChild(xmliqorderIDs);
                        
                    }
                    if (allWebOrderIDs.Count > 0)
                    {
                        XmlElement xmliqorderIDs = doc.CreateElement("WebOrderIDs");
                        xmliqorderIDs.InnerText = string.Join(", ", allWebOrderIDs);
                        orderHeader.AppendChild(xmliqorderIDs);

                    }

                    if (allWebAuthTokens.Count > 0)
                    {
                        XmlElement xmliqorderIDs = doc.CreateElement("WebAuthorizationTokens");
                        xmliqorderIDs.InnerText = string.Join(", ", allWebAuthTokens);
                        orderHeader.AppendChild(xmliqorderIDs);

                    }

                    if (allNotes.Count > 0)
                    {
                        XmlElement xmlnotes = doc.CreateElement("Notes");
                        xmlnotes.InnerText = string.Join(", ", allNotes);
                        orderHeader.AppendChild(xmlnotes);

                    }

                    XmlElement reportDate = doc.CreateElement("ReportDate");
                    reportDate.InnerText = DateTime.Now.ToString();
                    orderHeader.AppendChild(reportDate);

                    XmlElement OrderForm = doc.CreateElement("OrderForm");
                    OrderForm.AppendChild(OrderForm.OwnerDocument.ImportNode(OrderFormXML, true));
                    orderHeader.AppendChild(OrderForm);

                    string orderSourceString = "Flow";
                    if (CurrentOrderPackages[0].IQOrderID > 0)
                        orderSourceString = "ImageQuix";
                    if (CurrentOrderPackages[0].WebOrderID > 0)
                        orderSourceString = "Web";
                    XmlElement orderSource = doc.CreateElement("OrderSource");
                    orderSource.InnerText = orderSourceString;
                    orderHeader.AppendChild(orderSource);

                    XmlElement contactInfo = doc.CreateElement("ContactInfo");
                    orderHeader.AppendChild(contactInfo);
                    {
                        XmlElement contactName = doc.CreateElement("ContactName");
                        contactName.InnerText = flowProject.Studio.OrganizationContactName;
                        contactInfo.AppendChild(contactName);

                        XmlElement contactPhone = doc.CreateElement("ContactPhone");
                        contactPhone.InnerText = flowProject.Studio.OrganizationContactPhone;
                        contactInfo.AppendChild(contactPhone);

                        XmlElement contactEmail = doc.CreateElement("ContactEmail");
                        contactEmail.InnerText = flowProject.Studio.OrganizationContactEmail;
                        contactInfo.AppendChild(contactEmail);

                    }

                    XmlElement billingInfo = doc.CreateElement("BillingInfo");
                    orderHeader.AppendChild(billingInfo);
                    {
                        XmlElement shippingName = doc.CreateElement("BillingName");
                        shippingName.InnerText = shipOrg.OrganizationName;
                        billingInfo.AppendChild(shippingName);

                        XmlElement billingAddress = doc.CreateElement("BillingAddress");
                        billingAddress.InnerText = flowProject.Studio.OrganizationAddressLine1;
                        billingInfo.AppendChild(billingAddress);

                        XmlElement billingAddress2 = doc.CreateElement("BillingAddress");
                        billingAddress2.InnerText = flowProject.Studio.OrganizationAddressLine2;
                        billingInfo.AppendChild(billingAddress2);

                        XmlElement billingCity = doc.CreateElement("BillingCity");
                        billingCity.InnerText = flowProject.Studio.OrganizationCity;
                        billingInfo.AppendChild(billingCity);

                        XmlElement billingState = doc.CreateElement("BillingState");
                        billingState.InnerText = flowProject.Studio.OrganizationStateCode;
                        billingInfo.AppendChild(billingState);

                        XmlElement billingZip = doc.CreateElement("BillingZip");
                        billingZip.InnerText = flowProject.Studio.OrganizationZipCode;
                        billingInfo.AppendChild(billingZip);

                    }

                    XmlElement shippingInfo = doc.CreateElement("ShippingInfo");
                    orderHeader.AppendChild(shippingInfo);
                    {
                        XmlElement shippingName = doc.CreateElement("ShippingName");
                        shippingName.InnerText = shipOrg.OrganizationName;
                        shippingInfo.AppendChild(shippingName);

                        XmlElement shippingAddress = doc.CreateElement("ShippingAddress");
                        shippingAddress.InnerText = shipOrg.OrganizationShippingAddressLine1;
                        shippingInfo.AppendChild(shippingAddress);

                        XmlElement shippingAddress2 = doc.CreateElement("ShippingAddress");
                        shippingAddress2.InnerText = shipOrg.OrganizationShippingAddressLine2;
                        shippingInfo.AppendChild(shippingAddress2);

                        XmlElement shippingCity = doc.CreateElement("ShippingCity");
                        shippingCity.InnerText = shipOrg.OrganizationShippingCity;
                        shippingInfo.AppendChild(shippingCity);

                        XmlElement shippingState = doc.CreateElement("ShippingState");
                        shippingState.InnerText = shipOrg.OrganizationShippingStateCode;
                        shippingInfo.AppendChild(shippingState);

                        XmlElement shippingZip = doc.CreateElement("ShippingZip");
                        shippingZip.InnerText = shipOrg.OrganizationShippingZipCode;
                        shippingInfo.AppendChild(shippingZip);
                    }

                }

                
                double grandTotal = 0;
                XmlElement productCategories = doc.CreateElement("ProductCategories");
                orderReport.AppendChild(productCategories);
                {
                    foreach (KeyValuePair<string, Dictionary<string, List<object>>> kvp in AllUnitCatagories)
                    {
                        XmlElement productCategory = doc.CreateElement("ProductCategory");
                        productCategories.AppendChild(productCategory);
                        {
                            XmlElement catagoryDescription = doc.CreateElement("Description");
                            catagoryDescription.InnerText = kvp.Key;
                            productCategory.AppendChild(catagoryDescription);
                            
                            int sumQty = 0;
                            double sumTotal = 0;
                            XmlElement products = doc.CreateElement("Products");
                            productCategory.AppendChild(products);
                            {
                                foreach (KeyValuePair<string, List<object>> prod in kvp.Value)
                                {
                                    sumQty += (int)prod.Value[0];
                                    sumTotal = sumTotal + (double)(((Convert.ToDouble(prod.Value[0].ToString())) * (Convert.ToDouble(prod.Value[2].ToString()))));

                                    XmlElement product = doc.CreateElement("Product");
                                    products.AppendChild(product);
                                    {

                                        XmlElement productDescription = doc.CreateElement("Description");
                                        productDescription.InnerText = prod.Key;
                                        product.AppendChild(productDescription);

                                        XmlElement productSKU = doc.CreateElement("SKU");
                                        productSKU.InnerText = prod.Value[1].ToString();
                                        product.AppendChild(productSKU);

                                        XmlElement productCost = doc.CreateElement("Cost");
                                        productCost.InnerText = "$" + prod.Value[2].ToString();
                                        product.AppendChild(productCost);

                                        XmlElement productQty = doc.CreateElement("Qty");
                                        productQty.InnerText = prod.Value[0].ToString();
                                        product.AppendChild(productQty);

                                        XmlElement productTotal = doc.CreateElement("Total");
                                        productTotal.InnerText = "$" + (Convert.ToDouble(prod.Value[0]) * Convert.ToDouble(prod.Value[2])).ToString("0.00");
                                        product.AppendChild(productTotal);

                                    }

                                }
                            }

                            XmlElement catagoryTotalQty = doc.CreateElement("TotalQty");
                            catagoryTotalQty.InnerText = sumQty.ToString();
                            productCategory.AppendChild(catagoryTotalQty);

                            XmlElement catagoryTotalCost = doc.CreateElement("TotalCost");
                            catagoryTotalCost.InnerText = "$" + sumTotal.ToString("0.00");
                            productCategory.AppendChild(catagoryTotalCost);

                            grandTotal = grandTotal + sumTotal;

                        }
                    }
                }

                //Add in the Total cost in the header
                XmlElement totalCost = doc.CreateElement("TotalCost");
                totalCost.InnerText = "$" + grandTotal.ToString("0.00");
                orderHeader.AppendChild(totalCost);


            }

            return doc;
        }


        public static C1PrintDocument GetBillingReport(FlowProject flowProject, String orderID, List<OrderPackage> CurrentOrderPackages, List<SubjectImagePlus> OrderSubjectImages, Organization shipOrg, out Dictionary<string, Dictionary<string, List<object>>> AllUnitCatagories)
        {
            //lets first build our datasource
            //if (string.IsNullOrEmpty(orderID))
            //    orderID = orderFilter;

            string orderfilterstring = "all";
            bool onlineorders = false;
            if (orderID.ToLower() == "new online orders")
            {
                onlineorders = true;
                orderfilterstring = null;
            }
            else if (orderID.ToLower().Contains("new orders"))
            {
                orderfilterstring = null;
            }
            else if (orderID.ToLower().Contains(" - "))
            {
                orderfilterstring = orderID.Split(" - ")[0];
            }
            else if (orderID.ToLower().Contains("all"))
            {
                orderfilterstring = "all";
            }
            else
            {
                orderfilterstring = orderID;
            }

            if (CurrentOrderPackages == null)
            {
                int parseResult = 0;
                if(orderfilterstring == null)
                    CurrentOrderPackages = flowProject.AllFlowOrders.Where(o => o.IQOrderID != null && o.LabOrderID == null).ToList();
                else if(orderfilterstring == "new orders")
                    CurrentOrderPackages = flowProject.AllFlowOrders.Where(o => o.IQOrderID == null && o.LabOrderID == null).ToList();
                else if (orderfilterstring == "all")
                    CurrentOrderPackages = flowProject.AllFlowOrders.ToList();
                else if (Int32.TryParse(orderfilterstring, out parseResult))
                    CurrentOrderPackages = flowProject.AllFlowOrders.Where(o => o.LabOrderID == parseResult).ToList();
            }

            if (OrderSubjectImages == null)
            {
                OrderSubjectImages = new List<SubjectImagePlus>();
                foreach (OrderPackage op in CurrentOrderPackages)
                {
                    foreach (OrderProduct prod in op.OrderProducts)
                    {
                        int nodeLoop = 0;
                        foreach (OrderProductNode node in prod.OrderProductNodes)
                        {
                            if (node.SubjectImage != null && node.ImageQuixProductNodeType == 1) // 1 is the image nodetype
                            {
                                SubjectImagePlus sip = new SubjectImagePlus(node.SubjectImage, null, null, null, false);
                                if (!OrderSubjectImages.Any(b => b.SubjectImage == sip.SubjectImage && b.Background == sip.Background && b.NewImagePath == sip.NewImagePath))
                                    OrderSubjectImages.Add(sip);
                            }
                        }
                    }
                }
            }

            if (shipOrg == null && CurrentOrderPackages.Count > 0)
            {
                shipOrg = new Organization();
                shipOrg.OrganizationShippingAddressLine1 = CurrentOrderPackages[0].ShippingAddress;
                shipOrg.OrganizationShippingCity = CurrentOrderPackages[0].ShippingCity;
                shipOrg.OrganizationShippingStateCode = CurrentOrderPackages[0].ShippingState;
                shipOrg.OrganizationShippingZipCode = CurrentOrderPackages[0].ShippingZip;
                shipOrg.OrganizationName = CurrentOrderPackages[0].ShippingFirstName + " " + CurrentOrderPackages[0].ShippingLastName;
            }
            


            AllUnitCatagories = new Dictionary<string, Dictionary<string, List<object>>>();
            List<ImageQuixProduct> allProducts = new List<ImageQuixProduct>();

            List<string> allIQOrderIDs = new List<string>();
            List<string> allNotes = new List<string>();

            foreach (OrderPackage op in CurrentOrderPackages)
            {
                if (op.SubjectOrder.IsMarkedForDeletion)
                    continue;

                if (op.IQOrderID != null && op.IQOrderID > 0)
                    allIQOrderIDs.Add(op.IQOrderID.ToString());

                if (!string.IsNullOrEmpty(op.Notes))
                    allNotes.Add(op.Notes);

                foreach (OrderProduct prod in op.OrderProducts)
                {
                    for (int j = 0; j < op.Qty; j++)
                    {
                        for (int i = 0; i < prod.Quantity; i++)
                        {
                            ImageQuixProduct iqProd = flowProject.FlowMasterDataContext.ImageQuixProducts.FirstOrDefault(iqp => iqp.PrimaryKey == prod.ImageQuixProductPk);
                            if (iqProd == null)
                                continue;
                            allProducts.Add(iqProd);
                            if (!AllUnitCatagories.ContainsKey(iqProd.ImageQuixProductGroup.Label))
                            {
                                AllUnitCatagories.Add(iqProd.ImageQuixProductGroup.Label, new Dictionary<string, List<object>>());
                            }

                            if (!AllUnitCatagories[iqProd.ImageQuixProductGroup.Label].ContainsKey(iqProd.Label))
                            {
                                AllUnitCatagories[iqProd.ImageQuixProductGroup.Label].Add(iqProd.Label, new List<object>());
                                AllUnitCatagories[iqProd.ImageQuixProductGroup.Label][iqProd.Label].Add(0);
                                if (string.IsNullOrEmpty(iqProd.SKU))
                                    AllUnitCatagories[iqProd.ImageQuixProductGroup.Label][iqProd.Label].Add("");
                                else
                                    AllUnitCatagories[iqProd.ImageQuixProductGroup.Label][iqProd.Label].Add(iqProd.SKU);

                                //if (string.IsNullOrEmpty(iqProd.Map))
                                //    AllUnitCatagories[iqProd.ImageQuixProductGroup.Label][iqProd.Label].Add("");
                                //else
                                //    AllUnitCatagories[iqProd.ImageQuixProductGroup.Label][iqProd.Label].Add(iqProd.Map);
                                AllUnitCatagories[iqProd.ImageQuixProductGroup.Label][iqProd.Label].Add(iqProd.Price);
                            }

                            AllUnitCatagories[iqProd.ImageQuixProductGroup.Label][iqProd.Label][0] = ((int)AllUnitCatagories[iqProd.ImageQuixProductGroup.Label][iqProd.Label][0]) + 1;
                        }
                    }
                }

            }

          
             foreach (SubjectImagePlus si in OrderSubjectImages)
            {
                foreach (OrderImageOption oio in si.SubjectImage.OrderImageOptions)
                {

                {
                    //foreach (OrderProduct prod in op.OrderProducts)
                    //{
                    if (flowProject.FlowMasterDataContext.ImageQuixCatalogOptions.Any(iqo => iqo.ResourceURL == oio.ResourceURL))
                    {
                        ImageQuixCatalogOption iqOpt = flowProject.FlowMasterDataContext.ImageQuixCatalogOptions.First(iqo => iqo.ResourceURL == oio.ResourceURL);

                        //allProducts.Add(iqProd);
                        if (!AllUnitCatagories.ContainsKey(iqOpt.ImageQuixCatalogOptionGroup.Label))
                        {
                            AllUnitCatagories.Add(iqOpt.ImageQuixCatalogOptionGroup.Label, new Dictionary<string, List<object>>());
                        }

                        if (!AllUnitCatagories[iqOpt.ImageQuixCatalogOptionGroup.Label].ContainsKey(iqOpt.Label))
                        {
                            AllUnitCatagories[iqOpt.ImageQuixCatalogOptionGroup.Label].Add(iqOpt.Label, new List<object>());
                            AllUnitCatagories[iqOpt.ImageQuixCatalogOptionGroup.Label][iqOpt.Label].Add(0);

                            //Need SKU
                            if (string.IsNullOrEmpty(iqOpt.SKU))
                                AllUnitCatagories[iqOpt.ImageQuixCatalogOptionGroup.Label][iqOpt.Label].Add("");
                            else
                                AllUnitCatagories[iqOpt.ImageQuixCatalogOptionGroup.Label][iqOpt.Label].Add(iqOpt.SKU);
                            //if (string.IsNullOrEmpty(iqProd.Map))
                            //    AllUnitCatagories[iqOpt.ImageQuixCatalogOptionGroup.Label][iqProd.Label].Add("");
                            //else
                            //    AllUnitCatagories[iqOpt.ImageQuixCatalogOptionGroup.Label][iqProd.Label].Add(iqProd.Map);


                            AllUnitCatagories[iqOpt.ImageQuixCatalogOptionGroup.Label][iqOpt.Label].Add(iqOpt.Price);
                        }

                        AllUnitCatagories[iqOpt.ImageQuixCatalogOptionGroup.Label][iqOpt.Label][0] = ((int)AllUnitCatagories[iqOpt.ImageQuixCatalogOptionGroup.Label][iqOpt.Label][0]) + 1;
                    }
                   }
                }
            }

            C1PrintDocument doc = new C1PrintDocument();
            doc.StartDoc();
            doc.PageLayout.PageSettings.UsePrinterPaperSize = false;
            doc.PageLayout.PageSettings.Width = "216mm";
            doc.PageLayout.PageSettings.Height = "279mm";
            doc.PageLayout.PageSettings.TopMargin = "10mm";

            RenderTable rtDate = new RenderTable();
            rtDate.Width = "100%";
            rtDate.ColumnSizingMode = TableSizingModeEnum.Auto;
            rtDate.Style.GridLines.All = new LineDef(0, System.Windows.Media.Color.FromRgb(0, 0, 0));
            rtDate.CellStyle.Padding.All = "5mm";
            rtDate.CellStyle.Padding.Top = "0mm";
            rtDate.Cells[0, 0].Text = DateTime.Now.ToShortDateString();
            rtDate.Style.Font = new System.Drawing.Font("Tahoma", 10);
            rtDate.Style.TextAlignHorz = AlignHorzEnum.Right;
            rtDate.Style.TextAlignVert = AlignVertEnum.Top;
            doc.Body.Children.Add(rtDate);

            RenderTable rtheader = new RenderTable();
            rtheader.Width = "100%";
            rtheader.ColumnSizingMode = TableSizingModeEnum.Auto;
            rtheader.Style.GridLines.All = new LineDef(0, System.Windows.Media.Color.FromRgb(0, 0, 0));
            rtheader.CellStyle.Padding.All = "5mm";
            rtheader.CellStyle.Padding.Top = "0mm";
            rtheader.Cells[0, 0].Text = "Lab Billing Report";
            rtheader.Style.Font = new System.Drawing.Font("Tahoma", 18);
            rtheader.Style.TextAlignHorz = AlignHorzEnum.Center;
            doc.Body.Children.Add(rtheader);

            string orderSourceString = "Flow";
            if (CurrentOrderPackages.Count > 0 && CurrentOrderPackages[0].IQOrderID > 0)
                orderSourceString = "ImageQuix";
                    

            RenderTable projectInfo = new RenderTable();
            projectInfo.Width = Unit.Auto;
            projectInfo.ColumnSizingMode = TableSizingModeEnum.Auto;
            projectInfo.Style.GridLines.All = new LineDef(0, System.Windows.Media.Color.FromRgb(0, 0, 0));
            projectInfo.CellStyle.Padding.Right = "2mm";
            projectInfo.Cells[0, 0].Text = "Studio:";
            projectInfo.Cells[0, 1].Text = flowProject.Studio.OrganizationName;
            projectInfo.Cells[1, 0].Text = "Lab Customer ID:";
            projectInfo.Cells[1, 1].Text = flowProject.Studio.LabCustomerID;
            projectInfo.Cells[2, 0].Text = "Project:";
            projectInfo.Cells[2, 1].Text = flowProject.FlowProjectDesc;
            projectInfo.Cells[3, 0].Text = "Order:";
            projectInfo.Cells[3, 1].Text = orderID;
            projectInfo.Cells[4, 0].Text = "Order Source:";
            projectInfo.Cells[4, 1].Text = orderSourceString;

            if (allIQOrderIDs.Count > 0)
            {
                projectInfo.Cells[5, 0].Text = "IQ Order IDs:";
                projectInfo.Cells[5, 1].Text = string.Join(", ", allIQOrderIDs);
            }
            if (allNotes.Count > 0)
            {
                projectInfo.Cells[6, 0].Text = "Order Notes:";
                projectInfo.Cells[6, 1].Text = string.Join(", ", allNotes);
            }

            projectInfo.Style.Font = new System.Drawing.Font("Tahoma", 12);
            projectInfo.Style.TextAlignHorz = AlignHorzEnum.Left;
            doc.Body.Children.Add(projectInfo);


            double grandTotal = 0;
            foreach (KeyValuePair<string, Dictionary<string, List<object>>> kvp in AllUnitCatagories)
            {
                RenderField fld = new RenderField();
                fld.Height = ".50";
                fld.Style.Font = new System.Drawing.Font("Tahoma", 14);
                fld.Text = kvp.Key;
                fld.Style.TextAlignVert = AlignVertEnum.Bottom;
                doc.Body.Children.Add(fld);

                RenderTable rt = new RenderTable();
                rt.Width = Unit.Auto;
                rt.ColumnSizingMode = TableSizingModeEnum.Auto;
                rt.Style.GridLines.All = new LineDef(0, System.Windows.Media.Color.FromRgb(0,0,0));
                rt.CellStyle.Padding.All = "1mm";
                doc.Body.Children.Add(rt);
                int row = 1;
                rt.Cells[0, 0].Text = "Product";
                rt.Cells[0, 1].Text = "SKU";
                rt.Cells[0, 2].Text = "Cost";
                rt.Cells[0, 3].Text = "Qty";
                rt.Cells[0, 4].Text = "Total";

                rt.Rows[0].Style.FontBold = true;
                rt.Rows[0].Style.FontUnderline = true;
                int sumQty = 0;
                double sumTotal = 0;
                foreach (KeyValuePair<string, List<object>> prod in kvp.Value)
                {
                    sumQty += (int)prod.Value[0];
                    sumTotal = sumTotal + (double)(((Convert.ToDouble(prod.Value[0].ToString())) * (Convert.ToDouble(prod.Value[2].ToString()))));
                    rt.Cells[row, 0].Text = prod.Key + ": ";
                    rt.Cells[row, 1].Text = prod.Value[1].ToString();
                    rt.Cells[row, 2].Text = "$" + prod.Value[2].ToString();
                    rt.Cells[row, 3].Text = prod.Value[0].ToString();
                    rt.Cells[row, 4].Text = "$" + (Convert.ToDouble(prod.Value[0]) * Convert.ToDouble(prod.Value[2])).ToString("0.00");

                    row++;
                }

                rt.Cells[row, 0].Text = "TOTAL:";
                rt.Cells[row, 1].Text = "";
                rt.Cells[row, 2].Text = "";
                rt.Cells[row, 3].Text = sumQty.ToString();
                rt.Cells[row, 4].Text = "$" + sumTotal.ToString("0.00");
                rt.Rows[row].Style.FontBold = true;
                rt.Rows[row-1].Style.FontUnderline = true;

                doc.Body.Children.Add(new RenderLine());
                grandTotal = grandTotal + sumTotal;
               

            }

             RenderField Total = new RenderField();
            Total.Height = ".50";
            Total.Style.Font = new System.Drawing.Font("Tahoma", 14);
            Total.Text = "Total: $" + grandTotal.ToString("0.00");
            Total.Style.TextAlignVert = AlignVertEnum.Bottom;
            doc.Body.Children.Add(Total);

            doc.EndDoc();
            return doc;
        }

        public static C1PrintDocument GetCatalogReport(FlowCatalog FlowCatalog, bool includeBarCodes, bool includeProducts, bool includeImageOptions)
        {
            C1PrintDocument doc = new C1PrintDocument();
            doc.StartDoc();

            doc.PageLayout.PageSettings.UsePrinterPaperSize = false;
            doc.PageLayout.PageSettings.Width = "216mm";
            doc.PageLayout.PageSettings.Height = "279mm";


            doc.PageLayout.PageSettings.TopMargin = "10mm";
            doc.PageLayout.PageSettings.LeftMargin = "10mm";

            RenderTable rtDate = new RenderTable();
            rtDate.Width = "100%";
            rtDate.ColumnSizingMode = TableSizingModeEnum.Auto;
            rtDate.Style.GridLines.All = new LineDef(0, System.Windows.Media.Color.FromRgb(0, 0, 0));
            rtDate.CellStyle.Padding.All = "5mm";
            rtDate.CellStyle.Padding.Top = "0mm";
            rtDate.Cells[0, 0].Text = "Catalog Package Report - " + DateTime.Now.ToShortDateString();
            rtDate.Style.Font = new System.Drawing.Font("Tahoma", 10);
            rtDate.Style.TextAlignHorz = AlignHorzEnum.Right;
            rtDate.Style.TextAlignVert = AlignVertEnum.Top;
            doc.Body.Children.Add(rtDate);

            RenderTable rtheader = new RenderTable();
            rtheader.Width = "100%";
            rtheader.ColumnSizingMode = TableSizingModeEnum.Auto;
            rtheader.Style.GridLines.All = new LineDef(0, System.Windows.Media.Color.FromRgb(0, 0, 0));
            rtheader.CellStyle.Padding.All = "5mm";
            rtheader.CellStyle.Padding.Top = "0mm";
            rtheader.Cells[0, 0].Text = FlowCatalog.FlowCatalogDesc;
            rtheader.Style.Font = new System.Drawing.Font("Tahoma", 18);
            rtheader.Style.TextAlignHorz = AlignHorzEnum.Center;
            doc.Body.Children.Add(rtheader);

            //RenderTable projectInfo = new RenderTable();
            //projectInfo.Width = Unit.Auto;
            //projectInfo.ColumnSizingMode = TableSizingModeEnum.Auto;
            //projectInfo.Style.GridLines.All = new LineDef(0, System.Windows.Media.Color.FromRgb(0, 0, 0));
            //projectInfo.CellStyle.Padding.Right = "2mm";
            //projectInfo.Cells[0, 0].Text = "Catalog:";
            //projectInfo.Cells[0, 1].Text = FlowCatalog.FlowCatalogDesc;
            //projectInfo.Style.Font = new System.Drawing.Font("Tahoma", 12);
            //projectInfo.Style.TextAlignHorz = AlignHorzEnum.Left;
            //doc.Body.Children.Add(projectInfo);

            RenderTable mainGrid = new RenderTable();
            mainGrid.Width = Unit.Auto;
            mainGrid.Style.GridLines.All = new LineDef(.02, System.Windows.Media.Color.FromRgb(120, 120, 120), DashStyle.Dash);
            mainGrid.ColumnSizingMode = TableSizingModeEnum.Auto;
            mainGrid.CellStyle.Padding.All = "2mm";
            doc.Body.Children.Add(mainGrid);

            int mainRow = 0;
            int mainCol = 0;
            foreach (ProductPackage pp in FlowCatalog.ProductPackages)
            {
               
                RenderTable rt = new RenderTable();
                rt.Width = 2.4;
                rt.ColumnSizingMode = TableSizingModeEnum.Auto;
                rt.Style.GridLines.All = new LineDef(0, System.Windows.Media.Color.FromRgb(0, 0, 0));
                rt.CellStyle.Padding.All = .02;
                mainGrid.Cells[mainRow, mainCol].Area.Children.Add(rt);
                //doc.Body.Children.Add(rt);
                //rt.Rows[0].Style.FontBold = true;
                rt.Cols[0].Style.TextAlignHorz = AlignHorzEnum.Center;
                rt.Cells[0, 0].Text = pp.ProductPackageDesc;
                rt.Cells[0, 0].Style.Font = new System.Drawing.Font("Tahoma", 14);
                if (includeBarCodes && (pp.Map.Length>0))
                {
                    rt.Cells[1, 0].Text = "*.PKG." + pp.Map.ToUpper() + "*";
                    rt.Cells[1, 0].SpanRows = 2;
                    rt.Cells[1, 0].Style.Font = new System.Drawing.Font("BC C39 2 to 1 Narrow", 40);
                    rt.Cells[3, 0].Text = "(" + pp.Map + ") - " + "$" + pp.Price;
                }
                //rt.Cells[4, 0].Text = "$" + pp.Price;

                int row = 4;
                if (includeProducts)
                {
                    foreach (ProductPackageComposition prod in pp.ProductPackageCompositions)
                    {
                        rt.Cells[row, 0].Text = "(Qty: " + prod.Quantity + ") - " + prod.DisplayLabel;
                        rt.Cells[row, 0].Style.TextAlignHorz = AlignHorzEnum.Left;
                        row++;
                    }

                }
                if (mainCol == 2)
                {
                    mainCol = 0;
                    mainRow++;
                }
                else mainCol++;
            }
               // doc.Body.Children.Add(new RenderLine());

            if (includeImageOptions)
            {
                foreach (FlowCatalogOption fco in FlowCatalog.FlowCatalogImageOptionList)
                {
                    RenderTable rt = new RenderTable();
                    rt.Width = 2.4;
                    rt.ColumnSizingMode = TableSizingModeEnum.Auto;
                    rt.Style.GridLines.All = new LineDef(0, System.Windows.Media.Color.FromRgb(0, 0, 0));
                    rt.CellStyle.Padding.All = .02;
                    mainGrid.Cells[mainRow, mainCol].Area.Children.Add(rt);
                    //doc.Body.Children.Add(rt);
                    //rt.Rows[0].Style.FontBold = true;
                    rt.Cols[0].Style.TextAlignHorz = AlignHorzEnum.Center;
                    rt.Cells[0, 0].Text = fco.ImageQuixCatalogOption.Label;
                    rt.Cells[0, 0].Style.Font = new System.Drawing.Font("Tahoma", 14);
                    if (includeBarCodes && (fco.Map.Length > 0))
                    {
                        //pko denotes a Catalog Option
                        rt.Cells[1, 0].Text = "*.PKO." + fco.Map.ToUpper() + "*";
                        rt.Cells[1, 0].SpanRows = 2;
                        rt.Cells[1, 0].Style.Font = new System.Drawing.Font("BC C39 2 to 1 Narrow", 40);
                        rt.Cells[3, 0].Text = "(" + fco.Map + ") - " + "$" + fco.Price;
                    }

                    if (mainCol == 2)
                    {
                        mainCol = 0;
                        mainRow++;
                    }
                    else mainCol++;
                }
            }

            doc.EndDoc();
            return doc;
        }

        public static C1Report GetSlateReport(FlowProject flowProject)
        {
            C1Report doc = new C1Report();

            string Slate3upPath = Path.Combine(FlowContext.FlowReportsDirPath, "Slate_3up.xml");
            doc.Load(Slate3upPath, "Slate_3up");
            doc.DataSource.ConnectionString = flowProject.ConnectionString;
            doc.DataSource.RecordSource = "Subject";
            DataTable dt = new DataTable();
            //dt.Columns.Add("TicketCode");
            dt.Columns.Add("TicketCodeBarCode");
            dt.Columns.Add("FormalFullName");
            dt.Columns.Add("ProjectOrganizationName");
            dt.Columns.Add("ProjectName");
            foreach (ProjectSubjectField fieldName in flowProject.ProjectSubjectFieldList)
            {
                dt.Columns.Add(fieldName.ProjectSubjectFieldDesc);
            }

            foreach (Subject sub in flowProject.SubjectList.View)
            {
                DataRow dr = dt.NewRow();
                //dr["TicketCode"] = sub.TicketCode;
                dr["TicketCodeBarCode"] = "*.NST." + sub.TicketCode + "*";
                dr["FormalFullName"] = sub.FormalFullName;
                dr["ProjectOrganizationName"] = flowProject.Organization.OrganizationName;
                dr["ProjectName"] = flowProject.FlowProjectName;
                foreach (ProjectSubjectField fieldName in flowProject.ProjectSubjectFieldList)
                {
                    dr[fieldName.ProjectSubjectFieldDesc] = sub.SubjectDataRow[fieldName.ProjectSubjectFieldDisplayName];
                }
                dt.Rows.Add(dr);
        }

            doc.DataSource.Recordset = dt;

            return doc;
        }

        public static C1Report GetCustomReport(FlowProject flowProject, string reportFileName)
        {
            C1Report doc = new C1Report();
            
            string CustomReport = Path.Combine(FlowContext.FlowReportsDirPath, reportFileName);
            doc.Load(CustomReport, reportFileName.Replace(".xml", ""));

            bool doCustomSource = false;
            if (doc.DataSource.ConnectionString != null && doc.DataSource.ConnectionString.EndsWith(".xml"))
            {
                doCustomSource = true;
            }
            else
            {
                flowProject.PrepareForExport();
            }
            doc.DataSource.ConnectionString = flowProject.ConnectionString;
            doc.DataSource.DataProvider = DataProvider.SqlServerCe4_0;

            if(string.IsNullOrEmpty(doc.DataSource.RecordSource))
                doc.DataSource.RecordSource = "Subject";

            if (doCustomSource)
            {
                DataSet ds = flowProject.GetCustomReportDataSet(true);
                doc.DataSource.Recordset = ds.Tables[doc.DataSource.RecordSource];
            }
                                   
            return doc;
        }

        internal static void CreateImageMatchDataFile(FlowProject flowProject, string orderID, List<Subject> SubjectList, List<OrderPackage> CurrentOrderPackages, List<SubjectImagePlus> OrderSubjectImages, string exportFilePath, bool renderGreenscreen)
        {
            DataTable exportDataTable = flowProject.GetLimitedExportData(OrderSubjectImages, CurrentOrderPackages, renderGreenscreen);

            DataColumn[] columnList = new DataColumn[exportDataTable.Columns.Count];

            exportDataTable.Columns.CopyTo(columnList, 0);

            StringBuilder builder = new StringBuilder();

            string columnString = String.Join(",", columnList.Select(c => @"""" + c.ColumnName + @"""").ToArray());

            builder.AppendLine(columnString);

            //progressInfo.Update("Processing subject data {0} of {1}", 0, exportDataTable.Rows.Count, 1, true);

            foreach (DataRow row in exportDataTable.Rows)
            {
                string[] items = row.ItemArray.Select(i => @"""" + i.ToString() + @"""").ToArray();

                string rowString = String.Join(",", items);

                builder.AppendLine(rowString);

                //progressInfo++;
            }

            //string exportFilePath = FlowContext.Combine(selectedImageMatchExportPath, flowProject.FileSafeProjectName + ".csv");

            File.WriteAllText(exportFilePath, builder.ToString(), Encoding.Default);
        }
    }
}
