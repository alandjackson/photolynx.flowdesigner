﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.Controller.Project;
using Flow.Lib.AsyncWorker;
using Flow.Lib;
using System.ComponentModel;
using Flow.Schema.LinqModel.DataContext;
using System.IO;
using System.Windows.Threading;
using System.Threading;
using StructureMap;
using Flow.Schema;
using NLog;
using Flow.Lib.Helpers;

namespace Flow.Controller
{
    public class FlowServerCrawler
    {
        FlowController FlowController { get; set; }
        
        FlowBackgroundWorker bWorker;

        private static Logger logger = LogManager.GetCurrentClassLogger();

        bool keepRunning = false;

        private Dispatcher dispatcher { get; set; }


        public FlowServerCrawler(FlowController flowController, Dispatcher disp)
        {
            this.FlowController = flowController;
            this.dispatcher = disp;
        }

        public void Begin()
        {
            if (keepRunning == true)
                return;

            //start a thread that will be looking for new incoming projects to merge
            keepRunning = true;
            bWorker = new FlowBackgroundWorker();
            bWorker = FlowBackgroundWorkerManager.RunWorker(ServerEngine, ServerEngine_Stopped);

            //Thread t = new Thread(delegate()
            //{
            //    StartServerEngine();
            //});
            //t.SetApartmentState(ApartmentState.STA);
            //t.Start();

        }

        void ServerEngine_Stopped(object sender, RunWorkerCompletedEventArgs e)
        {
          
        }

        void ServerEngine(object sender, DoWorkEventArgs e)
        {
            try
            {
                StartServerEngine();
            }
            catch (Exception ex)
            {
                logger.Error("Error in FlowServerCrawler: " + ex.Message);
                throw ex;
            }
        }

        void StartServerEngine()
        {
            string pafIncomingFolder = Path.Combine(this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.ProjectsDirectory, "incoming");
            bool firstrun = true;
            Thread.Sleep(5000);
            while (keepRunning)
            {
                if (firstrun)
                {
                    foreach (FlowProject fp in this.FlowController.ProjectController.FlowMasterDataContext.FlowProjectList)
                    {
                        //if (!this.FlowController.ProjectController.FlowServerIsOn)
                        //    break;
                        if (fp.FlowMasterDataContext == null)
                            fp.FlowMasterDataContext = this.FlowController.ProjectController.FlowMasterDataContext;
                        string incomingFolder = Path.Combine(fp.DirPath, "incoming");
                        string inProgressFolder = Path.Combine(fp.DirPath, "inProgress");
                        string qcFolder = Path.Combine(fp.DirPath, "qc");
                        if (Directory.Exists(Path.Combine(fp.DirPath, "inProgress")))
                        {
                            foreach (string dbToMerge in Directory.GetFiles(inProgressFolder, "*.sdf"))
                            {
                                string destFile = Path.Combine(qcFolder, new FileInfo(dbToMerge).Name);
                                if (!Directory.Exists(qcFolder))
                                    Directory.CreateDirectory(qcFolder);
                                if(File.Exists(destFile))
                                    destFile = destFile.Replace(".sdf", "_" + TicketGenerator.GenerateTicketString(4) + ".sdf");

                                File.Move(dbToMerge, destFile);


                            }
                        }

                    }



                }
                firstrun = false;
                Thread.Sleep(100);
                string dots = "";
                foreach (FlowProject fp in this.FlowController.ProjectController.FlowMasterDataContext.FlowProjectList)
                {
                    //if (!this.FlowController.ProjectController.FlowServerIsOn)
                    //    break;

                    fp.FlowMasterDataContext = this.FlowController.ProjectController.FlowMasterDataContext;

                    string incomingFolder = Path.Combine(fp.DirPath, "incoming");
                    string inProgressFolder = Path.Combine(fp.DirPath, "inProgress");
                    string qcFolder = Path.Combine(fp.DirPath, "qc");

                    if (!Directory.Exists(inProgressFolder))
                        Directory.CreateDirectory(inProgressFolder);
                    string inProgressFile = Path.Combine(incomingFolder, "inProgress.txt");
                    if(Directory.Exists(Path.Combine(fp.DirPath, "incoming")))
                    {
                        foreach (string dbToMerge in Directory.GetFiles(Path.Combine(fp.DirPath, "incoming"),"*.sdf"))
                        {
                            if (!this.FlowController.ProjectController.IncomingClientProjects.Any(p => p.FilePath == dbToMerge))
                            {
                                this.dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                                {
                                    this.FlowController.ProjectController.IncomingClientProjects.Add(new IncomingProjectFile(this.FlowController, dbToMerge));
                                }));
                            }


                        }
                    }

                    if (Directory.Exists(Path.Combine(fp.DirPath, "qc")))
                    {
                        foreach (string dbToMerge in Directory.GetFiles(Path.Combine(fp.DirPath, "qc"), "*.sdf"))
                        {
                            if (!this.FlowController.ProjectController.IncomingClientProjectsQC.Any(p => p.FilePath == dbToMerge))
                            {
                                this.dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                                {
                                    this.FlowController.ProjectController.IncomingClientProjectsQC.Add(new IncomingProjectFile(this.FlowController, dbToMerge));
                                }));
                            }


                        }
                    }
                    //Thread.Sleep(100);
                }
                //while (!this.FlowController.ProjectController.FlowServerIsOn)
                //{
                //    this.FlowController.ProjectController.FlowServerStatus = "Server is Off";
                //    Thread.Sleep(3000);
                //}

                //look for new paf files to merge
                if (!Directory.Exists(pafIncomingFolder))
                    Directory.CreateDirectory(pafIncomingFolder);

                foreach(string pafFile in Directory.GetFiles(pafIncomingFolder,"*.paf"))
                {

                    if (!this.FlowController.ProjectController.IncomingClientProjects.Any(p => p.FilePath == pafFile))
                    {
                         this.dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                            {
                                this.FlowController.ProjectController.IncomingClientProjects.Add(new IncomingProjectFile(this.FlowController, pafFile));
                            }));
                    }

                }

                foreach (string pafDir in Directory.GetDirectories(pafIncomingFolder))
                {
                    //count files, and wait 10 seconds, then count again. we need to detect when files are all there.

                    int fileCount = Directory.GetFiles(pafDir).Count();
                    if(Directory.Exists(Path.Combine(pafDir, "Image")))
                        fileCount += Directory.GetFiles(Path.Combine(pafDir, "Image")).Count();

                    Thread.Sleep(10000);

                    int newFileCount = Directory.GetFiles(pafDir).Count();
                    if (Directory.Exists(Path.Combine(pafDir, "Image")))
                        newFileCount += Directory.GetFiles(Path.Combine(pafDir, "Image")).Count();

                    if (newFileCount > fileCount)
                    {
                        //dont add it if files are still being added
                        continue;
                    }

                    //get the sdf and make sure its not locked
                    string sdfFile = "";
                    foreach (string thisFile in Directory.GetFiles(pafDir))
                    {
                        if (thisFile.EndsWith("sdf"))
                            sdfFile = Path.Combine(pafDir, thisFile);
                    }
                    if (File.Exists(sdfFile) && IOUtil.IsFileInUse(sdfFile))
                    {
                        //dont add it if the sdf file is in use (its still copying)
                        continue;
                    }

                    if (!this.FlowController.ProjectController.IncomingClientProjects.Any(p => p.FilePath == pafDir))
                    {
                        this.dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                        {
                            this.FlowController.ProjectController.IncomingClientProjects.Add(new IncomingProjectFile(this.FlowController, pafDir));
                        }));
                    }

                }

                foreach (IncomingProjectFile iFile in this.FlowController.ProjectController.IncomingClientProjects.ToList())
                {
                    if (!File.Exists(iFile.FilePath) && !Directory.Exists(iFile.FilePath))
                    {
                        this.dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                            {
                                this.FlowController.ProjectController.IncomingClientProjects.Remove(iFile);
                            }));
                    }
                }
                foreach (IncomingProjectFile iFile in this.FlowController.ProjectController.IncomingClientProjectsQC.ToList())
                {
                    if (!File.Exists(iFile.FilePath))
                    {
                        this.dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                            {
                                this.FlowController.ProjectController.IncomingClientProjectsQC.Remove(iFile);
                            }));
                    }
                }
                //this.FlowController.ProjectController.FlowServerStatus = "Server is On";
            }
        }


        internal void Stop()
        {
            this.keepRunning = false;
        }
    }

    public class IncomingProjectFile : NotifyPropertyBase
    {
        public string FilePath { get; private set; }
        public FlowController FlowController { get; private set; }

        //public event PropertyChangedEventHandler PropertyChanged;

        private bool _isLocked = false;
        public bool IsLocked
        {
            get
            {
                return _isLocked;
            }
            set
            {
                _isLocked = value;
                SendPropertyChanged("IsLocked");
                SendPropertyChanged("DisplayNameShort");
            }
        }

        public string FileType
        {
            get
            {
                try
                {
                    return (new FileInfo(FilePath)).Extension;
                }
                catch
                {
                    return "";
                }
            }
        }

        public string DisplayName
        {
            get
            {
                if (this.FileType == ".paf" || this.FileType == "")
                    return this.FileName;
                FileInfo fi = new FileInfo(FilePath);
                string fileName =  fi.Name;
                string dirName = fi.Directory.Parent.Name;
                string projName = this.FlowController.ProjectController.FlowMasterDataContext.FlowProjectList.FirstOrDefault(p => p.FlowProjectGuid == new Guid(dirName)).FlowProjectName;
                return projName + " (" + fileName + ")";
            }
        }

        public string DisplayNameShort
        {
            get
            {
                if (this.FileType == ".paf")
                {
                    if (this.IsLocked)
                        return this.FileName + " (LOCKED)";

                    return this.FileName;
                }
                FileInfo fi = new FileInfo(FilePath);
                string fileName = fi.Name;
                string dirName = fi.Directory.Parent.Name;
                string projName = fileName;
                Guid rGuid;
                if (Guid.TryParse(dirName, out rGuid))
                {
                    projName = this.FlowController.ProjectController.FlowMasterDataContext.FlowProjectList.FirstOrDefault(p => p.FlowProjectGuid == rGuid).FlowProjectName;
                }
                    if (this.IsLocked)
                    return projName + " (LOCKED)";

                return projName;
            }
        }

        public string FileName
        {
            get
            {
                FileInfo fi = new FileInfo(FilePath);
                return fi.Name;
            }
        }

        public Guid ProjectGuid
        {
            get
            {
                if (this.FileType == ".paf")
                    return new Guid();//does not matter

                FileInfo fi = new FileInfo(FilePath);
                string fileName = fi.Name;
                string dirName = fi.Directory.Parent.Name;
                return new Guid(dirName);
            }
        }

        public IncomingProjectFile(FlowController fc, string pathToFile)
        {
            FilePath = pathToFile;
            FlowController = fc;
        }

        //protected void SendPropertyChanged(string propertyName)
        //{
        //    if (this.PropertyChanged != null)
        //        this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        //}
    }
}
