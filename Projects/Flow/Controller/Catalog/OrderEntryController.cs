﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

using Flow.Controller.Project;
using Flow.Controls.View.RecordDisplay;
using Flow.Lib.Helpers;
using Flow.Schema.LinqModel.DataContext;
using Flow.View.Catalog;
using Flow.Controller.Edit;
using Flow.View.Dialogs;
using System.Windows.Input;
using NLog;


namespace Flow.Controller.Catalog
{
	public class OrderEntryController
	{
        private static Logger logger = LogManager.GetCurrentClassLogger();

		internal ProjectController ProjectController { get; set; }

        internal EditContentController EditContentController { get; set; }

		public OrderEntryPanel OrderEntryPanel { get; set; }

		public OrderEntryController() { }

        public event EventHandler<RoutedEventArgs> CloseOrderEntryRequested = delegate { };

		public void Main(OrderEntryPanel orderEntryPanel)
		{
			this.OrderEntryPanel = orderEntryPanel;
	
			orderEntryPanel.DataContext = this.ProjectController;

            orderEntryPanel._this.KeyUp += new System.Windows.Input.KeyEventHandler(OrderEntryPanel_KeyUp);

            orderEntryPanel.RemoveImageOptionInvoked += new EventHandler<EventArgs<OrderImageOption>>(orderEntryPanel_RemoveImageOptionInvoked);
            orderEntryPanel.AddCatalogOptionInvoked += new EventHandler<EventArgs<FlowCatalogOption>>(orderEntryPanel_AddCatalogOptionInvoked);

            orderEntryPanel.RemoveSubjectOrderOptionInvoked += new EventHandler<EventArgs<OrderSubjectOrderOption>>(orderEntryPanel_RemoveSubjectOrderOptionInvoked);

			orderEntryPanel.AddOrderPackageInvoked += new EventHandler<EventArgs<ProductPackage>>(OrderEntryPanel_AddOrderPackageInvoked);
			orderEntryPanel.AddOrderPackageToAllInvoked += new EventHandler<EventArgs<KeyValuePair<ProductPackage, string>>>(OrderEntryPanel_AddOrderPackageToAllInvoked);
	
			orderEntryPanel.DuplicateOrderPackageInvoked += new EventHandler<EventArgs<OrderPackage>>(OrderEntryPanel_DuplicateOrderPackageInvoked);
			orderEntryPanel.RemoveOrderPackageInvoked += new EventHandler<EventArgs<OrderPackage>>(OrderEntryPanel_RemoveOrderPackageInvoked);

			orderEntryPanel.AssignPreviousImageRequested += new EventHandler<EventArgs<OrderProductNode>>(OrderEntryPanel_AssignPreviousImageRequested);
			orderEntryPanel.AssignNullImageRequested += new EventHandler<EventArgs<OrderProductNode>>(OrderEntryPanel_AssignNullImageRequested);
			orderEntryPanel.AssignNextImageRequested += new EventHandler<EventArgs<OrderProductNode>>(OrderEntryPanel_AssignNextImageRequested);

            orderEntryPanel.CloseOrderEntryRequested += new EventHandler<RoutedEventArgs>(orderEntryPanel_CloseOrderEntryRequested);

            orderEntryPanel.ClearKeyHistoryRequested += new EventHandler<System.Windows.Input.MouseButtonEventArgs>(orderEntryPanel_ClearKeyHistoryRequested);
			
            //orderEntryPanel.ResolveIncompleteOrdersButton.Click += new RoutedEventHandler(ResolveIncompleteOrdersButton_Click);
            //orderEntryPanel.ClearResolutionFilterButton.Click += new RoutedEventHandler(ClearResolutionFilterButton_Click);

			orderEntryPanel.ResolveMissingImagesButton.Click += new RoutedEventHandler(ResolveMissingImagesButton_Click);
			orderEntryPanel.ClearMissingImageResolutionFilterButton.Click += new RoutedEventHandler(ClearResolutionFilterButton_Click);

			orderEntryPanel.SavePaymentInfoButton.Click += delegate { this.ProjectController.CurrentFlowProject.Save(); };

			this.ProjectController.CurrentFlowProject.SubjectList.CurrentChanged += new EventHandler<EventArgs<Subject>>(SubjectList_CurrentChanged);
			//this.ProjectController.CurrentFlowProject.HasIncompleteProductsChanged += new EventHandler<EventArgs<bool>>(CurrentFlowProject_HasIncompleteProductsChanged);
			this.ProjectController.CurrentFlowProject.HasMissingImagesChanged += new EventHandler<EventArgs<bool>>(CurrentFlowProject_HasMissingImagesChanged);


			//this.SetProductOrder(this.ProjectController.CurrentFlowProject.SubjectList.CurrentItem);


		}


        void orderEntryPanel_RemoveImageOptionInvoked(object sender, EventArgs<OrderImageOption> e)
        {
            RemoveImageOption(e.Data);
        }

        private void RemoveImageOption(OrderImageOption orderImageOption)
        {
            Subject subject = this.ProjectController.CurrentFlowProject.SubjectList.CurrentItem;
            subject.RemoveCatalogOption(orderImageOption);

            logger.Info("Removed image option '{0}' from current subject", orderImageOption.FlowCatalogOption.ImageQuixCatalogOption.Label);

           // this.ProjectController.CurrentFlowProject.RefreshHasIncompleteProducts();

           // if (EditContentController != null)
           //     EditContentController.ApplyFilter();
            this.ProjectController.CurrentFlowProject.SubjectList.CurrentItem = subject;
            this.ProjectController.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();
        }

        void orderEntryPanel_RemoveSubjectOrderOptionInvoked(object sender, EventArgs<OrderSubjectOrderOption> e)
        {
            RemoveSubjectOrderOption(e.Data);
        }

        private void RemoveSubjectOrderOption(OrderSubjectOrderOption orderSubjectOrderOption)
        {
            Subject subject = this.ProjectController.CurrentFlowProject.SubjectList.CurrentItem;
            subject.RemoveCatalogOption(orderSubjectOrderOption);

            logger.Info("Removed subject order option '{0}' from current subject", orderSubjectOrderOption.FlowCatalogOption.ImageQuixCatalogOption.Label);

            // this.ProjectController.CurrentFlowProject.RefreshHasIncompleteProducts();

            // if (EditContentController != null)
            //     EditContentController.ApplyFilter();
            this.ProjectController.CurrentFlowProject.SubjectList.CurrentItem = subject;
        }


        void orderEntryPanel_AddCatalogOptionInvoked(object sender, EventArgs<FlowCatalogOption> e)
        {
            AddCatalogOption(e.Data);
        }

        public void AddCatalogOption(FlowCatalogOption flowCatalogOption)
        {
            Subject subject = this.ProjectController.CurrentFlowProject.SubjectList.CurrentItem;
            if (subject == null || subject.SubjectImages.Count < 1)
            {
                FlowMessageBox msg = new FlowMessageBox("Add Option", "You can not add an Option to a subject that has no images.\nYou must first add an image to the subject.");
                msg.CancelButtonVisible = false;
                msg.ShowDialog();
                return;
            }
            //SubjectDataRow will be null if adding a new subject and have not hit saved.
            if (subject.SubjectDataRow != null)
            {
                this.SetProductOrder(subject);

                logger.Info("Adding catalog option '{0}' to current subject", flowCatalogOption.ImageQuixCatalogOption.Label);
                if (this.ProjectController.FlowMasterDataContext.IsPUDConfiguration)
                {
                    if (subject.SubjectOrder.OrderPackages.Count() == 0)
                    {
                        FlowMessageBox msg = new FlowMessageBox("Add Option", "You can not add an Option to a subject that has no packages.\nYou must first add a package to the subject.");
                        msg.CancelButtonVisible = false;
                        msg.ShowDialog();
                        return;
                    }
                    OrderPackage op = subject.SubjectOrder.OrderPackages.Last();
                    OrderProduct prod = op.OrderProducts.First();
                    {
                        OrderProductOption prodOption = prod.OrderProductOptions.FirstOrDefault(opo=>opo.ImageQuixOptionPk == null);
                        if (prodOption == null && prod.OrderProductOptions.Count > 0) prodOption = prod.OrderProductOptions.Last();

                        prod.TempOrderPackage = op;
                        //ImageQuixOption iqOption = this.ProjectController.FlowMasterDataContext.ImageQuixOptions.First(iqo => iqo.ResourceURL == flowCatalogOption.Map);

                        FlowCatalogOption fcOption = this.ProjectController.CurrentFlowProject.FlowCatalog.FlowCatalogOptions.First(iqo => iqo.Map == flowCatalogOption.Map);

                        //foreach (ImageQuixOption o in this.ProjectController.FlowMasterDataContext.ImageQuixCatalogOptions.ImageQuixOptions)
                        //{
                        //    logger.Info(o.ResourceURL);
                        //}

                        ImageQuixOption iqOption = this.ProjectController.FlowMasterDataContext.ImageQuixOptions.FirstOrDefault(iqo => iqo.ResourceURL == fcOption.ImageQuixCatalogOption.ResourceURL);
                        if (iqOption != null)
                        {
                            string optionGroupLabel = "";
                            if (iqOption.ImageQuixOptionGroup != null)
                                optionGroupLabel = iqOption.ImageQuixOptionGroup.Label;
                            this.AddImageOption(prodOption, iqOption.Label, optionGroupLabel, fcOption.Map);
                        }
                        else
                        {
                            subject.AddCatalogOption(flowCatalogOption);
                            //ImageQuixCatalogOption iqcOption = this.ProjectController.FlowMasterDataContext.ImageQuixCatalogOptions.FirstOrDefault(iqo => iqo.ResourceURL == fcOption.ImageQuixCatalogOption.ResourceURL);
                            //string optionGroupLabel = "";
                            //if (iqcOption.ImageQuixCatalogOptionGroup != null)
                            //    optionGroupLabel = iqcOption.ImageQuixCatalogOptionGroup.Label;
                            //this.AddImageOption(prodOption, iqcOption.Label, optionGroupLabel);
                        }
                        
                    }

                    //OrderPackage op = subject.SubjectOrder.OrderPackages.Last();
                    //op.OrderProducts[0].OrderProductNodes.Where(opn=>opn.SubjectImage !=null).First().SubjectImage.AddCatalogOption(flowCatalogOption);
                    op.UpdatePackageOptions();
                }
                else
                {
                    subject.AddCatalogOption(flowCatalogOption);
                }

                this.ProjectController.CurrentFlowProject.RefreshHasIncompleteProducts();

                if (EditContentController != null)
                    EditContentController.ApplyFilter();

                this.ProjectController.CurrentFlowProject.SubjectList.CurrentItem = subject;

                subject.SubjectOrder.UpdateOrderPackages();
                
                //this.ProjectController.CurrentFlowProject.SubjectList.MoveCurrentToNext();
                //this.ProjectController.CurrentFlowProject.SubjectList.MoveCurrentToPrevious();
            }
            else
            {
                FlowMessageBox msg = new FlowMessageBox("Please Save Subject", "You must Save the Current Subject Before adding an image option");
                msg.CancelButtonVisible = false;
                msg.ShowDialog();
            }
            SetDefaultFocus();
        }

        public void AddImageOption(OrderProductOption thisOption, string thisOptionLabel, string thisOPtionGroupLabel, string thisOptionMap)
        {
            OrderPackage op = thisOption.OrderProduct.TempOrderPackage;
            if (op == null) return;

            //just need to init this object by calling it
            object temp = thisOption.OrderProduct.OrderProductOptionList;

            if (thisOption == null) return;

            //string thisOptionLabel = iqOption.Label;
            //string thisOptionGroupLabel = null;
            //if (iqOption.ImageQuixOptionGroup != null)
             //   thisOptionGroupLabel = iqOption.ImageQuixOptionGroup.Label;


            //string thisOptionLabel = this.ProjectController.CurrentFlowProject.FlowMasterDataContext.ImageQuixCatalogOptionGroups.First(g => g.ImageQuixCatalogID == this.ProjectController.CurrentFlowProject.FlowCatalogID && g.PrimaryKey == thisOption.ImageQuixOptionGroupPk).ImageQuixCatalogOptions.First(o => o.PrimaryKey == thisOption.ImageQuixOptionPk).Label;
            foreach (OrderProduct prod in op.OrderProducts)
            {
                foreach (OrderProductOption opt in prod.OrderProductOptionList)
                {

                    if (opt.OptionGroupLabel == thisOption.OptionGroupLabel &&
                        this.ProjectController.CurrentFlowProject.FlowMasterDataContext.ImageQuixOptions.Any(i => i.ImageQuixOptionGroupPk == opt.ImageQuixOptionGroupPk && thisOptionLabel == "<None>"))
                    {
                        opt.ImageQuixOptionPk = -1;
                        opt.ImageQuixOptionPkDisplay = -1;
                    }

                    if (opt.OptionGroupLabel == thisOption.OptionGroupLabel &&
                        this.ProjectController.CurrentFlowProject.FlowMasterDataContext.ImageQuixOptions.Any(i => i.ImageQuixOptionGroupPk == opt.ImageQuixOptionGroupPk && i.Label == thisOptionLabel))
                    {
                        opt.ImageQuixOptionPk = this.ProjectController.CurrentFlowProject.FlowMasterDataContext.ImageQuixOptions.First(i => i.ImageQuixOptionGroupPk == opt.ImageQuixOptionGroupPk && i.Label == thisOptionLabel).PrimaryKey;
                        opt.ImageQuixOptionPkDisplay = (int)opt.ImageQuixOptionPk;
                        opt.Map = thisOptionMap;

                    }

                }
            }
            

            if (!this.ProjectController.CurrentFlowProject.IsNew)
            {
                //if (((ComboBox)sender).SelectionBoxItem.GetType() == typeof(ImageQuixOption))
                this.ProjectController.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();
            }
            SetDefaultFocus();

        }

        void orderEntryPanel_ClearKeyHistoryRequested(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            clearKeyMap();
        }

        public void clearKeyMap()
        {
            this.keyHistory = "";
            OrderEntryPanel.lblCurrentKeyMap.Content = keyHistory;
            //this.SetDefaultFocus();
        }

        public void SetDefaultFocus()
        {
            if(this.OrderEntryPanel != null)
                this.OrderEntryPanel.SetDefaultFocus();
        }

        void orderEntryPanel_CloseOrderEntryRequested(object sender, RoutedEventArgs e)
        {
            clearKeyMap();
            this.CloseOrderEntryRequested(sender, e);
            logger.Info("Close Order Entry requested");
        }

		void OrderEntryPanel_AddOrderPackageInvoked(object sender, EventArgs<ProductPackage> e)
		{
            AddOrderPackage(e.Data);
		}

        public void AddOrderPackage(ProductPackage pp)
        {
            
            Subject subject = this.ProjectController.CurrentFlowProject.SubjectList.CurrentItem;
            if (subject == null || subject.SubjectImages.Count < 1)
            {
                FlowMessageBox msg = new FlowMessageBox("Add Package", "You can not add a package to a subject that has no images.\nYou must first add an image to the subject.");
                msg.CancelButtonVisible = false;
                msg.ShowDialog();
                return;
            }

            if (pp.ProductPackageCompositionList.IsEmpty)
            {
                FlowMessageBox msg = new FlowMessageBox("Add Package", "You can not add this package to a subject because the package contains no products.\nYou must first add an item to the package in the Catalog panel.");
                msg.CancelButtonVisible = false;
                msg.ShowDialog();
                return;
            }
            if (subject.SubjectOrder.OrderPackages.Count() == 0)
                subject.SubjectOrder.Directshipment = false;

            //SubjectDataRow will be null if adding a new subject and have not hit saved.
            if (subject.SubjectDataRow != null)
            {
                this.SetProductOrder(subject);

                logger.Info("Adding order package '{0}' to current subject", pp.ProductPackageDesc);
                subject.AddOrderPackage(pp);
               

                subject.SubjectOrder.ReCalculateGrandTotal();
                this.ProjectController.CurrentFlowProject.RefreshHasIncompleteProducts();

                if (EditContentController != null)
                    EditContentController.ApplyFilter();
                this.ProjectController.CurrentFlowProject.SubjectList.CurrentItem = subject;
            }
            else
            {
                FlowMessageBox msg = new FlowMessageBox("Please Save Subject", "You must Save the Current Subject Before adding a package");
                msg.CancelButtonVisible = false;
                msg.ShowDialog();
            }
            SetDefaultFocus();
        }


		void OrderEntryPanel_AddOrderPackageToAllInvoked(object sender, EventArgs<KeyValuePair<ProductPackage, string>> e)
		{
            //logger.Info("Adding order package '{0}' to all subjects", e.Data.Key.ProductPackageTitle);

			this.ProjectController.CurrentFlowProject.AddProductPackageToAllSubjects(e.Data.Key, e.Data.Value);
			this.ProjectController.CurrentFlowProject.RefreshHasIncompleteProducts();
            SetDefaultFocus();
		}

		void OrderEntryPanel_DuplicateOrderPackageInvoked(object sender, EventArgs<OrderPackage> e)
		{
            //logger.Info("Duplicating order package '{0}' for current subject", e.Data.ProductPackage.ProductPackageTitle);

            //before we would duplicate, we reall want to Increase qty
			//this.ProjectController.CurrentFlowProject.SubjectList.CurrentItem.DuplicateOrderPackage(e.Data);
            ((OrderPackage)e.Data).Qty++;
            ((OrderPackage)e.Data).SubjectOrder.Subject.UpdatePackageSummary();
            this.ProjectController.CurrentFlowProject.SubjectList.CurrentItem.SubjectOrder.ReCalculateGrandTotal();

            SetDefaultFocus();
		}

		void OrderEntryPanel_RemoveOrderPackageInvoked(object sender, EventArgs<OrderPackage> e)
		{
            //logger.Info("Removing order package '{0}' from current subject", e.Data.ProductPackage.ProductPackageTitle);

            if (((OrderPackage)e.Data).Qty > 1)
            {
                ((OrderPackage)e.Data).Qty--;
                ((OrderPackage)e.Data).SubjectOrder.Subject.UpdatePackageSummary();
            }
            else
            {
                Subject subject = this.ProjectController.CurrentFlowProject.SubjectList.CurrentItem;

                this.ProjectController.CurrentFlowProject.SubjectList.CurrentItem.RemoveOrderPackage(e.Data);

                //this.ProjectController.CurrentFlowProject.RefreshHasIncompleteProducts();

                if (EditContentController != null)
                    EditContentController.ApplyFilter();
                this.ProjectController.CurrentFlowProject.SubjectList.CurrentItem = subject;
            }
            this.ProjectController.CurrentFlowProject.SubjectList.CurrentItem.SubjectOrder.ReCalculateGrandTotal();
		}


		void OrderEntryPanel_AssignPreviousImageRequested(object sender, EventArgs<OrderProductNode> e)
		{
            logger.Info("Assign Previous Image requested");
			this.SetOrderProductNode(e.Data, false);
		}

		void OrderEntryPanel_AssignNullImageRequested(object sender, EventArgs<OrderProductNode> e)
		{
            logger.Info("Assign Null Image requested");
			this.SetOrderProductNode(e.Data, null);
		}

		void OrderEntryPanel_AssignNextImageRequested(object sender, EventArgs<OrderProductNode> e)
		{
            logger.Info("Assign Next Image requested");
			this.SetOrderProductNode(e.Data, true);
		}

		void ResolveIncompleteOrdersButton_Click(object sender, RoutedEventArgs e)
		{
            logger.Info("Clicked Resolve Incomplete Orders button");

			this.ProjectController.OpenCapture();

			foreach (Subject subject in this.ProjectController.CurrentFlowProject.SubjectList)
			{
				subject.SetProductOrder(this.ProjectController.CurrentFlowProject.FlowCatalog);
                subject.FlagForMerge = true;
			}

			this.ProjectController.CurrentFlowProject.SubjectList.ApplyResolutionFilter();
		}

		void ClearResolutionFilterButton_Click(object sender, RoutedEventArgs e)
		{
            logger.Info("Clicked Clear Resolution Filter button");

			this.ProjectController.CurrentFlowProject.SubjectList.ClearFilter();
			this.ProjectController.CurrentFlowProject.SubjectList.MoveCurrentToFirst();
            if (ProjectController != null && ProjectController.CurrentFlowProject != null)
                this.ProjectController.CurrentFlowProject.RefreshHasMissingImages();
		}

		void CurrentFlowProject_HasIncompleteProductsChanged(object sender, EventArgs<bool> e)
		{
			if (!e.Data)
				this.ProjectController.CurrentFlowProject.SubjectList.ClearFilter();
		}

		void ResolveMissingImagesButton_Click(object sender, RoutedEventArgs e)
		{
            logger.Info("Clicked Resolve Missing Images button");

			this.ProjectController.OpenCapture();

			foreach (Subject subject in this.ProjectController.CurrentFlowProject.SubjectList)
			{
				subject.SetProductOrder(this.ProjectController.CurrentFlowProject.FlowCatalog);
                subject.FlagForMerge = true;
			}

			this.ProjectController.CurrentFlowProject.SubjectList.ApplyMissingImageFilter();
		}

		void CurrentFlowProject_HasMissingImagesChanged(object sender, EventArgs<bool> e)
		{
			//if (!e.Data)
				//this.ProjectController.CurrentFlowProject.SubjectList.ClearFilter();
		}

		void SubjectList_CurrentChanged(object sender, EventArgs<Subject> e)
		{
//			this.OrderEntryPanel.OrderPackagesTabControl.SelectedIndex = 0;
            this.OrderEntryPanel.btnCloseOrderEntry.Focus();

            if (e.Data != null && !((Subject)e.Data).IsNew)
            {
                logger.Info("Current subject changed to '{0}'", e.Data.FormalFullName);
                //this.SetProductOrder(e.Data);
            }
		}

		private void SetProductOrder(Subject subject)
		{
			if (this.ProjectController.CurrentFlowProject.FlowCatalog != null && subject != null)
			{
				subject.SetProductOrder(this.ProjectController.CurrentFlowProject.FlowCatalog);
			}
		}

        string keyHistory = "";
        DateTime lastkeyuptime = DateTime.Now;
        void OrderEntryPanel_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (lastkeyuptime < DateTime.Now.AddSeconds(-5))
                clearKeyMap();
            lastkeyuptime = DateTime.Now;

            if (e.Key == Key.Return)
            {
                if (keyHistory != "")
                {
                    foreach (FlowCatalogOption fco in this.ProjectController.CurrentFlowProject.FlowCatalog.FlowCatalogOptions)
                    {
                        if (fco.Map.ToLower() == keyHistory.ToLower())
                        {
                            logger.Info("Keymap '{0}' matched Flow catalog option '{1}'", fco.Map, fco.ImageQuixCatalogOption.Label);
                            AddCatalogOption(fco);
                            clearKeyMap();
                            return;
                        }
                    }
                    foreach (ProductPackage pp in this.ProjectController.CurrentFlowProject.FlowCatalog.ProductPackages)
                    {
                        if (pp.Map.ToLower() == keyHistory.ToLower())
                        {
                            logger.Info("Keymap '{0}' matched product package '{1}'", pp.Map, pp.ProductPackageTitle);
                            AddOrderPackage(pp);
                            clearKeyMap();
                            return;
                        }
                    }

                    clearKeyMap();
                    return;
                }
            }

            if (e.Key == Key.Back || e.Key == Key.Delete || e.Key == Key.Return)
            {
                clearKeyMap();
                return;
            }

            if (this.ProjectController.CurrentFlowProject.FlowCatalog == null || this.ProjectController.CurrentFlowProject.FlowCatalog.ProductPackages == null)
                return;

            bool foundStartsWith = false;
            if (e.Key == Key.D0 || e.Key == Key.D1 || e.Key == Key.D2 || e.Key == Key.D3 || e.Key == Key.D4 || e.Key == Key.D5 || e.Key == Key.D6 || e.Key == Key.D7 || e.Key == Key.D8 || e.Key == Key.D9)
            {
                keyHistory = keyHistory + e.Key.ToString().Replace("D", "");
            }
            else
            {
                keyHistory = keyHistory + e.Key;
            }
            foreach (FlowCatalogOption fco in this.ProjectController.CurrentFlowProject.FlowCatalog.FlowCatalogOptions)
            {
               
                if(fco.Map.ToLower().StartsWith(keyHistory.ToLower()))
                    foundStartsWith = true;
            }
            foreach (ProductPackage pp in this.ProjectController.CurrentFlowProject.FlowCatalog.ProductPackages)
            {
               
                if (pp.Map.ToLower().StartsWith(keyHistory.ToLower()))
                    foundStartsWith = true;
            }
            if (!foundStartsWith)
            {
                clearKeyMap();
                return;
            }
            OrderEntryPanel.lblCurrentKeyMap.Content = keyHistory;
        }


		private void SetOrderProductNode(OrderProductNode orderProductNode, bool? direction)
		{
			Subject currentSubject = this.ProjectController.CurrentFlowProject.SubjectList.CurrentItem;

			if (currentSubject != null)
			{
				SubjectImage currentImage = currentSubject.ImageList.CurrentItem;

				if (currentImage != null)
				{
					if (orderProductNode.SubjectImage == null)
						orderProductNode.SubjectImage = currentImage;
					else
					{
						currentSubject.ImageList.MoveCurrentTo(orderProductNode.SubjectImage);

						if (direction.HasValue)
						{
							if (direction.Value)
								currentSubject.ImageList.MoveCurrentToNext();
							else
								currentSubject.ImageList.MoveCurrentToPrevious();

							orderProductNode.SubjectImage = currentSubject.ImageList.CurrentItem;
						}
						else
						{
							orderProductNode.SubjectImage = null;
						}
					}
				}

				this.ProjectController.CurrentFlowProject.Save();
				this.ProjectController.CurrentFlowProject.RefreshHasIncompleteProducts();
			}
		}


	}	// END class

}	// END namespace
