﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.IO;
using System.Text;
using System.Windows;

using Flow.Controls.View.RecordDisplay;
using Flow.Lib;
using Flow.Lib.Helpers;
using Flow.Schema.LinqModel.DataContext;
using Flow.View.Catalog;
using Flow.View.Dialogs;

namespace Flow.Controller.Catalog
{
	/// NOTE: this class is obsolete; functionality has been transfered to FlowCatalogController

	public class ImageQuixCatalogController
	{
		internal FlowController FlowController { get; set; }

		internal ImageQuixCatalogPanel ImageQuixCatalogPanel { get; set; }

		public ImageQuixCatalogController() {}

		public void Main(FlowController flowController, ImageQuixCatalogPanel iqcp)
		{
			this.FlowController = flowController;
			this.ImageQuixCatalogPanel = iqcp;

			this.ImageQuixCatalogPanel.DataContext = this.FlowController.ProjectController.FlowMasterDataContext;

			this.ImageQuixCatalogPanel.RetrieveImageQuixCatalogListButton.Click += new RoutedEventHandler(RetrieveImageQuixCatalogListButton_Click);
			this.ImageQuixCatalogPanel.ImportImageQuixCatalogsButton.Click += new RoutedEventHandler(ImportImageQuixCatalogsButton_Click);

			this.ImageQuixCatalogPanel.ClearCatalogsButton.Click += new RoutedEventHandler(ClearCatalogsButton_Click);
			this.ImageQuixCatalogPanel.CancelClearCatalogsButton.Click += new RoutedEventHandler(CancelClearCatalogsButton_Click);
			this.ImageQuixCatalogPanel.ConfirmClearCatalogsButton.Click += new RoutedEventHandler(ConfirmClearCatalogsButton_Click);
		}

		void RetrieveImageQuixCatalogListButton_Click(object sender, RoutedEventArgs e)
		{
            //
            //this can all be ignored, we are taking care of this else where (in flow catalog controller)
            //

            //if (this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.CatalogRequest.ImageQuixBaseUrl.EndsWith(".pud"))
            //{
               
            //}
            //if (!this.FlowController.ProjectController.FlowMasterDataContext.RefreshRemoteImageQuixCatalogList())
            //{
            //    FlowMessageBox msg = new FlowMessageBox("Lab Catalogs", "There are no Lab Catalogs Available");
            //    msg.CancelButtonVisible = false;
            //    msg.ShowDialog();
            //    return;
            //}
		}

		void ImportImageQuixCatalogsButton_Click(object sender, RoutedEventArgs e)
		{
			this.FlowController.ProjectController.FlowMasterDataContext.ImportMarkedRemoteImageQuixCatalogs(null, null);
            this.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogList.CurrentItem.RefreshProductList();
			this.FlowController.Catalog();
           
		}

		void ClearCatalogsButton_Click(object sender, RoutedEventArgs e)
		{
			this.ImageQuixCatalogPanel.ClearCatalogsConfirmPanelVisibility = Visibility.Visible;
		}

		void CancelClearCatalogsButton_Click(object sender, RoutedEventArgs e)
		{
			this.ImageQuixCatalogPanel.ClearCatalogsConfirmPanelVisibility = Visibility.Collapsed;
		}

		void ConfirmClearCatalogsButton_Click(object sender, RoutedEventArgs e)
		{
			this.FlowController.ProjectController.FlowMasterDataContext.ClearCatalogs();

			this.ImageQuixCatalogPanel.ClearCatalogsConfirmPanelVisibility = Visibility.Collapsed;
		}

	}
}
