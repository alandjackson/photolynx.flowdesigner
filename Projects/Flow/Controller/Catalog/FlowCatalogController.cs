﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.IO;
using System.Text;
using System.Windows;

using Flow.Controls.View.RecordDisplay;
using Flow.Lib;
using Flow.Lib.Helpers;
using Flow.Schema.LinqModel.DataContext;
using Flow.View.Catalog;
using Flow.View.Dialogs;
using StructureMap;
using Flow.Lib.AsyncWorker;
using NLog;
using System.Windows.Threading;

namespace Flow.Controller.Catalog
{
	public class FlowCatalogController
	{
		internal FlowController FlowController { get; set; }

		internal CatalogDockPanel CatalogDockPanel { get; set; }
		internal FlowCatalogEditor FlowCatalogEditor { get { return this.CatalogDockPanel.FlowCatalogEditor; } }
        internal ImageQuixCatalogPanel ImageQuixCatalogPanel { get { if (this.CatalogDockPanel == null) return null; else return this.CatalogDockPanel.ImageQuixCatalogPanel; } }

       

        private NotificationProgressInfo _notificationInfo;

        private static Logger logger = LogManager.GetCurrentClassLogger();

		//private FlowObservableCollection<FlowCatalog> FlowCatalogList { get {return this.FlowController.ProjectController.FlowMasterDataContext.FlowCatalogList;} }

		public FlowCatalogController()
		{
		}

		public void Main(FlowController fc, CatalogDockPanel fce)
		{
			this.FlowController = fc;
			this.CatalogDockPanel = fce;

			this.CatalogDockPanel.DataContext = this.FlowController.ProjectController.FlowMasterDataContext;

			//this.FlowCatalogList = this.FlowController.ProjectController.FlowMasterDataContext.FlowCatalogList;

			if (!this.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogList.HasItems)
				this.CatalogDockPanel.ImageQuixCatalogDocumentContent.SetAsActive();

			this.InitializeFlowCatalogEditor();
			this.InitializeImageQuixCatalogEditor();

            //this.FlowCatalogEditor.AllowCatalogEditingClicked += new EventHandler<EventArgs>(FlowCatalogEditor_AllowCatalogEditingClicked);
            this.FlowCatalogEditor.DuplicateCatalogClicked += new EventHandler<EventArgs<FlowCatalog>>(FlowCatalogEditor_DuplicateCatalogClicked);
		}

        void FlowCatalogEditor_DuplicateCatalogClicked(object sender, EventArgs<FlowCatalog> e)
        {
            logger.Info("Duplicate Catalog clicked: name='{0}' guid='{1}'",
               e.Data.FlowCatalogDesc,
               e.Data.FlowCatalogGuid);

            FlowCatalog newCatalog = new FlowCatalog(e.Data);
            this.FlowController.ProjectController.FlowMasterDataContext.FlowCatalogList.Add(newCatalog);
            this.FlowController.ProjectController.FlowMasterDataContext.FlowCatalogs.InsertOnSubmit(newCatalog);
            this.FlowController.ProjectController.FlowMasterDataContext.SubmitChanges();
            this.FlowController.ProjectController.FlowMasterDataContext.RefreshFlowCatalogList();
        }
        //void FlowCatalogEditor_AllowCatalogEditingClicked(object sender, EventArgs e)
        //{
        //    FlowMessageBox msg = new FlowMessageBox("Catalog Edit Warning", "");
        //    msg.txtMessage.Text = "\nThis catalog is already assigned to a project.\nIf you make any changes to this catalog, it might negativly affect your project.\n\nHowever, if you have not assigned any of these packages to a subject\nand if you have not exported this project, you can continue.\n\n\n";
        //    //msg.buttonCancel.Height = 100;
        //    msg.Height = 600;
        //    if((bool)msg.ShowDialog())
        //        (this.FlowCatalogEditor.FlowCatalogsComboBox.SelectedItem as FlowCatalog).AllowCatalogEditing = true;
        //}

		private void InitializeFlowCatalogEditor()
		{
			this.FlowCatalogEditor.FlowCatalogEditButtonStrip.SetSourceCollection<FlowCatalog>(
                this.FlowController.ProjectController.FlowMasterDataContext.FlowCatalogList
			);

			this.FlowCatalogEditor.FlowCatalogEditButtonStrip.EditStateChanged +=
				new EventHandler<EditStateChangedEventArgs>(CollectionEditButtonStrip_EditStateChanged);

			this.FlowCatalogEditor.ConfirmDeleteButton.Click += new RoutedEventHandler(ConfirmDeleteButton_Click);
			this.FlowCatalogEditor.CancelDeleteButton.Click += new RoutedEventHandler(CancelDeleteButton_Click);

			this.FlowCatalogEditor.CreateProductPackageButton.Click += new RoutedEventHandler(CreateProductPackageButton_Click);

			this.FlowCatalogEditor.EditItemInvoked += new EventHandler<EventArgs<ProductPackage>>(ProductCatalogEditor_EditItemInvoked);
			this.FlowCatalogEditor.DeleteItemInvoked += new EventHandler<EventArgs<ProductPackage>>(ProductCatalogEditor_DeleteItemInvoked);

            this.FlowCatalogEditor.SaveChangesInvoked += new EventHandler<EventArgs>(FlowCatalogEditor_SaveChangesInvoked);
		}

        void FlowCatalogEditor_SaveChangesInvoked(object sender, EventArgs e)
        {
            this.FlowController.ProjectController.FlowMasterDataContext.SubmitChanges();
        }

		void CollectionEditButtonStrip_EditStateChanged(object sender, EditStateChangedEventArgs e)
		{
			switch (e.OldEditState)
			{
				case EditState.View:

					switch (e.NewEditState)
					{
						case EditState.Add:
							this.FlowCatalogEditor.EditingControlsEnabled = true;
                            this.FlowController.ProjectController.FlowMasterDataContext.FlowCatalogList.CreateNew();
                            this.FlowCatalogEditor.addEditFlowCatalogFields.Visibility = Visibility.Visible;
                            this.FlowCatalogEditor.ImageQuixCatalogsComboBox.SelectedIndex = 0;
                            this.FlowCatalogEditor.FlowCatalogDescriptionTextBox.Focus();
                            if (this.FlowCatalogEditor.FlowCatalogDescriptionTextBox.Text.Length > 0)
                                this.FlowCatalogEditor.FlowCatalogEditButtonStrip.IsSaveEnabled = true;
                            else
                                this.FlowCatalogEditor.FlowCatalogEditButtonStrip.IsSaveEnabled = false;
							break;

						case EditState.Edit:
                            this.FlowCatalogEditor.addEditFlowCatalogFields.Visibility = Visibility.Visible;
                            this.FlowCatalogEditor.FlowCatalogDescriptionTextBox.Focus();
							this.FlowCatalogEditor.EditingControlsEnabled = true;
                            if (this.FlowCatalogEditor.FlowCatalogDescriptionTextBox.Text.Length > 0)
                                this.FlowCatalogEditor.FlowCatalogEditButtonStrip.IsSaveEnabled = true;
                            else
                                this.FlowCatalogEditor.FlowCatalogEditButtonStrip.IsSaveEnabled = false;
							break;

						case EditState.Delete:
							this.FlowCatalogEditor.DeleteConfirmationPrompt.Visibility = Visibility.Visible;
							break;
					}
					break;


				case EditState.Add:
					switch (e.NewEditState)
					{
						case EditState.Save:
                            if (this.FlowController.ProjectController.FlowMasterDataContext.FlowCatalogList.CurrentItem.FlowCatalogDesc == null || this.FlowController.ProjectController.FlowMasterDataContext.FlowCatalogList.CurrentItem.FlowCatalogDesc.Length < 1)
                            {
                                FlowMessageBox msg = new FlowMessageBox("Flow Catalog", "You Must fill in the Description field");
                                msg.CancelButtonVisible = false;
                                msg.ShowDialog();
                                this.FlowController.ProjectController.FlowMasterDataContext.FlowCatalogList.CancelNew();
                                this.FlowCatalogEditor.EditingControlsEnabled = false;
                                this.FlowCatalogEditor.FlowCatalogEditButtonStrip.EditState = EditState.View;
                                return;
                            }
                            if (this.FlowCatalogEditor.ImageQuixCatalogsComboBox.SelectedItem == null ||  this.FlowCatalogEditor.ImageQuixCatalogsComboBox.SelectedIndex == -1)
                            {
                                FlowMessageBox msg = new FlowMessageBox("Flow Catalog", "You Must select a lab catalog");
                                msg.CancelButtonVisible = false;
                                msg.ShowDialog();
                                this.FlowController.ProjectController.FlowMasterDataContext.FlowCatalogList.CancelNew();
                                this.FlowCatalogEditor.EditingControlsEnabled = false;
                                this.FlowCatalogEditor.FlowCatalogEditButtonStrip.EditState = EditState.View;
                                return;
                            }

                            this.FlowController.ProjectController.FlowMasterDataContext.FlowCatalogList.CurrentItem.FlowMasterDataContext =
								this.FlowController.ProjectController.FlowMasterDataContext;

                            this.FlowController.ProjectController.FlowMasterDataContext.FlowCatalogList.CurrentItem.FlowCatalogGuid = Guid.NewGuid();

                            this.FlowController.ProjectController.FlowMasterDataContext.FlowCatalogList.CommitNew();
							this.FlowController.ProjectController.FlowMasterDataContext.SubmitChanges();

							this.FlowCatalogEditor.EditingControlsEnabled = false;
							this.FlowCatalogEditor.FlowCatalogEditButtonStrip.EditState = EditState.View;

                            this.FlowController.ProjectController.FlowMasterDataContext.FlowCatalogList.CurrentItem.ImageQuixCatalog.RefreshCatalogOptionList();
                            this.FlowController.ProjectController.FlowMasterDataContext.FlowCatalogList.CurrentItem.InitCatalogOptions(this.FlowController.ProjectController.FlowMasterDataContext, this.FlowController.ProjectController.FlowMasterDataContext.FlowCatalogList.CurrentItem.ImageQuixCatalog);
                            this.FlowController.ProjectController.FlowMasterDataContext.FlowCatalogList.CurrentItem.RefreshFlowCatalogOptions();
                            this.FlowCatalogEditor.CatalogOptionList.ItemsSource = this.FlowController.ProjectController.FlowMasterDataContext.FlowCatalogList.CurrentItem.FlowCatalogOptionList;
                            this.FlowCatalogEditor.CatalogOptionList.UpdateLayout();
                            this.FlowCatalogEditor.addEditFlowCatalogFields.Visibility = Visibility.Collapsed;
							break;

						case EditState.Cancel:
                            this.FlowController.ProjectController.FlowMasterDataContext.FlowCatalogList.CancelNew();

							this.FlowCatalogEditor.EditingControlsEnabled = false;
							this.FlowCatalogEditor.FlowCatalogEditButtonStrip.EditState = EditState.View;
                            this.FlowCatalogEditor.addEditFlowCatalogFields.Visibility = Visibility.Collapsed;
							break;
					}
					break;


				case EditState.Edit:
					switch (e.NewEditState)
					{
						case EditState.Save:
							this.FlowController.ProjectController.FlowMasterDataContext.SubmitChanges();

							this.FlowCatalogEditor.EditingControlsEnabled = false;
							this.FlowCatalogEditor.FlowCatalogEditButtonStrip.EditState = EditState.View;
                            this.FlowCatalogEditor.addEditFlowCatalogFields.Visibility = Visibility.Collapsed;
							break;

						case EditState.Cancel:

                            this.FlowController.ProjectController.FlowMasterDataContext.FlowCatalogList.RevertCurrent();
                            this.FlowController.ProjectController.FlowMasterDataContext.Revert();
                            this.FlowController.ProjectController.FlowMasterDataContext.RefreshFlowCatalogList();

							this.FlowCatalogEditor.EditingControlsEnabled = false;
							this.FlowCatalogEditor.FlowCatalogEditButtonStrip.EditState = EditState.View;
                            this.FlowCatalogEditor.addEditFlowCatalogFields.Visibility = Visibility.Collapsed;
							break;
					}

					break;


				case EditState.Delete:
					this.FlowCatalogEditor.DeleteConfirmationPrompt.Visibility = Visibility.Collapsed;
					break;

			}
            
		}

		void ConfirmDeleteButton_Click(object sender, RoutedEventArgs e)
		{
            logger.Info("Clicked Confirm Delete button");
            this.FlowController.ProjectController.FlowMasterDataContext.FlowCatalogList.CurrentItem.FlowMasterDataContext = this.FlowController.ProjectController.FlowMasterDataContext;
            this.FlowController.ProjectController.FlowMasterDataContext.FlowCatalogList.CurrentItem.PrepareDelete();
            this.FlowController.ProjectController.FlowMasterDataContext.FlowCatalogList.DeleteCurrent();
			this.FlowController.ProjectController.FlowMasterDataContext.SubmitChanges();
			this.FlowCatalogEditor.FlowCatalogEditButtonStrip.EditState = EditState.View;
		}

		void CancelDeleteButton_Click(object sender, RoutedEventArgs e)
		{
            logger.Info("Clicked Cancel Delete button");
			this.FlowCatalogEditor.FlowCatalogEditButtonStrip.EditState = EditState.View;
		}

		void CreateProductPackageButton_Click(object sender, RoutedEventArgs e)
		{
            logger.Info("Clicked Create Product Package button");

            if (this.FlowCatalogEditor.FlowCatalogEditButtonStrip.EditState == EditState.Edit ||
                this.FlowCatalogEditor.FlowCatalogEditButtonStrip.EditState == EditState.Add)
            {
                FlowMessageBox msg = new FlowMessageBox("Edit in Progress", "You are currently adding or editing a catalog.\n\nYou must Save or Cancel before you can add/edit packages.");
                msg.CancelButtonVisible = false;
                msg.ShowDialog();
                return;
            }
			this.FlowController.Product();
		}

		void ProductCatalogEditor_EditItemInvoked(object sender, EventArgs<ProductPackage> e)
		{
			this.FlowController.Product();
		}

		void ProductCatalogEditor_DeleteItemInvoked(object sender, EventArgs<ProductPackage> e)
		{
            this.FlowController.ProjectController.FlowMasterDataContext.FlowCatalogList.CurrentItem.DeleteProductPackage(e.Data);
		}


/// 


		private void InitializeImageQuixCatalogEditor()
		{
			// ImageQuix Catalog
			this.ImageQuixCatalogPanel.DataContext = this.FlowController.ProjectController.FlowMasterDataContext;

			this.ImageQuixCatalogPanel.ImageQuixCatalogEditButtonStrip.SetSourceCollection<ImageQuixCatalog>(
				this.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogList
			);

			this.ImageQuixCatalogPanel.ImageQuixCatalogEditButtonStrip.EditStateChanged += new EventHandler<EditStateChangedEventArgs>(ImageQuixCatalogEditButtonStrip_EditStateChanged);

			this.ImageQuixCatalogPanel.RetrieveImageQuixCatalogListButton.Click += new RoutedEventHandler(RetrieveImageQuixCatalogListButton_Click);
			this.ImageQuixCatalogPanel.ImportImageQuixCatalogsButton.Click += new RoutedEventHandler(ImportImageQuixCatalogsButton_Click);

			this.ImageQuixCatalogPanel.ConfirmDeleteButton.Click += new RoutedEventHandler(ImageQuixCatalogConfirmDeleteButton_Click);
			this.ImageQuixCatalogPanel.CancelDeleteButton.Click += new RoutedEventHandler(ImageQuixCatalogCancelDeleteButton_Click);

			this.ImageQuixCatalogPanel.ClearCatalogsButton.Click += new RoutedEventHandler(ClearCatalogsButton_Click);
			this.ImageQuixCatalogPanel.CancelClearCatalogsButton.Click += new RoutedEventHandler(CancelClearCatalogsButton_Click);
			this.ImageQuixCatalogPanel.ConfirmClearCatalogsButton.Click += new RoutedEventHandler(ConfirmClearCatalogsButton_Click);

            this.ImageQuixCatalogPanel.DeleteUnusedProductsButton.Click += new RoutedEventHandler(DeleteUnusedProductsButton_Click);
            this.ImageQuixCatalogPanel.CancelDeleteUnusedProductsButton.Click += new RoutedEventHandler(CancelDeleteUnusedProductsButton_Click);
            this.ImageQuixCatalogPanel.ConfirmDeleteUnusedProductsButton.Click += new RoutedEventHandler(ConfirmDeleteUnusedProductsButton_Click);
            
		}

		void ImageQuixCatalogEditButtonStrip_EditStateChanged(object sender, EditStateChangedEventArgs e)
		{
			switch (e.OldEditState)
			{
				case EditState.View:

					switch (e.NewEditState)
					{
						case EditState.Delete:
							this.ImageQuixCatalogPanel.DeleteConfirmationPrompt.Visibility = Visibility.Visible;
							break;
					}
					break;

				case EditState.Delete:
					this.ImageQuixCatalogPanel.DeleteConfirmationPrompt.Visibility = Visibility.Collapsed;
					break;
			}
		}

		void RetrieveImageQuixCatalogListButton_Click(object sender, RoutedEventArgs e)
		{
            logger.Info("Clicked Retrieve ImageQuix Catalog List button");

            if (this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.CatalogRequest.ImageQuixBaseUrl.Contains(".pud"))
            {
                //this is a pud, treat it as such
                PUDCatalogController controller = new PUDCatalogController(this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.CatalogRequest.ImageQuixBaseUrl, this);
                controller.forceUpdate = true;
                controller.ProcessPUD();

               

                return;
            }
            if (this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.CatalogRequest.ImageQuixBaseUrl.Contains("plic.io"))
            {
                //this is a plic catalog, treat it as such
                PlicCatalogController controller = new PlicCatalogController(this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.CatalogRequest.ImageQuixBaseUrl, this);
                controller.forceUpdate = true;
                controller.ProcessPlicCatalog();



                return;
            }

            try
            {
                if (!this.FlowController.ProjectController.FlowMasterDataContext.RefreshRemoteImageQuixCatalogList())
                {
                    FlowMessageBox msg = new FlowMessageBox("Lab Catalogs", "There are no Lab Catalogs Available");
                    msg.CancelButtonVisible = false;
                    msg.ShowDialog();
                    logger.Warn("There are no Lab Catalogs Available");
                    return;
                }

                if (this.FlowController.ProjectController.FlowMasterDataContext.RemoteImageQuixCatalogListDeletes.Count > 0)
                {

                    foreach(Flow.Lib.Helpers.ImageQuix.ImageQuixCatalogListing listing in this.FlowController.ProjectController.FlowMasterDataContext.RemoteImageQuixCatalogListDeletes)
                    {
                        int badFlowCatalogCount = 0;
                        foreach (FlowCatalog flowCatalog in this.FlowController.ProjectController.FlowMasterDataContext.FlowCatalogList)
                        {
                            if (flowCatalog.ImageQuixCatalogID == listing.PrimaryKey)
                            {
                                badFlowCatalogCount++;
                                //there is a catalog associated with this lab catalog
                                FlowMessageBox msg = new FlowMessageBox("Deleted Lab Products", "You have a catalog that is referencing a deleted lab catalog.\nYou must redo your catalog.\n" + listing.Label);
                                msg.CancelButtonVisible = false;
                                msg.ShowDialog();

                            }
                        }
                        if (badFlowCatalogCount == 0)
                        {
                            ImageQuixCatalog catalog = this.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogList.FirstOrDefault(iqc => iqc.PrimaryKey == listing.PrimaryKey);
                            if(catalog != null)
                            {
                                catalog.PrepareForDeletion(this.FlowController.ProjectController.FlowMasterDataContext);
                                this.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogList.Remove(catalog);
                                this.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogs.DeleteOnSubmit(catalog);
                                this.FlowController.ProjectController.FlowMasterDataContext.SubmitChanges();
                            }

                            
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                FlowMessageBox msg = new FlowMessageBox("Lab Catalogs", "There was an error connecting to the catalog server.\nPlease check your catalog settings.\nPreferences -> Remote Connection -> Lab Catalog");
                msg.CancelButtonVisible = false;
                msg.ShowDialog();
                logger.ErrorException("There was an error connecting to the catalog server", ex);
                return;
                
            }
            if (this.FlowController.ProjectController.FlowMasterDataContext.RemoteImageQuixCatalogList.Count() > 0)
                this.ImageQuixCatalogPanel.ImportImageQuixCatalogsButton.IsEnabled = true;
		}

		void ImportImageQuixCatalogsButton_Click(object sender, RoutedEventArgs e)
		{
            logger.Info("Clicked Import ImageQuix Catalogs button");

            IShowsNotificationProgress progressNotification = ObjectFactory.GetInstance<IShowsNotificationProgress>();
             _notificationInfo = new NotificationProgressInfo("Catalog Download", "Downloading Catalog...");
            _notificationInfo.NotificationProgress = progressNotification;

            this.FlowController.ApplicationPanel.IsEnabled = false;

           


            //need to call this to initialize the ImageQuixCatalog list
            FlowObservableCollection<ImageQuixCatalog> iqc = this.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogList;
            FlowBackgroundWorker worker = FlowBackgroundWorkerManager.RunWorker(CatalogSync, CatalogSync_Completed);   
		}
        void CatalogSync(object sender, DoWorkEventArgs e)
        {
            this.FlowController.ProjectController.FlowMasterDataContext.ImportMarkedRemoteImageQuixCatalogs(_notificationInfo, this.CatalogDockPanel.Dispatcher);
            this.FlowController.ProjectController.FlowMasterDataContext.SubmitChanges();
            this.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogList.CurrentItem.RefreshProductList();
            this.CatalogDockPanel.Dispatcher.Invoke(DispatcherPriority.Background, (Action)(() =>
                {
                    this.FlowController.ProjectController.FlowMasterDataContext.UpdateImageQuixCatalogList();
                    this.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogList.CurrentItem.RefreshProductList();
                    this.ImageQuixCatalogPanel.CatalogProductGroupList.ItemsSource = null;
                    this.ImageQuixCatalogPanel.CatalogProductGroupList.UpdateLayout();
                    this.ImageQuixCatalogPanel.CatalogProductGroupList.ItemsSource = this.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogList.CurrentItem.ImageQuixProductGroups;
                    this.ImageQuixCatalogPanel.CatalogProductGroupList.UpdateLayout();
                    this.FlowController.ProjectController.FlowMasterDataContext.UpdateImageQuixCatalogList();
                    this.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogList.CurrentItem.RefreshProductList();
                    this.ImageQuixCatalogPanel.CatalogProductGroupList.UpdateLayout();
                }));
            _notificationInfo.Complete("Finished downloading catalog");
        }
        void CatalogSync_Completed(object sender, RunWorkerCompletedEventArgs e)
        {
            Console.Write("-----------------\n-----------------\nDONE WITH CATALOG SYNC\n-----------------\n-----------------\n");
            if(this.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogList.CurrentItem != null)
                this.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogList.CurrentItem.RefreshProductList();
            this.FlowController.ApplicationPanel.IsEnabled = true;

        }

        void CatalogUpdate_Worker(object sender, DoWorkEventArgs e)
        {
           
        }

		void ImageQuixCatalogConfirmDeleteButton_Click(object sender, RoutedEventArgs e)
		{
            logger.Info("Clicked ImageQuix Catalog Confirm Delete button");
			this.FlowController.ProjectController.FlowMasterDataContext.DeleteCurrentImageQuixCatalog();
			this.ImageQuixCatalogPanel.ImageQuixCatalogEditButtonStrip.EditState = EditState.View;
		}

		void ImageQuixCatalogCancelDeleteButton_Click(object sender, RoutedEventArgs e)
		{
            logger.Info("Clicked ImageQuix Catalog Cancel Delete button");
			this.ImageQuixCatalogPanel.ImageQuixCatalogEditButtonStrip.EditState = EditState.View;
		}

		void ClearCatalogsButton_Click(object sender, RoutedEventArgs e)
		{
            logger.Info("Clicked Clear Catalogs button");
			this.ImageQuixCatalogPanel.ClearCatalogsConfirmPanelVisibility = Visibility.Visible;
		}

		void CancelClearCatalogsButton_Click(object sender, RoutedEventArgs e)
		{
            logger.Info("Clicked Cancel Clear Catalogs button");
			this.ImageQuixCatalogPanel.ClearCatalogsConfirmPanelVisibility = Visibility.Collapsed;
		}

		void ConfirmClearCatalogsButton_Click(object sender, RoutedEventArgs e)
		{
            logger.Info("Clicked Confirm Clear Catalogs button");
			this.FlowController.ProjectController.FlowMasterDataContext.ClearCatalogs();
			this.ImageQuixCatalogPanel.ClearCatalogsConfirmPanelVisibility = Visibility.Collapsed;
		}

        void DeleteUnusedProductsButton_Click(object sender, RoutedEventArgs e)
		{
            logger.Info("Clicked Delete Unused Products button");
            this.ImageQuixCatalogPanel.DeleteUnusedProductsConfirmPanelVisibility = Visibility.Visible;
		}

        void CancelDeleteUnusedProductsButton_Click(object sender, RoutedEventArgs e)
		{
            logger.Info("Clicked Cancel Delete Unused Products button");
            this.ImageQuixCatalogPanel.DeleteUnusedProductsConfirmPanelVisibility = Visibility.Collapsed;
		}

        void ConfirmDeleteUnusedProductsButton_Click(object sender, RoutedEventArgs e)
		{
            logger.Info("Clicked Confirm Delete Unused Products button");
            this.FlowController.ProjectController.FlowMasterDataContext.DeleteUnusedProducts();
            this.ImageQuixCatalogPanel.DeleteUnusedProductsConfirmPanelVisibility = Visibility.Collapsed;
            this.FlowController.Catalog();
		}

	}	// END class

}	// END namespace
