﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using Flow.Lib;
using System.Windows.Threading;
using System.Web.Script.Serialization;
using Flow.Schema.LinqModel.DataContext;
using System.Web;
using StructureMap;
using Flow.Lib.AsyncWorker;
using System.ComponentModel;
using Flow.View.Dialogs;
using System.Threading;


namespace Flow.Controller.Catalog
{
    class PlicCatalogController
    {

        private string _catURL;

        private NotificationProgressInfo _notificationInfo;

        private Dispatcher dispatcher { get; set; }

        public bool forceUpdate = false;

        public string AccessToken { get; set; }

        public bool ShowNotification = true;

        private FlowCatalogController _flowCatalogController {get;set;}
        public PlicCatalogController(string catURL, FlowCatalogController fcc)
        {
            this._catURL = catURL;
            this._flowCatalogController = fcc;

        }

        internal void ProcessPlicCatalog()
        {

            if (_flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogs == null ||
                _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogs.Count() == 0)
                _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ExecuteCommand("ALTER TABLE ImageQuixCatalog ALTER COLUMN ImageQuixCatalogID IDENTITY (1,1);");
            if (_flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogOptions == null ||
                _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogOptions.Count() == 0)
                _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ExecuteCommand("ALTER TABLE ImageQuixCatalogOption ALTER COLUMN ImageQuixCatalogOptionID IDENTITY (1,1);");
            if (_flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogOptionGroups == null ||
                _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogOptionGroups.Count() == 0)
                _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ExecuteCommand("ALTER TABLE ImageQuixCatalogOptionGroup ALTER COLUMN ImageQuixCatalogOptionGroupID IDENTITY (1,1);");

            if (_flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixOptions == null ||
                _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixOptions.Count() == 0)
                _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ExecuteCommand("ALTER TABLE ImageQuixOption ALTER COLUMN ImageQuixOptionID IDENTITY (1,1);");
            if (_flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixOptionCommands == null ||
                _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixOptionCommands.Count() == 0)
                _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ExecuteCommand("ALTER TABLE ImageQuixOptionCommand ALTER COLUMN ImageQuixOptionCommandID IDENTITY (1,1);");
            if (_flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixOptionGroups == null ||
                _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixOptionGroups.Count() == 0)
                _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ExecuteCommand("ALTER TABLE ImageQuixOptionGroup ALTER COLUMN ImageQuixOptionGroupID IDENTITY (1,1);");

            if (_flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixProducts == null ||
                _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixProducts.Count() == 0)
                _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ExecuteCommand("ALTER TABLE ImageQuixProduct ALTER COLUMN ImageQuixProductID IDENTITY (1,1);");
            if (_flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixProductGroups == null ||
                _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixProductGroups.Count() == 0)
                _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ExecuteCommand("ALTER TABLE ImageQuixProductGroup ALTER COLUMN ImageQuixProductGroupID IDENTITY (1,1);");
            if (_flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixProductNodes == null ||
                _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixProductNodes.Count() == 0)
                _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ExecuteCommand("ALTER TABLE ImageQuixProductNode ALTER COLUMN ImageQuixProductNodeID IDENTITY (1,1);");


            dispatcher = Dispatcher.CurrentDispatcher;
           

            _flowCatalogController.FlowController.ApplicationPanel.IsEnabled = false;
            //need to call this to initialize the ImageQuixCatalog list
            FlowObservableCollection<ImageQuixCatalog> iqc = _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogList;
            FlowBackgroundWorker worker = FlowBackgroundWorkerManager.RunWorker(CatalogSync, CatalogSync_Completed);
        }

        void CatalogSync_Completed(object sender, RunWorkerCompletedEventArgs e)
        {
            ImageQuixCatalog newItem = this._flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogs.FirstOrDefault();
            if (newItem != null)
            {
                newItem.RefreshProductList();
                newItem.RefreshCatalogOptionList();
            }

            if (ShowNotification && _notificationInfo != null)
                _notificationInfo.Complete("Finished processing catalog");

            
            Console.Write("-----------------\n-----------------\nDONE WITH PLIC CATALOG SYNC\n-----------------\n-----------------\n");
            _flowCatalogController.FlowController.ApplicationPanel.IsEnabled = true;
            if (newItem == null) return;

            if (this._flowCatalogController.ImageQuixCatalogPanel != null)
            {
                this._flowCatalogController.ImageQuixCatalogPanel.CatalogProductGroupList.ItemsSource = null;
                this._flowCatalogController.ImageQuixCatalogPanel.CatalogProductGroupList.ItemsSource = this._flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogList.CurrentItem.ProductGroupList;
                this._flowCatalogController.ImageQuixCatalogPanel.CatalogProductGroupList.UpdateLayout();
            }
            this._flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.UpdateImageQuixCatalogList();
            if( this._flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogList.Count > 0)
                this._flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogList.CurrentItem.RefreshProductList();



        }

        void CatalogSync(object sender, DoWorkEventArgs e)
        {
           //No longer need to Authenticate to get a catalog
           //string rA = Authenticate(_flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.CatalogRequest.GetCatalogUsername,
           //     _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.CatalogRequest.GetCatalogPassword, this._catURL);
           //JavaScriptSerializer json_serializer = new JavaScriptSerializer();
           //AuthenticateResults authenticateResults = json_serializer.Deserialize<AuthenticateResults>(rA);
           //AccessToken = authenticateResults.access_token;


           string CatalogJson = GetCatalog(_catURL, AccessToken);

           //if cat json is null, then the catalog most likely was removed from PLIC, so lets not worry about looking for the update.
            if (CatalogJson == null) return;

           File.WriteAllText(Path.Combine(FlowContext.FlowConfigDirPath, "PlicCat.txt"), CatalogJson);

           JavaScriptSerializer json_serializer2 = new JavaScriptSerializer();
           CatalogContent PlicCatalog = json_serializer2.Deserialize<CatalogContent>(CatalogJson);

            _catURL = _catURL.TrimEnd('/');
            
            string sGuid = _catURL.Split('/').Last();
            Guid thisCatalogGuid = new Guid(sGuid);
            //Save the PlicCatalog Source
           PLICCatalog pcat = _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.PLICCatalogs.FirstOrDefault(pc => pc.Guid == thisCatalogGuid);

            DateTime rightNow = DateTime.Now;
           if (pcat == null)
           {
               
               pcat = new PLICCatalog();
               pcat.CreateDate = rightNow;
               pcat.Guid = thisCatalogGuid;
               pcat.JSON = CatalogJson;
               pcat.ModifiedDate = rightNow;
               pcat.Name = PlicCatalog.content.Name;
               _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.PLICCatalogs.InsertOnSubmit(pcat);
           }
           else
           {
               pcat.JSON = CatalogJson;
               pcat.ModifiedDate = rightNow;
           }
           _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.SubmitChanges();
            

            string dtstring = PlicCatalog.content.LastModifiedDate;
            DateTime lastModified = Convert.ToDateTime(dtstring);
            ImageQuixCatalog flowLabCatalog = null;
            //does this catalog exist?
            if (_flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogs != null &&
                _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogs.Count() > 0 &&
                _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogs.Any(iqc => iqc.Label == PlicCatalog.content.Name))
            {
                flowLabCatalog = _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogs.First(iqc => iqc.Label == PlicCatalog.content.Name);
                if (!forceUpdate && (flowLabCatalog.ModifyDate == lastModified))
                {
                    //thist catalog has not been changed and does not need to be updated
                    //TODO: disbatch a message and then return
                    ShowNotification = false;
                    return;
                }
            }
            else
            {
               //do nothing yet
                
            }
            bool dlgresult = false;
             this.dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                    {
                        FlowMessageBox msg = new FlowMessageBox("Catalog Update", "A new lab catalog is available, click okay to update now");
                        dlgresult = (bool)msg.ShowDialog();
                    }));
             if (dlgresult)
            {

                  this.dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                    {
                        IShowsNotificationProgress progressNotification = ObjectFactory.GetInstance<IShowsNotificationProgress>();
                        _notificationInfo = new NotificationProgressInfo("Catalog Download", "Checking for new catalog...");
                        _notificationInfo.NotificationProgress = progressNotification;
                        if (ShowNotification)
                            progressNotification.ShowNotification(_notificationInfo);
                    }));
                if (flowLabCatalog == null)
                {
                    flowLabCatalog = new ImageQuixCatalog();
                    this.dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                    {
                        this._flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogList.Add(flowLabCatalog);
                    }));
                }

                GenerateFlowLabCatalog(PlicCatalog.content, flowLabCatalog);
                this._flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.SubmitChanges();
            }


        }

        private void GenerateFlowLabCatalog(Model.Plic.Catalog.Catalog catalog, ImageQuixCatalog flowLabCatalog)
        {

            List<string> alltempProdIDs = new List<string>();
            //flowLabCatalog.ModifyDate = new DateTime(catalog.LastModifiedDate);
            flowLabCatalog.ModifyDate = Convert.ToDateTime(catalog.LastModifiedDate);
            flowLabCatalog.Label = catalog.Name;
            flowLabCatalog.ResourceURL = _catURL;
            flowLabCatalog.PrimaryKey = (this._flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogs.Count() + 1) + 500;
            this._flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.SubmitChanges();

            //flowLabCatalog.ImageQuixProductGroups = new System.Data.Linq.EntitySet<ImageQuixProductGroup>();
            //_flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.SubmitChanges();

            //CATALOG OPTIONS
            foreach (Flow.Model.Plic.Catalog.OptionGroup oog in catalog.OrderOptions)
            {
                //ImageQuixCatalogOptionGroup iqcog = this._flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogOptionGroups.FirstOrDefault(a => a.Label == oog.Name);
                ImageQuixCatalogOptionGroup iqcog = this._flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogOptionGroups.FirstOrDefault(a => a.ResourceURL == oog.LabID);
                if (iqcog == null)
                {
                    iqcog = new ImageQuixCatalogOptionGroup();

                    iqcog.IsMandatory = oog.Required;
                    iqcog.IsMultiSelect = oog.MulitSelect;
                    iqcog.Label = oog.Name;
                    iqcog.ResourceURL = oog.LabID;
                    iqcog.PrimaryKey = (this._flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogOptionGroups.Count() + 1) + 7000;
                    iqcog.ImageQuixCatalog = flowLabCatalog;
                    iqcog.ImageQuixCatalogID = flowLabCatalog.ImageQuixCatalogID;
                    this.dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                    {
                        this._flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogOptionGroups.InsertOnSubmit(iqcog);
                        //flowLabCatalog.CatalogOptionGroupList.Add(iqcog);
                    }));
                    _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.SubmitChanges();
                }
                else
                {
                    iqcog.IsMandatory = oog.Required;
                    iqcog.IsMultiSelect = oog.MulitSelect;
                    iqcog.Label = oog.Name;
                }

                foreach (Flow.Model.Plic.Catalog.Option oo in oog.Options)
                {
                    //ImageQuixCatalogOption opt = this._flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogOptions.FirstOrDefault(a => a.Label == oo.Name && a.ImageQuixCatalogOptionGroupID == iqcog.ImageQuixCatalogOptionGroupID);
                    ImageQuixCatalogOption opt = this._flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogOptions.FirstOrDefault(a => a.ResourceURL == oo.LabID && a.ImageQuixCatalogOptionGroupID == iqcog.ImageQuixCatalogOptionGroupID);
                    if (opt == null)
                    {
                        opt = new ImageQuixCatalogOption();
                        opt.Label = oo.Name;
                        opt.Price = getPrice(oo.Price);
                        opt.SKU = oo.SKU;
                        opt.ResourceURL = oo.LabID;
                        opt.PrimaryKey = (this._flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogOptions.Count() + 1) + 3000;
                        opt.ImageQuixCatalogOptionGroup = iqcog;
                        opt.ImageQuixCatalogOptionGroupID = iqcog.ImageQuixCatalogOptionGroupID;
                        opt.ImageQuixCatalogOptionGroupPk = iqcog.PrimaryKey;
                        this.dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                       {
                           //iqcog.OptionList.Add(opt);
                           this._flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogOptions.InsertOnSubmit(opt);
                       }));
                        _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.SubmitChanges();
                    }
                    else
                    {
                        opt.Label = oo.Name;
                        opt.Price = getPrice(oo.Price);
                        opt.SKU = oo.SKU;
                    }
                }

                
            }

            //IMAGE OPTIONS
            foreach (Flow.Model.Plic.Catalog.OptionGroup oog in catalog.ImageOptions)
            {
                //ImageQuixCatalogOptionGroup iqcog = this._flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogOptionGroups.FirstOrDefault(a => a.Label == oog.Name);
                ImageQuixCatalogOptionGroup iqcog = this._flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogOptionGroups.FirstOrDefault(a =>  a.ResourceURL == oog.LabID);
                if (iqcog == null)
                {
                    iqcog = new ImageQuixCatalogOptionGroup();
                    iqcog.IsMandatory = oog.Required;
                    iqcog.IsMultiSelect = oog.MulitSelect;
                    iqcog.IsImageOption = true;
                    iqcog.Label = oog.Name;
                    iqcog.ResourceURL = oog.LabID;
                    iqcog.PrimaryKey = (this._flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogOptionGroups.Count() + 1) + 7000;
                    iqcog.ImageQuixCatalog = flowLabCatalog;
                    iqcog.ImageQuixCatalogID = flowLabCatalog.ImageQuixCatalogID;
                    this.dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                    {
                        this._flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogOptionGroups.InsertOnSubmit(iqcog);
                    }));
                    _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.SubmitChanges();
                }
                else
                {
                    iqcog.IsMandatory = oog.Required;
                    iqcog.IsMultiSelect = oog.MulitSelect;
                    iqcog.IsImageOption = true;
                    iqcog.Label = oog.Name;
                }

                foreach (Flow.Model.Plic.Catalog.Option oo in oog.Options)
                {
                    //ImageQuixCatalogOption opt = this._flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogOptions.FirstOrDefault(a => a.Label == oo.Name && a.ImageQuixCatalogOptionGroupID == iqcog.ImageQuixCatalogOptionGroupID);
                    ImageQuixCatalogOption opt = this._flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogOptions.FirstOrDefault(a => a.ResourceURL == oo.LabID && a.ImageQuixCatalogOptionGroupID == iqcog.ImageQuixCatalogOptionGroupID);
                    if (opt == null)
                    {
                        opt = new ImageQuixCatalogOption();
                        opt.Label = oo.Name;
                        opt.Price = getPrice(oo.Price);
                        opt.SKU = oo.SKU;
                        opt.ResourceURL = oo.LabID;
                        opt.PrimaryKey = (this._flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogOptions.Count() + 1) + 3000;
                        opt.ImageQuixCatalogOptionGroup = iqcog;
                        opt.ImageQuixCatalogOptionGroupID = iqcog.ImageQuixCatalogOptionGroupID;
                        opt.ImageQuixCatalogOptionGroupPk = iqcog.PrimaryKey;
                        this.dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                        {
                            //iqcog.OptionList.Add(opt);
                            this._flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogOptions.InsertOnSubmit(opt);
                        }));
                        _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.SubmitChanges();
                    }
                    else
                    {
                        opt.Label = oo.Name;
                        opt.Price = getPrice(oo.Price);
                        opt.SKU = oo.SKU;
                    }
                }
            }



            foreach (Flow.Model.Plic.Catalog.Filter f in catalog.Filters)
            {
                
                
                if(f.Filters == null || f.Filters.Count == 0){
                    ImageQuixProductGroup thispg = this._flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixProductGroups.FirstOrDefault(a => a.Label == f.Name);
                    if (thispg == null)
                    {
                        thispg = new ImageQuixProductGroup();
                        thispg.Label = f.Name;
                        thispg.PrimaryKey = Int32.Parse(f.ID);
                        thispg.ResourceURL = f.ID;
                        thispg.ImageQuixCatalog = flowLabCatalog;
                        thispg.ImageQuixCatalogID = flowLabCatalog.ImageQuixCatalogID;
                        this.dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                       {
                           this._flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixProductGroups.InsertOnSubmit(thispg);
                       }));
                        _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.SubmitChanges();
                    }
                }
                else{

                    foreach (Flow.Model.Plic.Catalog.Filter subFilter in f.Filters)
                    {
                        ImageQuixProductGroup thispg = this._flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixProductGroups.FirstOrDefault(a => a.Label == f.Name + " - " + subFilter.Name);
                        if (thispg == null)
                        {
                            thispg = new ImageQuixProductGroup();
                            thispg.Label = f.Name + " - " + subFilter.Name;
                            int result = 0;
                            if (Int32.TryParse(subFilter.ID, out result))
                                thispg.PrimaryKey = result;
                            thispg.ResourceURL = subFilter.ID;
                            thispg.ImageQuixCatalog = flowLabCatalog;
                            thispg.ImageQuixCatalogID = flowLabCatalog.ImageQuixCatalogID;
                            this.dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                               {
                                   this._flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixProductGroups.InsertOnSubmit(thispg);
                               }));
                            _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.SubmitChanges();
                        }
                    }
                }
            }

            
            foreach (Flow.Model.Plic.Catalog.Product p in catalog.Products)
            {
                    ImageQuixProductGroup thispg = this._flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixProductGroups.FirstOrDefault(ipg => p.Filters.Contains(ipg.ResourceURL));
                    if (thispg == null)
                    {
                        thispg = flowLabCatalog.ImageQuixProductGroups.FirstOrDefault(ipg => ipg.ResourceURL == "22022");
                    }
                    if (thispg == null)
                    {
                        thispg = new ImageQuixProductGroup();
                        thispg.Label = "Other Products";
                        thispg.PrimaryKey = 22022;
                        thispg.ResourceURL = "22022";
                        thispg.ImageQuixCatalog = flowLabCatalog;
                        thispg.ImageQuixCatalogID = flowLabCatalog.ImageQuixCatalogID;
                        this.dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                           {
                               this._flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixProductGroups.InsertOnSubmit(thispg);
                           }));
                        _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.SubmitChanges();
                    }


                        ImageQuixProduct thisp = this._flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixProducts.FirstOrDefault(ipg => ipg.ResourceURL == p.ID && ipg.ImageQuixProductGroupID == thispg.ImageQuixProductGroupID);
                        if (thisp == null)
                        {
                            thisp = new ImageQuixProduct();
                            thisp.Label = p.Name;
                            thisp.PrimaryKey = (this._flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixProducts.Count() + 1) + 90000;
                            thisp.ResourceURL = p.ID;
                            thisp.Price = getPrice(p.Pricing.FirstOrDefault().Amount);
                            thisp.SKU = p.SKU;
                            thisp.Map = p.Map;
                            thisp.ImageQuixCatalog = flowLabCatalog;
                            thisp.ImageQuixCatalogID = flowLabCatalog.ImageQuixCatalogID;
                            thisp.ImageQuixProductGroupID = thispg.ImageQuixProductGroupID;
                            thisp.ImageQuixProductGroup = thispg;
                            this.dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                               {
                                   this._flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixProducts.InsertOnSubmit(thisp);
                               }));
                            _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.SubmitChanges();
                        }
                        else
                        {
                            thisp.Label = p.Name;
                            thisp.Price = getPrice(p.Pricing.FirstOrDefault().Amount);
                            thisp.SKU = p.SKU;
                            thisp.Map = p.Map;
                        }

                        alltempProdIDs.Add(thispg.ImageQuixProductGroupID + "-,-" + thisp.ResourceURL);

                        foreach (Flow.Model.Plic.Catalog.ImageNode n in p.ImageNodes)
                        {
                            ImageQuixProductNode thispn = this._flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixProductNodes.FirstOrDefault(ipg => ipg.PrimaryKey == n.ID && ipg.ImageQuixProductID == thisp.ImageQuixProductID);
                            if (thispn == null)
                            {
                                thispn = new ImageQuixProductNode();
                                thispn.Height = n.Height;
                                thispn.Width = n.Width;
                                thispn.X = n.X;
                                thispn.Y = n.Y;
                                thispn.PrimaryKey = n.ID;
                                thispn.ResourceURL = n.ID.ToString();
                                thispn.Type = 1; // hardcode type 1 is image
                                thispn.ImageQuixProduct = thisp;
                                thispn.ImageQuixProductID = thisp.ImageQuixProductID;
                                this.dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                                   {
                                       this._flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixProductNodes.InsertOnSubmit(thispn);
                                   }));
                                _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.SubmitChanges();
                            }
                            
                        }

                        //PRODUCT OPTIONS
                        foreach (Flow.Model.Plic.Catalog.OptionGroup oog in p.ProductOptions)
                        {
                            //ImageQuixOptionGroup iqcog = this._flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixOptionGroups.FirstOrDefault(a => a.Label == oog.Name && a.ImageQuixProductID == thisp.ImageQuixProductID);
                            ImageQuixOptionGroup iqcog = this._flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixOptionGroups.FirstOrDefault(a => a.ResourceURL == oog.LabID && a.ImageQuixProductID == thisp.ImageQuixProductID);
                            if (iqcog == null)
                            {
                                iqcog = new ImageQuixOptionGroup();
                                iqcog.IsMandatory = oog.Required;
                                iqcog.Label = oog.Name;
                                iqcog.PrimaryKey = (this._flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixOptionGroups.Count() + 1) + 1000;
                                //iqcog.ResourceURL = oog.Name;
                                iqcog.ResourceURL = oog.LabID;
                                iqcog.ImageQuixProduct = thisp;
                                iqcog.ImageQuixProductID = thisp.ImageQuixProductID;
                                this.dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                               {
                                   this._flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixOptionGroups.InsertOnSubmit(iqcog);
                               }));
                                _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.SubmitChanges();
                            }
                            else
                            {
                                iqcog.IsMandatory = oog.Required;
                                iqcog.Label = oog.Name;
                            }
                            foreach (Flow.Model.Plic.Catalog.Option oo in oog.Options)
                            {
                                //ImageQuixOption opt = this._flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixOptions.FirstOrDefault(a => a.ImageQuixOptionGroupID == iqcog.ImageQuixOptionGroupID && a.Label == oo.Name);
                                ImageQuixOption opt = this._flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixOptions.FirstOrDefault(a => a.ImageQuixOptionGroupID == iqcog.ImageQuixOptionGroupID && a.ResourceURL == oo.LabID);
                                if (opt == null)
                                {
                                    opt = new ImageQuixOption();

                                    this.dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                                   {
                                       opt.Label = oo.Name;
                                       opt.ResourceURL = oo.LabID;
                                       opt.ImageQuixOptionGroupID = iqcog.ImageQuixOptionGroupID;
                                       opt.ImageQuixOptionGroup = iqcog;
                                       opt.ImageQuixOptionGroupPk = iqcog.PrimaryKey;
                                       opt.PrimaryKey = (this._flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixOptions.Count() + 1) + 3000;
                                       this._flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixOptions.InsertOnSubmit(opt);
                                       _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.SubmitChanges();
                                   }));
                                    _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.SubmitChanges();
                                }
                                else
                                {
                                    opt.Label = oo.Name;
                                }
                                
                            }
                            
                        }
                    
            }

            //Any products get removed?
            foreach (ImageQuixProduct p in flowLabCatalog.ImageQuixProducts)
            {
                string id = p.ImageQuixProductGroupID + "-,-" + p.ResourceURL;
                if (!alltempProdIDs.Contains(id))
                {
                    //this item is missing from the catalog
                    if (p.RetiredOn == null)
                        p.RetiredOn = DateTime.Now.AddDays(-1);
                }
            }
        }

        private decimal getPrice(int p)
        {
            return (decimal)p / (decimal)(100);
        }

        public string Authenticate(string username, string password, string catUrl)
        {

            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls;

            string pubURL = "https://plic.io/oauth/token?grant_type=password&username=" + HttpUtility.UrlEncode(username) + "&password=" + HttpUtility.UrlEncode(password) + "&client_id=" + "d6cce43ec29774e594d8fe7f2299d55de3a13bd4f1cbc77718f9d8b2d2fd04aa";
            if(catUrl.Contains("test.plic.io"))
                pubURL = "https://test.plic.io/oauth/token?grant_type=password&username=" + HttpUtility.UrlEncode(username) + "&password=" + HttpUtility.UrlEncode(password) + "&client_id=" + "d6cce43ec29774e594d8fe7f2299d55de3a13bd4f1cbc77718f9d8b2d2fd04aa";
            
            int trycount = 0;

            var result = "";

            while (trycount < 20)
            {
                try
                {
                    var httpWebRequest = (HttpWebRequest)WebRequest.Create(pubURL);
                    //httpWebRequest.ContentType = "text/json";
                    httpWebRequest.Method = "POST";

                    
                    using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                    {
                        streamWriter.Write("");
                        streamWriter.Flush();
                        streamWriter.Close();

                        var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                        using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                        {
                            result = streamReader.ReadToEnd();
                        }
                    }
                    trycount = 21;
                }
                catch (Exception ex)
                {
                    Thread.Sleep(500);
                    trycount++;
                }
            }
            return result;
        }



        public string GetCatalog(string catURL, string currentToken)
        {
            //string currentToken = "a9fb81bbf0a0d006e94b8e78850cc778df3877c3f4403644343613f6cff37de5";

            string pubURL = catURL;

            var httpWebRequest = (HttpWebRequest)WebRequest.Create(pubURL);
            httpWebRequest.ContentType = "text/json";
            httpWebRequest.Method = "GET";
            httpWebRequest.Headers.Add("Authorization: Bearer " + currentToken);
            //httpWebRequest.Headers.Add("Accept: ");
            httpWebRequest.Accept = "application/vnd.plic.io.v1+json";



            var result = "";

            try
            {
                using (WebResponse response = httpWebRequest.GetResponse())
                {
                    using (Stream stream = response.GetResponseStream())
                    {
                        using (var streamReader = new StreamReader(stream))
                        {
                            result = streamReader.ReadToEnd();
                        }
                    }
                }
            }
            catch (WebException wex)
            {
                return null;
            }
            return result;
        }

    }


    public class AuthenticateResults
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
    }

    public class CatalogPublishResutls
    {

        public String ID { get; set; }

    }

    public class CatalogSummary
    {
        public Content content { get; set; }
    }
    public class Content
    {
        public string id { get; set; }
        public string LastModifiedDate { get; set; }
    }

    public class CatalogContent
    {
        public Flow.Model.Plic.Catalog.Catalog content { get; set; }
    }
}
