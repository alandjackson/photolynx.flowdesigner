﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.IO;
using System.Text;
using System.Windows;

using Flow.Controls.View.RecordDisplay;
using Flow.Lib;
using Flow.Lib.Helpers;
using Flow.Schema.LinqModel.DataContext;
using Flow.View.Catalog;
using Flow.View.Dialogs;

using NLog;

namespace Flow.Controller.Catalog
{
	public class ProductPackageController
	{
		internal FlowController FlowController { get; set; }
		internal ProductPackageEditor ProductPackageEditor { get; set; }

        internal string[] _backgroundPaths { get; set; }

        private static Logger logger = LogManager.GetCurrentClassLogger();

		private FlowObservableCollection<FlowCatalog> FlowCatalogList { get; set; }
		private FlowObservableCollection<ProductPackage> ProductPackageList
		{
			get { return (this.FlowCatalogList.CurrentItem == null) ? null : this.FlowCatalogList.CurrentItem.ProductPackageList; }
		}

		public ProductPackageController()
		{
		}

		public void Main(FlowController fc, ProductPackageEditor ppe)
		{
			this.FlowController = fc;
			this.ProductPackageEditor = ppe;

			this.ProductPackageEditor.DataContext = this.FlowController.ProjectController.FlowMasterDataContext;

			this.FlowCatalogList =
				this.FlowController.ProjectController.FlowMasterDataContext.FlowCatalogList;
			

			this.ProductPackageEditor.ProductPackageEditButtonStrip.SetSourceCollection<ProductPackage>(
				this.FlowController.ProjectController.FlowMasterDataContext.ProductPackageList
			);

			this.ProductPackageEditor.ProductPackageEditButtonStrip.EditStateChanged +=
				new EventHandler<EditStateChangedEventArgs>(CollectionEditButtonStrip_EditStateChanged);

			this.ProductPackageEditor.ConfirmDeleteButton.Click += new RoutedEventHandler(ConfirmDeleteButton_Click);
			this.ProductPackageEditor.CancelDeleteButton.Click += new RoutedEventHandler(CancelDeleteButton_Click);

			this.ProductPackageEditor.ViewFlowCatalogsButton.Click += new RoutedEventHandler(ViewCatalogsButton_Click);

			this.ProductPackageEditor.AddItemInvoked += new EventHandler<EventArgs<ImageQuixProduct>>(ProductPackageEditor_AddItemInvoked);
			this.ProductPackageEditor.DeleteItemInvoked += new EventHandler<EventArgs<ImageQuixProduct>>(ProductPackageEditor_DeleteItemInvoked);
            this.ProductPackageEditor.AddALaCarteInvoked += new EventHandler<EventArgs<ImageQuixProduct>>(ProductPackageEditor_AddALaCarteInvoked);

			this.ProductPackageEditor.EditProductPackageInvoked += new EventHandler<EventArgs<ProductPackage>>(ProductPackageEditor_EditProductPackageInvoked);

			this.ProductPackageEditor.EditingControlsEnabled = false;
            this.ProductPackageEditor.EditingControlsDisabled = true;

            
		}

		void CollectionEditButtonStrip_EditStateChanged(object sender, Flow.Controls.View.RecordDisplay.EditStateChangedEventArgs e)
		{
			switch (e.OldEditState)
			{
				case EditState.View:

					switch (e.NewEditState)
					{
						case EditState.Add:
							this.ProductPackageList.CreateNew();
							this.ProductPackageEditor.EditingControlsEnabled = true;
                            this.ProductPackageEditor.EditingControlsDisabled = false;
                            this.ProductPackageEditor.PackageInfoFields.Visibility = Visibility.Visible;
                            this.ProductPackageEditor.ProductPackageDescTextBox.Focus();
                            if(this.ProductPackageEditor.ProductPackageDescTextBox.Text.Length > 0)
                                this.ProductPackageEditor.ProductPackageEditButtonStrip.IsSaveEnabled = true;
                            else
                                this.ProductPackageEditor.ProductPackageEditButtonStrip.IsSaveEnabled = false;
							
							break;

						case EditState.Edit:
							this.ProductPackageEditor.EditingControlsEnabled = true;
                            this.ProductPackageEditor.EditingControlsDisabled = false;
                            this.ProductPackageEditor.PackageInfoFields.Visibility = Visibility.Visible;
                            this.ProductPackageEditor.ProductPackageDescTextBox.Focus();
                            if(this.ProductPackageEditor.ProductPackageDescTextBox.Text.Length > 0)
                                this.ProductPackageEditor.ProductPackageEditButtonStrip.IsSaveEnabled = true;
                            else
                                this.ProductPackageEditor.ProductPackageEditButtonStrip.IsSaveEnabled = false;
							break;

						case EditState.Delete:
                            if(this.ProductPackageList.CurrentItem != null)
							    this.ProductPackageEditor.DeleteConfirmationPrompt.Visibility = Visibility.Visible;
                            else
                                this.ProductPackageEditor.ProductPackageEditButtonStrip.EditState = EditState.View;
							break;
					}
					break;


				case EditState.Add:
					switch (e.NewEditState)
					{
						case EditState.Save:
                            if (this.ProductPackageList.NewItem.ProductPackageDesc == null || this.ProductPackageList.NewItem.ProductPackageDesc.Length < 1)
                            {
                                FlowMessageBox msg = new FlowMessageBox("Product Packages", "You Must fill in the Description field");
                                msg.CancelButtonVisible = false;
                                msg.ShowDialog();
                                this.ProductPackageList.CancelNew();
                                this.ProductPackageEditor.EditingControlsEnabled = false;
                                this.ProductPackageEditor.EditingControlsDisabled = true;
                                this.ProductPackageEditor.ProductPackageEditButtonStrip.EditState = EditState.View;
                                return;
                            }
                            if (this.ProductPackageList.NewItem.Map == null || this.ProductPackageList.NewItem.Map.Length < 1)
                            {
                                FlowMessageBox msg = new FlowMessageBox("Product Packages", "You Must fill in the Map field");
                                msg.CancelButtonVisible = false;
                                msg.ShowDialog();
                                this.ProductPackageList.CancelNew();
                                this.ProductPackageEditor.EditingControlsEnabled = false;
                                this.ProductPackageEditor.EditingControlsDisabled = true;
                                this.ProductPackageEditor.ProductPackageEditButtonStrip.EditState = EditState.View;
                                return;
                            }
							this.ProductPackageList.CommitNew();
							this.FlowController.ProjectController.FlowMasterDataContext.SubmitChanges();
							this.ProductPackageEditor.EditingControlsEnabled = false;
                            this.ProductPackageEditor.EditingControlsDisabled = true;
							this.ProductPackageEditor.ProductPackageEditButtonStrip.EditState = EditState.View;
                            this.ProductPackageEditor.PackageInfoFields.Visibility = Visibility.Collapsed;
                            this.FlowController.ProjectController.FlowMasterDataContext.FlowCatalogList.CurrentItem.RefreshPackageList();
							break;

						case EditState.Cancel:
							this.ProductPackageList.CancelNew();
							this.ProductPackageEditor.EditingControlsEnabled = false;
                            this.ProductPackageEditor.EditingControlsDisabled = true;
							this.ProductPackageEditor.ProductPackageEditButtonStrip.EditState = EditState.View;
                            this.ProductPackageEditor.PackageInfoFields.Visibility = Visibility.Collapsed;
							break;
					}
					break;


				case EditState.Edit:
					switch (e.NewEditState)
					{
						case EditState.Save:
							this.FlowController.ProjectController.FlowMasterDataContext.SubmitChanges();
							this.ProductPackageEditor.EditingControlsEnabled = false;
                            this.ProductPackageEditor.EditingControlsDisabled = true;
							this.ProductPackageEditor.ProductPackageEditButtonStrip.EditState = EditState.View;
                            this.ProductPackageEditor.PackageInfoFields.Visibility = Visibility.Collapsed;
                            this.FlowController.ProjectController.FlowMasterDataContext.FlowCatalogList.CurrentItem.RefreshPackageList();
							break;

						case EditState.Cancel:
							this.ProductPackageList.RevertCurrent();
							this.ProductPackageEditor.EditingControlsEnabled = false;
                            this.ProductPackageEditor.EditingControlsDisabled = true;
							this.ProductPackageEditor.ProductPackageEditButtonStrip.EditState = EditState.View;
                            this.ProductPackageEditor.PackageInfoFields.Visibility = Visibility.Collapsed;
							break;
					}

					break;


				case EditState.Delete:
					this.ProductPackageEditor.DeleteConfirmationPrompt.Visibility = Visibility.Collapsed;
                    this.FlowController.ProjectController.FlowMasterDataContext.FlowCatalogList.CurrentItem.RefreshPackageList();
					break;

			}

            
		}

		void ConfirmDeleteButton_Click(object sender, RoutedEventArgs e)
		{
            logger.Info("Clicked Confirm Delete button: package='{0}'", this.ProductPackageList.CurrentItem.ProductPackageDesc);
			this.FlowCatalogList.CurrentItem.DeleteProductPackage(this.ProductPackageList.CurrentItem);
			this.ProductPackageEditor.ProductPackageEditButtonStrip.EditState = EditState.View;
		}

		void CancelDeleteButton_Click(object sender, RoutedEventArgs e)
		{
            logger.Info("Clicked Cancel Delete button: package='{0}'", this.ProductPackageList.CurrentItem.ProductPackageDesc);
			this.ProductPackageEditor.ProductPackageEditButtonStrip.EditState = EditState.View;
		}

		void ViewCatalogsButton_Click(object sender, RoutedEventArgs e)
		{
            logger.Info("Clicked View Catalogs button");
			this.FlowController.Catalog();
		}

		void ProductPackageEditor_AddItemInvoked(object sender, EventArgs<ImageQuixProduct> e)
		{
            if (this.ProductPackageList.CurrentItem != null)
            {
                if (this.ProductPackageList.CurrentItem.IsALaCarte && this.ProductPackageList.CurrentItem.ProductPackageCompositions.Count > 0)
                {
                    FlowMessageBox msg = new FlowMessageBox("Can not add item", "You can't add another product to an a-la-carte item");
                    msg.CancelButtonVisible = false;
                    msg.ShowDialog();
                    return;
                }

                if (this.ProductPackageList.CurrentItem.IsAnyXPackage)
                {
                    FlowMessageBox msg = new FlowMessageBox("Can not add item", "You can't add a product to an Any X Package");
                    msg.CancelButtonVisible = false;
                    msg.ShowDialog();
                    return;
                }

                this.ProductPackageList.CurrentItem.AddItemToPackage(e.Data);
            }
		}

		void ProductPackageEditor_DeleteItemInvoked(object sender, EventArgs<ImageQuixProduct> e)
		{
			if (this.ProductPackageList.CurrentItem != null)
				this.ProductPackageList.CurrentItem.RemoveItemFromPackage(e.Data);
		}

        void ProductPackageEditor_AddALaCarteInvoked(object sender, EventArgs<ImageQuixProduct> e)
        {
            decimal price = 8;
            if (this.FlowCatalogList.CurrentItem.ProductPackages.Any(p => p.IsALaCarte == true))
                price = this.FlowCatalogList.CurrentItem.ProductPackages.First(p => p.IsALaCarte == true).Price;
            CreateALaCarteProduct dlg = new CreateALaCarteProduct(e.Data, price, this.FlowController.ProjectController.FlowMasterDataContext);
            bool? result = dlg.ShowDialog();
            if (result == true)
            {
                //really nothing to do for now
            }
        }

		void ProductPackageEditor_EditProductPackageInvoked(object sender, EventArgs<ProductPackage> e)
		{
			switch (this.ProductPackageEditor.ProductPackageEditButtonStrip.EditState)
			{
				case EditState.Edit:
				case EditState.Add:
				case EditState.Delete:
					this.ProductPackageEditor.ProductPackageEditButtonStrip.EditState = EditState.Cancel;
					break;
			}

			this.ProductPackageList.MoveCurrentTo(e.Data);
			this.ProductPackageEditor.ProductPackageEditButtonStrip.EditState = EditState.Edit;
		}



	}	// END class

}	// END namespace
