﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.Lib;
using StructureMap;
using Flow.Schema.LinqModel.DataContext;
using Flow.Lib.AsyncWorker;
using System.ComponentModel;
using System.Net;
using System.IO;
using System.Xml;
using System.Windows.Threading;
using System.Threading;
using System.Globalization;

namespace Flow.Controller.Catalog
{
    class PUDCatalogController
    {

        private string _pudURL;

        private NotificationProgressInfo _notificationInfo;

        private Dispatcher dispatcher { get; set; }

        public bool forceUpdate = false;

        private FlowCatalogController _flowCatalogController {get;set;}
        public PUDCatalogController(string pudURL, FlowCatalogController fcc)
        {
            this._pudURL = pudURL;
            this._flowCatalogController = fcc;

        }



        internal void ProcessPUD()
        {

            if (_flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogs == null ||
                _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogs.Count() == 0)
                _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ExecuteCommand("ALTER TABLE ImageQuixCatalog ALTER COLUMN ImageQuixCatalogID IDENTITY (1,1);");
            if (_flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogOptions == null ||
                _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogOptions.Count() == 0)
                _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ExecuteCommand("ALTER TABLE ImageQuixCatalogOption ALTER COLUMN ImageQuixCatalogOptionID IDENTITY (1,1);");
            if (_flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogOptionGroups == null ||
                _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogOptionGroups.Count() == 0)
                _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ExecuteCommand("ALTER TABLE ImageQuixCatalogOptionGroup ALTER COLUMN ImageQuixCatalogOptionGroupID IDENTITY (1,1);");

            if (_flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixOptions == null ||
                _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixOptions.Count() == 0)
                _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ExecuteCommand("ALTER TABLE ImageQuixOption ALTER COLUMN ImageQuixOptionID IDENTITY (1,1);");
            if (_flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixOptionCommands == null ||
                _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixOptionCommands.Count() == 0)
                _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ExecuteCommand("ALTER TABLE ImageQuixOptionCommand ALTER COLUMN ImageQuixOptionCommandID IDENTITY (1,1);");
            if (_flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixOptionGroups == null ||
                _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixOptionGroups.Count() == 0)
                _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ExecuteCommand("ALTER TABLE ImageQuixOptionGroup ALTER COLUMN ImageQuixOptionGroupID IDENTITY (1,1);");

            if (_flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixProducts == null ||
                _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixProducts.Count() == 0)
                _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ExecuteCommand("ALTER TABLE ImageQuixProduct ALTER COLUMN ImageQuixProductID IDENTITY (1,1);");
            if (_flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixProductGroups == null ||
                _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixProductGroups.Count() == 0)
                _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ExecuteCommand("ALTER TABLE ImageQuixProductGroup ALTER COLUMN ImageQuixProductGroupID IDENTITY (1,1);");
            if (_flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixProductNodes == null ||
                _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixProductNodes.Count() == 0)
                _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ExecuteCommand("ALTER TABLE ImageQuixProductNode ALTER COLUMN ImageQuixProductNodeID IDENTITY (1,1);");



            dispatcher = Dispatcher.CurrentDispatcher;
            IShowsNotificationProgress progressNotification = ObjectFactory.GetInstance<IShowsNotificationProgress>();
            _notificationInfo = new NotificationProgressInfo("Catalog Download", "Processing PUD Catalog...");
            _notificationInfo.NotificationProgress = progressNotification;
            progressNotification.ShowNotification(_notificationInfo);

            _flowCatalogController.FlowController.ApplicationPanel.IsEnabled = false;
            //need to call this to initialize the ImageQuixCatalog list
            FlowObservableCollection<ImageQuixCatalog> iqc = _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogList;
            FlowBackgroundWorker worker = FlowBackgroundWorkerManager.RunWorker(CatalogSync, CatalogSync_Completed);
        }

        void CatalogSync(object sender, DoWorkEventArgs e)
        {
            string pudName = (_pudURL.Split('/')).Last();
            pudName = (pudName.Split('?')).First();


            //get last modify date
            DateTime lastModified = GetLastModifiedDate();

            ImageQuixCatalog catalog;
            //does this catalog exist?
            if (_flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogs != null &&
                _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogs.Count() > 0 &&
                _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogs.Any(iqc => iqc.Label == pudName))
            {
                catalog = _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogs.First(iqc => iqc.Label == pudName);
                if (!forceUpdate && (catalog.ModifyDate == lastModified))
                {
                    //thist catalog has not been changed and does not need to be updated
                    //TODO: disbatch a message and then return
                    return;
                }
            }
            else
            {
                //this is a new catalog
                catalog = new ImageQuixCatalog();
                catalog.ModifyDate = lastModified;
                catalog.Label = pudName;
                _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogs.InsertOnSubmit(catalog);
                _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.SubmitChanges();
                if (!this._flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogList.Contains(catalog))
                {
                    this.dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                    {
                        try
                        {
                            this._flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogList.Add(catalog);
                        }
                        catch
                        {
                            //do nothing
                        }
                    }));
                }

            }

            catalog.ModifyDate = lastModified;
            this._flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogList.MoveCurrentToFirst();

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(_pudURL);

           
            

            //create/get the image quix product groupd
            ImageQuixProductGroup prodGroup;
            if (catalog.ImageQuixProductGroups.Any(p => p.Label == "Units"))
                prodGroup = catalog.ImageQuixProductGroups.First(p => p.Label == "Units");
            else
            {
                prodGroup = new ImageQuixProductGroup();
                prodGroup.Label = "Units";
                prodGroup.ImageQuixCatalogID = catalog.ImageQuixCatalogID;
                _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixProductGroups.InsertOnSubmit(prodGroup);
                _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.SubmitChanges();
                this.dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                {
                    if(!catalog.ProductGroupList.Contains(prodGroup))
                   catalog.ProductGroupList.Add(prodGroup);
                }));
            }

            //first process Units
            XmlNodeList units = xmlDoc.GetElementsByTagName("Unit");

            foreach (XmlNode unit in units)
            {
                if (unit.ChildNodes.Count < 2)
                    continue;
               //map the pud unit node values to an ImageQuix product
                 string map = "";
                if(unit["Map"] != null)
                    map = unit["Map"].InnerText;

                string resourceURL = map;
                string label = unit["Description"].InnerText;
                string width = unit["X"].InnerText;
                string height = unit["Y"].InnerText;

                ImageQuixProduct prod;
                if (catalog.ProductList.Any(p => p.ResourceURL == resourceURL))
                    prod = catalog.ImageQuixProducts.First(p => p.ResourceURL == resourceURL);
                else
                {
                    prod = new ImageQuixProduct();
                    prod.ResourceURL = resourceURL;
                    prod.ImageQuixProductGroupID = prodGroup.ImageQuixProductGroupID;
                    prod.ImageQuixCatalogID = catalog.ImageQuixCatalogID;
                    _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixProducts.InsertOnSubmit(prod);
                    this.dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                    {
                        if (!prodGroup.ImageQuixProducts.Contains(prod))
                            prodGroup.ImageQuixProducts.Add(prod);
                        //if(!prodGroup.ProductList.Contains(prod))
                        //    prodGroup.ProductList.Add(prod);
                    }));
                }

                prod.Label = label;
                prod.Width = Convert.ToDouble(width, CultureInfo.InvariantCulture);
                prod.Height = Convert.ToDouble(height, CultureInfo.InvariantCulture);
                prod.Map = map;
                _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.SubmitChanges();
                prod.PrimaryKey = prod.ImageQuixProductID;
                _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.SubmitChanges();


                



                //every unit has 1 product and 1 product node
                ImageQuixProductNode prodNode;
                if (prod.ProductNodeList.Any(p => p.ImageQuixProductID == prod.ImageQuixProductID))
                    prodNode = prod.ProductNodeList.First(p => p.ImageQuixProductID == prod.ImageQuixProductID);
                else
                {
                    prodNode = new ImageQuixProductNode();
                    prodNode.ResourceURL = resourceURL;
                    prodNode.ImageQuixProductID = prod.ImageQuixProductID;
                    prodNode.Type = 1; // hardcode type 1 is image
                    
                    _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixProductNodes.InsertOnSubmit(prodNode);
                    this.dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                    {
                        if (!prod.ProductNodeList.Contains(prodNode))
                            prod.ProductNodeList.Add(prodNode);
                    }));

                 }

                _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.SubmitChanges();
                prodNode.PrimaryKey = prodNode.ImageQuixProductNodeID;

                //next process RiplynxOptions
                XmlNodeList options = xmlDoc.GetElementsByTagName("RiplynxOption");
                List<XmlNode> productOptions = new List<XmlNode>();
                foreach (XmlNode option in options)
                {
                    string tmptype = null;
                    try
                    {
                        tmptype = option["Primitive"]["PrimitivesArray"]["anyType"]["PrimitiveObject"].GetAttribute("xsi:type");
                    }
                    catch
                    {
                        //do nothing
                    }

                    if (tmptype == null || tmptype == "ColorManagement")
                        continue;

                    if (tmptype == "BackgroundImage")
                    {
                        continue;
                        //try
                        //{
                        //    string filepath = option["Primitive"]["PrimitivesArray"]["anyType"]["PrimitiveObject"]["Filename"].InnerText;
                        //    tmplabel = (new FileInfo(filepath)).Name;
                        //}
                        //catch
                        //{
                        //    //do nothing
                        //}
                    }
                    productOptions.Add(option);
                }

                int optGroupCount = 3;
                if (productOptions.Count < 3) optGroupCount = productOptions.Count;
                for (int i = 0; i < optGroupCount; i++)
                {
                    ImageQuixOptionGroup opg = null;
                    if ((prod.ImageQuixOptionGroups != null && prod.ImageQuixOptionGroups.Count > 0))
                        if (prod.ImageQuixOptionGroups.Any(o => o.Label == "option " + (i + 1) + ":"))
                            opg = prod.ImageQuixOptionGroups.First(o => o.Label == "option " + (i + 1) + ":");

                    if (opg == null)
                    {
                        opg = new ImageQuixOptionGroup();
                        opg.PrimaryKey = (prod.ImageQuixOptionGroups.Count + 1) + 1000;// Convert.ToInt32(Flow.Lib.Helpers.TicketGenerator.GenerateNumericTicketString(9));
                        opg.ImageQuixProduct = prod;
                        opg.Label = "option " + (i + 1) + ":";
                        opg.ResourceURL = "option " + (i + 1) + ":";
                        opg.Parent = prod;
                        this.dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                        {
                            prod.ImageQuixOptionGroups.Add(opg);
                            _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixOptionGroups.InsertOnSubmit(opg);
                        }));
                    }


                    foreach (XmlNode option in productOptions)
                    {
                        string tmpmap = option["Map"].InnerText;
                        string tmpresourceURL = tmpmap;
                        string tmplabel = option["Description"].InnerText;
                        string tmpcost = option["Cost"].InnerText;

                        ImageQuixOption opt = null;
                        if ((opg.ImageQuixOptions != null && opg.ImageQuixOptions.Count > 0))
                            if (opg.ImageQuixOptions.Any(o => o.Label == tmplabel))
                                opt = opg.ImageQuixOptions.First(o => o.Label == tmplabel);

                        if (opt == null)
                        {
                            opt = new ImageQuixOption();
                            opt.ResourceURL = tmpresourceURL;
                            opt.Label = tmplabel;
                            opt.ImageQuixOptionGroupID = opg.ImageQuixOptionGroupID;
                            opt.ImageQuixOptionGroup = opg;
                            opt.ImageQuixOptionGroupPk = opg.PrimaryKey;
                            opt.IsDefault = false;
                            opt.PrimaryKey = (this._flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixOptions.Count() + 1) + 3000;// Convert.ToInt32(Flow.Lib.Helpers.TicketGenerator.GenerateNumericTicketString(9));

                            this.dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                            {
                                opg.ImageQuixOptions.Add(opt);
                                _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixOptions.InsertOnSubmit(opt);
                                _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.SubmitChanges();
                            }));
                        }
                    }
                }
                if (this._flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixOptions.Where(iqo => iqo.PrimaryKey == 3002).Count() > 1 ||
                    this._flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixOptions.Where(iqo => iqo.PrimaryKey == 3003).Count() > 1 ||
                    this._flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixOptions.Where(iqo => iqo.PrimaryKey == 3004).Count() > 1)
                {
                    int n = 1;
                    foreach (ImageQuixOption o in this._flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixOptions)
                    {
                        o.PrimaryKey = 3000 + n;
                        n++;
                    }
                }
                _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.SubmitChanges();
                catalog.RefreshCatalogOptionList();
                catalog.RefreshProductList();



               



                
            }


             //create/get the image quix option group
            ImageQuixCatalogOptionGroup opGroup;
            if (catalog.ImageQuixCatalogOptionGroups.Any(p => p.Label == "Image Options"))
                opGroup = catalog.ImageQuixCatalogOptionGroups.First(p => p.Label == "Image Options");
            else
            {
                opGroup = new ImageQuixCatalogOptionGroup();
                opGroup.Label = "Image Options";
                opGroup.ImageQuixCatalogID = catalog.ImageQuixCatalogID;
                _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogOptionGroups.InsertOnSubmit(opGroup);

                _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.SubmitChanges();
                opGroup.PrimaryKey = opGroup.ImageQuixCatalogOptionGroupID;
                this.dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                    {
                        if(!catalog.CatalogOptionGroupList.Contains(opGroup))
                            catalog.CatalogOptionGroupList.Add(opGroup);
                    }));
            }

            //next process RiplynxOptions
            XmlNodeList BGoptions = xmlDoc.GetElementsByTagName("RiplynxOption");
            foreach (XmlNode option in BGoptions)
            {

                string map = option["Map"].InnerText;
                string resourceURL = map;
                string label = option["Description"].InnerText;
                string cost = option["Cost"].InnerText;
                string type = null;
                try
                {
                    type = option["Primitive"]["PrimitivesArray"]["anyType"]["PrimitiveObject"].GetAttribute("xsi:type");
                }
                catch
                {
                    //do nothing
                }
                
                //if (type == null || type == "ColorManagement")
                //    continue;

                if (type == "BackgroundImage")
                {
                    try
                    {
                        string filepath = option["Primitive"]["PrimitivesArray"]["anyType"]["PrimitiveObject"]["Filename"].InnerText;
                        label = (new FileInfo(filepath)).Name;
                    }
                    catch
                    {
                        //do nothing
                    }
                }
                else 
                {
                    
                        //do nothing
                }
                //else
                //    continue;

                ImageQuixCatalogOption op;
                if (this._flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogOptions.Any(p => p.ResourceURL == resourceURL))
                    op = this._flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogOptions.First(p => p.ResourceURL == resourceURL);
                else
                {
                    op = new ImageQuixCatalogOption();
                    op.ResourceURL = resourceURL;
                    op.ImageQuixCatalogOptionGroupID = opGroup.ImageQuixCatalogOptionGroupID;
                    _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogOptions.InsertOnSubmit(op);
                    this.dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                    {
                        if (!opGroup.OptionList.Contains(op))
                            opGroup.OptionList.Add(op);
                        if (!catalog.CatalogOptionList.Contains(op))
                            catalog.CatalogOptionList.Add(op);
                    }));

                }
                op.AppliesTo = "Image";
                if (type == "BackgroundImage")
                {
                    op.AppliesTo = "Background";
                }
                op.Label = label;
                op.ImageQuixCatalogOptionGroupPk = opGroup.PrimaryKey;
                op.Price = Convert.ToDecimal(cost);
                _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.SubmitChanges();
                op.PrimaryKey = op.ImageQuixCatalogOptionID;
                _flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.SubmitChanges();

                
            }
           
            
        }

        private DateTime GetLastModifiedDate()
        {
            Uri myUri = new Uri(_pudURL);
            // Creates an HttpWebRequest for the specified URL. 
            HttpWebRequest myHttpWebRequest = (HttpWebRequest)WebRequest.Create(myUri);
            HttpWebResponse myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();

            DateTime date =  myHttpWebResponse.LastModified;
            // Releases the resources of the response.
            myHttpWebResponse.Close();

            return date;
        }


        void CatalogSync_Completed(object sender, RunWorkerCompletedEventArgs e)
        {
            ImageQuixCatalog newItem = this._flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogs.First();
            newItem.RefreshProductList();
            newItem.RefreshCatalogOptionList();
            _notificationInfo.Complete("Finished processing catalog");
            Console.Write("-----------------\n-----------------\nDONE WITH PUD CATALOG SYNC\n-----------------\n-----------------\n");
            _flowCatalogController.FlowController.ApplicationPanel.IsEnabled = true;


                this._flowCatalogController.ImageQuixCatalogPanel.CatalogProductGroupList.ItemsSource = null;
                this._flowCatalogController.ImageQuixCatalogPanel.CatalogProductGroupList.ItemsSource = this._flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogList.CurrentItem.ProductGroupList;
                this._flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.UpdateImageQuixCatalogList();
                this._flowCatalogController.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogList.CurrentItem.RefreshProductList();
                this._flowCatalogController.ImageQuixCatalogPanel.CatalogProductGroupList.UpdateLayout();



        }


       
    }
}
