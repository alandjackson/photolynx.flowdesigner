﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Flow.Lib.ImageUtils;
using System.Windows;
using Flow.View.Edit.ImageAdjustments;
using System.Windows.Media.Effects;

using Flow.Schema.LinqModel.DataContext;
using System.Threading;
using Flow.Lib.AsyncWorker;
using NLog;

namespace Flow.Controller.Edit.ImageAdjustments
{
    public class CropToolController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        Dictionary<string, double> _cropPresets;

        CropToolPanel _cropToolPanel;
        ImageDisplayPanel _currentImagePanel;
        public ToolController ToolController { get; set; }

        Canvas _canvasMask;
        Canvas[] _maskGrips;
        RectangleGeometry _maskTransparent;
        RectangleGeometry _maskVisible;
        Grid _maskGripGrid;
        BitmapEffect _imageDisplayEffects;

        int _maskTransformMode = -1;
        bool _maskResizing = false;
        Point _lastMousePosition;
        int _gripSize = 7;

		public SubjectImage CurrentImage { get; set; }

        double CurrentRatio
        {
            get { return (double)_cropToolPanel.comboBoxCropPresets.SelectedValue; }
        }
        
        public CropToolController(ImageDisplayPanel iPanel, CropToolPanel cPanel)
        {
            
            _cropToolPanel = cPanel;
            _currentImagePanel = iPanel;

            _cropToolPanel.buttonShowMask.Click += new System.Windows.RoutedEventHandler(buttonShowMask_Click);

            _currentImagePanel.MouseWheel += new MouseWheelEventHandler(currentImagePanel_MouseWheel);

            _canvasMask = new Canvas();
            _canvasMask.Background = new SolidColorBrush(Colors.Black);
            _canvasMask.Opacity = .5;
            _maskTransparent = new RectangleGeometry();
            _maskVisible = new RectangleGeometry();
            _canvasMask.Clip = new CombinedGeometry(GeometryCombineMode.Exclude, _maskTransparent, _maskVisible);

            _maskGripGrid = new Grid();
            _maskGripGrid.VerticalAlignment = VerticalAlignment.Top;
            _maskGripGrid.HorizontalAlignment = HorizontalAlignment.Left;
            _maskGripGrid.RowDefinitions.Add(new RowDefinition());
            _maskGripGrid.RowDefinitions.Add(new RowDefinition());
            _maskGripGrid.RowDefinitions.Add(new RowDefinition());
            _maskGripGrid.ColumnDefinitions.Add(new ColumnDefinition());
            _maskGripGrid.ColumnDefinitions.Add(new ColumnDefinition());
            _maskGripGrid.ColumnDefinitions.Add(new ColumnDefinition());

            _maskGrips = new Canvas[8];
            for (int i = 0; i < 8; i++)
            {
                _maskGrips[i] = new Canvas();
                _maskGrips[i].Background = new SolidColorBrush(Colors.WhiteSmoke);
                _maskGrips[i].Width = _gripSize;
                _maskGrips[i].Height = _gripSize;
                _maskGripGrid.Cursor = Cursors.Hand;
                _maskGripGrid.Children.Add(_maskGrips[i]);
                _maskGrips[i].Name = "g" + i;
                _maskGrips[i].MouseLeftButtonDown += new MouseButtonEventHandler(CropToolController_MouseLeftButtonDown);
                _maskGrips[i].MouseMove += new MouseEventHandler(CropToolController_MouseMove);
                _maskGrips[i].MouseLeftButtonUp += new MouseButtonEventHandler(CropToolController_MouseLeftButtonUp);
            }

            _currentImagePanel.Image.MouseLeftButtonDown += new MouseButtonEventHandler(Image_MouseLeftButtonDown);
            _currentImagePanel.Image.MouseLeftButtonUp += new MouseButtonEventHandler(Image_MouseLeftButtonUp);
            _currentImagePanel.Image.MouseMove += new MouseEventHandler(Image_MouseMove);

            _currentImagePanel.imgOverlay.MouseLeftButtonDown += new MouseButtonEventHandler(Image_MouseLeftButtonDown);
            _currentImagePanel.imgOverlay.MouseLeftButtonUp += new MouseButtonEventHandler(Image_MouseLeftButtonUp);
            _currentImagePanel.imgOverlay.MouseMove += new MouseEventHandler(Image_MouseMove);

            _maskGrips[0].SetValue(Grid.RowProperty, 0);
            _maskGrips[0].SetValue(Grid.ColumnProperty, 0);
            _maskGrips[0].HorizontalAlignment = HorizontalAlignment.Left;
            _maskGrips[0].VerticalAlignment = VerticalAlignment.Top;
            _maskGrips[0].Margin = new Thickness(-_gripSize / 2, -_gripSize / 2, 0, 0);

            _maskGrips[1].SetValue(Grid.RowProperty, 0);
            _maskGrips[1].SetValue(Grid.ColumnProperty, 1);
            _maskGrips[1].HorizontalAlignment = HorizontalAlignment.Center;
            _maskGrips[1].VerticalAlignment = VerticalAlignment.Top;
            _maskGrips[1].Margin = new Thickness(0, -_gripSize / 2, 0, 0);

            _maskGrips[2].SetValue(Grid.RowProperty, 0);
            _maskGrips[2].SetValue(Grid.ColumnProperty, 2);
            _maskGrips[2].HorizontalAlignment = HorizontalAlignment.Right;
            _maskGrips[2].VerticalAlignment = VerticalAlignment.Top;
            _maskGrips[2].Margin = new Thickness(0, -_gripSize / 2, -_gripSize / 2, 0);

            _maskGrips[3].SetValue(Grid.RowProperty, 1);
            _maskGrips[3].SetValue(Grid.ColumnProperty, 0);
            _maskGrips[3].HorizontalAlignment = HorizontalAlignment.Left;
            _maskGrips[3].VerticalAlignment = VerticalAlignment.Center;
            _maskGrips[3].Margin = new Thickness(-_gripSize / 2, 0, 0, 0);

            _maskGrips[4].SetValue(Grid.RowProperty, 1);
            _maskGrips[4].SetValue(Grid.ColumnProperty, 2);
            _maskGrips[4].HorizontalAlignment = HorizontalAlignment.Right;
            _maskGrips[4].VerticalAlignment = VerticalAlignment.Center;
            _maskGrips[4].Margin = new Thickness(0, 0, -_gripSize / 2, 0);

            _maskGrips[5].SetValue(Grid.RowProperty, 2);
            _maskGrips[5].SetValue(Grid.ColumnProperty, 0);
            _maskGrips[5].HorizontalAlignment = HorizontalAlignment.Left;
            _maskGrips[5].VerticalAlignment = VerticalAlignment.Bottom;
            _maskGrips[5].Margin = new Thickness(-_gripSize / 2, 0, 0, -_gripSize / 2);

            _maskGrips[6].SetValue(Grid.RowProperty, 2);
            _maskGrips[6].SetValue(Grid.ColumnProperty, 1);
            _maskGrips[6].HorizontalAlignment = HorizontalAlignment.Center;
            _maskGrips[6].VerticalAlignment = VerticalAlignment.Bottom;
            _maskGrips[6].Margin = new Thickness(0, 0, 0, -_gripSize / 2);

            _maskGrips[7].SetValue(Grid.RowProperty, 2);
            _maskGrips[7].SetValue(Grid.ColumnProperty, 2);
            _maskGrips[7].HorizontalAlignment = HorizontalAlignment.Right;
            _maskGrips[7].VerticalAlignment = VerticalAlignment.Bottom;
            _maskGrips[7].Margin = new Thickness(0, 0, -_gripSize / 2, -_gripSize / 2);

            _cropToolPanel.comboBoxCropPresets.SelectionChanged += new SelectionChangedEventHandler(comboBoxCropPresets_SelectionChanged);

            _cropPresets = new Dictionary<string, double>();
            _cropPresets.Add("None", 0);
            _cropPresets.Add("8x10", 8.0 / 10);
            _cropPresets.Add("10x8", 10.0 / 8);
            _cropPresets.Add("5x7", 5.0 / 7);
            _cropPresets.Add("7x5", 7.0 / 5);
            _cropToolPanel.comboBoxCropPresets.ItemsSource = _cropPresets;
            _cropToolPanel.comboBoxCropPresets.SelectedIndex = 0;
            
        }

        void Image_MouseMove(object sender, MouseEventArgs e)
        {
            
            if (_maskTransformMode == 8)
            {
                CropToolController_MouseMove(sender, e);
            }
            if (needImageRefresh)
            {
                this.CurrentImage.UpdateGroupImageCrop();
                this.CurrentImage.RefreshImage();
                needImageRefresh = false;
                this.CurrentImage.ExistsInOnlineGallery = false;
                ToolController.ImageAdjustmentsController.FlowController.ProjectController.CurrentFlowProject.FlowProjectDataContext.UpdateNetworkProject = true;
            }
            
        }

        void Image_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            
            if (CurrentImage == null) return;

            if (_maskTransformMode == 8)
            {
                _maskTransformMode = -1;
                (sender as Image).ReleaseMouseCapture();
                _maskResizing = false;
            }
            this.CurrentImage.UpdateGroupImageCrop();
            this.CurrentImage.RefreshImage();
            this.CurrentImage.ExistsInOnlineGallery = false;
            ToolController.ImageAdjustmentsController.FlowController.ProjectController.CurrentFlowProject.FlowProjectDataContext.UpdateNetworkProject = true;
            
        }

        void Image_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            
            if (_currentImagePanel.GridDisplay.Children.Contains(_canvasMask))
            {
                (sender as Image).CaptureMouse();
                _maskResizing = true;
                _maskTransformMode = 8;
                _lastMousePosition = Mouse.GetPosition(_currentImagePanel.Image);
            }
            
        }

        void comboBoxCropPresets_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
            if(CurrentImage != null)
                SetVisibleCanvasProperties();
            
        }

        void CropToolController_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            
            if (this.CurrentImage == null)
                return;

            if (_maskResizing)
            {
                _maskResizing = false;
                (sender as Canvas).ReleaseMouseCapture();
                _maskTransformMode = -1;
            }
            this.CurrentImage.ExistsInOnlineGallery = false;
            ToolController.ImageAdjustmentsController.FlowController.ProjectController.CurrentFlowProject.FlowProjectDataContext.UpdateNetworkProject = true;
            //return;
            this.CurrentImage.RefreshImage();
            
        }

        void CropToolController_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            
            (sender as Canvas).CaptureMouse();
            _maskResizing = true;
            _lastMousePosition = Mouse.GetPosition(_currentImagePanel.Image);
            _maskTransformMode = Convert.ToInt16((sender as Canvas).Name[1]) - 48;
            
        }

        void AdjustLeft()
        {
            
            CurrentImage.CropW += CurrentImage.CropL;
            CurrentImage.CropL = 0;
            
        }

        private void AdjustTop()
        {
            
            CurrentImage.CropH += CurrentImage.CropT;
            CurrentImage.CropT = 0;
            
        }

        private void AdjustRight()
        {
            
            CurrentImage.CropW = 1 - CurrentImage.CropL;
            
        }

        private void AdjustBottom()
        {
            
            CurrentImage.CropH = 1 - CurrentImage.CropT;
            
        }

        bool needImageRefresh = false;

        void currentImagePanel_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            
            if (!this.ToolController.ImageAdjustmentsController.cDown && !this.ToolController.ImageAdjustmentsController.oDown)
            {
                if (e.Delta > 0)
                    DecreaseCropArea();
                else if (e.Delta < 0)
                    IncreaseCropArea();
            }
            
        }

        void IncreaseCropArea()
        {
            
            if (_currentImagePanel.GridDisplay.Children.Contains(_canvasMask))
            {
                if (CurrentImage.CropT + CurrentImage.CropH < 1)
                {
                    CurrentImage.CropH += .02;
                    CurrentImage.CropW += .02;
                    CurrentImage.CropL -= .01;
                    CurrentImage.CropT -= .01;
                    if (CurrentImage.CropL < 0)
                        CurrentImage.CropL = 0;
                    if (CurrentImage.CropL + CurrentImage.CropW > 1)
                        CurrentImage.CropW = 1 - CurrentImage.CropL;

                    if (CurrentImage.CropT < 0)
                        CurrentImage.CropT = 0;
                    if (CurrentImage.CropT + CurrentImage.CropH > 1)
                        CurrentImage.CropH = 1 - CurrentImage.CropT;
                }
                SetVisibleCanvasProperties();
                needImageRefresh = true;
               
            }
            
        }

        void DecreaseCropArea()
        {
            
            if (_currentImagePanel.GridDisplay.Children.Contains(_canvasMask))
            {
                CurrentImage.CropH -= .02;
                CurrentImage.CropW -= .02;
                CurrentImage.CropL += .01;
                CurrentImage.CropT += .01;
                SetVisibleCanvasProperties();
                needImageRefresh = true;
               
            }
            
        }

        void CropToolController_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            
            if (CurrentImage == null)
                return;
            if (_maskResizing)
            {

                Point position = Mouse.GetPosition(_currentImagePanel.Image);

                if (position.Y > _currentImagePanel.Image.ActualHeight)
                {
                    position.Y = _currentImagePanel.Image.ActualHeight;
                }
                if (position.Y < 0)
                {
                    position.Y = 0;
                }

                if (position.X < 0)
                {
                    position.X = 0;
                }

                double dx = (position.X - _lastMousePosition.X) /
                    _currentImagePanel.Image.ActualWidth;
                double dy = (position.Y - _lastMousePosition.Y) /
                    _currentImagePanel.Image.ActualHeight;


                if ((double)_cropToolPanel.comboBoxCropPresets.SelectedValue != (double)0.0)
                {
                    if (CurrentImage.CropH + CurrentImage.CropT >= 1)
                    {
                        if (_maskTransformMode == 3 && dx < 0)
                        {
                            dx = 0;
                        }
                        if (_maskTransformMode == 4 && dx > 0)
                        {
                            dx = 0;
                        }
                        CurrentImage.CropH = 1 - CurrentImage.CropT;
                    }

                    //CurrentImage.CropW + CurrentImage.CropL >= _currentImagePanel.Image.ActualWidth
                }

                switch (_maskTransformMode)
                {
                    case 0://NW
                        if (CurrentImage.CropT + dy >= 0 && CurrentImage.CropL + dx >= 0)
                        {
                            CurrentImage.CropL += dx;
                            CurrentImage.CropW -= dx;
                            CurrentImage.CropT += dy;
                            CurrentImage.CropH -= dy;
                            break;
                        }
                        if (CurrentImage.CropT + dy < 0)
                        {
                            AdjustTop();
                        }
                        if (CurrentImage.CropL + dx < 0)
                        {
                            AdjustLeft();
                        }
                        break;
                    case 1://N
                        if (CurrentImage.CropL + CurrentImage.CropW >= 1 && (double)_cropToolPanel.comboBoxCropPresets.SelectedValue != 0
                            && dy < 0)
                        {
                            break;
                        }
                        if (CurrentImage.CropT + dy < 0)
                        {
                            AdjustTop();
                        }
                        else
                        {
                            CurrentImage.CropT += dy;
                            CurrentImage.CropH -= dy;
                        }
                        break;
                    case 2://NE
                        if (CurrentImage.CropT + dy >= 0 && CurrentImage.CropL + CurrentImage.CropW + dx <= 1)
                        {
                            CurrentImage.CropT += dy;
                            CurrentImage.CropH -= dy;
                            CurrentImage.CropW += dx;
                            break;
                        }
                        if (CurrentImage.CropT + dy < 0)
                        {
                            AdjustTop();
                        }
                        if (CurrentImage.CropL + CurrentImage.CropW + dx > 1)
                        {
                            AdjustRight();
                        }
                        break;
                    case 3://W
                        if (CurrentImage.CropL + dx < 0)
                        {
                            AdjustLeft();
                        }
                        else
                        {
                            CurrentImage.CropL += dx;
                            CurrentImage.CropW -= dx;
                        }
                        break;
                    case 4://E
                        if (CurrentImage.CropL + CurrentImage.CropW + dx > 1)
                        {
                            AdjustRight();
                        }
                        else
                        {
                            CurrentImage.CropW += dx;
                        }
                        break;
                    case 5://SW
                        if (CurrentImage.CropT + CurrentImage.CropH + dy <= 1 && CurrentImage.CropL + dx >= 0)
                        {
                            if (CurrentImage.CropT + CurrentImage.CropH >= 1 && dx < 0 &&
                                (double)_cropToolPanel.comboBoxCropPresets.SelectedValue != 0)
                                break;
                            CurrentImage.CropH += dy;
                            CurrentImage.CropL += dx;
                            CurrentImage.CropW -= dx;
                            break;
                        }
                        if (CurrentImage.CropT + CurrentImage.CropH + dy > 1)
                        {
                            AdjustBottom();
                        }
                        if (CurrentImage.CropL + dx < 0)
                        {
                            AdjustLeft();
                        }
                        break;
                    case 6://S
                        if (CurrentImage.CropL + CurrentImage.CropW >= 1 && (double)_cropToolPanel.comboBoxCropPresets.SelectedValue != 0
                            && dy > 0)
                        {
                            break;
                        }
                        if (CurrentImage.CropT + CurrentImage.CropH + dy > 1)
                        {
                            AdjustBottom();
                        }
                        else
                        {
                            CurrentImage.CropH += dy;
                        }
                        break;
                    case 7://SE
                        if (CurrentImage.CropL + CurrentImage.CropW >= 1 && (double)_cropToolPanel.comboBoxCropPresets.SelectedValue != 0
                            && (dx > 0 || dy > 0))
                        {
                            break;
                        }
                        if (CurrentImage.CropL + CurrentImage.CropW + dx <= 1 && CurrentImage.CropT + CurrentImage.CropH + dy <= 1)
                        {
                            CurrentImage.CropW += dx;
                            CurrentImage.CropH += dy;
                            break;
                        }
                        if (CurrentImage.CropT + CurrentImage.CropH + dy > 1)
                        {
                            AdjustBottom();
                        }
                        if (CurrentImage.CropL + CurrentImage.CropW + dx > 1)
                        {
                            AdjustRight();
                        }
                        break;
                    case 8://Move
                        bool moveOk = true;
                        if (CurrentImage.CropT + dy < 0)
                        {
                            CurrentImage.CropT = 0;
                            moveOk = false;
                        }
                        if (CurrentImage.CropL + dx < 0)
                        {
                            CurrentImage.CropL = 0;
                            moveOk = false;
                        }
                        if (CurrentImage.CropT + CurrentImage.CropH + dy > 1)
                        {
                            CurrentImage.CropT = 1 - CurrentImage.CropH;
                            moveOk = false;
                        }
                        if (CurrentImage.CropL + dx + CurrentImage.CropW > 1)
                        {
                            CurrentImage.CropL = 1 - CurrentImage.CropW;
                            moveOk = false;
                        }
                        if (moveOk)
                        {
                            CurrentImage.CropL += dx;
                            CurrentImage.CropT += dy;
                        }
                        break;
                    default:
                        break;
                }

                _lastMousePosition = position;

                SetVisibleCanvasProperties();
                
            }
            
        }

        void buttonShowMask_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            
            logger.Info("Clicked Show/Hide Mask button");

            if (_currentImagePanel.GridDisplay.Children.Contains(_canvasMask))
            {
                HideMask();
            }
            else
            {
                ShowMask();
            }
            
        }

        public void ShowMask()
        {
            
            if (!_currentImagePanel.GridDisplay.Children.Contains(_canvasMask))
            {
                logger.Info("Setting mask to shown");
                _currentImagePanel.GridDisplay.Children.Add(_canvasMask);
                _currentImagePanel.GridDisplay.Children.Add(_maskGripGrid);
                SetVisibleCanvasProperties();
                _imageDisplayEffects = (BitmapEffect)_currentImagePanel.Image.GetValue(UIElement.BitmapEffectProperty);
                _currentImagePanel.Image.SetValue(UIElement.BitmapEffectProperty, null);
            }
            
        }
        public void HideMask()
        {
            
            if (_currentImagePanel.GridDisplay.Children.Contains(_canvasMask))
            {
                logger.Info("Setting mask to hidden");
                _currentImagePanel.GridDisplay.Children.Remove(_canvasMask);
                _currentImagePanel.GridDisplay.Children.Remove(_maskGripGrid);
                _currentImagePanel.Image.SetValue(UIElement.BitmapEffectProperty, _imageDisplayEffects);
            }
            
        }

        private void AdjustForRatioLock()
        {

           

            if (CurrentImage == null) return;

            if (_maskTransformMode == -1 //Not being dragged
                && (CurrentImage.CropH <= 0 || CurrentImage.CropW <= 0))
            {
                if (CurrentRatio == 0)
                {
                    CurrentImage.CropL = 0;
                    CurrentImage.CropH = 1;
                    CurrentImage.CropT = 0;
                    CurrentImage.CropW = 1;
                }
                else if (CurrentRatio < 1)//tall
                {
                    CurrentImage.CropW = 1;
                    CurrentImage.CropL = 0;
                    //CurrentImage.CropH = CurrentRatio;
                    CurrentImage.CropH = 1;
                    CurrentImage.CropT = 0;
                }
            }

            if (_cropToolPanel.comboBoxCropPresets.SelectedIndex != 0 && _currentImagePanel.Image.Source != null)
            {
                switch (_maskTransformMode)
                {
                    case 0:
                        double width = CurrentImage.CropH * (double)_cropToolPanel.comboBoxCropPresets.SelectedValue * _currentImagePanel.Image.ActualHeight / _currentImagePanel.Image.ActualWidth;
                        CurrentImage.CropL += CurrentImage.CropW - width;
                        CurrentImage.CropW = width;
                        break;
                    case 1:
                    case 2:
                    case 6:
                    case 7:
                        CurrentImage.CropW = CurrentImage.CropH * (double)_cropToolPanel.comboBoxCropPresets.SelectedValue * (_currentImagePanel.Image.ActualHeight / _currentImagePanel.Image.ActualWidth);
                        break;
                    default:
                    case 3:
                    case 4:
                    case 5:
                        if (_cropToolPanel.comboBoxCropPresets.SelectedValue == null)
                            break;
                        double divisor = ((double)_cropToolPanel.comboBoxCropPresets.SelectedValue * (_currentImagePanel.Image.ActualHeight / _currentImagePanel.Image.ActualWidth));
                        if (divisor == 0)
                            CurrentImage.CropH = 0;
                        else
                            CurrentImage.CropH = CurrentImage.CropW / divisor;

                        //if (CurrentImage.CropH + CurrentImage.CropT > 1)
                        //{
                        //    double excess = CurrentImage.CropH + CurrentImage.CropT - 1;
                        //    CurrentImage.CropH -= excess;
                        //    CurrentImage.CropW -= excess;
                        //}
                        break;
                }

            }

            if (CurrentImage.CropH > 1) { CurrentImage.CropH = 1; CurrentImage.CropT = 0; }
            if (CurrentImage.CropW > 1) { CurrentImage.CropW = 1; CurrentImage.CropL = 0;}
        }

        public void SetVisibleCanvasProperties()
        {

            if (CurrentImage.CropH > 1) CurrentImage.CropH = 1;
            if (CurrentImage.CropW > 1) CurrentImage.CropW = 1;

            if (CurrentImage == null) return;
            AdjustForRatioLock();

            double width = _currentImagePanel.Image.ActualWidth * CurrentImage.CropW;
            double height = _currentImagePanel.Image.ActualHeight * CurrentImage.CropH;
            double top = CurrentImage.CropT * _currentImagePanel.Image.ActualHeight;
            double left = CurrentImage.CropL * _currentImagePanel.Image.ActualWidth;
            if (width < 0)
            {
                width = 0;
            }
            if (height < 0)
            {
                height = 0;
            }

            _maskTransparent.Rect = new Rect(_currentImagePanel.Image.ActualWidth * -.05, _currentImagePanel.Image.ActualHeight * -.05,
                _currentImagePanel.Image.ActualWidth * 1.05, _currentImagePanel.Image.ActualHeight * 1.05);
            _maskVisible.Rect = new Rect(left, top, width, height);
            _maskGripGrid.Width = width;
            _maskGripGrid.Height = height;
            _maskGripGrid.Margin = new Thickness(left, top, 0, 0);


            //int oLeft = (int)_currentImagePanel.Image.ActualWidth - (int)(CurrentImage.CropW * _currentImagePanel.Image.ActualWidth);
            //int oTop = (int)_currentImagePanel.Image.ActualHeight - (int)(CurrentImage.CropH * _currentImagePanel.Image.ActualHeight);

            if (width > 0)
            {
                _currentImagePanel.imgOverlay.Height = height;//CurrentImage.CropH * _currentImagePanel.Image.ActualHeight;
                _currentImagePanel.imgOverlay.Width = width;//CurrentImage.CropW * _currentImagePanel.Image.ActualWidth;
                //double leftBuffer = ((width - _currentImagePanel.imgOverlay.Source.Width) / 2);
                //double topBuffer = ((height - _currentImagePanel.imgOverlay.Source.Height) / 2);
                //if (leftBuffer > 0)
                //    left = leftBuffer + left;
                //if (topBuffer > 0)
                //    top = topBuffer + top;
                _currentImagePanel.canvasImageOverlay.Margin = new Thickness(left, top, 0, 0);
                _currentImagePanel.imgOverlay.Margin = new Thickness(0, 0, 0, 0);
                
            }

           
            //_currentImagePanel.imgOverlay.Margin.Top = oTop;
            
        }

        internal void Update()
        {

            //if (CurrentImage.CropH == 0) CurrentImage.CropH = 1;
            //if (CurrentImage.CropW == 0) CurrentImage.CropW = 1;
            //if (CurrentImage.CropH > 1) { CurrentImage.CropH = 1; AdjustForRatioLock(); }
           // if (CurrentImage.CropW > 1) { CurrentImage.CropW = 1; AdjustForRatioLock(); }


            double width = _currentImagePanel.Image.ActualWidth * CurrentImage.CropW;
            double height = _currentImagePanel.Image.ActualHeight * CurrentImage.CropH;

            //if (height > _currentImagePanel.Image.ActualHeight)
            //{
            //    height = _currentImagePanel.Image.ActualHeight;
            //    CurrentImage.CropH = 1;
            //}

            if (width > 0 && height > 0)
            {
                if (((double)(width / height) >= (((double)8 / 10) - ((double).01))) && ((double)(width / height) <= (((double)8 / 10) + ((double).01))))
                    this._cropToolPanel.comboBoxCropPresets.SelectedIndex = 1;
                if (((double)(width / height) >= (((double)5 / 7) - ((double).01))) && ((double)(width / height) <= (((double)5 / 7) + ((double).01))))
                    this._cropToolPanel.comboBoxCropPresets.SelectedIndex = 3;
                if (((double)(width / height) >= (((double)10 / 8) - ((double).01))) && ((double)(width / height) <= (((double)10 / 8) + ((double).01))))
                    this._cropToolPanel.comboBoxCropPresets.SelectedIndex = 2;
                if (((double)(width / height) >= (((double)7 / 5) - ((double).01))) && ((double)(width / height) <= (((double)7 / 5) + ((double).01))))
                    this._cropToolPanel.comboBoxCropPresets.SelectedIndex = 4;
                
                //if(this._cropToolPanel.comboBoxCropPresets.SelectedIndex == 0 && height > width)
                //    this._cropToolPanel.comboBoxCropPresets.SelectedIndex = 1;
                //if (this._cropToolPanel.comboBoxCropPresets.SelectedIndex == 0 && height < width)
                //    this._cropToolPanel.comboBoxCropPresets.SelectedIndex = 2;


            }

            
            SetVisibleCanvasProperties();
            
        }

        internal void InitCrop(SubjectImage image)
        {
            double thisRatio = image.CropW / image.CropH;
            if (Math.Round(thisRatio, 1) == .8)
            {
                _cropToolPanel.comboBoxCropPresets.SelectedIndex = 2;
            }
            if (Math.Round(thisRatio, 1) == 1.2)
            {
                _cropToolPanel.comboBoxCropPresets.SelectedIndex = 1;
            }
        }
    }
}
