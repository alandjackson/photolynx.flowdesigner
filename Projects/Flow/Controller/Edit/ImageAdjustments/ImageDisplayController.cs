﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.View.Edit.ImageAdjustments;
using Flow.Model;
using Flow.Schema.LinqModel.DataContext;
using System.Windows.Media.Imaging;
using Flow.Lib.ImageUtils;
using System.IO;

namespace Flow.Controller.Edit.ImageAdjustments
{
    public class ImageDisplayController
    {
        public ImageDisplayPanel ImageDisplayPanel { get; set; }

        public ImageDisplayController(ImageDisplayPanel pnl)
        {
            ImageDisplayPanel = pnl;
        }
    }
}
