﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.View.Edit.ImageAdjustments;
using Flow.Schema.LinqModel.DataContext;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using Flow.Lib.ImageUtils;
using Flow.Interop.ImageRender;

namespace Flow.Controller.Edit.ImageAdjustments
{
    public class ToolController
    {

        public ImageAdjustmentsController ImageAdjustmentsController { get; set; }

		SubjectImage _image;
        ImageDisplayPanel _iPanel;

        LevelsToolController _levelsToolC;
        LevelsToolPanel _levelsToolPanel;
        public LevelsToolPanel LevelsToolPanel
        {
            get { return _levelsToolPanel; }
            set
            {
                _levelsToolPanel = value;
                _levelsToolC.ColorToolPanel = value;
            }
        }

        ExposureToolPanel _exposureTP;
        public ExposureToolPanel ExposureToolPanel
        {
            get { return _exposureTP; }
            set
            {
                _exposureTP = value;
                _levelsToolC.ExposureToolPanel = value;
            }
        }        



        GreenScreenToolController _gsToolController;
        GreenScreenToolPanel _gsToolPanel;
        public GreenScreenToolPanel GreenScreenSettingsPanel
        {
            get { return _gsToolPanel; }
            set
            {
                _gsToolPanel = value;
                _gsToolController = new GreenScreenToolController(value);
            }
        }

        CropToolController _cropToolC;
        CropToolPanel _cropToolP;
        public CropToolPanel CropToolPanel
        {
            get { return _cropToolP; }
            set
            {
                _cropToolP = value;
                _cropToolC = new CropToolController(_iPanel, _cropToolP);
                _cropToolC.ToolController = this;
            }
        }

        public ToolController(ImageDisplayPanel iPanel)
        {
            _iPanel = iPanel;
            _iPanel.SizeChanged += new SizeChangedEventHandler(CurrentImagePanel_SizeChanged);
            _levelsToolC = new LevelsToolController(_iPanel);

            //_levelsToolC.ImageDisplayed += new ImageDisplayedEventHandler(_levelsToolC_ImageDisplayed);
            //_iPanel.Image.Loaded += new RoutedEventHandler(Image_Loaded);
            _iPanel.Image.SizeChanged += new SizeChangedEventHandler(Image_SizeChanged);
        }

        void Image_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            _cropToolC.Update();
        }

        //void Image_Loaded(object sender, RoutedEventArgs e)
        //{
        //    _cropToolC.Update();
        //}

        //void _levelsToolC_ImageDisplayed(Flow.Schema.LinqModel.DataContext.Image displayedImage)
        //{
        //    _cropToolC.Update();
        //}

		public void SelectImage(SubjectImage image)
        {
            
            _image = image;
            LoadImageProperties();
            _levelsToolC.Update();
            _cropToolC.InitCrop(image);
            _cropToolC.Update();
        }

        private void LoadImageProperties()
        {
            //_levelsToolC.Red = (double)_image.Red;
            //_levelsToolC.Green = (double)_image.Green;
            //_levelsToolC.Blue = (double)_image.Blue;
            //_levelsToolC.Contrast = (double)_image.Contrast;
            //_levelsToolC.Density = (double)_image.Density;
            _levelsToolC.CurrentImage = _image;
            if (_image.CropH < 0 || double.IsNaN(_image.CropH))
                _image.CropH = 0;
            _cropToolC.CurrentImage = _image;
        }

        void CurrentImagePanel_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (_image != null)
            {
                _cropToolC.SetVisibleCanvasProperties();
            }
        }

        public void ShowMask()
        {
            _cropToolC.ShowMask();
        }
    }
}
