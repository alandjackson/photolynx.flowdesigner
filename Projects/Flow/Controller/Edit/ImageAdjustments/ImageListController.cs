﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

using Flow.Controller.Edit.ImageAdjustments;
using Flow.Schema.LinqModel.DataContext;
using Flow.View.Edit.ImageAdjustments;
using System.Windows.Data;

namespace Flow.Lib
{
    public class ImageListController
    {
        ImageDisplayController CurrentImageController { get; set; }
        public ImageListPanel ImageListPanel { get; set; }

		private FlowObservableCollection<SubjectImage> _projectImageList;
		public FlowObservableCollection<SubjectImage> ProjectImageList
		{
			get { return _projectImageList; }
			set
			{
				_projectImageList = value;
                //_imageList = new AdjustmentImages();
                //foreach (Image image in value)
                //{

                //}
                ImageListPanel.ImageListbox.ItemsSource = _projectImageList;
			}
		}

        public ImageListController(ImageListPanel pnl, ImageDisplayController c)
        {
            ImageListPanel = pnl;
            CurrentImageController = c;
        }

    }
}
