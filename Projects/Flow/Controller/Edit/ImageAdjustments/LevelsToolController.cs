﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Imaging;
using Flow.Lib.ImageUtils;
using Flow.View.Edit.ImageAdjustments;
using System.Windows.Controls;
using System.ComponentModel;
using System.Windows.Threading;
using Flow.Interop.ImageRender;

using Flow.Schema.LinqModel.DataContext;

namespace Flow.Controller.Edit.ImageAdjustments
{
    //public delegate void ImageDisplayedEventHandler(Flow.Schema.LinqModel.DataContext.Image displayedImage);

    public class LevelsToolController
    {
        AdjustImage _currentAdjustmentImage;
        AdjustImage CurrentAdjustImage
        {
            get
            {
                if (_currentAdjustmentImage == null || !_currentAdjustmentImage.ImagePath.Equals(CurrentImage.ImagePath))
                {
                    UpdateAdjustImage();
                }

                return _currentAdjustmentImage;
            }
        }

		public SubjectImage CurrentImage { get; set; }

        ImageDisplayPanel _currentImagePanel;

        //public event ImageDisplayedEventHandler ImageDisplayed;

        LevelsToolPanel _colorToolPanel { get; set; }
        public LevelsToolPanel ColorToolPanel
        {
            get { return _colorToolPanel; }
            set
            {
                _colorToolPanel = value;

                ColorToolPanel.sliderR.ValueChanged += new System.Windows.RoutedPropertyChangedEventHandler<double>(ValueChanged);
                ColorToolPanel.sliderG.ValueChanged += new System.Windows.RoutedPropertyChangedEventHandler<double>(ValueChanged);
                ColorToolPanel.sliderB.ValueChanged += new System.Windows.RoutedPropertyChangedEventHandler<double>(ValueChanged);

            }
        }

        ExposureToolPanel _exposureToolPanel;
        public ExposureToolPanel ExposureToolPanel
        {
            get { return _exposureToolPanel; }
            set
            {
                _exposureToolPanel = value;
                ExposureToolPanel.sliderC.ValueChanged += new System.Windows.RoutedPropertyChangedEventHandler<double>(ValueChanged);
                ExposureToolPanel.sliderD.ValueChanged += new System.Windows.RoutedPropertyChangedEventHandler<double>(ValueChanged);
                ExposureToolPanel.DataContext = this;
            }
        }

        public LevelsToolController(ImageDisplayPanel iPanel)
        {
            _currentImagePanel = iPanel;
        }


        void ValueChanged(object sender, System.Windows.RoutedPropertyChangedEventArgs<double> e)
        {
            AdjustImageLevels();
        }

        void UpdateAdjustImage()
        {
            _currentAdjustmentImage = new AdjustImage((int)CurrentImage.Red, (int)CurrentImage.Green,
                        (int)CurrentImage.Blue, (int)CurrentImage.Density, CurrentImage.Contrast, CurrentImage.ImagePath);
        }

        public void AdjustImageLevels()
        {
            _currentImagePanel.Dispatcher.BeginInvoke((Action)delegate
            {
                _currentImagePanel.Image.Source = ImageLoader.LoadImage(CurrentImage.ImagePath);
            }, DispatcherPriority.Render);

        }

        internal void Update()
        {
            ColorToolPanel.DataContext = null;
            ColorToolPanel.DataContext = CurrentImage;
            ExposureToolPanel.DataContext = null;
            ExposureToolPanel.DataContext = CurrentImage;
            AdjustImageLevels();
        }
    }
}
