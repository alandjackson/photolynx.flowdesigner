﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.View.Edit.ImageAdjustments;
using Flow.Lib;
using Flow.Schema.LinqModel.DataContext;
using Flow.Model;
using System.Data.Linq;
using System.Windows.Forms;
using System.Windows.Controls;
using System.IO;
using System.Windows.Input;
using System.Threading;
using NLog;
using Flow.View.Dialogs;
using System.ComponentModel;

namespace Flow.Controller.Edit.ImageAdjustments
{
    public class ImageAdjustmentsController : INotifyPropertyChanged
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public ImageAdjustmentsPanel ImageAdjustmentsPanel { get; set; }
        public ImageDisplayController ImageDisplayController { get; set; }
        public ImageListController ImageListContoller { get; set; }
        public ToolController ToolController { get; set; }
        public FlowController FlowController { get; set; }
        public bool cDown = false;
        public bool oDown = false;
        public bool ShowCropOverlay {get;set;}

		public SubjectImage CurrentImage { get; set; }


        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        protected void SendPropertyChanged(string propName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propName));
        }
        
        public List<string> AvailableOverlays {
        get{
            List<string> tempList = new List<string>();
            string OverlayDir = this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Edit.OverlaysDirectory;
            //tempList.Add("None");
            foreach (string file in Directory.GetFiles(OverlayDir))
            {
                tempList.Add(new FileInfo(file).Name);

            }
            return tempList;
            }
        }

		public ImageAdjustmentsController(FlowController fc) 
		{
			//Main(new ImageAdjustmentsPanel(), fc);
		}

		public void Main(ImageAdjustmentsPanel pnl, FlowController fc)
        {
            this.FlowController = fc;
            ImageAdjustmentsPanel = pnl;

            ImageAdjustmentsPanel.KeyDown += new System.Windows.Input.KeyEventHandler(pnl_KeyDown);
            ImageAdjustmentsPanel.KeyUp += new System.Windows.Input.KeyEventHandler(pnl_KeyUp);
            ImageAdjustmentsPanel.MouseWheel += new MouseWheelEventHandler(pnl_MouseWheel);

            ImageDisplayController = new ImageDisplayController(ImageAdjustmentsPanel.CurrentImagePanel);

            ImageListContoller = new ImageListController(ImageAdjustmentsPanel.ImageListPanel, ImageDisplayController);
            ImageListContoller.ImageListPanel.ImageListbox.ListBoxSelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(imageListbox_ListBoxSelectionChanged);
            
           


            ToolController = new ToolController(ImageDisplayController.ImageDisplayPanel);
            ToolController.LevelsToolPanel = ImageAdjustmentsPanel.ColorToolPanel;
            ToolController.CropToolPanel = ImageAdjustmentsPanel.CropToolPanel;
            ToolController.ExposureToolPanel = ImageAdjustmentsPanel.ExposureToolPanel;
            ToolController.GreenScreenSettingsPanel = ImageAdjustmentsPanel.GreenScreenSettingsPanel;
            ToolController.ImageAdjustmentsController = this;
            ImageListContoller.ImageListPanel.ImageListbox.SelectedIndex = 0;
            //ImageListContoller.ImageListPanel.ImageListbox.SelectedIndex = 0;
            
            pnl.LostFocus += new System.Windows.RoutedEventHandler(pnl_LostFocus);

            ImageDisplayController.ImageDisplayPanel.buttonNext.Click += new System.Windows.RoutedEventHandler(buttonNext_Click);
            ImageDisplayController.ImageDisplayPanel.buttonPrev.Click += new System.Windows.RoutedEventHandler(buttonPrev_Click);
            ImageDisplayController.ImageDisplayPanel.buttonApplyToAll.Click += new System.Windows.RoutedEventHandler(buttonApplyToAll_Click);
            ImageDisplayController.ImageDisplayPanel.buttonClearSettings.Click += new System.Windows.RoutedEventHandler(buttonClearSettings_Click);
            pnl.DataContext = this;
            pnl.cmbAvailableOverlays.SelectionChanged += new SelectionChangedEventHandler(cmbAvailableOverlays_SelectionChanged);


            ImageDisplayController.ImageDisplayPanel.Image.DataContextChanged +=new System.Windows.DependencyPropertyChangedEventHandler(Image_DataContextChanged);



            ShowCropOverlay = true;
            
        }

        void pnl_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            //logger.Info("pnl_MouseWheel START");

                if (oDown)
                {
                    if (e.Delta > 0)
                    {
                        if (this.ImageAdjustmentsPanel.cmbAvailableOverlays.SelectedIndex > 0)
                            this.ImageAdjustmentsPanel.cmbAvailableOverlays.SelectedIndex--;
                    }
                    else if (e.Delta < 0)
                    {
                        if (this.ImageAdjustmentsPanel.cmbAvailableOverlays.SelectedIndex < this.ImageAdjustmentsPanel.cmbAvailableOverlays.Items.Count)
                            this.ImageAdjustmentsPanel.cmbAvailableOverlays.SelectedIndex++;
                    }
                }

                if (cDown)
                {
                    if (e.Delta > 0)
                    {
                        if (this.ImageAdjustmentsPanel.CropToolPanel.comboBoxCropPresets.SelectedIndex > 0)
                            this.ImageAdjustmentsPanel.CropToolPanel.comboBoxCropPresets.SelectedIndex--;
                    }
                    else if (e.Delta < 0)
                    {
                        if (this.ImageAdjustmentsPanel.CropToolPanel.comboBoxCropPresets.SelectedIndex < this.ImageAdjustmentsPanel.CropToolPanel.comboBoxCropPresets.Items.Count)
                            this.ImageAdjustmentsPanel.CropToolPanel.comboBoxCropPresets.SelectedIndex++;
                    }
                }

                //logger.Info("pnl_MouseWheel END");
        }

        void pnl_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.C)
                this.cDown = false;
            if (e.Key == Key.O)
                this.oDown = false;
        }

        void pnl_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.C)
                this.cDown = true;
            if (e.Key == Key.O)
            {
                this.oDown = true;
                //this.ImageAdjustmentsPanel.cmbAvailableOverlays.Focus();
            }
        }

        
        private void cmbAvailableOverlays_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Edit.CurrentAdjustImageOverlay = (FileInfo)e.AddedItems[0];
             this.FlowController.ProjectController.FlowMasterDataContext.SavePreferences();
             this.FlowController.ProjectController.FlowMasterDataContext.SubmitChanges();
             this.FlowController.ProjectController.FlowMasterDataContext.UpdateOverlayImage();
            //this.ImageAdjustmentsPanel.ImageListPanel.ImageListbox.ImageOverlay = AdjustImageOverlay;
            
        }

        //public FileInfo ImageOverlay
        //{
        //    get
        //    {
        //        return this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Edit.CurrentAdjustImageOverlay;
        //        ////return (this.Subject.FlowProjectDataContext.FlowMasterDataContext.PreferenceManager.Capture.OverlayPngPath);
        //        //string pathToOverlays = this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Layout.OverlaysDirectory;
        //        //if (CurrentAdjustImageOverlay == null || CurrentAdjustImageOverlay.Length < 1 || CurrentAdjustImageOverlay.ToLower() == "none")
        //        //    return null;
        //        //string overlayPath = Path.Combine(pathToOverlays, CurrentAdjustImageOverlay);
        //        //if (File.Exists(overlayPath))
        //        //    return overlayPath;
        //        //else
        //        //    return null;
        //        //return new Uri(this.Subject.FlowProjectDataContext.FlowMasterDataContext.PreferenceManager.Edit.CurrentAdjustImageOverlay);
        //        //return new Uri(this.Subject.FlowProjectDataContext.FlowMasterDataContext.PreferenceManager.Capture.OverlayPngPath);
        //    }
        //}

        void buttonClearSettings_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            logger.Info("Clicked Clear Settings button");

            if (CurrentImage == null) return;

            CurrentImage.Red = 0;
            CurrentImage.Green = 0;
            CurrentImage.Blue = 0;
            CurrentImage.CropW = 0;
            CurrentImage.CropL = 0;
            CurrentImage.CropT = 0;
            CurrentImage.CropH = 0;
            CurrentImage.Density = 0;
            CurrentImage.Contrast = 0;
            ToolController.SelectImage(CurrentImage);
            CurrentImage.ExistsInOnlineGallery = false;
        }

        void buttonApplyToAll_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            logger.Info("Clicked Apply to All button");

            FlowMessageBox msg = new FlowMessageBox("Apply Crop To All", "You are about to apply this crop to " + ImageListContoller.ProjectImageList.Count + " Images.");
            if(msg.ShowDialog() == false) return;


			foreach (SubjectImage image in ImageListContoller.ProjectImageList.Reverse())
            {
                image.Subject.FlagForMerge = true;
                if (CurrentImage != image)
                {
                    if (CurrentImage.CropH < 0 || double.IsNaN(CurrentImage.CropH))
                        CurrentImage.CropH = 0;

                    image.Blue = CurrentImage.Blue;
                    image.Contrast = CurrentImage.Contrast;
                    image.CropH = CurrentImage.CropH;
                    image.CropL = CurrentImage.CropL;
                    image.CropT = CurrentImage.CropT;
                    image.CropW = CurrentImage.CropW;
                    image.Density = CurrentImage.Density;
                    image.Green = CurrentImage.Green;
                    image.Red = CurrentImage.Red;
                    image.Sharpen = CurrentImage.Sharpen;
                }
                image.RefreshImage();
                image.ExistsInOnlineGallery = false;
            }

            if (FlowController.ProjectController.CurrentFlowProject.FlowProjectDataContext.GetChangeSet().Updates.Count > 0)
                FlowController.ProjectController.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();
        }

        void buttonPrev_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            logger.Info("Clicked Previus button");

            if (ImageListContoller.ImageListPanel.ImageListbox.SelectedIndex > 0)
            {
                ImageListContoller.ImageListPanel.ImageListbox.SelectedIndex--;
            }
        }

        void buttonNext_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            logger.Info("Clicked Next button");

            if (ImageListContoller.ImageListPanel.ImageListbox.SelectedIndex < ImageListContoller.ProjectImageList.Count - 1)
            {
                ImageListContoller.ImageListPanel.ImageListbox.SelectedIndex++;
                //ImageListContoller.ProjectImageList.MoveCurrentToNext();
            }

        }


        void Image_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e)
        {

                int x = 1;
                //this.ImageAdjustmentsPanel.CurrentImagePanel.Image.R
            
        }

        void imageListbox_ListBoxSelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (e.RemovedItems.Count > 0)
                SaveChanges();
            if (e.AddedItems.Count > 0)
            {
                //AdjustmentImage image = (AdjustmentImage)e.AddedItems[0];
				SelectImage(e.AddedItems[0] as SubjectImage);
            }
        }

        void pnl_LostFocus(object sender, System.Windows.RoutedEventArgs e)
        {
            SaveChanges();
        }

        //void ImageListbox_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        //{
        //    if (e.RemovedItems.Count > 0)
        //        SaveChanges();
        //    if (e.AddedItems.Count > 0)
        //    {
        //        AdjustmentImage image = (AdjustmentImage)e.AddedItems[0];
        //        SelectImage(image.Image);
        //    }
        //}

		public void SelectImage(SubjectImage image)
        {
            CurrentImage = image;
            ToolController.SelectImage(CurrentImage);
            this.SendPropertyChanged("CurrentImage");
           
        }

        void SaveChanges()
        {
            try
            {
                FlowController.ProjectController.CurrentFlowProject.FlowProjectDataContext.SubmitChanges(ConflictMode.ContinueOnConflict);
            }
            catch(Exception e)
            {
                Thread.Sleep(500);
                try
                {
                    FlowController.ProjectController.CurrentFlowProject.FlowProjectDataContext.ChangeConflicts.ResolveAll(RefreshMode.KeepCurrentValues);
                    FlowController.ProjectController.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();
                }
                catch(Exception ex)
                {
                    throw (ex);
                }
            }
        }
    }
}
