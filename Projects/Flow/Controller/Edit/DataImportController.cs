﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Objects;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

using Flow.Controller.Capture;
using Flow.Lib;
using Flow.Lib.Helpers;
using Flow.Schema;
using Flow.Schema.LinqModel.DataContext;
using Flow.View;
using Flow.View.Edit;

namespace Flow.Controller
{
	public class DataImportController
	{
		DataImportPanel DataImportPanel { get; set; }

		private FlowController _flowController = null;
		internal FlowController FlowController
		{
			get { return _flowController; }
			set
			{
				_flowController = value;
				DataImportPanel.FlowController = _flowController;
			}
		}

		public DataImportController()
		{
		}

		public void Main(DataImportPanel pnl, FlowController flowController)
		{
			this.DataImportPanel = pnl;
			this.FlowController = flowController;
		}
	}
}
