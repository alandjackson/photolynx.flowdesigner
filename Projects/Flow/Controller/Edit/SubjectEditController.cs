﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Flow.Schema.LinqModel.DataContext;
using Flow.View.Edit;

namespace Flow.Controller.Edit
{
	public class SubjectEditController
	{
		SubjectEditPanel SubjectEditPanel { get; set; }

		public void Main(SubjectEditPanel pnl)
		{
			SubjectEditPanel = pnl;
		}
	}
}

