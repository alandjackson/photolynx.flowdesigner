﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;

using Flow.Designer.Lib.Service;
using Flow.Designer.Model;
using Flow.Designer.Model.Settings;
using Flow.Lib;
using Flow.Lib.Helpers;
using Flow.Lib.Helpers.BarcodeScan;
using Flow.Lib.Persister;
using Flow.Lib.UnitOfWork;
using Flow.Schema.LinqModel.DataContext;
using Flow.View.Edit;
using StructureMap;

using Flow.Controller.Catalog;
using NLog;

namespace Flow.Controller.Edit
{
	public class EditController
	{
		EditDockPanel EditDockPanel { get; set; }

		OrganizationEditController OrganizationEditController { get; set; }
		EditContentController EditContentController { get; set; }

		internal FlowController FlowController { get; private set; }

		public BarcodeScanDetector BarcodeScanDetector { get; private set; }

        private static Logger logger = LogManager.GetCurrentClassLogger();

		public EditController(
			OrganizationEditController oc,
			EditContentController ecc
		)
		{
			OrganizationEditController = oc;
			EditContentController = ecc;
		}

		public void Main(EditDockPanel pnl, FlowController flowController)
		{
			this.FlowController = flowController;
	
			// Assign the currently open Flow project to the EditContentController
			this.EditContentController.ProjectController = flowController.ProjectController;

			flowController.ProjectController.PropertyChanged +=
				new PropertyChangedEventHandler(this.EditContentController.ProjectController_PropertyChanged);
	
			EditDockPanel = pnl;
			EditDockPanel.EditController = this;

            

			this.EditContentController.Main(this.EditDockPanel.EditContentPanel);

			this.EditDockPanel.EditContentPanel.ImportDataButton.Click += new System.Windows.RoutedEventHandler(ImportDataButton_Click);

			this.EditDockPanel.EditContentPanel.SubjectListEditPanel.SubjectImageList.OpenAdjustmentsInvoked += new EventHandler<EventArgs<SubjectImage>>(SubjectImageList_OpenAdjustmentsInvoked);
			this.EditDockPanel.EditContentPanel.SubjectEditPanel.SubjectImageList.OpenAdjustmentsInvoked += new EventHandler<EventArgs<SubjectImage>>(SubjectImageList_OpenAdjustmentsInvoked);

            this.EditDockPanel.EditContentPanel.buttonAdjustImagesCurrentView.Click += new System.Windows.RoutedEventHandler(buttonAdjustImagesCurrentView_Click);

			this.EditDockPanel.EditContentPanel.SubjectListEditPanel.OrderEntryInvoked += delegate
			{
				this.DisplayOrderEntry();
			};

			this.EditDockPanel.EditContentPanel.SubjectEditPanel.OrderEntryInvoked += delegate
			{
				this.DisplayOrderEntry();
			};

            CheckPermissions(EditDockPanel);

			
			this.BarcodeScanDetector = new BarcodeScanDetector(
                flowController.ProjectController.CurrentFlowProject.BarcodeScanInitSequence,
                flowController.ProjectController.CurrentFlowProject.BarcodeScanTermSequence,
                flowController.ProjectController.CurrentFlowProject.BarcodeScanValueLength
			);
            this.EditContentController.EditOrderEntryPanel.OrderEntryPanel.CloseOrderEntryRequested += new EventHandler<RoutedEventArgs>(OrderEntryPanel_CloseOrderEntryRequested);
		}

        void OrderEntryPanel_CloseOrderEntryRequested(object sender, RoutedEventArgs e)
        {
            this.EditDockPanel.EditContentPanel.EditContentTabControl.SelectedIndex = 1;
            logger.Info("Closed Order Entry");
        }

        

        private void CheckPermissions(EditDockPanel edp)
        {
            if (!this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Permissions.CanModifySubjectData ||
                !this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Permissions.CanAddSubjects ||
                !this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Permissions.CanDeleteSubjects)
                edp.EditContentPanel.SubjectListEditPanel.SubjectListDataGrid.ReadOnly = true;
            else
                edp.EditContentPanel.SubjectListEditPanel.SubjectListDataGrid.ReadOnly = true;

            
            //if (this.FlowMasterDataContext.PreferenceManager.Permissions.CanAddSubjects)
            //    ((EditContentPanel)sender).SubjectListEditPanel.SubjectListDataGrid
            //else
            //    ((EditContentPanel)sender).CollectionNavigator.AddItemButtonVisibility = Visibility.Hidden;

            //if (this.FlowMasterDataContext.PreferenceManager.Permissions.CanDeleteSubjects)
            //    ((EditContentPanel)sender).CollectionNavigator.DeleteItemButtonVisibility = Visibility.Visible;
            //else
            //    ((EditContentPanel)sender).CollectionNavigator.DeleteItemButtonVisibility = Visibility.Hidden;
        }

        void buttonAdjustImagesCurrentView_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            logger.Info("Clicked Adjust Images Current View button");
            FlowController.ImageAdjustmentsCurrentView();
        }

		void SubjectImageList_OpenAdjustmentsInvoked(object sender, Flow.Lib.Helpers.EventArgs<SubjectImage> e)
		{
			this.FlowController.ImageAdjustmentsCurrentRecord(e.Data);
		}

		void ImportDataButton_Click(object sender, System.Windows.RoutedEventArgs e)
		{
            logger.Info("Clicked Import Data button");
			this.FlowController.DataImport();
		}

		internal void DisplayOrderEntry()
		{
			if (this.FlowController.ProjectController.CurrentFlowProject.HasSubjects)
			{
				// Toggle visibility of the OrderEntryPanel based on the current visibility
				if (this.EditDockPanel.EditContentPanel.OrderEntryTabItem.Visibility == Visibility.Collapsed)
					this.EditDockPanel.EditContentPanel.OrderEntryTabItem.Visibility = Visibility.Visible;

				this.EditDockPanel.EditContentPanel.EditContentTabControl.SelectedIndex = 4;
			}
		}

		/// <summary>
		/// Detects if the input text matches the pattern of the barcode scan key,
		/// and invokes a barcode lookup against the SubjectList if so
		/// </summary>
		/// <param name="scanText"></param>
		/// <param name="endsWithCarriageReturn"></param>
		internal bool DetectScan(string scanText, bool endsWithCarriageReturn)
		{
			// Check the pattern of the input text and store the embedded data
			// if the text matches the barcode scan pattern
			string scanResult = this.BarcodeScanDetector.DetectScan(scanText);

			bool scanInvoked = false;

			if (!string.IsNullOrEmpty(scanResult))
			{
				// Invoke the barcode lookup if a scan was detected
				this.EditContentController.InvokeScan(scanResult, endsWithCarriageReturn);

				scanInvoked = true;
			}

			return scanInvoked;
		}



        internal void SetSearchString(string p)
        {
            //this.EditDockPanel.EditContentPanel.CollectionNavigator.SearchTextBox.Text = p;
        }


        internal void ClearDataGridColumns()
        {
            this.EditContentController.EditContentPanel.SubjectListEditPanel.SubjectListDataGrid.Columns.Clear();
        }

        public void SetFocus()
        {
            if (this.EditDockPanel.EditContentPanel.OrderEntryTabItem.IsEnabled)
                this.EditDockPanel.EditContentPanel.EditOrderEntryPanel.Focus();
            //this.EditDockPanel.EditContentPanel.CollectionNavigator.SearchTextBox.Focus();
        }

    }
}
