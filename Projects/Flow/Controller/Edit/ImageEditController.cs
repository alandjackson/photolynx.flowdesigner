﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Flow.View.Edit;

namespace Flow.Controller.Edit
{
	public class ImageEditController
	{
		ImageEditPanel ImageEditPanel { get; set; }

		public void Main(ImageEditPanel pnl)
		{
			ImageEditPanel = pnl;
		}
	}
}
