﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Flow.View.Edit;

namespace Flow.Controller.Edit
{
	public class OrganizationEditController
	{
		OrganizationEditPanel OrganizationEditPanel { get; set; }

		public void Main(OrganizationEditPanel pnl)
		{
			OrganizationEditPanel = pnl;
		}
	}
}
