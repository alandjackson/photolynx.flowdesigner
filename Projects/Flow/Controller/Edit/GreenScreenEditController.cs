﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.View.Edit;
using System.Windows;
using Flow.Controller.Project;

namespace Flow.Controller.Edit
{
    public class GreenScreenEditController
    {
        public ProjectController ProjectController { get; set; }

        public void Initialize(GreenScreenEditPanel panel)
        {
            panel.DataContextChanged += panel_DataContextChanged;

            CPIGreenScreen.Util.CPISettings.Instance.BackgroundFolder =
                ProjectController.FlowMasterDataContext.PreferenceManager.Edit.BackgroundsDirectory;
            panel.InitializeGreenScreenPanel();
        }

        void panel_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
        }
    }
}
