﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;

using Flow.Controls.DataDisplay;
using Flow.Controls.ImageDisplay;
using Flow.Controls.View.DataDisplay;
using Flow.Controls.View.RecordDisplay;
using Flow.Controller.Project;
using Flow.Lib.Helpers;
using Flow.Schema.LinqModel.DataContext;
using Flow.View.Catalog;
using Flow.View.Edit;

using Xceed.Wpf.DataGrid;

using Flow.Controller.Catalog;
using System.Diagnostics;
using System.Windows.Controls;
using System.Windows.Data;
using Xceed.Wpf.Controls;
using Flow.View.Dialogs;
using Flow.GreenScreen.Data;
using NLog;

namespace Flow.Controller.Edit
{
	public class EditContentController
	{
		public event EventHandler<EventArgs<Subject>> CurrentSubjectChanged = delegate { };

		public ProjectController ProjectController { get; set; }
		public OrderEntryController OrderEntryController { get; set; }
        public EditContentPanel EditContentPanel { get; set; }
		
		public FlowProject CurrentFlowProject 
        { 
            get { return (ProjectController == null) ? null : ProjectController.CurrentFlowProject; } 
        }
		CollectionNavigator CollectionNavigator { get { return EditContentPanel.CollectionNavigator; } }
		SubjectListEditPanel SubjectListEditPanel { get { return EditContentPanel.SubjectListEditPanel; } }
		SubjectEditPanel SubjectEditPanel { get { return EditContentPanel.SubjectEditPanel; } }
		ImageEditPanel ImageEditPanel { get { return EditContentPanel.ImageEditPanel; } }
		public EditOrderEntryPanel EditOrderEntryPanel { get { return EditContentPanel.EditOrderEntryPanel; } }
		DataGridControl SubjectListDataGrid { get { return SubjectListEditPanel.SubjectListDataGrid; } }
        public GreenScreenEditController GreenScreenController { get; set; }

        private static Logger logger = LogManager.GetCurrentClassLogger();

        private string _currentImageFilter = "No Filter";
        private DateTime? _imageFilterBeginDate;
        private DateTime? _imageFilterEndDate;
        private string _imageFilterImageSource;

        public void ApplyFilter()
        {
            ApplyImageFilter();
        }

		public EditContentController(OrderEntryController oec)
		{
			OrderEntryController = oec;
            oec.EditContentController = this;
            GreenScreenController = new GreenScreenEditController();
		}

      
		public void Main(EditContentPanel pnl)
		{
			EditContentPanel = pnl;

			SetDependencies();
            SetEventHandlers();
            pnl.EditContentController = this;
			OrderEntryController.ProjectController = ProjectController;
			EditOrderEntryPanel.OrderEntryController = OrderEntryController;
            SubjectListEditPanel.Loaded += new RoutedEventHandler(SubjectListEditPanel_Loaded);

            GreenScreenController.ProjectController = ProjectController;
            GreenScreenController.Initialize(pnl.GreenScreenEditPanel);

            this.SubjectListEditPanel.ApplyFieldFilter += new EventHandler<EventArgs<ProjectSubjectField>>(SubjectListEditPanel_ApplyFieldFilter);
           
		}



        void SubjectListEditPanel_ApplyFieldFilter(object sender, EventArgs<ProjectSubjectField> e)
        {
            SubjectSearchResult results;
            results = CurrentFlowProject.SubjectList.ApplyFieldFilter(e.Data);
        }

        void SubjectListEditPanel_Loaded(object sender, RoutedEventArgs e)
        {

            updateVisibleColumns();
        }

        public void updateVisibleColumns()
        {
            try
            {
                if (this.ProjectController.CurrentFlowProject == null) return;

                this.ProjectController.CurrentFlowProject.FlowProjectDataContext.SubmitChangesLocal();

                foreach (ProjectSubjectField psf in this.ProjectController.CurrentFlowProject.FlowProjectDataContext.ProjectSubjectFields)
                {
                    if (psf.IsShownInEdit)
                    {
                        SubjectListDataGrid.Columns[psf.ProjectSubjectFieldDisplayName].Visible = true;
                    }
                    else
                        SubjectListDataGrid.Columns[psf.ProjectSubjectFieldDisplayName].Visible = false;
                }
                this.SubjectListEditPanel.SubjectListDataGrid.UpdateLayout();
            }
            catch
            {
                //do nothing, if this failed, no harm in ignoring it.
            }
        }

        protected void SetEventHandlers()
        {
            CollectionNavigator.EditStateChanged +=
                new EventHandler<EditStateChangedEventArgs>(CollectionNavigator_EditStateChanged);

            EditContentPanel.ConfirmDeleteButton.Click +=
                new RoutedEventHandler(ConfirmDeleteButton_Click);
            EditContentPanel.CancelDeleteButton.Click +=
                new RoutedEventHandler(CancelDeleteButton_Click);

            EditContentPanel.SortInvoked +=
                new EventHandler<EventArgs<SortDescriptionCollection>>(EditContentPanel_SortInvoked);
            EditContentPanel.CollectionNavigator.SearchInvoked +=
                new EventHandler<SearchInvokedEventArgs>(CollectionNavigator_SearchInvoked);

           // EditContentPanel.ImageFilter.ImageFilterChanged +=
           //     new EventHandler<SelectionChangedEventArgs>(ImageFilter_Changed);

           // EditContentPanel.ImageFilter.ImageFilterBeginDateChanged +=
           //     new EventHandler<EventArgs>(FilterBeginDate_Changed);
           // EditContentPanel.ImageFilter.ImageFilterEndDateChanged +=
           //     new EventHandler<EventArgs>(FilterEndDate_Changed);
          //  EditContentPanel.ImageFilter.ImageSourceChanged += new EventHandler<EventArgs>(ImageFilter_ImageSourceChanged);

            EditContentPanel.SubjectEditPanel.SubjectImageList.UnassignImageInvoked +=
                new EventHandler<EventArgs<SubjectImage>>(SubjectImageList_UnassignImageInvoked);
            EditContentPanel.SubjectListEditPanel.SubjectImageList.UnassignImageInvoked +=
                new EventHandler<EventArgs<SubjectImage>>(SubjectImageList_UnassignImageInvoked);

            EditContentPanel.SubjectEditPanel.SubjectImageList.DeleteImageInvoked +=
                new EventHandler<EventArgs<SubjectImage>>(SubjectImageList_DeleteImageInvoked);
            SubjectListEditPanel.SubjectImageList.DeleteImageInvoked +=
                new EventHandler<EventArgs<SubjectImage>>(SubjectImageList_DeleteImageInvoked);

            EditContentPanel.SubjectEditPanel.SubjectImageList.OpenExternalEditorInvoked +=
                new EventHandler<EventArgs<SubjectImage>>(SubjectImageList_OpenExternalEditorInvoked);
            EditContentPanel.SubjectListEditPanel.SubjectImageList.OpenExternalEditorInvoked +=
                new EventHandler<EventArgs<SubjectImage>>(SubjectImageList_OpenExternalEditorInvoked);

            EditContentPanel.SubjectEditPanel.SubjectImageList.ToggleGreenscreenInvoked +=
               new EventHandler<EventArgs<SubjectImage>>(SubjectImageList_ToggleGreenscreenInvoked);
            EditContentPanel.SubjectListEditPanel.SubjectImageList.ToggleGreenscreenInvoked +=
                new EventHandler<EventArgs<SubjectImage>>(SubjectImageList_ToggleGreenscreenInvoked);

            EditContentPanel.SubjectEditPanel.SubjectImageList.ToggleFlagImageInvoked +=
               new EventHandler<EventArgs<SubjectImage>>(SubjectImageList_ToggleFlagImageInvoked);
            EditContentPanel.SubjectListEditPanel.SubjectImageList.ToggleFlagImageInvoked +=
                new EventHandler<EventArgs<SubjectImage>>(SubjectImageList_ToggleFlagImageInvoked);

            EditContentPanel.SubjectEditPanel.SubjectImageList.ToggleRemoteImageServiceInvoked +=
              new EventHandler<EventArgs<SubjectImage>>(SubjectImageList_ToggleRemoteImageServiceInvoked);
            EditContentPanel.SubjectListEditPanel.SubjectImageList.ToggleRemoteImageServiceInvoked +=
                new EventHandler<EventArgs<SubjectImage>>(SubjectImageList_ToggleRemoteImageServiceInvoked);

            EditContentPanel.SubjectEditPanel.SubjectImageList.RefreshImageInvoked +=
                new EventHandler<EventArgs<SubjectImage>>(SubjectImageList_RefreshImageInvoked);
            EditContentPanel.SubjectListEditPanel.SubjectImageList.RefreshImageInvoked +=
                new EventHandler<EventArgs<SubjectImage>>(SubjectImageList_RefreshImageInvoked);

            SubjectListEditPanel.DataGridSortInvoked +=
                new EventHandler<EventArgs<SortDescriptionCollection>>(SubjectListEditPanel_DataGridSortInvoked);

            SubjectListEditPanel.UpdateVisibleColumns += new EventHandler<RoutedEventArgs>(SubjectListEditPanel_UpdateVisibleColumns);
            CurrentFlowProject.SubjectList.SortCompleted +=
                new EventHandler(SubjectListEditPanel.OnSortCompleted);
        }

        void ImageFilter_ImageSourceChanged(object sender, EventArgs e)
        {
            if (this.CurrentFlowProject != null && this.CurrentFlowProject.FlowMasterDataContext != null)
            {
                //this.CurrentFlowProject.FlowMasterDataContext.SavePreferences();
                if (((ComboBox)sender).SelectedValue != null)
                {
                    _imageFilterImageSource = (String)((ComboBox)sender).SelectedValue;
                }
                ApplyImageFilter();
            }
        }

        void SubjectListEditPanel_UpdateVisibleColumns(object sender, RoutedEventArgs e)
        {
            this.updateVisibleColumns();
        }

		internal void ProjectController_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
            if (e.PropertyName == "CurrentFlowProject")
            {
                SetDependencies();
                if (SubjectListDataGrid.Columns.Count > 1)
                    this.updateVisibleColumns();
            }
            
		}

		private void SetDependencies()
		{
            if(CollectionNavigator != null)
			    CollectionNavigator.EditState = EditState.View;

            if(EditContentPanel !=null)
			    EditContentPanel.DataContext = CurrentFlowProject;

            if(CurrentFlowProject != null && !CurrentFlowProject.IsNew && CurrentFlowProject.SubjectList != null)
				CurrentFlowProject.SubjectList.RemoveDataGridItemProperty("Formal Name");

            
		}

		void CollectionNavigator_EditStateChanged(object sender, EditStateChangedEventArgs e)
		{
			switch (e.OldEditState)
			{
				case EditState.View:

					switch (e.NewEditState)
					{
						case EditState.Add:
							CurrentFlowProject.SubjectList.ClearFilter();

							CurrentFlowProject.SubjectList.CreateNew();
							break;

						case EditState.Delete:
							EditContentPanel.DeleteConfirmationPrompt.Visibility = Visibility.Visible;
							break;
					}
					break;


				case EditState.Add:

					switch (e.NewEditState)
					{
						case EditState.Save:

							// Flag if the project has no subject records
							bool emptySubjectList = !CurrentFlowProject.HasSubjects;
                            CurrentFlowProject.SubjectList.CurrentItem.TicketCode = TicketGenerator.GenerateTicketString(8);
							CurrentFlowProject.SubjectList.SaveCurrent();

							// If this is the first subject record, invoke SetDependencies to avoid the "zero subjects" synch issue
							if (emptySubjectList)
								SetDependencies();

							CollectionNavigator.EditState = EditState.View;
							break;

						case EditState.Cancel:
							CurrentFlowProject.SubjectList.CancelNew();
							CollectionNavigator.EditState = EditState.View;
							break;
					}
					break;

				case EditState.Edit:
					switch (e.NewEditState)
					{
						case EditState.Save:
							CurrentFlowProject.SubjectList.SaveCurrent();
							CollectionNavigator.EditState = EditState.View;
							break;

						case EditState.Cancel:
							CurrentFlowProject.SubjectList.RevertCurrent();
							CollectionNavigator.EditState = EditState.View;
							break;
					}
					break;

				case EditState.Delete:
					EditContentPanel.DeleteConfirmationPrompt.Visibility = Visibility.Collapsed;
					break;

			}
		}


		void ConfirmDeleteButton_Click(object sender, RoutedEventArgs e)
		{
            logger.Info("Clicked Confirm Delete button");

            IEnumerable<SubjectTicket> tickets = this.CurrentFlowProject.FlowProjectDataContext.SubjectTickets.Where(t => t.SubjectID == this.CurrentFlowProject.SubjectList.CurrentItem.SubjectID);
            if (tickets.Count() > 0)
            {
                foreach (SubjectTicket ticket in tickets)
                {
                    this.CurrentFlowProject.FlowProjectDataContext.SubjectTickets.DeleteOnSubmit(ticket);

                }
                this.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();
            }
			CurrentFlowProject.SubjectList.DeleteCurrent();
			CollectionNavigator.EditState = EditState.View;
		}

		void CancelDeleteButton_Click(object sender, RoutedEventArgs e)
		{
            logger.Info("Clicked Cancel Delete button");
			CollectionNavigator.EditState = EditState.View;
		}

		void EditContentPanel_SortInvoked(object sender, EventArgs<SortDescriptionCollection> e)
		{
			CurrentFlowProject.SubjectList.Sort(e.Data, false);
		}

		void SubjectListEditPanel_DataGridSortInvoked(object sender, EventArgs<SortDescriptionCollection> e)
		{
			CurrentFlowProject.SubjectList.Sort(e.Data, true);
		}

		void FilterOnImagesCheckbox_CheckedChanged(object sender, RoutedEventArgs e)
		{
		}

        void FilterBeginDate_Changed(object sender, EventArgs e)
        {
            return;
            //this.CurrentFlowProject.FlowMasterDataContext.PreferenceManager.Edit.ImageFilterBeginDate = (DateTime)((DatePicker)sender).SelectedDate;
            if (this.CurrentFlowProject != null && this.CurrentFlowProject.FlowMasterDataContext != null)
            {
                this.CurrentFlowProject.FlowMasterDataContext.SavePreferences();
                if (((Xceed.Wpf.Controls.DatePicker)sender).SelectedDate != null)
                {
                    _imageFilterBeginDate = ((Xceed.Wpf.Controls.DatePicker)sender).SelectedDate;
                }
                ApplyImageFilter();
            }
            
        }
        void FilterEndDate_Changed(object sender, EventArgs e)
        {
            return;
            //this.CurrentFlowProject.FlowMasterDataContext.PreferenceManager.Edit.ImageFilterEndDate = (DateTime)((DatePicker)sender).SelectedDate;
            if (this.CurrentFlowProject != null && this.CurrentFlowProject.FlowMasterDataContext != null)
            {
                this.CurrentFlowProject.FlowMasterDataContext.SavePreferences();
                _imageFilterEndDate = ((Xceed.Wpf.Controls.DatePicker)sender).SelectedDate;
                ApplyImageFilter();
            }
           
        }

        void ImageFilter_Changed(object sender, SelectionChangedEventArgs e)
		{
            return;
           // this.CurrentFlowProject.FlowMasterDataContext.PreferenceManager.Edit.ImageFilterIndex = ((ComboBox)e.Source).SelectedIndex;
            if (this.CurrentFlowProject != null && this.CurrentFlowProject.FlowMasterDataContext != null)
            {
                this.CurrentFlowProject.FlowMasterDataContext.SavePreferences();
                _currentImageFilter = ((ComboBoxItem)((ComboBox)e.Source).SelectedItem).Content.ToString();
                ApplyImageFilter();
            }
            
		}

        public void ApplyMissingImageFilter()
        {
            CurrentFlowProject.SubjectList.ApplyMissingImageFilter();
        }
        void ApplyImageFilter()
        {
            return;
            SubjectSearchResult results;
            switch (_currentImageFilter)
            {
                case "No Filter":
                    results = CurrentFlowProject.SubjectList.ApplyFilter(false, false, null, null, _imageFilterImageSource);
                    break;
                case "With Images":
                    results = CurrentFlowProject.SubjectList.ApplyFilter(false, true, null, null, _imageFilterImageSource);
                    break;
                case "Without Images":
                    results = CurrentFlowProject.SubjectList.ApplyFilter(true, false, null, null, _imageFilterImageSource);
                    break;
                case "Before Date":
                    results = CurrentFlowProject.SubjectList.ApplyFilter(false, true, null, _imageFilterEndDate, _imageFilterImageSource);
                    break;
                case "After Date":
                    results = CurrentFlowProject.SubjectList.ApplyFilter(false, true, _imageFilterBeginDate, null, _imageFilterImageSource);
                    break;
                case "Between Dates":
                    results = CurrentFlowProject.SubjectList.ApplyFilter(false, true, _imageFilterBeginDate, _imageFilterEndDate, _imageFilterImageSource);
                    break;
            }

        }

        

		void SubjectImageList_UnassignImageInvoked(object sender, EventArgs<SubjectImage> e)
		{

            if ((((SubjectImage)e.Data).OrderProductNodes.Count > 0)
                && ProjectController.CurrentFlowProject.SubjectList.CurrentItem.SubjectOrder != null
                && !ProjectController.CurrentFlowProject.SubjectList.CurrentItem.SubjectOrder.IsMarkedForDeletion
                && ProjectController.CurrentFlowProject.SubjectList.CurrentItem.SubjectOrder.OrderPackages.Count > 0)
                {
                    FlowMessageBox msg = new FlowMessageBox("Remove Image", "You may not remove an image when there is still an order attached to the image.\nYou must first delete the order");
                    msg.CancelButtonVisible = false;
                    msg.ShowDialog();
                    return;
                }

            int loopCnt = 0;
            while (((SubjectImage)e.Data).OrderImageOptions.Count > 0)
            {
                ((SubjectImage)e.Data).OrderImageOptions.Clear();
                loopCnt++;
                if (loopCnt > 20)//avoid an endless loop
                    break;
            }

            Subject preFilterSubject = ProjectController.CurrentFlowProject.SubjectList.CurrentItem;
            int preFilterSubjectIndex = ProjectController.CurrentFlowProject.SubjectList.DataGridCollectionView.CurrentPosition;

			SubjectImage image = e.Data;

			FileInfo imageFile = new FileInfo(image.ImagePath);

			CurrentFlowProject.SubjectList.CurrentItem.DetachImage(image.ImageFileName);

            //only move it to hot folder if its not a group image
            if (!image.IsGroupImage)
            {
                string newImageFilePath = Path.Combine(
                    ProjectController.FlowMasterDataContext.PreferenceManager.Capture.HotFolderDirectory,
                    image.ImageOriginalFileName
                );
                string sourceFile = Path.Combine(this.ProjectController.CurrentFlowProject.ImageDirPath, image.ImageFileName);
                this.ProjectController.MoveFile(sourceFile, newImageFilePath);
            }
            ApplyFilter();

            if (ProjectController.CurrentFlowProject.SubjectList.DataGridCollectionView.Contains(preFilterSubject))
                ProjectController.CurrentFlowProject.SubjectList.CurrentItem = preFilterSubject;
            else
            {
                if (preFilterSubjectIndex >= ProjectController.CurrentFlowProject.SubjectList.DataGridCollectionView.Count)
                    preFilterSubjectIndex--;
                if (preFilterSubjectIndex < 0)
                    preFilterSubjectIndex = 0;
                if (preFilterSubjectIndex >= 0)
                {
                    try
                    {
                        ProjectController.CurrentFlowProject.SubjectList.DataGridCollectionView.MoveCurrentToPosition(preFilterSubjectIndex);
                    }
                    catch
                    {
                        //ignore for now
                    }
                }
            }
		}

        void SubjectImageList_OpenExternalEditorInvoked(object sender, EventArgs<SubjectImage> e)
        {
            SubjectImage image = e.Data;

            FileInfo imageFile = new FileInfo(image.ImagePathFullRes);
           
            String externalEditor = ProjectController.FlowMasterDataContext.PreferenceManager.Edit.ExternalEditorPath;
            if ((externalEditor != null && File.Exists(externalEditor)) && (imageFile.FullName != null && File.Exists(imageFile.FullName)))
            {
                ProcessStartInfo procInfo = new ProcessStartInfo(externalEditor, imageFile.FullName);
                Process.Start(procInfo);
            }
            else
            {
                MessageBox.Show("An External Editor has not been specified in Preferences->Edit");
            }


            //Delete server thumbnail
            if (image.ImagePath.Contains("\\thumbnails\\") && image.ImagePath != image.ImagePathFullRes)
            {
                File.Delete(image.ImagePath);
            }

            //Delete Greenscreen Cache for subject
            image.Subject.ClearGSCache(Path.Combine(this.CurrentFlowProject.ImageDirPath, "PLCache", "GreenScreen"));
        }

        void SubjectImageList_ToggleFlagImageInvoked(object sender, EventArgs<SubjectImage> e)
        {
            SubjectImage image = e.Data;
            if (image.ImageFlagged == true)
                image.ImageFlagged = false;
            else
                image.ImageFlagged = true;
            this.ProjectController.CurrentFlowProject.FlowProjectDataContext.UpdateHoldImageList();
        }

        void SubjectImageList_ToggleRemoteImageServiceInvoked(object sender, EventArgs<SubjectImage> e)
        {
            SubjectImage image = e.Data;
            if (image.RemoteImageService == true && (image.RemoteImageServiceStatus == null || image.RemoteImageServiceStatus == "new"))
                image.RemoteImageService = false;
            else
                image.RemoteImageService = true;

            //this.ProjectController.CurrentFlowProject.FlowProjectDataContext.UpdateHoldImageList();
        }

        void SubjectImageList_ToggleGreenscreenInvoked(object sender, EventArgs<SubjectImage> e)
        {
            SubjectImage image = e.Data;

            if (string.IsNullOrEmpty(image.GreenScreenSettings))
                image.GreenScreenSettings = this.ProjectController.CurrentFlowProject.DefaultGSxml;
            else
                image.GreenScreenSettings = null;

            image.Subject.FlagForMerge = true;
            image.UpdateGS();

            if (image.IsGroupImage)
            {
                foreach (SubjectImage si in image.GroupImage.GetSubjectImageList)
                {
                    si.GreenScreenSettings = image.GreenScreenSettings;
                    si.Subject.FlagForMerge = true;
                }
            }

            this.ProjectController.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();
        }

        void SubjectImageList_RefreshImageInvoked(object sender, EventArgs<SubjectImage> e)
        {
            SubjectImage image = e.Data;
            image.FlowImage.Refresh();
            image.RefreshImage();
            if (image.IsGreenscreen)
                image.Subject.ClearGSCache(Path.Combine(this.CurrentFlowProject.ImageDirPath, "PLCache", "GreenScreen"));
        }

		void SubjectImageList_DeleteImageInvoked(object sender, EventArgs<SubjectImage> e)
		{
            if ((((SubjectImage)e.Data).OrderProductNodes.Count > 0) && !ProjectController.CurrentFlowProject.SubjectList.CurrentItem.SubjectOrder.IsMarkedForDeletion && ProjectController.CurrentFlowProject.SubjectList.CurrentItem.SubjectOrder.OrderPackages.Count > 0)
            {
                FlowMessageBox msg = new FlowMessageBox("Remove Image", "You may not remove an image when there is still an order attached to the image.\nYou must first delete the order");
                msg.CancelButtonVisible = false;
                msg.ShowDialog();
                return;
            }

            if (ProjectController.CurrentFlowProject.SubjectList.CurrentItem.ImageList.CurrentItem.OrderImageOptions.Count > 0)
            {
                FlowMessageBox msg = new FlowMessageBox("Remove Image", "You may not remove an image when there is still an Image Option attached to the image.\nYou must first delete the Image Option in Order Entry");
                msg.CancelButtonVisible = false;
                msg.ShowDialog();
                return;
            }

            Subject preFilterSubject = ProjectController.CurrentFlowProject.SubjectList.CurrentItem;
            int preFilterSubjectIndex = ProjectController.CurrentFlowProject.SubjectList.DataGridCollectionView.CurrentPosition;


			SubjectImage image = e.Data;

			FileInfo imageFile = new FileInfo(image.ImagePath);

			CurrentFlowProject.SubjectList.CurrentItem.DetachImage(image.ImageFileName);

            string thisFile = image.ImagePathFullRes;
            if(string.IsNullOrEmpty(thisFile))
                thisFile = Path.Combine(ProjectController.CurrentFlowProject.ImageDirPath, image.ImageFileName);
			DeleteFileAfterGC(thisFile);

            ApplyFilter();

            if (ProjectController.CurrentFlowProject.SubjectList.DataGridCollectionView.Contains(preFilterSubject))
                ProjectController.CurrentFlowProject.SubjectList.CurrentItem = preFilterSubject;
            else
            {
                if (preFilterSubjectIndex >= ProjectController.CurrentFlowProject.SubjectList.DataGridCollectionView.Count)
                    preFilterSubjectIndex--;
                if (preFilterSubjectIndex >= 0)
                    ProjectController.CurrentFlowProject.SubjectList.DataGridCollectionView.MoveCurrentToPosition(preFilterSubjectIndex);
            }

		}


        internal void DeleteFileAfterGC(string path)
        {
            FileInfo source = new FileInfo(path);

            System.GC.Collect();
            System.GC.WaitForPendingFinalizers();
            source.Delete();
        }


		internal void DisplayOrderEntry()
		{
			// Toggle visibility of the OrderEntryPanel based on the current visibility
			if (EditContentPanel.OrderEntryTabItem.Visibility == Visibility.Visible)
			{
				EditContentPanel.OrderEntryTabItem.Visibility = Visibility.Collapsed;
			}
			else
			{
				EditContentPanel.OrderEntryTabItem.Visibility = Visibility.Visible;
			}

		}


		/// <summary>
		/// Responds to a request for a search or a barcode lookup; this can be induced by the
		/// user hitting enter or clicking the search button in the CollectionNavigator control,
		/// or by the barcode scan key pattern being detected
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		void CollectionNavigator_SearchInvoked(object sender, SearchInvokedEventArgs e)
		{
			IEnumerable<Subject> queryResult = null;

            if (e.IsDefiniteBarcodeScan)
            {
                queryResult = CurrentFlowProject.SubjectList.PerformBarcodeLookup(e.Criterion);

                if (queryResult != null && queryResult.Count() > 0)
                    CurrentFlowProject.SubjectList.MoveCurrentTo(queryResult.ElementAt(0));
            }
            else
            {
                CurrentFlowProject.SubjectList.ApplyFilter(e.Criterion, e.FieldDesc);
            }
		}

		public void InvokeScan(string scanText, bool endsWithCarriageReturn)
		{
			CollectionNavigator.InvokeScan(scanText, endsWithCarriageReturn);
		}

	}
}

