﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Flow.Schema.LinqModel.DataContext;
using Flow.Controls.View.Dialogs;
using ICSharpCode.SharpZipLib.Zip;
using Flow.Lib;
using Flow.Lib.Helpers;
using StructureMap;
using System.ComponentModel;
using System.Threading;
using NLog;
using System.Drawing;
using System.Drawing.Imaging;

namespace Flow.Controller.Project
{
    public class ProjectExporter
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public delegate void ExportCompleteDelegate(FlowProject project, string message, bool showMessage, bool abortDelete);

        public event ExportCompleteDelegate ExportComplete;

        FlowProject _flowProject;
        string _destinationPath;
        FlowMasterDataContext _flowMasterDataContext;
        public bool IncludeImages = true;
        public bool IsUncompressed = false;
        public NotificationProgressInfo ProgressInfo { get; set; }
        public bool ResizeImages = false;
        public string ResizeSize = "400";

        public bool HadErrors = false;
        public string LastExportedPath { get; set; }

        public ProjectExporter(FlowProject flowProject, string destinationPath, FlowMasterDataContext fmdc)
        {
            _flowProject = flowProject;
            _destinationPath = destinationPath;
            _flowMasterDataContext = fmdc;
        }

        public void ExportProjectWorker(object sender, DoWorkEventArgs e)
        {
            this._flowProject.ExportInProgress = true;
            //wait for the project to load before we try to archive it
            int cnt = 0;
            while (!this._flowProject.LoadingProgressInfo.IsComplete && ++cnt < 120)
            {
                Thread.Sleep(1000);
            }

            int waitCount = 0;
            while (this._flowProject.initingSubjects)
            {
                waitCount++;
                Thread.Sleep(1000);

                if (waitCount == (4 * 60)) //if its taken longer than 4 minutes, bail
                    this._flowProject.initingSubjects = false;

            }


            if (ProgressInfo != null)
            {
                ProgressInfo.Message = "Building export...";
                ProgressInfo.Update();
            }
            string cpName = String.IsNullOrEmpty(this._flowProject.FlowMasterDataContext.LoginID) ? System.Environment.MachineName : this._flowProject.FlowMasterDataContext.LoginID;

            try
            {
                //lets first copy everything to the temp folder
                string tempProjectFolder = Path.Combine(FlowContext.FlowTempDirPath, TicketGenerator.GenerateTicketString(10));
                if (IncludeImages)
                    IOUtil.CopyDirectoryRecursive(_flowProject.DirPath, tempProjectFolder);
                else
                    IOUtil.CopyDirectory(_flowProject.DirPath, tempProjectFolder);

                if (ResizeImages)
                {
                    int newSize = 400;
                    try
                    {
                        newSize = Int32.Parse(ResizeSize);
                    }
                    catch
                    {
                        //do nothing
                    }

                    foreach (string img in Directory.GetFiles(Path.Combine(tempProjectFolder, "Image")))
                    {
                        string tmpImage = Path.Combine(FlowContext.FlowTempDirPath, TicketGenerator.GenerateTicketString(10) + Path.GetExtension(img));
                        using (var image = Image.FromFile(img))
                        using (var newImage = ScaleImage(image, newSize, newSize))
                        {
                            if(img.ToLower().EndsWith("png"))
                                newImage.Save(tmpImage, ImageFormat.Png);
                            else
                                newImage.Save(tmpImage, ImageFormat.Jpeg);
                        }
                        File.Delete(img);
                        File.Move(tmpImage, img);
                    }

                }


                if (this.IsUncompressed) // Folder export
                {
                    string destinationProjectDirectory = Path.Combine(_destinationPath, _flowProject.FileSafeProjectName + "_" + cpName);
                    string duplicateDestinationDirectory = destinationProjectDirectory;
                   

                    int duplicateCount = 1;

                    while (Directory.Exists(duplicateDestinationDirectory))
                        duplicateDestinationDirectory = destinationProjectDirectory + "_" + (++duplicateCount).ToString();

                    LastExportedPath = duplicateDestinationDirectory;

                    if (IncludeImages)
                        IOUtil.CopyDirectoryRecursive(tempProjectFolder, duplicateDestinationDirectory);
                    else
                        IOUtil.CopyDirectory(tempProjectFolder, duplicateDestinationDirectory);


                }
                else // PAF export
                {
                    string destinationFilePath = Path.Combine(_destinationPath, _flowProject.FileSafeProjectName + "_" + cpName + ".paf");

                    LastExportedPath = destinationFilePath;

                    FastZipEvents events = new FastZipEvents();
                    events.Progress = new ICSharpCode.SharpZipLib.Core.ProgressHandler(ExportProject_OnZipProgress);
                    FastZip zip = new FastZip(events);
                    
                    if (IncludeImages)
                        zip.CreateZip(destinationFilePath, tempProjectFolder, true, null, "^((?!incoming|qc|thumbnails|PlCache).)*$");
                    else
                        zip.CreateZip(destinationFilePath, tempProjectFolder, false, null);

                }
            }
            catch (IOException ex)
            {
                HadErrors = true;
                if (ex.Message.Contains("not enough space on the disk"))
                {
                    logger.ErrorException("Export failed to save due to insufficient disk space.", ex);

                    if (ProgressInfo != null)
                    {
                        ProgressInfo.Update("Export FAILED (not enough space on disk)");
                        ProgressInfo.ShowDoneButton();
                    }

                    FlowMessageDialog msg = new FlowMessageDialog("Export Failed", "There is not enough space on the destination disk.");
                    msg.CancelButtonVisible = false;
                    msg.ShowDialog();
                }
                else
                    throw;
            }

            if (ProgressInfo != null)
            {
				//ProgressInfo.Message = "Export complete.";
				//ProgressInfo.Progress = -1;
				//ProgressInfo.Update();

				ProgressInfo.Update("Export complete.");
                ProgressInfo.ShowDoneButton();
            }

            this._flowProject.ExportInProgress = false;
        }

        public void BeginAsyncExportProject()
        {
            ProgressInfo = new NotificationProgressInfo("Export Project", "Initializing...");
            IShowsNotificationProgress progressNotification = ObjectFactory.GetInstance<IShowsNotificationProgress>();

            progressNotification.ShowNotification(ProgressInfo);
            ProgressInfo.NotificationProgress = progressNotification;

            FlowBackgroundWorkerManager.RunWorker(ExportProjectWorker, ExportProject_Completed);
        }

        void ExportProject_Completed(object sender, RunWorkerCompletedEventArgs e)
        {
            if (ExportComplete != null)
            {
                bool abortDelete = true;
                //does the export exists?
                if (!HadErrors && (File.Exists(LastExportedPath) || Directory.Exists(LastExportedPath)))
                    abortDelete = false;
                    
                ExportComplete(_flowProject, "Project has been archived successfully. Would you like to remove this project from Flow?", !BatchMode, abortDelete);
            }

        }

        private void ExportProject_OnZipProgress(object o, ICSharpCode.SharpZipLib.Core.ProgressEventArgs args)
        {
            if (ProgressInfo != null)
            {
                ProgressInfo.Progress = (int)Math.Floor(args.PercentComplete);
                ProgressInfo.Update();
            }
        }

        public bool BatchMode { get; set; }

        public static Image ScaleImage(Image image, int maxWidth, int maxHeight)
        {
            var ratioX = (double)maxWidth / image.Width;
            var ratioY = (double)maxHeight / image.Height;
            var ratio = Math.Min(ratioX, ratioY);

            var newWidth = (int)(image.Width * ratio);
            var newHeight = (int)(image.Height * ratio);

            var newImage = new Bitmap(newWidth, newHeight);

            using (var graphics = Graphics.FromImage(newImage))
                graphics.DrawImage(image, 0, 0, newWidth, newHeight);

            return newImage;
        }
    }
}
