﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.Lib;
using System.Xml;
using Flow.Schema.LinqModel.DataContext;
using StructureMap;
using System.Globalization;
using Flow.Lib.UpdateCheckSync;

namespace Flow.Controller.Project
{
    class ProjectTemplateSyncController
    {
         public static void ProcessProjectTemplate(UpdateCheckStore returnStore, NotificationProgressInfo notificationProgressInfo, FlowMasterDataContext flowMasterDataContext)
        {
            notificationProgressInfo.NotificationProgress = ObjectFactory.GetInstance<IShowsNotificationProgress>();
            notificationProgressInfo.Message = "Adding New Project Template...";
            notificationProgressInfo.Progress = 0;
            notificationProgressInfo.Update();
            int loopCnt = 0;
            foreach (XmlDocument thisXml in returnStore.xmlFiles)
            {
                loopCnt++;
                notificationProgressInfo.Progress = returnStore.xmlFiles.Count / loopCnt;
                notificationProgressInfo.Update("Adding New Project Template...");
                ProjectTemplate newProjectTemplate = new ProjectTemplate(flowMasterDataContext);
                //newProjectTemplate.ProjectTemplateGuid = Guid.NewGuid();
                XmlNode rootNode = thisXml.DocumentElement;
                foreach (XmlNode tableNode in rootNode.ChildNodes)
                {

                    if (tableNode.Name.Equals("ProjectTemplate"))
                    {
                        foreach (XmlNode childNode in tableNode.ChildNodes)
                        {
                            if (childNode.Name.Equals("OrganizationTypeID") && childNode.InnerText.Length > 0) newProjectTemplate.OrganizationTypeID = Convert.ToInt32(childNode.InnerText);
                            if (childNode.Name.Equals("ProjectTemplateDesc") && childNode.InnerText.Length > 0) newProjectTemplate.ProjectTemplateDesc = childNode.InnerText.ToString();
                            if (childNode.Name.Equals("FlowCatalogID") && childNode.InnerText.Length > 0) newProjectTemplate.FlowCatalogID = Convert.ToInt32(childNode.InnerText);
                            if (childNode.Name.Equals("ImageFormatTypeID") && childNode.InnerText.Length > 0) newProjectTemplate.ImageFormatTypeID = Convert.ToInt32(childNode.InnerText);
                            if (childNode.Name.Equals("ImageFormatSeparatorChar") && childNode.InnerText.Length > 0) newProjectTemplate.ImageFormatSeparatorChar = childNode.InnerText;
                            if (childNode.Name.Equals("ImageFormatField1") && childNode.InnerText.Length > 0) newProjectTemplate.ImageFormatField1 = childNode.InnerText;
                            if (childNode.Name.Equals("ImageFormatField2") && childNode.InnerText.Length > 0) newProjectTemplate.ImageFormatField2 = childNode.InnerText;
                            if (childNode.Name.Equals("ImageFormatField3") && childNode.InnerText.Length > 0) newProjectTemplate.ImageFormatField3 = childNode.InnerText;
                            if (childNode.Name.Equals("SubjectLastNameIsSearchable") && childNode.InnerText.Length > 0) newProjectTemplate.SubjectLastNameIsSearchable = Convert.ToBoolean(childNode.InnerText);
                            if (childNode.Name.Equals("SubjectFirstNameIsSearchable") && childNode.InnerText.Length > 0) newProjectTemplate.SubjectFirstNameIsSearchable = Convert.ToBoolean(childNode.InnerText);
                            if (childNode.Name.Equals("BarcodeScanInitSequence") && childNode.InnerText.Length > 0) newProjectTemplate.BarcodeScanInitSequence = childNode.InnerText;
                            if (childNode.Name.Equals("BarcodeScanTermSequence") && childNode.InnerText.Length > 0) newProjectTemplate.BarcodeScanTermSequence = childNode.InnerText;
                            if (childNode.Name.Equals("DateCreated") && childNode.InnerText.Length > 0) 
                                newProjectTemplate.DateCreated = DateTime.ParseExact(childNode.InnerText, "G", new CultureInfo("en-US", false));
                            if (childNode.Name.Equals("DateModified") && childNode.InnerText.Length > 0) 
                                newProjectTemplate.DateModified = DateTime.ParseExact(childNode.InnerText, "G", new CultureInfo("en-US", false));
                            if (childNode.Name.Equals("ProjectTemplateGuid") && childNode.InnerText.Length > 0) newProjectTemplate.ProjectTemplateGuid = new Guid(childNode.InnerText);
                            if (childNode.Name.Equals("ShortDescription") && childNode.InnerText.Length > 0) newProjectTemplate.ShortDescription = childNode.InnerText;
                            
                        }
                        notificationProgressInfo.Update("Adding New Project Template: " + newProjectTemplate.ProjectTemplateDesc + "...");
                        flowMasterDataContext.ProjectTemplateList.Add(newProjectTemplate);
                        flowMasterDataContext.SubmitChanges();

                    }

                    if (tableNode.Name.Equals("ProjectTemplateEventTrigger"))
                    {
                        bool isnew = false;
                        ProjectTemplateEventTrigger thisEventTrigger = null;
                        foreach (XmlNode childNode in tableNode.ChildNodes)
                        {
                            if (childNode.Name.Equals("EventTriggerID") && childNode.InnerText.Length > 0)
                                thisEventTrigger = flowMasterDataContext.ProjectTemplateEventTriggers.Where(t => t.EventTriggerID == Convert.ToInt32(childNode.InnerText)).FirstOrDefault();
                        }
                        if (thisEventTrigger == null)
                        {
                            thisEventTrigger = new ProjectTemplateEventTrigger();
                            isnew = true;
                        }

                        thisEventTrigger.ProjectTemplate = newProjectTemplate;
                        thisEventTrigger.ProjectTemplateID = newProjectTemplate.ProjectTemplateID;
                        foreach (XmlNode childNode in tableNode.ChildNodes)
                        {
                            if (childNode.Name.Equals("EventTriggerID") && childNode.InnerText.Length > 0) thisEventTrigger.EventTriggerID = Convert.ToInt32(childNode.InnerText);
                            if (childNode.Name.Equals("EventTriggerTypeID") && childNode.InnerText.Length > 0) thisEventTrigger.EventTriggerTypeID = Convert.ToInt32(childNode.InnerText);
                            if (childNode.Name.Equals("Active") && childNode.InnerText.Length > 0) thisEventTrigger.Active = Convert.ToBoolean(childNode.InnerText);

                        }

                        if (isnew)
                            flowMasterDataContext.ProjectTemplateEventTriggers.InsertOnSubmit(thisEventTrigger);

                        flowMasterDataContext.SubmitChanges();


                    }

                    if (tableNode.Name.Equals("ProjectTemplateImageType"))
                    {
                        ProjectTemplateImageType thisImageType = new ProjectTemplateImageType();
                        thisImageType.ProjectTemplate = newProjectTemplate;
                        thisImageType.ProjectTemplateID = newProjectTemplate.ProjectTemplateID;
                        foreach (XmlNode childNode in tableNode.ChildNodes)
                        {
                            if (childNode.Name.Equals("ProjectTemplateImageTypeDesc") && childNode.InnerText.Length > 0) thisImageType.ProjectTemplateImageTypeDesc = childNode.InnerText;
                            if (childNode.Name.Equals("IsExclusive") && childNode.InnerText.Length > 0) thisImageType.IsExclusive = Convert.ToBoolean(childNode.InnerText);

                        }

                        //a project template might have a ImageType field that is not in the local client's flowmaster, so we must add it
                        if (!ImageTypeExists(thisImageType.ProjectTemplateImageTypeDesc, flowMasterDataContext))
                        {
                            ImageType it = new ImageType();
                            it.ImageTypeDesc = thisImageType.ProjectTemplateImageTypeDesc;
                            it.IsExclusive = thisImageType.IsExclusive;
                           
                            flowMasterDataContext.ImageTypeList.Add(it);
                        }

                        flowMasterDataContext.SubmitChanges();

                    }

                    if (tableNode.Name.Equals("ProjectTemplateOrganizationalUnitType"))
                    {
                        ProjectTemplateOrganizationalUnitType thisOrgUnitType = new ProjectTemplateOrganizationalUnitType();
                        thisOrgUnitType.ProjectTemplate = newProjectTemplate;
                        thisOrgUnitType.ProjectTemplateID = newProjectTemplate.ProjectTemplateID;
                        foreach (XmlNode childNode in tableNode.ChildNodes)
                        {
                            if (childNode.Name.Equals("ProjectTemplateOrganizationalUnitTypeDesc") && childNode.InnerText.Length > 0) thisOrgUnitType.ProjectTemplateOrganizationalUnitTypeDesc = childNode.InnerText;
                            if (childNode.Name.Equals("ProjectTemplateOrganizationalUnitTypeParentID") && childNode.InnerText.Length > 0) thisOrgUnitType.ProjectTemplateOrganizationalUnitTypeParentID = Convert.ToInt32(childNode.InnerText);

                        }
                        flowMasterDataContext.SubmitChanges();

                    }

                    if (tableNode.Name.Equals("ProjectTemplateSubjectField"))
                    {
                        ProjectTemplateSubjectField thisSubjectField = new ProjectTemplateSubjectField();
                        thisSubjectField.ProjectTemplate = newProjectTemplate;
                        thisSubjectField.ProjectTemplateID = newProjectTemplate.ProjectTemplateID;
                        foreach (XmlNode childNode in tableNode.ChildNodes)
                        {
                            if (childNode.Name.Equals("FieldDataTypeID") && childNode.InnerText.Length > 0) thisSubjectField.FieldDataTypeID = Convert.ToInt32(childNode.InnerText);
                            if (childNode.Name.Equals("FieldDataSize") && childNode.InnerText.Length > 0) thisSubjectField.FieldDataSize = Convert.ToInt32(childNode.InnerText);
                            if (childNode.Name.Equals("ProjectTemplateSubjectFieldDesc") && childNode.InnerText.Length > 0) thisSubjectField.ProjectTemplateSubjectFieldDesc = childNode.InnerText;
                            if (childNode.Name.Equals("ProjectTemplateSubjectFieldDisplayName") && childNode.InnerText.Length > 0) thisSubjectField.ProjectTemplateSubjectFieldDisplayName = childNode.InnerText;
                            if (childNode.Name.Equals("IsSystemField") && childNode.InnerText.Length > 0) thisSubjectField.IsSystemField = Convert.ToBoolean(childNode.InnerText);
                            if (childNode.Name.Equals("IsKeyField") && childNode.InnerText.Length > 0) thisSubjectField.IsKeyField = Convert.ToBoolean(childNode.InnerText);
                            if (childNode.Name.Equals("IsScanKeyField") && childNode.InnerText.Length > 0) thisSubjectField.IsScanKeyField = Convert.ToBoolean(childNode.InnerText);
                            if (childNode.Name.Equals("IsRequiredField") && childNode.InnerText.Length > 0) thisSubjectField.IsRequiredField = Convert.ToBoolean(childNode.InnerText);
                            if (childNode.Name.Equals("IsProminentField") && childNode.InnerText.Length > 0) thisSubjectField.IsProminentField = Convert.ToBoolean(childNode.InnerText);
                            if (childNode.Name.Equals("IsSearchableField") && childNode.InnerText.Length > 0) thisSubjectField.IsSearchableField = Convert.ToBoolean(childNode.InnerText);

                            
                        }
                        flowMasterDataContext.SubmitChanges();
                        //a project template might have a subject field that is not in the local client's flowmaster, so we must add it
                        if (!SubectFieldExists(thisSubjectField.ProjectTemplateSubjectFieldDesc, flowMasterDataContext))
                        {
                            SubjectField sf = new SubjectField();
                            sf.SubjectFieldDesc = thisSubjectField.ProjectTemplateSubjectFieldDesc;
                            sf.SubjectFieldDisplayName = thisSubjectField.ProjectTemplateSubjectFieldDisplayName;
                            sf.FieldDataTypeID = thisSubjectField.FieldDataTypeID;
                            sf.FieldDataSize = thisSubjectField.FieldDataSize;
                            sf.IsSystemField = thisSubjectField.IsSystemField;
                            sf.IsKeyField = thisSubjectField.IsKeyField;
                            sf.IsScanKeyField = thisSubjectField.IsScanKeyField;
                            sf.IsRequiredField = thisSubjectField.IsRequiredField;
                            sf.IsProminentField = thisSubjectField.IsProminentField;
                            sf.IsSearchableField = thisSubjectField.IsSearchableField;
                            flowMasterDataContext.SubjectFieldList.Add(sf);
                            flowMasterDataContext.SubmitChanges();
                        }

                        

                    }

                    if (tableNode.Name.Equals("ProjectTemplateSubjectType"))
                    {
                        ProjectTemplateSubjectType thisSubjectType = new ProjectTemplateSubjectType();
                        thisSubjectType.ProjectTemplate = newProjectTemplate;
                        thisSubjectType.ProjectTemplateID = newProjectTemplate.ProjectTemplateID;
                        foreach (XmlNode childNode in tableNode.ChildNodes)
                        {
                            if (childNode.Name.Equals("ProjectTemplateOrganizationalUnitTypeID") && childNode.InnerText.Length > 0) thisSubjectType.ProjectTemplateOrganizationalUnitTypeID = Convert.ToInt32(childNode.InnerText);

                            if (childNode.Name.Equals("ProjectTemplateSubjectTypeDesc") && childNode.InnerText.Length > 0) thisSubjectType.ProjectTemplateSubjectTypeDesc = childNode.InnerText;

                        }
                        flowMasterDataContext.SubmitChanges();

                    }
                }
            }
            notificationProgressInfo.Complete("Finished Syncing Project Templates");
        }
        private static bool SubectFieldExists(string ProjectTemplateSubjectFieldDesc, FlowMasterDataContext fmdc)
        {
            foreach (SubjectField sf in fmdc.SubjectFields)
            {
                if (sf.SubjectFieldDesc == ProjectTemplateSubjectFieldDesc)
                    return true;
            }
            return false;
        }

        private static bool ImageTypeExists(string ProjectTemplateImageTypeDesc, FlowMasterDataContext fmdc)
        {
            foreach (ImageType it in fmdc.ImageTypes)
            {
                if (it.ImageTypeDesc == ProjectTemplateImageTypeDesc)
                    return true;
            }
            return false;
        }
    }

    }