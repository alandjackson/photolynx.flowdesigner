﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.Schema.LinqModel.DataContext;
using System.Net;
using System.IO;
using NetServ.Net.Json;
using System.Windows.Media.Imaging;
using Flow.Lib.ImageUtils;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Reflection;
using Flow.Lib.Helpers;
using Flow.Lib;
using Flow.View.Dialogs;
using System.Windows.Threading;
using System.Threading;
using Flow.Schema.LinqModel.DataContext.FlowMaster;
using System.Windows;
using NLog;
using System.Security.Cryptography;
using System.Xml.Serialization;
using Flow.Controller.OnlineOrdering;

namespace Flow.Controller.Project
{

    class SubmitOnlineOrderDataNew
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        private FlowProject _flowProject {get;set;}
        private FlowMasterDataContext _flowMasterDataContext {get; set;}
        private ProjectGallery _projectGallery { get; set; }
        private List<Subject> _applicableSubjects { get; set; }

        private JsonObject existingGalleryJson { get; set; }
        private JsonObject existingPSJson { get; set; }

        public NotificationProgressInfo NotificationInfo { get; set; }

        public bool SwitchShippingEnabled { get; set; }
        public bool doCrop { get; set; }
        public int allowableYearbookSelections { get; set; }
        
        //public string WelcomeMessage { get; set; }
        //public string WelcomeImage { get; set; }
        //public string GalleryName { get; set; }
        //public string GalleryPassword { get; set; }
        //public bool SingleImagePagackes { get; set; }
        //public string EventID { get; set; }


        public SubmitOnlineOrderDataNew(FlowProject fp, FlowMasterDataContext fmdc, ProjectGallery fpg, List<Subject> applicableSubjects)
        {
            _flowProject = fp;
            _flowMasterDataContext = fmdc;
            _projectGallery = fpg;
            _applicableSubjects = applicableSubjects;
        }

        public bool Begin()
        {
            // Check if any of the product packages have a price less than 1.00
            foreach (ProductPackage productPackage in _flowProject.FlowCatalog.ProductPackages)
            {
                if ((double)productPackage.Price < 1.0 && productPackage.ShowInOnlinePricesheet == true)
                {
                    this._flowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                    {
                        string msg_text = String.Format("Product package '{0}' has a price less than $1.00, which is not allowed.\n\nPlease set the price to $1.00 or greater.", productPackage.ProductPackageDesc);
                        FlowMessageBox msg = new FlowMessageBox("Invalid Pricesheet", msg_text);
                        msg.CancelButtonVisible = false;
                        msg.ShowDialog();
                    }));
                    if (NotificationInfo != null)
                        this.NotificationInfo.Complete("Invalid Pricesheet");

                    return false;
                }
            }

             NotificationInfo.Title = "Uploading Gallery";
            NotificationInfo.Update();
            //string eventID = "1056-8030-0014";

            //dont leave this hard coded
            bool isGreenScreen = false;
            
            if (this._flowProject.IsGreenScreen == true)
                isGreenScreen = true;


            //string baseUri = this._flowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixLoginURL;
            

           

            //string mainUri = baseUri + "/customer";
            string custId = this._flowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixCustomerID;
            string custPw = this._flowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixCustomerPassword;
            string installID = this._flowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixApplicationID;
            int eventCounter = this._flowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixEventCounter;

            //string baseUri = this._flowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixBaseURL;
            string baseUri = "http://api.imagequix.com";
            string publisherUri = baseUri + "/publisher/rest/customer/" + custId;
            string catalogUri = baseUri + "/catalog/rest/customer/" + custId;
            string publishImageUri = baseUri + "/publish/customer/" + custId + "/gallery/";


            string DigitalDownloadBaseRef = publisherUri + "/service/1/product/";

            string labCatalogSource = this._flowMasterDataContext.PreferenceManager.Upload.CatalogRequest.ImageQuixBaseUrl;
            string labCatalogURL = null;
            string labCatalogURLDump = null;
            if(labCatalogSource.Contains(".pud") && labCatalogSource.Contains("id="))
            {
                string vars = labCatalogSource.Split("?")[1];
                string catID = "";
                string catService = "";
                foreach (string var in vars.Split("&"))
                {
                    if(var.StartsWith("id"))
                        catID = var.Split("=")[1];
                    if (var.StartsWith("service"))
                        catService = var.Split("=")[1];
                }
                //string catID = labCatalogSource.Split("id=")[1]; //1481
                labCatalogURL = baseUri + "/catalog/rest/service/" + catService + "/catalog/" + catID;
                labCatalogURLDump = baseUri + "/catalog/rest/service/" + catService + "/catalogdump/" + catID;
            }
            else if (labCatalogSource.Contains(".pud") && !labCatalogSource.Contains("id="))
            {
                NotificationInfo.Update("PUD Catalog is missing IQ catalogID, please contact your lab");
                NotificationInfo.ShowDoneButton();
                return false;
            }



            bool isNewGallery = false;

            if (_projectGallery.LastModifyDate == null)
            {
                isNewGallery = true;
                _projectGallery.EventID = "0";
            }

            string loginUri = baseUri + "/publisher/rest/" + "auth";
            string postNewEventUri = publisherUri  + "/" + "gallery";
            string postUpdateEventUri = publisherUri  + "/" + "gallery/" + _projectGallery.GalleryCode;

            string updateImageCacheUri = publisherUri  + "/" + "event/" + _projectGallery.EventID + "/image/cleanCache?filter=IQ";


            WebHeaderCollection returnHeader = new WebHeaderCollection();
            if (NotificationInfo != null)
            {
                NotificationInfo.Update("Checking ImageQuix Credentials");
            }
            
            ////////////////////////////////
            //validate login credentials
            ///////////////////////////////

            if (PostToUri(loginUri, custId, custPw) != HttpStatusCode.OK)
            {
                //Error Connection - maybe not authorized?
                NotificationInfo.Update("FAILED SERVER LOGIN");
                NotificationInfo.ShowDoneButton();
                return false;
            }

            List<KeyValuePair<string, string>> GalleryCodes = new List<KeyValuePair<string, string>>();
            List<KeyValuePair<string, string>> GallerySubjectIDs = new List<KeyValuePair<string, string>>();

            string allPricesheetURL = publisherUri + "/" + "pricesheet";


            string getGalleryURL = publisherUri  + "/" + "gallery/" + _projectGallery.GalleryCode;
            string getSubjectsURL = publisherUri + "/" + "gallery/" + _projectGallery.GalleryCode;
            //https://vando.imagequix.com/rest/cust/T9EPQ96/event/1087-9980-0008/data/?filter=IQ

            string getPSURL = publisherUri  + "/pricesheet";


            JsonNumber existingGalleryCode = 0;
            JsonNumber marketingCampaignId = 0;


            int existingGalleryCatID = 0;
            ////////////////////////////////
            //lets get the existing gallery json
            ////////////////////////////////
            logger.Info("A");
            //if (isNewGallery)
            {
                logger.Info("B");
                existingGalleryJson = null;
                existingPSJson = null;
                string JsonResponse = GetJson(allPricesheetURL, custId, custPw, "");
                if (JsonResponse != null)
                {
                    logger.Info("C");
                    JsonParser parser = new JsonParser(new StringReader(JsonResponse), true);
                    //NetServ.Net.Json.JsonParser.TokenType nextTokenTrash = parser.NextToken();

                    JsonArray allPricesheets = parser.ParseArray();

                    foreach (JsonObject ps in allPricesheets)
                    {
                        logger.Info("D");
                        //JsonObject ps = (JsonObject)psitem["pricesheet"];
                        if ((JsonString)ps["name"] == this._flowProject.FlowCatalog.FlowCatalogDesc)
                        {
                            logger.Info("E");
                            //getPricesheetURL:
                            getPSURL = (ps["resource"]).ToString();
                            string fullPSstring = GetJson(getPSURL, custId, custPw, "");
                            JsonParser parserPS = new JsonParser(new StringReader(fullPSstring), true);
                            existingPSJson = parserPS.ParseObject();
                            _projectGallery.PricesheetID = ((JsonNumber)ps["id"]).ToString();
                            if (ps.ContainsKey("categories"))
                                Int32.TryParse(((JsonObject)((JsonArray)ps["categories"])[0])["id"].ToString(), out existingGalleryCatID);

                            JsonWriter _writerPS = new JsonWriter();
                            existingPSJson.Write(_writerPS);
                            string _postDataPS = _writerPS.ToString().Replace("\\/", "/");
                            string existingGalleryPSFile = Path.Combine(FlowContext.FlowTempDirPath, _projectGallery.EventID + "_existingPS.txt");
                            File.WriteAllText(existingGalleryPSFile, _postDataPS);

                        }
                    }
                }
            }
            //else
            {
                logger.Info("F");
                if (!isNewGallery)
                {
                    string JsonResponse = GetJson(getGalleryURL, custId, custPw, "");
                    if (JsonResponse == null)
                    {
                        logger.Info("G");
                        existingGalleryJson = null;
                    }
                    else
                    {
                        logger.Info("H");
                        //int loc = JsonResponse.IndexOf("eventType");
                        //JsonResponse = JsonResponse.Remove(loc, "eventType".Length).Insert(loc, "eventType2");
                        JsonParser parser = new JsonParser(new StringReader(JsonResponse), true);
                        //NetServ.Net.Json.JsonParser.TokenType nextToken = parser.NextToken();
                        //NetServ.Net.Json.JsonParser.TokenType nextToken2 = parser.NextToken();
                        existingGalleryJson = parser.ParseObject();
                        existingGalleryCode = existingGalleryJson["id"] as JsonNumber;
                        marketingCampaignId = existingGalleryJson["marketingCampaign"] as JsonNumber;

                        JsonWriter _writerPS = new JsonWriter();
                        existingGalleryJson.Write(_writerPS);
                        string _postDataPS = _writerPS.ToString().Replace("\\/", "/");
                        string existingGalleryPostJsonFile = Path.Combine(FlowContext.FlowTempDirPath, _projectGallery.EventID + "_existingGallery.txt");
                        File.WriteAllText(existingGalleryPostJsonFile, _postDataPS);



                    }

                    logger.Info("I");
                    //string JsonResponse2 = GetJson(getPSURL, custId, custPw, "");
                    //
                    if (string.IsNullOrWhiteSpace(_projectGallery.PricesheetID) && existingGalleryJson != null && existingGalleryJson.ContainsKey("priceSheet") && existingGalleryJson["priceSheet"].JsonTypeCode == JsonTypeCode.String)
                    {
                        logger.Info("J");
                        string psheetID = ((JsonString)existingGalleryJson["priceSheet"]).ToString().Split('/').Last();
                        _projectGallery.PricesheetID = psheetID;
                        logger.Info("Found the true pricesheetID in the gallery json: " + psheetID);
                    }
                
                    logger.Info("K");
                    if(getPSURL.EndsWith("sheet"))
                        getPSURL += ("/" + _projectGallery.PricesheetID);

                    logger.Info("My pricesheet url is: " + getPSURL);

                    string fullPSstring = GetJson(getPSURL, custId, custPw, "");

                    logger.Info("L");

                    //logger.Info("My pricesheet JSON: " + fullPSstring);

                    if (fullPSstring == null || fullPSstring == "null")
                        existingPSJson = null;
                    else
                    {
                        //int loc = JsonResponse.IndexOf("eventType");
                        //JsonResponse = JsonResponse.Remove(loc, "eventType".Length).Insert(loc, "eventType2");
                        //JsonParser parser2 = new JsonParser(new StringReader(JsonResponse2), true);
                        //NetServ.Net.Json.JsonParser.TokenType nextToken = parser.NextToken();
                        //NetServ.Net.Json.JsonParser.TokenType nextToken2 = parser.NextToken();
                        //existingPSJson = parser2.ParseObject();
                        logger.Info("L1");
                        JsonParser parserPS = new JsonParser(new StringReader(fullPSstring), true);
                        logger.Info("L2");
                        existingPSJson = parserPS.ParseObject();
                        logger.Info("L3");
                       // _projectGallery.PricesheetID = ((JsonNumber)ps["id"]).ToString();
                    }

                    logger.Info("M");
                

                    string SubjectJson = GetJson(getSubjectsURL, custId, custPw, "");
                    if (SubjectJson != null)
                    {
                        logger.Info("N");
                        JsonParser parser3 = new JsonParser(new StringReader(SubjectJson), true);

                        JsonObject subjectData = parser3.ParseObject();
                        if (subjectData.ContainsKey("subjects"))
                        {
                            JsonArray existingSubjectJson = (JsonArray)subjectData["subjects"];
                            foreach (JsonObject jo in existingSubjectJson)
                            {

                                JsonString thisexistingTicketCode = jo["code"] as JsonString;
                                JsonNumber thisexistingID = jo["id"] as JsonNumber;
                                GallerySubjectIDs.Add(new KeyValuePair<string, string>(thisexistingTicketCode.ToString(), thisexistingID.ToString()));
                            }
                        }
                    }

                }
            }

            logger.Info("O");
            if (NotificationInfo != null)
            {
                NotificationInfo.Update("Creating Gallery Pricesheet");
            }

            ////////////////////////////////
            // Make sure all PUD products are in IQ Catalog
            ////////////////////////////////
            if (labCatalogURL != null)
            {
                logger.Info("P");
                UpdateLocalProductsWithIQID(labCatalogURLDump);

                JsonObject root = new JsonObject();
                JsonArray products = new JsonArray();
                List<string> newLabels = new List<string>();
                root.Add("products", products);
                bool addedProduct = false;
                foreach (ProductPackage pp in _flowProject.FlowCatalog.ProductPackages)
                {
                    foreach (ProductPackageComposition ppc in pp.ProductPackageCompositions)
                    {

                        ImageQuixProduct IQProduct = ppc.ImageQuixProduct;
                        if (IQProduct.tempItemID < 500 && !newLabels.Contains(IQProduct.Label))
                        {
                            JsonObject product = new JsonObject();
                            product.Add("id", 0);
                            product.Add("name", IQProduct.Label);
                            product.Add("width", Convert.ToInt32(IQProduct.Width));
                            product.Add("height", Convert.ToInt32(IQProduct.Height));
                            product.Add("description", IQProduct.Label);
                            product.Add("internalLabID", IQProduct.ResourceURL);
                            product.Add("price", Convert.ToInt32(IQProduct.Price));
                            product.Add("billingCode", IQProduct.ResourceURL);
                            products.Add(product);
                            addedProduct = true;
                            newLabels.Add(IQProduct.Label);
                        }


                        //IQProduct.PrimaryKey = IQProduct.PrimaryKey;
                    }
                }
                if (addedProduct)
                {

                    JsonWriter _writerPS = new JsonWriter();
                    root.Write(_writerPS);
                    string _postDataIQLabProductsNeedAdded = _writerPS.ToString().Replace("\\/", "/");
                    string newProdsFile = Path.Combine(FlowContext.FlowTempDirPath, _projectGallery.EventID + "_IQLabProductsNeedAdded.txt");
                    File.WriteAllText(newProdsFile, _postDataIQLabProductsNeedAdded);


                    WebHeaderCollection theCatHeaders = new WebHeaderCollection();
                    JsonWriter writerCat = new JsonWriter();
                    root.Write(writerCat);
                    string rootData = writerCat.ToString().Replace("\\/", "/");
                    PostPendingEvent(labCatalogURL.Replace("/service", "/flow/service"), "flow", "flow", rootData, true, out theCatHeaders, null);

                    UpdateLocalProductsWithIQID(labCatalogURLDump);
                }
                


            }
            logger.Info("Q");
            ////////////////////////////////
            //Create Gallery Pricesheet Json
            ////////////////////////////////
            JsonArray backgroundSets = new JsonArray();
            if (existingPSJson != null)
                backgroundSets = (JsonArray)existingPSJson["backgroundSets"];


            JsonObject galleryPricesheet = GenerateGalleryPriceSheetJson(_projectGallery.PricesheetID, existingGalleryCatID, isGreenScreen, backgroundSets, DigitalDownloadBaseRef);
            JsonWriter writerPS = new JsonWriter();
            galleryPricesheet.Write(writerPS);
            string postDataPS = writerPS.ToString().Replace("\\/", "/");

            //write post data to temp folder
            string galleryPricesheetPostJsonFile = Path.Combine(FlowContext.FlowTempDirPath, _projectGallery.EventID + "_galleryPricesheetPost.txt");
            File.WriteAllText(galleryPricesheetPostJsonFile, postDataPS);


            string postNewPricesheetUri = publisherUri  + "/" + "pricesheet";
            string postUpdatePricesheetUri = publisherUri  + "/" + "pricesheet/" + _projectGallery.PricesheetID;

            logger.Info("R");
            ////////////////////////////////
            // Post Price Sheet (receive back Pricesheet ID)
            ////////////////////////////////
            if (existingPSJson != null && _projectGallery.DoNotUpdateExistingPricesheet == true)
            {
                logger.Info("S");
                //do not update the pricesheet
                galleryPricesheet = existingPSJson;
                _projectGallery.PricesheetID = existingPSJson["id"].ToString();
            }
            else
            {
                logger.Info("T");
                String ResponsePS = "";
                if (string.IsNullOrEmpty(_projectGallery.PricesheetID))
                {
                    logger.Info("U");
                    try
                    {
                        ResponsePS = PostPendingEvent(postNewPricesheetUri, custId, custPw, postDataPS, false, out returnHeader, "publish");
                    }
                    catch (Exception e)
                    {
                        //this._projectGallery.EventID = OriginalEventID;
                        this._projectGallery.PricesheetID = null;
                        NotificationInfo.Update("FAILED TO PUBLISH PRICE SHEET");
                        NotificationInfo.ShowDoneButton();
                        this._flowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                        {
                            FlowMessageBox msg = new FlowMessageBox("Failed To Upload", "FAILED TO PUBLISH PRICE SHEET.\n\n" + e.Message);
                            msg.CancelButtonVisible = false;
                            msg.ShowDialog();
                        }));
                        return false;
                    }
                }
                else
                {
                    logger.Info("V");
                    try
                    {
                        ResponsePS = PostPendingEvent(postUpdatePricesheetUri, custId, custPw, postDataPS, true, out returnHeader, "update");
                    }
                    catch (Exception e)
                    {
                        try
                        {
                            //maybe it failed because the pricesheet does not exists, so lets try to publish it as new
                            ResponsePS = PostPendingEvent(postNewPricesheetUri, custId, custPw, postDataPS, false, out returnHeader, "publish");


                        }
                        catch (Exception exc)
                        {
                            //this._projectGallery.EventID = OriginalEventID;
                            this._projectGallery.PricesheetID = null;
                            NotificationInfo.Update("FAILED TO UPDATE PRICE SHEET");
                            NotificationInfo.ShowDoneButton();
                            this._flowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                            {
                                FlowMessageBox msg = new FlowMessageBox("Failed To Upload", "FAILED TO UPDATE PRICESHEET.\n\n" + exc.Message);
                                msg.CancelButtonVisible = false;
                                msg.ShowDialog();
                            }));
                            return false;
                        }
                    }
                }

                logger.Info("W");
                //get pricesheetID from ResponsePS
                //http://api.dev.imagequix.com/publisher/rest//publisher/rest/customer/T9EPQ96/pricesheet/56884
                if (ResponsePS != null && ResponsePS.StartsWith("http"))
                {
                    logger.Info("X");
                    _projectGallery.PricesheetID = ResponsePS.Split('/').Last();
                }
                else
                {
                    logger.Info("Y");
                    JsonParser parserPS = new JsonParser(new StringReader(ResponsePS), true);
                    NetServ.Net.Json.JsonParser.TokenType nextTokenPS = parserPS.NextToken();
                    JsonObject rootObjectPS = parserPS.ParseObject();
                    if (string.IsNullOrEmpty(_projectGallery.PricesheetID))
                    {

                        _projectGallery.PricesheetID = rootObjectPS["id"].ToString();
                    }
                }
                //update the ps json to include the pricesheetid
                galleryPricesheet.Remove("id");
                galleryPricesheet.Add("id", Convert.ToInt32(_projectGallery.PricesheetID));
            }
            logger.Info("Z");


            //
            //update all galleries that are referencing this pricesheet
            // this is only needed if we are not preserving the pricesheet
            //
            string getAllGalleriesURL = publisherUri  + "/gallery?filter=IQ";


            //get a list of all galleries
            string JsonResponseAG = GetJson(getAllGalleriesURL, custId, custPw, "");
            if (JsonResponseAG != null && _projectGallery.DoNotUpdateExistingPricesheet != true)
            {
                JsonParser parser = new JsonParser(new StringReader(JsonResponseAG), true);
                //NetServ.Net.Json.JsonParser.TokenType nextTokenTrash = parser.NextToken();

                JsonArray allGalleries = parser.ParseArray();

                foreach (JsonObject gallery in allGalleries)
                {
                    if (((JsonNumber)gallery["priceSheetID"]).ToString() == _projectGallery.PricesheetID && _projectGallery.PricesheetID != null && _projectGallery.PricesheetID.Length > 0)
                    {
                        string updateGalleryPSUrl = publisherUri  + "/gallery/" + ((JsonNumber)gallery["id"]).ToString() + "/pricesheet/" + _projectGallery.PricesheetID;
                        try
                        {
                            string ResponsePS = PostPendingEvent(updateGalleryPSUrl, custId, custPw, postDataPS, true, out returnHeader, "update");
                            break;
                        }
                        catch (Exception e)
                        {
                            //we can ignor these exceptions
                        }
                    }
                }
            }

            //
            //publish the gallery
            //

            List<Subject> CurrentSubjectBatch = new List<Subject>();
            foreach (Subject sub in this._applicableSubjects.Where(s => s.HasAssignedImages))
            {
                //foreach (SubjectImage si in sub.SubjectImages)
               // {
                    //if (si.ExistsInOnlineGallery == true)
                    //{
                        CurrentSubjectBatch.Add(sub);
                        //break;
                    //}
               // }

            }
            //Create Gallery Json
            JsonObject galleryJson = GenerateGalleryJson(_projectGallery.EventID, galleryPricesheet, isGreenScreen, isNewGallery, CurrentSubjectBatch, existingGalleryCode, marketingCampaignId, GallerySubjectIDs, existingGalleryJson);


            JsonWriter writer = new JsonWriter();
            galleryJson.Write(writer);
            string postData = writer.ToString().Replace("\\/", "/");

            //write post data to temp folder
            string galleryPostJsonFile = Path.Combine(FlowContext.FlowTempDirPath, _projectGallery.EventID + "_GalleryPost.txt");
            File.WriteAllText(galleryPostJsonFile, postData);

            String Response = "";
            bool UpdateExistingFailed = false;
            if (!isNewGallery)
            {
                try
                {

                    try
                    {
                        //JsonWriter writerImgs = new JsonWriter();
                        //updatedImagesJson.Write(writerImgs);
                        //string ImageUpdateListPostData = writerImgs.ToString().Replace("\\/", "/");
                        //WebHeaderCollection updateListReturnHeader = new WebHeaderCollection();
                        //logger.Info("about to post gallery json");
                        //string ResponseUpdateImgList = PostPendingEvent(updateImageCacheUri, custId, custPw, ImageUpdateListPostData, true, out updateListReturnHeader, "update");
                    }
                    catch (Exception ex)
                    {
                        //ignore exception
                    }
                    Response = PostPendingEvent(postUpdateEventUri, custId, custPw, postData, true, out returnHeader, "update");
                }
                catch (Exception e)
                {
                    //UpdateExistingFailed = true;

                    NotificationInfo.Update("FAILED TO UPDATE GALLERY");
                    NotificationInfo.ShowDoneButton();
                    if (!checkAndRepairMissingImages(e.Message))
                    {
                        logger.Info(e.Message);
                        this._flowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                        {

                            FlowMessageBox msg = new FlowMessageBox("Failed To Upload", "FAILED TO UPDATE THIS GALLERY.\n\n" + e.Message);
                            msg.CancelButtonVisible = false;
                            msg.ShowDialog();
                        }));



                    }

                    return false;
                }
            }
            if (isNewGallery)
            {
                try
                {
                    Response = PostPendingEvent(postNewEventUri, custId, custPw, postData, false, out returnHeader, "publish");
                    isNewGallery = false;

                }
                catch (Exception e)
                {
                    NotificationInfo.Update("FAILED TO UPDATE GALLERY");
                    NotificationInfo.ShowDoneButton();

                    if (!checkAndRepairMissingImages(e.Message))
                    {


                        this._flowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                        {

                            FlowMessageBox msg = new FlowMessageBox("Failed To Upload", "FAILED TO PUBLISH THIS GALLERY.\n\n" + e.Message);
                            msg.CancelButtonVisible = false;
                            msg.ShowDialog();
                        }));


                    }
                    return false;
                }
            }


            if (string.IsNullOrEmpty(_projectGallery.GalleryCode))
            {

                if (returnHeader.AllKeys.Contains("Location"))
                {
                    _projectGallery.GalleryCode = returnHeader["Location"].Split("/").Last();
                    //_projectGallery.GalleryURL = returnHeader["x-iq-gallery-url"];
                    // string url = returnHeader["x-iq-gallery-url"];
                    this._flowMasterDataContext.RefreshGalleryList();
                }
            }



            _projectGallery.LastModifyDate = DateTime.Now;
            this._flowMasterDataContext.SubmitChanges();
            this._flowProject.FlowProjectDataContext.SubmitChanges();



            publishImageUri += _projectGallery.GalleryCode;

            
            int welcomeRes = 400;
            int sourceRes = 1200;

            string publishImageSpec = GetJson(publishImageUri, custId, custPw, "");
            if (!String.IsNullOrEmpty(publishImageSpec))
            {
                JsonParser specParser = new JsonParser(new StringReader(publishImageSpec), true);
                JsonObject theSpec = specParser.ParseObject();
                if (theSpec.ContainsKey("welcomeImageSize"))
                    welcomeRes = Int32.Parse((theSpec["welcomeImageSize"] as JsonNumber).ToString());
                if (theSpec.ContainsKey("sourceImageSize"))
                    sourceRes = Int32.Parse((theSpec["sourceImageSize"] as JsonNumber).ToString());
            }



            if (PutToUri(getGalleryURL + "/status/active", custId, custPw) != HttpStatusCode.OK)
            {
                //Error Connection - maybe not authorized?
                NotificationInfo.Update("FAILED TO ACTIVATE GALLERY");
                //NotificationInfo.ShowDoneButton();
                //return false;
            }















            //iterate thru the list looking for galleries that reference this pricesheet

            //update the pricesheet for each gallery
            //


            ////////////////////////////////
            //Upload product images and background images
            ////////////////////////////////

            // not used for now - string itrequestUriProductImage = mainUri + "/" + custId + "/pricesheetStore/" + _projectGallery.PricesheetID + "/psImage/";
            //if (isGreenScreen)
            //{
            //    List<string> uploadedbackgrounds = null;
            //    if (this._projectGallery.GalleryBackgroundList == null)
            //        uploadedbackgrounds = new List<string>();
            //    else
            //    {
            //        uploadedbackgrounds = this._projectGallery.GalleryBackgroundList.Split(",").ToList();
            //    }

            //    string itrequestUriGS = publisherUri  + "/pricesheetStore/" + _projectGallery.PricesheetID + "/psBackground/";
            //    int imgCountGS = _flowMasterDataContext.GreenScreenBackgrounds.Count();
            //    int prgCountGS = 0;
            //    foreach (GreenScreenBackground gsb in _flowMasterDataContext.GreenScreenBackgrounds)
            //    {
            //        if (uploadedbackgrounds.Any(w=> w.Trim() == gsb.FileName.Trim()))
            //            continue;


            //        prgCountGS++;
            //        if (NotificationInfo != null)
            //        {
            //            NotificationInfo.Progress = (int)(((float)prgCountGS / (float)imgCountGS) * (float)100);
            //            NotificationInfo.Update("Uploading Green Screen Image " + gsb.FileName);
            //        }

            //        String ImageTokeResponse = "";

            //        try
            //        {
            //            ImageTokeResponse = RequestImageToken(itrequestUriGS + gsb.FileName, custId, custPw, "");
            //        }
            //        catch
            //        {
            //            //try 10 times to get image token
            //            int tryLoop = 0;
            //            while (++tryLoop <= 10)
            //            {
            //                try
            //                {
            //                    NotificationInfo.Update("FAILED TO GET IMAGE TOKEN, Trying Again.... " + tryLoop + " ....");
            //                    ImageTokeResponse = RequestImageToken(itrequestUriGS + gsb.FileName, custId, custPw, "");
            //                    break;
            //                }
            //                catch
            //                {
                                
            //                    if (tryLoop == 10)
            //                    {
            //                        //this._projectGallery.EventID = OriginalEventID;
            //                        NotificationInfo.Update("FAILED TO GET IMAGE TOKEN, ABORTING....");
            //                        NotificationInfo.ShowDoneButton();
            //                        return false;
            //                    }
            //                    continue;
            //                }
            //            }
                       
            //        }

            //        JsonParser parser2 = new JsonParser(new StringReader(ImageTokeResponse), true);
            //        NetServ.Net.Json.JsonParser.TokenType nextToken2 = parser2.NextToken();
            //        JsonObject rootObject2 = parser2.ParseObject();


            //        if (UploadImage(rootObject2, gsb.FullPath, custId, custPw, 0, "") != HttpStatusCode.OK)
            //        {
            //            //Error, did not upload
            //            NotificationInfo.Update("FAILED TO UPLOAD IMAGE");
            //        }
            //        //this._flowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
            //        //{
            //        //    si.ExistsInOnlineGallery = true;
            //        //}));

            //        //this._flowProject.FlowProjectDataContext.SubmitChanges();
            //        //this._flowProject.UpdateGalleryCounts();
            //        uploadedbackgrounds.Add(gsb.FileName);
            //    }
            //    string allbgs="";
            //    foreach(string b in uploadedbackgrounds)
            //        allbgs += b + ", ";

            //    this._projectGallery.GalleryBackgroundList = allbgs;
            //}








            //
            // Begin Big Batch Loop
            //

            //
            // Changes for Background Uploader
            //
            bool useUploadQueue = true;

            OrderUploadHistory ouh = new OrderUploadHistory();
            ouh.ProjectName = this._flowProject.FlowProjectName;
            ouh.IsImageUpload = true;
            ouh.ImageCount = 0;
            this._flowMasterDataContext.OrderUploadHistories.InsertOnSubmit(ouh);
            this._flowMasterDataContext.SubmitChanges();
            this._flowMasterDataContext.RefreshOrderUploadHistoryList();

            string LabOrderQueueFolder = this._flowMasterDataContext.PreferenceManager.Application.LabOrderQueueDirectory;

            ImageUploadJob imageUploadJob = new ImageUploadJob();

            string uploadJob = System.IO.Path.Combine(LabOrderQueueFolder, "ImageUpload_" + ouh.OrderUploadHistoryID);
            if (!Directory.Exists(uploadJob)) Directory.CreateDirectory(uploadJob);


            //
            // END changes for Background uploader
            //


            List<string> MissingImages = new List<string>();
            bool keepgoing = true;
            bool firstTime = true;
            
            //while (keepgoing)
            //{
                // List<Subject> livesubjects = new List<Subject>();
                logger.Info("getting list of subjects to process");
                CurrentSubjectBatch = new List<Subject>();
                CurrentSubjectBatch = this._applicableSubjects.Where(s => s.HasAssignedImages).ToList();


                int newsubcount = 0;


                logger.Info("Subject Count: " + CurrentSubjectBatch.Count);
                logger.Info("NEW Subject Count: " + newsubcount);
                
                //if (!firstTime && newsubcount == 0)
                //{
                //    logger.Info("break - not first time && newsubcount is 0");
                //    break;
                //}

                if (!firstTime)
                {
                    isNewGallery = false;
                }
                firstTime = false;

                if (NotificationInfo != null)
                {
                    NotificationInfo.Update("Creating Gallery Template");
                }

                //
                // send images
                //
                //string itrequestUri = mainUri + "/" + custId + "/eventcust/" + custId + "/event/" + eventID + "/image/";
                NotificationInfo.Update("Sending Images");
                logger.Info("About to send images");
                string itrequestUri = publishImageUri + "/" + custId + "/event/" + _projectGallery.EventID + "/image/";
                int imgCount = 0;
                int prgCount = 0;
                //this._flowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                //{
                   // imgCount = _flowProject.FlowProjectDataContext.SubjectImages.Count();
                   // prgCount = _flowProject.GalleryLiveImageCount;
                   // _flowProject.UpdateGalleryCounts();
                //}));

                if (NotificationInfo != null)
                {
                    NotificationInfo.Count = imgCount;
                }

                JsonArray updatedImagesJson = new JsonArray();
                if (!isNewGallery)
                {

                   
                  

                    List<string> uploadedGroupImages = new List<string>();
                    int curiter = 1;
                    
                    foreach (Subject sub in CurrentSubjectBatch)
                    {
                        logger.Info("existing Gallery - upload subject images - " + sub.FormalFullName);
                        bool hasmissing = false;
                        NotificationInfo.Update(prgCount + " out of " + imgCount + "Uploading Images for: " + sub.FormalFullName);
                        foreach (SubjectImage si in sub.SubjectImages.Where(si => si.ExistsInOnlineGallery == false))
                        {
                            logger.Info("get image ready for upload - " + si.ImageFileName);
                            hasmissing = true;
                            if (!File.Exists(si.ImagePathFullRes))
                                continue;

                            NotificationInfo.Update(prgCount + " out of " + imgCount + " Uploading Image: " + si.ImageFileName);

                            if (si.IsGroupImage)
                            {
                                NotificationInfo.Update(prgCount + " out of " + imgCount + " Group Image, Check if already uploaded: " + si.ImageFileName);
                                if (uploadedGroupImages.Any(gi => gi == si.ImageFileName.Trim()))
                                {
                                    //this._flowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                                    //{
                                    //    si.ExistsInOnlineGallery = true;
                                    //    sub.FlagForMerge = true;
                                    //    this._flowProject.UpdateGalleryCounts();
                                    //}));
                                    continue;
                                }
                                uploadedGroupImages.Add(si.ImageFileName.Trim());
                            }



                            NotificationInfo.Update(prgCount + " out of " + imgCount + " Make sure its not already uploaded: " + si.ImageFileName);
                            if (!si.ExistsInOnlineGallery)
                            {
                                NotificationInfo.Update(prgCount + " out of " + imgCount + " make sure the original file exists: " + si.ImageFileName);
                                if (!File.Exists(si.ImagePathFullRes))
                                {
                                    MissingImages.Add(si.ImageFileName);
                                    continue;
                                }

                                prgCount++;


                                string imageFileName = si.ImageFileName;
                                //string pathToImage = si.ImagePathFullRes;

                                string pathToImage = si.RenderTempImage(FlowContext.FlowTempDirPath, doCrop, isGreenScreen, this._flowProject.DefaultGreenScreenBackground, true, 60, 300, false); 

                                //if (isGreenScreen && si.GreenScreenSettings != null)
                                if (1 == 2 && isGreenScreen && !String.IsNullOrEmpty(si.GreenScreenSettings))
                                 {
                                    NotificationInfo.Update(prgCount + " out of " + imgCount + " Rendering GreenScreen Image: " + imageFileName);
                                    string pathToPng = "";
                                    int wcount = 0;
                                    while (string.IsNullOrEmpty(pathToPng))
                                    {
                                        wcount++;
                                        try
                                        {

                                            pathToPng = si.GetRenderedPngImageFilename(null);
                                            //Thread t = new Thread(delegate()
                                            //{
                                            //    pathToPng = si.GetRenderedPngImageFilename(null);
                                            //});
                                            //t.SetApartmentState(ApartmentState.STA);
                                            //t.Start();
                                            //t.Join();
                                            //t.Abort();//just make sure the thread is killed

                                        }
                                        catch (Exception ex)
                                        {
                                            pathToPng = "";
                                            logger.Info(prgCount + " out of " + imgCount + " Rendering GreenScreen Image (Take" + wcount + "): " + imageFileName);
                                            if (wcount == 3)
                                            {

                                                NotificationInfo.Update(prgCount + " out of " + imgCount + " Rendering GreenScreen Image (Take" + wcount + "): " + imageFileName);
                                                throw ex;
                                            }
                                        }
                                    }
                                    if (pathToPng != null)
                                    {
                                        pathToImage = pathToPng;
                                        imageFileName = new FileInfo(pathToPng).Name;
                                    }

                                }

                                NotificationInfo.Update(prgCount + " out of " + imgCount + " Creating Image Object: " + imageFileName);
                                System.Drawing.Bitmap origBmp = (new Bitmap(pathToImage));

                                updatedImagesJson.Add(imageFileName);


                                if (NotificationInfo != null)
                                {
                                    NotificationInfo.Progress = (int)(((float)prgCount / (float)imgCount) * (float)100);
                                    NotificationInfo.Title = prgCount + " out of " + imgCount ;

                                    NotificationInfo.Update(prgCount + " out of " + imgCount + " Requesting Image Token for " + imageFileName);
                                }

                                
                                bool FailedImageUpload = false;


                                //origBmp.Save(Path.Combine(FlowContext.FlowTempDirPath, "trash.png"));

                                NotificationInfo.Update(prgCount + " out of " + imgCount + " Uploading Thumbnail Image: " + imageFileName);
                                int trycount = 0;
                                int maxtrys = 5;
                                while (trycount < maxtrys)
                                {
                                    trycount++;
                                    try
                                    {
                                        FailedImageUpload = false;
                                        JsonObject imageInfo = new JsonObject();
                                        string imageName = new FileInfo(si.ImagePathFullRes).Name;
                                        if (si.IsGreenscreen)
                                        {
                                            imageName = imageName.Replace(".jpg", ".png");
                                            imageName = imageName.Replace(".JPG", ".png");

                                            imageInfo.Add("size", 1200);
                                        }
                                        else
                                            imageInfo.Add("size", sourceRes);

                                        imageInfo.Add("uploadUrl", publishImageUri + "/image/" + imageName);
                                        //imageInfo.Add("size", sourceRes);

                                       
                                        NotificationInfo.Update(prgCount + " out of " + imgCount + " Uploading Primary Image: " + imageFileName);

                                        //
                                        //
                                        // Add to upload queue
                                        //
                                        //
                                        if (useUploadQueue)
                                        {

                                            string destFile = System.IO.Path.Combine(uploadJob, imageName);
                                            


                                            string url = imageInfo["uploadUrl"].ToString();
                                            logger.Info("queue for upload to this url: " + url);
                                            int imgSize = 1200;
                                            //if (imageInfo.ContainsKey("size"))
                                             //   imgSize = (int)(JsonNumber)imageInfo["size"];

                                            logger.Info("doing md5 hash stuff");
                                            string md5hashOrig;
                                            using (var md5 = MD5.Create())
                                            {
                                                using (MemoryStream strm = new MemoryStream())
                                                {
                                                    origBmp.Save(strm, System.Drawing.Imaging.ImageFormat.Bmp);
                                                    strm.Position = 0;

                                                    byte[] bytes = strm.ToArray();
                                                    md5hashOrig = BitConverter.ToString(md5.ComputeHash(bytes)).Replace("-", "").ToLower();
                                                }

                                            }

                                            logger.Info("saving image to uploadqueue");
                                            //System.Drawing.Image bmp = resizeImage(origBmp, imgSize);
                                            System.Drawing.Image bmp = OnlineApiHelpers.resizeImage(origBmp, imgSize);
                                            origBmp.Dispose();
                                            //Stream ms = new MemoryStream();
                                            if (isGreenScreen && destFile.EndsWith(".png"))
                                            {
                                                //bmp.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                                                bmp.Save(destFile, System.Drawing.Imaging.ImageFormat.Png);
                                                //bmp.Save(si.GetDropoutImagePath(false));
                                            }
                                            else
                                            {
                                                //bmp.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                                                bmp.Save(destFile, System.Drawing.Imaging.ImageFormat.Jpeg);
                                            }

                                            if (pathToImage.Contains(FlowContext.FlowTempDirPath) && File.Exists(pathToImage))
                                                File.Delete(pathToImage);

                                            //byte[] buffer = ReadToEnd(ms);

                                            //string md5hash;
                                            //using (var md5 = MD5.Create())
                                            //{

                                            //    md5hash = BitConverter.ToString(md5.ComputeHash(buffer)).Replace("-", "").ToLower();
                                            //}
                                            //Bitmap nb = new Bitmap(ms);
                                            //nb.Save(Path.Combine(FlowContext.FlowTempDirPath, md5hash + ".jpg"));

                                            logger.Info("adding instructions for this upload");
                                            ImageUploadItem imageUploadItem = new ImageUploadItem();
                                            imageUploadItem.ImageName = imageName;
                                            imageUploadItem.Url = url;
                                            imageUploadItem.Username = custId;
                                            imageUploadItem.Password = custPw;
                                            imageUploadItem.Method = "PUT";
                                            //imageUploadItem.HttpHeaders.Add(new ImageUploadHeader("x-iq-etag", md5hash));
                                            imageUploadItem.HttpHeaders.Add(new ImageUploadHeader("x-iq-etag-orig", md5hashOrig));
                                            imageUploadItem.HttpHeaders.Add(new ImageUploadHeader("x-iq-user", custId));


                                            imageUploadJob.images.Add(imageUploadItem);


                                            //writting temp upload file to disk
                                            logger.Info("writting temp upload file to disk");
                                            ouh.ImageCount = imageUploadJob.images.Count();
                                            //this._flowMasterDataContext.SubmitChanges();
                                            this._flowMasterDataContext.RefreshOrderUploadHistoryList();
                                            this._flowProject.UpdateGalleryCounts();
                                            string DataFile = System.IO.Path.Combine(uploadJob, "JobInfoTemp.xml");
                                            if (File.Exists(DataFile)) File.Delete(DataFile);
                                            var serializer = new XmlSerializer(typeof(ImageUploadJob));
                                            using (StreamWriter writer2 = new StreamWriter(DataFile))
                                            {
                                                serializer.Serialize(writer2, imageUploadJob);
                                            }



                                            logger.Info("about to update ExistInOnlineGallery and FlagForMerge Flags");
                                            //this._flowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                                            //{
                                                this._flowProject.UploadedImage(si);

                                            //si.ExistsInOnlineGallery = true;
                                            //sub.FlagForMerge = true;
                                            //}));

                                        }
                                        else
                                        {
                                            //
                                            //
                                            // Old way of inline uploading
                                            //
                                            //
                                            if (UploadImage(imageInfo, custId, custPw, si.Orientation, origBmp, isGreenScreen && !string.IsNullOrEmpty(si.GreenScreenSettings)) != HttpStatusCode.OK)
                                            {
                                                //Error, did not upload
                                                FailedImageUpload = true;
                                                throw new Exception("FAILED TO UPLOAD IMAGE full size");
                                            }
                                            if (!FailedImageUpload)
                                            {
                                                this._flowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                                                {
                                                    si.ExistsInOnlineGallery = true;
                                                    sub.FlagForMerge = true;
                                                    //this._flowProject.UpdateGalleryCounts();
                                                }));
                                            }
                                        }
                                        //if it made it here, we are good
                                        trycount = maxtrys + 1;

                                    }
                                    catch (Exception e)
                                    {
                                        logger.ErrorException("Exception occured while trying to upload image.", e);

                                        //this._projectGallery.EventID = OriginalEventID;
                                        if (trycount == maxtrys)
                                        {
                                            origBmp.Dispose();
                                            NotificationInfo.Update(prgCount + " out of " + imgCount + " FAILED TO UPLOAD IMAGE");
                                            logger.Error("Failed to upload image, giving up after " + trycount + " trys : " + imageFileName);
                                            NotificationInfo.ShowDoneButton();
                                            return false;
                                        }
                                        else
                                        {
                                            Thread.Sleep(3000);
                                            NotificationInfo.Update(prgCount + " out of " + imgCount + " Failed to upload image, trying again (" + trycount + "): " + imageFileName);
                                            logger.Info("Failed to upload image, trying again (" + trycount + "): " + imageFileName);
                                        }
                                    }
                                }


                                NotificationInfo.Update(prgCount + " out of " + imgCount + " Dispose of Image from memory: " + imageFileName);
                                origBmp.Dispose();

                                NotificationInfo.Update(prgCount + " out of " + imgCount + " Update Gallery Counts: " + imageFileName);
                                //this._flowProject.UpdateGalleryCounts();
                            }


                        }
                        if (hasmissing)
                            curiter++;

                        NotificationInfo.Update(prgCount + " out of " + imgCount + " Submit Changes: ");
                        this._flowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                        {
                            this._flowProject.FlowProjectDataContext.SubmitChanges();
                        }));
                    }
                   
                }
                //
                // end send image
                //

                if (NotificationInfo != null)
                {
                    NotificationInfo.Update("Submitting Gallery Definition");
                }

                // Create Pending Event (receive back welcome image token)
                Response = "";
               

                

                

                _projectGallery.LastModifyDate = DateTime.Now;
                this._flowMasterDataContext.SubmitChanges();
                this._flowProject.FlowProjectDataContext.SubmitChanges();

            //}

            if (useUploadQueue)
            {

                //foreach (SubjectImage si in UploadedImages)
                //{
                //    si.ExistsInOnlineGallery = true;
                //    si.Subject.FlagForMerge = true;
                //    this._flowProject.UpdateGalleryCounts();
                //}


                ouh.ImageCount = imageUploadJob.images.Count();
                this._flowMasterDataContext.SubmitChanges();
                this._flowMasterDataContext.RefreshOrderUploadHistoryList();
                this._flowProject.UpdateGalleryCounts();

                string DataFile = System.IO.Path.Combine(uploadJob, "JobInfo.xml");
                var serializer = new XmlSerializer(typeof(ImageUploadJob));
                using (StreamWriter writer2 = new StreamWriter(DataFile))
                {
                    serializer.Serialize(writer2, imageUploadJob);
                }

                try
                {
                    string tempFile = System.IO.Path.Combine(uploadJob, "JobInfoTemp.xml");
                    File.Delete(tempFile);
                }
                catch (Exception e) { }
            }
            //
            //DONE WITH Batch LOOP
            //











            //DONE????
            //if (NotificationInfo != null)
            //{
            //    NotificationInfo.Update("Activating Event");
            //}
            //// activate event
            //string activateUri = mainUri + "/" + custId + "/event/" + _projectGallery.EventID + "/status/active";
            //HttpStatusCode returnCode = PostToUri(activateUri, custId, custPw);
            //if (returnCode != HttpStatusCode.OK)
            //{
            //    if (returnCode == HttpStatusCode.NotModified)
            //    {
            //        NotificationInfo.Update("No Changes Made To Gallery");
            //        NotificationInfo.Complete("No Changes Made To Gallery");
            //        return;
            //    }
            //    //Error Connection - maybe not authorized?
            //    NotificationInfo.Update("Error Activating Event");
            //    NotificationInfo.Complete("Error Activating Event");
            //    return;
            //}
            if (!isNewGallery && NotificationInfo != null)
            {
                NotificationInfo.Update("Done Publishing Gallery");
                NotificationInfo.Complete("Done Publishing Gallery");
            }

            //_flowProject.GalleryLiveImageCount;
            _flowProject.UpdateGalleryCounts();

            if (isNewGallery)
            {
                NotificationInfo.Update("Done Creating Gallery");
                return true;
            }


            if (MissingImages.Count > 0)
            {
                string missing = "";
                foreach (string si in MissingImages)
                    missing += si + "\n";
                this._flowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                {
                    FlowMessageBox msg = new FlowMessageBox("Missing Images", "This Gallery Has Missing Images: .\n\n" + missing);
                    msg.CancelButtonVisible = false;
                    msg.ShowDialog();
                }));
            }

            if (NotificationInfo != null)
            {
                NotificationInfo.Complete("Done Publishing Gallery");
            }

            return false;
        }

        private void UpdateLocalProductsWithIQID(string labCatalogURL)
        {
            string thisCat = GetJson(labCatalogURL, "flow", "flow", "");
            if (thisCat == null || thisCat == "null")
                return;
            JsonParser parser = new JsonParser(new StringReader(thisCat), true);
            JsonObject CatRoot = parser.ParseObject();

            JsonWriter _writerPS = new JsonWriter();
            CatRoot.Write(_writerPS);
            string _postDataIQLabProducts = _writerPS.ToString().Replace("\\/", "/");
            string existingProdsFile = Path.Combine(FlowContext.FlowTempDirPath, _projectGallery.EventID + "_IQLabProducts.txt");
            File.WriteAllText(existingProdsFile, _postDataIQLabProducts);


            if (!CatRoot.ContainsKey("productGroups"))
                return;
            foreach (JsonObject prdGrp in (JsonArray)CatRoot["productGroups"])
            {
                if (!prdGrp.ContainsKey("products"))
                    continue;
                foreach (JsonObject prd in (JsonArray)prdGrp["products"])
                {
                    int newID = Convert.ToInt32(((JsonNumber)prd["id"]).Value);
                    string name = ((JsonString)prd["name"]).Value;
                    string intLabID = ((JsonString)prd["internalLabID"]).Value;
                    string catalogItemRef = ((JsonString)prd["resource"]).Value;
                    ImageQuixProduct IQProduct = this._flowMasterDataContext.ImageQuixCatalogList[0].ImageQuixProducts.FirstOrDefault(iqp => iqp.Label == name && iqp.ResourceURL == intLabID);
                    if (IQProduct != null)
                    {
                        IQProduct.tempItemID = newID;
                        IQProduct.catalogItemRef = catalogItemRef;
                    }
                }
            }
        }

        //private bool checkAndRepairMissingImages(string p)
        //{
        //    if (p.Contains("Some of the images that should be included as part of the gallery are missing"))
        //    {
                
        //        string filesDirty = p.Split("customMessage")[1];
        //        string filesClean = filesDirty.Replace("<", "").Replace(">", "").Replace("\"", "").Replace("\\", "").Replace("/", "").Replace("[", "").Replace("]", "");
        //        logger.Error("Missing Image: " + filesClean);
        //        int countFound = 0;
        //        int countFlagged = 0;
        //        foreach(string fileName in filesClean.Split(","))
        //        {
        //            countFound++;
        //            SubjectImage foundImage = this._flowProject.FlowProjectDataContext.SubjectImages.FirstOrDefault(si => si.ImageFileName == fileName || si.ImageFileName == fileName.Replace(".png",".JPG") );
        //            if (foundImage != null)
        //            {
        //                countFlagged++;
        //                this._flowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
        //                           {
        //                               foundImage.ExistsInOnlineGallery = false;
        //                           }));
        //            }
        //        }
        //        this._flowProject.UpdateGalleryCounts();
        //        this._flowProject.FlowProjectDataContext.SubmitChanges();
        //        this._flowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
        //        {

        //            FlowMessageBox msg = new FlowMessageBox("Failed To Upload", "This Gallery contained missing images.\n" + countFlagged + " out of " + countFound + " Images have been flagged for upload again\n\nPlease re-upload this gallery");
        //            msg.CancelButtonVisible = false;
        //            msg.ShowDialog();
        //        }));
        //        return true;
        //    }
        //    else
        //        return false;


        //}


        private bool checkAndRepairMissingImages(string p)
        {
            if (p.Contains("Some of the images that should be included as part of the gallery are missing"))
            {

                string filesDirty = p.Split("customMessage")[1];
                string filesClean = filesDirty.Replace("<", "").Replace(">", "").Replace("\"", "").Replace("\\", "").Replace("/", "").Replace("[", "").Replace("]", "");
                logger.Error("Missing Image: " + filesClean);
                int countFound = 0;
                int countFlagged = 0;
                foreach (string fileName in filesClean.Split(","))
                {
                    logger.Info("fixing a file: " + fileName);
                    countFound++;
                    this._flowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                    {
                        SubjectImage foundImage = this._flowProject.FlowProjectDataContext.SubjectImages.FirstOrDefault(si => si.ImageFileName == fileName || si.ImageFileName.ToLower() == fileName.Replace(".png", ".JPG").ToLower());
                        if (foundImage != null)
                        {
                            countFlagged++;

                            foundImage.ExistsInOnlineGallery = false;
                            foundImage.Subject.FlagForMerge = true;

                        }
                    }));


                    logger.Info("fixed a file: " + fileName);

                }
                this._flowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                {
                    this._flowProject.UpdateGalleryCounts();
                    this._flowProject.FlowProjectDataContext.SubmitChanges();

                    FlowMessageBox msg = new FlowMessageBox("Failed To Upload", "This Gallery contained missing images.\n" + countFlagged + " out of " + countFound + " Images have been flagged for upload again\n\nPlease re-upload this gallery");
                    msg.CancelButtonVisible = false;
                    msg.ShowDialog();
                }));
                return true;
            }
            else
                return false;


        }


        private string GetJson(string uri, string custId, string custPw, string postData)
        {
            logger.Info("l " + uri);
            HttpWebRequest rq = (HttpWebRequest)WebRequest.Create(uri);
            rq.Credentials = new NetworkCredential(custId, custPw);
            rq.Headers.Add("x-iq-token", "A15M876TJ8");
            rq.Headers.Add("x-iq-user", custId);
            rq.Timeout = (15000);//try for 15 seconds
            rq.Method = "GET";

            HttpWebResponse rs = null;
            int tryCount = 0;
            while (tryCount < 5)
            {
                try
                {
                    rs = (HttpWebResponse)rq.GetResponse();
                    StreamReader reader = new StreamReader(rs.GetResponseStream());
                    string jsonText = reader.ReadToEnd();

                    reader.Close();
                    rs.Close();
                    return jsonText;
                }
                catch (WebException e)
                {

                    tryCount++;

                }
            }

            return null;
        }

        private string RequestImageToken(string uri, string custId, string custPw, string postData)
        {
            logger.Info("m " + uri);
            HttpWebRequest rq = (HttpWebRequest)WebRequest.Create(uri);
            rq.Credentials = new NetworkCredential(custId, custPw);
            rq.Headers.Add("x-iq-token", "A15M876TJ8");
            rq.Headers.Add("x-iq-user", custId);
            rq.Headers.Add("x-iq-action", "publish");
            rq.Timeout = ((20) * 1000);//try for no seconds
            rq.Method = "POST";
            rq.ContentType = "application/json";

            rq.ContentLength = postData.Length;


            StreamWriter stOut = new
            StreamWriter(rq.GetRequestStream(),
            System.Text.Encoding.ASCII);
            stOut.Write(postData);
            stOut.Close();

            HttpWebResponse rs = null;
            int tryCount = 0;
            while (tryCount < 21)
            {
                try
                {
                    rs = (HttpWebResponse)rq.GetResponse();
                    break;
                }
                catch (WebException e)
                {
                    tryCount++;
                    if(tryCount == 21)
                        throw e;
                    Thread.Sleep(5000);
                }
            }

            //Stream s = rs.GetResponseStream();
            StreamReader reader = new StreamReader(rs.GetResponseStream());
            string jsonText = reader.ReadToEnd();

            reader.Close();
            rs.Close();
            return jsonText;
        }



        

        private HttpStatusCode UploadImage(JsonObject rootObject, string username, string password, int rotation, Image origBmp, bool isGreenScreen)
        {
            string url = rootObject["uploadUrl"].ToString();
            logger.Info("uploading to this url: " + url);
            int imgSize = 3200;
            if(rootObject.ContainsKey("size"))
                imgSize = (int)(JsonNumber)rootObject["size"];


            string md5hashOrig;
            using (var md5 = MD5.Create())
            {
                using (MemoryStream strm = new MemoryStream())
                {
                    origBmp.Save(strm, System.Drawing.Imaging.ImageFormat.Bmp);
                    strm.Position = 0;

                    byte[] bytes = strm.ToArray();
                    md5hashOrig = BitConverter.ToString(md5.ComputeHash(bytes)).Replace("-", "").ToLower();
                }

            }


            //System.Drawing.Image bmp = resizeImage(origBmp, imgSize);
            System.Drawing.Image bmp = OnlineApiHelpers.resizeImage(origBmp, imgSize);
            Stream ms = new MemoryStream();
            if (isGreenScreen)
            {
                bmp.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                //bmp.Save(si.GetDropoutImagePath(false));
            }
            else
                bmp.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);

            byte[] buffer = ReadToEnd(ms);

            string md5hash;
            using (var md5 = MD5.Create())
            {

                md5hash = BitConverter.ToString(md5.ComputeHash(buffer)).Replace("-", "").ToLower();
            }
            //Bitmap nb = new Bitmap(ms);
            //nb.Save(Path.Combine(FlowContext.FlowTempDirPath, md5hash + ".jpg"));

           
            HttpWebRequest rq = (HttpWebRequest)WebRequest.Create(url);
            rq.Credentials = new NetworkCredential(username, password);
            rq.Headers.Add("x-iq-etag", md5hash);
            rq.Headers.Add("x-iq-etag-orig", md5hashOrig);
            if (isGreenScreen)
                rq.ContentType = "image/png";
            else
                rq.ContentType = "image/jpg";

            rq.Headers.Add("x-iq-user", username);
            rq.Timeout = ((60 * 5) * 1000);//try for 5 minutes (60 seconds * 5 minutes * 1000 = miliseconds)
            rq.Method = "PUT";
           
            string currentMessage = NotificationInfo.Message;
            //NotificationInfo.Update(currentMessage + " resize image");

            rq.ContentLength = ms.Length;
            Stream requestStream = rq.GetRequestStream();


            //NotificationInfo.Update(currentMessage + " saving to memory stream");
          


           
            //NotificationInfo.Update(currentMessage + " writing image to server");
            requestStream.Write(buffer, 0, (int)ms.Length);

            requestStream.Flush();
            HttpWebResponse rs;
            try
            {
                NotificationInfo.Update(currentMessage + " getting response");
                rs = (HttpWebResponse)rq.GetResponse();
            }
            catch (Exception e)
            {

                throw e;
            }

            logger.Info("image upload response status: " + rs.StatusCode + " - " + rs.StatusDescription);
            //Stream s = rs.GetResponseStream();
            //NotificationInfo.Update(currentMessage + " reading response");
            StreamReader reader = new StreamReader(rs.GetResponseStream());
            string jsonText = reader.ReadToEnd();
            logger.Info("image upload response text: " + jsonText);
            
            //NotificationInfo.Update(currentMessage + " closing reader");
            reader.Close();
            bmp.Dispose();
            //NotificationInfo.Update(currentMessage + " done uploading");

            HttpStatusCode code = rs.StatusCode;
            rs.Close();
            return code;


        }

        private HttpStatusCode UploadImage(JsonObject rootObject, string pathToImage, string username, string password, int rotation, string actionHeader)
        {
            string url = rootObject["uploadUrl"].ToString();
            int imgSize = 1200;
            if (rootObject.ContainsKey("size"))
                imgSize = (int)(JsonNumber)rootObject["size"];

            string md5hashOrig;
            using (var md5 = MD5.Create())
            {
                using (var stream = File.OpenRead(pathToImage))
                {
                    //byte[] bytes = stream.To
                    md5hashOrig = BitConverter.ToString(md5.ComputeHash(stream)).Replace("-", "").ToLower();
                }
            }


            //System.Drawing.Image bmp = resizeImage((Image)(new Bitmap(pathToImage)), imgSize);
            System.Drawing.Image bmp = OnlineApiHelpers.resizeImage((Image)(new Bitmap(pathToImage)), imgSize);

            if (rotation == 90)
                bmp.RotateFlip(RotateFlipType.Rotate90FlipNone);
            else if (rotation == 180)
                bmp.RotateFlip(RotateFlipType.Rotate180FlipNone);
            else if (rotation == 270)
                bmp.RotateFlip(RotateFlipType.Rotate270FlipNone);

            Stream ms = new MemoryStream();
            if (pathToImage.ToLower().EndsWith(".png"))
                bmp.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
            else
                bmp.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);

            byte[] buffer2 = ReadToEnd(ms);

            string md5hash;
            using (var md5 = MD5.Create())
            {

                md5hash = BitConverter.ToString(md5.ComputeHash(buffer2)).Replace("-", "").ToLower();                
            }

            logger.Info("n " + url);
            HttpWebRequest rq = (HttpWebRequest)WebRequest.Create(url);
            rq.Credentials = new NetworkCredential(username, password);
            rq.Headers.Add("x-iq-etag", md5hash);
            rq.Headers.Add("x-iq-etag-orig", md5hashOrig);
            rq.ContentType = "image/jpg";
            //rq.Headers.Add("x-iq-token", "A15M876TJ8");
            rq.Headers.Add("x-iq-user", username);
            rq.Timeout = ((60 * 1) * 1000);//try for 1 minutes (60 seconds * 5 minutes * 1000 = miliseconds)
            rq.Method = "PUT";
            




            


            rq.ContentLength = ms.Length;
            Stream requestStream = rq.GetRequestStream();



           

            requestStream.Write(buffer2, 0, (int)ms.Length);

            requestStream.Flush();
            HttpWebResponse rs;
            try
            {
                rs = (HttpWebResponse)rq.GetResponse();
            }
            catch (Exception e)
            {

                throw e;
            }

            //Stream s = rs.GetResponseStream();
            StreamReader reader = new StreamReader(rs.GetResponseStream());
            string jsonText = reader.ReadToEnd();
            
            reader.Close();
            HttpStatusCode code = rs.StatusCode;
            rs.Close();
            bmp.Dispose();
            return code;


        }


        private JsonObject GenerateGalleryPriceSheetJson(string pricesheetID, int ProductCatID, bool isGreenScreen, JsonArray backgroundSets, string DigitalDownloadBaseRef)
        {
            logger.Info("Begin Generate Pricesheet");
            JsonObject pricesheet = new JsonObject();

            pricesheet.Add("backgroundSets", backgroundSets);

            JsonArray categories = new JsonArray();
            pricesheet.Add("categories", categories);

            if(string.IsNullOrEmpty(pricesheetID))
                pricesheet.Add("id", 0);
            else
                pricesheet.Add("id", Convert.ToInt32(pricesheetID));

            pricesheet.Add("name", this._flowProject.FlowCatalog.FlowCatalogDesc);
            //pricesheet.Add("lab-id", this._flowProject.FlowCatalog.FlowCatalogID);
            //pricesheet.Add("lab-id",1);
            pricesheet.Add("lastModified", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));

            //pricesheet.Add("orderfulfillment-type", "full-control");
           // pricesheet.Add("type", "standard");
            pricesheet.Add("version", 2);

            //
            //PACKAGES
            //
            try
            {
                JsonObject packages = new JsonObject();
                categories.Add(packages);
                //packages.Add("id", this._flowProject.FlowCatalog.FlowCatalogID);
                packages.Add("name", "Packages");
                //packages.Add("behaviors", new JsonArray());
                //packages.Add("ID", ProductCatID);
                //packages.Add("oID", );

                //packages.Add("lab-id", this._flowProject.FlowCatalog.FlowCatalogID);
                //packages.Add("required", "none");
                packages.Add("selectionType", "none");
                packages.Add("oID", Guid.NewGuid().ToString());
                JsonArray products = new JsonArray();
                //packages.Add("products", products);
                logger.Info("Pricesheet - Found " + _flowProject.FlowCatalog.ProductPackages.Count() + " Packages");

                if (existingPSJson != null)
                {
                    JsonArray cats1 = existingPSJson["categories"] as JsonArray;
                    foreach (JsonObject cat in cats1)
                    {
                        if (cat.ContainsKey("required") && (cat["required"].JsonTypeCode != JsonTypeCode.Null))
                        {
                            packages.Remove("required");
                            packages.Add("required", cat["required"]);
                        }


                        if (cat.ContainsKey("minOrderAmount") && (cat["minOrderAmount"].JsonTypeCode != JsonTypeCode.Null))
                        {
                            packages.Remove("minOrderAmount");
                            packages.Add("minOrderAmount", ((JsonNumber)cat["minOrderAmount"]));
                        }
                    }
                }

                foreach (ProductPackage pp in _flowProject.FlowCatalog.ProductPackages)
                {
                    if (pp.ProductPackageCompositions.Count == 0 && String.IsNullOrEmpty(pp.DigitalDownload))
                    {
                        continue;
                    }

                    logger.Info("Pricesheet - Adding Package: " + pp.ProductPackageDesc);

                    if (!pp.ShowInOnlinePricesheet)
                    {
                        logger.Info("Pricesheet - Adding Package: " + pp.ProductPackageDesc + "DO NOT SHOW IN PRICESHEET");
                        continue;
                    }

                    JsonObject existingPkg = null;
                    if (existingPSJson != null )
                    {
                        JsonArray cats = existingPSJson["categories"] as JsonArray;
                        foreach (JsonObject cat in cats)
                        {
                            JsonArray pkgs = cat["products"] as JsonArray;
                            foreach (JsonObject pkg in pkgs)
                            {
                                if (pkg["refID"] as JsonString == pp.ProductPackageID.ToString() || pkg["description"] as JsonString == pp.ProductPackageDesc)
                                {
                                    existingPkg = pkg;
                                    break;
                                }
                            }

                        }
                    }
                    //foreach package
                    JsonObject package = new JsonObject();

                    logger.Info("Pricesheet - " + pp.ProductPackageDesc + " : starting package");
                    package.Add("description", pp.ProductPackageDesc);
                    package.Add("type", "package");
                    //package.Add("labProductId", pp.ProductPackageID);//??????
                    if (existingPkg != null && existingPkg["id"] != null)
                        package.Add("id", existingPkg["id"]);
                    else
                        package.Add("id", 0);


                    package.Add("price", (double)pp.Price);

                    logger.Info("Pricesheet - " + pp.ProductPackageDesc + " : look for existing product image");
                    if (existingPkg != null && existingPkg.ContainsKey("productImage") && existingPkg["productImage"] != null)
                        package.Add("productImage", existingPkg["productImage"]);
                    else
                        package.Add("productImage",JsonNull.Null);

                    logger.Info("Pricesheet - " + pp.ProductPackageDesc + " : setting refID");

                    package.Add("refID", pp.ProductPackageID.ToString());
                    package.Add("name", pp.ProductPackageDesc);
                    if ((bool)_projectGallery.OneImagePerPackage)
                    {
                        package.Add("allowAdditionalImages", false);
                        package.Add("includedImages", 1);
                    }
                    else
                    {
                        package.Add("allowAdditionalImages", true);
                        package.Add("includedImages", 1);
                    }

                    package.Add("multiImageSurcharge", 0.0);
                    package.Add("multiImageSurchargeType", "noSurcharge");
                    JsonArray packageOptionGroups = new JsonArray();
                   
                    package.Add("optionGroups", packageOptionGroups);

                    
                    JsonArray packageProducts = new JsonArray();
                    //package.Add("products", packageProducts);

                    
                    if (pp.DigitalDownload != null && pp.DigitalDownload != "None" && pp.DigitalDownload.Length > 0)
                    {
                        logger.Info("Pricesheet - " + pp.ProductPackageDesc + " : adding digital download");
                        JsonObject product = new JsonObject();
                        packageProducts.Add(product);
                        product.Add("type", "imageDownload");
                        //product.Add("labProductID", 1);
                        product.Add("id",0);
                        product.Add("price", 0);
                        product.Add("productImage", JsonNull.Null);
                        
                        product.Add("description", "Image Download (" + pp.DigitalDownload + ")");

                        product.Add("name", "Image Download (" + pp.DigitalDownload + ")");
                        JsonArray productOptions = new JsonArray();
                        product.Add("optionGroups", productOptions);
                       
                        product.Add("packageUnits", 0);
                        product.Add("minImages", 1);
                        product.Add("maxImages", 1);
                        if (pp.DigitalDownload.StartsWith("400"))
                        {
                            product.Add("iqID", 101);
                            product.Add("size", "400");
                            product.Add("refID", "101");
                            product.Add("catalogItemRef", DigitalDownloadBaseRef + "101");
                        }
                        else if (pp.DigitalDownload.StartsWith("800"))
                        {
                            product.Add("iqID", 102);
                            product.Add("size", "800");
                            product.Add("refID", "102");
                            product.Add("catalogItemRef", DigitalDownloadBaseRef + "102");
                        }
                        else if (pp.DigitalDownload.StartsWith("1200"))
                        {
                            product.Add("iqID", 103);
                            product.Add("size", "1200");
                            product.Add("refID", "103");
                            product.Add("catalogItemRef", DigitalDownloadBaseRef + "103");
                        }

                        product.Add("iqPrice", 1);
                        
                        product.Add("fullResolution", false);
                        product.Add("downloadType", "single");
                        product.Add("isCustomizable", false);
                    }

                        foreach (ProductPackageComposition ppc in pp.ProductPackageCompositions)
                        {
                            logger.Info("Pricesheet - " + pp.ProductPackageDesc + " : adding product");
                            ImageQuixProduct IQProduct = ppc.ImageQuixProduct;

                            logger.Info("Pricesheet - " + ppc.ImageQuixProduct.Label + " : checking for existing product images");
                            JsonObject existingPrd = null;
                            try
                            {
                                if (existingPkg != null)
                                {
                                    foreach (JsonObject prd in existingPkg["products"] as JsonArray)
                                    {
                                        if (prd["refID"] as JsonString == IQProduct.PrimaryKey.ToString() && prd["description"] as JsonString == ppc.DisplayLabel)
                                        {
                                            existingPrd = prd;
                                            break;
                                        }
                                    }
                                }
                            }
                            catch (Exception exc)
                            {
                                //if this fails, dont sink the ship, just ignore the existing product image
                            }


                            //foreach product in package
                            logger.Info("Pricesheet - " + ppc.ImageQuixProduct.Label + " : create actual product object");
                            JsonObject product = new JsonObject();
                            

                            packageProducts.Add(product);
                            product.Add("type", "product");
                            //product.Add("labProductId", IQProduct.PrimaryKey);
                            //product.Add("lab-product-id", IQProduct.ImageQuixProductID);
                            //product.Add("lab-product-id", TicketGenerator.GenerateNumericTicketString(5));
                            //product.Add("id", IQProduct.PrimaryKey);
                            if (this._flowMasterDataContext.IsPUDConfiguration)
                                product.Add("refID", IQProduct.tempItemID.ToString());
                            else
                                product.Add("refID", IQProduct.PrimaryKey.ToString());
                            product.Add("description", ppc.DisplayLabel);
                            product.Add("name", ppc.DisplayLabel + " x " + ppc.Quantity);
                            //product.Add("resource", IQProduct.ResourceURL);
                            product.Add("requiresGroupImage", false);

                            if (existingPrd != null && existingPrd["displayImage"] != null)
                                product.Add("displayImage", existingPrd["displayImage"]);
                            else
                                product.Add("displayImage", JsonNull.Null);

                            if (existingPrd != null && existingPrd["displayImageURL"] != null)
                                product.Add("displayImageURL", existingPrd["displayImageURL"]);
                            else
                                product.Add("displayImageURL", JsonNull.Null);


                            //JsonArray catalogProducts = new JsonArray();
                            //product.Add("catalogProduct", catalogProducts);
                            //JsonObject catalogProduct = new JsonObject();
                            //catalogProducts.Add(catalogProduct);

                            //catalogProduct.Add("type", "product");

                            //catalogProduct.Add("height", (double)IQProduct.Height);
                            //catalogProduct.Add("rotation", 0);
                            //catalogProduct.Add("price", 0);

                            //logger.Info("Pricesheet - " + ppc.ImageQuixProduct.Label + " : apply existing product images (if exist)");

                            //if (existingPrd != null && existingPrd["displayImage"] != null)
                            //    catalogProduct.Add("displayImage", existingPrd["displayImage"]);
                            //else
                            //    catalogProduct.Add("displayImage", JsonNull.Null);

                            //if (existingPrd != null && existingPrd["displayImageURL"] != null)
                            //    catalogProduct.Add("displayImageURL", existingPrd["displayImageURL"]);
                            //else
                            //    catalogProduct.Add("displayImageURL", JsonNull.Null);

                            //catalogProduct.Add("resource", IQProduct.ResourceURL);

                            //catalogProduct.Add("internalLabID", JsonNull.Null);

                            //logger.Info("Pricesheet - " + ppc.ImageQuixProduct.Label + " : adding ref-id");
                            //catalogProduct.Add("id", IQProduct.PrimaryKey);
                            ////product.Add("description", IQProduct.Label);
                            //catalogProduct.Add("description", ppc.DisplayLabel);
                            //catalogProduct.Add("width", (double)IQProduct.Width);
                            //catalogProduct.Add("name", ppc.DisplayLabel + " x " + ppc.Quantity);
                            //catalogProduct.Add("optionGroups", new JsonArray());
                            //catalogProduct.Add("properties", new JsonArray());
                            //catalogProduct.Add("attributes", new JsonArray());

                            logger.Info("Pricesheet - " + ppc.ImageQuixProduct.Label + " : adding product Options");
                            JsonArray productOptions = new JsonArray();
                            //foreach (ImageQuixOptionGroup og in IQProduct.OptionGroupList)
                            //{
                            //    //foreach(ImageQuixOption op in og.OptionList)
                            //    //{
                            //    //foreach option
                            //    JsonObject productOption = new JsonObject();
                            //    productOption.Add("type", og.);
                            //    productOption.Add("id", (int)og.PrimaryKey);
                            //    productOption.Add("label", og.Label);
                            //    productOption.Add("required", og.IsMandatory);
                            //    productOption.Add("maxlength", 100);
                            //    productOption.Add("multiline", false);
                            //    productOption.Add("value", "");
                            //    productOptions.Add(productOption);
                            //    //end foreach option
                            //    //}
                            //}
                            
                            product.Add("optionGroups", productOptions);
                            //product.Add("isCustomizable", true);
                            if(this._flowMasterDataContext.IsPUDConfiguration)
                                product.Add("catalogItemRef", IQProduct.catalogItemRef);
                            else
                                product.Add("catalogItemRef", IQProduct.ResourceURL);

                            //packageProducts.Add(product);
                            //end foreach product in package
                        }
                        logger.Info("Pricesheet - " + pp.ProductPackageDesc + " : adding product list to package");
                    package.Add("products", packageProducts);
                    //package.Add("isCustomizable", false);
                    logger.Info("Pricesheet - " + pp.ProductPackageDesc + " : adding package to the list of packages");
                    products.Add(package);
                    //end foreach package
                }
                logger.Info("Pricesheet - adding list of packages to the main product node");
                packages.Add("products", products);
                //pricesheet.Add("last-modified", DateTime.Today.ToString("yyyyMMdd"));
                //categories.Add("pricesheet", pricesheet);
            }
            catch(Exception ex)
            {
                string a = ex.Message;
                logger.Error("Pricesheet - ERROR - " + a);
                //throw ex;
                
                //FlowMessageBox msgb = new FlowMessageBox("Problem With PriceSheet", a);
                //msgb.CancelButtonVisible = false;
                //msgb.ShowDialog();
            }

            //
            //BACKGROUNDS
            //
            
            //if(isGreenScreen){
            //    logger.Info("Pricesheet - Adding Backgrounds now");
            //    JsonObject backgrounds = new JsonObject();
            //    categories.Add(backgrounds);
            //    backgrounds.Add("name", "iqBackgroundCategory");
                
            //    JsonArray products = new JsonArray();
            //    backgrounds.Add("products", products);
                

            //    JsonObject product = new JsonObject();
            //    products.Add(product);
            //    JsonArray backgrounditems = new JsonArray();
            //    product.Add("backgrounds", backgrounditems);

            //    foreach (GreenScreenBackground gsb in _flowMasterDataContext.GreenScreenBackgrounds)
            //    {

            //        if (!gsb.IsPremium)
            //            backgrounditems.Add(gsb.FileName);
                    
            //    }
            //    product.Add("description", JsonNull.Null);
            //    product.Add("id", 0);
            //    product.Add("isCustomizable",false);
            //    //product.Add("labProductID",0);
            //    product.Add("name","Choose Your Background");
            //    product.Add("optionGroups", new JsonArray());
            //    product.Add("price",0);
            //    product.Add("productImage", JsonNull.Null);
            //    product.Add("refID", JsonNull.Null);
            //    product.Add("type", "backgrounds");

            //    backgrounds.Add("required", "none");



            //    logger.Info("Pricesheet - Adding Premium Backgrounds now");

            //    JsonObject pproduct = new JsonObject();
            //    products.Add(pproduct);
            //    JsonArray pbackgrounditems = new JsonArray();
            //    pproduct.Add("backgrounds", pbackgrounditems);

            //    foreach (GreenScreenBackground gsb in _flowMasterDataContext.GreenScreenBackgrounds)
            //    {
            //        if (gsb.IsPremium)
            //            pbackgrounditems.Add(gsb.FileName);

            //    }
            //    pproduct.Add("description", JsonNull.Null);
            //    pproduct.Add("id", 0);
            //    pproduct.Add("isCustomizable", false);
            //    //pproduct.Add("labProductID", 0);
            //    pproduct.Add("name", "Choose Your Background");
            //    pproduct.Add("optionGroups", new JsonArray());
            //    pproduct.Add("price", this._flowMasterDataContext.PreferenceManager.Edit.PremiumBackgroundPrice);
            //    pproduct.Add("productImage", JsonNull.Null);
            //    pproduct.Add("refID", JsonNull.Null);
            //    pproduct.Add("type", "backgrounds");


            //}



            return pricesheet;
        }


        public JsonObject GenerateGalleryJson(string eventID, JsonObject galleryPricesheet, bool isGreenScreen, bool isNewGallery, List<Subject> CurrentSubjectBatch, JsonNumber existingGalleryCode, JsonNumber marketingCampaignId, List<KeyValuePair<string, string>> GallerySubjectIDs, JsonObject existingGalleryJson)
        {
            string studioName = ((Organization)this._flowMasterDataContext.Organizations.First(o => o.OrganizationID == this._flowProject.StudioID)).OrganizationName; 
            JsonObject root = new JsonObject();
            //if(allowYearbookSelection)
            //    root.Add("allow-yearbook-selection", true);
            //else
            //    root.Add("allow-yearbook-selection", false);
            
            root.Add("numYearbookSelection", allowableYearbookSelections);
            //root.Add("creationDate",JsonNull.Null);
            //root.Add("customDataSpec", JsonNull.Null);
            root.Add("galleryType", "subject");
            root.Add("galleryStatus", "active");
            //root.Add("simpleWorkflow", false);
            //root.Add("searchable", true);
            //root.Add("id", Int32.Parse(eventID));
            //root.Add("galleryGroupOID", JsonNull.Null);
            root.Add("title", _projectGallery.GalleryName);
            
            root.Add("hidden", false);
            //root.Add("marketingCampaignID", JsonNull.Null);
            
            if(_projectGallery.EventDate == null)
                root.Add("eventDate", DateTime.Now.ToString("yyyy-MM-dd"));
            else
                root.Add("eventDate", ((DateTime)_projectGallery.EventDate).ToString("yyyy-MM-dd"));

            if (_projectGallery.ExpirationDate == null)
                root.Add("expirationDate", JsonNull.Null);
            else
                root.Add("expirationDate", ((DateTime)_projectGallery.ExpirationDate).ToString("yyyy-MM-dd"));


            
            root.Add("keyword", _projectGallery.Keyword);
            root.Add("password", _projectGallery.GalleryPassword);
            //root.Add("galleryCode", 0);
            if(existingGalleryCode != null && existingGalleryCode.Value > 0)
                root.Add("id", existingGalleryCode);
            if (marketingCampaignId != null && marketingCampaignId.Value > 0)
                root.Add("marketingCampaign", marketingCampaignId);

            //root.Add("orderFulfillment", "fullControl");
           
            
            root.Add("isGreenScreen", isGreenScreen);
            //root.Add("hiResImageLocationType", JsonNull.Null);
            root.Add("isPreOrder", false);
            root.Add("priceSheetID", _projectGallery.PricesheetID);
            //root.Add("priceSheet", _projectGallery.PricesheetID);
            root.Add("priceSheet", "customer/" + this._flowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixCustomerID + "/pricesheet/" + _projectGallery.PricesheetID);
            //root.Add("galleryConfig", JsonNull.Null);
            
            //root.Add("requiresPasswordChange", false);

            //if (File.Exists(_projectGallery.WelcomeImage))
                //root.Add("welcomeImage", (new FileInfo(_projectGallery.WelcomeImage)).Name.Replace(" ", "%20"));
            //else
                //root.Add("welcomeImage", "welcome.jpg");

            //root.Add("eventStatus", "draft");

            if (existingGalleryJson != null && existingGalleryJson.Count > 0)
            {
                //root.Add("watermark", existingGalleryJson["watermark"]);
                //root.Add("watermark-position",  existingGalleryJson["watermark-position"]);
                // root.Add("directShip", existingGalleryJson["directShip"]);
                if (existingGalleryJson.ContainsKey("welcomeMessage")) root.Add("welcomeMessage", existingGalleryJson["welcomeMessage"]);
                if (existingGalleryJson.ContainsKey("welcomeImage")) root.Add("welcomeImage", existingGalleryJson["welcomeImage"]);
                
            }

            if (existingGalleryJson != null && existingGalleryJson.Count > 0 && existingGalleryJson.ContainsKey("jobType"))
            {
                root.Add("jobType", existingGalleryJson["jobType"]);
            }
            else
            {
                root.Add("jobType", "underclass_fall");
            }

            if (existingGalleryJson != null && existingGalleryJson.Count > 0 && existingGalleryJson.ContainsKey("galleryConfig") && existingGalleryJson["galleryConfig"].GetType() != typeof(JsonNull))
            {
                root.Add("galleryConfig", existingGalleryJson["galleryConfig"]);
            }

            if (existingGalleryJson != null && existingGalleryJson.Count > 0 && existingGalleryJson.ContainsKey("discounts") && existingGalleryJson["discounts"].GetType() != typeof(JsonNull))
            {
                root.Add("discounts", existingGalleryJson["discounts"]);
            }

            //root.Add("folder", eventID);
           // root.Add("useGalleryConfig", true);

            //total hack... if we want IQ to replace cached images, we must change the watermark
            //string wmarkText = this._projectGallery.WatermarkText;
            //if (wmarkText.EndsWith(" "))
            //    wmarkText.TrimEnd();
            //else
            //    wmarkText += " ";

            //root.Add("watermark", wmarkText);
            //root.Add("watermarkPosition", this._projectGallery.WatermarkLocation);

            //JsonObject galleryConfig = new JsonObject();


            //JsonArray galleryContacts = new JsonArray();
            //root.Add("galleryContacts", galleryContacts);

            //galleryConfig.Add("id", Int32.Parse(eventID));

            //if(this._projectGallery.DirectShip == true)
            //    galleryConfig.Add("shipmentType", "direct");
            //else if (this._projectGallery.PickupShip == true)
            //    galleryConfig.Add("shipmentType", "pickup");
            //else if (this._projectGallery.CustomerChoiceShip == true)
            //    galleryConfig.Add("shipmentType", "choice");
            //else 
            //    galleryConfig.Add("shipmentType", "pickup");


            //if (SwitchShippingEnabled)
            //{
            //    string startDateNow = ((DateTime)this._projectGallery.SwitchShippingDate).ToString("yyyyMMdd");//DateTime.Today.ToString("yyyyMMdd");
            //    galleryConfig.Add("shipStartDate", startDateNow);
            //}
            //else
            //    galleryConfig.Add("shipStartDate", JsonNull.Null);

            //galleryConfig.Add("shipping", Convert.ToDouble((decimal)this._projectGallery.ShippingCost));

            //if(_projectGallery.HandlingRateType == "percent")
            //    galleryConfig.Add("handling", Convert.ToDouble((decimal)this._projectGallery.HandlingCost)/100);
            //else
            //    galleryConfig.Add("handling", Convert.ToDouble((decimal)this._projectGallery.HandlingCost));
            //galleryConfig.Add("tax", Convert.ToDouble((decimal)this._projectGallery.TaxRate)/100);
            //galleryConfig.Add("taxShipping", this._projectGallery.TaxShipping);
            //galleryConfig.Add("showBwAndSepia", this._projectGallery.ShowBWSepia);
            ////galleryConfig.Add("disable-full-control-cropping", this._projectGallery.DisableFullControlCropping);
            //galleryConfig.Add("pickupLabel", this._projectGallery.PickupText);

            ////new, are these required?
            //galleryConfig.Add("defaultShowImageNames", _projectGallery.DefaultShowImageName);
            //galleryConfig.Add("disableCroping", _projectGallery.DisableCropping);
            //galleryConfig.Add("handlingLabel", _projectGallery.HandlingLabel);
            //galleryConfig.Add("handlingRateType", _projectGallery.HandlingRateType);
            ////galleryConfig.Add("handling-rate-type", _projectGallery.HandlingRateType);

            //if (_projectGallery.MinOrderAmount == null)
            //    galleryConfig.Add("minOrderAmount", 1.00);
            //else
            //    galleryConfig.Add("minOrderAmount", Convert.ToDouble((decimal)_projectGallery.MinOrderAmount));

            //galleryConfig.Add("orderThankyouMessage", _projectGallery.OrderThankyouMessage);
            //galleryConfig.Add("shippingLabel", _projectGallery.ShippingLabel);
            //galleryConfig.Add("taxAllOrders", _projectGallery.TaxAllOrders);
            //galleryConfig.Add("taxLabel", _projectGallery.TaxLabel);
            //galleryConfig.Add("theme", _projectGallery.Theme);
            //galleryConfig.Add("wallpaper", _projectGallery.Wallpaper);

            //root.Add("galleryConfig", galleryConfig);

           
            //groups
            root.Add("groups", new JsonArray());
            
            //pricesheet
            //root.Add("pricesheet", galleryPricesheet);

            //subjects
            JsonArray allSubjects = new JsonArray();
            //if (!isNewGallery)
            {
                //foreach (Subject sub in this._flowProject.SubjectList.Where(s => s.HasAssignedImages))
                foreach (Subject sub in CurrentSubjectBatch)
                {
                    SubjectImage ybPose1 = null;
                    SubjectImage ybPose2 = null;

                    if (sub.SubjectImages.Any(si => si.SubjectImageTypeList.Any(sit => sit.ProjectImageType.ImageTypeDesc.ToLower() == "yearbook pose" && sit.IsAssigned == true)))
                        ybPose1 = sub.SubjectImages.First(si => si.SubjectImageTypeList.Any(sit => sit.ProjectImageType.ImageTypeDesc.ToLower() == "yearbook pose" && sit.IsAssigned == true));

                    if (sub.SubjectImages.Any(si => si.SubjectImageTypeList.Any(sit => sit.ProjectImageType.ImageTypeDesc.ToLower() == "yearbook pose 2" && sit.IsAssigned == true)))
                        ybPose2 = sub.SubjectImages.First(si => si.SubjectImageTypeList.Any(sit => sit.ProjectImageType.ImageTypeDesc.ToLower() == "yearbook pose 2" && sit.IsAssigned == true));

                    //foreach subject
                    JsonObject subject = new JsonObject();
                    subject.Add("name", sub.FormalFullName);
                    subject.Add("firstName", sub.SubjectDataRow["First Name"].ToString());
                    subject.Add("lastName", sub.SubjectDataRow["Last Name"].ToString());
                    //subject.Add("custom1", sub.TicketCode);
                    subject.Add("subjectID", JsonNull.Null);

                    subject.Add("address", JsonNull.Null);
                    subject.Add("city", JsonNull.Null);
                    subject.Add("state", JsonNull.Null);
                    subject.Add("zip", JsonNull.Null);
                    subject.Add("phone1", JsonNull.Null);
                    subject.Add("phone2", JsonNull.Null);
                    //subject.Add("custom1", JsonNull.Null);
                    //subject.Add("custom2", JsonNull.Null);
                    subject.Add("email", JsonNull.Null);
                    //subject.Add("galleryCode", JsonNull.Null);
                    //if (GalleryCodes.Any(kvp => kvp.Key == sub.TicketCode))
                    //{
                    //    KeyValuePair<string, string> subjectGalleryCode = GalleryCodes.FirstOrDefault(kvp => kvp.Key == sub.TicketCode);
                    //    subject.Add("galleryCode", subjectGalleryCode.Value);
                    //}
                    //else
                    //    subject.Add("galleryCode", JsonNull.Null);

                    if (GallerySubjectIDs.Any(kvp => kvp.Key == sub.TicketCode))
                    {
                        KeyValuePair<string, string> subjectID = GallerySubjectIDs.FirstOrDefault(kvp => kvp.Key == sub.TicketCode);
                        subject.Add("id", Int32.Parse(subjectID.Value));
                    }
                    else
                        subject.Add("id", JsonNull.Null);



                    //subject.Add("galleryGroupID", 0);
                    //subject.Add("galleryGroupOID", Guid.NewGuid().ToString());
                    //subject.Add("groupName", JsonNull.Null);
                    //subject.Add("id", 0);
                   // subject.Add("phone", JsonNull.Null);
                    //subject.Add("photoSafeID", JsonNull.Null);
                    subject.Add("notes", JsonNull.Null);
                    subject.Add("organization", JsonNull.Null);
                    subject.Add("refNo", JsonNull.Null);

                    subject.Add("mother", JsonNull.Null);
                    subject.Add("father", JsonNull.Null);

                    //subject.Add("state", JsonNull.Null);
                    subject.Add("year", JsonNull.Null);

                    subject.Add("grade", JsonNull.Null);
                    subject.Add("teacher", JsonNull.Null);
                    subject.Add("homeroom", JsonNull.Null);
                    subject.Add("personalization", JsonNull.Null);
                    subject.Add("jerseyNumber", JsonNull.Null);

                    if(ybPose1 == null)
                        subject.Add("yearbookSelection1", JsonNull.Null);
                    else
                        subject.Add("yearbookSelection1", ybPose1.ImageFileName);

                    if (ybPose2 == null)
                        subject.Add("yearbookSelection2", JsonNull.Null);
                    else
                        subject.Add("yearbookSelection2", ybPose2.ImageFileName);


                    subject.Add("custom1", sub.TicketCode);
                    subject.Add("custom2", JsonNull.Null);
                    subject.Add("custom3", JsonNull.Null);

                    //if they dont have a ticketId, create one
                    if (sub.TicketCode == null || (sub.TicketCode.Length == 0))
                    {
                        sub.TicketCode = TicketGenerator.GenerateTicketString(8);
                        sub.FlagForMerge = true;
                    }
                    else
                        subject.Add("code", sub.TicketCode);

                    JsonArray AllImages = new JsonArray();
                    foreach (SubjectImage si in sub.ImageList)
                    {
                        if (!File.Exists(si.ImagePathFullRes))
                            continue;

                        if (isGreenScreen && !string.IsNullOrEmpty(si.GreenScreenSettings))
                            AllImages.Add(si.ImageFileName.Replace(".jpg", ".png").Replace(".JPG", ".png"));
                        else
                            AllImages.Add(si.ImageFileName);

                    }
                    
                    subject.Add("createDate", JsonNull.Null);
                    subject.Add("lastModifiedDate", JsonNull.Null);

                    subject.Add("images", AllImages);
                    allSubjects.Add(subject);
                    //end foreach subject
                }
            }
            root.Add("subjects", allSubjects);

            return root;
        }

        public string PostPendingEvent(string uri, string username, string password, string postData, bool doPUT, out WebHeaderCollection headers, string actionHeader)
        {
            logger.Info("o " + uri);
            HttpWebRequest rq = (HttpWebRequest)WebRequest.Create(uri);
            rq.Credentials = new NetworkCredential(username, password);
            rq.Headers.Add("x-iq-token", "A15M876TJ8");
            rq.Headers.Add("x-iq-user", username);
            if(!string.IsNullOrEmpty(actionHeader))
                rq.Headers.Add("x-iq-action", actionHeader);
            rq.Timeout = ((60 * 10) * 1000);//try for 5 minutes (60 seconds * 5 minutes * 1000 = miliseconds)
            if (doPUT)
            {
                //rq.Headers.Add("x-iq-action", "update");
                rq.Method = "PUT";
            }
            else
            {
                //rq.Headers.Add("x-iq-action", "publish");
                rq.Method = "POST";
            }
            rq.ContentType = "application/json";

            rq.ContentLength = postData.Length;


            StreamWriter stOut = new
            StreamWriter(rq.GetRequestStream(),
            System.Text.Encoding.ASCII);
            stOut.Write(postData);
            stOut.Close();

            HttpWebResponse rs;
            try
            {
                rs = (HttpWebResponse)rq.GetResponse();
            }
            catch (Exception exc)
            {
                if (exc.GetType() == typeof(WebException))
                {
                    WebException ex = (WebException)exc;
                    string pageOutput = "";
                    if (ex.Response != null)
                    {
                        using (Stream stream = ex.Response.GetResponseStream())
                        {

                            using (StreamReader xreader = new StreamReader(stream))
                            {

                                pageOutput = xreader.ReadToEnd().Trim();
                            }
                        }
                    }
                    throw new Exception(((HttpWebResponse)ex.Response).StatusDescription + "\n\nDetails: " + pageOutput);
                }

                else if (exc.GetType() == typeof(TimeoutException))
                {
                    TimeoutException ex = (TimeoutException)exc;
                    logger.Info("Hit Timeout Exception");
                    throw ex;
                }

                else
                    throw exc;
            }
            headers = rs.Headers;

            //Stream s = rs.GetResponseStream();
            StreamReader reader = new StreamReader(rs.GetResponseStream());
            string jsonText = reader.ReadToEnd();
            
            reader.Close();
            rs.Close();
            if (headers.AllKeys.Any(k => k == "Location"))
                return headers["Location"];
            else
                return uri;
        }

        public string GetInstallID(string installCodeUri, string custId, string custPw)
        {
            logger.Info("p " + installCodeUri);
            HttpWebRequest rq = (HttpWebRequest)WebRequest.Create(installCodeUri);
            rq.Credentials = new NetworkCredential(custId, custPw);
            rq.Headers.Add("x-iq-token", "A15M876TJ8");
            rq.Headers.Add("x-iq-user", custId);
            rq.Timeout = 10000;//only try for 10 seconds
            rq.Method = "GET";
            HttpWebResponse rs;
            try
            {
                rs = (HttpWebResponse)rq.GetResponse();
            }
            catch (WebException e)
            {

                return "" ;
            }

            //Stream s = rs.GetResponseStream();
            StreamReader reader = new StreamReader(rs.GetResponseStream());
            string returnV = reader.ReadToEnd();

            reader.Close();
            rs.Close();
            return returnV;
        }

        public HttpStatusCode PutToUri(string uri, string un, string pw)
        {
            logger.Info("q " + uri);
            HttpWebRequest rq = (HttpWebRequest)WebRequest.Create(uri);
            rq.Credentials = new NetworkCredential(un, pw);
            rq.Headers.Add("x-iq-token", "A15M876TJ8");
            rq.Headers.Add("x-iq-user", un);
            rq.Timeout = 30000;//only try for 30 seconds
            rq.Method = "PUT";
            HttpWebResponse rs;
            try
            {
                rs = (HttpWebResponse)rq.GetResponse();
            }
            catch (WebException e)
            {
                if (e.Message.Contains("304"))
                    return HttpStatusCode.NotModified;

                return HttpStatusCode.Unauthorized;
            }

            HttpStatusCode code = rs.StatusCode;
            rs.Close();
            return code;

        }

        public HttpStatusCode PostToUri(string loginUri, string un, string pw)
        {
            logger.Info("r " + loginUri);
            HttpWebRequest rq = (HttpWebRequest)WebRequest.Create(loginUri);
            rq.Credentials = new NetworkCredential(un, pw);
            rq.Headers.Add("x-iq-token", "A15M876TJ8");
            rq.Headers.Add("x-iq-user", un);
            rq.Timeout = 30000;//only try for 30 seconds
            rq.Method = "GET";
             HttpWebResponse rs;
            try
            {
               rs = (HttpWebResponse)rq.GetResponse();
            }
            catch (WebException e)
            {
                if(e.Message.Contains("304"))
                    return HttpStatusCode.NotModified;

                return HttpStatusCode.Unauthorized;
            }

            HttpStatusCode code = rs.StatusCode;
            rs.Close();
            return code;
           
        }

        private static Image resizeImage(Image imgToResize, int iSize)
        {
            
            int sourceWidth = imgToResize.Width;
            int sourceHeight = imgToResize.Height;
            System.Drawing.Size size = new System.Drawing.Size(iSize, iSize);
            if (sourceWidth > sourceHeight)
            {
                float multiplier = ((float)iSize / (float)sourceWidth);
                size.Width = iSize;
                size.Height = Convert.ToInt32((sourceHeight * multiplier));
            }
            if (sourceWidth < sourceHeight)
            {
                float mulitplier = ((float)iSize / (float)sourceHeight);
                size.Height = iSize;
                size.Width = Convert.ToInt32((sourceWidth * mulitplier));
            }
            float nPercent = 0;
            float nPercentW = 0;
            float nPercentH = 0;

            nPercentW = ((float)size.Width / (float)sourceWidth);
            nPercentH = ((float)size.Height / (float)sourceHeight);

            if (nPercentH < nPercentW)
                nPercent = nPercentH;
            else
                nPercent = nPercentW;

            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            Bitmap b = new Bitmap(destWidth, destHeight);
            Graphics g = Graphics.FromImage((Image)b);
            g.InterpolationMode = InterpolationMode.Low;

            g.DrawImage(imgToResize, 0, 0, destWidth, destHeight);
            g.Dispose();

            return (Image)b;
        }

        private static Int32Rect getCropArea(Bitmap iSrc, int rotation, double CropL, double CropT, double CropW, double CropH)
        {


            double width = iSrc.Width;
            double height = iSrc.Height;

            int cropAreaL = (int)(CropL * width);
            int cropAreaT = (int)(CropT * height);
            int cropAreaW = (int)(CropW * width);
            int cropAreaH = (int)(CropH * height);

            if (cropAreaL + cropAreaW > width)
                cropAreaW = (int)width - cropAreaL;
            if (cropAreaT + cropAreaH > height)
                cropAreaH = (int)height - cropAreaT;

            if (rotation == 90)
            {
                return new Int32Rect(
                    cropAreaT,
                    ((int)width - cropAreaL) - cropAreaW,
                    cropAreaH,
                    cropAreaW
                    );
            }

            if (rotation == 270)
            {
                return new Int32Rect(
                    ((int)height - cropAreaT) - cropAreaH,
                    cropAreaL,
                    cropAreaH,
                    cropAreaW
                    );
            }

            return new Int32Rect(
                cropAreaL,
                cropAreaT,
                cropAreaW,
                cropAreaH
                );
        }

        public static byte[] ReadToEnd(System.IO.Stream stream)
        {
            long originalPosition = stream.Position;
            stream.Position = 0;

            try
            {
                byte[] readBuffer = new byte[4096];

                int totalBytesRead = 0;
                int bytesRead;

                while ((bytesRead = stream.Read(readBuffer, totalBytesRead, readBuffer.Length - totalBytesRead)) > 0)
                {
                    totalBytesRead += bytesRead;

                    if (totalBytesRead == readBuffer.Length)
                    {
                        int nextByte = stream.ReadByte();
                        if (nextByte != -1)
                        {
                            byte[] temp = new byte[readBuffer.Length * 2];
                            Buffer.BlockCopy(readBuffer, 0, temp, 0, readBuffer.Length);
                            Buffer.SetByte(temp, totalBytesRead, (byte)nextByte);
                            readBuffer = temp;
                            totalBytesRead++;
                        }
                    }
                }

                byte[] buffer = readBuffer;
                if (readBuffer.Length != totalBytesRead)
                {
                    buffer = new byte[totalBytesRead];
                    Buffer.BlockCopy(readBuffer, 0, buffer, 0, totalBytesRead);
                }
                return buffer;
            }
            finally
            {
                stream.Position = originalPosition;
            }
        }
    }
}
