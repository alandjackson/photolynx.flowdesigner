﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.Schema.LinqModel.DataContext;
using NetServ.Net.Json;
using Flow.Lib;
using System.IO;
using System.Net;
using System.ComponentModel;
using StructureMap;
using System.Runtime.InteropServices;
using Flow.Schema.Reports;
using Flow.Controller.Reports;
using System.Collections.ObjectModel;
using System.Windows.Threading;
using Rebex.Net;
using Flow.Lib.Net;
using Flow.Controls.View.Dialogs;
using Flow.View.Dialogs;
using System.Xml;
using Flow.Lib.Log;
using C1.C1Preview;
using Flow.Lib.Helpers;
using Flow.Designer.Model;
using Flow.Designer.Model.Settings;
using System.Windows.Media.Imaging;
using System.Windows.Controls;
using Flow.Designer.Lib.Service;
using System.Threading;
using Flow.Lib.Persister;
using System.Drawing;
using Flow.Service;
using Flow.Lib.Web;
using NLog;
using Flow.Lib.FlowOrder;
using Flow.Lib.Activation;
using System.Web.Script.Serialization;
using System.Web;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Drawing.Imaging;


namespace Flow.Controller.Project
{
    class SendLabOrderNew
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        private FlowProject _flowProject { get; set; }
        private FlowController _flowController { get; set; }

        public string SortField1 { get; set; }
        public string SortField2 { get; set; }
        public bool RenderGreenScreen { get; set; }
        public bool ApplyCrop { get; set; }
        public bool UseOriginalFileNames { get; set; }
        public bool IncludeAllSubjectsAndImages { get; set; }
        public string SaveOrderPath { get; set; }

        public List<View.Project.OrderOptionSelected> OrderOptionList { get; set; }

        public string Mail = null;

        public bool doneFileUploaded = false;

        public NotificationProgressInfo NotificationInfo { get; set; }

        public SendLabOrderNew(FlowProject fp, FlowController fc)
        {
            _flowProject = fp;
            _flowController = fc;
        }




        public string SendOrder(List<OrderPackage> CurrentOrderPackages, Organization orgForBulkShipping)
        {
            logger.Info("Begin Send Order");

            //FlowObservableCollection<OrderPackage> TheseOrderPackages = this._flowProject.FlowProjectDataContext.FilteredOrderPackages;
            
            ulong freeBytesThreshold = (ulong)8 * 1024 * 1024 * 1024; // 8 GiB
            if (CurrentOrderPackages.Count < 10)
                 freeBytesThreshold = (ulong)1 * 1024 * 1024 * 1024; // 1 GiB
            else if (CurrentOrderPackages.Count < 20)
                freeBytesThreshold = (ulong)2 * 1024 * 1024 * 1024; // 2 GiB
            else if (CurrentOrderPackages.Count < 30)
                freeBytesThreshold = (ulong)3 * 1024 * 1024 * 1024; // 3 GiB
            else if (CurrentOrderPackages.Count < 50)
                freeBytesThreshold = (ulong)1 * 1024 * 1024 * 1024; // 5 GiB

            if (IOUtil.DriveFreeBytesAvailable(FlowContext.FlowTempDirPath) < freeBytesThreshold)
            {
                string message = "There is insufficient free space on the local system drive. Submit order cannot continue.";
                _flowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                {
                    FlowMessageBox msg = new FlowMessageBox("Submit Order Error", message);
                    msg.CancelButtonVisible = false;
                    msg.ShowDialog();
                }));
                NotificationInfo.Message = "Error Submitting an Order";
                NotificationInfo.ShowDoneButton();

                logger.Warn(message);

                return null;
            }

            bool isDp2 = !this._flowController.ProjectController.FlowMasterDataContext.IsPUDConfiguration;
            bool isPlic = this._flowController.ProjectController.FlowMasterDataContext.IsPLICConfiguration;
            doneFileUploaded = false;

            IOrderedEnumerable<OrderPackage> CurrentOrderPackagesSorted = CurrentOrderPackages.OrderBy(s => true);

            if (!String.IsNullOrEmpty(SortField1) && SortField1 != "[None]")
            {
                CurrentOrderPackagesSorted = CurrentOrderPackagesSorted.ThenBy(s => s.SubjectOrder.Subject.SubjectData[SortField1].ComparisonValue);

                if (!String.IsNullOrEmpty(SortField2) && SortField2 != "[None]")
                    CurrentOrderPackagesSorted = CurrentOrderPackagesSorted.ThenBy(s => s.SubjectOrder.Subject.SubjectData[SortField2].ComparisonValue);
            }

            CurrentOrderPackagesSorted = CurrentOrderPackagesSorted
                .ThenBy(s => s.SubjectOrder.Subject.SubjectData["Last Name"].ComparisonValue)
                .ThenBy(s => s.SubjectOrder.Subject.SubjectData["First Name"].ComparisonValue);

            CurrentOrderPackages = CurrentOrderPackagesSorted.ToList();

            logger.Info("Getting List of Order Subjects");
            int product_unit_count = 0;
            //get a list of subjects for this order
            List<Subject> OrderSubjects = new List<Subject>();
            OrderSubjects = CurrentOrderPackages.Select(os => os.SubjectOrder.Subject).Distinct().ToList();

            if (IncludeAllSubjectsAndImages)
                OrderSubjects = this._flowProject.FlowProjectDataContext.Subjects.ToList();


            logger.Info("Order Contains " + CurrentOrderPackages.Count + " Packages");
            logger.Info("Order Contains " + product_unit_count + " Product Units");

            IOrderedEnumerable<Subject> OrderSubjectsSorted = OrderSubjects.OrderBy(s => true);
            if (!String.IsNullOrEmpty(SortField1) && SortField1 != "[None]")
            {
                OrderSubjectsSorted = OrderSubjectsSorted.ThenBy(s => s.SubjectData[SortField1].ComparisonValue);

                if (!String.IsNullOrEmpty(SortField2) && SortField2 != "[None]")
                    OrderSubjectsSorted = OrderSubjectsSorted.ThenBy(s => s.SubjectData[SortField2].ComparisonValue);
            }

            OrderSubjectsSorted = OrderSubjectsSorted
                .ThenBy(s => s.SubjectData["Last Name"].ComparisonValue)
                .ThenBy(s => s.SubjectData["First Name"].ComparisonValue);

            OrderSubjects = OrderSubjectsSorted.ToList();

            logger.Info("Get list of images for this order");
            //get a list of images for this order


            //lets create a progress bar for iterating over packages since this can take a long long time if images need to be rendered.
            int pkgCount = CurrentOrderPackages.Count;
            List<SubjectImagePlus> OrderSubjectImages = new List<SubjectImagePlus>();
            NotificationProgressInfo prepPackagesProgress = new NotificationProgressInfo("Rendering Images", "Rendering Images", 0, pkgCount, 1);
            IShowsNotificationProgress pNotification = ObjectFactory.GetInstance<IShowsNotificationProgress>();
            prepPackagesProgress.NotificationProgress = pNotification;
            pNotification.ShowNotification(prepPackagesProgress);

            
            int copCount = 0;
            foreach (OrderPackage op in CurrentOrderPackages)
            {
                copCount++;
                prepPackagesProgress.Update("Rendering Images for " + op.SubjectOrder.Subject.FormalFullName + " - " + op.ProductPackageDesc + "(" + copCount + "/" + pkgCount + ")");
                prepPackagesProgress.Step();
                //System.Data.Linq.EntitySet<OrderProduct> oProducts = op.OrderProducts;
                if (op.ProductPackage.IsAnyXPackage)
                {
                    //oProducts = new System.Data.Linq.EntitySet<OrderProduct>();
                    if (op.Units != null && op.Units.Length > 1)
                    {
                        foreach (string id in op.Units.Split(";"))
                        {
                            string map = id.Split("-")[0];
                            ProductPackage pptemp = this._flowProject.FlowCatalog.ProductPackages.FirstOrDefault(ptemp => ptemp.Map == map);
                            if (pptemp != null)
                            {
                                ProductPackageComposition ppctemp = pptemp.ProductPackageCompositions.FirstOrDefault();
                                OrderProduct optemp = new OrderProduct(ppctemp.ImageQuixProduct,op.SubjectOrder ,ppctemp.DisplayLabel, ppctemp.ImageQuixProduct.ResourceURL);

                            if (op.SubjectImage != null) // 1 is the image nodetype
                            {
                                string bground = null;
                                string newImageName = op.SubjectImage.Dp2FileName(UseOriginalFileNames);
                                string newImagePath = op.SubjectImage.ImagePathFullRes;

                                if (ApplyCrop)
                                {
                                    logger.Info("About to apply a crop");
                                    System.Drawing.Image img = System.Drawing.Image.FromFile(newImagePath);
                                    if (op.SubjectImage.Orientation == 90)
                                        img.RotateFlip(RotateFlipType.Rotate90FlipNone);
                                    if (op.SubjectImage.Orientation == 180)
                                        img.RotateFlip(RotateFlipType.Rotate180FlipNone);
                                    if (op.SubjectImage.Orientation == 270)
                                        img.RotateFlip(RotateFlipType.Rotate270FlipNone);

                                    if (((op.SubjectImage.CropW > 0) || (op.SubjectImage.CropH > 0)))
                                        img = cropImage(img, getCropArea(img, op.SubjectImage.CropL, op.SubjectImage.CropT, op.SubjectImage.CropH, op.SubjectImage.CropW));
                                    string tempImageMatchExportPath = System.IO.Path.Combine(FlowContext.FlowTempDirPath, this._flowProject.FileSafeProjectName);
                                    if (!Directory.Exists(tempImageMatchExportPath))
                                        Directory.CreateDirectory(tempImageMatchExportPath);
                                    newImagePath = System.IO.Path.Combine(tempImageMatchExportPath, (new FileInfo(newImagePath)).Name);

                                    if (!File.Exists(newImagePath))
                                        img.Save(newImagePath, System.Drawing.Imaging.ImageFormat.Jpeg);

                                    img.Dispose();
                                    logger.Info("Done apply a crop");
                                }


                                bool isRendered = false;
                                if (ppctemp != null && ppctemp.Renderlayout)
                                {
                                    logger.Info("About to render a layout");

                                    isRendered = true;
                                    //objNode.Add("filename", node.UniqueNodeImageName);
                                    newImageName = op.SubjectOrder.Subject.TicketCode + "_" + optemp.OrderProductID + ".jpg";
                                    newImagePath = Path.Combine(FlowContext.FlowTempDirPath, newImageName);
                                    var orderedSubjectImage = op.SubjectImage;

                                    //this is where we render the layout....
                                    _flowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                                    {
                                        var layoutsDir = this._flowController.ProjectController.FlowMasterDataContext.PreferenceManager.Layout.LayoutsDirectory;
                                        DisplayableFile currentLayoutFile = new DisplayableFile(new FileInfo(Path.Combine(layoutsDir, ppctemp.Layout)));
                                        string gsbg = null;
                                        if (op.GreenScreenBackground != null)
                                            gsbg = op.GreenScreenBackground;

                                        using (MemoryStream ms = RenderLayoutPreviewToMs(op.SubjectOrder.Subject, orderedSubjectImage, currentLayoutFile, gsbg))
                                        using (Bitmap bmp = new Bitmap(ms))
                                        {
                                            bmp.Save(newImagePath);
                                        }
                                    }));

                                }
                                else if (this._flowProject.IsGreenScreen == true)
                                {
                                    if (op.GreenScreenBackground != null && File.Exists(op.GreenScreenBackground))
                                        bground = op.GreenScreenBackground;
                                    else if (this._flowProject.DefaultGreenScreenBackground != null && File.Exists(this._flowProject.DefaultGreenScreenBackground))
                                        bground = this._flowProject.DefaultGreenScreenBackground;
                                    else if (this._flowProject.FlowProjectDataContext.FlowMasterDataContext.GreenScreenBackgrounds.Count > 0 && this._flowProject.FlowProjectDataContext.FlowMasterDataContext.GreenScreenBackgrounds[0] != null && File.Exists(this._flowProject.FlowProjectDataContext.FlowMasterDataContext.GreenScreenBackgrounds[0].FullPath))
                                        bground = this._flowProject.FlowProjectDataContext.FlowMasterDataContext.GreenScreenBackgrounds[0].FullPath;

                                    if (RenderGreenScreen)
                                    {
                                        logger.Info("About to render greenscreen");
                                        newImageName = op.SubjectImage.GetDp2FileName(bground, UseOriginalFileNames, RenderGreenScreen);
                                        bool isRenderingGS = true;
                                        _flowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                                        {
                                            newImagePath = op.SubjectImage.GetImagePath(true, bground, newImagePath);
                                            Thread.Sleep(500);
                                            isRenderingGS = false;
                                        }));

                                        int loopCnt = 0;
                                        while (isRenderingGS && loopCnt++ < 45)
                                        {
                                            Thread.Sleep(1000);
                                        }
                                        Thread.Sleep(500);
                                    }
                                }

                           

                                logger.Info("About to apply a crop");
                                SubjectImagePlus sip = new SubjectImagePlus(op.SubjectImage, bground, newImageName, newImagePath, isRendered);
                                if (!OrderSubjectImages.Any(b => b.SubjectImage == sip.SubjectImage && b.Background == sip.Background && b.NewImagePath == sip.NewImagePath))
                                    OrderSubjectImages.Add(sip);
                            }



                            }
                        }
                    }
                }
                else
                {
                    foreach (OrderProduct prod in op.OrderProducts)
                    {
                        int nodeLoop = 0;
                        foreach (OrderProductNode node in prod.OrderProductNodes)
                        {
                            nodeLoop++;
                            if (node.SubjectImage != null && node.ImageQuixProductNodeType == 1) // 1 is the image nodetype
                            {
                                string bground = null;
                                string newImageName = node.SubjectImage.Dp2FileName(UseOriginalFileNames);
                                string newImagePath = node.SubjectImage.ImagePathFullRes;
                                if (prod.ProductPackageComposition == null && !string.IsNullOrEmpty(prod.ResourceURL))
                                    prod.UpdateProductPackageComposition(prod.ResourceURL);
                                if ((prod.ProductPackageComposition != null && prod.ProductPackageComposition.Renderlayout == false) || string.IsNullOrEmpty(prod.ResourceURL))
                                { //only do this if its not a rendered layout
                                    prod.UpdateResourceURL(this._flowController.ProjectController.FlowMasterDataContext.ImageQuixProducts.FirstOrDefault(iqp => iqp.PrimaryKey == prod.ImageQuixProductPk));
                           
                                    if (string.IsNullOrEmpty(prod.ResourceURL))
                                    {
                                        int trycount = 0;
                                        while (trycount++ < 5)
                                        {
                                            try
                                            {
                                                logger.Info("missing prod resource url, trying to correct it");
                                                //prod.UpdateResourceURL(this._flowController.ProjectController.FlowMasterDataContext.ImageQuixProducts.FirstOrDefault(iqp => iqp.PrimaryKey == prod.ImageQuixProductPk));
                                                ImageQuixProduct iqprod = this._flowController.ProjectController.FlowMasterDataContext.ImageQuixProducts.FirstOrDefault(iqp => iqp.PrimaryKey == prod.ImageQuixProductPk);
                                                logger.Info("missing prod resource url: " + iqprod.ResourceURL);
                                                prod.ResourceURL = iqprod.ResourceURL;
                                                logger.Info("missing prod resource url, done correcting");
                                                trycount = 6;
                                            }
                                            catch (Exception e)
                                            {
                                                logger.Info("missing prod resource url, failed buy will retry: " + trycount);
                                                Thread.Sleep(500);
                                                continue;
                                                //do nothing for now

                                            }
                                        }
                                    }
                                }

                              
                                //if this is a GS project, lets get the knocked out png
                                if (this._flowProject.IsGreenScreen == true && (node.SubjectImage.IsGreenscreen || node.SubjectImage.IsPng))
                                {
                                    this._flowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                                    {
                                        if (node.SubjectImage.IsPng)
                                            newImagePath = node.SubjectImage.ImagePathFullRes;
                                        else
                                        {
                                            newImagePath = node.SubjectImage.GetRenderedPngImageFilename(prod.GreenScreenBackground);
                                        }
                                    }));
                                }

                                if (ApplyCrop)
                                {
                                    

                                    logger.Info("About to apply a crop");
                                    System.Drawing.Image img = System.Drawing.Image.FromFile(newImagePath);
                                    if (!newImagePath.ToLower().EndsWith(".png")) //png files do not need to be rotated
                                    {
                                        if (node.SubjectImage.Orientation == 90)
                                            img.RotateFlip(RotateFlipType.Rotate90FlipNone);
                                        if (node.SubjectImage.Orientation == 180)
                                            img.RotateFlip(RotateFlipType.Rotate180FlipNone);
                                        if (node.SubjectImage.Orientation == 270)
                                            img.RotateFlip(RotateFlipType.Rotate270FlipNone);
                                    }

                                    if (((node.SubjectImage.CropW > 0) || (node.SubjectImage.CropH > 0)))
                                        img = cropImage(img, getCropArea(img, node.SubjectImage.CropL, node.SubjectImage.CropT, node.SubjectImage.CropH, node.SubjectImage.CropW));
                                    //FileInfo source = new FileInfo(si.ImagePath);


                                    string tempImageMatchExportPath = System.IO.Path.Combine(FlowContext.FlowTempDirPath, this._flowProject.FileSafeProjectName);
                                    if (!Directory.Exists(tempImageMatchExportPath))
                                        Directory.CreateDirectory(tempImageMatchExportPath);
                                    newImagePath = System.IO.Path.Combine(tempImageMatchExportPath, (new FileInfo(newImagePath)).Name);
                                    
                                    if (!File.Exists(newImagePath))
                                    {
                                        if (newImagePath.ToLower().EndsWith("png"))
                                            img.Save(newImagePath, System.Drawing.Imaging.ImageFormat.Png);
                                        else
                                        {
                                            ImageCodecInfo jpgEncoder = GetEncoder(ImageFormat.Jpeg);
                                            System.Drawing.Imaging.Encoder myEncoder = System.Drawing.Imaging.Encoder.Quality;
                                            EncoderParameters myEncoderParameters = new EncoderParameters(1);
                                            EncoderParameter myEncoderParameter = new EncoderParameter(myEncoder, 100L);
                                            myEncoderParameters.Param[0] = myEncoderParameter;
                                            img.Save(newImagePath, jpgEncoder, myEncoderParameters);
                                            //img.Save(newImagePath, System.Drawing.Imaging.ImageFormat.Jpeg);
                                        }
                                    }

                                    img.Dispose();
                                    logger.Info("Done apply a crop");
                                }


                                bool isRendered = false;
                                if (prod.ProductPackageComposition != null && prod.ProductPackageComposition.Renderlayout)
                                {
                                    logger.Info("About to render a layout");
                                    if (nodeLoop > 1)
                                        break;

                                    isRendered = true;
                                    //objNode.Add("filename", node.UniqueNodeImageName);
                                    newImageName = node.OrderProduct.UniqueProductImageName;
                                    newImagePath = Path.Combine(FlowContext.FlowTempDirPath, newImageName);
                                    var orderedSubjectImage = prod.OrderProductNodes
                                        .FirstOrDefault(n => n.SubjectImage != null).SubjectImage;

                                    //this is where we render the layout....
                                    _flowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                                    {
                                        var layoutsDir = this._flowController.ProjectController.FlowMasterDataContext.PreferenceManager.Layout.LayoutsDirectory;
                                        DisplayableFile currentLayoutFile = new DisplayableFile(new FileInfo(Path.Combine(layoutsDir, prod.ProductPackageComposition.Layout)));

                                        string gsbg = null;
                                        if (prod.GreenScreenBackground != null)
                                            gsbg = prod.GreenScreenBackground;

                                        using (MemoryStream ms = RenderLayoutPreviewToMs(op.SubjectOrder.Subject, orderedSubjectImage, currentLayoutFile, gsbg))
                                        using (Bitmap bmp = new Bitmap(ms))
                                        {
                                            bmp.Save(newImagePath);
                                        }
                                    }));

                                }
                                else if (this._flowProject.IsGreenScreen == true)
                                {
                                
                                    if (prod.GreenScreenBackground != null && File.Exists(prod.GreenScreenBackground))
                                        bground = prod.GreenScreenBackground;
                                    else if (node.SubjectImage.GreenScreenBackground != null && File.Exists(node.SubjectImage.GreenScreenBackground))
                                        bground = node.SubjectImage.GreenScreenBackground;
                                    else if (this._flowProject.DefaultGreenScreenBackground != null && File.Exists(this._flowProject.DefaultGreenScreenBackground))
                                        bground = this._flowProject.DefaultGreenScreenBackground;
                                    else if (this._flowProject.FlowProjectDataContext.FlowMasterDataContext.GreenScreenBackgrounds.Count > 0 && this._flowProject.FlowProjectDataContext.FlowMasterDataContext.GreenScreenBackgrounds[0] != null && File.Exists(this._flowProject.FlowProjectDataContext.FlowMasterDataContext.GreenScreenBackgrounds[0].FullPath))
                                        bground = this._flowProject.FlowProjectDataContext.FlowMasterDataContext.GreenScreenBackgrounds[0].FullPath;

                                    if (RenderGreenScreen)
                                    {
                                        logger.Info("About to render greenscreen");
                                        newImageName = node.SubjectImage.GetDp2FileName(bground, UseOriginalFileNames, true);
                                        
                                        bool isRenderingGS = true;
                                        _flowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                                        {
                                            newImagePath = node.SubjectImage.GetImagePath(true, bground, newImagePath);
                                            Thread.Sleep(500);
                                            isRenderingGS = false;                                    
                                        }));

                                        int loopCnt = 0;
                                        while (isRenderingGS && loopCnt++ < 45)
                                        {
                                            Thread.Sleep(1000);
                                        }
                                        Thread.Sleep(500);
                                    }
                                }

                           

                                logger.Info("About to apply a crop");
                                SubjectImagePlus sip = new SubjectImagePlus(node.SubjectImage, bground, newImageName, newImagePath, isRendered);
                                if (!OrderSubjectImages.Any(b => b.SubjectImage == sip.SubjectImage && b.Background == sip.Background && b.NewImagePath == sip.NewImagePath))
                                    OrderSubjectImages.Add(sip);
                            }
                        }
                    }
                    //do not include group images for now
                    //foreach (SubjectImage si in op.SubjectOrder.Subject.ImageList.Where(i => i.IsGroupImage == true))
                    //{
                    //    SubjectImagePlus sip = new SubjectImagePlus(si, null, si.ImageFileName, si.ImagePathFullRes, false);
                    //    if (!OrderSubjectImages.Any(b => b.NewImagePath == sip.NewImagePath))
                    //        OrderSubjectImages.Add(sip);
                    //}
                }
            }
            prepPackagesProgress.Complete("Done Rendering Images");

            if (IncludeAllSubjectsAndImages)
            {
                List<SubjectImage> sis = _flowProject.FlowProjectDataContext.SubjectImages.ToList();
                if (this._flowProject.FlowProjectDataContext.OrderFilterFilteredSubjects)
                {
                    _flowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                   {
                       sis = this._flowProject.GetFilteredSubjectList().SelectMany(s => s.SubjectImages).ToList();
                   }));
                }

                foreach (SubjectImage si in sis)
                {
                    string bground = null;
                    string newImageName = si.Dp2FileName(UseOriginalFileNames);
                    string newImagePath = si.ImagePath;
                    if (RenderGreenScreen)
                    {
                        if (si.GreenScreenBackground != null && File.Exists(si.GreenScreenBackground))
                            bground = si.GreenScreenBackground;
                        else if (si.GreenScreenBackground != null && File.Exists(si.GreenScreenBackground))
                            bground = si.GreenScreenBackground;
                        else if (this._flowProject.DefaultGreenScreenBackground != null && File.Exists(this._flowProject.DefaultGreenScreenBackground))
                            bground = this._flowProject.DefaultGreenScreenBackground;
                        else if (this._flowProject.FlowProjectDataContext.FlowMasterDataContext.GreenScreenBackgrounds.Count > 0 && this._flowProject.FlowProjectDataContext.FlowMasterDataContext.GreenScreenBackgrounds[0] != null && File.Exists(this._flowProject.FlowProjectDataContext.FlowMasterDataContext.GreenScreenBackgrounds[0].FullPath))
                            bground = this._flowProject.FlowProjectDataContext.FlowMasterDataContext.GreenScreenBackgrounds[0].FullPath;

                        newImageName = si.GetDp2FileName(bground, UseOriginalFileNames, RenderGreenScreen);
                        _flowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                        {
                            newImagePath = si.GetImagePath(true, bground);
                        }));
                    }

                    if (ApplyCrop)
                    {
                        System.Drawing.Image img = System.Drawing.Image.FromFile(newImagePath);
                        if (si.Orientation == 90)
                            img.RotateFlip(RotateFlipType.Rotate90FlipNone);
                        if (si.Orientation == 180)
                            img.RotateFlip(RotateFlipType.Rotate180FlipNone);
                        if (si.Orientation == 270)
                            img.RotateFlip(RotateFlipType.Rotate270FlipNone);

                        if (((si.CropW > 0) || (si.CropH > 0)))
                            img = cropImage(img, getCropArea(img, si.CropL, si.CropT, si.CropH, si.CropW));
                        //FileInfo source = new FileInfo(si.ImagePath);
                        string tempImageMatchExportPath = System.IO.Path.Combine(FlowContext.FlowTempDirPath, this._flowProject.FileSafeProjectName);
                        if (!Directory.Exists(tempImageMatchExportPath))
                            Directory.CreateDirectory(tempImageMatchExportPath);
                        newImagePath = System.IO.Path.Combine(tempImageMatchExportPath, (new FileInfo(newImagePath)).Name);

                        if (!File.Exists(newImagePath))
                            img.Save(newImagePath, System.Drawing.Imaging.ImageFormat.Jpeg);
                        img.Dispose();
                    }
                    SubjectImagePlus sip = new SubjectImagePlus(si, bground, newImageName, newImagePath, false);
                    if (!OrderSubjectImages.Any(b => b.SubjectImage == sip.SubjectImage && b.Background == sip.Background && b.NewImagePath == sip.NewImagePath))
                        OrderSubjectImages.Add(sip);
                }

            }

            logger.Info("About to upload order to lab");
            int estimatedSteps = 4; //generate 4 reports
            estimatedSteps += 1; //generate json
            estimatedSteps += 1; //send json
            estimatedSteps += 5; // files to ftp
            estimatedSteps += OrderSubjectImages.Count; //images to ftp

            List<string> filesToUpload = new List<string>();

            NotificationInfo = new NotificationProgressInfo("Uploading Order To Lab", "Preparing Upload...", 0, estimatedSteps, 1);

            IShowsNotificationProgress progressNotification = ObjectFactory.GetInstance<IShowsNotificationProgress>();
            NotificationInfo.NotificationProgress = progressNotification;
            progressNotification.ShowNotification(NotificationInfo);

            Organization tempShipOrg;
            if (orgForBulkShipping != null)
            {
                tempShipOrg = orgForBulkShipping;
            }
            else
            {
                tempShipOrg = new Organization();
                tempShipOrg.OrganizationShippingAddressLine1 = CurrentOrderPackages[0].ShippingAddress;
                tempShipOrg.OrganizationShippingCity = CurrentOrderPackages[0].ShippingCity;
                tempShipOrg.OrganizationShippingStateCode = CurrentOrderPackages[0].ShippingState;
                if(CurrentOrderPackages[0].ShippingZip != null) tempShipOrg.OrganizationShippingZipCode = CurrentOrderPackages[0].ShippingZip.Trim();

                if (String.IsNullOrEmpty(tempShipOrg.OrganizationShippingZipCode) && CurrentOrderPackages[0].SubjectOrder.ShippingZipCode != null) tempShipOrg.OrganizationShippingZipCode = CurrentOrderPackages[0].SubjectOrder.ShippingZipCode.Trim();

                tempShipOrg.OrganizationName = CurrentOrderPackages[0].ShippingFirstName + " " + CurrentOrderPackages[0].ShippingLastName;

            }


            //must be a digit
            //string orderID = TicketGenerator.GenerateNumericTicketString(8).Trim();
            string orderID = "0";

            JsonObject rootOrder = new JsonObject();
            ///
            /// DP2 order
            ///
            if (isDp2)
            {
                logger.Info("Generate Order JSON");
                //generate the order json
                NotificationInfo.Update("Generate Order JSON");
                NotificationInfo.Step();

                rootOrder.Add("name", _flowProject.FlowProjectName);
                rootOrder.Add("labCustomerId", _flowProject.Studio.LabCustomerID);
                //rootOrder.Add("order-date", DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"));

                if (isPlic)
                {
                    JsonArray orderOptions = new JsonArray();
                    if (OrderOptionList != null)
                    {
                        foreach (Flow.View.Project.OrderOptionSelected oos in OrderOptionList)
                        {
                            JsonObject objOption = new JsonObject();
                            if (isPlic)
                            {
                                objOption.Add("optionGroupID", oos.OptionGroupLabel);
                                objOption.Add("optionID", oos.OptionLabel);
                            }

                            orderOptions.Add(objOption);
                        }
                    }
                    rootOrder.Add("orderOptionSelections", orderOptions);
                }
                else
                {

                    rootOrder.Add("orderOptionSelections", getOrderOptionSelections(CurrentOrderPackages[0]));
                }
                string shipmentOption = getShipmentOptionSelections();
                if (string.IsNullOrEmpty(shipmentOption))
                    rootOrder.Add("shipmentOption", JsonNull.Null);
                else
                {
                    //doing this for just one customer who has a bad shipping option url
                    if (shipmentOption.Contains("catalog/34/"))
                        shipmentOption = shipmentOption.Replace("catalog/34/", "");

                    rootOrder.Add("shipmentOption", shipmentOption);
                }

                if (orgForBulkShipping != null)
                {
                   getCustomerInformation(orgForBulkShipping, ref rootOrder);
                }
                else
                {
                    getCustomerInformation(CurrentOrderPackages[0], this._flowProject.Studio, ref rootOrder);
                }
                JsonArray entries = null;
                try
                {
                    entries = getEntries(CurrentOrderPackages, isPlic);
                }
                catch (Exception ex)
                {
                    _flowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                   {
                       FlowMessageBox msg = new FlowMessageBox("Error with Subject", ex.Message);
                       msg.CancelButtonVisible = false;
                       msg.ShowDialog();
                   }));
                    NotificationInfo.Message = "Error Submitting an Order";
                    NotificationInfo.ShowDoneButton();

                    logger.ErrorException("Error with subject", ex);

                    return null;
                }
                if (CurrentOrderPackages[0].IQOrderID != null && CurrentOrderPackages[0].IQOrderID > 0)
                    rootOrder.Add("IQOrderID", CurrentOrderPackages[0].IQOrderID.ToString());

                if (CurrentOrderPackages[0].WebOrderID != null && CurrentOrderPackages[0].WebOrderID > 0)
                    rootOrder.Add("WebOrderID", CurrentOrderPackages[0].WebOrderID.ToString());

                if (CurrentOrderPackages[0].Notes != null && CurrentOrderPackages[0].Notes.Length > 0)
                    rootOrder.Add("Notes", CurrentOrderPackages[0].Notes.ToString());
                rootOrder.Add("entries", entries);
                rootOrder.Add("imageData", getImageData(OrderSubjectImages, ApplyCrop));
                if(isPlic)
                    rootOrder.Add("metadata", getMetaDataPLIC(OrderSubjects, CurrentOrderPackages, OrderSubjectImages, isPlic));
                else
                    rootOrder.Add("metadata", getMetaData(OrderSubjects, CurrentOrderPackages, OrderSubjectImages, isPlic));

                JsonWriter writert = new JsonWriter();
                rootOrder.Write(writert);
                string jsonDatat = writert.ToString().Replace("\\/", "/");
                string jsonDataFilePath = FlowContext.Combine(FlowContext.FlowTempDirPath, "JSON_ORDER_temp" + TicketGenerator.GenerateTicketString(6) + ".txt");
                StreamWriter swt = File.CreateText(jsonDataFilePath);
                swt.Write(jsonDatat);
                swt.Close();


                logger.Info("Submit Order JSON to Lab Server");
                //submit the order json to get the order id
                NotificationInfo.Update("Submit Order JSON to Lab Server");
                NotificationInfo.Step();
                string orderUri = _flowProject.FlowMasterDataContext.PreferenceManager.Upload.OrderSubmission.ImageQuixOrderSubmissionUrl;
                string username = _flowProject.FlowMasterDataContext.PreferenceManager.Upload.CatalogRequest.GetCatalogUsername;
                string password = _flowProject.FlowMasterDataContext.PreferenceManager.Upload.CatalogRequest.GetCatalogPassword;
                JsonObject SubmitReturnObject = new JsonObject();

                try
                {
                    if (isPlic)
                    {
                        string baseURL = _flowProject.FlowMasterDataContext.PreferenceManager.Upload.CatalogRequest.ImageQuixBaseUrl.Split("/catalogs/")[0];
                        string catalogGuid = _flowProject.FlowMasterDataContext.PreferenceManager.Upload.CatalogRequest.ImageQuixBaseUrl.Split("/catalogs/")[1];
                        orderUri = baseURL + "/orders";

                        //string rA = SubmitJsonOrderPlic(rootOrder, orderUri, username, password, catalogGuid);
                        string rA = SubmitJsonOrderPlic(new JsonObject(), orderUri, username, password, catalogGuid);
                        JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                        OrderResults orderResults = json_serializer.Deserialize<OrderResults>(rA);
                        orderID = orderResults.order_short.sequential_id.ToString();
                    }
                    else
                    {
                        SubmitReturnObject = SubmitJsonOrder(rootOrder, orderUri, username, password);
                        orderID = SubmitReturnObject["orderID"].ToString();
                    }
                }
                catch (Exception ex)
                {
                    _flowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                    {
                        FlowMessageBox msg = new FlowMessageBox("Error Sending Order", "Flow is unable to connect to the Lab's Server.\nServer: " + orderUri + "\n\n" + ex.Message);
                        msg.CancelButtonVisible = false;
                        msg.ShowDialog();
                    }));
                    NotificationInfo.Cancel();
                    logger.ErrorException("Unable to connect to the Lab's Server to send order", ex);
                    return null;
                }
               

            }
            else
            {
                //get the flow Order ID from flow admin
                try
                {
                    Lib.Web.PostSubmitter PostSubmitter = new Lib.Web.PostSubmitter();
                    //PostSubmitter.Url = @"http://localhost:64844/orderseq";
                    PostSubmitter.Url = @"http://www.flowadmin.com/activationservice/orderseq";
                    PostSubmitter.Type = Lib.Web.PostSubmitter.PostTypeEnum.Post;
                    string resultMsgStr = PostSubmitter.Post();

                    if (resultMsgStr != null)
                    {
                        orderID = resultMsgStr;
                    }
                    double temp;
                    if (orderID == "0" || !double.TryParse(orderID, out temp))
                        orderID = "2" + TicketGenerator.GenerateNumericTicketString(6).Trim();
                }
                catch
                {
                    orderID = "2" + TicketGenerator.GenerateNumericTicketString(6).Trim();
                }
            }








            //Define Temp Folder
            string OrderTempPath = FlowContext.Combine(FlowContext.FlowTempDirPath, "ORDER_" + orderID);
            if (!Directory.Exists(OrderTempPath))
                Directory.CreateDirectory(OrderTempPath);

            if (isDp2)
            {
                // Create text File containing json order
                logger.Info("Write Order JSON Disk");
                NotificationInfo.Update("Write Order JSON Disk");
                JsonWriter writert = new JsonWriter();
                rootOrder.Write(writert);
                string jsonDatat = writert.ToString().Replace("\\/", "/");
                string jsonDataFilePath = FlowContext.Combine(OrderTempPath, "JSON_ORDER_" + orderID + ".txt");
                StreamWriter swt = File.CreateText(jsonDataFilePath);
                swt.Write(jsonDatat);
                swt.Close();
                filesToUpload.Add(jsonDataFilePath);

                try
                {
                    SubmitJsonOrderAnalytics(orderID, jsonDataFilePath);
                }
                catch (Exception ex)
                {
                    // Log it but continue (so the rest of the order can continue)
                    logger.ErrorException("Unable to send order analytics", ex);
                }
            }

            //Generate ImageMatch File
            NotificationInfo.Update("Creating ImageMatch data file");
            NotificationInfo.Step();

            _flowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
            {
                try
                {
                    string ImageMatchDataFileName = System.IO.Path.Combine(OrderTempPath, "ImageMatchData_" + orderID + ".txt");
                    CustomReports.CreateImageMatchDataFile(_flowController.ProjectController.CurrentFlowProject, orderID, OrderSubjects, CurrentOrderPackages, OrderSubjectImages, ImageMatchDataFileName, RenderGreenScreen);
                    filesToUpload.Add(ImageMatchDataFileName);
                }
                catch (Exception ex)
                {
                    // Ignore it, but log it
                    logger.WarnException("Ignoring a caught exception: ", ex);
                }
            }));


            if (!isDp2)
            {
                string prmFile = System.IO.Path.Combine(OrderTempPath, "FlowCatalog.prm");
                this._flowController.ProjectController.CurrentFlowProject.FlowCatalog.WritePRMData(prmFile);
                filesToUpload.Add(prmFile);
            }

            XmlDocument tdoc = new XmlDocument();
            XmlDeclaration dec = tdoc.CreateXmlDeclaration("1.0", null, null);
            tdoc.AppendChild(dec);// Create the root element
            XmlElement orderForm = tdoc.CreateElement("OrderForm");
            tdoc.AppendChild(orderForm);
            foreach (OrderFormFieldLocal field in _flowController.ProjectController.CurrentFlowProject.FlowProjectDataContext.OrderFormFieldLocals)
            {
                if (!string.IsNullOrWhiteSpace(field.FieldName))
                {
                    XmlElement thisField = tdoc.CreateElement(Flow.Util.RemoveSpecialCharacters(field.FieldName));
                    thisField.InnerText = field.SelectedValue;
                    orderForm.AppendChild(thisField);
                }
            }

            //create cropdata file
            logger.Info("Creating CropData file");
            NotificationInfo.Update("Creating CropData file");
            NotificationInfo.Step();
            _flowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
            {
                string cropDataFile = System.IO.Path.Combine(OrderTempPath, "CropData.txt");
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(cropDataFile))
                {
                    file.WriteLine("ImageName\tImagePath\tCropL\tCropT\tCropW\tCropH\tCropErrors\tRotation");

                    foreach (SubjectImagePlus sip in OrderSubjectImages)
                    {
                        file.WriteLine(sip.NewImageName + "\t" + sip.NewImageName + "\t" + sip.SubjectImage.CropL + "\t" + sip.SubjectImage.CropT + "\t" + sip.SubjectImage.CropW + "\t" + sip.SubjectImage.CropH);
                    }
                }

                filesToUpload.Add(cropDataFile);
            }));


             int keepTryingCnt = 0;
            while (keepTryingCnt < 5)
            {
                keepTryingCnt++;
                try
                {
            //create reports
            logger.Info("Creating Billing XML Report");
            NotificationInfo.Update("Creating Billing XML Report");
            NotificationInfo.Step();
            bool doneWithReport = false;
            _flowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
            {
                string billingReportFileName = System.IO.Path.Combine(OrderTempPath, "BillingReport_" + orderID + ".xml");
                XmlDocument doc = CustomReports.GetXMLBillingReport(_flowController.ProjectController.CurrentFlowProject, orderID, CurrentOrderPackages, OrderSubjectImages, tempShipOrg, orderForm);
               
                doc.Save(billingReportFileName);
                filesToUpload.Add(billingReportFileName);
                doneWithReport = true;
            }));
            int waitCnt = 0;
            while (doneWithReport == false)
            {
                Thread.Sleep(1000);
                if (waitCnt++ > 30)
                    break;
            }
            keepTryingCnt = 100;
                }
                catch (Exception e)
                {

                }
            }


            keepTryingCnt = 0;
            while (keepTryingCnt < 5)
            {
                keepTryingCnt++;
                try
                {
                    //create reports
                    logger.Info("Creating Billing PDF Report");
                    NotificationInfo.Update("Creating Billing PDF Report");
                    NotificationInfo.Step();
                    bool doneWithReport = false;
                    _flowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                    {
                        string billingReportFileName = System.IO.Path.Combine(OrderTempPath, "BillingReport_" + orderID + ".pdf");
                        Dictionary<string, Dictionary<string, List<object>>> AllUnitCatagories = null;
                        C1PrintDocument doc = CustomReports.GetBillingReport(_flowController.ProjectController.CurrentFlowProject, orderID, CurrentOrderPackages, OrderSubjectImages, tempShipOrg, out AllUnitCatagories);
                        //doc.Save(billingReportFileName, C1DocumentFormatEnum.C1d,);
                        ReportDataDefinitions.ExportReport(
                            "BillingReport_" + orderID + ".pdf",
                            doc, new KeyValuePair<string, string>("PDF", "Adobe PDF (*.pdf)|*.pdf"),
                            billingReportFileName, false
                        );

                        filesToUpload.Add(billingReportFileName);
                        doneWithReport = true;
                    }));
            int waitCnt = 0;
            while (doneWithReport == false)
            {
                Thread.Sleep(1000);
                if (waitCnt++ > 30)
                    break;
            }
            keepTryingCnt = 100;
                }
                catch (Exception e)
                {

                }
            }

            string orderFormFileName = System.IO.Path.Combine(OrderTempPath, "OrderForm_" + orderID + ".pdf");
            keepTryingCnt = 0;
            while (keepTryingCnt < 5)
            {
                keepTryingCnt++;
                try
                {
                    logger.Info("Creating Order Form pdf");
                    NotificationInfo.Update("Creating Order Form pdf");
                    NotificationInfo.Step();
                    //Send Order Form
                    
                    bool doneWithReport = false;
                    _flowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                    {
                        _flowController.ProjectController.CurrentFlowProject.SelectedReportName = "Order Form";

                        FlowReportView newReportView = new FlowReportView("Order Form", _flowController.ProjectController.CurrentFlowProject);

                        newReportView.ShippingOrganization = tempShipOrg;

                        ReportDataDefinitions.GetReportData(newReportView, _flowController.ProjectController.CurrentFlowProject.FlowProjectDataContext.FilteredOrderPackages.ToList());
                        FlowReport report = new FlowReport(newReportView);
                        report.OrderNumber = orderID;
                        report.SortString = SortField1 + ", " + SortField2 + ", LastName, FirstName";
                        report.Generate();


                        ReportDataDefinitions.ExportReport(
                            _flowController.ProjectController.CurrentFlowProject.SelectedReportName + " - " + _flowController.ProjectController.CurrentFlowProject.FlowProjectName,
                            report.C1Document, new KeyValuePair<string, string>("PDF", "Adobe PDF (*.pdf)|*.pdf"),
                            orderFormFileName, false
                        );
                        filesToUpload.Add(orderFormFileName);
                        doneWithReport = true;
                    }));
                    int waitCnt = 0;
                    while (doneWithReport == false)
                    {
                        Thread.Sleep(1000);
                        if (waitCnt++ > 30)
                            break;
                    }
                    keepTryingCnt = 100;
                }
                catch (Exception e)
                {
                   
                }
            }


            keepTryingCnt = 0;
            while (keepTryingCnt < 5)
            {
                keepTryingCnt++;
                try
                {
            logger.Info("Creating Catalog Package pdf");
            NotificationInfo.Update("Creating Catalog Package pdf");
            NotificationInfo.Step();
            //Send Catalog Summary
            bool doneWithReport = false;
            _flowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
            {
                _flowController.ProjectController.CurrentFlowProject.ReportShowBarCodes = false;
                _flowController.ProjectController.CurrentFlowProject.ReportShowProducts = true;
                _flowController.ProjectController.CurrentFlowProject.ReportShowImageOptions = true;

                string catalogPackagesFileName = System.IO.Path.Combine(OrderTempPath, "CatalogPackages.pdf");
                //C1PrintDocument doc = CustomReports.GetBillingReport(_flowController.ProjectController.CurrentFlowProject, this.OrderFilter, orderID);
                C1PrintDocument doc = CustomReports.GetCatalogReport(_flowController.ProjectController.CurrentFlowProject.FlowCatalog, _flowController.ProjectController.CurrentFlowProject.ReportShowBarCodes, _flowController.ProjectController.CurrentFlowProject.ReportShowProducts, _flowController.ProjectController.CurrentFlowProject.ReportShowImageOptions);

                //doc.Save(billingReportFileName, C1DocumentFormatEnum.C1d,);
                ReportDataDefinitions.ExportReport(
                    "BillingReport_" + orderID + ".pdf",
                    doc, new KeyValuePair<string, string>("PDF", "Adobe PDF (*.pdf)|*.pdf"),
                    catalogPackagesFileName, false
                );

                filesToUpload.Add(catalogPackagesFileName);

                doneWithReport = true;
            }));
            int waitCnt = 0;
            while (doneWithReport == false)
            {
                Thread.Sleep(1000);
                if (waitCnt++ > 30)
                    break;
            }
            keepTryingCnt = 100;
                }
                catch (Exception e)
                {

                }
            }



             keepTryingCnt = 0;
            while (keepTryingCnt < 5)
            {
                keepTryingCnt++;
                try
                {
            logger.Info("Creating Subject Orders Detailed pdf");
            NotificationInfo.Update("Creating Subject Orders Detailed pdf");
            NotificationInfo.Step();

            bool doneWithReport = false;
            _flowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
            {
                try
                {
                    _flowController.ProjectController.CurrentFlowProject.ReportShowPrice = false;
                    //_flowController.ProjectController.CurrentFlowProject.SelectedReportOrderFilter = OrderFilter;
                    _flowController.ProjectController.CurrentFlowProject.SelectedReportName = "Subject Orders Detailed";

                    FlowReportView newReportView = new FlowReportView("Subject Orders Detailed", _flowController.ProjectController.CurrentFlowProject);

                    ObservableCollection<ProjectSubjectField> subjectFields = new ObservableCollection<ProjectSubjectField>();
                    if (!String.IsNullOrEmpty(SortField1) && SortField1 != "[None]")
                        subjectFields.Add(_flowController.ProjectController.CurrentFlowProject.ProjectSubjectFieldList.First(sfl => sfl.SubjectFieldDisplayName == SortField1));
                    if (!String.IsNullOrEmpty(SortField1) && SortField2 != "[None]")
                        subjectFields.Add(_flowController.ProjectController.CurrentFlowProject.ProjectSubjectFieldList.First(sfl => sfl.SubjectFieldDisplayName == SortField2));

                    newReportView.SubjectFieldList = subjectFields;
                    newReportView.ShippingOrganization = tempShipOrg;
                    ReportDataDefinitions.GetReportData(newReportView, CurrentOrderPackages);

                    FlowReport report = new FlowReport(newReportView);

                    report.ApplicablePackages = CurrentOrderPackages;
                    report.ApplicableSubjects = OrderSubjects;
                    report.Generate();

                    string reportFileName = System.IO.Path.Combine(OrderTempPath, "SubjectOrdersDetailed.pdf");
                    ReportDataDefinitions.ExportReport(
                        _flowController.ProjectController.CurrentFlowProject.SelectedReportName + " - " + _flowController.ProjectController.CurrentFlowProject.FlowProjectName,
                        report.C1Document, new KeyValuePair<string, string>("PDF", "Adobe PDF (*.pdf)|*.pdf"),
                        reportFileName, false
                    );
                    filesToUpload.Add(reportFileName);
                }
                catch (Exception ex)
                {
                    logger.Error("FAILED SUBJECT ORDER DETAIL REPORT: " + ex.Message);
                    //if something failed, we wont include this report
                }
                doneWithReport = true;
            }));

            int waitCnt = 0;
            while (doneWithReport == false)
            {
                Thread.Sleep(1000);
                if (waitCnt++ > 30)
                    break;
            }
            keepTryingCnt = 100;
                }
                catch (Exception e)
                {

                }
            }


            //add all the subject Images to the list of files to upload

            string doneFileFilePath = FlowContext.Combine(FlowContext.FlowTempDirPath, "done.txt");
            if (!File.Exists(doneFileFilePath))
                (File.Create(doneFileFilePath)).Close();

            //submit each file via ftp or save files locally
            if (!String.IsNullOrEmpty(SaveOrderPath) && Directory.Exists(SaveOrderPath))
            {
                //is this being queued for lab upload?
                bool isLabUpload = false;
                if (SaveOrderPath == this._flowProject.FlowMasterDataContext.PreferenceManager.Application.LabOrderQueueDirectory)
                    isLabUpload = true;

                string destdir = Path.Combine(SaveOrderPath, "order_" + orderID);
                if (!Directory.Exists(destdir)) Directory.CreateDirectory(destdir);

                foreach (string file in filesToUpload)
                {
                    string fileName = (new FileInfo(file)).Name;
                    logger.Info("Saving File: fileName='{0}'", fileName);
                    NotificationInfo.Update("Saving File " + fileName);
                    NotificationInfo.Step();
                    string destFile = Path.Combine(destdir, fileName);
                    int cnt = 1;
                    while (File.Exists(destFile))
                    {
                        string ext = fileName.Split(".").Last();
                        string newFileName = fileName.Replace("." + ext, "_" + cnt++ + "." + ext);
                        destFile = Path.Combine(destdir, newFileName);
                    }

                    File.Copy(file, destFile);
                }
                foreach (SubjectImagePlus si in OrderSubjectImages)
                {      //if the file does not existon the server, or if its smaller on the server, then upload this file 
                    string newImageName = si.NewImageName;
                    string newImagePath = si.NewImagePath;
                    //string newImageName = si.SubjectImage.Dp2FileName;
                    //string newImagePath = si.SubjectImage.ImagePath;


                    //else if (RenderGreenScreen)
                    //{
                    //    newImageName = si.SubjectImage.GetDp2FileName(si.Background);
                    //    newImagePath = si.SubjectImage.GetImagePath(true, si.Background);
                    //}

                    if (!File.Exists(Path.Combine(destdir, newImageName)) || (new FileInfo(Path.Combine(destdir, newImageName)).Length < (new FileInfo(newImagePath).Length)))
                    {
                        logger.Info("Saving File: fileName='{0}'", si.SubjectImage.ImageFileName);
                        NotificationInfo.Update("Saving File " + si.SubjectImage.ImageFileName);
                        NotificationInfo.Step();


                        string destFile = Path.Combine(destdir, newImageName);
                        int cnt = 1;
                        while (File.Exists(destFile))
                        {
                            string ext = newImageName.Split(".").Last();
                            string newFileName = newImageName.Replace("." + ext, "_" + cnt++ + "." + ext);
                            destFile = Path.Combine(destdir, newFileName);
                        }

                        File.Copy(newImagePath, destFile);


                        //File.Copy(newImagePath, Path.Combine(destdir, newImageName));
                    }
                }

                NotificationInfo.Update("Saving File done.txt");
                NotificationInfo.Step();
                File.Copy(doneFileFilePath, Path.Combine(destdir, "done.txt"));

                if (this._flowController.ProjectController.IsNetworkProject)
                {
                    _flowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                       {
                           this._flowController.ProjectController.CurrentFlowProject.FlowProjectDataContext.UpdateNetworkProject = false;
                           this._flowController.ProjectController.UpdateNetworkParent();
                       }));
                }
                if (isLabUpload)
                {
                    string mailFile = Path.Combine(destdir, "mail.txt");
                    //Emailer mail = null;
                    try
                    {
                        //if (doneFileUploaded)
                        {
                            string studioemail = null;
                            if (_flowProject.FlowMasterDataContext.Organizations.First(o => o.OrganizationID == (int)_flowProject.StudioID).OrganizationContactEmail != null &&
                                _flowProject.FlowMasterDataContext.Organizations.First(o => o.OrganizationID == (int)_flowProject.StudioID).OrganizationContactEmail.Length > 1)
                                studioemail = _flowProject.FlowMasterDataContext.Organizations.First(o => o.OrganizationID == (int)_flowProject.StudioID).OrganizationContactEmail;

                            string studioName = _flowProject.FlowMasterDataContext.Organizations.First(o => o.OrganizationID == (int)_flowProject.StudioID).OrganizationName;
                            if (_flowProject.FlowMasterDataContext.PreferenceManager.Application.LabEmail != null && _flowProject.FlowMasterDataContext.PreferenceManager.Application.LabEmail.Length > 2)
                            {

                                using (StreamWriter sw = File.CreateText(mailFile))
                                {
                                    sw.WriteLine("To: " + _flowProject.FlowMasterDataContext.PreferenceManager.Application.LabEmail);
                                    sw.WriteLine("From: " + "flow@flowadmin.com");
                                    sw.WriteLine("CC: " + studioemail);
                                    sw.WriteLine("Subject: " + "Flow Product Order Uploaded - " + studioName + " -- DO NOT REPLY");
                                    sw.WriteLine("Attatchment: " + orderFormFileName);
                                    sw.WriteLine("New Flow Product Order Uploaded\n\nStudio: " + studioName + "\nProject: " + _flowProject.FlowProjectName + "\n\nSubject Count: " + OrderSubjects.Count + "\nImage Count: " + OrderSubjectImages.Count + "\nPackage Count: " + CurrentOrderPackages.Count + "\n\n");
                                }

                            }




                            FlowOrderStore store = new FlowOrderStore();
                            store.OrderID = Int32.Parse(new FileInfo(orderFormFileName).Name.Split("_")[1].Split('.')[0]);
                            store.OrderDate = DateTime.Now;

                            ActivationValidator activation = new ActivationValidator(this._flowController.ActivationKeyFile);
                            ActivationStore activationStore = activation.Repository.Load();

                            store.ActivationKey = activationStore.ActivationKey;
                            store.ProjectName = _flowProject.FlowProjectName;
                            store.ComputerName = activationStore.ComputerName;
                            store.StudioName = studioName;
                            store.FlowVersion = FlowContext.GetFlowVersion();
                            store.IsImageMatchExport = false;
                            store.SubjectCount = OrderSubjects.Count;
                            store.ImageCount = OrderSubjectImages.Count;
                            store.PackageCount = CurrentOrderPackages.Count;
                            FlowOrderPost FOPost = new FlowOrderPost();
                            string returnMsg = FOPost.Post(store);

                            OrderUploadHistory hist = new OrderUploadHistory();
                            hist.LabOrderId = new FileInfo(orderFormFileName).Name.Split("_")[1].Split('.')[0];
                            hist.CreateDate = DateTime.Now;
                            hist.ProjectName = _flowProject.FlowProjectName;
                            hist.ProjectGuid = _flowProject.FlowProjectGuid.ToString();
                            hist.ComputerName = activationStore.ComputerName;
                            hist.FlowVersion = FlowContext.GetFlowVersion();
                            hist.IsImageMatchExport = false;
                            hist.SubjectCount = OrderSubjects.Count;
                            hist.ImageCount = OrderSubjectImages.Count;
                            hist.PackageCount = CurrentOrderPackages.Count;
                            this._flowController.ProjectController.FlowMasterDataContext.OrderUploadHistories.InsertOnSubmit(hist);
                            this._flowController.ProjectController.FlowMasterDataContext.SubmitChanges();
                            this._flowController.ProjectController.FlowMasterDataContext.RefreshOrderUploadHistoryList();
                        }
                    }
                    catch (Exception e)
                    {
                        logger.ErrorException("Ignoring a caught exception when sending lab order: ", e);
                    }
                    NotificationInfo.Complete("Finished processing. Added to Upload Queue");
                }
                else
                {
                    NotificationInfo.Complete("Done Saving Order Locally");
                }

            }

            //else
            //{

            //    string ftpAddress = _flowProject.FlowMasterDataContext.PreferenceManager.Upload.OrderSubmission.ImageQuixFtpAddress.Trim();
            //    int ftpPort = _flowProject.FlowMasterDataContext.PreferenceManager.Upload.OrderSubmission.ImageQuixFtpPort;
            //    string ftpUserName = _flowProject.FlowMasterDataContext.PreferenceManager.Upload.OrderSubmission.ImageQuixFtpUserName.Trim();
            //    string ftpPassword = _flowProject.FlowMasterDataContext.PreferenceManager.Upload.OrderSubmission.ImageQuixFtpPassword.Trim();
            //    string ftpDirectory = _flowProject.FlowMasterDataContext.PreferenceManager.Upload.OrderSubmission.ImageQuixFtpDirectory.Trim();
            //    bool useSftp = _flowProject.FlowMasterDataContext.PreferenceManager.Upload.OrderSubmission.ImageQuixUseSftp;



            //    ftpDirectory = ftpDirectory + "/order_" + orderID ;
            //    if (ftpDirectory.StartsWith("//"))
            //        ftpDirectory = ftpDirectory.Substring(1);
            //    if (!ftpDirectory.StartsWith("/"))
            //        ftpDirectory = "/" + ftpDirectory;

            //    ftpDirectory = "." + ftpDirectory;





            //    bool doUpload = true;
            //    // If the request is for order submission to an IQ server
            //    if (doUpload)
            //    {

            //        if (useSftp)
            //        {
            //            FileTransfer ft = new FileTransfer(ftpAddress, ftpPort, ftpUserName, ftpPassword, useSftp, NotificationInfo);
            //            //ft.Connect();
            //            ft.autoCloseUploadDialog = true;
            //            foreach (string file in filesToUpload)
            //            {
            //                string fileName = (new FileInfo(file)).Name;
            //                NotificationInfo.Update("Sending File " + fileName);
            //                ft.SendFile(file, ftpDirectory + "/" + fileName);
            //                NotificationInfo.Step();
            //            }
            //            foreach (SubjectImagePlus si in OrderSubjectImages)
            //            {      //if the file does not existon the server, or if its smaller on the server, then upload this file 
            //                string newImageName = si.NewImageName;
            //                string newImagePath = si.NewImagePath;
            //                //string newImageName = si.SubjectImage.Dp2FileName;
            //                //string newImagePath = si.SubjectImage.ImagePath;
            //                //if (RenderGreenScreen)
            //                //{
            //                //    newImageName = si.SubjectImage.GetDp2FileName(si.Background);

            //                //    _flowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
            //                //    {
            //                //        newImagePath = si.SubjectImage.GetImagePath(true, si.Background);
            //                //    }));
            //                //}
            //                if (!ft.sftpClient.FileExists(ftpDirectory + "/" + newImageName) || (ft.sftpClient.GetFileLength(ftpDirectory + "/" + newImageName) < (new FileInfo(newImagePath).Length)))
            //                {
            //                    NotificationInfo.Update("Sending File " + si.SubjectImage.ImageFileName);
            //                    ft.SendFile(newImagePath, ftpDirectory + "/" + newImageName);
            //                    if (!ft.sftpClient.FileExists(ftpDirectory + "/" + newImageName) || (ft.sftpClient.GetFileLength(ftpDirectory + "/" + newImageName) < (new FileInfo(newImagePath).Length)))
            //                    {
            //                        NotificationInfo.Update("Sending File FAILED, trying again" + si.SubjectImage.ImageFileName);
            //                        ft.SendFile(newImagePath, ftpDirectory + "/" + newImageName);
            //                        if (!ft.sftpClient.FileExists(ftpDirectory + "/" + newImageName) || (ft.sftpClient.GetFileLength(ftpDirectory + "/" + newImageName) < (new FileInfo(newImagePath).Length)))
            //                        {
            //                            logger.Error("Sending Order - Sending File FAILED 2 times {0} - {1}", si.SubjectImage.ImageFileName, newImageName);
            //                        }
            //                    }
            //                    NotificationInfo.Step();
            //                }
            //            }

            //            NotificationInfo.Update("Sending File done.txt");
            //            ft.SendFile(doneFileFilePath, ftpDirectory + "/done.txt");
            //            doneFileUploaded = true;
            //            NotificationInfo.Step();
            //            ft.Disconnect();

            //        }
            //        else
            //        {
            //            FileTransfer ft = new FileTransfer(ftpAddress, ftpPort, ftpUserName, ftpPassword, useSftp, NotificationInfo);
            //            //ft.Connect();
            //            ft.autoCloseUploadDialog = true;

            //            foreach (string file in filesToUpload)
            //            {
            //                string fileName = (new FileInfo(file)).Name;
            //                NotificationInfo.Update("Sending File " + fileName);
            //                NotificationInfo.Step();

            //                ft.SendFile(file, ftpDirectory + "/" + fileName);
            //                NotificationInfo.Step();
            //            }
            //            foreach (SubjectImagePlus si in OrderSubjectImages)
            //            {      //if the file does not existon the server, or if its smaller on the server, then upload this file 
            //                string newImageName = si.NewImageName;
            //                string newImagePath = si.NewImagePath;
            //                //string newImageName = si.SubjectImage.Dp2FileName;
            //                //string newImagePath = si.SubjectImage.ImagePath;
            //                //if (RenderGreenScreen)
            //                //{
            //                //    newImageName = si.SubjectImage.GetDp2FileName(si.Background);
            //                //    newImagePath = si.SubjectImage.GetImagePath(true, si.Background);
            //                //}
            //                if (!ft.ftpClient.FileExists(ftpDirectory + "/" + newImageName) || (ft.ftpClient.GetFileLength(ftpDirectory + "/" + newImageName) < (new FileInfo(newImagePath).Length)))
            //                {
            //                    NotificationInfo.Update("Sending File " + si.SubjectImage.ImageFileName);
            //                    ft.SendFile(newImagePath, ftpDirectory + "/" + newImageName);
            //                    NotificationInfo.Step();
            //                }
            //            }

            //            NotificationInfo.Update("Sending File done.txt");
            //            ft.SendFile(doneFileFilePath, ftpDirectory + "/done.txt");
            //            doneFileUploaded = true;
            //            NotificationInfo.Step();
            //            ft.Disconnect();

            //        }

            //        Emailer mail = null;
            //        try
            //        {
            //            if (doneFileUploaded)
            //            {
            //                string studioemail = null;
            //                if (_flowProject.FlowMasterDataContext.Organizations.First(o => o.OrganizationID == (int)_flowProject.StudioID).OrganizationContactEmail != null &&
            //                    _flowProject.FlowMasterDataContext.Organizations.First(o => o.OrganizationID == (int)_flowProject.StudioID).OrganizationContactEmail.Length > 1)
            //                    studioemail = _flowProject.FlowMasterDataContext.Organizations.First(o => o.OrganizationID == (int)_flowProject.StudioID).OrganizationContactEmail;

            //                string studioName = _flowProject.FlowMasterDataContext.Organizations.First(o => o.OrganizationID == (int)_flowProject.StudioID).OrganizationName;
            //                if (_flowProject.FlowMasterDataContext.PreferenceManager.Application.LabEmail != null && _flowProject.FlowMasterDataContext.PreferenceManager.Application.LabEmail.Length > 2)
            //                {
            //                    mail = new Emailer("Flow Product Order Uploaded - " + studioName + " -- DO NOT REPLY",
            //                        "New Flow Product Order Uploaded\n\nStudio: " + studioName + "\nProject: " + _flowProject.FlowProjectName + "\n\nSubject Count: " + OrderSubjects.Count + "\nImage Count: " + OrderSubjectImages.Count + "\nPackage Count: " + CurrentOrderPackages.Count + "\n\n",
            //                        false,
            //                        _flowProject.FlowMasterDataContext.PreferenceManager.Application.LabEmail,
            //                        "flow@flowadmin.com",
            //                        studioemail);

            //                    mail.AddAttachment(orderFormFileName);
            //                    mail.Send();
            //                }

            //                FlowOrderStore store = new FlowOrderStore();
            //                store.OrderID = Int16.Parse(new FileInfo(orderFormFileName).Name.Split("_")[1].Split('.')[0]);
            //                store.OrderDate = DateTime.Now;

            //                ActivationValidator activation = new ActivationValidator(this._flowController.ActivationKeyFile);
            //                ActivationStore activationStore = activation.Repository.Load();

            //                store.ActivationKey = activationStore.ActivationKey;
            //                store.ProjectName = _flowProject.FlowProjectName;
            //                store.ComputerName = activationStore.ComputerName;
            //                store.StudioName = studioName;
            //                store.FlowVersion = FlowContext.GetFlowVersion();
            //                store.IsImageMatchExport = false;
            //                store.SubjectCount = OrderSubjects.Count;
            //                store.ImageCount = OrderSubjectImages.Count;
            //                store.PackageCount = CurrentOrderPackages.Count;
            //                FlowOrderPost FOPost = new FlowOrderPost();
            //                string returnMsg = FOPost.Post(store);

            //            }
            //        }
            //        catch (Exception)
            //        {

            //        }
            //    }

            //    NotificationInfo.Complete("Done Sending Order To Lab");
            //}





            return orderID;
        }

        private void SubmitJsonOrderAnalytics(string orderID, string jsonFilePath)
        {
            if (orderID == null || jsonFilePath == null)
            {
                logger.Warn("Tried to send empty JSON order analytics");
                return;
            }

            string remoteDir = String.Format("FlowOrders/{0}", orderID);
            string subdir1 = "";
            string subdir2 = "";
            if (orderID.Length > 4)
            {
                subdir1 = orderID.Substring(0, 2);
                subdir2 = orderID.Substring(2, 2);
                remoteDir = String.Format("FlowOrders/{0}/{1}", subdir1, subdir2);
            }
            string remoteFile = orderID + ".json";

            // TODO: Don't hard-code FTP info
            string host = "ftp://flowadmin.com";
            string user = "photolynx";
            string pass = "p@wprint$";

            Ftp ftpClient = new Ftp();
            ftpClient.Connect(host, 21);
            ftpClient.Login(user, pass);
            ftpClient.Timeout = 5000;

            if (!ftpClient.DirectoryExists("FlowOrders"))
                ftpClient.CreateDirectory("FlowOrders");
            if (!ftpClient.DirectoryExists("FlowOrders/" + subdir1))
                ftpClient.CreateDirectory("FlowOrders/" + subdir1);
            if (!ftpClient.DirectoryExists(remoteDir))
                ftpClient.CreateDirectory(remoteDir);

            ftpClient.PutFile(jsonFilePath, remoteDir + "/" + remoteFile);
            ftpClient.Disconnect();

            logger.Info("Sent Order JSON info for analytics");
        }

        private JsonObject SubmitJsonOrder(JsonObject orderObject, string uri, string username, string password)
        {
            logger.Info("d " + uri);
            DateTime beginTime = DateTime.Now;
            NotificationInfo.Update("START - Submit Order JSON to Lab Server: " + beginTime);
            HttpWebRequest rq = (HttpWebRequest)WebRequest.Create(uri);
            rq.Credentials = new NetworkCredential(username, password);
            rq.Headers.Add("x-iq-token", "A15M876TJ8");
            rq.Headers.Add("x-iq-user", username);
            //we need to make this 45 minutes, we had an order that took over 30 minutes, so this could take a while.
            rq.Timeout = ((60 * 45) * 1000);//try for 1 minutes (60 seconds * 1 minute * 1000 = miliseconds)
            rq.Method = "POST";
            rq.ContentType = "application/json";

            JsonWriter writer = new JsonWriter();
            orderObject.Write(writer);
            string postData = writer.ToString().Replace("\\/", "/");
            //Console.Write(postData);

            rq.ContentLength = postData.Length;


            StreamWriter stOut = new
            StreamWriter(rq.GetRequestStream(),
            System.Text.Encoding.ASCII);
            stOut.Write(postData);
            stOut.Close();

            HttpWebResponse rs;
            try
            {
                rs = (HttpWebResponse)rq.GetResponse();
                //HttpWebResponse rs = (HttpWebResponse)rq.GetResponse();
                string orderLocation = rs.Headers["Location"];
                string IQ_FtpServer = rs.Headers["IQ-FTP-SERVER"];
                string IQ_user = rs.Headers["IQ-FTP-USER"];
                string IQ_password = rs.Headers["IQ-FTP-PASSWORD"];
                string IQ_ftpType = rs.Headers["IQ-FTP-TYPE"];

                string orderID = orderLocation.ToString().Split('/').Last();
                JsonObject returnObject = new JsonObject();
                returnObject.Add("orderID", orderID);
                returnObject.Add("orderLocation", orderLocation);
                returnObject.Add("IQ_FtpServer", IQ_FtpServer);
                returnObject.Add("IQ_user", IQ_user);
                returnObject.Add("IQ_password", IQ_password);
                returnObject.Add("IQ_ftpType", IQ_ftpType);

                DateTime endTime = DateTime.Now;
                int minutes = (endTime -beginTime).Minutes;
                int seconds = (endTime - beginTime).Seconds;
                logger.Info("Submit Order JSON took : " + minutes + ":" + seconds + " minutes");

                return returnObject;
            }
            catch (WebException ex)
            {
                DateTime endTime = DateTime.Now;
                int minutes = (endTime - beginTime).Minutes;
                int seconds = (endTime - beginTime).Seconds;
                NotificationInfo.Update("END - Submit Order JSON to Lab Server: " + endTime);
                logger.Info("Submit Order FAILED After : " + minutes + ":" + seconds + " minutes");

                string pageOutput = "";
                if (ex != null && ex.Response != null)
                {
                    using (Stream stream = ex.Response.GetResponseStream())
                    {

                        using (StreamReader xreader = new StreamReader(stream))
                        {

                            pageOutput = xreader.ReadToEnd().Trim();
                        }
                    }
                    logger.Error("ERROR - " + ((HttpWebResponse)ex.Response).StatusDescription + "\n\nDetails: " + pageOutput);
                    throw new Exception(((HttpWebResponse)ex.Response).StatusDescription + "\n\nDetails: " + pageOutput);
                }
                else
                {
                    logger.Error("ERROR - null Response from server, it must have timed out");
                    throw new Exception("ERROR - null Response from server, it must have timed out");
                }



            }

        }

        private string SubmitJsonOrderPlic(JsonObject orderObject, string uri, string username, string password, string catalogid)
        {

    
            Flow.Controller.Catalog.PlicCatalogController controller = new Flow.Controller.Catalog.PlicCatalogController(this._flowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.CatalogRequest.ImageQuixBaseUrl, null);

            //logger.Info("Authenticate PLIC via oauth");
            //string rA = controller.Authenticate(username, password, uri);
            //JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            //AuthenticateResults authenticateResults = json_serializer.Deserialize<AuthenticateResults>(rA);

            logger.Info("Connecting to PLIC for order submit: " + uri);
            DateTime beginTime = DateTime.Now;
            NotificationInfo.Update("START - Submit Order JSON to Lab Server: " + beginTime);
            //uri = uri + "?order[content]=JSON_SERIALIZED_ORDER&catalog_id=" + catalogid;

            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls;

            //string boundary = "--XXXXX--";
            HttpWebRequest rq = (HttpWebRequest)WebRequest.Create(uri);
            rq.Credentials = new NetworkCredential(username, password);
            //rq.Headers.Add("Authorization: Bearer " + authenticateResults.access_token);
            rq.Headers.Add("X-Activation-Key: " + this._flowController.ActivationKey);
            rq.Timeout = ((60 * 45) * 1000);//try for 1 minutes (60 seconds * 1 minute * 1000 = miliseconds)
            rq.Method = "POST";
            rq.ContentType = "application/x-www-form-urlencoded";
            rq.Accept = "application/vnd.plic.io.v2+json";
            

            JsonWriter writer = new JsonWriter();
            orderObject.Write(writer);
            string postData = writer.ToString().Replace("\\/", "/");
            //Console.Write(postData);

            string myparams = "catalog_id=" + HttpUtility.UrlEncode(catalogid) + "&order[content]=" + HttpUtility.UrlEncode(postData);
            postData = myparams;

            rq.ContentLength = postData.Length;


            StreamWriter stOut = new
            StreamWriter(rq.GetRequestStream(),
            System.Text.Encoding.ASCII);
            stOut.Write(postData);
            stOut.Close();
            string result;
                try
                {
                    using (WebResponse response = rq.GetResponse())
                    {
                        using (Stream stream = response.GetResponseStream())
                        {
                            using (var streamReader = new StreamReader(stream))
                            {
                                result = streamReader.ReadToEnd();
                            }
                        }
                    }
                }
                catch (WebException wex)
                {
                    return null;
                }
                return result;
           

        }

        private JsonObject getMetaDataPLIC(List<Subject> OrderSubjects, List<OrderPackage> CurrentOrderPackages, List<SubjectImagePlus> OrderSubjectImages, bool isPlic)
        {
            JsonObject metadata = new JsonObject();
            metadata.Add("guid", _flowProject.FlowProjectTemplateShortDescription);

            JsonArray fields = new JsonArray();

            JsonObject fieldsubID = new JsonObject();
            fieldsubID.Add("field", "subjectid");
            fieldsubID.Add("length", "31");
            fieldsubID.Add("fieldType", "CHAR");
            fields.Add(fieldsubID);

            foreach (ProjectSubjectField f in _flowProject.FlowProjectDataContext.ProjectSubjectFields)
            {
                JsonObject field = new JsonObject();
                field.Add("field", f.ProjectSubjectFieldDesc.ToLower().Replace(" ", ""));
                if (f.FieldDataTypeID == 1 && f.FieldDataSize > 0)
                {
                    if (f.FieldDataSize < 4000)
                    {
                        field.Add("length", f.FieldDataSize.ToString());
                        field.Add("fieldType", "CHAR");
                    }
                    else
                    {
                        field.Add("length", "max");
                        field.Add("fieldType", "VARCHAR");
                    }

                }
                else
                {
                    field.Add("length", "128");
                    field.Add("fieldType", "CHAR");
                }
                fields.Add(field);
            }
            //for (int i = 1; i < 11; i++)
            //{
            //    JsonObject field = new JsonObject();
            //    field.Add("field", "packageorder" + i);
            //    fields.Add(field);
            //}

            JsonObject fieldA = new JsonObject();
            fieldA.Add("field", "ordersummary");
            fieldA.Add("length", "max");
            fieldA.Add("fieldType", "VARCHAR");
            fields.Add(fieldA);

            JsonObject fieldB = new JsonObject();
            fieldB.Add("field", "groupimage");
            fieldB.Add("length", "128");
            fieldB.Add("fieldType", "CHAR");
            fields.Add(fieldB);

            //this is not really a field in the table, just used for linking subject to images
            if (!isPlic)
            {
                JsonObject fieldC = new JsonObject();
                fieldC.Add("field", "images");
                fieldC.Add("length", "256");
                fieldC.Add("fieldType", "CHAR");
                fields.Add(fieldC);
            }

            JsonObject fieldD = new JsonObject();
            fieldD.Add("field", "organization_name");
            fieldD.Add("length", "128");
            fieldD.Add("fieldType", "CHAR");
            fields.Add(fieldD);

            JsonObject fieldE = new JsonObject();
            fieldE.Add("field", "project_name");
            fieldE.Add("length", "128");
            fieldE.Add("fieldType", "CHAR");
            fields.Add(fieldE);

            JsonObject fieldF = new JsonObject();
            fieldF.Add("field", "project_event");
            fieldF.Add("length", "128");
            fieldF.Add("fieldType", "CHAR");
            fields.Add(fieldF);



            metadata.Add("fields", fields);



            JsonArray records = new JsonArray();
            foreach (Subject subject in OrderSubjects)
            {
                JsonObject recordRoot = new JsonObject();
                
                JsonArray recordFields = new JsonArray();
                

                JsonObject field = new JsonObject();
                field.Add("field", "subjectid");
                field.Add("value", subject.SubjectID.ToString());
                recordFields.Add(field);

                foreach (SubjectDatum datum in subject.SubjectData)
                {
                    if (datum.Value == null)
                        datum.Value = null;
                    // datum.Value = new object();
                    //if this is a DateTime and the Time is 00:00:00, then just return the data
                    if ((datum.Value.GetType() == typeof(DateTime)) && (((DateTime)datum.Value).TimeOfDay.TotalSeconds == 0))
                    {
                        //record.Add(datum.FieldDesc.ToLower(), ((DateTime)datum.Value).Date.ToShortDateString());
                        JsonObject field2 = new JsonObject();
                        field2.Add("field", datum.FieldDesc.ToLower());
                        field2.Add("value", ((DateTime)datum.Value).Date.ToShortDateString());
                        recordFields.Add(field2);
                    }
                    else
                    {
                        //record.Add(datum.FieldDesc.ToLower(), datum.Value.ToString());
                        JsonObject field3 = new JsonObject();
                        field3.Add("field", datum.FieldDesc.ToLower());
                        field3.Add("value", datum.Value.ToString());
                        recordFields.Add(field3);
                    }

                }


                string orderSummary = "";
                if (CurrentOrderPackages == null)
                {
                    //for (int i = 0; i < 11; i++)
                    //    record.Add("packageorder" + i, "");
                }
                else
                {
                    int i = 0;

                    foreach (OrderPackage thisPackage in subject.SubjectOrder.OrderPackages)
                    {
                        //if (i == 10) break;
                        if (CurrentOrderPackages.Contains(thisPackage))
                        {
                            i++;
                            //record.Add("packageorder" + i, thisPackage.ProductPackageDesc);
                            if (CurrentOrderPackages.Contains(thisPackage))
                            {
                                orderSummary = orderSummary + thisPackage.ProductPackageDesc + ":";
                                foreach (OrderProduct thisProduct in thisPackage.OrderProducts)
                                {
                                    ImageQuixProduct iqProduct = _flowProject.FlowMasterDataContext.ImageQuixProducts.First(iqp => iqp.PrimaryKey == thisProduct.ImageQuixProductPk);
                                    orderSummary = orderSummary + "- (" + thisProduct.Quantity + ") - " + iqProduct.Label + ":\n";
                                }
                                orderSummary = orderSummary + "\n";
                            }
                        }
                    }
                    //while (i < 10)
                    //{
                    //    i++;
                    //    record.Add("packageorder" + i, "");
                    //}
                }
                JsonObject field4 = new JsonObject();
                field4.Add("field", "ordersummary");
                field4.Add("value", orderSummary);
                recordFields.Add(field4);
                //record.Add("ordersummary", orderSummary);



                string groupImageDesc = "";
                if (subject.ImageList.Count(il => il.IsGroupImage) > 0)
                {
                    GroupImage gi = subject.ImageList.First(il => il.IsGroupImage).GroupImage;
                    groupImageDesc = gi.ImageFileName;
                    //if (gi.GroupImageDesc != null && gi.GroupImageDesc.Length > 0)
                    //    groupImageDesc = gi.GroupImageDesc;
                    //else
                    //    groupImageDesc = gi.ImageFileName;
                }
                //record.Add("groupimage", groupImageDesc);
                JsonObject field5 = new JsonObject();
                field5.Add("field", "groupimage");
                field5.Add("value", groupImageDesc);
                recordFields.Add(field5);

                List<string> images = new List<string>();
                foreach (SubjectImage si in subject.SubjectImages)
                {
                    if (OrderSubjectImages.Any(a => a.SubjectImage == si))
                    {
                        foreach (SubjectImagePlus thisOne in OrderSubjectImages.Where(a => a.SubjectImage == si && a.IsRendered == false))
                        {
                            if (RenderGreenScreen)
                                images.Add(thisOne.SubjectImage.GetDp2FileName(thisOne.Background, UseOriginalFileNames, RenderGreenScreen));
                            else
                                images.Add(thisOne.SubjectImage.Dp2FileName(UseOriginalFileNames));
                        }
                    }

                }
                foreach (OrderPackage thisPackage in subject.SubjectOrder.OrderPackages)
                {
                    if (CurrentOrderPackages.Contains(thisPackage))
                    {
                        foreach (OrderProduct thisProd in thisPackage.OrderProducts)
                        {
                            if (thisProd.ProductPackageComposition != null && thisProd.ProductPackageComposition.Renderlayout)
                            {
                                images.Add(thisProd.UniqueProductImageName);
                            }
                        }
                    }
                }
                //record.Add("images", images);
                JsonObject field6 = new JsonObject();
                field6.Add("field", "images");
                field6.Add("value", String.Join(",", images));
                recordFields.Add(field6);


                //record.Add("organization_name", this._flowProject.Organization.OrganizationName);
                //record.Add("project_name", this._flowProject.FlowProjectName);
                //record.Add("project_event", this._flowProject.FlowProjectDesc);

                JsonObject field7 = new JsonObject();
                field7.Add("field", "organization_name");
                field7.Add("value", this._flowProject.Organization.OrganizationName);
                recordFields.Add(field7);

                JsonObject field8 = new JsonObject();
                field8.Add("field", "project_name");
                field8.Add("value", this._flowProject.FlowProjectName);
                recordFields.Add(field8);

                JsonObject field9 = new JsonObject();
                field9.Add("field", "project_event");
                field9.Add("value", this._flowProject.FlowProjectDesc);
                recordFields.Add(field9);

                recordRoot.Add("recordFields", recordFields);
                records.Add(recordRoot);
            }

            metadata.Add("records", records);
            return metadata;

        }

        //this is the old method, should retire when all goes through PLIC
        private JsonObject getMetaData(List<Subject> OrderSubjects, List<OrderPackage> CurrentOrderPackages, List<SubjectImagePlus> OrderSubjectImages, bool isPlic)
        {
            JsonObject metadata = new JsonObject();
            metadata.Add("guid", _flowProject.FlowProjectTemplateShortDescription);

            JsonArray fields = new JsonArray();

            JsonObject fieldsubID = new JsonObject();
            fieldsubID.Add("field", "subjectid");
            fieldsubID.Add("length", "31");
            fieldsubID.Add("fieldType", "CHAR");
            fields.Add(fieldsubID);

            foreach (ProjectSubjectField f in _flowProject.FlowProjectDataContext.ProjectSubjectFields)
            {
                JsonObject field = new JsonObject();
                field.Add("field", f.ProjectSubjectFieldDesc.ToLower().Replace(" ", ""));
                if (f.FieldDataTypeID == 1 && f.FieldDataSize > 0)
                {
                    if (f.FieldDataSize < 4000)
                    {
                        field.Add("length", f.FieldDataSize.ToString());
                        field.Add("fieldType", "CHAR");
                    }
                    else
                    {
                        field.Add("length", "max");
                        field.Add("fieldType", "VARCHAR");
                    }

                }
                else
                {
                    field.Add("length", "128");
                    field.Add("fieldType", "CHAR");
                }
                fields.Add(field);
            }
            //for (int i = 1; i < 11; i++)
            //{
            //    JsonObject field = new JsonObject();
            //    field.Add("field", "packageorder" + i);
            //    fields.Add(field);
            //}

            JsonObject fieldA = new JsonObject();
            fieldA.Add("field", "ordersummary");
            fieldA.Add("length", "max");
            fieldA.Add("fieldType", "VARCHAR");
            fields.Add(fieldA);

            JsonObject fieldB = new JsonObject();
            fieldB.Add("field", "groupimage");
            fieldB.Add("length", "128");
            fieldB.Add("fieldType", "CHAR");
            fields.Add(fieldB);

            //this is not really a field in the table, just used for linking subject to images
            if (!isPlic)
            {
                JsonObject fieldC = new JsonObject();
                fieldC.Add("field", "images");
                fieldC.Add("length", "256");
                fieldC.Add("fieldType", "CHAR");
                fields.Add(fieldC);
            }

            JsonObject fieldD = new JsonObject();
            fieldD.Add("field", "organization_name");
            fieldD.Add("length", "128");
            fieldD.Add("fieldType", "CHAR");
            fields.Add(fieldD);

            JsonObject fieldE = new JsonObject();
            fieldE.Add("field", "project_name");
            fieldE.Add("length", "128");
            fieldE.Add("fieldType", "CHAR");
            fields.Add(fieldE);

            JsonObject fieldF = new JsonObject();
            fieldF.Add("field", "project_event");
            fieldF.Add("length", "128");
            fieldF.Add("fieldType", "CHAR");
            fields.Add(fieldF);



            metadata.Add("fields", fields);



            JsonArray records = new JsonArray();
            foreach (Subject subject in OrderSubjects)
            {
                JsonObject record = new JsonObject();
                record.Add("subjectid", subject.SubjectID.ToString());

                foreach (SubjectDatum datum in subject.SubjectData)
                {
                    if (datum.Value == null)
                        datum.Value = null;
                    // datum.Value = new object();
                    //if this is a DateTime and the Time is 00:00:00, then just return the data
                    if ((datum.Value.GetType() == typeof(DateTime)) && (((DateTime)datum.Value).TimeOfDay.TotalSeconds == 0))
                        record.Add(datum.FieldDesc.ToLower(), ((DateTime)datum.Value).Date.ToShortDateString());
                    else
                        record.Add(datum.FieldDesc.ToLower(), datum.Value.ToString());

                }


                string orderSummary = "";
                if (CurrentOrderPackages == null)
                {
                    //for (int i = 0; i < 11; i++)
                    //    record.Add("packageorder" + i, "");
                }
                else
                {
                    int i = 0;

                    foreach (OrderPackage thisPackage in subject.SubjectOrder.OrderPackages)
                    {
                        //if (i == 10) break;
                        if (CurrentOrderPackages.Contains(thisPackage))
                        {
                            i++;
                            //record.Add("packageorder" + i, thisPackage.ProductPackageDesc);
                            if (CurrentOrderPackages.Contains(thisPackage))
                            {
                                orderSummary = orderSummary + thisPackage.ProductPackageDesc + ":";
                                foreach (OrderProduct thisProduct in thisPackage.OrderProducts)
                                {
                                    ImageQuixProduct iqProduct = _flowProject.FlowMasterDataContext.ImageQuixProducts.First(iqp => iqp.PrimaryKey == thisProduct.ImageQuixProductPk);
                                    orderSummary = orderSummary + "- (" + thisProduct.Quantity + ") - " + iqProduct.Label + ":\n";
                                }
                                orderSummary = orderSummary + "\n";
                            }
                        }
                    }
                    //while (i < 10)
                    //{
                    //    i++;
                    //    record.Add("packageorder" + i, "");
                    //}
                }
                record.Add("ordersummary", orderSummary);



                string groupImageDesc = "";
                if (subject.ImageList.Count(il => il.IsGroupImage) > 0)
                {
                    GroupImage gi = subject.ImageList.First(il => il.IsGroupImage).GroupImage;
                    groupImageDesc = gi.ImageFileName;
                    //if (gi.GroupImageDesc != null && gi.GroupImageDesc.Length > 0)
                    //    groupImageDesc = gi.GroupImageDesc;
                    //else
                    //    groupImageDesc = gi.ImageFileName;
                }
                record.Add("groupimage", groupImageDesc);

                JsonArray images = new JsonArray();
                foreach (SubjectImage si in subject.SubjectImages)
                {
                    if (OrderSubjectImages.Any(a => a.SubjectImage == si))
                    {
                        foreach (SubjectImagePlus thisOne in OrderSubjectImages.Where(a => a.SubjectImage == si && a.IsRendered == false))
                        {
                            if (RenderGreenScreen)
                                images.Add(thisOne.SubjectImage.GetDp2FileName(thisOne.Background, UseOriginalFileNames, RenderGreenScreen));
                            else
                                images.Add(thisOne.SubjectImage.Dp2FileName(UseOriginalFileNames));
                        }
                    }

                }
                foreach (OrderPackage thisPackage in subject.SubjectOrder.OrderPackages)
                {
                    if (CurrentOrderPackages.Contains(thisPackage))
                    {
                        foreach (OrderProduct thisProd in thisPackage.OrderProducts)
                        {
                            if (thisProd.ProductPackageComposition != null && thisProd.ProductPackageComposition.Renderlayout)
                            {
                                images.Add(thisProd.UniqueProductImageName);
                            }
                        }
                    }
                }
                record.Add("images", images);

                record.Add("organization_name", this._flowProject.Organization.OrganizationName);
                record.Add("project_name", this._flowProject.FlowProjectName);
                record.Add("project_event", this._flowProject.FlowProjectDesc);
                records.Add(record);
            }

            metadata.Add("records", records);
            return metadata;

        }


         private JsonArray getImageData(List<SubjectImagePlus> OrderSubjectImages)
        { return getImageData(OrderSubjectImages, false); }
        private JsonArray getImageData(List<SubjectImagePlus> OrderSubjectImages, bool applycrops)
        {
            JsonArray imageData = new JsonArray();
            foreach (SubjectImagePlus si in OrderSubjectImages)
            {
                JsonObject image = new JsonObject();
                image.Add("name", si.NewImageName);
                //if(RenderGreenScreen)
                //    image.Add("name", si.SubjectImage.GetDp2FileName(si.Background));
                //else
                //    image.Add("name", si.SubjectImage.Dp2FileName);

                if (si.SubjectImage.CropH < 0 || double.IsNaN(si.SubjectImage.CropH))
                    si.SubjectImage.CropH = 0;


                string rotate = si.SubjectImage.Orientation.ToString();

                double _cropX = double.Parse((si.SubjectImage.CropL * 100).ToString("N5"));
                double _cropY = double.Parse((si.SubjectImage.CropT * 100).ToString("N5"));
                double _cropW = double.Parse((si.SubjectImage.CropW * 100).ToString("N5"));
                double _cropH = double.Parse((si.SubjectImage.CropH * 100).ToString("N5"));
                if (si.IsRendered || applycrops)
                {
                    _cropX = 0;
                    _cropY = 0;
                    _cropW = 0;
                    _cropH = 0;
                    rotate = "0";
                }
                image.Add("cropX", (_cropX + (_cropW / 2)).ToString());
                image.Add("cropY", (_cropY + (_cropH / 2)).ToString());
                image.Add("cropW", _cropW.ToString());
                image.Add("cropH", _cropH.ToString());
                if (RenderGreenScreen && si.SubjectImage.IsGreenscreen)
                    rotate = "0";
                image.Add("orientation", rotate);
                image.Add("source", si.SubjectImage.SourceComputer);

                JsonArray imageOptions = new JsonArray();
                foreach (OrderImageOption oio in si.SubjectImage.OrderImageOptions)
                {
                        ImageQuixCatalogOption iqco = this._flowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogOptions.FirstOrDefault(a => a.ResourceURL == oio.ResourceURL);
                        if (iqco != null)
                        {
                            JsonObject objOption = new JsonObject();
                            objOption.Add("optionGroupID", iqco.ImageQuixCatalogOptionGroup.ResourceURL);
                            objOption.Add("optionID", oio.ResourceURL);
                            imageOptions.Add(objOption);
                        }
                }
                image.Add("imageOptionSelections", imageOptions);

                //JsonArray imageOptions = new JsonArray();
                //if (si.SubjectImage.OrderImageOptions.Count > 0)
                //{
                   
                //    foreach (OrderImageOption oio in si.SubjectImage.OrderImageOptions)
                //    {
                //        JsonObject objOption = new JsonObject();

                //        objOption.Add("optionGroupID", "Image Options");

                //        objOption.Add("optionID", oio.ResourceURL);

                //        imageOptions.Add(objOption);
                //    }

                //}
                //image.Add("imageOptionSelections", imageOptions);

                //
                //JsonArray orderOptions = new JsonArray();
                //    foreach (Flow.View.Project.OrderOptionSelected oos in OrderOptionList)
                //    {
                //        JsonObject objOption = new JsonObject();
                //        if (isPlic)
                //        {
                //            objOption.Add("optionGroupID", oos.OptionGroupLabel);
                //            objOption.Add("optionID", oos.OptionLabel);
                //        }

                //        orderOptions.Add(objOption);
                //    }
                //    rootOrder.Add("orderOptionSelections", orderOptions);
                //

                imageData.Add(image);
            }


            return imageData;
        }

        private JsonArray getEntries(List<OrderPackage> CurrentOrderPackages, bool isPlic)
        {
            JsonArray entries = new JsonArray();
            foreach (OrderPackage op in CurrentOrderPackages)
            {


                if (op.ProductPackage.IsAnyXPackage)
                {
                    //oProducts = new System.Data.Linq.EntitySet<OrderProduct>();
                    if (op.Units != null && op.Units.Length > 1)
                    {
                        foreach (string id in op.Units.Split(";"))
                        {
                            string map = id.Split("-")[0];
                            ProductPackage pptemp = this._flowProject.FlowCatalog.ProductPackages.FirstOrDefault(ptemp => ptemp.Map == map);
                            if (pptemp != null)
                            {
                                ProductPackageComposition ppctemp = pptemp.ProductPackageCompositions.FirstOrDefault();
                                OrderProduct optemp = new OrderProduct(ppctemp.ImageQuixProduct, op.SubjectOrder, ppctemp.DisplayLabel, ppctemp.ImageQuixProduct.ResourceURL);

                                ImageQuixProduct iqProd = ppctemp.ImageQuixProduct;

                                JsonObject product = new JsonObject();
                                product.Add("type", "product");
                                if (isPlic)
                                {
                                    product.Add("productID", iqProd.ResourceURL);
                                    product.Add("map", iqProd.Map);
                                }
                                else
                                    product.Add("productRef", iqProd.ResourceURL);

                                product.Add("quantity", 1);
                                JsonArray nodes = new JsonArray();
                                //int imgnodeLoop = 0;
                                foreach (ImageQuixProductNode iqNode in iqProd.ImageQuixProductNodes)
                                {


                                    //only do image nodes
                                    if (iqNode.Type != 1)
                                        continue;

                                    JsonObject objNode = new JsonObject();
                                    if (isPlic)
                                    {
                                        objNode.Add("nodeID", iqNode.ResourceURL);
                                    }
                                    else
                                        objNode.Add("nodeRef", iqNode.ResourceURL);

                                    objNode.Add("type", "image");
                                    if (op.SubjectImage == null)
                                        throw new Exception("Image Node is missing an Image.\n\nSubject: " + op.SubjectOrder.Subject.FormalFullName + " (" + op.SubjectOrder.Subject.TicketCode + ")");

                                    //TODO must support renderlayout and greenscreen here
                                    if (ppctemp != null && ppctemp.Renderlayout)
                                        objNode.Add("filename", op.SubjectOrder.Subject.TicketCode + "_" + optemp.OrderProductID + ".jpg");
                                    else if (this.RenderGreenScreen)
                                        objNode.Add("filename", op.SubjectImage.GetDp2FileName(op.GreenScreenBackground, UseOriginalFileNames, RenderGreenScreen));
                                    else
                                        objNode.Add("filename", op.SubjectImage.Dp2FileName(UseOriginalFileNames));

                                    if (!isPlic)
                                    {
                                        objNode.Add("cropX", "0");
                                        objNode.Add("cropY", "0");
                                        objNode.Add("cropW", "0");
                                        objNode.Add("cropH", "0");
                                        objNode.Add("orientation", "0");
                                    }




                                    nodes.Add(objNode);
                                }

                                product.Add("nodes", nodes);
                                entries.Add(product);
                            }
                        }
                    }
                }
                else
                {
                    foreach (OrderProduct prod in op.OrderProducts)
                    {
                        ImageQuixProduct iqProd = null;
                        if (isPlic)
                            iqProd = _flowProject.FlowMasterDataContext.ImageQuixProducts.First(iqp => iqp.ResourceURL == prod.ResourceURL);
                        else
                            iqProd = _flowProject.FlowMasterDataContext.ImageQuixProducts.First(iqp => iqp.PrimaryKey == prod.ImageQuixProductPk);

                        JsonObject product = new JsonObject();
                        product.Add("type", "product");
                        if (isPlic)
                        {
                            product.Add("productID", iqProd.ResourceURL);
                            product.Add("map", iqProd.Map);
                        }
                        else
                            product.Add("productRef", iqProd.ResourceURL);

                        product.Add("quantity", ((int)prod.Quantity * op.Qty).ToString());
                        JsonArray nodes = new JsonArray();
                        //int imgnodeLoop = 0;
                        foreach (OrderProductNode node in prod.OrderProductNodes)
                        {
                            //if (node.SubjectImage != null && node.ImageQuixProductNodeType == 1) // 1 is the image nodetype
                            //{
                            //    imgnodeLoop++;
                            //    if (prod.ProductPackageComposition != null && prod.ProductPackageComposition.Renderlayout)
                            //    {
                            //        if (imgnodeLoop > 1)
                            //            break;
                            //    }
                            //}

                            ImageQuixProductNode iqNode = _flowProject.FlowMasterDataContext.ImageQuixProductNodes.First(iqpn => iqpn.PrimaryKey == node.ImageQuixProductNodePk);

                            //only do image nodes
                            if (node.ImageQuixProductNodeType != 1)
                                continue;

                            JsonObject objNode = new JsonObject();
                            if (isPlic)
                            {
                                objNode.Add("nodeID", iqNode.ResourceURL);
                            }
                            else
                                objNode.Add("nodeRef", iqNode.ResourceURL);
                            switch (node.ImageQuixProductNodeType)
                            {
                                // IMAGE 1 
                                case 1:
                                    objNode.Add("type", "image");
                                    if (node.SubjectImage == null)
                                        throw new Exception("Image Node is missing an Image.\n\nSubject: " + op.SubjectOrder.Subject.FormalFullName + " (" + op.SubjectOrder.Subject.TicketCode + ")");

                                    if (prod.ProductPackageComposition != null && prod.ProductPackageComposition.Renderlayout)
                                        objNode.Add("filename", node.OrderProduct.UniqueProductImageName);
                                    else if (this.RenderGreenScreen)
                                        objNode.Add("filename", node.SubjectImage.GetDp2FileName(prod.GreenScreenBackground, UseOriginalFileNames, RenderGreenScreen));
                                    else
                                        objNode.Add("filename", node.SubjectImage.Dp2FileName(UseOriginalFileNames));

                                    if (!isPlic)
                                    {
                                        objNode.Add("cropX", "0");
                                        objNode.Add("cropY", "0");
                                        objNode.Add("cropW", "0");
                                        objNode.Add("cropH", "0");
                                        objNode.Add("orientation", "0");
                                    }
                                    break;

                                // TEXT 2
                                case 2:
                                    objNode.Add("type", "text");
                                    objNode.Add("text", "");
                                    if (!isPlic)
                                    {
                                        objNode.Add("cropX", "0");
                                        objNode.Add("cropY", "0");
                                        objNode.Add("cropW", "0");
                                        objNode.Add("cropH", "0");
                                        objNode.Add("orientation", "0");
                                    }
                                    break;
                                // NONE 0
                                default:
                                    objNode.Add("type", "none");
                                    objNode.Add("text", "");
                                    if (!isPlic)
                                    {
                                        objNode.Add("cropX", "0");
                                        objNode.Add("cropY", "0");
                                        objNode.Add("cropW", "0");
                                        objNode.Add("cropH", "0");
                                        objNode.Add("orientation", "0");
                                    }
                                    break;
                            }



                            nodes.Add(objNode);
                        }
                        product.Add("nodes", nodes);





                        JsonArray options = new JsonArray();
                        foreach (OrderProductOption opt in prod.OrderProductOptions)
                        {
                            ImageQuixOptionGroup iqOpt = _flowProject.FlowMasterDataContext.ImageQuixOptionGroups.First(iqo => iqo.PrimaryKey == opt.ImageQuixOptionGroupPk);

                            //if this is richmond, and this is optiongroup 1408, we are going to skip it
                            if (this._flowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.SupportURL.Contains("richmond") && (iqOpt.ResourceURL.EndsWith("1408") || iqOpt.ResourceURL.EndsWith("1409") || iqOpt.ResourceURL.EndsWith("1421")))
                            {
                                logger.Info("This is a richmond catalog - skip bad optionGroup");
                                continue;
                            }

                            if (opt.ImageQuixOptionPk != null)
                            {
                                ImageQuixOption iqO = _flowProject.FlowMasterDataContext.ImageQuixOptions.First(iqo => iqo.PrimaryKey == opt.ImageQuixOptionPk);
                                JsonObject objOption = new JsonObject();
                                if (isPlic)
                                {
                                    objOption.Add("optionGroupID", iqOpt.Label);
                                    objOption.Add("optionID", iqO.Label);
                                }
                                else
                                {
                                    objOption.Add("optionGroupRef", iqOpt.ResourceURL);
                                    objOption.Add("optionRef", iqOpt.ResourceURL + "/option/" + opt.ImageQuixOptionPk);
                                }
                                options.Add(objOption);
                            }
                        }
                        product.Add("optionSelections", options);
                        entries.Add(product);
                    }
                }
            }
            return entries;

        }


        private string getShipmentOptionSelections()
        {
            foreach (OrderShippingOption ooo in _flowProject.FlowProjectDataContext.OrderShippingOptions)
            {
                return ooo.ResourceURL;
            }
            return null;
        }
        private JsonArray getOrderOptionSelections(OrderPackage firstOrderPackage)
        {

            JsonArray orderOptionResourceURLs = new JsonArray();
            foreach (OrderOrderOption ooo in _flowProject.FlowProjectDataContext.OrderOrderOptions)
            {
                orderOptionResourceURLs.Add(ooo.ResourceURL);
            }

            foreach (OrderPackagingOption ooo in _flowProject.FlowProjectDataContext.OrderPackagingOptions)
            {
                orderOptionResourceURLs.Add(ooo.ResourceURL);
            }
            foreach (OrderSubjectOrderOption ooo in firstOrderPackage.SubjectOrder.OrderSubjectOrderOptions)
            {
                orderOptionResourceURLs.Add(ooo.ResourceURL);
            }
            return orderOptionResourceURLs;
        }


        private void getCustomerInformation(OrderPackage orderPackage, Organization orgForBilling, ref JsonObject rootOrder)
        {

            if (orderPackage == null)
                logger.Info("getCustomerInformation: orderPackage is null");
            if (orgForBilling == null)
                logger.Info("getCustomerInformation: orgForBilling is null");
            if (rootOrder == null)
                logger.Info("getCustomerInformation: rootOrder is null");

            if (this._flowProject == null)
                logger.Info("getCustomerInformation: this._flowProject is null");
            else if (this._flowProject.Studio == null)
                logger.Info("getCustomerInformation: this._flowProject.Studio is null");

            if (rootOrder == null)
                logger.Info("getCustomerInformation: rootOrder is null");


            if (orgForBilling != null && orgForBilling.LabCustomerID != null)
                rootOrder.Add("billingAccountNumber", orgForBilling.LabCustomerID.ToString());
            else
            {
                if (this._flowProject.Studio.LabCustomerID != null)
                    rootOrder.Add("billingAccountNumber", this._flowProject.Studio.LabCustomerID);
                else
                    rootOrder.Add("billingAccountNumber", "");
            }

            if (orderPackage.ShippingAddress == null && orgForBilling != null)
            {
                logger.Info("getCustomerInformation: using orgForBilling");
                rootOrder.Add("shippingFirstName", orgForBilling.OrganizationContactName);
                rootOrder.Add("shippingLastName", "");
                rootOrder.Add("shippingAddress1", orgForBilling.OrganizationAddressLine1);
                rootOrder.Add("shippingAddress2", orgForBilling.OrganizationAddressLine2);
                rootOrder.Add("shippingCity", orgForBilling.OrganizationCity);
                rootOrder.Add("shippingState", orgForBilling.OrganizationStateCode);
                rootOrder.Add("shippingZipcode", orgForBilling.OrganizationShippingZipCode.Trim());
                rootOrder.Add("shippingCountry", "");
                rootOrder.Add("shippingPhoneNumber", orgForBilling.OrganizationContactPhone);
                rootOrder.Add("emailAddress", orgForBilling.OrganizationContactEmail);
            }
            else if (orderPackage.ShippingAddress == null && orgForBilling == null)
            {
                logger.Info("getCustomerInformation: using orderPackage.SubejectOrder");
                rootOrder.Add("shippingFirstName", orderPackage.SubjectOrder.ShippingName);
                rootOrder.Add("shippingAddress1", orderPackage.SubjectOrder.ShippingAddress);
                rootOrder.Add("shippingAddress2", null);
                rootOrder.Add("shippingCity", orderPackage.SubjectOrder.ShippingCity);
                rootOrder.Add("shippingState", orderPackage.SubjectOrder.ShippingState);
                rootOrder.Add("shippingZipcode", orderPackage.SubjectOrder.ShippingZipCode.Trim());
                rootOrder.Add("shippingCountry", "");
                rootOrder.Add("shippingPhoneNumber", "");
                rootOrder.Add("emailAddress", "");
            }
            else
            {
                logger.Info("getCustomerInformation: using orderPackage");

                logger.Info("A getCustomerInformation: using orderPackage - " + orderPackage.ShippingFirstName);
                if(!string.IsNullOrEmpty(orderPackage.ShippingFirstName))
                    rootOrder.Add("shippingFirstName", orderPackage.ShippingFirstName);
                else if (!string.IsNullOrEmpty(orderPackage.SubjectOrder.ShippingName))
                    rootOrder.Add("shippingFirstName", orderPackage.SubjectOrder.ShippingName);

                logger.Info("B getCustomerInformation: using orderPackage - " + orderPackage.ShippingLastName);
                rootOrder.Add("shippingLastName", orderPackage.ShippingLastName);


                logger.Info("C getCustomerInformation: using orderPackage - " + orderPackage.ShippingAddress);
                if (!string.IsNullOrEmpty(orderPackage.ShippingAddress))
                    rootOrder.Add("shippingAddress1", orderPackage.ShippingAddress);
                else if (!string.IsNullOrEmpty(orderPackage.SubjectOrder.ShippingAddress))
                    rootOrder.Add("shippingAddress1", orderPackage.SubjectOrder.ShippingAddress);

                logger.Info("D getCustomerInformation: using orderPackage null");
                rootOrder.Add("shippingAddress2", null);


                logger.Info("E getCustomerInformation: using orderPackage - " + orderPackage.ShippingCity);
                if (!string.IsNullOrEmpty(orderPackage.ShippingCity))
                    rootOrder.Add("shippingCity", orderPackage.ShippingCity);
                else if (!string.IsNullOrEmpty(orderPackage.SubjectOrder.ShippingCity))
                    rootOrder.Add("shippingCity", orderPackage.SubjectOrder.ShippingCity);

                logger.Info("F getCustomerInformation: using orderPackage - " + orderPackage.ShippingState);
                if (!string.IsNullOrEmpty(orderPackage.ShippingState))
                    rootOrder.Add("shippingState", orderPackage.ShippingState);
                else if (!string.IsNullOrEmpty(orderPackage.SubjectOrder.ShippingState))
                    rootOrder.Add("shippingState", orderPackage.SubjectOrder.ShippingState);

                logger.Info("G getCustomerInformation: using orderPackage - " + orderPackage.ShippingZip);
                if (!string.IsNullOrEmpty(orderPackage.ShippingZip))
                    rootOrder.Add("shippingZipcode", orderPackage.ShippingZip.Trim());
                else if (!string.IsNullOrEmpty(orderPackage.SubjectOrder.ShippingZipCode))
                    rootOrder.Add("shippingZipcode", orderPackage.SubjectOrder.ShippingZipCode);

                logger.Info("H getCustomerInformation: using orderPackage - " + orderPackage.ShippingCountry);
                if (!string.IsNullOrEmpty(orderPackage.ShippingCountry))
                    rootOrder.Add("shippingCountry", orderPackage.ShippingCountry);
                else
                    rootOrder.Add("shippingCountry", "");

                rootOrder.Add("shippingPhoneNumber", "");
                rootOrder.Add("emailAddress", "");
            }
            //return returnObject;
        }

        private ImageCodecInfo GetEncoder(ImageFormat format)
        {
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();
            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.FormatID == format.Guid)
                {
                    return codec;
                }
            }
            return null;
        }

        private void getCustomerInformation(Organization orgForBulkShipping, ref JsonObject rootOrder)
        {
            //JsonObject returnObject = new JsonObject();

            //
            //TODO: WE SHOULD ALWAYS USE STUDIO FOR BILLING INFO, BUT CP DOES NOT WANT ME TO DO THAT YET 10-12-2012 - Chad
            //
            //if (this._flowProject.Studio.LabCustomerID != null)
            //    returnObject.Add("billing-account-number", this._flowProject.Studio.LabCustomerID);
            //else
            //    returnObject.Add("billing-account-number", "");
            //returnObject.Add("billing-company", this._flowProject.Studio.OrganizationName);
            //returnObject.Add("billing-first-name", this._flowProject.Studio.OrganizationContactName);
            //returnObject.Add("billing-last-name", "");
            //returnObject.Add("billing-address-1", this._flowProject.Studio.OrganizationAddressLine1);
            //returnObject.Add("billing-address-2", this._flowProject.Studio.OrganizationAddressLine2);
            //returnObject.Add("billing-city", this._flowProject.Studio.OrganizationCity);
            //returnObject.Add("billing-state", this._flowProject.Studio.OrganizationStateCode);
            //returnObject.Add("billing-zipcode", this._flowProject.Studio.OrganizationShippingZipCode);
            //returnObject.Add("billing-country", "");
            //returnObject.Add("billing-phone-number", this._flowProject.Studio.OrganizationContactPhone);
            if (orgForBulkShipping.LabCustomerID != null)
                rootOrder.Add("billingAccountNumber", orgForBulkShipping.LabCustomerID.ToString());
            else
                if (this._flowProject.Studio.LabCustomerID != null)
                    rootOrder.Add("billingAccountNumber", this._flowProject.Studio.LabCustomerID);
                else
                    rootOrder.Add("billingAccountNumber", "");


            //returnObject.Add("billing-company", orgForBulkShipping.OrganizationName);
            //returnObject.Add("billing-first-name", orgForBulkShipping.OrganizationContactName);
            //returnObject.Add("billing-last-name", "");
            //returnObject.Add("billing-address-1", orgForBulkShipping.OrganizationAddressLine1);
            //returnObject.Add("billing-address-2", orgForBulkShipping.OrganizationAddressLine2);
            //returnObject.Add("billing-city", orgForBulkShipping.OrganizationCity);
            //returnObject.Add("billing-state", orgForBulkShipping.OrganizationStateCode);
            //returnObject.Add("billing-zipcode", orgForBulkShipping.OrganizationShippingZipCode.Trim());
            //returnObject.Add("billing-country", "");
            //returnObject.Add("billing-phone-number", orgForBulkShipping.OrganizationContactPhone);


            rootOrder.Add("shippingFirstName", orgForBulkShipping.OrganizationName);
            rootOrder.Add("shippingLastName", orgForBulkShipping.OrganizationContactName);
            rootOrder.Add("shippingAddress1", orgForBulkShipping.OrganizationShippingAddressLine1);
            rootOrder.Add("shippingAddress2", orgForBulkShipping.OrganizationShippingAddressLine2);
            rootOrder.Add("shippingCity", orgForBulkShipping.OrganizationShippingCity);
            rootOrder.Add("shippingState", orgForBulkShipping.OrganizationShippingStateCode);
            rootOrder.Add("shippingZipcode", orgForBulkShipping.OrganizationShippingZipCode.Trim());
            rootOrder.Add("shippingCountry", "");
            rootOrder.Add("shippingPhoneNumber", orgForBulkShipping.OrganizationContactPhone);
            rootOrder.Add("emailAddress", orgForBulkShipping.OrganizationContactEmail);
            //return returnObject;
        }



        MemoryStream RenderLayoutPreviewToMs(Subject curSubject, SubjectImage orderedSubjectImage, DisplayableFile _currentLayoutFile, string gsBg)
        {
            if (_currentLayoutFile == null) return null;
            try
            {
                Layout layout = ObjectSerializer.FromXmlFile<Layout>(_currentLayoutFile.FileInfo.FullName);
                
                layout.ImageTypes = Flow.Designer.Model.Project.ImageTypes.Build(this._flowController.ProjectController.FlowMasterDataContext);
                layout.DesignerDynamicFields = Flow.Designer.Model.Project.DesignerDynamicFields.Build(this._flowController.ProjectController.CurrentFlowProject);
                layout.OrderedSubjectImage = orderedSubjectImage;

                if (!string.IsNullOrEmpty(gsBg))
                {
                    layout.OrderedSubjectImage.GreenScreenBackground = gsBg;
                }

                FlowProject proj = this._flowController.ProjectController.CurrentFlowProject;
                if (proj.SubjectCount > 0)
                {
                    var curSub = curSubject;
                    var curImages = curSub.SubjectImages;


                    layout.Subject = curSub.ToDesignerSubject();
                }

                Canvas canvas = layout.ToCanvas(300);

                MemoryStream ms = new MemoryStream();
                new FrameworkElementRenderService().RenderToJpegStream(canvas, 300, ms);
                return ms;
            }
            catch (Exception e)
            {
                System.Windows.MessageBox.Show("Error: " + e.ToString());
                logger.ErrorException("Exception occurred", e);
                return null;
            }
        }

        private System.Drawing.Rectangle getCropArea(System.Drawing.Image iSrc, double CropL, double CropT, double CropH, double CropW)
        {
            double width = iSrc.Width;
            double height = iSrc.Height;

            int cropAreaL = (int)(CropL * width);
            int cropAreaT = (int)(CropT * height);
            int cropAreaW = (int)(CropW * width);
            int cropAreaH = (int)(CropH * height);

            if (cropAreaL + cropAreaW > width)
                cropAreaW = (int)width - cropAreaL;
            if (cropAreaT + cropAreaH > height)
                cropAreaH = (int)height - cropAreaT;


            return new System.Drawing.Rectangle(
                cropAreaL,
                cropAreaT,
                cropAreaW,
                cropAreaH
                );
        }

        private static System.Drawing.Image cropImage(System.Drawing.Image img, System.Drawing.Rectangle cropArea)
        {
            Bitmap bmpImage = new Bitmap(img);
            Bitmap bmpCrop = bmpImage.Clone(cropArea,
            bmpImage.PixelFormat);
            bmpImage.Dispose();
            return (System.Drawing.Image)(bmpCrop);
        }




        
    }

    public class AuthenticateResults
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
    }

    public class OrderResults
    {
        public OrderShort order_short { get; set; }
    }

    public class OrderShort
    {
        public string id { get; set; }
        public string catalog_id { get; set; }
        public string lab_id { get; set; }
        public int sequential_id { get; set; }
        public string studio_id { get; set; }
        public string status { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }
    }


    //public class SubjectImagePlus
    //{
    //    public SubjectImage SubjectImage { get; private set; }
    //    public string Background { get; private set; }
    //    public string NewImageName { get; private set; }
    //    public string NewImagePath { get; private set; }
    //    public bool IsRendered { get; private set; }

    //    public SubjectImagePlus(SubjectImage si, string background, string newImageName, string newImagePath, bool isRendered)
    //    {
    //        SubjectImage = si;
    //        Background = background;
    //        NewImageName = newImageName;
    //        NewImagePath = newImagePath;
    //        IsRendered = isRendered;
    //    }
    //}
}
