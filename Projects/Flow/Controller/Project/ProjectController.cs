﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.IO;
using System.Text;
using System.Windows.Threading;

using Flow.Lib;
using Flow.Lib.Helpers;
using Flow.View.Project;
using Flow.Schema;
using Flow.Schema.LinqModel.DataContext;
//using Flow.Schema.LinqModel.FlowMaster;

using ICSharpCode.SharpZipLib.Zip;
using StructureMap;
using Flow.Lib.Net;
using Flow.View.Dialogs;
using System.Threading;
using Flow.Lib.AsyncWorker;
using System.Collections.ObjectModel;
using System.Windows.Media.Imaging;
using System.Data.SqlServerCe;
using System.Data.Common;
using NLog;
using System.Net;
using NetServ.Net.Json;
using System.Web.Script.Serialization;
using Flow.Lib.Preferences;
using System.Xml.Serialization;

namespace Flow.Controller.Project
{
	/// <summary>
	/// Manages all aspects of Flow Project resources; derived from Ling-to-SQL entity class FlowProject
	/// </summary>
	public class ProjectController : INotifyPropertyChanged		//, IProjectControllerDataContext
	{
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public event EventHandler OpenCaptureInvoked = delegate { };

        static readonly object locker = new object();

        public CustomSettings CustomSettings { get; set; }

        public List<string> AllLayouts { get; set; }

        public List<string> SelectedLayouts { get; set; }

        public List<string> GalleryShippingOptions
        {
            get
            {
                List<string> l = new List<string>();
                l.Add("pickup"); l.Add("school"); l.Add("home");
                return l;
            }
        }
        private bool _flowServerIsOn = false;
        public bool FlowServerIsOn {
            get{ return _flowServerIsOn; }
            set{ 
                _flowServerIsOn = value;
                if (_flowServerIsOn)
                {
                    FlowServerStatus = "Server is On";
                    this._flowController.ApplicationPanel.MainMenuItems.IsEnabled = false;
                    this._flowController.ApplicationPanel.PreferencesMenuItem.IsEnabled = false;
                    this._flowController.ProjectController.MainProjectPanel.Project_menu_items.IsEnabled = false;
                    this.FlowController.FlowServerPanel();
                }
                else
                {
                    FlowServerStatus = "Server is Off";
                    this._flowController.ApplicationPanel.MainMenuItems.IsEnabled = true;
                    this._flowController.ApplicationPanel.PreferencesMenuItem.IsEnabled = true;
                    this._flowController.ProjectController.MainProjectPanel.Project_menu_items.IsEnabled = true;
                }
                logger.Info("Server status changed: {0}", FlowServerStatus);
                NotifyPropertyChanged("FlowServerIsOn");
            }
        }

        private string _flowServerStatus = "Server is Off";
        public string FlowServerStatus
        {
            get { return _flowServerStatus; }
            set { _flowServerStatus = value;
            NotifyPropertyChanged("FlowServerStatus");
            }
        }

      

        public ObservableCollection<IncomingProjectFile> IncomingClientProjects = new ObservableCollection<IncomingProjectFile>();
        public ObservableCollection<IncomingProjectFile> IncomingClientProjectsQC = new ObservableCollection<IncomingProjectFile>();

        public void UpdateServerIncoming()
        {
            NotifyPropertyChanged("IncomingClientProjects");
            NotifyPropertyChanged("IncomingClientProjectsQC");
        }
        private List<AllProjectSearchResult> _allProjectsSearchResults { get; set; }
        public List<AllProjectSearchResult> AllProjectsSearchResults{
            get {
                if (_allProjectsSearchResults == null) 
                    _allProjectsSearchResults = new List<AllProjectSearchResult>(); 
                return _allProjectsSearchResults;
            }
            set {_allProjectsSearchResults = value;}
        }

       
        private PlicCatalogContent _plicCatalog { get; set; }
        public PlicCatalogContent PlicCatalog
        {
            get{
                if(_plicCatalog == null)
                {
                    PLICCatalog pcatStore = this.FlowMasterDataContext.PLICCatalogs.FirstOrDefault();
                    if (pcatStore != null)
                    {
                        JavaScriptSerializer json_serializer2 = new JavaScriptSerializer();
                        _plicCatalog = json_serializer2.Deserialize<PlicCatalogContent>(pcatStore.JSON);
                       
                    }
                }
                return _plicCatalog;
            }
        }

		#region INotifyPropertyChanged Members

		public event PropertyChangedEventHandler PropertyChanged;

	    private void NotifyPropertyChanged(String info)
		{
			if (PropertyChanged != null)
				PropertyChanged(this, new PropertyChangedEventArgs(info));
		}

		#endregion

        FlowController _flowController;
        public FlowController FlowController
        {
            get { return _flowController; }
        }

        public bool IsProjectLoaded
        {
            get
            {
                if (this.CurrentFlowProject != null)
                    return true;
                return false;
            }
        }

        public bool IsNetworkProject
        {
            get
            {
                if (this.CurrentFlowProject != null && this.CurrentFlowProject.IsNetworkProject == true)
                    return true;

                return false;
            }

        }

        public bool NetworkProjectNeedsUpdate
        {
            get
            {
                if (this.CurrentFlowProject != null && this.CurrentFlowProject.FlowProjectDataContext.UpdateNetworkProject == true)
                    return true;

                return false;
            }

        }
        public bool specialBarCodeHandled = false;

		private MainProjectPanel _mainProjectPanel = null;
		internal MainProjectPanel MainProjectPanel
		{
			set { _mainProjectPanel = value; }
            get { return _mainProjectPanel; }
		}
	
		private FlowMasterDataContext _flowMasterDataContext = null;
		public FlowMasterDataContext FlowMasterDataContext
		{
			get { return _flowMasterDataContext; }
		}

        public FlowObservableCollection<OrderFormFieldLocal> OrderFormFieldsLocalList = new FlowObservableCollection<OrderFormFieldLocal>();
        public void CreateOrderFormFields(string projectTemplateName)
        {
            OrderFormFieldsLocalList.Clear();
            foreach (OrderFormField field in this.FlowMasterDataContext.OrderFormFields.Where(o => o.ProjectTemplateName == projectTemplateName).OrderBy(o => o.SortOrder))
            {
                OrderFormFieldLocal localField = new OrderFormFieldLocal();
                localField.FieldID = field.FieldID;
                localField.ConfigFileID = field.ConfigFileID;
                localField.FieldName = field.FieldName;
                localField.FieldType = field.FieldType;
                localField.FieldValue = field.FieldValue;
                localField.IsRequired = field.IsRequired;
                localField.SortOrder = field.SortOrder;
                
                if (field.FieldType == 3)
                    //localField.SelectedValue = field.FieldValue.Split(',')[0];
                    localField.SelectedValue = "";
                else
                    localField.SelectedValue = field.FieldValue;
                
                OrderFormFieldsLocalList.Add(localField);
            }
        }

		// Represents the Flow Project currently open in the application
		private FlowProject _currentFlowProject = null;
		public FlowProject CurrentFlowProject
		{
			get { return _currentFlowProject; }
            private set
            {
                _currentFlowProject = value;
                if(value == null)
                    logger.Info("Current project changed and is now null");
                else
                    logger.Info("Current project changed: name='{0}' guid='{1}'", value.FlowProjectName, value.FlowProjectGuid);
                this.NotifyPropertyChanged("IsProjectLoaded");
                
                //this.NotifyPropertyChanged("CurrentFlowProjectGallery");
            }
		}

        public ProjectGallery CurrentFlowProjectGallery
        {
            get
            {
                if (this.CurrentFlowProject != null && this.CurrentFlowProject.FlowProjectDataContext.ProjectGalleries.Any())
                {
                    return this.CurrentFlowProject.FlowProjectDataContext.ProjectGalleries.First();
                }

                return null;
            }
        }
        public OnlineGallery CurrentFlowOnlineGallery
        {
            get
            {
                if (this.CurrentFlowProject != null && this.CurrentFlowProject.FlowProjectDataContext.OnlineGalleries.Any())
                {
                    return this.CurrentFlowProject.FlowProjectDataContext.OnlineGalleries.First();
                }

                return null;
            }
        }
        public void UpdateCurrentFlowOnlineGallery()
        {
            logger.Info("Current flow project gallery updated");
            this.NotifyPropertyChanged("CurrentFlowOnlineGallery");
        }
        public void UpdateCurrentFlowProjectGallery()
        {
            logger.Info("Current flow project gallery updated");
            this.NotifyPropertyChanged("CurrentFlowProjectGallery");
        }
        private FlowProject _newFlowProject = null;
        public FlowProject NewFlowProject
        {
            get { return _newFlowProject; }
            private set
            {
                _newFlowProject = value;
            }
        }

		// Represents the Flow Project Template currently referenced in the app
		private ProjectTemplate _currentProjectTemplate = null;
		public ProjectTemplate CurrentProjectTemplate
		{
			get { return _currentProjectTemplate; }
			set
			{
				if (_currentProjectTemplate != value)
				{
					_currentProjectTemplate = value;
                    if(_currentProjectTemplate != null)
                        logger.Info("Current project template changed: id='{0}' desc='{1}' guid='{2}'",
                            value.ProjectTemplateID, value.ProjectTemplateDesc, value.ProjectTemplateGuid);
					NotifyPropertyChanged("CurrentProjectTemplate");
				}
			}
		}

        
		/// <summary>
		/// Constructor
		/// </summary>
		public ProjectController(FlowController flowController)
		{
            _flowController = flowController;
//            _flowMasterDataContext = new FlowMasterRepository().BuildStandardDataContext();

            string connectionString2 = "Data Source=" + this.FlowController.FlowMasterFile + ";Max Database Size=1024;";
            SqlCeEngine eng = new SqlCeEngine(connectionString2);
            BackupSDF35(eng,this.FlowController.FlowMasterFile);
            eng.Dispose();
			_flowMasterDataContext = new FlowMasterDataContext(new SqlCeConnection("Data Source=" + this.FlowController.FlowMasterFile + ";Max Database Size=1024;"));
            //_flowMasterDataContext = new FlowMasterDataContext(false);
            
            //_flowMasterDataContext = flowController.ProjectController.FlowMasterDataContext;

            


			//Preference prefCaptureHotFolder = _flowMasterDataContext.PreferenceManager.Capture.HotFolderDirectory["Capture.HotFolderDirectory"];
	
			//if(prefCaptureHotFolder.NativeValue == null)
			//{
			//    prefCaptureHotFolder.Value = FlowContext.FlowHotFolderDirPath;
			//    _flowMasterDataContext.SubmitChanges();
			//}
            
		}


        public void BackupSDF35(System.Data.SqlServerCe.SqlCeEngine engine, string dbPath)
        {
            SQLCEVersion fileversion = DetermineVersion(dbPath);
            if (fileversion < SQLCEVersion.SQLCE40)
            {
                string BackupFolder = System.IO.Path.Combine(FlowContext.FlowAppDataDirPath, "BackupSDF35");
                if (!Directory.Exists(BackupFolder))
                    Directory.CreateDirectory(BackupFolder);

                string destFile = System.IO.Path.Combine(BackupFolder, (new FileInfo(dbPath)).Name);
                if (!File.Exists(destFile))
                    File.Copy(dbPath, destFile);
                try
                {
                    engine.Upgrade();
                }
                catch
                {
                    //do nothing
                }
            }
        }

        private enum SQLCEVersion
        {
            SQLCE20 = 0,
            SQLCE30 = 1,
            SQLCE35 = 2,
            SQLCE40 = 3
        }

        private static SQLCEVersion DetermineVersion(string filename)
        {
            var versionDictionary = new Dictionary<int, SQLCEVersion> 
        { 
            { 0x73616261, SQLCEVersion.SQLCE20 }, 
            { 0x002dd714, SQLCEVersion.SQLCE30},
            { 0x00357b9d, SQLCEVersion.SQLCE35},
            { 0x003d0900, SQLCEVersion.SQLCE40}
        };
            int versionLONGWORD = 0;
            try
            {
                using (var fs = new FileStream(filename, FileMode.Open))
                {
                    fs.Seek(16, SeekOrigin.Begin);
                    using (BinaryReader reader = new BinaryReader(fs))
                    {
                        versionLONGWORD = reader.ReadInt32();
                    }
                }
            }
            catch
            {
                throw;
            }
            if (versionDictionary.ContainsKey(versionLONGWORD))
            {
                return versionDictionary[versionLONGWORD];
            }
            else
            {
                throw new ApplicationException("Unable to determine database file version");
            }
        }


        public void LoadCustomSettings()
        {
            string CustomSettingsFolder = Path.Combine(FlowContext.FlowAppDataDirPath, "CustomSettings");
            string SettingsFile = Path.Combine(CustomSettingsFolder, "CustomSettings.xml");

            if (!FlowContext.ShowNewFeatures)
            {
                //if showNewFeatures is off, load default CustomSettings and delete any custom settings file
                this.CustomSettings = new CustomSettings();
                this.CustomSettings.Initialize();
                if (File.Exists(SettingsFile))
                {
                    File.Delete(SettingsFile);
                }
                return;
            }


            if (!Directory.Exists(CustomSettingsFolder))
                Directory.CreateDirectory(CustomSettingsFolder);

            if (File.Exists(SettingsFile))
            {
                using (var stream = System.IO.File.OpenRead(SettingsFile))
                {
                    var serializer = new XmlSerializer(typeof(CustomSettings));
                    CustomSettings = serializer.Deserialize(stream) as CustomSettings;
                }
            }
            //else
            //{
            if (this.CustomSettings == null)
                this.CustomSettings = new CustomSettings();
            this.CustomSettings.Initialize();
            var serializer2 = new XmlSerializer(typeof(CustomSettings));
            using (var writer = new StreamWriter(SettingsFile))
            {
                serializer2.Serialize(writer, CustomSettings);
            }
            //}

            //act on any of the custom settings:
                if (this.CustomSettings.UseSubjectExcludeFlag)
                    this.FlowMasterDataContext.PreferenceManager.Capture.UseExcludeSubjectFlag = true;

               
  
        }

        void FlowProjectDataContext_UpdateNetworkProjectNeeded(object sender, EventArgs e)
        {
            this.NotifyPropertyChanged("NetworkProjectNeedsUpdate");
        }

		public void OpenCapture()
		{
			this.OpenCaptureInvoked(this, new EventArgs());
		}

		/// <summary>
		/// Create a blank project object
		/// </summary>
		public void CreateNewProject()
		{
            logger.Info("Creating new blank project object");
            //if (this.FlowController.EditController != null)
            //    this.FlowController.EditController.ClearDataGridColumns();

			this.NewFlowProject = new FlowProject();
            this.NewFlowProject.IsGreenScreen = false;
            //Very Very Very Important
           // NotifyPropertyChanged("CurrentFlowProject");
			_mainProjectPanel.ShowNewProjectPanel();
		}

        public void OpenSearchAllProjects()
		{
            _mainProjectPanel.ShowSearchAllProjects();
		}
		/// <summary>
		/// OPen the project preferences screen for editing
		/// </summary>
		/// <param name="flowProject"></param>
		public void EditProjectPreferences(FlowProject flowProject)
		{
			if (this.CurrentFlowProject == flowProject)
			{
				_mainProjectPanel.ShowEditProjectPanel();
				return;
			}

			this.Load(flowProject);
            if (flowProject.FlowProjectDataContext != null)
            {
                flowProject.FlowProjectDataContext.SubjectDataLoaded += delegate
                {
                    _mainProjectPanel.ShowEditProjectPanel();
                    if (this.CurrentFlowProject.ProblemLoadingSubjectData)
                    {
                        string warning = "There was a problem loading some Subject Data in this project.\n\nYou may have some subjects that are missing required field data.";
                        FlowMessageBox msg = new FlowMessageBox("Warning", warning);
                        logger.Warn(warning);
                        msg.CancelButtonVisible = false;
                        msg.ShowDialog();
                    }
                };
            }
		}

		/// <summary>
		/// Imports an external project (.sdf file)
		/// </summary>
		public void ImportProject()
		{
            if (this.CurrentFlowProject != null)
                _mainProjectPanel.ShowProjectExportPanel(this.CurrentFlowProject, false);
			_mainProjectPanel.ShowProjectImportPanel();
		}

		/// <summary>
		/// Opens the DataImportPanel for export or upload of project
		/// </summary>
		/// <param name="flowProject"></param>
		/// <param name="forUpload"></param>
		public void ExportProject(FlowProject flowProject, bool forUpload)
		{
			_mainProjectPanel.ShowProjectExportPanel(flowProject, forUpload);
		}

        /// <summary>
        /// Opens the OrderPanel for viewing and submittting
        /// </summary>
        /// <param name="flowProject"></param>
        /// <param name="forUpload"></param>
        public void SubmitOrders(FlowProject flowProject)
        {
            _mainProjectPanel.ShowOrderPanel(flowProject);
           
        }

        public void SubmitOnlineOrderData(FlowProject flowProject)
        {
            _mainProjectPanel.ShowOnlineOrderPanel(flowProject);
           
        }

        public void SubmitOnlineOrders(FlowProject flowProject)
        {
            _mainProjectPanel.ShowOnlineOrdersPanel(flowProject);

        }

        public void ViewRemoteImageServiceStatus(FlowProject flowProject)
        {
            _mainProjectPanel.ShowRemoteImageServiceStatus(flowProject);
           
        }

        

		/// <summary>
		/// Assigns the provided project template as the current
		/// </summary>
		/// <param name="projectTemplate"></param>
		public void EditProjectTemplate(ProjectTemplate projectTemplate)
		{
            
			this.CurrentProjectTemplate = projectTemplate;
            this.CurrentProjectTemplate.UpdateProjectTemplateTriggers();
            this.CurrentProjectTemplate.UpdateImageOptions();
			_mainProjectPanel.ShowEditProjectTemplatePanel();
		}

		/// <summary>
		/// Creates a blank project template object
		/// </summary>
		public void CreateNewProjectTemplate()
		{
			this.CurrentProjectTemplate = new ProjectTemplate(this.FlowMasterDataContext);
            this.CurrentProjectTemplate.ProjectTemplateGuid = Guid.NewGuid();
			_mainProjectPanel.ShowEditProjectTemplatePanel();
		}

		/// <summary>
		/// 
		/// </summary>
		public void CreateNewFlowCatalog()
		{
			_mainProjectPanel.ShowFlowCatalogEditor();
		}

		/// <summary>
		/// 
		/// </summary>
		public void ImportImageQuixCatalogs()
		{
			_mainProjectPanel.ShowImportImageQuixCatalogs();
		}


		//public void ImportData()
		//{
		//    _mainProjectPanel.ShowDataImportPanel();
		//}




		/// <summary>
		/// Generates a new FlowProject record and creates all necessary resources
		/// </summary>
		public void GenerateProject(Flow.Schema.LinqModel.DataContext.FlowProject newFlowProject, ProjectTemplate projectTemplate)
		{
            logger.Info("BEGIN generating new FlowProject record: name='{0}' guid='{1}'",
                newFlowProject.FlowProjectName, newFlowProject.FlowProjectGuid);
           
			// Save the new project record (FlowMaster.FlowProject)
			_flowMasterDataContext.SaveProject(newFlowProject, projectTemplate); 

			// Create the project directory and image subdirectory
			IOUtil.CreateDirectory(newFlowProject.DirPath, true);
			IOUtil.CreateDirectory(newFlowProject.ImageDirPath, true);

			// Copy project template files into the project directory
			CreateProjectFiles(newFlowProject);

			// Copy data from the project template to the 
			newFlowProject.InitializeProjectFromTemplate(projectTemplate);

			//this.CurrentFlowProject = newFlowProject;
            //Very Very Very Important
            //NotifyPropertyChanged("CurrentFlowProject");

            logger.Info("DONE generating new FlowProject record: name='{0}' guid='{1}'",
                newFlowProject.FlowProjectName, newFlowProject.FlowProjectGuid);
		}

		/// <summary>
		/// Copies all project template files into the new project directory
		/// </summary>
		/// <param name="projectGuid"></param>
		public void CreateProjectFiles(Flow.Schema.LinqModel.DataContext.FlowProject newFlowProject)
		{
			// Copy the project template database to the project directory
			string sourcePath = Path.Combine(FlowContext.FlowConfigDirPath, "FlowProjectTemplate.sdf");

			FileInfo templateDatabase = new FileInfo(sourcePath);

			templateDatabase.CopyTo(newFlowProject.LocalDBPath);

			// Copy all other files...
		}

		///// <summary>
		///// Imports an external flow project into the local Flow instance
		///// </summary>
		///// <param name="projectFilePath"></param>
		///// <param name="openAfterImport"></param>
		//public void AttachProject(string projectFilePath, bool openAfterImport)
		//{
		//    FileInfo sourceDatabase = new FileInfo(projectFilePath);

		//    // Get the base filename {guid}.sdf
		//    string sourceDatabaseFileName = sourceDatabase.Name;

		//    // Extract the project guid from the filename
		//    string projectGuid = sourceDatabaseFileName.Remove(sourceDatabaseFileName.LastIndexOf('.'));
	
		//    // Assign the directory path for the project
		//    string targetDatabaseDirPath = Path.Combine(FlowContext.FlowProjectsDirPath, projectGuid);

		//    // Assign the full file path for the target project database
		//    string targetDatabaseFilePath = Path.Combine(targetDatabaseDirPath, sourceDatabaseFileName);

		//    // Create the project dir (and Image sub-dir)
		//    IOUtil.CreateDirectory(targetDatabaseDirPath, true);
		//    IOUtil.CreateDirectory(
		//        Path.Combine(targetDatabaseDirPath, "Image"),
		//        true
		//    );

		//    // Copy the source db file to its destination location
		//    sourceDatabase.CopyTo(targetDatabaseFilePath);

		//    // Attach the project (generate FlowMaster project meta-data records
		//    FlowProject importProject = _flowMasterDataContext.AttachProject(targetDatabaseFilePath);

		//    if (openAfterImport)
		//        this.CurrentFlowProject = importProject;
		//}

        public FlowProject AttachNetworkProject(DirectoryInfo networkProjectFolder)
        {
            logger.Info("Attaching network project: directory='{0}'", networkProjectFolder.ToString());
            FileInfo sdfFile = networkProjectFolder.GetFiles().First(fn => fn.Extension == ".sdf");
            // Assign the full file path for the target project database
            string targetDatabaseFilePath = sdfFile.FullName;

            //copy the sdf locally
            string localcopy = Path.Combine(this.FlowMasterDataContext.PreferenceManager.Application.ProjectsDirectory, sdfFile.Name);
            FileCopy(sdfFile.FullName, localcopy, true);
            new System.Security.Permissions.FileIOPermission(System.Security.Permissions.FileIOPermissionAccess.Read, new string[] { sdfFile.FullName }).Demand();


            string constr = "Data Source=" + FlowContext.FlowProjectTemplateFilePath + ";Max Database Size=1024;";
            SqlCeEngine eng = new SqlCeEngine(constr);
            BackupSDF35(eng,FlowContext.FlowProjectTemplateFilePath);

            // Attach the project (generate FlowMaster project meta-data records
            FlowProject importedProject = _flowMasterDataContext.AttachProject(localcopy, true);
            //UpdatedSavedSubjectCount(importedProject);

            if (importedProject.ProblemImportingFlowCatalog && FlowMasterDataContext.PreferenceManager.Application.IsNotBasic)
            {
                string warning = "The project you are importing contains a catalog that does not match current Flow settings.\nPlease check this catalog before further use of this project!\n";
                logger.Warn(warning);
                FlowMessageBox msg = new FlowMessageBox("Warning", warning);
                msg.CancelButtonVisible = false;
                msg.ShowDialog();
            }
            
            //_flowController.Project();
            //this._flowController.ApplicationPanel.IsEnabled = true;
            return importedProject;
        }

		/// <summary>
		/// Imports an external flow project archive into the local Flow instance
		/// </summary>
		/// <param name="projectFilePath"></param>
		/// <param name="openAfterImport"></param>
		public void AttachProject(string archiveFilePath, bool openAfterImport, FlowController flowController)
		{
            logger.Info("Importing Flow project PAF: archiveFilePath='{0}' openAfterImport={1}",
                archiveFilePath, openAfterImport.ToString());

            //this will clear the current image filter
            if (CurrentFlowProject != null && CurrentFlowProject.SubjectList != null)
                CurrentFlowProject.SubjectList.ClearFilter();

            this._flowController.ApplicationPanel.IsEnabled = false;
			string tempID = Guid.NewGuid().ToString();
            _flowController = flowController;
            ProjectImporter importer = new ProjectImporter(_flowController, this, FlowMasterDataContext);
           
            importer.ImportCompelete += new ProjectImporter.ImportCompleteDelegate(importer_ImportComplete);
            importer.BeginAsyncAttachProject(archiveFilePath, openAfterImport);
		}

        /// <summary>
        /// Imports an external flow project directory (uncompressed) into the local Flow instance
        /// </summary>
        /// <param name="projectFilePath"></param>
        /// <param name="openAfterImport"></param>
        public void AttachProjectUncompressed(string sourceDirectory, bool openAfterImport, FlowController flowController)
        {
            logger.Info("Importing uncompressed Flow project folder: sourceDirectory='{0}' openAfterImport={1}",
                sourceDirectory, openAfterImport.ToString());

            //this will clear the current image filter
            if (CurrentFlowProject != null && CurrentFlowProject.SubjectList != null)
                CurrentFlowProject.SubjectList.ClearFilter();

            this._flowController.ApplicationPanel.IsEnabled = false;
            string tempID = Guid.NewGuid().ToString();
            _flowController = flowController;
            ProjectImporter importer = new ProjectImporter(_flowController, this, FlowMasterDataContext);

            importer.ImportCompelete += new ProjectImporter.ImportCompleteDelegate(importer_ImportComplete);
            importer.BeginAsyncAttachProjectUncompressed(sourceDirectory, openAfterImport);
        }

        void importer_ImportComplete(ProjectImporter importer)
        {
            logger.Info("Completing project import");

            FileInfo sourceDatabase = new FileInfo(importer.SourceDatabaseFiles[0]);

            // Get the base filename {guid}.sdf
            string sourceDatabaseFileName = sourceDatabase.Name.Replace(".paf", ".sdf"); ;

            // Extract the project guid from the filename
            string projectGuid = sourceDatabaseFileName.Remove(sourceDatabaseFileName.LastIndexOf('.'));

            string targetDatabaseDirPath = importer.TempPath.Replace(importer.TempID, projectGuid);

            if (Directory.Exists(targetDatabaseDirPath))
            {

                try
                {
                    Directory.Delete(targetDatabaseDirPath, true);
                }
                catch {
                    FlowMessageBox msg = new FlowMessageBox("Unable to Import", "There was an error creating the directory needed for importing, please restart flow and try to import again");
                    logger.Error("{0} - {1}", msg.Title, msg.txtMessage);
                    msg.CancelButtonVisible = false;
                    msg.ShowDialog();
                    this._flowController.ApplicationPanel.IsEnabled = true;
                    return;
                }

            }

            //when moving the folder, if it fails, try again 3 times before erroring out
            bool tryagain = true;
            int tryCount = 0;
            while (tryagain)
            {
                tryCount++;
                try
                {
                    Directory.Move(importer.TempPath, targetDatabaseDirPath);
                    tryagain = false;
                }
                catch(Exception e)
                {
                    if (tryCount == 3)
                    {
                        tryagain = false;
                        throw e;
                    }
                }
            }
            // Assign the full file path for the target project database
            string targetDatabaseFilePath = Path.Combine(targetDatabaseDirPath, sourceDatabaseFileName);

            // Attach the project (generate FlowMaster project meta-data records
            FlowProject importedProject = _flowMasterDataContext.AttachProject(targetDatabaseFilePath, false);
            UpdatedSavedSubjectCount(importedProject);
            importedProject.Connect(_flowMasterDataContext);
            importedProject.PrepareForExport();

            if (importedProject.ProblemImportingFlowCatalog && FlowMasterDataContext.PreferenceManager.Application.IsNotBasic)
            {
                FlowMessageBox msg = new FlowMessageBox("Warning", "The project you are importing contains a catalog that does not match current Flow settings.\nPlease check this catalog before further use of this project!\n");
                msg.CancelButtonVisible = false;
                msg.ShowDialog();
            }
            if (importer.OpenAfterImport)
            {
                _flowController.ProjectController.Load(importedProject, false);
                importedProject.FlowProjectDataContext.SubjectDataLoaded += delegate
                {
                    _flowController.Capture();
                    if (this.CurrentFlowProject.ProblemLoadingSubjectData)
                    {
                        FlowMessageBox msg = new FlowMessageBox("Warning", "There was a problem loading some Subject Data in this project.\n\nYou may have some subjects that are missing required field data.");
                        msg.CancelButtonVisible = false;
                        msg.ShowDialog();
                    }
                };
            }
            else
            {
                _flowController.Project();
            }
            this.FlowMasterDataContext.RefreshOrgList();
            this._flowController.ApplicationPanel.IsEnabled = true;
            this.NotifyPropertyChanged("ProjectImportComplete");
            logger.Info("Project import complete");
        }


		/// <summary>
		/// 
		/// </summary>
		/// <param name="archiveFilePath"></param>
		/// <param name="rootProject"></param>
		/// <param name="openAfterImport"></param>
		/// <param name="flowController"></param>
		internal void MergeProject(string archiveFilePath, FlowProject rootProject, bool openAfterImport, FlowController flowController)
		{

            if (archiveFilePath.EndsWith(".sdf"))
            {
                 string connectionString2 = "Data Source=" + archiveFilePath + ";Max Database Size=1024;";
                 SqlCeEngine eng = new SqlCeEngine(connectionString2);
                BackupSDF35(eng, archiveFilePath);
                eng.Dispose();
            }
            if (rootProject.MainDispatcher == null)
                rootProject.MainDispatcher = Dispatcher.CurrentDispatcher;

            logger.Info("Merging Flow project PAF: archiveFilePath='{0}' openAfterImport={1}",
                archiveFilePath, openAfterImport.ToString());

            //this._flowController.ApplicationPanel.IsEnabled = false;
            this._flowController.ApplicationPanel.MainMenuItems.IsEnabled = false;
            this._flowController.ApplicationPanel.PreferencesMenuItem.IsEnabled = false;
            this._flowController.ProjectController.MainProjectPanel.Project_menu_items.IsEnabled = false;

            if (this.FlowController.EditController != null)
            {
                this.FlowController.EditController.ClearDataGridColumns();
            }
            //this will clear the current image filter
            if(CurrentFlowProject != null && CurrentFlowProject.SubjectList != null)
                CurrentFlowProject.SubjectList.ClearFilter();

			MergeManager mergeManager = new MergeManager();

			mergeManager.ArchiveFilePath = archiveFilePath;
			mergeManager.RootProject = rootProject;
			mergeManager.FlowMasterDataContext = this.FlowMasterDataContext;

			mergeManager.ProgressInfo = new NotificationProgressInfo("Merge project", "Initializing...");
			mergeManager.ProgressInfo.NotificationProgress = ObjectFactory.GetInstance<IShowsNotificationProgress>();

			mergeManager.MergeCompleted += delegate
			{
                this.CurrentFlowProject = null;
                NotifyPropertyChanged("MergeComplete");
                logger.Info("Done merging project: archiveFilePath='{0}' openAfterImport={1}", archiveFilePath, openAfterImport.ToString());
                if (openAfterImport)
                {
                    //this.CurrentFlowProject = rootProject.Connect(this.FlowMasterDataContext);
                    //rootProject.FlowProjectDataContext.InitSubjectList();
                    ////Very Very Very Important
                    //NotifyPropertyChanged("CurrentFlowProject");
                    Load(rootProject, true, true);
                    CurrentFlowProject.FlowProjectDataContext.SubjectDataLoaded += delegate
                    {
                        flowController.Capture();
                    };
                }
                else
                {
                    
                    if (this.FlowMasterDataContext.PreferenceManager.Application.IsFlowServer)
                        flowController.FlowServerPanel();
                    else
                        flowController.Project();
                }

                //this._flowController.ApplicationPanel.IsEnabled = true;
                if (!FlowServerIsOn)
                {
                    this._flowController.ApplicationPanel.MainMenuItems.IsEnabled = true;
                    this._flowController.ApplicationPanel.PreferencesMenuItem.IsEnabled = true;
                    this._flowController.ProjectController.MainProjectPanel.Project_menu_items.IsEnabled = true;
                }
			};

			// Temporary thread override for troubleshooting
            try
            {
                string connectionString = "Data Source=" + FlowContext.FlowProjectTemplateFilePath + ";Max Database Size=1024;";
                SqlCeEngine eng = new SqlCeEngine(connectionString);
                BackupSDF35(eng, FlowContext.FlowProjectTemplateFilePath);
                eng.Dispose();


                string connectionString2 = "Data Source=" + mergeManager.RootProject.DatabasePath + ";Max Database Size=1024;";
                SqlCeEngine eng2 = new SqlCeEngine(connectionString2);
                BackupSDF35(eng2, mergeManager.RootProject.DatabasePath);
                eng2.Dispose();

                mergeManager.Merge();
            }
            catch (Exception e)
            {
                Thread.Sleep(8000);
                try
                {
                    mergeManager.Merge();
                }
                catch
                {
                    mergeManager.ProgressInfo.Complete("Database might be corrupt");
                    this._flowController.ApplicationPanel.IsEnabled = true;
                    throw (e);
                }
            }

			//FlowBackgroundWorkerManager.RunWorker(
			//    delegate
			//    {
			//        mergeManager.Merge();
			//    }
			//);
		}


        /// <summary>
        /// 
        /// </summary>
        /// <param name="archiveFilePath"></param>
        /// <param name="rootProject"></param>
        /// <param name="openAfterImport"></param>
        /// <param name="flowController"></param>
        internal void MergeProjectUncompressed(string sourceDirectory, FlowProject rootProject, bool openAfterImport, FlowController flowController)
        {
            if (rootProject.MainDispatcher == null)
                rootProject.MainDispatcher = Dispatcher.CurrentDispatcher;

            logger.Info("Merging uncompressed Flow project folder: sourceDirectory='{0}' openAfterImport={1}",
                sourceDirectory, openAfterImport.ToString());

            //this._flowController.ApplicationPanel.IsEnabled = false;
            this._flowController.ApplicationPanel.MainMenuItems.IsEnabled = false;
            this._flowController.ApplicationPanel.PreferencesMenuItem.IsEnabled = false;
            this._flowController.ProjectController.MainProjectPanel.Project_menu_items.IsEnabled = false;

            if (this.FlowController.EditController != null)
                this.FlowController.EditController.ClearDataGridColumns();

            //this will clear the current image filter
            if (CurrentFlowProject != null && CurrentFlowProject.SubjectList != null)
                CurrentFlowProject.SubjectList.ClearFilter();

            MergeManager mergeManager = new MergeManager();

            mergeManager.SourceDirectory = sourceDirectory;
            mergeManager.RootProject = rootProject;

            mergeManager.FlowMasterDataContext = this.FlowMasterDataContext;

            mergeManager.ProgressInfo = new NotificationProgressInfo("Merge project", "Initializing...");
            mergeManager.ProgressInfo.NotificationProgress = ObjectFactory.GetInstance<IShowsNotificationProgress>();

            mergeManager.MergeCompleted += delegate
            {
                try
                {

                    this.CurrentFlowProject = null;
                    NotifyPropertyChanged("MergeComplete");
                    logger.Info("Done merging uncompressed project folder: sourceDirectory='{0}' openAfterImport={1}", sourceDirectory, openAfterImport.ToString());
                    if (openAfterImport)
                    {
                        //this.CurrentFlowProject = rootProject.Connect(this.FlowMasterDataContext);
                        //rootProject.FlowProjectDataContext.InitSubjectList();
                        ////Very Very Very Important
                        //NotifyPropertyChanged("CurrentFlowProject");
                        Load(rootProject, true, true);
                        CurrentFlowProject.FlowProjectDataContext.SubjectDataLoaded += delegate
                        {
                            flowController.Capture();
                        };
                    }
                    else
                    {

                        if (this.FlowMasterDataContext.PreferenceManager.Application.IsFlowServer)
                            flowController.FlowServerPanel();
                        else
                            flowController.Project();
                    }

                    //this._flowController.ApplicationPanel.IsEnabled = true;
                    if (!FlowServerIsOn)
                    {
                        this._flowController.ApplicationPanel.MainMenuItems.IsEnabled = true;
                        this._flowController.ApplicationPanel.PreferencesMenuItem.IsEnabled = true;
                        this._flowController.ProjectController.MainProjectPanel.Project_menu_items.IsEnabled = true;
                    }
                }
                catch (Exception e)
                {
                    //no real harm if this fails.
                }
                    
            };

            // Temporary thread override for troubleshooting
            try
            {
                mergeManager.Merge();
            }
            catch (Exception e)
            {
                Thread.Sleep(8000);
                try
                {
                    mergeManager.Merge();
                }
                catch
                {
                    mergeManager.ProgressInfo.Complete("Database might be corrupt");
                    this._flowController.ApplicationPanel.IsEnabled = true;
                    throw (e);
                }
            }

            //FlowBackgroundWorkerManager.RunWorker(
            //    delegate
            //    {
            //        mergeManager.Merge();
            //    }
            //);
        }


		/// <summary>
		/// Exports a Flow project (database and image files) to an archive file (.paf);
		/// option to detach removes references to project from FlowMaster
		/// </summary>
		/// <param name="flowProject"></param>
		/// <param name="destinationPath"></param>
		/// <param name="withDetach"></param>
        //public void ExportProject(FlowProject flowProject, string destinationPath, bool withDetach)
        //{
        //    string destinationFilePath = Path.Combine(destinationPath, flowProject.FullProjectName + ".paf");

        //    FastZipEvents events = new FastZipEvents();
        //    events.Progress = new ICSharpCode.SharpZipLib.Core.ProgressHandler(ExportProject_OnZipProgress);
        //    FastZip zip = new FastZip(events);
        //    zip.CreateZip(destinationFilePath, flowProject.DirPath, true, null);

        //    ZipUtility.CreateArchive(destinationFilePath, flowProject.DirPath, true);

        //    if (withDetach)
        //    {
        //        Directory.Delete(flowProject.DirPath, true);

        //        _flowMasterDataContext.DetachProject(flowProject);
        //    }
        //}

        /// <summary>
        /// Uploads a Flow project archive (database and image files) to an ftp server;
        /// option to detach removes references to project from FlowMaster
        /// </summary>
        /// <param name="flowProject"></param>
        /// <param name="destinationPath"></param>
        /// <param name="withDetach"></param>

        public void UploadProject(FlowProject flowProject, bool withDetach, bool withReducedImages, string reducedSize)
        {
            ProjectUploader uploader = new ProjectUploader(this.FlowController, _flowMasterDataContext, flowProject, withDetach, withReducedImages, this._mainProjectPanel.Dispatcher, reducedSize);
            
            uploader.BeginAsyncUploadProject();

            
        }

		/// <summary>
		/// Loads flow project data based on supplied project id
		/// </summary>
		/// <param name="flowProjectID"></param>
		public void Load(int flowProjectID)
		{
			this.Load(
				_flowMasterDataContext.FlowProjects.Single(p => p.FlowProjectID == flowProjectID)
			);
		}

		/// <summary>
		/// Loads flow project data based on supplied project GUID
		/// </summary>
		/// <param name="projectGuid"></param>
		public void Load(Guid flowProjectGuid)
		{
			this.Load(
				_flowMasterDataContext.FlowProjects.Single(p => p.FlowProjectGuid == flowProjectGuid)
			);
		}

		/// <summary>
		/// Loads flow project data by project object reference; performs version synch
		/// </summary>
		/// <param name="projectGuid"></param>
		public void Load(FlowProject flowProject)
		{

           
			this.Load(flowProject, true);
           

            
		}

		/// <summary>
		/// Loads flow project data by project object reference; synch is optional
		/// </summary>
		/// <param name="flowProject"></param>
		/// <param name="performVersionSynch"></param>
        public void Load(FlowProject flowProject, bool performVersionSynch)
        {
            Load(flowProject, performVersionSynch, false);
        }

        public FlowBackgroundWorker networkProjectUpdater { get; set; }
        public void UpdateNetworkParent()
        {
            if (this.IsExporting || this.IsUploading)
                return;

            this.CurrentFlowProject.PrepareForExport();
            this.CurrentFlowProject.FlowProjectDataContext.SubmitChangesLocal();

            string timestamp = DateTime.Now.Ticks.ToString();
            string destFolder = Path.Combine(this.CurrentFlowProject.DirPath, "incoming");
            if (!Directory.Exists(destFolder))
                Directory.CreateDirectory(destFolder);
            foreach (string fileName in Directory.GetFiles(destFolder).Where(s=>s.EndsWith("_" + this.FlowController.FlowSessionID + ".sdf")))
                File.Delete(Path.Combine(destFolder, fileName));

            string destFile = Path.Combine(destFolder, timestamp + "_" + this.FlowController.FlowSessionID + ".sdf");
            File.Copy(this.CurrentFlowProject.LocalDBPath, destFile,true);
            new System.Security.Permissions.FileIOPermission(System.Security.Permissions.FileIOPermissionAccess.Read, new string[] { destFile }).Demand();
        }

        //only used by flow server
        public void PrepareServerProject(FlowProject flowProject)
        {
            if (this.CurrentFlowProject != null)
            {
                if (this.FlowMasterDataContext.PreferenceManager.Application.IsFlowServer)
                    CurrentFlowProject.PrepareForExport();

                this.CurrentFlowProject.initingSubjects = false;
            }

            if (NewFlowProject != null)
                NewFlowProject = null;

            flowProject.FlowMasterDataContext = this.FlowMasterDataContext;

            DbVersionManager.SynchFlowProject(flowProject);


            this.CurrentFlowProject = flowProject.Connect(_flowMasterDataContext);

            flowProject.PrepareForExport();
            flowProject.FlowProjectDataContext.SubmitChanges();
            flowProject.Disconnect();
            this.CurrentFlowProject = null;
        }

		public void Load(FlowProject flowProject, bool performVersionSynch, bool allowReload)
		{
            if (flowProject == null)
            {
                CurrentFlowProject = null;
                return;
            }

            this.FlowController.StartUploader(); //will only start if its not already started

            if (this.CurrentFlowProject != null)
            {
                if(this.FlowMasterDataContext.PreferenceManager.Application.IsFlowServer)
                    CurrentFlowProject.PrepareForExport();

                this.CurrentFlowProject.initingSubjects = false;
                if (CurrentFlowProject.IsNetworkProject == true && CurrentFlowProject.FlowProjectDataContext.UpdateNetworkProject)
                {
                    CurrentFlowProject.FlowProjectDataContext.UpdateNetworkProject = false;
                    UpdateNetworkParent();
                }

            }

            if (NewFlowProject != null)
                NewFlowProject = null;


            

            //if the current project has not changed, and we are not reloading, just bail
            if (!allowReload && this.CurrentFlowProject == flowProject)
            {
                return;
            }

            
            flowProject.FlowMasterDataContext = this.FlowMasterDataContext;
            
            //if this is a network project, lets copy the sdf locally
            if (flowProject.IsNetworkProject == true)
            {
                if (!File.Exists(flowProject.DatabasePath))
                {
                    FlowMessageBox msg = new FlowMessageBox("Network Project", "Failed to open network project, you may not be connected to the network");
                    msg.CancelButtonVisible = false;
                    msg.ShowDialog();
                    this.CurrentFlowProject = null;
                    return;
                }
                string incomingFolder = Path.Combine(flowProject.DirPath, "incoming");
               bool openlocal = false;

               //if the file exists called latestfromserver.txt, then dont even look in the incoming folder, always get the latest from server
                //this is a setting that is set with a file in the Common App Data Dir
               if (File.Exists(flowProject.LocalDBPath) && !File.Exists(Path.Combine(FlowContext.FlowCommonAppDataDirPath, "latestfromserver.txt")))
               {
                   if (Directory.Exists(incomingFolder) && Directory.GetFiles(incomingFolder).Count() > 0)
                   {
                       FlowMessageBox msg = new FlowMessageBox("Open Network Project", "The server is currently processing this project. If you open this project it may not contain the latest changes.");
                       if (msg.ShowDialog() == true)
                       {
                           openlocal = true;
                       }
                       else
                           return;
                   }
               }

                if (!openlocal)
                {
                    if (File.Exists(flowProject.DatabasePath) && File.Exists(flowProject.LocalDBPath) &&
                        (flowProject.DatabasePath != flowProject.LocalDBPath))
                    {
                        File.Delete(flowProject.LocalDBPath);

                    }

                    int trys = 0;
                    while (trys < 88)
                    {
                        try
                        {
                            FileCopy(flowProject.DatabasePath, flowProject.LocalDBPath, true);
                            trys = 100;
                        }
                        catch (Exception ex)
                        {
                            trys++;
                            if (trys > 10)
                                throw ex;
                            Thread.Sleep(1000);
                        }
                    }


                    new System.Security.Permissions.FileIOPermission(System.Security.Permissions.FileIOPermissionAccess.Read, new string[] { flowProject.DatabasePath }).Demand();
                }



                //this.DeleteProject(flowProject, "", false);
                //this.FlowMasterDataContext.FlowProjectList.Remove(flowProject);
                //flowProject = this.AttachNetworkProject(new DirectoryInfo(flowProject.DirPath));
                  string connectionString2 = "Data Source=" + flowProject.LocalDBPath + ";Max Database Size=1024;";
                 SqlCeEngine eng = new SqlCeEngine(connectionString2);
                    this.BackupSDF35(eng, flowProject.LocalDBPath);
                    eng.Dispose();

                    string conn = "Data Source=" + FlowContext.FlowProjectTemplateFilePath + ";Max Database Size=1024;";
                 SqlCeEngine eng2 = new SqlCeEngine(conn);
                 this.BackupSDF35(eng2, FlowContext.FlowProjectTemplateFilePath);
                 eng2.Dispose();
                

                this.FlowMasterDataContext.ApplyControlTables(flowProject.LocalDBPath, true, flowProject);
                


                Guid currentFlowGuid = flowProject.FlowProjectGuid;
                if (networkProjectUpdater != null)
                {
                    networkProjectUpdater.CancelAsync();
                    networkProjectUpdater = null;
                }
                if (networkProjectUpdater == null)
                {
                    networkProjectUpdater = FlowBackgroundWorkerManager.RunWorker(delegate
                    {
                        while (flowProject.IsNetworkProject == true)
                        {
                            Thread.Sleep((15 * 60) * 1000); //15 minutes
                            if (flowProject.FlowProjectDataContext.UpdateNetworkProject)
                            {
                                flowProject.FlowProjectDataContext.UpdateNetworkProject = false;
                                UpdateNetworkParent();
                            }
                            if (CurrentFlowProject.FlowProjectGuid != flowProject.FlowProjectGuid)
                                break;
                        }
                    });
                    networkProjectUpdater.WorkerSupportsCancellation = true;
                }
            }

            FlowMasterDataContext.PreferenceManager.Edit.ImageFilterIndex = 0;
            //FlowMasterDataContext.PreferenceManager.Edit.ImageFilterBeginDate = null;
            //FlowMasterDataContext.PreferenceManager.Edit.ImageFilterEndDate = null;

            this._flowController.ApplicationPanel.IsEnabled = false;

            //Force Ticket Code to be shown in capture subject data
            //this.CurrentFlowProject.FlowProjectDataContext.ProjectSubjectFields.First(psf => psf.ProjectSubjectFieldDisplayName == "Ticket Code").IsProminentField = true;




            //this._flowController.ResetControllers();
            if(this.FlowController.EditController != null)
                this.FlowController.EditController.ClearDataGridColumns();
            //if (this.CurrentFlowProject != null)
            //{
            //    this.CurrentFlowProject.FlowProjectDataContext.Dispose();
            //}

            if (flowProject.FlowMasterDataContext == null)
                flowProject.FlowMasterDataContext = this.FlowMasterDataContext;

			// Check for schema updates and implement any updates, if requested
			if (performVersionSynch)
				DbVersionManager.SynchFlowProject(flowProject);

           
            this.CurrentFlowProject = flowProject.Connect(_flowMasterDataContext);

           

            this.CurrentFlowProject.FlowProjectDataContext.InitOrderFilter();

            if (this.CurrentFlowProject.IsGreenScreen == null) { this.CurrentFlowProject.IsGreenScreen = false; }

            this.CurrentFlowProject.UpdateSelectedGroupImageFlags();

            //add TicketCode and Package Summary if needed
            //this.CurrentFlowProject.FlowProjectDataContext.addMoreColumnsToSubjectFields();

            //int estSecs = (this.CurrentFlowProject.FlowProjectDataContext.Subjects.Count() / 22);
            IShowsNotificationProgress progressNotification = ObjectFactory.GetInstance<IShowsNotificationProgress>();
            NotificationProgressInfo info = new NotificationProgressInfo("Loading Project","Loading...");
            info.NotificationProgress = progressNotification;
            this.CurrentFlowProject.LoadingProgressInfo = info;
            //if (estSecs > 20)
            //{
            //    FlowBackgroundWorker wrkerA = FlowBackgroundWorkerManager.RunWorker(delegate
            //   {
            //       while (info != null && !info.IsComplete && info.Title == "Loading Project")
            //       {
            //           info++;
            //           Thread.Sleep(1000);

            //       }
            //   });
            //}

            //info.Update("Preparing Project...");

            this.CurrentFlowProject.MainDispatcher = Dispatcher.CurrentDispatcher;

            //Flow.Lib.AsyncWorker.BackgroundWorker worker = new Flow.Lib.AsyncWorker.BackgroundWorker();
            //worker.DoWork = (Action)delegate
            //{

            if (this.FlowController.CaptureController != null && this.FlowController.CaptureController.LayoutPreviewController != null)
            {
                if (this.CurrentFlowProject.GetEventTriggerSetting(3))
                {
                    //autoprint
                    this.FlowController.CaptureController.LayoutPreviewController.TurnOnAutoPrint();
                }
                else
                {
                    this.FlowController.CaptureController.LayoutPreviewController.TurnOffLayoutPreview();
                    this.FlowController.CaptureController.LayoutPreviewController.TurnOffAutoPrint();

                }
            }
          

            FlowBackgroundWorker wrker = FlowBackgroundWorkerManager.RunWorker(delegate
            {
                //int allsubCount = this.CurrentFlowProject.FlowProjectDataContext.ExecuteQuery<int>("select count(*) from Subject","").First();
                //int ticketIDCount = this.CurrentFlowProject.FlowProjectDataContext.ExecuteQuery<int>("select count(*) from Subject where TicketCode is not null","").First();
                //if (allsubCount != ticketIDCount)
                //    this.CurrentFlowProject.FlowProjectDataContext.NeedPackageSummaryAndTicketCodeUpdated = true;

                //object otherList = this.CurrentFlowProject.FlowProjectDataContext.Subjects;
                if (this.CurrentFlowProject == null)
                    return;
                this.CurrentFlowProject.FlowProjectDataContext.InitSubjectList();
                this.CurrentFlowProject.FlowProjectDataContext.SubmitChangesLocal();
                if (CurrentFlowProject != null)
                {
                    string tempLayoutsDir = Path.Combine(CurrentFlowProject.DirPath, "Layouts");
                    if (Directory.Exists(tempLayoutsDir) && Directory.GetFiles(tempLayoutsDir).Count() > 0)
                    {
                        this.CustomSettings.DefaultCaptureLayout = Directory.GetFiles(tempLayoutsDir)[0];
                    }
                }
                

                this.CurrentFlowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
             {
                 this.CurrentFlowProject.IsReportShowFooter = this.FlowMasterDataContext.PreferenceManager.Application.ReportShowFooter;

                 //is their a flowmaster gallery for this project?
                if(this.FlowMasterDataContext.FlowProjectGalleries.Any(g=>g.FlowProjectGuid == flowProject.FlowProjectGuid) && !flowProject.FlowProjectDataContext.ProjectGalleries.Any())
                {
                    FlowProjectGallery oldGal = this.FlowMasterDataContext.FlowProjectGalleries.First(g => g.FlowProjectGuid == flowProject.FlowProjectGuid);
                    ProjectGallery gal = new ProjectGallery();
                    gal.FlowProjectGuid = oldGal.FlowProjectGuid;
                    gal.EventID = oldGal.EventID;
                    gal.GalleryURL = oldGal.GalleryURL;
                    gal.DirectShip = oldGal.DirectShip;
                    gal.GalleryName = oldGal.GalleryName;
                    gal.WelcomeImage = oldGal.WelcomeImage;
                    gal.WelcomeText = oldGal.WelcomeText;
                    gal.GalleryPassword = oldGal.GalleryPassword;
                    gal.OneImagePerPackage = oldGal.OneImagePerPackage;
                    gal.ShippingCost = oldGal.ShippingCost;
                    gal.HandlingCost = oldGal.HandlingCost;
                    gal.TaxRate = oldGal.TaxRate;
                    gal.TaxShipping = oldGal.TaxShipping;
                    gal.ShowBWSepia = oldGal.ShowBWSepia;
                    gal.DisableFullControlCropping = oldGal.DisableFullControlCropping;
                    gal.SwitchShippingDate = oldGal.SwitchShippingDate;
                    gal.SwitchShipping = oldGal.SwitchShipping;
                    gal.PickupText = oldGal.PickupText;
                    gal.WatermarkText = oldGal.WatermarkText;
                    gal.WatermarkLocation = oldGal.WatermarkLocation;
                    gal.PickupShip = oldGal.PickupShip;
                    gal.CustomerChoiceShip = oldGal.CustomerChoiceShip;
                    gal.PricesheetID = oldGal.PricesheetID;
                    gal.GalleryCode = oldGal.GalleryCode;
                    gal.GalleryBackgroundList = oldGal.GalleryBackgroundList;
                    gal.YearbookSelection = oldGal.YearbookSelection;
                    gal.ApplyCrops = oldGal.ApplyCrops;
                    gal.YearbookPoses = oldGal.YearbookPoses;
                    gal.EventDate = oldGal.EventDate;
                    gal.ExpirationDate = oldGal.ExpirationDate;
                    gal.Keyword = oldGal.Keyword;
                    gal.MinOrderAmount = oldGal.MinOrderAmount;
                    gal.ShippingLabel = oldGal.ShippingLabel;
                    gal.HandlingLabel = oldGal.HandlingLabel;
                    gal.HandlingRateType = oldGal.HandlingRateType;
                    gal.TaxLabel = oldGal.TaxLabel;
                    gal.TaxAllOrders = oldGal.TaxAllOrders;
                    gal.OrderThankyouMessage = oldGal.OrderThankyouMessage;
                    gal.DisableCropping = oldGal.DisableCropping;
                    gal.DefaultShowImageName = oldGal.DefaultShowImageName;
                    gal.Theme = oldGal.Theme;
                    gal.Wallpaper = oldGal.Wallpaper;
                    gal.DoNotUpdateExistingPricesheet = oldGal.DoNotUpdateExistingPricesheet;

                    flowProject.FlowProjectDataContext.ProjectGalleries.InsertOnSubmit(gal);
                    this.FlowMasterDataContext.FlowProjectGalleries.DeleteOnSubmit(oldGal);
                }


                 //add a yearbook pose if one does not already exist
                //if (1==2 && CustomSettings.AlwaysYearbookPose == true)
                //{
                //    if (!this.CurrentFlowProject.FlowProjectDataContext.ProjectImageTypes.Any(t => t.ProjectImageTypeDesc.ToLower().Contains("yearbook pose")))
                //    {
                //        ProjectImageType newItem = new ProjectImageType();
                //        newItem.ProjectImageTypeDesc = "Yearbook Pose";
                //        newItem.IsExclusive = true;

                //        this.CurrentFlowProject.FlowProjectDataContext.ProjectImageTypes.InsertOnSubmit(newItem);

                //    }
                //    if (this.CurrentFlowProject.FlowProjectDataContext.ProjectImageTypes.Any(t => t.ProjectImageTypeDesc.ToLower().Contains("yearbook pose") && t.IsExclusive == false))
                //    {
                //        ProjectImageType newItem = this.CurrentFlowProject.FlowProjectDataContext.ProjectImageTypes.First(t => t.ProjectImageTypeDesc.ToLower().Contains("yearbook pose") && t.IsExclusive == false);
                //        newItem.IsExclusive = true;
                //    }
                //    if (!this.CurrentFlowProject.FlowProjectDataContext.ProjectImageTypes.Any(t => t.ProjectImageTypeDesc.ToLower().Contains("yearbook pose 2")))
                //    {
                //        ProjectImageType newItem = new ProjectImageType();
                //        newItem.ProjectImageTypeDesc = "Yearbook Pose 2";
                //        newItem.IsExclusive = true;

                //        this.CurrentFlowProject.FlowProjectDataContext.ProjectImageTypes.InsertOnSubmit(newItem);

                //    }
                //}

                 //for PUD, add map to the OrderProductOption table if its missing
                 if(CurrentFlowProject.isPUD)
                 {
                     foreach (OrderProductOption opo in this.CurrentFlowProject.FlowProjectDataContext.OrderProductOptions)
                     {
                         if (opo.ImageQuixOptionPk != null && opo.Map == null)
                         {
                             ImageQuixOptionGroup iqOGroup = this.FlowMasterDataContext.ImageQuixOptionGroups.FirstOrDefault(iqog => iqog.PrimaryKey == opo.ImageQuixOptionGroupPk);
                            if(iqOGroup != null)
                            {
                                ImageQuixOption iqoption = iqOGroup.OptionList.FirstOrDefault(iqo => iqo.PrimaryKey == opo.ImageQuixOptionPk);
                                if (iqoption != null)
                                {
                                    opo.Map = iqoption.ResourceURL;
                                }
                            }
                         }
                         if (opo.ImageQuixOptionGroupPk != null && opo.OptionGroupLabel == null)
                         {
                             ImageQuixOptionGroup iqOGroup = this.FlowMasterDataContext.ImageQuixOptionGroups.FirstOrDefault(iqog => iqog.PrimaryKey == opo.ImageQuixOptionGroupPk);
                             if (iqOGroup != null)
                             {
                                 opo.OptionGroupLabel = iqOGroup.Label;
                             }
                         }
                     }
                     this.CurrentFlowProject.FlowProjectDataContext.SubmitChangesLocal();
                 }

                 //for PUD, if there are any OrderProductOptions that don't match the local catalog, update the references based on the map value
                 if (CurrentFlowProject.isPUD)
                 {
                     foreach (OrderProductOption opo in this.CurrentFlowProject.FlowProjectDataContext.OrderProductOptions)
                     {
                         if (opo.OptionGroupLabel != null && this.FlowMasterDataContext.ImageQuixOptionGroups.Count(i=>i.PrimaryKey == opo.ImageQuixOptionGroupPk) == 0)
                         {
                             ImageQuixOptionGroup iqOGroup = this.FlowMasterDataContext.ImageQuixOptionGroups.FirstOrDefault(iqog => iqog.Label == opo.OptionGroupLabel);
                             if (iqOGroup != null)
                             {
                                 opo.ImageQuixOptionGroupPk = iqOGroup.PrimaryKey;
                                 if (opo.Map != null && iqOGroup.OptionList.Count(j => j.PrimaryKey == opo.ImageQuixOptionPk) == 0)
                                 {
                                     ImageQuixOption iqoption = iqOGroup.OptionList.FirstOrDefault(iqo => iqo.ResourceURL == opo.Map);
                                     if (iqoption != null)
                                     {
                                         opo.ImageQuixOptionPk = iqoption.PrimaryKey;
                                     }
                                 }
                             }
                         }
                     }
                 }

                 //update all OrderPackages that might be missing the LabOrderID
                 if (this.CurrentFlowProject.FlowProjectDataContext.OrderHistories.Count() > 0)
                 {
                     foreach (OrderPackage op in this.CurrentFlowProject.FlowProjectDataContext.OrderPackages)
                     {
                         if (op.LabOrderID == null && op.OrderHistories.Count > 0)
                         {
                             op.LabOrderID = Convert.ToInt32(op.OrderHistories.Last().OrderID);
                             op.ModifyDate = DateTime.Now;
                             op.LabOrderDate = op.OrderHistories.Last().OrderSubmittedDate;
                         }
                     }
                     this.CurrentFlowProject.FlowProjectDataContext.OrderHistories.DeleteAllOnSubmit(this.CurrentFlowProject.FlowProjectDataContext.OrderHistories);
                     this.CurrentFlowProject.FlowProjectDataContext.SubmitChangesLocal();
                 }

                 //load missingImage place holder into memory
                 BitmapImage bi = new BitmapImage();
                 bi.BeginInit();
                 //bi.DecodePixelWidth = 25;
                 bi.CacheOption = BitmapCacheOption.OnLoad;
                 bi.UriSource = new Uri(@"pack://application:,,,/Images/missing.png", UriKind.RelativeOrAbsolute);
                 bi.EndInit();
                 this.CurrentFlowProject.FlowProjectDataContext.MissingImage = bi; 
                 ////////////////////////////

                 if (flowProject.SubjectList.Count() > 0)
                 {
                     int index = this.FlowMasterDataContext.PreferenceManager.Application.CurrentSubjectIndex;
                     if (index < flowProject.SubjectList.Count() && index > 0)
                         flowProject.SubjectList.CurrentItem = flowProject.SubjectList[index];
                 }
                 flowProject.SubjectList.CurrentChanged +=new EventHandler<EventArgs<Subject>>(SubjectList_CurrentChanged);
                 foreach (GroupImage gi in CurrentFlowProject.FlowProjectDataContext.GroupImageList)
                 {
                     gi.Initialize(CurrentFlowProject.FlowProjectDataContext);
                 }

                 if (this.FlowController.CaptureController != null)
                 {
                     this.FlowController.CaptureController.CaptureDockPanel.pnlGroupPhotos.DataContext = this.CurrentFlowProject.FlowProjectDataContext.GroupImageList;
                     this.FlowController.CaptureController.CaptureDockPanel.pnlGroupPhotos.FlowController = this.FlowController;
                     this.FlowController.CaptureController.CaptureDockPanel.CurrentImagePanel.ClearCurrentImage();
                 }

                 //if (this.FlowController.EditController == null)
                 //    this.FlowController.Edit();

                // this.FlowController.Capture();

                 this.CurrentFlowProject.SubjectList.RaiseSubjectDataLoaded();


                 UpdatedSavedSubjectCount();
                //if (this.CurrentFlowProject.SavedSubjectCount != this.CurrentFlowProject.FlowProjectDataContext.Subjects.Count())
                //{
                //    this.CurrentFlowProject.SavedSubjectCount = this.CurrentFlowProject.FlowProjectDataContext.Subjects.Count();
                //    _flowMasterDataContext.FlowProjects.First(fp => fp.FlowProjectGuid == this.CurrentFlowProject.FlowProjectGuid).SavedSubjectCount = this.CurrentFlowProject.SavedSubjectCount;
                //    _flowMasterDataContext.SubmitChanges();
                //}

                this.CurrentFlowProject.FlowProjectDataContext.SubjectImagesRemoved +=
                    new EventHandler<EventArgs<IEnumerable<SubjectImage>>>(FlowProjectDataContext_SubjectImagesRemoved);

 
                //flowProject.FlowProjectDataContext.SubjectDataLoaded += delegate
                //{
                    this.CurrentFlowProject.SavedSubjectCount = this.CurrentFlowProject.SubjectList.Count;
                    this.CurrentFlowProject.SubjectList.CollectionChanged += delegate
                    {
                        this.CurrentFlowProject.SavedSubjectCount = this.CurrentFlowProject.SubjectList.Count; 
                        _flowMasterDataContext.SubmitChanges();
                    };

                 
                    SubjectList_CurrentChanged();
                    //Very Very Very Important
                    NotifyPropertyChanged("CurrentFlowProject");

                    if (this.CurrentFlowProject.ProblemLoadingSubjectData)
                    {
                        FlowMessageBox msg = new FlowMessageBox("Warning", "There was a problem loading some Subject Data in this project.\n\nYou may have some subjects that are missing required field data.");
                        msg.CancelButtonVisible = false;
                        msg.ShowDialog();
                    }
                    this._flowController.ApplicationPanel.IsEnabled = true;

                    if (this.FlowMasterDataContext.PreferenceManager.Application.IsFlowServer || (this.FlowMasterDataContext.PreferenceManager.Application.NetworkProjectsDirectory != null && File.Exists(this.FlowMasterDataContext.PreferenceManager.Application.NetworkProjectsDirectory)))
                        this.CurrentFlowProject.PrepareForExport();

                    


                    //set the UseOption value for the order options
                    if (this.CurrentFlowProject.FlowCatalog != null)
                    {
                        try
                        {
                            foreach (FlowCatalogOption co in this.CurrentFlowProject.FlowCatalog.FlowCatalogOrderOptionList)
                            {
                                if (this.CurrentFlowProject.FlowProjectDataContext.OrderOrderOptions.Count(ooo => ooo.FlowCatalogOptionGuid == co.FlowCatalogOptionGuid) > 0)
                                    co.UseOption = true;
                                else
                                    co.UseOption = false;
                            }
                        }
                        catch (Exception ex)
                        {
                            //do nothing for now
                        }

                        if (this.CurrentFlowProject.FlowProjectDataContext.OrderShippingOptions.Count() == 0)
                        {
                            IEnumerable<FlowCatalogOption> defaults = this.CurrentFlowProject.FlowCatalog.FlowCatalogShippingOptionList.Where(so => so.ImageQuixCatalogOption.IsDefault == true);
                            if (defaults.Count() > 0)
                            {

                                FlowCatalogOption co = defaults.First();
                                OrderShippingOption oso = new OrderShippingOption();
                                oso.FlowCatalogOptionGuid = (Guid)co.FlowCatalogOptionGuid;
                                oso.ResourceURL = co.ImageQuixCatalogOption.ResourceURL;
                                this.CurrentFlowProject.FlowProjectDataContext.OrderShippingOptions.InsertOnSubmit(oso);
                                this.CurrentFlowProject.FlowProjectDataContext.SubmitChangesLocal();
                            }
                        }
                        if (this.CurrentFlowProject.FlowProjectDataContext.OrderPackagingOptions.Count() == 0)
                        {
                            IEnumerable<FlowCatalogOption> defaults = this.CurrentFlowProject.FlowCatalog.FlowCatalogPackagingOptionList.Where(so => so.ImageQuixCatalogOption.IsDefault == true);
                            if (defaults.Count() > 0)
                            {

                                FlowCatalogOption co = defaults.First();
                                OrderPackagingOption opo = new OrderPackagingOption();
                                opo.FlowCatalogOptionGuid = (Guid)co.FlowCatalogOptionGuid;
                                opo.ResourceURL = co.ImageQuixCatalogOption.ResourceURL;
                                this.CurrentFlowProject.FlowProjectDataContext.OrderPackagingOptions.InsertOnSubmit(opo);
                                this.CurrentFlowProject.FlowProjectDataContext.SubmitChangesLocal();
                            }
                        }
                    }

                    

                    CurrentFlowProject.UpdateProjectSubjectFieldListForSearch();
                //};

                    //info.Complete("Done");
             }));

                //lets set up a background worker to init all subjectData
                if (this.CurrentFlowProject != null && !this.CurrentFlowProject.ExportInProgress)
                    initializeSubjectData();

                
                if (1==2 && !this.CurrentFlowProject.ExportInProgress)
                {
                    this.CurrentFlowProject.initingSubjects = true;
                    IShowsNotificationProgress progressN = ObjectFactory.GetInstance<IShowsNotificationProgress>();
                    int subCount = this.CurrentFlowProject.SubjectList.Count;
                    NotificationProgressInfo pInfo = new NotificationProgressInfo("Initializing Subjects", "Initializing Subjects...", 0, subCount, 1);
                    pInfo.NotificationProgress = progressNotification;
                    pInfo.AllowCancel = true;
                    pInfo.Display(true);
                    pInfo.Update("Initializing Subject {0} of {1}", 0, subCount, 1, true);
                    //List<Subject> allSubjects = new List<Subject>();
                    //foreach (Subject sub in this.CurrentFlowProject.SubjectList)
                    //    allSubjects.Add(sub);

                    foreach (Subject s in this.CurrentFlowProject.SubjectList)
                    {
                        if (!this.CurrentFlowProject.initingSubjects)
                            break;
                        if (pInfo.IsCanceled)
                            break;

                        pInfo++;

                        try
                        {
                            this.CurrentFlowProject.MainDispatcher.Invoke(DispatcherPriority.Background, (Action)(() =>
                                                {
                                                    if (this.CurrentFlowProject.initingSubjects)
                                                    {
                                                        object inito = s.SubjectData;
                                                    }
                                                }));
                        }
                        catch (Exception)
                        {

                            this.CurrentFlowProject.initingSubjects = false;
                            pInfo.Complete("Done Initializing Subjects");

                        }

                    }
                    this.CurrentFlowProject.initingSubjects = false;
                    pInfo.Complete("Done Initializing Subjects");
                }
            });


		}

        private void FileCopy(string source, string dest, bool overwrite)
        {
            if (overwrite && File.Exists(dest))
                File.Delete(dest);
            FileStream inf = new FileStream(source, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            FileStream outf = new FileStream(dest, FileMode.Create);
            int a;
            while ((a = inf.ReadByte()) != -1)
            {
                outf.WriteByte((byte)a);
            }
            inf.Close();
            inf.Dispose();
            outf.Close();
            outf.Dispose();
        }

        public void initializeSubjectData()
        {
            this.CurrentFlowProject.initingSubjects = true;

                IShowsNotificationProgress progressN = ObjectFactory.GetInstance<IShowsNotificationProgress>();
                int subCount = this.CurrentFlowProject.SubjectList.Count;
                NotificationProgressInfo pInfo = new NotificationProgressInfo("Preparing Subjects", "Please Wait While we Initialize Subjects for Search...", 0, subCount, 1);
                pInfo.NotificationProgress = progressN;
                pInfo.AllowCancel = true;
                pInfo.Display(true);
                pInfo.Update("Initializing Subject {0} of {1}", 0, subCount, 1, true);
                
                //just need to initialize the ProjectSubjectFieldList fro the the project
                this.CurrentFlowProject.MainDispatcher.Invoke(DispatcherPriority.Background, (Action)(() =>
                {
                    Object initiObj = this.CurrentFlowProject.ProjectSubjectFieldList;
                }));
                int loopCount = 0;
                while (loopCount < subCount)
                {
                    if (this.CurrentFlowProject == null || !this.CurrentFlowProject.initingSubjects)
                        break;
                    if (pInfo.IsCanceled)
                        break;
                    Subject s = this.CurrentFlowProject.SubjectList[loopCount];
                    pInfo++;
                    loopCount++;
                    try
                    {
                        this.CurrentFlowProject.MainDispatcher.Invoke(DispatcherPriority.Background, (Action)(() =>
                                            {
                                                if (this.CurrentFlowProject != null && this.CurrentFlowProject.initingSubjects)
                                                {
                                                    object inito = s.SubjectData;
                                                }
                                            }));
                    }
                    catch (Exception)
                    {

                        this.CurrentFlowProject.initingSubjects = false;
                        pInfo.Complete("Done Initializing Subjects");

                    }

                }
                if(this.CurrentFlowProject != null)
                    this.CurrentFlowProject.initingSubjects = false;
                pInfo.Complete("Done Initializing Subjects");

        }
        public void UpdatedSavedSubjectCount()
        {
            UpdatedSavedSubjectCount(CurrentFlowProject);
        }
        public void UpdatedSavedSubjectCount(FlowProject proj)
        {
            //if (this.CurrentFlowProject.SavedSubjectCount != this.CurrentFlowProject.FlowProjectDataContext.Subjects.Count())
            //{
            //    this.CurrentFlowProject.SavedSubjectCount = this.CurrentFlowProject.FlowProjectDataContext.Subjects.Count();
            //    _flowMasterDataContext.FlowProjects.First(fp => fp.FlowProjectGuid == this.CurrentFlowProject.FlowProjectGuid).SavedSubjectCount = this.CurrentFlowProject.SavedSubjectCount;
            //    _flowMasterDataContext.SubmitChanges();
            //}

            proj.FlowMasterDataContext = this.FlowMasterDataContext;


            if (this.CurrentFlowProject != null && this.CurrentFlowProject.FlowProjectDataContext != null &&
                this.CurrentFlowProject.FlowProjectDataContext.Connection != null && 
                this.CurrentFlowProject.FlowProjectDataContext.Connection.State == System.Data.ConnectionState.Open)
            {
                SqlCeConnection cn = this.CurrentFlowProject.FlowProjectDataContext.Connection as SqlCeConnection;
                SqlCeCommand command = new SqlCeCommand("select count(*) from Subject", cn);

                SqlCeResultSet rs = command.ExecuteResultSet(new ResultSetOptions());
                while (rs.Read())
                {
                    int count = rs.GetInt32(0);
                    if (_flowMasterDataContext.FlowProjects.Any(fp => fp.FlowProjectGuid == proj.FlowProjectGuid))
                    {
                        _flowMasterDataContext.FlowProjects.First(fp => fp.FlowProjectGuid == proj.FlowProjectGuid).SavedSubjectCount = count;
                        _flowMasterDataContext.SubmitChanges();
                    }
                }
            }
            else
            {
                string connectString = proj.ConnectionString;
                SqlCeConnection cn = new SqlCeConnection(connectString);
                cn.Open();

                SqlCeCommand command = new SqlCeCommand("select count(*) from Subject", cn);

                SqlCeResultSet rs = command.ExecuteResultSet(new ResultSetOptions());
                while (rs.Read())
                {
                    int count = rs.GetInt32(0);
                    if (_flowMasterDataContext.FlowProjects.Any(fp => fp.FlowProjectGuid == proj.FlowProjectGuid))
                    {
                        _flowMasterDataContext.FlowProjects.First(fp => fp.FlowProjectGuid == proj.FlowProjectGuid).SavedSubjectCount = count;
                        _flowMasterDataContext.SubmitChanges();
                    }
                }

                cn.Close();
                cn.Dispose();
            }
            
           
            
        }
        void SubjectList_CurrentChanged(object sender, EventArgs e) { SubjectList_CurrentChanged(); }

        void SubjectList_CurrentChanged()
        {
            if (this.FlowMasterDataContext.PreferenceManager.Application.CurrentSubjectIndex != null && this.FlowMasterDataContext.PreferenceManager.Application.CurrentSubjectIndex >= 0)
            {
                if (this.CurrentFlowProject.SubjectList != null && this.CurrentFlowProject.SubjectList.HasItems && this.CurrentFlowProject.SubjectList.Count > 0 && this.FlowMasterDataContext.PreferenceManager.Application.CurrentSubjectIndex < this.CurrentFlowProject.SubjectList.Count)
                {
                    Subject sub = this.CurrentFlowProject.SubjectList[this.FlowMasterDataContext.PreferenceManager.Application.CurrentSubjectIndex];
                    foreach (SubjectImage img in sub.SubjectImages)
                    {
                        img.ClearImage();
                    }
                }
            }
            if (this.FlowMasterDataContext.PreferenceManager.Application.CurrentSubjectIndex != this.CurrentFlowProject.SubjectList.CurrentPosition)
            {
                this.FlowMasterDataContext.PreferenceManager.Application.CurrentSubjectIndex = this.CurrentFlowProject.SubjectList.CurrentPosition;
                this.FlowMasterDataContext.SavePreferences();
            }
        }

        public void ReLoadCurrentProject()
        {
            Load(this.CurrentFlowProject, false, true);
            logger.Info("Reloaded current project");
        }

		public void GenerateProjectTemplate()
		{
			_currentProjectTemplate.DateCreated = DateTime.Now;
            _currentProjectTemplate.DateModified = DateTime.Now;
			_flowMasterDataContext.ProjectTemplateList.Add(_currentProjectTemplate);

            logger.Info("Generated new project template: id={0} desc='{1}'",
                _currentProjectTemplate.ProjectTemplateID, _currentProjectTemplate.ProjectTemplateDesc);
		}


		internal void SaveProjectTemplate()
		{
			if (_currentProjectTemplate.IsNew)
				GenerateProjectTemplate();
            _currentProjectTemplate.DateModified = DateTime.Now;
//			_currentProjectTemplate.SynchEventTriggers();

			_flowMasterDataContext.SubmitChanges();

            logger.Info("Saved project template: id={0} desc='{1}'",
                _currentProjectTemplate.ProjectTemplateID, _currentProjectTemplate.ProjectTemplateDesc);
		}

		/// <summary>
		/// Return images assigned to Subject
		/// </summary>
		private void FlowProjectDataContext_SubjectImagesRemoved(object sender, EventArgs<IEnumerable<SubjectImage>> e)
		{
			foreach (SubjectImage image in e.Data)
			{
				MoveFile(image.ImagePath, Path.Combine(FlowContext.FlowHotFolderDirPath, image.ImageFileName));
			}
		}

        public void MoveFile(string sourceFilePath, string destFilePath)
        {
            bool keepTrying = true;
            while (keepTrying)
            {
                try
                {
                    IOUtil.MoveFile(sourceFilePath, destFilePath);
                    keepTrying = false;
                }
                catch (Exception ex)
                {
                    FlowMessageBox msg = new FlowMessageBox("Problem Moving File", "Flow could not remove the old file after it was copied to the new location.\n\n" + sourceFilePath + "\n\nWould you like to try and remove the old file again?");
                    msg.buttonOkay.Content = "Try Again";
                    if (msg.ShowDialog() == true)
                        keepTrying = true;
                    else
                        keepTrying = false;
                }

            }
        }



        internal void DeleteProject(FlowProject flowProject)
        {
            //this one is called if they actually clicked the Delete project button (not archive button)
            DeleteProject(flowProject, null, true, false);
        }

        internal void DeleteProject(FlowProject flowProject, string message, bool showMessage, bool abortDelete)
        {

            if (abortDelete)
            {
                FlowMessageBox msg = new FlowMessageBox("Deleting Project", "There was an Error, the project can't be deleted at this time");
                msg.CancelButtonVisible = false;
                msg.ShowDialog();
                return;
            }

            if (message == null)
                message = "Are you sure you want to delete this project from Flow?\n\nTHIS CANNOT BE UNDONE!";

            bool result = true;
            if (showMessage)
            {
                FlowMessageBox msg = new FlowMessageBox("Deleting Project", message);
                msg.setButtonText("Delete", "Cancel");
                msg.setConfirmationText("I am positive I want to remove this project from my computer");
                result = (bool)msg.ShowDialog();
            }

            if (result)
            {
                logger.Info("Deleting project: name='{0}' guid={1}", flowProject.FlowProjectName, flowProject.FlowProjectGuid);

                this.FlowMasterDataContext.DetachProject(flowProject);
                this.FlowMasterDataContext.SubmitChanges();
                try
                {
                    if (flowProject.IsNetworkProject == true)
                        File.Delete(flowProject.LocalDBPath);
                    else
                        Directory.Delete(flowProject.DirPath, true);
                }
                catch (Exception e)
                {
                    //if we cant delete, oh well

                    logger.WarnException("Problem deleting project, ignoring", e);
                }
            }
            if (flowProject == this.CurrentFlowProject)
            {
                this.CurrentFlowProject = null;
                this._mainProjectPanel.FlowController.ApplicationPanel.uxCapture.IsEnabled = false;
                this._mainProjectPanel.FlowController.ApplicationPanel.uxEdit.IsEnabled = false;
                this._mainProjectPanel.FlowController.ApplicationPanel.uxReports.IsEnabled = false;
                this.NotifyPropertyChanged("IsProjectLoaded");

                
            }

            this.FlowMasterDataContext.FilteredFlowProjectList = null;
            this.FlowMasterDataContext.RefreshFilteredProjectList();
            this.FlowMasterDataContext.RefreshOrgList();
            this.FlowMasterDataContext.FilteredFlowProjectList = this.FlowMasterDataContext.FlowProjects.ToList(); ;
            this._mainProjectPanel.FlowController.ApplicationPanel.UpdateLayout();
            this.FlowController.ApplicationPanel.ShowMainProjectPanel();
            
            
        }


        internal void ArchiveDeleteProject(FlowProject flowProject)
        {
            ArchiveDeleteProject(flowProject, false);
        }
        internal void ArchiveDeleteProject(FlowProject flowProject, bool batchmode)
        {
            

            string archiveFolder = this.FlowMasterDataContext.PreferenceManager.Application.ProjectArchiveDirectory;
            if (archiveFolder == null)
            {
                FlowMessageBox msg = new FlowMessageBox("Archive Folder not Specified", "Before archiving a project,\nplease specify an archive folder in Preferences -> Application");
                msg.CancelButtonVisible = false;
                msg.ShowDialog();
                return;
            }
            if (!Directory.Exists(archiveFolder))
                Directory.CreateDirectory(archiveFolder);

            if (flowProject == this.CurrentFlowProject)
            {
                // Prepare the project for export (create control records)
                flowProject.PrepareForExport();
                this.ExportAfterPrepare(flowProject, archiveFolder, batchmode);
            }
            else
            {
                // Load the project data, and when loaded...
                flowProject.Connect(this.FlowMasterDataContext);
                flowProject.FlowProjectDataContext.InitSubjectList();
                flowProject.FlowProjectDataContext.SubjectDataLoaded += delegate
                {
                    // Prepare the project for export (create control records)
                    flowProject.PrepareForExport();
                    this.ExportAfterPrepare(flowProject, archiveFolder, batchmode);
                };
            }
           
        }

     
        private void ExportAfterPrepare(FlowProject flowProject, string archiveFolder, bool batchmode)
        {
            if (this.IsExporting)
            {
                return;
            }

            this.IsExporting = true;
            ProjectExporter exporter = new ProjectExporter(flowProject,
                   archiveFolder, FlowMasterDataContext);

            //have it call DeleteProject when done exporting
            exporter.BatchMode = batchmode;
            exporter.ExportComplete += new ProjectExporter.ExportCompleteDelegate(DeleteProject);
            exporter.ExportComplete += delegate { this.IsExporting = false; };
            exporter.BeginAsyncExportProject();

        }

        public bool IsUploading { get; set; }

        internal void UpdateAllProjectsSearchResults()
        {
            this.NotifyPropertyChanged("AllProjectsSearchResults");
        }

        public void GotoTicketID(string ticketId)
        {
            IEnumerable<Subject> sub = this.FlowController.ProjectController.CurrentFlowProject.SubjectList.Where(s => s.TicketCode == ticketId);
            if(sub != null && sub.Count() > 0)
                this.FlowController.ProjectController.CurrentFlowProject.SubjectList.CurrentItem = sub.First();
        }


        public bool IsExporting { get; set; }



        private string GetJson(string uri, string custId, string custPw, string postData)
        {
            logger.Info("a " + uri);
            HttpWebRequest rq = (HttpWebRequest)WebRequest.Create(uri);
            rq.Credentials = new NetworkCredential(custId, custPw);
            rq.Headers.Add("x-iq-token", "A15M876TJ8");
            rq.Headers.Add("x-iq-user", custId);
            rq.Timeout = (15000);//try for 15 seconds
            rq.Method = "GET";

            HttpWebResponse rs = null;
            int tryCount = 0;
            while (tryCount < 5)
            {
                try
                {
                    rs = (HttpWebResponse)rq.GetResponse();

                    StreamReader reader = new StreamReader(rs.GetResponseStream());
                    string jsonText = reader.ReadToEnd();

                    reader.Close();
                    rs.Close();
                    return jsonText;

                }
                catch (WebException e)
                {

                    tryCount++;

                    //if (tryCount == 5)
                    //    throw e;
                }
            }


            return null;
        }


        public void initOnlineGallerySettings()
        {
            if (CurrentFlowProject == null)
                return;
            //if (this.ProjectController.CurrentFlowProjectGallery == null)
            //    return;

           
            //this.txtGalleryName.Text = this.ProjectController.CurrentFlowProject.FlowProjectName;
            if (this.CurrentFlowProject != null && this.CurrentFlowProjectGallery == null)
            {
                //get default values:
                string mainUri = this.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixLoginURL;
                string custId = this.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixCustomerID;
                string custPw = this.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixCustomerPassword;


                ProjectGallery fpg = new ProjectGallery(this.CurrentFlowProject.FlowProjectGuid, this.CurrentFlowProject.FlowProjectName);
                fpg.DirectShip = false;
                fpg.CustomerChoiceShip = false;
                fpg.PickupShip = true;
                fpg.OneImagePerPackage = true;
                fpg.GalleryPassword = "";
                fpg.WelcomeImage = FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.WelcomeImagePath;
                fpg.WelcomeText = "Welcome to Online Ordering";
                fpg.SwitchShipping = false;
                fpg.SwitchShippingDate = DateTime.Now.AddDays(45);
                fpg.ShowBWSepia = false;
                fpg.DisableFullControlCropping = true;
                fpg.ShippingCost = (decimal)6.50;
                fpg.HandlingCost = (decimal)3.75;
                fpg.TaxRate = (decimal)5.25;
                fpg.TaxShipping = false;
                fpg.WatermarkText = "";
                fpg.WatermarkLocation = "middle-center";
                fpg.PickupText = "School";
                fpg.EventDate = DateTime.Today;
                fpg.ExpirationDate = DateTime.Today.AddMonths(3);
                fpg.Keyword = "key";
                fpg.MinOrderAmount = (decimal)1.00;
                fpg.ShippingLabel = "Shipping";
                fpg.HandlingLabel = "Handling";
                fpg.HandlingRateType = "percent";
                fpg.TaxLabel = "Tax";
                fpg.TaxAllOrders = false;
                fpg.OrderThankyouMessage = "Thank You!";
                fpg.DisableCropping = true;
                fpg.DefaultShowImageName = true;
                fpg.Theme = "black";
                fpg.Wallpaper = "https://imagequix.s3.amazonaws.com/vando/wallpapers/school01.jpg";
                fpg.ApplyCrops = true;
                fpg.DoNotUpdateExistingPricesheet = true;
                //so, that was "our defaults", now lets override those with the customer's defaults



                if (!string.IsNullOrEmpty(custId))
                {

                    string getGallerySettingsURL = mainUri + "/" + custId + "?filter=IQ";

                    string JsonResponse = GetJson(getGallerySettingsURL, custId, custPw, "");

                    if (JsonResponse != null && JsonResponse.Length > 5)
                    {
                        JsonParser parser = new JsonParser(new StringReader(JsonResponse), true);
                        JsonObject existingGalleryJson = parser.ParseObject();

                        fpg.MinOrderAmount = Convert.ToDecimal(((existingGalleryJson["galleryConfig"] as JsonObject)["min-order-amount"] as JsonNumber).Value);
                        fpg.ShippingLabel = (existingGalleryJson["galleryConfig"] as JsonObject)["shipping-label"].ToString();
                        fpg.HandlingLabel = (existingGalleryJson["galleryConfig"] as JsonObject)["handling-label"].ToString();
                        fpg.HandlingRateType = (existingGalleryJson["galleryConfig"] as JsonObject)["handling-rate-type"].ToString();
                        fpg.TaxLabel = (existingGalleryJson["galleryConfig"] as JsonObject)["tax-label"].ToString();

                        fpg.TaxAllOrders = ((existingGalleryJson["galleryConfig"] as JsonObject)["tax-all-orders"] as JsonBoolean).Value;
                        fpg.OrderThankyouMessage = (existingGalleryJson["galleryConfig"] as JsonObject)["order-thankyou-message"].ToString();
                        fpg.DisableCropping = ((existingGalleryJson["galleryConfig"] as JsonObject)["disable-cropping"] as JsonBoolean).Value;
                        fpg.DefaultShowImageName = ((existingGalleryJson["galleryConfig"] as JsonObject)["default-show-image-names"] as JsonBoolean).Value;

                        if ((existingGalleryJson["galleryConfig"] as JsonObject)["shipmentType"].ToString() == "pickup")
                        {
                            fpg.PickupShip = true;
                            fpg.DirectShip = false;
                            fpg.CustomerChoiceShip = false;
                        }
                        else if ((existingGalleryJson["galleryConfig"] as JsonObject)["shipmentType"].ToString() == "direct")
                        {
                            fpg.PickupShip = false;
                            fpg.DirectShip = true;
                            fpg.CustomerChoiceShip = false;
                        }
                        else
                        {
                            fpg.PickupShip = false;
                            fpg.DirectShip = false;
                            fpg.CustomerChoiceShip = true;
                        }
                        if ((existingGalleryJson["galleryConfig"] as JsonObject)["shipStartDate"] != JsonNull.Null)
                        {
                            fpg.SwitchShippingDate = DateTime.ParseExact((existingGalleryJson["galleryConfig"] as JsonObject)["shipStartDate"].ToString(), "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture);
                            fpg.SwitchShipping = true;
                        }
                        else
                        {
                            fpg.SwitchShippingDate = null;
                            fpg.SwitchShipping = false;
                        }
                        fpg.PickupText = (existingGalleryJson["galleryConfig"] as JsonObject)["pickupLabel"].ToString();

                        if (((existingGalleryJson["galleryConfig"] as JsonObject)["shipping"] as JsonNumber).Value > 0)
                            fpg.ShippingCost = Convert.ToDecimal(((existingGalleryJson["galleryConfig"] as JsonObject)["shipping"] as JsonNumber).Value);
                        else
                            fpg.ShippingCost = 0;

                        if (((existingGalleryJson["galleryConfig"] as JsonObject)["handling"] as JsonNumber).Value > 0)
                            fpg.HandlingCost = Convert.ToDecimal(((existingGalleryJson["galleryConfig"] as JsonObject)["handling"] as JsonNumber).Value);
                        else
                            fpg.HandlingCost = 0;

                        if (((existingGalleryJson["galleryConfig"] as JsonObject)["tax"] as JsonNumber).Value > 0)
                            fpg.TaxRate = Convert.ToDecimal(((existingGalleryJson["galleryConfig"] as JsonObject)["tax"] as JsonNumber).Value);
                        else
                            fpg.TaxRate = 0;

                        if (((existingGalleryJson["galleryConfig"] as JsonObject)["handling"] as JsonNumber).Value > 0)
                            fpg.HandlingCost = Convert.ToDecimal(((existingGalleryJson["galleryConfig"] as JsonObject)["handling"] as JsonNumber).Value);
                        else
                            fpg.HandlingCost = 0;

                        fpg.TaxShipping = ((existingGalleryJson["galleryConfig"] as JsonObject)["tax-shipping"] as JsonBoolean).Value;

                        fpg.ShowBWSepia = ((existingGalleryJson["galleryConfig"] as JsonObject)["show-bw-sepia"] as JsonBoolean).Value;

                        fpg.Theme = (existingGalleryJson["galleryConfig"] as JsonObject)["theme"].ToString();
                        fpg.Wallpaper = (existingGalleryJson["galleryConfig"] as JsonObject)["wallpaper"].ToString();
                    }
                }
                this.CurrentFlowProject.FlowProjectDataContext.ProjectGalleries.InsertOnSubmit(fpg);

                this.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();

            }

            ProjectGallery gallery = this.CurrentFlowProjectGallery;

            if (gallery.DirectShip == gallery.PickupShip == gallery.CustomerChoiceShip == false)
                gallery.PickupShip = true;

            if (gallery.WatermarkLocation == null)
                gallery.WatermarkLocation = "middle-center";

            if (gallery.OneImagePerPackage == null)
                gallery.OneImagePerPackage = true;
            if (gallery.PickupText == null || gallery.PickupText.Length < 1)
                gallery.PickupText = "School";

            if (gallery.ApplyCrops == null)
                gallery.ApplyCrops = true;

            if (gallery.DoNotUpdateExistingPricesheet == null)
                gallery.DoNotUpdateExistingPricesheet = true;
            //this.DataContext = gallery;            
        }



        internal void FixOrphanedImages()
        {
            logger.Info("Started fixing orphaned images");
            int fixCount = 0;
            List<string> images = Directory.GetFiles(this.CurrentFlowProject.ImageDirPath).ToList();
            foreach (string image in images)
            {
                if (image.ToLower().EndsWith("jpg"))
                {
                    bool ableToAssign = false;
                    string filename = (new FileInfo(image)).Name;
                    if (!this.CurrentFlowProject.FlowProjectDataContext.SubjectImages.Any(si => si.ImageFileName.ToLower() == filename.ToLower()))
                    {
                        //this image is not in flow

                        //try and parse the filename
                        string[] parsedFilename = filename.Split("_");
                        if (parsedFilename.Count() < 2)
                            parsedFilename = filename.Split("-");

                        if (parsedFilename.Count() > 1)
                        {
                            string firstName = parsedFilename[1];
                            string lastName = parsedFilename[0];

                            List<Subject> subs = new List<Subject>();
                           foreach( Subject sub in  this.CurrentFlowProject.FlowProjectDataContext.Subjects)
                           {
                                if (sub.FormalFullName.ToLower() == lastName.ToLower() + ", " + firstName.ToLower())
                                {
                                    subs.Add(sub);
                                }
                            }
                           if (subs.Count > 1)
                           {
                               List<Subject> subsWithImages = new List<Subject>();
                               foreach (Subject s in subs)
                               {
                                   if (s.HasAssignedImages)
                                       subsWithImages.Add(s);
                               }
                               if (subsWithImages.Count == 1)
                                   subs = subsWithImages;
                           }
                           if (subs.Count == 1)
                           {
                               fixCount++;
                               //FlowMessageBox msg = new FlowMessageBox("fix image", "We are able to assign the orphaned image: " + filename);
                               //msg.CancelButtonVisible = false;
                               //msg.ShowDialog();

                               subs[0].AssignImage(filename, true, FlowMasterDataContext.LoginID, CustomSettings.Force8x10Crop);
                               ableToAssign = true;
                               this.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();
                           }
                           

                        }

                        if (!ableToAssign)
                        {
                            //FlowMessageBox msg = new FlowMessageBox("fix image", "We are NOT able to assign the orphaned image: " + filename + "\n\n Leaving It alone for now");
                            //msg.CancelButtonVisible = false;
                            //msg.ShowDialog();
                            //File.Move(image, Path.Combine(this.FlowMasterDataContext.PreferenceManager.Capture.HotFolderDirectory, TicketGenerator.GenerateTicketString(6) + "_" + filename));
                        }
                    }

                   
                }

            }

            logger.Info("Done fixing orphaned images");
            FlowMessageBox msg = new FlowMessageBox("fix image", "Done fixing orphaned images: " + fixCount);
            msg.CancelButtonVisible = false;
            msg.ShowDialog();
        }

        internal void JpgPngSwap(string pngFilePath, string remoteimageservicestatus)
        {
            string pngFileName = (new FileInfo(pngFilePath)).Name;
            string jpgFileName = pngFileName.Replace(".png", ".JPG");
            SubjectImage si = this.CurrentFlowProject.FlowProjectDataContext.SubjectImages.FirstOrDefault(i => i.ImageFileName.ToLower() == jpgFileName.ToLower());
            if(si != null)
            {
                si.ImagePath = pngFilePath;
                si.ImagePathFullRes = pngFilePath;
                si.ImageFileName = pngFileName;
                si.GreenScreenSettings = null;
                if (si.IsGroupImage)
                {
                    si.GroupImage.ImagePath = pngFilePath;
                    si.GroupImage.ImageFileName = new FileInfo(pngFilePath).Name;
                    foreach (SubjectImage subi in si.GroupImage.GetSubjectImageList)
                    {
                        subi.ImagePath = pngFilePath;
                        subi.ImagePathFullRes = pngFilePath;
                        subi.ImageFileName = new FileInfo(pngFilePath).Name;
                        subi.GreenScreenSettings = null;
                        subi.RefreshImage();
                        subi.Subject.ClearGSCache(Path.Combine(this.CurrentFlowProject.ImageDirPath, "PLCache", "GreenScreen"));
                    }
                    si.GroupImage.RefreshImage();
                }
                si.Subject.ClearGSCache(Path.Combine(this.CurrentFlowProject.ImageDirPath, "PLCache", "GreenScreen"));
                if (remoteimageservicestatus != null && remoteimageservicestatus.Length > 0)
                    si.RemoteImageServiceStatus = remoteimageservicestatus;

                this.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();
                
            }
        }
    }
    public class AllProjectSearchResult
    {
        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        public FlowProject FlowProject { get; private set; }
        public string ProjectName { get; private set; }
        public string TicketCode { get; private set; }

        public string FullName { get { return LastName + ", " + FirstName; } }

        public AllProjectSearchResult(string first, string last, FlowProject proj, string pName, string ticket)
        {
            FirstName = first;
            LastName = last;
            FlowProject = proj;
            ProjectName = pName;
            TicketCode = ticket;
        }
    }

    public class PlicCatalogContent
    {
        public Flow.Model.Plic.Catalog.Catalog content { get; set; }
    }

   

}
