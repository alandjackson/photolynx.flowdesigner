﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Flow.Schema.LinqModel.DataContext;
using ICSharpCode.SharpZipLib.Zip;
using Flow.Lib;
using Flow.Lib.Helpers;
using StructureMap;
using System.ComponentModel;

namespace Flow.Controller.Project
{
    public class ProjectImporter
    {
        public delegate void ImportCompleteDelegate(ProjectImporter importer);

        public event ImportCompleteDelegate ImportCompelete;

        ProjectController _projectController;
        FlowController _flowController;
        FlowMasterDataContext _flowMasterDataContext;
        string _archiveFilePath; // For PAF imports
        string _sourceDirectory; // For uncompressed directory imports
        public bool OpenAfterImport { get; set; }
        public string TempID { get; set; }
        public NotificationProgressInfo ProgressInfo { get; set; }
        public string[] SourceDatabaseFiles { get; set; }
        public string TempPath { get; set; }

        public ProjectImporter(FlowController flowController, ProjectController projectController, FlowMasterDataContext flowMasterDataContext)
        {
            _flowController = flowController;
            _projectController = projectController;
            _flowMasterDataContext = flowMasterDataContext;
        }

        public void BeginAsyncAttachProject(string archiveFilePath, bool openAfterImport)
        {
            _archiveFilePath = archiveFilePath;
            _sourceDirectory = null;
            OpenAfterImport = openAfterImport;

            ProgressInfo = new NotificationProgressInfo("Import Project", "Initializing...");
            IShowsNotificationProgress progressNotification = ObjectFactory.GetInstance<IShowsNotificationProgress>();

            progressNotification.ShowNotification(ProgressInfo);
            ProgressInfo.NotificationProgress = progressNotification;


            //_flowController.ApplicationPanel.Dispatcher.BeginInvoke((Action)delegate
            // {
            //     AttachProject();
            // });


            FlowBackgroundWorkerManager.RunWorker(AttachProject);
        }

        public void BeginAsyncAttachProjectUncompressed(string sourceDirectory, bool openAfterImport)
        {
            _sourceDirectory = sourceDirectory;
            _archiveFilePath = null;
            OpenAfterImport = openAfterImport;

            ProgressInfo = new NotificationProgressInfo("Import Project", "Initializing...");
            IShowsNotificationProgress progressNotification = ObjectFactory.GetInstance<IShowsNotificationProgress>();

            progressNotification.ShowNotification(ProgressInfo);
            ProgressInfo.NotificationProgress = progressNotification;

            //_flowController.ApplicationPanel.Dispatcher.BeginInvoke((Action)delegate
            // {
            //     AttachProject();
            // });

            FlowBackgroundWorkerManager.RunWorker(AttachProject);
        }

        private void ImportProject_OnZipProgress(object o, ICSharpCode.SharpZipLib.Core.ProgressEventArgs args)
        {
            if (ProgressInfo != null)
            {
                ProgressInfo.Progress = (int)Math.Floor(args.PercentComplete);
                ProgressInfo.Update();
            }
        }

        public void AttachProject(object sender, DoWorkEventArgs e)
        {
            TempID = Guid.NewGuid().ToString();
            string projectsDir = this._flowMasterDataContext.PreferenceManager.Application.ProjectsDirectory;
            TempPath = Path.Combine(projectsDir, TempID);

            if (_archiveFilePath != null) // PAF Import
            {
                // Extract the archive file to the target project directory
                FastZipEvents events = new FastZipEvents();
                events.Progress = new ICSharpCode.SharpZipLib.Core.ProgressHandler(ImportProject_OnZipProgress);
                FastZip zip = new FastZip(events);
                zip.ExtractZip(_archiveFilePath, TempPath, FastZip.Overwrite.Always, null, null, null, false);
            }
            else // Uncompressed Import
            {
                // Copy the already uncompressed files
                IOUtil.CopyDirectoryRecursive(_sourceDirectory, TempPath);
            }

            if (ProgressInfo != null)
            {
				//ProgressInfo.Message = "Import complete.";
				//ProgressInfo.Progress = -1;
				//ProgressInfo.Update();

				//ProgressInfo.Complete("Import complete.");
                ProgressInfo.Update("Import complete.");
                ProgressInfo.ShowDoneButton();
            }

            //lets handle layouts and graphics in this folder
            string projLayouts = Path.Combine(TempPath, "Layouts");
            string projGraphics = Path.Combine(TempPath, "Graphics");
            if (Directory.Exists(projLayouts))
            {
                foreach (string file in Directory.GetFiles(projLayouts))
                {
                    string destFile = Path.Combine(FlowContext.FlowLayoutsDirPath, new FileInfo(file).Name);
                    if(!File.Exists(destFile))
                        File.Copy(file, destFile);
                }
            }

            if (Directory.Exists(projGraphics))
            {
                foreach (string file in Directory.GetFiles(projGraphics))
                {
                    string destFile = Path.Combine(FlowContext.FlowGraphicsDirPath, new FileInfo(file).Name);
                    if (!File.Exists(destFile))
                        File.Copy(file, destFile);
                }
            }






            SourceDatabaseFiles = Directory.GetFiles(TempPath, "*.sdf");

            if (SourceDatabaseFiles.Length == 1)
            {
                if (ImportCompelete != null)
                {
                    _flowController.ApplicationPanel.Dispatcher.BeginInvoke((Action)delegate
                     {
                         ImportCompelete(this);
                     });
                }
            }
            else
            {
                throw new Exception("Invalid archive file; no database file found.");
            }
        }
    }
}
