﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;

using Flow.Controls;
using Flow.Lib;
using Flow.Lib.Helpers;
using Flow.Lib.ImageUtils;
using Flow.Lib.Net;
using Flow.Lib.Preferences;
using Flow.Schema.LinqModel.DataContext;
using ICSharpCode.SharpZipLib.Zip;
using StructureMap;
using Flow.Schema.Reports;
using Flow.Controller.Reports;
using System.Windows.Threading;
using Rebex.Net;
using System.Threading;
using Flow.Controls.View.Dialogs;
using NLog;
using Flow.Lib.FlowOrder;
using Flow.Lib.Activation;

namespace Flow.Controller.Project
{
    public class ProjectUploader
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        FlowProject _flowProject;
        bool _withDetach;
		bool _withReducedImages;
        int _reducedSize;
        FlowMasterDataContext _flowMasterDataContext;
        FlowController _flowController;
        Dispatcher dispatcher;

        public NotificationProgressInfo ProgressInfo { get; set; }

        public ProjectUploader(FlowController fctrl, FlowMasterDataContext flowMasterDataContext, FlowProject flowProject, bool withDetach, bool withReducedImages, Dispatcher disp, string newSize)
        {
            dispatcher = disp;
            _flowMasterDataContext = flowMasterDataContext;
            _flowProject = flowProject;
            _withDetach = withDetach;
            _withReducedImages = withReducedImages;
            _flowController = fctrl;
            try
            {
                _reducedSize = Int32.Parse(newSize);
            }
            catch
            {
                _reducedSize = 400;
            }
        }

        /// <summary>
        /// Worker thread for executing project upload operations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
		
		void UploadProject_Worker(object sender, DoWorkEventArgs e)
        {

           

           //wait for init subjects
            //wait to init subjects
            int waitCount = 0;
            string dots = ".";
            while (this._flowProject.initingSubjects)
            {
                dots += ".";
                if(ProgressInfo != null)
                    ProgressInfo.Update("Waiting for subjects to initialize." + dots);
                waitCount++;
                Thread.Sleep(3000);

                if (waitCount == (4 * 60)) //if its taken longer than 4 minutes, bail
                    this._flowProject.initingSubjects = false;

            }


            OrderUploadHistory hist = new OrderUploadHistory();
            hist.LabOrderId = "0";
            this._flowController.ProjectController.FlowMasterDataContext.OrderUploadHistories.InsertOnSubmit(hist);
            this._flowController.ProjectController.FlowMasterDataContext.SubmitChanges();
            int orderhistoryID = hist.OrderUploadHistoryID;

            if (ProgressInfo != null)
                ProgressInfo.Update("Preparing Project for Upload");

            string destinationFilePath = PrepareUpload();

           

            string queueDir = Path.Combine(_flowProject.FlowMasterDataContext.PreferenceManager.Application.LabOrderQueueDirectory, "Project_exp_" + orderhistoryID.ToString());
            if (!Directory.Exists(queueDir))
                Directory.CreateDirectory(queueDir);
            File.Move(destinationFilePath, System.IO.Path.Combine(queueDir, _flowProject.FileSafeProjectName + ".paf"));

                //send email
            string mailFile = System.IO.Path.Combine(queueDir, _flowProject.FileSafeProjectName + "_mail.txt");

            string studioemail = null;
            if (_flowProject.FlowMasterDataContext.Organizations.First(o => o.OrganizationID == (int)_flowProject.StudioID).OrganizationContactEmail != null &&
                _flowProject.FlowMasterDataContext.Organizations.First(o => o.OrganizationID == (int)_flowProject.StudioID).OrganizationContactEmail.Length > 1)
                studioemail = _flowProject.FlowMasterDataContext.Organizations.First(o => o.OrganizationID == (int)_flowProject.StudioID).OrganizationContactEmail;

            string studioName = _flowProject.FlowMasterDataContext.Organizations.First(o => o.OrganizationID == (int)_flowProject.StudioID).OrganizationName;
            if (_flowProject.FlowMasterDataContext.PreferenceManager.Application.LabEmail != null && _flowProject.FlowMasterDataContext.PreferenceManager.Application.LabEmail.Length > 2)
            {
                if (ProgressInfo != null)
                    ProgressInfo.Update("Rendering Order Form");

                string orderFormFileName = System.IO.Path.Combine(queueDir, _flowProject.FileSafeProjectName + "_OrderForm.pdf");
                using (StreamWriter sw = File.CreateText(mailFile))
                {
                    sw.WriteLine("To: " + _flowProject.FlowMasterDataContext.PreferenceManager.Application.LabEmail);
                    sw.WriteLine("From: " + "flow@flowadmin.com");
                    sw.WriteLine("CC: " + studioemail);
                    sw.WriteLine("Subject: " + "Flow Project Export Uploaded - " + studioName + " -- DO NOT REPLY");
                    sw.WriteLine("Attatchment: " + orderFormFileName);
                    sw.WriteLine("New Flow Project Export Uploaded\n\nStudio: " + studioName + "\nProject: " + _flowProject.FlowProjectName + "\n\n");
                }

                this.dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                {

                    this._flowController.ProjectController.CurrentFlowProject.SelectedReportName = "Order Form";

                    FlowReportView newReportView = new FlowReportView("Order Form", this._flowController.ProjectController.CurrentFlowProject);

                    ReportDataDefinitions.GetReportData(newReportView, _flowController.ProjectController.CurrentFlowProject.FlowProjectDataContext.FilteredOrderPackages.ToList());
                    FlowReport report = new FlowReport(newReportView);
                    report.OrderNumber = "None";
                    report.Generate();


                    ReportDataDefinitions.ExportReport(
                        this._flowController.ProjectController.CurrentFlowProject.SelectedReportName + " - " + this._flowController.ProjectController.CurrentFlowProject.FileSafeProjectName,
                        report.C1Document, new KeyValuePair<string, string>("PDF", "Adobe PDF (*.pdf)|*.pdf"),
                        orderFormFileName, false
                    );

                }));
            }

            if (ProgressInfo != null)
                ProgressInfo.Update("Preparing export for upload.");

                FlowOrderStore store = new FlowOrderStore();
                store.OrderID = 0;
                store.OrderDate = DateTime.Now;

                ActivationValidator activation = new ActivationValidator(this._flowController.ActivationKeyFile);
                ActivationStore activationStore = activation.Repository.Load();

                store.ActivationKey = activationStore.ActivationKey;
                store.ProjectName = _flowProject.FlowProjectName;
                store.ComputerName = activationStore.ComputerName;
                store.StudioName = studioName;
                store.FlowVersion = FlowContext.GetFlowVersion();
                store.IsImageMatchExport = false;
                store.SubjectCount = _flowProject.SubjectList.Count;
                store.ImageCount = _flowProject.ImageList.Count;
                store.PackageCount = _flowProject.OrderPackageCount;
                FlowOrderPost FOPost = new FlowOrderPost();
                string returnMsg = FOPost.Post(store);

                hist.LabOrderId = "0";
                hist.CreateDate = DateTime.Now;
                hist.ProjectName = _flowProject.FlowProjectName;
                hist.ProjectGuid = _flowProject.FlowProjectGuid.ToString();
                hist.ComputerName = activationStore.ComputerName;
                hist.FlowVersion = FlowContext.GetFlowVersion();
                hist.IsImageMatchExport = false;
                hist.SubjectCount = _flowProject.SubjectList.Count;
                hist.ImageCount = _flowProject.ImageList.Count;
                hist.PackageCount = _flowProject.OrderPackageCount;
                this._flowController.ProjectController.FlowMasterDataContext.SubmitChanges();
                this._flowController.ProjectController.FlowMasterDataContext.RefreshOrderUploadHistoryList();
                
                using (StreamWriter sw = File.AppendText(mailFile))
                {
                    sw.WriteLine("HistoryID: " + orderhistoryID);
                }

                this.dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                {
                    this._flowController.ApplicationPanel.IsEnabled = true;
                }));
            if (_withDetach)
            {
                _flowMasterDataContext.DetachProject(_flowProject);
                try
                {
                    Directory.Delete(_flowProject.DirPath, true);
                }
                catch (Exception ex)
                {
                    logger.WarnException("Problem deleting project directory, ignoring", ex);
                }
                
            }

            if (ProgressInfo != null)
                ProgressInfo.Complete("Done Preparing Export. Upload has been queued.");
            ProgressInfo = null;
        }

       

        void UploadProject_Completed(object sender, RunWorkerCompletedEventArgs e)
        {
            this._flowController.ApplicationPanel.IsEnabled = true;
            foreach (OrderFormFieldLocal field in this._flowProject.FlowProjectDataContext.OrderFormFieldLocalList)
            {
                field.SelectedValue = field.FieldValue;
            }
            this._flowProject.FlowProjectDataContext.SubmitChanges();
        }

        /// <summary>
        /// Uploads a Flow project archive (database and image files) to an ftp server;
        /// option to detach removes references to project from FlowMaster
        /// </summary>
        /// <param name="flowProject"></param>
        /// <param name="destinationPath"></param>
        /// <param name="withDetach"></param>
        public void BeginAsyncUploadProject()
        {
            ProgressInfo = new NotificationProgressInfo("Upload Project", "Initializing...");
			ProgressInfo.AllowCancel = true;
            IShowsNotificationProgress progressNotification = ObjectFactory.GetInstance<IShowsNotificationProgress>();

            progressNotification.ShowNotification(ProgressInfo);
            ProgressInfo.NotificationProgress = progressNotification;
            try
            {
                FlowBackgroundWorkerManager.RunWorker(
                    UploadProject_Worker,
                    UploadProject_Completed
                );
            }
            catch (Exception e)
            {

            }
        }

        public string PrepareUpload()
        {
            FileInfo tmpDir = new FileInfo(FlowContext.FlowTempDirPath);
            if (!tmpDir.Exists)
                tmpDir.Directory.Create();

            string destinationFilePath = Path.Combine(tmpDir.FullName, _flowProject.FileSafeProjectName + ".paf");

            string sourcePath = _flowProject.DirPath;

            if (_withReducedImages)
            {
                sourcePath = FlowContext.CombineDir(FlowContext.FlowTempDirPath, Guid.NewGuid().ToString());
                string sourceImagePath = FlowContext.CombineDir(sourcePath, "Image");

                DirectoryInfo imageDirInfo = new DirectoryInfo(_flowProject.ImageDirPath);

                FileInfo projectDbFile = new FileInfo(_flowProject.LocalDBPath);

                // Copy the db file to the temp dir
                IOUtil.CopyFile(_flowProject.LocalDBPath, Path.Combine(sourcePath, projectDbFile.Name));

                FileInfo[] fileList = imageDirInfo.GetFiles("*.jpg");

                ProgressInfo.Update("Reducing image", 0, fileList.Length, 1, false);

                foreach (FileInfo fileInfo in fileList)
                {
                    ProgressInfo++;

                    ProgressInfo.Update("Reducing image " + fileInfo.Name);

                    ImageLoader.SaveReducedImage(fileInfo.FullName, sourceImagePath, _reducedSize);
                }
            }

            FastZipEvents events = new FastZipEvents();
            events.Progress = new ICSharpCode.SharpZipLib.Core.ProgressHandler(UploadProject_OnZipProgress);
            FastZip zip = new FastZip(events);
            zip.CreateZip(destinationFilePath, sourcePath, true, null);

            return destinationFilePath;
        }

        private void UploadProject_OnZipProgress(object o, ICSharpCode.SharpZipLib.Core.ProgressEventArgs args)
        {
            if (ProgressInfo != null)
            {
                ProgressInfo.Progress = (int)Math.Floor(args.PercentComplete);
                ProgressInfo.Update();
            }
        }
    }
}
