﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.Schema.LinqModel.DataContext;
using System.Net;
using System.IO;
using NetServ.Net.Json;
using System.Windows.Media.Imaging;
using Flow.Lib.ImageUtils;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Reflection;
using Flow.Lib.Helpers;
using Flow.Lib;
using Flow.View.Dialogs;
using System.Windows.Threading;
using System.Threading;
using Flow.Schema.LinqModel.DataContext.FlowMaster;
using System.Windows;
using NLog;

namespace Flow.Controller.Project
{

    class SubmitOnlineOrderData
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        private FlowProject _flowProject {get;set;}
        private FlowMasterDataContext _flowMasterDataContext {get; set;}
        private ProjectGallery _projectGallery { get; set; }
        private List<Subject> _applicableSubjects { get; set; }

        private JsonObject existingGalleryJson { get; set; }
        private JsonObject existingPSJson { get; set; }

        public NotificationProgressInfo NotificationInfo { get; set; }

        public bool SwitchShippingEnabled { get; set; }
        public bool doCrop { get; set; }
        public int allowableYearbookSelections { get; set; }
        
        //public string WelcomeMessage { get; set; }
        //public string WelcomeImage { get; set; }
        //public string GalleryName { get; set; }
        //public string GalleryPassword { get; set; }
        //public bool SingleImagePagackes { get; set; }
        //public string EventID { get; set; }


        public SubmitOnlineOrderData(FlowProject fp, FlowMasterDataContext fmdc, ProjectGallery fpg, List<Subject> applicableSubjects)
        {
            _flowProject = fp;
            _flowMasterDataContext = fmdc;
            _projectGallery = fpg;
            _applicableSubjects = applicableSubjects;
        }

        public bool Begin()
        {
            // Check if any of the product packages have a price less than 1.00
            //if(_projectGallery.DoNotUpdateExistingPricesheet != true)
            foreach (ProductPackage productPackage in _flowProject.FlowCatalog.ProductPackages)
            {
                if ((double)productPackage.Price < 1.0 && productPackage.ShowInOnlinePricesheet == true)
                {
                    this._flowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                    {
                        string msg_text = String.Format("Product package '{0}' has a price less than $1.00, which is not allowed.\n\nPlease set the price to $1.00 or greater.", productPackage.ProductPackageDesc);
                        FlowMessageBox msg = new FlowMessageBox("Invalid Pricesheet", msg_text);
                        msg.CancelButtonVisible = false;
                        msg.ShowDialog();
                    }));
                    if (NotificationInfo != null)
                        this.NotificationInfo.Complete("Invalid Pricesheet");

                    return false;
                }
            }
            
            NotificationInfo.Title = "Uploading Gallery";
            NotificationInfo.Update();
            //string eventID = "1056-8030-0014";

            //dont leave this hard coded
            bool isGreenScreen = false;
            
            if (this._flowProject.IsGreenScreen == true)
                isGreenScreen = true;


            //string mainUri = this._flowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixLoginURL;
            //string mainUri = "http://demo.imagequix.com:8080/osgi/rest/customer";
            string publisherUri = this._flowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixBaseURL + "/publisher/rest/customer";
            string catalogUri = this._flowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixBaseURL + "/catalog/rest/customer";
            string publishImageUri = this._flowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixBaseURL + "/publisher/rest/customer";

            string custId = this._flowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixCustomerID;
            string custPw = this._flowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixCustomerPassword;
            string installID = this._flowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixApplicationID;
            int eventCounter = this._flowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixEventCounter;

            bool isNewGallery = false;
            //Generate eventID

            string installCodeUri = publisherUri + "/" + custId + "/" + "installcode";

            //check or get installID
            if (installID == null || installID == "")
            {
                if (NotificationInfo != null)
                {
                    NotificationInfo.Update("Getting ImageQuix Install ID");
                }
                installID = GetInstallID(installCodeUri, custId, custPw);
                if (installID == null || installID == "")
                {
                    
                    NotificationInfo.Update("FAILED TO GET INSTALLID");
                    NotificationInfo.ShowDoneButton();
                    return false;
                }
                this._flowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixApplicationID = installID;
                this._flowMasterDataContext.SavePreferences();
            }
            //string OriginalEventID = this._projectGallery.EventID;

            //this._projectGallery.EventID = null;
            if (this._projectGallery.EventID == null)
            {
                isNewGallery = true;
                this._flowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixEventCounter = ++eventCounter;
                this._flowMasterDataContext.SavePreferences();
                string eventCounterS = eventCounter.ToString().PadLeft(5, '0');
                string eventID = installID.Substring(0, 4) + "-" + installID.Substring(4) + eventCounterS.Substring(0, 1) + "-" + eventCounterS.Substring(1);
                this._projectGallery.EventID = eventID;
            }

            if(_projectGallery.LastModifyDate == null)
                isNewGallery = true;

            string loginUri = publisherUri + "/" + custId + "/" + "auth";
            string postNewEventUri = publisherUri + "/" + custId + "/" + "event/publish";
            string postUpdateEventUri = publisherUri + "/" + custId + "/" + "event/" + _projectGallery.EventID + "/update";

            string updateImageCacheUri = publisherUri + "/" + custId + "/" + "event/" + _projectGallery.EventID + "/image/cleanCache?filter=IQ";

            
           


            WebHeaderCollection returnHeader = new WebHeaderCollection();
            if (NotificationInfo != null)
            {
                NotificationInfo.Update("Checking ImageQuix Credentials");
            }
            //validate login credentials
            if (PostToUri(loginUri, custId, custPw) != HttpStatusCode.OK)
            {
                //Error Connection - maybe not authorized?
                NotificationInfo.Update("FAILED SERVER LOGIN");
                NotificationInfo.ShowDoneButton();
                return false;
            }


            List<KeyValuePair<string, string>> GalleryCodes = new List<KeyValuePair<string, string>>();

            string allPricesheetURL = publisherUri + "/" + custId + "/" + "pricesheetStore";

            string getGalleryURL = publisherUri + "/" + custId + "/" + "event/" + _projectGallery.EventID + "?filter=IQ";
            string getSubjectsURL = publisherUri + "/" + custId + "/" + "event/" + _projectGallery.EventID + "/data/?filter=IQ";
            //https://vando.imagequix.com/rest/cust/T9EPQ96/event/1087-9980-0008/data/?filter=IQ
            string getPSURL = publisherUri + "/" + custId + "/" + "event/" + _projectGallery.EventID + "/pricesheet?filter=IQ";

            JsonNumber existingGalleryCode = 0;
            JsonNumber marketingCampaignId = 0;
            //lets get the existing gallery json
            if (isNewGallery)
            {
                existingGalleryJson = null;
                existingPSJson = null;


                string JsonResponse = GetJson(allPricesheetURL, custId, custPw, "");
                if (JsonResponse != null)
                {
                    JsonParser parser = new JsonParser(new StringReader(JsonResponse), true);
                    //NetServ.Net.Json.JsonParser.TokenType nextTokenTrash = parser.NextToken();

                    JsonArray allPricesheets = parser.ParseArray();

                    foreach (JsonObject psitem in allPricesheets)
                    {
                        JsonObject ps = (JsonObject)psitem["pricesheet"];
                        if ((JsonString)ps["name"] == this._flowProject.FlowCatalog.FlowCatalogDesc)
                        {
                            existingPSJson = ps;
                            _projectGallery.PricesheetID = ((JsonNumber)ps["id"]).ToString();
                        }
                    }
                }

            }
            else
            {
                string JsonResponse = GetJson(getGalleryURL, custId, custPw, "");
                if (JsonResponse == null)
                    existingGalleryJson = null;
                else
                {
                    //int loc = JsonResponse.IndexOf("eventType");
                    //JsonResponse = JsonResponse.Remove(loc, "eventType".Length).Insert(loc, "eventType2");
                    JsonParser parser = new JsonParser(new StringReader(JsonResponse), true);
                    //NetServ.Net.Json.JsonParser.TokenType nextToken = parser.NextToken();
                    //NetServ.Net.Json.JsonParser.TokenType nextToken2 = parser.NextToken();
                    existingGalleryJson = parser.ParseObject();
                    existingGalleryCode = existingGalleryJson["gallery-code"] as JsonNumber;
                    marketingCampaignId = existingGalleryJson["marketing-campaign-id"] as JsonNumber;

                    JsonWriter _writerPS = new JsonWriter();
                    existingGalleryJson.Write(_writerPS);
                    string _postDataPS = _writerPS.ToString().Replace("\\/", "/");
                    string existingGalleryPostJsonFile = Path.Combine(FlowContext.FlowTempDirPath, _projectGallery.EventID + "_existingGallery.txt");
                    File.WriteAllText(existingGalleryPostJsonFile, _postDataPS);

                }

               


                string JsonResponse2 = GetJson(getPSURL, custId, custPw, "");
                if (JsonResponse2 == null || JsonResponse2 == "null")
                    existingPSJson = null;
                else
                {
                    //int loc = JsonResponse.IndexOf("eventType");
                    //JsonResponse = JsonResponse.Remove(loc, "eventType".Length).Insert(loc, "eventType2");
                    JsonParser parser2 = new JsonParser(new StringReader(JsonResponse2), true);
                    //NetServ.Net.Json.JsonParser.TokenType nextToken = parser.NextToken();
                    //NetServ.Net.Json.JsonParser.TokenType nextToken2 = parser.NextToken();
                    existingPSJson = parser2.ParseObject();
                }

                string SubjectJson = GetJson(getSubjectsURL, custId, custPw, "");
                if (SubjectJson != null)
                {
                    JsonParser parser3 = new JsonParser(new StringReader(SubjectJson), true);
                    
                    JsonArray existingSubjectJson = parser3.ParseArray();
                    foreach (JsonObject jo in existingSubjectJson)
                    {
                        JsonString thisexistingGalleryCode = jo["gallery-code"] as JsonString;
                        JsonString thisexistingTicketCode = jo["code"] as JsonString;
                    
                        GalleryCodes.Add(new KeyValuePair<string, string>(thisexistingTicketCode.ToString(), thisexistingGalleryCode.ToString()));
                    }
                }
            }


            if (NotificationInfo != null)
            {
                NotificationInfo.Update("Creating Gallery Pricesheet");
                
            }
            //Create Gallery Pricesheet Json
            JsonObject galleryPricesheet = GenerateGalleryPriceSheetJson(_projectGallery.PricesheetID, isGreenScreen);
            JsonWriter writerPS = new JsonWriter();
            galleryPricesheet.Write(writerPS);
            string postDataPS = writerPS.ToString().Replace("\\/", "/");

            //write post data to temp folder
            string galleryPricesheetPostJsonFile = Path.Combine(FlowContext.FlowTempDirPath, _projectGallery.EventID + "_galleryPricesheetPost.txt");
            File.WriteAllText(galleryPricesheetPostJsonFile, postDataPS);


            string postNewPricesheetUri = publisherUri + "/" + custId + "/" + "pricesheetStore?filter=IQ";
            string postUpdatePricesheetUri = publisherUri + "/" + custId + "/" + "pricesheetStore/" + _projectGallery.PricesheetID + "?filter=IQ";


            // Post Price Sheet (receive back Pricesheet ID)
            if (existingPSJson != null && _projectGallery.DoNotUpdateExistingPricesheet == true)
            {
                //do not update the pricesheet
                galleryPricesheet = existingPSJson;
                _projectGallery.PricesheetID = existingPSJson["id"].ToString();
            }
            else
            {
                String ResponsePS = "";
                if (string.IsNullOrEmpty(_projectGallery.PricesheetID))
                {
                    try
                    {
                        ResponsePS = PostPendingEvent(postNewPricesheetUri, custId, custPw, postDataPS, false, out returnHeader);
                    }
                    catch (Exception e)
                    {
                        //this._projectGallery.EventID = OriginalEventID;
                        this._projectGallery.PricesheetID = null;
                        NotificationInfo.Update("FAILED TO PUBLISH PRICE SHEET");
                        NotificationInfo.ShowDoneButton();
                        this._flowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                        {
                            FlowMessageBox msg = new FlowMessageBox("Failed To Upload", "FAILED TO PUBLISH PRICE SHEET.\n\n" + e.Message);
                            msg.CancelButtonVisible = false;
                            msg.ShowDialog();
                        }));
                        return false;
                    }
                }
                else
                {
                    try
                    {
                        ResponsePS = PostPendingEvent(postUpdatePricesheetUri, custId, custPw, postDataPS, true, out returnHeader);
                    }
                    catch (Exception e)
                    {
                        //this._projectGallery.EventID = OriginalEventID;
                        this._projectGallery.PricesheetID = null;
                        NotificationInfo.Update("FAILED TO UPDATE PRICE SHEET");
                        NotificationInfo.ShowDoneButton();
                        this._flowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                        {
                            FlowMessageBox msg = new FlowMessageBox("Failed To Upload", "FAILED TO UPDATE PRICESHEET.\n\n" + e.Message);
                            msg.CancelButtonVisible = false;
                            msg.ShowDialog();
                        }));
                        return false;
                    }
                }

                //get pricesheetID from ResponsePS
                JsonParser parserPS = new JsonParser(new StringReader(ResponsePS), true);
                NetServ.Net.Json.JsonParser.TokenType nextTokenPS = parserPS.NextToken();
                JsonObject rootObjectPS = parserPS.ParseObject();
                if (string.IsNullOrEmpty(_projectGallery.PricesheetID))
                {

                    _projectGallery.PricesheetID = rootObjectPS["id"].ToString();
                }

                //update the ps json to include the pricesheetid
                galleryPricesheet.Remove("id");
                galleryPricesheet.Add("id", Convert.ToInt32(_projectGallery.PricesheetID));
            }
            

            //
            //update all galleries that are referencing this pricesheet
            // this is only needed if we are not preserving the pricesheet
            //
            string getAllGalleriesURL = publisherUri + "/" + custId + "/event?filter=IQ";
           

            //get a list of all galleries
            string JsonResponseAG = GetJson(getAllGalleriesURL, custId, custPw, "");
            if (JsonResponseAG != null && _projectGallery.DoNotUpdateExistingPricesheet != true)
            {
                JsonParser parser = new JsonParser(new StringReader(JsonResponseAG), true);
                //NetServ.Net.Json.JsonParser.TokenType nextTokenTrash = parser.NextToken();

                JsonArray allGalleries = parser.ParseArray();

                foreach (JsonObject gallery in allGalleries)
                {
                    if (((JsonNumber)gallery["pricesheet-id"]).ToString() == _projectGallery.PricesheetID)
                    {
                        string updateGalleryPSUrl = publisherUri + "/" + custId + "/event/" + ((JsonString)gallery["eventID"]).ToString() + "/pricesheet/" + _projectGallery.PricesheetID;
                        try
                        {
                            string ResponsePS = PostPendingEvent(updateGalleryPSUrl, custId, custPw, postDataPS, true, out returnHeader);
                            break;
                        }
                        catch (Exception e)
                        {
                           //we can ignor these exceptions
                        }
                    }
                }
            }

            //iterate thru the list looking for galleries that reference this pricesheet

            //update the pricesheet for each gallery
            //
            //Upload product images and background images
            //
            // not used for now - string itrequestUriProductImage = mainUri + "/" + custId + "/pricesheetStore/" + _projectGallery.PricesheetID + "/psImage/";
            if (isGreenScreen)
            {
                List<string> uploadedbackgrounds = null;
                if (this._projectGallery.GalleryBackgroundList == null)
                    uploadedbackgrounds = new List<string>();
                else
                {
                    uploadedbackgrounds = this._projectGallery.GalleryBackgroundList.Split(",").ToList();
                }

                string itrequestUriGS = publisherUri + "/" + custId + "/pricesheetStore/" + _projectGallery.PricesheetID + "/psBackground/";
                int imgCountGS = _flowMasterDataContext.GreenScreenBackgrounds.Count();
                int prgCountGS = 0;
                foreach (GreenScreenBackground gsb in _flowMasterDataContext.GreenScreenBackgrounds)
                {
                    if (uploadedbackgrounds.Any(w=> w.Trim() == gsb.FileName.Trim()))
                        continue;


                    prgCountGS++;
                    if (NotificationInfo != null)
                    {
                        NotificationInfo.Progress = (int)(((float)prgCountGS / (float)imgCountGS) * (float)100);
                        NotificationInfo.Update("Uploading Green Screen Image " + gsb.FileName);
                    }

                    String ImageTokeResponse = "";

                    try
                    {
                        ImageTokeResponse = RequestImageToken(itrequestUriGS + gsb.FileName, custId, custPw, "");
                    }
                    catch
                    {
                        //try 10 times to get image token
                        int tryLoop = 0;
                        while (++tryLoop <= 10)
                        {
                            try
                            {
                                NotificationInfo.Update("FAILED TO GET IMAGE TOKEN, Trying Again.... " + tryLoop + " ....");
                                ImageTokeResponse = RequestImageToken(itrequestUriGS + gsb.FileName, custId, custPw, "");
                                break;
                            }
                            catch
                            {
                                
                                if (tryLoop == 10)
                                {
                                    //this._projectGallery.EventID = OriginalEventID;
                                    NotificationInfo.Update("FAILED TO GET IMAGE TOKEN, ABORTING....");
                                    NotificationInfo.ShowDoneButton();
                                    return false;
                                }
                                continue;
                            }
                        }
                       
                    }

                    JsonParser parser2 = new JsonParser(new StringReader(ImageTokeResponse), true);
                    NetServ.Net.Json.JsonParser.TokenType nextToken2 = parser2.NextToken();
                    JsonObject rootObject2 = parser2.ParseObject();


                    if (UploadImage(rootObject2, gsb.FullPath, custId, custPw, 0) != HttpStatusCode.OK)
                    {
                        //Error, did not upload
                        NotificationInfo.Update("FAILED TO UPLOAD IMAGE");
                    }
                    //this._flowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                    //{
                    //    si.ExistsInOnlineGallery = true;
                    //}));

                    //this._flowProject.FlowProjectDataContext.SubmitChanges();
                    //this._flowProject.UpdateGalleryCounts();
                    uploadedbackgrounds.Add(gsb.FileName);
                }
                string allbgs="";
                foreach(string b in uploadedbackgrounds)
                    allbgs += b + ", ";

                this._projectGallery.GalleryBackgroundList = allbgs;
            }

            //
            // Begin Big Batch Loop
            //
            List<string> MissingImages = new List<string>();
            bool keepgoing = true;
            bool firstTime = true;
            while (keepgoing)
            {
               // List<Subject> livesubjects = new List<Subject>();
                logger.Info("getting list of subjects to process");
                List<Subject> CurrentSubjectBatch = new List<Subject>();


               
                int newsubcount = 0;
                foreach (Subject sub in this._applicableSubjects.Where(s => s.HasAssignedImages))
                {
                    foreach (SubjectImage si in sub.SubjectImages)
                    {
                        if (si.ExistsInOnlineGallery == true )
                        {

                            CurrentSubjectBatch.Add(sub);
                            break;
                        }
                    }
                    if(newsubcount < 20)
                    {
                        foreach (SubjectImage si in sub.SubjectImages)
                        {
                            if (!MissingImages.Contains(si.ImageFileName)) //make sure its not a known missing image
                            {
                                if (si.ExistsInOnlineGallery != true)
                                {
                                    if (!File.Exists(si.ImagePathFullRes))
                                    {
                                        MissingImages.Add(si.ImageFileName);
                                    }
                                    else
                                    {
                                        if (!CurrentSubjectBatch.Contains(sub))
                                            CurrentSubjectBatch.Add(sub);
                                        newsubcount++;
                                        logger.Info("new subject image needs uploading: " + si.ImageFileName);
                                        break;
                                    }
                                }
                            }
                        }
                    }

                }
                logger.Info("Subject Count: " + CurrentSubjectBatch.Count);
                logger.Info("NEW Subject Count: " + newsubcount);
                if (!firstTime && newsubcount == 0)
                {
                    logger.Info("break - not first time && newsubcount is 0");
                    
                    break;
                }

                if (!firstTime)
                {
                    isNewGallery = false;
                }
                firstTime = false;

                if (NotificationInfo != null)
                {
                    NotificationInfo.Update("Creating Gallery Template");
                }




                //Create Gallery Json
                JsonObject galleryJson = GenerateGalleryJson(_projectGallery.EventID, galleryPricesheet, isGreenScreen, isNewGallery, CurrentSubjectBatch, existingGalleryCode, marketingCampaignId, GalleryCodes);
                JsonWriter writer = new JsonWriter();
                galleryJson.Write(writer);
                string postData = writer.ToString().Replace("\\/", "/");

                //write post data to temp folder
                string galleryPostJsonFile = Path.Combine(FlowContext.FlowTempDirPath, _projectGallery.EventID + "_GalleryPost.txt");
                File.WriteAllText(galleryPostJsonFile, postData);

                NotificationInfo.Update("Sending Welcome Image");
                //
                //Welcome Image
                //
                if (!string.IsNullOrEmpty(_projectGallery.WelcomeImage) && File.Exists(_projectGallery.WelcomeImage))
                {
                    string itrequestUriWelcomeImage = publishImageUri + "/" + custId + "/event/" + _projectGallery.EventID + "/image/" + new FileInfo(_projectGallery.WelcomeImage).Name + "?type=welcome";
                    string WelcomeImageTokeResponse = "";
                    try
                    {
                        WelcomeImageTokeResponse = RequestImageToken(itrequestUriWelcomeImage, custId, custPw, "");
                    }
                    catch
                    {
                        //try 10 times to get image token
                        int tryLoop = 0;
                        while (++tryLoop <= 10)
                        {
                            try
                            {
                                NotificationInfo.Update("FAILED TO GET WELCOME IMAGE TOKEN, Trying Again.... " + tryLoop + " ....");
                                WelcomeImageTokeResponse = RequestImageToken(itrequestUriWelcomeImage, custId, custPw, "");
                                break;
                            }
                            catch
                            {

                                if (tryLoop == 10)
                                {
                                    //this._projectGallery.EventID = OriginalEventID;
                                    NotificationInfo.Update("FAILED TO GET WELCOME IMAGE TOKEN, ABORTING....");
                                    NotificationInfo.ShowDoneButton();
                                    return false;
                                }
                                continue;
                            }
                        }

                        //this._projectGallery.EventID = OriginalEventID;
                        //NotificationInfo.Update("FAILED TO GET IMAGE TOKEN, ABORTING....");
                        //NotificationInfo.ShowDoneButton();
                        //return;
                    }

                    JsonParser parser = new JsonParser(new StringReader(WelcomeImageTokeResponse), true);
                    NetServ.Net.Json.JsonParser.TokenType nextToken = parser.NextToken();
                    JsonObject rootObject = parser.ParseObject();

                    if (NotificationInfo != null)
                    {
                        NotificationInfo.Update("Uploading Welcome File");
                    }
                    // Send Welcome Image to Amazon S3 Server
                    if (File.Exists(_projectGallery.WelcomeImage))
                    {


                        if (UploadImage(rootObject, _projectGallery.WelcomeImage, custId, custPw, 0) != HttpStatusCode.OK)
                        {
                            //Error, did not upload
                            NotificationInfo.Update("FAILED TO UPLOAD WELCOME IMAGE");
                        }
                    }

                }
                //
                // send images
                //
                //string itrequestUri = mainUri + "/" + custId + "/eventcust/" + custId + "/event/" + eventID + "/image/";
                NotificationInfo.Update("Sending Images");
                logger.Info("About to send images");
                string itrequestUri = publishImageUri + "/" + custId + "/event/" + _projectGallery.EventID + "/image/";
                int imgCount =0;
                int prgCount = 0;
                this._flowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                {
                    imgCount = _flowProject.FlowProjectDataContext.SubjectImages.Count();
                    prgCount = _flowProject.GalleryLiveImageCount;
                    _flowProject.UpdateGalleryCounts();
                }));

                if (NotificationInfo != null)
                {
                    NotificationInfo.Count = imgCount;
                }

                JsonArray updatedImagesJson = new JsonArray();
                if (!isNewGallery)
                {
                    List<string> uploadedGroupImages = new List<string>();
                    int curiter = 1;
                    foreach (Subject sub in CurrentSubjectBatch)
                    {
                        logger.Info("existing Gallery - upload subject images - " + sub.FormalFullName);
                        bool hasmissing = false;
                        NotificationInfo.Update(prgCount + " out of " + imgCount + " (Current Batch: " + curiter + "/20) " + "Uploading Images for: " + sub.FormalFullName);
                        foreach (SubjectImage si in sub.SubjectImages.Where(si => si.ExistsInOnlineGallery == false))
                        {
                            logger.Info("uploading image - " + si.ImageFileName);
                            hasmissing = true;
                            if (!File.Exists(si.ImagePathFullRes))
                                continue;

                            NotificationInfo.Update(prgCount + " out of " + imgCount + " (Current Batch: " + curiter + "/20) " + "Uploading Image: " + si.ImageFileName);

                            if (si.IsGroupImage)
                            {
                                NotificationInfo.Update(prgCount + " out of " + imgCount + " (Current Batch: " + curiter + "/20) " + "Group Image, Check if already uploaded: " + si.ImageFileName);
                                if (uploadedGroupImages.Any(gi => gi == si.ImageFileName.Trim()))
                                {
                                    this._flowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                                        {
                                            si.ExistsInOnlineGallery = true;
                                            sub.FlagForMerge = true;
                                            this._flowProject.UpdateGalleryCounts();
                                        }));
                                    //this._flowProject.FlowProjectDataContext.SubmitChanges();
                                    //this._flowProject.UpdateGalleryCounts();
                                    continue;
                                }
                                uploadedGroupImages.Add(si.ImageFileName.Trim());
                            }



                            NotificationInfo.Update(prgCount + " out of " + imgCount + " (Current Batch: " + curiter + "/20) " + "Make sure its not already uploaded: " + si.ImageFileName);
                            if (!si.ExistsInOnlineGallery)
                            {
                                NotificationInfo.Update(prgCount + " out of " + imgCount + " (Current Batch: " + curiter + "/20) " + "make sure the original file exists: " + si.ImageFileName);
                                if (!File.Exists(si.ImagePathFullRes))
                                {
                                    MissingImages.Add(si.ImageFileName);
                                    continue;
                                }

                                prgCount++;


                                string imageFileName = si.ImageFileName;


                                string pathToImage = si.ImagePathFullRes;

                                if (isGreenScreen && !String.IsNullOrEmpty(si.GreenScreenSettings))
                                {
                                    NotificationInfo.Update(prgCount + " out of " + imgCount + " (Current Batch: " + curiter + "/20) " + "Rendering GreenScreen Image: " + imageFileName);
                                    string pathToPng = "";
                                    int wcount = 0;
                                    while (string.IsNullOrEmpty(pathToPng))
                                    {
                                        wcount++;
                                        try
                                        {

                                            //this._flowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                                            // {
                                            //use this one
                                            Thread t = new Thread(delegate()
                                            {
                                                pathToPng = si.GetRenderedPngImageFilename(null);
                                            });
                                            t.SetApartmentState(ApartmentState.STA);
                                            t.Start();
                                            t.Join();
                                            t.Abort();//just make sure the thread is killed


                                            //pathToPng = si.GetDropoutImagePath(true);

                                            //this one does not use the full tools
                                            //pathToPng = si.GetBasicDropoutImagePath(true);
                                            // }));
                                        }
                                        catch (Exception ex)
                                        {
                                            pathToPng = "";
                                            logger.Info(prgCount + " out of " + imgCount + " (Current Batch: " + curiter + "/20) " + "Rendering GreenScreen Image (Take" + wcount + "): " + imageFileName);
                                            if (wcount == 3)
                                            {

                                                NotificationInfo.Update(prgCount + " out of " + imgCount + " (Current Batch: " + curiter + "/20) " + "Rendering GreenScreen Image (Take" + wcount + "): " + imageFileName);
                                                throw ex;
                                            }
                                        }
                                    }
                                    if (pathToPng != null)
                                    {
                                        pathToImage = pathToPng;
                                        imageFileName = new FileInfo(pathToPng).Name;
                                    }

                                }

                                NotificationInfo.Update(prgCount + " out of " + imgCount + " (Current Batch: " + curiter + "/20) " + "Creating Image Object: " + imageFileName);
                                System.Drawing.Bitmap origBmp = (new Bitmap(pathToImage));

                                updatedImagesJson.Add(imageFileName);

                                //if this is a jpg, we did not render it, so it needs to be rotated before we crop it
                                if (pathToImage.ToLower().EndsWith(".jpg"))
                                {
                                    NotificationInfo.Update(prgCount + " out of " + imgCount + " (Current Batch: " + curiter + "/20) " + "rotating image: " + si.ImageFileName);
                                    if (si.Orientation == 90)
                                        origBmp.RotateFlip(RotateFlipType.Rotate90FlipNone);
                                    if (si.Orientation == 180)
                                        origBmp.RotateFlip(RotateFlipType.Rotate180FlipNone);
                                    if (si.Orientation == 270)
                                        origBmp.RotateFlip(RotateFlipType.Rotate270FlipNone);
                                }

                                if (doCrop)
                                {

                                    //origBmp = si.IntermediateImage
                                    NotificationInfo.Update(prgCount + " out of " + imgCount + " (Current Batch: " + curiter + "/20) " + "Cropping Image: " + imageFileName);
                                    if ((si.CropL + si.CropT + si.CropW + si.CropH) > 0)
                                    {
                                        //there are crop values that need to be used
                                        //Bitmap tmpBmp = new Bitmap(pathToImage);


                                        Int32Rect rect = getCropArea(origBmp, 0, si.CropL, si.CropT, si.CropW, si.CropH);
                                        Bitmap tempBmp = origBmp.Clone(new Rectangle(rect.X, rect.Y, rect.Width, rect.Height), origBmp.PixelFormat);
                                        origBmp.Dispose();
                                        origBmp = tempBmp.Clone() as Bitmap;
                                        tempBmp.Dispose();

                                    }



                                }


                                if (NotificationInfo != null)
                                {
                                    NotificationInfo.Progress = (int)(((float)prgCount / (float)imgCount) * (float)100);
                                    NotificationInfo.Title = prgCount + " out of " + imgCount + " (Current Batch: " + curiter + "/20) ";
                                    
                                    NotificationInfo.Update(prgCount + " out of " + imgCount + " (Current Batch: " + curiter + "/20) " + "Requesting Image Token for " + imageFileName);
                                }

                                String ImageTokeResponse = "";
                                
                                try
                                {
                                    ImageTokeResponse = RequestImageToken(itrequestUri + imageFileName, custId, custPw, "");
                                    logger.Info("ImageTokenResponse - " + ImageTokeResponse);
                                }
                                catch
                                {
                                    logger.Info("failed to get ImageTokenResponse - trying 10 times");
                                    //try 10 times to get image token
                                    int tryLoop = 0;
                                    while (++tryLoop <= 10)
                                    {
                                        try
                                        {
                                            NotificationInfo.Update(prgCount + " out of " + imgCount + " (Current Batch: " + curiter + "/20) " + "FAILED TO GET IMAGE TOKEN, Trying Again.... " + tryLoop + " ....");
                                            ImageTokeResponse = RequestImageToken(itrequestUri + imageFileName, custId, custPw, "");
                                            break;
                                        }
                                        catch
                                        {

                                            if (tryLoop == 10)
                                            {
                                                //this._projectGallery.EventID = OriginalEventID;
                                                NotificationInfo.Update(prgCount + " out of " + imgCount + " (Current Batch: " + curiter + "/20) " + "FAILED TO GET IMAGE TOKEN, ABORTING....");
                                                NotificationInfo.ShowDoneButton();
                                                return false;
                                            }
                                            continue;
                                        }
                                    }

                                    //NotificationInfo.Update("FAILED TO GET IMAGE TOKEN, ABORTING....");
                                    //this._projectGallery.EventID = null;
                                    //NotificationInfo.ShowDoneButton();
                                    //return;
                                }

                                JsonParser parser2 = new JsonParser(new StringReader(ImageTokeResponse), true);
                                NetServ.Net.Json.JsonParser.TokenType nextToken2 = parser2.NextToken();
                                JsonObject rootObject2 = parser2.ParseObject();

                                bool FailedImageUpload = false;


                                //origBmp.Save(Path.Combine(FlowContext.FlowTempDirPath, "trash.png"));

                                NotificationInfo.Update(prgCount + " out of " + imgCount + " (Current Batch: " + curiter + "/20) " + "Uploading Thumbnail Image: " + imageFileName);
                                int trycount = 0;
                                int maxtrys = 5;
                                while (trycount < maxtrys)
                                {
                                    trycount++;
                                    try
                                    {
                                        FailedImageUpload = false;
                                        if (UploadImage((JsonObject)rootObject2["thumb-data"], custId, custPw, si.Orientation, origBmp, isGreenScreen && !string.IsNullOrEmpty(si.GreenScreenSettings)) != HttpStatusCode.OK)
                                        {
                                            //Error, did not upload
                                            FailedImageUpload = true;
                                            throw new Exception("FAILED TO UPLOAD IMAGE Thumbnail");
                                        }

                                        NotificationInfo.Update(prgCount + " out of " + imgCount + " (Current Batch: " + curiter + "/20) " + "Uploading Primary Image: " + imageFileName);
                                        if (UploadImage(rootObject2, custId, custPw, si.Orientation, origBmp, isGreenScreen && !string.IsNullOrEmpty(si.GreenScreenSettings)) != HttpStatusCode.OK)
                                        {
                                            //Error, did not upload
                                            FailedImageUpload = true;
                                            throw new Exception("FAILED TO UPLOAD IMAGE full size");
                                        }
                                        if (!FailedImageUpload)
                                        {
                                            
                                            this._flowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                                            {
                                                si.ExistsInOnlineGallery = true;
                                                sub.FlagForMerge = true;
                                                this._flowProject.UpdateGalleryCounts();
                                            }));
                                        }
                                        //if it made it here, we are good
                                        trycount = maxtrys + 1;

                                    }
                                    catch (Exception e)
                                    {
                                        logger.ErrorException("Exception occured while trying to upload image.", e);

                                        //this._projectGallery.EventID = OriginalEventID;
                                        if (trycount == maxtrys)
                                        {
                                            origBmp.Dispose();
                                            NotificationInfo.Update(prgCount + " out of " + imgCount + " (Current Batch: " + curiter + "/20) " + "FAILED TO UPLOAD IMAGE");
                                            logger.Error("Failed to upload image, giving up after " + trycount + " trys : " + imageFileName);
                                            NotificationInfo.ShowDoneButton();
                                            return false;
                                        }
                                        else
                                        {
                                            Thread.Sleep(3000);
                                            NotificationInfo.Update(prgCount + " out of " + imgCount + " (Current Batch: " + curiter + "/20) " + "Failed to upload image, trying again (" + trycount + "): " + imageFileName);
                                            logger.Info("Failed to upload image, trying again (" + trycount + "): " + imageFileName);
                                        }
                                    }
                                }


                                NotificationInfo.Update(prgCount + " out of " + imgCount + " (Current Batch: " + curiter + "/20) " + "Dispose of Image from memory: " + imageFileName);
                                origBmp.Dispose();

                                NotificationInfo.Update(prgCount + " out of " + imgCount + " (Current Batch: " + curiter + "/20) " + "Update Gallery Counts: " + imageFileName);
                                //this._flowProject.UpdateGalleryCounts();
                            }

                            
                        }
                        if (hasmissing)
                            curiter++;

                        NotificationInfo.Update(prgCount + " out of " + imgCount + " (Current Batch: " + curiter + "/20) " + "Submit Changes: ");
                        this._flowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                        {
                            this._flowProject.FlowProjectDataContext.SubmitChanges();
                        }));
                    }
                }
                //
                // end send image
                //

                if (NotificationInfo != null)
                {
                    NotificationInfo.Update("Submitting Gallery Definition");
                }

                // Create Pending Event (receive back welcome image token)
                String Response = "";
                bool UpdateExistingFailed = false;
                if (!isNewGallery)
                {
                    try
                    {

                        try
                        {
                            JsonWriter writerImgs = new JsonWriter();
                            updatedImagesJson.Write(writerImgs);
                            string ImageUpdateListPostData = writerImgs.ToString().Replace("\\/", "/");
                            WebHeaderCollection updateListReturnHeader = new WebHeaderCollection();
                            logger.Info("about to post gallery json");
                            string ResponseUpdateImgList = PostPendingEvent(updateImageCacheUri, custId, custPw, ImageUpdateListPostData, true, out updateListReturnHeader);
                        }
                        catch (Exception ex)
                        {
                            //ignore exception
                        }
                        Response = PostPendingEvent(postUpdateEventUri, custId, custPw, postData, true, out returnHeader);
                    }
                    catch (Exception e)
                    {
                        //UpdateExistingFailed = true;

                        NotificationInfo.Update("FAILED TO UPDATE GALLERY");
                        NotificationInfo.ShowDoneButton();
                        if (!checkAndRepairMissingImages(e.Message))
                        {
                            logger.Info(e.Message);
                            this._flowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                            {

                                FlowMessageBox msg = new FlowMessageBox("Failed To Upload", "FAILED TO UPDATE THIS GALLERY.\n\n" + e.Message);
                                msg.CancelButtonVisible = false;
                                msg.ShowDialog();
                            }));
                            
                           
                            
                        }

                        return false;
                    }
                }

                if (isNewGallery || UpdateExistingFailed)
                {
                    try
                    {
                        Response = PostPendingEvent(postNewEventUri, custId, custPw, postData, false, out returnHeader);

                    }
                    catch (Exception e)
                    {
                        //if (e is WebException)
                        //{
                        //    MessageBox.Show("Yahooo.... " + ((HttpWebResponse)((e) as WebException).Response).StatusDescription);
                        //}
                        //else
                        //    MessageBox.Show("dang..... " + e.Message);

                        //this._projectGallery.EventID = OriginalEventID;
                       
                        //foreach(string thisOne in returnHeader.AllKeys)
                        //{
                        //    MessageBox.Show(returnHeader.);
                        //}
                        //MessageBox.Show("Response: \n\n");

                        NotificationInfo.Update("FAILED TO UPDATE GALLERY");
                        NotificationInfo.ShowDoneButton();

                        if (!checkAndRepairMissingImages(e.Message))
                        {
                            

                            this._flowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                            {

                                FlowMessageBox msg = new FlowMessageBox("Failed To Upload", "FAILED TO PUBLISH THIS GALLERY.\n\n" + e.Message);
                                msg.CancelButtonVisible = false;
                                msg.ShowDialog();
                            }));
                            
                            
                        }
                        return false;
                    }
                }

                if (string.IsNullOrEmpty(_projectGallery.GalleryCode))
                {

                    if (returnHeader.AllKeys.Contains("x-iq-gallery-code"))
                    {
                        _projectGallery.GalleryCode = returnHeader["x-iq-gallery-code"];
                        _projectGallery.GalleryURL = "https://vando.imagequix.com/g" + _projectGallery.GalleryCode;
                        // string url = returnHeader["x-iq-gallery-url"];
                        this._flowMasterDataContext.RefreshGalleryList();
                    }
                }

                _projectGallery.LastModifyDate = DateTime.Now;
                this._flowMasterDataContext.SubmitChanges();
                this._flowProject.FlowProjectDataContext.SubmitChanges();
                
            } 
            //
            //DONE WITH Batch LOOP
            //



            //DONE????
            //if (NotificationInfo != null)
            //{
            //    NotificationInfo.Update("Activating Event");
            //}
            //// activate event
            //string activateUri = mainUri + "/" + custId + "/event/" + _projectGallery.EventID + "/status/active";
            //HttpStatusCode returnCode = PostToUri(activateUri, custId, custPw);
            //if (returnCode != HttpStatusCode.OK)
            //{
            //    if (returnCode == HttpStatusCode.NotModified)
            //    {
            //        NotificationInfo.Update("No Changes Made To Gallery");
            //        NotificationInfo.Complete("No Changes Made To Gallery");
            //        return;
            //    }
            //    //Error Connection - maybe not authorized?
            //    NotificationInfo.Update("Error Activating Event");
            //    NotificationInfo.Complete("Error Activating Event");
            //    return;
            //}
            if (!isNewGallery && NotificationInfo != null)
            {
                NotificationInfo.Update("Done Publishing Gallery");
                NotificationInfo.Complete("Done Publishing Gallery");
            }

            //_flowProject.GalleryLiveImageCount;
            _flowProject.UpdateGalleryCounts();

            if (isNewGallery)
            {
                NotificationInfo.Update("Done Creating Gallery");
                return true;
            }

            if (MissingImages.Count > 0)
            {
                string missing = "";
                foreach (string si in MissingImages)
                    missing += si + "\n";
                this._flowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                            {
                                FlowMessageBox msg = new FlowMessageBox("Missing Images", "This Gallery Has Missing Images: .\n\n" + missing);
                                msg.CancelButtonVisible = false;
                                msg.ShowDialog();
                            }));
            }

            if (NotificationInfo != null)
            {
                NotificationInfo.Complete("Done Publishing Gallery");
            }

            return false;
        }

        //private bool checkAndRepairMissingImages(string p)
        //{
        //    if (p.Contains("Some of the images that should be included as part of the gallery are missing"))
        //    {

        //        string filesDirty = p.Split("customMessage")[1];
        //        string filesClean = filesDirty.Replace("<", "").Replace(">", "").Replace("\"", "").Replace("\\", "").Replace("/", "").Replace("[", "").Replace("]", "");
        //        logger.Error("Missing Image: " + filesClean);
        //        int countFound = 0;
        //        int countFlagged = 0;
        //        foreach (string fileName in filesClean.Split(","))
        //        {
        //            countFound++;
        //            SubjectImage foundImage = this._flowProject.FlowProjectDataContext.SubjectImages.FirstOrDefault(si => si.ImageFileName == fileName || si.ImageFileName == fileName.Replace(".png", ".JPG"));
        //            if (foundImage != null)
        //            {
        //                countFlagged++;
        //                this._flowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
        //                {
        //                    foundImage.ExistsInOnlineGallery = false;
        //                    foundImage.Subject.FlagForMerge = true;
        //                }));
        //            }
        //        }
        //        this._flowProject.UpdateGalleryCounts();
        //        this._flowProject.FlowProjectDataContext.SubmitChanges();
        //        this._flowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
        //        {

        //            FlowMessageBox msg = new FlowMessageBox("Failed To Upload", "This Gallery contained missing images.\n" + countFlagged + " out of " + countFound + " Images have been flagged for upload again\n\nPlease re-upload this gallery");
        //            msg.CancelButtonVisible = false;
        //            msg.ShowDialog();
        //        }));
        //        return true;
        //    }
        //    else
        //        return false;


        //}

        private bool checkAndRepairMissingImages(string p)
        {
            if (p.Contains("Some of the images that should be included as part of the gallery are missing"))
            {

                string filesDirty = p.Split("customMessage")[1];
                string filesClean = filesDirty.Replace("<", "").Replace(">", "").Replace("\"", "").Replace("\\", "").Replace("/", "").Replace("[", "").Replace("]", "");
                logger.Error("Missing Image: " + filesClean);
                int countFound = 0;
                int countFlagged = 0;
                foreach (string fileName in filesClean.Split(","))
                {
                    logger.Info("fixing a file: " + fileName);
                    countFound++;
                    this._flowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                    {
                        SubjectImage foundImage = this._flowProject.FlowProjectDataContext.SubjectImages.FirstOrDefault(si => si.ImageFileName == fileName || si.ImageFileName.ToLower() == fileName.Replace(".png", ".JPG").ToLower());
                        if (foundImage != null)
                        {
                            countFlagged++;

                            foundImage.ExistsInOnlineGallery = false;
                            foundImage.Subject.FlagForMerge = true;

                        }
                    }));

                    
                    logger.Info("fixed a file: " + fileName);
                    
                }
                this._flowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                {
                    this._flowProject.UpdateGalleryCounts();
                    this._flowProject.FlowProjectDataContext.SubmitChanges();

                    FlowMessageBox msg = new FlowMessageBox("Failed To Upload", "This Gallery contained missing images.\n" + countFlagged + " out of " + countFound + " Images have been flagged for upload again\n\nPlease re-upload this gallery");
                    msg.CancelButtonVisible = false;
                    msg.ShowDialog();
                }));
                return true;
            }
            else
                return false;


        }

        private string GetJson(string uri, string custId, string custPw, string postData)
        {
            logger.Info("e " + uri);
            HttpWebRequest rq = (HttpWebRequest)WebRequest.Create(uri);
            rq.Credentials = new NetworkCredential(custId, custPw);
            rq.Headers.Add("IQ-TOKEN", "Flow");
            rq.Headers.Add("x-iq-token", "A15M876TJ8");
            rq.Headers.Add("x-iq-user", custId);
            rq.Timeout = (15000);//try for 15 seconds
            rq.Method = "GET";

            HttpWebResponse rs = null;
            int tryCount = 0;
            while (tryCount < 5)
            {
                try
                {
                    rs = (HttpWebResponse)rq.GetResponse();
                    StreamReader reader = new StreamReader(rs.GetResponseStream());
                    string jsonText = reader.ReadToEnd();

                    reader.Close();

                    return jsonText;
                }
                catch (WebException e)
                {

                    tryCount++;

                }
            }

            return null;
        }

        private string RequestImageToken(string uri, string custId, string custPw, string postData)
        {
            logger.Info("f " + uri);
            HttpWebRequest rq = (HttpWebRequest)WebRequest.Create(uri);
            rq.Credentials = new NetworkCredential(custId, custPw);
            rq.Headers.Add("IQ-TOKEN", "Flow");
            rq.Headers.Add("x-iq-token", "A15M876TJ8");
            rq.Headers.Add("x-iq-user", custId);
            rq.Timeout = ((60 * 5) * 1000);//try for 5 minutes (60 seconds * 5 minutes * 1000 = miliseconds)
            rq.Method = "POST";
            rq.ContentType = "application/json";

            rq.ContentLength = postData.Length;


            StreamWriter stOut = new
            StreamWriter(rq.GetRequestStream(),
            System.Text.Encoding.ASCII);
            stOut.Write(postData);
            stOut.Close();

            HttpWebResponse rs = null;
            int tryCount = 0;
            while (tryCount < 21)
            {
                try
                {
                    rs = (HttpWebResponse)rq.GetResponse();
                    break;
                }
                catch (WebException e)
                {
                    tryCount++;
                    if(tryCount == 21)
                        throw e;
                    Thread.Sleep(5000);
                }
            }

            //Stream s = rs.GetResponseStream();
            StreamReader reader = new StreamReader(rs.GetResponseStream());
            string jsonText = reader.ReadToEnd();

            reader.Close();

            return jsonText;
        }



        private HttpStatusCode UploadImage(JsonObject rootObject, string username, string password, int rotation, Image origBmp, bool isGreenScreen)
        {
            string url = rootObject["upload-url"].ToString();
            logger.Info("uploading to this url: " + url);
            int imgSize = 1200;
            if(rootObject.ContainsKey("size"))
                imgSize = (int)(JsonNumber)rootObject["size"];
            
            JsonObject httpHeaders = (JsonObject)rootObject["http-headers"];
            Dictionary<string, string> headerList = new Dictionary<string, string>();
            if(httpHeaders.Keys.Contains("x-amz-acl"))
                headerList.Add("x-amz-acl", httpHeaders["x-amz-acl"].ToString());
            headerList.Add("Authorization", httpHeaders["Authorization"].ToString());
            headerList.Add("x-amz-storage-class", httpHeaders["x-amz-storage-class"].ToString());
            headerList.Add("Date", httpHeaders["Date"].ToString());
            //headerList.Add("Expires", httpHeaders["Expires"].ToString());
            headerList.Add("Content-Type", httpHeaders["Content-Type"].ToString());


            logger.Info("about to make upload call");
            logger.Info("g " + url);
            HttpWebRequest rq = (HttpWebRequest)WebRequest.Create(url);
            rq.Credentials = new NetworkCredential(username, password);
            rq.Headers.Add("IQ-TOKEN", "Flow");
            rq.Headers.Add("x-iq-token", "A15M876TJ8");
            rq.Headers.Add("x-iq-user", username);
            rq.Timeout = ((60 * 3) * 1000);//try for 3 minutes (60 seconds * 5 minutes * 1000 = miliseconds)
            rq.Method = "PUT";
            MethodInfo priMethod = rq.Headers.GetType().GetMethod("AddWithoutValidate", BindingFlags.Instance | BindingFlags.NonPublic);
            priMethod.Invoke(rq.Headers, new[] { "Date", headerList["Date"] });
            rq.ContentType = headerList["Content-Type"];
            foreach (string key in headerList.Keys)
            {
                try
                {
                    //if(key == "Date")
                    //    rq.Headers.Add("x-amz-date", headerList[key]);
                    //else

                    //Date and Content-Type should exist, so update them
                    //the others are new, so add them
                    if (key == "Date")
                    {
                        //do noting Date is protected
                    }
                    else if (key == "Content-Type")
                        rq.ContentType = headerList["Content-Type"];
                    else
                        rq.Headers.Add(key, headerList[key]);
                }
                catch { }
            }
            string currentMessage = NotificationInfo.Message;
            //NotificationInfo.Update(currentMessage + " resize image");
            System.Drawing.Image bmp = resizeImage(origBmp, imgSize);



           // NotificationInfo.Update(currentMessage + " saving to memory stream");
            Stream ms = new MemoryStream();
            if (isGreenScreen)
            {
                bmp.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                //bmp.Save(si.GetDropoutImagePath(false));
            }
            else
                bmp.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);


            rq.ContentLength = ms.Length;
            
            Stream requestStream = rq.GetRequestStream();
            


            byte[] buffer = ReadToEnd(ms);
           // NotificationInfo.Update(currentMessage + " writing image to server");
            requestStream.Write(buffer, 0, (int)ms.Length);

            requestStream.Flush();
            HttpWebResponse rs;
            
            try
            {
                //NotificationInfo.Update(currentMessage + " getting response");
                rs = (HttpWebResponse)rq.GetResponse();
            }
            catch (Exception e)
            {

                throw e;
            }
            logger.Info("image upload response status: " + rs.StatusCode + " - " + rs.StatusDescription);
            //Stream s = rs.GetResponseStream();
            //NotificationInfo.Update(currentMessage + " reading response");
            StreamReader reader = new StreamReader(rs.GetResponseStream());
            string jsonText = reader.ReadToEnd();
            logger.Info("image upload response text: " + jsonText);
           // NotificationInfo.Update(currentMessage + " closing reader");
            reader.Close();
            bmp.Dispose();
           // NotificationInfo.Update(currentMessage + " done uploading");
            return rs.StatusCode;


        }

        private HttpStatusCode UploadImage(JsonObject rootObject, string pathToImage, string username, string password, int rotation)
        {
            string url = rootObject["upload-url"].ToString();
            int imgSize = 1200;
            if (rootObject.ContainsKey("size"))
                imgSize = (int)(JsonNumber)rootObject["size"];

            JsonObject httpHeaders = (JsonObject)rootObject["http-headers"];
            Dictionary<string, string> headerList = new Dictionary<string, string>();
            if (httpHeaders.Keys.Contains("x-amz-acl"))
                headerList.Add("x-amz-acl", httpHeaders["x-amz-acl"].ToString());
            headerList.Add("Authorization", httpHeaders["Authorization"].ToString());
            headerList.Add("x-amz-storage-class", httpHeaders["x-amz-storage-class"].ToString());
            headerList.Add("Date", httpHeaders["Date"].ToString());
            //headerList.Add("Expires", httpHeaders["Expires"].ToString());
            headerList.Add("Content-Type", httpHeaders["Content-Type"].ToString());
            logger.Info("h " + url);
            HttpWebRequest rq = (HttpWebRequest)WebRequest.Create(url);
            rq.Credentials = new NetworkCredential(username, password);
            rq.Headers.Add("IQ-TOKEN", "Flow");
            rq.Headers.Add("x-iq-token", "A15M876TJ8");
            rq.Headers.Add("x-iq-user", username);
            rq.Timeout = ((60 * 1) * 1000);//try for 1 minutes (60 seconds * 5 minutes * 1000 = miliseconds)
            rq.Method = "PUT";
            MethodInfo priMethod = rq.Headers.GetType().GetMethod("AddWithoutValidate", BindingFlags.Instance | BindingFlags.NonPublic);
            priMethod.Invoke(rq.Headers, new[] { "Date", headerList["Date"] });
            rq.ContentType = headerList["Content-Type"];
            foreach (string key in headerList.Keys)
            {
                try
                {
                    //if(key == "Date")
                    //    rq.Headers.Add("x-amz-date", headerList[key]);
                    //else
                    if (key == "Date")
                    {
                        //do noting Date is protected
                    }
                    else if (key == "Content-Type")
                        rq.ContentType = headerList["Content-Type"];
                    else
                        rq.Headers.Add(key, headerList[key]);
                }
                catch { }
            }




            System.Drawing.Image bmp = resizeImage((Image)(new Bitmap(pathToImage)), imgSize);
            if (rotation == 90)
                bmp.RotateFlip(RotateFlipType.Rotate90FlipNone);
            else if (rotation == 180)
                bmp.RotateFlip(RotateFlipType.Rotate180FlipNone);
            else if (rotation == 270)
                bmp.RotateFlip(RotateFlipType.Rotate270FlipNone);

            Stream ms = new MemoryStream();
            if (pathToImage.ToLower().EndsWith(".png"))
                bmp.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
            else
                bmp.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);


            rq.ContentLength = ms.Length;
            Stream requestStream = rq.GetRequestStream();



            byte[] buffer = ReadToEnd(ms);

            requestStream.Write(buffer, 0, (int)ms.Length);

            requestStream.Flush();
            HttpWebResponse rs;
            try
            {
                rs = (HttpWebResponse)rq.GetResponse();
            }
            catch (Exception e)
            {

                throw e;
            }

            //Stream s = rs.GetResponseStream();
            StreamReader reader = new StreamReader(rs.GetResponseStream());
            string jsonText = reader.ReadToEnd();
            
            reader.Close();
            bmp.Dispose();
            return rs.StatusCode;


        }


        private JsonObject GenerateGalleryPriceSheetJson(string pricesheetID, bool isGreenScreen)
        {
            logger.Info("Begin Generate Pricesheet");
            JsonObject pricesheet = new JsonObject();
            

            JsonArray categories = new JsonArray();
            pricesheet.Add("categories", categories);

            if(string.IsNullOrEmpty(pricesheetID))
                pricesheet.Add("id", 0);
            else
                pricesheet.Add("id", Convert.ToInt32(pricesheetID));

            pricesheet.Add("name", this._flowProject.FlowCatalog.FlowCatalogDesc);
            //pricesheet.Add("lab-id", this._flowProject.FlowCatalog.FlowCatalogID);
            pricesheet.Add("lab-id",1);
            pricesheet.Add("last-modified", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));

            pricesheet.Add("orderfulfillment-type", "full-control");
            pricesheet.Add("type", "standard");
            pricesheet.Add("version", 2);

            //
            //PACKAGES
            //
            try
            {
                JsonObject packages = new JsonObject();
                categories.Add(packages);
                //packages.Add("id", this._flowProject.FlowCatalog.FlowCatalogID);
                packages.Add("name", "Packages");
                //packages.Add("lab-id", this._flowProject.FlowCatalog.FlowCatalogID);
                packages.Add("required", "none");

                JsonArray products = new JsonArray();
                //packages.Add("products", products);
                logger.Info("Pricesheet - Found " + _flowProject.FlowCatalog.ProductPackages.Count() + " Packages");

                if (existingPSJson != null)
                {
                    JsonArray cats1 = existingPSJson["categories"] as JsonArray;
                    foreach (JsonObject cat in cats1)
                    {
                        if (cat.ContainsKey("required") && (cat["required"].JsonTypeCode != JsonTypeCode.Null))
                        {
                            packages.Remove("required");
                            packages.Add("required", cat["required"]);
                        }


                        if (cat.ContainsKey("min-order-amount") && (cat["min-order-amount"].JsonTypeCode != JsonTypeCode.Null))
                        {
                            packages.Remove("min-order-amount");
                            packages.Add("min-order-amount", ((JsonNumber)cat["min-order-amount"]));
                        }
                    }
                }
                foreach (ProductPackage pp in _flowProject.FlowCatalog.ProductPackages)
                {
                    logger.Info("Pricesheet - Adding Package: " + pp.ProductPackageDesc);

                    if (!pp.ShowInOnlinePricesheet)
                    {
                        logger.Info("Pricesheet - Adding Package: " + pp.ProductPackageDesc + "DO NOT SHOW IN PRICESHEET");
                        continue;
                    }

                    JsonObject existingPkg = null;
                    if (existingPSJson != null )
                    {
                        JsonArray cats = existingPSJson["categories"] as JsonArray;
                        foreach (JsonObject cat in cats)
                        {
                         
                            JsonArray pkgs = cat["products"] as JsonArray;
                            foreach (JsonObject pkg in pkgs)
                            {
                                if (pkg["id"] as JsonNumber == pp.ProductPackageID)
                                {
                                    existingPkg = pkg;
                                    break;
                                }
                            }

                        }
                    }
                    //foreach package
                    JsonObject package = new JsonObject();

                    logger.Info("Pricesheet - " + pp.ProductPackageDesc + " : starting package");
                    package.Add("description", pp.ProductPackageDesc);
                    package.Add("type", "package");
                    package.Add("lab-product-id", pp.ProductPackageID);
                    package.Add("id", pp.ProductPackageID);
                    package.Add("price", (double)pp.Price);

                    logger.Info("Pricesheet - " + pp.ProductPackageDesc + " : look for existing product image");
                    if(existingPkg != null && existingPkg["product-image"] != null)
                        package.Add("product-image", existingPkg["product-image"]);
                    else
                        package.Add("product-image",JsonNull.Null);

                    package.Add("ref-id", pp.ProductPackageID);
                    package.Add("name", pp.ProductPackageDesc);
                    if ((bool)_projectGallery.OneImagePerPackage)
                    {
                        package.Add("allow-additional-images", false);
                        package.Add("included-images", 1);
                    }
                    else
                    {
                        package.Add("allow-additional-images", true);
                        package.Add("included-images", 1);
                    }

                    package.Add("multi-image-surcharge", 0.0);
                    package.Add("multi-image-surcharge-type", "noSurcharge");
                    JsonArray packageOptionGroups = new JsonArray();
                   
                    package.Add("option-groups", packageOptionGroups);

                    
                    JsonArray packageProducts = new JsonArray();
                    //package.Add("products", packageProducts);

                    
                    if (pp.DigitalDownload != null && pp.DigitalDownload != "None" && pp.DigitalDownload.Length > 0)
                    {
                        logger.Info("Pricesheet - " + pp.ProductPackageDesc + " : adding digital download");
                        JsonObject product = new JsonObject();
                        packageProducts.Add(product);
                        product.Add("type", "image-download");
                        product.Add("lab-product-id", 1);
                        product.Add("id", TicketGenerator.GenerateNumericTicketString(5));
                        product.Add("price", 0);
                        product.Add("product-image", JsonNull.Null);
                        product.Add("ref-id", JsonNull.Null);
                        product.Add("description", "Image Download (" + pp.DigitalDownload + ")");

                        product.Add("name", "Image Download (" + pp.DigitalDownload + ")");
                        JsonArray productOptions = new JsonArray();
                        product.Add("option-groups", productOptions);
                       
                        product.Add("package-units", 0);
                        product.Add("min-images", 0);
                        product.Add("max-images", 0);
                        if (pp.DigitalDownload.StartsWith("400"))
                        {
                            product.Add("iq-id", 501);
                            product.Add("size", "400");
                        }
                        else if (pp.DigitalDownload.StartsWith("800"))
                        {
                            product.Add("iq-id", 502);
                            product.Add("size", "800");
                        }
                        else if (pp.DigitalDownload.StartsWith("1200"))
                        {
                            product.Add("iq-id", 503);
                            product.Add("size", "1200");
                        }

                        product.Add("iq-price", 1);
                        
                        product.Add("full-resolution", false);
                        product.Add("download-type", "single");
                        product.Add("is-customizable", false);
                    }

                        foreach (ProductPackageComposition ppc in pp.ProductPackageCompositions)
                        {
                            logger.Info("Pricesheet - " + pp.ProductPackageDesc + " : adding product");
                            ImageQuixProduct IQProduct = ppc.ImageQuixProduct;

                            logger.Info("Pricesheet - " + ppc.ImageQuixProduct.Label + " : checking for existing product images");
                            JsonObject existingPrd = null;
                            try
                            {
                                if (existingPkg != null)
                                {
                                    foreach (JsonObject prd in existingPkg["products"] as JsonArray)
                                    {
                                        if (prd["ref-id"] as JsonString == IQProduct.PrimaryKey.ToString() && prd["description"] as JsonString == ppc.DisplayLabel)
                                        {
                                            existingPrd = prd;
                                            break;
                                        }
                                    }
                                }
                            }
                            catch (Exception exc)
                            {
                                //if this fails, dont sink the ship, just ignore the existing product image
                            }


                            //foreach product in package
                            logger.Info("Pricesheet - " + ppc.ImageQuixProduct.Label + " : create actual product object");
                            JsonObject product = new JsonObject();
                            packageProducts.Add(product);
                            product.Add("type", "product");
                            product.Add("lab-product-id", IQProduct.PrimaryKey);
                            //product.Add("lab-product-id", IQProduct.ImageQuixProductID);
                            //product.Add("lab-product-id", TicketGenerator.GenerateNumericTicketString(5));
                            //product.Add("id", IQProduct.PrimaryKey);
                            product.Add("id", TicketGenerator.GenerateNumericTicketString(5));
                            product.Add("height", (double)IQProduct.Height);
                            product.Add("price", 0);

                            logger.Info("Pricesheet - " + ppc.ImageQuixProduct.Label + " : apply existing product images (if exist)");

                            if (existingPrd != null && existingPrd["product-image"] != null)
                                product.Add("product-image", existingPrd["product-image"]);
                            else
                                product.Add("product-image", JsonNull.Null);

                            logger.Info("Pricesheet - " + ppc.ImageQuixProduct.Label + " : adding ref-id");
                            product.Add("ref-id", IQProduct.PrimaryKey);
                            //product.Add("description", IQProduct.Label);
                            product.Add("description", ppc.DisplayLabel);
                            product.Add("width", (double)IQProduct.Width);
                            product.Add("name", ppc.DisplayLabel + " x " + ppc.Quantity);

                            logger.Info("Pricesheet - " + ppc.ImageQuixProduct.Label + " : adding product Options");
                            JsonArray productOptions = new JsonArray();
                            //foreach (ImageQuixOptionGroup og in IQProduct.OptionGroupList)
                            //{
                            //    //foreach(ImageQuixOption op in og.OptionList)
                            //    //{
                            //    //foreach option
                            //    JsonObject productOption = new JsonObject();
                            //    productOption.Add("type", og.);
                            //    productOption.Add("id", (int)og.PrimaryKey);
                            //    productOption.Add("label", og.Label);
                            //    productOption.Add("required", og.IsMandatory);
                            //    productOption.Add("maxlength", 100);
                            //    productOption.Add("multiline", false);
                            //    productOption.Add("value", "");
                            //    productOptions.Add(productOption);
                            //    //end foreach option
                            //    //}
                            //}
                            product.Add("option-groups", productOptions);
                            product.Add("is-customizable", true);

                            //packageProducts.Add(product);
                            //end foreach product in package
                        }
                        logger.Info("Pricesheet - " + pp.ProductPackageDesc + " : adding product list to package");
                    package.Add("products", packageProducts);
                    package.Add("is-customizable", false);
                    logger.Info("Pricesheet - " + pp.ProductPackageDesc + " : adding package to the list of packages");
                    products.Add(package);
                    //end foreach package
                }
                logger.Info("Pricesheet - adding list of packages to the main product node");
                packages.Add("products", products);
                //pricesheet.Add("last-modified", DateTime.Today.ToString("yyyyMMdd"));
                //categories.Add("pricesheet", pricesheet);
            }
            catch(Exception ex)
            {
                string a = ex.Message;
                logger.Error("Pricesheet - ERROR - " + a);
                
                FlowMessageBox msgb = new FlowMessageBox("Problem With PriceSheet", a);
                msgb.CancelButtonVisible = false;
                msgb.ShowDialog();
            }

            //
            //BACKGROUNDS
            //
            
            if(isGreenScreen){
                logger.Info("Pricesheet - Adding Backgrounds now");
                JsonObject backgrounds = new JsonObject();
                categories.Add(backgrounds);
                backgrounds.Add("name", "iq_background_category");
                
                JsonArray products = new JsonArray();
                backgrounds.Add("products", products);
                

                JsonObject product = new JsonObject();
                products.Add(product);
                JsonArray backgrounditems = new JsonArray();
                product.Add("backgrounds", backgrounditems);

                foreach (GreenScreenBackground gsb in _flowMasterDataContext.GreenScreenBackgrounds)
                {
                    if(!gsb.IsPremium)
                        backgrounditems.Add(gsb.FileName);
                    
                }
                product.Add("description", JsonNull.Null);
                product.Add("id", 31);
                product.Add("is-customizable",false);
                product.Add("lab-product-id",0);
                product.Add("name","Choose Your Background");
                product.Add("option-groups", new JsonArray());
                product.Add("price",0);
                product.Add("product-image", JsonNull.Null);
                product.Add("ref-id", JsonNull.Null);
                product.Add("type", "backgrounds");

                backgrounds.Add("required", "none");




                logger.Info("Pricesheet - Adding Premium Backgrounds now");
               
                JsonObject pproduct = new JsonObject();
                products.Add(pproduct);
                JsonArray pbackgrounditems = new JsonArray();
                pproduct.Add("backgrounds", pbackgrounditems);

                foreach (GreenScreenBackground gsb in _flowMasterDataContext.GreenScreenBackgrounds)
                {
                    if (gsb.IsPremium)
                        pbackgrounditems.Add(gsb.FileName);

                }
                pproduct.Add("description", JsonNull.Null);
                pproduct.Add("id", 32);
                pproduct.Add("is-customizable", false);
                pproduct.Add("lab-product-id", 0);
                pproduct.Add("name", "Choose Your Background");
                pproduct.Add("option-groups", new JsonArray());
                pproduct.Add("price", this._flowMasterDataContext.PreferenceManager.Edit.PremiumBackgroundPrice);
                pproduct.Add("product-image", JsonNull.Null);
                pproduct.Add("ref-id", JsonNull.Null);
                pproduct.Add("type", "backgrounds");

               
            }



            return pricesheet;
        }


        public JsonObject GenerateGalleryJson(string eventID, JsonObject galleryPricesheet, bool isGreenScreen, bool isNewGallery, List<Subject> CurrentSubjectBatch, JsonNumber existingGalleryCode, JsonNumber marketingCampaignId,  List<KeyValuePair<string, string>> GalleryCodes)
        {
            string studioName = ((Organization)this._flowMasterDataContext.Organizations.First(o => o.OrganizationID == this._flowProject.StudioID)).OrganizationName; 
            JsonObject root = new JsonObject();
            //if(allowYearbookSelection)
            //    root.Add("allow-yearbook-selection", true);
            //else
            //    root.Add("allow-yearbook-selection", false);
            
            root.Add("num-yearbook-selection", allowableYearbookSelections);
            root.Add("creationDate",JsonNull.Null);
            root.Add("customDataSpec", JsonNull.Null);
            root.Add("eventType", "subject");
            root.Add("eventID", eventID);
            root.Add("title", _projectGallery.GalleryName);
            
            if(_projectGallery.EventDate == null)
                root.Add("eventDate", JsonNull.Null);
            else
                root.Add("eventDate", ((DateTime)_projectGallery.EventDate).ToString("yyyyMMdd"));

            if (_projectGallery.ExpirationDate == null)
                root.Add("expirationDate", JsonNull.Null);
            else
                root.Add("expirationDate", ((DateTime)_projectGallery.ExpirationDate).ToString("yyyyMMdd"));


            
            root.Add("keyword", _projectGallery.Keyword);
            root.Add("eventPassword", _projectGallery.GalleryPassword);
            root.Add("gallery-code", existingGalleryCode);
            if(marketingCampaignId.Value > 0)
                root.Add("marketing-campaign-id", marketingCampaignId);
            root.Add("order-fulfillment", "full-control");
            //root.Add("directShip", false);
            root.Add("welcomeMessage", _projectGallery.WelcomeText);
            root.Add("isGreenScreen", isGreenScreen);
            root.Add("hiResImageLocationType", JsonNull.Null);

            root.Add("pricesheet-id", _projectGallery.PricesheetID);


            root.Add("requiresPasswordChange", false);

            if (File.Exists(_projectGallery.WelcomeImage))
                root.Add("welcomeImage", (new FileInfo(_projectGallery.WelcomeImage)).Name.Replace(" ", "%20"));
            else
                root.Add("welcomeImage", "welcome.jpg");
            root.Add("eventStatus", "draft");
            //root.Add("watermark", "© " + studioName);
            //root.Add("watermark-position", "bottom-right");
            root.Add("folder", eventID);
            root.Add("useGalleryConfig", true);

            //total hack... if we want IQ to replace cached images, we must change the watermark
            string wmarkText = this._projectGallery.WatermarkText;
            if (wmarkText.EndsWith(" "))
                wmarkText.TrimEnd();
            else
                wmarkText += " ";

            root.Add("watermark", wmarkText);
            root.Add("watermark-position", this._projectGallery.WatermarkLocation);

            JsonObject galleryConfig = new JsonObject();
            if(this._projectGallery.DirectShip == true)
                galleryConfig.Add("shipmentType", "direct");
            else if (this._projectGallery.PickupShip == true)
                galleryConfig.Add("shipmentType", "pickup");
            else if (this._projectGallery.CustomerChoiceShip == true)
                galleryConfig.Add("shipmentType", "choice");
            else 
                galleryConfig.Add("shipmentType", "pickup");


            if (SwitchShippingEnabled)
            {
                string startDateNow = ((DateTime)this._projectGallery.SwitchShippingDate).ToString("yyyyMMdd");//DateTime.Today.ToString("yyyyMMdd");
                galleryConfig.Add("shipStartDate", startDateNow);
            }
            else
                galleryConfig.Add("shipStartDate", null);

            galleryConfig.Add("shipping", Convert.ToDouble((decimal)this._projectGallery.ShippingCost));

            if(_projectGallery.HandlingRateType == "percent")
                galleryConfig.Add("handling", Convert.ToDouble((decimal)this._projectGallery.HandlingCost)/100);
            else
                galleryConfig.Add("handling", Convert.ToDouble((decimal)this._projectGallery.HandlingCost));
            galleryConfig.Add("tax", Convert.ToDouble((decimal)this._projectGallery.TaxRate)/100);
            galleryConfig.Add("tax-shipping", this._projectGallery.TaxShipping);
            galleryConfig.Add("show-bw-sepia", this._projectGallery.ShowBWSepia);
            //galleryConfig.Add("disable-full-control-cropping", this._projectGallery.DisableFullControlCropping);
            galleryConfig.Add("pickupLabel", this._projectGallery.PickupText);

            //new, are these required?
            galleryConfig.Add("default-show-image-names", _projectGallery.DefaultShowImageName);
            galleryConfig.Add("disable-croping", _projectGallery.DisableCropping);
            galleryConfig.Add("handling-label", _projectGallery.HandlingLabel);
            galleryConfig.Add("handling-rate-type", _projectGallery.HandlingRateType);

            if (_projectGallery.MinOrderAmount == null)
                galleryConfig.Add("min-order-amount", 1.00);
            else
                galleryConfig.Add("min-order-amount", Convert.ToDouble((decimal)_projectGallery.MinOrderAmount));

            galleryConfig.Add("order-thankyou-message", _projectGallery.OrderThankyouMessage);
            galleryConfig.Add("shipping-label", _projectGallery.ShippingLabel);
            galleryConfig.Add("tax-all-orders", _projectGallery.TaxAllOrders);
            galleryConfig.Add("tax-label", _projectGallery.TaxLabel);
            galleryConfig.Add("theme", _projectGallery.Theme);
            galleryConfig.Add("wallpaper", _projectGallery.Wallpaper);

            root.Add("galleryConfig", galleryConfig);

           
            //groups
            root.Add("groups", new JsonArray());
            
            //pricesheet
            root.Add("pricesheet", galleryPricesheet);

            //subjects
            JsonArray allSubjects = new JsonArray();
            if (!isNewGallery)
            {
                //foreach (Subject sub in this._flowProject.SubjectList.Where(s => s.HasAssignedImages))
                foreach (Subject sub in CurrentSubjectBatch)
                
                {
                    SubjectImage ybPose1 = null;
                    SubjectImage ybPose2 = null;

                    if (sub.SubjectImages.Any(si => si.SubjectImageTypeList.Any(sit => sit.ProjectImageType.ImageTypeDesc.ToLower() == "yearbook pose" && sit.IsAssigned == true)))
                        ybPose1 = sub.SubjectImages.First(si => si.SubjectImageTypeList.Any(sit => sit.ProjectImageType.ImageTypeDesc.ToLower() == "yearbook pose" && sit.IsAssigned == true));

                    if (sub.SubjectImages.Any(si => si.SubjectImageTypeList.Any(sit => sit.ProjectImageType.ImageTypeDesc.ToLower() == "yearbook pose 2" && sit.IsAssigned == true)))
                        ybPose2 = sub.SubjectImages.First(si => si.SubjectImageTypeList.Any(sit => sit.ProjectImageType.ImageTypeDesc.ToLower() == "yearbook pose 2" && sit.IsAssigned == true));

                    //foreach subject
                    JsonObject subject = new JsonObject();
                    subject.Add("name", sub.FormalFullName);
                    subject.Add("custom1", sub.TicketCode);

                    subject.Add("address", JsonNull.Null);
                    subject.Add("city", JsonNull.Null);
                    //subject.Add("custom1", JsonNull.Null);
                    subject.Add("custom2", JsonNull.Null);
                    subject.Add("email", JsonNull.Null);


                    if (GalleryCodes.Any(kvp => kvp.Key == sub.TicketCode))
                    {
                        KeyValuePair<string, string> subjectGalleryCode = GalleryCodes.FirstOrDefault(kvp => kvp.Key == sub.TicketCode);
                        subject.Add("gallery-code", subjectGalleryCode.Value);
                    }
                    else
                        subject.Add("gallery-code", JsonNull.Null);

                    subject.Add("group-name", JsonNull.Null);
                    subject.Add("id", JsonNull.Null);
                    subject.Add("phone", JsonNull.Null);
                    subject.Add("photo-safe-id", JsonNull.Null);
                    subject.Add("refNo", JsonNull.Null);
                    subject.Add("state", JsonNull.Null);
                    subject.Add("year", JsonNull.Null);

                    if(ybPose1 == null)
                        subject.Add("yearbook-selection-1", JsonNull.Null);
                    else
                        subject.Add("yearbook-selection-1", ybPose1.ImageFileName);

                    if (ybPose2 == null)
                        subject.Add("yearbook-selection-2", JsonNull.Null);
                    else
                        subject.Add("yearbook-selection-2", ybPose2.ImageFileName);

                    subject.Add("zip", JsonNull.Null);

                    //if they dont have a ticketId, create one
                    if (sub.TicketCode == null || (sub.TicketCode.Length == 0))
                    {
                        sub.TicketCode = TicketGenerator.GenerateTicketString(8);
                        sub.FlagForMerge = true;
                    }
                    else
                        subject.Add("code", sub.TicketCode);

                    JsonArray AllImages = new JsonArray();
                    foreach (SubjectImage si in sub.ImageList)
                    {
                        if (!File.Exists(si.ImagePathFullRes))
                            continue;

                        if (isGreenScreen && !string.IsNullOrEmpty(si.GreenScreenSettings))
                            AllImages.Add(si.ImageFileName.Replace(".jpg", ".png").Replace(".JPG", ".png"));
                        else
                            AllImages.Add(si.ImageFileName);

                    }
                    subject.Add("images", AllImages);
                    allSubjects.Add(subject);
                    //end foreach subject
                }
            }
            root.Add("subjects", allSubjects);

            return root;
        }

        public string PostPendingEvent(string uri, string username, string password, string postData, bool doPUT, out WebHeaderCollection headers)
        {
            logger.Info("i " + uri);
            HttpWebRequest rq = (HttpWebRequest)WebRequest.Create(uri);
            rq.Credentials = new NetworkCredential(username, password);
            rq.Headers.Add("IQ-TOKEN", "Flow");
            rq.Headers.Add("x-iq-token", "A15M876TJ8");
            rq.Headers.Add("x-iq-user", username);
            rq.Timeout = ((60 * 10) * 1000);//try for 5 minutes (60 seconds * 5 minutes * 1000 = miliseconds)
            if(doPUT)
                rq.Method = "PUT";
            else
                rq.Method = "POST";
            rq.ContentType = "application/json";

            rq.ContentLength = postData.Length;


            StreamWriter stOut = new
            StreamWriter(rq.GetRequestStream(),
            System.Text.Encoding.ASCII);
            stOut.Write(postData);
            stOut.Close();

            HttpWebResponse rs;
            try
            {
                rs = (HttpWebResponse)rq.GetResponse();
            }
            catch (Exception exc)
            {
                if (exc.GetType() == typeof(WebException))
                {
                    WebException ex = (WebException)exc;
                    string pageOutput = "";
                    if (ex.Response != null)
                    {
                        using (Stream stream = ex.Response.GetResponseStream())
                        {

                            using (StreamReader xreader = new StreamReader(stream))
                            {

                                pageOutput = xreader.ReadToEnd().Trim();
                            }
                        }
                    }
                    throw new Exception(((HttpWebResponse)ex.Response).StatusDescription + "\n\nDetails: " + pageOutput);
                }

                else if (exc.GetType() == typeof(TimeoutException))
                {
                    TimeoutException ex = (TimeoutException)exc;
                    logger.Info("Hit Timeout Exception");
                    throw ex;
                }

                else
                    throw exc;
            }
            
            
            headers = rs.Headers;

            //Stream s = rs.GetResponseStream();
            StreamReader reader = new StreamReader(rs.GetResponseStream());
            string jsonText = reader.ReadToEnd();

            reader.Close();

            return jsonText;
        }

        public string GetInstallID(string installCodeUri, string custId, string custPw)
        {

            logger.Info("j " + installCodeUri);
            HttpWebRequest rq = (HttpWebRequest)WebRequest.Create(installCodeUri);
            rq.Credentials = new NetworkCredential(custId, custPw);
            rq.Headers.Add("IQ-TOKEN", "Flow");
            rq.Headers.Add("x-iq-token", "A15M876TJ8");
            rq.Headers.Add("x-iq-user", custId);
            //rq.Headers.Add("x-iq-token", "A15M876TJ8");
            //rq.Headers.Add("x-iq-user", custId);
            rq.Method = "GET";

            rq.Timeout = 10000;//only try for 10 seconds
            HttpWebResponse rs;
            try
            {
                rs = (HttpWebResponse)rq.GetResponse();
            }
            catch (WebException e)
            {

                return "" ;
            }

            //Stream s = rs.GetResponseStream();
            StreamReader reader = new StreamReader(rs.GetResponseStream());
            string returnV = reader.ReadToEnd();

            reader.Close();
            return returnV;
        }

        public HttpStatusCode PostToUri(string loginUri, string un, string pw)
        {
            logger.Info("k " + loginUri);
            HttpWebRequest rq = (HttpWebRequest)WebRequest.Create(loginUri);
            rq.Credentials = new NetworkCredential(un, pw);
            rq.Headers.Add("IQ-TOKEN", "Flow");
            rq.Headers.Add("x-iq-token", "A15M876TJ8");
            rq.Headers.Add("x-iq-user", un);
            rq.Timeout = 30000;//only try for 30 seconds
            rq.Method = "PUT";
             HttpWebResponse rs;
            try
            {
               rs = (HttpWebResponse)rq.GetResponse();
            }
            catch (WebException e)
            {
                if(e.Message.Contains("304"))
                    return HttpStatusCode.NotModified;

                return HttpStatusCode.Unauthorized;
            }

            return rs.StatusCode;
           
        }

        private static Image resizeImage(Image imgToResize, int iSize)
        {
            
            int sourceWidth = imgToResize.Width;
            int sourceHeight = imgToResize.Height;
            System.Drawing.Size size = new System.Drawing.Size(iSize, iSize);
            if (sourceWidth > sourceHeight)
            {
                float multiplier = ((float)iSize / (float)sourceWidth);
                size.Width = iSize;
                size.Height = Convert.ToInt32((sourceHeight * multiplier));
            }
            if (sourceWidth < sourceHeight)
            {
                float mulitplier = ((float)iSize / (float)sourceHeight);
                size.Height = iSize;
                size.Width = Convert.ToInt32((sourceWidth * mulitplier));
            }
            float nPercent = 0;
            float nPercentW = 0;
            float nPercentH = 0;

            nPercentW = ((float)size.Width / (float)sourceWidth);
            nPercentH = ((float)size.Height / (float)sourceHeight);

            if (nPercentH < nPercentW)
                nPercent = nPercentH;
            else
                nPercent = nPercentW;

            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            Bitmap b = new Bitmap(destWidth, destHeight);
            Graphics g = Graphics.FromImage((Image)b);
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;

            g.DrawImage(imgToResize, 0, 0, destWidth, destHeight);
            g.Dispose();

            return (Image)b;
        }

        private static Int32Rect getCropArea(Bitmap iSrc, int rotation, double CropL, double CropT, double CropW, double CropH)
        {


            double width = iSrc.Width;
            double height = iSrc.Height;

            int cropAreaL = (int)(CropL * width);
            int cropAreaT = (int)(CropT * height);
            int cropAreaW = (int)(CropW * width);
            int cropAreaH = (int)(CropH * height);

            if (cropAreaL + cropAreaW > width)
                cropAreaW = (int)width - cropAreaL;
            if (cropAreaT + cropAreaH > height)
                cropAreaH = (int)height - cropAreaT;

            if (rotation == 90)
            {
                return new Int32Rect(
                    cropAreaT,
                    ((int)width - cropAreaL) - cropAreaW,
                    cropAreaH,
                    cropAreaW
                    );
            }

            if (rotation == 270)
            {
                return new Int32Rect(
                    ((int)height - cropAreaT) - cropAreaH,
                    cropAreaL,
                    cropAreaH,
                    cropAreaW
                    );
            }

            return new Int32Rect(
                cropAreaL,
                cropAreaT,
                cropAreaW,
                cropAreaH
                );
        }

        public static byte[] ReadToEnd(System.IO.Stream stream)
        {
            long originalPosition = stream.Position;
            stream.Position = 0;

            try
            {
                byte[] readBuffer = new byte[4096];

                int totalBytesRead = 0;
                int bytesRead;

                while ((bytesRead = stream.Read(readBuffer, totalBytesRead, readBuffer.Length - totalBytesRead)) > 0)
                {
                    totalBytesRead += bytesRead;

                    if (totalBytesRead == readBuffer.Length)
                    {
                        int nextByte = stream.ReadByte();
                        if (nextByte != -1)
                        {
                            byte[] temp = new byte[readBuffer.Length * 2];
                            Buffer.BlockCopy(readBuffer, 0, temp, 0, readBuffer.Length);
                            Buffer.SetByte(temp, totalBytesRead, (byte)nextByte);
                            readBuffer = temp;
                            totalBytesRead++;
                        }
                    }
                }

                byte[] buffer = readBuffer;
                if (readBuffer.Length != totalBytesRead)
                {
                    buffer = new byte[totalBytesRead];
                    Buffer.BlockCopy(readBuffer, 0, buffer, 0, totalBytesRead);
                }
                return buffer;
            }
            finally
            {
                stream.Position = originalPosition;
            }
        }
    }
}
