﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.Lib.Net;

namespace Flow.Controller.Project
{
    public class ProjectEmailer : Emailer
    {
        public ProjectEmailer(string subject, string body) : this(subject, body, true){}

        public ProjectEmailer(string subject, string body, bool showProgress)
            : base(subject, body, showProgress) { }

        public ProjectController ProjectController { get; set; }
    }
}
