﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.Schema.LinqModel.DataContext;
using System.Net;
using System.IO;
using NetServ.Net.Json;
using System.Windows.Media.Imaging;
using Flow.Lib.ImageUtils;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Reflection;
using Flow.Lib.Helpers;
using Flow.Lib;
using StructureMap;
using System.ComponentModel;
using Flow.Schema.LinqModel.DataContext.FlowMaster;
using System.Windows.Threading;
using System.Threading;
using Flow.View.Dialogs;
using NLog;

namespace Flow.Controller.Project
{

    public class PullOnlineOrder
    {
        private Logger logger = LogManager.GetCurrentClassLogger();

        private FlowProject _flowProject {get;set;}
        private FlowMasterDataContext _flowMasterDataContext {get; set;}
        private FlowController flowController { get; set; }
        public NotificationProgressInfo ProgressInfo { get; set; }


        public PullOnlineOrder(FlowMasterDataContext fmdc)
        {
            _flowMasterDataContext = fmdc;
        
        }

        public JsonArray GetGalleryStats(string statsURL, string galleryID)
        {
            logger.Info("Getting ImageQuix gallery stats");

            string custId = this._flowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixCustomerID;
            string custPw = this._flowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixCustomerPassword;
           
            String ImageTokeResponse = GetJson(statsURL, custId, custPw, "", galleryID);
            if (ImageTokeResponse == null)
                return null;
            JsonParser parser2 = new JsonParser(new StringReader(ImageTokeResponse), true);
            NetServ.Net.Json.JsonParser.TokenType nextToken2 = parser2.NextToken();
            JsonArray rootArray2 = parser2.ParseArray();
            //(JsonObject)rootObject2["thumb-data"]

            logger.Info("Done getting gallery stats");
            //if (ProgressInfo != null)
            //{
            //    ProgressInfo.Update("Done Getting Stats");
            //    ProgressInfo.Complete("Done Getting Stats");
            //}

            return rootArray2;
        }

        private string GetJson(string uri, string custId, string custPw, string postData, string galleryID)
        {
            logger.Info("OrderURL: " + uri);
            logger.Info(custId + " / " + custPw);
            logger.Info("Gallery: " + galleryID);

            logger.Info("b " + uri);
            HttpWebRequest rq = (HttpWebRequest)WebRequest.Create(uri);
            rq.Credentials = new NetworkCredential(custId, custPw);
            rq.Headers.Add("x-iq-token", "A15M876TJ8");
            rq.Headers.Add("x-iq-user", custId);
            rq.Headers.Add("x-iq-galleryid", galleryID);
            rq.Timeout = (15000);//try for 15 seconds
            rq.Method = "GET";

 
                try
                {
                    using (HttpWebResponse rs = (HttpWebResponse)rq.GetResponse())
                    {
                        StreamReader reader = new StreamReader(rs.GetResponseStream());
                        string jsonText = reader.ReadToEnd();

                        reader.Close();

                        rs.Close();
                        return jsonText;
                    }

                }
                catch (WebException e)
                {
                    return null;

                }

            return null;
        }

       
        private HttpStatusCode PostToUri(string loginUri, string un, string pw)
        {
            logger.Info("c " + loginUri);
            HttpWebRequest rq = (HttpWebRequest)WebRequest.Create(loginUri);
            rq.Credentials = new NetworkCredential(un, pw);
            rq.Headers.Add("x-iq-token", "A15M876TJ8");
            rq.Headers.Add("x-iq-user", un);
            rq.Timeout = 10000;//only try for 10 seconds
            rq.Method = "PUT";
             HttpWebResponse rs;
            try
            {
               rs = (HttpWebResponse)rq.GetResponse();
            }
            catch (WebException e)
            {
                
                return HttpStatusCode.Unauthorized;
            }

            HttpStatusCode code = rs.StatusCode;
            rs.Close();
            return code;
           
        }








        private FlowProject flowProject { get; set; }
        private ProjectGallery gallery { get; set; }
        private Dispatcher Dispatcher { get; set; }


        /// <summary>
        /// Worker thread
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
		
		void PullOrders_Worker(object sender, DoWorkEventArgs e)
        {
            logger.Info("Waiting to download orders");
            if (ProgressInfo != null)
            {
                ProgressInfo.Message = "Please Wait To Load Order...";
                ProgressInfo.Update();
            }
            Thread.Sleep(3000);

            logger.Info("Downloading orders...");
            if (ProgressInfo != null)
            {
                ProgressInfo.Message = "Downloading Orders...";
                ProgressInfo.Update();
            }

            
            ProjectGallerySummary gallerySummary = this._flowMasterDataContext.LiveFlowProjectGalleries.First(g => g.EventID == gallery.GalleryCode);;
            this.Dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
            {
            
            //System.Windows.Forms.MessageBox.Show("A");
            if (gallerySummary.YBPoseList1 != null && gallerySummary.YBPoseList1.Count > 0)
            {
                logger.Info("Handling yearbook poses (pose 1)");
                if (!this.flowProject.FlowProjectDataContext.ProjectImageTypes.Any(t => t.ProjectImageTypeDesc.ToLower().Contains("yearbook pose")))
                {
                    ProjectImageType newItem = new ProjectImageType();
                    newItem.ProjectImageTypeDesc = "Yearbook Pose";
                    newItem.IsExclusive = true;

                    this.flowProject.FlowProjectDataContext.ProjectImageTypes.InsertOnSubmit(newItem);
                    this.flowProject.FlowProjectDataContext.SubmitChanges();
                }
                if (this.flowProject.FlowProjectDataContext.ProjectImageTypes.Any(t => t.ProjectImageTypeDesc.ToLower().Contains("yearbook pose") && t.IsExclusive == false))
                {
                    ProjectImageType newItem = this.flowProject.FlowProjectDataContext.ProjectImageTypes.First(t => t.ProjectImageTypeDesc.ToLower().Contains("yearbook pose") && t.IsExclusive == false);
                    newItem.IsExclusive = true;
                    this.flowProject.FlowProjectDataContext.SubmitChanges();
                }

                //lets first get the yearbook poses handled
                ProjectImageType type = this.flowProject.FlowProjectDataContext.ProjectImageTypes.First(pit => pit.ProjectImageTypeDesc.ToLower() == "yearbook pose");
                foreach (string imageName in gallerySummary.YBPoseList1)
                {
                    string imgName = imageName;
                    if (imgName.EndsWith(".png"))
                        imgName = imgName.Replace(".png", ".JPG");
                    SubjectImage img = this.flowProject.FlowProjectDataContext.SubjectImages.FirstOrDefault(si => si.ImageFileName.ToLower() == imgName.ToLower());
                    if (img == null)
                    {
                        FlowMessageBox msg = new FlowMessageBox("Missing Yearbook Pose", "The yearbook pose 1 selected by the user does not exists: " + imgName);
                        msg.CancelButtonVisible = false;
                        msg.ShowDialog();
                        continue;
                    }

                    if (img.SubjectImageTypeList.Any(sit => sit.ProjectImageTypeID == type.ProjectImageTypeID))
                    {
                        img.SubjectImageTypes.First(sit => sit.ProjectImageTypeID == type.ProjectImageTypeID).IsAssigned = true;
                    }
                    img.Subject.FlagForMerge = true;
                }
            }
            else
            {
                logger.Info("No Pose 1 yearbook poses to handle");
            }

            if (gallerySummary.YBPoseList2 != null && gallerySummary.YBPoseList2.Count > 0)
            {
                logger.Info("Handling yearbook poses (pose 2)");

                if (!this.flowProject.FlowProjectDataContext.ProjectImageTypes.Any(t => t.ProjectImageTypeDesc.ToLower().Contains("yearbook pose 2")))
                {
                    ProjectImageType newItem = new ProjectImageType();
                    newItem.ProjectImageTypeDesc = "Yearbook Pose 2";
                    newItem.IsExclusive = true;

                    this.flowProject.FlowProjectDataContext.ProjectImageTypes.InsertOnSubmit(newItem);
                    this.flowProject.FlowProjectDataContext.SubmitChanges();
                }

                ProjectImageType type2 = this.flowProject.FlowProjectDataContext.ProjectImageTypes.First(pit => pit.ProjectImageTypeDesc.ToLower() == "yearbook pose 2");
                foreach (string imageName in gallerySummary.YBPoseList2)
                {
                    string imgName = imageName;
                    if (imgName.EndsWith(".png"))
                        imgName = imgName.Replace(".png", ".JPG");

                    SubjectImage img = this.flowProject.FlowProjectDataContext.SubjectImages.FirstOrDefault(si => si.ImageFileName.ToLower() == imgName.ToLower());
                    if (img == null)
                    {
                        FlowMessageBox msg = new FlowMessageBox("Missing Yearbook Pose", "The yearbook pose 2 selected by the user does not exists: " + imgName);
                        msg.CancelButtonVisible = false;
                        msg.ShowDialog();
                        continue;
                    }
                    if (img.SubjectImageTypeList.Any(sit => sit.ProjectImageTypeID == type2.ProjectImageTypeID))
                    {
                        img.SubjectImageTypes.First(sit => sit.ProjectImageTypeID == type2.ProjectImageTypeID).IsAssigned = true;
                    }
                    img.Subject.FlagForMerge = true;
                }
            }
            else
            {
                logger.Info("No Pose 2 yearbook poses to handle");
            }

            
            this.flowProject.FlowProjectDataContext.SubmitChanges();
            
            gallerySummary.YearbookPoseCount = 0;
            

            }));
            

            string custId = this._flowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixCustomerID;
            string custPw = this._flowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixCustomerPassword;

            
            IEnumerable<IQOrder> orders = gallerySummary.newOrders;


            if (orders == null)
            {
                logger.Info("Done downloading orders");
                if (ProgressInfo != null)
                {
                    ProgressInfo.Complete("Done with Orders");
                    ProgressInfo.Update();
                }
                return;

            }
                
            int total = orders.Count();
            if (ProgressInfo != null)
            {
                ProgressInfo.Count = total;
                ProgressInfo.Index = 0;
            }
            int n = 1;
            

            logger.Info("Downloading all orders...");
            foreach (IQOrder order in orders)
            {
                logger.Info("Downloading order {0} of {1}", n, total);
                if (ProgressInfo != null)
                {
                    ProgressInfo.Message = "Downloading Orders " + n++ + "/" + total;
                    ProgressInfo.Progress = total / n;
                    ProgressInfo.Update();
                }
                string orderURL = this._flowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixPublishURL + "/customer/" + custId + "/order/" + order.OrderID;
                
                string backgroundURL = this._flowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixBaseURL + "/catalog/rest/flow/background/";

                String JsonResponse = GetJson(orderURL, custId, custPw, "", gallerySummary.EventID);
                if (JsonResponse == null)
                {
                    continue;
                }
                JsonParser parser = new JsonParser(new StringReader(JsonResponse), true);
                NetServ.Net.Json.JsonParser.TokenType nextToken2 = parser.NextToken();
                JsonObject rootObject = parser.ParseObject();
                //get any rootObject Level Stuff here

                int orderOrderID = (int)(JsonNumber)rootObject["id"];
                logger.Info("id = " + orderOrderID);
                string notes = "";
                JsonArray orderDatas = (JsonArray)rootObject["galleryOrders"];
                logger.Info("found galleryOrders");
                foreach (JsonObject orderdata in orderDatas)
                {
                    string orderGalleryID = ((JsonNumber)orderdata["galleryID"]).Value.ToString();
                    //string orderGalleryTitle = ((JsonString)orderdata["galleryTitle"]).Value;
                    if (orderGalleryID != this.gallery.GalleryCode)
                    {
                        notes += " (Matching Order: " + orderOrderID.ToString() + ") ";
                        logger.Info(" (Matching Order: " + orderOrderID.ToString() + ") ");
                    }
                }

                
                foreach (JsonObject orderdata in orderDatas)
                {
                    string orderGalleryID = ((JsonNumber)orderdata["galleryID"]).Value.ToString();
                    logger.Info("currentGalleryID = " + this.gallery.GalleryCode);
                    logger.Info("orderGalleryID = " + orderGalleryID);
                    if (orderGalleryID != this.gallery.GalleryCode)
                    {
                        continue;
                    }

                    string StudentName = "";
                    if (orderdata.ContainsKey("studentIdentifier") && (orderdata["studentIdentifier"]).GetType() != typeof(JsonNull))
                        StudentName = ((JsonString)orderdata["studentIdentifier"]).Value;

                    logger.Info("Downloading order for " + StudentName);

                    string StudentTicketCode = null;
                    //if (orderdata.ContainsKey("subjectCustomData1") && (orderdata["subjectCustomData1"]).GetType() != typeof(JsonNull))
                    if (orderdata.ContainsKey("subject") && (orderdata["subject"]).GetType() != typeof(JsonNull))
                    {
                        JsonObject sub = (JsonObject)orderdata["subject"];
                        if (sub.ContainsKey("code") && (sub["code"]).GetType() != typeof(JsonNull))
                            StudentTicketCode = ((JsonString)sub["code"]).Value;
                    }

                    JsonArray orderItems = (JsonArray)orderdata["orderItems"];

                    foreach (JsonObject orderItem in orderItems)
                    {
                        //root package info
                        string packageName = ((JsonString)orderItem["name"]).Value;
                        logger.Info("Downloading package " + packageName);

                        string packageId = "-1";
                        if(orderItem["refID"].JsonTypeCode != JsonTypeCode.Null)
                            packageId = ((JsonString)orderItem["refID"]).Value;
                        int intPackageId;
                        Int32.TryParse(packageId,out intPackageId);



                        string imageName = "";
                        string backgroundName = null;

                        if (orderItem.ContainsKey("imageName"))
                        {
                            imageName = ((JsonString)orderItem["imageName"]).Value;
                        }
                        else
                        {
                            if (orderItem.ContainsKey("orderItems"))
                            {
                                int qty = (int)(JsonNumber)orderItem["quantity"];
                                for (int j = 1; j <= qty; j++)
                                {
                                    JsonArray items = ((JsonArray)orderItem["orderItems"]);
                                    OrderPackage op = null;
                                    List<string> usedRefIDs = new List<string>();

                                    foreach (JsonObject item in items)
                                    {
                                        
                                        
                                        if (item.ContainsKey("nodes"))
                                        {
                                            JsonArray nodes = ((JsonArray)item["nodes"]);
                                            foreach (JsonObject node in nodes)
                                            {
                                                if (node.ContainsKey("filename") && node["filename"].JsonTypeCode != JsonTypeCode.Null)
                                                {
                                                    imageName = ((JsonString)node["filename"]).Value;
                                                    if (imageName.EndsWith(".png"))
                                                        imageName = imageName.Replace(".png", ".jpg");
                                                }

                                                if ((node.ContainsKey("backgroundID") && node["backgroundID"].JsonTypeCode != JsonTypeCode.Null) ||
                                                    (node.ContainsKey("backgrounID") && node["backgrounID"].JsonTypeCode != JsonTypeCode.Null))
                                                {
                                                    JsonNumber backgroundID = null ;
                                                    if(node.ContainsKey("backgroundID"))
                                                        backgroundID = (JsonNumber)node["backgroundID"];
                                                    else if (node.ContainsKey("backgrounID"))
                                                        backgroundID = (JsonNumber)node["backgrounID"];

                                                    //http://api.dev.imagequix.com/catalog/rest/flow/background/31271
                                                    String JsonResponsebg = GetJson(backgroundURL + backgroundID.Value, custId, custPw, "", "");
                                                    JsonParser parserbg = new JsonParser(new StringReader(JsonResponsebg), true);
                                                    JsonObject rootObjectbg = parserbg.ParseObject();
                                                    JsonObject bgObject = (JsonObject)rootObjectbg["background"];
                                                    
                                                    backgroundName = ((JsonString)bgObject["name"]).Value;
                                                    if (!this._flowMasterDataContext.GreenScreenBackgrounds.Any(g => g.FileName == backgroundName))
                                                    {
                                                        string remoteFileUrl = ((JsonString)rootObjectbg["backgroundURL"]).Value;
                                                        string localFileName = Path.Combine(FlowContext.FlowBackgroundsDirPath, backgroundName);
                                                        WebClient webClient = new WebClient();
                                                        webClient.DownloadFile(remoteFileUrl, localFileName);
                                                        this._flowMasterDataContext.GreenScreenBackgroundsRefresh();
                                                    }
                                                    logger.Info("Has Background:  " + backgroundName);
                                                }
                                            }
                                        }
                                        //product level info
                                        if (!string.IsNullOrEmpty(imageName) && !string.IsNullOrEmpty(StudentTicketCode))
                                        {
                                            string altImageName = imageName.Replace(".jpg", ".png");
                                            logger.Info("Looking For Image:  " + imageName);
                                            logger.Info("Alt Image name: :" + altImageName);
                                            logger.Info("Looking For TicketCode:  " + StudentTicketCode);
                                            logger.Info("Looking For Name:  " + StudentName);
                                            this.Dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                                            {
                                                //imageName = ((JsonString)item["image-name"]).Value;

                                                //find the subject
                                                SubjectImage subImage = null;
                                                Subject sub = null;

                                                if (flowProject.ImageList.Any(i => i.ImageFileName.ToLower().StartsWith(imageName.ToLower()) || i.ImageFileName.ToLower().StartsWith(altImageName.ToLower())))
                                                {

                                                    bool found = false;
                                                    List<SubjectImage> possibleResults = flowProject.ImageList.Where(i => (i.ImageFileName.ToLower().StartsWith(imageName.ToLower()) || i.ImageFileName.ToLower().StartsWith(altImageName.ToLower())) && i.Subject.TicketCode == StudentTicketCode).ToList();
                                                    if (!found && StudentTicketCode != null && possibleResults.Count() == 1)
                                                    {
                                                        logger.Info("a - Looking For:  " + imageName);
                                                        try
                                                        {
                                                            subImage = possibleResults.First();
                                                            sub = subImage.Subject;
                                                            found = true;
                                                        }
                                                        catch (Exception exc)
                                                        {
                                                            //do nothing, found is still false, it will keep trying
                                                        }
                                                    }
                                                    else
                                                    {
                                                        logger.Info("Matching rows for imagename and ticketcode: " + possibleResults.Count());
                                                    }

                                                    if (!found)
                                                    {
                                                        possibleResults = flowProject.ImageList.Where(i => i.ImageFileName.ToLower().StartsWith(imageName.ToLower()) || i.ImageFileName.ToLower().StartsWith(altImageName.ToLower())).ToList();
                                                        if (possibleResults.Count() == 1)
                                                        {
                                                            try{
                                                                logger.Info("b - Looking For:  " + imageName);
                                                                subImage = possibleResults.First();
                                                                sub = subImage.Subject;
                                                                found = true;
                                                            }
                                                            catch (Exception exc)
                                                            {
                                                                //do nothing, found is still false, it will keep trying
                                                            }
                                                        }
                                                        else
                                                        {
                                                            logger.Info("Matching rows for just imagename: " + possibleResults.Count());
                                                        }
                                                    }
                                                    if (!found)
                                                    {
                                                        possibleResults = flowProject.ImageList.Where(i => (i.ImageFileName.ToLower().StartsWith(imageName.ToLower()) || i.ImageFileName.ToLower().StartsWith(altImageName.ToLower())) && (i.Subject.SubjectData["First Name"].ToString() + " " + i.Subject.SubjectData["Last Name"].ToString()) == StudentName).ToList();
                                                        if (possibleResults.Count() == 1)
                                                        {
                                                            try{
                                                            logger.Info("c - Looking For:  " + imageName);
                                                            subImage = possibleResults.First();
                                                            sub = subImage.Subject;
                                                            found = true;
                                                            }
                                                            catch (Exception exc)
                                                            {
                                                                //do nothing, found is still false, it will keep trying
                                                            }
                                                        }
                                                        else
                                                        {
                                                            logger.Info("Matching rows for imagename and subject name: " + possibleResults.Count());
                                                        }
                                                    }

                                                    if(!found)
                                                    {
                                                        FlowMessageBox msg = new FlowMessageBox("Can't Find Subject", "Unable to find " + StudentName + "\n\nAre there multiple subjects with this name that have the same image? (" + imageName + ")");
                                                        msg.CancelButtonVisible = false;
                                                        msg.ShowDialog();
                                                    }
                                                }
                                                if(sub != null &&  subImage != null)
                                                {

                                                    sub.FlagForMerge = true;
                                                    flowProject.ImageList.CurrentItem = subImage;
                                                    sub.ImageList.CurrentItem = subImage;

                                                    if (op == null)
                                                    {
                                                        ProductPackage pp = null;
                                                        
                                                        if (flowProject.FlowCatalog.ProductPackageList.Any(p => p.ProductPackageID == intPackageId))
                                                        {
                                                            logger.Info("a - Looking For packageId:  " + intPackageId);
                                                            pp = this.flowProject.FlowCatalog.ProductPackageList.First(p => p.ProductPackageID == intPackageId);
                                                        }
                                                        else
                                                        {


                                                            if (flowProject.FlowCatalog.ProductPackageList.Any(p => p.ProductPackageDesc.ToLower().Replace(" ", "") == packageName.ToLower().Replace(" ", "")))
                                                            {
                                                                logger.Info("b - Looking For packageId:  " + intPackageId);
                                                                pp = flowProject.FlowCatalog.ProductPackageList.First(p => p.ProductPackageDesc.ToLower().Replace(" ", "") == packageName.ToLower().Replace(" ", ""));

                                                            }
                                                            else
                                                            {
                                                                FlowMessageBox msg = new FlowMessageBox("Can't Find Package", "OrderID: " + order.OrderID + "\n\nUnable to find a package named " + packageName + " with a PackageId of " + intPackageId + "\n\nThis Order cannot be downloaded.");
                                                                msg.CancelButtonVisible = false;
                                                                msg.ShowDialog();
                                                            }
                                                        }
                                                        if (pp != null)
                                                        {
                                                            logger.Info("found productpackage:  " + pp.ProductPackageDesc);

                                                            op = sub.AddOrderPackage(pp, subImage);
                                                            
                                                            op.IsOnlineOrder = true;
                                                            op.IQOrderID = (int)((JsonNumber)rootObject["id"]).Value;
                                                            op.WebOrderID = op.IQOrderID;
                                                            op.Notes = notes;
                                                            op.ModifyDate = DateTime.Now;
                                                            string orderDate = ((JsonString)rootObject["orderDate"]).Value.Replace("-", "");
                                                            op.IQOrderDate = new DateTime(Convert.ToInt32(orderDate.Substring(0, 4)), Convert.ToInt32(orderDate.Substring(4, 2)), Convert.ToInt32(orderDate.Substring(6, 2)));
                                                            op.ShipmentType = ((JsonString)rootObject["shipmentType"]).Value;
                                                            if (op.ShipmentType == "direct")
                                                            {
                                                                if (rootObject["shippingFirstName"] != JsonNull.Null) op.ShippingFirstName = ((JsonString)rootObject["shippingFirstName"]).Value;
                                                                if (rootObject["shippingLastName"] != JsonNull.Null) op.ShippingLastName = ((JsonString)rootObject["shippingLastName"]).Value;
                                                                if (rootObject["shippingAddress"] != JsonNull.Null) op.ShippingAddress = ((JsonString)rootObject["shippingAddress"]).Value;
                                                                if (rootObject["shippingCity"] != JsonNull.Null) op.ShippingCity = ((JsonString)rootObject["shippingCity"]).Value;
                                                                if (rootObject["shippingState"] != JsonNull.Null) op.ShippingState = ((JsonString)rootObject["shippingState"]).Value;
                                                                if (rootObject["shippingZip"] != JsonNull.Null) op.ShippingZip = ((JsonString)rootObject["shippingZip"]).Value;
                                                                if (rootObject["shippingCountry"] != JsonNull.Null) op.ShippingCountry = ((JsonString)rootObject["shippingCountry"]).Value;

                                                                //if(rootObject["billingFirstName"] != JsonNull.Null) op.ShippingFirstName = ((JsonString)rootObject["billingFirstName"]).Value;
                                                                //if (rootObject["billingLastName"] != JsonNull.Null) op.ShippingLastName = ((JsonString)rootObject["billingLastName"]).Value;
                                                                //if (rootObject["billingAddress"] != JsonNull.Null) op.ShippingAddress = ((JsonString)rootObject["billingAddress"]).Value;
                                                                //if (rootObject["billingCity"] != JsonNull.Null) op.ShippingCity = ((JsonString)rootObject["billingCity"]).Value;
                                                                //if (rootObject["billingState"] != JsonNull.Null) op.ShippingState = ((JsonString)rootObject["billingState"]).Value;
                                                                //if (rootObject["billingZip"] != JsonNull.Null) op.ShippingZip = ((JsonString)rootObject["billingZip"]).Value;
                                                                //if (rootObject["billingCountry"] != JsonNull.Null) op.ShippingCountry = ((JsonString)rootObject["billingCountry"]).Value;
                                                            }
                                                            PulledIQOrder pio = new PulledIQOrder();
                                                            pio.IQOrderID = op.IQOrderID;
                                                            pio.IQOrderDate = op.IQOrderDate;
                                                            pio.FlowProjectGuid = flowProject.FlowProjectGuid;
                                                            this._flowMasterDataContext.PulledIQOrders.InsertOnSubmit(pio);
                                                        }

                                                    }
                                                    if (op != null)
                                                    {
                                                        OrderProduct oProd = null;
                                                        logger.Info("we have an orderPackage now:  " + op.ProductPackageDesc);

                                                        try { logger.Info("refID:  " + item["refID"]); }catch (Exception exc){}
                                                         try { logger.Info("id:  " + Convert.ToInt32(((JsonString)item["id"]).ToString()));}catch (Exception exc){}
                                                         try { logger.Info("Name:  " + ((JsonString)item["name"]).ToString());}catch (Exception exc){}
                                                         try { logger.Info("op.OrderProducts count:  " + op.OrderProducts.Count); }catch (Exception exc) { }
                                                        foreach (OrderProduct thisop in op.OrderProducts)
                                                        {
                                                             try { logger.Info("thisop.ImageQuixProductPk:  " + thisop.ImageQuixProductPk);}catch (Exception exc){}
                                                             try { logger.Info("thisop.Label:  " + thisop.Label);}catch (Exception exc){}
                                                             try { logger.Info("thisop.OrderProductLabel:  " + thisop.OrderProductLabel); }
                                                             catch (Exception exc) { }
                                                        }

                                                        {
                                                            logger.Info("b - getting orderProduct:  " + ((JsonString)item["name"]).ToString().Trim());

                                                            string refid = "";
                                                            if (item["refID"].JsonTypeCode != JsonTypeCode.Null)
                                                                refid = ((JsonString)item["refID"]).ToString();
                                                            string pName = "";
                                                             if (item["name"].JsonTypeCode != JsonTypeCode.Null)
                                                                pName = ((JsonString)item["name"]).ToString().Trim();
                                                            string cmb = refid + "_" + pName;
                                                            int indexToGet = 0;
                                                            foreach (string s in usedRefIDs)
                                                                if (s == cmb) indexToGet++;

                                                            usedRefIDs.Add(cmb);

                                                            if (String.IsNullOrEmpty(refid) && (item["productRef"].ToString().EndsWith("/101") || item["productRef"].ToString().EndsWith("/102") || item["productRef"].ToString().EndsWith("/103") || item["productRef"].ToString().EndsWith("/104")))
                                                            {
                                                                //continue;
                                                            }
                                                            else
                                                            {
                                                                //new way
                                                                if (op.OrderProducts.Count(o => o.ImageQuixProductPk == Convert.ToInt32(((JsonString)item["refID"]).ToString())) > 1)
                                                                    oProd = op.OrderProducts.Where(o => o.ImageQuixProductPk == Convert.ToInt32(((JsonString)item["refID"]).ToString()) && ((JsonString)item["name"]).ToString().Trim().StartsWith(o.Label.Trim())).ToList()[indexToGet];
                                                                else if (op.OrderProducts.Count(o => o.ImageQuixProductPk == Convert.ToInt32(((JsonString)item["refID"]).ToString())) == 1)
                                                                    oProd = op.OrderProducts.Where(o => o.ImageQuixProductPk == Convert.ToInt32(((JsonString)item["refID"]).ToString())).ToList()[indexToGet];


                                                                if (oProd == null && item["refID"].JsonTypeCode != JsonTypeCode.Null)
                                                                {
                                                                    logger.Info("ref-id is null, trying it the old way (match on product Name)  ");
                                                                    //old way
                                                                    if (op.OrderProducts.Any(o => o.Label == pName))
                                                                    {
                                                                        logger.Info("matching product on: " + pName);
                                                                        oProd = op.OrderProducts.Where(o => o.Label == pName).ToList()[indexToGet];
                                                                    }
                                                                    else
                                                                    {
                                                                        pName = pName.Trim().Remove(pName.Length - 4).Trim();
                                                                        logger.Info("matching product on: " + pName);
                                                                        if (op.OrderProducts.Any(o => o.Label == pName))
                                                                            oProd = op.OrderProducts.Where(o => o.Label == pName).ToList()[indexToGet];
                                                                    }
                                                                }
                                                            }
                                                        }

                                                        
                                                        if (oProd == null)
                                                        {
                                                            //101 - 104 are digital downloads, there are no products to match
                                                            if (!item["productRef"].ToString().EndsWith("101") && !item["productRef"].ToString().EndsWith("102") && !item["productRef"].ToString().EndsWith("103") && !item["productRef"].ToString().EndsWith("104"))
                                                            {
                                                                string errormsg = "Package: " + op.ProductPackageDesc + " - Unable to find a match for this product: " + item["name"] + " (" + item["refID"] + ")";
                                                                logger.Info("No Order Product Could be Found - " + errormsg);
                                                                FlowMessageBox msg = new FlowMessageBox("Can't Find Package Product", errormsg);
                                                                msg.CancelButtonVisible = false;
                                                                msg.ShowDialog();
                                                            }
                                                        }
                                                        else
                                                        {
                                                            logger.Info("Product Matched");
                                                            if (!string.IsNullOrEmpty(backgroundName))
                                                            {
                                                                logger.Info("Looking for Background:  " + backgroundName);
                                                                oProd.GreenScreenBackground = this._flowMasterDataContext.GreenScreenBackgrounds.Where(gsb => gsb.FileName == backgroundName).First().FullPath;
                                                                logger.Info("Found Background:  " + oProd.GreenScreenBackground);
                                                            }
                                                            foreach (OrderProductNode orderProductNode in oProd.OrderProductNodes)
                                                            {
                                                                logger.Info("setting node image");
                                                                logger.Info("imageName: " + subImage.ImageFileName);
                                                                orderProductNode.SubjectImage = subImage;

                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    FlowMessageBox msg = new FlowMessageBox("Can't Find Subject", "Unable to find a subject for this image: " + imageName);
                                                    msg.CancelButtonVisible = false;
                                                    msg.ShowDialog();
                                                }

                                            }));

                                            //}
                                        }
                                    }
                                    if (op != null)
                                    {
                                        this.Dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                                        {
                                            logger.Info("about to submit changes");
                                            this.flowProject.FlowProjectDataContext.SubmitChanges();
                                            logger.Info("done with submit changes");
                                        }));
                                    }

                                }
                            }
                        }
                        //if (imageName.Length > 0)
                        //{
                        //    this.Dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                        //    {
                        //        //find the subject
                        //        SubjectImage subImage = flowProject.ImageList.First(i => i.ImageFileName.StartsWith(imageName));
                        //        Subject sub = subImage.Subject;
                        //        flowProject.ImageList.CurrentItem = subImage;
                        //        sub.ImageList.CurrentItem = subImage;

                        //        //create the package
                        //        ProductPackage pp = this.flowProject.FlowCatalog.ProductPackageList.First(p => p.ProductPackageID == intPackageId);
                        //        //add package to subject
                        //        completedOrders.Add(order.OrderID);
                        //        sub.AddOrderPackage(pp, subImage);


                        //    }));
                        //}
                    }
                }
                //set order status to Complete only if it is not a multiproject order
                if (string.IsNullOrWhiteSpace(notes))
                {
                    string orderStatusURL = this._flowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixPublishURL + "/customer/" + custId + "/order/" + order.OrderID + "/status/Shipped";
                    PostToUri(orderStatusURL, custId, custPw);
                }


                gallerySummary.NewOnlineOrders = 0;
                gallerySummary.newOrders = null;
                gallerySummary.ShowDoneLabel = true;
                gallerySummary.ShowGetOrderButton = false;
                this.flowProject.UpdateListOfOnlineOrders();
                _flowMasterDataContext.SavePreferences();
            }

            logger.Info("Done downloading orders");
            if (ProgressInfo != null)
            {
                ProgressInfo.Complete("Done with Orders");
                ProgressInfo.Update();
            }
        }

        void PullOrders_Completed(object sender, RunWorkerCompletedEventArgs e)
        {

            this.flowController.PullingOrders = false;
            try
            {
                flowController.ApplicationPanel.IsEnabled = true;
            }
            catch
            {
                this.Dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                {
                    flowController.ApplicationPanel.IsEnabled = true;
                }));
            }
            
        }

        /// <summary>
        /// Uploads a Flow project archive (database and image files) to an ftp server;
        /// option to detach removes references to project from FlowMaster
        /// </summary>
        /// <param name="flowProject"></param>
        /// <param name="destinationPath"></param>
        /// <param name="withDetach"></param>

        public void BeginDownloadOrders(FlowProject fp, ProjectGallerySummary gal, Dispatcher dis, FlowController fc)
        {
            this.flowProject = fp;
            this.gallery = fp.Gallery;
            this.Dispatcher = dis;
            this.flowController = fc;

            this.flowController.PullingOrders = true;
            ProgressInfo = new NotificationProgressInfo("Download Orders", "Initializing...");
            ProgressInfo.AllowCancel = true;
            IShowsNotificationProgress progressNotification = ObjectFactory.GetInstance<IShowsNotificationProgress>();

            progressNotification.ShowNotification(ProgressInfo);
            ProgressInfo.NotificationProgress = progressNotification;
            try
            {
                FlowBackgroundWorkerManager.RunWorker(PullOrders_Worker, PullOrders_Completed);
            }
            catch (Exception e)
            {
                this.flowController.PullingOrders = false;

                logger.WarnException("Problem starting PullOrders_Worker.", e);
            }
        }
    }
}
