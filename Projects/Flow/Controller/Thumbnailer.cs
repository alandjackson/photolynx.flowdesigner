﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.Controller.Project;
using Flow.Lib.AsyncWorker;
using Flow.Lib;
using System.ComponentModel;
using Flow.Schema.LinqModel.DataContext;
using System.IO;
using System.Windows.Threading;
using System.Threading;
using StructureMap;
using Flow.Schema;
using NLog;
using Flow.Lib.Helpers;
using Flow.Lib.Net;
using Flow.Controls.View.Dialogs;
using System.Drawing.Imaging;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace Flow.Controller
{
    public class Thumbnailer : NotifyPropertyBase
    {
        string projectsDir { get; set; }
        int ThumbSize { get; set; } 
        
        FlowBackgroundWorker bWorker;

        private static Logger logger = LogManager.GetCurrentClassLogger();

        bool keepRunning { get; set; }

        int errorCount { get; set; }
       
        

        public Thumbnailer(FlowController flowController)
        {
            projectsDir = flowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.ProjectsDirectory;
            ThumbSize = flowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.ThumbSize;
        }

        void UploadQueue_AddingNew(object sender, AddingNewEventArgs e)
        {
            SendPropertyChanged("ImagesToProcess");
        }

        public void Begin()
        {
            errorCount = 0;
            //start a thread that will be looking for files to upload to lab
            keepRunning = true;
            bWorker = new FlowBackgroundWorker();
            bWorker = FlowBackgroundWorkerManager.RunWorker(ThumbnailEngine, ThumbnailEngine_Stopped);
            //bWorker.RunWorkerAsync();
        }


        void ThumbnailEngine_Stopped(object sender, RunWorkerCompletedEventArgs e)
        {

        }

        void ThumbnailEngine(object sender, DoWorkEventArgs e)
        {

            
            //Thread.Sleep(5000);
            while (keepRunning)
            {
                Thread.Sleep(5000);
                try
                {
                    foreach (string projectDir in Directory.GetDirectories(projectsDir))
                    {
                        string imageFolder = Path.Combine(projectDir, "Image");
                        if (Directory.Exists(imageFolder))
                        {

                            Thread.Sleep(2000);
                            
                            string FullResPath = imageFolder;
                            string ThumbPath = Path.Combine(FullResPath, "thumbnails");
                            if (!Directory.Exists(ThumbPath))
                                Directory.CreateDirectory(ThumbPath);
                            int FileModCount = 0;
                            DateTime before = DateTime.Now;
                            foreach (string file in Directory.GetFiles(FullResPath))
                            {
                                if (file.ToLower().EndsWith(".jpg") || file.ToLower().EndsWith(".png"))
                                {
                                   
                                    string thisFile = file;
                                    string thisThumb = Path.Combine(ThumbPath, (new FileInfo(file)).Name);
                                    if (!File.Exists(thisThumb))
                                    {
                                        FileModCount++;
                                        if (FileModCount == 1)
                                        {
                                            before = DateTime.Now;
                                            logger.Info("Found thumbnails to create in " + FullResPath);
                                        }

                                        File.Copy(thisFile, thisThumb);
                                        ResizeImage(new FileInfo(thisThumb), ThumbSize, 60);

                                    }
                                }
                            }
                            DateTime after = DateTime.Now;
                            if (FileModCount > 0)
                            {
                                TimeSpan timeDiff = (after - before);
                                logger.Info("Processed " + FileModCount + " thumbnails in " + FullResPath + " (" + timeDiff.TotalSeconds + " seconds)");
                            }
                            
                        }
                    }
                    errorCount = 0;
                }
                catch (Exception ex)
                {
                    errorCount++;
                    logger.Error("ERROR - THUMBNAILER - " + ex.Message);

                    //there was an error
                    if (errorCount > 30)
                    {
                        keepRunning = false;
                        logger.Error("ERROR - THUMBNAILER - FAILED MORE THAN 30 TIMES AND IS NOW QUITING");
                    }
                    
                   
                    Thread.Sleep(10000);//sleep 10 seconds
                }
            }
        }


       
        private bool ResizeImage(FileInfo file, int maximumEdgeSize, long quality)
        {
            FileStream memoryStream = null;
            int newWidth, newHeight;
            try
            {
                // Read the properties and the image
                Stack<PropertyItem> properties = new Stack<PropertyItem>();
                memoryStream = file.OpenRead();
                System.Drawing.Image image = System.Drawing.Image.FromStream(memoryStream);
                if (image.PropertyItems != null)
                    foreach (PropertyItem item in image.PropertyItems)
                        properties.Push(item);
                memoryStream.Close();
                memoryStream.Dispose();

                double ratio = (double)image.Width / (double)image.Height;
                if (ratio < 1)
                {
                    // Portrait
                    newHeight = maximumEdgeSize;
                    newWidth = Convert.ToInt32((double)maximumEdgeSize * ratio);
                }
                else
                {
                    // Lanscape
                    newWidth = maximumEdgeSize;
                    newHeight = Convert.ToInt32((double)maximumEdgeSize / ratio);
                }
                if (newWidth > image.Width || newHeight > image.Height)
                    return false;


                System.Drawing.Image thumbnail = new Bitmap(newWidth, newHeight);
                System.Drawing.Graphics graphic = System.Drawing.Graphics.FromImage(thumbnail);

                // Resize, delete the old image, create the new one and add the properties
                graphic.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphic.SmoothingMode = SmoothingMode.HighQuality;
                graphic.PixelOffsetMode = PixelOffsetMode.HighQuality;
                graphic.CompositingQuality = CompositingQuality.HighQuality;
                graphic.DrawImage(image, 0, 0, newWidth, newHeight);
                ImageCodecInfo[] info = ImageCodecInfo.GetImageEncoders();
                EncoderParameters encoderParameters;
                encoderParameters = new EncoderParameters(1);
                encoderParameters.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, quality);
                File.Delete(file.FullName);
                memoryStream = file.OpenWrite();
                while (properties.Count > 0)
                    thumbnail.SetPropertyItem(properties.Pop());
                thumbnail.Save(memoryStream, info[1], encoderParameters);
                return true;
            }
            catch
            {
                return false;
            }
            finally
            {
                if (memoryStream != null)
                    memoryStream.Close();
            }

        }

       

    }

    
}
