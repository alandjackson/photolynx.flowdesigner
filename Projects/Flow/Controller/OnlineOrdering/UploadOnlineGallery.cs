﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NLog;
using Flow.Schema.LinqModel.DataContext;
using NetServ.Net.Json;
using Flow.Lib;
using System.Windows.Threading;
using Flow.View.Dialogs;
using System.Net;
using System.IO;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows;
using System.Security.Cryptography;
using System.Xml;
using Flow.Schema.LinqModel.DataContext.FlowMaster;
using System.Web;
using System.Threading;
using System.Xml.Serialization;

namespace Flow.Controller.OnlineOrdering
{
    public class UploadOnlineGallery
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        private FlowProject flowProject { get; set; }
        private FlowMasterDataContext flowMasterDataContext { get; set; }
        private OnlineGallery onlineGallery { get; set; }
        private List<Subject> applicableSubjects { get; set; }

        private OnlineApiHelpers OnlineApiHelpers { get; set; }

        private string apiurl { get; set; }
        private string apikey { get; set; }

        public NotificationProgressInfo NotificationInfo { get; set; }

        public bool SwitchShippingEnabled { get; set; }
        public int allowableYearbookSelections { get; set; }

        public UploadOnlineGallery(FlowProject fp, FlowMasterDataContext fmdc, OnlineGallery fpg, List<Subject> applicableSubjects, string url, string key)
        {
            flowProject = fp;
            flowMasterDataContext = fmdc;
            onlineGallery = fpg;
            this.applicableSubjects = applicableSubjects;
            apiurl = url;
            apikey = key;
            OnlineApiHelpers = new OnlineApiHelpers(key);
        }


        public bool Begin()
        {

            //this.flowProject.UpdateGalleryCounts();

            // Check if any of the product packages have a price less than 1.00
            foreach (ProductPackage productPackage in flowProject.FlowCatalog.ProductPackages)
            {
                if ((double)productPackage.Price < 1.0 && productPackage.ShowInOnlinePricesheet == true)
                {
                    this.flowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                    {
                        string msg_text = String.Format("Product package '{0}' has a price less than $1.00, which is not allowed.\n\nPlease set the price to $1.00 or greater.", productPackage.ProductPackageDesc);
                        FlowMessageBox msg = new FlowMessageBox("Invalid Pricesheet", msg_text);
                        msg.CancelButtonVisible = false;
                        msg.ShowDialog();
                    }));
                    if (NotificationInfo != null)
                        this.NotificationInfo.Complete("Invalid Pricesheet");

                    return false;
                }
            }
            this.flowProject.FlowProjectDataContext.SubmitChanges();
            NotificationInfo.Title = "Uploading Gallery";
            NotificationInfo.Update();
            //string eventID = "1056-8030-0014";

            //dont leave this hard coded
            bool isGreenScreen = false;

            if (this.flowProject.IsGreenScreen == true)
                isGreenScreen = true;


            //string url = projectGallery.GalleryURL;
            if (string.IsNullOrEmpty(onlineGallery.GalleryCode))
            {
                onlineGallery.GalleryCode = Guid.NewGuid().ToString();
                flowProject.FlowProjectDataContext.SubmitChanges();
            }
            this.flowProject.FlowProjectDataContext.SubmitChanges();
            //
            // Get or Create Gallery Online
            //
            NotificationInfo.Update("Connecting with online gallery");
            JsonObject GalleryJson = null;
            logger.Info("gallery guid: " + onlineGallery.GalleryCode);
            GalleryJson = OnlineApiHelpers.GetJson(Path.Combine(apiurl, "GetGalleryByGuid"), "{\"guid\":\"" + onlineGallery.GalleryCode + "\"}");
            if (GalleryJson == null)
            {
                logger.Info("gallery does not exists, publish it!");
                //create new gallery
                GalleryJson = OnlineApiHelpers.GetJson(Path.Combine(apiurl, "CreateGallery"), "{ \"Guid\": \"" + onlineGallery.GalleryCode + "\", \"Name\": \"" + onlineGallery.GalleryName + "\", \"shipping_option\": \"" + onlineGallery.ShippingOption + "\" }");
                
                GalleryJson = OnlineApiHelpers.GetJson(Path.Combine(apiurl, "GetGalleryByGuid"), "{\"guid\":\"" + onlineGallery.GalleryCode + "\"}");
            
            }
            if (GalleryJson == null)
            {
                this.flowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                    {
                        FlowMessageBox msg = new FlowMessageBox("Error Creating Gallery", "There was a problem creating a gallery");
                        msg.CancelButtonVisible = false;
                        msg.ShowDialog();
                        
                    }));
                return false;
            }

            string galleryid = ((JsonNumber)GalleryJson["id"]).ToString();
            this.onlineGallery.GalleryID = galleryid;
            this.onlineGallery.GalleryURL = this.flowMasterDataContext.PreferenceManager.Upload.OnlineOrder.EcommerceAPIURL.Replace("/Api/", "/?id=").Replace("/Api", "/?id=") + galleryid;
            this.flowProject.FlowProjectDataContext.SubmitChanges();
            //
            //Get or Create Pricesheet
            //

            //check and upload all missing backgrounds
            
            Dictionary<string, string> bgsMd5 = new Dictionary<string, string>();
            if (isGreenScreen)
            {
                List<string> uploadedbackgrounds = new List<string>();
                if (this.onlineGallery.GalleryBackgroundList != null)
                {
                    uploadedbackgrounds = this.onlineGallery.GalleryBackgroundList.Split(',').ToList();
                }

                NotificationInfo.Update("Uploading Green Screen Backgrounds");
                foreach (GreenScreenBackground bg in this.flowProject.FlowMasterDataContext.GreenScreenBackgrounds)
                {
                    

                    
                    //Resize bg and get md5
                    System.Drawing.Bitmap origBmp = (new Bitmap(bg.FullPath));
                    logger.Info("about to resize " + bg.FullPath);
                    System.Drawing.Image bmp = OnlineApiHelpers.resizeImage(origBmp, 500);
                    Stream ms = new MemoryStream();

                    bmp.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);

                    byte[] buffer = OnlineApiHelpers.ReadToEnd(ms);

                    string md5hash;
                    using (var md5 = MD5.Create())
                    {
                        //md5hash = BitConverter.ToString(md5.ComputeHash(buffer)).Replace("-", "").ToLower();
                        md5hash = Convert.ToBase64String(md5.ComputeHash(buffer));
                    }
                    bgsMd5.Add(bg.FullPath, md5hash);

                    //I needthe md5hash, but no need to upload the image if its already up
                    //if (uploadedbackgrounds.Contains(bg.FullPath))
                    //    continue;

                    //does the bg exist online?
                    JsonObject JsonResult = OnlineApiHelpers.GetJson(Path.Combine(apiurl, "ImageExistsWithMd5"), "\"" + md5hash + "\"");
                    
                    if (JsonResult != null && JsonResult.ContainsKey("results") && (JsonBoolean)JsonResult["results"] == true)
                    //if(uploadedbackgrounds.Contains(bg.FullPath))
                    {
                        //image already exists
                    }
                    else
                    {
                        JsonObject StoredImage = OnlineApiHelpers.GetJson(Path.Combine(apiurl, "StoreImage"), "{ \"GalleryGuid\": \"" + onlineGallery.GalleryCode + "\", \"OriginalImageFilename\": \"" + bg.FullPath + "\", \"ThumbnailMd5\": \"" + md5hash + "\", \"ThumbnailBase64\": \"" + Convert.ToBase64String(buffer) + "\"  }");
                        
                    }
                    if(!uploadedbackgrounds.Contains(bg.FullPath))
                        uploadedbackgrounds.Add(bg.FullPath);
                }
                this.onlineGallery.GalleryBackgroundList = string.Join(",", uploadedbackgrounds); ;
            }
            //create/update pricesheet

            NotificationInfo.Update("Updating online pricesheet");

            XmlDocument doc = null;
            this.flowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                    {
                        doc = GeneratePricesheet(isGreenScreen, bgsMd5);
                    }));

            Console.WriteLine(doc.OuterXml);

            logger.Info("Pricesheet JSON:" + "{ \"galleryguid\": \"" + onlineGallery.GalleryCode + "\", \"packageProgramXml\": \"" + OnlineApiHelpers.EscapeForJson(doc.OuterXml) + "\" }");

            JsonObject Pricesheet = OnlineApiHelpers.GetJson(Path.Combine(apiurl, "AssignCatalog"), "{ \"galleryguid\": \"" + onlineGallery.GalleryCode + "\", \"packageProgramXml\": \"" + OnlineApiHelpers.EscapeForJson(doc.OuterXml) + "\" }");
            if (Pricesheet == null)
            {
                this.flowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                    {
                        FlowMessageBox msg = new FlowMessageBox("Error Creating Online Catalog", "There was an error creating the online catalog.");
                        msg.CancelButtonVisible = false;
                        msg.ShowDialog();
                    }));
                return false;
            }

            //
            // Get or Create Subjects and images Online
            //


            ////////
            ////////
            //Create the OrderUploadHistories Class
            bool useUploadQueue = true;

            OrderUploadHistory ouh = new OrderUploadHistory();
            ouh.ProjectName = this.flowProject.FlowProjectName;
            ouh.IsImageUpload = true;
            ouh.ImageCount = 0;
            this.flowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
            {
                this.flowMasterDataContext.OrderUploadHistories.InsertOnSubmit(ouh);
                this.flowMasterDataContext.SubmitChanges();
                this.flowMasterDataContext.RefreshOrderUploadHistoryList();
            }));

            string LabOrderQueueFolder = this.flowMasterDataContext.PreferenceManager.Application.LabOrderQueueDirectory;

            ImageUploadJob imageUploadJob = new ImageUploadJob();

            string uploadJob = System.IO.Path.Combine(LabOrderQueueFolder, "ImageUpload_" + ouh.OrderUploadHistoryID);
            if (!Directory.Exists(uploadJob)) Directory.CreateDirectory(uploadJob);
            ////////
            ////////



            List<Subject> applicableSubjects = flowProject.SubjectList.ToList();
            NotificationInfo.Update("Processing Subjects and Images", 0, applicableSubjects.Count, 1, true);
            foreach (Subject sub in applicableSubjects)
            {
                NotificationInfo.Step();
                //("Processing All Orders...", 0, allOrders.Count, 1, true);
               
                //get subject by password
                JsonObject GallerySubject = GalleryJson = OnlineApiHelpers.GetJson(Path.Combine(apiurl, "GetSubjectByPassword"), "{ \"password\": \"" + sub.TicketCode + "\",  \"galleryguid\": \"" + onlineGallery.GalleryCode + "\" }");

                //create gallery subject if it does no exist
                if (GallerySubject == null || (GallerySubject.ContainsKey("message") && (JsonString)GallerySubject["message"] == "Not Found"))
                {
                    JsonObject subData = new JsonObject();
                    
                    //foreach (SubjectDatum sd in sub.SubjectData)
                    //{
                    //    if(sd.Value != null)
                    //        subData.Add(sd.Field, sd.Value.ToString());
                    //}

                    string subGuid = Guid.NewGuid().ToString();

                    if (subData.ContainsKey("Student ID") && string.IsNullOrEmpty(subData["Student ID"].ToString()))
                        subData["Student ID"] = (JsonString)sub.TicketCode;
                     if (!subData.ContainsKey("Student ID"))
                         subData.Add("Student ID", sub.TicketCode);
                     if (!subData.ContainsKey("First Name"))
                         subData.Add("First Name", sub.SubjectData["First Name"].Value.ToString());
                     if (!subData.ContainsKey("Last Name"))
                         subData.Add("Last Name", sub.SubjectData["Last Name"].Value.ToString());

                    JsonWriter _writerPS = new JsonWriter();
                    subData.Write(_writerPS);
                    string subDataString = _writerPS.ToString().Replace("\\/", "/");

                    GallerySubject = OnlineApiHelpers.GetJson(Path.Combine(apiurl, "CreateSubject"), "{ \"GalleryGuid\": \"" + onlineGallery.GalleryCode + "\", \"guid\": \"" + subGuid + "\", \"Name\": \"" + sub.FormalFullName + "\", \"Password\": \"" + sub.TicketCode + "\", \"SubjectData\": " + subDataString + " }");
                }


                if (GallerySubject == null)
                {
                    //this.flowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                    //{
                    //    FlowMessageBox msg = new FlowMessageBox("Error Creating Subject", "Failed to Create this Subject: " + sub.FormalFullName);
                    //    msg.buttonOkay.Content = "Continue";
                    //    msg.buttonCancel.Content = "Abort Update";
                    //    if (msg.ShowDialog() == true)
                    //        continue; //they clicked okay, so continue
                    //    else
                    //        return false; //they clicked cancel, abort
                    //}));
                }
                //at this point, the subject DOES exists, so lets upload its images

                //get a list of upload images for this subject
                JsonObject GallerySubjectImages = OnlineApiHelpers.GetJson(Path.Combine(apiurl, "GetSubjectImages"), "{ \"galleryGuid\": \"" + onlineGallery.GalleryCode + "\", \"password\": \"" + sub.TicketCode + "\"}");

                //delete any images that no longer belong to this subject
                //
                //lets not do this for now
                //
                //List<string> existingImagesOnline = new List<string>();
                //if (GallerySubjectImages != null)
                //{
                //    foreach (JsonString o in GallerySubjectImages["order"] as JsonArray)
                //    {
                //        string thisImage = o.Value;
                //        //any images that no longer exist for this subject should be deleted
                //        if (!sub.SubjectImages.Any(si => si.ImageFileName == thisImage))
                //        {
                //            //if an order has been placed against this image, delete will fail
                //            JsonObject DeleteResponse = OnlineApiHelpers.GetJson(Path.Combine(apiurl, "DeleteImage"), "{ \"galleryGuid\": \"" + onlineGallery.GalleryCode + "\", \"password\": \"" + sub.TicketCode + "\", \"OriginalImageFilename\": \"" + thisImage + "\"}");

                //        }
                //    }
                //}
                

                //check if image exists

               


                foreach (SubjectImage img in sub.SubjectImages)
                {
                    if (onlineGallery.IncludeGroupImages == false && img.IsGroupImage)
                        continue;

                    if (!img.ExistsInOnlineGallery)
                    {

                        string imageFileName = img.ImageFileName;
                        string pathToImage = img.ImagePathFullRes;
                        bool gsImage = false;
                        if (isGreenScreen && (!String.IsNullOrEmpty(img.GreenScreenSettings) || img.IsPng))
                        {
                            gsImage = true;
                            string pathToPng = "";
                            if (img.IsPng)
                                pathToPng = img.ImagePathFullRes;
                            else
                            {
                                int wcount = 0;
                                while (string.IsNullOrEmpty(pathToPng))
                                {
                                    wcount++;
                                    try
                                    {
                                         this.flowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                                        {
                                            if (img.IsPng)
                                                pathToImage = img.ImagePathFullRes;
                                            else
                                                pathToPng = img.GetRenderedPngImageFilename(null);
                                        }));
                                        //t.SetApartmentState(ApartmentState.STA);
                                        //t.Start();
                                        //t.Join();
                                        //t.Abort();//just make sure the thread is killed
                                    }
                                    catch (Exception ex)
                                    {
                                        pathToPng = "";
                                        if (wcount == 3)
                                        {
                                            throw ex;
                                        }
                                    }
                                }
                            }
                            if (pathToPng != null)
                            {
                                pathToImage = pathToPng;
                                imageFileName = new FileInfo(pathToPng).Name;
                            }

                        }


                        System.Drawing.Bitmap origBmp = (new Bitmap(pathToImage));

                        //if this is a jpg, we did not render it, so it needs to be rotated before we crop it
                        if (pathToImage.ToLower().EndsWith(".jpg"))
                        {
                            if (img.Orientation == 90)
                                origBmp.RotateFlip(RotateFlipType.Rotate90FlipNone);
                            if (img.Orientation == 180)
                                origBmp.RotateFlip(RotateFlipType.Rotate180FlipNone);
                            if (img.Orientation == 270)
                                origBmp.RotateFlip(RotateFlipType.Rotate270FlipNone);
                        }

                        if (onlineGallery.ApplyCrops == true)
                        {

                            //origBmp = si.IntermediateImage
                            if ((img.CropL + img.CropT + img.CropW + img.CropH) > 0)
                            {

                                Int32Rect rect = OnlineApiHelpers.getCropArea(origBmp, 0, img.CropL, img.CropT, img.CropW, img.CropH);
                                Bitmap tempBmp = origBmp.Clone(new Rectangle(rect.X, rect.Y, rect.Width, rect.Height), origBmp.PixelFormat);
                                origBmp.Dispose();
                                origBmp = tempBmp.Clone() as Bitmap;
                                tempBmp.Dispose();

                            }



                        }

                        string destFile = System.IO.Path.Combine(uploadJob, imageFileName);

                        logger.Info("about to resize " + imageFileName);
                        System.Drawing.Image bmp = OnlineApiHelpers.resizeImage(origBmp, 500);
                        Stream ms = new MemoryStream();
                        if (gsImage)
                        {
                            bmp.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                            bmp.Save(destFile, System.Drawing.Imaging.ImageFormat.Png);
                        }
                        else
                        {
                            bmp.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                            bmp.Save(destFile, System.Drawing.Imaging.ImageFormat.Jpeg);
                        }

                        byte[] buffer = OnlineApiHelpers.ReadToEnd(ms);

                        string md5hash;
                        using (var md5 = MD5.Create())
                        {

                            //md5hash = BitConverter.ToString(md5.ComputeHash(buffer)).Replace("-", "").ToLower();
                            md5hash = Convert.ToBase64String(md5.ComputeHash(buffer));
                        }


                        JsonObject PostDataJson = new JsonObject();
                        //JsonObject tempSubject = new JsonObject();
                        //tempSubject.Add("Password", sub.TicketCode);
                        //tempSubject.Add("Name", sub.FormalFullName);
                        //tempSubject.Add("GalleryGuid", projectGallery.GalleryCode);

                        //PostDataJson.Add("Subject", tempSubject);
                        PostDataJson.Add("ImageType", 1);
                        PostDataJson.Add("GalleryGuid", onlineGallery.GalleryCode);
                        PostDataJson.Add("SubjectPassword", sub.TicketCode);
                        PostDataJson.Add("OriginalImageFilename", imageFileName);
                        PostDataJson.Add("ThumbnailBase64", Convert.ToBase64String(buffer));
                        PostDataJson.Add("ThumbnailMd5", md5hash);


                        JsonWriter _writerPS = new JsonWriter();
                        PostDataJson.Write(_writerPS);
                        string postData = _writerPS.ToString().Replace("\\/", "/");


                        //
                        if (useUploadQueue)
                        {
                            ImageUploadItem imageUploadItem = new ImageUploadItem();
                            imageUploadItem.ImageName = imageFileName;
                            imageUploadItem.Url = Path.Combine(apiurl, "CreateImage");
                            imageUploadItem.GalleryGuid = onlineGallery.GalleryCode;
                            imageUploadItem.SubjectPassword = sub.TicketCode;
                            //imageUploadItem.ThumbnailMd5 = md5hash;
                            imageUploadItem.Method = "POST";
                            imageUploadItem.HttpHeaders.Add(new ImageUploadHeader("JCart-Api-Key", apikey));



                            imageUploadJob.images.Add(imageUploadItem);
                            logger.Info("seeting ExistsInOnlineGallery Flag for: " + img.ImageFileName);

                            img.ExistsInOnlineGallery = true;
                            img.Subject.FlagForMerge = true;
                            flowProject.FlowProjectDataContext.SubmitChanges();

                            ouh.ImageCount = imageUploadJob.images.Count();
                            this.flowMasterDataContext.SubmitChanges();
                            this.flowMasterDataContext.RefreshOrderUploadHistoryList();

                            string DataFile = System.IO.Path.Combine(uploadJob, "JobInfoTemp.xml");
                            if (File.Exists(DataFile)) File.Delete(DataFile);
                            var serializer = new XmlSerializer(typeof(ImageUploadJob));
                            using (StreamWriter writer2 = new StreamWriter(DataFile))
                            {
                                serializer.Serialize(writer2, imageUploadJob);
                            }


                        }
                        else
                        {

                            //
                            JsonObject ImageJson = OnlineApiHelpers.GetJson(Path.Combine(apiurl, "CreateImage"), postData);
                            if (ImageJson != null)
                            {
                                img.ExistsInOnlineGallery = true;
                                img.Subject.FlagForMerge = true;
                                flowProject.FlowProjectDataContext.SubmitChanges();
                            }
                            else
                            {
                                //Failed to upload Image
                                JsonObject ImageExistsJson = OnlineApiHelpers.GetJson(Path.Combine(apiurl, "ImageExistsWithMd5"), md5hash);
                                if (ImageJson == null)
                                {
                                    //image is not already up
                                }
                            }
                        }
                        this.flowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                        {
                            this.flowProject.UpdateGalleryCounts();
                        }));

                    }
                }


                


            }

            if (useUploadQueue)
            {
                ouh.ImageCount = imageUploadJob.images.Count();
                this.flowMasterDataContext.SubmitChanges();
                this.flowMasterDataContext.RefreshOrderUploadHistoryList();

                string DataFile = System.IO.Path.Combine(uploadJob, "JobInfo.xml");
                
                var serializer = new XmlSerializer(typeof(ImageUploadJob));
                using (StreamWriter writer2 = new StreamWriter(DataFile))
                {
                    serializer.Serialize(writer2, imageUploadJob);
                }

                try
                {
                    string tempFile = System.IO.Path.Combine(uploadJob, "JobInfoTemp.xml");
                    File.Delete(tempFile);
                }
                catch (Exception e) { }
            }

            flowProject.FlowProjectDataContext.SubmitChanges();
            return true;
        }

        private XmlDocument GeneratePricesheet(bool isGreenScreen, Dictionary<string, string> bgsMd5)
        {
            string flowCatalogID = "-" + this.flowProject.FlowCatalog.FlowCatalogGuid.ToString().Substring(0, 5);
            XmlDocument doc = new XmlDocument();
            XmlElement program = (XmlElement)doc.AppendChild(doc.CreateElement("Program"));
            program.AppendChild(doc.CreateElement("Description")).InnerText = this.flowProject.FlowCatalog.FlowCatalogDesc + flowCatalogID;

            if (this.flowProject.FlowCatalog.TaxRate > 0)
            {
                XmlElement tax = doc.CreateElement("Tax");
                tax.AppendChild(doc.CreateElement("Include")).InnerText = "true";
                tax.AppendChild(doc.CreateElement("Rate")).InnerText = ((this.flowProject.FlowCatalog.TaxRate) * ((decimal).01)).ToString();
                program.AppendChild(tax);
            }

            if (onlineGallery.ShippingFee != null && onlineGallery.ShippingFee > 0)
            {
                XmlElement shippingFee = (doc.CreateElement("ShippingFee"));
                shippingFee.InnerText = onlineGallery.ShippingFee.ToString();
                program.AppendChild(shippingFee);
            }
            logger.Info("generating pricesheet - AnyXPackages");
            XmlElement anyXpackages = doc.CreateElement("AnyXPackages");
            program.AppendChild(anyXpackages);
            foreach (ProductPackage pp in this.flowProject.FlowCatalog.ProductPackages.Where(pp=>pp.IsAnyXPackage == true))
            {
                if (pp.ShowInOnlinePricesheet == false)
                    continue;
                XmlElement anyXpackage = doc.CreateElement("AnyXPackage");
                anyXpackages.AppendChild(anyXpackage);
                anyXpackage.AppendChild(doc.CreateElement("Description")).InnerText = pp.ProductPackageDesc;
                anyXpackage.AppendChild(doc.CreateElement("Price")).InnerText = pp.Price.ToString();
                anyXpackage.AppendChild(doc.CreateElement("Map")).InnerText = pp.Map + flowCatalogID;
                if (!String.IsNullOrEmpty(pp.DigitalDownload) && pp.DigitalDownload != "None")
                    anyXpackage.AppendChild(doc.CreateElement("IncludeDigitalDownload")).InnerText = "true";
                anyXpackage.AppendChild(doc.CreateElement("CatalogImageURL")).InnerText = pp.ImageURL;
                anyXpackage.AppendChild(doc.CreateElement("SheetCount")).InnerText = pp.MaxProductsAllowed.ToString();
            }

           
            XmlElement packages = doc.CreateElement("Packages");
            program.AppendChild(packages);
            foreach (ProductPackage pp in this.flowProject.FlowCatalog.ProductPackages.Where(pp=>pp.IsAnyXPackage == false && pp.IsALaCarte == false))
            {
                logger.Info("generating pricesheet - Packages: " + pp.ProductPackageDesc);
                if (pp.ShowInOnlinePricesheet == false)
                    continue;
                XmlElement package = doc.CreateElement("Package");
                packages.AppendChild(package);
                package.AppendChild(doc.CreateElement("Description")).InnerText = pp.ProductPackageDesc;
                package.AppendChild(doc.CreateElement("Price")).InnerText = pp.Price.ToString();
                package.AppendChild(doc.CreateElement("Map")).InnerText = pp.Map + flowCatalogID;
                if(!String.IsNullOrEmpty(pp.DigitalDownload))
                    package.AppendChild(doc.CreateElement("IncludeDigitalDownload")).InnerText = "true";
                package.AppendChild(doc.CreateElement("CatalogImageURL")).InnerText = pp.ImageURL;
                logger.Info("generating pricesheet - Package Units");
                foreach (ProductPackageComposition unit in pp.ProductPackageCompositions)
                {
                    logger.Info("generating pricesheet - Package Unit: " + unit.ImageQuixProduct.Label);
                    XmlElement unitXml = doc.CreateElement("Unit");
                    package.AppendChild(unitXml);
                    unitXml.AppendChild(doc.CreateElement("Description")).InnerText = unit.ImageQuixProduct.Label;
                    if (!string.IsNullOrEmpty(unit.ImageQuixProduct.ResourceURL))
                        unitXml.AppendChild(doc.CreateElement("Map")).InnerText = unit.ImageQuixProduct.ResourceURL + flowCatalogID;
                    else
                        unitXml.AppendChild(doc.CreateElement("Map")).InnerText = unit.ImageQuixProduct.SKU + flowCatalogID;
                    unitXml.AppendChild(doc.CreateElement("Quantity")).InnerText = unit.Quantity.ToString();
                    unitXml.AppendChild(doc.CreateElement("CatalogImageURL")).InnerText = "";//unit.ImageURL;
                }
            }

            logger.Info("generating pricesheet - Units");
            XmlElement units = doc.CreateElement("Units");
            program.AppendChild(units);
            foreach (ProductPackage pp in this.flowProject.FlowCatalog.ProductPackages.Where(pp=>pp.IsALaCarte == true))
            {
                logger.Info("generating pricesheet - Unit: " + pp.ProductPackageDesc);
                if (pp.ShowInOnlinePricesheet == false)
                    continue;
                XmlElement unit = doc.CreateElement("Unit");
                units.AppendChild(unit);
                unit.AppendChild(doc.CreateElement("Description")).InnerText = pp.ProductPackageDesc;
                unit.AppendChild(doc.CreateElement("Price")).InnerText = pp.Price.ToString();
                unit.AppendChild(doc.CreateElement("Map")).InnerText = pp.Map + flowCatalogID;
                unit.AppendChild(doc.CreateElement("CatalogImageURL")).InnerText = pp.ImageURL;
                unit.AppendChild(doc.CreateElement("UnitType")).InnerText = "Sheet";
            }

            //Retouch
            logger.Info("generating pricesheet - Retouch");
            //throw new Exception("Just a test");
            if (!string.IsNullOrEmpty(onlineGallery.RetouchOptionID))
            {
                int retouchID = Int32.Parse(onlineGallery.RetouchOptionID);
                FlowCatalogOption o = flowProject.FlowCatalog.FlowCatalogImageOptionList.FirstOrDefault(p => p.IQPrimaryKey == retouchID);
                if (o != null)
                {
                    XmlElement prodOptions = doc.CreateElement("ProductOptions");
                    program.AppendChild(prodOptions);

                    XmlElement prodOption = doc.CreateElement("ProductOption");
                    prodOptions.AppendChild(prodOption);
                    prodOption.AppendChild(doc.CreateElement("Description")).InnerText = o.ImageQuixCatalogOption.Label;
                    prodOption.AppendChild(doc.CreateElement("Price")).InnerText = o.Price.ToString();
                    prodOption.AppendChild(doc.CreateElement("Map")).InnerText = o.IQPrimaryKey.ToString() + flowCatalogID;
                    prodOption.AppendChild(doc.CreateElement("OptionType")).InnerText = "Retouching";
                }
           

            }



            if (isGreenScreen)
            {
                logger.Info("generating pricesheet - Backgrounds");
                foreach (GreenScreenBackground bg in this.flowProject.FlowMasterDataContext.GreenScreenBackgrounds)
                {
                    logger.Info("generating pricesheet - Background: " + bg.DisplayName);
                    XmlElement background = doc.CreateElement("Background");
                   
                    background.AppendChild(doc.CreateElement("Description")).InnerText = bg.DisplayName;

                    background.AppendChild(doc.CreateElement("Map")).InnerText = bg.DisplayName;
                    if (bg.IsPremium)
                        background.AppendChild(doc.CreateElement("Price")).InnerText = this.flowMasterDataContext.PreferenceManager.Edit.PremiumBackgroundPrice;
                    else
                        background.AppendChild(doc.CreateElement("Price")).InnerText = "0";
                    try
                    {
                        background.AppendChild(doc.CreateElement("BackgroundMd5")).InnerText = bgsMd5[bg.FullPath];
                    }
                    catch (Exception e)
                    {
                        logger.Error("FAILED TO FIND BACKGROUND IN MD5 HASH LIST - " + bg.DisplayName);
                        logger.ErrorException(e.Message,e);
                    }
                    program.AppendChild(background);
                }
            }
            return doc;
        }

    }
}
