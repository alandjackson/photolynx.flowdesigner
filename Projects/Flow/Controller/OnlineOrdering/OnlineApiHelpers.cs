﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetServ.Net.Json;
using NLog;
using System.Net;
using System.IO;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows;
using System.Threading;

namespace Flow.Controller.OnlineOrdering
{
    public class OnlineApiHelpers
    {

         private static Logger logger = LogManager.GetCurrentClassLogger();
         public string apikey { get; set; }

         public OnlineApiHelpers(string _apikey)
         {
             apikey = _apikey;
         }
         public JsonObject GetJson(string uri, string postData)
        {
            return GetJson(uri, postData, "application/json");
        }
        public JsonObject GetJson(string uri, string postData, string contentType)
        {

            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls;

            logger.Info("----------------Begin API Call-----------------------"); 
            logger.Info("URL: " + uri);
            logger.Info("KEY: " + apikey);
            if(postData.Length < 1048)
                logger.Info("DATA: " + postData);
            logger.Info("-----------------END API Call------------------------");

            int tcount = 0;

            var result = "";


            while (tcount < 20)
            {
                try
                {
                    HttpWebRequest rq = (HttpWebRequest)WebRequest.Create(uri);
                    rq.Headers.Add("JCart-Api-Key", apikey);
                    rq.ContentType = "application/json";
                    rq.ContentLength = postData.Length;
                    rq.Timeout = (120000);//try for 120 seconds
                    rq.Method = "POST";


                    ServicePointManager.ServerCertificateValidationCallback = delegate(
                    Object obj, System.Security.Cryptography.X509Certificates.X509Certificate certificate, System.Security.Cryptography.X509Certificates.X509Chain chain,
                    System.Net.Security.SslPolicyErrors errors)
                            {
                                return (true);
                            };


                    StreamWriter stOut = new
                    StreamWriter(rq.GetRequestStream(),
                    System.Text.Encoding.ASCII);
                    stOut.Write(postData);
                    stOut.Close();


                    HttpWebResponse rs = null;
                    int tryCount = 0;
                    while (tryCount < 1)
                    {
                        try
                        {
                            rs = (HttpWebResponse)rq.GetResponse();
                            StreamReader reader = new StreamReader(rs.GetResponseStream());
                            string jsonText = reader.ReadToEnd();

                            reader.Close();
                            rs.Close();

                            if (jsonText == "null")
                                return null;
                            if (jsonText == "true")
                            {
                                JsonObject returnObj = new JsonObject();
                                returnObj.Add("results", true);
                                return returnObj;
                            }
                            if (jsonText == "false")
                            {
                                JsonObject returnObj = new JsonObject();
                                returnObj.Add("results", false);
                                return returnObj;
                            }
                            if (jsonText.StartsWith("{"))
                            {
                                JsonParser parserPS = new JsonParser(new StringReader(jsonText), true);
                                return parserPS.ParseObject();
                            }
                            if (jsonText.StartsWith("["))
                            {
                                JsonParser parserPS = new JsonParser(new StringReader(jsonText), true);
                                JsonObject order = new JsonObject();
                                order.Add("order", parserPS.ParseArray());
                                return order;
                            }
                            //return jsonText;
                        }
                        catch (Exception e)
                        {

                            tryCount++;
                            logger.Error("Error posting to gallery: " + e.Message);
                            logger.Info("DATA: " + postData);

                        }
                    }
                    tcount = 21;
                }
                catch (Exception ex)
                {
                    Thread.Sleep(100);
                    tcount++;
                }
            }

            return null;
        }

        

        public HttpStatusCode PutToUri(string uri, string un, string pw)
        {
            logger.Info("q " + uri);
            HttpWebRequest rq = (HttpWebRequest)WebRequest.Create(uri);
            rq.Credentials = new NetworkCredential(un, pw);
            rq.Headers.Add("x-iq-token", "A15M876TJ8");
            rq.Headers.Add("x-iq-user", un);
            rq.Timeout = 30000;//only try for 30 seconds
            rq.Method = "PUT";
            HttpWebResponse rs;
            try
            {
                rs = (HttpWebResponse)rq.GetResponse();
            }
            catch (WebException e)
            {
                if (e.Message.Contains("304"))
                    return HttpStatusCode.NotModified;

                return HttpStatusCode.Unauthorized;
            }

            HttpStatusCode code = rs.StatusCode;
            rs.Close();
            return code;

        }

        public HttpStatusCode PostToUri(string loginUri, string un, string pw)
        {
            logger.Info("r " + loginUri);
            HttpWebRequest rq = (HttpWebRequest)WebRequest.Create(loginUri);
            rq.Credentials = new NetworkCredential(un, pw);
            rq.Headers.Add("x-iq-token", "A15M876TJ8");
            rq.Headers.Add("x-iq-user", un);
            rq.Timeout = 30000;//only try for 30 seconds
            rq.Method = "GET";
            HttpWebResponse rs;
            try
            {
                rs = (HttpWebResponse)rq.GetResponse();
            }
            catch (WebException e)
            {
                if (e.Message.Contains("304"))
                    return HttpStatusCode.NotModified;

                return HttpStatusCode.Unauthorized;
            }

            HttpStatusCode code = rs.StatusCode;
            rs.Close();
            return code;

        }

        public static Image resizeImage(Image imgToResize, int iSize)
        {
            
            int sourceWidth = imgToResize.Width;
            int sourceHeight = imgToResize.Height;
            System.Drawing.Size size = new System.Drawing.Size(iSize, iSize);
            if (sourceWidth > sourceHeight)
            {
                float multiplier = ((float)iSize / (float)sourceWidth);
                size.Width = iSize;
                size.Height = Convert.ToInt32((sourceHeight * multiplier));
            }
            if (sourceWidth < sourceHeight)
            {
                float mulitplier = ((float)iSize / (float)sourceHeight);
                size.Height = iSize;
                size.Width = Convert.ToInt32((sourceWidth * mulitplier));
            }
            float nPercent = 0;
            float nPercentW = 0;
            float nPercentH = 0;

            nPercentW = ((float)size.Width / (float)sourceWidth);
            nPercentH = ((float)size.Height / (float)sourceHeight);

            if (nPercentH < nPercentW)
                nPercent = nPercentH;
            else
                nPercent = nPercentW;

            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            Bitmap b = new Bitmap(destWidth, destHeight,imgToResize.PixelFormat);
            Graphics g = Graphics.FromImage((Image)b);
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;

            g.DrawImage(imgToResize, 0, 0, destWidth, destHeight);
            g.Dispose();
            imgToResize.Dispose();
            return (Image)b;
        }

        public static Int32Rect getCropArea(Bitmap iSrc, int rotation, double CropL, double CropT, double CropW, double CropH)
        {


            double width = iSrc.Width;
            double height = iSrc.Height;

            int cropAreaL = (int)(CropL * width);
            int cropAreaT = (int)(CropT * height);
            int cropAreaW = (int)(CropW * width);
            int cropAreaH = (int)(CropH * height);

            if (cropAreaL + cropAreaW > width)
                cropAreaW = (int)width - cropAreaL;
            if (cropAreaT + cropAreaH > height)
                cropAreaH = (int)height - cropAreaT;

            if (rotation == 90)
            {
                return new Int32Rect(
                    cropAreaT,
                    ((int)width - cropAreaL) - cropAreaW,
                    cropAreaH,
                    cropAreaW
                    );
            }

            if (rotation == 270)
            {
                return new Int32Rect(
                    ((int)height - cropAreaT) - cropAreaH,
                    cropAreaL,
                    cropAreaH,
                    cropAreaW
                    );
            }

            return new Int32Rect(
                cropAreaL,
                cropAreaT,
                cropAreaW,
                cropAreaH
                );
        }

      public string EscapeForJson(string s) {

          s = s.Replace("<", "\\u003c");
          s = s.Replace(">", "\\u003e");
          s = s.Replace("\"", "&quot;");
          return s;
        }

        public static byte[] ReadToEnd(System.IO.Stream stream)
        {
            long originalPosition = stream.Position;
            stream.Position = 0;

            try
            {
                byte[] readBuffer = new byte[4096];

                int totalBytesRead = 0;
                int bytesRead;

                while ((bytesRead = stream.Read(readBuffer, totalBytesRead, readBuffer.Length - totalBytesRead)) > 0)
                {
                    totalBytesRead += bytesRead;

                    if (totalBytesRead == readBuffer.Length)
                    {
                        int nextByte = stream.ReadByte();
                        if (nextByte != -1)
                        {
                            byte[] temp = new byte[readBuffer.Length * 2];
                            Buffer.BlockCopy(readBuffer, 0, temp, 0, readBuffer.Length);
                            Buffer.SetByte(temp, totalBytesRead, (byte)nextByte);
                            readBuffer = temp;
                            totalBytesRead++;
                        }
                    }
                }

                byte[] buffer = readBuffer;
                if (readBuffer.Length != totalBytesRead)
                {
                    buffer = new byte[totalBytesRead];
                    Buffer.BlockCopy(readBuffer, 0, buffer, 0, totalBytesRead);
                }
                return buffer;
            }
            finally
            {
                stream.Position = originalPosition;
            }
        }
    
    }
}
