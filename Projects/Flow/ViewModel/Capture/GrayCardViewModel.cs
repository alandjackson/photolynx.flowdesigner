﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using Flow.Lib;
using System.Drawing;
using System.Windows;
using Flow.Lib.Preferences;
using StructureMap;
using Flow.Controller;
using Flow.View.Capture;

namespace Flow.ViewModel.Capture
{
    public class GrayCardViewModel : INotifyPropertyChanged
    {
        CapturePreferences CapturePreferences { get; set; }
        GrayCardPanel View { get; set; }

        public GrayCardViewModel(GrayCardPanel pnl) 
            : this (pnl, null)
        {
            
        }

        public GrayCardViewModel(GrayCardPanel pnl, CapturePreferences pref)
        {
            View = pnl;
            this.PropertyChanged += new PropertyChangedEventHandler(GrayCardViewModel_PropertyChanged);
            CapturePreferences = ObjectFactory.GetInstance<FlowController>()
                .ProjectController.FlowMasterDataContext.PreferenceManager.Capture ??
                new CapturePreferences();
            
        }

        void GrayCardViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "AverageBrightness":
                    UpdateBarPosition();
                    break;
                case "AverageColor":
                    UpdateColorRecommendation();
                    break;
                case "BarPosition":
                    View.UpdateBarPosGumballs(BarPosition);
                    UpdateRecommendation();
                    break;
                case "MainImage":
                    UpdateAverageBrightness();
                    break;

            }
        }

        protected FlowImage _mainImage = null;
        public FlowImage MainImage
        {
            get { return _mainImage; }
            set
            {
                _mainImage = value;

                MainImageSource = _mainImage == null ? null : _mainImage.ToBitmapImage();

                if (MainBitmap != null)
                    MainBitmap.Dispose();
                MainBitmap = _mainImage == null ? null : _mainImage.ToBitmap();
                OnPropertyChanged("MainImage");
            }
        }

        protected Bitmap _mainBitmap = null;
        public Bitmap MainBitmap
        {
            get { return _mainBitmap; }
            set
            {
                _mainBitmap = value;
                OnPropertyChanged("MainBitmap");
            }
        }

        protected object _mainImageSource = null;
        public object MainImageSource
        {
            get { return _mainImageSource; }
            set
            {
                _mainImageSource = value;
                OnPropertyChanged("MainImageSource");
            }
        }

        protected int _averageBrightness = 0;
        public int AverageBrightness
        {
            get { return _averageBrightness; }
            set
            {
                if (_averageBrightness == value) return;
                _averageBrightness = value;
                OnPropertyChanged("AverageBrightness");
            }
        }

        protected Color _averageColor;
        public Color AverageColor
        {
            get { return _averageColor; }
            set
            {
                if (_averageColor == value) return;
                _averageColor = value;
                OnPropertyChanged("AverageColor");
            }
        }

        protected int _barPosition = -99;
        public int BarPosition
        {
            get { return _barPosition; }
            set
            {
                if (_barPosition == value) return;
                _barPosition = value;
                OnPropertyChanged("BarPosition");
            }
        }

        protected string _recommendation = "Click and drag to select the gray card area.";
        public string Recommendation
        {
            get { return _colorRecommendation; }
            set
            {
                if (_colorRecommendation == value) return;
                _colorRecommendation = value;
                OnPropertyChanged("Recommendation");
            }
        }

        protected string _colorRecommendation = "";
        public string ColorRecommendation
        {
            get { return _colorRecommendation; }
            set
            {
                if (_colorRecommendation == value) return;
                _colorRecommendation = value;
                OnPropertyChanged("ColorRecommendation");
            }
        }

        protected Rect _imageAreaSelected;
        public Rect ImageAreaSelected
        {
            get { return _imageAreaSelected; }
            set
            {
                _imageAreaSelected = value;
                UpdateAverageBrightness();
            }
        }

        protected void UpdateAverageBrightness()
        {
            if (MainBitmap == null) return;

            Rect pxArea = ImageAreaSelected;
            pxArea.Scale(MainBitmap.Width, MainBitmap.Height);

            var aveInfo = MainBitmap.GetAverageColorInfo(pxArea.ToRectangle());
            AverageBrightness = aveInfo.AverageBrightness;
            AverageColor = aveInfo.AverageColor;
        }

        protected void UpdateBarPosition()
        {
            int target;
            if (!Int32.TryParse(CapturePreferences.MrGrayTargetBrightness, out target))
                throw new ApplicationException("Unable to parse mr gray target brightness (int)");
            int tolerance;
            if (!Int32.TryParse(CapturePreferences.MrGrayTolerance, out tolerance))
                throw new ApplicationException("Unable to parse mr gray tolerance (int)");

            int diff = AverageBrightness - target;
            BarPosition = Clamp((int)((double)diff / tolerance), -4, 4);

            //float percent;
            //if (AverageBrightness <= target)
            //{
            //    int diff = target - AverageBrightness;
            //    percent = (float)diff / target * -1;
            //}
            //else
            //{
            //    int diff = AverageBrightness - target;
            //    percent = (float)diff / (765 - target);
            //}

            //BarPosition = Clamp((int)(percent * 8 / tolerance * 10), -4, 4);

        }

        protected int Clamp(int v, int min, int max)
        {
            if (v < min)
                return min;
            if (v > max)
                return max;
            return v;
        }

        protected void UpdateColorRecommendation()
        {
            if (!CapturePreferences.MrGrayShowRGBWarning) return;

            int rgbWarningLevel;
            if (!Int32.TryParse(CapturePreferences.MrGrayRGBWarningLevel, out rgbWarningLevel))
                throw new ApplicationException("Unable to parse MrGrayRGBWarningLevel (int)");

            int min = Min((int)AverageColor.R, (int)AverageColor.G, (int)AverageColor.B);
            int max = Max((int)AverageColor.R, (int)AverageColor.G, (int)AverageColor.B);
            if (max - min > rgbWarningLevel)
                ColorRecommendation = "Color balance is out of tolerance, perform a white balance with your camera";
            else
                ColorRecommendation = "";
        }

        protected int Min(params int[] values)
        {
            if (values.Length < 1)
                throw new ArgumentException("Min must be passed at least one value");
            if (values.Length == 1)
                return values[0];

            int min = values[0];
            for (int i = 1; i < values.Length; i++)
                if (values[i] < min)
                    min = values[i];

            return min;
        }

        protected int Max(params int[] values)
        {
            if (values.Length < 1)
                throw new ArgumentException("Min must be passed at least one value");
            if (values.Length == 1)
                return values[0];

            int max = values[0];
            for (int i = 1; i < values.Length; i++)
                if (values[i] > max)
                    max = values[i];

            return max;
        }

        protected void UpdateRecommendation()
        {
            string adjustMethod = CapturePreferences.MrGrayAdjustmentMethod;


            if (BarPosition == 0)
                Recommendation = "Exposure is GOOD.";
            else if (BarPosition < 0 && adjustMethod == "Move Lights")
                Recommendation = "Move Lights FORWARD.";
            else if (BarPosition < 0 && adjustMethod == "Change Camera F-Stop")
                Recommendation = "INCREASE the F-Stop";
            else if (BarPosition > 0 && adjustMethod == "Move Lights")
                Recommendation = "Move Lights BACK.";
            else if (BarPosition > 0 && adjustMethod == "Change Camera F-Stop")
                Recommendation = "DECREASE the F-Stop.";
            else
                throw new ApplicationException("Unknown mr gray adjust method: " + adjustMethod);
        }



        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }

        #endregion

    }

    public static class RectUtils
    {
        public static Rectangle ToRectangle(this Rect r)
        {
            return new Rectangle((int)r.X, (int)r.Y, (int)r.Width, (int)r.Height);
        }
    }

    public class AverageColorInfo
    {
        public int AverageBrightness { get; set; }
        public Color AverageColor { get; set; }
    }

    public static class BitmapUtils
    {
        public static AverageColorInfo GetAverageColorInfo(this Bitmap bitmap, Rectangle rect)
        {
            double ave_brightness = 0;
            double ave_r = 0;
            double ave_g = 0;
            double ave_b = 0;
            int num_pixels = rect.Width * rect.Height;

            for (int x = rect.X; x < rect.Right; x++)
            {
                for (int y = rect.Y; y < rect.Bottom; y++)
                {
                    // Make sure rectangle point is withing the bitmap bounds
                    if (x < 0 || x >= bitmap.Width || y < 0 || y >= bitmap.Height)
                    {
                        continue;
                    }

                    Color pixel_color = bitmap.GetPixel(x, y);
                    int pixel_brightness = pixel_color.R + pixel_color.G + pixel_color.B;
                    ave_brightness += (double)pixel_brightness / (double)num_pixels;
                    ave_r += (double)pixel_color.R / (double)num_pixels;
                    ave_g += (double)pixel_color.G / (double)num_pixels;
                    ave_b += (double)pixel_color.B / (double)num_pixels;
                }
            }

            return new AverageColorInfo() 
            { 
                AverageBrightness = (int) ave_brightness,
                AverageColor = Color.FromArgb((int)ave_r, (int)ave_g, (int)ave_b) 
            };
        }



    }
}
