using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Flow.Controls.ProgressNotificationControl;

namespace Flow
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        #region Private objects
        ProgressNotificationInfo notification1;
        ProgressNotificationInfo notification2;
        ProgressNotificationInfo notification3;
        List<Image> imgList;
        System.Windows.Forms.Timer timer;
        #endregion

        #region Contructor & Destructor
        public Window1()
        {
            InitializeComponent();
            pnc1.Visibility = Visibility.Collapsed;
        }

        ~Window1()
        {
            notification1 = null;
            notification2 = null;
            notification3 = null;
            imgList = null;
            timer = null;
        }
        #endregion

        #region Event methods
        private void btnNewProjectWizard_Click(object sender, RoutedEventArgs e)
        {
            showNewProjectWizard();
        }

        private void btnRunTests_Click(object sender, RoutedEventArgs e)
        {
            performUnitTests();
        }

        private void btnShowHideNotifications_Click(object sender, RoutedEventArgs e)
        {
            pnc1.Visibility = (pnc1.Visibility == Visibility.Visible ? Visibility.Collapsed : Visibility.Visible);
        }

        private void btnAction1_Click(object sender, RoutedEventArgs e)
        {
            createNotification1();
        }

        private void btnAction2_Click(object sender, RoutedEventArgs e)
        {
            createNotification2();
        }

        private void btnAction3_Click(object sender, RoutedEventArgs e)
        {
            createNotification3();
        }

        private void btnRemoveAllNotifications_Click(object sender, RoutedEventArgs e)
        {
            killNotification1();
            killNotification2();
            killNotification3();

            // Alternatively, this method can be called to remove all notifications
            // but it does not updates the client interface, only the control.
            //pnc1.RemoveAllNotifications();
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            if (notification2 != null)
            {
                // increase progress bar value
                notification2.ProgressBarValue += 1;

                // set thumb image at every 20 per cent of the progress bar
                if (notification2.ProgressBarValue % 20 == 0 && Convert.ToInt32(Math.Ceiling(notification2.ProgressBarValue / 20)) <= imgList.Count)
                    notification2.ThumbsImage = imgList[Convert.ToInt32(Math.Ceiling(notification2.ProgressBarValue / 20)) - 1];

                // update notification
                pnc1.UpdateNotification(notification2);

                // clean up memory when progress is finished
                if (notification2.ProgressBarValue >= 100)
                {
                    killNotification2();
                }
            }
        }
        #endregion

        #region Private methods
        private void createNotification1()
        {
            try
            {
                // Create a notification
                if (notification1 == null)
                {
                    notification1 = new ProgressNotificationInfo();
                    notification1.StatusTextTop = "Action 1";
                    notification1.StatusTextBottom = "Doing something...";

                    // create image from application's resource to be set as notification's icon
                    Image img = Util.LoadImageFromResource("images/camera.png");
                    notification1.IconImage = img;

                    pnc1.CreateNotification(notification1);
                    pnc1.Visibility = Visibility.Visible;
                    btnAction1.Content = "Kill Action 1";
                }
                else
                {
                    killNotification1();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void createNotification2()
        {
            try
            {
                // Create a notification
                if (notification2 == null)
                {
                    notification2 = new ProgressNotificationInfo();
                    notification2.ProgressBarValue = 0;
                    notification2.StatusTextBottom = "Please wait a while...";

                    // create image from application's resource to be set as notification's icon
                    Image img = Util.LoadImageFromResource("images/camera_edit.png");
                    notification2.IconImage = img;

                    pnc1.CreateNotification(notification2);
                    pnc1.Visibility = Visibility.Visible;
                    btnAction2.Content = "Kill Action 2";

                    // simulate processing
                    SimulateProcessUsingProgressBar();
                }
                else
                {
                    killNotification2();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void createNotification3()
        { 
            try
            {
                // Create a notification
                if (notification3 == null)
                {
                    notification3 = new ProgressNotificationInfo();
                    notification3.StatusTextTop = "Action 3";
                    notification3.StatusTextBottom = "Doing another something...";

                    // create image from application's resource to be set as notification's icon
                    Image img = Util.LoadImageFromResource("images/camera_link.png");
                    notification3.IconImage = img;

                    pnc1.CreateNotification(notification3);
                    pnc1.Visibility = Visibility.Visible;
                    btnAction3.Content = "Kill Action 3";
                }
                else
                {
                    killNotification3();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void killNotification1()
        {
            if (notification1 != null)
            {
                pnc1.RemoveNotification(notification1);
                notification1 = null;
                btnAction1.Content = "Fire Action 3";
            }
        }

        private void killNotification2()
        {
            if (notification2 != null)
            {
                pnc1.RemoveNotification(notification2);
                notification2 = null;
                imgList = null;
                if (timer != null)
                    timer.Stop();
                timer = null;
                btnAction2.Content = "Fire Action 2";
            }
        }

        private void killNotification3()
        {
            if (notification3 != null)
            {
                pnc1.RemoveNotification(notification3);
                notification3 = null;
                btnAction3.Content = "Fire Action 3";
            }
        }

        private void SimulateProcessUsingProgressBar()
        {
            imgList = new List<Image>();
            imgList.Add(Util.LoadImageFromResource("images/android1.png"));
            imgList.Add(Util.LoadImageFromResource("images/M2.jpg"));
            imgList.Add(Util.LoadImageFromResource("images/cubes.jpg"));
            imgList.Add(Util.LoadImageFromResource("images/android2.png"));

            timer = new System.Windows.Forms.Timer();
            timer.Interval = 100;
            timer.Tick += new EventHandler(timer_Tick);
            timer.Start();
        }

        private void performUnitTests()
        {
            // Kill all notifications if exist
            killNotification1();
            killNotification2();
            killNotification3();

            // Create all notifications
            createNotification1();
            createNotification2();
            createNotification3();

            // Update notification 1 setting its bottom status text
            if (notification1 != null)
            {
                notification1.StatusTextBottom = "Doing another action";
                pnc1.UpdateNotification(notification1);
            }

            // Update notification 3 setting its thumb image
            if (notification3 != null)
            {
                notification3.ThumbsImage = imgList[3];
                pnc1.UpdateNotification(notification3);
            }

            // Update notification 1 setting its thumb image
            if (notification1 != null)
            {
                notification1.ThumbsImage = imgList[2];
                pnc1.UpdateNotification(notification1);
            }

            // Update notification 2 setting its bottom status text
            if (notification2 != null)
            {
                notification2.StatusTextBottom = "View the progress bar...";
                pnc1.UpdateNotification(notification2);
            }
        }

        private void showNewProjectWizard()
        {
            // Create new project wizard dialog
            Flow.NewProjectWizard.WizardDialogBox wizard = null;
            try
            {
                wizard = new Flow.NewProjectWizard.WizardDialogBox();
                if ((bool)wizard.ShowDialog())
                {
                    string projectInfoText = "";
                    foreach (NewProjectWizard.ProjectInfo pi in wizard.ProjectSettings.ProjectInfoList)
                        projectInfoText += pi.FieldName + ": " + pi.FieldValue + "\n";

                    MessageBox.Show(projectInfoText, "ProjectSettings.ProjectInfoList", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Wizard Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            finally
            {
                wizard = null;
            }
        }
        #endregion
    }
}
