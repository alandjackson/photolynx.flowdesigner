﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Flow.Schema.LinqModel.DataContext;
using NLog;

namespace Flow.View.Dialogs
{
    /// <summary>
    /// Interaction logic for ProjectVerificationDialog.xaml
    /// </summary>
    public partial class ProjectVerificationDialog : Window
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        private FlowProject _flowProject { get; set; }
        public ProjectVerificationDialog(FlowProject fp)
        {
            _flowProject = fp;
            this.DataContext = _flowProject;
            InitializeComponent();
            
        }

        private void btnContinue_Click(object sender, RoutedEventArgs e)
        {
            logger.Info("Continue button clicked on Project Verification Form");
            DialogResult = true;
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            logger.Info("Cancel button clicked on Project Verification Form");
            DialogResult = false;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            lblTotalSubjects.Content = _flowProject.SubjectCount;
            lblTotalSubjectsPhotographed.Content = _flowProject.SubjectList.Count(s => s.PrimaryImage != null).ToString();
            lblTotalSubjectsNotPhotographed.Content = _flowProject.SubjectList.Count(s => s.PrimaryImage == null).ToString();
            
            //Image Flags
            foreach(ProjectImageType pit in _flowProject.FlowProjectDataContext.ProjectImageTypes)
            {
                StackPanel hor = new StackPanel();
                hor.Orientation = Orientation.Horizontal;

                pnlImageFlags.Children.Add(hor);

                Label lblPit= new Label();
                lblPit.Width = 150;
                lblPit.Content = pit.ImageTypeDesc;
                hor.Children.Add(lblPit);

                Label lblPitCount = new Label();
                lblPitCount.Width = 30;
                lblPitCount.Content = _flowProject.FlowProjectDataContext.SubjectImageTypes.Count(sit=>sit.ProjectImageTypeID == pit.ProjectImageTypeID && sit.IsAssigned == true);
                hor.Children.Add(lblPitCount);
            }


            //GroupFieldData
            if (_flowProject.UniqueClasses != null && _flowProject.UniqueClasses.Count > 0)
            {
                Label title = new Label();
                title.Content = "Classes";
                title.Margin = new Thickness(0, 15, 0, 3);
                title.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
                pnlGroupFieldData.Children.Add(title);

                foreach (string value in _flowProject.UniqueClasses)
                {
                    string c = value;
                    if (string.IsNullOrEmpty(c))
                        c = "<blank>";
                    StackPanel hor = new StackPanel();
                    hor.Orientation = Orientation.Horizontal;

                    pnlGroupFieldData.Children.Add(hor);

                    Label lbl = new Label();
                    lbl.Width = 150;
                    lbl.Content = c;
                    hor.Children.Add(lbl);

                    Label lblcnt = new Label();
                    lblcnt.Width = 30;
                    int cnt = 0;
                    foreach (Subject s in _flowProject.FlowProjectDataContext.Subjects)
                        if (s.SubjectData["Class"].Value != null && s.SubjectData["Class"].Value as string == value) cnt++;
                    lblcnt.Content = cnt;
                    hor.Children.Add(lblcnt);
                }

            }

            if (_flowProject.UniqueTeachers != null && _flowProject.UniqueTeachers.Count > 0)
            {
                Label title = new Label();
                title.Content = "Teachers";
                title.Margin = new Thickness(0, 15, 0, 3);
                title.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
                pnlGroupFieldData.Children.Add(title);

                foreach (string value in _flowProject.UniqueTeachers)
                {
                    string c = value;
                    if (string.IsNullOrEmpty(c))
                        c = "<blank>";
                    StackPanel hor = new StackPanel();
                    hor.Orientation = Orientation.Horizontal;

                    pnlGroupFieldData.Children.Add(hor);

                    Label lbl = new Label();
                    lbl.Width = 150;
                    lbl.Content = c;
                    hor.Children.Add(lbl);

                    Label lblcnt = new Label();
                    lblcnt.Width = 30;
                    int cnt = 0;
                    foreach (Subject s in _flowProject.FlowProjectDataContext.Subjects)
                        if (s.SubjectData["Teacher"].Value != null && s.SubjectData["Teacher"].Value as string == value) cnt++;
                    lblcnt.Content = cnt;
                    hor.Children.Add(lblcnt);
                }

            }
        }

       


    }
}
