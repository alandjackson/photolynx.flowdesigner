﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
using NLog;
using Flow.Lib.Helpers;
using System.Windows.Interop;
using System.Runtime.InteropServices;
using System.ComponentModel;
using System.Threading;

using WPFMediaKit;

namespace Flow.View.Dialogs
{
    /// <summary>
    /// Interaction logic for QuixiMessage.xaml
    /// </summary>
    public partial class CameraDialog : Window
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private string hotFolder {get;set;}


        double CropW;
        double CropH;
        double CropL;
        double CropT;

        Canvas _canvasMask;

        RectangleGeometry _maskTransparent;
        RectangleGeometry _maskVisible;

        public CameraDialog(string PathToHotFolder)
        {
            InitializeComponent();
            hotFolder = PathToHotFolder;

            this.Loaded += new RoutedEventHandler(MainWindow_Loaded);
            this.Closing += new CancelEventHandler(MainWindow_Closing);
            

        }

        void MainWindow_Closing(object sender, CancelEventArgs e)
        {
            
        }

        void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            _canvasMask = new Canvas();
            _canvasMask.Background = new SolidColorBrush(Colors.Black);
            _canvasMask.Opacity = .7;
            if(videoCapDevices.HasItems)
                videoCapDevices.SelectedIndex = 0; 
            ShowMask(); 
        }
        
        public void ShowMask()
        {
            
            
            if (!GridDisplay.Children.Contains(_canvasMask))
            {
                GridDisplay.Children.Add(_canvasMask);
            }

            if (CropH == 0)
                CropH = GridDisplay.ActualHeight - (GridDisplay.ActualHeight / 5);
            CropW = CropH * .8;

            CropL = (GridDisplay.ActualWidth / 2) - (CropW / 2);
            CropT = (GridDisplay.ActualHeight / 2) - (CropH / 2);

            UpdateMask();
        }

        private void UpdateMask()
        {

            _maskTransparent = new RectangleGeometry(new Rect(0, 0, GridDisplay.ActualWidth, GridDisplay.ActualHeight));
            
            Rect cropArea = new Rect(CropL, CropT, CropW, CropH);
            _maskVisible = new RectangleGeometry(cropArea);
            _canvasMask.Clip = new CombinedGeometry(GeometryCombineMode.Exclude, _maskTransparent, _maskVisible);

            GridDisplay.Children.Remove(_canvasMask);
            GridDisplay.Children.Add(_canvasMask);

        }

        public void HideMask()
        {
           
            if (GridDisplay.Children.Contains(_canvasMask))
            {
               
                GridDisplay.Children.Remove(_canvasMask);
               
            }
        }

       

        private void buttonCaptureImage_Click(object sender, RoutedEventArgs e)
        {

            DrawingVisual visual = new DrawingVisual();
            DrawingContext context = visual.RenderOpen();
            VisualBrush elementBrush = new VisualBrush(videoCapElement);
            int w = 1600;
            int h = 1200;
            context.DrawRectangle(elementBrush, null, new Rect(0, 0, w, h));
            context.Close();
            RenderTargetBitmap bitmap = new RenderTargetBitmap(w, h, 0, 0, PixelFormats.Default);
            bitmap.Render(visual);
            BitmapSource image = bitmap;

            //RenderTargetBitmap bmp = new RenderTargetBitmap(1600, 1200, 96, 96, PixelFormats.Default);
            //bmp.Render(videoCapElement);
            //BitmapSource image = bmp;



            CroppedBitmap cimage = new CroppedBitmap(image, new Int32Rect((int)CropL * 5, (int)CropT * 5, (int)CropW * 5, (int)CropH * 5));

            string randomFileName = TicketGenerator.GenerateTicketString(12) + ".jpg";
            string path = System.IO.Path.Combine(hotFolder, randomFileName);

            var jpegEncoder = new JpegBitmapEncoder();
            jpegEncoder.Frames.Add(BitmapFrame.Create(cimage));
            using (var fs = new FileStream(path, FileMode.Create, FileAccess.Write))
            {
                jpegEncoder.Save(fs);
            }

            DialogResult = true;

        }

        private void GridDisplay_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (e.Delta > 0)
            {
                CropH = CropH - 5;
                CropL = CropL + 2.5;
                CropT = CropT + 2.5;
            }
            else if (e.Delta < 0)
            {
                CropH = CropH + 5;
                CropL = CropL - 2.5;
                CropT = CropT - 2.5;
            }
            if (CropL < 0) CropL = 0;
            if (CropT < 0) CropT = 0;
            if (CropL + CropW > GridDisplay.ActualWidth) CropL = GridDisplay.ActualWidth - CropW;
            if (CropT + CropH > GridDisplay.ActualHeight) CropT = GridDisplay.ActualHeight - CropH;

            if (CropH > GridDisplay.ActualHeight)
                CropH = GridDisplay.ActualHeight;

            if (CropH == 0)
                CropH = GridDisplay.ActualHeight - (GridDisplay.ActualHeight / 5);
            CropW = CropH * .8;

            UpdateMask();
        }

       


        private bool leftbuttondown { get; set; }
        private double lastx { get; set; }
        private double lasty { get; set; }
        private void GridDisplay_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            leftbuttondown = true;
            lastx = -1;
        }

        private void GridDisplay_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            leftbuttondown = false;
            lastx = -1;
        }


        private void GridDisplay_MouseMove(object sender, MouseEventArgs e)
        {
            leftbuttondown = e.LeftButton == MouseButtonState.Pressed;
            if (leftbuttondown)
            {
                var pos = this.PointToScreen(Mouse.GetPosition(this));
                if (lastx > 0)
                {
                    CropL = CropL + (pos.X - lastx);
                    CropT = CropT + (pos.Y - lasty);
                }

                lastx = pos.X;
                lasty = pos.Y;

                if (CropL < 0) CropL = 0;
                if (CropT < 0) CropT = 0;
                if (CropL + CropW > GridDisplay.ActualWidth) CropL = GridDisplay.ActualWidth - CropW;
                if (CropT + CropH > GridDisplay.ActualHeight) CropT = GridDisplay.ActualHeight - CropH;

                UpdateMask();
            }
        }

       
    }


}
