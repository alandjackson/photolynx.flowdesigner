﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Flow.Schema.LinqModel.DataContext;
using NLog;

namespace Flow.View.Dialogs
{
    /// <summary>
    /// Interaction logic for LabOrderFormDialog.xaml
    /// </summary>
    public partial class LabOrderFormDialog : Window
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        private FlowProjectDataContext _flowProjectDataContext { get; set; }
        public LabOrderFormDialog(FlowProjectDataContext fpdc)
        {
            _flowProjectDataContext = fpdc;
            this.DataContext = _flowProjectDataContext;
            InitializeComponent();
            
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            logger.Info("Save button clicked on Lab Order Form");
            _flowProjectDataContext.SubmitChanges();
            DialogResult = true;
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            logger.Info("Cancel button clicked on Lab Order Form");
            DialogResult = false;
        }

       


    }
}
