﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
using NLog;

namespace Flow.View.Dialogs
{
    /// <summary>
    /// Interaction logic for QuixiMessage.xaml
    /// </summary>
    public partial class ShowAnImage : Window
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        //returns true if new image is picked, returns false if oldimage is picked
        public ShowAnImage(BitmapImage newImage)
        {
            InitializeComponent();

            imgNew.Source = newImage;
        }

        private void GridA_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
           
            DialogResult = true;
        }

        private void GridB_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            
            DialogResult = false;
        }

        private void buttonSelectA_Click(object sender, RoutedEventArgs e)
        {
            
            DialogResult = true;
        }

        private void buttonSelectB_Click(object sender, RoutedEventArgs e)
        {
           
            DialogResult = false;
        }

        private void Window_KeyUp(object sender, KeyEventArgs e)
        {

                DialogResult = true;

        }

        private void buttonClose_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
