﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
using NLog;

namespace Flow.View.Dialogs
{
    /// <summary>
    /// Interaction logic for QuixiMessage.xaml
    /// </summary>
    public partial class PickAnImage : Window
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        //returns true if new image is picked, returns false if oldimage is picked
        public PickAnImage(BitmapImage newImage, BitmapImage oldImage)
        {
            InitializeComponent();

            imgNew.Source = newImage;
            imgOld.Source = oldImage;
        }

        //public PreviewLayout(BitmapSource layout)
        //{
        //    InitializeComponent();
        //    this.imgLayout.Source = layout;
        //}

 
        private void GridA_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            logger.Info("New Image Picked over Old image");
            DialogResult = true;
        }

        private void GridB_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            logger.Info("Old Image Picked over New image");
            DialogResult = false;
        }

        private void buttonSelectA_Click(object sender, RoutedEventArgs e)
        {
            logger.Info("New Image Picked over Old image");
            DialogResult = true;
        }

        private void buttonSelectB_Click(object sender, RoutedEventArgs e)
        {
            logger.Info("Old Image Picked over New image");
            DialogResult = false;
        }

        private void Window_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.NumPad1 || e.Key == Key.D1 || e.Key == Key.A || e.Key == Key.Enter)
            {
                logger.Info("New Image Picked over Old image");
                DialogResult = true;
            }
            if (e.Key == Key.NumPad2 || e.Key == Key.D2 || e.Key == Key.B || e.Key == Key.Back || e.Key == Key.Delete)
            {
                logger.Info("Old Image Picked over New image");
                DialogResult = false;
            }
        }
    }
}
