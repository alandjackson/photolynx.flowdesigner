﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using NLog;
using Flow.Lib.Preferences;
using Flow.Schema.LinqModel.DataContext;

namespace Flow.View.Dialogs
{
    /// <summary>
    /// Interaction logic for FlowMessageBox.xaml
    /// </summary>
    public partial class Login : Window
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private FlowMasterDataContext FlowMasterDataContext { get; set; }

        public Login(FlowMasterDataContext fmdc)
        {
            FlowMasterDataContext = fmdc;
            InitializeComponent();
           
            logger.Info("Login Dialog");
            txtLoginID.Focus();
        }

       

        private void buttonOkay_Click(object sender, RoutedEventArgs e)
        {
            if (txtLoginID.Text.Length < 1)
            {
                lblRequired.Visibility = Visibility.Visible;
                return;
            }
            FlowMasterDataContext.LoginID = txtLoginID.Text;
            logger.Info("Clicked Okay button on login box");
            DialogResult = true;
        }

        private void buttonCancel_Click(object sender, RoutedEventArgs e)
        {
            logger.Info("Clicked Cancel button on login box");
            DialogResult = false;
        }
    }
}
