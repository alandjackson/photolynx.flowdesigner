﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
using NLog;
using Flow.Schema.LinqModel.DataContext;

namespace Flow.View.Dialogs
{
    /// <summary>
    /// 
    /// </summary>
    public partial class CreateALaCarteProduct : Window
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public ImageQuixProduct Product { get; set; }
        public FlowMasterDataContext FlowMasterDataContext{get;set;}
        //returns true if new image is picked, returns false if oldimage is picked

        public ProductPackage NewProductPackage { get; set; }
        public CreateALaCarteProduct(ImageQuixProduct product, decimal price, FlowMasterDataContext fmdc)
        {
            
            InitializeComponent();
            Product = product;
            txtName.Text = product.Label;
            txtPrice.Text = price.ToString();
            txtMap.Text = product.Map;
            FlowMasterDataContext = fmdc;
            NewProductPackage = new ProductPackage();
            this.DataContext = NewProductPackage;
        }


        private void buttonSave_Click(object sender, RoutedEventArgs e)
        {
            //Create Product
            if (string.IsNullOrEmpty(txtName.Text))
            {
                FlowMessageBox msg = new FlowMessageBox("Missing Value", "You must specify a Name for this product");
                msg.CancelButtonVisible = false;
                msg.ShowDialog();
                return;
            }
            decimal tmpDecimal;
            if (string.IsNullOrEmpty(txtPrice.Text) || !decimal.TryParse(txtPrice.Text, out tmpDecimal))
            {
                FlowMessageBox msg = new FlowMessageBox("Missing Value", "You must specify a Price for this product");
                msg.CancelButtonVisible = false;
                msg.ShowDialog();
                return;
            }
            if (string.IsNullOrEmpty(txtMap.Text))
            {
                FlowMessageBox msg = new FlowMessageBox("Missing Value", "You must specify a Map for this product");
                msg.CancelButtonVisible = false;
                msg.ShowDialog();
                return;
            }
            if (FlowMasterDataContext.ProductPackages.Any(p => p.Map == txtMap.Text))
            {
                FlowMessageBox msg = new FlowMessageBox("Missing Value", "The Map value that you selected is already in use");
                msg.CancelButtonVisible = false;
                msg.ShowDialog();
                return;
            }

            NewProductPackage.ProductPackageDesc = txtName.Text;
            NewProductPackage.Price = decimal.Parse(txtPrice.Text);
            NewProductPackage.Map = txtMap.Text;
            NewProductPackage.IsALaCarte = true;
            NewProductPackage.FlowCatalogID = FlowMasterDataContext.FlowCatalogList.CurrentItem.FlowCatalogID;
            NewProductPackage.FlowCatalog = FlowMasterDataContext.FlowCatalogList.CurrentItem;

            FlowMasterDataContext.ProductPackages.InsertOnSubmit(NewProductPackage);
            FlowMasterDataContext.SubmitChanges();
            NewProductPackage.AddItemToPackage(Product);
            FlowMasterDataContext.SubmitChanges();
            FlowMasterDataContext.FlowCatalogList.CurrentItem.RefreshPackageList();
            DialogResult = true;
        }

        private void buttonCancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

       

        private void chkShowInGallery_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void chkShowInGallery_Unchecked(object sender, RoutedEventArgs e)
        {

        }

       
    }
}
