﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using NLog;

namespace Flow.View.Dialogs
{
    /// <summary>
    /// Interaction logic for FlowMessageBox.xaml
    /// </summary>
    public partial class FlowMessageBox : Window
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public FlowMessageBox(string title, string message)
        {
            InitializeComponent();
            this.Title = title;
            lblTitle.Content = title;
            txtMessage.Text = message;

            logger.Info("Message box shown: {0} - {1}", title, message);
        }

        public void setButtonText(string okayButton, string cancelButton)
        {
            this.buttonOkay.Content = okayButton;
            this.buttonCancel.Content = cancelButton;
        }

        public void setConfirmationText(string confirmationText)
        {
            if (confirmationText != null)
                this.chkConfirmOkay.Content = confirmationText;

            this.chkConfirmOkay.IsChecked = false;
            this.chkConfirmOkay.Visibility = Visibility.Visible;
        }

        public bool CancelButtonVisible
        {
            get { return buttonCancel.Visibility == Visibility.Visible; }
            set
            {
                if (value) 
                    buttonCancel.Visibility = Visibility.Visible;
                else 
                    buttonCancel.Visibility = Visibility.Hidden;
            }
        }

        private void buttonOkay_Click(object sender, RoutedEventArgs e)
        {
            logger.Info("Clicked Okay button on message box");
            DialogResult = true;
        }

        private void buttonCancel_Click(object sender, RoutedEventArgs e)
        {
            logger.Info("Clicked Cancel button on message box");
            DialogResult = false;
        }
    }
}
