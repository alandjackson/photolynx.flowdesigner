﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
//using System.Windows.Shapes;
using System.IO;
using NLog;
using Flow.Lib.Helpers;
using System.Windows.Interop;
using System.Runtime.InteropServices;
using System.ComponentModel;
using System.Threading;

using WPFMediaKit;
using Flow.Schema.LinqModel.DataContext;
using System.Windows.Forms;
using Flow.Lib;
using System.Diagnostics;

namespace Flow.View.Dialogs
{
    /// <summary>
    /// Interaction logic for QuixiMessage.xaml
    /// </summary>
    public partial class ExportRetouch : Window
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private FlowProject curProject {get;set;}

        public NotificationProgressInfo NotificationInfo { get; set; }

        public ExportRetouch(FlowProject proj )
        {
            InitializeComponent();
            curProject = proj;
            this.DataContext = curProject;
            this.Loaded += new RoutedEventHandler(MainWindow_Loaded);
            this.Closing += new CancelEventHandler(MainWindow_Closing);
            

        }

        void MainWindow_Closing(object sender, CancelEventArgs e)
        {
            
        }

        void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            
        }

        private void btnBrowseExport_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new FolderBrowserDialog();
            dialog.SelectedPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            DialogResult result = dialog.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                txtExportPath.Text = dialog.SelectedPath;

            }
        }

        private void btnExport_Click(object sender, RoutedEventArgs e)
        {
            NotificationInfo = new NotificationProgressInfo("Export For Retouch", "Preparing images for export");
            NotificationInfo.Display(true);
            string exactPath = System.IO.Path.Combine(txtExportPath.Text, curProject.FileSafeProjectName);
            if (!Directory.Exists(exactPath))
            {
                Directory.CreateDirectory(exactPath);
            }
            foreach(SubjectImage si in curProject.ImageList)
            {
                string newFile = System.IO.Path.Combine(exactPath, si.ImageFileName);
                if(!File.Exists(newFile))
                    File.Copy(si.ImagePathFullRes, newFile);
            }
            NotificationInfo.Complete("Finished exporting for retouch");

            DialogResult = true;
            if (chkOpenFolder.IsChecked == true && Directory.Exists(exactPath))
                Process.Start(exactPath);
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void btnImport_Click(object sender, RoutedEventArgs e)
        {
            NotificationInfo = new NotificationProgressInfo("Export For Retouch", "Preparing images for import");
            NotificationInfo.Display(true);

            string importFolder = txtImportPath.Text;
            string imageFolder = this.curProject.ImageDirPath;
            List<string> files = Directory.GetFiles(importFolder).ToList();
            foreach (string file in files)
            {
                if (file.ToLower().EndsWith(".jpg") || file.ToLower().EndsWith(".png"))
                {
                    FileInfo fi = new FileInfo(file);
                    SubjectImage si = curProject.ImageList.FirstOrDefault(i => i.ImageFileName.ToLower() == fi.Name.ToLower());
                    if(si == null)
                        si = curProject.ImageList.FirstOrDefault(i => i.ImageFileName.ToLower() == fi.Name.ToLower().Replace("png", "jpg"));

                    string destFile = System.IO.Path.Combine(imageFolder, fi.Name);
                    if (File.Exists(destFile))
                    {
                        File.Delete(destFile);
                        File.Copy(file, destFile);
                    }
                    else
                    {
                        if (file.ToLower().EndsWith(".png"))
                        {
                            string destJpg = System.IO.Path.Combine(imageFolder, fi.Name.Replace(".png", ".jpg"));
                            if (!File.Exists(destJpg)) destJpg = System.IO.Path.Combine(imageFolder, fi.Name.Replace(".png", ".JPG"));
                            if (!File.Exists(destJpg)) destJpg = System.IO.Path.Combine(imageFolder, fi.Name.Replace(".PNG", ".jpg"));
                            if (!File.Exists(destJpg)) destJpg = System.IO.Path.Combine(imageFolder, fi.Name.Replace(".PNG", ".JPG"));
                            if (File.Exists(destJpg))
                            {
                                si = curProject.ImageList.FirstOrDefault(i => i.ImagePathFullRes.ToLower() == destJpg.ToLower());
                                File.Delete(destJpg);
                                File.Copy(file, destFile);
                                si.ImagePath = destFile;
                                si.ImagePathFullRes = destFile;
                                si.ImageFileName = new FileInfo(destFile).Name;
                                si.GreenScreenSettings = null;
                                if (si.IsGroupImage)
                                {
                                    si.GroupImage.ImagePath = destFile;
                                    si.GroupImage.ImageFileName = new FileInfo(destFile).Name;
                                    foreach (SubjectImage subi in si.GroupImage.GetSubjectImageList)
                                    {
                                        subi.ImagePath = destFile;
                                        subi.ImagePathFullRes = destFile;
                                        subi.ImageFileName = new FileInfo(destFile).Name;
                                        subi.GreenScreenSettings = null;
                                        subi.RefreshImage();
                                        subi.Subject.ClearGSCache(Path.Combine(this.curProject.ImageDirPath, "PLCache", "GreenScreen"));
                                    }
                                    si.GroupImage.RefreshImage();
                                }
                                si.Subject.ClearGSCache(Path.Combine(this.curProject.ImageDirPath, "PLCache", "GreenScreen"));
                            }
                        }

                    }

                    if (si != null)
                    {
                        si.RefreshImage();
                        si.Subject.ClearGSCache(Path.Combine(this.curProject.ImageDirPath, "PLCache", "GreenScreen"));
                    }
                }

            }

            NotificationInfo.Complete("Finished importing for retouch");
            this.curProject.FlowProjectDataContext.SubmitChanges();
            this.curProject.FlowMasterDataContext.SubmitChanges();
            DialogResult = true;
        }

        private void btnBrowseImport_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new FolderBrowserDialog();
            dialog.SelectedPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            DialogResult result = dialog.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                txtImportPath.Text = dialog.SelectedPath;
            }
        }
        
        

       
    }


}
