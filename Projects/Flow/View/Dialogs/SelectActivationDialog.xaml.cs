﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;

namespace Flow.View.Dialogs
{
    /// <summary>
    /// Interaction logic for FlowMessageBox.xaml
    /// </summary>
    public partial class SelectActivationDialog : Window
    {
        public SelectActivationDialog(List<string> files)
        {
            InitializeComponent();
            foreach (string file in files)
                cmbActivations.Items.Add(file);
        }

        public string selectedFileName
        {
            get
            {
                return cmbActivations.Items[cmbActivations.SelectedIndex] as string;
            }
        }

        private void buttonOkay_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }




    }
}
