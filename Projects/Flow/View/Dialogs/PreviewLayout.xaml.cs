﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
using NLog;

namespace Flow.View.Dialogs
{
    /// <summary>
    /// Interaction logic for QuixiMessage.xaml
    /// </summary>
    public partial class PreviewLayout : Window
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public PreviewLayout(byte[] imageBytes)
        {
            InitializeComponent();

            BitmapImage source = new BitmapImage();
            source.BeginInit();
            source.StreamSource = new MemoryStream(imageBytes);
            source.EndInit();
            imgLayout.Source = source;
        }

        public PreviewLayout(BitmapSource layout)
        {
            InitializeComponent();
            this.imgLayout.Source = layout;
        }

       

        private void buttonYes_Click(object sender, RoutedEventArgs e)
        {
            logger.Info("Clicked Yes for layout preview");
            DialogResult = true;
        }

        private void buttonNo_Click(object sender, RoutedEventArgs e)
        {
            logger.Info("Clicked No for layout preview");
            DialogResult = false;
        }
    }
}
