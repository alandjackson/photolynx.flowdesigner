﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Flow.View.Catalog
{
	/// <summary>
	/// Interaction logic for ImageQuixCatalogPanel.xaml
	/// </summary>
	public partial class ImageQuixCatalogPanel : UserControl
	{

		protected static DependencyProperty ClearCatalogsConfirmPanelVisibilityProperty = DependencyProperty.Register(
			"ClearCatalogsConfirmPanelVisibility", typeof(Visibility), typeof(ImageQuixCatalogPanel), new PropertyMetadata(Visibility.Collapsed)
		);

		public Visibility ClearCatalogsConfirmPanelVisibility
		{
			get { return (Visibility)this.GetValue(ClearCatalogsConfirmPanelVisibilityProperty); }
			set { this.SetValue(ClearCatalogsConfirmPanelVisibilityProperty, value); }
		}

        protected static DependencyProperty DeleteUnusedProductsConfirmPanelVisibilityProperty = DependencyProperty.Register(
            "DeleteUnusedProductsConfirmPanelVisibility", typeof(Visibility), typeof(ImageQuixCatalogPanel), new PropertyMetadata(Visibility.Collapsed)
		);

        public Visibility DeleteUnusedProductsConfirmPanelVisibility
		{
            get { return (Visibility)this.GetValue(DeleteUnusedProductsConfirmPanelVisibilityProperty); }
            set { this.SetValue(DeleteUnusedProductsConfirmPanelVisibilityProperty, value); }
		}
        
		
		public ImageQuixCatalogPanel()
		{
			InitializeComponent();
		}

		private void SetImageQuixCatalogServerUriButton_Click(object sender, RoutedEventArgs e)
		{
			this.ImageQuixCatalogServerUriTextBox.IsEnabled = !this.ImageQuixCatalogServerUriTextBox.IsEnabled;
			this.ImageQuixCatalogServerUriTextBox.Focus();
			this.ImageQuixCatalogServerUriTextBox.SelectAll();
		}
	}
}
