﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using Flow.Controller.Project;
using Flow.Controller.OnlineOrdering;
using NetServ.Net.Json;
using System.IO;
using Flow.Schema.LinqModel.DataContext;
using System.Globalization;
using Flow.View.Dialogs;

namespace Flow.View.Catalog
{
    /// <summary>
    /// Interaction logic for AllOrdersPanel.xaml
    /// </summary>
    public partial class AllOrdersPanel : UserControl 
    {
        

        public AllOrdersPanel()
        {
            InitializeComponent();
            
        }



        private void _this_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnLoad_Click(object sender, RoutedEventArgs e)
        {

        }

        internal void SetDataContext(Controller.Project.ProjectController projectController)
        {
            

            //foreach(OrderPackage op in projectController.CurrentFlowProject.FlowProjectDataContext.OrderPackages)
            //{
            //    op.IsOnlineOrder;
            //    op.IQOrderDateShort;
            //    op.ProductPackageDesc;
            //    op.SubjectOrder.Subject.FormalFullName;
            //    op.OrderType;

                    
            //}
        }

        private void orderImage_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            OrderPackage op = (sender as Image).DataContext as OrderPackage;
            ShowAnOrder show = new ShowAnOrder(op);
            show.ShowDialog();
        }

        private void btnImg_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            OrderPackage op = (sender as Button).DataContext as OrderPackage;
            ShowAnOrder show = new ShowAnOrder(op);
            show.ShowDialog();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            OrderPackage op = (sender as Button).DataContext as OrderPackage;
            (DataContext as ProjectController).GotoTicketID(op.SubjectOrder.Subject.TicketCode);
            (DataContext as ProjectController).OpenCapture();
        }

        //private void cmbOrderFilter_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    this.ProjectController.CurrentFlowProject.FlowProjectDataContext.UpdateFilteredOrderPackages();
        //}

        private void UpdateFilter()
        {

            if ((DataContext as ProjectController) != null)
            {
                (DataContext as ProjectController).CurrentFlowProject.FlowProjectDataContext.UpdateFilteredOrderPackages();
                (DataContext as ProjectController).CurrentFlowProject.FlowProjectDataContext.UpdateOrderFilterValues();
                UpdateCheckAll();
            }
        }

        private void FilterChanged(object sender, RoutedEventArgs e)
        {
            UpdateFilter();
        }

        private void rdoNew_Checked(object sender, RoutedEventArgs e)
        {
            (DataContext as ProjectController).CurrentFlowProject.FlowProjectDataContext.OrderFilterNew = true;
            (DataContext as ProjectController).CurrentFlowProject.FlowProjectDataContext.OrderFilterAll = false;
            (DataContext as ProjectController).CurrentFlowProject.FlowProjectDataContext.OrderFilterPrevious = false;
            //(DataContext as ProjectController).CurrentFlowProject.FlowProjectDataContext.OrderFilterPreviousSelection = "";
            UpdateFilter();
        }

        private void rdoAll_Checked(object sender, RoutedEventArgs e)
        {
            (DataContext as ProjectController).CurrentFlowProject.FlowProjectDataContext.OrderFilterAll = true;
            (DataContext as ProjectController).CurrentFlowProject.FlowProjectDataContext.OrderFilterNew = false;
            (DataContext as ProjectController).CurrentFlowProject.FlowProjectDataContext.OrderFilterPrevious = false;
            //(DataContext as ProjectController).CurrentFlowProject.FlowProjectDataContext.OrderFilterPreviousSelection = "";
            UpdateFilter();
        }

        private void rdoPrevious_Checked(object sender, RoutedEventArgs e)
        {
            (DataContext as ProjectController).CurrentFlowProject.FlowProjectDataContext.OrderFilterPrevious = true;
            (DataContext as ProjectController).CurrentFlowProject.FlowProjectDataContext.OrderFilterAll = false;
            (DataContext as ProjectController).CurrentFlowProject.FlowProjectDataContext.OrderFilterNew = false;
            //(DataContext as ProjectController).CurrentFlowProject.FlowProjectDataContext.OrderFilterPreviousSelection = "";
            UpdateFilter();
        }

        private void rdoBoth_Checked(object sender, RoutedEventArgs e)
        {
            (DataContext as ProjectController).CurrentFlowProject.FlowProjectDataContext.OrderFilterWebAndManual = true;
            (DataContext as ProjectController).CurrentFlowProject.FlowProjectDataContext.OrderFilterWeb = false;
            (DataContext as ProjectController).CurrentFlowProject.FlowProjectDataContext.OrderFilterManual = false;
            UpdateFilter();
        }

        private void rdoWeb_Checked(object sender, RoutedEventArgs e)
        {
            (DataContext as ProjectController).CurrentFlowProject.FlowProjectDataContext.OrderFilterWeb = true;
            (DataContext as ProjectController).CurrentFlowProject.FlowProjectDataContext.OrderFilterWebAndManual = false;
            (DataContext as ProjectController).CurrentFlowProject.FlowProjectDataContext.OrderFilterManual = false;
            UpdateFilter();
        }

        private void rdoManual_Checked(object sender, RoutedEventArgs e)
        {
            (DataContext as ProjectController).CurrentFlowProject.FlowProjectDataContext.OrderFilterManual = true;
            (DataContext as ProjectController).CurrentFlowProject.FlowProjectDataContext.OrderFilterWeb = false;
            (DataContext as ProjectController).CurrentFlowProject.FlowProjectDataContext.OrderFilterWebAndManual = false;
            UpdateFilter();
        }



        private void rdoAllSubjects_Checked(object sender, RoutedEventArgs e)
        {
            (DataContext as ProjectController).CurrentFlowProject.FlowProjectDataContext.OrderFilterAllSubjects = true;
            (DataContext as ProjectController).CurrentFlowProject.FlowProjectDataContext.OrderFilterFilteredSubjects = false;
            (DataContext as ProjectController).CurrentFlowProject.FlowProjectDataContext.OrderFilterCurrentSubject = false;
            (DataContext as ProjectController).CurrentFlowProject.FlowProjectDataContext.OrderFilterReadySubject = false;
            UpdateFilter();
        }

        private void rdoFilteredSubjects_Checked(object sender, RoutedEventArgs e)
        {
            (DataContext as ProjectController).CurrentFlowProject.FlowProjectDataContext.OrderFilterFilteredSubjects = true;
            (DataContext as ProjectController).CurrentFlowProject.FlowProjectDataContext.OrderFilterAllSubjects = false;
            (DataContext as ProjectController).CurrentFlowProject.FlowProjectDataContext.OrderFilterCurrentSubject = false;
            (DataContext as ProjectController).CurrentFlowProject.FlowProjectDataContext.OrderFilterReadySubject = false;
            UpdateFilter();
        }

        private void rdoCurrentSubject_Checked(object sender, RoutedEventArgs e)
        {
            (DataContext as ProjectController).CurrentFlowProject.FlowProjectDataContext.OrderFilterCurrentSubject = true;
            (DataContext as ProjectController).CurrentFlowProject.FlowProjectDataContext.OrderFilterFilteredSubjects = false;
            (DataContext as ProjectController).CurrentFlowProject.FlowProjectDataContext.OrderFilterAllSubjects = false;
            (DataContext as ProjectController).CurrentFlowProject.FlowProjectDataContext.OrderFilterReadySubject = false;
            UpdateFilter();
        }

        private void rdoReadySubject_Checked(object sender, RoutedEventArgs e)
        {
            (DataContext as ProjectController).CurrentFlowProject.FlowProjectDataContext.OrderFilterReadySubject = true;
            (DataContext as ProjectController).CurrentFlowProject.FlowProjectDataContext.OrderFilterFilteredSubjects = false;
            (DataContext as ProjectController).CurrentFlowProject.FlowProjectDataContext.OrderFilterCurrentSubject = false;
            (DataContext as ProjectController).CurrentFlowProject.FlowProjectDataContext.OrderFilterAllSubjects = false;
            UpdateFilter();
        }

        private void chkIncludeAllInOrder_Checked(object sender, RoutedEventArgs e)
        {
            UpdateCheckAll();
            UpdateFilter();
        }

        private void chkIncludeAllInOrder_Unchecked(object sender, RoutedEventArgs e)
        {
            UpdateCheckAll();
            UpdateFilter();
        }

        private void UpdateCheckAll()
        {
            foreach (OrderPackage op in (DataContext as ProjectController).CurrentFlowProject.FlowProjectDataContext.FilteredOrderPackages)
            {
                op.IncludeInOrderChecked = (DataContext as ProjectController).CurrentFlowProject.FlowProjectDataContext.OrderFilterCheckAll;
            }
        }

        private void cmbPackageAction_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string action = ((sender as ComboBox).SelectedValue as ComboBoxItem).Content as string;
            OrderPackage op = (sender as ComboBox).DataContext as OrderPackage;
            if (action.Contains("Set Lab OrderID to 0") && op.LabOrderID == null)
            {
                op.SubjectOrder.Subject.FlagForMerge = true;
                op.LabOrderDate = DateTime.Now;
                op.LabOrderID = 0;
                op.ModifyDate = DateTime.Now;
                UpdateFilter();
            }
            if (action.Contains("Set Lab OrderID to Blank"))
            {
                op.SubjectOrder.Subject.FlagForMerge = true;
                op.LabOrderDate = null;
                op.LabOrderID = null;
                op.ModifyDate = DateTime.Now;
                UpdateFilter();
            }
        }

        private void cmbSelectedOrder_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UpdateFilter();
        }

       
    }

    [ValueConversion(typeof(object), typeof(int))]
    public class NumberToPolarValueConverter : IValueConverter
    {
        public object Convert(
         object value, Type targetType,
         object parameter, CultureInfo culture)
        {
            double number = (double)System.Convert.ChangeType(value, typeof(double));

            if (number < 0.0)
                return -1;

            if (number == 0.0)
                return 0;

            return +1;
        }

        public object ConvertBack(
         object value, Type targetType,
         object parameter, CultureInfo culture)
        {
            throw new NotSupportedException("ConvertBack not supported");
        }
    }
}
