﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Lib.Helpers;
using Flow.Schema.LinqModel.DataContext;
using NLog;

namespace Flow.View.Catalog
{
	/// <summary>
	/// Interaction logic for ProductProgramEditor.xaml
	/// </summary>
	public partial class FlowCatalogEditor : UserControl
	{
		public event EventHandler<EventArgs<ProductPackage>> EditItemInvoked = delegate { };
		public event EventHandler<EventArgs<ProductPackage>> DeleteItemInvoked = delegate { };
        public event EventHandler<EventArgs> SaveChangesInvoked = delegate { };
        public event EventHandler<EventArgs> AllowCatalogEditingClicked = delegate { };

        public event EventHandler<EventArgs<FlowCatalog>> DuplicateCatalogClicked = delegate { };

		
        protected static DependencyProperty EditingControlsEnabledProperty =
		DependencyProperty.Register("EditingControlsEnabled", typeof(bool), typeof(FlowCatalogEditor), new PropertyMetadata(false));

        private static Logger logger = LogManager.GetCurrentClassLogger();

		public bool EditingControlsEnabled
		{
			get { return (bool)this.GetValue(EditingControlsEnabledProperty); }
			set { this.SetValue(EditingControlsEnabledProperty, value); }
		}

		public FlowCatalogEditor()
		{
			InitializeComponent();
            
		}

		private void ProductPackageRecordDisplay_EditItemInvoked(object sender, EventArgs<ProductPackage> e)
		{
			this.EditItemInvoked(sender, e);
		}

		private void ProductCatalogItemDisplay_DeleteItemInvoked(object sender, EventArgs<ProductPackage> e)
		{
			this.DeleteItemInvoked(sender, e);
		}

        private void CatalogOptionRecordDisplay_SaveChangesInvoked(object sender, EventArgs e)
        {
            this.SaveChangesInvoked(sender, e);
        }

        private void FlowCatalogDescriptionTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (FlowCatalogDescriptionTextBox.Text.Length > 0)
                FlowCatalogEditButtonStrip.IsSaveEnabled = true;
            else
                FlowCatalogEditButtonStrip.IsSaveEnabled = false;
        }

        private void btnAllowEditing_Click(object sender, RoutedEventArgs e)
        {
            logger.Info("Allow Editing button clicked");
            this.AllowCatalogEditingClicked(sender, e);
        }


        private void DuplicateCatalogButton_Click(object sender, EventArgs<FlowCatalog> e)
        {
            this.DuplicateCatalogClicked(sender, e);

           
            
        }

        private void DuplicateCatalog_Click(object sender, EventArgs<FlowCatalog> e)
        {
            this.DuplicateCatalogButton_Click(this, new EventArgs<FlowCatalog>(e.Data));
        }
	}
}
