﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Lib.Helpers;
using Flow.Schema.LinqModel.DataContext;
using Flow.Controls.View.Dialogs;

namespace Flow.View.Catalog
{
	/// <summary>
	/// Interaction logic for ProductPackageEditor.xaml
	/// </summary>
	public partial class ProductPackageEditor : UserControl
	{
		public event EventHandler<EventArgs<ImageQuixProduct>> AddItemInvoked = delegate { };
		public event EventHandler<EventArgs<ImageQuixProduct>> DeleteItemInvoked = delegate { };
        public event EventHandler<EventArgs<ImageQuixProduct>> AddALaCarteInvoked = delegate { };

		public event EventHandler<EventArgs<ProductPackage>> EditProductPackageInvoked = delegate { };


		protected static DependencyProperty EditingControlsEnabledProperty =
			DependencyProperty.Register("EditingControlsEnabled", typeof(bool), typeof(ProductPackageEditor));

        protected static DependencyProperty EditingControlsDisabledProperty =
            DependencyProperty.Register("EditingControlsDisabled", typeof(bool), typeof(ProductPackageEditor));

		public bool EditingControlsEnabled
		{
			get { return (bool)this.GetValue(EditingControlsEnabledProperty); }
			set { this.SetValue(EditingControlsEnabledProperty, value); }
		}

        public bool EditingControlsDisabled
        {
            get { return (bool)this.GetValue(EditingControlsDisabledProperty); }
            set { this.SetValue(EditingControlsDisabledProperty, value); }
        }

		public ProductPackageEditor()
		{
			InitializeComponent();

			Validation.AddErrorHandler(this.ProductPackageDescTextBox, ProductPackageDescTextBox_ErrorHandler);
		}

		private void ImageQuixProductRecordDisplay_AddItemInvoked(object sender, EventArgs<ImageQuixProduct> e)
		{
            this.AddItemInvoked(sender, e);
		}

        private void ImageQuixProductRecordDisplay_AddALaCarteInvoked(object sender, EventArgs<ImageQuixProduct> e)
        {
            this.AddALaCarteInvoked(sender, e);
        }

		private void ImageQuixProductRecordDisplay_DeleteItemInvoked(object sender, EventArgs<ImageQuixProduct> e)
		{
			this.DeleteItemInvoked(sender, e);
		}

		private void ProductPackageRecordDisplay_EditItemInvoked(object sender, EventArgs<ProductPackage> e)
		{
//			this.EditProductPackageInvoked(sender, e);
		}

		private void ProductPackageRecordDisplay_DeleteItemInvoked(object sender, EventArgs<ProductPackage> e)
		{
            
		}

		private void ProductPackageRecordDisplay_Click(object sender, EventArgs<ProductPackage> e)
		{
		}

		protected void ProductPackageDescTextBox_ErrorHandler(object sender, ValidationErrorEventArgs e)
		{

		}

        private void RefreshSaveEnabled()
        {
            if (this.ProductPackageDescTextBox.Text.Length > 0 && this.ProductPackageMapTextBox.Text.Length > 0)
                ProductPackageEditButtonStrip.IsSaveEnabled = true;
            else
                ProductPackageEditButtonStrip.IsSaveEnabled = false;
        }

        private void ProductPackageDescTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            RefreshSaveEnabled();
        }

        private void ProductPackageMapTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            RefreshSaveEnabled();
        }
	}
}
