﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Controller.Project;
using Flow.Controls.View.RecordDisplay;
using Flow.Lib.Helpers;
using Flow.Schema.LinqModel.DataContext;
using NLog;
using Flow.Controller.Catalog;

namespace Flow.View.Catalog
{
	/// <summary>
	/// Interaction logic for OrderEntryPanel.xaml
	/// </summary>
	public partial class OrderEntryPanel : UserControl
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public event EventHandler<EventArgs<OrderImageOption>> RemoveImageOptionInvoked = delegate { };
        public event EventHandler<EventArgs<OrderSubjectOrderOption>> RemoveSubjectOrderOptionInvoked = delegate { };

        public event EventHandler<EventArgs<FlowCatalogOption>> AddCatalogOptionInvoked = delegate{};
		public event EventHandler<EventArgs<ProductPackage>> AddOrderPackageInvoked = delegate {};
		public event EventHandler<EventArgs<KeyValuePair<ProductPackage, string>>> AddOrderPackageToAllInvoked = delegate { };
		public event EventHandler<EventArgs<OrderPackage>> DuplicateOrderPackageInvoked = delegate { };
		public event EventHandler<EventArgs<OrderPackage>> RemoveOrderPackageInvoked = delegate {};

		public event EventHandler<EventArgs<OrderProductNode>> AssignPreviousImageRequested = delegate { };
		public event EventHandler<EventArgs<OrderProductNode>> AssignNullImageRequested = delegate { };
		public event EventHandler<EventArgs<OrderProductNode>> AssignNextImageRequested = delegate { };

        public event EventHandler<RoutedEventArgs> CloseOrderEntryRequested = delegate { };

        public event EventHandler<MouseButtonEventArgs> ClearKeyHistoryRequested = delegate { };

        public OrderEntryController OrderEntryController { get {
            return (this.DataContext as ProjectController).FlowController.CaptureController.OrderEntryController;
        } }

		private ProjectController ProjectController
		{
			get { return this.DataContext as ProjectController; }
		}


		public OrderEntryPanel()
		{
			InitializeComponent();
            
		}

        public void SetDefaultFocus()
        {
           // this.btnCloseOrderEntry.Focus();
            //I created an empty label to give focus to when order entry is first open
            this.lblKeyMapReady.Focus();
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void ProductPackageRecordDisplay_AddOrderPackageInvoked(object sender, EventArgs<ProductPackage> e)
		{
			this.AddOrderPackageInvoked(sender, e);
            UpdatePackagesShipmentType(this.ProjectController.CurrentFlowProject.SubjectList.CurrentItem, null);
		}

        private void ProductPackageRecordDisplay_AddOrderPackageToAllInvoked(object sender, EventArgs<KeyValuePair<ProductPackage, string>> e)
		{
			this.AddOrderPackageToAllInvoked(sender, e);
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OrderPackageDisplay_DuplicateOrderPackageInvoked(object sender, EventArgs<OrderPackage> e)
		{
			this.DuplicateOrderPackageInvoked(sender, e);
            UpdatePackagesShipmentType(this.ProjectController.CurrentFlowProject.SubjectList.CurrentItem, null);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OrderPackageDisplay_RemoveOrderPackageInvoked(object sender, EventArgs<OrderPackage> e)
		{
			this.RemoveOrderPackageInvoked(sender, e);
		}

		private void OrderProductImageListDisplay_AssignNextImageRequested(object sender, EventArgs<OrderProductNode> e)
		{
			this.AssignNextImageRequested(sender, e);
		}

		private void OrderProductImageListDisplay_AssignNullImageRequested(object sender, EventArgs<OrderProductNode> e)
		{
			this.AssignNullImageRequested(sender, e);
		}

		private void OrderProductImageListDisplay_AssignPreviousImageRequested(object sender, EventArgs<OrderProductNode> e)
		{
			this.AssignPreviousImageRequested(sender, e);
		}

		private void OrderPackageDisplay_OptionListSelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if (!this.ProjectController.CurrentFlowProject.IsNew)
			{
                if (((ComboBox)sender).SelectionBoxItem.GetType() == typeof(ImageQuixOption))
                    this.ProjectController.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();
			}
		}

       

        private void OrderPackageDisplay_OptionListMasterSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //return;
            if (sender == null || (sender as ComboBox).DataContext == null || (sender as ComboBox).SelectedIndex < 0)
                return;

            OrderProductOption prodOption = (sender as ComboBox).DataContext as OrderProductOption;
            //ImageQuixOption iqOption = (sender as ComboBox).SelectedItem as ImageQuixOption;
            ImageQuixOption iqOption = this.ProjectController.FlowMasterDataContext.ImageQuixOptions.FirstOrDefault(i => i.PrimaryKey == prodOption.ImageQuixOptionPk);
            if(iqOption == null)
                iqOption = (sender as ComboBox).SelectedItem as ImageQuixOption;

            string iqOptionMap  = null;
            if (this.ProjectController.CurrentFlowProject.isPUD || this.ProjectController.CurrentFlowProject.isPLIC)
                iqOptionMap = iqOption.ResourceURL;

            if (prodOption != null && iqOption != null)
            {
                string optionGroupLabel = "";
                if (iqOption.ImageQuixOptionGroup != null)
                    optionGroupLabel = iqOption.ImageQuixOptionGroup.Label;
                this.OrderEntryController.AddImageOption(prodOption, iqOption.Label, optionGroupLabel, iqOptionMap);
            }
        }

        private void btnCloseOrderEntry_Click(object sender, RoutedEventArgs e)
        {
            logger.Info("Clicked Close Order Entry button");
            this.CloseOrderEntryRequested(sender, e);
        }

        private void _this_Loaded(object sender, RoutedEventArgs e)
        {
            this.SetDefaultFocus();
        }

        
        private void lblCurrentKeyMap_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            this.ClearKeyHistoryRequested(sender, e);
        }

        private void CatalogOptionRecordDisplay_AddCatalogOptionInvoked(object sender, EventArgs<FlowCatalogOption> e)
        {
            this.AddCatalogOptionInvoked(sender, e);
        }

        private void ImageOptionRecordDisplay_DeleteImageOptionInvoked(object sender, EventArgs<OrderImageOption> e)
        {
            this.RemoveImageOptionInvoked(sender, e);
        }

        private void SubjectOrderOptionRecordDisplay_DeleteSubjectOrderOptionInvoked(object sender, EventArgs<OrderSubjectOrderOption> e)
        {
            this.RemoveSubjectOrderOptionInvoked(sender, e);
        }

        private void chkDirectShip_Checked(object sender, RoutedEventArgs e)
        {
            UpdatePackagesShipmentType(this.ProjectController.CurrentFlowProject.SubjectList.CurrentItem, "direct");
        }

        private void UpdatePackagesShipmentType(Subject subject, string newType)
        {
            if (subject != null && subject.SubjectOrder != null)
            {
                if (newType == null)
                {
                    if (subject.SubjectOrder.Directship == true)
                        newType = "direct";
                    else
                        newType = "pickup";
                }

                if (newType == "direct")
                {
                    object firstname = this.ProjectController.CurrentFlowProject.SubjectList.CurrentItem.SubjectData.FirstOrDefault(sd => sd.FieldDesc.ToLower() == "firstname");
                    object lastname = this.ProjectController.CurrentFlowProject.SubjectList.CurrentItem.SubjectData.FirstOrDefault(sd => sd.FieldDesc.ToLower() == "lastname");
                    object address = this.ProjectController.CurrentFlowProject.SubjectList.CurrentItem.SubjectData.FirstOrDefault(sd => sd.FieldDesc.ToLower() == "address");
                    object city = this.ProjectController.CurrentFlowProject.SubjectList.CurrentItem.SubjectData.FirstOrDefault(sd => sd.FieldDesc.ToLower() == "city");
                    object state = this.ProjectController.CurrentFlowProject.SubjectList.CurrentItem.SubjectData.FirstOrDefault(sd => sd.FieldDesc.ToLower() == "state");
                    object zip = this.ProjectController.CurrentFlowProject.SubjectList.CurrentItem.SubjectData.FirstOrDefault(sd => sd.FieldDesc.ToLower() == "zip");

                    SubjectOrder so = this.ProjectController.CurrentFlowProject.SubjectList.CurrentItem.SubjectOrder;
                    if (firstname != null && lastname != null && string.IsNullOrEmpty(so.ShippingName)) so.ShippingName = ((SubjectDatum)firstname).Value as string + " " + ((SubjectDatum)lastname).Value as string;
                    if (address != null && string.IsNullOrEmpty(so.ShippingAddress)) so.ShippingAddress = ((SubjectDatum)address).Value as string;
                    if (city != null && string.IsNullOrEmpty(so.ShippingCity)) so.ShippingCity = ((SubjectDatum)city).Value as string;
                    if (state != null && string.IsNullOrEmpty(so.ShippingState)) so.ShippingState = ((SubjectDatum)state).Value as string;
                    if (zip != null && string.IsNullOrEmpty(so.ShippingZipCode)) so.ShippingZipCode = ((SubjectDatum)zip).Value as string;
                }
                else
                {
                    SubjectOrder so = this.ProjectController.CurrentFlowProject.SubjectList.CurrentItem.SubjectOrder;
                    so.ShippingName = "";
                    so.ShippingAddress = "";
                    so.ShippingCity = "";
                    so.ShippingState = "";
                    so.ShippingZipCode = "";
                }

                foreach (OrderPackage op in subject.SubjectOrder.OrderPackageList)
                {
                    if (op.LabOrderDate == null)
                    {
                       

                        op.ShipmentType = newType;
                        op.UpdateShipmentType();
                    }
                }
                UpdateDirectShipmentData();

                subject.SubjectOrder.UpdateOrderPackages();
                //this.OrderPackageListBox.ItemsSource = subject.SubjectOrder.OrderPackageList;
                //this.OrderPackageListBox.UpdateLayout();
            }
        }

       

        private void chkDirectShip_Unchecked(object sender, RoutedEventArgs e)
        {
            UpdatePackagesShipmentType(this.ProjectController.CurrentFlowProject.SubjectList.CurrentItem, "pickup");
        }

        private void UpdateDirectShipmentData()
        {

            

            foreach (OrderPackage op in this.ProjectController.CurrentFlowProject.SubjectList.CurrentItem.SubjectOrder.OrderPackages)
            {
                if (op.ShipmentType == "direct" && op.LabOrderDate == null && !op.IsOnlineOrder)
                {
                    if (!String.IsNullOrEmpty(txtDirectShipCity.Text))
                    {
                        op.ShippingFirstName = txtDirectShipName.Text;
                        op.ShippingAddress = txtDirectShipAddress.Text;
                        op.ShippingCity = txtDirectShipCity.Text;
                        op.ShippingState = txtDirectShipState.Text;
                        op.ShippingZip = txtDirectShipZip.Text;
                    }
                    else
                    {
                        op.ShippingFirstName = this.ProjectController.CurrentFlowProject.SubjectList.CurrentItem.SubjectOrder.ShippingName;
                        op.ShippingAddress = this.ProjectController.CurrentFlowProject.SubjectList.CurrentItem.SubjectOrder.ShippingAddress;
                        op.ShippingCity = this.ProjectController.CurrentFlowProject.SubjectList.CurrentItem.SubjectOrder.ShippingCity;
                        op.ShippingState = this.ProjectController.CurrentFlowProject.SubjectList.CurrentItem.SubjectOrder.ShippingState;
                        op.ShippingZip = this.ProjectController.CurrentFlowProject.SubjectList.CurrentItem.SubjectOrder.ShippingZipCode;
                    }
                }
            }
            this.ProjectController.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();
        }


        private void txtDirectShip_KeyUp(object sender, KeyEventArgs e)
        {
            UpdateDirectShipmentData();
        }

        private void FlowCatalogComboBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            e.Handled = true;
        }

        private void FlowCatalogComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            lblSelectCatalog.Visibility = Visibility.Collapsed;
            FlowCatalogComboBox.Visibility = Visibility.Collapsed;
        }
       

	}
}
