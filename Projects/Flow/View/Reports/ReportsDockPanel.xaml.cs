﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Controller.Reports;
using Flow.Schema.Reports;
using NLog;
using Flow.Lib.Helpers;
using System.Text.RegularExpressions;

namespace Flow.View.Reports
{
	/// <summary>
	/// Interaction logic for ReportsDockPanel.xaml
	/// </summary>
	public partial class ReportsDockPanel : UserControl
	{
		//protected static DependencyProperty SelectedReportNameProperty = DependencyProperty.Register(
		//    "SelectedReportName", typeof(string), typeof(ReportsDockPanel), new PropertyMetadata("Subject Orders")
		//);

		//public string SelectedReportName
		//{
		//    get { return (string)this.GetValue(SelectedReportNameProperty); }
		//    set { this.SetValue(SelectedReportNameProperty, value); }
		//}

        public event EventHandler PrintButtonInvoked = delegate { };
        public event EventHandler PdfButtonInvoked = delegate { };
        public event EventHandler CsvButtonInvoked = delegate { };
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public bool PrintButtonVisible { get; set; }
        public bool PdfButtonVisible { get; set; }
        public bool CsvButtonVisible { get; set; }

		protected static DependencyProperty SelectedExportFormatProperty = DependencyProperty.Register(
			"SelectedExportFormat", typeof(KeyValuePair<string,string>), typeof(ReportsDockPanel)
		);

		public KeyValuePair<string, string> SelectedExportFormat
		{
			get { return (KeyValuePair<string, string>)this.GetValue(SelectedExportFormatProperty); }
			set { this.SetValue(SelectedExportFormatProperty, value); }
		}

		protected DependencyProperty SelectedReportViewProperty = DependencyProperty.Register(
			"SelectedReportView", typeof(FlowReportView), typeof(ReportsDockPanel)
		);

		public FlowReportView SelectedReportView
		{
			get { return (FlowReportView)this.GetValue(SelectedReportViewProperty); }
			set { this.SetValue(SelectedReportViewProperty, value); }
		}

        public string SelectedOrderFilter { get; set; }
        

		public ReportsDockPanel()
		{
			InitializeComponent();
		}

        private void ReportComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //"Subject Orders Summary",
            //"Photographed Subjects",
            //"Unphotographed Subjects",
            //"Subject Orders Detailed",
            //"Order Form",
            //"Subject Barcodes"

            logger.Info("Report selection changed to '{0}'", ReportComboBox.SelectedValue);

            lblOrderFilter.Visibility = Visibility.Collapsed;
            ReportOrderFilterComboBox.Visibility = Visibility.Collapsed;
            lblLayout.Visibility = Visibility.Collapsed;
            ReportLayoutComboBox.Visibility = Visibility.Collapsed;
            chkStackSort.Visibility = Visibility.Collapsed;
            chkShowFooter.Visibility = Visibility.Collapsed;
            spTicketCount.Visibility = Visibility.Collapsed;
            chkShowPrice.Visibility = Visibility.Collapsed;
            chkShowBarCodes.Visibility = Visibility.Collapsed;
            chkShowProducts.Visibility = Visibility.Collapsed;
            chkShowImageOptions.Visibility = Visibility.Collapsed;
            fieldSelector.Visibility = Visibility.Collapsed;
            pnlSubjectCopies.Visibility = Visibility.Collapsed;

            lblReportView.Visibility = Visibility.Collapsed;
            SaveViewAsButton.Visibility = Visibility.Collapsed;
            ReportViewComboBox.Visibility = Visibility.Collapsed;
            ReportViewComboBox.SelectedIndex = 0;
            Orientation.Visibility = Visibility.Collapsed;

            lblPageBreak.Visibility = Visibility.Collapsed;
            PageBreakComboBox.Visibility = Visibility.Collapsed;

            
            //ExportFormatComboBox.Visibility = Visibility.Visible;
            //ReportExportButton.Visibility = Visibility.Visible;
            //lblExport.Visibility = Visibility.Visible;

            PrintButtonVisible = false;
            PdfButtonVisible = false;
            CsvButtonVisible = false;

            if (ReportComboBox.SelectedValue == "Subject Orders Detailed" || (ReportComboBox.SelectedValue == "Subject Orders Summary"))
            {
                lblOrderFilter.Visibility = Visibility.Visible;
                ReportOrderFilterComboBox.Visibility = Visibility.Visible;
                chkShowPrice.Visibility = Visibility.Visible;
                PrintButtonVisible = true;
                PdfButtonVisible = true;
                CsvButtonVisible = true;
            }

            else if ((ReportComboBox.SelectedValue == "Lab Billing Report"))
            {
                lblOrderFilter.Visibility = Visibility.Visible;
                ReportOrderFilterComboBox.Visibility = Visibility.Visible;
                PrintButtonVisible = true;
                PdfButtonVisible = true;
                CsvButtonVisible = true;
            }

            else if (ReportComboBox.SelectedValue == "Subject Barcodes")
            {
                lblLayout.Visibility = Visibility.Visible;
                ReportLayoutComboBox.Visibility = Visibility.Visible;
                chkStackSort.Visibility = Visibility.Visible;
                chkShowFooter.Visibility = Visibility.Visible;
                chkShowPrice.Visibility = Visibility.Collapsed;
                fieldSelector.Visibility = Visibility.Visible;
                lblPageBreak.Visibility = Visibility.Visible;
                PageBreakComboBox.Visibility = Visibility.Visible;
                PrintButtonVisible = true;
                PdfButtonVisible = true;
                CsvButtonVisible = false;
                pnlSubjectCopies.Visibility = Visibility.Visible;

            }

            else if (ReportComboBox.SelectedValue.ToString().StartsWith("> "))
            {

                string resultString = Regex.Match(ReportComboBox.SelectedValue.ToString(), @"\d+").Value;
                int resultInt = 1;
                if (!int.TryParse(resultString, out resultInt))
                {
                    resultInt = 1;
                    resultString = "1";
                }
                if (resultInt > 0)
                    txtHowManyUp.Text = resultString;

                pnlSubjectCopies.Visibility = Visibility.Visible;
                chkStackSort.Visibility = Visibility.Visible;
                pnlHowManyUp.Visibility = Visibility.Visible;

            }

            else if (ReportComboBox.SelectedValue == "Slate Sheet 3up" || ReportComboBox.SelectedValue.ToString().StartsWith(">"))
            {
                lblLayout.Visibility = Visibility.Visible;
                ReportLayoutComboBox.Visibility = Visibility.Collapsed;
                spTicketCount.Visibility = Visibility.Collapsed;
                chkShowPrice.Visibility = Visibility.Collapsed;
                fieldSelector.Visibility = Visibility.Collapsed;
                //ExportFormatComboBox.Visibility = Visibility.Collapsed;
                //ReportExportButton.Visibility = Visibility.Collapsed;
                //lblExport.Visibility = Visibility.Collapsed;
                PrintButtonVisible = true;
                PdfButtonVisible = true;
                CsvButtonVisible = false;
                
            }

            else if (ReportComboBox.SelectedValue == "New Ticket Barcodes")
            {
                lblLayout.Visibility = Visibility.Visible;
                ReportLayoutComboBox.Visibility = Visibility.Visible;
                spTicketCount.Visibility = Visibility.Visible;
                chkShowPrice.Visibility = Visibility.Collapsed;
                fieldSelector.Visibility = Visibility.Visible;
                chkShowFooter.Visibility = Visibility.Visible;
                PrintButtonVisible = true;
                PdfButtonVisible = true;
                CsvButtonVisible = false;
            }

            else if ((ReportComboBox.SelectedValue == "Catalog Packages"))
            {
                chkShowBarCodes.Visibility = Visibility.Visible;
                chkShowProducts.Visibility = Visibility.Visible;
                chkShowImageOptions.Visibility = Visibility.Visible;
                PrintButtonVisible = true;
                PdfButtonVisible = true;
                CsvButtonVisible = false;
            }
                
            else if ((ReportComboBox.SelectedValue == "Order Form"))
            {
                PrintButtonVisible = true;
                PdfButtonVisible = true;
                CsvButtonVisible = false;
            }
            else if ((ReportComboBox.SelectedValue == "Photographed Subjects" || ReportComboBox.SelectedValue == "Unphotographed Subjects"))
            {
                //lblPageBreak.Visibility = Visibility.Visible;
                //PageBreakComboBox.Visibility = Visibility.Visible;
                fieldSelector.Visibility = Visibility.Visible;
                lblReportView.Visibility = Visibility.Visible;
                SaveViewAsButton.Visibility = Visibility.Visible;
                ReportViewComboBox.Visibility = Visibility.Visible;
                Orientation.Visibility = Visibility.Visible;
                PrintButtonVisible = true;
                PdfButtonVisible = true;
                CsvButtonVisible = true;
            }
            else
            {
                fieldSelector.Visibility = Visibility.Visible;
                lblReportView.Visibility = Visibility.Visible;
                SaveViewAsButton.Visibility = Visibility.Visible;
                ReportViewComboBox.Visibility = Visibility.Visible;
                Orientation.Visibility = Visibility.Visible;
                PrintButtonVisible = true;
                PdfButtonVisible = true;
                CsvButtonVisible = true;
            }



        }

    //    public static readonly RoutedEvent TapEvent = EventManager.RegisterRoutedEvent(
    //"Tap", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(Button));

    //    private void ReportDocumentViewer_Click(object sender, RoutedEventArgs e)
    //    {
    //        if ((e.OriginalSource as Button).ToolTip.ToString().StartsWith("Print"))
    //        {
    //            e.Handled = true;
    //            e.RoutedEvent = TapEvent;
    //            this.PrintButtonInvoked(sender, e);

               
                
    //        }
    //    }

        private void btnPrint_Click(object sender, RoutedEventArgs e)
        {
            this.PrintButtonInvoked(sender, e);
        }

        private void pdfButton_Click(object sender, RoutedEventArgs e)
        {
            this.PdfButtonInvoked(sender, e);
        }

        private void csvButton_Click(object sender, RoutedEventArgs e)
        {
            this.CsvButtonInvoked(sender, e);
        }

        
	}
}
