﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Controls.Primitives;

namespace Flow.View.Capture
{
    /// <summary>
    /// Interaction logic for TemplatePreviewPanel.xaml
    /// </summary>
    public partial class TemplatePreviewPanel : UserControl
    {
        public TemplatePreviewPanel()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if(layoutPopup.IsOpen == true)
                layoutPopup.IsOpen = false;
            else
                layoutPopup.IsOpen = true;
        }

        private void layoutPopup_Opened(object sender, EventArgs e)
        {
            uxLayouts.Focus();
        }



        private void layoutPopup_LostFocus(object sender, RoutedEventArgs e)
        {
            //layoutPopup.IsOpen = false;
        }

        private void uxLayouts_LostFocus(object sender, RoutedEventArgs e)
        {
            //layoutPopup.IsOpen = false;
        }

        private void layoutPopup_MouseLeave(object sender, MouseEventArgs e)
        {
            layoutPopup.IsOpen = false;
        }

        

    }
}
