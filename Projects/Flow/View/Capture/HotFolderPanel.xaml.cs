﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Lib;
using Flow.Lib.Helpers;
using Flow.Model;
using Flow.Model.Capture;
using Flow.Schema.LinqModel.DataContext;

namespace Flow.View.Capture
{
    /// <summary>
    /// Interaction logic for HotFolder.xaml
    /// </summary>
    public partial class HotFolderPanel : UserControl
    {
        public event EventHandler ImageImport = delegate { };

        public event EventHandler<EventArgs<HotfolderImage>>
			SelectedHotfolderImageChanged = delegate { };

		public event EventHandler<EventArgs<Collection<string>>>
			HotfolderImageAssigned = delegate { };

        public event EventHandler<EventArgs<Collection<string>>>
            HotfolderGroupImageAssigned = delegate { };

        public event EventHandler<EventArgs<Collection<string>>>
            HotfolderImageDeleted = delegate { };

		protected static DependencyProperty HotfolderImageListProperty = DependencyProperty.Register(
			"HotfolderImageList", typeof(HotfolderImages), typeof(HotFolderPanel)
		);

		public HotfolderImages HotfolderImageList
		{
			get { return (HotfolderImages)this.GetValue(HotfolderImageListProperty); }
			set { this.SetValue(HotfolderImageListProperty, value); }
		}
		
		
		public HotFolderPanel()
        {
            InitializeComponent();
        }

		public void SetDataContext(HotfolderImages hotfolderImages)
		{
			HotfolderListbox.ItemsSource = hotfolderImages;
			//this.HotfolderImageList = hotfolderImages;
			//this.HotfolderListbox.DataContext = this.HotfolderListbox;
		}

        protected void OnSelectedHotfolderImageChanged(HotfolderImage image)
        {
//            if (SelectedHotfolderImageChanged != null)
                SelectedHotfolderImageChanged(this, new EventArgs<HotfolderImage>(image));
        }

		/// <summary>
		/// Assigns the selected image to the current Subject record
		/// </summary>
		private void MenuItem_Click(object sender, RoutedEventArgs e)
		{
			string imageFileName = (sender as MenuItem).CommandParameter as string;
			AssignImages(imageFileName);
		}


		/// <summary>
		/// Prepares 
		/// </summary>
		/// <param name="imageFileName"></param>
		internal void AssignImages(string imageFileName)
		{

			// List of filepaths of the images to be assigned
			Collection<string> selectedImageFileNames = new Collection<string>();

			// Iterate through the list of selected images
			foreach (HotfolderImage image in this.HotfolderListbox.GetSelectedItems<HotfolderImage>().ToList())
			{
				// Hide the image
//				this.HotfolderListbox.HideItem<HotfolderImage>(image);

				// Add the filepath to the list
				selectedImageFileNames.Add(image.ImageFilename);

                //clear the image
                image.Clear();
			}

			// If a filepath was passed as an argument and the list does not
			// already contain this filepath, then add it to the list
			if (!(imageFileName == null || selectedImageFileNames.Contains(imageFileName)))
			{
				selectedImageFileNames.Add(imageFileName);
			}

			// Invoke the assigment
			this.HotfolderImageAssigned(
				this,
				new EventArgs<Collection<string>>(selectedImageFileNames)
			);

		}

        private void AssignGroupImages(string imageFileName)
        {

            // List of filepaths of the images to be assigned
            Collection<string> selectedImageFileNames = new Collection<string>();

            // Iterate through the list of selected images
            foreach (HotfolderImage image in this.HotfolderListbox.GetSelectedItems<HotfolderImage>().ToList())
            {
                // Hide the image
                //				this.HotfolderListbox.HideItem<HotfolderImage>(image);

                // Add the filepath to the list
                selectedImageFileNames.Add(image.ImageFilename);

                //clear the image
                image.Clear();
            }

            // If a filepath was passed as an argument and the list does not
            // already contain this filepath, then add it to the list
            if (!(imageFileName == null || selectedImageFileNames.Contains(imageFileName)))
            {
                selectedImageFileNames.Add(imageFileName);
            }

            // Invoke the assigment
            this.HotfolderGroupImageAssigned(
                this,
                new EventArgs<Collection<string>>(selectedImageFileNames)
            );

        }

        private void DeleteImages(string imageFileName)
        {

            // List of filepaths of the images to be deleted
            Collection<string> selectedImageFileNames = new Collection<string>();

            // Iterate through the list of selected images
            //foreach (HotfolderImage image in this.HotfolderListbox.GetSelectedItems<HotfolderImage>().ToList())
            //{
            //    // Hide the image
            //    //				this.HotfolderListbox.HideItem<HotfolderImage>(image);

            //    // Add the filepath to the list
            //    selectedImageFileNames.Add(image.ImageFilename);
            //}

            // If a filepath was passed as an argument and the list does not
            // already contain this filepath, then add it to the list
            if (!(imageFileName == null || selectedImageFileNames.Contains(imageFileName)))
            {
                selectedImageFileNames.Add(imageFileName);
            }

            // Invoke the assigment
            this.HotfolderImageDeleted(
                this,
                new EventArgs<Collection<string>>(selectedImageFileNames)
            );

        }
		/// <summary>
		/// Selects all hotfolder images in preparation of assignment to the current subject record
		/// </summary>
		internal void AssignAllImages()
		{
			this.HotfolderListbox.SelectAllItems();
			this.AssignImages(null);
		}


		private void HotfolderListbox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if (e.AddedItems.Count > 0)
			{
				HotfolderImage selectedImage = e.AddedItems[0] as HotfolderImage;
				OnSelectedHotfolderImageChanged(selectedImage);
			}
			else
			{
				OnSelectedHotfolderImageChanged(null);
			}
		}

		private void HotfolderListbox_ImageDoubleClick(object sender, EventArgs e)
		{
			AssignImages(null);
		}

        private void HotfolderListbox_GroupImageClick(object sender, EventArgs e)
        {
            AssignGroupImages(null);
        }

        private void HotfolderListbox_DeleteImageClick(object sender, EventArgs e)
        {
            try
            {
                Button source = (Button)sender;
                HotfolderImage img = source.DataContext as HotfolderImage;

                if (!this.HotfolderListbox.GetSelectedItems<HotfolderImage>().Contains(img))
                {
                    DeleteImages(img.ImageFilename);
                }
                else
                {
                    foreach (HotfolderImage image in this.HotfolderListbox.GetSelectedItems<HotfolderImage>().ToList())
                    {
                        DeleteImages(image.ImageFilename);
                    }
                }
                //clear the image
                ((HotfolderImage)source.CommandParameter).Clear();
                ((HotfolderImage)source.CommandParameter).Dispose();
            }
            catch (Exception ex)
            {
                //do nothing
            }
        }

        private void btnImageImporter_Click(object sender, RoutedEventArgs e)
        {
            // Invoke the assigment
            this.ImageImport(this, null);
        }
	
	}
}
