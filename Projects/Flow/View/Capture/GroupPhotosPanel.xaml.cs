﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Lib;
using Flow.Lib.Preferences;
using Flow.Controller.Preferences;
using Flow.Controller;
using Flow.Lib.ImageUtils;
using Flow.Schema.LinqModel.DataContext;
using Flow.Lib.Helpers;

using NLog;

namespace Flow.View.Capture
{
    /// <summary>
    /// Interaction logic for CurrentImage.xaml
    /// </summary>
    public partial class GroupPhotosPanel : UserControl
    {

        public event EventHandler<EventArgs<GroupImage>>
           SelectedGroupImageChanged = delegate { };


		private FlowController _flowController = null;
		public FlowController FlowController
		{
			get { return _flowController; }
			set
			{
				_flowController = value;
				this.PreferenceManager = _flowController.ProjectController.FlowMasterDataContext.PreferenceManager;
			}
		}

        private static Logger logger = LogManager.GetCurrentClassLogger();

		private PreferenceManager PreferenceManager { get; set; }


        FlowImage selectedFlowImage { get; set; }
        string currentOverlayPng { get; set; }

        public GroupPhotosPanel()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void GroupPhotoListbox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                GroupImage selectedImage = e.AddedItems[0] as GroupImage;
                OnSelectedGroupImageChanged(selectedImage);
            }
            else
            {
                OnSelectedGroupImageChanged(null);
            }
        }

        private void GroupPhotoListbox_ImageDoubleClick(object sender, EventArgs e)
        {

        }

        private void GroupPhotoListbox_DeleteImageClick(object sender, EventArgs e)
        {

        }


        protected void OnSelectedGroupImageChanged(GroupImage image)
        {
            logger.Info("Selected group image changed and is now {0}", (image == null) ? "null" : image.ImageFileName);
            //            if (SelectedHotfolderImageChanged != null)
            SelectedGroupImageChanged(this, new EventArgs<GroupImage>(image));
        }









//        public event EventHandler<EventArgs<GroupImage>>
//            SelectedGroupImageChanged = delegate { };

//        public event EventHandler<EventArgs<Collection<string>>>
//            GroupImageAssigned = delegate { };

      
//        public event EventHandler<EventArgs<Collection<string>>>
//            GroupImageDeleted = delegate { };

//        protected static DependencyProperty GroupImageListProperty = DependencyProperty.Register(
//            "GroupImageList", typeof(GroupImages), typeof(GroupPhotosPanel)
//        );



//        public void SetDataContext(HotfolderImages hotfolderImages)
//        {
//            HotfolderListbox.ItemsSource = hotfolderImages;
//            //this.HotfolderImageList = hotfolderImages;
//            //this.HotfolderListbox.DataContext = this.HotfolderListbox;
//        }

//        protected void OnSelectedHotfolderImageChanged(HotfolderImage image)
//        {
////            if (SelectedHotfolderImageChanged != null)
//                SelectedHotfolderImageChanged(this, new EventArgs<HotfolderImage>(image));
//        }

//        /// <summary>
//        /// Assigns the selected image to the current Subject record
//        /// </summary>
//        private void MenuItem_Click(object sender, RoutedEventArgs e)
//        {
//            string imageFileName = (sender as MenuItem).CommandParameter as string;
//            AssignImages(imageFileName);
//        }


//        /// <summary>
//        /// Prepares 
//        /// </summary>
//        /// <param name="imageFileName"></param>
//        private void AssignImages(string imageFileName)
//        {

//            // List of filepaths of the images to be assigned
//            Collection<string> selectedImageFileNames = new Collection<string>();

//            // Iterate through the list of selected images
//            foreach (HotfolderImage image in this.HotfolderListbox.GetSelectedItems<HotfolderImage>().ToList())
//            {
//                // Hide the image
////				this.HotfolderListbox.HideItem<HotfolderImage>(image);

//                // Add the filepath to the list
//                selectedImageFileNames.Add(image.ImageFilename);
//            }

//            // If a filepath was passed as an argument and the list does not
//            // already contain this filepath, then add it to the list
//            if (!(imageFileName == null || selectedImageFileNames.Contains(imageFileName)))
//            {
//                selectedImageFileNames.Add(imageFileName);
//            }

//            // Invoke the assigment
//            this.HotfolderImageAssigned(
//                this,
//                new EventArgs<Collection<string>>(selectedImageFileNames)
//            );

//        }

//        private void AssignGroupImages(string imageFileName)
//        {

//            // List of filepaths of the images to be assigned
//            Collection<string> selectedImageFileNames = new Collection<string>();

//            // Iterate through the list of selected images
//            foreach (HotfolderImage image in this.HotfolderListbox.GetSelectedItems<HotfolderImage>().ToList())
//            {
//                // Hide the image
//                //				this.HotfolderListbox.HideItem<HotfolderImage>(image);

//                // Add the filepath to the list
//                selectedImageFileNames.Add(image.ImageFilename);
//            }

//            // If a filepath was passed as an argument and the list does not
//            // already contain this filepath, then add it to the list
//            if (!(imageFileName == null || selectedImageFileNames.Contains(imageFileName)))
//            {
//                selectedImageFileNames.Add(imageFileName);
//            }

//            // Invoke the assigment
//            this.HotfolderGroupImageAssigned(
//                this,
//                new EventArgs<Collection<string>>(selectedImageFileNames)
//            );

//        }

//        private void DeleteImages(string imageFileName)
//        {

//            // List of filepaths of the images to be deleted
//            Collection<string> selectedImageFileNames = new Collection<string>();

//            // Iterate through the list of selected images
//            //foreach (HotfolderImage image in this.HotfolderListbox.GetSelectedItems<HotfolderImage>().ToList())
//            //{
//            //    // Hide the image
//            //    //				this.HotfolderListbox.HideItem<HotfolderImage>(image);

//            //    // Add the filepath to the list
//            //    selectedImageFileNames.Add(image.ImageFilename);
//            //}

//            // If a filepath was passed as an argument and the list does not
//            // already contain this filepath, then add it to the list
//            if (!(imageFileName == null || selectedImageFileNames.Contains(imageFileName)))
//            {
//                selectedImageFileNames.Add(imageFileName);
//            }

//            // Invoke the assigment
//            this.HotfolderImageDeleted(
//                this,
//                new EventArgs<Collection<string>>(selectedImageFileNames)
//            );

//        }
//        /// <summary>
//        /// Selects all hotfolder images in preparation of assignment to the current subject record
//        /// </summary>
//        internal void AssignAllImages()
//        {
//            this.HotfolderListbox.SelectAllItems();
//            this.AssignImages(null);
//        }


//        private void HotfolderListbox_SelectionChanged(object sender, SelectionChangedEventArgs e)
//        {
//            if (e.AddedItems.Count > 0)
//            {
//                HotfolderImage selectedImage = e.AddedItems[0] as HotfolderImage;
//                OnSelectedHotfolderImageChanged(selectedImage);
//            }
//            else
//            {
//                OnSelectedHotfolderImageChanged(null);
//            }
//        }

//        private void HotfolderListbox_ImageDoubleClick(object sender, EventArgs e)
//        {
//            AssignImages(null);
//        }

//        private void HotfolderListbox_GroupImageClick(object sender, EventArgs e)
//        {
//            AssignGroupImages(null);
//        }

//        private void HotfolderListbox_DeleteImageClick(object sender, EventArgs e)
//        {
//            Button source = (Button)sender;
//            DeleteImages(((HotfolderImage)source.CommandParameter).ImageFilename);
//        }
	

        

	}

}
