﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Flow.Designer.Lib.SurfaceUi;
using Flow.Lib.Helpers;
using Flow.ViewModel.Capture;
using Flow.Controller.Project;
using Flow.Schema.LinqModel.DataContext;
using Flow.View.Dialogs;

namespace Flow.View.Capture
{
    /// <summary>
    /// Interaction logic for GroupPanel.xaml
    /// </summary>
    public partial class GroupPanel : UserControl
    {
        
        SurfaceSelectionAdorner SelectionAdorner { get; set; }
        SurfaceSelectionBand2 SelectionBand { get; set; }

        public GroupPanel()
        {
            InitializeComponent();
            ShowHideGroupData();
        }

        private void _this_Loaded(object sender, RoutedEventArgs e)
        {
            ShowHideGroupData();
        }

        private void GroupPhotoListbox_DeleteImageClick(object sender, EventArgs e)
        {

        }

        private void AddGroup_Click(object sender, RoutedEventArgs e)
        {
            pnlNewGroup.Visibility = System.Windows.Visibility.Visible;
        }

        private void DeleteGroup_Click(object sender, RoutedEventArgs e)
        {
            Group g = (Group)cmbGroups.SelectedValue;
            if (g == null)
                return;

            FlowMessageBox msg = new FlowMessageBox("Delete Warning", "You are about to Delete a Group, are you sure you want to do this?");
            if (msg.ShowDialog() == true)
            {
                CaptureDockPanel cdp = (CaptureDockPanel)this.DataContext;
                
                foreach (Subject s in g.Subjects)
                {
                    g.RemoveSubject(s);
                }
                foreach (GroupImage gi in g.GroupImages)
                {
                    g.RemoveGroupImage(gi);
                }
                cdp.CaptureController.FlowController.ProjectController.CurrentFlowProject.FlowProjectDataContext.Groups.DeleteOnSubmit(g);
                cdp.CaptureController.FlowController.ProjectController.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();

                cdp.CaptureController.FlowController.ProjectController.CurrentFlowProject.RefreshGroupList();

                
                
                ShowHideGroupData();
            }
        }

        private void CreateNewGroup_Click(object sender, RoutedEventArgs e)
        {
            Group g = CreateNewGroup(txtNewGroupName.Text);
            CaptureDockPanel cdp = (CaptureDockPanel)this.DataContext;
            cdp.FlowController.ProjectController.CurrentFlowProject.CurrentGroup = g;
           
        }

        private Group CreateNewGroup(string newName)
        {
           
            if (!String.IsNullOrEmpty(newName))
            {
                Group g = new Group();
                CaptureDockPanel cdp = (CaptureDockPanel)this.DataContext;

                g.GroupDesc = newName;
                g.GroupGuid = Guid.NewGuid();
                g.FlowProjectDataContext = cdp.CaptureController.FlowController.ProjectController.CurrentFlowProject.FlowProjectDataContext;
                
                cdp.CaptureController.FlowController.ProjectController.CurrentFlowProject.FlowProjectDataContext.Groups.InsertOnSubmit(g);
                cdp.CaptureController.FlowController.ProjectController.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();
                txtNewGroupName.Text = "";
                pnlNewGroup.Visibility = System.Windows.Visibility.Collapsed;
                cdp.CaptureController.FlowController.ProjectController.CurrentFlowProject.RefreshGroupList();
                ShowHideGroupData();
                return g;
            }

            return null;
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CaptureDockPanel cdp = (CaptureDockPanel)this.DataContext;

            Group g = (Group)cmbGroups.SelectedValue;
            if (g != null)
            {
                g.FlowProjectDataContext = cdp.CaptureController.FlowController.ProjectController.CurrentFlowProject.FlowProjectDataContext;
                this.lstGroups.DataContext = cmbGroups.SelectedValue;
                this.lstSubjects.DataContext = cmbGroups.SelectedValue;
                this.grdGroupData.DataContext = cmbGroups.SelectedValue;
                //cdp.CaptureController.SubjectRecordController.CurrentSubject;
                //this.CurrentFlowProject.SubjectList.CurrentItem
                cdp.CaptureController.FlowController.ProjectController.CurrentFlowProject.CurrentGroup = g;
            }

            ShowHideGroupData();
        }

        private void ShowHideGroupData()
        {
            CaptureDockPanel cdp = (CaptureDockPanel)this.DataContext;
            if (cdp == null) return;
            if (cmbGroups.HasItems && cmbGroups.SelectedIndex == -1)
                cmbGroups.SelectedIndex = 0;

            Group g = (Group)cmbGroups.SelectedValue;
            if (g == null)
            {
                grdCurrentGroup.Visibility = Visibility.Collapsed;
                if (cmbGroups.HasItems)
                    pnlAutoCreateGroups.Visibility = Visibility.Collapsed;
                else
                {
                    if (cdp != null && cdp.FlowController.ProjectController.CurrentFlowProject.UniqueClasses.Count > 0)
                    {
                        pnlAutoCreateGroups.Visibility = Visibility.Visible;
                        chkNewGroupClass.Visibility = System.Windows.Visibility.Collapsed;
                        chkNewGroupTeacher.Visibility = System.Windows.Visibility.Collapsed;
                        chkNewGroupHomeroom.Visibility = System.Windows.Visibility.Collapsed;
                        chkNewGroupTeam.Visibility = System.Windows.Visibility.Collapsed;
                        chkNewGroupCoach.Visibility = System.Windows.Visibility.Collapsed;

                        if (cdp.FlowController.ProjectController.CurrentFlowProject.UniqueClasses.Count > 0)
                        {
                            chkNewGroupClass.Visibility = System.Windows.Visibility.Visible;
                            chkNewGroupClass.IsChecked = true;
                        }
                        if (cdp.FlowController.ProjectController.CurrentFlowProject.UniqueTeachers.Count > 0)
                            chkNewGroupTeacher.Visibility = System.Windows.Visibility.Visible;

                    }
                    else
                    {
                        pnlAutoCreateGroups.Visibility = Visibility.Collapsed;
                    }
                }
            }
            else
            {
                grdCurrentGroup.Visibility = Visibility.Visible;
                pnlAutoCreateGroups.Visibility = Visibility.Collapsed;
            }

            pnlClass.Visibility = Visibility.Collapsed;
            pnlTeacher.Visibility = Visibility.Collapsed;
            pnlHomeroom.Visibility = Visibility.Collapsed;
            pnlTeam.Visibility = Visibility.Collapsed;
            pnlCoach.Visibility = Visibility.Collapsed;

            if (cdp.FlowController.ProjectController.CurrentFlowProject.UniqueClasses.Count > 0)
                pnlClass.Visibility = Visibility.Visible;
            if (cdp.FlowController.ProjectController.CurrentFlowProject.UniqueTeachers.Count > 0)
                pnlTeacher.Visibility = Visibility.Visible;

            
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            CaptureDockPanel cdp = (CaptureDockPanel)this.DataContext;
            cdp.CaptureController.FlowController.ProjectController.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();
        }

        private void AddSub_Click(object sender, RoutedEventArgs e)
        {
            Button b = (Button)sender;
            CaptureDockPanel cdp = (CaptureDockPanel)this.DataContext;
            Group g = (Group)cmbGroups.SelectedValue;
            g.AssignSubject((Subject)b.DataContext);
        }

        private void RemoveSub_Click(object sender, RoutedEventArgs e)
        {
            Button b = (Button)sender;
            CaptureDockPanel cdp = (CaptureDockPanel)this.DataContext;
            Group g = (Group)cmbGroups.SelectedValue;
            g.RemoveSubject((Subject)b.DataContext);
        }

        private void Label_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            Subject s = (Subject)((StackPanel)sender).DataContext;
             CaptureDockPanel cdp = (CaptureDockPanel)this.DataContext;
             cdp.CaptureController.FlowController.ProjectController.GotoTicketID(s.TicketCode);
        }

        private void Image_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            Subject s = (Subject)((Image)sender).DataContext;
            ShowAnOrder show = new ShowAnOrder(s.PrimaryImage);
            show.ShowDialog();
        }

        private void GroupImage_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            GroupImage g = (GroupImage)((Image)sender).DataContext;
            imgGroupImageLarge.Source = g.IntermediateImage;
        }

        private void DeleteGroupImage_Click(object sender, RoutedEventArgs e)
        {
            Group g = (Group)cmbGroups.SelectedValue;
            GroupImage gi = (GroupImage)((Button)sender).DataContext;
            g.RemoveGroupImage(gi);

        }

        private void AutoGroups_Click(object sender, RoutedEventArgs e)
        {
            CaptureDockPanel cdp = (CaptureDockPanel)this.DataContext;
            if (chkNewGroupClass.IsChecked == true)
            {
                foreach (string thisClass in cdp.FlowController.ProjectController.CurrentFlowProject.UniqueClasses)
                {
                    Group g = CreateNewGroup(thisClass);
                    if (g != null)
                    {
                        if (chkAutoAssignSubjects.IsChecked == true)
                        {
                            foreach (Subject s in cdp.FlowController.ProjectController.CurrentFlowProject.SubjectList)
                            {
                                if ((s.SubjectData["Class"].Value as String) == thisClass)
                                    g.AssignSubject(s);
                            }
                        }
                        cdp.FlowController.ProjectController.CurrentFlowProject.CurrentGroup = g;
                    }
                }
            }

        }

        

    }

}
