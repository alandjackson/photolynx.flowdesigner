﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Flow.Designer.Lib.SurfaceUi;
using Flow.Lib.Helpers;
using Flow.ViewModel.Capture;

namespace Flow.View.Capture
{
    /// <summary>
    /// Interaction logic for GrayCardPanel.xaml
    /// </summary>
    public partial class GrayCardPanel : UserControl
    {
        SurfaceSelectionAdorner SelectionAdorner { get; set; }
        SurfaceSelectionBand2 SelectionBand { get; set; }

        ImageSource barFourSource;
        ImageSource barThreeSource;
        ImageSource barTwoSource;
        ImageSource barOneSource;
        ImageSource barNoneSource;
        ImageSource barDisabledSource;

        public GrayCardPanel()
        {
            InitializeComponent();


            barFourSource = bar_n_4.Source;
            barThreeSource = bar_n_3.Source;
            barTwoSource = bar_n_2.Source;
            barOneSource = bar_n_1.Source;
            barNoneSource = bar_0.Source;
            barDisabledSource = bar_p_1.Source;

            bar_n_4.Source = barDisabledSource;
            bar_n_3.Source = barDisabledSource;
            bar_n_2.Source = barDisabledSource;
            bar_n_1.Source = barDisabledSource;
            bar_0.Source = barDisabledSource;
            bar_lbl.Content = "";

            SelectionBand = new SurfaceSelectionBand2()
                {
                    DragRectangle = selRectangle,
                    BoundsControl = imgGrid,
                    SelectionControl = imgCanvas,
                    MouseDownControl = imgCurrentImage
                    
                }.Init();
            SizeChanged += new SizeChangedEventHandler(GrayCardPanel_SizeChanged);
            SelectionBand.AreaSelected += new EventHandler<AreaSelectedArgs>(SelectionBand_AreaSelected);
        }

        void GrayCardPanel_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            SelectionBand.DragRectangle.Visibility = Visibility.Hidden;
        }

        void SelectionBand_AreaSelected(object sender, AreaSelectedArgs e)
        {
            UpdateArea(e);
        }

        protected void UpdateArea(AreaSelectedArgs e)
        {
            Rect imgAreaRel;
            try
            {
                var imgPoint = e.PointArgs.GetPosition(imgCurrentImage);
                var canvasPoint = e.PointArgs.GetPosition(imgCanvas);
                var xdiff = imgPoint.X - canvasPoint.X;
                var ydiff = imgPoint.Y - canvasPoint.Y;

                Rect imgArea = new Rect(
                    e.Area.X + xdiff,
                    e.Area.Y + ydiff,
                    e.Area.Width,
                    e.Area.Height);

                imgAreaRel = new Rect(
                    imgArea.X / imgCurrentImage.ActualWidth,
                    imgArea.Y / imgCurrentImage.ActualHeight,
                    imgArea.Width / imgCurrentImage.ActualWidth,
                    imgArea.Height / imgCurrentImage.ActualHeight);

            }
            catch (Exception)
            {
                return;
            }

            var vm = DataContext as GrayCardViewModel;
            if (vm != null)
                vm.ImageAreaSelected = imgAreaRel;
        }

        public void UpdateBarPosGumballs(int pos)
        {

            bar_n_4.Source = pos == -4 ? barFourSource : barDisabledSource;
            bar_n_3.Source = pos == -3 ? barThreeSource : barDisabledSource;
            bar_n_2.Source = pos == -2 ? barTwoSource : barDisabledSource;
            bar_n_1.Source = pos == -1 ? barOneSource : barDisabledSource;
            bar_0.Source = pos == 0 ? barNoneSource : barDisabledSource;
            bar_p_1.Source = pos == 1 ? barOneSource : barDisabledSource;
            bar_p_2.Source = pos == 2 ? barTwoSource : barDisabledSource;
            bar_p_3.Source = pos == 3 ? barThreeSource : barDisabledSource;
            bar_p_4.Source = pos == 4 ? barFourSource : barDisabledSource;

            if (pos < 0)
                bar_lbl.Content = "UNDER";
            else if (pos == 0)
                bar_lbl.Content = "OK";
            else
                bar_lbl.Content = "OVER";

        }
    }

    public class AreaSelectedArgs : EventArgs
    {
        public Rect Area { get; set; }
        public MouseButtonEventArgs PointArgs { get; set; }

        public AreaSelectedArgs()
        {
        }
    }


    public class SurfaceSelectionBand2
    {
        public event EventHandler<AreaSelectedArgs> AreaSelected;
        protected void OnAreaSelected(AreaSelectedArgs args)
        {
            if (AreaSelected != null)
                AreaSelected(this, args);
        }

        public Rectangle DragRectangle { get; set; }
        protected bool _dragInProgress = false;
        protected Point _startPoint;

        public FrameworkElement SelectionControl { get; set; }
        public FrameworkElement BoundsControl { get; set; }
        public FrameworkElement MouseDownControl { get; set; }
        public Panel DragRectanglePanel { get; set; }

        public SurfaceSelectionBand2()
        {

        }

        public SurfaceSelectionBand2 Init()
        {
            if (SelectionControl == null)
                throw new Exception("SelectionControl is null");
            if (BoundsControl == null)
                throw new Exception("Bounds Control is null");
            if (DragRectangle == null && DragRectanglePanel == null)
                throw new Exception("Need to create drag rectangle, but DragRectanglePanel is null");

            if (MouseDownControl != null)
                MouseDownControl.MouseLeftButtonDown += new MouseButtonEventHandler(uxLayoutCanvas_MouseLeftButtonDown);

            SelectionControl.MouseLeftButtonDown += new MouseButtonEventHandler(uxLayoutCanvas_MouseLeftButtonDown);
            BoundsControl.MouseLeftButtonUp += new MouseButtonEventHandler(uxLayoutCanvas_MouseLeftButtonUp);
            BoundsControl.MouseMove += new MouseEventHandler(uxLayoutCanvas_MouseMove);

            if (DragRectangle == null)
            {
                DragRectangle = new Rectangle();
                DragRectangle.Visibility = System.Windows.Visibility.Hidden;
                DragRectangle.RadiusX = 3;
                DragRectangle.RadiusY = 3;
                DragRectangle.Stroke = new SolidColorBrush(Colors.Black);
                DragRectangle.StrokeThickness = 1;
                DragRectangle.Fill = new SolidColorBrush(Colors.Blue);
                DragRectangle.Fill.Opacity = .20;
                DragRectanglePanel.Children.Add(DragRectangle);
            }
            return this;
        }


        void uxLayoutCanvas_MouseMove(object sender, MouseEventArgs e)
        {
            // BUG: Max calculations mess up the rect when moving negative
            // TODO: bound the rect to the top/left edges of the canvas
            if (_dragInProgress)
            {
                Point p = e.GetPosition(SelectionControl);

                double w = p.X - _startPoint.X;
                double h = p.Y - _startPoint.Y;
                double x = _startPoint.X;
                double y = _startPoint.Y;
                double wMax = SelectionControl.ActualWidth - x;
                double hMax = SelectionControl.ActualHeight - y;
                if (w < 0)
                {
                    x = p.X;
                    w = -w;
                }
                if (w > wMax)
                    w = wMax;
                if (h < 0)
                {
                    y = p.Y;
                    h = -h;
                }
                if (h > hMax)
                    h = hMax;

                Canvas.SetLeft(DragRectangle, x);
                Canvas.SetTop(DragRectangle, y);
                DragRectangle.Width = w;
                DragRectangle.Height = h;
            }
        }

        void uxLayoutCanvas_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (!_dragInProgress)
                return;

            _dragInProgress = false;
            //DragRectangle.Visibility = Visibility.Hidden;
            OnAreaSelected(new AreaSelectedArgs()
            {
                Area = new Rect(
                    Canvas.GetLeft(DragRectangle), 
                    Canvas.GetTop(DragRectangle),
                    DragRectangle.Width, 
                    DragRectangle.Height),
                PointArgs = e
            });
        }

        void uxLayoutCanvas_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            _dragInProgress = true;
            _startPoint = e.GetPosition(SelectionControl);
            Canvas.SetLeft(DragRectangle, _startPoint.X);
            Canvas.SetTop(DragRectangle, _startPoint.Y);
            DragRectangle.Width = 0;
            DragRectangle.Height = 0;
            DragRectangle.Visibility = Visibility.Visible;
        }



        void uxLayoutCanvas_MouseDown(object sender, MouseButtonEventArgs e)
        {
        }

    }

}
