﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Controller;
using Flow.Controller.Capture;
using Flow.Controller.Catalog;
using Flow.Lib;
using Flow.View.Project;
using Flow.View.Project.Base;
using Flow.Schema.LinqModel.DataContext;
using Flow.View.Dialogs;
using StructureMap;
using System.IO;

namespace Flow.View
{
    /// <summary>
    /// Interaction logic for CaptureDockPanel.xaml
    /// </summary>
    public partial class CaptureDockPanel : ProjectPanelBase
    {
        public CaptureController CaptureController { get; set; }

        public HotFolderController HotFolderController
        {
            set { if (value != null) value.Main(HotfolderPanel); }
            
        }

        public CurrentImageController CurrentImageController
        { set { if (value != null) value.Main(CurrentImagePanel); } }

		public SubjectRecordController SubjectRecordController
		{ set { if (value != null) value.Main(SubjectRecordPanel); } }

        private OrderEntryController _orderEntryController { get; set; }
		public OrderEntryController OrderEntryController
        { 
            set { if (value != null) value.Main(OrderEntryPanel); _orderEntryController = value; }
            get { return _orderEntryController; }
        }


        public CaptureDockPanel()
        {
            InitializeComponent();
        }

		// obsolete...
        private bool layoutFileLoaded = false;
		protected override void OnSetFlowController()
		{
			base.OnSetFlowController();
            this.DataContext = this.FlowController.ProjectController.FlowMasterDataContext;
            
//			this.SubjectRecordPanel.SubjectRecordDisplay.SetDataContext(this.CurrentFlowProject);

            //create the defaultsettings on first run before the user can change the settings (will be used to restore settings if needed)
            string defaultLayoutFile = System.IO.Path.Combine(FlowContext.FlowConfigDirPath, "DefaultDockSettings.xml");
            if(!File.Exists(defaultLayoutFile))
                this.DockMgr.SaveLayout(defaultLayoutFile);

            if (!layoutFileLoaded)
            {
                string layoutFile = System.IO.Path.Combine(FlowContext.FlowConfigDirPath, "DockSettings.xml");
                if (System.IO.File.Exists(layoutFile))
                    this.DockMgr.RestoreLayout(layoutFile);
                layoutFileLoaded = true;
            }

		}

        void clearKeyMap()
        {
            if(this.OrderEntryController != null)
                this.OrderEntryController.clearKeyMap();
        }

		/// <summary>
		/// Attempts to intercept the following keystrokes for HotKey handling:
		/// F3: Search focus
		/// F8: Order entry
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>

        DateTime lastF4 = DateTime.Now;
		private void CaptureDockPanel_PreviewKeyUp(object sender, KeyEventArgs e)
		{
            //if (e.Key == Key.Back || e.Key == Key.Delete)
            //{
            //    clearKeyMap();
            //    return;
            //}

            //if (this.ProjectController.CurrentFlowProject.FlowCatalog == null || this.ProjectController.CurrentFlowProject.FlowCatalog.ProductPackages == null)
            //    return;
            
            //keyHistory = keyHistory + e.Key;
            //foreach (ProductPackage pp in this.ProjectController.CurrentFlowProject.FlowCatalog.ProductPackages)
            //{
            //    if (pp.Map.ToLower() == keyHistory.ToLower())
            //    {
            //        this.OrderEntryController.AddOrderPackage(pp);
            //        clearKeyMap();
            //        break;
            //    }
            //}
            //OrderEntryPanel.lblCurrentKeyMap.Content = keyHistory;


            if (e.Key == Key.M && (Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control)
            {
                this.CaptureController.DisplayMrGray();
                clearKeyMap();
            }
            if (e.Key == Key.F1)
            {
                this.CaptureController.PrintOneLayout();
                clearKeyMap();
            }
            //F3 and NOT Ctrl+F3
            if(e.Key == Key.F3 && Keyboard.Modifiers != ModifierKeys.Control)
            {
                this.CaptureController.SetSearchFocus();
                clearKeyMap();
            }
            if (e.Key == Key.F8)
            {
                if (this.CaptureController.FlowController.ProjectController.CurrentFlowProject.SubjectCount > 0)
                    this.CaptureController.DisplayOrderEntry();
                clearKeyMap();
            }
            if (e.Key == Key.F5)
            {
                this.CaptureController.AddNewSubject();
                clearKeyMap();
            }
            if ((e.Key == Key.F10) || (e.Key == Key.System && e.SystemKey == Key.F10))
            {
                this.CaptureController.EditSubject();
                clearKeyMap();
            }

            if (((e.Key == Key.F5) || (e.Key == Key.System && e.SystemKey == Key.F5)) && (Keyboard.Modifiers & ModifierKeys.Alt) == ModifierKeys.Alt)
            {
                this.CaptureController.DeleteSubject();
                clearKeyMap();
            }

            if (e.Key == Key.F4)
            {
                //if F7 was pressed within 3 seconds, open SearchAllProject
                if (lastF4 >= DateTime.Now.AddSeconds(-2))
                    this.HotfolderPanel.AssignAllImages();
                else
                    this.HotfolderPanel.AssignImages(null);
                clearKeyMap();
                lastF4 = DateTime.Now;
            }
            //switch (e.Key)
            //{
            //    // [F3]: Set search focus
            //    case Key.F3:
            //        this.CaptureController.SetSearchFocus();
            //        break;

            //    // [F8]: Invoke order entry
            //    case Key.F8:
            //        if(this.CaptureController.FlowController.ProjectController.CurrentFlowProject.SubjectCount > 0)
            //            this.CaptureController.DisplayOrderEntry();
		
            //        break;
            //    // [Ctrl]+[M]: Invoke Mr Gray
            //    case Key.LeftCtrl | Key.M:
            //        this.CaptureController.DisplayMrGray();
            //        break;
            //    case Key.RightCtrl | Key.M:
            //        this.CaptureController.DisplayMrGray();
            //        break;

            //}
		}

		/// <summary>
		/// Invoked when text input is detected within the CaptureDockPanel; adds
		/// the input text the BarCodeScanDetector input buffer and tests the buffer value
		/// against the barcode scan key pattern, invoking a barcode lookup if so
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void ProjectPanelBase_PreviewTextInput(object sender, TextCompositionEventArgs e)
		{
			string inputText = e.Text;

			// If the input is a carriage return, flag this
			bool endsWithCarriageReturn = inputText.EndsWith("\r");

			// Match the current barcode scan input buffer against the barcode scan key pattern
			if(this.CaptureController.DetectScan(inputText, endsWithCarriageReturn))
				e.Handled = true;
		}


        private void GrayCardPanel_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if(this.GrayCardPanel.IsVisible)
                this.CaptureController.GrayCardViewModel.MainImage = this.CaptureController.HotfolderController.CurrentHotfolderImage;
        }

        private void CameraPanel_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {

        }
        
        private void _this_KeyDown(object sender, KeyEventArgs e)
        {
            if (CaptureController != null && CaptureController.FlowController != null &&
                CaptureController.FlowController.ScanController != null)
            {
                if (CaptureController.FlowController.ScanController.addKeyStroke(e.Key))
                {
                    //true means key has been handled
                    e.Handled = true;
                    //this.FlowController.ProjectController.specialBarCodeHandled = false;
                }
            }
           
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var temp = this.DataContext;
            CameraDialog dlg = new CameraDialog(this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Capture.HotFolderDirectory);
            dlg.ShowDialog();
        }

        private void btnClearHotfolder_Click(object sender, RoutedEventArgs e)
        {
            this.CaptureController.ArchiveHotFolder();
        }

        private void btnOpenArchive_Click(object sender, RoutedEventArgs e)
        {
            string hotfolderArchiveRoot = System.IO.Path.Combine(FlowContext.FlowAppDataDirPath, "HotFolderArchieve");
            string hotfolderArchiveDir = System.IO.Path.Combine(hotfolderArchiveRoot, this.FlowController.ProjectController.CurrentFlowProject.FileSafeProjectName);

            if(Directory.Exists(hotfolderArchiveDir))
                System.Diagnostics.Process.Start(hotfolderArchiveDir);
        }

        private void GroupPanel_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {

        }
       


    }
}
