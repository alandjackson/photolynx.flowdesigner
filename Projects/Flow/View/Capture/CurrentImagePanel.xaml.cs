﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Lib;
using Flow.Lib.Preferences;
using Flow.Controller.Preferences;
using Flow.Controller;
using Flow.Lib.ImageUtils;
using Flow.Schema.LinqModel.DataContext;
using Flow.Lib.Helpers;
using Flow.View.Dialogs;

namespace Flow.View.Capture
{
    /// <summary>
    /// Interaction logic for CurrentImage.xaml
    /// </summary>
    public partial class CurrentImagePanel : UserControl
    {
		private FlowController _flowController = null;
		public FlowController FlowController
		{
			get { return _flowController; }
			set
			{
				_flowController = value;
				this.PreferenceManager = _flowController.ProjectController.FlowMasterDataContext.PreferenceManager;
			}
		}

		private PreferenceManager PreferenceManager { get; set; }


        FlowImage selectedFlowImage { get; set; }
        string currentOverlayPng { get; set; }

		public CurrentImagePanel()
        {
            InitializeComponent();

//			checkBoxCropOverlay.DataContext = this;
            imgCurrentImage.SizeChanged += new SizeChangedEventHandler(imgCurrentImage_SizeChanged);
           // imgOverlay.LayoutUpdated += new EventHandler(imgOverlay_LayoutUpdated);

            pnlGroupImageSettings.Visibility = Visibility.Collapsed;
        }

		//protected static readonly DependencyProperty UseOverlayPngProperty =
		//    DependencyProperty.Register("UseOverlayPng", typeof(bool), typeof(CurrentImagePanel));

		//public bool UseOverlayPng
		//{
		//    get
		//    {
		//        return (bool)this.GetValue(UseOverlayPngProperty);

		//        //return Convert.ToBoolean(this.FlowController.ProjectController.FlowMasterDataContext
		//        //      .PreferenceManager["Capture.UseOverlayPng"].NativeValue);
		//    }
		//    set
		//    {
		//        this.SetValue(UseOverlayPngProperty, value);

		//        //this.FlowController.ProjectController.FlowMasterDataContext
		//        //      .PreferenceManager["Capture.UseOverlayPng"].NativeValue = Convert.ToString(value);
		//        //FlowController.ProjectController.FlowMasterDataContext.SubmitChanges();
		//    }
		//}

		private bool UseOverlayPng { get; set; }

		internal void DisplaySelectedImage(FlowImage selectedImage)
		{
            pnlGroupImageSettings.Visibility = Visibility.Collapsed;
            if (ConfigUtil.GetBoolSetting("CAPTURE_CURRENT_IMG_DISABLED"))
                return;

            if (selectedImage == null)
            {
                imgCurrentImage.Source = null;
                imgOverlay.Source = null;
            }
            else
            {
                imgCurrentImage.Source = selectedImage.GetProcessingImage();
                CheckOverlay();
            }
		}

        public GroupImage GroupImage { get; set; }
        public bool isGroupImage{
            get
            {
                if (GroupImage != null) return true;

                return false;
            }

        }
        internal void DisplaySelectedGroupImage(GroupImage selectedImage)
        {
            GroupImage = selectedImage;

            if (ConfigUtil.GetBoolSetting("CAPTURE_CURRENT_IMG_DISABLED"))
                return;

            if (selectedImage == null)
            {
                imgCurrentImage.Source = null;
                imgOverlay.Source = null;
                pnlGroupImageSettings.Visibility = Visibility.Collapsed;
            }
            else if (FlowContext.ShowNewFeatures)
            {
                imgCurrentImage.Source = selectedImage.GetProcessingImage();
                CheckOverlay();

                pnlGroupImageSettings.Visibility = Visibility.Visible;
                cmbGroupImageFlag.ItemsSource = this.FlowController.ProjectController.CurrentFlowProject.SelectedGroupImageFlags;
                cmbGroupImageFlag.SelectedValue = selectedImage.SelectedFlag;

                if (this.FlowController.ProjectController.CurrentFlowProject.SelectedGroupImageFlags.Count > 1)
                    pnlSelectedFlag.Visibility = Visibility.Visible;
                else
                    pnlSelectedFlag.Visibility = Visibility.Collapsed;

                if (this.FlowController.ProjectController.CurrentFlowProject.UniqueTeachers != null)
                {
                    pnlApplyToTeacher.Visibility = Visibility.Visible;
                    cmbAllTeachers.ItemsSource = null;
                    cmbAllTeachers.ItemsSource = this.FlowController.ProjectController.CurrentFlowProject.UniqueTeachers;
                }
                else
                    pnlApplyToTeacher.Visibility = Visibility.Collapsed;

                if (this.FlowController.ProjectController.CurrentFlowProject.UniqueGrades != null)
                {
                    pnlApplyToGrade.Visibility = Visibility.Visible;
                    cmbAllGrades.ItemsSource = null;
                    cmbAllGrades.ItemsSource = this.FlowController.ProjectController.CurrentFlowProject.UniqueGrades;
                }
                else
                    pnlApplyToGrade.Visibility = Visibility.Collapsed;

                if (this.FlowController.ProjectController.CurrentFlowProject.UniqueClasses != null)
                {
                    pnlApplyToClass.Visibility = Visibility.Visible;
                    cmbAllClasses.ItemsSource = null;
                    cmbAllClasses.ItemsSource = this.FlowController.ProjectController.CurrentFlowProject.UniqueClasses;
                }
                else
                    pnlApplyToClass.Visibility = Visibility.Collapsed;

                lblAssignedTo.Content = GroupImage.AssignedTo;

            }
            else
            {

                imgCurrentImage.Source = selectedImage.GetProcessingImage();
                CheckOverlay();
            }

        }

        internal void DisplaySelectedSubjectImage(SubjectImage selectedImage)
        {
            pnlGroupImageSettings.Visibility = Visibility.Collapsed;

            if (ConfigUtil.GetBoolSetting("CAPTURE_CURRENT_IMG_DISABLED"))
                return;

            if (selectedImage == null || !selectedImage.ImageFileExists)
            {
                imgCurrentImage.Source = null;
                imgOverlay.Source = null;
            }
            else
            {
                //selectedImage.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(selectedImage_PropertyChanged);
                imgCurrentImage.Source = selectedImage.GetProcessingImage();
                CheckOverlay();
            }
        }

        void selectedImage_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (ConfigUtil.GetBoolSetting("CAPTURE_CURRENT_IMG_DISABLED"))
                return;

            try
            {
                if ((sender as SubjectImage).Subject != null)
                    imgCurrentImage.Source = (sender as SubjectImage).GetProcessingImage();
            }
            catch
            {
                //ignore for now
            }
        }

        public void CheckOverlay()
        {
            if (UseOverlayPng)
            {
                try
                {
                    //String orientationQuery = "System.Photo.Orientation";
                    //BitmapSource img = BitmapFrame.Create(new Uri(selectedFlowImage.ImageFilename));
                    //BitmapMetadata meta = (BitmapMetadata)img.Metadata;
                    //if ((meta != null) && (meta.ContainsQuery(orientationQuery)))
                    //{
                    //    object o = meta.GetQuery(orientationQuery);
                    //    if (o != null)
                    //    {
                    if (UseOverlayPng && imgCurrentImage.Source != null && imgCurrentImage.Source.Height > imgCurrentImage.Source.Width)
                            {
								string overlayPngPath = this.PreferenceManager.Capture.OverlayPngPath;
								if (overlayPngPath != currentOverlayPng || imgOverlay.Source == null)
                                {
                                    MatchImageSizes();
									currentOverlayPng = overlayPngPath;
                                    imgOverlay.Source = ImageLoader.LoadImage(currentOverlayPng);
                                }
                            }
                            else
                            {
                                imgOverlay.Source = null;
                            }
                    //    }
                    //}
                    //else
                    //{
                    //    imgOverlay.Source = null;
                    //}
                }
                catch
                {
                    imgOverlay.Source = null;
                }
            }
            else
            {
                imgOverlay.Source = null;
            }
        }

        void imgCurrentImage_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            MatchImageSizes();
        }

        void MatchImageSizes()
        {
            //imgOverlay.RenderSize = new Size(imgCurrentImage.RenderSize.Width, imgCurrentImage.RenderSize.Height);
            imgOverlay.Width = imgCurrentImage.RenderSize.Width;
            imgOverlay.Height = imgCurrentImage.RenderSize.Height;
        }

        private void checkBoxCropOverlay_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                this.UseOverlayPng = checkBoxCropOverlay.IsChecked.Value;
                this.FlowController.ProjectController.FlowMasterDataContext.SubmitChanges();

                CheckOverlay();
            }
            catch (Exception ex)
            {
                //do nothing
            }
        }


        void imgOverlay_LayoutUpdated(object sender, EventArgs e)
        {
            //CheckOverlay();
        }

        private void cmbGroupImageFlag_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (GroupImage != null)
            {
                string sv = cmbGroupImageFlag.SelectedValue as String;
                if (!String.IsNullOrEmpty(sv) && sv != GroupImage.SelectedFlag)
                {
                    GroupImage.SelectedFlag = sv;
                    FlowController.ProjectController.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();
                }

            }
        }

        private void btnApplyToTeacher_Click(object sender, RoutedEventArgs e)
        {
            string targetTeacher = cmbAllTeachers.SelectedValue as String;
            if (!String.IsNullOrEmpty(targetTeacher))
            {
                List<Subject> subjects = FlowController.ProjectController.CurrentFlowProject.SubjectList.Where(s => s.SubjectData["Teacher"].Value as string == targetTeacher).ToList();
                FlowMessageBox msga = new FlowMessageBox("Group Image Assigned", "You are about to assign this image to " + subjects.Count + " Subjects.\n\nAre you sure you want to do this?");
                if (msga.ShowDialog() == false)
                    return;

                int count = 0;
                foreach (Subject sub in subjects)
                {
                    count++;
                    sub.AssignGroupImage(GroupImage, FlowController.ProjectController.FlowMasterDataContext.LoginID);
                }
                GroupImage.AssignedTo = targetTeacher;
                lblAssignedTo.Content = targetTeacher;
                FlowController.ProjectController.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();
                if (count > 0)
                {
                    
                    FlowMessageBox msg = new FlowMessageBox("Group Image Assigned", "Group image has been assigned to " + count + " Subjects.");
                    msg.CancelButtonVisible = false;
                    msg.ShowDialog();
                }
            }
        }

        private void btnApplyToGrade_Click(object sender, RoutedEventArgs e)
        {
            string targetGrade = cmbAllGrades.SelectedValue as String;
            if (!String.IsNullOrEmpty(targetGrade))
            {
                List<Subject> subjects = FlowController.ProjectController.CurrentFlowProject.SubjectList.Where(s => s.SubjectData["Grade"].Value as string == targetGrade).ToList();
                FlowMessageBox msga = new FlowMessageBox("Group Image Assigned", "You are about to assign this image to " + subjects.Count + " Subjects.\n\nAre you sure you want to do this?");
                if (msga.ShowDialog() == false)
                    return;
                int count = 0;
                foreach (Subject sub in subjects)
                {
                    count++;
                    sub.AssignGroupImage(GroupImage, FlowController.ProjectController.FlowMasterDataContext.LoginID);
                }
                GroupImage.AssignedTo = targetGrade;
                lblAssignedTo.Content = targetGrade;
                FlowController.ProjectController.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();
                if (count > 0)
                {
                    
                    FlowMessageBox msg = new FlowMessageBox("Group Image Assigned", "Group image has been assigned to " + count + " Subjects.");
                    msg.CancelButtonVisible = false;
                    msg.ShowDialog();
                }
            }
        }

        private void btnApplyToClass_Click(object sender, RoutedEventArgs e)
        {
            string targetClass = cmbAllClasses.SelectedValue as String;
            if (!String.IsNullOrEmpty(targetClass))
            {
                List<Subject> subjects = FlowController.ProjectController.CurrentFlowProject.SubjectList.Where(s => s.SubjectData["Class"].Value as string == targetClass).ToList();
                FlowMessageBox msga = new FlowMessageBox("Group Image Assigned", "You are about to assign this image to " + subjects.Count + " Subjects.\n\nAre you sure you want to do this?");
                if (msga.ShowDialog() == false)
                    return;
                int count = 0;
                foreach (Subject sub in subjects)
                {
                    count++;
                    sub.AssignGroupImage(GroupImage, FlowController.ProjectController.FlowMasterDataContext.LoginID);
                }
                GroupImage.AssignedTo = targetClass;
                lblAssignedTo.Content = targetClass;
                FlowController.ProjectController.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();
                if (count > 0)
                {
                    
                    FlowMessageBox msg = new FlowMessageBox("Group Image Assigned", "Group image has been assigned to " + count + " Subjects.");
                    msg.CancelButtonVisible = false;
                    msg.ShowDialog();
                }
            }
        }


        internal void ClearCurrentImage()
        {
            imgCurrentImage.Source = null;
            pnlGroupImageSettings.Visibility = Visibility.Collapsed;
        }

        private void btnRemovefromGrade_Click(object sender, RoutedEventArgs e)
        {
            string targetGrade = cmbAllGrades.SelectedValue as String;
            if (!String.IsNullOrEmpty(targetGrade))
            {
                int count = 0;
                List<SubjectImage> subjectImages = GroupImage.GetSubjectImageList.Where(s => s.Subject.SubjectData["Grade"].Value as string == targetGrade).ToList();
                FlowMessageBox msg = new FlowMessageBox("Group Image UnAssigned", "You are about to REMOVE this image from " + subjectImages.Count + " Subjects.\n\nGrade: " + targetGrade + "\n\nAre you sure you want to do this?");
                if (msg.ShowDialog() == false)
                    return;

                foreach (SubjectImage subImage in subjectImages)
                {
                    count++;
                    List<SubjectImage> tempList = new List<SubjectImage>();
                    tempList.Add(subImage);
                    this.FlowController.CaptureController.SubjectRecordController_ImageUnassigned(this, new EventArgs<IEnumerable<SubjectImage>>(tempList));
                
                }
                if (GroupImage.AssignedTo == targetGrade)
                {
                    GroupImage.AssignedTo = "";
                    lblAssignedTo.Content = "";
                }
                FlowController.ProjectController.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();
                //if (count > 0)
                {

                    FlowMessageBox msg2 = new FlowMessageBox("Group Image Remove", "Done Removing Group Image from Grade: " + targetGrade );
                    msg2.CancelButtonVisible = false;
                    msg2.ShowDialog();
                }
            }
        }

        private void btnRemovefromTeacher_Click(object sender, RoutedEventArgs e)
        {
            string targetTeacher = cmbAllTeachers.SelectedValue as String;
            if (!String.IsNullOrEmpty(targetTeacher))
            {
                int count = 0;
                List<SubjectImage> subjectImages = GroupImage.GetSubjectImageList.Where(s => s.Subject.SubjectData["Teacher"].Value as string == targetTeacher).ToList();
                FlowMessageBox msg = new FlowMessageBox("Group Image UnAssigned", "You are about to REMOVE this image from " + subjectImages.Count + " Subjects.\n\nTeacher: " + targetTeacher + "\n\nAre you sure you want to do this?");
                if (msg.ShowDialog() == false)
                    return;

                foreach (SubjectImage subImage in subjectImages)
                {
                    count++;
                    List<SubjectImage> tempList = new List<SubjectImage>();
                    tempList.Add(subImage);
                    this.FlowController.CaptureController.SubjectRecordController_ImageUnassigned(this, new EventArgs<IEnumerable<SubjectImage>>(tempList));

                }
                if (GroupImage.AssignedTo == targetTeacher)
                {
                    GroupImage.AssignedTo = "";
                    lblAssignedTo.Content = "";
                }
                FlowController.ProjectController.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();
                //if (count > 0)
                {

                    FlowMessageBox msg2 = new FlowMessageBox("Group Image Remove", "Done Removing Group Image from Teacher: " + targetTeacher);
                    msg2.CancelButtonVisible = false;
                    msg2.ShowDialog();
                }
            }
        }

        private void btnRemovefromClass_Click(object sender, RoutedEventArgs e)
        {
            string targetClass = cmbAllClasses.SelectedValue as String;
            if (!String.IsNullOrEmpty(targetClass))
            {
                int count = 0;
                List<SubjectImage> subjectImages = GroupImage.GetSubjectImageList.Where(s => s.Subject.SubjectData["Class"].Value as string == targetClass).ToList();
                FlowMessageBox msg = new FlowMessageBox("Group Image UnAssigned", "You are about to REMOVE this image from " + subjectImages.Count + " Subjects.\n\nClass: " + targetClass + "\n\nAre you sure you want to do this?");
                if (msg.ShowDialog() == false)
                    return;

                foreach (SubjectImage subImage in subjectImages)
                {
                    count++;
                    List<SubjectImage> tempList = new List<SubjectImage>();
                    tempList.Add(subImage);
                    this.FlowController.CaptureController.SubjectRecordController_ImageUnassigned(this, new EventArgs<IEnumerable<SubjectImage>>(tempList));

                }
                if (GroupImage.AssignedTo == targetClass)
                {
                    GroupImage.AssignedTo = "";
                    lblAssignedTo.Content = "";
                }
                FlowController.ProjectController.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();
                //if (count > 0)
                {

                    FlowMessageBox msg2 = new FlowMessageBox("Group Image Remove", "Done Removing Group Image from Class: " + targetClass);
                    msg2.CancelButtonVisible = false;
                    msg2.ShowDialog();
                }
            }
        }
    }

}
