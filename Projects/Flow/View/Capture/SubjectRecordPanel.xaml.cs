﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Lib.Helpers;
using Flow.Schema.LinqModel.DataContext;
using Flow.Controls.View.RecordDisplay;
using NLog;

namespace Flow.View.Capture
{

	/// <summary>
    /// Interaction logic for CurrentRecordPanel.xaml
    /// </summary>
	public partial class SubjectRecordPanel : UserControl
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

		public event EventHandler<EventArgs<IEnumerable<SubjectImage>>> ImageUnassigned
		{
			add { this.SubjectRecordDisplay.ImageUnassigned += value; }
			remove { this.SubjectRecordDisplay.ImageUnassigned -= value; }
		}

        public event EventHandler<EventArgs<SubjectImage>> ToggleGreenscreenInvoked
        {
            add { this.SubjectRecordDisplay.ToggleGreenscreenInvoked += value; }
            remove { this.SubjectRecordDisplay.ToggleGreenscreenInvoked -= value; }
        }

        public event EventHandler<EventArgs<SubjectImage>> ToggleFlagImageInvoked
        {
            add { this.SubjectRecordDisplay.ToggleFlagImageInvoked += value; }
            remove { this.SubjectRecordDisplay.ToggleFlagImageInvoked -= value; }
        }

        public event EventHandler<EventArgs<SubjectImage>> ToggleRemoteImageServiceInvoked
        {
            add { this.SubjectRecordDisplay.ToggleRemoteImageServiceInvoked += value; }
            remove { this.SubjectRecordDisplay.ToggleRemoteImageServiceInvoked -= value; }
        }

        public event EventHandler ShowOrderEntry 
        {
        
             add { this.SubjectRecordDisplay.ShowOrderEntry += value; }
            remove { this.SubjectRecordDisplay.ShowOrderEntry -= value; }
        
        }

		protected static DependencyProperty CurrentFlowProjectProperty =
			DependencyProperty.Register("CurrentFlowProject", typeof(FlowProject), typeof(SubjectRecordPanel));

		public FlowProject CurrentFlowProject
		{
			get	{ return (FlowProject)this.GetValue(CurrentFlowProjectProperty); }
			set { this.SetValue(CurrentFlowProjectProperty, value); }
		}


		public SubjectRecordPanel()
        {
            InitializeComponent();
        }

		private void SearchResultsList_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
			this.CurrentFlowProject = (FlowProject)this.DataContext;
		}

		private void CloseSearchResultsButton_Click(object sender, RoutedEventArgs e)
		{
            logger.Info("Clicked Close Search Results button");

			this.SearchResultsPanel.Visibility = Visibility.Collapsed;
            this.CurrentFlowProject.SubjectList.ClearFilter();
		}

		internal void SetSearchFocus()
		{
			this.Focus();
			this.SubjectRecordDisplay.CollectionNavigator.SetSearchFocus();
		}
        internal void SetSearchString(string text)
        {
            this.SubjectRecordDisplay.CollectionNavigator.SetSearchString(text);
        }
        private void SubjectRecordDisplay_Loaded(object sender, RoutedEventArgs e)
        {
            if (this.CurrentFlowProject != null)
            {
                if (this.CurrentFlowProject.FlowMasterDataContext.PreferenceManager.Permissions.CanModifySubjectData)
                    ((SubjectRecordDisplay)sender).CollectionNavigator.EditItemButtonVisibility = Visibility.Visible;
                else
                    ((SubjectRecordDisplay)sender).CollectionNavigator.EditItemButtonVisibility = Visibility.Hidden;

                if (this.CurrentFlowProject.FlowMasterDataContext.PreferenceManager.Permissions.CanAddSubjects)
                    ((SubjectRecordDisplay)sender).CollectionNavigator.AddItemButtonVisibility = Visibility.Visible;
                else
                    ((SubjectRecordDisplay)sender).CollectionNavigator.AddItemButtonVisibility = Visibility.Hidden;

                if (this.CurrentFlowProject.FlowMasterDataContext.PreferenceManager.Permissions.CanDeleteSubjects)
                    ((SubjectRecordDisplay)sender).CollectionNavigator.DeleteItemButtonVisibility = Visibility.Visible;
                else
                    ((SubjectRecordDisplay)sender).CollectionNavigator.DeleteItemButtonVisibility = Visibility.Hidden;
            }
        }
	
	}
}
