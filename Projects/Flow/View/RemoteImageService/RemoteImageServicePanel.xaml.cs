﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Flow.Schema.LinqModel.DataContext;
using Flow.View.Project.Base;
using Flow.Lib.Helpers;
using Flow.Schema.LinqModel.DataContext.FlowMaster;
using Flow.Controller;
using Flow.Controller.Project;
using System.Diagnostics;
using Flow.View.Dialogs;
using Flow.Lib;
using System.ComponentModel;
using StructureMap;
using Flow.Controls;
using System.Windows.Threading;
using System.Runtime.InteropServices;
using NetServ.Net.Json;
using System.Threading;
using System.IO;
using System.Net;
using NLog;
using System.Xml;
using Flow.Lib.Preferences;
using Flow.Lib.RIS;
using Flow.Lib.Activation;

namespace Flow.View
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class RemoteImageServicePanel : ProjectPanelBase
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();


        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        static extern EXECUTION_STATE SetThreadExecutionState(EXECUTION_STATE esFlags);

        [FlagsAttribute]
        public enum EXECUTION_STATE : uint
        {
            ES_AWAYMODE_REQUIRED = 0x00000040,
            ES_CONTINUOUS = 0x80000000,
            ES_DISPLAY_REQUIRED = 0x00000002,
            ES_SYSTEM_REQUIRED = 0x00000001
        }

        //no longer prevent the system from sleeping
        void AllowSleep()
        {
            SetThreadExecutionState(EXECUTION_STATE.ES_CONTINUOUS);
        }

        //prevents the system from sleeping untill the app closes or AllowSleep is called
        void PreventSleep()
        {
            // Prevent Idle-to-Sleep (monitor not affected)
            SetThreadExecutionState(EXECUTION_STATE.ES_CONTINUOUS | EXECUTION_STATE.ES_AWAYMODE_REQUIRED);
        }

        //just resets the idle clock, must keep calling this periodically to keep awake
        void KeepSystemAwake()
        {
            SetThreadExecutionState(EXECUTION_STATE.ES_SYSTEM_REQUIRED);
        }

        private NotificationProgressInfo _notificationInfo;



        public RemoteImageServicePanel()
        {
           
        }

        protected override void OnSetFlowController()
        {
            base.OnSetFlowController();

            //this.DataContext = this.ProjectController;

            //the pacement of this needs to be after DataContext is defined
            InitializeComponent();


        }

        public void setTabFocus(int tabIndex)
        {
            //tabCtrl.SelectedIndex = tabIndex;
        }

        private void btnUploadImages_Click(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrEmpty(this.FlowMasterDataContext.PreferenceManager.Application.RemoteImageServiceFtp))
            {
                this.tabCtrl.SelectedIndex = 3;
                FlowMessageBox msg = new FlowMessageBox("Remote Image Service", "Please enter in you service ftp information");
                msg.CancelButtonVisible = false;
                msg.ShowDialog();
                return;
            }
            if (this.ProjectController.CurrentFlowProject.RemoteImageServiceImagesNew.Count < 1)
            {
                FlowMessageBox msg = new FlowMessageBox("Remote Image Service", "You have not selected any images to upload");
                msg.CancelButtonVisible = false;
                msg.ShowDialog();
                return;
            }
            if (chkBasicRetouch.IsChecked == false && chkGSknock.IsChecked == false)
            {
                FlowMessageBox msg = new FlowMessageBox("Please select a service option", "You must check one of the service options before uploading images.");
                msg.CancelButtonVisible = false;
                msg.ShowDialog();
                return;
            }
            //return;
            OrderUploadHistory ouh = new OrderUploadHistory();
            ouh.ProjectName = this.CurrentFlowProject.FlowProjectName;
            ouh.IsRemoteImageServiceUpload = true;
            ouh.ImageCount = CurrentFlowProject.RemoteImageServiceImagesNew.Count();
            this.FlowMasterDataContext.OrderUploadHistories.InsertOnSubmit(ouh);
            this.FlowMasterDataContext.SubmitChanges();
            this.FlowMasterDataContext.RefreshOrderUploadHistoryList();


            XmlDocument doc = new XmlDocument();
            XmlNode docNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            doc.AppendChild(docNode);

            XmlNode RootNode = doc.CreateElement("Root");
            doc.AppendChild(RootNode);

            XmlNode jobNode = doc.CreateElement("Job");
            RootNode.AppendChild(jobNode);

            XmlNode idNode = doc.CreateElement("ID");
            jobNode.AppendChild(idNode);

            XmlNode jobID = doc.CreateElement("JobID");
            jobID.InnerText = this.CurrentFlowProject.FlowProjectGuid.ToString().Substring(0, 5);
            idNode.AppendChild(jobID);

            XmlNode project = doc.CreateElement("JobDisplayName");
            project.InnerText = this.CurrentFlowProject.FlowProjectName;
            idNode.AppendChild(project);

            XmlNode clientNode = doc.CreateElement("Client");
            jobNode.AppendChild(clientNode);

            XmlNode studio = doc.CreateElement("ID");
            studio.InnerText = this.CurrentFlowProject.Studio.OrganizationName;
            clientNode.AppendChild(studio);

            XmlNode studio2 = doc.CreateElement("DisplayName");
            studio2.InnerText = this.CurrentFlowProject.Studio.OrganizationName;
            clientNode.AppendChild(studio2);

            XmlNode studio3 = doc.CreateElement("NotificationEmail");
            studio3.InnerText = this.CurrentFlowProject.Studio.OrganizationContactEmail;
            clientNode.AppendChild(studio3);

            XmlNode prodNode = doc.CreateElement("Production");
            jobNode.AppendChild(prodNode);

            XmlNode date = doc.CreateElement("Date");
            date.InnerText = DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();
            prodNode.AppendChild(date);

            XmlNode imageCount = doc.CreateElement("SourceImageCount");
            imageCount.InnerText = CurrentFlowProject.RemoteImageServiceImagesNew.Count().ToString();
            prodNode.AppendChild(imageCount);

            XmlNode actionNode = doc.CreateElement("RequestedActions");
            prodNode.AppendChild(actionNode);


            XmlNode gs = doc.CreateElement("Chromakey");
            if (chkGSknock.IsChecked == true) gs.InnerText = "true"; else gs.InnerText = "false";
            actionNode.AppendChild(gs);

            XmlNode colorGrade = doc.CreateElement("ColorGrade");
            if (chkColorGrade.IsChecked == true) colorGrade.InnerText = "true"; else colorGrade.InnerText = "false";
            actionNode.AppendChild(colorGrade);

            XmlNode retouch = doc.CreateElement("AutoRetouch");
            if (chkBasicRetouch.IsChecked == true) retouch.InnerText = "true"; else retouch.InnerText = "false";
            actionNode.AppendChild(retouch);

            XmlNode notes = doc.CreateElement("notes");
            notes.InnerText = txtNotes.Text;
            actionNode.AppendChild(notes);


           

            XmlNode images = doc.CreateElement("Images");
            RootNode.AppendChild(images);



            string ImageServiceDataFile = "Studio:" + this.CurrentFlowProject.Studio.OrganizationName + "\n";
            ImageServiceDataFile += "Project:" + this.CurrentFlowProject.FlowProjectName + "\n";
            ImageServiceDataFile += "Guid:" + this.CurrentFlowProject.FlowProjectGuid + "\n";
            ImageServiceDataFile += "Date:" + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + "\n";
            ImageServiceDataFile += "Image Count:" + CurrentFlowProject.RemoteImageServiceImagesNew.Count() + "\n";
            
            string LabOrderQueueFolder = this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.LabOrderQueueDirectory;

            string uploadJob = System.IO.Path.Combine(LabOrderQueueFolder, "RemoteImageService_exp_" + ouh.OrderUploadHistoryID);

            if (!Directory.Exists(uploadJob))
                Directory.CreateDirectory(uploadJob);

            ImageServiceDataFile += "Images:\n";

            foreach (SubjectImage si in CurrentFlowProject.RemoteImageServiceImagesNew)
            {
                si.RemoteImageServiceStatus = "inprogress";
                File.Copy(si.ImagePathFullRes, System.IO.Path.Combine(uploadJob, si.ImageFileName));

                ImageServiceDataFile += si.ImageFileName + "\n";

                XmlNode image = doc.CreateElement("Image");
                images.AppendChild(image);

                XmlNode fileName = doc.CreateElement("FullFileName");
                fileName.InnerText = si.ImageFileName;
                image.AppendChild(fileName);

                int index = si.ImageFileName.LastIndexOf('.');
                string fname2 = si.ImageFileName.Substring(0, index);

                XmlNode fileName2 = doc.CreateElement("FileNameWithoutExtension");
                fileName2.InnerText = fname2;
                image.AppendChild(fileName2);

                XmlNode fName = doc.CreateElement("FirstName");
                fName.InnerText = si.Subject.SubjectDataRow["First Name"].ToString();
                image.AppendChild(fName);

                XmlNode lName = doc.CreateElement("LastName");
                lName.InnerText = si.Subject.SubjectDataRow["Last Name"].ToString();
                image.AppendChild(lName);

            }

            string DataFile = System.IO.Path.Combine(uploadJob, "JobInfo.txt");
            File.WriteAllText(DataFile, ImageServiceDataFile);


            string xmlDataFile = System.IO.Path.Combine(uploadJob, "JobInfo.xml");
            doc.Save(xmlDataFile);
            //File.WriteAllText(DataFile, ImageServiceDataFile);
            
            this.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();
            CurrentFlowProject.UpdateRemoteImageServiceLists();

            FlowMessageBox msg2 = new FlowMessageBox("Image Upload", "The images have been sent to the uploader");
            msg2.CancelButtonVisible = false;
            msg2.ShowDialog();
            this.CurrentFlowProject.UpdateRemoteImageServiceLists();
        }

        private void btnUpdateImages_Click(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrEmpty(this.FlowMasterDataContext.PreferenceManager.Application.RemoteImageServiceFtp))
            {
                this.tabCtrl.SelectedIndex = 3;
                FlowMessageBox msg = new FlowMessageBox("Remote Image Service", "Please enter in you service ftp information");
                msg.CancelButtonVisible = false;
                msg.ShowDialog();
                return;
            }
            this.FlowController.ApplicationPanel.IsEnabled = false;
            FlowBackgroundWorkerManager.RunWorker(ImageDownloadWorker, ImageDownload_Completed);
        }

        void ImageDownload_Completed(object sender, RunWorkerCompletedEventArgs e)
        {
            this.FlowController.ApplicationPanel.IsEnabled = true;

        }

        public void ImageDownloadWorker(object sender, DoWorkEventArgs e)
         {
            bool didNothing = true;
            //string ftpAddress = this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.ProjectTransfer.HostAddress;
            //int ftpPort = this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.ProjectTransfer.HostPort;
            //string ftpUserName = this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.ProjectTransfer.UserName;
            //string ftpPassword = this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.ProjectTransfer.Password;
            //string ftpDirectory = this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.ProjectTransfer.RemoteImageMatchDirectory;
            //bool useSftp = this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.ProjectTransfer.UseSftp;


            //string ftpAddress = "113.108.248.106";
           
            //string ftpAddress = "ftp.photoretouchonline.com";
            //int ftpPort = 21;
            //string ftpUserName = "FlowBridge@photoretouchonline.com";
            //string ftpPassword = "4c1665a5";
            //bool useSftp = false;
            //string ftpRoot = "";

            //string ftpAddress = "sb.jaleatech.com";
            //int ftpPort = 21;
            //string ftpUserName = "Photolynx";
            //string ftpPassword = "589305";
            //bool useSftp = false;
            //string ftpRoot = "";

            string serviceName = this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.RemoteImageServiceFtp;
            RemoteImageService ris = this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.RemoteImageServices.FirstOrDefault(r => r.Name == serviceName);
            
            string ftpAddress = ris.Ftp;
            int ftpPort = Int32.Parse(ris.Port);

            string ftpUserName = this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.RemoteImageServiceUser;
            string ftpPassword = this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.RemoteImageServicePW;
            bool useSftp = false;
            string ftpRoot = "";
            if (ftpAddress.Contains("sftp") || ftpPort == 22)
            {
                ftpPort = 22;
                useSftp = true;
            }

            //string ftpAddress = "69.162.106.166";
            //int ftpPort = 21;
            //string ftpUserName = "photolynx";
            //string ftpPassword = "p@wprint$";
            //bool useSftp = false;
            //string ftpRoot =  "/RemoteImageService";

            NotificationProgressInfo ProgressInfo = new NotificationProgressInfo("Downloadin Images", "Checking for images");
            IShowsNotificationProgress progressNotification = ObjectFactory.GetInstance<IShowsNotificationProgress>();
            progressNotification.ShowNotification(ProgressInfo);
            ProgressInfo.NotificationProgress = progressNotification;

            ftpRoot = ftpRoot + "/" + "DONE" + "/";

            string workingFolder = System.IO.Path.Combine(FlowContext.FlowAppDataDirPath, "NewImages");
            if (!Directory.Exists(workingFolder))
                Directory.CreateDirectory(workingFolder);

            //connect to remote server
            Flow.Lib.Net.FileTransfer ft = new Flow.Lib.Net.FileTransfer(ftpAddress, ftpPort, ftpUserName, ftpPassword, useSftp, null);
            ft.Connect();

            //get a list of folders in the remote Completed folder
            List<string> subfolders = new List<string>();
            if (useSftp)
            {
                subfolders = ft.sftpClient.GetNameList(ftpRoot).ToList();
            }
            else
            {
                subfolders = ft.ftpClient.GetNameList(ftpRoot).ToList();
            }
            foreach (string folder in subfolders)
            {
                string folderName = folder.Split("/").Last();

                if (folderName.StartsWith(this.CurrentFlowProject.FlowProjectGuid.ToString()))
                {
                    //this is the right folder
                    bool isReady = false;
                    string doneFile = ftpRoot + folderName + "/done.txt";
                    if (useSftp && ft.sftpClient.FileExists(doneFile))
                    {
                        isReady = true;
                    }
                    else if (ft.ftpClient.FileExists(doneFile))
                    {
                        isReady = true;
                    }

                    int imgCount = 0;

                    if (isReady)
                    {

                       
                        string DestFolder = System.IO.Path.Combine(workingFolder, folderName);
                        if (!Directory.Exists(DestFolder))
                            Directory.CreateDirectory(DestFolder);

                        
                        if (useSftp)
                        {
                            List<string> files = ft.sftpClient.GetNameList(ftpRoot + folderName + "/").ToList();
                            ProgressInfo.Update("Downloading Images {0} of {1}", 0, files.Count, 1, true);
                             foreach (string file in files)
                            {
                                ProgressInfo.Step();
                                    string fileName = file.Split("/").Last();
                                    string destFile = System.IO.Path.Combine(DestFolder, fileName);
                                    if (File.Exists(destFile))
                                        File.Move(destFile, destFile + ".orig");
                                    long fsize = ft.sftpClient.GetFile(ftpRoot + folderName + "/" + fileName, destFile);
                                    if (File.Exists(destFile) && fsize == (new FileInfo(destFile)).Length)
                                        ft.sftpClient.DeleteFile(ftpRoot + folderName + "/" + fileName);
                                    string newDestFile = NewFile(new FileInfo(destFile));
                                    if (File.Exists(destFile))
                                        File.Delete(destFile);
                                    if (newDestFile.ToLower().EndsWith(".png"))
                                        this.FlowController.ProjectController.JpgPngSwap(newDestFile, "done");

                                    if (file.ToLower().EndsWith(".jpg") || file.ToLower().EndsWith(".png"))
                                    {
                                    imgCount++;
                                    SubjectImage si = this.CurrentFlowProject.FlowProjectDataContext.SubjectImages.FirstOrDefault(i => i.ImageFileName.ToLower() == fileName.ToLower());
                                    if (si != null)
                                    {
                                        si.RefreshImage();
                                        si.Subject.ClearGSCache(System.IO.Path.Combine(this.CurrentFlowProject.ImageDirPath, "PLCache", "GreenScreen"));
                                        si.RemoteImageServiceStatus = "done";
                                        this.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();
                                        this.CurrentFlowProject.FlowMasterDataContext.SubmitChanges();
                                        si.ClearImage();
                                        si.UpdateGS();
                                    }
                                }
                            }
                            if (ft.sftpClient.GetNameList(ftpRoot + folderName + "/").Count() == 0)
                                ft.sftpClient.Delete(ftpRoot + folderName + "/", Rebex.IO.TraversalMode.NonRecursive);
                        }
                        else
                        {
                            List<string> files = ft.ftpClient.GetNameList(ftpRoot + folderName + "/").ToList();
                            ProgressInfo.Update("Downloading Images {0} of {1}", 0, files.Where(f => f.ToLower().EndsWith(".png") || f.ToLower().EndsWith(".jpg")).Count(), 1, true);

                            foreach (string file in files)
                            {

                                if (file.ToLower().EndsWith(".jpg") || file.ToLower().EndsWith(".png"))
                                {
                                    imgCount++;
                                    ProgressInfo.Step();
                                }
                                    string fileName = file.Split("/").Last();
                                    string destFile = System.IO.Path.Combine(DestFolder, fileName);
                                    if (File.Exists(destFile))
                                        File.Move(destFile, destFile + ".orig");
                                    long fsize = ft.ftpClient.GetFile(ftpRoot + folderName + "/" + fileName, destFile);
                                    if (File.Exists(destFile) && fsize == (new FileInfo(destFile)).Length)
                                        ft.ftpClient.DeleteFile(ftpRoot + folderName + "/" + fileName);
                                    string newDestFile = NewFile(new FileInfo(destFile));
                                    if (File.Exists(destFile))
                                        File.Delete(destFile);
                                    if (newDestFile != null && newDestFile.ToLower().EndsWith(".png"))
                                        this.FlowController.ProjectController.JpgPngSwap(newDestFile, "done");

                                    if (file.ToLower().EndsWith(".jpg") || file.ToLower().EndsWith(".png"))
                                    {
                                    SubjectImage si = this.CurrentFlowProject.FlowProjectDataContext.SubjectImages.FirstOrDefault(i => i.ImageFileName.ToLower() == fileName.ToLower());
                                    if (si != null)
                                    {
                                        //dispatch this
                                        this.FlowController.ProjectController.CurrentFlowProject.MainDispatcher.Invoke((Action)delegate
                                        {
                                            si.FlowImage.Refresh();
                                            si.RefreshImage();
                                            si.Subject.ClearGSCache(System.IO.Path.Combine(this.CurrentFlowProject.ImageDirPath, "PLCache", "GreenScreen"));
                                            si.RemoteImageServiceStatus = "done";
                                            this.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();
                                            this.CurrentFlowProject.FlowMasterDataContext.SubmitChanges();
                                            si.UpdateGS();
                                        });
                                    }
                                }
                            }
                            if (ft.ftpClient.GetNameList(ftpRoot + folderName + "/").Count() == 0)
                                ft.ftpClient.Delete(ftpRoot + folderName + "/", Rebex.IO.TraversalMode.NonRecursive);
                        }
                        didNothing = false;
                        Directory.Delete(DestFolder, true);
                        ProgressInfo.ShowDoneButton();
                        ProgressInfo.Complete("Done importing " + imgCount + " image(s)");

                        //FlowMessageBox msg = new FlowMessageBox("Images Processed", "Done importing " + imgCount + " image(s)");
                        //msg.CancelButtonVisible = false;
                        //msg.ShowDialog();
                        //return;
                    }




                    //
                    //UPLOAD STATS
                    //
                    RISStore store = new RISStore();

                    ActivationValidator activation = new ActivationValidator(this.FlowController.ActivationKeyFile);
                    ActivationStore activationStore = activation.Repository.Load();

                    store.Application = "Flow";
                    store.CustomerName = "";
                    store.ServiceFTP = ftpAddress;
                    store.ServiceUserName = ftpUserName;
                    store.RemoteFolderName = folderName;
                    store.DownImageCount = imgCount;
                    store.ActivationKey = activationStore.ActivationKey;
                    store.ProjectName = "";
                    store.ApplicationVersion = FlowContext.GetFlowVersion();
                    store.ServicesOrdered = "";
                    RISPost RISPost = new RISPost();
                    string returnMsg = RISPost.Post(store);
                    //
                    //END UPLOAD STATS
                    //


                }
            }
            this.CurrentFlowProject.UpdateRemoteImageServiceLists();
            if(didNothing)
            {
                this.FlowController.ProjectController.CurrentFlowProject.MainDispatcher.Invoke((Action)delegate
                    {
                        FlowMessageBox msg = new FlowMessageBox("Image not ready", "Your Images are not ready yet");
                        msg.CancelButtonVisible = false;
                        msg.ShowDialog();
                     });


                ProgressInfo.ShowDoneButton();
                ProgressInfo.Complete("Your Images are not ready yet");
                    
            }



        }

        private string NewFile(FileInfo fi)
        {
            if(fi.Name.ToLower().EndsWith(".jpg") || fi.Name.ToLower().EndsWith(".png"))
            {
                string backupFolder = System.IO.Path.Combine(this.CurrentFlowProject.ImageDirPath, "ImageBackups");
                if (!Directory.Exists(backupFolder))
                    Directory.CreateDirectory(backupFolder);
                string destFile = System.IO.Path.Combine(this.CurrentFlowProject.ImageDirPath, fi.Name);
                string backupFile = System.IO.Path.Combine(backupFolder, fi.Name);
                if (File.Exists(backupFile))
                    File.Delete(backupFile);
                if (File.Exists(destFile))
                    File.Move(destFile, backupFile);

                File.Move(fi.FullName, destFile);
                return destFile;
            }
            return null;
        }

        private void Grid_GotFocus(object sender, RoutedEventArgs e)
        {
            this.CurrentFlowProject.UpdateRemoteImageServiceLists();
        }

        private void btnAddAllImages_Click(object sender, RoutedEventArgs e)
        {
            foreach (Subject s in this.CurrentFlowProject.SubjectList.View)
            {
                foreach (SubjectImage si in s.ImageList)
                {
                    if (si.IsSuppressed != true)
                    {
                        si.RemoteImageService = true;
                        si.RemoteImageServiceStatus = "new";
                    }
                }
            }
            this.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();
            this.CurrentFlowProject.UpdateRemoteImageServiceLists();
        }

        private void btnRemoveAllImages_Click(object sender, RoutedEventArgs e)
        {
            
            foreach (SubjectImage si in this.CurrentFlowProject.RemoteImageServiceImagesNew)
            {
                if (si.RemoteImageServiceStatus == null || si.RemoteImageServiceStatus == "new")
                {
                    si.RemoteImageService = false;
                    si.RemoteImageServiceStatus = null;
                }
              
            }

            this.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();
            this.CurrentFlowProject.UpdateRemoteImageServiceLists();
        }

        private void btnSaveSettings_Click(object sender, RoutedEventArgs e)
        {
            this.ProjectController.FlowMasterDataContext.SavePreferences();
            NotificationProgressInfo.QuickStatus("Remote Image Service", "FTP settings have been saved");
        }
    }
}
