﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Data;
using System.Linq;
using System.Windows;

using Flow.Config;
using System.Reflection;

namespace Flow
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
		protected override void OnStartup(StartupEventArgs e)
		{
            //// Get Reference to the current Process
            //Process thisProc = Process.GetCurrentProcess();
            //// Check how many total processes have the same name as the current one
            //if (Process.GetProcessesByName(thisProc.ProcessName).Length > 1)
            //{
            //    // If ther is more than one, than it is already running.
            //    MessageBox.Show("Flow Application is already running.");
            //    Application.Current.Shutdown();
               
            //    return;
            //}

            //if (e.Args.Length == 1 && e.Args[0] == "INSTALLER") { Process.Start(Assembly.GetExecutingAssembly().FullName); return; }

			base.OnStartup(e);
            Bootstrapper.Initialize();
		}
	}
}
