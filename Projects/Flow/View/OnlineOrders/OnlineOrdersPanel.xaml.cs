﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Flow.Schema.LinqModel.DataContext;
using Flow.View.Project.Base;
using Flow.Lib.Helpers;
using Flow.Schema.LinqModel.DataContext.FlowMaster;
using Flow.Controller;
using Flow.Controller.Project;
using System.Diagnostics;
using Flow.View.Dialogs;
using Flow.Lib;
using System.ComponentModel;
using StructureMap;
using Flow.Controls;
using System.Windows.Threading;
using System.Runtime.InteropServices;
using NetServ.Net.Json;
using System.Threading;
using System.IO;
using System.Net;
using NLog;
using Flow.Controller.OnlineOrdering;
using Microsoft.Win32;
using System.Web.Script.Serialization;
using System.Web;

namespace Flow.View
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class OnlineOrdersPanel : ProjectPanelBase
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        private bool renderGreenScreen = false;
        //private bool applyCrops = false;
        //private bool includeGroupImages = false;
        private bool useOriginalFileNames = false;

        private string saveOrderPath = null;

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        static extern EXECUTION_STATE SetThreadExecutionState(EXECUTION_STATE esFlags);

        [FlagsAttribute]
        public enum EXECUTION_STATE : uint
        {
            ES_AWAYMODE_REQUIRED = 0x00000040,
            ES_CONTINUOUS = 0x80000000,
            ES_DISPLAY_REQUIRED = 0x00000002,
            ES_SYSTEM_REQUIRED = 0x00000001
        }

        //no longer prevent the system from sleeping
        void AllowSleep()
        {
            SetThreadExecutionState(EXECUTION_STATE.ES_CONTINUOUS);
        }

        //prevents the system from sleeping untill the app closes or AllowSleep is called
        void PreventSleep()
        {
            // Prevent Idle-to-Sleep (monitor not affected)
            SetThreadExecutionState(EXECUTION_STATE.ES_CONTINUOUS | EXECUTION_STATE.ES_AWAYMODE_REQUIRED);
        }

        //just resets the idle clock, must keep calling this periodically to keep awake
        void KeepSystemAwake()
        {
            SetThreadExecutionState(EXECUTION_STATE.ES_SYSTEM_REQUIRED);
        }

        private NotificationProgressInfo _notificationInfo;
        private NotificationProgressInfo _notificationInfo2;


        /// /////////////////////////////////////////////////////////////////////////
        #region DEPENDENCY PROPERTIES
        /// /////////////////////////////////////////////////////////////////////////




        protected static DependencyProperty SelectedFlowProjectProperty = DependencyProperty.Register(
            "SelectedFlowProject", typeof(FlowProject), typeof(OnlineOrdersPanel),
            new FrameworkPropertyMetadata(new PropertyChangedCallback(SelectedFlowProjectPropertyChanged))
        );

        public FlowProject SelectedFlowProject
        {
            get { return (FlowProject)this.GetValue(SelectedFlowProjectProperty); }
            set { this.SetValue(SelectedFlowProjectProperty, value); }
        }

        public static void SelectedFlowProjectPropertyChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            OnlineOrdersPanel source = o as OnlineOrdersPanel;

            if (source != null)
            {
                source.FlowProjectSelected = (e.NewValue != null);
                
            }
        }

        protected static DependencyProperty FlowProjectSelectedProperty =
            DependencyProperty.Register("FlowProjectSelected", typeof(bool), typeof(OnlineOrdersPanel));

        public bool FlowProjectSelected
        {
            get { return (bool)this.GetValue(FlowProjectSelectedProperty); }
            set { this.SetValue(FlowProjectSelectedProperty, value); }
        }




        /// /////////////////////////////////////////////////////////////////////////
        #endregion DEPENDENCY PROPERTIES
        /// /////////////////////////////////////////////////////////////////////////

        public FlowProject DefaultProject { get; set; }

        private FlowProject RootProject { get; set; }

      
        public OnlineOrdersPanel()
        {
            //don init component until FlowController is set
            
        }


        protected override void OnSetFlowController()
        {
            base.OnSetFlowController();

            this.DataContext = this.ProjectController;

            if (this.CurrentFlowProject == null)
                return;

            this.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();

            PlicCatalogContent PCC = ProjectController.PlicCatalog;
            

           //this.FlowMasterDataContext.SelectedStudio

            if (this.ProjectController.IsProjectLoaded)
            {
                initOnlineGallerySettings();
            }
            else
            {
                //ShowHideShippingOptions();
            }
            if(this.ProjectController.CurrentFlowOnlineGallery.ApplyCrops == null)
                this.ProjectController.CurrentFlowOnlineGallery.ApplyCrops = true;

            if (this.ProjectController.CurrentFlowOnlineGallery.IncludeGroupImages == null)
                this.ProjectController.CurrentFlowOnlineGallery.IncludeGroupImages = false;

            //this.ProjectController.PropertyChanged += new PropertyChangedEventHandler(ProjectController_PropertyChanged);

            if (this.ProjectController.CurrentFlowOnlineGallery == null)
                this.ProjectController.initOnlineGallerySettings();

            try
            {
                var IEVAlue = 9000; // can be: 9999 , 9000, 8888, 8000, 7000
                var targetApplication = System.Diagnostics.Process.GetCurrentProcess().ProcessName + ".exe";
                var localMachine = Registry.LocalMachine;
                var parentKeyLocation = @"SOFTWARE\Microsoft\Internet Explorer\MAIN\FeatureControl";
                var keyName = "FEATURE_BROWSER_EMULATION";
                logger.Info("opening up Key: {0} at {1}", keyName, parentKeyLocation);
                var parentKey = localMachine.OpenSubKey(parentKeyLocation, RegistryKeyPermissionCheck.ReadWriteSubTree);
                var subKey = parentKey.CreateSubKey(keyName,RegistryKeyPermissionCheck.ReadWriteSubTree);
                subKey.SetValue(targetApplication, IEVAlue, RegistryValueKind.DWord);
                logger.Info("all done, now try it on a new process");
            }
            catch (Exception ex)
            {
                logger.Info("NOTE: you need to run this under no UAC");
                logger.Info(ex);
            }

            //this needs to be after DataContext is defined
            InitializeComponent();

            this.AllOrdersPanel.SetDataContext(this.ProjectController);


            string username = this.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.EcommerceUsername;// "test@photolynx-api.com";
            string password = this.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.EcommercePassword; //"photolynx-artona";
            if (!string.IsNullOrEmpty(username))
            {
                string loginuri = this.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.EcommerceAPIURL.Replace("/Api/", "/admin/login?") + "&staff_user[email]=" + username + "&staff_user[password]=" + password;
                if(loginuri.StartsWith("http") || loginuri.StartsWith("www")) this.browser.Navigate(new Uri(loginuri, UriKind.Absolute));
                
            }
            else
            {
                string loginuri = this.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.EcommerceAPIURL.Replace("/Api/", "/admin/login");
                if (loginuri.StartsWith("http") || loginuri.StartsWith("www")) this.browser.Navigate(new Uri(loginuri, UriKind.Absolute));
            }

            if (this.CurrentFlowProject.OnlineGallery != null && !string.IsNullOrEmpty(this.CurrentFlowProject.OnlineGallery.GalleryURL))
            {
                string uri = this.CurrentFlowProject.OnlineGallery.GalleryURL.Replace("?id=", "admin/galleries/");
                if (uri.StartsWith("http") || uri.StartsWith("www")) this.browser.Navigate(new Uri(uri, UriKind.Absolute));
            }
            this.browser.ObjectForScripting = new ScriptingHelper();


            this.CurrentFlowProject.UpdateGalleryCounts();

            this.CurrentFlowProject.FlowProjectDataContext.UpdateFilteredOrderPackages();

            this.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();

            if (this.CurrentFlowProject.OnlineGallery.RetouchOptionID != null)
            {
                cmbRetouch.SelectedItem = this.CurrentFlowProject.OnlineGallery.RetouchOptionID;
            }

            HandleAPEWizardVisibility();
        }

        [ComVisible(true)]
        public class ScriptingHelper
        {
            public void ShowMessage(string message)
            {
                MessageBox.Show(message);
            }
        }

        UploadOnlineGallery UploadOnlineGallery { get; set; }
        private void Submit_Click(object sender, RoutedEventArgs e)
        {

            this.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();


            List<Subject> ReadySubjects = this.CurrentFlowProject.SubjectList.Where(s => s.Ready == true).ToList();
            List<Subject> LiveSubjects = this.CurrentFlowProject.SubjectList.Where(s =>  s.ImageList.Where(i => i.ExistsInOnlineGallery == true).Count() > 0).ToList();
            List<Subject> ApplicableSubjects = this.CurrentFlowProject.SubjectList.Where(s => s.Ready == true || s.ImageList.Where(i => i.ExistsInOnlineGallery == true).Count() > 0).ToList();
            if (this.UseReadySubjects.IsChecked == true && this.FlowMasterDataContext.PreferenceManager.Capture.UseReadyFlag == true)
            {
                FlowMessageBox msg1 = new FlowMessageBox("Ready Subjects Being Used", "Only Publishing Gallery for Existing Live Images and Ready Subjects:\n\n Ready Subjects: " + ReadySubjects.Count() + "\nLive Subjects: " + LiveSubjects.Count() + "\nReady OR Live Subjects: " + ApplicableSubjects.Count());
                if (msg1.ShowDialog() != true)
                    return;
            }
            else
            {
                ApplicableSubjects = this.CurrentFlowProject.SubjectList.ToList();
            }
            this.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();
            //if (string.IsNullOrEmpty(this.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixCustomerID) ||
            //    (string.IsNullOrEmpty(this.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixCustomerPassword)))
            //{
            //    FlowMessageBox msg = new FlowMessageBox("Missing ImageQuix Account Info", "Missing username and password in ImageQuix Account Info.");
            //    msg.CancelButtonVisible = false;
            //    msg.ShowDialog();
            //    return;
            //}

            if (this.FlowController.ProjectController.CurrentFlowProject.FlowCatalog == null)
            {
                FlowMessageBox msg = new FlowMessageBox("No Catalog", "You can not submit an online gallery for a project\nthat has no Package Catalog\nSelect a Catalog in Capture -> Order Entry");
                msg.CancelButtonVisible = false;
                msg.ShowDialog();
                return;
            }
            //if (this.CurrentFlowProject.IsGreenScreen == true)
            //{
            //    int missingGSsettings = 0;
            //    foreach (SubjectImage si in this.CurrentFlowProject.FlowProjectDataContext.SubjectImages.Where(si=>si.ExistsInOnlineGallery==false))
            //    {
            //        if (si.GreenScreenSettings == null)
            //            missingGSsettings++;
            //    }

            //    if (missingGSsettings > 0)
            //    {
            //        FlowMessageBox fmsg = new FlowMessageBox("Missing Green Screen Data", missingGSsettings + " of the images do not have any Green Screen Settings applied. This will result in photos with the Green not dropped out");
            //        fmsg.buttonCancel.Content = "Cancel Upload";
            //        fmsg.buttonOkay.Content = "Continue";
            //        if (fmsg.ShowDialog() == false)
            //            return;
            //    }
            //}
            PreventSleep();
            this.FlowController.ApplicationPanel.IsEnabled = false;
            this.FlowController.ProjectController.IsUploading = true;
            this.FlowMasterDataContext.SavePreferences();
            //this.FlowMasterDataContext.SubmitChanges();
            FlowProject flowProject = this.SelectedFlowProject;
            _notificationInfo = new NotificationProgressInfo("Uploading To ImageQuix Gallery", "Preparing Upload...");

            this.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();
            _notificationInfo.Update("Preparing Upload...");

            
            UploadOnlineGallery = new UploadOnlineGallery(this.ProjectController.CurrentFlowProject, this.FlowMasterDataContext, this.ProjectController.CurrentFlowOnlineGallery, ApplicableSubjects, txtAPIURL.Text, txtAPIKEY.Text);

            this.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();
            UploadOnlineGallery.NotificationInfo = _notificationInfo;

            FlowBackgroundWorkerManager.RunWorker(UploadWorker, UploadWorker_Completed, true);
            
        }

        void UploadWorker(object sender, DoWorkEventArgs e)
        {

            IShowsNotificationProgress progressNotification = ObjectFactory.GetInstance<IShowsNotificationProgress>();
            _notificationInfo.NotificationProgress = progressNotification;
            progressNotification.ShowNotification(_notificationInfo);

            int waitCount = 0;
            string dots = ".";
            while (this.CurrentFlowProject.initingSubjects)
            {
                dots += ".";
                _notificationInfo.Update("Waiting for subjects to initialize." + dots);
                waitCount++;
                Thread.Sleep(3000);

                if (waitCount == (4 * 60)) //if its taken longer than 4 minutes, bail
                    this.CurrentFlowProject.initingSubjects = false;

            }

            this.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();
            //string userId = this.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixCustomerID;
            //string userP = this.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixCustomerPassword;

            UploadOnlineGallery.Begin();
  
        }

        void UploadWorker_Completed(object sender, RunWorkerCompletedEventArgs e)
        {

            this.Submit.Content = "Update Gallery";

            //string custID = this.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixCustomerID;
            //if (!string.IsNullOrEmpty(this.ProjectController.CurrentFlowProjectGallery.GalleryCode))
            //    this.lblGallerySite.Text = "https://vando.imagequix.com/g" + this.ProjectController.CurrentFlowProjectGallery.GalleryCode;
            //else
            //{
            //    this.lblGallerySite.Text = "https://vando.imagequix.com/gallery.html?id=" + custID + "&eventid=" + this.ProjectController.CurrentFlowProjectGallery.EventID;
            //}
            //this.ProjectController.CurrentFlowProjectGallery.GalleryURL = this.lblGallerySite.Text;
            this._notificationInfo.Complete("Done uploading");
            this.FlowMasterDataContext.SavePreferences();
            //this.FlowMasterDataContext.SubmitChanges();


            this.FlowController.ApplicationPanel.IsEnabled = true;
            this.FlowController.ProjectController.IsUploading = false;
            AllowSleep();

            this.FlowMasterDataContext.RefreshGalleryList();

            
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            logger.Info("Cancel button clicked");
            this.FlowController.Project();
        }

     

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            logger.Info("Save button clicked");

            this.FlowMasterDataContext.SavePreferences();
            //this.FlowMasterDataContext.SubmitChanges();

            // Hide the account settings alert if settings have been entered
            //if (this.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.HasImageQuixCustomerCredentials())
            //{
            //    this.accountSettingsAlert.Visibility = Visibility.Collapsed;
            //    this.accountExpander.IsExpanded = false;
            //}
            //else
            //{
            //    this.accountSettingsAlert.Visibility = Visibility.Visible;
            //    this.accountExpander.IsExpanded = true;
            //}
        }

     
        private void btnUnMarkLiveImage_Click(object sender, RoutedEventArgs e)
        {
            foreach (SubjectImage si in this.CurrentFlowProject.FlowProjectDataContext.SubjectImages.Where(i => i.ExistsInOnlineGallery == true))
            {
                si.ExistsInOnlineGallery = false;
                si.Subject.FlagForMerge = true;
            }
            this.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();
            this.CurrentFlowProject.UpdateGalleryCounts();         

        }

        private void initOnlineGallerySettings()
        {
            if (this.ProjectController == null || this.ProjectController.CurrentFlowProject == null)
                return;

            if (this.ProjectController != null && this.ProjectController.CurrentFlowProject  != null && this.ProjectController.CurrentFlowProject.OnlineGallery == null)
            {
                OnlineGallery gal = new OnlineGallery(this.ProjectController.CurrentFlowProject.FlowProjectGuid, this.ProjectController.CurrentFlowProject.FlowProjectName);
                this.ProjectController.CurrentFlowProject.FlowProjectDataContext.OnlineGalleries.InsertOnSubmit(gal);
                this.ProjectController.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();
                if (this.ProjectController.CurrentFlowProject.Gallery != null && this.ProjectController.CurrentFlowProject.Gallery.GalleryURL != null && this.ProjectController.CurrentFlowProject.Gallery.GalleryURL.Contains("schooldayphoto.com"))
                {
                    ProjectGallery old = this.ProjectController.CurrentFlowProject.Gallery;
                    gal.GalleryCode = old.GalleryCode;
                    gal.GalleryID = old.EventID;
                    gal.GalleryName = old.GalleryName;
                    gal.GalleryURL = old.GalleryURL;
                    gal.ApplyCrops = old.ApplyCrops;
                    gal.GalleryBackgroundList = old.GalleryBackgroundList;
                    gal.ShippingOption = "pickup";
                    gal.ShippingFee = 0;
                }

            }

            if (!string.IsNullOrEmpty(this.ProjectController.CurrentFlowProject.OnlineGallery.RetouchOptionID) && this.cmbRetouch != null)
            {
                int retouchID = Int32.Parse(this.ProjectController.CurrentFlowProject.OnlineGallery.RetouchOptionID);
                FlowCatalogOption o = ProjectController.CurrentFlowProject.FlowCatalog.FlowCatalogImageOptionList.FirstOrDefault(p => p.IQPrimaryKey == retouchID);
                this.cmbRetouch.SelectedItem = o;
            }

            this.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();

            if (string.IsNullOrEmpty(this.ProjectController.CurrentFlowProject.OnlineGallery.GalleryName))
                this.ProjectController.CurrentFlowProject.OnlineGallery.GalleryName = this.ProjectController.CurrentFlowProject.FlowProjectName;
            if (string.IsNullOrEmpty(this.ProjectController.CurrentFlowProject.OnlineGallery.GalleryURL))
                this.ProjectController.CurrentFlowProject.OnlineGallery.GalleryURL = "Gallery Not Yet Uploaded";
            if (string.IsNullOrEmpty(this.ProjectController.CurrentFlowProject.OnlineGallery.ShippingOption))
                this.ProjectController.CurrentFlowProject.OnlineGallery.ShippingOption = "pickup";
            if (this.ProjectController.CurrentFlowProject.OnlineGallery.ShippingFee == null)
                this.ProjectController.CurrentFlowProject.OnlineGallery.ShippingFee = 0;

            this.ProjectController.UpdateCurrentFlowOnlineGallery();

            this.ProjectController.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();

        }

        //private void updateExistingGallery()
        //{
        //    //ProjectGallery _ProjectGallery = (sender as TabItem).DataContext as ProjectGallery;
        //    ProjectGallery _ProjectGallery = this.CurrentFlowProject.FlowProjectDataContext.ProjectGalleries.First(a => a.FlowProjectGuid == this.CurrentFlowProject.FlowProjectGuid);
        //    bool isNewGallery = true;
        //    JsonObject existingGalleryJson = null;

        //    if (_ProjectGallery != null && _ProjectGallery.EventID != null)
        //        isNewGallery = false;

        //    string mainUri = this.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixLoginURL;
        //    string custId = this.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixCustomerID;
        //    string custPw = this.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixCustomerPassword;

        //    string getGalleryURL = mainUri + "/" + custId + "/" + "event/" + _ProjectGallery.EventID + "?filter=IQ";
        //    string getPSURL = mainUri + "/" + custId + "/" + "event/" + _ProjectGallery.EventID + "/pricesheet?filter=IQ";

        //    //lets get the existing gallery json
        //    if (isNewGallery)
        //    {
        //        existingGalleryJson = null;
        //        //existingPSJson = null;
        //    }
        //    else
        //    {
        //        string JsonResponse = GetJson(getGalleryURL, custId, custPw, "");
        //        if (JsonResponse == null)
        //            existingGalleryJson = null;
        //        else
        //        {
        //            //int loc = JsonResponse.IndexOf("eventType");
        //            //JsonResponse = JsonResponse.Remove(loc, "eventType".Length).Insert(loc, "eventType2");
        //            JsonParser parser = new JsonParser(new StringReader(JsonResponse), true);
        //            //NetServ.Net.Json.JsonParser.TokenType nextToken = parser.NextToken();
        //            //NetServ.Net.Json.JsonParser.TokenType nextToken2 = parser.NextToken();
        //            existingGalleryJson = parser.ParseObject();
        //        }

        //        if (existingGalleryJson != null)
        //        {

        //            _ProjectGallery.GalleryName = existingGalleryJson["title"].ToString();
        //            _ProjectGallery.WelcomeText = existingGalleryJson["welcomeMessage"].ToString();
        //            // _ProjectGallery.WelcomeImage = existingGalleryJson["welcomeImage"].ToString().Split("/").Last();
        //            _ProjectGallery.EventDate = DateTime.ParseExact(existingGalleryJson["eventDate"].ToString(), "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture);
        //            if (existingGalleryJson["expirationDate"] == JsonNull.Null)
        //                _ProjectGallery.ExpirationDate = null;
        //            else
        //                _ProjectGallery.ExpirationDate = DateTime.ParseExact(existingGalleryJson["expirationDate"].ToString(), "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture);
        //            _ProjectGallery.Keyword = existingGalleryJson["keyword"].ToString();

        //            //config
        //            _ProjectGallery.MinOrderAmount = Convert.ToDecimal(((existingGalleryJson["galleryConfig"] as JsonObject)["min-order-amount"] as JsonNumber).Value);
        //            _ProjectGallery.ShippingLabel = (existingGalleryJson["galleryConfig"] as JsonObject)["shipping-label"].ToString();
        //            _ProjectGallery.HandlingLabel = (existingGalleryJson["galleryConfig"] as JsonObject)["handling-label"].ToString();
        //            _ProjectGallery.HandlingRateType = (existingGalleryJson["galleryConfig"] as JsonObject)["handling-rate-type"].ToString();
        //            _ProjectGallery.TaxLabel = (existingGalleryJson["galleryConfig"] as JsonObject)["tax-label"].ToString();

        //            _ProjectGallery.TaxAllOrders = ((existingGalleryJson["galleryConfig"] as JsonObject)["tax-all-orders"] as JsonBoolean).Value;
        //            _ProjectGallery.OrderThankyouMessage = (existingGalleryJson["galleryConfig"] as JsonObject)["order-thankyou-message"].ToString();
        //            _ProjectGallery.DisableCropping = ((existingGalleryJson["galleryConfig"] as JsonObject)["disable-cropping"] as JsonBoolean).Value;
        //            _ProjectGallery.DefaultShowImageName = ((existingGalleryJson["galleryConfig"] as JsonObject)["default-show-image-names"] as JsonBoolean).Value;

        //            if ((existingGalleryJson["galleryConfig"] as JsonObject)["shipmentType"].ToString() == "pickup")
        //            {
        //                _ProjectGallery.PickupShip = true;
        //                _ProjectGallery.DirectShip = false;
        //                _ProjectGallery.CustomerChoiceShip = false;
        //            }
        //            else if ((existingGalleryJson["galleryConfig"] as JsonObject)["shipmentType"].ToString() == "direct")
        //            {
        //                _ProjectGallery.PickupShip = false;
        //                _ProjectGallery.DirectShip = true;
        //                _ProjectGallery.CustomerChoiceShip = false;
        //            }
        //            else
        //            {
        //                _ProjectGallery.PickupShip = false;
        //                _ProjectGallery.DirectShip = false;
        //                _ProjectGallery.CustomerChoiceShip = true;
        //            }
        //            if ((existingGalleryJson["galleryConfig"] as JsonObject)["shipStartDate"] != JsonNull.Null)
        //            {
        //                _ProjectGallery.SwitchShippingDate = DateTime.ParseExact((existingGalleryJson["galleryConfig"] as JsonObject)["shipStartDate"].ToString(), "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture);
        //                _ProjectGallery.SwitchShipping = true;
        //            }
        //            else
        //            {
        //                _ProjectGallery.SwitchShippingDate = null;
        //                _ProjectGallery.SwitchShipping = false;
        //            }
        //            _ProjectGallery.PickupText = (existingGalleryJson["galleryConfig"] as JsonObject)["pickupLabel"].ToString();
        //            //_ProjectGallery.ShippingCost = Convert.ToDecimal(((existingGalleryJson["galleryConfig"] as JsonObject)["shipping"] as JsonNumber).Value);
        //            //_ProjectGallery.HandlingCost = Convert.ToDecimal(((existingGalleryJson["galleryConfig"] as JsonObject)["handling"] as JsonNumber).Value);
        //            //_ProjectGallery.TaxRate = Convert.ToDecimal(((existingGalleryJson["galleryConfig"] as JsonObject)["tax"] as JsonNumber).Value);

        //            if (((existingGalleryJson["galleryConfig"] as JsonObject)["shipping"] as JsonNumber).Value > 0)
        //                _ProjectGallery.ShippingCost = Convert.ToDecimal(((existingGalleryJson["galleryConfig"] as JsonObject)["shipping"] as JsonNumber).Value);
        //            else
        //                _ProjectGallery.ShippingCost = 0;

        //            if (((existingGalleryJson["galleryConfig"] as JsonObject)["handling"] as JsonNumber).Value > 0)
        //                if (_ProjectGallery.HandlingRateType == "percent")
        //                {
        //                    _ProjectGallery.HandlingCost = Convert.ToDecimal(((existingGalleryJson["galleryConfig"] as JsonObject)["handling"] as JsonNumber).Value) * 100;
        //                    if (_ProjectGallery.HandlingCost > 60)
        //                        _ProjectGallery.HandlingCost = _ProjectGallery.HandlingCost / 100;
        //                }
        //                else
        //                    _ProjectGallery.HandlingCost = Convert.ToDecimal(((existingGalleryJson["galleryConfig"] as JsonObject)["handling"] as JsonNumber).Value);
        //            else
        //                _ProjectGallery.HandlingCost = 0;

        //            if (((existingGalleryJson["galleryConfig"] as JsonObject)["tax"] as JsonNumber).Value > 0)
        //            {
        //                _ProjectGallery.TaxRate = Convert.ToDecimal(((existingGalleryJson["galleryConfig"] as JsonObject)["tax"] as JsonNumber).Value) * 100;
        //                if (_ProjectGallery.TaxRate > 60)
        //                {
        //                    _ProjectGallery.TaxRate = _ProjectGallery.TaxRate / 100;
        //                }
        //            }
        //            else
        //                _ProjectGallery.TaxRate = 0;

        //            _ProjectGallery.TaxShipping = ((existingGalleryJson["galleryConfig"] as JsonObject)["tax-shipping"] as JsonBoolean).Value;

        //            _ProjectGallery.ShowBWSepia = ((existingGalleryJson["galleryConfig"] as JsonObject)["show-bw-sepia"] as JsonBoolean).Value;

        //            _ProjectGallery.Theme = (existingGalleryJson["galleryConfig"] as JsonObject)["theme"].ToString();
        //            _ProjectGallery.Wallpaper = (existingGalleryJson["galleryConfig"] as JsonObject)["wallpaper"].ToString();

        //            //end config

        //            _ProjectGallery.WatermarkText = existingGalleryJson["watermark"].ToString();
        //            _ProjectGallery.WatermarkLocation = existingGalleryJson["watermark-position"].ToString();

        //            _ProjectGallery.YearbookPoses = Convert.ToInt32((existingGalleryJson["num-yearbook-selection"] as JsonNumber).ToString());

        //            this.FlowMasterDataContext.SubmitChanges();
        //        }
        //    }
        //}

        private string GetJson(string uri, string custId, string custPw, string postData)
        {
            logger.Info("t " + uri);

            HttpWebRequest rq = (HttpWebRequest)WebRequest.Create(uri);
            rq.Credentials = new NetworkCredential(custId, custPw);
            rq.Timeout = (15000);//try for 15 seconds
            rq.Method = "GET";

            HttpWebResponse rs = null;
            int tryCount = 0;
            while (tryCount < 5)
            {
                try
                {
                    rs = (HttpWebResponse)rq.GetResponse();

                    StreamReader reader = new StreamReader(rs.GetResponseStream());
                    string jsonText = reader.ReadToEnd();

                    reader.Close();
                    rs.Close();
                    return jsonText;

                }
                catch (WebException e)
                {

                    tryCount++;

                    //if (tryCount == 5)
                    //    throw e;
                }
            }

           
            return null;
        }

        private void txtIQCustomerPW_Loaded(object sender, RoutedEventArgs e)
        {
            //this.txtCustomerPW.Password = this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixCustomerPassword;
        }

        private void txtIQCustomerPW_PasswordChanged(object sender, RoutedEventArgs e)
        {
           // this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixCustomerPassword = this.txtCustomerPW.Password;
        }

        private void UseReadySubjects_Checked(object sender, RoutedEventArgs e)
        {
            
        }

        private void accountSettingsAlert_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void txtCustomerPW_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void rdoDirect_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void rdoChoice_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void rdoPickup_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void lblGallerySite_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            LaunchURL((sender as TextBox).Text);
        }

        private void LaunchURL(string url)
        {
            if (url.ToLower().Contains("id="))
            {
                if ((url != null && url != ""))
                {
                    ProcessStartInfo procInfo = new ProcessStartInfo(url);
                    Process.Start(procInfo);
                }
                else
                {
                    // MessageBox.Show("Bade URL");
                }
            }
        }

        private void rdoPickup_Unchecked(object sender, RoutedEventArgs e)
        {

        }

        private void rdoChoice_Unchecked(object sender, RoutedEventArgs e)
        {

        }

        private void rdoDirect_Unchecked(object sender, RoutedEventArgs e)
        {

        }

        private void txtCustomerPW_PasswordChanged(object sender, RoutedEventArgs e)
        {

        }

        private void btnCheckForOrders_Click(object sender, RoutedEventArgs e)
        {
            List<string> OrderErrors = new List<string>();
            List<int> CompleteOrders = new List<int>();

            string apiurl = this.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.EcommerceAPIURL;
            OnlineApiHelpers OnlineApiHelpers = new OnlineApiHelpers(this.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.EcommerceAPIKey);

            JsonObject GalleryOrders = null;
            GalleryOrders = OnlineApiHelpers.GetJson(System.IO.Path.Combine(apiurl, "GetOrdersForJobWithGuid"), "{\"guid\":\"" + this.ProjectController.CurrentFlowProject.OnlineGallery.GalleryCode + "\", \"markComplete\":\"false\"}");

            JavaScriptSerializer json_serializer2 = new JavaScriptSerializer();
            string order = json_serializer2.Serialize(GalleryOrders);
            File.WriteAllText(System.IO.Path.Combine(FlowContext.FlowTempDirPath, "Order.txt"), order);

            
            if (GalleryOrders == null || GalleryOrders.Count < 1)
            {
                FlowMessageBox msg = new FlowMessageBox("Download Orders", "There are no new orders");
                msg.CancelButtonVisible = false;
                msg.ShowDialog();
                return;
            }
            int cnt = 0;
            foreach (JsonObject GalleryOrder in GalleryOrders["order"] as JsonArray)
            {
                cnt++;
                string ticketCode = ((JsonString)GalleryOrder["SubjectPassword"]).ToString();
                int OrderId = (int)(((JsonNumber)GalleryOrder["OrderGuid"]).Value);
                string Authorization = "";
                if(GalleryOrder.ContainsKey("Authorization") && GalleryOrder["Authorization"].JsonTypeCode == JsonTypeCode.String)
                    Authorization = ((JsonString)GalleryOrder["Authorization"]).ToString();

                Subject sub = this.ProjectController.CurrentFlowProject.SubjectList.FirstOrDefault(o => o.TicketCode == ticketCode);
                if (!sub.SubjectOrder.OrderPackages.Any(op => op.WebOrderID == OrderId))
                {
                    bool failed = false;
                    foreach (JsonObject orderItem in (JsonArray)GalleryOrder["OrderItems"])
                    {
                        string mapKey = ((orderItem["Package"]).ToString()).Split('-')[0];
                        FlowCatalogOption op = null;
                        ProductPackage ppkg = this.ProjectController.CurrentFlowProject.FlowCatalog.ProductPackages.FirstOrDefault(p => p.Map == mapKey);
                        if (ppkg == null)
                        {
                            //this might be an option
                            op = this.ProjectController.CurrentFlowProject.FlowCatalog.FlowCatalogOptions.FirstOrDefault(p => p.IQPrimaryKey.ToString() == mapKey);
                            if(op == null)
                                continue;
                        }
                            
                        
                        string imageName = ((JsonString)orderItem["OriginalImageFilename"]).ToString();
                        SubjectImage si = this.ProjectController.CurrentFlowProject.ImageList.FirstOrDefault(im => im.ImageFileName == imageName);
                        if (si == null)
                        {
                            //might be a png JPG
                            imageName = ((JsonString)orderItem["OriginalImageFilename"]).ToString().Replace(".png", ".JPG");
                            si = this.ProjectController.CurrentFlowProject.ImageList.FirstOrDefault(im => im.ImageFileName == imageName);
                        }
                        if (si == null)
                        {
                            //might be a png jpg
                            imageName = ((JsonString)orderItem["OriginalImageFilename"]).ToString().Replace(".png", ".jpg");
                            si = this.ProjectController.CurrentFlowProject.ImageList.FirstOrDefault(im => im.ImageFileName == imageName);
                        }
                        if (si == null)
                        {
                            failed = true;
                            OrderErrors.Add("Order: " + OrderId + " - Failed to match the ordered image on a product: " + ((JsonString)orderItem["OriginalImageFilename"]).ToString());
                            break;
                        }

                        string background = ((JsonString)orderItem["Background"]).ToString();
                        //string backgroundFullPath = null;
                        GreenScreenBackground gsBack = null;
                        if(!string.IsNullOrEmpty(background))
                        {
                            if (this.FlowMasterDataContext.GreenScreenBackgrounds.Any(gs => gs.DisplayName == background))
                                gsBack = this.FlowMasterDataContext.GreenScreenBackgrounds.FirstOrDefault(gs => gs.DisplayName == background);
                            else
                            {
                                failed = true;
                                OrderErrors.Add("Order: " + OrderId + " - Failed to match the background on a GS product: " + background);
                                break;
                            }
                        }
                        
                        if(ppkg != null)
                        {
                            string bkgrd = "";
                            if(gsBack != null)
                                bkgrd = gsBack.FullPath;
                            OrderPackage pg = new OrderPackage(ppkg, sub.SubjectOrder, "Primary", si, bkgrd);
                            if (ppkg.IsAnyXPackage == true)
                            {
                                List<string> units = ((orderItem["Package"]).ToString()).Split(';').ToList();
                                units.RemoveAt(0);//the first one is the package id, the following are the units in the package
                                pg.Units = string.Join(";", units);
                            }
                            if (gsBack != null)
                            {
                                pg.GreenScreenBackground = gsBack.FullPath;
                            }
                            pg.WebOrderID = OrderId;
                            pg.WebOrderAuthorizationToken = Authorization;
                            pg.WebOrderDate = DateTime.Now;
                            pg.IsOnlineOrder = true;
                            pg.ShipmentType = "direct";
                            if (orderItem.ContainsKey("ShippingName"))
                            {
                                pg.ShippingFirstName = ((JsonString)orderItem["ShippingName"]).ToString();
                                pg.ShippingAddress = ((JsonString)orderItem["ShippingAddress1"]).ToString() + " " + ((JsonString)orderItem["ShippingAddress2"]).ToString();
                                pg.ShippingCity = ((JsonString)orderItem["ShippingCity"]).ToString();
                                pg.ShippingState = ((JsonString)orderItem["ShippingState"]).ToString();
                                pg.ShippingZip = ((JsonString)orderItem["ShippingZip"]).ToString();
                            }
                            sub.SubjectOrder.OrderPackages.Add(pg);
                        }
                        if (op != null)
                        {
                            OrderImageOption oio = si.AddCatalogOption(op);
                            oio.WebOrderID = OrderId;
                            oio.WebOrderDate = DateTime.Now;
                        }


                        
                        sub.SubjectOrder.RefreshOrderDetails();
                    }
                    if (failed) continue;
                }
                CompleteOrders.Add(OrderId);
            }

            //mark orders as complete
            string idString = string.Join(",", CompleteOrders);
            JsonObject markCompleteResults = OnlineApiHelpers.GetJson(System.IO.Path.Combine(apiurl, "MarkOrdersComplete"), "{\"galleryGuid\":\"" + this.ProjectController.CurrentFlowProject.OnlineGallery.GalleryCode + "\", \"orderIDs\":[" + idString + "]}");
            
            
            //if we made it here, there were new orders loaded
            this.ProjectController.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();
            this.ProjectController.CurrentFlowProject.FlowProjectDataContext.UpdateOrderPackages();
            
            string errormessage = "";
            if(OrderErrors.Count > 0)
                errormessage = "\n\nThe following orders could not be processed:\n" + string.Join(",\n", OrderErrors);

            FlowMessageBox msg2 = new FlowMessageBox("Download Orders", CompleteOrders.Count + " new orders were downloaded\n" + errormessage);
            msg2.CancelButtonVisible = false;
            msg2.ShowDialog();
        }

        private void FilterChanged(object sender, RoutedEventArgs e)
        {

            if ((DataContext as ProjectController) != null)
            {
                (DataContext as ProjectController).CurrentFlowProject.FlowProjectDataContext.UpdateFilteredOrderPackages();
                (DataContext as ProjectController).CurrentFlowProject.FlowProjectDataContext.UpdateOrderFilterValues();

            }
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            ProjectGallery pg = this.CurrentFlowProject.Gallery;
            if(pg != null)
                this.CurrentFlowProject.FlowProjectDataContext.ProjectGalleries.DeleteOnSubmit(pg);
            OnlineGallery fpg = (sender as Button).DataContext as OnlineGallery;
            if(fpg != null)
                this.CurrentFlowProject.FlowProjectDataContext.OnlineGalleries.DeleteOnSubmit(fpg);
            this.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();
            initOnlineGallerySettings();
            this.FlowMasterDataContext.RefreshGalleryList();
        }

        private void cmbRetouch_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                FlowCatalogOption o = e.AddedItems[0] as FlowCatalogOption;
                this.CurrentFlowProject.OnlineGallery.RetouchOptionID = o.IQPrimaryKey.ToString();
            }
        }

        private void HandleAPEWizardVisibility()
        {
            pnlLaunchWarning.Visibility = Visibility.Collapsed;
            if (this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.ShowEcommerceSignupPanel == true)
            {
                bool hasAPIKey = !string.IsNullOrEmpty(this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.EcommerceAPIKey);
                if (!hasAPIKey || !IsSiteLive())
                {
                    pnlLaunchWarning.Visibility = Visibility.Visible;
                }

            }
        }

        private bool IsSiteLive()
        {
            try
            {

                string apiURL = this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.EcommerceAPIURL;
                string apiKey = this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.EcommerceAPIKey;

                if (String.IsNullOrEmpty(apiKey)) return false;

                string fullAPI = apiURL + "/GetStudioDetails";

                HttpWebRequest rq = (HttpWebRequest)WebRequest.Create(fullAPI);
                //rq.Headers.Add("JCart-Api-Key", apikey);
                rq.ContentType = "application/x-www-form-urlencoded";
                rq.Headers.Add("JCart-Api-Key", apiKey);
                rq.Timeout = (120000);//try for 120 seconds
                rq.Method = "POST";

                ServicePointManager.ServerCertificateValidationCallback = delegate(
                        Object obj, System.Security.Cryptography.X509Certificates.X509Certificate certificate, System.Security.Cryptography.X509Certificates.X509Chain chain,
                        System.Net.Security.SslPolicyErrors errors)
                {
                    return (true);
                };


                //StreamWriter stOut = new
                //StreamWriter(rq.GetRequestStream(),
                //System.Text.Encoding.ASCII);
                //stOut.Write(postData);
                //stOut.Close();


                HttpWebResponse rs = (HttpWebResponse)rq.GetResponse();
                StreamReader reader = new StreamReader(rs.GetResponseStream());
                string jsonText = reader.ReadToEnd();

                reader.Close();
                rs.Close();


                if (jsonText.StartsWith("{"))
                {
                    JsonParser parserPS = new JsonParser(new StringReader(jsonText), true);
                    JsonObject JsonResult = parserPS.ParseObject();
                    if (JsonResult != null && JsonResult.ContainsKey("studio"))
                    {
                        JsonObject jStudio = (JsonObject)JsonResult["studio"];
                        if (jStudio != null && jStudio.ContainsKey("launched"))
                        {
                            if (((JsonBoolean)jStudio["launched"]) == true)
                                return true;
                        }
                    }
                }





                return false;
            }
            catch (Exception exc)
            {
                //if there is an error, just assume its live.
                return true;
            }
        }

        private void pnlLaunchWarning_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            Flow.View.Preferences.PreferenceSectionsPanel pp = this.FlowController.ApplicationPanel.ShowPreferencesPanel();
            pp.FlowController = this.FlowController;
            pp.ShowOnlineOrderingPanel();
            return;
        }

        private void cmbShippingOption_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }


    }
}
