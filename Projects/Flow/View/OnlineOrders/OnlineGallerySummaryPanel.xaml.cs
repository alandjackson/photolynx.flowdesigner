﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Flow.Schema.LinqModel.DataContext;
using Flow.View.Project.Base;
using Flow.Lib.Helpers;
using Flow.Schema.LinqModel.DataContext.FlowMaster;
using Flow.Controller;
using Flow.Controller.Project;
using System.Diagnostics;
using Flow.View.Dialogs;
using Flow.Lib;
using System.ComponentModel;
using StructureMap;
using Flow.Controls;
using System.Windows.Threading;
using System.Runtime.InteropServices;
using NetServ.Net.Json;
using System.Threading;
using System.IO;
using System.Net;
using NLog;

namespace Flow.View
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class OnlineGallerySummaryPanel : ProjectPanelBase
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        private bool renderGreenScreen = false;
        private bool applyCrops = false;
        private bool useOriginalFileNames = false;

        private string saveOrderPath = null;

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        static extern EXECUTION_STATE SetThreadExecutionState(EXECUTION_STATE esFlags);

        [FlagsAttribute]
        public enum EXECUTION_STATE : uint
        {
            ES_AWAYMODE_REQUIRED = 0x00000040,
            ES_CONTINUOUS = 0x80000000,
            ES_DISPLAY_REQUIRED = 0x00000002,
            ES_SYSTEM_REQUIRED = 0x00000001
        }

        //no longer prevent the system from sleeping
        void AllowSleep()
        {
            SetThreadExecutionState(EXECUTION_STATE.ES_CONTINUOUS);
        }

        //prevents the system from sleeping untill the app closes or AllowSleep is called
        void PreventSleep()
        {
            // Prevent Idle-to-Sleep (monitor not affected)
            SetThreadExecutionState(EXECUTION_STATE.ES_CONTINUOUS | EXECUTION_STATE.ES_AWAYMODE_REQUIRED);
        }

        //just resets the idle clock, must keep calling this periodically to keep awake
        void KeepSystemAwake()
        {
            SetThreadExecutionState(EXECUTION_STATE.ES_SYSTEM_REQUIRED);
        }

        private NotificationProgressInfo _notificationInfo;
        private NotificationProgressInfo _notificationInfo2;


        /// /////////////////////////////////////////////////////////////////////////
        #region DEPENDENCY PROPERTIES
        /// /////////////////////////////////////////////////////////////////////////




        protected static DependencyProperty SelectedFlowProjectProperty = DependencyProperty.Register(
            "SelectedFlowProject", typeof(FlowProject), typeof(OnlineGallerySummaryPanel),
            new FrameworkPropertyMetadata(new PropertyChangedCallback(SelectedFlowProjectPropertyChanged))
        );

        public FlowProject SelectedFlowProject
        {
            get { return (FlowProject)this.GetValue(SelectedFlowProjectProperty); }
            set { this.SetValue(SelectedFlowProjectProperty, value); }
        }

        public static void SelectedFlowProjectPropertyChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            OnlineGallerySummaryPanel source = o as OnlineGallerySummaryPanel;

            if (source != null)
            {
                source.FlowProjectSelected = (e.NewValue != null);

            }
        }

        protected static DependencyProperty FlowProjectSelectedProperty =
            DependencyProperty.Register("FlowProjectSelected", typeof(bool), typeof(OnlineGallerySummaryPanel));

        public bool FlowProjectSelected
        {
            get { return (bool)this.GetValue(FlowProjectSelectedProperty); }
            set { this.SetValue(FlowProjectSelectedProperty, value); }
        }




        /// /////////////////////////////////////////////////////////////////////////
        #endregion DEPENDENCY PROPERTIES
        /// /////////////////////////////////////////////////////////////////////////

        public FlowProject DefaultProject { get; set; }

        private FlowProject RootProject { get; set; }

        public List<string> WatermarkLocations { get; set; }
        public List<string> HandlingRateTypes { get; set; }

        private string sortField1 = "";
        private string sortField2 = "";

        public OnlineGallerySummaryPanel()
        {
            //don init component until FlowController is set

            WatermarkLocations = new List<string>();
            WatermarkLocations.Add("top-left");
            WatermarkLocations.Add("top-center");
            WatermarkLocations.Add("top-right");
            WatermarkLocations.Add("middle-left");
            WatermarkLocations.Add("middle-center");
            WatermarkLocations.Add("middle-right");
            WatermarkLocations.Add("bottom-left");
            WatermarkLocations.Add("bottom-center");
            WatermarkLocations.Add("bottom-right");
            WatermarkLocations.Add("tiled-horizontal");
            WatermarkLocations.Add("tiled-diagonal");

            HandlingRateTypes = new List<string>();
            HandlingRateTypes.Add("percent");
            HandlingRateTypes.Add("fixed");
        }

        private void btnSelectSaveOrderExportPath_Click(object sender, RoutedEventArgs e)
        {
            logger.Info("Select Save Order Export Path button clicked");
            this.SaveOrderExportPath = DialogUtility.SelectFolder(Environment.SpecialFolder.Desktop, "Select destination folder.");

            this.ExportButtonEnabled = !String.IsNullOrEmpty(this.SaveOrderExportPath);
        }

        protected static DependencyProperty SaveOrderExportPathProperty =
            DependencyProperty.Register("SaveOrderExportPath", typeof(string), typeof(OnlineGallerySummaryPanel));

        public string SaveOrderExportPath
        {
            get { return (string)this.GetValue(SaveOrderExportPathProperty); }
            set { this.SetValue(SaveOrderExportPathProperty, value); }
        }


        protected static DependencyProperty ExportButtonEnabledProperty =
           DependencyProperty.Register("ExportButtonEnabled", typeof(bool), typeof(OnlineGallerySummaryPanel));

        public bool ExportButtonEnabled
        {
            get { return (bool)this.GetValue(ExportButtonEnabledProperty); }
            set { this.SetValue(ExportButtonEnabledProperty, value); }
        }
        protected override void OnSetFlowController()
        {
            base.OnSetFlowController();

            this.DataContext = this.ProjectController;
            
            //the pacement of this needs to be after DataContext is defined
            InitializeComponent();


            if (this.ProjectController.IsProjectLoaded)
            {
                initOnlineGallerySettings();
            }
            else
            {
                //ShowHideShippingOptions();
            }

            this.ProjectController.PropertyChanged += new PropertyChangedEventHandler(ProjectController_PropertyChanged);


            this.AllOrdersPanel.SetDataContext(this.ProjectController);
           
        }

        //private void ShowHideShippingOptions()
        //{
        //    cmbAllOrderOptions.Visibility = Visibility.Visible;
        //    cmbAllShippingOptions.Visibility = Visibility.Visible;
        //    cmbAllPackagingOptions.Visibility = Visibility.Visible;
        //    lblOrderOptions.Visibility = Visibility.Visible;
        //    lblShippingOptions.Visibility = Visibility.Visible;
        //    lblPackagingOptions.Visibility = Visibility.Visible;


        //    if (this.CurrentFlowProject == null || this.CurrentFlowProject.FlowCatalog == null)
        //    {
        //        cmbAllOrderOptions.Visibility = Visibility.Collapsed;
        //        cmbAllShippingOptions.Visibility = Visibility.Collapsed;
        //        cmbAllPackagingOptions.Visibility = Visibility.Collapsed;
        //        lblOrderOptions.Visibility = Visibility.Collapsed;
        //        lblShippingOptions.Visibility = Visibility.Collapsed;
        //        lblPackagingOptions.Visibility = Visibility.Collapsed;
        //    }
        //    else
        //    {
        //        if (this.CurrentFlowProject.FlowCatalog.FlowCatalogOrderOptionList.Count > 0)
        //            cmbAllOrderOptions.ItemsSource = this.CurrentFlowProject.FlowCatalog.FlowCatalogOrderOptionList;
        //        else
        //        {
        //            cmbAllOrderOptions.Visibility = Visibility.Collapsed;
        //            lblOrderOptions.Visibility = Visibility.Collapsed;
        //        }

        //        if (this.CurrentFlowProject.FlowCatalog.FlowCatalogShippingOptionList.Count == 0)
        //        {
        //            cmbAllShippingOptions.Visibility = Visibility.Collapsed;
        //            lblShippingOptions.Visibility = Visibility.Collapsed;
        //        }
        //        else
        //        {
        //            cmbAllShippingOptions.SelectedIndex = 0;
        //        }

        //        if (this.CurrentFlowProject.FlowCatalog.FlowCatalogPackagingOptionList.Count == 0)
        //        {
        //            cmbAllPackagingOptions.Visibility = Visibility.Collapsed;
        //            lblPackagingOptions.Visibility = Visibility.Collapsed;
        //        }


        //        SelectProperShippingMethod();
        //        SelectProperPackagingMethod();

        //    }
        //}


        //private void SelectProperShippingMethod()
        //{
        //    if (this.CurrentFlowProject.FlowProjectDataContext.OrderShippingOptions.Count() > 0)
        //    {
        //        IEnumerable<FlowCatalogOption> defaults = this.CurrentFlowProject.FlowCatalog.FlowCatalogShippingOptionList.Where(so => so.FlowCatalogOptionGuid == this.CurrentFlowProject.FlowProjectDataContext.OrderShippingOptions.First().FlowCatalogOptionGuid);
        //        if (defaults.Count() > 0)
        //        {
        //            cmbAllShippingOptions.SelectedItem = defaults.First();
        //        }

        //    }
        //}

        //private void SelectProperPackagingMethod()
        //{
        //    if (this.CurrentFlowProject.FlowProjectDataContext.OrderPackagingOptions.Count() > 0)
        //    {
        //        IEnumerable<FlowCatalogOption> defaults = this.CurrentFlowProject.FlowCatalog.FlowCatalogPackagingOptionList.Where(so => so.FlowCatalogOptionGuid == this.CurrentFlowProject.FlowProjectDataContext.OrderPackagingOptions.First().FlowCatalogOptionGuid);
        //        if (defaults.Count() > 0)
        //        {
        //            cmbAllPackagingOptions.SelectedItem = defaults.First();
        //        }

        //    }
        //}

        private void initOnlineGallerySettings()
        {
            //ShowHideShippingOptions();

            this.ProjectController.initOnlineGallerySettings();
            this.cmbFlowProjects.SelectedItem = this.ProjectController.CurrentFlowProject;
            if (this.ProjectController.CurrentFlowProjectGallery.GalleryCode != null)
            {
                this.lblGallerySite.Text = "https://vando.imagequix.com/g" + this.ProjectController.CurrentFlowProjectGallery.GalleryCode;
                this.Submit.Content = "Update Gallery";
            }
            else
            {
                this.lblGallerySite.Text = "Gallery Not Yet Uploaded";
            }

            this.ProjectController.UpdateCurrentFlowProjectGallery();

            //if (this.CurrentFlowProject.IsGreenScreen == true)
            //{
            //    lblRenderGreenScreen.Visibility = Visibility.Visible;
            //    chkRenderGreenScreen.Visibility = Visibility.Visible;

            //}
            //else
            //{
            //    lblRenderGreenScreen.Visibility = Visibility.Collapsed;
            //    chkRenderGreenScreen.Visibility = Visibility.Collapsed;
            //}

            updateExistingGallery();
        }






        public void setTabFocus(int tabIndex)
        {
            tabCtrl.SelectedIndex = tabIndex;
        }
        void ProjectController_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "CurrentFlowProject")
            {
                initOnlineGallerySettings();
            }
        }

        private void accountSettingsAlert_Loaded(object sender, RoutedEventArgs e)
        {
            // Show the alert if ImageQuix account information is missing
            if (!this.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.HasImageQuixCustomerCredentials())
            {
                this.accountSettingsAlert.Visibility = Visibility.Visible;
                this.accountExpander.IsExpanded = true;
            }
        }

        //private void ListBox1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    ListBox1.SelectedItem = null;
        //}

        private void ImageQuixGalleryOrderSummary_LoadOrdersInvoked(object sender, EventArgs<ProjectGallerySummary> e)
        {
            ProjectGallerySummary gallery = e.Data;
            
            //Load Project
            FlowProject fp = gallery.FlowProject;

            if(this.CurrentFlowProject == fp)
            {
                //start a worker thread with progress bar and key in each order
                PullOnlineOrder pullOrder = new PullOnlineOrder(this.FlowMasterDataContext);
                pullOrder.BeginDownloadOrders(fp, gallery, this.Dispatcher, this.FlowController);
            }
            else
            {
                this.FlowController.ProjectController.Load(fp);

                fp.FlowProjectDataContext.SubjectDataLoaded += delegate
                {
                    //start a worker thread with progress bar and key in each order
                    PullOnlineOrder pullOrder = new PullOnlineOrder(this.FlowMasterDataContext);
                    pullOrder.BeginDownloadOrders(fp, gallery, this.Dispatcher, this.FlowController);
                };
            }
            this.CurrentFlowProject.UpdateListOfOnlineOrders();
            
        }

        DateTime GetOrdersLastClicked = DateTime.Now;
        private void btnImportOrders_Click(object sender, RoutedEventArgs e)
        {
            logger.Info("Import Orders button clicked");

            if (DateTime.Now <= GetOrdersLastClicked.AddSeconds(3))
                return;

            GetOrdersLastClicked = DateTime.Now;
            this.FlowController.ApplicationPanel.IsEnabled = false;
            ProjectGallerySummary gallery = (ProjectGallerySummary)((sender as Button).DataContext);


            this.ImageQuixGalleryOrderSummary_LoadOrdersInvoked(this, new EventArgs<ProjectGallerySummary>(gallery));
            
        }




        private void GalleryURL_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            logger.Info("Gallery URL double clicked");
            LaunchURL((sender as TextBox).Text);
        }

        private void SubmitOnlineOrderData_Click(object sender, RoutedEventArgs e)
        {
            logger.Info("Submit Online Order Data button clicked");
            this.FlowController.ProjectController.SubmitOnlineOrderData(this.FlowController.ProjectController.CurrentFlowProject);
        }

        SubmitOnlineOrderDataNew SubmitOnlineOrderData { get; set; }
        private void Submit_Click(object sender, RoutedEventArgs e)
        {

           


            List<Subject> ReadySubjects = this.CurrentFlowProject.SubjectList.Where(s => s.Ready == true).ToList();
            List<Subject> LiveSubjects = this.CurrentFlowProject.SubjectList.Where(s =>  s.ImageList.Where(i => i.ExistsInOnlineGallery == true).Count() > 0).ToList();
            List<Subject> ApplicableSubjects = this.CurrentFlowProject.SubjectList.Where(s => s.Ready == true || s.ImageList.Where(i => i.ExistsInOnlineGallery == true).Count() > 0).ToList();
            if (this.UseReadySubjects.IsChecked == true && this.FlowMasterDataContext.PreferenceManager.Capture.UseReadyFlag == true)
            {
                FlowMessageBox msg1 = new FlowMessageBox("Ready Subjects Being Used", "Only Publishing Gallery for Existing Live Images and Ready Subjects:\n\n Ready Subjects: " + ReadySubjects.Count() + "\nLive Subjects: " + LiveSubjects.Count() + "\nReady OR Live Subjects: " + ApplicableSubjects.Count());
                if (msg1.ShowDialog() != true)
                    return;
            }
            else
            {
                ApplicableSubjects = this.CurrentFlowProject.SubjectList.ToList();
            }

            if (string.IsNullOrEmpty(this.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixCustomerID) ||
                (string.IsNullOrEmpty(this.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixCustomerPassword)))
            {
                FlowMessageBox msg = new FlowMessageBox("Missing ImageQuix Account Info", "Missing username and password in ImageQuix Account Info.");
                msg.CancelButtonVisible = false;
                msg.ShowDialog();
                return;
            }

            if (this.FlowController.ProjectController.CurrentFlowProject.FlowCatalog == null)
            {
                FlowMessageBox msg = new FlowMessageBox("No Catalog", "You can not submit an online gallery for a project\nthat has no Package Catalog\nSelect a Catalog in Capture -> Order Entry");
                msg.CancelButtonVisible = false;
                msg.ShowDialog();
                return;
            }
            if (this.CurrentFlowProject.IsGreenScreen == true)
            {
                int missingGSsettings = 0;
                foreach (SubjectImage si in this.CurrentFlowProject.FlowProjectDataContext.SubjectImages.Where(si=>si.ExistsInOnlineGallery==false))
                {
                    if (si.GreenScreenSettings == null)
                        missingGSsettings++;
                }

                if (missingGSsettings > 0)
                {
                    FlowMessageBox fmsg = new FlowMessageBox("Missing Green Screen Data", missingGSsettings + " of the images do not have any Green Screen Settings applied. This will result in photos with the Green not dropped out");
                    fmsg.buttonCancel.Content = "Cancel Upload";
                    fmsg.buttonOkay.Content = "Continue";
                    if (fmsg.ShowDialog() == false)
                        return;
                }
            }
            PreventSleep();
            this.FlowController.ApplicationPanel.IsEnabled = false;
            this.FlowController.ProjectController.IsUploading = true;
            this.FlowMasterDataContext.SavePreferences();
            //this.FlowMasterDataContext.SubmitChanges();
            FlowProject flowProject = this.SelectedFlowProject;
            _notificationInfo = new NotificationProgressInfo("Uploading To ImageQuix Gallery", "Preparing Upload...");


            

            

            _notificationInfo.Update("Preparing Upload...");

            SubmitOnlineOrderData = new SubmitOnlineOrderDataNew(flowProject, this.FlowMasterDataContext, this.ProjectController.CurrentFlowProjectGallery, ApplicableSubjects);
            SubmitOnlineOrderData.doCrop = false;
            if (chkDoCrop.IsChecked == true)
                SubmitOnlineOrderData.doCrop = true;

            SubmitOnlineOrderData.allowableYearbookSelections = Convert.ToInt32(cmbYearbookSelectionPoses.Text);

            //SubmitOnlineOrderData.WelcomeImage = txtWelcomeImage.Text;
            //SubmitOnlineOrderData.WelcomeMessage = txtWelcomeMessage.Text;
            //SubmitOnlineOrderData.GalleryName = txtGalleryName.Text;
            //SubmitOnlineOrderData.GalleryPassword = txtGalleryPassword.Text;
            //SubmitOnlineOrderData.SingleImagePagackes = (bool)chkOneImage.IsChecked;
            //string eventID = null;
            //if (this.FlowMasterDataContext.PreferenceManager.Application.ProjectIQEventIDs.Keys.Contains(this.ProjectController.CurrentFlowProject.FlowProjectGuid.ToString()))
            //   eventID = this.FlowMasterDataContext.PreferenceManager.Application.ProjectIQEventIDs[this.ProjectController.CurrentFlowProject.FlowProjectGuid.ToString()];
            //if (eventID != null)
            //{
            //    SubmitOnlineOrderData.EventID = eventID;
            //}
            if (chkDirectShipAfterDate.IsChecked == true)
                SubmitOnlineOrderData.SwitchShippingEnabled = true;
            
            SubmitOnlineOrderData.NotificationInfo = _notificationInfo;

            FlowBackgroundWorkerManager.RunWorker(UploadWorker, UploadWorker_Completed, true);

        }

        void UploadWorker(object sender, DoWorkEventArgs e)
        {

            if (this.CurrentFlowProject.Gallery.YearbookPoses > 0 && this.CurrentFlowProject.Gallery.LastModifyDate != null && !string.IsNullOrEmpty(this.CurrentFlowProject.Gallery.GalleryCode))
            {
                //_notificationInfo.Update("Checking for online orders...");
                this.CurrentFlowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
               {
                   //CheckForOnlineOrders();
                   CheckForYearbookPoses();
               }));

                ProjectGallerySummary gallery = FlowMasterDataContext.LiveFlowProjectGalleries.Where(s => s.FlowProject.FlowProjectGuid == this.CurrentFlowProject.FlowProjectGuid).FirstOrDefault();

                if (gallery != null && (gallery.YearbookPoses > 0))
                {
                    //_notificationInfo.Update("Loading existing orders and yearbook selections...");
                    this.ImageQuixGalleryOrderSummary_LoadOrdersInvoked(this, new EventArgs<ProjectGallerySummary>(gallery));
                    Thread.Sleep(2000);
                    while (this.FlowController.PullingOrders)
                    {
                        Thread.Sleep(500);
                    }
                    this.CurrentFlowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                       {
                            this.FlowController.ApplicationPanel.IsEnabled = false;
                       }));
                }
            }

            IShowsNotificationProgress progressNotification = ObjectFactory.GetInstance<IShowsNotificationProgress>();
            _notificationInfo.NotificationProgress = progressNotification;
            progressNotification.ShowNotification(_notificationInfo);

            int waitCount = 0;
            string dots = ".";
            while (this.CurrentFlowProject.initingSubjects)
            {
                dots += ".";
                _notificationInfo.Update("Waiting for subjects to initialize." + dots);
                waitCount++;
                Thread.Sleep(3000);

                if (waitCount == (4 * 60)) //if its taken longer than 4 minutes, bail
                    this.CurrentFlowProject.initingSubjects = false;

            }

            if (SubmitOnlineOrderData.Begin())
            {
                //return true, if a new gallery was just published, it now needs to be updated

                this.CurrentFlowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                {
                    
                    this.Submit.Content = "Update Gallery";

                    string custID = this.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixCustomerID;
                    if (!string.IsNullOrEmpty(this.ProjectController.CurrentFlowProjectGallery.GalleryCode))
                        this.lblGallerySite.Text = "https://vando.imagequix.com/g" + this.ProjectController.CurrentFlowProjectGallery.GalleryCode;
                    else
                    {
                        this.lblGallerySite.Text = "https://vando.imagequix.com/gallery.html?id=" + custID + "&eventid=" + this.ProjectController.CurrentFlowProjectGallery.EventID;
                    }
                    this.ProjectController.CurrentFlowProjectGallery.GalleryURL = this.lblGallerySite.Text;
                    
                }));
                //SubmitOnlineOrderData.Begin();
            }
            
        }

        void UploadWorker_Completed(object sender, RunWorkerCompletedEventArgs e)
        {
            //string eventID = SubmitOnlineOrderData.EventID;

            this.Submit.Content = "Update Gallery";

            string custID = this.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixCustomerID;
            if (!string.IsNullOrEmpty(this.ProjectController.CurrentFlowProjectGallery.GalleryCode))
                this.lblGallerySite.Text = "https://vando.imagequix.com/g" + this.ProjectController.CurrentFlowProjectGallery.GalleryCode;
            else
            {
                this.lblGallerySite.Text = "https://vando.imagequix.com/gallery.html?id=" + custID + "&eventid=" + this.ProjectController.CurrentFlowProjectGallery.EventID;
            }
            this.ProjectController.CurrentFlowProjectGallery.GalleryURL = this.lblGallerySite.Text;
            
            ////if the event list already has an event for this project, then update it, else add a new one
            //if (this.FlowMasterDataContext.PreferenceManager.Application.ProjectIQEventIDs.Keys.Contains(this.ProjectController.CurrentFlowProject.FlowProjectGuid.ToString()))
            //    this.FlowMasterDataContext.PreferenceManager.Application.ProjectIQEventIDs[this.ProjectController.CurrentFlowProject.FlowProjectGuid.ToString()] = eventID;
            //else
            //    this.FlowMasterDataContext.PreferenceManager.Application.ProjectIQEventIDs.Add(this.ProjectController.CurrentFlowProject.FlowProjectGuid.ToString(), eventID);


            this.FlowMasterDataContext.SavePreferences();
            //this.FlowMasterDataContext.SubmitChanges();


            this.FlowController.ApplicationPanel.IsEnabled = true;
            this.FlowController.ProjectController.IsUploading = false;
            AllowSleep();

            this.FlowMasterDataContext.RefreshGalleryList();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            logger.Info("Cancel button clicked");
            this.FlowController.Project();
        }

        private void btnWelcomeImage_Click(object sender, RoutedEventArgs e)
        {
            logger.Info("Welcome Image File Select button clicked");
            try
            {
                string selectedFilePath = DialogUtility.SelectFile(
                    "Select Welcome Image File",
                    this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Layout.GraphicsDirectory,
                    "JPG File (*.jpg)|*.jpg"
                );

                if (!String.IsNullOrEmpty(selectedFilePath))
                {
                    this.FlowController.ProjectController.FlowMasterDataContext
                        .PreferenceManager.Upload.OnlineOrder.WelcomeImagePath = selectedFilePath;
                    this.ProjectController.CurrentFlowProjectGallery.WelcomeImage = selectedFilePath;
                    txtWelcomeImage.Text = selectedFilePath;
                    this.FlowMasterDataContext.SavePreferences();
                    //this.FlowMasterDataContext.SubmitChanges();
                    //this.ProjectController.UpdateCurrentProjectGallery();
                }
            }
            catch (Exception err)
            {
                logger.ErrorException("Exception occurred", err);
            }
        }

        private void _this_Loaded(object sender, RoutedEventArgs e)
        {


        }



        public void CheckForYearbookPoses()
        {
            this.FlowMasterDataContext.GenerateFlowGalleryList();

            //this.FlowController.ApplicationPanel.IsEnabled = false;

            //IShowsNotificationProgress progressNotification = ObjectFactory.GetInstance<IShowsNotificationProgress>();
            //_notificationInfo2 = new NotificationProgressInfo("Checking for yearbook poses", "Checking for yearbook poses");
            //_notificationInfo2.NotificationProgress = progressNotification;
            //progressNotification.ShowNotification(_notificationInfo2);

           // FlowBackgroundWorkerManager.RunWorker(delegate
           // {
                List<IQOrder> allOrders = new List<IQOrder>();


                this.FlowMasterDataContext.OnlineGalleryList.Clear();


                string customerID = this.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixCustomerID;
                string custPw = this.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixCustomerPassword;
                
                string baseURL = this.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixPublishURL;

                if (customerID != null && customerID.Length > 0)
                {
                    foreach (ProjectGallerySummary gallerySummary in FlowMasterDataContext.LiveFlowProjectGalleries)
                    {
                        if (gallerySummary == null)
                            continue;

                        List<string> allYearbookPoses1 = new List<string>();
                        List<string> allYearbookPoses2 = new List<string>();
                        //
                        //
                        //
                        //check for yearbook pose selecion
                        //
                        //
                        //
                        if (gallerySummary.YearbookPoses > 0)
                        {
                            string publisherUri = baseURL + "/customer/" + customerID;
                            string getSubjectsURL = publisherUri + "/" + "gallery/" + gallerySummary.EventID;
                            //string ybCheckUrl = baseURL + "/customer/" + customerID + "/order";

                            logger.Info("yb pose check: " + getSubjectsURL);
                            logger.Info(customerID + " / " + custPw);

                            PullOnlineOrder pullyb = new PullOnlineOrder(this.FlowMasterDataContext);
                            string rootJson = this.GetJson(getSubjectsURL, customerID, custPw,"");

                            logger.Info("did yb pose check");

                            if (rootJson != null)
                            {
                                JsonParser parser3 = new JsonParser(new StringReader(rootJson), true);
                                JsonObject subjectData = parser3.ParseObject();
                                JsonArray existingSubjectJson = (JsonArray)subjectData["subjects"];
                                foreach (JsonObject subject in existingSubjectJson)
                                {
                                    if (subject.ContainsKey("yearbookSelection1") && subject["yearbookSelection1"] != JsonNull.Null && !string.IsNullOrEmpty((subject["yearbookSelection1"] as JsonString).ToString()))
                                    {
                                        string imageName1 = (subject["yearbookSelection1"] as JsonString).Value;
                                        if (!gallerySummary.knownYB1ImageNames.Contains(imageName1))
                                            allYearbookPoses1.Add(imageName1);
                                    }
                                    if (subject.ContainsKey("yearbookSelection2") && subject["yearbookSelection2"] != JsonNull.Null && !string.IsNullOrEmpty((subject["yearbookSelection2"] as JsonString).ToString()))
                                    {
                                        string imageName2 = (subject["yearbookSelection2"] as JsonString).Value;
                                        if (!gallerySummary.knownYB2ImageNames.Contains(imageName2))
                                            allYearbookPoses2.Add(imageName2);
                                    }
                                }
                            }

                            gallerySummary.YBPoseList1 = allYearbookPoses1;
                            gallerySummary.YBPoseList2 = allYearbookPoses2;
                            gallerySummary.YearbookPoseCount = allYearbookPoses1.Count() + allYearbookPoses2.Count();
                        }
                      
                    }
                }

           // },
           // delegate
           // {
                //this.FlowController.ApplicationPanel.IsEnabled = true;
                //this.FlowMasterDataContext.RefreshGalleryList();
                //_notificationInfo2.Complete("Done Checking for all yearbook poses");
                //_notificationInfo2.Dispose();


            //});
        }


        public void CheckForOnlineOrders()
        {
            this.FlowMasterDataContext.GenerateFlowGalleryList();

            this.FlowController.ApplicationPanel.IsEnabled = false;

            IShowsNotificationProgress progressNotification = ObjectFactory.GetInstance<IShowsNotificationProgress>();
            _notificationInfo2 = new NotificationProgressInfo("Checking for online orders", "Checking for online orders");
            _notificationInfo2.NotificationProgress = progressNotification;
            progressNotification.ShowNotification(_notificationInfo2);

            FlowBackgroundWorkerManager.RunWorker(delegate{


            CheckForYearbookPoses();

            List<IQOrder> allOrders = new List<IQOrder>();
           

            this.FlowMasterDataContext.OnlineGalleryList.Clear();


            string customerID = this.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixCustomerID;
            string baseURL = this.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixPublishURL;

            if (customerID != null && customerID.Length > 0)
            {
                foreach (ProjectGallerySummary gallerySummary in FlowMasterDataContext.LiveFlowProjectGalleries)
                {
                    if (gallerySummary == null)
                        continue;

                   

                    string LastGalleryCheck = this.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.LastGalleryCheck.ToString("yyyyMMdd");
                    //get the order for this customer
                    string url = baseURL + "/customer/" + customerID + "/order";

                    //if we want, we can set the LastGalleryCheck, so that next time we only check for recent orders.
                    //this.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.LastGalleryCheck = DateTime.Now;


                    PullOnlineOrder pull = new PullOnlineOrder(this.FlowMasterDataContext);
                    JsonArray orders = pull.GetGalleryStats(url, gallerySummary.EventID);
                    if (orders != null)
                    {
                        foreach (JsonObject order in orders)
                        {
                            int orderId = Convert.ToInt32(((JsonNumber)order["id"]).Value);
                            string status = "";
                            if ((order["orderStatus"].JsonTypeCode != JsonTypeCode.Null))
                                status = ((JsonString)order["orderStatus"]).Value;
                            
                            if (status.ToLower().StartsWith("new"))
                            {
                                if (!gallerySummary.knownOrderIDs.Contains(orderId))
                                {
                                    IQOrder ord = new IQOrder(orderId, gallerySummary.EventID);
                                    allOrders.Add(ord);
                                }
                            }
                        }
                    }

                    //update the info for the online gallery stuff
                    //foreach (ProjectGallery gallery in this.FlowMasterDataContext.FlowProjectGalleries)
                    //{
                    List<IQOrder> eventOrders = allOrders.Where(o => o.EventID == gallerySummary.EventID).ToList();

                        // lets get info for this gallery
                    gallerySummary.newOrders = eventOrders;
                    gallerySummary.NewOnlineOrders = eventOrders.Count();

                    //if (gallerySummary.GalleryName == null && this.FlowMasterDataContext.FlowProjects.Any(fp => fp.FlowProjectGuid == gallerySummary.FlowProjectGuid))
                    //    {
                    //        gallerySummary.GalleryName = this.FlowMasterDataContext.FlowProjects.Where(fp => fp.FlowProjectGuid == gallerySummary.FlowProjectGuid).First().FlowProjectName;
                    //        this.FlowMasterDataContext.SubmitChanges();
                    //    }
                        //this.FlowMasterDataContext.OnlineGalleryList.Add(gallery);

                    //}
                }
            }
           
            }, 
            delegate {
                this.FlowController.ApplicationPanel.IsEnabled = true;
                this.FlowMasterDataContext.RefreshGalleryList();
                _notificationInfo2.Complete("Done Checking for all online orders");
                _notificationInfo2.Dispose();
                
               
            });
            
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            logger.Info("Save button clicked");

            this.FlowMasterDataContext.SavePreferences();
            //this.FlowMasterDataContext.SubmitChanges();

            // Hide the account settings alert if settings have been entered
            if (this.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.HasImageQuixCustomerCredentials())
            {
                this.accountSettingsAlert.Visibility = Visibility.Collapsed;
                this.accountExpander.IsExpanded = false;
            }
            else
            {
                this.accountSettingsAlert.Visibility = Visibility.Visible;
                this.accountExpander.IsExpanded = true;
            }
        }

        //private void ShowOrderOptions_Click(object sender, RoutedEventArgs e)
        //{
        //    panelSubmitOrdersSettings.Visibility = Visibility.Visible;
        //    ShowOrderOptions.IsEnabled = false;
        //}

        //private void SubmitNewOrder_Click(object sender, RoutedEventArgs e)
        //{
        //    logger.Info("Submit New Order button clicked");
            
        //    bool hasBulkOrders = false;
        //    Organization orgBulkShipping = null;

        //    if (rdoSaveOrder.IsChecked == true)
        //        saveOrderPath = txtSaveExportDestination.Text;
        //    else
        //        saveOrderPath = this.FlowMasterDataContext.PreferenceManager.Application.LabOrderQueueDirectory;

        //    if (chkRenderGreenScreen.IsChecked == true)
        //        renderGreenScreen = true;
        //    else
        //        renderGreenScreen = false;

        //    if (chkApplyCrops.IsChecked == true)
        //        applyCrops = true;
        //    else
        //        applyCrops = false;

        //    if (chkOriginalFileName.IsChecked == true)
        //        useOriginalFileNames = true;
        //    else
        //        useOriginalFileNames = false;
        //    bool canContinue = false;
        //    while (canContinue == false)
        //    {
        //        LabOrderFormDialog dlg = new LabOrderFormDialog(this.CurrentFlowProject.FlowProjectDataContext);

        //        if (dlg.ShowDialog() == true)
        //        {
        //            bool foundMissingValue = false;
        //            foreach (OrderFormFieldLocal field in this.CurrentFlowProject.FlowProjectDataContext.OrderFormFieldLocalList)
        //            {
        //                if (field.IsRequired == true && string.IsNullOrEmpty(field.SelectedValue))
        //                {
        //                    FlowMessageBox msg = new FlowMessageBox("Complete the Order Form", "Please Complete the required fields in the order form before you submit your order");
        //                    msg.CancelButtonVisible = false;
        //                    msg.ShowDialog();

        //                    foundMissingValue = true;
        //                    break;
        //                }
        //            }

        //            canContinue = !foundMissingValue;
        //        }
        //        else
        //        {
        //            return;
        //        }
        //    }

        //    if (this.CurrentFlowProject.NewOnlineOrders.Any(o => o.ShipmentType == "pickup"))
        //        hasBulkOrders = true;
        //    if (hasBulkOrders)
        //    {
        //        if (cmbAllShippingMethods.SelectedItem == null)
        //        {
        //            FlowMessageBox msg = new FlowMessageBox("Please Select Shipping", "Please select a shipping method");
        //            msg.CancelButtonVisible = false;
        //            msg.ShowDialog();
        //            return;
        //        }

        //        if (((KeyValuePair<int, string>)cmbAllShippingMethods.SelectedItem).Value == "Ship To School")
        //        {
        //            orgBulkShipping = this.CurrentFlowProject.Organization;
        //        }

        //        if (((KeyValuePair<int, string>)cmbAllShippingMethods.SelectedItem).Value == "Ship To Studio")
        //        {
        //            orgBulkShipping = this.CurrentFlowProject.Studio;
        //        }

        //        if ((orgBulkShipping.OrganizationShippingAddressLine1 == null && orgBulkShipping.OrganizationShippingAddressLine2 == null) ||
        //            orgBulkShipping.OrganizationShippingCity == null ||
        //            orgBulkShipping.OrganizationShippingStateCode == null ||
        //            orgBulkShipping.OrganizationShippingZipCode == null)
        //        {
        //            FlowMessageBox msg = new FlowMessageBox("Missing Shipping Info", "There is no shipping information for the selected shipping type (" + orgBulkShipping.OrganizationName + ")");
        //            msg.CancelButtonVisible = false;
        //            msg.ShowDialog();
        //            return;
        //        }
        //    }


        //    //panelSubmitOrdersSettings.Visibility = Visibility.Collapsed;
        //    //ShowOrderOptions.IsEnabled = true;
        //    this.FlowController.ApplicationPanel.IsEnabled = false;
        //    this.FlowController.ProjectController.IsUploading = true;
        //    SubmitOnlineOrders(hasBulkOrders, orgBulkShipping);
        //}



        //private void CancelSubmit_Click(object sender, RoutedEventArgs e)
        //{
        //    panelSubmitOrdersSettings.Visibility = Visibility.Collapsed;
        //    //ShowOrderOptions.IsEnabled = true;
        //}

        

        //private void rdoSubmitOrder_Checked(object sender, RoutedEventArgs e)
        //{

        //}




        //private void SubmitOnlineOrders(bool hasBulkOrders, Organization orgForBulkShipping)
        //{
        //    List<List<object>> allOrders = new List<List<object>>();

        //    if (hasBulkOrders)
        //    {
        //        List<OrderPackage> BulkOrderPackages = this.CurrentFlowProject.NewOnlineOrders.Where(o => o.ShipmentType == "pickup").ToList();
        //        List<object> order = new List<object>();
        //        order.Add(BulkOrderPackages);
        //        order.Add(orgForBulkShipping);
        //        allOrders.Add(order);
        //    }

        //    List<OrderPackage> AllDirectOrderPackages = this.CurrentFlowProject.NewOnlineOrders.Where(o => o.ShipmentType == "direct").ToList();
        //    if (AllDirectOrderPackages != null && AllDirectOrderPackages.Count > 0)
        //    {
        //        List<int?> UniqueIQOrderIDs = AllDirectOrderPackages.Select(op => op.IQOrderID).Distinct().ToList();

        //        foreach (int IQOrderID in UniqueIQOrderIDs)
        //        {
        //            List<OrderPackage> thisDirectOrderPackages = this.CurrentFlowProject.NewOnlineOrders.Where(o => o.ShipmentType == "direct" && o.IQOrderID == IQOrderID).ToList();
        //            List<object> order = new List<object>();
        //            order.Add(thisDirectOrderPackages);
        //            order.Add(null);
        //            allOrders.Add(order);
        //        }
        //    }

        //    PreventSleep();
        //    sortField1 = SubmitOrderSortControl.SortField1;
        //    sortField2 = SubmitOrderSortControl.SortField2;
        //    FlowBackgroundWorkerManager.RunWorker(SendOrderWorker, SendOrderWorker_Completed, allOrders);



        //}


        //void SendOrderWorker(object sender, DoWorkEventArgs e)
        //{

        //    List<List<object>> allOrders = e.Argument as List<List<object>>;

        //    IShowsNotificationProgress progressNotification = ObjectFactory.GetInstance<IShowsNotificationProgress>();
        //    _notificationInfo = new NotificationProgressInfo("Uploading Orders To Lab", "Processing All Orders...", 0, allOrders.Count, 1);
        //    _notificationInfo.NotificationProgress = progressNotification;
        //    progressNotification.ShowNotification(_notificationInfo);

        //    //wait to init subjects
        //    int waitCount = 0;
        //    string dots = ".";
        //    while (this.CurrentFlowProject.initingSubjects)
        //    {
        //        dots += ".";
        //        _notificationInfo.Update("Waiting for subjects to initialize." + dots);
        //        waitCount++;
        //        Thread.Sleep(3000);

        //        if (waitCount == (4 * 60)) //if its taken longer than 4 minutes, bail
        //            this.CurrentFlowProject.initingSubjects = false;

        //    }
        //    _notificationInfo.Update("Processing All Orders...", 0, allOrders.Count, 1, true);

        //    foreach (List<object> order in allOrders)
        //    {
        //        _notificationInfo.Step();
        //        List<OrderPackage> CurrentOrderPackages = order[0] as List<OrderPackage>;
        //        Organization org = order[1] as Organization;
        //        string orderID = PlaceOrder(CurrentOrderPackages, org);
        //        if (string.IsNullOrEmpty(orderID))
        //            continue;
        //        CurrentFlowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
        //        {
        //            //mark the order as sent
        //            foreach (OrderPackage op in CurrentOrderPackages)
        //            {
        //                op.LabOrderID = Convert.ToInt32(orderID);
        //                op.ModifyDate = DateTime.Now;
        //                op.LabOrderDate = DateTime.Now;
        //                op.SubjectOrder.Subject.FlagForMerge = true;
        //            }
        //            CurrentFlowProject.FlowProjectDataContext.SubmitChanges();
        //            this.CurrentFlowProject.UpdateListOfOnlineOrders();
        //        }));
        //        Thread.Sleep(2000);

        //    }
        //}

        //void SendOrderWorker_Completed(object sender, RunWorkerCompletedEventArgs e)
        //{
        //    //_notificationInfo.Complete("Done Processing All Orders");
        //    if (saveOrderPath == this.FlowMasterDataContext.PreferenceManager.Application.LabOrderQueueDirectory)
        //        _notificationInfo.Complete("Done processing. Added to Upload Queue.");
        //    else
        //        _notificationInfo.Complete("Done processing: " + saveOrderPath);
        //    this.FlowController.ApplicationPanel.IsEnabled = true;
        //    this.FlowController.ProjectController.IsUploading = false;

        //    foreach (OrderFormFieldLocal field in this.CurrentFlowProject.FlowProjectDataContext.OrderFormFieldLocalList)
        //    {
        //        field.SelectedValue = field.FieldValue;
        //    }
        //    this.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();
        //    AllowSleep();
        //}

        //private string PlaceOrder(List<OrderPackage> CurrentOrderPackages, Organization orgForBulkShipping)
        //{

        //    SendLabOrder SendLabOrder = new SendLabOrder(this.CurrentFlowProject, this.FlowController);
        //    SendLabOrder.SortField1 = sortField1;
        //    SendLabOrder.SortField2 = sortField2;
        //    SendLabOrder.RenderGreenScreen = renderGreenScreen;
        //    SendLabOrder.ApplyCrop = applyCrops;
        //    SendLabOrder.UseOriginalFileNames = useOriginalFileNames;
        //    SendLabOrder.SaveOrderPath = saveOrderPath;
        //    SendLabOrder.OrderFilter = "New Online Orders";

        //    return SendLabOrder.SendOrder(CurrentOrderPackages, orgForBulkShipping);
        //}

        private void LaunchURL(string url)
        {
            if (url.ToLower().Contains("vando"))
            {
                if ((url != null && url != ""))
                {
                    ProcessStartInfo procInfo = new ProcessStartInfo(url);
                    Process.Start(procInfo);
                }
                else
                {
                    // MessageBox.Show("Bade URL");
                }
            }
        }

        private void lblGallerySite_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            LaunchURL((sender as TextBox).Text);
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            ProjectGallerySummary fpg = (sender as Button).DataContext as ProjectGallerySummary;
            this.CurrentFlowProject.FlowProjectDataContext.ProjectGalleries.DeleteOnSubmit(fpg.FlowProject.Gallery);
            this.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();
            initOnlineGallerySettings();
            this.FlowMasterDataContext.RefreshGalleryList();
        }
        private void btnLoad_Click(object sender, RoutedEventArgs e)
        {
            ProjectGallerySummary fpg = (sender as Button).DataContext as ProjectGallerySummary;
            if (fpg.FlowProject.FlowProjectGuid != null && FlowMasterDataContext.FlowProjects.Any(fp => fp.FlowProjectGuid == fpg.FlowProject.FlowProjectGuid))
            {
                this.ProjectController.Load((Guid)fpg.FlowProject.FlowProjectGuid);
                setTabFocus(1);
                this.FlowController.ApplicationPanel.uxCapture.IsEnabled = true;
                this.FlowController.ApplicationPanel.uxEdit.IsEnabled = true;
                this.FlowController.ApplicationPanel.uxReports.IsEnabled = true;
            }
            else
            {
                //show message, cant open project
                FlowMessageBox msg = new FlowMessageBox("Can't Load Project", "This Project has been removed from flow and can't be loaded");
                msg.CancelButtonVisible = false;
                msg.ShowDialog();
            }
       
        }

        private void btnCheckForOrders_Click(object sender, RoutedEventArgs e)
        {
            
            CheckForOnlineOrders();
        }

        private void btnDeleteOrder_Click(object sender, RoutedEventArgs e)
        {
            FlowMessageBox msg = new FlowMessageBox("Remove Online Order", "You are about to remove an Order from New Orders.\n\nThe Order will show up as a new order on the server and \nwill be imported the next time you get orders from the online gallery");
            if ((bool)msg.ShowDialog())
            {
                OrderPackage op = ((sender as Button).DataContext) as OrderPackage;
                //delete it
                this.FlowMasterDataContext.PulledIQOrders.DeleteAllOnSubmit(this.FlowMasterDataContext.PulledIQOrders.Where(pio=>pio.IQOrderID == op.IQOrderID));
                foreach (OrderPackage thisPackage in this.CurrentFlowProject.FlowProjectDataContext.OrderPackages.Where(fop => fop.IQOrderID == op.IQOrderID))
                    thisPackage.SubjectOrder.Subject.RemoveOrderPackage(thisPackage);
                this.FlowMasterDataContext.SubmitChanges();
                this.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();
                this.CurrentFlowProject.UpdateListOfOnlineOrders();

                string custId = this.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixCustomerID;
                string custPw = this.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixCustomerPassword;
                string orderStatusURL = this.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixPublishURL + "/customer/" + custId + "/order/" + op.IQOrderID + "/status/New";
                PutToUri(orderStatusURL, custId, custPw);
            }
        }

        private void rdoDirect_Checked(object sender, RoutedEventArgs e)
        {
            //this.ProjectController.CurrentFlowProjectGallery.DirectShip = true;
            //this.ProjectController.CurrentFlowProjectGallery.PickupShip = !true;
            //this.ProjectController.CurrentFlowProjectGallery.CustomerChoiceShip = !true;
        }

        private void rdoDirect_Unchecked(object sender, RoutedEventArgs e)
        {

        }

        private void rdoChoice_Checked(object sender, RoutedEventArgs e)
        {
            //this.ProjectController.CurrentFlowProjectGallery.DirectShip = !true;
            //this.ProjectController.CurrentFlowProjectGallery.PickupShip = !true;
            //this.ProjectController.CurrentFlowProjectGallery.CustomerChoiceShip = true;
        }

        private void rdoChoice_Unchecked(object sender, RoutedEventArgs e)
        {

        }

        private void rdoPickup_Checked(object sender, RoutedEventArgs e)
        {
            //this.ProjectController.CurrentFlowProjectGallery.DirectShip = !true;
            //this.ProjectController.CurrentFlowProjectGallery.PickupShip = true;
            //this.ProjectController.CurrentFlowProjectGallery.CustomerChoiceShip = !true;
        }

        private void rdoPickup_Unchecked(object sender, RoutedEventArgs e)
        {
            
        }

        private void btnDoneOrder_Click(object sender, RoutedEventArgs e)
        {
            //FlowMessageBox msg = new FlowMessageBox("Mark as Done", "You are about to mark this order as done.\n\nThe order will show up in the Order History page.");
            //if ((bool)msg.ShowDialog())
            //{
                OrderPackage op = ((sender as Button).DataContext) as OrderPackage;
                //delete it
                //this.FlowMasterDataContext.PulledIQOrders.DeleteAllOnSubmit(this.FlowMasterDataContext.PulledIQOrders.Where(pio => pio.IQOrderID == op.IQOrderID));
                foreach (OrderPackage thisPackage in this.CurrentFlowProject.FlowProjectDataContext.OrderPackages.Where(fop => fop.IQOrderID == op.IQOrderID))
                {
                    thisPackage.LabOrderDate = DateTime.Now;
                    thisPackage.ModifyDate = DateTime.Now;
                    thisPackage.LabOrderID = 0;
                }
                this.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();
                this.CurrentFlowProject.UpdateListOfOnlineOrders();


           // }
        }


        private HttpStatusCode PutToUri(string loginUri, string un, string pw)
        {
            logger.Info("s " + loginUri);
            HttpWebRequest rq = (HttpWebRequest)WebRequest.Create(loginUri);
            rq.Credentials = new NetworkCredential(un, pw);
            rq.Headers.Add("x-iq-token", "A15M876TJ8");
            rq.Headers.Add("x-iq-user", un);
            rq.Timeout = 10000;//only try for 10 seconds
            rq.Method = "PUT";
            HttpWebResponse rs;
            try
            {
                rs = (HttpWebResponse)rq.GetResponse();
            }
            catch (WebException e)
            {

                return HttpStatusCode.Unauthorized;
            }

            HttpStatusCode code = rs.StatusCode;
            rs.Close();
            return code;

        }

        private void btnNewOrder_Click(object sender, RoutedEventArgs e)
        {
            //FlowMessageBox msg = new FlowMessageBox("Mark as New", "You are about to mark this order as New.\n\nThe order will show up in the New Order page.");
            //if ((bool)msg.ShowDialog())
            //{
                OrderPackage op = ((sender as Button).DataContext) as OrderPackage;
                //delete it
                //this.FlowMasterDataContext.PulledIQOrders.DeleteAllOnSubmit(this.FlowMasterDataContext.PulledIQOrders.Where(pio => pio.IQOrderID == op.IQOrderID));
                foreach (OrderPackage thisPackage in this.CurrentFlowProject.FlowProjectDataContext.OrderPackages.Where(fop => fop.IQOrderID == op.IQOrderID))
                {
                    thisPackage.LabOrderDate = null;
                    thisPackage.LabOrderID = null;
                    thisPackage.ModifyDate = DateTime.Now;
                }
                this.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();
                this.CurrentFlowProject.UpdateListOfOnlineOrders();
            //}
        }

        private void btnUnMarkLiveImage_Click(object sender, RoutedEventArgs e)
        {
            foreach (SubjectImage si in this.CurrentFlowProject.FlowProjectDataContext.SubjectImages.Where(i => i.ExistsInOnlineGallery == true))
            {
                si.ExistsInOnlineGallery = false;
                si.Subject.FlagForMerge = true;
            }
            this.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();
            this.CurrentFlowProject.UpdateGalleryCounts();         

        }

        private void updateExistingGallery()
        {
            //ProjectGallery _ProjectGallery = (sender as TabItem).DataContext as ProjectGallery;
            ProjectGallery _ProjectGallery = this.CurrentFlowProject.FlowProjectDataContext.ProjectGalleries.First(a => a.FlowProjectGuid == this.CurrentFlowProject.FlowProjectGuid);
            bool isNewGallery = true;
            JsonObject existingGalleryJson = null;

            if (_ProjectGallery != null && _ProjectGallery.EventID != null)
                isNewGallery = false;

            string mainUri = this.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixLoginURL;
            string custId = this.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixCustomerID;
            string custPw = this.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixCustomerPassword;

            string getGalleryURL = mainUri + "/" + custId + "/" + "event/" + _ProjectGallery.EventID + "?filter=IQ";
            string getPSURL = mainUri + "/" + custId + "/" + "event/" + _ProjectGallery.EventID + "/pricesheet?filter=IQ";

            //lets get the existing gallery json
            if (isNewGallery)
            {
                existingGalleryJson = null;
                //existingPSJson = null;
            }
            else
            {
                string JsonResponse = GetJson(getGalleryURL, custId, custPw, "");
                if (JsonResponse == null)
                    existingGalleryJson = null;
                else
                {
                    //int loc = JsonResponse.IndexOf("eventType");
                    //JsonResponse = JsonResponse.Remove(loc, "eventType".Length).Insert(loc, "eventType2");
                    JsonParser parser = new JsonParser(new StringReader(JsonResponse), true);
                    //NetServ.Net.Json.JsonParser.TokenType nextToken = parser.NextToken();
                    //NetServ.Net.Json.JsonParser.TokenType nextToken2 = parser.NextToken();
                    existingGalleryJson = parser.ParseObject();
                }

                if (existingGalleryJson != null)
                {

                    _ProjectGallery.GalleryName = existingGalleryJson["title"].ToString();
                    _ProjectGallery.WelcomeText = existingGalleryJson["welcomeMessage"].ToString();
                    // _ProjectGallery.WelcomeImage = existingGalleryJson["welcomeImage"].ToString().Split("/").Last();
                    _ProjectGallery.EventDate = DateTime.ParseExact(existingGalleryJson["eventDate"].ToString(), "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture);
                    if (existingGalleryJson["expirationDate"] == JsonNull.Null)
                        _ProjectGallery.ExpirationDate = null;
                    else
                        _ProjectGallery.ExpirationDate = DateTime.ParseExact(existingGalleryJson["expirationDate"].ToString(), "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture);
                    _ProjectGallery.Keyword = existingGalleryJson["keyword"].ToString();

                    //config
                    _ProjectGallery.MinOrderAmount = Convert.ToDecimal(((existingGalleryJson["galleryConfig"] as JsonObject)["min-order-amount"] as JsonNumber).Value);
                    _ProjectGallery.ShippingLabel = (existingGalleryJson["galleryConfig"] as JsonObject)["shipping-label"].ToString();
                    _ProjectGallery.HandlingLabel = (existingGalleryJson["galleryConfig"] as JsonObject)["handling-label"].ToString();
                    _ProjectGallery.HandlingRateType = (existingGalleryJson["galleryConfig"] as JsonObject)["handling-rate-type"].ToString();
                    _ProjectGallery.TaxLabel = (existingGalleryJson["galleryConfig"] as JsonObject)["tax-label"].ToString();

                    _ProjectGallery.TaxAllOrders = ((existingGalleryJson["galleryConfig"] as JsonObject)["tax-all-orders"] as JsonBoolean).Value;
                    _ProjectGallery.OrderThankyouMessage = (existingGalleryJson["galleryConfig"] as JsonObject)["order-thankyou-message"].ToString();
                    _ProjectGallery.DisableCropping = ((existingGalleryJson["galleryConfig"] as JsonObject)["disable-cropping"] as JsonBoolean).Value;
                    _ProjectGallery.DefaultShowImageName = ((existingGalleryJson["galleryConfig"] as JsonObject)["default-show-image-names"] as JsonBoolean).Value;

                    if ((existingGalleryJson["galleryConfig"] as JsonObject)["shipmentType"].ToString() == "pickup")
                    {
                        _ProjectGallery.PickupShip = true;
                        _ProjectGallery.DirectShip = false;
                        _ProjectGallery.CustomerChoiceShip = false;
                    }
                    else if ((existingGalleryJson["galleryConfig"] as JsonObject)["shipmentType"].ToString() == "direct")
                    {
                        _ProjectGallery.PickupShip = false;
                        _ProjectGallery.DirectShip = true;
                        _ProjectGallery.CustomerChoiceShip = false;
                    }
                    else
                    {
                        _ProjectGallery.PickupShip = false;
                        _ProjectGallery.DirectShip = false;
                        _ProjectGallery.CustomerChoiceShip = true;
                    }
                    if ((existingGalleryJson["galleryConfig"] as JsonObject)["shipStartDate"] != JsonNull.Null)
                    {
                        _ProjectGallery.SwitchShippingDate = DateTime.ParseExact((existingGalleryJson["galleryConfig"] as JsonObject)["shipStartDate"].ToString(), "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture);
                        _ProjectGallery.SwitchShipping = true;
                    }
                    else
                    {
                        _ProjectGallery.SwitchShippingDate = null;
                        _ProjectGallery.SwitchShipping = false;
                    }
                    _ProjectGallery.PickupText = (existingGalleryJson["galleryConfig"] as JsonObject)["pickupLabel"].ToString();
                    //_ProjectGallery.ShippingCost = Convert.ToDecimal(((existingGalleryJson["galleryConfig"] as JsonObject)["shipping"] as JsonNumber).Value);
                    //_ProjectGallery.HandlingCost = Convert.ToDecimal(((existingGalleryJson["galleryConfig"] as JsonObject)["handling"] as JsonNumber).Value);
                    //_ProjectGallery.TaxRate = Convert.ToDecimal(((existingGalleryJson["galleryConfig"] as JsonObject)["tax"] as JsonNumber).Value);

                    if (((existingGalleryJson["galleryConfig"] as JsonObject)["shipping"] as JsonNumber).Value > 0)
                        _ProjectGallery.ShippingCost = Convert.ToDecimal(((existingGalleryJson["galleryConfig"] as JsonObject)["shipping"] as JsonNumber).Value);
                    else
                        _ProjectGallery.ShippingCost = 0;

                    if (((existingGalleryJson["galleryConfig"] as JsonObject)["handling"] as JsonNumber).Value > 0)
                        if (_ProjectGallery.HandlingRateType == "percent")
                        {
                            _ProjectGallery.HandlingCost = Convert.ToDecimal(((existingGalleryJson["galleryConfig"] as JsonObject)["handling"] as JsonNumber).Value) * 100;
                            if (_ProjectGallery.HandlingCost > 60)
                                _ProjectGallery.HandlingCost = _ProjectGallery.HandlingCost / 100;
                        }
                        else
                            _ProjectGallery.HandlingCost = Convert.ToDecimal(((existingGalleryJson["galleryConfig"] as JsonObject)["handling"] as JsonNumber).Value);
                    else
                        _ProjectGallery.HandlingCost = 0;

                    if (((existingGalleryJson["galleryConfig"] as JsonObject)["tax"] as JsonNumber).Value > 0)
                    {
                        _ProjectGallery.TaxRate = Convert.ToDecimal(((existingGalleryJson["galleryConfig"] as JsonObject)["tax"] as JsonNumber).Value) * 100;
                        if (_ProjectGallery.TaxRate > 60)
                        {
                            _ProjectGallery.TaxRate = _ProjectGallery.TaxRate / 100;
                        }
                    }
                    else
                        _ProjectGallery.TaxRate = 0;

                    _ProjectGallery.TaxShipping = ((existingGalleryJson["galleryConfig"] as JsonObject)["tax-shipping"] as JsonBoolean).Value;

                    _ProjectGallery.ShowBWSepia = ((existingGalleryJson["galleryConfig"] as JsonObject)["show-bw-sepia"] as JsonBoolean).Value;

                    _ProjectGallery.Theme = (existingGalleryJson["galleryConfig"] as JsonObject)["theme"].ToString();
                    _ProjectGallery.Wallpaper = (existingGalleryJson["galleryConfig"] as JsonObject)["wallpaper"].ToString();

                    //end config

                    _ProjectGallery.WatermarkText = existingGalleryJson["watermark"].ToString();
                    _ProjectGallery.WatermarkLocation = existingGalleryJson["watermark-position"].ToString();

                    _ProjectGallery.YearbookPoses = Convert.ToInt32((existingGalleryJson["num-yearbook-selection"] as JsonNumber).ToString());

                    this.FlowMasterDataContext.SubmitChanges();
                }
            }
        }

        private string GetJson(string uri, string custId, string custPw, string postData)
        {
            logger.Info("t " + uri);

            HttpWebRequest rq = (HttpWebRequest)WebRequest.Create(uri);
            rq.Credentials = new NetworkCredential(custId, custPw);
            rq.Headers.Add("x-iq-token", "A15M876TJ8");
            rq.Headers.Add("x-iq-user", custId);
            rq.Timeout = (15000);//try for 15 seconds
            rq.Method = "GET";

            HttpWebResponse rs = null;
            int tryCount = 0;
            while (tryCount < 5)
            {
                try
                {
                    rs = (HttpWebResponse)rq.GetResponse();

                    StreamReader reader = new StreamReader(rs.GetResponseStream());
                    string jsonText = reader.ReadToEnd();

                    reader.Close();
                    rs.Close();
                    return jsonText;

                }
                catch (WebException e)
                {

                    tryCount++;

                    //if (tryCount == 5)
                    //    throw e;
                }
            }

           
            return null;
        }

        private void txtIQCustomerPW_Loaded(object sender, RoutedEventArgs e)
        {
            this.txtIQCustomerPW.Password = this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixCustomerPassword;
        }

        private void txtIQCustomerPW_PasswordChanged(object sender, RoutedEventArgs e)
        {
            this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixCustomerPassword = this.txtIQCustomerPW.Password;
        }

        private void UseReadySubjects_Checked(object sender, RoutedEventArgs e)
        {
            
        }

        private void cmbAllOrderOptions_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void cmbAllOrderOptions_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void chkUseOption_Checked(object sender, RoutedEventArgs e)
        {
            FlowCatalogOption co = (FlowCatalogOption)((CheckBox)e.Source).DataContext;
            updateFlowCatalogOptionIsUsed(co, true);
        }

        private void updateFlowCatalogOptionIsUsed(FlowCatalogOption co, bool isChecked)
        {
            if (isChecked)
            {
                //if it was false, its about to be true
                OrderOrderOption ooo = new OrderOrderOption();
                ooo.FlowCatalogOptionGuid = (Guid)co.FlowCatalogOptionGuid;
                ooo.ResourceURL = co.ImageQuixCatalogOption.ResourceURL;
                this.ProjectController.CurrentFlowProject.FlowProjectDataContext.OrderOrderOptions.InsertOnSubmit(ooo);


            }
            else
            {
                //if it was true, its about to be false
                this.ProjectController.CurrentFlowProject.FlowProjectDataContext.OrderOrderOptions.DeleteOnSubmit(
                   this.ProjectController.CurrentFlowProject.FlowProjectDataContext.OrderOrderOptions.First(ooo => ooo.FlowCatalogOptionGuid == co.FlowCatalogOptionGuid)
               );
            }

            this.ProjectController.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();
            //this.cmbAllOrderOptions.SelectedIndex = -1;
        }

        private void chkUseOption_Unchecked(object sender, RoutedEventArgs e)
        {
            FlowCatalogOption co = (FlowCatalogOption)((CheckBox)e.Source).DataContext;
            updateFlowCatalogOptionIsUsed(co, false);
        }

        private void cmbAllOrderOptions_DropDownOpened(object sender, EventArgs e)
        {

        }



        private void cmbAllShippingOptions_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.CurrentFlowProject == null || this.CurrentFlowProject.FlowProjectDataContext == null ||
                this.CurrentFlowProject.FlowProjectDataContext.OrderShippingOptions == null || ((ComboBox)e.Source) == null)
                return;

            foreach (OrderShippingOption oso in this.CurrentFlowProject.FlowProjectDataContext.OrderShippingOptions)
                this.CurrentFlowProject.FlowProjectDataContext.OrderShippingOptions.DeleteOnSubmit(oso);

            FlowCatalogOption co = null;
            if ((FlowCatalogOption)((ComboBox)e.Source).SelectedItem == null)
            {
                if (CurrentFlowProject.FlowCatalog != null && CurrentFlowProject.FlowCatalog.FlowCatalogShippingOptionList != null)
                    co = CurrentFlowProject.FlowCatalog.FlowCatalogShippingOptionList[0];
            }
            else
                co = (FlowCatalogOption)((ComboBox)e.Source).SelectedItem;
            if (co != null)
            {
                OrderShippingOption so = new OrderShippingOption();
                so.FlowCatalogOptionGuid = (Guid)co.FlowCatalogOptionGuid;
                so.ResourceURL = co.ImageQuixCatalogOption.ResourceURL;
                this.CurrentFlowProject.FlowProjectDataContext.OrderShippingOptions.InsertOnSubmit(so);

                this.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();
            }
        }

        private void cmbAllPackagingOptions_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.CurrentFlowProject == null || this.CurrentFlowProject.FlowProjectDataContext == null ||
                this.CurrentFlowProject.FlowProjectDataContext.OrderPackagingOptions == null || ((ComboBox)e.Source) == null)
                return;

            foreach (OrderPackagingOption opo in this.CurrentFlowProject.FlowProjectDataContext.OrderPackagingOptions)
                this.CurrentFlowProject.FlowProjectDataContext.OrderPackagingOptions.DeleteOnSubmit(opo);

            FlowCatalogOption co = (FlowCatalogOption)((ComboBox)e.Source).SelectedItem;

            if (co != null)
            {
                OrderPackagingOption po = new OrderPackagingOption();
                po.FlowCatalogOptionGuid = (Guid)co.FlowCatalogOptionGuid;
                po.ResourceURL = co.ImageQuixCatalogOption.ResourceURL;
                this.ProjectController.CurrentFlowProject.FlowProjectDataContext.OrderPackagingOptions.InsertOnSubmit(po);

                this.ProjectController.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();
            }
        }

        //private void cmbAllShippingMethods_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    this.CurrentFlowProject.ShippingMethod = cmbAllShippingMethods.SelectedIndex;
        //    this.CurrentFlowProject.FlowMasterDataContext.SubmitChanges();
        //}

        private void chkUseOption_Click(object sender, RoutedEventArgs e)
        {
            logger.Info("Use Option checkbox clicked: isChecked={0}", ((CheckBox)e.Source).IsChecked);
            FlowCatalogOption co = (FlowCatalogOption)((CheckBox)e.Source).DataContext;
        }

        

        private void tabCtrl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.Source is TabControl) //if this event fired from TabControl then enter
            {
                if (Orders.IsSelected)
                {
                    //Do your job here
                    CurrentFlowProject.FlowProjectDataContext.UpdateFilteredOrderPackages();
                    CurrentFlowProject.FlowProjectDataContext.UpdateOrderFilterValues();
                }
            }
        }
    }
}
