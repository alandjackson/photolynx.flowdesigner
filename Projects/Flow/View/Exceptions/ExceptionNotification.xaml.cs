﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Flow.Lib.Net;
using Flow.Lib.Exceptions;

namespace Flow.View.Exceptions
{
    /// <summary>
    /// Interaction logic for ExceptionNotification.xaml
    /// </summary>
    public partial class ExceptionNotification : Window
    {
        const int MAX_ERROR_ID = 1000000;
        const double EXPANDED_HEIGHT = 645;
        const double COLLAPSED_HEIGHT = 545;

        Exception _exception;
        int _errorID;

        public int ErrorID
        {
            get { return _errorID; }
        }

        public bool SendSS
        {
            get { return checkBoxSS.IsChecked.Value; }
        }

        public bool SendProject
        {
            get { return checkBoxUploadProject.IsChecked.Value; }
        }

        public bool SendReport
        {
            get { return checkBoxSendReport.IsChecked.Value; }
        }

        public string AdditionalInfo
        {
            get { return textBoxDescription.Text; }
        }

        public ExceptionNotification(Exception e)
        {
            InitializeComponent();

            Random rand = new Random();
            _errorID = Convert.ToInt32(Math.Floor(rand.NextDouble() * MAX_ERROR_ID));

            textBoxErrorID.Text = _errorID.ToString();

            //Uri iconUri = new Uri(@"..\..\Resources\flow_icon.png", UriKind.RelativeOrAbsolute);
            //this.Icon = BitmapFrame.Create(iconUri);
            
            checkBoxSendReport.IsChecked = true;
            checkBoxSS.IsChecked = true;
            checkBoxUploadProject.IsChecked = false;

            _exception = e;

            PopulateInfo();

            this.Loaded += new RoutedEventHandler(ExceptionNotification_Loaded);
            checkBoxSendReport.Click += new RoutedEventHandler(checkBoxSendReport_Click);

            this.Title = String.Format("Flow Error Reporting - ID {0}", ErrorID);

            this.MinHeight = COLLAPSED_HEIGHT;
            this.Width = this.MinWidth;

            expanderDetails.Expanded += new RoutedEventHandler(expanderDetails_Expanded);
            expanderDetails.Collapsed += new RoutedEventHandler(expanderDetails_Collapsed);
        }

        void expanderDetails_Collapsed(object sender, RoutedEventArgs e)
        {
            this.MinHeight = COLLAPSED_HEIGHT;
            this.Height = this.MinHeight;
        }

        void expanderDetails_Expanded(object sender, RoutedEventArgs e)
        {
            this.MinHeight = EXPANDED_HEIGHT;
        }

        void checkBoxSendReport_Click(object sender, RoutedEventArgs e)
        {
            bool isChecked = (sender as CheckBox).IsChecked.Value;

            textBoxDescription.IsEnabled = isChecked;
            labelDescription.IsEnabled = isChecked;

            checkBoxSS.IsChecked = isChecked;
            checkBoxSS.IsEnabled = isChecked;

            checkBoxUploadProject.IsEnabled = isChecked;
        }

        void ExceptionNotification_Loaded(object sender, RoutedEventArgs e)
        {
            textBoxDescription.Focus();
        }

        private void PopulateInfo()
        {
            string message;
            if( _exception is EmailException)
            {
                message = (_exception as EmailException).EmailEventArgs.Error.Message;
            }
            else
            {
                message = _exception.Message;
            }

            string info = String.Format(
                "A problem happened while running: {0} and will be stopped.\n\n" + 
                "{1}\n\n" +
                "Select [OK] to pass to technical support immediately.\n" +
                "Select [Cancel] if you think that this is a settings or preferences related issue that you can solve yourself.\n\n" +
		"If you need assistance with this problem, allow the message to be sent and take note of the Error ID {2} as it will be referred to by the support personnel.\n" +
                "* If there is no internet connection at the time of the error it will be saved and sent as soon as a connection is available.",
                _exception.TargetSite.Name, message, _errorID);

            textBoxInfo.Text = info;

            string excemptionDetails = _exception.ToString();
            
            if(_exception.InnerException != null)
            {
                excemptionDetails += "\n\n\nInnerException:" +  _exception.InnerException.ToString();
                    if(_exception.InnerException.InnerException != null)
                    {
                        excemptionDetails += "\n\n\nInnerException:" +  _exception.InnerException.InnerException.ToString();
                        if(_exception.InnerException.InnerException.InnerException != null)
                        {
                            excemptionDetails += "\n\n\nInnerException:" +  _exception.InnerException.InnerException.InnerException.ToString();
                        }
                    }

            }

            textBoxDetails.Text = excemptionDetails;

        }

        private void buttonOkay_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = SendReport;
        }

        private void buttonCancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }
}
