﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Controls.ProgressNotificationControl;
using Flow.Lib;
using Flow.View.Catalog;
using Flow.View.Edit;
using Flow.View.Edit.ImageAdjustments;
using Flow.View.Preferences;
using Flow.View.Project;
using Flow.View.Reports;

using Microsoft.Win32;
using System.IO;
using StructureMap;
using NLog;
using Flow.Schema.LinqModel.DataContext;
using Flow.View.Dialogs;

namespace Flow.View
{
    /// <summary>
    /// Interaction logic for ApplicationPanel.xaml
    /// </summary>
    public partial class ApplicationPanel : UserControl, IShowsNotificationProgress
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public Flow.Controller.FlowController FlowController { get; set; } 
        protected List<Button> HeaderButtons = new List<Button>();
        protected MultipleSubPanelManager SubPanelManager { get; set; }

        public ApplicationPanel()
        {
            InitializeComponent();
            HeaderButtons.Add(uxTest);
            HeaderButtons.Add(uxProject);
            HeaderButtons.Add(uxEdit);
            HeaderButtons.Add(uxCapture);
            HeaderButtons.Add(uxLayout);
			HeaderButtons.Add(uxCatalog);
			HeaderButtons.Add(uxReports);
            HeaderButtons.Add(uxPreferences);

            SubPanelManager = new MultipleSubPanelManager(HeaderButtons, uxMainChild);
            SubPanelManager.PanelShown += new EventHandler<Flow.Lib.Helpers.EventArgs<UIElement>>(SubPanelManager_PanelShown);

            FilterPanel.btnCloseFilter.Click += new RoutedEventHandler(btnCloseFilter_Click);
            SearchPanel.btnCloseSearch.Click += new RoutedEventHandler(btnCloseSearch_Click);
            txtFlowVersion.Text = "Version: " + FlowContext.GetFlowVersion() + " - Click for Support";


            
            
		}

        void btnCloseFilter_Click(object sender, RoutedEventArgs e)
        {
            FilterPanel.Visibility = Visibility.Collapsed;
        }

        void btnCloseSearch_Click(object sender, RoutedEventArgs e)
        {
            SearchPanel.Visibility = Visibility.Collapsed;
        }


        public void RefreshSubPanelManager()
        {
            //this.SubPanelManager.ClearChildPanels();
            //SubPanelManager = new MultipleSubPanelManager(HeaderButtons, uxMainChild);
            //SubPanelManager.PanelShown += new EventHandler<Flow.Lib.Helpers.EventArgs<UIElement>>(SubPanelManager_PanelShown);

        }
        void SubPanelManager_PanelShown(object sender, Flow.Lib.Helpers.EventArgs<UIElement> e)
        {
            // If no flow project is loaded, disable the Capture and Edit buttons;
            // a better solution than this should be implemented, time permitting
            if (
				this.FlowController.ProjectController.CurrentFlowProject == null ||
				this.FlowController.ProjectController.CurrentFlowProject.IsNew
			)
            {
                uxCapture.IsEnabled = false;
                uxEdit.IsEnabled = false;
                uxReports.IsEnabled = false;
            }

            UpdateBrandedLogo();
        }

        public void UpdateBrandedLogo()
        {
            string graphicsDir = this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Layout.GraphicsDirectory;
            if (File.Exists(graphicsDir + "/flowlogo.png"))
            {
                FileStream stream = new FileStream(graphicsDir + "/flowlogo.png", FileMode.Open, FileAccess.Read);
                BitmapImage src = new BitmapImage();
                src.BeginInit();
                src.StreamSource = stream;
                src.EndInit();
                CachedBitmap csrc = new CachedBitmap(src, BitmapCreateOptions.None, BitmapCacheOption.OnLoad);
                ImgLabLogo.Source = csrc;
                stream.Close();
                
            }
        }

        private void uxTest_Click(object sender, RoutedEventArgs e)
        {
            FlowController.Test();
        }
        private void uxTest2_Click(object sender, RoutedEventArgs e)
        {
            this.ShowNotification(new NotificationProgressInfo("Title", "Message"));
        }
        private void uxProject_Click(object sender, RoutedEventArgs e)
        {
            logger.Info("Project button clicked in main navigation bar");
			FlowController.Project();
		}
        private void uxCapture_Click(object sender, RoutedEventArgs e)
        {
            logger.Info("Capture button clicked in main navigation bar");
            FlowController.Capture();
        }
        private void uxEdit_Click(object sender, RoutedEventArgs e)
        {
            logger.Info("Edit button clicked in main navigation bar");
            FlowController.Edit();
        }
        private void uxLayout_Click(object sender, RoutedEventArgs e)
        {
            logger.Info("Layout button clicked in main navigation bar");
            FlowController.Layout();
        }

		private void uxCatalog_Click(object sender, RoutedEventArgs e)
		{
            logger.Info("Catalog button clicked in main navigation bar");
			FlowController.Catalog();
		}

		private void uxReports_Click(object sender, RoutedEventArgs e)
		{
            logger.Info("Reports button clicked in main navigation bar");
			FlowController.Reports();
		}

        private void uxPreferences_Click(object sender, RoutedEventArgs e)
        {
            logger.Info("Preferences button clicked in main navigation bar");
            FlowController.Preferences();
        }
        
        private void uxSupportURL_Click(object sender, RoutedEventArgs e)
        {
            logger.Info("Support URL button clicked on Flow logo");
            this.LaunchSupportURL();
        }

        public void ShowTestPanel()
        {
            SubPanelManager.ShowPanel<TestPanel>(uxTest);
        }

        public MainProjectPanel ShowMainProjectPanel()
		{
            MainProjectPanel pnl = SubPanelManager.ShowPanel<MainProjectPanel>(uxProject);
            if(pnl.FlowController == null)
                pnl.FlowController = this.FlowController;
            return pnl;
		}

        public void ShowFlowServerPanel()
		{
            MainProjectPanel pnl = SubPanelManager.ShowPanel<MainProjectPanel>(uxProject);
            if(pnl.FlowController == null)
                pnl.FlowController = this.FlowController;
            FlowServerPanel fsp = pnl.ShowFlowServerPanel();
            fsp.IncomingList.UpdateLayout();
		}
        


		public CaptureDockPanel ShowCaptureDockPanel()
		{
            return SubPanelManager.ShowPanel<CaptureDockPanel>(uxCapture);
		}

		public EditDockPanel ShowEditDockPanel()
        {
            return SubPanelManager.ShowPanel<EditDockPanel>(uxEdit);
        }

		public DataImportPanel ShowDataImportPanel()
		{
            return SubPanelManager.ShowPanel<DataImportPanel>(uxEdit);
		}

		public ImageAdjustmentsPanel ShowImageAdjustmentsPanel()
		{
            return SubPanelManager.ShowPanel<ImageAdjustmentsPanel>(uxEdit);
		}

        public LayoutPanel ShowLayoutPanel()
        {
            return SubPanelManager.ShowPanel<LayoutPanel>(uxLayout);
        }

		public CatalogDockPanel ShowFlowCatalogEditor()
		{
            return SubPanelManager.ShowPanel<CatalogDockPanel>(uxCatalog);
		}

		public ReportsDockPanel ShowReports()
		{
			return SubPanelManager.ShowPanel<ReportsDockPanel>(uxReports);
		}

		public ImageQuixCatalogPanel ShowImageQuixCatalogPanel()
		{
			return SubPanelManager.ShowPanel<ImageQuixCatalogPanel>(uxCatalog);
		}

		public ProductPackageEditor ShowProductPackageEditor()
		{
            return SubPanelManager.ShowPanel<ProductPackageEditor>(uxCatalog);
		}

        public PreferenceSectionsPanel ShowPreferencesPanel()
        {
            return SubPanelManager.ShowPanel<PreferenceSectionsPanel>(uxPreferences);
        }

        Dictionary<NotificationProgressInfo, ProgressNotificationControlData> progressMap
            = new Dictionary<NotificationProgressInfo, ProgressNotificationControlData>();


		/// <summary>
		/// 
		/// </summary>
		/// <param name="info"></param>
		public void ShowNotification(NotificationProgressInfo info)
        {
            ProgressNotificationControlData notification1 = new ProgressNotificationControlData();
            progressMap[info] = notification1;
            notification1.StatusTextTop = info.Title;
            notification1.StatusTextBottom = info.Message;

			notification1.AllowCancel = info.AllowCancel;

            if (info.Progress != -1)
                notification1.ProgressBarValue = info.Progress;

            notification1.ControlContainer = uxProgNotification;

            uxProgNotification.InvokeIfRequired(() =>
            {
                uxProgNotification.CreateNotification(notification1);
                uxProgNotification.Visibility = Visibility.Visible;

                uxProgNotification.SetValue(Grid.ZIndexProperty, 100);
            });

            logger.Info("BEGIN {0}", info.Title);
        }


		/// <summary>
		/// 
		/// </summary>
		/// <param name="info"></param>
		public void UpdateNotification(NotificationProgressInfo info)
        {
            if (!progressMap.ContainsKey(info))
                ShowNotification(info);

            ProgressNotificationControlData notification1 = progressMap[info];

			if (notification1.IsCanceled)
			{
				notification1.IsCanceled = false;
				info.Cancel();
			}
			else
			{
				notification1.StatusTextTop = info.Title;
				notification1.StatusTextBottom = info.Message;
				notification1.ProgressBarValue = info.Progress;
				notification1.IsComplete = info.IsComplete;
                notification1.DoneButtonOn = info.DoneButtonOn;

				//if (info.Progress == -1)
				//{
				//    notification1.ProgressBarVisible = false;
				//}

				uxProgNotification.InvokeIfRequired(() =>
				{
					uxProgNotification.UpdateNotification(notification1);
				});

                if (info.IsComplete)
                {
                    logger.Info("DONE {0}", info.Title);
                }
			}
        }

        private void LaunchSupportURL()
        {
            //first check for a flow.chm file
            string chmFile = System.IO.Path.Combine(FlowContext.FlowAppDataDirPath, "Help", "flow.chm");
            if (File.Exists(chmFile))
            {
                System.Windows.Forms.Help.ShowHelp(new System.Windows.Forms.Control(), chmFile);
                return;
            }

            String supportURL = this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.SupportURL;
            if ((supportURL != null && supportURL != ""))
            {
                logger.Info("Launching support URL '{0}'", supportURL);
                ProcessStartInfo procInfo = new ProcessStartInfo(supportURL);
                Process.Start(procInfo);
            }
            else
            {
                logger.Warn("Not launching support URL because one has not been specified in Preferences->Application");
               // MessageBox.Show("A Support URL has not been specified in Preferences->Application");
            }
        }

        private void EditProjectPreferences_Click(object sender, RoutedEventArgs e)
        {
            logger.Info("Clicked Edit Project Preferences button");

            this.ShowMainProjectPanel();
            this.FlowController.ProjectController.EditProjectPreferences(this.FlowController.ProjectController.CurrentFlowProject);
        }



        private void ArchiveProject_Click(object sender, RoutedEventArgs e)
        {
            logger.Info("Clicked Archive Project button");

            this.ShowMainProjectPanel();
            this.FlowController.ProjectController.ExportProject(this.FlowController.ProjectController.CurrentFlowProject, false);
        }

        private void SubmitOrders_Click(object sender, RoutedEventArgs e)
        {
            logger.Info("Clicked Submit Orders button");

            this.ShowMainProjectPanel();
            this.FlowController.ProjectController.SubmitOrders(this.FlowController.ProjectController.CurrentFlowProject);
        }

        private void UploadProject_Click(object sender, RoutedEventArgs e)
        {
            logger.Info("Clicked Upload Project button");

            this.ShowMainProjectPanel();
            this.FlowController.ProjectController.ExportProject(this.FlowController.ProjectController.CurrentFlowProject, true);
        }

        private void SubmitOnlineOrderData_Click(object sender, RoutedEventArgs e)
        {
            logger.Info("Clicked Submit Online Order Data button");

            this.btnOnlineGalleryAlert.Visibility = Visibility.Collapsed;

            this.ShowMainProjectPanel();
            this.FlowController.ProjectController.SubmitOnlineOrderData(this.FlowController.ProjectController.CurrentFlowProject);
        }



        private void RemoteImageService_Click(object sender, RoutedEventArgs e)
        {
            logger.Info("Clicked Remote Image Service button");

            //this.btnOnlineGalleryAlert.Visibility = Visibility.Collapsed;

            this.ShowMainProjectPanel();
            this.FlowController.ProjectController.ViewRemoteImageServiceStatus(this.FlowController.ProjectController.CurrentFlowProject);
            this.FlowController.ProjectController.CurrentFlowProject.UpdateRemoteImageServiceLists();
        }

        private void btnDeleteProject_Click(object sender, RoutedEventArgs e)
        {
            logger.Info("Clicked Delete Project button");

            this.ShowMainProjectPanel();
            this.FlowController.ProjectController.DeleteProject(this.FlowController.ProjectController.CurrentFlowProject, "Do You Really Want To Delete This Project?", true, false);
        }

        private void btnArchiveProject_Click(object sender, RoutedEventArgs e)
        {
            logger.Info("Clicked Archive Project button");

            this.ShowMainProjectPanel();
            this.FlowController.ProjectController.ArchiveDeleteProject(this.FlowController.ProjectController.CurrentFlowProject);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            logger.Info("Clicked Save Project button");

            //this.FlowController.ProjectController.UpdateNetworkParent();

            if (this.FlowController.ProjectController.CurrentFlowProject.FlowProjectDataContext.UpdateNetworkProject)
            {
                this.FlowController.ProjectController.CurrentFlowProject.FlowProjectDataContext.UpdateNetworkProject = false;
                this.FlowController.ProjectController.UpdateNetworkParent();
                NotificationProgressInfo.QuickStatus("Project Saved", "Project Saved");
            }
            else
            {
                NotificationProgressInfo.QuickStatus("Save Project", "Done, no new changes detected");
            }
           
        }
        DateTime lastF7 = DateTime.Now;
        private void UserControl_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.F7)
            {
                if (this.FlowController.ProjectController.CurrentFlowProject == null)
                    return;
                //save project settings
                //this.FlowController.ProjectController.UpdateNetworkParent();

                //if F7 was pressed within 3 seconds, open SearchAllProject
                if (lastF7 >= DateTime.Now.AddSeconds(-3))
                {
                    this.FlowController.Project();
                    this.FlowController.ProjectController.OpenSearchAllProjects();
                    return;
                }

                if (this.FlowController.ProjectController.CurrentFlowProject.FlowProjectDataContext.UpdateNetworkProject)
                {
                    this.FlowController.ProjectController.CurrentFlowProject.FlowProjectDataContext.UpdateNetworkProject = false;
                    this.FlowController.ProjectController.UpdateNetworkParent();
                    NotificationProgressInfo.QuickStatus("Project Saved", "Project Saved");
                }
                else
                {

                    NotificationProgressInfo.QuickStatus("Save Project", "Done, no new changes detected");

                }

                lastF7 = DateTime.Now;
            }

            
            //(e.KeyCode == Keys.V && e.Modifiers == Keys.Control)
            //if (((e.Key == Key.F3) || (e.Key == Key.System && e.SystemKey == Key.F3)) && (Keyboard.Modifiers & ModifierKeys.Shift) == ModifierKeys.Shift)
            //if(e.Key == Key.F3 && Keyboard.Modifiers == ModifierKeys.Control)
            //{
            //    this.FlowController.ProjectController.OpenSearchAllProjects();
            //}
        }

        private void Uploading_Button_Click(object sender, RoutedEventArgs e)
        {
            this.ShowMainProjectPanel();
            this.FlowController.ProjectController.MainProjectPanel.ShowLabUploadStatusPanel();
           
        }

        private void btnHoldItems_Click(object sender, RoutedEventArgs e)
        {
            if (cmbHoldOptions.IsDropDownOpen == true)
                cmbHoldOptions.IsDropDownOpen = false;
            else
                cmbHoldOptions.IsDropDownOpen = true;
        }

        private void cmbHoldOptions_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cmbHoldOptions.SelectedValue != null)
            {
                if (((ComboBoxItem)cmbHoldOptions.SelectedValue).Content.Equals("Clear Hold Images"))
                    this.FlowController.ProjectController.CurrentFlowProject.FlowProjectDataContext.ClearHoldImageList();
                   
                cmbHoldOptions.SelectedIndex = -1;
            }
        }

        private void btnOnlineGalleryAlert_Loaded(object sender, RoutedEventArgs e)
        {
            // Show this alert if ImageQuix account information is missing
            //if (this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixCustomerID == null ||
            //    this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixCustomerID == "" ||
            //    this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixCustomerPassword == null ||
            //    this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixCustomerPassword == "")
            //{
            //    this.btnOnlineGalleryAlert.Visibility = Visibility.Visible;
            //}
        }

        private void lbxHoldImages_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                this.FlowController.ProjectController.GotoTicketID(((SubjectImage)e.AddedItems[0]).Subject.TicketCode);
            }
            
        }

        private void btnFilter_Click(object sender, RoutedEventArgs e)
        {
            if (FilterPanel.Visibility == Visibility.Visible)
            {
                FilterPanel.Visibility = Visibility.Collapsed;
            }
            else
            {
                FilterPanel.DataContext = this.FlowController;
                FilterPanel.Visibility = Visibility.Visible;
                SearchPanel.Visibility = Visibility.Collapsed;
            }
        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            if (SearchPanel.Visibility == Visibility.Visible)
            {
                SearchPanel.Visibility = Visibility.Collapsed;
            }
            else
            {
                SearchPanel.DataContext = this.FlowController;
                SearchPanel.Visibility = Visibility.Visible;
                FilterPanel.Visibility = Visibility.Collapsed;
            }
        }

        private void btnImportData_Click(object sender, RoutedEventArgs e)
        {
            this.FlowController.DataImport();
        }

        private void SubmitOnlineOrders_Click(object sender, RoutedEventArgs e)
        {
            this.btnOnlineGalleryAlert.Visibility = Visibility.Collapsed;
            logger.Info("Clicked Submit Online Order Data button");
            this.ShowMainProjectPanel();

            //if no settings are in place, take them to the preferencepanel
            if (String.IsNullOrEmpty(this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.EcommerceAPIURL) &&
                String.IsNullOrEmpty(this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixCustomerID))
            {
                PreferenceSectionsPanel pp = this.ShowPreferencesPanel();
                pp.FlowController = this.FlowController;
                pp.ShowOnlineOrderingPanel();
                return;
            }

            //if no project is loaded and there are IQ creds, take them to the IQ screen
            if (this.FlowController.ProjectController.CurrentFlowProject == null && !String.IsNullOrEmpty(this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixCustomerID))
            {
                this.FlowController.ProjectController.SubmitOnlineOrderData(this.FlowController.ProjectController.CurrentFlowProject);
                return;
            }

            //project is loaded and there is an PL API gallery already
            if (this.FlowController.ProjectController.CurrentFlowProject != null && this.FlowController.ProjectController.CurrentFlowProject.OnlineGallery != null && !String.IsNullOrEmpty(this.FlowController.ProjectController.CurrentFlowProject.OnlineGallery.GalleryURL))
            {
                this.FlowController.ProjectController.SubmitOnlineOrders(this.FlowController.ProjectController.CurrentFlowProject);
                return;
            }

            //project is loaded and there is an IQ gallery already
            if (this.FlowController.ProjectController.CurrentFlowProject != null && this.FlowController.ProjectController.CurrentFlowProject.Gallery != null && !String.IsNullOrEmpty(this.FlowController.ProjectController.CurrentFlowProject.Gallery.GalleryURL))
            {
                this.FlowController.ProjectController.SubmitOnlineOrderData(this.FlowController.ProjectController.CurrentFlowProject);
                return;
            }

            //there are PL API URL and NO IQ creds
            if (this.FlowController.ProjectController.CurrentFlowProject != null && !String.IsNullOrEmpty(this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.EcommerceAPIURL) &&
                String.IsNullOrEmpty(this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixCustomerID))
            {
                //new PL Ecommerce API
                this.FlowController.ProjectController.SubmitOnlineOrders(this.FlowController.ProjectController.CurrentFlowProject);
                return;
            }
            
            //there are IQ creds and no PL API URL
            if (!String.IsNullOrEmpty(this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixCustomerID) &&
                String.IsNullOrEmpty(this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.EcommerceAPIURL))
            {
                //old IQ API
                this.FlowController.ProjectController.SubmitOnlineOrderData(this.FlowController.ProjectController.CurrentFlowProject);
                return;
            }
           
            //not sure what to do, so show the preference panel
            PreferenceSectionsPanel psp = this.ShowPreferencesPanel();
            psp.FlowController = this.FlowController;
            psp.ShowOnlineOrderingPanel();
            return;
          
        }

        private void btnExportForRetouch_Click(object sender, RoutedEventArgs e)
        {
            ExportRetouch wnd = new ExportRetouch(this.FlowController.ProjectController.CurrentFlowProject);
            wnd.ShowDialog();
        }
	}

    public static class UserControlExtensions
    {
        public static void InvokeIfRequired(this UserControl uc, Action a)
        {
            if (Thread.CurrentThread != uc.Dispatcher.Thread)
                uc.Dispatcher.Invoke(a);
            else
                a();
        }
    }
}
