﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Flow.Controller;
using Flow.Lib;
using Flow.Controller.Edit.ImageAdjustments;
using System.Windows.Forms;
using System.IO;
using Flow.Schema.LinqModel.DataContext;


namespace Flow.View.Edit.ImageAdjustments
{
    /// <summary>
    /// Interaction logic for CaptureDockPanel.xaml
    /// </summary>
    public partial class ImageAdjustmentsPanel : System.Windows.Controls.UserControl
    {
        public ImageAdjustmentsPanel()
        {
            InitializeComponent();
        }

        private void btnImportCropData_Click(object sender, RoutedEventArgs e)
        {
            FlowController fc = (this.DataContext as ImageAdjustmentsController).FlowController;

            // Create an instance of the open file dialog box.
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.InitialDirectory = fc.ProjectController.CurrentFlowProject.ImageDirPath;
            openFileDialog1.Filter = "Text Files (.txt)|*.txt|All Files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            string line;
            int size = -1;
            DialogResult result = openFileDialog1.ShowDialog(); // Show the dialog.
            if (result == DialogResult.OK) // Test result.
            {
                string file = openFileDialog1.FileName;
                try
                {
                    StreamReader s = new StreamReader(file);
                    int inameIndex = -1;
                    int cropLIndex = -1;
                    int cropTIndex = -1;
                    int cropWIndex = -1;
                    int cropHIndex = -1;

                    while ((line = s.ReadLine()) != null)
                    {
                        List<string> lineValues = line.Split('\t').ToList();
                        if (inameIndex < 0)
                        {
                            inameIndex = lineValues.IndexOf("ImageName");
                            cropLIndex = lineValues.IndexOf("CropL");
                            cropTIndex = lineValues.IndexOf("CropT");
                            cropWIndex = lineValues.IndexOf("CropW");
                            cropHIndex = lineValues.IndexOf("CropH");
                            continue;
                        }

                       
                        SubjectImage image = fc.ProjectController.CurrentFlowProject.FlowProjectDataContext.SubjectImages.FirstOrDefault(si => si.ImageFileName == lineValues[inameIndex]);
                        if (image != null)
                        {
                            image.CropL = double.Parse(lineValues[cropLIndex]);
                            image.CropT = double.Parse(lineValues[cropTIndex]);
                            image.CropW = double.Parse(lineValues[cropWIndex]);
                            image.CropH = double.Parse(lineValues[cropHIndex]);
                            image.RefreshImage();
                        }
                    }

                    s.Close();
                    fc.ProjectController.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();
                }
                catch (IOException)
                {
                }
            }
        }
    }
}
