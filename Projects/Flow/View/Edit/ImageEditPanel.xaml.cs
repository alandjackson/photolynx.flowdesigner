﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Schema.LinqModel.DataContext;

namespace Flow.View.Edit
{
	/// <summary>
	/// Interaction logic for ImageEditPanel.xaml
	/// </summary>
	public partial class ImageEditPanel : UserControl
	{
		public static DependencyProperty SliderValueProperty =
			DependencyProperty.Register("SliderValue", typeof(double), typeof(ImageEditPanel), null);


		public double SliderValue
		{
			get { return (double)this.GetValue(SliderValueProperty); }
			set { this.SetValue(SliderValueProperty, value); }
		}

		public ImageEditPanel()
		{
			InitializeComponent();
			sldImageSize.Value = .55;
		}

		//public void SetDataContext(FlowProject currentFlowProject)
		//{
		//    this.ImageWrapPanel.ItemsSource = currentFlowProject.ImageList;
		//}

		private void UserControl_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
		{
			sldImageSize.Value += (e.Delta > 0) ? .05 : -.05;
		}

		private void ImageWrapPanel_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if(e.AddedItems.Count > 0)
				this.ImageWrapPanel.ScrollIntoView(e.AddedItems[0]);
		}
	}


	public class ViewBoxHeightConverter : IMultiValueConverter
	{
		#region IMultiValueConverter Members

		public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			if(
				values == null			||
				!(values[0] is double)	||
				!(values[1] is double)
			)
				return 0;

			double sliderValue = (double)(values[0]);

			double containerHeight = (double)(values[1]);

			return (double)(sliderValue * containerHeight);
		}

		public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
		{
			throw new NotImplementedException();
		}

		#endregion
	}
}
