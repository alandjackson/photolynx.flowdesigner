﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Controls.View.RecordDisplay;
using Flow.Lib.Helpers;
using Flow.Schema.LinqModel.DataContext;
using Flow.View.Project.Base;

using Xceed.Wpf.DataGrid;
using Flow.Controller.Edit;
using NLog;

namespace Flow.View.Edit
{
	/// <summary>
	/// Interaction logic for EditContentPanel.xaml
	/// </summary>
	public partial class EditContentPanel : ProjectPanelBase
	{
		//public static DependencyProperty IsFilteredOnImagesProperty =
		//    DependencyProperty.Register("IsFilteredOnImages", typeof(bool), typeof(EditContentPanel), null);

		//private bool IsFilteredOnImages
		//{
		//    get { return (bool)this.GetValue(IsFilteredOnImagesProperty); }
		//    set { this.SetValue(IsFilteredOnImagesProperty, value); }
		//}

        private static Logger logger = LogManager.GetCurrentClassLogger();

		public event EventHandler<EventArgs<SortDescriptionCollection>> SortInvoked = delegate { };

        public EditContentController EditContentController {get; set;}

		public EditContentPanel()
		{
			InitializeComponent();
		}

		protected override void OnSetFlowController()
		{
			base.OnSetFlowController();
		}

		private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			this.CollectionNavigator.IsEditingDisabled = SubjectListTabItem.IsSelected;

            if (EditContentTabControl.SelectedItem != null)
            {
                String tabName = (EditContentTabControl.SelectedItem as TabItem).Header as String;
                stkSortAndFilter.Visibility = (tabName == "Green Screen") ? Visibility.Collapsed : Visibility.Visible;
                logger.Info("Edit tab switched to '{0}'", tabName);
            }
		}

		private void SortButton_Click(object sender, RoutedEventArgs e)
		{
            logger.Info("Clicked Sort button");

			SortDescriptionCollection sortItems = new SortDescriptionCollection();
			sortItems.Add(this.SortControl1.SortDescription);

			if(!String.IsNullOrEmpty(this.SortControl2.SortDescription.PropertyName))
				sortItems.Add(this.SortControl2.SortDescription);

			this.SortInvoked(this, new EventArgs<SortDescriptionCollection>(sortItems));
		}

        private void buttonAdjustImages_Click(object sender, RoutedEventArgs e)
        {
            logger.Info("Clicked Adjust Images button");
            this.FlowController.ImageAdjustmentsCurrentView();
        }

        internal void SetSearchFocus()
        {
            this.CollectionNavigator.SetSearchFocus();
        }

        private void buttonToggleShowOriginal_Click(object sender, RoutedEventArgs e)
        {
            if (this.EditContentController.CurrentFlowProject.FlowMasterDataContext.ShowOriginalFile == true)
            {
                this.EditContentController.CurrentFlowProject.FlowMasterDataContext.ShowOriginalFile = false;
                this.buttonToggleShowOriginal.Content = "Show Original Image";
                logger.Info("Switched to showing crop images");
            }
            else
            {
                this.EditContentController.CurrentFlowProject.FlowMasterDataContext.ShowOriginalFile = true;
                this.buttonToggleShowOriginal.Content = "Show Crop Image";
                logger.Info("Switched to showing original images");
            }

            if (this.EditContentController.CurrentFlowProject.SubjectList.Count() > 0 &&
                this.EditContentController.CurrentFlowProject.SubjectList.CurrentItem != null)
            {
                this.EditContentController.CurrentFlowProject.SubjectList.CurrentItem.RefreshSubjectImages();
            }
        }

        private void _this_Loaded(object sender, RoutedEventArgs e)
        {
            //if (GreenScreenTabItem != null)
            //{
            //    if (!this.EditContentController.CurrentFlowProject.FlowMasterDataContext.PreferenceManager.Application.CanGreenScreen)
            //        GreenScreenTabItem.Opacity = 0.395;
            //}
        }

        private void buttonShowMissingImages_Click(object sender, RoutedEventArgs e)
        {
            logger.Info("Clicked Show Missing Images button");
            this.EditContentController.ApplyMissingImageFilter();
        }
	}
}
