﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Controls;
using Flow.Lib;
using Flow.Lib.Helpers;
using Flow.Schema;
using Flow.Schema.Import;
using Flow.Schema.LinqModel.DataContext;
using Flow.Schema.Utility;
using Flow.View.Project.Base;

using Xceed.Wpf.DataGrid;

namespace Flow.View.Edit
{
	/// <summary>
	/// Interaction logic for DataImportPanel.xaml
	/// </summary>
	public partial class DataImportPanel : ProjectPanelBase
	{
		private DataTable _sourceDataTable = null;
		private DataGridCollectionView _view = null;

		FlowImportDataSource _dataSource = null;

		ImportColumnPairCollection<ProjectSubjectField> _importColumnMappings = null;

		private bool _dataLoaded = false;

//		private DataAdapter _dataAdapter = null;

		public DataImportPanel()
		{
			InitializeComponent();
		}

		protected override void OnSetFlowController()
		{
			this.DataContext = this.CurrentFlowProject;
		}

		private void btnSelectSourceFile_Click(object sender, RoutedEventArgs e)
		{
			string selectedFilePath = DialogUtility.SelectFile(
				"Select Source File",
				FlowContext.FlowProjectsDirPath,
				"Flow Project (*.sdf)|*.sdf|MS Access Database (*.mdb;*.accdb)|*.mdb;*.accdb|Excel Workbook (*.xls;*.xslx)|*.xls;*.xslx|Delimited Text File (*.csv;*.txt)|*.csv;*.txt|All Files (*.*)|*.*"
			);

			if (!String.IsNullOrEmpty(selectedFilePath))
			{
				_dataLoaded = false;

				if (_importColumnMappings != null)
					_importColumnMappings.Clear();

				dgcImportData.Columns.Clear();
				dgcImportData.ItemsSource = null;

//				_dataSource.SourceTable.
	
				txtSourceDataFileName.Text = selectedFilePath;

				_dataSource = new FlowImportDataSource(selectedFilePath);

				switch (_dataSource.SourceType)
				{
					case FlowImportDataSourceType.FlowProject:
						break;

					case FlowImportDataSourceType.MsAccess:
						cmbSourceTables.ItemsSource = _dataSource.GetSourceTableList();
						cmbSourceTables.SelectedIndex = 0;
						break;

					case FlowImportDataSourceType.MsExcel:
						cmbSourceTables.ItemsSource = _dataSource.GetSourceTableList();
						cmbSourceTables.SelectedIndex = 0;
						break;

					case FlowImportDataSourceType.DelimitedText:
						break;
				}

				stkSourceSpecificOptions.Visibility = (_dataSource.IsFlowProjectSource)
					? Visibility.Hidden : Visibility.Visible;

				stkExcelAccessOptions.Visibility = (
					_dataSource.IsMsAccessSource		||
					_dataSource.IsMsExcelSource
				) ? Visibility.Visible : Visibility.Collapsed;

				stkExcelTextOptions.Visibility = (
					_dataSource.IsMsExcelSource			||
					_dataSource.IsDelimitedTextSource
				) ? Visibility.Visible : Visibility.Collapsed;

				stkDelimiter.Visibility = (_dataSource.IsDelimitedTextSource)
					? Visibility.Visible : Visibility.Collapsed;
			
			}

		}

		private void btnPreviewData_Click(object sender, RoutedEventArgs e)
		{
			this.PreviewData();
		}

		private void cmbSourceTables_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if(_dataLoaded)
				this.PreviewData();
		}

		private void PreviewData()
		{
			switch (_dataSource.SourceType)
			{
				case FlowImportDataSourceType.MsAccess:
					_dataSource.SourceTable = cmbSourceTables.SelectedValue as string;
					break;

				case FlowImportDataSourceType.MsExcel:
					_dataSource.SourceTable = cmbSourceTables.SelectedValue as string;
					_dataSource.HasColumnHeaders = chkHasColumnHeaders.IsChecked.Value;
					break;

				case FlowImportDataSourceType.DelimitedText:
					_dataSource.HasColumnHeaders = chkHasColumnHeaders.IsChecked.Value;
					break;
			}

			_sourceDataTable = new DataTable();

			_dataSource.Fill(_sourceDataTable);

			ImportColumnStrategies columnStrategy;

			if (rdoColumnAutomap.IsChecked.Value)
			{
				if (chkAutomapMatchedOnly.IsChecked.Value)
					columnStrategy = ImportColumnStrategies.AutoMapMatchedOnly;
				else
					columnStrategy = ImportColumnStrategies.Automap;
			}
			else
				columnStrategy = ImportColumnStrategies.ManualAssign;


			_importColumnMappings =	new ImportColumnPairCollection<ProjectSubjectField>(
					_sourceDataTable,
					this.CurrentFlowProject.ProjectSubjectFieldList,
					columnStrategy
				);

			lsvColumnMappings.ItemsSource = _importColumnMappings;
			lsvColumnMappings.SelectedIndex = 0;

			stkColumnProperties.Visibility = Visibility.Visible;

			dgcImportData.Columns.Clear();
			_view = new DataGridCollectionView(_sourceDataTable.DefaultView);
			dgcImportData.ItemsSource = _view;

			_dataLoaded = true;
		}

		private void lsvColumnMappings_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if(e.AddedItems.Count > 0)
				uxColumnProperties.DataContext = e.AddedItems[0];
		}

		//private void uxColumnProperties_OmitColumnChecked(object sender, EventArgs<object> e)
		//{
		//    ((ImportColumnPair<ProjectSubjectField>)e.Data).NullifyDestinationField();
		//}

		//private void uxColumnProperties_OmitColumnUnchecked(object sender, EventArgs<object> e)
		//{
		//    ((ImportColumnPair<ProjectSubjectField>)e.Data).InferDestinationField();
		//}

		private void ColumnMappingButton_Click(object sender, RoutedEventArgs e)
		{
			Button sourceButton = sender as Button;

			DockPanel buttonParent = sourceButton.Parent as DockPanel;

			lsvColumnMappings.SelectedItem = buttonParent.DataContext;

			buttonParent.ContextMenu.PlacementTarget = buttonParent;
			buttonParent.ContextMenu.Placement = PlacementMode.Right;

			buttonParent.ContextMenu.IsOpen = true;
		}

		private void ColumnMappingMenuCloseButton_Click(object sender, RoutedEventArgs e)
		{
			Button sourceButton = sender as Button;

			((ContextMenu)sourceButton.Tag).IsOpen = false;
		}

		private void uxColumnProperties_SubjectFieldSelected(object sender, EventArgs<ISubjectField> e)
		{
			((ImportColumnPair<ProjectSubjectField>)(sender as UserControl).DataContext).AssignSubjectField(e.Data);
		}

		private void btnCancelImport_Click(object sender, RoutedEventArgs e)
		{
			this.FlowController.Edit();
		}

		private void btnImport_Click(object sender, RoutedEventArgs e)
		{
			_importColumnMappings.IgnoreDuplicates = chkIgnoreDuplicates.IsChecked.Value;
	
			// If there are no existing Subject records, or existing records are to be replaced...
			if (!this.CurrentFlowProject.HasSubjects || rdoReplaceData.IsChecked.Value)
			{
				_importColumnMappings.ImportUnmatched = true;

				// Remove columns (and any data) from Subject table
				this.CurrentFlowProject.ClearSubjectTableColumns();
				
				// Delete from ProjectSubjectField table
				this.CurrentFlowProject.ProjectSubjectFieldList.Clear();

				// Add specified column defintions to ProjectSubjectField table
				foreach (ImportColumnPair<ProjectSubjectField> item in _importColumnMappings)
				{
					if (item.DestinationField != null)
					{
						if (item.DestinationField.SubjectFieldDataType != item.SourceField.SubjectFieldDataType)
							item.DestinationField.SubjectFieldDataType = item.SourceField.SubjectFieldDataType;

						if (item.DestinationField.IsKeyField)
							_importColumnMappings.KeyFields.Add(item);
		
						this.CurrentFlowProject.ProjectSubjectFieldList.Add(item.DestinationField);
					}
				}

				// Save changes from prior operations
				this.CurrentFlowProject.Save();

				// Create Subject table columns as specified in the import 
				this.CurrentFlowProject.CreateSubjectTableColumns();
			}

			// If there are Subject records which will not be replaced...
			else
			{
				_importColumnMappings.ImportUnmatched = chkUpdateImportUnmatched.IsChecked.Value;
	
				foreach (ImportColumnPair<ProjectSubjectField> item in _importColumnMappings)
				{
					if (item.DestinationField != null)
					{
						if (item.DestinationField.IsKeyField)
							_importColumnMappings.KeyFields.Add(item);
					}
				}
			}

			// Save the source data in the Subject table
			this.CurrentFlowProject.ImportData(_importColumnMappings, _sourceDataTable);

			// Reload the project to reflect changes
			this.FlowController.ProjectController.Load(this.CurrentFlowProject);

			// Move app control to the Edit display
			this.FlowController.Edit();
		}
	}
}
