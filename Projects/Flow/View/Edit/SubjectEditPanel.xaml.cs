﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Controls.View.RecordDisplay;
using Flow.Lib.Helpers;
using Flow.Schema.LinqModel.DataContext;

using Xceed.Wpf.DataGrid;
using Flow.Controls.View.ImageDisplay;
using NLog;

namespace Flow.View.Edit
{
	/// <summary>
	/// Interaction logic for SubjectEditPanel.xaml
	/// </summary>
	public partial class SubjectEditPanel : UserControl
	{
        private static Logger logger = LogManager.GetCurrentClassLogger();

		public event EventHandler OrderEntryInvoked = delegate { };

		public SubjectEditPanel()
		{
			InitializeComponent();
		}

		private void OrderEntryButton_Click(object sender, RoutedEventArgs e)
		{
            logger.Info("Clicked Order Entry button");
			this.OrderEntryInvoked(this, new EventArgs());
		}

        private void SubjectImageList_MouseEnter(object sender, MouseEventArgs e)
        {
            if (((FlowProject)this.DataContext).FlowMasterDataContext.PreferenceManager.Permissions.CanUnassignImagesFromSubjects)
                ((FlowImageListBoxEditTemp)sender).UnAssignButtonVisibility = Visibility.Visible;
            else
                ((FlowImageListBoxEditTemp)sender).UnAssignButtonVisibility = Visibility.Hidden;

            if (((FlowProject)this.DataContext).FlowMasterDataContext.PreferenceManager.Permissions.CanDeleteImages)
                ((FlowImageListBoxEditTemp)sender).DeleteButtonVisibility = Visibility.Visible;
            else
                ((FlowImageListBoxEditTemp)sender).DeleteButtonVisibility = Visibility.Hidden;

            if (((FlowProject)this.DataContext).FlowMasterDataContext.PreferenceManager.Permissions.CanFlagImageAs)
                ((FlowImageListBoxEditTemp)sender).FlagAsButtonVisibility = Visibility.Visible;
            else
                ((FlowImageListBoxEditTemp)sender).FlagAsButtonVisibility = Visibility.Hidden;

            if (((FlowProject)this.DataContext).FlowMasterDataContext.PreferenceManager.Permissions.CanViewImageAdjustments)
                ((FlowImageListBoxEditTemp)sender).AdjustButtonVisibility = Visibility.Visible;
            else
                ((FlowImageListBoxEditTemp)sender).AdjustButtonVisibility = Visibility.Hidden;
        }

        private void SubjectRecordWrapDisplay_BailBailBail(object sender, EventArgs<bool> e)
        {

        }

        private void SubjectRecordWrapDisplay_CheckChanged(object sender, EventArgs<bool> e)
        {
            ((FlowProject)this.DataContext).FlowProjectDataContext.SubmitChanges();
        }
	}
}
