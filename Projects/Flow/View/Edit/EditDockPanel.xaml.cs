﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Controller;
using Flow.Controller.Edit;
using Flow.Lib;
using Flow.View.Project;
using Flow.View.Project.Base;

namespace Flow.View.Edit
{
	/// <summary>
	/// Interaction logic for EditDockPanel.xaml
	/// </summary>
	public partial class EditDockPanel : ProjectPanelBase
	{
		public EditController EditController { get; set; }

		public EditDockPanel()
		{
			InitializeComponent();
			//this.DockingManager.ToggleAutoHide(this.OrganizationDockablePane);
		}

		//protected override void OnSetFlowController()
		//{
		//    base.OnSetFlowController();

		//    this.OrganizationEditPanel.SetDataContext(this.CurrentFlowProject);
		//    this.EditContentPanel.FlowController = this.FlowController;
		//}

		private void EditDockPanel_PreviewKeyUp(object sender, KeyEventArgs e)
		{
			switch (e.Key)
			{
				// [F3]: Set search focus
				case Key.F3:
					//this.EditController.SetSearchFocus();
                    if (e.Key == Key.F3 && Keyboard.Modifiers != ModifierKeys.Control)
                    {
                        this.EditContentPanel.SetSearchFocus();
                    }
                    break;

				// [F8]: Invoke order entry
				case Key.F8:
					if(this.EditController.FlowController.ProjectController.CurrentFlowProject.HasSubjects)
						this.EditController.DisplayOrderEntry();
					break;
			}
		}

		private void ProjectPanelBase_PreviewTextInput(object sender, TextCompositionEventArgs e)
		{
			string inputText = e.Text;

			// If the input is a carriage return, flag this
			bool endsWithCarriageReturn = inputText.EndsWith("\r");

			// Match the current barcode scan input buffer against the barcode scan key pattern
			if (this.EditController.DetectScan(inputText, endsWithCarriageReturn))
				e.Handled = true;
		}

        private void _this_KeyDown(object sender, KeyEventArgs e)
        {
            if (EditController.FlowController.ScanController.addKeyStroke(e.Key))
            {
                //true means key has been handled
                e.Handled = true;
                if (this.EditContentPanel.EditOrderEntryPanel.IsVisible)
                    this.EditContentPanel.EditContentController.OrderEntryController.SetDefaultFocus();
                //this.FlowController.ProjectController.specialBarCodeHandled = false;
            }
        }

        //private void EditContentPanel_Loaded(object sender, RoutedEventArgs e)

    }
}
