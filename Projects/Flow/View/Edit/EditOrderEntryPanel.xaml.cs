﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Controller.Catalog;

namespace Flow.View.Edit
{
	/// <summary>
	/// Interaction logic for EditOrderEntryPanel.xaml
	/// </summary>
	public partial class EditOrderEntryPanel : UserControl
	{
		public OrderEntryController OrderEntryController
		{ set { if (value != null) value.Main(this.OrderEntryPanel); } }

	
		public EditOrderEntryPanel()
		{
			InitializeComponent();
		}
	}
}
