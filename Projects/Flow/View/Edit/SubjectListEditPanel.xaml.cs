﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Lib.Helpers;
using Flow.Schema.LinqModel.DataContext;

using Xceed.Wpf.DataGrid;
using Flow.Controls.View.ImageDisplay;

using NLog;

namespace Flow.View.Edit
{
	/// <summary>
	/// Interaction logic for SubjectListEditPanel.xaml
	/// </summary>
	public partial class SubjectListEditPanel : UserControl
	{
		public event EventHandler<EventArgs<SortDescriptionCollection>> DataGridSortInvoked = delegate { };
        public event EventHandler<RoutedEventArgs> UpdateVisibleColumns = delegate { };

        public event EventHandler<EventArgs<ProjectSubjectField>>
        ApplyFieldFilter = delegate { };

		public event EventHandler OrderEntryInvoked = delegate { };


		private ColumnManagerCell ColumnHeader { get; set; }

        private static Logger logger = LogManager.GetCurrentClassLogger();

		public SubjectListEditPanel()
		{
			InitializeComponent();

			this.SubjectListDataGrid.Loaded += new RoutedEventHandler(SubjectListDataGrid_Loaded);
		}

		void ColumnManagerCell_PreviewMouseDown(object sender, MouseButtonEventArgs e)
		{
			this.ColumnHeader = sender as ColumnManagerCell;
		}

		void ColumnManagerCell_PreviewMouseUp(object sender, MouseButtonEventArgs e)
		{
			if (!(this.ColumnHeader == null || this.ColumnHeader.IsBeingDragged))
			{
				// Get the column against which the sort is being requested
				ColumnBase sortRequestColumn = this.ColumnHeader.ParentColumn;
	
				SortDescriptionCollection sortItems = new SortDescriptionCollection();

				// Get the set of existing sorted columns
				IOrderedEnumerable<Column> sortedColumns =
					this.SubjectListDataGrid.Columns
						.Cast<Column>()
						.Where(c =>
							(Keyboard.Modifiers == ModifierKeys.Shift && c.SortDirection != SortDirection.None)	||
							c.FieldName == sortRequestColumn.FieldName
						)
						.OrderByDescending(c => c.SortIndex);

				// Iterate through this list and add SortDesscriptions to list
				foreach (Column column in sortedColumns)
				{
					ListSortDirection direction = ListSortDirection.Ascending;

					if(column.FieldName == sortRequestColumn.FieldName)
					{
						if(column.SortDirection == SortDirection.Ascending)
							direction = ListSortDirection.Descending;
						else if(column.SortDirection == SortDirection.Descending)
							continue;
					}
					else
					{
						direction = (column.SortDirection == SortDirection.Ascending)
							? ListSortDirection.Ascending
							: ListSortDirection.Descending;

					}

					sortItems.Add(new SortDescription(column.FieldName, direction));
				}

				this.DataGridSortInvoked(this, new EventArgs<SortDescriptionCollection>(sortItems));

				this.ColumnHeader = null;
			}
		}

		void SubjectListDataGrid_Loaded(object sender, RoutedEventArgs e)
		{
            if (this.SubjectListDataGrid.Columns["SubjectID"] != null)
                this.SubjectListDataGrid.Columns["SubjectID"].ReadOnly = true;
            if (this.SubjectListDataGrid.Columns["Ticket Code"] != null)
                this.SubjectListDataGrid.Columns["Ticket Code"].ReadOnly = true;
            if (this.SubjectListDataGrid.Columns["Package Summary"] != null)
                this.SubjectListDataGrid.Columns["Package Summary"].ReadOnly = true;

            this.SubjectListDataGrid.View.OverridesDefaultStyle = true;
            
            
		}

		private void PrintDataGridButton_Click(object sender, RoutedEventArgs e)
		{
            logger.Info("Print Data Grid button clicked");
			this.SubjectListDataGrid.Print();
		}

		public void OnSortCompleted(object sender, EventArgs e)
		{
			this.SubjectListDataGrid.BringItemIntoView(this.SubjectListDataGrid.SelectedItem);
		}

		private void OrderEntryButton_Click(object sender, RoutedEventArgs e)
		{
            logger.Info("Clicked Order Entry button");
			this.OrderEntryInvoked(this, new EventArgs());
		}

        private void SubjectImageList_MouseEnter(object sender, MouseEventArgs e)
        {
            if (this.DataContext == null)
                return;

            if (((FlowProject)this.DataContext).FlowMasterDataContext.PreferenceManager.Permissions.CanUnassignImagesFromSubjects)
                ((FlowImageListBoxEditTemp)sender).UnAssignButtonVisibility = Visibility.Visible;
            else
                ((FlowImageListBoxEditTemp)sender).UnAssignButtonVisibility = Visibility.Hidden;

            if (((FlowProject)this.DataContext).FlowMasterDataContext.PreferenceManager.Permissions.CanDeleteImages)
                ((FlowImageListBoxEditTemp)sender).DeleteButtonVisibility = Visibility.Visible;
            else
                ((FlowImageListBoxEditTemp)sender).DeleteButtonVisibility = Visibility.Hidden;

            if (((FlowProject)this.DataContext).FlowMasterDataContext.PreferenceManager.Permissions.CanFlagImageAs)
                ((FlowImageListBoxEditTemp)sender).FlagAsButtonVisibility = Visibility.Visible;
            else
                ((FlowImageListBoxEditTemp)sender).FlagAsButtonVisibility = Visibility.Hidden;

            if (((FlowProject)this.DataContext).FlowMasterDataContext.PreferenceManager.Permissions.CanViewImageAdjustments)
                ((FlowImageListBoxEditTemp)sender).AdjustButtonVisibility = Visibility.Visible;
            else
                ((FlowImageListBoxEditTemp)sender).AdjustButtonVisibility = Visibility.Hidden;
        }

        private void CheckBox_Click(object sender, RoutedEventArgs e)
        {
            this.UpdateVisibleColumns(sender, e);
        }

        private void btnApplyFilter_Click(object sender, RoutedEventArgs e)
        {
            logger.Info("Clicked Apply Filter button");
            ProjectSubjectField psf = (sender as Button).DataContext as ProjectSubjectField;
            ApplyFieldFilter(this, new EventArgs<ProjectSubjectField>(psf));
        }



	}
}
