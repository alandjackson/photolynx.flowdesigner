﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Flow.Schema.LinqModel.DataContext;
using Flow.Lib.Helpers;
using Flow.GreenScreen.Data;
using Flow.GreenScreen.Managers;
using Flow.GreenScreen.Managers.Panels;
using Flow.Controller.Edit;
using NLog;

namespace Flow.View.Edit
{
    /// <summary>
    /// Interaction logic for GreenScreenEditPanel.xaml
    /// </summary>
    public partial class GreenScreenEditPanel : UserControl
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public GreenScreenEditPanel()
        {
            InitializeComponent();
            DataContextChanged += (s, e) => UpdatePanelWithProject(DataContext as FlowProject);

            //InitializeGreenScreenPanel();
        }

        public void InitializeGreenScreenPanel()
        {
            GreenScreenPanel.Initialize();

            GreenScreenPanel.Data.SetConfigurationXml = SetConfigXml;
            GreenScreenPanel.Data.GetConfigurationXml = GetConfigXml;
            GreenScreenPanel.Data.GetAdjustmentBitmap = GetAdjustedBitmap;
            SubjectImageList.ListBoxSelectionChanged += SubjectImageList_ListBoxSelectionChanged;
            GreenScreenPanel.IsVisibleChanged += GreenScreenPanel_IsVisibleChanged;
            
        }

        System.Drawing.Bitmap GetAdjustedBitmap(string image)
        {
            var bitmap = new System.Drawing.Bitmap(image);
            var bmpUnlinked = new System.Drawing.Bitmap(bitmap); //this Bitmap object is not linked to the file
            bitmap.Dispose();

            var project = (DataContext as FlowProject);
            if (project == null) return bmpUnlinked;

            //dont include suppressed images in greenscreen image list
            var images = project.SubjectList.CurrentItem.SubjectImages.Where(si=>si.IsSuppressed != true).ToArray();
            //var images = project.SubjectList.CurrentItem.SubjectImages.ToArray();
            //var subjImage = images.Where(i => i.ImagePath == image).FirstOrDefault()
            var subjImage = GetCurrentSubjectImage(images, image);
            if (subjImage == null)
                return bmpUnlinked;

            bmpUnlinked.RotateFlip(GetRotateFlip(subjImage.Orientation));

            return bmpUnlinked;
        }

        SubjectImage GetCurrentSubjectImage(SubjectImage[] images, string image)
        {
            if (images.Count() == 0)
                return null;

            var ndx = SubjectImageList.SelectedIndex;

            if (ndx == -1 && images[0].ImagePathFullRes == image)
                return images[0];
            else if (ndx < 0 || ndx > images.Length)
                return null;
            else
                return images[ndx];
        }


        System.Drawing.RotateFlipType GetRotateFlip(int orientation)
        {
            if (orientation == 90)
                return System.Drawing.RotateFlipType.Rotate90FlipNone;
            else if (orientation == 180)
                return System.Drawing.RotateFlipType.Rotate180FlipNone;
            else if (orientation == 270)
                return System.Drawing.RotateFlipType.Rotate270FlipNone;
            else
                return System.Drawing.RotateFlipType.RotateNoneFlipNone;
        }

        string GetConfigXml(string image)
        {
            var project = (DataContext as FlowProject);
            if (project == null) return "";

            var images = project.SubjectList.CurrentItem.SubjectImages.Where(si => si.IsSuppressed != true).ToArray();
            var subjImage = GetCurrentSubjectImage(images, image);
            if (subjImage == null) return "";

            return subjImage.GreenScreenSettings ?? "";
        }

        void SetConfigXml(string image, string xml, SaveGsSettingsTo applyTo)
        {   
            var project = (DataContext as FlowProject);
            if (project == null) return;

            if (project.SubjectList.Count == 0 || project.SubjectList.CurrentItem == null)
                return;

            var images = project.SubjectList.CurrentItem.SubjectImages.Where(si => si.IsSuppressed != true).ToArray();

            var gsBackground = string.IsNullOrEmpty(xml) ? "" :
                (ManagerTracker.SettingsPanelManager as CPISettingsPanelManager).BackgroundPath;

            if (applyTo == SaveGsSettingsTo.Image)
            {
                var subjImage = GetCurrentSubjectImage(images, image);
                ApplySettingsToSubjectImage(subjImage, xml, gsBackground);
            }
            else if (applyTo == SaveGsSettingsTo.Subject)
            {
                images.Where(si => si.GreenScreenSettings != null).ToList().ForEach(subjImage => 
                    ApplySettingsToSubjectImage(subjImage, xml, gsBackground));    
            }
            else if (applyTo == SaveGsSettingsTo.Project)
            {
                project.SubjectList.SelectMany(s => s.SubjectImages.Where(si=>si.GreenScreenSettings != null)).ToList().ForEach(subjImage =>
                    ApplySettingsToSubjectImage(subjImage, xml, gsBackground));
            }
            else if (applyTo == SaveGsSettingsTo.Filter)
            {
                project.GetFilteredSubjectList().SelectMany(s => s.SubjectImages.Where(si => !si.IsSuppressed && si.GreenScreenSettings != null)).ToList().ForEach(subjImage =>
                    ApplySettingsToSubjectImage(subjImage, xml, gsBackground));
            }

            project.FlowProjectDataContext.SubmitChanges();

        }

        void ApplySettingsToSubjectImage(SubjectImage subjImage, string xml, string bg)
        {
            if (subjImage == null) return;

            subjImage.GreenScreenSettings = xml ?? "";
            subjImage.GreenScreenBackground = bg;
            subjImage.ExistsInOnlineGallery = false;
            subjImage.Subject.FlagForMerge = true;
            logger.Info("Applying Green Screen settings to subject image: subjectName='{0}' imageFileName='{1}'",
                subjImage.Subject.FormalFullName, subjImage.ImageFileName);
        }


        void SubjectImageList_ListBoxSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
            // Nothing selected, but don't update green screen
            if (SubjectImageList.SelectedIndex == -1)
            {
                //return;
                if (SubjectImageList.HasContent)
                    SubjectImageList.SelectedIndex = 0;
                else
                    return;
            }
            
            SetSelectedIndex(SubjectImageList.SelectedIndex);
        }

        void UpdatePanelWithProject(FlowProject project)
        {
            if (project == null || project.SubjectList == null || project.SubjectList.Count == 0)
            {
                UpdatePanelWithSubject(null);
                return;
            }

            project.SubjectList.CurrentChanged += (ss, ee) => UpdatePanelWithSubject(ee.Data);
            UpdatePanelWithSubject(project.SubjectList.CurrentItem);
        }

        void UpdatePanelWithSubject(Subject e)
        {
            if (e == null)
            {
                SetImageFiles(new string[0]);
                return;
            }


            string[] imageFiles = e.ImageList.Where(si=> si.IsSuppressed != true && si.IsPng != true).Select(si => si.ImagePathFullRes).ToArray();
            //string[] imageFiles = e.SubjectImages.Where(si => si.IsSuppressed != true).Select(si => si.ImagePathFullRes).ToArray();
            SetImageFiles(imageFiles);
            if (imageFiles.Length > 0)
            {
                SetSelectedIndex(0);
            }
        }

        string [] _lastImageFiles = null;
        void SetImageFiles(string[] files)
        {
            if (GreenScreenPanel.IsVisible)
            {
                GreenScreenPanel.Data.SetImageFiles(files);
            }
            else
            {
                _lastImageFiles = files;
            }
            //MessageBox.Show("GS Setting Image Files");
        }

        int _lastSelNdx = -1;
        void SetSelectedIndex(int ndx)
        {
            if (GreenScreenPanel.IsVisible)
            {
                GreenScreenPanel.SelectedIndex = ndx;
            }
            else
            {
                _lastSelNdx = ndx;
            }
            //MessageBox.Show("GS setting selected index");
        }

        void GreenScreenPanel_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            // load the currently selected subject when the panel is becoming visible
            if (GreenScreenPanel.IsVisible)
            {
                if (_lastImageFiles != null)
                {
                    SetImageFiles(_lastImageFiles);
                    _lastImageFiles = null;
                }
                if (_lastSelNdx != -1)
                {
                    SetSelectedIndex(_lastSelNdx);
                    _lastSelNdx = -1;
                }

                // Enable the green screen controls only of there are images
                GreenScreenPanel.IsEnabled = !(this.GreenScreenPanel.Data.Images.Count() == 0);
            }
            // make sure the current settings have been saved when the panel is losing visibility
            else
            {
                //GreenScreenPanel.Data.SetConfigurationXml 
            }

        }


    }
}
