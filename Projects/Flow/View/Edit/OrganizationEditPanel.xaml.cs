﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Schema.LinqModel.DataContext;

namespace Flow.View.Edit
{
	/// <summary>
	/// Interaction logic for OrganizationEditPanel.xaml
	/// </summary>
	public partial class OrganizationEditPanel : UserControl
	{
		FlowProject _flowProject = null;
	
		public OrganizationEditPanel()
		{
			InitializeComponent();
		}

		public void SetDataContext(FlowProject flowProject)
		{
			_flowProject = flowProject;
			SetTreeViewFromOrgStructure();
		}

		/// <summary>
		/// Populates the treeview from the underlying org structure data
		/// </summary>
		internal void SetTreeViewFromOrgStructure()
		{
			if (_flowProject.OrganizationalStructure == null)
				trvOrganizationStructure.Items.Clear();
			else
			{
				trvOrganizationStructure.Items.Add(
					NewTreeViewItem(_flowProject.OrganizationalStructure)
				);
			}
		}

		/// <summary>
		/// Creates a treeview item for each OrganizationalType assigned to the org structure;
		/// and recursively adds children as well
		/// </summary>
		/// <param name="dataItem"></param>
		private TreeViewItem NewTreeViewItem(ProjectOrganizationalUnitType dataItem)
		{
			TreeViewItem currentItem = new TreeViewItem();
			currentItem.Header = dataItem.ProjectOrganizationalUnitTypeDesc;
			currentItem.Tag = dataItem;
			currentItem.IsExpanded = true;

			// Add any subject types assigned to the current org type
			foreach (ProjectSubjectType childDataItem in dataItem.ProjectSubjectTypes)
			{
				TreeViewItem childItem = new TreeViewItem();
				childItem.Header = childDataItem.ProjectSubjectTypeDesc;
				childItem.Tag = childDataItem;
				childItem.IsExpanded = true;

				currentItem.Items.Add(childItem);
			}

			// Add any child org types assigned to the current org type
			foreach (ProjectOrganizationalUnitType childDataItem in dataItem.ProjectOrganizationalUnitTypes)
			{
				currentItem.Items.Add(NewTreeViewItem(childDataItem));
			}

			return currentItem;
		}

	}
}
