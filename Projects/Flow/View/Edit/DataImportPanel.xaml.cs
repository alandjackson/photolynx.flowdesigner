﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;

using Flow.Controls;
using Flow.Lib;
using Flow.Lib.Helpers;
using Flow.Schema;
using Flow.Schema.Import;
using Flow.Schema.LinqModel.DataContext;
using Flow.Schema.Utility;
using Flow.View.Project.Base;

using StructureMap;

using Xceed.Wpf.DataGrid;
using Flow.View.Dialogs;
using System.Windows.Threading;
using NLog;

namespace Flow.View.Edit
{
	/// <summary>
	/// Interaction logic for DataImportPanel.xaml
	/// </summary>
	public partial class DataImportPanel : ProjectPanelBase
	{
        private static Logger logger = LogManager.GetCurrentClassLogger();

		private DataTable _sourceDataTable = null;
		private DataGridCollectionView _view = null;

        private string selectedImageField = "";
        private string selectedImageDir = "";

		FlowImportDataSource _dataSource = null;

		protected static DependencyProperty ImportColumnMappingsProperty = DependencyProperty.Register(
			"ImportColumnMappings", typeof(ImportColumnPairCollection<ProjectSubjectField>), typeof(DataImportPanel)
		);

		public ImportColumnPairCollection<ProjectSubjectField> ImportColumnMappings
		{
			get { return (ImportColumnPairCollection<ProjectSubjectField>)this.GetValue(ImportColumnMappingsProperty); }
			set { this.SetValue(ImportColumnMappingsProperty, value); }
		}


		protected static DependencyProperty DuplicateKeyPanelVisibilityProperty = DependencyProperty.Register(
			"DuplicateKeyPanelVisibility", typeof(Visibility), typeof(DataImportPanel), new PropertyMetadata(Visibility.Collapsed)
		);

		public Visibility DuplicateKeyPanelVisibility
		{
			get { return (Visibility)this.GetValue(DuplicateKeyPanelVisibilityProperty); }
			set { this.SetValue(DuplicateKeyPanelVisibilityProperty, value); }
		}

        protected static DependencyProperty MissingRequiredFieldPanelVisibilityProperty = DependencyProperty.Register(
            "MissingRequiredFieldPanelVisibility", typeof(Visibility), typeof(DataImportPanel), new PropertyMetadata(Visibility.Collapsed)
        );

        public Visibility MissingRequiredFieldPanelVisibility
        {
            get { return (Visibility)this.GetValue(MissingRequiredFieldPanelVisibilityProperty); }
            set { this.SetValue(MissingRequiredFieldPanelVisibilityProperty, value); }
        }


		private bool _dataLoaded = false;
		private bool _commaDelimited = true;


		/// <summary>
		/// Constructor
		/// </summary>
		public DataImportPanel()
		{
			InitializeComponent();

			this.ClearSelections();
			//this.ImportColumnMappings = new ImportColumnPairCollection<ProjectSubjectField>();
		}

		protected override void OnSetFlowController()
		{
			this.DataContext = this.CurrentFlowProject;
			this.ProjectController.PropertyChanged += new PropertyChangedEventHandler(ProjectController_PropertyChanged);
		}

		void ProjectController_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			// If the project changes, clear panel bindings
			if (e.PropertyName == "CurrentFlowProject")
			{
				this.ClearSelections();
				//this.ImportColumnMappings = new ImportColumnPairCollection<ProjectSubjectField>();
				//txtSourceDataFileName.Text = "";
				//dgcImportData.ItemsSource = null;
			}
		}

		private void ClearSelections()
		{
			this.ImportColumnMappings = new ImportColumnPairCollection<ProjectSubjectField>();
			txtSourceDataFileName.Text = "";
			dgcImportData.ItemsSource = null;
		}

		private void Reset(string selectedFilePath)
		{
            logger.Info("Data import panel reset");

			_dataLoaded = false;

			if (ImportColumnMappings != null)
				ImportColumnMappings.Clear();

			dgcImportData.Columns.Clear();
			dgcImportData.ItemsSource = null;

			txtSourceDataFileName.Text = selectedFilePath;

			_dataSource = new FlowImportDataSource(selectedFilePath);

			if (_dataSource.IsMsAccessSource || _dataSource.IsMsExcelSource)
			{
                try
                {
                    cmbSourceTables.ItemsSource = _dataSource.GetSourceTableList();
                    cmbSourceTables.SelectedIndex = 0;
                }
                catch (Exception ex)
                {
                    if (ex.Message.StartsWith("The process cannot access the file"))
                    {
                        FlowMessageBox msg = new FlowMessageBox("Source File Locked", "The file you are importing is locked by another program.\nPlease close the other program and try to import it again.");
                        msg.CancelButtonVisible = false;
                        msg.ShowDialog();
                    }

                }
			}

			//switch (_dataSource.SourceType)
			//{
			//    case FlowImportDataSourceType.FlowProject:
			//        break;

			//    case FlowImportDataSourceType.MsAccess:
			//        cmbSourceTables.ItemsSource = _dataSource.GetSourceTableList();
			//        cmbSourceTables.SelectedIndex = 0;
			//        break;

			//    case FlowImportDataSourceType.MsExcel:
			//        cmbSourceTables.ItemsSource = _dataSource.GetSourceTableList();
			//        cmbSourceTables.SelectedIndex = 0;
			//        break;

			//    case FlowImportDataSourceType.DelimitedText:
			//        break;
			//}

            //stkSourceSpecificOptions.Visibility = (_dataSource.IsFlowProjectSource)
            //    ? Visibility.Hidden : Visibility.Visible;

            //Load the Preview
            

			stkExcelAccessOptions.Visibility = (
				_dataSource.IsMsAccessSource		||
				_dataSource.IsMsExcelSource
			) ? Visibility.Visible : Visibility.Collapsed;

            //stkExcelTextOptions.Visibility = (
            //    _dataSource.IsMsExcelSource			||
            //    _dataSource.IsDelimitedTextSource
            //) ? Visibility.Visible : Visibility.Collapsed;

            //stkDelimiter.Visibility = (_dataSource.IsDelimitedTextSource)
            //    ? Visibility.Visible : Visibility.Collapsed;
			
		}

		private void btnSelectSourceFile_Click(object sender, RoutedEventArgs e)
		{
            logger.Info("Clicked source file select button in Data Import");

			string selectedFilePath = DialogUtility.SelectFile(
				"Select Source File",
				this.FlowMasterDataContext.PreferenceManager.Application.ProjectsDirectory,
                "All Supported File Types (*.csv;*.txt;*.xls)|*.csv;*.txt;*.xls|Delimited Text File (*.csv;*.txt)|*.csv;*.txt|Excel Workbook (*.xls)|*.xls|All Files (*.*)|*.*"
			);

			if (!String.IsNullOrEmpty(selectedFilePath))
			{
                logger.Info("Selected data import path '{0}'", selectedFilePath);

				this.Reset(selectedFilePath);
                this.txtImageDir.Text = (new FileInfo(selectedFilePath)).Directory.FullName;
                this.PreviewData();
			}
		}

		private void btnPreviewData_Click(object sender, RoutedEventArgs e)
		{
            logger.Info("Clicked Preview Data button in Data Import");
			this.PreviewData();
		}

		private void cmbSourceTables_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
            logger.Info("Changed Source Tables selection in Data Import");
			//if(_dataLoaded)
				this.PreviewData();
		}

		/// <summary>
		/// Loads the source data into the grid for review and column assignment
		/// </summary>
		private void PreviewData()
		{
            logger.Info("Loading import data into preview grid");

            try
            {
                if (!_dataSource.IsMsAccessSource && !_dataSource.IsMsExcelSource)
                {
                    this.Reset(txtSourceDataFileName.Text);
                }

                if (_dataSource == null)
                    return;
                
			    switch (_dataSource.SourceType)
			    {
				    case FlowImportDataSourceType.MsAccess:
					    _dataSource.SourceTable = cmbSourceTables.SelectedValue as string;
					    break;

				    case FlowImportDataSourceType.MsExcel:
					    _dataSource.SourceTable = cmbSourceTables.SelectedValue as string;
					    _dataSource.HasColumnHeaders = chkHasColumnHeaders.IsChecked.Value;
					    break;

				    case FlowImportDataSourceType.DelimitedText:
                        _dataSource.HasColumnHeaders = chkHasColumnHeaders.IsChecked.Value;
                        //_dataSource.IsTabDelimited = rdoIsTabDelimited.IsChecked.Value;
					    break;
			    }

			    _sourceDataTable = new DataTable();

                _sourceDataTable = _dataSource.Fill(_sourceDataTable);
               

			    ImportColumnStrategies columnStrategy;

			    if (rdoColumnAutomap.IsChecked.Value)
			    {
				    if (chkAutomapMatchedOnly.IsChecked.Value)
					    columnStrategy = ImportColumnStrategies.AutoMapMatchedOnly;
				    else
					    columnStrategy = ImportColumnStrategies.Automap;
			    }
			    else
				    columnStrategy = ImportColumnStrategies.ManualAssign;


			    this.ImportColumnMappings.Clear();
			    this.ImportColumnMappings.Import(
					    _sourceDataTable,
					    this.CurrentFlowProject.FlowProjectDataContext.ProjectSubjectFields.Where(s=>s.IsReadOnly != true || s.ProjectSubjectFieldDisplayName == "Ticket Code"),
					    columnStrategy
				    );

			    lsvColumnMappings.SelectedIndex = 0;

			    dgcImportData.Columns.Clear();
			    _view = new DataGridCollectionView(_sourceDataTable.DefaultView);
			    dgcImportData.ItemsSource = _view;


			    _dataLoaded = true;
            }
            catch (Exception e)
            {
                FlowMessageBox msg = new FlowMessageBox("Data Import", "There was an Error importing your data file.\n Please make sure your import file is formatted correctly");
                msg.CancelButtonVisible = false;
                msg.ShowDialog();
                logger.ErrorException("Exception occured while importing data.", e);
                _dataLoaded = false;
            }
            stkColumnMapMapWarnings.Visibility = Visibility.Visible;
            grdColumnMapMap.Visibility = Visibility.Visible;
		}


		private void uxColumnProperties_ColumnMappingContextMenuOpened(object sender, EventArgs<object> e)
		{
            logger.Info("Column mapping context menu opened in Data Import");
			lsvColumnMappings.SelectedItem = e.Data;
		}

		private void uxColumnProperties_SubjectFieldSelected(object sender, EventArgs<ISubjectField> e)
		{
			ImportColumnPair<ProjectSubjectField> mapping =
				(ImportColumnPair<ProjectSubjectField>)(sender as UserControl).DataContext;
            
			this.ImportColumnMappings.AssignDestinationField(mapping, e.Data);

            logger.Info("Column mapping selected in Data Import: source='{0}' destination='{1}'",
                mapping.SourceField.SubjectFieldDisplayName, mapping.DestinationField.SubjectFieldDisplayName);
		}

		private void btnCancelImport_Click(object sender, RoutedEventArgs e)
		{
            logger.Info("Clicked Cancel Import button in Data Import");
			this.FlowController.Edit();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnImport_Click(object sender, RoutedEventArgs e)
		{
            logger.Info("Clicked Import button in Data Import");

            _sourceDataTable = this.ImportColumnMappings.RemoveBlankLines(_sourceDataTable);
            string missingFieldMap = "";
            string badField = "";
            string badValue = "";
            if (this.ImportColumnMappings.TestForDuplicatesByKey(_sourceDataTable))
            {
                this.DuplicateKeyPanelVisibility = Visibility.Visible;
                return;
            }
            else if (this.TestForRequiredFieldsMapped(_sourceDataTable, out missingFieldMap))
            {
                this.MissingRequiredFieldPanelMessage.Text = "Flow has detected that the following required field has not been mapped: " + missingFieldMap + ". Please map this field before you import the data.";
                this.MissingRequiredFieldPanelVisibility = Visibility.Visible;
                return;
            }
            else if (this.TestForRequiredFieldsNotNull(_sourceDataTable, out badField))
            {
                this.MissingRequiredFieldPanelMessage.Text = "Flow has detected that some of the data has a null value in a required field: " + badField + ". Please correct this before you import the data.";
                this.MissingRequiredFieldPanelVisibility = Visibility.Visible;
                return;
            }
            else if (this.TestForBadDateValues(_sourceDataTable, out badField, out badValue))
            {
                this.MissingRequiredFieldPanelMessage.Text = "Flow has detected that some of the data in a Date Field has bad values: " + badField + " : " + badValue + ".\nPlease correct this before you import the data.";
                this.MissingRequiredFieldPanelVisibility = Visibility.Visible;
                return;
            }
			this.ImportPreferencesExpander.IsExpanded = false;

            // Disable the UI while the import is happening
            this.FlowController.ApplicationPanel.IsEnabled = false;

			// If there are no existing Subject recordsd...
            if (!this.CurrentFlowProject.HasSubjects)
            {
                ImportColumnMappings.ImportUnmatched = true;

                // Remove columns (and any data) from Subject table
                this.CurrentFlowProject.ClearSubjectTableColumns();

                // Delete from ProjectSubjectField table
                //this.CurrentFlowProject.ProjectSubjectFieldList.Clear();

                // Add specified column defintions to ProjectSubjectField table
                //foreach (ImportColumnPair<ProjectSubjectField> item in ImportColumnMappings)
                //{
                //    if (item.DestinationField != null)
                //    {
                //        // Ensure that the data-type assignment is consistent with the data;
                //        // manual string assignment overrides the source type in all cases
                //        if (
                //            !item.DestinationField.SubjectFieldDataType.IsString &&
                //            item.DestinationField.SubjectFieldDataType != item.SourceField.SubjectFieldDataType
                //        )
                //            item.DestinationField.SubjectFieldDataType = item.SourceField.SubjectFieldDataType;

                //        if (item.DestinationField.IsKeyField)
                //            ImportColumnMappings.KeyFields.Add(item);

                //        this.CurrentFlowProject.ProjectSubjectFieldList.Add(item.DestinationField);
                //    }
                //}

                // Save changes from prior operations
                this.CurrentFlowProject.Save();

                // Create Subject table columns as specified in the import 
                this.CurrentFlowProject.CreateSubjectTableColumns();
            }

            //we are never gonig to replace data

            //if (rdoReplaceData.IsChecked.Value)
            //{
            //    ImportColumnMappings.ImportUnmatched = true;

            //    // Remove columns (and any data) from Subject table
            //    //this.CurrentFlowProject.ClearSubjectTableColumns();
            //    this.CurrentFlowProject.SubjectList.Clear();
            //    this.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();
            //}
            //// If there are Subject records which will not be replaced...
            //else
			{
				ImportColumnMappings.ImportUnmatched = chkUpdateImportUnmatched.IsChecked.Value;
	
				foreach (ImportColumnPair<ProjectSubjectField> item in ImportColumnMappings)
				{
					if (item.DestinationField != null)
					{
						if (item.DestinationField.IsKeyField)
							ImportColumnMappings.KeyFields.Add(item);
					}
				}

			}

            //if (ImportColumnMappings.Any(m => m.DestinationFieldDisplayName == "Ticket Code"))
            //{
            //    ImportColumnMappings.KeyFields.Add(ImportColumnMappings["Ticket Code"]);
            //}

            //if (this.CurrentFlowProject.SubjectList.Count() > 0 && ImportColumnMappings.KeyFields.Count == 0)
            //{
            //    FlowMessageBox msg = new FlowMessageBox("Import Error", "When importing merge data, ticket code must be a field in the import file");
            //    msg.CancelButtonVisible = false;
            //    msg.ShowDialog();
            //    return;
            //}

            if (cmbFirstCriteria.SelectedValue != null && ImportColumnMappings.Any(m => m.DestinationFieldDisplayName == ((cmbFirstCriteria.SelectedValue as ImportColumnPair<ProjectSubjectField>).DestinationFieldDisplayName)) )
            {
                ImportColumnMappings.KeyFields.Add(ImportColumnMappings[(cmbFirstCriteria.SelectedValue as ImportColumnPair<ProjectSubjectField>).DestinationFieldDisplayName]);
            }
            if (cmbSecondCriteria.SelectedValue != null &&  chkSecondCriteria.IsChecked == true && ImportColumnMappings.Any(m => m.DestinationFieldDisplayName == (cmbSecondCriteria.SelectedValue as ImportColumnPair<ProjectSubjectField>).DestinationFieldDisplayName))
            {
                ImportColumnMappings.KeyFields.Add(ImportColumnMappings[(cmbSecondCriteria.SelectedValue as ImportColumnPair<ProjectSubjectField>).DestinationFieldDisplayName]);
            }
            if (cmbThirdCriteria.SelectedValue != null && chkThirdCriteria.IsChecked == true && chkThirdCriteria.IsEnabled == true && ImportColumnMappings.Any(m => m.DestinationFieldDisplayName == (cmbThirdCriteria.SelectedValue as ImportColumnPair<ProjectSubjectField>).DestinationFieldDisplayName))
            {
                ImportColumnMappings.KeyFields.Add(ImportColumnMappings[(cmbThirdCriteria.SelectedValue as ImportColumnPair<ProjectSubjectField>).DestinationFieldDisplayName]);
            }
            //if (this.CurrentFlowProject.SubjectList.Count() > 0 && ImportColumnMappings.KeyFields.Count == 0)
            //{
            //    FlowMessageBox msg = new FlowMessageBox("Import Error", "When importing merge data, ticket code must be a field in the import file");
            //    msg.CancelButtonVisible = false;
            //    msg.ShowDialog();
            //    return;
            //}
            BeginDataImport();
            //only important fields are the ones that got mapped
            if (ProjectController.CustomSettings.UpdateVisiblefieldsFromMappedDataImport == true)
            {
                foreach (ProjectSubjectField psf in this.FlowController.ProjectController.CurrentFlowProject.FlowProjectDataContext.ProjectSubjectFields)
                {
                    if ((ImportColumnMappings.Count(s => s.DestinationField != null && s.DestinationField.ProjectSubjectFieldDisplayName == psf.ProjectSubjectFieldDisplayName) > 0) ||
                        psf.ProjectSubjectFieldDisplayName == "Ticket Code")
                    {
                        if (this.ProjectController.CustomSettings.HideTicketCodeAndPackageSummaryInCapture && (psf.ProjectSubjectFieldDisplayName == "Ticket Code" || psf.ProjectSubjectFieldDisplayName == "Package Summary"))
                        {
                            psf.IsProminentField = false;
                            psf.IsShownInEdit = false;
                        }
                        else
                        {
                            psf.IsProminentField = true;
                            psf.IsShownInEdit = true;
                        }
                    }
                    else
                    {
                        psf.IsProminentField = false;
                        psf.IsShownInEdit = false;
                    }
                    this.FlowController.ProjectController.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();

                }
            }
		}

        public bool TestForRequiredFieldsMapped(DataTable dataTable, out string fieldName)
		{
            fieldName = "";
            foreach (ProjectSubjectField sField in this.ProjectController.CurrentFlowProject.ProjectSubjectFieldList)
            {
                if (sField.SubjectFieldDisplayName == "SubjectID" || sField.SubjectFieldDisplayName == "Package Summary" || sField.SubjectFieldDisplayName == "Ticket Code")
                    continue;
                if (sField.IsRequiredField)
                {
                    bool isMapped = false;
                    foreach (ImportColumnPair<ProjectSubjectField> keyField in this.ImportColumnMappings)
                    {
                        if (keyField.DestinationFieldDisplayName == sField.SubjectFieldDisplayName)
                            isMapped = true;
                    }
                    if (!isMapped)
                    {
                        fieldName = sField.SubjectFieldDisplayName;
                        return true;
                    }
                }
            }
            //if (this.KeyFields.Count == 0 || dataTable.Rows.Count < 2)
            //    return false;

            return false;
        }

        public bool TestForRequiredFieldsNotNull(DataTable dataTable, out string fieldName)
        {
            fieldName = "";
            foreach (ProjectSubjectField sField in this.ProjectController.CurrentFlowProject.ProjectSubjectFieldList)
            {
                if (sField.SubjectFieldDisplayName == "SubjectID" || sField.SubjectFieldDisplayName == "Package Summary" || sField.SubjectFieldDisplayName == "Ticket Code")

                    continue;
                if (sField.IsRequiredField)
                {
                    foreach (System.Data.DataRow row in dataTable.Rows)
                    {
                        string origColumnName = this.ImportColumnMappings[sField.SubjectFieldDisplayName].SourceField.SubjectFieldDisplayName.ToString();
                        string fieldValue = row[origColumnName].ToString();
                        if (fieldValue == null || fieldValue.Trim().Length < 1)
                        {
                            fieldName = sField.SubjectFieldDisplayName;
                            return true;
                        }
                    }
                }
            }
            //if (this.KeyFields.Count == 0 || dataTable.Rows.Count < 2)
            //    return false;

            return false;
        }
        public bool TestForBadDateValues(DataTable dataTable, out string fieldName, out string fieldValue)
        {
            fieldName = "";
            fieldValue = "";
            foreach (ProjectSubjectField sField in this.ProjectController.CurrentFlowProject.ProjectSubjectFieldList)
            {
                if (sField.SubjectFieldDisplayName == "SubjectID" || sField.SubjectFieldDisplayName == "Package Summary" || sField.SubjectFieldDisplayName == "Ticket Code")
                    continue;
                if (sField.SubjectFieldDataType == FieldDataType.FIELD_DATA_TYPE_DATETIME)
                {
                    foreach (System.Data.DataRow row in dataTable.Rows)
                    {
                        if (this.ImportColumnMappings.Any( icm => icm.DestinationFieldDisplayName == sField.SubjectFieldDisplayName))
                        {
                            string origColumnName = this.ImportColumnMappings[sField.SubjectFieldDisplayName].SourceField.SubjectFieldDisplayName.ToString();
                            string thisFieldValue = row[origColumnName].ToString();
                            DateTime dateTimeValue;
                            if (thisFieldValue != null && (thisFieldValue.Trim().Length > 0) && (DateTime.TryParse(thisFieldValue, out dateTimeValue) == false))
                            {
                                fieldName = sField.SubjectFieldDisplayName;
                                fieldValue = thisFieldValue;
                                return true;
                            }
                        }
                    }
                }

                if (sField.SubjectFieldDataType == FieldDataType.FIELD_DATA_TYPE_INTEGER)
                {
                    foreach (System.Data.DataRow row in dataTable.Rows)
                    {
                        if (this.ImportColumnMappings.Any(icm => icm.DestinationFieldDisplayName == sField.SubjectFieldDisplayName))
                        {
                            string origColumnName = this.ImportColumnMappings[sField.SubjectFieldDisplayName].SourceField.SubjectFieldDisplayName.ToString();
                            string thisFieldValue = row[origColumnName].ToString();
                            int tryResult;
                            if (thisFieldValue != null && (Int32.TryParse(thisFieldValue, out tryResult) == false))
                            {
                                fieldName = sField.SubjectFieldDisplayName;
                                fieldValue = thisFieldValue;
                                return true;
                            }
                        }
                    }
                }
            }
            //if (this.KeyFields.Count == 0 || dataTable.Rows.Count < 2)
            //    return false;

            return false;
        }

		private void ConfirmDuplicateImportButton_Click(object sender, RoutedEventArgs e)
		{
            logger.Info("Clicked Confirm Duplicate Import button in Data Import");

			this.DuplicateKeyPanelVisibility = Visibility.Collapsed;

			this.ImportColumnMappings.ImportKeyedDuplicates = true;

			this.BeginDataImport();
		}

		private void CancelDuplicateImportButton_Click(object sender, RoutedEventArgs e)
		{
            logger.Info("Clicked Cancel Duplicate Import button in Data Import");

			this.DuplicateKeyPanelVisibility = Visibility.Collapsed;
		}

        private void CancelMissingFieldImportButton_Click(object sender, RoutedEventArgs e)
        {
            logger.Info("Clicked Cancel Missing Field Import button in Data Import");

            this.MissingRequiredFieldPanelVisibility = Visibility.Collapsed;
        }

		private void BeginDataImport()
		{
            this.selectedImageDir = txtImageDir.Text;
            this.selectedImageField = "";
            if ((ImportColumnPair<ProjectSubjectField>)cmbImageField.SelectedValue != null)
                this.selectedImageField = ((ImportColumnPair<ProjectSubjectField>)cmbImageField.SelectedValue).SourceField.SubjectFieldDisplayName;

			IShowsNotificationProgress progressNotification = ObjectFactory.GetInstance<IShowsNotificationProgress>();

			NotificationProgressInfo info = new NotificationProgressInfo("Import Subject Data", "Importing...", 0);
			info.NotificationProgress = progressNotification;

			progressNotification.ShowNotification(info);
			info.Update();

			// Save the source data in the Subject table
			//Create thread which calls a function with paramters

           


			ImportThreadParameters param = new ImportThreadParameters();
			param.ImportColumnPairMappings = ImportColumnMappings;
			param.NotificationProgressInfo = info;

            

            BackgroundWorker importDataWorker = new BackgroundWorker();
            importDataWorker.DoWork += new DoWorkEventHandler(BeginDataImportThread);
            importDataWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(ImportDataWorker_RunWorkerCompleted);
            importDataWorker.RunWorkerAsync(param);
		}

       



        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
		private void BeginDataImportThread(object sender, DoWorkEventArgs e)
        {
            ImportThreadParameters param = (ImportThreadParameters)e.Argument;

                try
                {
                    
                    this.CurrentFlowProject.ImportData(param.ImportColumnPairMappings, _sourceDataTable, param.NotificationProgressInfo, this.Dispatcher, this.selectedImageDir, this.selectedImageField, ProjectController.CustomSettings.Force8x10Crop);
                    
                }
                catch (Exception ex)
                {
                    this.Dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                    {                
                        FlowMessageBox msg = new FlowMessageBox("Error Importing", ex.Message);
                        msg.CancelButtonVisible = false;
                        msg.ShowDialog();
                        logger.ErrorException("Error Importing", ex);
                    }));
                    //throw;
                }
                
        }        
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		void ImportDataWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            logger.Info("Data import worker completed");

            // Re-enable the UI now that the import is finished
            this.FlowController.ApplicationPanel.IsEnabled = true;

            this.ClearSelections();
            //this.FlowController.ProjectController.ReLoadCurrentProject();
            this.Dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
               {
                   //this.FlowController.ProjectController.Load(this.CurrentFlowProject, false);
                   this.FlowController.Capture();
                   this.ProjectController.UpdatedSavedSubjectCount();
                   //this.FlowMasterDataContext.FlowProjects.First(fp => fp.FlowProjectGuid == this.FlowMasterDataContext.FlowProjectList.CurrentItem.FlowProjectGuid).SavedSubjectCount = this.FlowMasterDataContext.FlowProjectList.CurrentItem.SubjectCount;
                   //this.FlowMasterDataContext.SubmitChanges();

               }));

			//// Reload the project to reflect changes
			//this.FlowController.ProjectController.Load(this.CurrentFlowProject);

			//this.CurrentFlowProject.SubjectList.SubjectDataLoaded +=
			//    delegate { this.FlowController.Capture(); };

            // Move app control to the Edit display
//            this.FlowController.Capture();
        }

		/// <summary>
		/// 
		/// </summary>
		class ImportThreadParameters
		{
			public ImportColumnPairCollection<ProjectSubjectField> ImportColumnPairMappings;
			public NotificationProgressInfo NotificationProgressInfo;
		}


		#region IMPORT COLUMN OPTION TOGGLING

		private void ToggleIsRequired_Click(object sender, RoutedEventArgs e)
		{
			bool toggleValue = false;

			var _mappingsWithDestinations =
				ImportColumnMappings.Where(m => m.DestinationField != null && !m.DestinationFieldIsName);

			foreach (ImportColumnPair<ProjectSubjectField> item in _mappingsWithDestinations)
			{
				if (!item.DestinationField.IsRequiredField)
				{
					toggleValue = true;
					break;
				}
			}

			foreach (ImportColumnPair<ProjectSubjectField> item in _mappingsWithDestinations)
			{
				item.DestinationField.IsRequiredField = toggleValue;
			}
		}

		private void IsScanKey_Check(object sender, RoutedEventArgs e)
		{
			CheckBox source = e.OriginalSource as CheckBox;

			if (source != null)
			{
				ProjectSubjectField item = source.Tag as ProjectSubjectField;

				if (item != null)
				{
					var _mappingsWithDestinations =
						ImportColumnMappings.Where(m => m.DestinationField != null && !m.DestinationFieldIsName);

					foreach (ImportColumnPair<ProjectSubjectField> field in _mappingsWithDestinations)
					{
						if (item.ProjectSubjectFieldDesc != field.DestinationField.ProjectSubjectFieldDesc)
							field.DestinationField.IsScanKeyField = false;
					}
				}
			}
		}

		private void ToggleIsProminentField_Click(object sender, RoutedEventArgs e)
		{
			bool toggleValue = false;

			var _mappingsWithDestinations =
				ImportColumnMappings.Where(m => m.DestinationField != null && !m.DestinationFieldIsName);

			foreach (ImportColumnPair<ProjectSubjectField> item in _mappingsWithDestinations)
			{
				if (!item.DestinationField.IsProminentField)
				{
					toggleValue = true;
					break;
				}
			}

			foreach (ImportColumnPair<ProjectSubjectField> item in _mappingsWithDestinations)
			{
				item.DestinationField.IsProminentField = toggleValue;
			}
		}

		private void ToggleIsSearchable_Click(object sender, RoutedEventArgs e)
		{
			bool toggleValue = false;

			var _mappingsWithDestinations =
				ImportColumnMappings.Where(m => m.DestinationField != null);

			foreach (ImportColumnPair<ProjectSubjectField> item in _mappingsWithDestinations)
			{
				if (!item.DestinationField.IsSearchableField)
				{
					toggleValue = true;
					break;
				}
			}

			foreach (ImportColumnPair<ProjectSubjectField> item in ImportColumnMappings)
			{
				item.DestinationField.IsSearchableField = toggleValue;
			}
		}

		#endregion

		private void DelimiterRadioButton_Checked(object sender, RoutedEventArgs e)
		{
			RadioButton source = sender as RadioButton;

            if (source != null && source.CommandParameter != null)
            {
                bool isCommaDelimited = (source.CommandParameter.ToString() == "True");

                if (isCommaDelimited != _commaDelimited && !String.IsNullOrEmpty(txtSourceDataFileName.Text))
                {
                    this.Reset(txtSourceDataFileName.Text);
                }

                _commaDelimited = isCommaDelimited;

                logger.Info("Delemiter radio button checked in Data Import: iscommaDelimited={0}", isCommaDelimited);
            }
		}

        private void chkHasColumnHeaders_Click(object sender, RoutedEventArgs e)
        {
            logger.Info("Clicked Has Column Headers checkbox in Data Import");
            this.PreviewData();
        }

        private void btnSelectImageDir_Click(object sender, RoutedEventArgs e)
        {
            logger.Info("Selecting Image Directory in Data Import");

            if (string.IsNullOrEmpty(txtSourceDataFileName.Text))
            {
                FlowMessageBox msg = new FlowMessageBox("Select Import File", "Please select an import file before you select an image directory");
                msg.CancelButtonVisible = false;
                msg.ShowDialog();
                return;
            }
            this.txtImageDir.Text = DialogUtility.SelectFolder((new FileInfo(txtSourceDataFileName.Text)).Directory.FullName, "Select Image Folder");

        }

        private void btnDeleteMapping_Click(object sender, RoutedEventArgs e)
        {
            ImportColumnPair<ProjectSubjectField> psf = ((sender as Button).DataContext as ImportColumnPair<ProjectSubjectField>);
            //psf.DestinationField = null;
            this.ImportColumnMappings.AssignDestinationField(psf, null);

            logger.Info("Clicked to delete mapping in Data Import: source='{0}' destination='{1}'", psf.SourceField, psf.DestinationField);
        }

	}	// END class

}	// END namespace
