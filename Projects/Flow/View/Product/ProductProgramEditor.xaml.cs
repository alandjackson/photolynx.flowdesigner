﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Lib.Helpers;
using Flow.Schema.LinqModel.DataContext;

namespace Flow.View.Product
{
	/// <summary>
	/// Interaction logic for ProductProgramEditor.xaml
	/// </summary>
	public partial class ProductProgramEditor : UserControl
	{
		public event EventHandler<EventArgs<ProductPackage>> AssignItemInvoked = delegate { };

		public ProductProgramEditor()
		{
			InitializeComponent();
		}

		//private void AddProductProgramButton_Click(object sender, RoutedEventArgs e)
		//{

		//}

		//private void SaveProductProgramButton_Click(object sender, RoutedEventArgs e)
		//{

		//}

		private void ProductPackageRecordDisplay_AssignItemInvoked(object sender, EventArgs<ProductPackage> e)
		{
			this.AssignItemInvoked(this, e);
		}
	}
}
