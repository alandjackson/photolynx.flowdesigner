﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Controller;
using Flow.Schema.LinqModel.DataContext;
using Flow.View.Project.Base;
using Flow.Lib;
using Flow.Schema.LinqModel.DataContext.FlowMaster;
using Flow.Controller.Project;
using NetServ.Net.Json;
using Flow.Controls.Model;
using Flow.Lib.Net;
using Flow.View.Dialogs;
using System.Data.SqlServerCe;
using NLog;

namespace Flow.View.Project
{
	/// <summary>
	/// Interaction logic for MainProjectPanel.xaml
	/// </summary>
	public partial class MainProjectPanel : ProjectPanelBase
	{
		protected List<Button> SidebarButtons = new List<Button>();


        private static Logger logger = LogManager.GetCurrentClassLogger();

        //protected ViewProjectsPanel _viewProjectsPanel;
        //public ViewProjectsPanel ViewProjectsPanel { get { return _viewProjectsPanel; } }

        //protected NewProjectPanel _newProjectPanel = null;
        //public NewProjectPanel NewProjectPanel { get { return _newProjectPanel; } }

        //protected ProjectImportPanel _projectImportPanel = null;
        //public ProjectImportPanel ProjectImportPanel { get { return _projectImportPanel; } }

        //protected ViewProjectTemplatesPanel _viewProjectTemplatesPanel = null;
        //public ViewProjectTemplatesPanel ViewProjectTemplatesPanel { get { return _viewProjectTemplatesPanel; } }

        //protected EditProjectTemplatePanel _editProjectTemplatesPanel = null;
        //public EditProjectTemplatePanel EditProjectTemplatePanel { get { return _editProjectTemplatesPanel; } }

        //protected ViewProductCatalogsPanel _viewProductCatalogsPanel = null;
        //public ViewProductCatalogsPanel ViewProductCatalogsPanel { get { return _viewProductCatalogsPanel; } }

        protected MultipleSubPanelManager SubPanelManager { get; set; }

		//protected DataImportPanel _dataImportPanel = null;
		//public DataImportPanel DataImportPanel { get { return _dataImportPanel; } }

		//protected TestCollectionNavigator _t = null;
		//public TestCollectionNavigator t { get { return _t; } }

		public MainProjectPanel()
		{
			InitializeComponent();

			SidebarButtons.Add(btnViewProjects);
			SidebarButtons.Add(btnCreateNewProject);
			SidebarButtons.Add(btnViewProjectTemplates);
			SidebarButtons.Add(btnCreateNewProjectTemplate);
			SidebarButtons.Add(btnImportProject);

            SubPanelManager = new MultipleSubPanelManager(SidebarButtons, uxContentPanel);
            SubPanelManager.PanelShown += new EventHandler<Flow.Lib.Helpers.EventArgs<UIElement>>(SubPanelManager_PanelShown);

            

        }

        void SubPanelManager_PanelShown(object sender, Flow.Lib.Helpers.EventArgs<UIElement> e)
        {
            ProjectPanelBase existingPnl = (ProjectPanelBase) e.Data;
            if(existingPnl.FlowController != this.FlowController)
                existingPnl.FlowController = this.FlowController;

            if (this.FlowMasterDataContext.PreferenceManager.Application.IsFlowServer)
            {
                FlowServerPanel.Visibility = Visibility.Visible;
            }

            if (this.FlowMasterDataContext.PreferenceManager.Application.EnableRemoteImageService)
                RemoteImageService.Visibility = Visibility.Visible;
            else
                RemoteImageService.Visibility = Visibility.Collapsed;

            if (!this.FlowMasterDataContext.PreferenceManager.Permissions.CanCreateNewProjects)
            {
                btnCreateNewProject.IsEnabled = false;
                imgCreateNewProjectLock.Visibility = Visibility.Visible;
                
            }
            else
            {
                btnCreateNewProject.IsEnabled = true;
                imgCreateNewProjectLock.Visibility = Visibility.Hidden;
            }

            if (!this.FlowMasterDataContext.PreferenceManager.Permissions.CanImportMergeProjects)
            {
                btnImportProject.IsEnabled = false;
                imgImportProjectLock.Visibility = Visibility.Visible;
                
            }
            else
            {
                btnImportProject.IsEnabled = true;
                imgImportProjectLock.Visibility = Visibility.Hidden;
            }

            if (!this.FlowMasterDataContext.PreferenceManager.Permissions.CanViewProjectTemplates)
            {
                btnViewProjectTemplates.IsEnabled = false;
                imgViewProjectTemplatesLock.Visibility = Visibility.Visible;
            }
            else
            {
                btnViewProjectTemplates.IsEnabled = true;
                imgViewProjectTemplatesLock.Visibility = Visibility.Hidden;
            }

            if (!this.FlowMasterDataContext.PreferenceManager.Permissions.CanAddEditDeleteProjectTemplates)
            {
                btnCreateNewProjectTemplate.IsEnabled = false;
                imgCreateProjectTemplatesLock.Visibility = Visibility.Visible;
            }
            else
            {
                btnCreateNewProjectTemplate.IsEnabled = true;
                imgCreateProjectTemplatesLock.Visibility = Visibility.Hidden;
            }
        }

		protected override void OnSetFlowController()
		{
			this.ProjectController.MainProjectPanel = this;
			ShowViewProjectsPanel();
       }

		private void btnViewProjects_Click(object sender, RoutedEventArgs e)
		{
			ShowViewProjectsPanel();
            
		}

		private void btnCreateNewProject_Click(object sender, RoutedEventArgs e)
		{
            //throw new Exception("forced error");

            //this.ProjectController.CreateOrderFormFields();
			this.ProjectController.CreateNewProject();
		}

        private void btnSearchAllTheProjects_Click(object sender, RoutedEventArgs e)
        {
            this.ProjectController.OpenSearchAllProjects();
        }


		private void btnImportProject_Click(object sender, RoutedEventArgs e)
		{
			this.ProjectController.ImportProject();
		}

		private void btnViewProjectTemplates_Click(object sender, RoutedEventArgs e)
		{
			ShowViewProjectTemplatesPanel();
		}

		private void btnCreateNewProjectTemplate_Click(object sender, RoutedEventArgs e)
		{
			this.ProjectController.CreateNewProjectTemplate();
		}

		private void btnViewFlowCatalogs_Click(object sender, RoutedEventArgs e)
		{
			ShowViewFlowCatalogsPanel();
		}

		private void btnCreateNewFlowCatalog_Click(object sender, RoutedEventArgs e)
		{
			this.ProjectController.CreateNewFlowCatalog();
		}

		private void btnImportImageQuixCatalogs_Click(object sender, RoutedEventArgs e)
		{
			this.ProjectController.ImportImageQuixCatalogs();
		}

		//private void btnImportData_Click(object sender, RoutedEventArgs e)
		//{
		//    this.ProjectController.ImportData();
		//}

		/// <summary>
		///  Displays a list of available Flow projects
		/// </summary>
        public ViewProjectsPanel ShowViewProjectsPanel()
		{
            ViewProjectsPanel pnl = SubPanelManager.ShowPanel<ViewProjectsPanel>(btnViewProjects);
            //_viewProjectsPanel = ShowPanel<ViewProjectsPanel>(btnViewProjects, _viewProjectsPanel);
            pnl.RefreshBindings();
            return pnl;
		}

		/// <summary>
		/// Displays the New Project config panel
		/// </summary>
		public void ShowNewProjectPanel()
		{
            SubPanelManager.KillPanel<NewProjectPanel>(btnCreateNewProject);
           NewProjectPanel pnl = SubPanelManager.ShowPanel<NewProjectPanel>(btnCreateNewProject);
				pnl.RefreshBindings();
			//_newProjectPanel = ShowPanel<NewProjectPanel>(btnCreateNewProject, _newProjectPanel);
		}

        public FlowServerPanel ShowFlowServerPanel()
        {
            FlowServerPanel pnl = SubPanelManager.ShowPanel<FlowServerPanel>(btnFlowServerPanel);
            if (pnl.DataContext == null)
                pnl.DataContext = this.FlowController;

            return pnl;
        } 

        public void ShowSearchAllProjects()
		{
            SubPanelManager.KillPanel<NewProjectPanel>(btnSearchAllTheProjects);
            SearchAllProjectsPanel pnl = SubPanelManager.ShowPanel<SearchAllProjectsPanel>(btnSearchAllTheProjects);
			pnl.RefreshBindings();
		}
		/// <summary>
		/// Displays the panel for editing project preferences
		/// </summary>
		public void ShowEditProjectPanel()
		{
			EditProjectPanel pnl = SubPanelManager.ShowPanel<EditProjectPanel>(null);
            if(pnl.FlowController == null || pnl.FlowController != this.FlowController)
                pnl.FlowController = this.FlowController;
			pnl.DataContext = this.ProjectController;
            pnl.uxProjectGeneralPanel.FlowController = this.FlowController;
            pnl.uxProjectGeneralPanel.RefreshBindings();
            pnl.uxProjectImageOptionsPanel.FlowController = this.FlowController;
            pnl.uxProjectImageOptionsPanel.RefreshBindings();
            pnl.uxEventTriggersPanel.FlowController = this.FlowController;
            //pnl.uxEventTriggersPanel.RefreshBindings();
		}

		/// <summary>
		/// Displayes the Project Import panel
		/// </summary>
		public void ShowProjectImportPanel()
		{
            ProjectImportPanel pnl = SubPanelManager.ShowPanel<ProjectImportPanel>(btnImportProject);
            pnl.showImport();
            
            //_projectImportPanel = ShowPanel<ProjectImportPanel>(btnImportProject, _projectImportPanel);
		}

        /// <summary>
        /// Displayes the Project Import panel
        /// </summary>
        public void ShowProjectExportPanel(FlowProject fp, bool forUpload)
        {
            ProjectImportPanel pnl = SubPanelManager.ShowPanel<ProjectImportPanel>(btnImportProject);
            pnl.DefaultProject = fp;
            pnl.showExport(forUpload);
            
            //_projectImportPanel = ShowPanel<ProjectImportPanel>(btnImportProject, _projectImportPanel);
        }


        public OrderPanel OrderPanel { get; set; }
        /// <summary>
        /// Displayes the Order panel
        /// </summary>
        public void ShowOrderPanel(FlowProject fp)
        {
            //if (OrderPanel != null)
            //    SubPanelManager.KillPanel<OrderPanel>(new object());

            OrderPanel = SubPanelManager.ShowPanel<OrderPanel>(btnImportProject);
            OrderPanel.DefaultProject = fp;
            OrderPanel.showOrder();
            OrderPanel.ApplyDefaults();
            OrderPanel.LoadAgain();

            //_projectImportPanel = ShowPanel<ProjectImportPanel>(btnImportProject, _projectImportPanel);
        }

        /// <summary>
        /// Displayes the Online Order panel
        /// </summary>
        public void ShowOnlineOrderPanel(FlowProject fp)
        {
            if(fp != null)
                ShowOnlineGallerySummaryPanel(1);
            else
                ShowOnlineGallerySummaryPanel(0);
            //OnlineOrderPanel pnl = SubPanelManager.ShowPanel<OnlineOrderPanel>(btnImportProject);
            //pnl.DefaultProject = fp;
            //pnl.showOnlineOrder();

            //_projectImportPanel = ShowPanel<ProjectImportPanel>(btnImportProject, _projectImportPanel);
        }

        public void ShowOnlineOrdersPanel(FlowProject fp)
        {
            ShowOnlineOrdersPanel(1);
            //OnlineOrderPanel pnl = SubPanelManager.ShowPanel<OnlineOrderPanel>(btnImportProject);
            //pnl.DefaultProject = fp;
            //pnl.showOnlineOrder();

            //_projectImportPanel = ShowPanel<ProjectImportPanel>(btnImportProject, _projectImportPanel);
        }


        public void ShowRemoteImageServiceStatus(FlowProject fp)
        {
            ShowRemoteImageServiceStatus(0);
        }
        
		/// <summary>
		/// Displayes the Project Import panel
		/// </summary>
		public void ShowProjectImportPanel(FlowProject flowProject, bool forUpload)
		{
			SubPanelManager.ShowPanel<ProjectImportPanel>(btnImportProject);

			//_projectImportPanel = ShowPanel<ProjectImportPanel>(btnImportProject, _projectImportPanel);
		}

		/// <summary>
		/// Displays a list of available Flow project templates
		/// </summary>
		public void ShowViewProjectTemplatesPanel()
		{
            SubPanelManager.ShowPanel<ViewProjectTemplatesPanel>(btnViewProjectTemplates);
            //_viewProjectTemplatesPanel = ShowPanel<ViewProjectTemplatesPanel>(btnViewProjectTemplates, _viewProjectTemplatesPanel);
		}

		/// <summary>
		/// Displays an editor creating/modifying project templates
		/// </summary>
		public void ShowEditProjectTemplatePanel()
		{
            
           EditProjectTemplatePanel pnl = SubPanelManager.ShowPanel<EditProjectTemplatePanel>(btnCreateNewProjectTemplate);
           pnl.uxEventTriggersPanel.DataContext = this.ProjectController.CurrentProjectTemplate;
           pnl.uxImageOptionsPanel.UpdateDefaults();
            //_editProjectTemplatesPanel = ShowPanel<EditProjectTemplatePanel>(btnCreateNewProjectTemplate, _editProjectTemplatesPanel);
		}

		/// <summary>
		/// Displays an editor creating/modifying project templates
		/// </summary>
		public void ShowEditProjectTemplatePanel(Flow.Schema.LinqModel.DataContext.ProjectTemplate projectTemplate)
		{
            this.ProjectController.CurrentProjectTemplate = projectTemplate;
            SubPanelManager.ShowPanel<EditProjectTemplatePanel>(btnCreateNewProjectTemplate);
            //_editProjectTemplatesPanel = ShowPanel<EditProjectTemplatePanel>(btnCreateNewProjectTemplate, _editProjectTemplatesPanel);
		}

		//public void ShowDataImportPanel()
		//{
		//    _dataImportPanel = ShowPanel<DataImportPanel>(btnImportData, _dataImportPanel);
		//}

		/// <summary>
		/// Displays the list of existing product packages
		/// </summary>
		public void ShowViewFlowCatalogsPanel()
		{
            SubPanelManager.ShowPanel<ViewFlowCatalogsPanel>(btnViewFlowCatalogs);
            //_viewProductCatalogsPanel = ShowPanel<ViewProductCatalogsPanel>(btnViewProductCatalogs, _viewProductCatalogsPanel);
		}

		internal void ShowFlowCatalogEditor()
		{
			this.FlowController.Catalog();
		}

		internal void ShowImportImageQuixCatalogs()
		{
			this.FlowController.ImageQuixCatalog();
		}

        private void btnOpenOnlineGallerySummary_Click(object sender, RoutedEventArgs e)
        {
            OnlineGallerySummaryPanel pnl = ShowOnlineGallerySummaryPanel(0);
            //pnl.CheckForOnlineOrders();
        }

        private OnlineGallerySummaryPanel ShowOnlineGallerySummaryPanel(int defaultTab)
        {
            //show the panel
            OnlineGallerySummaryPanel pnl = SubPanelManager.ShowPanel<OnlineGallerySummaryPanel>(new Button());
            pnl.FlowController = this.FlowController;
            pnl.setTabFocus(defaultTab);
            return pnl;
            
            //pnl.FlowMasterDataContext = this.FlowMasterDataContext;
        }

        public OnlineOrdersPanel OnlineOrdersPanel { get; set; }
        private OnlineOrdersPanel ShowOnlineOrdersPanel(int defaultTab)
        {
           // if (OnlineOrdersPanel != null)
           //     SubPanelManager.KillPanel<OnlineOrdersPanel>(new object());

            //show the panel
            OnlineOrdersPanel = SubPanelManager.ShowPanel<OnlineOrdersPanel>(new Button());
            OnlineOrdersPanel.FlowController = this.FlowController;
            //pnl.setTabFocus(defaultTab);
            return OnlineOrdersPanel;

            //pnl.FlowMasterDataContext = this.FlowMasterDataContext;
        }


        private RemoteImageServicePanel ShowRemoteImageServiceStatus(int defaultTab)
        {
            //show the panel
            RemoteImageServicePanel pnl = SubPanelManager.ShowPanel<RemoteImageServicePanel>(btnOpenRemoteImageService);
            pnl.FlowController = this.FlowController;
            pnl.DataContext = this.CurrentFlowProject;
            pnl.setTabFocus(defaultTab);
            return pnl;
            
            //pnl.FlowMasterDataContext = this.FlowMasterDataContext;
        }

        
        private void _this_Loaded(object sender, RoutedEventArgs e)
        {
#if !DEBUG_OFFLINE


            if (this.FlowController == null)
                logger.Error("FlowController is null");
            else if (this.FlowController.ProjectController == null)
                logger.Error("FlowController ProjectController is null");
            else if (this.FlowController.ProjectController.FlowMasterDataContext == null)
                logger.Error("FlowController.ProjectController.FlowMasterDataContext is null");
            else if (this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager == null)
                logger.Error("FlowController.ProjectController.FlowMasterDataContext.PreferenceManager is null");
            else if (this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application == null)
                logger.Error("this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application is null");


             string pingTestURL = "http://www.google.com";
            try
            {
                pingTestURL = this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.PingTestURL;
            }
            catch (Exception ex)
            {
                    logger.Error("Cant get pingTestURL: " + ex.StackTrace);
            }
             if ((Emailer.CheckForInternetConnection() || Emailer.PingServer(pingTestURL)) && 
                 this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.IsNotBasic)
             {
                 if ((this.ProjectController.FlowMasterDataContext.PreferenceManager.Application.LastCurrentVersion == "" ||
                this.ProjectController.FlowMasterDataContext.PreferenceManager.Application.LastCurrentVersion != FlowContext.GetFlowVersion()))
                 {
                     this.ProjectController.FlowMasterDataContext.PreferenceManager.Application.LastCurrentVersion = FlowContext.GetFlowVersion();
                     this.ProjectController.FlowMasterDataContext.SavePreferences();
                     SubPanelManager.ShowPanel<FlowChangeLogPanel>(this);
                 }
             }
#endif
             if (this.ProjectController.FlowMasterDataContext.PreferenceManager.Application.IsFlowServer)
             {
                 
                 FlowServerPanel pnl = SubPanelManager.ShowPanel<FlowServerPanel>(this);
                 if (pnl.DataContext == null)
                     pnl.DataContext = this.FlowController;
             }
            
        }

        private void btnFlowChangesWebPage_Click(object sender, RoutedEventArgs e)
        {
            string pingTestURL = this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.PingTestURL;
            if (Emailer.CheckForInternetConnection() || Emailer.PingServer(pingTestURL))
            {
                SubPanelManager.ShowPanel<FlowChangeLogPanel>(this);
            }
            else
            {
                FlowMessageBox msg = new FlowMessageBox("Not Connected", "You need to be connected to the Internet to see the change log");
                msg.CancelButtonVisible = false;
                msg.ShowDialog();

            }
        }

        private void btnFlowServerPanel_Click(object sender, RoutedEventArgs e)
        {
            FlowServerPanel pnl = SubPanelManager.ShowPanel<FlowServerPanel>(this);
            if(pnl.DataContext == null)
                pnl.DataContext = this.FlowController;
        }

        private void btnLabUploadStatusPanel_Click(object sender, RoutedEventArgs e)
        {
            ShowLabUploadStatusPanel();
        }


  






        ///// <summary>
        ///// Enables/disables sidebar menu buttons depending on the context
        ///// </summary>
        ///// <param name="sender"></param>
        //private void EnableSender(object sender)
        //{
        //    foreach (Button btn in SidebarButtons)
        //        btn.IsEnabled = !(sender == btn);
        //}

        ///// <summary>
        ///// Shows panels based upon user selection
        ///// </summary>
        ///// <typeparam name="T"></typeparam>
        ///// <param name="sender"></param>
        ///// <param name="existingPnl"></param>
        ///// <returns></returns>
        //private T ShowPanel<T>(object sender, T existingPnl) where T : ProjectPanelBase, new()
        //{
        //    if (sender != null)
        //        EnableSender(sender);

        //    if (existingPnl == null)
        //    {
        //        existingPnl = new T();
        //        uxContentPanel.Children.Add(existingPnl);
        //        existingPnl.FlowController = this.FlowController;
        //    }

        //    // show the test panel
        //    foreach (UIElement child in uxContentPanel.Children)
        //    {
        //        child.Visibility =
        //            (child == existingPnl ? Visibility.Visible : Visibility.Hidden);
        //    }

        //    return existingPnl;
        //}



        internal void ShowLabUploadStatusPanel()
        {
            LabUploadStatusPanel pnl = SubPanelManager.ShowPanel<LabUploadStatusPanel>(this);
            if (pnl.DataContext == null)
                pnl.DataContext = this.FlowController;
            if (pnl.LabOrderUploader == null)
                pnl.LabOrderUploader = this.FlowController.LabOrderUploader;
        }

        private void btnOpenRemoteImageService_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
