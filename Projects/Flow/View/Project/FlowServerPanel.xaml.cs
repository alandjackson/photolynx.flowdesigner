using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Controls;
using Flow.Controller.Project;
using Flow.Lib;
using Flow.Lib.Helpers;
using Flow.Schema.LinqModel.DataContext;
using Flow.View.Project.Base;
using Flow.View.Preferences;

using StructureMap;
using System.Drawing;
using Flow.Designer.Lib.ImageAdjustments.Model;
using Flow.Designer.Lib.Service;
using System.Windows.Threading;
using Flow.View.Dialogs;
using Flow.Schema.Reports;
using Flow.Controller.Reports;
using C1.C1Report;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.InteropServices;
using Flow.Controller;

namespace Flow.View.Project
{
	/// <summary>
	/// Interaction logic for OnlineOrderPanel.xaml
	/// </summary>
	public partial class FlowServerPanel : ProjectPanelBase
	{
        private bool diddrop = false;
        public FlowServerPanel()
		{
			InitializeComponent();
            this.DataContextChanged += new DependencyPropertyChangedEventHandler(FlowServerPanel_DataContextChanged);
            this.IncomingList.AllowDrop = true;
            this.QCList.AllowDrop = true;

            Style itemContainerStyle = new Style(typeof(ListBoxItem));
            itemContainerStyle.Setters.Add(new Setter(ListBoxItem.AllowDropProperty, true));
            itemContainerStyle.Setters.Add(new EventSetter(ListBoxItem.PreviewMouseLeftButtonDownEvent, new MouseButtonEventHandler(s_PreviewMouseLeftButtonDown)));
            itemContainerStyle.Setters.Add(new EventSetter(ListBoxItem.DropEvent, new DragEventHandler(listbox1_Drop)));
            IncomingList.ItemContainerStyle = itemContainerStyle;


            Style itemContainerStyleQC = new Style(typeof(ListBoxItem));
            itemContainerStyleQC.Setters.Add(new Setter(ListBoxItem.AllowDropProperty, true));
            itemContainerStyleQC.Setters.Add(new EventSetter(ListBoxItem.PreviewMouseLeftButtonDownEvent, new MouseButtonEventHandler(s_PreviewMouseLeftButtonDown)));
            itemContainerStyleQC.Setters.Add(new EventSetter(ListBoxItem.DropEvent, new DragEventHandler(listbox2_Drop)));
            QCList.ItemContainerStyle = itemContainerStyleQC;


		}

        void FlowServerPanel_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            IncomingList.ItemsSource = (this.DataContext as FlowController).ProjectController.IncomingClientProjects;
            QCList.ItemsSource = (this.DataContext as FlowController).ProjectController.IncomingClientProjectsQC;

            
           
        }

        void s_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

            if (sender is ListBoxItem)
            {
                ListBoxItem draggedItem = sender as ListBoxItem;
                DragDrop.DoDragDrop(draggedItem, draggedItem.DataContext, DragDropEffects.Move);
                draggedItem.IsSelected = true;
            }
        }

        //these are going in the incoming folder
        void listbox1_Drop(object sender, DragEventArgs e)
        {
            diddrop = true;
            
            IncomingProjectFile target = ((ListBoxItem)(sender)).DataContext as IncomingProjectFile;
            int targetIdx = IncomingList.Items.IndexOf(target);
           
            IncomingProjectFile droppedData = e.Data.GetData(typeof(IncomingProjectFile)) as IncomingProjectFile;
           

            int removedIdx = IncomingList.Items.IndexOf(droppedData);
            

            ObservableCollection<IncomingProjectFile> oldList = (this.DataContext as FlowController).ProjectController.IncomingClientProjects;
            ObservableCollection<IncomingProjectFile> newList = (this.DataContext as FlowController).ProjectController.IncomingClientProjects;

            if (new FileInfo(droppedData.FilePath).Directory.Name == "qc")
            {
                removedIdx = QCList.Items.IndexOf(droppedData);
                oldList = (this.DataContext as FlowController).ProjectController.IncomingClientProjectsQC;
                string incomingDir = System.IO.Path.Combine((new FileInfo(droppedData.FilePath)).Directory.Parent.FullName, "incoming");
                string oldPath = droppedData.FilePath;
                string newPath = System.IO.Path.Combine(incomingDir, droppedData.FileName);
                droppedData = new IncomingProjectFile(this.FlowController, newPath);
                if (!Directory.Exists(incomingDir))
                    Directory.CreateDirectory(incomingDir);
                if (File.Exists(newPath))
                    File.Move(newPath, newPath.Replace(".sdf", "_" + TicketGenerator.GenerateTicketString(4) + ".sdf"));

                File.Move(oldPath, newPath);
                return;
            }


            if (removedIdx < targetIdx)
            {
                newList.Insert(targetIdx + 1, droppedData);
                oldList.RemoveAt(removedIdx);
            }
            else
            {
                int remIdx = removedIdx + 1;
                if (newList.Count + 1 > remIdx && targetIdx >= 0)
                {
                    newList.Insert(targetIdx, droppedData);
                    oldList.RemoveAt(remIdx);
                }
            }
        }


        void Incoming_Drop(object sender, DragEventArgs e)
        {
            if (diddrop)
            {
                diddrop = false;
                return;
            }
            int targetIdx = (this.DataContext as FlowController).ProjectController.IncomingClientProjects.Count() - 1;
            if (targetIdx < 0)
                targetIdx = 0;

            IncomingProjectFile droppedData = e.Data.GetData(typeof(IncomingProjectFile)) as IncomingProjectFile;


            int removedIdx = IncomingList.Items.IndexOf(droppedData);


            ObservableCollection<IncomingProjectFile> oldList = (this.DataContext as FlowController).ProjectController.IncomingClientProjects;
            ObservableCollection<IncomingProjectFile> newList = (this.DataContext as FlowController).ProjectController.IncomingClientProjects;

            if (new FileInfo(droppedData.FilePath).Directory.Name == "qc")
            {
                removedIdx = QCList.Items.IndexOf(droppedData);
                oldList = (this.DataContext as FlowController).ProjectController.IncomingClientProjectsQC;
                string incomingDir = System.IO.Path.Combine((new FileInfo(droppedData.FilePath)).Directory.Parent.FullName, "incoming");
                string oldPath = droppedData.FilePath;
                string newPath = System.IO.Path.Combine(incomingDir, droppedData.FileName);
                droppedData = new IncomingProjectFile(this.FlowController, newPath);
                if (!Directory.Exists(incomingDir))
                    Directory.CreateDirectory(incomingDir);
                if (File.Exists(newPath))
                    File.Move(newPath, newPath.Replace(".sdf", "_" + TicketGenerator.GenerateTicketString(4) + ".sdf"));
                File.Move(oldPath, newPath);
                return;
            }


            if (removedIdx < targetIdx)
            {
                newList.Insert(targetIdx + 1, droppedData);
                oldList.RemoveAt(removedIdx);
            }
            else
            {
                int remIdx = removedIdx ;
                if (newList.Count + 1 > remIdx && targetIdx >= 0)
                {
                    newList.Insert(targetIdx, droppedData);
                    oldList.RemoveAt(remIdx);
                }
            }
        }


        //these are going in the qc folder
        void listbox2_Drop(object sender, DragEventArgs e)
        {
            diddrop = true;

            IncomingProjectFile target = ((ListBoxItem)(sender)).DataContext as IncomingProjectFile;
            int targetIdx = QCList.Items.IndexOf(target);

            IncomingProjectFile droppedData = e.Data.GetData(typeof(IncomingProjectFile)) as IncomingProjectFile;


            int removedIdx = QCList.Items.IndexOf(droppedData);


            ObservableCollection<IncomingProjectFile> oldList = (this.DataContext as FlowController).ProjectController.IncomingClientProjectsQC;
            ObservableCollection<IncomingProjectFile> newList = (this.DataContext as FlowController).ProjectController.IncomingClientProjectsQC;

            if (new FileInfo(droppedData.FilePath).Directory.Name == "incoming")
            {
                removedIdx = IncomingList.Items.IndexOf(droppedData);
                oldList = (this.DataContext as FlowController).ProjectController.IncomingClientProjects;
                string qcDir = System.IO.Path.Combine((new FileInfo(droppedData.FilePath)).Directory.Parent.FullName, "qc");
                string oldPath = droppedData.FilePath;
                string newPath = System.IO.Path.Combine(qcDir, droppedData.FileName);
                droppedData = new IncomingProjectFile(this.FlowController, newPath);
                if (!Directory.Exists(qcDir))
                    Directory.CreateDirectory(qcDir);
                if (File.Exists(newPath))
                    File.Move(newPath, newPath.Replace(".sdf", "_" + TicketGenerator.GenerateTicketString(4) + ".sdf"));
                File.Move(oldPath, newPath);
                return;
            }


            if (removedIdx < targetIdx)
            {
                newList.Insert(targetIdx + 1, droppedData);
                oldList.RemoveAt(removedIdx);
            }
            else
            {
                int remIdx = removedIdx + 1;
                if (newList.Count + 1 > remIdx && targetIdx >= 0)
                {
                    newList.Insert(targetIdx, droppedData);
                    oldList.RemoveAt(remIdx);
                }
            }
        }

        void qc_Drop(object sender, DragEventArgs e)
        {
            if (diddrop)
            {
                diddrop = false;
                return;
            }
            int targetIdx = (this.DataContext as FlowController).ProjectController.IncomingClientProjectsQC.Count() - 1;
            if (targetIdx < 0)
                targetIdx = 0;

            IncomingProjectFile droppedData = e.Data.GetData(typeof(IncomingProjectFile)) as IncomingProjectFile;


            int removedIdx = QCList.Items.IndexOf(droppedData);

            ObservableCollection<IncomingProjectFile> oldList = (this.DataContext as FlowController).ProjectController.IncomingClientProjectsQC;
            ObservableCollection<IncomingProjectFile> newList = (this.DataContext as FlowController).ProjectController.IncomingClientProjectsQC;

            if (new FileInfo(droppedData.FilePath).Directory.Name == "incoming")
            {
                removedIdx = IncomingList.Items.IndexOf(droppedData);
                oldList = (this.DataContext as FlowController).ProjectController.IncomingClientProjects;
                string qcDir = System.IO.Path.Combine((new FileInfo(droppedData.FilePath)).Directory.Parent.FullName, "qc");
                string oldPath = droppedData.FilePath;
                string newPath = System.IO.Path.Combine(qcDir, droppedData.FileName);
                droppedData = new IncomingProjectFile(this.FlowController, newPath);
                if (!Directory.Exists(qcDir))
                    Directory.CreateDirectory(qcDir);

                if (File.Exists(newPath))
                    File.Move(newPath, newPath.Replace(".sdf", "_" + TicketGenerator.GenerateTicketString(4) + ".sdf"));
                File.Move(oldPath, newPath);
                return;
            }


            if (removedIdx < targetIdx)
            {
                newList.Insert(targetIdx + 1, droppedData);
                oldList.RemoveAt(removedIdx);
            }
            else
            {
                int remIdx = removedIdx + 1;
                if (newList.Count + 1 > remIdx && targetIdx >= 0)
                {
                    newList.Insert(targetIdx, droppedData);
                    oldList.RemoveAt(remIdx);
                }
            }
        }


	}
}
