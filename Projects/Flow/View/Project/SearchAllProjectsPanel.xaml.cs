﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Controller;
using Flow.Controller.Project;
using Flow.Schema.LinqModel.DataContext;
using Flow.View.Project.Base;
using Flow.View.Preferences;
using Flow.Lib;
using System.Threading;
using Flow.View.Dialogs;
using System.Data.SqlServerCe;
using NLog;

namespace Flow.View.Project
{
	/// <summary>
	/// Interaction logic for NewProjectPanel.xaml
	/// </summary>
    public partial class SearchAllProjectsPanel : ProjectPanelBase
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public SearchAllProjectsPanel()
        {
            InitializeComponent();
            txtSearchAllProjects.Focus();
        }

        protected override void OnSetFlowController()
        {
            this.DataContext = this.ProjectController;
            txtSearchAllProjects.Focus();
        }

        /// <summary>
        /// Resets the SelectedValue of comboxes to null
        /// </summary>
        public void RefreshBindings()
        {
            txtSearchAllProjects.Focus();
        }


        private void btnSearchAllProjects_Click(object sender, RoutedEventArgs e)
        {
            logger.Info("Clicked Search All Projects button");

            bool isbarcodescan = false;
            FlowProject barcodeProject = null;
            string barcodeTicketCode = "";

            string term = txtSearchAllProjects.Text.ToLower();
            if (term.StartsWith(".nst."))
            {
                term = term.Replace(".nst.", "");
                isbarcodescan = true;
            }

            if (term.Length < 1)
                return;
            List<AllProjectSearchResult> theList = this.ProjectController.AllProjectsSearchResults;
            theList.Clear();

            foreach (FlowProject proj in this.FlowMasterDataContext.FlowProjects)
            {
                proj.FlowMasterDataContext = this.FlowMasterDataContext;
                string connectString = proj.ConnectionString;
                SqlCeConnection cn = new SqlCeConnection(connectString);
                cn.Open();
                SqlCeCommand command = new SqlCeCommand("select FirstName, LastName, TicketCode from Subject where (LOWER(FirstName) like @likeTerm or LOWER(LastName) like @likeTerm  or LOWER(TicketCode) = @term)", cn);
                command.Parameters.AddWithValue("@likeTerm", "%"+term+"%");
                command.Parameters.AddWithValue("@term", term);

                try
                {
                    SqlCeResultSet rs = command.ExecuteResultSet(new ResultSetOptions());
                    while (rs.Read())
                    {

                        string first = rs.GetString(0);
                        string last = rs.GetString(1);
                        string ticket = rs.GetString(2);
                        theList.Add(new AllProjectSearchResult(first, last, proj, proj.FlowProjectName, ticket));
                        if (isbarcodescan)
                        {
                            barcodeProject = proj;
                            barcodeTicketCode = ticket;
                        }

                    }
                }
                catch (Exception ex)
                {
                    //do nothing
                }
                cn.Close();
                cn.Dispose();
            }
            this.ProjectController.AllProjectsSearchResults = theList.OrderBy(p => p.LastName).ToList();
            this.ProjectController.UpdateAllProjectsSearchResults();
            logger.Info("Search All Subjects returned {0} results for term '{1}'", theList.Count, term);
            lstSearchResults.ItemsSource = this.ProjectController.AllProjectsSearchResults;
            lstSearchResults.UpdateLayout();
            if (isbarcodescan && barcodeProject != null)
            {
                OpenAndGoto(barcodeProject, barcodeTicketCode);
                
            }
            txtSearchAllProjects.Text = "";
            txtSearchAllProjects.Focus();
        }

        private void FullName_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {

        }


        private void ProjectColumnHeader_Click(object sender, RoutedEventArgs e)
        {
            this.ProjectController.AllProjectsSearchResults = this.ProjectController.AllProjectsSearchResults.OrderBy(p => p.ProjectName).ThenBy(p => p.LastName).ToList();
            this.ProjectController.UpdateAllProjectsSearchResults();
            lstSearchResults.ItemsSource = this.ProjectController.AllProjectsSearchResults;
            lstSearchResults.UpdateLayout();
        }

        private void SubjectColumnHeader_Click(object sender, RoutedEventArgs e)
        {
            this.ProjectController.AllProjectsSearchResults = this.ProjectController.AllProjectsSearchResults.OrderBy(p => p.LastName).ToList();
            this.ProjectController.UpdateAllProjectsSearchResults();
            lstSearchResults.ItemsSource = this.ProjectController.AllProjectsSearchResults;
            lstSearchResults.UpdateLayout();
        }

        private void OpenAndGoto(FlowProject newFlowProject, string newTicketCode)
        {
            if (newFlowProject == this.CurrentFlowProject)
            {
                this.ProjectController.OpenCapture();
                this.ProjectController.GotoTicketID(newTicketCode);
            }
            else
            {
                this.ProjectController.Load(newFlowProject);
                //wait for it to load
                if (this.ProjectController.CurrentFlowProject != null)
                {
                    this.ProjectController.CurrentFlowProject.FlowProjectDataContext.SubjectDataLoaded += delegate
                    {
                        this.ProjectController.OpenCapture();
                        this.ProjectController.GotoTicketID(newTicketCode);
                    };
                }
            }
        }
        private void btnOpen_Click(object sender, RoutedEventArgs e)
        {
            FlowProject newFlowProject = ((sender as Button).DataContext as AllProjectSearchResult).FlowProject;
            string ticketCode = ((sender as Button).DataContext as AllProjectSearchResult).TicketCode;

            logger.Info("Clicked Open button, going to ticket code {0}", ticketCode);

            OpenAndGoto(newFlowProject, ticketCode);
            txtSearchAllProjects.Focus();
        }
    }
}
