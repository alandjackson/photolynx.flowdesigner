﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Controller;
using Flow.Schema.LinqModel.DataContext;
using Flow.View.Project.Base;

namespace Flow.View.Project
{
	/// <summary>
	/// Interaction logic for ProjectTemplatePanel.xaml
	/// </summary>
	public partial class EditProjectPanel : ProjectPanelBase
	{
		public EditProjectPanel()
		{
			InitializeComponent();
		}

		protected override void OnSetFlowController()
		{
			this.FlowController.ProjectController.PropertyChanged +=
				new PropertyChangedEventHandler(ProjectController_PropertyChanged);

			uxProjectImageOptionsPanel.FlowController = this.FlowController;
	
			//uxGeneralPanel.ProjectController = this.FlowController.ProjectController;
			//uxSubjectFieldPanel.ProjectController = this.FlowController.ProjectController;
			//uxSubjectPanel.ProjectController = this.FlowController.ProjectController;
			//uxOrganizationPanel.ProjectController = this.FlowController.ProjectController;
			//uxImageOptionsPanel.ProjectController = this.FlowController.ProjectController;
//			uxEventTriggersPanel.ProjectController = this.FlowController.ProjectController;
			//uxBarcodeSettingsPanel.ProjectController = this.FlowController.ProjectController;

//			uxEventTriggersPanel.DataContext = this.FlowController.ProjectController.CurrentProjectTemplate;
			//uxBarcodeSettingsPanel.DataContext = this.FlowController.ProjectController.CurrentProjectTemplate;
		}

		private void btnSaveProjectPreferences_Click(object sender, RoutedEventArgs e)
		{
            this.uxProjectImageOptionsPanel.SetProjectFilenameFormatFields();

            if (this.ProjectController.CurrentFlowProject.SelectedGroupImageFlags != null && this.ProjectController.CurrentFlowProject.SelectedGroupImageFlags.Count > 0)
                this.ProjectController.CurrentFlowProject.GroupImageFlags = String.Join(";", this.ProjectController.CurrentFlowProject.SelectedGroupImageFlags);

			//uxImageOptionsPanel.SetProjectFilenameFormatFields();
			//this.ProjectController.SaveProjectTemplate();
			//uxOrganizationPanel.SaveOrgStructureFromTreeView();
            if (this.ProjectController.CurrentFlowProject != null)
            {
                //if((int)uxProjectGeneralPanel.cmbPhotographer.SelectedIndex > 0)
                //    this.ProjectController.CurrentFlowProject.OwnerUserID = (int)uxProjectGeneralPanel.cmbPhotographer.SelectedValue;

                this.ProjectController.CurrentFlowProject.Save();
                this.ProjectController.FlowMasterDataContext.SavePreferences();
                this.ProjectController.MainProjectPanel.ShowViewProjectsPanel();
            }


            this.CurrentFlowProject._imageFileNameFormatter = new Flow.Schema.ImageFileNameFormatter(this.CurrentFlowProject);

            
		}

		private void btnCancelProjectEdit_Click(object sender, RoutedEventArgs e)
		{
			//this.CurrentFlowProject.FlowProjectDataContext.Revert();

			//this.CurrentFlowProject.Revert();
//			this.ProjectController.CurrentProjectTemplate = null;
            this.ProjectController.FlowMasterDataContext.Revert();
			this.FlowController.Project();
            this.ProjectController.MainProjectPanel.ShowViewProjectsPanel();
		}

		//private void btnNewProjectFromTemplate_Click(object sender, RoutedEventArgs e)
		//{
		//    this.ProjectController.CreateNewProject();
		//}

		private void ProjectController_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			// Ensures that the first tab is selected when the selection of a Project Template occurs
            if (e.PropertyName == "CurrentFlowProject")
            {
                tabEditProjectTemplate.SelectedIndex = 0;
                //this.uxProjectImageOptionsPanel.UpdateDefaults();
            }
		}
	
	}
}
