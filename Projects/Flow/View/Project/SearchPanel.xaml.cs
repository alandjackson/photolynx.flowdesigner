﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Flow.View.Project.Base;
using Flow.Schema.LinqModel.DataContext;
using Flow.Controller;
using Flow.Controller.Project;
using System.Data;
using System.Globalization;
using System.Text.RegularExpressions;
using Flow.View.Dialogs;
using NLog;

namespace Flow.View.Project
{
    /// <summary>
    /// Interaction logic for FilterPanel.xaml
    /// </summary>
    public partial class SearchPanel : ProjectPanelBase
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public SearchPanel()
        {
            InitializeComponent();
            this.DataContextChanged += new DependencyPropertyChangedEventHandler(SearchPanel_DataContextChanged);
        }

        void SearchPanel_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (this.DataContext.GetType() == typeof(ProjectController))
                this.FlowController = ((ProjectController)this.DataContext).FlowController;
            else if (this.DataContext.GetType() == typeof(FlowController))
                this.FlowController = (FlowController)this.DataContext;

        }



        private void andor2_CheckedChanged(object sender, RoutedEventArgs e)
        {
            
            CheckBox send = (CheckBox)sender;
            if (send == or2)
            {
                or2.IsChecked = send.IsChecked;
                and2.IsChecked = false;
            }
            if (send == and2)
            {
                or2.IsChecked = false;
                and2.IsChecked = send.IsChecked;
            }
            if (and2.IsChecked == true || or2.IsChecked == true)
            {
                field2.IsEnabled = true;
                lblcontains2.IsEnabled = true;
                value2.IsEnabled = true;

                //search3.Visibility = Visibility.Visible;
            }
            else
            {
                field2.IsEnabled = false;
                lblcontains2.IsEnabled = false;
                value2.IsEnabled = false;

                //search3.Visibility = Visibility.Collapsed;
            }
        }

        private void andor3_CheckedChanged(object sender, RoutedEventArgs e)
        {
            CheckBox send = (CheckBox)sender;
            if (send == or3)
            {
                //or2.IsChecked = true;
                and3.IsChecked = false;
            }
            if (send == and3)
            {
                or3.IsChecked = false;
                //and2.IsChecked = true;
            }

            if (and3.IsChecked == true || or3.IsChecked == true)
            {
                field3.IsEnabled = true;
                lblcontains3.IsEnabled = true;
                value3.IsEnabled = true;

                //search4.Visibility = Visibility.Visible;
            }
            else
            {
                field3.IsEnabled = false;
                lblcontains3.IsEnabled = false;
                value3.IsEnabled = false;

                //search4.Visibility = Visibility.Collapsed;
            }
        }

        private void andor4_CheckedChanged(object sender, RoutedEventArgs e)
        {
            CheckBox send = (CheckBox)sender;
            if (send == or4)
            {
                //or2.IsChecked = true;
                and4.IsChecked = false;
            }
            if (send == and4)
            {
                or4.IsChecked = false;
                //and2.IsChecked = true;
            }

            if (and4.IsChecked == true || or4.IsChecked == true)
            {
                field4.IsEnabled = true;
                operator4.IsEnabled = true;
                value4.IsEnabled = true;

                search5.Visibility = Visibility.Visible;
            }
            else
            {
                field4.IsEnabled = false;
                operator4.IsEnabled = false;
                value4.IsEnabled = false;

                search5.Visibility = Visibility.Collapsed;
            }
        }

        private void andor5_CheckedChanged(object sender, RoutedEventArgs e)
        {
            CheckBox send = (CheckBox)sender;
            if (send == or5)
            {
                //or2.IsChecked = true;
                and5.IsChecked = false;
            }
            if (send == and5)
            {
                or5.IsChecked = false;
                //and2.IsChecked = true;
            }

            if (and5.IsChecked == true || or5.IsChecked == true)
            {
                field5.IsEnabled = true;
                operator5.IsEnabled = true;
                value5.IsEnabled = true;

            }
            else
            {
                field5.IsEnabled = false;
                operator5.IsEnabled = false;
                value5.IsEnabled = false;

            }
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            string Criterion = SearchTextBox.Text;
            //string FieldDesc = "All Fields";
            string FieldDesc = cmbSearchField.SelectedValue.ToString();
            PerformSimpleSearch(Criterion, FieldDesc);
        }

        private void PerformSearchAndReplace()
        {
            string searchText = SearchTextBoxR.Text;
            string searchField = cmbSearchFieldR.SelectedValue.ToString();

            string newText = ReplaceTextBoxR.Text;
            string destField = cmbReplaceFieldR.SelectedValue.ToString();

            List<Subject> removeList = new List<Subject>();
            List<Subject> queryResult = GetMatchingSubjects(searchText, searchField);

            // Let the user confirm that they really want to perform this replace
            FlowMessageBox confirm = new FlowMessageBox("Confirm Replace", String.Format("Are you sure you wan to replace \"{0}\" with \"{1}\" for {2} Subjects? THIS CANNOT BE UNDONE", searchText, newText, queryResult.Count));
            confirm.CancelButtonVisible = true;
            confirm.setButtonText("Yes", "No");
            confirm.setConfirmationText("I am sure I want to replace this data");

            // If the user chose "No", quit now
            if (!(bool)confirm.ShowDialog())
                return;

            // If the user chose "Yes", continue

            foreach (Subject s in queryResult)
            {
                //if (searchField == "All Fields")
                //{
                //    bool foundMatch = false;
                //    foreach (ProjectSubjectField f in this.CurrentFlowProject.FlowProjectDataContext.ProjectSubjectFieldList)
                //    {
                //        bool goodmatch = true;
                //        if (chkCaseMatch.IsChecked == true && !s.SubjectData[f.ProjectSubjectFieldDesc].Value.ToString().Contains(searchText))
                //        {
                //            goodmatch = false;
                //        }
                //        if (chkExactMatch.IsChecked == true && s.SubjectData[f.ProjectSubjectFieldDesc].Value.ToString().ToLower() != searchText.ToLower())
                //        {
                //            goodmatch = false;
                //        }


                //        if (goodmatch == true)
                //        {
                //            foundMatch = true;
                //            if (destField == "All Fields")
                //            {
                //                UpdateField(s, f.ProjectSubjectFieldDesc, searchText, newText);
                //                s.DateModified = DateTime.Now;
                //                s.FlagForMerge = true;
                //            }
                //            else
                //            {
                //                UpdateField(s, destField, searchText, newText);
                //                s.DateModified = DateTime.Now;
                //                s.FlagForMerge = true;
                //            }
                //        }
                //    }
                //    if(foundMatch == false)
                //        removeList.Add(s);
                //}
                //else{
                    bool goodmatch = true;
                    if (chkCaseMatch.IsChecked == true && !s.SubjectData[searchField].Value.ToString().Contains(searchText))
                    {
                        goodmatch = false;
                    }
                    if (chkExactMatch.IsChecked == true && s.SubjectData[searchField].Value.ToString().ToLower() != searchText.ToLower())
                    {
                        goodmatch = false;
                    }


                    if (goodmatch == true)
                    {
                        UpdateField(s, destField, searchText, newText);
                        s.DateModified = DateTime.Now;
                        s.FlagForMerge = true;
                    }
                    else
                        removeList.Add(s);
                //}
            }

            foreach (Subject s in removeList)
                queryResult.Remove(s);

            UpdateSearchResults(queryResult);
            this.CurrentFlowProject.FlowProjectDataContext.UpdateAllSubjectData();
            this.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();
            FlowMessageBox msg = new FlowMessageBox("Search and Replace", "Updated this field for " + queryResult.Count + " subjects.");
            msg.CancelButtonVisible = false;
            msg.ShowDialog();
        }

        private void UpdateField(Subject s, string destField, string oldText, string newText)
        {
            if (rdoReplaceOnlyMatchingText.IsChecked == true)
            {
                s.SubjectData[destField].Value = Regex.Replace(s.SubjectData[destField].Value.ToString(), oldText, newText, RegexOptions.IgnoreCase);
                s.FlagForMerge = true;
                //s.Save();
            }
            else if (rdoReplaceEntireFieldValue.IsChecked == true)
            {
                s.SubjectData[destField].Value = newText;
                s.FlagForMerge = true;
                //s.Save();
            }
        }


        private void FormatText()
        {
            string destField = cmbFormatField.SelectedValue.ToString();

            foreach (Subject s in this.CurrentFlowProject.SubjectList.GetDataGridCollectionViewSubjects())
            {
                if(destField == "All Fields")
                {
                    foreach(ProjectSubjectField f in this.CurrentFlowProject.FlowProjectDataContext.ProjectSubjectFieldList)
                    {
                        FormatField(s, f.ProjectSubjectFieldDesc);
                    }
                }
                else{

                    FormatField(s, destField);

                }

               
            }

            this.CurrentFlowProject.FlowProjectDataContext.UpdateAllSubjectData();
            this.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();
            FlowMessageBox msg = new FlowMessageBox("Text Formatting", "Updated this field for " + this.CurrentFlowProject.SubjectList.GetDataGridCollectionViewSubjects().Count() + " subjects.");
            msg.CancelButtonVisible = false;
            msg.ShowDialog();
        }

        private void FormatField(Subject s, string destField)
        {
            if (rdoLowerCase.IsChecked == true)
            {
                s.SubjectData[destField].Value = s.SubjectData[destField].Value.ToString().ToLower();
                s.DateModified = DateTime.Now;
                s.FlagForMerge = true;
                //s.Save();
                
            }
            if (rdoUpperCase.IsChecked == true)
            {
                s.SubjectData[destField].Value = s.SubjectData[destField].Value.ToString().ToUpper();
                s.DateModified = DateTime.Now;
                s.FlagForMerge = true;
                //s.Save();

            }
            if (rdoNameCase.IsChecked == true)
            {
                s.SubjectData[destField].Value = StringExtensions.ToProperCase(s.SubjectData[destField].Value.ToString());
                s.DateModified = DateTime.Now;
                s.FlagForMerge = true;
                //s.Save();

            }
        }

        

        private List<Subject> GetMatchingSubjects(string Criterion, string FieldDesc)
        {
             if (this.ProjectController.specialBarCodeHandled)
                return new List<Subject>();

            List<Subject> queryResult = null;

            queryResult = this.CurrentFlowProject.SubjectList.GetQueryResultsForDataGridItems(Criterion, true, FieldDesc).ToList();

            return queryResult;
        }

        private void PerformAdvancedSearch()
        {
            List<Subject> result1 = GetMatchingSubjects(value1.Text, field1.SelectedValue.ToString());
            List<Subject> result2 = GetMatchingSubjects(value2.Text, field2.SelectedValue.ToString());
            List<Subject> result3 = GetMatchingSubjects(value3.Text, field3.SelectedValue.ToString());

            List<Subject> removeList = new List<Subject>();
            List<Subject> returnList = new List<Subject>(result1);

            if (and2.IsChecked == true)
            {
                foreach (Subject s in returnList)
                    if (!result2.Contains(s))
                        removeList.Add(s);
            }
            if (or2.IsChecked == true)
            {
                foreach (Subject s in result2)
                    if (!returnList.Contains(s))
                        returnList.Add(s);
            }
            if (and3.IsChecked == true)
            {
                foreach (Subject s in returnList)
                    if (!result3.Contains(s))
                        removeList.Add(s);
            }
            if (or3.IsChecked == true)
            {
                foreach (Subject s in result3)
                    if (!result1.Contains(s) && !result2.Contains(s))
                        returnList.Add(s);
            }



            foreach (Subject s in removeList)
                if (returnList.Contains(s))
                    returnList.Remove(s);


            UpdateSearchResults(returnList);
        }


        private void PerformSimpleSearch(string Criterion, string FieldDesc)
        {
            List<Subject> queryResult = GetMatchingSubjects(Criterion, FieldDesc);
            UpdateSearchResults(queryResult);
        }

        private void UpdateSearchResults(List<Subject> queryResult)
        {
            this.SearchResultsList.ItemsSource = queryResult;

            int queryResultCount = (queryResult == null) ? 0 : queryResult.Count();

            if (queryResultCount == 0)
            {
                this.NoSearchResultsLabel.Visibility = Visibility.Visible;
                this.SearchResultsPanel.Visibility = Visibility.Collapsed;
            }
            else
            {

                this.SearchResultsList.ItemsSource = queryResult;
                this.CurrentFlowProject.SubjectList.MoveCurrentToFirst();

                this.SearchResultsPanel.Visibility = Visibility.Visible;
                this.NoSearchResultsLabel.Visibility = Visibility.Collapsed;
                this.SearchResultsList.SelectedIndex = 0;
            }
        }

        private void SearchTextBox_GotFocus(object sender, RoutedEventArgs e)
        {

        }

        private void SearchTextBox_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void SearchTextBox_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key != System.Windows.Input.Key.Enter) return;

            string Criterion = SearchTextBox.Text;
            string FieldDesc = "All Fields";
            //            string FieldDesc = cmbSearchField.SelectedValue.ToString();
            PerformSimpleSearch(Criterion, FieldDesc);
        }

        private void cmbSearchField_DropDownOpened(object sender, EventArgs e)
        {

        }

        private void SearchResultsList_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {

        }

        private void SearchResultsList_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (this.CurrentFlowProject.SubjectList.CurrentItem != this.SearchResultsList.SelectedItem)
                this.CurrentFlowProject.SubjectList.CurrentItem = (Subject)this.SearchResultsList.SelectedItem;
        }

        private void AdvancedSearchButton_Click(object sender, RoutedEventArgs e)
        {
            PerformAdvancedSearch();
        }

        private void AdvancedSearchTextBox_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key != System.Windows.Input.Key.Enter) return;
            PerformAdvancedSearch();
        }


        private void btnFormat_Click(object sender, RoutedEventArgs e)
        {
            FormatText();
        }

        

        private void btnReplaceAll_Click(object sender, RoutedEventArgs e)
        {
            PerformSearchAndReplace();
        }

        private void cmbSearchFieldR_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            cmbReplaceFieldR.SelectedIndex = cmbSearchFieldR.SelectedIndex;
        }

        private void btnClearSearch_Click(object sender, RoutedEventArgs e)
        {
            logger.Info("Clear Search button clicked");

            // Search results
            this.SearchResultsList.ItemsSource = null;
            this.CurrentFlowProject.SubjectList.MoveCurrentToFirst();
            this.SearchResultsPanel.Visibility = Visibility.Collapsed;
            this.NoSearchResultsLabel.Visibility = Visibility.Collapsed;

            // Search
            this.SearchTextBox.Clear();
            this.cmbSearchField.SelectedIndex = 0;

            // Replace
            this.SearchTextBoxR.Clear();
            this.cmbSearchFieldR.SelectedIndex = 0;
            this.ReplaceTextBoxR.Clear();
            this.cmbReplaceFieldR.SelectedIndex = 0;
            this.rdoReplaceEntireFieldValue.IsChecked = true;
            this.chkExactMatch.IsChecked = false;
            this.chkCaseMatch.IsChecked = false;

            // Text Formatting
            this.cmbFormatField.SelectedIndex = 0;
            this.rdoNameCase.IsChecked = true;

            logger.Info("Done clearing search results");
        }
    }


    public static class StringExtensions
    {
        public static string ToProperCase(this string original)
        {
            if (String.IsNullOrEmpty(original))
                return original;

            string result = _properNameRx.Replace(original.ToLower(CultureInfo.CurrentCulture), HandleWord);
            return result;
        }

        public static string WordToProperCase(this string word)
        {
            if (String.IsNullOrEmpty(word))
                return word;

            if (word.Length > 1)
                return Char.ToUpper(word[0], CultureInfo.CurrentCulture) + word.Substring(1);

            return word.ToUpper(CultureInfo.CurrentCulture);
        }

        private static readonly Regex _properNameRx = new Regex(@"\b(\w+)\b");
        private static readonly string[] _prefixes = {
                                                         "mc"
                                                     };

        private static string HandleWord(Match m)
        {
            string word = m.Groups[1].Value;

            foreach (string prefix in _prefixes)
            {
                if (word.StartsWith(prefix, StringComparison.CurrentCultureIgnoreCase))
                    return prefix.WordToProperCase() + word.Substring(prefix.Length).WordToProperCase();
            }

            return word.WordToProperCase();
        }
    }
}
