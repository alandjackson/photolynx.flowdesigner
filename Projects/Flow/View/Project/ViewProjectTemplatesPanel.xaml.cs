﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Controller;
using Flow.Controls.View.RecordDisplay;
using Flow.Schema.LinqModel.DataContext;
using Flow.View.Project.Base;

using Flow.Lib.Helpers;
using Flow.View.Dialogs;

namespace Flow.View.Project
{
	/// <summary>
	/// Interaction logic for ProjectTemplatesPanel.xaml
	/// </summary>
	public partial class ViewProjectTemplatesPanel : ProjectPanelBase
	{
		public ViewProjectTemplatesPanel()
		{
			InitializeComponent();
		}

		protected override void OnSetFlowController()
		{
			//var templates = 
			//    from t in this.FlowMasterDataContext.ProjectTemplates
			//    select t;

			//if (templates.Count() == 0)
			//    txtNoProjectsNotice.Visibility = Visibility.Visible;

			this.DataContext = this.FlowMasterDataContext.ProjectTemplateList;

//			objFlowProjectTemplatesItemList.ItemsSource = templates;
		}

		private void FlowProjectTemplateRecordDisplay_MouseDoubleClick(object sender, MouseButtonEventArgs e)
		{
			FlowProjectTemplateRecordDisplay selectedItem = sender as FlowProjectTemplateRecordDisplay;

			if (selectedItem != null)
			{
                Flow.Schema.LinqModel.DataContext.ProjectTemplate pt = selectedItem.DataContext
                    as Flow.Schema.LinqModel.DataContext.ProjectTemplate;
                pt.FlowMasterDataContext = this.FlowMasterDataContext;
				this.ProjectController.EditProjectTemplate(pt);
			}
		}
        private void DeleteProjectTemplateInvoked(object sender, EventArgs<Flow.Schema.LinqModel.DataContext.ProjectTemplate> e)
        {
            FlowMessageBox msgb = new FlowMessageBox("Delete Project Template", "Are you sure you want to delete this project template?");
            msgb.CancelButtonVisible = true;
            if (msgb.ShowDialog() == false)
                return;

            Flow.Schema.LinqModel.DataContext.ProjectTemplate FlowProjectTemplate = e.Data;
            foreach (Flow.Schema.LinqModel.DataContext.ProjectTemplate thisOne in this.FlowMasterDataContext.ProjectTemplates)
            {
                if (thisOne == FlowProjectTemplate)
                {
                    
                    while (FlowProjectTemplate.ProjectTemplateEventTriggers.Count > 0)
                    {
                        this.FlowMasterDataContext.ProjectTemplateEventTriggers.DeleteOnSubmit(FlowProjectTemplate.ProjectTemplateEventTriggers[0]);
                        FlowProjectTemplate.ProjectTemplateEventTriggers.RemoveAt(0);
                    }
                    while (FlowProjectTemplate.ProjectTemplateSubjectFields.Count > 0)
                    {
                        this.FlowMasterDataContext.ProjectTemplateSubjectFields.DeleteOnSubmit(FlowProjectTemplate.ProjectTemplateSubjectFields[0]);
                        FlowProjectTemplate.ProjectTemplateSubjectFields.RemoveAt(0);
                    }
                    while (FlowProjectTemplate.ProjectTemplateImageTypes.Count > 0)
                    {
                        this.FlowMasterDataContext.ProjectTemplateImageTypes.DeleteOnSubmit(FlowProjectTemplate.ProjectTemplateImageTypes[0]);
                        FlowProjectTemplate.ProjectTemplateImageTypes.RemoveAt(0);
                    }
                }
            }
            this.FlowController.ProjectController.FlowMasterDataContext.ProjectTemplateList.Remove(FlowProjectTemplate);
            this.FlowController.ProjectController.FlowMasterDataContext.SubmitChanges();

          
            

            //if (this.CurrentFlowProject == e.Data)
            //{
            //    this.FlowController.Edit();
            //    return;
            //}

            //this.ProjectController.Load(e.Data);


            //e.Data.FlowProjectDataContext.SubjectDataLoaded +=
            //    delegate
            //    {

            //        this.FlowController.Edit();
            //        if (this.ProjectController.CurrentFlowProject.SubjectList.Count() > 0)
            //            this.ProjectController.CurrentFlowProject.SubjectList.CurrentItem.RefreshSubjectImages();

            //    };
        }
	}
}
