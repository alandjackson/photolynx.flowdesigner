using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Controls;
using Flow.Controller.Project;
using Flow.Lib;
using Flow.Lib.Helpers;
using Flow.Schema.LinqModel.DataContext;
using Flow.View.Project.Base;
using Flow.View.Preferences;

using StructureMap;
using System.Drawing;
using Flow.Designer.Lib.ImageAdjustments.Model;
using Flow.Designer.Lib.Service;
using System.Windows.Threading;
using Flow.View.Dialogs;
using Flow.Schema.Reports;
using Flow.Controller.Reports;
using C1.C1Report;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Threading;
using NLog;

namespace Flow.View.Project
{
	/// <summary>
	/// Interaction logic for OrderPanel.xaml
	/// </summary>
	public partial class OrderPanel : ProjectPanelBase
	{

        public List<OrderOptionSelected> OrderOptionList { get; set; }

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        static extern EXECUTION_STATE SetThreadExecutionState(EXECUTION_STATE esFlags);

        [FlagsAttribute]
        public enum EXECUTION_STATE : uint
        {
            ES_AWAYMODE_REQUIRED = 0x00000040,
            ES_CONTINUOUS = 0x80000000,
            ES_DISPLAY_REQUIRED = 0x00000002,
            ES_SYSTEM_REQUIRED = 0x00000001
        }

        //no longer prevent the system from sleeping
        void AllowSleep()
        {
            SetThreadExecutionState(EXECUTION_STATE.ES_CONTINUOUS);
        }

        //prevents the system from sleeping untill the app closes or AllowSleep is called
        void PreventSleep()
        {
            // Prevent Idle-to-Sleep (monitor not affected)
            SetThreadExecutionState(EXECUTION_STATE.ES_CONTINUOUS | EXECUTION_STATE.ES_AWAYMODE_REQUIRED);
        }

        //just resets the idle clock, must keep calling this periodically to keep awake
        void KeepSystemAwake()
        {
            SetThreadExecutionState(EXECUTION_STATE.ES_SYSTEM_REQUIRED);
        }

        private NotificationProgressInfo _notificationInfo;


		private enum ExportMethod
		{
			ExportToPaf,
			ExportForImageMatch,
			SaveOrder,
			UploadProject,
			SubmitOrder
		}

/// /////////////////////////////////////////////////////////////////////////
		#region DEPENDENCY PROPERTIES
/// /////////////////////////////////////////////////////////////////////////

        //protected static DependencyProperty SelectedProjectFilePathProperty =
        //    DependencyProperty.Register("SelectedProjectFilePath", typeof(string), typeof(OrderPanel));

        //public string SelectedProjectFilePath
        //{
        //    get { return (string)this.GetValue(SelectedProjectFilePathProperty); }
        //    set
        //    {
        //        this.SetValue(SelectedProjectFilePathProperty, value);
        //        this.ImportButtonEnabled = !string.IsNullOrEmpty(this.SelectedProjectFilePath);
        //    }
        //}

        //protected static DependencyProperty ImportButtonEnabledProperty =
        //    DependencyProperty.Register("ImportButtonEnabled", typeof(bool), typeof(OrderPanel));

        //public bool ImportButtonEnabled
        //{
        //    get { return (bool)this.GetValue(ImportButtonEnabledProperty); }
        //    set { this.SetValue(ImportButtonEnabledProperty, value); }
        //}


        //protected static DependencyProperty ImportButtonVisibilityProperty =
        //    DependencyProperty.Register("ImportButtonVisibility", typeof(Visibility), typeof(OrderPanel));

        //public Visibility ImportButtonVisibility
        //{
        //    get { return (Visibility)this.GetValue(ImportButtonVisibilityProperty); }
        //    set { this.SetValue(ImportButtonVisibilityProperty, value); }
        //}


        //protected static DependencyProperty MergeControlsVisibilityProperty =
        //    DependencyProperty.Register("MergeControlsVisibility", typeof(Visibility), typeof(OrderPanel), new PropertyMetadata(Visibility.Collapsed));

        //public Visibility MergeControlsVisibility
        //{
        //    get { return (Visibility)this.GetValue(MergeControlsVisibilityProperty); }
        //    set { this.SetValue(MergeControlsVisibilityProperty, value); }
        //}


        //protected static DependencyProperty ImportAfterOpenProperty =
        //    DependencyProperty.Register("ImportAfterOpen", typeof(bool), typeof(OrderPanel), new PropertyMetadata(true));

        //public bool ImportAfterOpen
        //{
        //    get { return (bool)this.GetValue(ImportAfterOpenProperty); }
        //    set { this.SetValue(ImportAfterOpenProperty, value); }
        //}

        //protected static DependencyProperty SelectedImageMatchExportPathProperty =
        //    DependencyProperty.Register("SelectedImageMatchExportPath", typeof(string), typeof(OrderPanel));

        //public string SelectedImageMatchExportPath
        //{
        //    get { return (string)this.GetValue(SelectedImageMatchExportPathProperty); }
        //    set	{ this.SetValue(SelectedImageMatchExportPathProperty, value); }
        //}


		protected static DependencyProperty SelectedFlowProjectProperty = DependencyProperty.Register(
			"SelectedFlowProject", typeof(FlowProject), typeof(OrderPanel),
			new FrameworkPropertyMetadata(new PropertyChangedCallback(SelectedFlowProjectPropertyChanged))
		);

		public FlowProject SelectedFlowProject
		{
			get { return (FlowProject)this.GetValue(SelectedFlowProjectProperty); }
			set { this.SetValue(SelectedFlowProjectProperty, value); }
		}

		public static void SelectedFlowProjectPropertyChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
		{
			OrderPanel source = o as OrderPanel;
            
			if (source != null)
			{
				source.FlowProjectSelected = (e.NewValue != null);
                source.ExportButtonEnabled = true;
			}
		}

		protected static DependencyProperty FlowProjectSelectedProperty =
			DependencyProperty.Register("FlowProjectSelected", typeof(bool), typeof(OrderPanel));

		public bool FlowProjectSelected
		{
			get { return (bool)this.GetValue(FlowProjectSelectedProperty); }
			set { this.SetValue(FlowProjectSelectedProperty, value); }
		}

        //protected static DependencyProperty SelectedExportPathProperty =
        //    DependencyProperty.Register("SelectedExportPath", typeof(string), typeof(OrderPanel));

        //public string SelectedExportPath
        //{
        //    get { return (string)this.GetValue(SelectedExportPathProperty); }
        //    set
        //    {
        //        this.SetValue(SelectedExportPathProperty, value);
        //        this.ExportButtonEnabled = !string.IsNullOrEmpty(this.SelectedExportPath);
        //    }
        //}

		protected static DependencyProperty SaveOrderExportPathProperty =
			DependencyProperty.Register("SaveOrderExportPath", typeof(string), typeof(OrderPanel));

		public string SaveOrderExportPath
		{
			get { return (string)this.GetValue(SaveOrderExportPathProperty); }
			set { this.SetValue(SaveOrderExportPathProperty, value); }
		}

        protected static DependencyProperty ExportButtonEnabledProperty =
            DependencyProperty.Register("ExportButtonEnabled", typeof(bool), typeof(OrderPanel));

        public bool ExportButtonEnabled
        {
            get { return (bool)this.GetValue(ExportButtonEnabledProperty); }
            set { this.SetValue(ExportButtonEnabledProperty, value); }
        }

/// /////////////////////////////////////////////////////////////////////////
#endregion DEPENDENCY PROPERTIES
/// /////////////////////////////////////////////////////////////////////////


        public FlowProject DefaultProject { get; set; }

		private FlowProject RootProject { get; set; }

		//private ProjectImageType SelectedImageType { get; set; }

        private ExportMethod SelectedExportMethod { get; set; }

        private string sortField1 = "";
        private string sortField2 = "";
        private bool renderGreenScreen = false;
        private bool applyCrops = false;
        private bool useOriginalFileNames = false;
        private string orderFilter = "New Orders";
        private string saveOrderPath = null;
        private bool includeAllSubjectsAndImages = false;

        private static Logger logger = LogManager.GetCurrentClassLogger();

		public OrderPanel()
		{
			InitializeComponent();
		}


		protected override void OnSetFlowController()
		{
			base.OnSetFlowController();

//			cmbFlowProjects.ItemsSource = this.FlowMasterDataContext.StudioProjectList;
            //this.txtUploadDestination.Text = ProjectController.FlowMasterDataContext.PreferenceManager.Upload.ProjectTransfer.HostAddress;

			this.DataContext = this.ProjectController;

            this.CurrentFlowProject.FlowProjectDataContext.SubjectDataLoaded += delegate
            {
               InitPanel();
            };

            ApplyDefaults();

            this.AllOrdersPanelB.SetDataContext(this.ProjectController);

            
		}


/// /////////////////////////////////////////////////////////////////////////
		#region IMPORT SECTION
/// /////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Imports the project and opens the project in Capture if requested
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
        //private void btnImport_Click(object sender, RoutedEventArgs e)
        //{
        //    this.ProjectController.AttachProject(this.SelectedProjectFilePath, this.ImportAfterOpen, this.FlowController);
        //}

        //private void btnMerge_Click(object sender, RoutedEventArgs e)
        //{
        //    this.ProjectController.MergeProject(
        //        this.SelectedProjectFilePath,
        //        this.RootProject,
        //        this.ImportAfterOpen,
        //        this.FlowController
        //    );

        //    this.MergeControlsVisibility = Visibility.Collapsed;
        //    this.ImportButtonVisibility = Visibility.Visible;
        //}

        //private void btnSelectProjectFile_Click(object sender, RoutedEventArgs e)
        //{
        //    this.SelectedProjectFilePath = DialogUtility.SelectFile(
        //        "Select Source File",
        //        this.FlowMasterDataContext.PreferenceManager.Application.ProjectsDirectory,
        //        "Project Archive File (*.paf)|*.paf"
        //    );

        //    if (String.IsNullOrEmpty(this.SelectedProjectFilePath))
        //        return;

        //    this.RootProject = this.ProjectController.FlowMasterDataContext.GetExistingProject(this.SelectedProjectFilePath);


        //    if (this.RootProject == null)	// merge
        //    {
        //        this.ImportButtonVisibility = Visibility.Visible;
        //        this.MergeControlsVisibility = Visibility.Collapsed;
        //    }
        //    else							// attach
        //    {
        //        this.ImportButtonVisibility = Visibility.Collapsed;
        //        this.MergeControlsVisibility = Visibility.Visible;
        //    }
        //}

/// /////////////////////////////////////////////////////////////////////////
		#endregion IMPORT SECTION
/// /////////////////////////////////////////////////////////////////////////


/// /////////////////////////////////////////////////////////////////////////
		#region EXPORT SECTION
/// /////////////////////////////////////////////////////////////////////////

        //private void btnSelectExportFilePath_Click(object sender, RoutedEventArgs e)
        //{
        //    this.SelectedExportPath = DialogUtility.SelectFolder(Environment.SpecialFolder.Desktop, "Select destination folder.");
        //}

        //private void btnSelectImageMatchExportPath_Click(object sender, RoutedEventArgs e)
        //{
        //    string path = DialogUtility.SelectFolder(Environment.SpecialFolder.Desktop, "Select destination folder.");

        //    bool pathSelected = !String.IsNullOrEmpty(path);

        //    if (!String.IsNullOrEmpty(path))
        //    {
        //        this.SelectedImageMatchExportPath = System.IO.Path.Combine(path, this.SelectedFlowProject.FileSafeProjectName);

        //        if (this.SelectedFlowProject != this.ProjectController.CurrentFlowProject)
        //        {
        //            this.SelectedFlowProject.Connect(this.FlowMasterDataContext);
        //            this.SelectedFlowProject.FlowProjectDataContext.SubjectDataLoaded += delegate
        //            {
        //                this.ProjectController.CurrentFlowProject = this.SelectedFlowProject;
        //                this.ProjectController.CurrentFlowProject.RefreshPropertyBinding("ProjectImageTypeList");

        //                this.ImageTypePanel.Visibility = Visibility.Visible;
        //            };
        //        }
        //        else
        //        {
        //            this.ProjectController.CurrentFlowProject.RefreshPropertyBinding("ProjectImageTypeList");
        //            this.ImageTypePanel.Visibility = Visibility.Visible;
        //        }
        //    }

        //    this.ExportButtonEnabled = pathSelected;
        //}

		private void btnSelectSaveOrderExportPath_Click(object sender, RoutedEventArgs e)
		{
			this.SaveOrderExportPath = DialogUtility.SelectFolder(Environment.SpecialFolder.Desktop, "Select destination folder.");

			this.ExportButtonEnabled = !String.IsNullOrEmpty(this.SaveOrderExportPath);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
        private void SelectedExportMethod_Changed(object sender, RoutedEventArgs e)
        {
            if (!this.IsInitialized)
                return;

            RadioButton source = sender as RadioButton;

            if (source != null)
            {
                this.SetExportMethod();
            }
        }

		private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if (!this.IsInitialized)
				return;

			TabControl exportTabControl = sender as TabControl;

			if (exportTabControl != null)
			{
				this.SetExportMethod();
			}
		}

		/// <summary>
		/// Sets the export type method (selected operation) based on UI selections
		/// </summary>
		private void SetExportMethod()
		{
            //if (this.ExportTabControl.SelectedIndex == 0)
            //{
            //    if (rdoExportServer.IsChecked.Value)
            //    {
            //        this.SelectedExportMethod = ExportMethod.UploadProject;
            //        this.ExportButtonEnabled = true;
            //        btnExport.Content = "Upload";
            //    }
            //    else if (rdoExportFile.IsChecked.Value)
            //    {
            //        this.SelectedExportMethod = ExportMethod.ExportToPaf;
            //        this.ExportButtonEnabled = !String.IsNullOrEmpty(this.SelectedExportPath);
            //        btnExport.Content = "Export";
            //    }
            //    else
            //    {
            //        this.SelectedExportMethod = ExportMethod.ExportForImageMatch;
            //        this.ExportButtonEnabled = !String.IsNullOrEmpty(this.SelectedImageMatchExportPath);
            //        btnExport.Content = "Export IM";
            //    }
            //}
            //else
            //{
				if (rdoSubmitOrder.IsChecked.Value)
				{
					this.SelectedExportMethod = ExportMethod.SubmitOrder;
					this.ExportButtonEnabled = true;
					btnExport.Content = "Submit";
				}
				else
				{
					this.SelectedExportMethod = ExportMethod.SaveOrder;
					this.ExportButtonEnabled = !String.IsNullOrEmpty(this.SaveOrderExportPath);
					btnExport.Content = "Save";
				}
			//}
            if (this.SelectedFlowProject == null)
                this.ExportButtonEnabled = false;
		}

        
        NotificationProgressInfo firstProgress;

		/// <summary>
		/// Performs the selected export operation
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnExport_Click(object sender, RoutedEventArgs e)
		{
            firstProgress = new NotificationProgressInfo("", "Verifying Catalog", 0, 4, 1);
            IShowsNotificationProgress firstNotification = ObjectFactory.GetInstance<IShowsNotificationProgress>();
            firstProgress.NotificationProgress = firstNotification;
            firstNotification.ShowNotification(firstProgress);

            logger.Info("Clicked export button in Order Panel");

            firstProgress.Update("Verifying Catalog");
            firstProgress.Step();

            if (this.SelectedFlowProject.FlowCatalog == null)
            {
                FlowMessageBox msg = new FlowMessageBox("Submit Order", "You can not submit an order for this project because no Catalog is assigned.");
                msg.CancelButtonVisible = false;
                msg.ShowDialog();
                return;
            }
			FlowProject flowProject = this.SelectedFlowProject;

            firstProgress.Update("Checking Order Type");
            firstProgress.Step();

			switch (this.SelectedExportMethod)
			{
				
				case ExportMethod.SubmitOrder:
				case ExportMethod.SaveOrder:
					string saveOrderExportPath = (this.SelectedExportMethod == ExportMethod.SaveOrder)
						? this.SaveOrderExportPath
						: this.FlowMasterDataContext.PreferenceManager.Application.LabOrderQueueDirectory
					;

					bool includeAllSubjects = this.SubmitOrderIncludeAllSubjectsCheckBox.IsChecked.Value;
				
					string sortField1 = (this.SelectedExportMethod == ExportMethod.SaveOrder)
						? this.SaveOrderSortControl.SortField1
						: this.SubmitOrderSortControl.SortField1
					;
				
					string sortField2 = (this.SelectedExportMethod == ExportMethod.SaveOrder)
						? this.SaveOrderSortControl.SortField2
						: this.SubmitOrderSortControl.SortField2
					;

                    //if (this.cmbOrderFilter.SelectedValue == null)
                    //{
                    //    FlowMessageBox msg = new FlowMessageBox("Submit Order", "Please select an order filter.");
                    //    msg.CancelButtonVisible = false;
                    //    msg.ShowDialog();
                    //    return;
                    //}
               
                    //string orderFilter = this.cmbOrderFilter.SelectedValue.ToString();

					if (flowProject == this.CurrentFlowProject)
					{
						this.SubmitOrder(
							flowProject,
							includeAllSubjects,
							saveOrderExportPath,
							sortField1,
							sortField2,
                            orderFilter
						);
					}
					else
					{
						flowProject.Connect(this.FlowMasterDataContext);
                        flowProject.FlowProjectDataContext.InitSubjectList();
                        flowProject.FlowProjectDataContext.SubjectDataLoaded += delegate
						{
							this.SubmitOrder(
								flowProject,
								includeAllSubjects,
								saveOrderExportPath,
								sortField1,
								sortField2,
                                orderFilter
							);
						};
					}
                   
					break;

			}
           
		}

		/// <summary>
		/// Exports the 
		/// </summary>
		/// <param name="flowProject"></param>
        //private void ExportAfterPrepare(FlowProject flowProject)
        //{
        //    // If export to filesystem is selected
        //    if (rdoExportFile.IsChecked == true)
        //    {
        //        ProjectExporter exporter = new ProjectExporter(flowProject,
        //            this.SelectedExportPath, FlowMasterDataContext);

        //        if (chkDetachOnImport.IsChecked.Value)
        //        {
        //            exporter.ExportComplete += new ProjectExporter.ExportCompleteDelegate(exporter_ExportComplete);
        //        }
        //        exporter.BeginAsyncExportProject();

        //    }

        //    // If upload to remote server is selected
        //    else if (rdoExportServer.IsChecked == true)
        //        this.ProjectController.UploadProject(flowProject, chkDetachOnImport.IsChecked.Value, WithReducedImagesCheckBox.IsChecked.Value);
        //}

        //void exporter_ExportComplete(FlowProject project)
        //{
        //    Directory.Delete(project.DirPath, true);

        //    FlowMasterDataContext.DetachProject(project);
        //}

        //private void ExportForImageMatch(FlowProject flowProject)
        //{
        //    NotificationProgressInfo progressInfo = new NotificationProgressInfo("ImageMatch Export", "Initializing...");
        //    progressInfo.NotificationProgress = ObjectFactory.GetInstance<IShowsNotificationProgress>();

        //    string selectedImageMatchExportPath = this.SelectedImageMatchExportPath;
        //    bool WithCroppingCheckBox = this.WithCroppingCheckBox.IsChecked.Value;

        //    FlowBackgroundWorkerManager.RunWorker(
        //        delegate
        //        {
        //            progressInfo.Display(true);

        //            IEnumerable<string> imageFileList;

        //            DataTable exportDataTable = flowProject.GetExportData(this.SelectedImageType, out imageFileList);

        //            DataColumn[] columnList = new DataColumn[exportDataTable.Columns.Count];

        //            exportDataTable.Columns.CopyTo(columnList, 0);

        //            StringBuilder builder = new StringBuilder();

        //            string columnString = String.Join(",", columnList.Select(c => @"""" + c.ColumnName + @"""").ToArray());

        //            builder.AppendLine(columnString);

        //            progressInfo.Update("Processing subject data {0} of {1}", 0, exportDataTable.Rows.Count, 1, true);
		
        //            foreach (DataRow row in exportDataTable.Rows)
        //            {
        //                string[] items = row.ItemArray.Select(i => @"""" + i.ToString() + @"""").ToArray();

        //                string rowString = String.Join(",", items);

        //                builder.AppendLine(rowString);

        //                progressInfo++;
        //            }

        //            string exportFilePath = FlowContext.Combine(selectedImageMatchExportPath, flowProject.FileSafeProjectName + ".csv");

        //            File.WriteAllText(exportFilePath, builder.ToString(), Encoding.Default);

        //            //IOUtil.CopyFiles(flowProject.ImageDirPath, selectedImageMatchExportPath, imageFileList, progressInfo);
        //            if (WithCroppingCheckBox)
        //            {
        //                DirectoryInfo dirInfo = new DirectoryInfo(flowProject.ImageDirPath);
        //                int fileCount = imageFileList.Count();
        //                int loopcnt = 0;
        //                foreach (Subject sub in flowProject.SubjectList)
        //                {
        //                    foreach (SubjectImage si in sub.SubjectImages)
        //                    {
        //                        if (imageFileList.Contains(si.ImageFileName))
        //                        {
        //                            progressInfo.Update("Copying file {0} of {1}", loopcnt++, fileCount, 1, true);
        //                            System.Drawing.Image img = System.Drawing.Image.FromFile(si.ImagePath);
        //                            if (si.Orientation == 90)
        //                                img.RotateFlip(RotateFlipType.Rotate90FlipNone);
        //                            if (si.Orientation == 180)
        //                                img.RotateFlip(RotateFlipType.Rotate180FlipNone);
        //                            if (si.Orientation == 270)
        //                                img.RotateFlip(RotateFlipType.Rotate270FlipNone);

        //                            if (WithCroppingCheckBox && ((si.CropW > 0) || (si.CropH > 0)))
        //                                img = cropImage(img, getCropArea(img, si.CropL, si.CropT, si.CropH, si.CropW));
        //                            FileInfo source = new FileInfo(si.ImagePath);
        //                            img.Save(System.IO.Path.Combine(selectedImageMatchExportPath, source.Name),System.Drawing.Imaging.ImageFormat.Jpeg);
        //                            img.Dispose();
        //                            progressInfo++;
        //                        }


        //                    }
        //                }
        //            }
        //            else
        //            {
        //                IOUtil.CopyFiles(flowProject.ImageDirPath, selectedImageMatchExportPath, imageFileList, progressInfo);
        //            }
        //            progressInfo.Complete("ImageMatch Export complete.");
        //        }
        //    );
        //}


        private System.Drawing.Rectangle getCropArea(System.Drawing.Image iSrc, double CropL, double CropT, double CropH,  double CropW)
        {
            double width = iSrc.Width;
            double height = iSrc.Height;

            int cropAreaL = (int)(CropL * width);
            int cropAreaT = (int)(CropT * height);
            int cropAreaW = (int)(CropW * width);
            int cropAreaH = (int)(CropH * height);

            if (cropAreaL + cropAreaW > width)
                cropAreaW = (int)width - cropAreaL;
            if (cropAreaT + cropAreaH > height)
                cropAreaH = (int)height - cropAreaT;


            return new System.Drawing.Rectangle(
                cropAreaL,
                cropAreaT,
                cropAreaW,
                cropAreaH
                );
        }

        private static System.Drawing.Image cropImage(System.Drawing.Image img, System.Drawing.Rectangle cropArea)
        {
            Bitmap bmpImage = new Bitmap(img);
            Bitmap bmpCrop = bmpImage.Clone(cropArea,
            bmpImage.PixelFormat);
            return (System.Drawing.Image)(bmpCrop);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="includeAllSubjects"></param>
        private void SubmitOrder(FlowProject flowProject, bool includeAllSubjects, string saveOrderExportPath, string sortField1, string sortField2, string orderFilter)
        {
            

            //if (rdoSaveOrder.IsChecked == true)
            //    saveOrderPath = txtSaveExportDestination.Text;
            //else 
            //    saveOrderPath = null;

            saveOrderPath = saveOrderExportPath;

            if (chkRenderGreenScreen.IsChecked == true)
                renderGreenScreen = true;
            else
                renderGreenScreen = false;

            if (chkApplyCrops.IsChecked == true)
                applyCrops = true;
            else
                applyCrops = false;

            this.orderFilter = orderFilter;
            if (chkOriginalFileName.IsChecked == true)
                useOriginalFileNames = true;
            else
                useOriginalFileNames = false;

            bool canContinue = false;

            if (CurrentFlowProject.FlowProjectDataContext.OrderFilterAll == true)
            {
                FlowMessageBox msg = new FlowMessageBox("ALL Orders Selected", "WARNING!!! - you have selected to submit ALL ORDERS (not just new orders)\nIf you continue, all previous orders from this project will be submitted along with new orders.");
                msg.CancelButtonVisible = true;
                msg.buttonOkay.Content = "Continue";

                if (msg.ShowDialog() == true)
                {
                    //do nothing, will continue
                }
                else
                {
                    firstProgress.Complete("Order Aborted");
                    return;
                }

            }
            firstProgress.Update("Verifying Order Form");
            firstProgress.Step();
            while (canContinue == false)
            {
                
                LabOrderFormDialog dlg = new LabOrderFormDialog(this.CurrentFlowProject.FlowProjectDataContext);

                if (dlg.ShowDialog() == true)
                {
                    bool foundMissingValue = false;
                    foreach (OrderFormFieldLocal field in this.CurrentFlowProject.FlowProjectDataContext.OrderFormFieldLocalList)
                    {
                        firstProgress.Update("Verifying Order Form Field: " + field.FieldName);
                        if (field.IsRequired == true && string.IsNullOrEmpty(field.SelectedValue))
                        {
                            FlowMessageBox msg = new FlowMessageBox("Complete the Order Form", "Please Complete the required fields in the order form before you submit your order");
                            msg.CancelButtonVisible = false;
                            msg.ShowDialog();

                            foundMissingValue = true;
                            break;
                        }
                    }

                    canContinue = !foundMissingValue;
                }
                else
                {
                    firstProgress.Complete("Order Aborted");
                    return;
                }
            }



            firstProgress.Update("Verifying Shipment Information");
            firstProgress.Step();

            //bool hasBulkOrders = false;
            Organization orgBulkShipping = null;
            //if (this.CurrentFlowProject.HasFlowPickupOrders)
            //    hasBulkOrders = true;
            //if (hasBulkOrders)
            //{
                if (cmbAllShippingMethods.SelectedItem == null)
                {
                    FlowMessageBox msg = new FlowMessageBox("Please Select Shipping", "Please select a shipping method");
                    msg.CancelButtonVisible = false;
                    msg.ShowDialog();
                    return;
                }

                if (((KeyValuePair<int, string>)cmbAllShippingMethods.SelectedItem).Value == "Ship To School")
                {
                    orgBulkShipping = this.CurrentFlowProject.Organization;
                }

                if (((KeyValuePair<int, string>)cmbAllShippingMethods.SelectedItem).Value == "Ship To Studio")
                {
                    orgBulkShipping = this.CurrentFlowProject.Studio;
                }

                if ((orgBulkShipping.OrganizationShippingAddressLine1 == null && orgBulkShipping.OrganizationShippingAddressLine2 == null) ||
                    orgBulkShipping.OrganizationShippingCity == null ||
                    orgBulkShipping.OrganizationShippingStateCode == null ||
                    orgBulkShipping.OrganizationShippingZipCode == null)
                {
                    FlowMessageBox msg = new FlowMessageBox("Missing Shipping Info", "There is no shipping information for the selected shipping type (" + orgBulkShipping.OrganizationName + ")");
                    msg.CancelButtonVisible = false;
                    msg.ShowDialog();
                    return;
                }
            //}

            this.sortField1 = SubmitOrderSortControl.SortField1;
            this.sortField2 = SubmitOrderSortControl.SortField2;

            this.sortField1 = sortField1;
            this.sortField2 = sortField2;

            this.includeAllSubjectsAndImages = includeAllSubjects;

            this.FlowController.ApplicationPanel.IsEnabled = false;
            this.FlowController.ProjectController.IsUploading = true;
            SubmitTheOrders(orgBulkShipping, firstProgress);
        }

        private void SubmitTheOrders(Organization orgForBulkShipping, NotificationProgressInfo firstProgress)
        {

            firstProgress.Update("Building list of Orders");
            firstProgress.Step();

            List<List<object>> allOrders = new List<List<object>>();


            if (this.CurrentFlowProject.FlowProjectDataContext.FilteredOrderPackages.Any(o => o.ShipSeparately == false))
            {
                List<OrderPackage> BulkOrderPackages = this.CurrentFlowProject.FlowProjectDataContext.FilteredOrderPackages.Where(o => o.ShipSeparately == false).ToList();
                List<object> order = new List<object>();
                order.Add(BulkOrderPackages);
                order.Add(orgForBulkShipping);
                allOrders.Add(order);
            }


            //THIS IS HOW IT WAS DONE FOR IQ ORDERS
            //List<OrderPackage> AllDirectOrderPackages = this.CurrentFlowProject.NewOnlineOrders.Where(o => o.ShipmentType == "direct").ToList();
            //if (AllDirectOrderPackages != null && AllDirectOrderPackages.Count > 0)
            //{
            //    List<int?> UniqueIQOrderIDs = AllDirectOrderPackages.Select(op => op.IQOrderID).Distinct().ToList();

            //    foreach (int IQOrderID in UniqueIQOrderIDs)
            //    {
            //        List<OrderPackage> thisDirectOrderPackages = this.CurrentFlowProject.NewOnlineOrders.Where(o => o.ShipmentType == "direct" && o.IQOrderID == IQOrderID).ToList();
            //        List<object> order = new List<object>();
            //        order.Add(thisDirectOrderPackages);
            //        order.Add(null);
            //        allOrders.Add(order);
            //    }
            //}


            List<OrderPackage> AllDirectOrderPackages = this.CurrentFlowProject.FlowProjectDataContext.FilteredOrderPackages.Where(o => o.ShipSeparately == true).ToList();
            if (AllDirectOrderPackages != null && AllDirectOrderPackages.Count > 0)
            {
                List<string> UniqueAdds = AllDirectOrderPackages.Select(op => op.ShippingAddress).Distinct().ToList();

                foreach (string Address in UniqueAdds)
                {
                    List<OrderPackage> thisDirectOrderPackages = this.CurrentFlowProject.FlowProjectDataContext.FilteredOrderPackages.Where(o => o.ShipSeparately == true && o.ShippingAddress == Address).ToList();
                    List<object> order = new List<object>();
                    order.Add(thisDirectOrderPackages);
                    order.Add(null);
                    allOrders.Add(order);
                }
            }





            //if (orderFilter.Contains(" - ")) //resend a specific orderid
            //{
            //    string oldOrderId = orderFilter.Split(" - ")[0];
            //    if (this.CurrentFlowProject.AllFlowOrders.Any(o => o.LabOrderID == Convert.ToInt32(oldOrderId)))
            //    {
            //        List<OrderPackage> BulkOrderPackages = this.CurrentFlowProject.AllFlowOrders.Where(o => o.LabOrderID == Convert.ToInt32(oldOrderId)).ToList();

            //        List<object> order = new List<object>();
            //        order.Add(BulkOrderPackages);
            //        if (BulkOrderPackages[0].ShipmentType == "pickup")
            //            order.Add(orgForBulkShipping);
            //        else
            //            order.Add(null);

            //        allOrders.Add(order);
            //    }
            //}
            //else if (orderFilter.Contains("Filter (New Orders)")) //resend a specific orderid
            //{
            //    if (this.CurrentFlowProject.NewFlowOrdersInFilterView.Any(o => o.ShipSeparately == false))
            //    {
            //        List<OrderPackage> BulkOrderPackages = this.CurrentFlowProject.NewFlowOrdersInFilterView.Where(o => o.ShipSeparately == false).ToList();
            //        List<object> order = new List<object>();
            //        order.Add(BulkOrderPackages);
            //        order.Add(orgForBulkShipping);
            //        allOrders.Add(order);
            //    }

            //    List<OrderPackage> AllDirectOrderPackages = this.CurrentFlowProject.NewFlowOrdersInFilterView.Where(o => o.ShipSeparately == true).ToList();
            //    if (AllDirectOrderPackages != null && AllDirectOrderPackages.Count > 0)
            //    {
            //        List<string> UniqueAdds = AllDirectOrderPackages.Select(op => op.ShippingAddress).Distinct().ToList();

            //        foreach (string Address in UniqueAdds)
            //        {
            //            List<OrderPackage> thisDirectOrderPackages = this.CurrentFlowProject.NewFlowOrdersInFilterView.Where(o => o.ShipSeparately == true && o.ShippingAddress == Address).ToList();
            //            List<object> order = new List<object>();
            //            order.Add(thisDirectOrderPackages);
            //            order.Add(null);
            //            allOrders.Add(order);
            //        }
            //    }
            //}
            //else if (orderFilter.Contains("Filter (All Orders)")) //resend a specific orderid
            //{
            //    if (this.CurrentFlowProject.AllFlowOrdersInFilterView.Any(o => o.ShipSeparately == false))
            //    {
            //        List<OrderPackage> BulkOrderPackages = this.CurrentFlowProject.AllFlowOrdersInFilterView.Where(o => o.ShipSeparately == false).ToList();
            //        List<object> order = new List<object>();
            //        order.Add(BulkOrderPackages);
            //        order.Add(orgForBulkShipping);
            //        allOrders.Add(order);
            //    }

            //    List<OrderPackage> AllDirectOrderPackages = this.CurrentFlowProject.AllFlowOrdersInFilterView.Where(o => o.ShipSeparately == true).ToList();
            //    if (AllDirectOrderPackages != null && AllDirectOrderPackages.Count > 0)
            //    {
            //        List<string> UniqueAdds = AllDirectOrderPackages.Select(op => op.ShippingAddress).Distinct().ToList();

            //        foreach (string Address in UniqueAdds)
            //        {
            //            List<OrderPackage> thisDirectOrderPackages = this.CurrentFlowProject.AllFlowOrdersInFilterView.Where(o => o.ShipSeparately == true && o.ShippingAddress == Address).ToList();
            //            List<object> order = new List<object>();
            //            order.Add(thisDirectOrderPackages);
            //            order.Add(null);
            //            allOrders.Add(order);
            //        }
            //    }
            //}
            //else if (orderFilter.Contains("All Orders")) //send all order (even ones that were already sent
            //{
            //    if (this.CurrentFlowProject.AllFlowOrders.Any(o => o.ShipmentType == "pickup"))
            //    {
            //        List<OrderPackage> BulkOrderPackages = this.CurrentFlowProject.AllFlowOrders.Where(o => o.ShipmentType == "pickup").ToList();
            //        List<object> order = new List<object>();
            //        order.Add(BulkOrderPackages);
            //        order.Add(orgForBulkShipping);
            //        allOrders.Add(order);
            //    }

            //    List<OrderPackage> AllDirectOrderPackages = this.CurrentFlowProject.AllFlowOrders.Where(o => o.ShipSeparately == true).ToList();
            //    if (AllDirectOrderPackages != null && AllDirectOrderPackages.Count > 0)
            //    {
            //        List<string> UniqueAdds = AllDirectOrderPackages.Select(op => op.ShippingAddress).Distinct().ToList();

            //        foreach (string Address in UniqueAdds)
            //        {
            //            List<OrderPackage> thisDirectOrderPackages = this.CurrentFlowProject.AllFlowOrders.Where(o => o.ShipSeparately == true && o.ShippingAddress == Address).ToList();
            //            List<object> order = new List<object>();
            //            order.Add(thisDirectOrderPackages);
            //            order.Add(null);
            //            allOrders.Add(order);
            //        }
            //    }
            //}
            //else //send all New Orders
            //{
            //    if (this.CurrentFlowProject.NewFlowOrders.Any(o => o.ShipSeparately == false))
            //    {
            //        List<OrderPackage> BulkOrderPackages = this.CurrentFlowProject.NewFlowOrders.Where(o => o.ShipSeparately == false).ToList();
            //        List<object> order = new List<object>();
            //        order.Add(BulkOrderPackages);
            //        order.Add(orgForBulkShipping);
            //        allOrders.Add(order);
            //    }

            //    List<OrderPackage> AllDirectOrderPackages = this.CurrentFlowProject.NewFlowOrders.Where(o => o.ShipSeparately == true).ToList();
            //    if (AllDirectOrderPackages != null && AllDirectOrderPackages.Count > 0)
            //    {
            //        List<string> UniqueAdds = AllDirectOrderPackages.Select(op => op.ShippingAddress).Distinct().ToList();

            //        foreach (string Address in UniqueAdds)
            //        {
            //            List<OrderPackage> thisDirectOrderPackages = this.CurrentFlowProject.NewFlowOrders.Where(o => o.ShipSeparately == true && o.ShippingAddress == Address).ToList();
            //            List<object> order = new List<object>();
            //            order.Add(thisDirectOrderPackages);
            //            order.Add(null);
            //            allOrders.Add(order);
            //        }
            //    }
            //}
            firstProgress.Complete("Done Verifying Order Form");

            PreventSleep();
            
            FlowBackgroundWorkerManager.RunWorker(SendOrderWorker, SendOrderWorker_Completed, allOrders);



        }


        void SendOrderWorker(object sender, DoWorkEventArgs e)
        {

           


            List<List<object>> allOrders = e.Argument as List<List<object>>;

            IShowsNotificationProgress progressNotification = ObjectFactory.GetInstance<IShowsNotificationProgress>();
            _notificationInfo = new NotificationProgressInfo("Uploading Orders To Lab", "Processing All Orders...", 0, allOrders.Count, 1);
            _notificationInfo.NotificationProgress = progressNotification;
            progressNotification.ShowNotification(_notificationInfo);

            //wait to init subjects
            int waitCount = 0;
            string dots = ".";
            while (this.CurrentFlowProject.initingSubjects)
            {
                dots += ".";
                _notificationInfo.Update("Waiting for subjects to initialize." + dots);
                waitCount++;
                Thread.Sleep(3000);

                if (waitCount == (4 * 60)) //if its taken longer than 4 minutes, bail
                    this.CurrentFlowProject.initingSubjects = false;

            }
            _notificationInfo.Update("Processing All Orders...", 0, allOrders.Count, 1, true);

            foreach (List<object> order in allOrders)
            {
                _notificationInfo.Step();
                List<OrderPackage> CurrentOrderPackages = order[0] as List<OrderPackage>;
                Organization org = order[1] as Organization;
                string orderID = PlaceOrder(CurrentOrderPackages, org);
                if (string.IsNullOrEmpty(orderID))
                    continue;
                DateTime submitTimeStamp = DateTime.Now;
                CurrentFlowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                {
                    //mark the order as sent
                    foreach (OrderPackage op in CurrentOrderPackages)
                    {
                        op.LabOrderID = Convert.ToInt32(orderID);
                        op.LabOrderDate = submitTimeStamp;
                        op.ModifyDate = DateTime.Now;
                        op.SubjectOrder.Subject.FlagForMerge = true;
                        if (!this.CurrentFlowProject.FlowProjectDataContext.OrderHistoryMasters.Any(h => h.OrderID == orderID))
                        {
                            OrderHistoryMaster historyMaster = new OrderHistoryMaster();
                            historyMaster.OrderID = orderID;
                            historyMaster.OrderSubmittedDate = submitTimeStamp;
                            this.CurrentFlowProject.FlowProjectDataContext.OrderHistoryMasters.InsertOnSubmit(historyMaster);
                            CurrentFlowProject.FlowProjectDataContext.SubmitChanges();
                        }

                        if (!op.OrderHistories.Any(h => h.OrderID == orderID))
                        {
                            OrderHistory orderHistory = new OrderHistory();
                            orderHistory.OrderID = orderID;
                            orderHistory.OrderPackageID = op.OrderPackageID;
                            orderHistory.OrderSubmittedDate = submitTimeStamp;
                            op.OrderHistories.Add(orderHistory);
                        }


                    }
                    CurrentFlowProject.FlowProjectDataContext.SubmitChanges();
                    this.CurrentFlowProject.UpdateListOfFlowOrders();
                }));

                Thread.Sleep(2000);
            }
        }

        void SendOrderWorker_Completed(object sender, RunWorkerCompletedEventArgs e)
        {
            if(saveOrderPath == this.FlowMasterDataContext.PreferenceManager.Application.LabOrderQueueDirectory)
                _notificationInfo.Update("Done processing. Added to Upload Queue.");
            else
                _notificationInfo.Update("Done processing: " + saveOrderPath);
            _notificationInfo.ShowDoneButton();
            this.FlowController.ApplicationPanel.IsEnabled = true;
            this.FlowController.ProjectController.IsUploading = false;
            this.SelectedFlowProject.UpdateOrderHistoryMasterList();

            //if(this.cmbOrderFilter.Items != null && this.cmbOrderFilter.Items.Count > 0)
            //    this.cmbOrderFilter.SelectedIndex = this.cmbOrderFilter.Items.Count - 1;

            foreach (OrderFormFieldLocal field in this.CurrentFlowProject.FlowProjectDataContext.OrderFormFieldLocalList)
            {
                field.SelectedValue = field.FieldValue;
            }
            this.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();
            this.CurrentFlowProject.FlowProjectDataContext.UpdateFilteredOrderPackages();
            AllowSleep();
        }

        private string PlaceOrder(List<OrderPackage> CurrentOrderPackages, Organization orgForBulkShipping)
        {


            if (this.FlowController.ProjectController.FlowMasterDataContext.IsPLICConfiguration || this.FlowController.ProjectController.FlowMasterDataContext.IsPUDConfiguration)
            {
                SendLabOrderNew SendLabOrder = new SendLabOrderNew(this.CurrentFlowProject, this.FlowController);
                SendLabOrder.SortField1 = sortField1;
                SendLabOrder.SortField2 = sortField2;
                SendLabOrder.IncludeAllSubjectsAndImages = includeAllSubjectsAndImages;
                SendLabOrder.RenderGreenScreen = renderGreenScreen;
                SendLabOrder.ApplyCrop = applyCrops;
                SendLabOrder.UseOriginalFileNames = useOriginalFileNames;
                //SendLabOrder.OrderFilter = orderFilter;
                SendLabOrder.SaveOrderPath = saveOrderPath;
                SendLabOrder.OrderOptionList = OrderOptionList;
                return SendLabOrder.SendOrder(CurrentOrderPackages, orgForBulkShipping);
            }
            else
            {
                SendLabOrder SendLabOrder = new SendLabOrder(this.CurrentFlowProject, this.FlowController);
                SendLabOrder.SortField1 = sortField1;
                SendLabOrder.SortField2 = sortField2;
                SendLabOrder.IncludeAllSubjectsAndImages = includeAllSubjectsAndImages;
                SendLabOrder.RenderGreenScreen = renderGreenScreen;
                SendLabOrder.ApplyCrop = applyCrops;
                SendLabOrder.UseOriginalFileNames = useOriginalFileNames;
                //SendLabOrder.OrderFilter = orderFilter;
                SendLabOrder.SaveOrderPath = saveOrderPath;
                SendLabOrder.OrderOptionList = OrderOptionList;
                return SendLabOrder.SendOrder(CurrentOrderPackages, orgForBulkShipping);
            }
        }















		/// <summary>
		/// 
		/// </summary>
		/// <param name="includeAllSubjects"></param>
        private void SubmitOrderOriginal(FlowProject flowProject, bool includeAllSubjects, string saveOrderExportPath, string sortField1, string sortField2, string orderFilter)
		{
			if (flowProject.HasIncompleteProducts || flowProject.HasMissingImages)
			{
				//this.ProjectController.CurrentFlowProject = flowProject;
				this.ProjectController.OpenCapture();
			}
			else
			{
                this.FlowController.ApplicationPanel.IsEnabled = false;
                this.FlowController.ProjectController.IsUploading = true;

                bool renderGreenScreen = false;
                if (chkRenderGreenScreen.IsChecked == true)
                    renderGreenScreen = true;

				//flowProject.SubmitOrder(includeAllSubjects, saveOrderExportPath, sortField1, sortField2);
                bool submitReport = SubmitOrderIncludeAllOrderDetailReport.IsChecked.Value;
                //string orderFilterValue = cmbOrderFilter.SelectedValue.ToString();

                string reportFileName = null;
                string orderFormFileName = null;
                string catalogPackagesFileName = null;

                string doneFileFilePath = FlowContext.Combine(FlowContext.FlowTempDirPath, "done.txt");
                if (!File.Exists(doneFileFilePath))
                    File.Create(doneFileFilePath);

				//return;
                FlowMessageBox msg;
				FlowBackgroundWorkerManager.RunWorker(
					delegate
					{
                        string message = "";


                        this.CurrentFlowProject.LoadingProgressInfo = new NotificationProgressInfo("Preparing Order", "Initializing... Preparing Reports");
                        this.CurrentFlowProject.LoadingProgressInfo.NotificationProgress = ObjectFactory.GetInstance<IShowsNotificationProgress>();
                        this.CurrentFlowProject.LoadingProgressInfo.AllowCancel = true;
                        this.CurrentFlowProject.LoadingProgressInfo.Display(true);
                        
                        this.CurrentFlowProject.LoadingProgressInfo.Update("Preparing Reports", 0, 4, 1, false);
                        List<OrderPackage> applicablePackages;

                        //get applicable subjects
                        IOrderedEnumerable<Subject> applicableSubjects = flowProject.GetOrderSubjects(includeAllSubjects, sortField1, sortField2, orderFilter, out applicablePackages);

                        if (applicablePackages.Count() == 0 && (!includeAllSubjects || (includeAllSubjects && this.CurrentFlowProject.SubjectCount == 0)))
                        {
                            this.Dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                           {
                               msg = new FlowMessageBox("No Orders", "There are no Packages to Order");
                               msg.CancelButtonVisible = false;
                               msg.ShowDialog();
                               this.FlowController.ApplicationPanel.IsEnabled = true;
                               this.FlowController.ProjectController.IsUploading = false;
                               this.CurrentFlowProject.LoadingProgressInfo.Complete("No Packages to Order");
                           }));
                            return;
                        }

                        //check to see if all applicable subject images have green screen settings
                        if (renderGreenScreen)
                        {
                            this.CurrentFlowProject.LoadingProgressInfo.Message = "Checking for Green Screen Settings";
                            List<int> checkedIds = new List<int>();
                            int gsImages = 0;
                            int gsImagesWithSettings = 0;
                            foreach (OrderPackage pkg in applicablePackages)
                            {
                                foreach (OrderProduct prod in pkg.OrderProducts)
                                {
                                    foreach (OrderProductNode node in prod.OrderProductNodes)
                                    {
                                        if (node.SubjectImage != null && node.ImageQuixProductNodeType == 1) // 1 is the image nodetype
                                        {
                                            if (!checkedIds.Contains((int)node.SubjectImageID))
                                            {
                                                gsImages++;
                                                if (node.SubjectImage.GreenScreenSettings != null)
                                                    gsImagesWithSettings++;
                                                checkedIds.Add((int)node.SubjectImageID);
                                            }
                                        }

                                    }
                                }
                            }
                            if (gsImages != gsImagesWithSettings)
                            {
                                bool continueAnyway = false;
                                this.Dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                                {
                                    msg = new FlowMessageBox("Warning", "WARNING\n\n" + (gsImages - gsImagesWithSettings) + " out of " + gsImages + " Images DO NOT have green screen settings set\n\n Are you sure you want to continue?");
                                    msg.CancelButtonVisible = true;
                                    msg.buttonCancel.Content = "NO";
                                    msg.buttonOkay.Content = "YES";
                                    if ((bool)msg.ShowDialog())
                                        continueAnyway = true;
                                    else
                                    {
                                        this.FlowController.ApplicationPanel.IsEnabled = true;
                                        this.FlowController.ProjectController.IsUploading = false;
                                    }
                                }));
                                if (!continueAnyway)
                                {
                                    this.CurrentFlowProject.LoadingProgressInfo.Complete("Cancel - Incomplete GreenScreen Settings");
                                    return;
                                }
                            }
                        }
                        
                        this.CurrentFlowProject.LoadingProgressInfo.Message = "Creating Order Form pdf";
                        this.CurrentFlowProject.LoadingProgressInfo.Step();
                        //Send Order Form
                        this.Dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                        {
                            this.FlowController.ProjectController.CurrentFlowProject.SelectedReportName = "Order Form";

                            FlowReportView newReportView = new FlowReportView("Order Form", this.FlowController.ProjectController.CurrentFlowProject);

                            ReportDataDefinitions.GetReportData(newReportView, FlowController.ProjectController.CurrentFlowProject.FlowProjectDataContext.FilteredOrderPackages.ToList());
                            FlowReport report = new FlowReport(newReportView);
                            report.OrderNumber = "None";
                            report.SortString = sortField1 + ", " + sortField2 + ", LastName, FirstName";
                            report.Generate();

                            orderFormFileName = System.IO.Path.Combine(FlowContext.FlowTempDirPath, "OrderForm.pdf");
                            ReportDataDefinitions.ExportReport(
                                this.FlowController.ProjectController.CurrentFlowProject.SelectedReportName + " - " + this.FlowController.ProjectController.CurrentFlowProject.FlowProjectName,
                                report.C1Document, new KeyValuePair<string, string>("PDF", "Adobe PDF (*.pdf)|*.pdf"),
                                orderFormFileName, false
                            );
                        }));

                        if (this.CurrentFlowProject.LoadingProgressInfo.IsCanceled)
                            return;
                        this.CurrentFlowProject.LoadingProgressInfo.Message = "Creating Catalog Package pdf";
                        this.CurrentFlowProject.LoadingProgressInfo.Step();
                        //Send Catalog Summary
                        this.Dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                        {
                            this.FlowController.ProjectController.CurrentFlowProject.SelectedReportName = "Catalog Packages";

                            FlowReportView newReportView = new FlowReportView("Catalog Packages", this.FlowController.ProjectController.CurrentFlowProject);

                            ReportDataDefinitions.GetReportData(newReportView, FlowController.ProjectController.CurrentFlowProject.FlowProjectDataContext.FilteredOrderPackages.ToList());
                            FlowReport report = new FlowReport(newReportView);
                            report.OrderNumber = "None";
                            report.SortString = sortField1 + ", " + sortField2 + ", LastName, FirstName";
                            report.Generate();

                            catalogPackagesFileName = System.IO.Path.Combine(FlowContext.FlowTempDirPath, "CatalogPackages.pdf");
                            ReportDataDefinitions.ExportReport(
                                this.FlowController.ProjectController.CurrentFlowProject.SelectedReportName + " - " + this.FlowController.ProjectController.CurrentFlowProject.FlowProjectName,
                                report.C1Document, new KeyValuePair<string, string>("PDF", "Adobe PDF (*.pdf)|*.pdf"),
                                catalogPackagesFileName, false
                            );
                        }));

                        if (this.CurrentFlowProject.LoadingProgressInfo.IsCanceled)
                            return;
                        this.CurrentFlowProject.LoadingProgressInfo.Message = "Creating Subject Orders Detailed pdf";
                        this.CurrentFlowProject.LoadingProgressInfo.Step();
                        if (submitReport)
                        {
                            this.Dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                            {
                                this.FlowController.ProjectController.CurrentFlowProject.ReportShowPrice = false;
                                //this.FlowController.ProjectController.CurrentFlowProject.SelectedReportOrderFilter = orderFilterValue;
                                this.FlowController.ProjectController.CurrentFlowProject.SelectedReportName = "Subject Orders Detailed";

                                FlowReportView newReportView = new FlowReportView("Subject Orders Detailed", this.FlowController.ProjectController.CurrentFlowProject);

                                ObservableCollection<ProjectSubjectField> subjectFields = new ObservableCollection<ProjectSubjectField>();
                                if (sortField1 != "[None]")
                                    subjectFields.Add(this.FlowController.ProjectController.CurrentFlowProject.ProjectSubjectFieldList.First(sfl => sfl.SubjectFieldDisplayName == sortField1));
                                if (sortField2 != "[None]")
                                    subjectFields.Add(this.FlowController.ProjectController.CurrentFlowProject.ProjectSubjectFieldList.First(sfl => sfl.SubjectFieldDisplayName == sortField2));

                                newReportView.SubjectFieldList = subjectFields;

                                ReportDataDefinitions.GetReportData(newReportView, this.FlowController.ProjectController.CurrentFlowProject.FlowProjectDataContext.FilteredOrderPackages.ToList<OrderPackage>());
                                FlowReport report = new FlowReport(newReportView);

                                report.ApplicablePackages = applicablePackages;
                                report.ApplicableSubjects = applicableSubjects.ToList<Subject>();
                                report.Generate();

                                reportFileName = System.IO.Path.Combine(FlowContext.FlowTempDirPath, "SubjectOrdersDetailed.pdf");
                                ReportDataDefinitions.ExportReport(
                                    this.FlowController.ProjectController.CurrentFlowProject.SelectedReportName + " - " + this.FlowController.ProjectController.CurrentFlowProject.FlowProjectName,
                                    report.C1Document, new KeyValuePair<string, string>("PDF", "Adobe PDF (*.pdf)|*.pdf"),
                                    reportFileName, false
                                );
                            }));
                        }
                        if (this.CurrentFlowProject.LoadingProgressInfo.IsCanceled)
                            return;
                        this.CurrentFlowProject.LoadingProgressInfo.Step();
                        this.CurrentFlowProject.LoadingProgressInfo.Complete("Done Preparing Reports");

                        if (!flowProject.SubmitOrder(includeAllSubjects, saveOrderExportPath, sortField1, sortField2, orderFilter, reportFileName, orderFormFileName, catalogPackagesFileName,  applicableSubjects, applicablePackages, renderGreenScreen, useOriginalFileNames, out message))
                        {
                            
                            bool result = false;
                            this.Dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                            {
                                this.FlowController.ApplicationPanel.IsEnabled = true;
                                this.FlowController.ProjectController.IsUploading = false;
                                msg = new FlowMessageBox("Problem With Order", message);
                                if (msg.ShowDialog() == true)
                                    result = true;
                            }));

                            if (result == false)
                                return;
                        }

                        
                       this.Dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                            {
                                this.FlowController.ApplicationPanel.IsEnabled = true;
                                this.FlowController.ProjectController.IsUploading = false;
                                 this.FlowController.Project();
                            }));
					}
				);
			}
		}

/// /////////////////////////////////////////////////////////////////////////
		#endregion EXPORT SECTION
/// /////////////////////////////////////////////////////////////////////////

		private void btnCancel_Click(object sender, RoutedEventArgs e)
		{
            logger.Info("Cancel button clicked in Order Panel");
			this.FlowController.Project();
		}

        public void showOrder()
		{
            this.cmbFlowProjects.SelectedItem = DefaultProject;
            //tabExport.IsSelected = true;
            //rdoExportServer.IsChecked = forUpload;
            //rdoExportFile.IsChecked = !forUpload;
            
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            logger.Info("Save button clicked in Order Panel");
            this.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();
            this.ExportTabControl.SelectedIndex = 0;
        }

        private void chkUseOption_Click(object sender, RoutedEventArgs e)
        {
            logger.Info("Use Option checkbox clicked: isChecked={0}", ((CheckBox)e.Source).IsChecked);
            FlowCatalogOption co = (FlowCatalogOption)((CheckBox)e.Source).DataContext;
        }

        private void cmbAllOrderOptions_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
        
        }

        private void cmbAllOrderOptions_Loaded(object sender, RoutedEventArgs e)
        {
            
        }

        private void chkUseOption_Checked(object sender, RoutedEventArgs e)
        {
            FlowCatalogOption co = (FlowCatalogOption)((CheckBox)e.Source).DataContext;
            updateFlowCatalogOptionIsUsed(co, true);
        }

        private void updateFlowCatalogOptionIsUsed(FlowCatalogOption co, bool isChecked)
        {
            if (isChecked)
            {
                //if it was false, its about to be true
                OrderOrderOption ooo = new OrderOrderOption();
                ooo.FlowCatalogOptionGuid = (Guid)co.FlowCatalogOptionGuid;
                ooo.ResourceURL = co.ImageQuixCatalogOption.ResourceURL;
                this.ProjectController.CurrentFlowProject.FlowProjectDataContext.OrderOrderOptions.InsertOnSubmit(ooo);


            }
            else
            {
                //if it was true, its about to be false
                this.ProjectController.CurrentFlowProject.FlowProjectDataContext.OrderOrderOptions.DeleteOnSubmit(
                   this.ProjectController.CurrentFlowProject.FlowProjectDataContext.OrderOrderOptions.First(ooo => ooo.FlowCatalogOptionGuid == co.FlowCatalogOptionGuid)
               );
            }

            this.ProjectController.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();
            this.cmbAllOrderOptions.SelectedIndex = -1;
        }

        private void chkUseOption_Unchecked(object sender, RoutedEventArgs e)
        {
            FlowCatalogOption co = (FlowCatalogOption)((CheckBox)e.Source).DataContext;
            updateFlowCatalogOptionIsUsed(co, false);
        }

        private void cmbAllOrderOptions_DropDownOpened(object sender, EventArgs e)
        {
            
        }

        private void _this_Loaded(object sender, RoutedEventArgs e)
        {
            //if its enabled, than we can call the proper shipping method
            if (this.IsEnabled)
            {
                InitPanel();
                
            }
        }

        private void InitPanel()
        {
            if (this.CurrentFlowProject.FlowCatalog == null)
            {
                cmbAllOrderOptions.Visibility = Visibility.Collapsed;
                cmbAllShippingOptions.Visibility = Visibility.Collapsed;
                cmbAllPackagingOptions.Visibility = Visibility.Collapsed;
                lblOrderOptions.Visibility = Visibility.Collapsed;
                lblShippingOptions.Visibility = Visibility.Collapsed;
                lblPackagingOptions.Visibility = Visibility.Collapsed;
            }
            else
            {

                OrderOptionList = new List<OrderOptionSelected>();
                if (this.CurrentFlowProject.FlowCatalog.ImageQuixCatalog.CatalogOptionGroupList.Count > 0)
                {
                    foreach (ImageQuixCatalogOptionGroup grp in this.CurrentFlowProject.FlowCatalog.ImageQuixCatalog.CatalogOptionGroupList)
                    {
                        if (grp.IsImageOption || (!FlowController.ProjectController.FlowMasterDataContext.IsPLICConfiguration && grp.Label.ToLower().Contains("image option")))
                            continue;

                        StackPanel tmpSP = new StackPanel();
                        tmpSP.Orientation = Orientation.Horizontal;
                        tmpSP.Margin = new Thickness(3);

                        Label tmpLabel = new Label();
                        tmpLabel.Content = grp.Label;
                        tmpSP.Children.Add(tmpLabel);

                        ComboBox tmpCmb = new ComboBox();
                        tmpCmb.Width = 200;
                        tmpCmb.Margin = new Thickness(8, 0, 0, 0);
                        tmpCmb.DisplayMemberPath = "Label";
                        tmpCmb.SelectionChanged += new SelectionChangedEventHandler(Add_OrderOption);

                        foreach (FlowCatalogOption o in this.CurrentFlowProject.FlowCatalog.FlowCatalogOptions)
                        {
                            //ComboBoxItem item = new ComboBoxItem();
                            //item.Content = o.ImageQuixCatalogOption;
                            if(o.ImageQuixCatalogOption.ImageQuixCatalogOptionGroupID == grp.ImageQuixCatalogOptionGroupID)
                                tmpCmb.Items.Add(o.ImageQuixCatalogOption);
                            
                        }
                        if (grp.IsMandatory == true)
                            tmpCmb.SelectedIndex = 0;
                        else
                            tmpCmb.Items.Add("<None>");
                        tmpSP.Children.Add(tmpCmb);


                        this.orderOptionsPanel.Children.Add(tmpSP);
                    }

                }



                //if (this.CurrentFlowProject.FlowCatalog.FlowCatalogOrderOptionList.Count > 0)
                //    cmbAllOrderOptions.ItemsSource = this.CurrentFlowProject.FlowCatalog.FlowCatalogOrderOptionList;
                //else
                {
                    cmbAllOrderOptions.Visibility = Visibility.Collapsed;
                    lblOrderOptions.Visibility = Visibility.Collapsed;
                }


                if(1==1)// (this.CurrentFlowProject.FlowCatalog.FlowCatalogShippingOptionList.Count == 0)
                {
                    cmbAllShippingOptions.Visibility = Visibility.Collapsed;
                    lblShippingOptions.Visibility = Visibility.Collapsed;
                }
                else
                {
                    cmbAllShippingOptions.SelectedIndex = 0;
                }

                if (1==1)//(this.CurrentFlowProject.FlowCatalog.FlowCatalogPackagingOptionList.Count == 0)
                {
                    cmbAllPackagingOptions.Visibility = Visibility.Collapsed;
                    lblPackagingOptions.Visibility = Visibility.Collapsed;
                }


                SelectProperShippingMethod();
                SelectProperPackagingMethod();

            }

            //if (this.CurrentFlowProject.IsGreenScreen == true)
            //{
            //    lblRenderGreenScreen.Visibility = Visibility.Visible;
            //    chkRenderGreenScreen.Visibility = Visibility.Visible;

            //}
            //else
            //{
            //    lblRenderGreenScreen.Visibility = Visibility.Collapsed;
            //    chkRenderGreenScreen.Visibility = Visibility.Collapsed;
            //}

        }


        private void Add_OrderOption(object sender, SelectionChangedEventArgs e)
        {
            if (((ComboBox)e.Source).SelectedValue == "<None>")
                return;
            ImageQuixCatalogOption op = ((ComboBox)e.Source).SelectedItem as ImageQuixCatalogOption;
            if (!this.FlowController.ProjectController.FlowMasterDataContext.IsPLICConfiguration)
            {
                if(OrderOptionList.Any(oo=>oo.OptionGroupLabel == op.ImageQuixCatalogOptionGroup.ResourceURL))
                {
                    OrderOptionList.RemoveAll(oo=>oo.OptionGroupLabel == op.ImageQuixCatalogOptionGroup.ResourceURL);
                }
            }
            OrderOptionList.Add(new OrderOptionSelected(op.ImageQuixCatalogOptionGroup.ResourceURL, op.ResourceURL, op.ImageQuixCatalogOptionGroup.Label));

        }
        private void SelectProperShippingMethod()
        {
            if (this.CurrentFlowProject.FlowProjectDataContext.OrderShippingOptions.Count() > 0)
            {
                IEnumerable<FlowCatalogOption> defaults = this.CurrentFlowProject.FlowCatalog.FlowCatalogShippingOptionList.Where(so => so.FlowCatalogOptionGuid == this.CurrentFlowProject.FlowProjectDataContext.OrderShippingOptions.First().FlowCatalogOptionGuid);
                if (defaults.Count() > 0)
                {
                    cmbAllShippingOptions.SelectedItem = defaults.First();
                }

            }
        }

        private void SelectProperPackagingMethod()
        {
            if (this.CurrentFlowProject.FlowProjectDataContext.OrderPackagingOptions.Count() > 0)
            {
                IEnumerable<FlowCatalogOption> defaults = this.CurrentFlowProject.FlowCatalog.FlowCatalogPackagingOptionList.Where(so => so.FlowCatalogOptionGuid == this.CurrentFlowProject.FlowProjectDataContext.OrderPackagingOptions.First().FlowCatalogOptionGuid);
                if (defaults.Count() > 0)
                {
                    cmbAllPackagingOptions.SelectedItem = defaults.First();
                }

            }
        }

        private void cmbAllShippingOptions_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.CurrentFlowProject == null || this.CurrentFlowProject.FlowProjectDataContext == null ||
                this.CurrentFlowProject.FlowProjectDataContext.OrderShippingOptions == null || ((ComboBox)e.Source) == null)
                return;

            foreach (OrderShippingOption oso in this.CurrentFlowProject.FlowProjectDataContext.OrderShippingOptions)
                this.CurrentFlowProject.FlowProjectDataContext.OrderShippingOptions.DeleteOnSubmit(oso);

            FlowCatalogOption co = null;
            if ((FlowCatalogOption)((ComboBox)e.Source).SelectedItem == null)
            {
                if(CurrentFlowProject.FlowCatalog != null && CurrentFlowProject.FlowCatalog.FlowCatalogShippingOptionList != null)
                    co = CurrentFlowProject.FlowCatalog.FlowCatalogShippingOptionList[0];
            }
            else
                co = (FlowCatalogOption)((ComboBox)e.Source).SelectedItem;
            if (co != null)
            {
                OrderShippingOption so = new OrderShippingOption();
                so.FlowCatalogOptionGuid = (Guid)co.FlowCatalogOptionGuid;
                so.ResourceURL = co.ImageQuixCatalogOption.ResourceURL;
                this.CurrentFlowProject.FlowProjectDataContext.OrderShippingOptions.InsertOnSubmit(so);

                this.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();
            }
        }

        private void cmbAllPackagingOptions_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.CurrentFlowProject == null || this.CurrentFlowProject.FlowProjectDataContext == null ||
                this.CurrentFlowProject.FlowProjectDataContext.OrderPackagingOptions == null || ((ComboBox)e.Source) == null)
                return;

            foreach (OrderPackagingOption opo in this.CurrentFlowProject.FlowProjectDataContext.OrderPackagingOptions)
                this.CurrentFlowProject.FlowProjectDataContext.OrderPackagingOptions.DeleteOnSubmit(opo);

            FlowCatalogOption co = (FlowCatalogOption)((ComboBox)e.Source).SelectedItem;

            if (co != null)
            {
                OrderPackagingOption po = new OrderPackagingOption();
                po.FlowCatalogOptionGuid = (Guid)co.FlowCatalogOptionGuid;
                po.ResourceURL = co.ImageQuixCatalogOption.ResourceURL;
                this.ProjectController.CurrentFlowProject.FlowProjectDataContext.OrderPackagingOptions.InsertOnSubmit(po);

                this.ProjectController.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();
            }
        }

        private void cmbAllShippingMethods_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.CurrentFlowProject.ShippingMethod = cmbAllShippingMethods.SelectedIndex;
            this.CurrentFlowProject.FlowMasterDataContext.SubmitChanges();
        }
  



        //private void btnConfigUpload_Click(object sender, RoutedEventArgs e)
        //{
        //    PreferenceSectionsPanel pnl = base.FlowController.ApplicationPanel.ShowPreferencesPanel();
        //    pnl.FlowController = this.FlowController;
        //    pnl.ShowUploadPanel();
        //}

        //private void ImageTypeOption_Checked(object sender, RoutedEventArgs e)
        //{
        //    RadioButton source = sender as RadioButton;

        //    if (source != null)
        //    {
        //        this.SelectedImageType = source.CommandParameter as ProjectImageType;
        //    }
        //}




        internal void ApplyDefaults()
        {
            if (this.ProjectController.FlowMasterDataContext.PreferenceManager.Application.OrderUploadToLab)
            { rdoSubmitOrder.IsChecked = true; rdoSaveOrder.IsChecked = false; }
            if (this.ProjectController.FlowMasterDataContext.PreferenceManager.Application.OrderSaveLocally)
            { rdoSubmitOrder.IsChecked = false; rdoSaveOrder.IsChecked = true; }
            if (!String.IsNullOrEmpty(this.ProjectController.FlowMasterDataContext.PreferenceManager.Application.OrderSort1))
            {
                string s1 = this.ProjectController.FlowMasterDataContext.PreferenceManager.Application.OrderSort1;
                if (this.ProjectController.CurrentFlowProject.SortableSubjectFieldList.Any(s => s.ToLower().Replace(" ", "").Trim() == s1.ToLower().Replace(" ", "").Trim()))
                {
                    SubmitOrderSortControl.SortField1 = this.ProjectController.CurrentFlowProject.SortableSubjectFieldList.FirstOrDefault(s => s.ToLower().Replace(" ", "").Trim() == s1.ToLower().Replace(" ", "").Trim());
                    SaveOrderSortControl.SortField1 = this.ProjectController.CurrentFlowProject.SortableSubjectFieldList.FirstOrDefault(s => s.ToLower().Replace(" ", "").Trim() == s1.ToLower().Replace(" ", "").Trim());
                }
            }
            if (!String.IsNullOrEmpty(this.ProjectController.FlowMasterDataContext.PreferenceManager.Application.OrderSort2))
            {
                string s2 = this.ProjectController.FlowMasterDataContext.PreferenceManager.Application.OrderSort2;
                if (this.ProjectController.CurrentFlowProject.SortableSubjectFieldList.Any(s => s.ToLower().Replace(" ", "").Trim() == s2.ToLower().Replace(" ", "").Trim()))
                {
                    SubmitOrderSortControl.SortField2 = this.ProjectController.CurrentFlowProject.SortableSubjectFieldList.FirstOrDefault(s => s.ToLower().Replace(" ", "").Trim() == s2.ToLower().Replace(" ", "").Trim());
                    SaveOrderSortControl.SortField2 = this.ProjectController.CurrentFlowProject.SortableSubjectFieldList.FirstOrDefault(s => s.ToLower().Replace(" ", "").Trim() == s2.ToLower().Replace(" ", "").Trim());
                }
            }
            chkOriginalFileName.IsChecked = this.ProjectController.FlowMasterDataContext.PreferenceManager.Application.OrderUseOriginalFileNames;
            if (CurrentFlowProject.isPUD)
                chkApplyCrops.IsChecked = this.ProjectController.FlowMasterDataContext.PreferenceManager.Application.OrderApplyCrops;
            if (CurrentFlowProject.IsGreenScreen == true)
                chkRenderGreenScreen.IsChecked = this.ProjectController.FlowMasterDataContext.PreferenceManager.Application.OrderRenderGreenScreen;

            SubmitOrderIncludeAllSubjectsCheckBox.IsChecked = this.ProjectController.FlowMasterDataContext.PreferenceManager.Application.OrderIncludeAllSubjects;
            SubmitOrderIncludeAllOrderDetailReport.IsChecked = this.ProjectController.FlowMasterDataContext.PreferenceManager.Application.OrderIncludeSubjectDetailReport;
        }

        private void cmbOrderFilter_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CurrentFlowProject.FlowProjectDataContext.UpdateFilteredOrderPackages();
        }

        private void FilterChanged(object sender, RoutedEventArgs e)
        {

            if (CurrentFlowProject != null)
            {
                CurrentFlowProject.FlowProjectDataContext.UpdateFilteredOrderPackages();
                CurrentFlowProject.FlowProjectDataContext.UpdateOrderFilterValues();

            }
        }


        internal void LoadAgain()
        {
            this.CurrentFlowProject.FlowProjectDataContext.UpdateFilteredOrderPackages();
        }
    }

    public class OrderOptionSelected
    {
        public string OptionGroupLabel { get; set; }
        public string OptionLabel { get; set; }
        public string OptionGroupName { get; set; }

        public OrderOptionSelected(string grp, string opt, string name)
        {
            OptionGroupLabel = grp;
            OptionLabel = opt;
            OptionGroupName = name;
        }
    }
}
