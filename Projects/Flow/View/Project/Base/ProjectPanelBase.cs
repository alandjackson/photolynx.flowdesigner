﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Controller;
using Flow.Controller.Project;
using Flow.Schema.LinqModel.DataContext;

namespace Flow.View.Project.Base
{
	/// <summary>
	/// Base class for Project panels (View, Create New, View Templates, Edit Template;
	/// -- Used for central assignment of the FlowController object;
	/// -- OnSetFlowController is overridden in subclasses allowing for access to the FlowMaster db
	/// (through the FlowController object) for intializing data controls
	/// </summary>
	public class ProjectPanelBase : UserControl
	{
		private FlowController _flowController = null;
		public FlowController FlowController
		{
			get { return _flowController; }
			set
			{
				_flowController = value;

				if(_flowController != null)
					OnSetFlowController();
			}
		}

		public ProjectController ProjectController
		{
			get { return (_flowController != null) ? _flowController.ProjectController : null; } 
		}

		public FlowMasterDataContext FlowMasterDataContext
		{
			get { return (this.ProjectController != null) ? this.ProjectController.FlowMasterDataContext : null; }
		}

		public FlowProject CurrentFlowProject
		{
			get { return (this.ProjectController != null) ? this.ProjectController.CurrentFlowProject : null; }
		}

        public FlowProject NewFlowProject
        {
            get { return (this.ProjectController != null) ? this.ProjectController.NewFlowProject : null; }
        }

		public Flow.Schema.LinqModel.DataContext.ProjectTemplate CurrentProjectTemplate
		{
			get { return (this.ProjectController != null) ? this.ProjectController.CurrentProjectTemplate : null; }
		}

		public ProjectPanelBase() { }

		protected virtual void OnSetFlowController() {}

	}
}
