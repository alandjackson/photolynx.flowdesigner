﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Controller;
using Flow.Controller.Project;
using Flow.Schema.LinqModel.DataContext;
using Flow.View.Project.Base;
using Flow.View.Preferences;
using Flow.Lib;
using System.Threading;
using Flow.View.Dialogs;
using NLog;
using System.ComponentModel;

namespace Flow.View.Project
{
	/// <summary>
	/// Interaction logic for NewProjectPanel.xaml
	/// </summary>
	public partial class NewProjectPanel : ProjectPanelBase
	{
        private static Logger logger = LogManager.GetCurrentClassLogger();
		
		public NewProjectPanel()
		{
            InitializeComponent();

		}

		protected override void OnSetFlowController()
		{
			this.DataContext = this.ProjectController;

			 //Populate the Photographer combobox
            cmbPhotographer.ItemsSource =
                from u in this.FlowMasterDataContext.UserList.OrderBy(u => u.UserLastName)
                    where u.IsPhotographer == true
                select new
                {
                    PhotographerName = u.FormalFullName,
                    PhotographerID = u.UserID
                };

            //this.OrderFormWrapDisplay.OrderFormFieldList = this.ProjectController.OrderFormFieldsLocalList;


			// Initialize the Project Date control
//			calProjectDate.SelectedDate = DateTime.Today;
//			calProjectDate.Text = calProjectDate.DisplayDate.ToString();
		}

		/// <summary>
		/// Resets the SelectedValue of comboxes to null
		/// </summary>
		public void RefreshBindings()
		{
            // Populate the Photographer combobox
            cmbPhotographer.ItemsSource =
                from u in this.FlowMasterDataContext.UserList.OrderBy(u => u.UserLastName)
                where u.IsPhotographer == true
                select new
                {
                    PhotographerName = u.FormalFullName,
                    PhotographerID = u.UserID
                };
            //this.ProjectController.NewFlowProject = new FlowProject();

			cmbStudio.GetBindingExpression(ComboBox.SelectedValueProperty).UpdateTarget();
			cmbProjectTemplate.GetBindingExpression(ComboBox.SelectedValueProperty).UpdateTarget();
			cmbOrganization.GetBindingExpression(ComboBox.SelectedValueProperty).UpdateTarget();
			cmbPhotographer.GetBindingExpression(ComboBox.SelectedValueProperty).UpdateTarget();

            cmbOrganization.SelectedIndex = -1;

            cmbSelectedGroupImageFlags.ItemsSource = null;
            cmbSelectedGroupImageFlags.ItemsSource = this.NewFlowProject.SelectedGroupImageFlagsNoBlank;
		}

		private void btnCancel_Click(object sender, RoutedEventArgs e)
		{
            logger.Info("Cancel button clicked");
			this.FlowController.Project();
            this.ProjectController.MainProjectPanel.ShowViewProjectsPanel();
		}

		private void btnCreate_Click(object sender, RoutedEventArgs e)
		{
            logger.Info("Create Project button clicked. Creating project...");

			// Get a reference to a new project
		
			Flow.Schema.LinqModel.DataContext.ProjectTemplate projectTemplate =
				cmbProjectTemplate.SelectedItem as Flow.Schema.LinqModel.DataContext.ProjectTemplate;

            if (this.NewFlowProject.SelectedGroupImageFlags != null && this.NewFlowProject.SelectedGroupImageFlags.Count > 0)
                this.NewFlowProject.GroupImageFlags = String.Join(";", this.NewFlowProject.SelectedGroupImageFlags);

            this.NewFlowProject.FlowCatalogID = projectTemplate.FlowCatalogID;
            this.NewFlowProject.FlowProjectTemplateGuid = projectTemplate.ProjectTemplateGuid;
            this.NewFlowProject.FlowProjectTemplateShortDescription = projectTemplate.ShortDescription;
            if(cmbCatalog.SelectedItem != null)
                this.NewFlowProject.FlowCatalogID = ((FlowCatalog)cmbCatalog.SelectedItem).FlowCatalogID;

			// Set explicitly to avoid strange conversion error
			this.NewFlowProject.DateCreated = calProjectDate.SelectedDate.Value;
            this.NewFlowProject.DueDate = calDueDate.SelectedDate.Value;

			// Create the new project record and all associated project resources
			this.ProjectController.GenerateProject(this.NewFlowProject, projectTemplate);

            this.ProjectController.CreateOrderFormFields(projectTemplate.ProjectTemplateDesc);

            foreach (OrderFormFieldLocal field in this.ProjectController.OrderFormFieldsLocalList)
            {
                this.NewFlowProject.FlowProjectDataContext.OrderFormFieldLocals.InsertOnSubmit(field);
            }

            this.NewFlowProject.FlowProjectDataContext.SubmitChanges();
            this.FlowMasterDataContext.SubmitChanges();
            
            //load the project
            //this.FlowController.ProjectController.Load(this.NewFlowProject, false);
            //this.NewFlowProject.RefreshPropertyBinding("ProjectSubjectFieldList");
            this.ProjectController.Load(this.NewFlowProject, false);
            






            if (ProjectController.CustomSettings.CreateNewProjectShowOrderForm == true)
            {
                
                bool canContinue = false;
                int failCnt = 0;
                while (canContinue == false)
                {
                    Thread.Sleep(1000);//give it a second to load the new project
                    try
                    {
                        LabOrderFormDialog dlg = new LabOrderFormDialog(this.CurrentFlowProject.FlowProjectDataContext);
                        dlg.lblTitle.Content = ProjectController.CustomSettings.LabOrderFormLabel;
                        if (dlg.ShowDialog() == true)
                        {
                            bool foundMissingValue = false;
                            foreach (OrderFormFieldLocal field in this.CurrentFlowProject.FlowProjectDataContext.OrderFormFieldLocalList)
                            {

                                if (field.IsRequired == true && string.IsNullOrEmpty(field.SelectedValue))
                                {
                                    FlowMessageBox msg = new FlowMessageBox("Complete the Order Form", "Please Complete the required fields in the order form before you continue");
                                    msg.CancelButtonVisible = false;
                                    msg.ShowDialog();

                                    foundMissingValue = true;
                                    break;
                                }
                            }

                            canContinue = !foundMissingValue;
                        }
                        else
                        {
                            canContinue = true;
                        }
                    }
                    catch
                    {
                        if (failCnt++ > 3)
                            canContinue = true;
                    }
                }
            }


            this.ProjectController.MainProjectPanel.ShowViewProjectsPanel();
			// Move control to the DataImport or Capture display
            //this.ProjectController.CurrentFlowProject.FlowProjectDataContext.SubjectDataLoaded += delegate
            {
                foreach (ProjectSubjectField psf in this.ProjectController.CurrentFlowProject.FlowProjectDataContext.ProjectSubjectFields)
                {
                    if (this.ProjectController.CustomSettings.HideTicketCodeAndPackageSummaryInCapture && (psf.ProjectSubjectFieldDisplayName == "Ticket Code" || psf.ProjectSubjectFieldDisplayName == "Package Summary"))
                    {
                        psf.IsProminentField = false;
                        psf.IsShownInEdit = false;
                        continue;
                    }
                    if (psf.IsProminentField)
                    {
                        psf.IsShownInEdit = true;
                    }
                    else
                    {
                        psf.IsShownInEdit = false;
                    }
                    this.ProjectController.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();

                }

                logger.Info("Done creating project.");
                if (this.CurrentFlowProject.GetEventTriggerSetting(1))
                     this.FlowController.DataImport();
                else if (this.CurrentFlowProject.GetEventTriggerSetting(2))
                {
                    this.FlowController.Reports();
                    this.CurrentFlowProject.SelectedReportName = "New Ticket Barcodes";
                }
                else if (chkImportOnCreate.IsChecked.Value)
                    this.FlowController.DataImport();
                else
                {
                    this.FlowController.Capture();
                }
            };
		}

        private void btnAddUser_Click(object sender, RoutedEventArgs e)
        {
            logger.Info("Add User button clicked");

            if (gridAddPhotographer.Visibility == Visibility.Visible)
            {
                gridAddPhotographer.Visibility = Visibility.Collapsed;
                return;
            }

            gridAddOrg.Visibility = Visibility.Collapsed;
            gridAddPhotographer.Visibility = Visibility.Visible;
            this.txtNewPhotographerFirstName.Text = "";
            this.txtNewPhographertLastName.Text = "";
            this.txtNewPhotographerFirstName.Focus();

            
            //PreferenceSectionsPanel pnl =  base.FlowController.ApplicationPanel.ShowPreferencesPanel();
            //pnl.FlowController = this.FlowController;
            //pnl.ShowUserPanel();
            
        }

        private void btnAddOrganization_Click(object sender, RoutedEventArgs e)
        {
            logger.Info("Add Organization button clicked");

            if (gridAddOrg.Visibility == Visibility.Visible)
            {
                gridAddOrg.Visibility = Visibility.Collapsed;
                return;
            }

            this.txtNewOrgName.Text = "";
            this.cmbNewOrgType.SelectedIndex = 0;
            gridAddPhotographer.Visibility = Visibility.Collapsed;
            gridAddOrg.Visibility = Visibility.Visible;
            this.txtNewOrgName.Focus();

            
            //PreferenceSectionsPanel pnl = base.FlowController.ApplicationPanel.ShowPreferencesPanel();
            //pnl.FlowController = this.FlowController;
            //pnl.ShowOrganizationPanel();
        }

        private void btnSaveOrg_Click(object sender, RoutedEventArgs e)
        {
            logger.Info("Save Organization button clicked");
            if (ProjectController.CustomSettings.RequireCustomerAddress == true && (txtNewOrgAddress.Text.Length < 1 || txtNewOrgCity.Text.Length < 1 || txtNewOrgState.Text.Length < 1 || txtNewOrgZip.Text.Length < 1))
            {
                FlowMessageBox msg = new FlowMessageBox("Add Organization", "You must enter all shipping information before saving");
                msg.CancelButtonVisible = false;
                msg.ShowDialog();
                return;
            }
            if (txtNewOrgName.Text == null || txtNewOrgName.Text.Length == 0)
            {
                FlowMessageBox msg = new FlowMessageBox("Add Organization", "You must enter an organization name before saving");
                msg.CancelButtonVisible = false;
                msg.ShowDialog();
                return;
            }

            string orgName = txtNewOrgName.Text;
            int orgType = (int)cmbNewOrgType.SelectedValue;
            Organization org = new Organization();
            org.OrganizationName = orgName;
            org.OrganizationTypeID = orgType;
            org.OrganizationShippingAddressLine1 = txtNewOrgAddress.Text;
            org.OrganizationShippingCity = txtNewOrgCity.Text;
            org.OrganizationShippingStateCode = txtNewOrgState.Text;
            org.OrganizationShippingZipCode = txtNewOrgZip.Text;
            org.DateCreated = DateTime.Now;
            org.OrganizationGuid = Guid.NewGuid();
            this.FlowMasterDataContext.Organizations.InsertOnSubmit(org);
            this.FlowMasterDataContext.SubmitChanges();

            this.FlowMasterDataContext.RefreshOrgList();

            //this.cmbOrganization.ItemsSource = this.ProjectController.FlowMasterDataContext.SiteOrganizationList;
            this.cmbOrganization.UpdateLayout();
            this.cmbOrganization.SelectedValue = org.OrganizationID;
            gridAddOrg.Visibility = Visibility.Collapsed;

            

        }

        private void btnSavePhotographer_Click(object sender, RoutedEventArgs e)
        {
            logger.Info("Save Photographer button clicked");

            if (txtNewPhotographerFirstName.Text == null || txtNewPhotographerFirstName.Text.Length == 0 ||
                txtNewPhographertLastName.Text == null || txtNewPhographertLastName.Text.Length == 0)
            {
                FlowMessageBox msg = new FlowMessageBox("Add Photographer", "You must enter first and last name before saving");
                msg.CancelButtonVisible = false;
                msg.ShowDialog();
                return;
            }

            string first = txtNewPhotographerFirstName.Text;
            string last = txtNewPhographertLastName.Text;
            User usr = new User();
            usr.UserFirstName = first;
            usr.UserLastName = last;
            usr.IsPhotographer = true;
            usr.UserGuid = Guid.NewGuid();
            this.FlowMasterDataContext.Users.InsertOnSubmit(usr);
            this.FlowMasterDataContext.SubmitChanges();

            this.cmbPhotographer.ItemsSource = from u in this.FlowMasterDataContext.Users.OrderBy(u => u.UserLastName)
                                               //where u.IsPhotographer == true
                                               select new
                                               {
                                                   PhotographerName = u.FormalFullName,
                                                   PhotographerID = u.UserID
                                               };
            this.cmbPhotographer.UpdateLayout();
            this.cmbPhotographer.SelectedValue = usr.UserID;

            this.FlowMasterDataContext.RefreshUserList();
            
            gridAddPhotographer.Visibility = Visibility.Collapsed;
        }

        private void lbxPackageGreenScreen_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.DataContext == null)
                return;
            if ((sender as ListBox).SelectedItem == null)
                return;
            //if (initSelChng == false) { initSelChng = true; return; }

            
            //string gsBack = ((sender as ListBox).SelectedItem as Flow.Schema.LinqModel.DataContext.FlowMaster.GreenScreenBackground).FullPath;
            if (this.NewFlowProject != null)
            {
                this.NewFlowProject.DefaultGreenScreenBackgroundObject = ((sender as ListBox).SelectedItem as Flow.Schema.LinqModel.DataContext.FlowMaster.GreenScreenBackground);
                DefaultBackgroundThumbnail.DataContext = this.NewFlowProject.DefaultGreenScreenBackgroundObject;
                BackgroundThumbnailBig.DataContext = this.NewFlowProject.DefaultGreenScreenBackgroundObject;
            }
        }


        private void btnAddGroupImageFlag_Click(object sender, RoutedEventArgs e)
        {
            string v = (sender as Button).DataContext as String;
            this.NewFlowProject.SelectedGroupImageFlags.Add(v);
            cmbAllGroupImageFlags.IsDropDownOpen = false;
            cmbSelectedGroupImageFlags.ItemsSource = null;
            cmbSelectedGroupImageFlags.ItemsSource = this.NewFlowProject.SelectedGroupImageFlagsNoBlank;
            //this.NewFlowProject.UpdateSelectedGroupImageFlags();
            //cmbSelectedGroupImageFlags.UpdateLayout();
        }

        private void btnShowGroupImageFlags_Click(object sender, RoutedEventArgs e)
        {
            cmbAllGroupImageFlags.IsDropDownOpen = !cmbAllGroupImageFlags.IsDropDownOpen;
        }

        private void btnRemoveGroupImageFlag_Click(object sender, RoutedEventArgs e)
        {
            string v = (sender as Button).DataContext as String;
            this.NewFlowProject.SelectedGroupImageFlags.Remove(v);
            cmbSelectedGroupImageFlags.ItemsSource = null;
            cmbSelectedGroupImageFlags.ItemsSource = this.NewFlowProject.SelectedGroupImageFlagsNoBlank;
            //this.NewFlowProject.UpdateSelectedGroupImageFlags();
            //cmbSelectedGroupImageFlags.UpdateLayout();
        }

        private void calProjectDate_SourceUpdated(object sender, DataTransferEventArgs e)
        {
            calDueDate.SelectedDate = calProjectDate.SelectedDate.Value.AddDays(7);
        }

        private void TextBlock_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            string v = (sender as TextBlock).DataContext as String;
            this.NewFlowProject.SelectedGroupImageFlags.Add(v);
            cmbAllGroupImageFlags.IsDropDownOpen = false;
            cmbSelectedGroupImageFlags.ItemsSource = null;
            cmbSelectedGroupImageFlags.ItemsSource = this.NewFlowProject.SelectedGroupImageFlagsNoBlank;
        }

	}

    public class StringNullOrEmptyToVisibilityConverter : System.Windows.Markup.MarkupExtension, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return string.IsNullOrEmpty(value as string)
                ? Visibility.Collapsed : Visibility.Visible;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }
}
