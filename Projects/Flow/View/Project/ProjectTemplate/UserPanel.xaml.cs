﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Controls.Extension;
using Flow.Schema;
using Flow.Schema.LinqModel.DataContext;
using Flow.View.Project.ProjectTemplate.Base;

namespace Flow.View.Project.ProjectTemplate
{
	/// <summary>
	/// Interaction logic for ImageOptions.xaml
	/// </summary>
	public partial class UserPanel : ProjectTemplatePanelBase
	{
			
		public UserPanel()
		{
            InitializeComponent();
            //PopulateUserComboBox();
            this.DataContext = this.FlowMasterDataContext;
		}

        private void cmbUser_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }


        private void PopulateUserComboBox()
        {

            


            // Populate the User combobox
            cmbUser.ItemsSource =
                from u in this.FlowMasterDataContext.Users
                where u.UserTypeID == 1
                select new
                {
                    UserName = u.UserLastName + ", " + u.UserFirstName,
                    UserID = u.UserID
                };

        }
	}
}