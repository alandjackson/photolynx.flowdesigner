﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Controller.Project;
using Flow.Lib.Helpers;
using Flow.Schema.LinqModel.DataContext;

namespace Flow.View.Project.ProjectTemplate.Base
{

	/// <summary>
	/// Interaction logic for ProjectTemplateDisplayBase.xaml
	/// </summary>
	public class ProjectTemplatePanelBase : UserControl
	{
		private ProjectController _projectController = null;
		internal ProjectController ProjectController
		{
			get { return _projectController; }
			set
			{
				_projectController = value;

				_projectController.PropertyChanged += new PropertyChangedEventHandler(ProjectController_PropertyChanged);

				// Notify that project controller has been assigned, thus that a project template is current
				OnCurrentProjectTemplateChanged();
				
				this.FlowMasterDataContext = _projectController.FlowMasterDataContext;
				this.DataContext = this.ProjectController;
			}
		}

		internal FlowMasterDataContext FlowMasterDataContext { get; private set; }

		public Flow.Schema.LinqModel.DataContext.ProjectTemplate CurrentProjectTemplate
		{
			get { return (this.ProjectController == null) ? null : this.ProjectController.CurrentProjectTemplate ; }
		}

		private void ProjectController_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			if(e.PropertyName == "CurrentProjectTemplate")
				OnCurrentProjectTemplateChanged();
		}

		/// <summary>
		/// Provides derived class objects with method for handling selection of a Project Template
		/// </summary>
		protected virtual void OnCurrentProjectTemplateChanged() { }
	}
}