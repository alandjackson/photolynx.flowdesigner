﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Controller.Project;
using Flow.Lib;
using Flow.Lib.Helpers;
using Flow.Schema.LinqModel.DataContext;
using Flow.View.Project.ProjectTemplate.Base;

namespace Flow.View.Project.ProjectTemplate
{
	/// <summary>
	/// Interaction logic for GeneralSection.xaml
	/// </summary>
	public partial class BarcodeSettingsPanel : ProjectTemplatePanelBase
	{

		private string _scannedValue = "";

		private string _initiationSequence = null;
		private int? _barcodeScanValueLength = null;
		private string _terminationSequence = null;

		public BarcodeSettingsPanel()
		{
			InitializeComponent();
		}

		private void CalibrateButton_Click(object sender, RoutedEventArgs e)
		{
			string embeddedValue = Regex.Escape(txtEmbeddedValue.Text);

			Regex pattern = new Regex(embeddedValue);

			Match match = pattern.Match(_scannedValue);

			if (match.Success)
			{
				_initiationSequence = _scannedValue.Substring(0, match.Index);

				_barcodeScanValueLength = match.Length;

				_terminationSequence = _scannedValue.Substring(match.Index + match.Length);

				txtInitiationSequence.Text = _initiationSequence.BarcodeEscape();

				txtValuePattern.Text = _barcodeScanValueLength.ToString() + "-character alphanumeric value";

				txtTerminationSequence.Text = _terminationSequence.BarcodeEscape();

				this.PatternResultPanel.Visibility = Visibility.Visible;
				this.ValueNotFoundLabel.Visibility = Visibility.Collapsed;

			}
			else
			{
				this.PatternResultPanel.Visibility = Visibility.Collapsed;
				this.ValueNotFoundLabel.Visibility = Visibility.Visible;
			}

			_scannedValue = "";
		
		}

		private void txtScannedValue_PreviewTextInput(object sender, TextCompositionEventArgs e)
		{
			_scannedValue += e.Text;
		}

		private void ConfirmBarcodeSettings_Click(object sender, RoutedEventArgs e)
		{
			this.ProjectController.CurrentProjectTemplate.BarcodeScanInitSequence = _initiationSequence;
			this.ProjectController.CurrentProjectTemplate.BarcodeScanValueLength = _barcodeScanValueLength;
			this.ProjectController.CurrentProjectTemplate.BarcodeScanTermSequence = _terminationSequence;

			this.PatternResultPanel.Visibility = Visibility.Collapsed;
		}

		private void ClearBarcodeSettings_Click(object sender, RoutedEventArgs e)
		{
			_initiationSequence = null;
			_barcodeScanValueLength = null;
			_terminationSequence = null;
	
			txtInitiationSequence.Text = "";
			txtTerminationSequence.Text = "";
			txtInitiationSequence.Focus();

			this.PatternResultPanel.Visibility = Visibility.Collapsed;
		}

	}
}
