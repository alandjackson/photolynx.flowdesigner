﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Schema.LinqModel.DataContext;
using Flow.View.Project.ProjectTemplate.Base;
using Flow.View.Dialogs;

namespace Flow.View.Project.ProjectTemplate
{
	/// <summary>
	/// Interaction logic for GroupingSection.xaml
	/// </summary>
	public partial class SubjectFieldPanel : ProjectTemplatePanelBase
	{
		protected DependencyProperty CurrentSubjectFieldProperty =
			DependencyProperty.Register("CurrentSubjectField", typeof(SubjectField), typeof(SubjectFieldPanel));

		protected SubjectField CurrentSubjectField
		{
			get { return (SubjectField)this.GetValue(CurrentSubjectFieldProperty); }
			set { this.SetValue(CurrentSubjectFieldProperty, value); }
		}

		private Dictionary<ProjectTemplateSubjectField, Button> _assignedFieldSourceButtonMap;
		private Dictionary<ProjectTemplateSubjectField, Button> AssignedFieldSourceButtonMap
		{
			get
			{
				if (_assignedFieldSourceButtonMap == null)
					_assignedFieldSourceButtonMap = new Dictionary<ProjectTemplateSubjectField, Button>();

				return _assignedFieldSourceButtonMap;
			}
		}

		public SubjectFieldPanel()
		{
			InitializeComponent();
		}

		private void lstCommonFields_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			SynchCurrentSubjectFieldWithList();
		}

		private void btnAddSubjectField_Click(object sender, RoutedEventArgs e)
		{
            this.AddSubjectFields.IsEnabled = true;
            this.btnAddSubjectField.IsEnabled = false;

			this.CurrentSubjectField = new SubjectField();
			//txtFieldName.Text = "";
			//cmbFieldType.SelectedIndex = 0;
			//chkIsRequired.IsChecked = false;
			//chkIsScanKey.IsChecked = false;
			//chkIsProminentField.IsChecked = true;
			//chkIsSearchable.IsChecked = true;
		}

		private void btnSaveSubjectField_Click(object sender, RoutedEventArgs e)
		{
            if (this.CurrentSubjectField == null)
                return;
            if (this.CurrentSubjectField.SubjectFieldDesc.ToLower().Replace(" ", "").Contains("-"))
            {
                
                FlowMessageBox msg = new FlowMessageBox("Subject Field Name Check", "A subject field cannot contain a dash, press okay to remove the dash.");
                
                if (msg.ShowDialog() == true)
                {
                    this.CurrentSubjectField.SubjectFieldDesc = this.CurrentSubjectField.SubjectFieldDesc.Replace("-", "");
                    this.CurrentSubjectField.SubjectFieldDisplayName = this.CurrentSubjectField.SubjectFieldDisplayName.Replace("-", " ");
                }
                else
                {
                    return;
                }
            }
            if (this.CurrentSubjectField.SubjectFieldDesc.ToLower().Replace(" ","").Trim() == "subjectid")
            {
                this.CurrentSubjectField.SubjectFieldDesc = "";
                this.CurrentSubjectField.SubjectFieldDisplayName = "";
                FlowMessageBox msg = new FlowMessageBox("Subject Field Name Check", "SubjectID is a reserved column name and cannot be used");
                msg.CancelButtonVisible = false;
                msg.ShowDialog();
                return;
            }
            if (this.CurrentSubjectField.SubjectFieldDesc.ToLower().Replace(" ", "").Trim() == "firstname")
            {
                this.CurrentSubjectField.SubjectFieldDesc = "";
                this.CurrentSubjectField.SubjectFieldDisplayName = "";
                FlowMessageBox msg = new FlowMessageBox("Subject Field Name Check", "FirstName is a reserved column name and cannot be used");
                msg.CancelButtonVisible = false;
                msg.ShowDialog();
                return;
            }
            if (this.CurrentSubjectField.SubjectFieldDesc.ToLower().Replace(" ", "").Trim() == "lastname")
            {
                this.CurrentSubjectField.SubjectFieldDesc = "";
                this.CurrentSubjectField.SubjectFieldDisplayName = "";
                FlowMessageBox msg = new FlowMessageBox("Subject Field Name Check", "LastName is a reserved column name and cannot be used");
                msg.CancelButtonVisible = false;
                msg.ShowDialog();
                return;
            }
            //this.CurrentSubjectField.SubjectFieldDisplayName = txtFieldName.Text;
            this.CurrentSubjectField.FieldDataTypeID = ((FieldDataType)cmbFieldType.SelectedValue).FieldDataTypeID;
            //this.CurrentSubjectField.IsRequiredField = chkIsRequired.IsChecked.Value;

			if (!this.FlowMasterDataContext.SubjectFieldList.ContainsDesc(this.CurrentSubjectField))
			{
				if (this.CurrentSubjectField.SubjectFieldID == 0)
					this.FlowMasterDataContext.SubjectFieldList.Add(this.CurrentSubjectField);
			}

			lstCommonFields.SelectedItem = this.CurrentSubjectField;
//			lstSubjectFields.ScrollIntoView(lstSubjectFields.SelectedItem);

            txtFieldName.Text = "";
            cmbFieldType.SelectedIndex = -1;
            chkIsRequired.IsChecked = false;
            chkIsScanKey.IsChecked = false;
            chkIsProminentField.IsChecked = false;
            chkIsSearchable.IsChecked = false;
            this.AddSubjectFields.IsEnabled = false;
            this.btnAddSubjectField.IsEnabled = true;

		}

		private void btnCancelSubjectFieldEdit_Click(object sender, RoutedEventArgs e)
		{
			SynchCurrentSubjectFieldWithList();
            txtFieldName.Text = "";
            cmbFieldType.SelectedIndex = -1;
            chkIsRequired.IsChecked = false;
            chkIsScanKey.IsChecked = false;
            chkIsProminentField.IsChecked = false;
            chkIsSearchable.IsChecked = false;
            this.AddSubjectFields.IsEnabled = false;
            this.btnAddSubjectField.IsEnabled = true;
		}

		//private void SetSubjectFieldListDataSource()
		//{
		//    lstCommonFields.ItemsSource = this.FlowMasterDataContext.SubjectFieldList.Where(f => !f.IsSystemField);
		//}

		private void SynchCurrentSubjectFieldWithList()
		{
			if (lstCommonFields.SelectedIndex > -1)
			{
				this.CurrentSubjectField = lstCommonFields.SelectedItem as SubjectField;
				//txtFieldName.Text = _currentSubjectField.SubjectFieldDisplayName;
				//cmbFieldType.SelectedValue = _currentSubjectField.FieldDataTypeID;
				//chkIsRequired.IsChecked = _currentSubjectField.IsRequiredField;
			}
		}

		//private void lstSubjectFields_Drop(object sender, DragEventArgs e)
		//{
		//    if (e.Data.GetDataPresent(DataFormats.UnicodeText, true))
		//    {
		//        SubjectField selectedSubjectField = e.Data.GetData(DataFormats.UnicodeText, true) as SubjectField;

		//        if (selectedSubjectField != null)
		//        {
		//            ProjectTemplateSubjectField newSubjectField =
		//                new ProjectTemplateSubjectField(this.CurrentProjectTemplate.ProjectTemplateID, selectedSubjectField);

		//            if(!this.CurrentProjectTemplate.ProjectTemplateSubjectFieldList.ContainsDesc(newSubjectField))
		//                this.CurrentProjectTemplate.ProjectTemplateSubjectFieldList.Add(newSubjectField);

		//            e.Effects = ((e.KeyStates & DragDropKeyStates.ControlKey) != 0)
		//                ? DragDropEffects.Copy : DragDropEffects.Move;
		//        }

		//        e.Handled = true;
		//    }
		//}

		//private void lstCommonFields_MouseDoubleClick(object sender, MouseButtonEventArgs e)
		//{
		//}

		//private void lstSubjectFields_MouseDoubleClick(object sender, MouseButtonEventArgs e)
		//{
		//}

		private void AssignSubjectField_Click(object sender, RoutedEventArgs e)
		{
			Button source = e.OriginalSource as Button;

			if (source != null)
			{
				SubjectField field = source.Tag as SubjectField;

				if (field != null)
				{
					ProjectTemplateSubjectField assignedField = new ProjectTemplateSubjectField(field);

					this.CurrentProjectTemplate.ProjectTemplateSubjectFieldList.Add(assignedField);

					source.Visibility = Visibility.Hidden;

					this.AssignedFieldSourceButtonMap.Add(assignedField, source);
				}
			}
		}

		private void PrimaryKeyCheckBox_Checked(object sender, RoutedEventArgs e)
		{
			CheckBox source = sender as CheckBox;

			ProjectTemplateSubjectField selectedItem = source.DataContext as ProjectTemplateSubjectField;

			foreach (ProjectTemplateSubjectField item in this.CurrentProjectTemplate.ProjectTemplateSubjectFieldList)
			{
				item.IsKeyField = (item == selectedItem);
			}
		}

		private void ToggleIsPrimaryKey_Click(object sender, RoutedEventArgs e)
		{
		}

		private void ToggleIsRequired_Click(object sender, RoutedEventArgs e)
		{
			bool toggleValue = false;

			foreach (ProjectTemplateSubjectField item in this.CurrentProjectTemplate.ProjectTemplateSubjectFieldList)
			{
				if (!item.IsRequiredField)
				{
					toggleValue = true;
					break;
				}
			}

			foreach (ProjectTemplateSubjectField item in this.CurrentProjectTemplate.ProjectTemplateSubjectFieldList)
			{
				item.IsRequiredField = toggleValue;
			}
		}

		private void ToggleIsScanKey_Click(object sender, RoutedEventArgs e)
		{

		}

		private void ToggleIsProminentField_Click(object sender, RoutedEventArgs e)
		{
			bool toggleValue = false;

			foreach (ProjectTemplateSubjectField item in this.CurrentProjectTemplate.ProjectTemplateSubjectFieldList)
			{
				if (!item.IsProminentField)
				{
					toggleValue = true;
					break;
				}
			}

			foreach (ProjectTemplateSubjectField item in this.CurrentProjectTemplate.ProjectTemplateSubjectFieldList)
			{
				item.IsProminentField = toggleValue;
			}
		}

		private void ToggleIsSearchable_Click(object sender, RoutedEventArgs e)
		{
			bool toggleValue = false;

			foreach (ProjectTemplateSubjectField item in this.CurrentProjectTemplate.ProjectTemplateSubjectFieldList)
			{
				if (!item.IsSearchableField)
				{
					toggleValue = true;
					break;
				}
			}

			foreach (ProjectTemplateSubjectField item in this.CurrentProjectTemplate.ProjectTemplateSubjectFieldList)
			{
				item.IsSearchableField = toggleValue;
			}
		}

		private void DetachAll_Click(object sender, RoutedEventArgs e)
		{
			this.CurrentProjectTemplate.ProjectTemplateSubjectFieldList.Clear();

			foreach (Button item in this.AssignedFieldSourceButtonMap.Values)
			{
				item.Visibility = Visibility.Visible;
			}

			this.AssignedFieldSourceButtonMap.Clear();
		}

		private void DetachItem_Click(object sender, RoutedEventArgs e)
		{
			Button source = e.OriginalSource as Button;

			if (source != null)
			{
				ProjectTemplateSubjectField field = source.Tag as ProjectTemplateSubjectField;

				if (field != null)
				{
					this.CurrentProjectTemplate.ProjectTemplateSubjectFieldList.Remove(field);

					this.AssignedFieldSourceButtonMap[field].Visibility = Visibility.Visible;
		
					this.AssignedFieldSourceButtonMap.Remove(field);
				}
			}

		}

		private void IsScanKey_Check(object sender, RoutedEventArgs e)
		{
			CheckBox source = e.OriginalSource as CheckBox;

			if (source != null)
			{
				ProjectTemplateSubjectField item = source.Tag as ProjectTemplateSubjectField;

				if (item != null)
				{
					foreach (ProjectTemplateSubjectField field in this.CurrentProjectTemplate.ProjectTemplateSubjectFieldList)
					{
						if (item.ProjectTemplateSubjectFieldID != field.ProjectTemplateSubjectFieldID)
							field.IsScanKeyField = false;
					}
				}
			}
		}

		private void SubjectFieldPanel_Loaded(object sender, RoutedEventArgs e)
		{
			foreach (KeyValuePair<ProjectTemplateSubjectField, Button> item in this.AssignedFieldSourceButtonMap)
			{
				item.Value.Visibility = Visibility.Visible;
			}
	
			this.AssignedFieldSourceButtonMap.Clear();
	
			foreach (ProjectTemplateSubjectField projectTemplateSubjectField in this.CurrentProjectTemplate.ProjectTemplateSubjectFieldList)
			{
				SubjectField assignedSubjectField =
					this.FlowMasterDataContext.SubjectFieldList.FirstOrDefault(s => s.SubjectFieldDesc == projectTemplateSubjectField.ProjectTemplateSubjectFieldDesc);

				if (assignedSubjectField != null)
				{
					ListBoxItem listBoxItem = (ListBoxItem)(lstCommonFields.ItemContainerGenerator.ContainerFromItem(assignedSubjectField));

					if (listBoxItem != null)
					{
						ContentPresenter contentPresenter = FindVisualChild<ContentPresenter>(listBoxItem);

						DataTemplate dataTemplate = contentPresenter.ContentTemplate;

						Button button = (Button)dataTemplate.FindName("AssignSubjectFieldButton", contentPresenter);

						this.AssignedFieldSourceButtonMap.Add(projectTemplateSubjectField, button);

						button.Visibility = Visibility.Hidden;
					}
				}
			}
		}

		private T FindVisualChild<T>(DependencyObject obj)
			where T : DependencyObject
		{

			for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
			{
				DependencyObject child = VisualTreeHelper.GetChild(obj, i);

				if (child != null && child is T)
					return (T)child;

				else
				{
					T childOfChild = FindVisualChild<T>(child);

					if (childOfChild != null)
						return childOfChild;
				}
			}

			return null;
		}



	}
}
