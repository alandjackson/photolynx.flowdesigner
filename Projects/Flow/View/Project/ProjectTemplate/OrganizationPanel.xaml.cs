﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Data.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Schema.LinqModel.DataContext;
using Flow.View.Project.ProjectTemplate.Base;

namespace Flow.View.Project.ProjectTemplate
{
	/// <summary>
	/// Interaction logic for OrganizationSection.xaml
	/// </summary>
	public partial class OrganizationPanel : ProjectTemplatePanelBase
	{
		private OrganizationalUnitType _currentOrganizationalUnitType = null;
		private SubjectType _currentSubjectType = null;


		public OrganizationPanel()
		{
			InitializeComponent();
		}

		protected override void OnCurrentProjectTemplateChanged()
		{
			if(this.CurrentProjectTemplate != null)
				SetTreeViewFromOrgStructure();
		}

		private void RefreshOrganizationTypeChildren()
		{
			if (cmbOrganizationType.SelectedIndex > -1)
			{
				int organizationTypeID = (int)cmbOrganizationType.SelectedValue;

				RefreshOrganizationalUnitTypeList(organizationTypeID);
				RefreshSubjectTypeList(organizationTypeID);
			}
		}

		private void RefreshOrganizationalUnitTypeList(int organizationTypeID)
		{
			// Populate the Organizational Unit Type combo box
			lstOrganizationalUnitType.ItemsSource =
				from u in this.FlowMasterDataContext.OrganizationalUnitTypeList
				where u.OrganizationTypeID == organizationTypeID
				select u;

			if (lstOrganizationalUnitType.Items.Count > 0)
				lstOrganizationalUnitType.SelectedIndex = 0;
		}

		private void RefreshSubjectTypeList(int organizationTypeID)
		{
			// Populate the Subject Type combo box
			lstSubjectType.ItemsSource =
				from s in this.FlowMasterDataContext.SubjectTypeList
				where s.OrganizationTypeID == organizationTypeID
				select s;

			if (lstSubjectType.Items.Count > 0)
				lstSubjectType.SelectedIndex = 0;
		}

		private void cmbOrganizationType_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			RefreshOrganizationTypeChildren();
		}

		private void lstOrganizationalUnitType_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			SynchCurrentOrganizationalUnitTypeWithList();
		}

		private void lstSubjectType_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			SynchCurrentSubjectTypeWithList();
		}

///
		#region ORGANIZATIONAL UNIT TYPE EDITOR
///

		private void btnAddOrganizationlUnitType_Click(object sender, RoutedEventArgs e)
		{
			_currentOrganizationalUnitType = new OrganizationalUnitType();
			txtOrganizationalUnitTypeDesc.Text = "";
		}

		private void btnSaveOrganizationalUnitType_Click(object sender, RoutedEventArgs e)
		{
			_currentOrganizationalUnitType.OrganizationalUnitTypeDesc = txtOrganizationalUnitTypeDesc.Text;
			_currentOrganizationalUnitType.OrganizationTypeID = (int)cmbOrganizationType.SelectedValue;

			if (_currentOrganizationalUnitType.OrganizationalUnitTypeID == 0)
				this.FlowMasterDataContext.OrganizationalUnitTypeList.Add(_currentOrganizationalUnitType);

			lstOrganizationalUnitType.SelectedItem = _currentOrganizationalUnitType;
			RefreshOrganizationalUnitTypeList((int)cmbOrganizationType.SelectedValue);
		}

		private void btnCancelOrganizationalUnitTypeEdit_Click(object sender, RoutedEventArgs e)
		{
			SynchCurrentOrganizationalUnitTypeWithList();
		}

		private void SynchCurrentOrganizationalUnitTypeWithList()
		{
			if (lstOrganizationalUnitType.SelectedIndex > -1)
			{
				_currentOrganizationalUnitType = lstOrganizationalUnitType.SelectedItem as OrganizationalUnitType;
				txtOrganizationalUnitTypeDesc.Text = _currentOrganizationalUnitType.OrganizationalUnitTypeDesc;
			}
		}
///
		#endregion
///


///
		#region SUBJECT TYPE EDITOR
///

		private void btnAddSubjectType_Click(object sender, RoutedEventArgs e)
		{
			_currentSubjectType = new SubjectType();
			txtSubjectTypeDesc.Text = "";
		}

		private void btnSaveSubjectType_Click(object sender, RoutedEventArgs e)
		{
			_currentSubjectType.SubjectTypeDesc = txtSubjectTypeDesc.Text;
			_currentSubjectType.OrganizationTypeID = (int)cmbOrganizationType.SelectedValue;

			if (_currentSubjectType.SubjectTypeID == 0)
				this.FlowMasterDataContext.SubjectTypeList.Add(_currentSubjectType);

			lstSubjectType.SelectedItem = _currentSubjectType;
			RefreshSubjectTypeList((int)cmbOrganizationType.SelectedValue);
		}

		private void btnCancelSubjectTypeEdit_Click(object sender, RoutedEventArgs e)
		{
			SynchCurrentSubjectTypeWithList();
		}

		private void SynchCurrentSubjectTypeWithList()
		{

			if (lstSubjectType.SelectedIndex > -1)
			{
				_currentSubjectType = lstSubjectType.SelectedItem as SubjectType;
				txtSubjectTypeDesc.Text = _currentSubjectType.SubjectTypeDesc;
			}
		}

///
		#endregion
///


///
		#region ORG STRUCTURE TREEVIEW
///

		/// <summary>
		/// Populates the treeview from the underlying org structure data
		/// </summary>
		internal void SetTreeViewFromOrgStructure()
		{
			Flow.Schema.LinqModel.DataContext.ProjectTemplate t = this.CurrentProjectTemplate;

			if (this.CurrentProjectTemplate.OrganizationalStructure == null)
				trvOrganizationStructure.Items.Clear();
			else
			{
				trvOrganizationStructure.Items.Add(
					NewTreeViewItem(this.CurrentProjectTemplate.OrganizationalStructure)
				);
			}
		}

		/// <summary>
		/// Creates a treeview item for each OrganizationalType assigned to the org structure;
		/// and recursively adds children as well
		/// </summary>
		/// <param name="dataItem"></param>
		private TreeViewItem NewTreeViewItem(ProjectTemplateOrganizationalUnitType dataItem)
		{
			TreeViewItem currentItem = new TreeViewItem();
			currentItem.Header = dataItem.ProjectTemplateOrganizationalUnitTypeDesc;
			currentItem.Tag = dataItem;
			currentItem.IsExpanded = true;

			// Add any subject types assigned to the current org type
			foreach (ProjectTemplateSubjectType childDataItem in dataItem.ProjectTemplateSubjectTypes)
			{
				TreeViewItem childItem = new TreeViewItem();
				childItem.Header = childDataItem.ProjectTemplateSubjectTypeDesc;
				childItem.Tag = childDataItem;
				childItem.IsExpanded = true;

				currentItem.Items.Add(childItem);
			}

			// Add any child org types assigned to the current org type
			foreach (ProjectTemplateOrganizationalUnitType childDataItem in dataItem.ProjectTemplateOrganizationalUnitTypes)
			{
				currentItem.Items.Add(NewTreeViewItem(childDataItem));
			}

			return currentItem;
		}


		public void SaveOrgStructureFromTreeView()
		{
			if(!trvOrganizationStructure.HasItems)
				return;

			TreeViewItem rootItem = (trvOrganizationStructure.Items[0] as TreeViewItem);

			//NOTE: Should consider revising ConvertFrom implementation
			ProjectTemplateOrganizationalUnitType rootDataItem = OrganizationalUnitType.ConvertFrom(rootItem.Tag);

			rootDataItem.ProjectTemplateID = this.CurrentProjectTemplate.ProjectTemplateID;

			this.CurrentProjectTemplate.ProjectTemplateOrganizationalUnitTypes.Add(rootDataItem);
			this.FlowMasterDataContext.SubmitChanges();
			
			AddOrgStructureItems(rootItem, rootDataItem);
		}


		private void AddOrgStructureItems(TreeViewItem parentItem, ProjectTemplateOrganizationalUnitType parentDataItem)
		{
			foreach (object item in parentItem.Items)
			{
				TreeViewItem childItem = item as TreeViewItem;

				ProjectTemplateSubjectType childSubjectType = SubjectType.ConvertFrom(childItem.Tag);

				if (childSubjectType != null)
				{
					childSubjectType.ProjectTemplateID = parentDataItem.ProjectTemplateID;
		
					parentDataItem.ProjectTemplateSubjectTypes.Add(childSubjectType);
				}
				else
				{
					ProjectTemplateOrganizationalUnitType childOrganizationalUnitType = OrganizationalUnitType.ConvertFrom(childItem.Tag);

					childOrganizationalUnitType.ProjectTemplateID = parentDataItem.ProjectTemplateID;
					childOrganizationalUnitType.ProjectTemplateOrganizationalUnitTypeParentID = parentDataItem.ProjectTemplateOrganizationalUnitTypeID;

					this.CurrentProjectTemplate.ProjectTemplateOrganizationalUnitTypes.Add(childOrganizationalUnitType);
					this.FlowMasterDataContext.SubmitChanges();

					if(childItem.HasItems)
						AddOrgStructureItems(childItem, childOrganizationalUnitType);				
				}
			}
		}


		private void trvOrganizationStructure_Drop(object sender, DragEventArgs e)
		{
			OrganizationalUnitType orgUnitType = null;
			SubjectType subjectType = null;

			orgUnitType = e.Data.GetData(DataFormats.UnicodeText, true) as OrganizationalUnitType;
			if (orgUnitType == null)
			{
				subjectType = e.Data.GetData(DataFormats.UnicodeText, true) as SubjectType;

				if (subjectType == null)
					return;
			}
			
			FrameworkElement target = (e.Source as FrameworkElement);

			Type targetType = target.GetType();

			if(targetType == typeof(TreeView) && orgUnitType != null)
			{
				if (!trvOrganizationStructure.HasItems)
				{
					TreeViewItem rootItem = new TreeViewItem();
					rootItem.Tag = orgUnitType;
					rootItem.Header = orgUnitType.OrganizationalUnitTypeDesc;
					rootItem.IsExpanded = true;

					trvOrganizationStructure.Items.Add(rootItem);
				}
			}
			else
			{
				TreeViewItem parentItem = target as TreeViewItem;

				if(parentItem != null)
				{

					if (parentItem.Tag.GetType() == typeof(SubjectType))
						return;

					TreeViewItem newItem = new TreeViewItem();

					if (orgUnitType != null)
					{
						newItem.Tag = orgUnitType;
						newItem.Header = orgUnitType.OrganizationalUnitTypeDesc;
					}
					else
					{
						newItem.Tag = subjectType;
						newItem.Header = subjectType.SubjectTypeDesc;
					}

					newItem.IsExpanded = true;
					parentItem.Items.Add(newItem);
				}
				
			
			}
		}


///
		#endregion
///

	}	// END class

}	// END namespace
