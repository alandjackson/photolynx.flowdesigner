﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Schema.LinqModel.DataContext;
using Flow.View.Project.ProjectTemplate.Base;

namespace Flow.View.Project.ProjectTemplate
{
	/// <summary>
	/// Interaction logic for GroupingSection.xaml
	/// </summary>
	public partial class SubjectPanel : ProjectTemplatePanelBase
	{
		private SubjectField _currentSubjectField = null;

		public SubjectPanel()
		{
			InitializeComponent();
		}

		private void lstCommonFields_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			SynchCurrentSubjectFieldWithList();
		}

		private void btnAddSubjectField_Click(object sender, RoutedEventArgs e)
		{
			_currentSubjectField = new SubjectField();
			txtFieldName.Text = "";
			cmbFieldType.SelectedIndex = 0;
			chkIsRequired.IsChecked = false;
			chkIsScanKey.IsChecked = false;
			chkIsProminentField.IsChecked = true;
			chkIsSearchable.IsChecked = true;
		}

		private void btnSaveSubjectField_Click(object sender, RoutedEventArgs e)
		{
			_currentSubjectField.SubjectFieldDisplayName = txtFieldName.Text;
			_currentSubjectField.FieldDataTypeID = (int)cmbFieldType.SelectedValue;
            if (_currentSubjectField.FieldDataTypeID == 1)
                 _currentSubjectField.FieldDataSize = 256;
            if (_currentSubjectField.FieldDataTypeID == 2)
                 _currentSubjectField.FieldDataSize = 4;

			_currentSubjectField.IsRequiredField = chkIsRequired.IsChecked.Value;

			if (!this.FlowMasterDataContext.SubjectFieldList.ContainsDesc(_currentSubjectField))
			{
				if (_currentSubjectField.SubjectFieldID == 0)
					this.FlowMasterDataContext.SubjectFieldList.Add(_currentSubjectField);
			}

			lstCommonFields.SelectedItem = _currentSubjectField;
			lstSubjectFields.ScrollIntoView(lstSubjectFields.SelectedItem);
		}

		private void btnCancelSubjectFieldEdit_Click(object sender, RoutedEventArgs e)
		{
			SynchCurrentSubjectFieldWithList();
		}

		//private void SetSubjectFieldListDataSource()
		//{
		//    lstCommonFields.ItemsSource = this.FlowMasterDataContext.SubjectFieldList.Where(f => !f.IsSystemField);
		//}

		private void SynchCurrentSubjectFieldWithList()
		{
			if (lstCommonFields.SelectedIndex > -1)
			{
				_currentSubjectField = lstCommonFields.SelectedItem as SubjectField;
				txtFieldName.Text = _currentSubjectField.SubjectFieldDisplayName;
				cmbFieldType.SelectedValue = _currentSubjectField.FieldDataTypeID;
				chkIsRequired.IsChecked = _currentSubjectField.IsRequiredField;
			}
		}

		private void lstSubjectFields_Drop(object sender, DragEventArgs e)
		{
			if (e.Data.GetDataPresent(DataFormats.UnicodeText, true))
			{
				SubjectField selectedSubjectField = e.Data.GetData(DataFormats.UnicodeText, true) as SubjectField;

				if (selectedSubjectField != null)
				{
					ProjectTemplateSubjectField newSubjectField =
						new ProjectTemplateSubjectField(this.CurrentProjectTemplate.ProjectTemplateID, selectedSubjectField);

					if(!this.CurrentProjectTemplate.ProjectTemplateSubjectFieldList.ContainsDesc(newSubjectField))
						this.CurrentProjectTemplate.ProjectTemplateSubjectFieldList.Add(newSubjectField);

					e.Effects = ((e.KeyStates & DragDropKeyStates.ControlKey) != 0)
						? DragDropEffects.Copy : DragDropEffects.Move;
				}

				e.Handled = true;
			}
		}

		private void lstCommonFields_MouseDoubleClick(object sender, MouseButtonEventArgs e)
		{
		}

		private void lstSubjectFields_MouseDoubleClick(object sender, MouseButtonEventArgs e)
		{
		}
	}
}
