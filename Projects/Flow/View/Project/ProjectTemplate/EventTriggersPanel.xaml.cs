﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

// using Flow.Controls.View.ProjectTemplateDisplay.EventTriggerControls;
using Flow.Schema.LinqModel.DataContext;
using Flow.View.Project.ProjectTemplate.Base;

namespace Flow.View.Project.ProjectTemplate
{
	/// <summary>
	/// Interaction logic for EventTriggers.xaml
	/// </summary>
	public partial class EventTriggersPanel : ProjectTemplatePanelBase
	{
		public EventTriggersPanel()
		{
			InitializeComponent();
		}

		private void EventTriggerTypeGroupBox_Loaded(object sender, RoutedEventArgs e)
		{
			//EventTriggerTypeGroupBox control = sender as EventTriggerTypeGroupBox;

			//if (control != null)
			//{
			//    control.SetEventTriggers(this.FlowMasterDataContext);
			//}
		}

		private void ProjectTemplatePanelBase_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
			// Update the event trigger checkboxes to reflect the ProjectTemplateEventTrigger active values
			// the checkboxes are bound to items in the EventTrigger table
			//foreach (ProjectTemplateEventTrigger item in this.CurrentProjectTemplate.ProjectTemplateEventTriggers)
			//{
			//    this.FlowMasterDataContext.EventTriggers
			//        .First(et => et.EventTriggerID == item.EventTriggerID)
			//        .Active = item.Active;
			//}
		}
	
	}
}
