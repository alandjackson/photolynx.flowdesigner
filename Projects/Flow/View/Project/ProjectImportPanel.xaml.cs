using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Controls;
using Flow.Controller.Project;
using Flow.Lib;
using Flow.Lib.Helpers;
using Flow.Schema.LinqModel.DataContext;
using Flow.View.Project.Base;
using Flow.View.Preferences;

using StructureMap;
using System.Drawing;
using Flow.Designer.Lib.ImageAdjustments.Model;
using Flow.Designer.Lib.Service;
using System.Windows.Threading;
using Flow.View.Dialogs;
using ICSharpCode.SharpZipLib.Core;
using Flow.Lib.Net;
using Flow.Schema.Reports;
using Flow.Controller.Reports;
using Rebex.Net;
using Flow.Controls.View.Dialogs;
using System.Threading;
using Flow.Lib.FlowOrder;
using Flow.Lib.Activation;
using System.Net;
using NetServ.Net.Json;
using NLog;
using System.Drawing.Imaging;
using System.Xml;


namespace Flow.View.Project
{
	/// <summary>
	/// Interaction logic for ProjectImportPanel.xaml
	/// </summary>
	public partial class ProjectImportPanel : ProjectPanelBase
	{
		private enum ExportMethod
		{
			ExportToPaf,
			ExportForImageMatch,
			SaveOrder,
			UploadProject,
            UploadIMProject,
			SubmitOrder,
            ExportDataOnly,
            ExportDataOnlyJumpSeat,
            ExportImagesAlpha,
            ExportImagesShotOrder
		}

/// /////////////////////////////////////////////////////////////////////////
		#region DEPENDENCY PROPERTIES
/// /////////////////////////////////////////////////////////////////////////

		protected static DependencyProperty SelectedProjectFilePathProperty =
			DependencyProperty.Register("SelectedProjectFilePath", typeof(string), typeof(ProjectImportPanel));

		public string SelectedProjectFilePath
		{
			get { return (string)this.GetValue(SelectedProjectFilePathProperty); }
			set
			{
				this.SetValue(SelectedProjectFilePathProperty, value);
				this.ImportButtonEnabled = !string.IsNullOrEmpty(this.SelectedProjectFilePath);
			}
		}

        protected static DependencyProperty SelectedProjectFolderPathProperty =
            DependencyProperty.Register("SelectedProjectFolderPath", typeof(string), typeof(ProjectImportPanel));

        public string SelectedProjectFolderPath
        {
            get { return (string)this.GetValue(SelectedProjectFolderPathProperty); }
            set
            {
                this.SetValue(SelectedProjectFolderPathProperty, value);
                this.ImportButtonEnabled = !string.IsNullOrEmpty(this.SelectedProjectFolderPath);
            }
        }

		protected static DependencyProperty ImportButtonEnabledProperty =
			DependencyProperty.Register("ImportButtonEnabled", typeof(bool), typeof(ProjectImportPanel));

		public bool ImportButtonEnabled
		{
			get { return (bool)this.GetValue(ImportButtonEnabledProperty); }
			set { this.SetValue(ImportButtonEnabledProperty, value); }
		}


		protected static DependencyProperty ImportButtonVisibilityProperty =
			DependencyProperty.Register("ImportButtonVisibility", typeof(Visibility), typeof(ProjectImportPanel));

		public Visibility ImportButtonVisibility
		{
			get { return (Visibility)this.GetValue(ImportButtonVisibilityProperty); }
			set { this.SetValue(ImportButtonVisibilityProperty, value); }
		}


		protected static DependencyProperty MergeControlsVisibilityProperty =
			DependencyProperty.Register("MergeControlsVisibility", typeof(Visibility), typeof(ProjectImportPanel), new PropertyMetadata(Visibility.Collapsed));

		public Visibility MergeControlsVisibility
		{
			get { return (Visibility)this.GetValue(MergeControlsVisibilityProperty); }
			set { this.SetValue(MergeControlsVisibilityProperty, value); }
		}


		protected static DependencyProperty ImportAfterOpenProperty =
			DependencyProperty.Register("ImportAfterOpen", typeof(bool), typeof(ProjectImportPanel), new PropertyMetadata(false));

		public bool ImportAfterOpen
		{
			get { return (bool)this.GetValue(ImportAfterOpenProperty); }
			set { this.SetValue(ImportAfterOpenProperty, value); }
		}

		protected static DependencyProperty SelectedImageMatchExportPathProperty =
			DependencyProperty.Register("SelectedImageMatchExportPath", typeof(string), typeof(ProjectImportPanel));

		public string SelectedImageMatchExportPath
		{
			get { return (string)this.GetValue(SelectedImageMatchExportPathProperty); }
			set	{ this.SetValue(SelectedImageMatchExportPathProperty, value); }
		}


		protected static DependencyProperty SelectedFlowProjectProperty = DependencyProperty.Register(
			"SelectedFlowProject", typeof(FlowProject), typeof(ProjectImportPanel),
			new FrameworkPropertyMetadata(new PropertyChangedCallback(SelectedFlowProjectPropertyChanged))
		);

		public FlowProject SelectedFlowProject
		{
			get { return (FlowProject)this.GetValue(SelectedFlowProjectProperty); }
			set { this.SetValue(SelectedFlowProjectProperty, value); }
		}

		public static void SelectedFlowProjectPropertyChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
		{
			ProjectImportPanel source = o as ProjectImportPanel;
            
			if (source != null)
			{
				source.FlowProjectSelected = (e.NewValue != null);
                //source.ExportButtonEnabled = true;

                
                    
			}

            
		}

		protected static DependencyProperty FlowProjectSelectedProperty =
			DependencyProperty.Register("FlowProjectSelected", typeof(bool), typeof(ProjectImportPanel));

		public bool FlowProjectSelected
		{
			get { return (bool)this.GetValue(FlowProjectSelectedProperty); }
			set { this.SetValue(FlowProjectSelectedProperty, value); }
		}

		protected static DependencyProperty SelectedExportPathProperty =
			DependencyProperty.Register("SelectedExportPath", typeof(string), typeof(ProjectImportPanel));

		public string SelectedExportPath
		{
			get { return (string)this.GetValue(SelectedExportPathProperty); }
			set
			{
				this.SetValue(SelectedExportPathProperty, value);
				this.ExportButtonEnabled = !string.IsNullOrEmpty(this.SelectedExportPath);
			}
		}

        protected static DependencyProperty SelectedExportPathDataFileProperty =
            DependencyProperty.Register("SelectedExportPathDataFile", typeof(string), typeof(ProjectImportPanel));

        public string SelectedExportPathDataFile
        {
            get { return (string)this.GetValue(SelectedExportPathDataFileProperty); }
            set
            {
                this.SetValue(SelectedExportPathDataFileProperty, value);
                this.ExportButtonEnabled = !string.IsNullOrEmpty(this.SelectedExportPathDataFile);
            }
        }


        protected static DependencyProperty SelectedExportPathImagesProperty =
            DependencyProperty.Register("SelectedExportPathImages", typeof(string), typeof(ProjectImportPanel));

        public string SelectedExportPathImages
        {
            get { return (string)this.GetValue(SelectedExportPathImagesProperty); }
            set
            {
                this.SetValue(SelectedExportPathImagesProperty, value);
                this.ExportButtonEnabled = !string.IsNullOrEmpty(this.SelectedExportPathImages);
            }
        }


		protected static DependencyProperty SaveOrderExportPathProperty =
			DependencyProperty.Register("SaveOrderExportPath", typeof(string), typeof(ProjectImportPanel));

		public string SaveOrderExportPath
		{
			get { return (string)this.GetValue(SaveOrderExportPathProperty); }
			set { this.SetValue(SaveOrderExportPathProperty, value); }
		}

		protected static DependencyProperty ExportButtonEnabledProperty =
			DependencyProperty.Register("ExportButtonEnabled", typeof(bool), typeof(ProjectImportPanel));

		public bool ExportButtonEnabled
		{
			get { return (bool)this.GetValue(ExportButtonEnabledProperty); }
			set { this.SetValue(ExportButtonEnabledProperty, value); }
		}

/// /////////////////////////////////////////////////////////////////////////
#endregion DEPENDENCY PROPERTIES
/// /////////////////////////////////////////////////////////////////////////


        public FlowProject DefaultProject { get; set; }

        public string resizeValue { get; set; }

		private FlowProject RootProject { get; set; }

		private ProjectImageType SelectedImageType { get; set; }

        private String SelectedImageGroupType { get; set; }

		private ExportMethod SelectedExportMethod { get; set; }

        private Logger logger = LogManager.GetCurrentClassLogger();

		public ProjectImportPanel()
		{
			InitializeComponent();

            




            
		}


		protected override void OnSetFlowController()
		{
			base.OnSetFlowController();

//			cmbFlowProjects.ItemsSource = this.FlowMasterDataContext.StudioProjectList;
            this.txtUploadDestination.Text = ProjectController.FlowMasterDataContext.PreferenceManager.Upload.ProjectTransfer.HostAddress;
            if (ProjectController.FlowMasterDataContext.PreferenceManager.Upload.ProjectTransfer.RemoteImageMatchDirectory != null)
            {
                this.txtUploadIMDestination.Text = System.IO.Path.Combine(ProjectController.FlowMasterDataContext.PreferenceManager.Upload.ProjectTransfer.HostAddress,
                                                                ProjectController.FlowMasterDataContext.PreferenceManager.Upload.ProjectTransfer.RemoteImageMatchDirectory);
            }
            else
            {
                this.txtUploadIMDestination.Text = ProjectController.FlowMasterDataContext.PreferenceManager.Upload.ProjectTransfer.HostAddress;
            }
                this.DataContext = this.ProjectController;

                if (this.CurrentFlowProject != null)
                {
                    if ((bool)this.CurrentFlowProject.IsGreenScreen)
                        chkRenderGreenScreen.Visibility = Visibility.Visible;
                    else
                        chkRenderGreenScreen.Visibility = Visibility.Collapsed;
                }

                if (this.ProjectController.FlowMasterDataContext.PreferenceManager.Application.ExportDefaultProjectExport)
                    this.ExportTabControl.SelectedIndex = 0;
                else if (this.ProjectController.FlowMasterDataContext.PreferenceManager.Application.ExportDefaultImageMatchExport)
                    this.ExportTabControl.SelectedIndex = 1;
                else if (this.ProjectController.FlowMasterDataContext.PreferenceManager.Application.ExportDefaultDataExport)
                    this.ExportTabControl.SelectedIndex = 2;

                if (this.ExportTabControl.SelectedIndex == 0)
                {
                    //full project
                    rdoExportFile.IsChecked = false;
                    rdoExportServer.IsChecked = false;
                    rdoImageMatchExport.IsChecked = false;
                    rdoExportIMServer.IsChecked = false;
                    rdoExportDataFile.IsChecked = false;
                    rdoExportDataFileJumpSeat.IsChecked = false;

                    if (this.ProjectController.FlowMasterDataContext.PreferenceManager.Application.ExportSaveLocally)
                        rdoExportFile.IsChecked = true;
                    if (this.ProjectController.FlowMasterDataContext.PreferenceManager.Application.ExportUploadToLab)
                        rdoExportServer.IsChecked = true;

                    chkFullProjectIncludeImages.IsChecked = this.ProjectController.FlowMasterDataContext.PreferenceManager.Application.ExportIncludeImages;
                    chkFullProjectUncompressed.IsChecked = this.ProjectController.FlowMasterDataContext.PreferenceManager.Application.ExportUncompressed;
                }
                else if (this.ExportTabControl.SelectedIndex == 1)
                {
                    //ImageMatch
                    rdoExportFile.IsChecked = false;
                    rdoExportServer.IsChecked = false;
                    rdoImageMatchExport.IsChecked = false;
                    rdoExportIMServer.IsChecked = false;
                    rdoExportDataFile.IsChecked = false;
                    rdoExportDataFileJumpSeat.IsChecked = false;

                    if (this.ProjectController.FlowMasterDataContext.PreferenceManager.Application.ExportSaveLocally)
                        rdoImageMatchExport.IsChecked = true;
                    if (this.ProjectController.FlowMasterDataContext.PreferenceManager.Application.ExportUploadToLab)
                        rdoExportIMServer.IsChecked = true;



                }
                else if (this.ExportTabControl.SelectedIndex == 2)
                {
                    //Data Only
                    rdoExportFile.IsChecked = false;
                    rdoExportServer.IsChecked = false;
                    rdoImageMatchExport.IsChecked = false;
                    rdoExportIMServer.IsChecked = false;
                    rdoExportDataFile.IsChecked = false;
                    rdoExportDataFileJumpSeat.IsChecked = false;
                }

                if (this.ProjectController.FlowMasterDataContext.PreferenceManager.Application.ExportApplyCrops)
                    WithCroppingCheckBox.IsChecked = true;
                else
                    WithCroppingCheckBox.IsChecked = false;
                if (this.ProjectController.FlowMasterDataContext.PreferenceManager.Application.ExportRenderGreenScreen)
                    chkRenderGreenScreen.IsChecked = true;
                else
                    chkRenderGreenScreen.IsChecked = false;
                if (this.ProjectController.FlowMasterDataContext.PreferenceManager.Application.ExportUseOriginalFileName)
                    chkOriginalFileNames.IsChecked = true;
                else
                    chkOriginalFileNames.IsChecked = false;
                if (this.ProjectController.FlowMasterDataContext.PreferenceManager.Application.ExportGroupPhoto)
                    chkGroupPhoto.IsChecked = true;
                else
                    chkGroupPhoto.IsChecked = false;
                if (this.ProjectController.FlowMasterDataContext.PreferenceManager.Application.ExportPose)
                    chkPose.IsChecked = true;
                else
                    chkPose.IsChecked = false;
                if (this.ProjectController.FlowMasterDataContext.PreferenceManager.Application.ExportAllImages)
                    rdoAllImages.IsChecked = true;
                else
                    rdoAllImages.IsChecked = false;
                if (this.ProjectController.FlowMasterDataContext.PreferenceManager.Application.ExportOnlyPrimaryImage)
                    rdoPrimaryImage.IsChecked = true;
                else
                    rdoPrimaryImage.IsChecked = false;


                this.ProjectController.AllLayouts = IOUtil.DirSearch(FlowContext.FlowLayoutsDirPath).Where(s=>s.EndsWith(".lyt")).ToList();
                this.ProjectController.AllLayouts.Sort();
                this.ProjectController.SelectedLayouts = new List<string>();
		}

/// /////////////////////////////////////////////////////////////////////////
		#region IMPORT SECTION
/// /////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Imports the project and opens the project in Capture if requested
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnImport_Click(object sender, RoutedEventArgs e)
		{
            if (this.rdoImportProjectFolder.IsChecked == true)
                this.ProjectController.AttachProjectUncompressed(this.SelectedProjectFolderPath, this.ImportAfterOpen, this.FlowController);
            else
                this.ProjectController.AttachProject(this.SelectedProjectFilePath, this.ImportAfterOpen, this.FlowController);

            this.SelectedProjectFilePath = null;

            
		}

		private void btnMerge_Click(object sender, RoutedEventArgs e)
		{
            if (this.rdoImportProjectFolder.IsChecked == true)
                this.ProjectController.MergeProjectUncompressed(this.SelectedProjectFolderPath, this.RootProject, this.ImportAfterOpen, this.FlowController);
            else
                this.ProjectController.MergeProject(this.SelectedProjectFilePath, this.RootProject, this.ImportAfterOpen, this.FlowController);

			this.MergeControlsVisibility = Visibility.Collapsed;
			this.ImportButtonVisibility = Visibility.Visible;
            this.SelectedProjectFilePath = null;
		}

        private void SelectedImportFormat_Changed(object sender, RoutedEventArgs e)
        {
        }

        private void btnSelectProjectFolder_Click(object sender, RoutedEventArgs e)
        {
            string defaultDir = this.FlowMasterDataContext.PreferenceManager.Application.ProjectArchiveDirectory;

            this.SelectedProjectFolderPath = DialogUtility.SelectFolder(defaultDir, "Select Source Folder");

            if (String.IsNullOrEmpty(this.SelectedProjectFolderPath))
                return;

            try
            {
                this.RootProject = this.ProjectController.FlowMasterDataContext.GetExistingProjectUncompressed(this.SelectedProjectFolderPath);
            }
            catch (Exception exception)
            {
                FlowMessageBox msg = new FlowMessageBox("Error Opening Project", "There is an error with the project folder you slected, please choose again");
                msg.CancelButtonVisible = false;
                msg.ShowDialog();
                this.SelectedProjectFilePath = "";

                logger.ErrorException("Exception occured while trying to import project: ", exception);
            }

            if (this.RootProject == null)	// merge
            {
                this.ImportButtonVisibility = Visibility.Visible;
                this.MergeControlsVisibility = Visibility.Collapsed;
            }
            else							// attach
            {
                this.ImportButtonVisibility = Visibility.Collapsed;
                this.MergeControlsVisibility = Visibility.Visible;
            }
        }

		private void btnSelectProjectFile_Click(object sender, RoutedEventArgs e)
		{
            string defaultDir = this.SelectedProjectFilePath;
            //if (defaultDir == null || defaultDir.Length == 0)
                defaultDir = this.FlowMasterDataContext.PreferenceManager.Application.ProjectArchiveDirectory;

			this.SelectedProjectFilePath = DialogUtility.SelectFile(
				"Select Source File",
                defaultDir,
				"Project Archive File (*.paf)|*.paf"
			);

			if (String.IsNullOrEmpty(this.SelectedProjectFilePath))
				return;

            try
            {
                this.RootProject = this.ProjectController.FlowMasterDataContext.GetExistingProject(this.SelectedProjectFilePath);

            }
            catch (Exception exception)
            {
                
                FlowMessageBox msg = new FlowMessageBox("Error Opening Project", "There is an error with the projectFile you slected, please choose again");
                msg.CancelButtonVisible = false;
                msg.ShowDialog();
                this.SelectedProjectFilePath = "";

                logger.ErrorException("Exception occured while trying to import project: ", exception);
            }

			if (this.RootProject == null)	// merge
			{
				this.ImportButtonVisibility = Visibility.Visible;
				this.MergeControlsVisibility = Visibility.Collapsed;
			}
			else							// attach
			{
				this.ImportButtonVisibility = Visibility.Collapsed;
				this.MergeControlsVisibility = Visibility.Visible;
			}
		}

/// /////////////////////////////////////////////////////////////////////////
		#endregion IMPORT SECTION
/// /////////////////////////////////////////////////////////////////////////


/// /////////////////////////////////////////////////////////////////////////
		#region EXPORT SECTION
/// /////////////////////////////////////////////////////////////////////////

		private void btnSelectExportFilePath_Click(object sender, RoutedEventArgs e)
		{
			this.SelectedExportPath = DialogUtility.SelectFolder(Environment.SpecialFolder.Desktop, "Select destination folder.");
		}

		private void btnSelectImageMatchExportPath_Click(object sender, RoutedEventArgs e)
		{
			string path = DialogUtility.SelectFolder(Environment.SpecialFolder.Desktop, "Select destination folder.");

			bool pathSelected = !String.IsNullOrEmpty(path);

			if (!String.IsNullOrEmpty(path))
			{
				this.SelectedImageMatchExportPath = System.IO.Path.Combine(path, this.SelectedFlowProject.FileSafeProjectName);

				if (this.SelectedFlowProject != this.ProjectController.CurrentFlowProject)
				{
					this.SelectedFlowProject.Connect(this.FlowMasterDataContext);
                    this.SelectedFlowProject.FlowProjectDataContext.InitSubjectList();
                    this.SelectedFlowProject.FlowProjectDataContext.SubjectDataLoaded += delegate
					{
						//this.ProjectController.CurrentFlowProject = this.SelectedFlowProject;
						this.ProjectController.CurrentFlowProject.RefreshPropertyBinding("ProjectImageTypeList");

						this.ImageTypePanel.Visibility = Visibility.Visible;
					};
				}
				else
				{
					this.ProjectController.CurrentFlowProject.RefreshPropertyBinding("ProjectImageTypeList");
					this.ImageTypePanel.Visibility = Visibility.Visible;
				}
			}

			this.ExportButtonEnabled = pathSelected;
		}

		private void btnSelectSaveOrderExportPath_Click(object sender, RoutedEventArgs e)
		{
			this.SaveOrderExportPath = DialogUtility.SelectFolder(Environment.SpecialFolder.Desktop, "Select destination folder.");

			this.ExportButtonEnabled = !String.IsNullOrEmpty(this.SaveOrderExportPath);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void SelectedExportMethod_Changed(object sender, RoutedEventArgs e)
		{
			if (!this.IsInitialized)
				return;
	
			RadioButton source = sender as RadioButton;

			if (source != null)
			{
				this.SetExportMethod();
			}
		}

		private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if (!this.IsInitialized)
				return;

			TabControl exportTabControl = sender as TabControl;

			if (exportTabControl != null)
			{

                this.SetExportMethod();
			}
		}

		/// <summary>
		/// Sets the export type method (selected operation) based on UI selections
		/// </summary>
		private void SetExportMethod()
		{



            if (this.ExportTabControl.SelectedIndex == 0)
            {
                //full project
                if (rdoExportServer.IsChecked.Value)
                {
                    this.SelectedExportMethod = ExportMethod.UploadProject;
                    this.ExportButtonEnabled = true;
                    btnExport.Content = "Upload";
                }
                else if (rdoExportFile.IsChecked.Value)
                {
                    this.SelectedExportMethod = ExportMethod.ExportToPaf;
                    this.ExportButtonEnabled = !String.IsNullOrEmpty(this.SelectedExportPath);
                    btnExport.Content = "Export";
                }
                else
                    this.ExportButtonEnabled = false;
            }
            if (this.ExportTabControl.SelectedIndex == 1)
            {
                if (rdoExportIMServer.IsChecked.Value)
                {
                    this.SelectedExportMethod = ExportMethod.UploadIMProject;
                    this.ExportButtonEnabled = true;
                    btnExport.Content = "Upload";
                }
                else if (rdoImageMatchExport.IsChecked.Value)
                {
                    this.SelectedExportMethod = ExportMethod.ExportForImageMatch;
                    this.ExportButtonEnabled = !String.IsNullOrEmpty(this.SelectedImageMatchExportPath);
                    btnExport.Content = "Export IM";
                }
                else
                    this.ExportButtonEnabled = false;
            }
                 if (this.ExportTabControl.SelectedIndex == 2)
                {
                     if (rdoExportDataFile.IsChecked.Value)
                    {
                        this.SelectedExportMethod = ExportMethod.ExportDataOnly;
                        this.ExportButtonEnabled = !String.IsNullOrEmpty(this.SelectedExportPathDataFile);
                        btnExport.Content = "Export";
                    }
                    else if (rdoExportDataFileJumpSeat.IsChecked.Value)
                    {
                        this.SelectedExportMethod = ExportMethod.ExportDataOnlyJumpSeat;
                        this.ExportButtonEnabled = !String.IsNullOrEmpty(this.SelectedExportPathDataFile);
                        btnExport.Content = "Export";
                    }
                     else
                         this.ExportButtonEnabled = false;
                   

                }

            if (this.SelectedFlowProject == null)
                this.ExportButtonEnabled = false;
		}

		/// <summary>
		/// Performs the selected export operation
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
        /// 
        bool exportInProgress = false;
		private void btnExport_Click(object sender, RoutedEventArgs e)
		{
            if (this.ExportTabControl.SelectedIndex == 0)
            {
                //full project
                if (rdoExportFile.IsChecked.Value)
                    this.SelectedExportMethod = ExportMethod.ExportToPaf;
                else if (rdoExportFile.IsChecked.Value)
                    this.SelectedExportMethod = ExportMethod.UploadProject;
            }
            else if (this.ExportTabControl.SelectedIndex == 1)
            {
                //ImageMatch
                if (rdoImageMatchExport.IsChecked.Value)
                    this.SelectedExportMethod = ExportMethod.ExportForImageMatch;
                else if (rdoExportIMServer.IsChecked.Value)
                    this.SelectedExportMethod = ExportMethod.UploadIMProject;
            }
            else if (this.ExportTabControl.SelectedIndex == 2)
            {
                //Data Only
                if (rdoExportDataFile.IsChecked.Value)
                    this.SelectedExportMethod = ExportMethod.ExportDataOnly;
                else if (rdoExportDataFileJumpSeat.IsChecked.Value)
                    this.SelectedExportMethod = ExportMethod.ExportDataOnlyJumpSeat;
            }
            else if (this.ExportTabControl.SelectedIndex == 3)
            {
                //Images (with data)
                //if (chkShotOrder.IsChecked.Value)
                //    this.SelectedExportMethod = ExportMethod.ExportImagesShotOrder;
                //else
                    this.SelectedExportMethod = ExportMethod.ExportImagesAlpha;
            }
            if (this.ExportTabControl.SelectedIndex == 4)
            {
                //full project BasicID
               this.SelectedExportMethod = ExportMethod.ExportToPaf;
               resizeValue = txtReducedImageSize.Text;
            }
            if (exportInProgress)
                return;

            this.FlowController.ApplicationPanel.IsEnabled = false;
            

           
            exportInProgress = true;
			FlowProject flowProject = this.SelectedFlowProject;

			switch (this.SelectedExportMethod)
			{
				case ExportMethod.UploadProject:
				case ExportMethod.ExportToPaf:
                    if (rdoExportServer.IsChecked == true)
                    {
                        bool canContinue = false;
                        while (canContinue == false)
                        {
                            LabOrderFormDialog dlg = new LabOrderFormDialog(this.CurrentFlowProject.FlowProjectDataContext);

                            if (dlg.ShowDialog() == true)
                            {
                                bool foundMissingValue = false;
                                foreach (OrderFormFieldLocal field in this.CurrentFlowProject.FlowProjectDataContext.OrderFormFieldLocalList)
                                {
                                    if (field.IsRequired == true && string.IsNullOrEmpty(field.SelectedValue))
                                    {
                                        FlowMessageBox msg = new FlowMessageBox("Complete the Order Form", "Please Complete the required fields in the order form before you upload the project");
                                        msg.CancelButtonVisible = false;
                                        msg.ShowDialog();

                                        foundMissingValue = true;
                                        break;
                                    }
                                }

                                canContinue = !foundMissingValue;
                            }
                            else
                            {
                                this.FlowController.ApplicationPanel.IsEnabled = true;
                                exportInProgress = false;
                                return;
                            }
                        }
                    }
                    bool DoExportVerificationCheck = true;
                    if (DoExportVerificationCheck)
                    {
                        ProjectVerificationDialog dlg = new ProjectVerificationDialog(this.CurrentFlowProject);
                        if (dlg.ShowDialog() != true)
                        {
                            this.FlowController.ApplicationPanel.IsEnabled = true;
                            exportInProgress = false;
                            return;
                        }

                    }
					if (flowProject == this.CurrentFlowProject)
					{
						// Prepare the project for export (create control records)
                        if(this.ProjectController.CustomSettings.PafIncludeJsonData == true)
                            flowProject.GenerateExportData();
						flowProject.PrepareForExport();
						this.ExportAfterPrepare(flowProject);
					}
					else
					{
						// Load the project data, and when loaded...
						flowProject.Connect(this.FlowMasterDataContext);
                        flowProject.FlowProjectDataContext.InitSubjectList();
                        flowProject.FlowProjectDataContext.SubjectDataLoaded += delegate
						{
							// Prepare the project for export (create control records)
							flowProject.PrepareForExport();
							this.ExportAfterPrepare(flowProject);
						};
					}
					break;
                case ExportMethod.UploadIMProject:
                  
                    bool canContinueB = false;
                    while (canContinueB == false)
                    {
                        LabOrderFormDialog dlg = new LabOrderFormDialog(this.CurrentFlowProject.FlowProjectDataContext);

                        if (dlg.ShowDialog() == true)
                        {
                            bool foundMissingValue = false;
                            foreach (OrderFormFieldLocal field in this.CurrentFlowProject.FlowProjectDataContext.OrderFormFieldLocalList)
                            {
                                if (field.IsRequired == true && string.IsNullOrEmpty(field.SelectedValue))
                                {
                                    FlowMessageBox msg = new FlowMessageBox("Complete the Order Form", "Please Complete the required fields in the order form before you submit your order");
                                    msg.CancelButtonVisible = false;
                                    msg.ShowDialog();

                                    foundMissingValue = true;
                                    break;
                                }
                            }

                            canContinueB = !foundMissingValue;
                        }
                        else
                        {
                            this.FlowController.ApplicationPanel.IsEnabled = true;
                            exportInProgress = false;
                            return;
                        }
                    }

                    if (flowProject == this.CurrentFlowProject)
                    {
                        string location = this.ExportForImageMatch(flowProject, true);
                        
                    }
                    else
                    {
                        flowProject.Connect(this.FlowMasterDataContext);
                        flowProject.FlowProjectDataContext.InitSubjectList();
                        flowProject.FlowProjectDataContext.SubjectDataLoaded += delegate
                        {
                            string location = this.ExportForImageMatch(flowProject, true);
                        };
                    }
                    break;
				case ExportMethod.ExportForImageMatch:
					if (flowProject == this.CurrentFlowProject)
						this.ExportForImageMatch(flowProject, false);
					else
					{
						flowProject.Connect(this.FlowMasterDataContext);
                        flowProject.FlowProjectDataContext.InitSubjectList();
                        flowProject.FlowProjectDataContext.SubjectDataLoaded += delegate
						{
							this.ExportForImageMatch(flowProject, false);
						};
					}
					break;

                case ExportMethod.ExportDataOnly:
                    ExportDataOnly();
                    this.FlowController.ApplicationPanel.IsEnabled = true;
                    break;

                case ExportMethod.ExportDataOnlyJumpSeat:
                    ExportDataOnlyJumpSeat();
                    this.FlowController.ApplicationPanel.IsEnabled = true;
                    break;

                case ExportMethod.ExportImagesAlpha:
                    ExportImages(false);
                    this.FlowController.ApplicationPanel.IsEnabled = true;
                    break;

                case ExportMethod.ExportImagesShotOrder:
                    ExportImages(true);
                    this.FlowController.ApplicationPanel.IsEnabled = true;
                    break;

				case ExportMethod.SubmitOrder:
				case ExportMethod.SaveOrder:
					string saveOrderExportPath = (this.SelectedExportMethod == ExportMethod.SaveOrder)
						? this.SaveOrderExportPath
						: null
					;

					bool includeAllSubjects = this.SubmitOrderIncludeAllSubjectsCheckBox.IsChecked.Value;
				
					string sortField1 = (this.SelectedExportMethod == ExportMethod.SaveOrder)
						? this.SaveOrderSortControl.SortField1
						: this.SubmitOrderSortControl.SortField1
					;
				
					string sortField2 = (this.SelectedExportMethod == ExportMethod.SaveOrder)
						? this.SaveOrderSortControl.SortField2
						: this.SubmitOrderSortControl.SortField2
					;

                    string orderFilter = this.cmbOrderFilter.SelectedValue.ToString();

					if (flowProject == this.CurrentFlowProject)
					{
						this.SubmitOrder(
							flowProject,
							includeAllSubjects,
							saveOrderExportPath,
							sortField1,
							sortField2,
                            orderFilter,
                            false
						);
					}
					else
					{
						flowProject.Connect(this.FlowMasterDataContext);
                        flowProject.FlowProjectDataContext.InitSubjectList();
                        flowProject.FlowProjectDataContext.SubjectDataLoaded += delegate
						{
							this.SubmitOrder(
								flowProject,
								includeAllSubjects,
								saveOrderExportPath,
								sortField1,
								sortField2,
                                orderFilter,
                                false
							);
						};
					}
                   
					break;

			}

            exportInProgress = false;
           
		}

        private void ExportImages(bool shotOrder)
        {
            bool doPackageSummary = false;
            logger.Info("Exporting Images...");

            string exportDir = FlowContext.Combine(SelectedExportPathImages, this.CurrentFlowProject.FileSafeProjectName);
            if (!Directory.Exists(exportDir))
                Directory.CreateDirectory(exportDir);
            NotificationProgressInfo progressInfo = new NotificationProgressInfo("Image Export", "Initializing...");
            progressInfo.NotificationProgress = ObjectFactory.GetInstance<IShowsNotificationProgress>();
            progressInfo.Display(true);

            List<SubjectImage> ImageList = this.CurrentFlowProject.FlowProjectDataContext.SubjectImages.ToList();
            if (shotOrder)
                ImageList = ImageList.OrderBy(i => i.DateAssigned).ToList();

            List<string> colHeaders = new List<string>();
            colHeaders.Add("Image Name");
            foreach (SubjectDatum sd in ImageList[0].Subject.SubjectData)
            {
                if(sd.FieldDesc != "PackageSummary")
                    colHeaders.Add(sd.FieldDesc);
            }

            if (doPackageSummary)
            {
                colHeaders.Add("Packages");
            }

            string headerRow = String.Join(",", colHeaders);

            StringBuilder builder = new StringBuilder();
            builder.AppendLine(headerRow);

            int imageNumber = 0;
            foreach (SubjectImage si in ImageList)
            {
                
                if (!File.Exists(si.ImagePathFullRes))
                    continue;

                imageNumber++;

                string newName = si.ImageFileName;
                if(shotOrder)
                    newName = (imageNumber).ToString("00000") + "_" + si.ImageFileName;
                if (!File.Exists(System.IO.Path.Combine(exportDir, newName)))
                    File.Copy(si.ImagePathFullRes, System.IO.Path.Combine(exportDir,newName));

                List<string> rowValues = new List<string>();
                rowValues.Add(newName);
                foreach (SubjectDatum sd in si.Subject.SubjectData)
                {
                    if (sd.FieldDesc != "PackageSummary")
                        rowValues.Add(sd.Value.ToString());
                }


                if (doPackageSummary)
                {
                    string PackageSummary = "";
                    if (si.Subject.SubjectOrder.OrderPackages.Any(op => op.FirstImage.ImageFileName == si.ImageFileName))
                    {

                        foreach (OrderPackage pac in si.Subject.SubjectOrder.OrderPackages.Where(op => op.FirstImage.ImageFileName == si.ImageFileName))
                        {
                            if (pac.ProductPackage == null)
                                PackageSummary = PackageSummary + "??" + "-" + pac.Qty + ";";
                            else
                                PackageSummary = PackageSummary + pac.ProductPackage.Map + "-" + pac.Qty + ";";
                        }

                    }
                    rowValues.Add(PackageSummary);
                }

                string rowString = String.Join(",", rowValues);
                builder.AppendLine(rowString);
            }

            string exportFilePath = FlowContext.Combine(exportDir, this.CurrentFlowProject.FileSafeProjectName + ".csv");

            File.WriteAllText(exportFilePath, builder.ToString(), Encoding.Default);

            logger.Info("Data Export complete.");
            progressInfo.Update("Data Export complete.");
            progressInfo.ShowDoneButton();

        }

        private void ExportDataOnly()
        {
            logger.Info("Exporting Data Only...");

            NotificationProgressInfo progressInfo = new NotificationProgressInfo("Data Export", "Initializing...");
            progressInfo.NotificationProgress = ObjectFactory.GetInstance<IShowsNotificationProgress>();
            progressInfo.Display(true);

            List<Subject> filteredSubjectList = null;

            if ((bool)this.chkExportDataFilteredData.IsChecked)
                filteredSubjectList = this.CurrentFlowProject.GetFilteredSubjectList();

            //IEnumerable<string> imageFileList;
            //List<string> BackgroundFileList;

            List<Subject> FilteredSubjectList = null;
            if (chkIMFilteredData.IsChecked == true)
                FilteredSubjectList = this.CurrentFlowProject.GetFilteredSubjectList();

            DataTable exportDataTable = this.CurrentFlowProject.GetBasicDataExport(filteredSubjectList);
            //DataTable exportDataTable = this.CurrentFlowProject.GetExportData(this.SelectedImageType, true, out imageFileList, out BackgroundFileList, true, FilteredSubjectList);

            DataColumn[] columnList = new DataColumn[exportDataTable.Columns.Count];

            exportDataTable.Columns.CopyTo(columnList, 0);

            StringBuilder builder = new StringBuilder();

            string columnString = String.Join(",", columnList.Select(c => @"""" + c.ColumnName + @"""").ToArray());

            builder.AppendLine(columnString);

            progressInfo.Update("Processing subject data {0} of {1}", 0, exportDataTable.Rows.Count, 1, true);

            foreach (DataRow row in exportDataTable.Rows)
            {
                string[] items = row.ItemArray.Select(i => @"""" + i.ToString() + @"""").ToArray();

                string rowString = String.Join(",", items);

                builder.AppendLine(rowString);

                progressInfo++;
            }

            string exportFilePath = FlowContext.Combine(SelectedExportPathDataFile, this.CurrentFlowProject.FileSafeProjectName + ".csv");

            File.WriteAllText(exportFilePath, builder.ToString(), Encoding.Default);

            logger.Info("Data Export complete.");
            progressInfo.Update("Data Export complete.");
            progressInfo.ShowDoneButton();
        }

        private void ExportDataOnlyJumpSeat()
        {
            logger.Info("Exporting JumpSeat Data...");

            NotificationProgressInfo progressInfo = new NotificationProgressInfo("Data Export for JumpSeat", "Initializing...");
            progressInfo.NotificationProgress = ObjectFactory.GetInstance<IShowsNotificationProgress>();
            progressInfo.Display(true);

            //First Get Account UserName
            if (string.IsNullOrEmpty(this.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixCustomerID) ||
                (string.IsNullOrEmpty(this.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixCustomerPassword)))
            {
                FlowMessageBox msg = new FlowMessageBox("Missing ImageQuix Account Info", "Missing username and password in ImageQuix Account Info.");
                msg.CancelButtonVisible = false;
                msg.ShowDialog();
                return;
            }
            string IQCustomerID = this.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixCustomerID;

            if (this.CurrentFlowProject.Gallery == null)
            {
                this.ProjectController.initOnlineGallerySettings();
                
            }

            //Get EventId (create it if it does not exist)
            string IQEventID = null;
            if (this.CurrentFlowProject.Gallery != null && this.CurrentFlowProject.Gallery.EventID != null)
                IQEventID = this.CurrentFlowProject.Gallery.EventID;
            else
            {
                SubmitOnlineOrderDataNew SubmitOnlineOrderData = new Controller.Project.SubmitOnlineOrderDataNew(this.CurrentFlowProject, this.FlowMasterDataContext, this.CurrentFlowProject.Gallery, this.CurrentFlowProject.SubjectList.ToList());
                    
                string mainUri = this.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixLoginURL;
                string custId = this.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixCustomerID;
                string custPw = this.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixCustomerPassword;
                string installID = this.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixApplicationID;
                int eventCounter = this.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixEventCounter;

                string installCodeUri = mainUri + "/" + custId + "/" + "installcode";

                //check or get installID
                if (installID == null || installID == "")
                {
                   installID = SubmitOnlineOrderData.GetInstallID(installCodeUri, custId, custPw);
                    if (installID == null || installID == "")
                    {

                        FlowMessageBox msg = new FlowMessageBox("Failed to get Install ID", "Failed to get IQ Install ID");
                        msg.CancelButtonVisible = false;
                        msg.ShowDialog();
                        return;
                    }
                    this.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixApplicationID = installID;
                    this.FlowMasterDataContext.SavePreferences();
                }

                if (this.CurrentFlowProject.Gallery.EventID == null)
                {
                    this.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixEventCounter = ++eventCounter;
                    this.FlowMasterDataContext.SavePreferences();
                    string eventCounterS = eventCounter.ToString().PadLeft(5, '0');
                    string eventID = installID.Substring(0, 4) + "-" + installID.Substring(4) + eventCounterS.Substring(0, 1) + "-" + eventCounterS.Substring(1);
                    this.CurrentFlowProject.Gallery.EventID = eventID;
                }

                ////publish a blank gallery
                //string loginUri = mainUri + "/" + custId + "/" + "auth";
                //string postNewEventUri = mainUri + "/" + custId + "/" + "event/publish";

                ////validate login credentials
                //if (SubmitOnlineOrderData.PostToUri(loginUri, custId, custPw) != HttpStatusCode.OK)
                //{
                //    //Error Connection - maybe not authorized?
                //    FlowMessageBox msg = new FlowMessageBox("Failed to Login to ImageQuix Server", "Failed to Login to ImageQuix Server");
                //    msg.CancelButtonVisible = false;
                //    msg.ShowDialog();
                //    return;
                //}

                //try
                //{
                //    JsonObject galleryJson = SubmitOnlineOrderData.GenerateGalleryJson(this.CurrentFlowProject.Gallery.EventID, null, false, true);
                //    JsonWriter writer = new JsonWriter();
                //    galleryJson.Write(writer);
                //    string postData = writer.ToString().Replace("\\/", "/");

                //    WebHeaderCollection returnHeader = new WebHeaderCollection();
                //    SubmitOnlineOrderData.PostPendingEvent(postNewEventUri, custId, custPw, postData, false, out returnHeader);

                //}
                //catch (Exception e)
                //{

                //    FlowMessageBox msg = new FlowMessageBox("Failed to Generate ImageQuix Gallery", "Failed to Generate ImageQuix Gallery for this Project");
                //    msg.CancelButtonVisible = false;
                //    msg.ShowDialog();
                //    return;
                    
                //}
                IQEventID = this.CurrentFlowProject.Gallery.EventID;
            }

            //this.CurrentFlowProject.Gallery.GalleryURL = "https://vando.imagequix.com/g" + this.CurrentFlowProject.Gallery.GalleryCode;

            //Generate Data File
            //CustomerID,Password,EventID

            StringBuilder builder = new StringBuilder();
            builder.AppendLine("CustomerID,Password,EventID");

            List<Subject> filteredSubjectList = ((bool)this.chkExportDataFilteredData.IsChecked) ?
                this.CurrentFlowProject.GetFilteredSubjectList() :
                this.CurrentFlowProject.SubjectList.ToList();

            foreach (Subject subject in filteredSubjectList)
                builder.AppendLine(IQCustomerID + "," + subject.TicketCode + "," + IQEventID);

            string exportFilePath = FlowContext.Combine(SelectedExportPathDataFile, this.CurrentFlowProject.FileSafeProjectName + "_JumpSeatData.csv");

            File.WriteAllText(exportFilePath, builder.ToString(), Encoding.Default);

            logger.Info("Done Creating JumpSeat Data Export.");
            progressInfo.Update("Done Creating JumpSeat Data Export.");
            progressInfo.ShowDoneButton();
        }

        private string ZipImFiles(string location, string projectName, NotificationProgressInfo progressInfo)
        {
            //DirectoryInfo imDir = new DirectoryInfo(location);
            
            //List<ZipFileMapping> IMFiles = new List<ZipFileMapping>();
            string ImZipFile = System.IO.Path.Combine(FlowContext.FlowTempDirPath, "ImageMatch_" + projectName + ".zip");
            if (File.Exists(ImZipFile)) File.Delete(ImZipFile);

            //foreach (FileInfo thisFile in imDir.GetFiles())
            //{
            //    IMFiles.Add(new ZipFileMapping(thisFile.FullName));
            //}
            //if(Directory.Exists(System.IO.Path.Combine(location, "GreenScreenBackgrounds")))
            //{
            //   DirectoryInfo bgDir = new DirectoryInfo(System.IO.Path.Combine(location, "GreenScreenBackgrounds"));
            //   foreach (FileInfo thisFile in bgDir.GetFiles())
            //    {
            //        IMFiles.Add(new ZipFileMapping(thisFile.FullName));
            //    }
            //}

            progressInfo = new NotificationProgressInfo("Creating zip file", "Initializing...");
            progressInfo.NotificationProgress = ObjectFactory.GetInstance<IShowsNotificationProgress>();
            progressInfo.Display(true);

            ZipUtility.CreateArchive(
                ImZipFile,
                location,
                true
            );

            //ZipUtility.CreateArchive(
            //    ImZipFile,
            //    IMFiles,
            //    progressInfo
            //);

            progressInfo.Complete("Zip file created.");
            //this.FlowController.ApplicationPanel.IsEnabled = true;
            return ImZipFile;
        }
		/// <summary>
		/// Exports the 
		/// </summary>
		/// <param name="flowProject"></param>
		private void ExportAfterPrepare(FlowProject flowProject)
		{
			// If export to filesystem is selected
			if (rdoExportFile.IsChecked == true)
			{
                ulong projectSize = IOUtil.GetDirectorySize(flowProject.ImageDirPath);

                //ulong freeBytesThreshold = (ulong)5 * 1024 * 1024 * 1024; // 8 GiB
                if (IOUtil.DriveFreeBytesAvailable(this.SelectedExportPath) < projectSize)
                {
                    string message = "There is insufficient free space on the destination disk. Export cannot continue.";

                    FlowMessageBox msg = new FlowMessageBox("Export Error", message);
                    msg.CancelButtonVisible = false;
                    msg.ShowDialog();

                    logger.Warn(message);

                    this.FlowController.ApplicationPanel.IsEnabled = true;
                    return;
                }

                //check for layouts to be included
                if (this.ProjectController.SelectedLayouts.Count > 0)
                {
                    string projLayoutDir = System.IO.Path.Combine(CurrentFlowProject.DirPath, "Layouts");
                    string projGraphicDir = System.IO.Path.Combine(CurrentFlowProject.DirPath, "Graphics");

                    if(Directory.Exists(projLayoutDir))
                        Directory.Delete(projLayoutDir, true);
                    if (Directory.Exists(projGraphicDir))
                        Directory.Delete(projGraphicDir, true);

                    Directory.CreateDirectory(projLayoutDir);
                    Directory.CreateDirectory(projGraphicDir);


                    foreach (string file in this.ProjectController.SelectedLayouts)
                    {
                        FileInfo fi = new FileInfo(file);

                        //copy lyt file
                        File.Copy(file, System.IO.Path.Combine(projLayoutDir, fi.Name));
                        //open lyt file and look for build a list of graphics
                        XmlDocument xDoc = new XmlDocument();
                        xDoc.Load(file);

                        XmlNodeList nodes = xDoc.GetElementsByTagName("ImageFilename");

                        //go over the graphics and copy them 
                        foreach (XmlNode n in nodes)
                        {
                            string graphic = n.InnerText;
                            if (!string.IsNullOrEmpty(graphic))
                            {
                                FileInfo fi2 = new FileInfo(graphic);
                                string destFile = System.IO.Path.Combine(projGraphicDir, fi2.Name);
                                if(!File.Exists(destFile))
                                    File.Copy(graphic, destFile);
                            }
                        }

                    }

                }
				ProjectExporter exporter = new ProjectExporter(flowProject,
                    this.SelectedExportPath, FlowMasterDataContext);

                exporter.ResizeImages = WithReducedImagesCheckBox.IsChecked.Value;
                exporter.ResizeSize = txtReducedSize.Text;
                if(!string.IsNullOrEmpty(resizeValue))
                {
                    exporter.ResizeImages = true;
                    exporter.ResizeSize = resizeValue;
                }

                if (this.chkFullProjectUncompressed.IsChecked == true) exporter.IsUncompressed = true;

				//if (chkDetachOnImport.IsChecked.Value)
				//{
                if (chkFullProjectIncludeImages.IsChecked == true)
                    exporter.IncludeImages = true;
                else
                    exporter.IncludeImages = false;

					exporter.ExportComplete += new ProjectExporter.ExportCompleteDelegate(exporter_ExportComplete);
                    exporter.ExportComplete += delegate { 
                        this.ProjectController.IsExporting = false;
                        this.FlowController.ApplicationPanel.IsEnabled = true;
                    };
				//}
				exporter.BeginAsyncExportProject();

			}

			// If upload to remote server is selected
			else if (rdoExportServer.IsChecked == true)
				this.ProjectController.UploadProject(flowProject, chkDetachOnImport.IsChecked.Value, WithReducedImagesCheckBox.IsChecked.Value, txtReducedSize.Text);

            //this.FlowController.ApplicationPanel.IsEnabled = true;
		}

        void exporter_ExportComplete(FlowProject project, string msg, bool showMessage, bool abortDelete)
        {
            if (chkDetachOnImport.IsChecked.Value)
            {
                FlowMasterDataContext.DetachProject(project);
                try
                {
                    Directory.Delete(project.DirPath, true);
                }
                catch (Exception e)
                {

                    //if we cant delete, oh well
                }
            }
            this.Dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
            {
                this.FlowController.Project();
            }));
            //FlowMasterDataContext.DetachProject(project);
        }

        
		private string ExportForImageMatch(FlowProject flowProject, bool forUpload)
		{
            FlowContext.ClearTempDirectory();
            bool useDefaultBG = chkDefaultBG.IsChecked == true ? true : false;
            bool renderGreenScreen = false;
            bool useOriginalFileName = false;
            bool shotOrder = this.chkIMShotOrder.IsChecked == true ? true : false;
            bool doCropData = this.chkIncludeCropData.IsChecked == true ? true : false;
            bool useHighJpgQuality = this.cmbJpgQuality.Text.StartsWith("High") ? true : false;
            bool primaryOnly = rdoPrimaryImage.IsChecked == true;
            var jpgQuality = 70L;
            if (useHighJpgQuality)
                jpgQuality = 100L;

            string imgNamePrefix = this.txtImageNamePrefix.Text;

            if (chkRenderGreenScreen.IsChecked == true)
                renderGreenScreen = true;
            if (chkOriginalFileNames.IsChecked == true)
                useOriginalFileName = true;

			NotificationProgressInfo progressInfo = new NotificationProgressInfo("ImageMatch Export", "Initializing...");
			progressInfo.NotificationProgress = ObjectFactory.GetInstance<IShowsNotificationProgress>();

            string selectedImageMatchExportPath = FlowContext.CombineDir(FlowContext.FlowTempDirPath, "IMTemp_" + TicketGenerator.GenerateTicketString(12));
            if (!Directory.Exists(selectedImageMatchExportPath))
                Directory.CreateDirectory(selectedImageMatchExportPath);

            if(!forUpload)
			    selectedImageMatchExportPath = this.SelectedImageMatchExportPath;
            bool WithCroppingCheckBox = this.WithCroppingCheckBox.IsChecked.Value;

            List<Subject> FilteredSubjectList = null;
            if (chkIMFilteredData.IsChecked == true)
                FilteredSubjectList = this.CurrentFlowProject.GetFilteredSubjectList();


            bool useAllImages = (bool)this.rdoAllImages.IsChecked;
			FlowBackgroundWorkerManager.RunWorker(
				delegate
				{


                   

					progressInfo.Display(true);

                    int waitCount = 0;
                    string dots = ".";
                    while (this.CurrentFlowProject.initingSubjects)
                    {
                        dots += ".";
                        progressInfo.Update("Waiting for subjects to initialize." + dots);
                        waitCount++;
                        Thread.Sleep(3000);

                        if (waitCount == (4 * 60)) //if its taken longer than 4 minutes, bail
                            this.CurrentFlowProject.initingSubjects = false;

                    }

                    progressInfo.Update("Building Export File...");
					IEnumerable<string> imageFileList;
                    List<string> BackgroundFileList;

                    bool groupPhotoAsSeperatePose = false;
                    if (this.SelectedImageGroupType != null && this.SelectedImageGroupType == "Pose")
                        groupPhotoAsSeperatePose = true;

                    

                    DataTable exportDataTable = flowProject.GetExportData(this.SelectedImageType, groupPhotoAsSeperatePose, out imageFileList, out BackgroundFileList, useAllImages, FilteredSubjectList);

                    Dictionary<String, String> FileRenames = new Dictionary<string, string>();

                    if (shotOrder)
                    {
                        // ImageList = ImageList.OrderBy(i => i.DateAssigned).ToList();
                        exportDataTable.DefaultView.Sort = "Date Assigned ASC";
                        
                        
                    }
					DataColumn[] columnList = new DataColumn[exportDataTable.Columns.Count];

					exportDataTable.Columns.CopyTo(columnList, 0);

					StringBuilder builder = new StringBuilder();

					string columnString = String.Join(",", columnList.Select(c => @"""" + c.ColumnName + @"""").ToArray());

					builder.AppendLine(columnString);

					progressInfo.Update("Processing subject data {0} of {1}", 0, exportDataTable.Rows.Count, 1, true);
                    
                    int seqCnt = 0;
                    foreach (DataRowView rowView in exportDataTable.DefaultView)
					{
                        if (renderGreenScreen)
                        {
                            string oldValue = rowView["Image Name"].ToString();
                            if (!string.IsNullOrEmpty(oldValue))
                            {
                                seqCnt++;
                                string newvalue = oldValue.Replace(".png", ".jpg").Replace(".PNG", ".jpg");
                                rowView["Image Name"] = newvalue;
                            }
                        }
                        if (shotOrder)
                        {
                            string oldValue = rowView["Image Name"].ToString();
                            if(!string.IsNullOrEmpty(oldValue))
                            {
                                
                                string newvalue = "";
                                if (FileRenames.ContainsKey(oldValue))
                                    newvalue = FileRenames[oldValue];
                                else
                                {
                                    seqCnt++;
                                    newvalue = imgNamePrefix + seqCnt.ToString("000000") + oldValue;
                                    FileRenames.Add(oldValue, newvalue);
                                }
                                rowView["Image Name"] = newvalue;
                            }
                        }
                        DataRow row = rowView.Row;
						string[] items = row.ItemArray.Select(i => @"""" + i.ToString() + @"""").ToArray();

						string rowString = String.Join(",", items);

						builder.AppendLine(rowString);

						progressInfo++;
					}

					string exportFilePath = FlowContext.Combine(selectedImageMatchExportPath, flowProject.FileSafeProjectName + ".csv");

                    try
                    {
                        File.WriteAllText(exportFilePath, builder.ToString(), Encoding.Default);
                    }
                    catch (IOException ex)
                    {
                        if (ex.Message.Contains("another process"))
                        {
                            this.Dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                            {
                                FlowMessageDialog msg = new FlowMessageDialog("Export Error", "The CSV file at this location already exists and is open in another application. Please close the file first or choose a different export destination.\n\n" + exportFilePath);
                                msg.CancelButtonVisible = false;
                                msg.ShowDialog();
                                this.FlowController.ApplicationPanel.IsEnabled = true;
                            }));
                            progressInfo.Update("ImageMatch Export failed.");
                            progressInfo.ShowDoneButton();
                            return;
                        }
                        else
                            throw;
                    }

                    //create cropdata file
                    if (doCropData)
                    {
                        logger.Info("Creating CropData file");

                        string cropDataFile = FlowContext.Combine(selectedImageMatchExportPath, "CropData.txt"); ;
                        using (System.IO.StreamWriter file = new System.IO.StreamWriter(cropDataFile))
                        {
                            file.WriteLine("ImageName\tImagePath\tCropL\tCropT\tCropW\tCropH\tCropErrors\tRotation");


                            foreach (SubjectImage si in CurrentFlowProject.FlowProjectDataContext.SubjectImages)
                            {
                                if (imageFileList.Contains(si.ImageFileName))
                                    file.WriteLine(si.ImageFileName + "\t" + si.ImageFileName + "\t" + si.CropL + "\t" + si.CropT + "\t" + si.CropW + "\t" + si.CropH);
                            }
                        }

                    }
                    if (BackgroundFileList != null && BackgroundFileList.Count > 0)
                    {
                        //Create GSBackgroundImages Folder
                        string gsDir = (System.IO.Path.Combine(selectedImageMatchExportPath, "GreenScreenBackgrounds"));
                        Directory.CreateDirectory(gsDir);

                        foreach (string bgFile in BackgroundFileList)
                        {
                            string destFile = System.IO.Path.Combine(gsDir, (new FileInfo(bgFile)).Name);
                            if (!File.Exists(destFile))
                                File.Copy(bgFile, destFile);
                        }
                    }

					//IOUtil.CopyFiles(flowProject.ImageDirPath, selectedImageMatchExportPath, imageFileList, progressInfo);
                    if (1 ==1 || WithCroppingCheckBox || renderGreenScreen)
                    {
                        DirectoryInfo dirInfo = new DirectoryInfo(flowProject.ImageDirPath);
                        int fileCount = imageFileList.Count();
                        int loopcnt = 0;
                        foreach (Subject sub in flowProject.SubjectList)
                        {
                            foreach (SubjectImage si in sub.SubjectImages)
                            {
                                if(!imageFileList.Contains(si.ImageFileName))
                                    continue;
                                if (primaryOnly == true && si.IsPrimary != true && si.IsGroupImage != true)
                                    continue;


                                ///Begin New
                                string tempImageMatchExportPath = System.IO.Path.Combine(FlowContext.FlowTempDirPath, this.CurrentFlowProject.FileSafeProjectName);
                                
                                string gsBackground = this.CurrentFlowProject.DefaultGreenScreenBackground;
                                if (!useDefaultBG)
                                {
                                    string orderedbg = si.GetOrderedBackground();
                                    if (!string.IsNullOrEmpty(orderedbg))
                                        gsBackground = orderedbg;
                                }

                                string tmpImagePath = si.RenderTempImage(tempImageMatchExportPath, WithCroppingCheckBox, renderGreenScreen, gsBackground, false, jpgQuality, 300, false);

                                 if (File.Exists(tmpImagePath))
                                {
                                     string destFile = selectedImageMatchExportPath + "/" + (new FileInfo(tmpImagePath)).Name;
                                    string destFileName = (new FileInfo(destFile)).Name;
                                    if (FileRenames.ContainsKey(destFileName))
                                        destFileName = FileRenames[destFileName];
                                    destFile = selectedImageMatchExportPath + "/" + destFileName;

                                    //source.CopyTo(System.IO.Path.Combine(selectedImageMatchExportPath, destFileName), true);
                                    if (File.Exists(destFile))
                                        File.Delete(destFile);

                                    File.Copy(tmpImagePath, destFile);
                                }

                               
                                //End New





                                ///BEGIN NEW CROP/GS
                                //string bground = null;
                                //string newImageName = si.Dp2FileName(useOriginalFileName);
                                //string newImagePath = si.ImagePathFullRes;

                                //if (WithCroppingCheckBox)
                                //{
                                //    logger.Info("About to apply a crop");
                                //    System.Drawing.Image img = System.Drawing.Image.FromFile(newImagePath);
                                //    if (si.Orientation == 90)
                                //        img.RotateFlip(RotateFlipType.Rotate90FlipNone);
                                //    if (si.Orientation == 180)
                                //        img.RotateFlip(RotateFlipType.Rotate180FlipNone);
                                //    if (si.Orientation == 270)
                                //        img.RotateFlip(RotateFlipType.Rotate270FlipNone);

                                //    if (((si.CropW > 0) || (si.CropH > 0)))
                                //        img = cropImage(img, getCropArea(img, si.CropL, si.CropT, si.CropH, si.CropW));
                                //    string tempImageMatchExportPath = System.IO.Path.Combine(FlowContext.FlowTempDirPath, this.CurrentFlowProject.FileSafeProjectName);
                                //    if (!Directory.Exists(tempImageMatchExportPath))
                                //        Directory.CreateDirectory(tempImageMatchExportPath);
                                //    newImagePath = System.IO.Path.Combine(tempImageMatchExportPath, (new FileInfo(newImagePath)).Name);

                                //    if (File.Exists(newImagePath))
                                //        File.Delete(newImagePath);

                                //    if (!File.Exists(newImagePath))
                                //    {
                                //        if(newImagePath.EndsWith(".png"))
                                //            img.Save(newImagePath, System.Drawing.Imaging.ImageFormat.Png);
                                //        else
                                //            img.Save(newImagePath, System.Drawing.Imaging.ImageFormat.Jpeg);
                                //    }

                                //    img.Dispose();
                                //    logger.Info("Done apply a crop");
                                //}


                                //bool isRendered = false;
                                //if (this.CurrentFlowProject.IsGreenScreen == true)
                                //{
                                //    if (!useDefaultBG && si.Subject.SubjectOrder.OrderPackages.Count > 0)
                                //    {
                                //        bground = si.Subject.SubjectOrder.OrderPackages.First().DefaultBackgound.FullPath;
                                //    }
                                //    else
                                //        bground = this.CurrentFlowProject.DefaultGreenScreenBackground;

                                //    //if (op.GreenScreenBackground != null && File.Exists(op.GreenScreenBackground))
                                //    //    bground = op.GreenScreenBackground;
                                //    //else if (this._flowProject.DefaultGreenScreenBackground != null && File.Exists(this._flowProject.DefaultGreenScreenBackground))
                                //    //    bground = this._flowProject.DefaultGreenScreenBackground;
                                //    //else if (this._flowProject.FlowProjectDataContext.FlowMasterDataContext.GreenScreenBackgrounds.Count > 0 && this._flowProject.FlowProjectDataContext.FlowMasterDataContext.GreenScreenBackgrounds[0] != null && File.Exists(this._flowProject.FlowProjectDataContext.FlowMasterDataContext.GreenScreenBackgrounds[0].FullPath))
                                //    //    bground = this._flowProject.FlowProjectDataContext.FlowMasterDataContext.GreenScreenBackgrounds[0].FullPath;

                                //    if (renderGreenScreen)
                                //    {
                                //        logger.Info("About to render greenscreen");
                                //        newImageName = si.GetDp2FileName(bground, useOriginalFileName, renderGreenScreen);
                                //        bool isRenderingGS = true;
                                //        CurrentFlowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                                //        {
                                //            newImagePath = si.GetImagePath(true, bground, newImagePath);
                                //            Thread.Sleep(500);
                                //            isRenderingGS = false;
                                //        }));

                                //        int loopCnt = 0;
                                //        while (isRenderingGS && loopCnt++ < 45)
                                //        {
                                //            Thread.Sleep(1000);
                                //        }
                                //        Thread.Sleep(500);
                                //    }
                                //}

                                ////destFileName = newImageName;
                                //if (!File.Exists(selectedImageMatchExportPath + "/" + (new FileInfo(newImagePath)).Name))
                                //    File.Copy(newImagePath, selectedImageMatchExportPath + "/" + (new FileInfo(newImagePath)).Name);
                                //logger.Info("About to apply a crop");
                                ////SubjectImagePlus sip = new SubjectImagePlus(op.SubjectImage, bground, newImageName, newImagePath, isRendered);
                                ////if (!OrderSubjectImages.Any(b => b.SubjectImage == sip.SubjectImage && b.Background == sip.Background && b.NewImagePath == sip.NewImagePath))
                                ////    OrderSubjectImages.Add(sip);

                                //END NEW CROP/GS
























                                //OLD

                                //if (chkIMFilteredData.IsChecked == true && si.IsSuppressed == true)
                                //    continue;

                                //if (imageFileList.Contains(si.ImageFileName) || imageFileList.Contains(si.ImageFileName.Replace(".jpg", ".png").Replace(".JPG", ".png")))
                                //{
                                //    if (si.CropH < 0 || double.IsNaN(si.CropH))
                                //        si.CropH = 0;
                                //    progressInfo.Update("Copying file {0} of {1}", loopcnt++, fileCount, 1, true);

                                //    System.Drawing.Image img = null;
                                //    if (renderGreenScreen && (si.GreenScreenSettings != null || si.IsPng))
                                //    {
                                //        string gsImage = "";

                                //        this.Dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                                //        {
                                //            gsImage = si.GetRenderedImageFilename(useDefaultBG);
                                //        }));

                                //        if (gsImage != null)
                                //            img = System.Drawing.Image.FromFile(gsImage);
                                //    }
                                //    else
                                //    {
                                //        //check if file exists befer creating the img object from it
                                //        if (File.Exists(si.ImagePathFullRes))
                                //        {
                                //            img = System.Drawing.Image.FromFile(si.ImagePathFullRes);
                                //            if (img == null)
                                //                continue;
                                //            if (si.Orientation == 90)
                                //                img.RotateFlip(RotateFlipType.Rotate90FlipNone);
                                //            if (si.Orientation == 180)
                                //                img.RotateFlip(RotateFlipType.Rotate180FlipNone);
                                //            if (si.Orientation == 270)
                                //                img.RotateFlip(RotateFlipType.Rotate270FlipNone);
                                //        }
                                //    }

                                //    if (img == null)
                                //        continue;

                                //    logger.Info("about to crop file: " + si.ImageFileName);
                                //    if (WithCroppingCheckBox && ((si.CropW > 0) || (si.CropH > 0)))
                                //        img = cropImage(img, getCropArea(img, si.CropL, si.CropT, si.CropH, si.CropW));
                                //    FileInfo source = new FileInfo(si.ImagePathFullRes);
                                //    string destFileName = source.Name;
                                //    if (FileRenames.ContainsKey(source.Name))
                                //        destFileName = FileRenames[source.Name];
                                //    if (renderGreenScreen && destFileName.ToLower().EndsWith(".png"))
                                //        destFileName = destFileName.Replace(".png", ".jpg").Replace(".PNG", ".jpg");
                                //    if (!File.Exists(System.IO.Path.Combine(selectedImageMatchExportPath, destFileName)) && destFileName.ToLower().EndsWith("png"))
                                //        img.Save(System.IO.Path.Combine(selectedImageMatchExportPath, destFileName), System.Drawing.Imaging.ImageFormat.Png);
                                //    else if (!File.Exists(System.IO.Path.Combine(selectedImageMatchExportPath, destFileName)))
                                //    {
                                //        ImageCodecInfo jpgEncoder = GetEncoder(ImageFormat.Jpeg);
                                //        System.Drawing.Imaging.Encoder myEncoder = System.Drawing.Imaging.Encoder.Quality;
                                //        EncoderParameters myEncoderParameters = new EncoderParameters(1);
                                //        EncoderParameter myEncoderParameter = new EncoderParameter(myEncoder, jpgQuality);
                                //        myEncoderParameters.Param[0] = myEncoderParameter;
                                //        img.Save(System.IO.Path.Combine(selectedImageMatchExportPath, destFileName), jpgEncoder, myEncoderParameters);
                                //    }

                                //    img.Dispose();
                                //    progressInfo++;
                                //    GC.Collect();
                                //}


                                //OLD


                            }
                        }
                    }
                    else
                    {

                        DirectoryInfo dirInfo = new DirectoryInfo(flowProject.ImageDirPath);
                        //int fileCount = dirInfo.GetFiles().Count();
                        int fileCount = imageFileList.Count();

                        IEnumerable<FileInfo> fileInfos = imageFileList.Select(f => new FileInfo(System.IO.Path.Combine(flowProject.ImageDirPath, f)));

                        progressInfo.Update("Copying file {0} of {1}", 0, fileCount, 1, true);

                        foreach (FileInfo source in fileInfos)
                        {
                            if (source.Exists)
                            {
                                string destFileName = source.Name;
                                if (FileRenames.ContainsKey(source.Name))
                                    destFileName = FileRenames[source.Name];

                                source.CopyTo(System.IO.Path.Combine(selectedImageMatchExportPath, destFileName), true);
                            }
                            progressInfo++;
                        }
                        //IOUtil.CopyFiles(flowProject.ImageDirPath, selectedImageMatchExportPath, imageFileList, progressInfo);
                    }
                    if (this.FlowMasterDataContext.IsPUDConfiguration && this.CurrentFlowProject.FlowCatalog != null)
                    {
                        string prmFile = System.IO.Path.Combine(selectedImageMatchExportPath, "FlowCatalog.prm");
                        this.CurrentFlowProject.FlowCatalog.WritePRMData(prmFile);
                    }
 
                    if (forUpload)
                    {

                        OrderUploadHistory hist = new OrderUploadHistory();
                        hist.LabOrderId = "0";
                        this.FlowController.ProjectController.FlowMasterDataContext.OrderUploadHistories.InsertOnSubmit(hist);
                        this.FlowController.ProjectController.FlowMasterDataContext.SubmitChanges();
                        int orderhistoryID = hist.OrderUploadHistoryID;

                        progressInfo.Complete("Finished Preparing Image Match files. Adding to upload queue");

                        //create the zip
                        string IMZipFilePath = this.ZipImFiles(selectedImageMatchExportPath, flowProject.FileSafeProjectName, progressInfo);
                        

                        //lets queue it up
                        string queueDir = System.IO.Path.Combine(flowProject.FlowMasterDataContext.PreferenceManager.Application.LabOrderQueueDirectory, "ImageMatch_exp_" + orderhistoryID.ToString());
                        if (!Directory.Exists(queueDir))
                            Directory.CreateDirectory(queueDir);
                        File.Move(IMZipFilePath, System.IO.Path.Combine(queueDir, flowProject.FileSafeProjectName + ".zip"));

                        //string ftpAddress = ProjectController.FlowMasterDataContext.PreferenceManager.Upload.ProjectTransfer.HostAddress;
                        //int ftpPort = ProjectController.FlowMasterDataContext.PreferenceManager.Upload.ProjectTransfer.HostPort;
                        //string ftpUserName = ProjectController.FlowMasterDataContext.PreferenceManager.Upload.ProjectTransfer.UserName;
                        //string ftpPassword = ProjectController.FlowMasterDataContext.PreferenceManager.Upload.ProjectTransfer.Password;
                        //string ftpDirectory = ProjectController.FlowMasterDataContext.PreferenceManager.Upload.ProjectTransfer.RemoteImageMatchDirectory;
                        //if (ftpDirectory == null) ftpDirectory = "";
                        //bool useSftp = ProjectController.FlowMasterDataContext.PreferenceManager.Upload.ProjectTransfer.UseSftp;

                        //string destFile = ftpDirectory + "/" + (new FileInfo(IMZipFilePath)).Name;
                        //destFile = destFile.TrimStart('/');

                        //this.FlowController.ProjectController.IsUploading = true;
                        //FileTransfer ft = new FileTransfer(ftpAddress, ftpPort, ftpUserName, ftpPassword, useSftp, null);
                        //try{
                        //    ft.Connect();
                        //}
                        //catch (Exception ex)
                        //{
                        //    this.Dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                        //    {
                        //        FlowMessageDialog msg = new FlowMessageDialog("FTP Error", "There was an error connecting to the ftp server.\n\nCheck your FTP settings\n\nFTP FAILED\n\n" + ex.Message);
                        //        msg.CancelButtonVisible = false;
                        //        msg.ShowDialog();
                                
                        //    }));
                        //    this.FlowController.ApplicationPanel.IsEnabled = true;
                        //    return;
                        //}
                        //ft.SendFile(IMZipFilePath, destFile);
                        //ft.Disconnect();
                        //this.FlowController.ProjectController.IsUploading = false;

                        //IFTPClient ftp;
                       
                        //if (useSftp)
                        //    ftp = new SFTPClient(ftpAddress, ftpPort, ftpUserName, ftpPassword);
                        //else
                        //    ftp = new FTPClient(ftpAddress, ftpPort, ftpUserName, ftpPassword);
                        //string[] uploadList = {IMZipFilePath};
                        //ftp.ProgressInfo = progressInfo;
                        //ftp.BeginFileUploads(uploadList, ftpDirectory);

                        //send email

                        string mailFile = System.IO.Path.Combine(queueDir, flowProject.FileSafeProjectName + "_mail.txt");

                        string studioemail = null;
                        if (flowProject.FlowMasterDataContext.Organizations.First(o => o.OrganizationID == (int)flowProject.StudioID).OrganizationContactEmail != null &&
                            flowProject.FlowMasterDataContext.Organizations.First(o => o.OrganizationID == (int)flowProject.StudioID).OrganizationContactEmail.Length > 1)
                            studioemail = flowProject.FlowMasterDataContext.Organizations.First(o => o.OrganizationID == (int)flowProject.StudioID).OrganizationContactEmail;

                        string studioName = flowProject.FlowMasterDataContext.Organizations.First(o => o.OrganizationID == (int)flowProject.StudioID).OrganizationName;
                        if (flowProject.FlowMasterDataContext.PreferenceManager.Application.LabEmail != null && flowProject.FlowMasterDataContext.PreferenceManager.Application.LabEmail.Length > 2)
                        {
                            string orderFormFileName = System.IO.Path.Combine(queueDir, flowProject.FileSafeProjectName + "_OrderForm.pdf");
                            using (StreamWriter sw = File.CreateText(mailFile))
                            {
                                sw.WriteLine("To: " + flowProject.FlowMasterDataContext.PreferenceManager.Application.LabEmail);
                                sw.WriteLine("From: " + "flow@flowadmin.com");
                                sw.WriteLine("CC: " + studioemail);
                                sw.WriteLine("Subject: " + "Flow Image Match Export Uploaded - " + studioName + " -- DO NOT REPLY");
                                sw.WriteLine("Attatchment: " + orderFormFileName);
                                sw.WriteLine("New Image Match Export Uploaded\n\nStudio: " + studioName + "\nProject: " + flowProject.FlowProjectName + "\n\n");
                            }

                            this.Dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                            {

                                this.FlowController.ProjectController.CurrentFlowProject.SelectedReportName = "Order Form";

                                FlowReportView newReportView = new FlowReportView("Order Form", this.FlowController.ProjectController.CurrentFlowProject);

                                ReportDataDefinitions.GetReportData(newReportView, FlowController.ProjectController.CurrentFlowProject.FlowProjectDataContext.FilteredOrderPackages.ToList());
                                FlowReport report = new FlowReport(newReportView);
                                report.OrderNumber = "None";
                                report.Generate();


                                ReportDataDefinitions.ExportReport(
                                    this.FlowController.ProjectController.CurrentFlowProject.SelectedReportName + " - " + this.FlowController.ProjectController.CurrentFlowProject.FileSafeProjectName,
                                    report.C1Document, new KeyValuePair<string, string>("PDF", "Adobe PDF (*.pdf)|*.pdf"),
                                    orderFormFileName, false
                                );

                            }));

                        }


                            //string studioemail = null;
                            //if (FlowMasterDataContext.Organizations.First(o => o.OrganizationID == (int)flowProject.StudioID).OrganizationContactEmail != null &&
                            //    FlowMasterDataContext.Organizations.First(o => o.OrganizationID == (int)flowProject.StudioID).OrganizationContactEmail.Length > 1)
                            //    studioemail = FlowMasterDataContext.Organizations.First(o => o.OrganizationID == (int)flowProject.StudioID).OrganizationContactEmail;

                            //string studioName = FlowMasterDataContext.Organizations.First(o => o.OrganizationID == (int)flowProject.StudioID).OrganizationName;
                            //if (this.FlowMasterDataContext.PreferenceManager.Application.LabEmail != null)
                            //{
                            //Emailer mail = new Emailer("Flow Image Match Export Uploaded - " + studioName,
                            //    "New Image Match Export Uploaded\n\nStudio: " + studioName + "\nProject: " + flowProject.FlowProjectName + "\n\n",
                            //    false,
                            //    this.FlowMasterDataContext.PreferenceManager.Application.LabEmail,
                            //    "flow@flowadmin.com", studioemail);


                            

                            
                            //mail.Send();
                        //}
                        FlowOrderStore store = new FlowOrderStore();
                        store.OrderID = 0;
                        store.OrderDate = DateTime.Now;

                        ActivationValidator activation = new ActivationValidator(this.FlowController.ActivationKeyFile);
                        ActivationStore activationStore = activation.Repository.Load();

                        store.ActivationKey = activationStore.ActivationKey;
                        store.ProjectName = this.CurrentFlowProject.FlowProjectName;
                        store.ComputerName = activationStore.ComputerName;
                        store.StudioName = studioName;
                        store.FlowVersion = FlowContext.GetFlowVersion();
                        store.IsImageMatchExport = true;
                        store.SubjectCount = flowProject.SubjectList.Count;
                        store.ImageCount = flowProject.ImageList.Count;
                        store.PackageCount = flowProject.OrderPackageCount;
                        FlowOrderPost FOPost = new FlowOrderPost();
                        string returnMsg = FOPost.Post(store);

                        hist.LabOrderId = "0";
                        hist.CreateDate = DateTime.Now;
                        hist.ProjectName = flowProject.FlowProjectName;
                        hist.ProjectGuid = flowProject.FlowProjectGuid.ToString();
                        hist.ComputerName = activationStore.ComputerName;
                        hist.FlowVersion = FlowContext.GetFlowVersion();
                        hist.IsImageMatchExport = true;
                        hist.SubjectCount = flowProject.SubjectList.Count;
                        hist.ImageCount = flowProject.ImageList.Count;
                        hist.PackageCount = flowProject.OrderPackageCount;
                        this.FlowController.ProjectController.FlowMasterDataContext.SubmitChanges();
                        this.FlowController.ProjectController.FlowMasterDataContext.RefreshOrderUploadHistoryList();
                        using (StreamWriter sw = File.AppendText(mailFile))
                        {
                            sw.WriteLine("HistoryID: " + orderhistoryID);
                        }

                        progressInfo.Update("ImageMatch Export complete. Files have been queued for upload");
                    }
                    else
					    progressInfo.Update("ImageMatch Export complete.");

                    progressInfo.ShowDoneButton();

                    foreach (OrderFormFieldLocal field in this.CurrentFlowProject.FlowProjectDataContext.OrderFormFieldLocalList)
                    {
                        field.SelectedValue = field.FieldValue;
                    }
                    this.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();
                    this.Dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                            {
                        this.FlowController.Project();
                        this.FlowController.ApplicationPanel.IsEnabled = true;
                            }));
				}
			);
            
            return selectedImageMatchExportPath;
		}

        private ImageCodecInfo GetEncoder(ImageFormat format)
        {
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();
            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.FormatID == format.Guid)
                {
                    return codec;
                }
            }
            return null;
        }


        private System.Drawing.Rectangle getCropArea(System.Drawing.Image iSrc, double CropL, double CropT, double CropH,  double CropW)
        {
            double width = iSrc.Width;
            double height = iSrc.Height;

            int cropAreaL = (int)(CropL * width);
            int cropAreaT = (int)(CropT * height);
            int cropAreaW = (int)(CropW * width);
            int cropAreaH = (int)(CropH * height);

            if (cropAreaL + cropAreaW > width)
                cropAreaW = (int)width - cropAreaL;
            if (cropAreaT + cropAreaH > height)
                cropAreaH = (int)height - cropAreaT;


            return new System.Drawing.Rectangle(
                cropAreaL,
                cropAreaT,
                cropAreaW,
                cropAreaH
                );
        }

        private static System.Drawing.Image cropImage(System.Drawing.Image img, System.Drawing.Rectangle cropArea)
        {
            Bitmap bmpImage = new Bitmap(img);
            Bitmap bmpCrop = bmpImage.Clone(cropArea,
            bmpImage.PixelFormat);
            bmpImage.Dispose();
            return (System.Drawing.Image)(bmpCrop);
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="includeAllSubjects"></param>
        private void SubmitOrder(FlowProject flowProject, bool includeAllSubjects, string saveOrderExportPath, string sortField1, string sortField2, string orderFilter, bool useOriginalFileNames)
		{
			if (flowProject.HasIncompleteProducts || flowProject.HasMissingImages)
			{
				//this.ProjectController.CurrentFlowProject = flowProject;
				this.ProjectController.OpenCapture();
			}
			else
			{
				//flowProject.SubmitOrder(includeAllSubjects, saveOrderExportPath, sortField1, sortField2);

				//return;
                FlowMessageBox msg;
				FlowBackgroundWorkerManager.RunWorker(
					delegate
					{
                        string message = "";
                        if (!flowProject.SubmitOrder(includeAllSubjects, saveOrderExportPath, sortField1, sortField2, orderFilter, null,null,null,null,null,false,useOriginalFileNames, out message))
                        {

                            bool result = false;
                            this.Dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                            {
                                msg = new FlowMessageBox("Problem With Order", message);
                                if (msg.ShowDialog() == true)
                                    result = true;
                            }));

                            if (result == false)
                                return;
                        }


                       
					}
				);
			}
		}

/// /////////////////////////////////////////////////////////////////////////
		#endregion EXPORT SECTION
/// /////////////////////////////////////////////////////////////////////////

		private void btnCancel_Click(object sender, RoutedEventArgs e)
		{
			this.FlowController.Project();
		}

        public void showExport(bool forUpload)
		{
            this.cmbFlowProjects.SelectedItem = DefaultProject;
            tabExport.IsSelected = true;
            //rdoExportServer.IsChecked = forUpload;
            //rdoExportFile.IsChecked = !forUpload;
            
        }

        public void showImport()
        {
            tabImport.IsSelected = true;
        }

        private void btnConfigUpload_Click(object sender, RoutedEventArgs e)
        {
            PreferenceSectionsPanel pnl = base.FlowController.ApplicationPanel.ShowPreferencesPanel();
            pnl.FlowController = this.FlowController;
            pnl.ShowUploadPanel();
        }

		private void ImageTypeOption_Checked(object sender, RoutedEventArgs e)
		{
			RadioButton source = sender as RadioButton;

			if (source != null)
			{
				this.SelectedImageType = source.CommandParameter as ProjectImageType;
			}
		}

        private void ImageGroupTypeOption_Checked(object sender, RoutedEventArgs e)
        {
            RadioButton source = sender as RadioButton;

            if (source != null)
            {
                this.SelectedImageGroupType = source.CommandParameter as String;
            }
        }

        private void btnExport_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {

        }

        private void btnSelectExportDataFilePath_Click(object sender, RoutedEventArgs e)
        {
            this.SelectedExportPathDataFile = DialogUtility.SelectFolder(Environment.SpecialFolder.Desktop, "Select destination folder.");
		
        }

        private void ExportTabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void btnSelectExportImagesPath_Click(object sender, RoutedEventArgs e)
        {
            this.SelectedExportPathImages = DialogUtility.SelectFolder(Environment.SpecialFolder.Desktop, "Select destination folder.");
        }



        private void btnAddLayout_Click(object sender, RoutedEventArgs e)
        {
            string v = (sender as Button).DataContext as String;
            this.ProjectController.SelectedLayouts.Add(v);
            cmbAllLayouts.IsDropDownOpen = false;
            cmbSelectedLayouts.ItemsSource = null;
            cmbSelectedLayouts.ItemsSource = this.ProjectController.SelectedLayouts;
            //this.NewFlowProject.UpdateSelectedGroupImageFlags();
            //cmbSelectedGroupImageFlags.UpdateLayout();
        }

        private void btnShowLayouts_Click(object sender, RoutedEventArgs e)
        {
            cmbAllLayouts.IsDropDownOpen = !cmbAllLayouts.IsDropDownOpen;
        }

        private void btnRemoveLayout_Click(object sender, RoutedEventArgs e)
        {
            string v = (sender as Button).DataContext as String;
            this.ProjectController.SelectedLayouts.Remove(v);
            cmbSelectedLayouts.ItemsSource = null;
            cmbSelectedLayouts.ItemsSource = this.ProjectController.SelectedLayouts;
            //this.NewFlowProject.UpdateSelectedGroupImageFlags();
            //cmbSelectedGroupImageFlags.UpdateLayout();
        }

        private void TextBlock_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            string v = (sender as TextBlock).DataContext as String;
            this.ProjectController.SelectedLayouts.Add(v);
            cmbAllLayouts.IsDropDownOpen = false;
            cmbSelectedLayouts.ItemsSource = null;
            cmbSelectedLayouts.ItemsSource = this.ProjectController.SelectedLayouts;
        }

	}
}
