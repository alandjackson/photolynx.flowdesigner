using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Controls;
using Flow.Controller.Project;
using Flow.Lib;
using Flow.Lib.Helpers;
using Flow.Schema.LinqModel.DataContext;
using Flow.View.Project.Base;
using Flow.View.Preferences;

using StructureMap;
using System.Drawing;
using Flow.Designer.Lib.ImageAdjustments.Model;
using Flow.Designer.Lib.Service;
using System.Windows.Threading;
using Flow.View.Dialogs;
using Flow.Schema.Reports;
using Flow.Controller.Reports;
using C1.C1Report;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.InteropServices;


namespace Flow.View.Project
{
	/// <summary>
	/// Interaction logic for OnlineOrderPanel.xaml
	/// </summary>
	public partial class FlowChangeLogPanel : ProjectPanelBase
	{

        public FlowChangeLogPanel()
		{
			InitializeComponent();

            string uri = "http://flow.photolynx.com";
            this.browser.Navigate(new Uri(uri, UriKind.Absolute));
            this.browser.ObjectForScripting = new ScriptingHelper();
		}

        [ComVisible(true)]
        public class ScriptingHelper
        {
            public void ShowMessage(string message)
            {
                MessageBox.Show(message);
            }
        }

	}
}
