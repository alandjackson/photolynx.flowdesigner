﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Flow.View.Project.Base;
using Flow.Schema.LinqModel.DataContext;
using Flow.Controller;
using Flow.Controller.Project;
using Flow.View.Dialogs;
using NLog;

namespace Flow.View.Project
{
    /// <summary>
    /// Interaction logic for FilterPanel.xaml
    /// </summary>
    public partial class FilterPanel : ProjectPanelBase
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public FilterPanel()
        {
            InitializeComponent();
            this.DataContextChanged += new DependencyPropertyChangedEventHandler(FilterPanel_DataContextChanged);
        }

        void FilterPanel_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if(this.DataContext.GetType() == typeof(ProjectController))
                this.FlowController = ((ProjectController)this.DataContext).FlowController;
            else if (this.DataContext.GetType() == typeof(FlowController))
                this.FlowController = (FlowController)this.DataContext;

           // this.cmbSearchField.ItemsSource = ProjectController.CurrentFlowProject.ProjectSubjectFieldListForSearch;
           // this.cmbImageSource.ItemsSource = ProjectController.CurrentFlowProject.FlowProjectDataContext.ImageSourceList;
            
        }

        private void cmbSearchField_DropDownOpened(object sender, EventArgs e)
        {
            //if (((sender as ComboBox).DataContext as SubjectObservableCollection).HasItems)
            //{
            //    ((sender as ComboBox).DataContext as SubjectObservableCollection)[0].FlowProjectDataContext.FlowProject.UpdateProjectSubjectFieldListForSearch();
            //}
            
        }

        private void SearchTextBox_GotFocus(object sender, RoutedEventArgs e)
        {

        }

        private void SearchTextBox_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void SearchTextBox_PreviewKeyUp(object sender, KeyEventArgs e)
        {

        }

        private void cmbImageSource_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void btnClearFilter_Click(object sender, RoutedEventArgs e)
        {
            ClearFilter();
            rdoNoSubjectFilter.IsChecked = true;
            rdoNoImageFilter.IsChecked = true;
            if (this.CurrentFlowProject.SubjectList.CurrentItem != null)
                this.CurrentFlowProject.SubjectList.CurrentItem.RefreshSubjectImages();

            if (this.CurrentFlowProject.FlowProjectDataContext.OrderFilterFilteredSubjects == true)
            {
                this.CurrentFlowProject.FlowProjectDataContext.UpdateFilteredOrderPackages();
                this.CurrentFlowProject.FlowProjectDataContext.UpdateOrderFilterValues();
            }
            
        }

        private void ClearFilter()
        {
            CurrentFlowProject.SubjectList.ClearFilter();

            this.FlowMasterDataContext.PreferenceManager.Edit.ImageFilterBeginDate = DateTime.Now;
            this.FlowMasterDataContext.PreferenceManager.Edit.ImageFilterEndDate = DateTime.Now;

            foreach (SubjectImage si in CurrentFlowProject.FlowProjectDataContext.SubjectImages)
                si.IsSuppressed = false;

            if (this.CurrentFlowProject.SubjectList.CurrentItem == null && this.CurrentFlowProject.SubjectList.Count > 1)
                this.CurrentFlowProject.SubjectList.CurrentOrdinalPosition = 1;

            this.CurrentFlowProject.SubjectList.CurrentFilterName = "No Filter Set";
            this.CurrentFlowProject.SubjectList.UpdateFilterSummary();
        }

        private void btnApplyFilter_Click(object sender, RoutedEventArgs e)
        {
            int originalSubjectListOrdinalPosition = this.CurrentFlowProject.SubjectList.CurrentOrdinalPosition;
            ClearFilter();
            string newFilterText = "";


            try
            {
                //One Subject Filter
                if (rdoStringMatch.IsChecked == true)
                {
                    //
                    if (CurrentFlowProject.SubjectList.DataGridCollectionView != null && CurrentFlowProject.SubjectList.DataGridCollectionView.Count > 0)
                    {
                        List<int> showSubjectIDs = CurrentFlowProject.SubjectList.GetQueryResultIndices(this.SearchTextBox.Text, false, this.cmbSearchField.SelectedValue.ToString()).ToList();
                        CurrentFlowProject.SubjectList.NarrowFilterByTheseSubjectIDs(showSubjectIDs);
                    }
                    else
                    {
                        CurrentFlowProject.SubjectList.ApplyFilter(this.SearchTextBox.Text, this.cmbSearchField.SelectedValue.ToString());
                    }
                    newFilterText = newFilterText + this.cmbSearchField.SelectedValue.ToString() + " contains " + this.SearchTextBox.Text;
                }

                //
                //IMAGE FILTERS
                //
                
                if (rdoImageFlag.IsChecked == true)
                {
                    if (cmbImageFlag.SelectedValue.ToString().ToLower().Contains("is hold image"))
                    {
                        this.FlowController.ProjectController.CurrentFlowProject.SubjectList.FilterOnTheseImages(CurrentFlowProject.FlowProjectDataContext.HoldImageList.ToList());
                        newFilterText = "Images On Hold";
                    }
                    else if (cmbImageFlag.SelectedValue.ToString().ToLower().Contains("is not hold image"))
                    {
                        List<SubjectImage> tmpList = new List<SubjectImage>(CurrentFlowProject.ImageList);
                        foreach (SubjectImage i in CurrentFlowProject.FlowProjectDataContext.HoldImageList)
                            tmpList.Remove(i);
                        this.FlowController.ProjectController.CurrentFlowProject.SubjectList.FilterOnTheseImages(tmpList);
                        newFilterText = "Images NOT On Hold";
                    }
                    else if (cmbImageFlag.SelectedValue.ToString().ToLower().Contains("is primary image"))
                    {
                        this.FlowController.ProjectController.CurrentFlowProject.SubjectList.FilterOnTheseImages(CurrentFlowProject.ImageList.Where(i => i.IsPrimary == true).ToList());
                        newFilterText = "Is Primary Image";
                    }
                    else if (cmbImageFlag.SelectedValue.ToString().ToLower().Contains("is not primary image"))
                    {
                        this.FlowController.ProjectController.CurrentFlowProject.SubjectList.FilterOnTheseImages(CurrentFlowProject.ImageList.Where(i => i.IsNotPrimary == true).ToList());
                        newFilterText = "Is NOT Primary Image";
                    }
                    else
                    {
                        string filter = cmbImageFlag.SelectedValue.ToString();
                        if (filter.ToLower().StartsWith("is not "))
                        {
                            string filterName = filter.Substring(7);
                            this.FlowController.ProjectController.CurrentFlowProject.SubjectList.FilterOnTheseImages(CurrentFlowProject.ImageList.Where(i => i.SubjectImageTypes.Count(sit => sit.ProjectImageType.ImageTypeDesc == filterName && sit.IsAssigned != true) > 0).ToList());
                            newFilterText = filter;
                        }
                        else
                        {
                            string filterName = filter.Substring(3);
                            this.FlowController.ProjectController.CurrentFlowProject.SubjectList.FilterOnTheseImages(CurrentFlowProject.ImageList.Where(i => i.SubjectImageTypes.Count(sit => sit.ProjectImageType.ImageTypeDesc == filterName && sit.IsAssigned == true) > 0).ToList());
                            newFilterText = filter;
                        }
                    }

                }

                if (rdoPhotographedBy.IsChecked == true)
                {
                    this.FlowController.ProjectController.CurrentFlowProject.SubjectList.FilterOnTheseImages(CurrentFlowProject.FlowProjectDataContext.SubjectImages.Where(i => i.SourceComputer.ToLower() == cmbImageSource.SelectedValue.ToString().ToLower()).ToList());
                    newFilterText =  newFilterText + "Subjects Assinged By " + cmbImageSource.SelectedValue.ToString();
                }

                if (rdoDateRange.IsChecked == true)
                {
                    List<SubjectImage> imagesOfFilteredSubjects = CurrentFlowProject.FlowProjectDataContext.SubjectImages.ToList();
                    if (CurrentFlowProject.SubjectList.IsFiltered)
                    {
                        imagesOfFilteredSubjects = new List<SubjectImage>();
                        foreach (Subject s in CurrentFlowProject.SubjectList.GetDataGridCollectionViewSubjects())
                        {
                            foreach (SubjectImage si in s.SubjectImages)
                                imagesOfFilteredSubjects.Add(si);
                        }
                    }
                    if (chkAfterDate.IsChecked == true && chkBeforeDate.IsChecked == false)
                    {
                        this.FlowController.ProjectController.CurrentFlowProject.SubjectList.FilterOnTheseImages(imagesOfFilteredSubjects.Where(i => i.DateAssigned >= calAfterDate.SelectedDate).ToList());
                        newFilterText = newFilterText + "Images Assigned After " + calAfterDate.SelectedDate;
                    }
                    if (chkAfterDate.IsChecked == false && chkBeforeDate.IsChecked == true)
                    {
                        this.FlowController.ProjectController.CurrentFlowProject.SubjectList.FilterOnTheseImages(imagesOfFilteredSubjects.Where(i => i.DateAssigned <= calBeforeDate.SelectedDate).ToList());
                        newFilterText = newFilterText + "Images Assigned Before " + calBeforeDate.SelectedDate;
                    }
                    if (chkAfterDate.IsChecked == true && chkBeforeDate.IsChecked == true)
                    {
                        this.FlowController.ProjectController.CurrentFlowProject.SubjectList.FilterOnTheseImages(imagesOfFilteredSubjects.Where(i => (i.DateAssigned <= calBeforeDate.SelectedDate) && (i.DateAssigned >= calAfterDate.SelectedDate)).ToList());
                        newFilterText = newFilterText + "Images Assigned Between " + calAfterDate.SelectedDate + " and " + calBeforeDate.SelectedDate;
                    }
                }

                if(newFilterText.Length > 0)
                    newFilterText =  newFilterText + " & ";
                //
                //SUBJECT FILTERS
                //
                
                

                if (rdoWithOrders.IsChecked == true)
                {
                    if (((ComboBoxItem)cmbOrders.SelectedValue).Content.ToString() == "New Orders")
                    {
                        this.FlowController.ProjectController.CurrentFlowProject.SubjectList.FilterOnTheseSubjects(CurrentFlowProject.FlowProjectDataContext.SubjectOrders.Where(o => o.OrderPackages.Count > 0 && o.OrderPackages.Count(p => p.LabOrderDate == null) > 0).Select(so => so.Subject).ToList());
                        newFilterText = newFilterText +  "Subjects With New Orders";
                    }
                    else if (((ComboBoxItem)cmbOrders.SelectedValue).Content.ToString() == "Submitted Orders")
                    {
                        this.FlowController.ProjectController.CurrentFlowProject.SubjectList.FilterOnTheseSubjects(CurrentFlowProject.FlowProjectDataContext.SubjectOrders.Where(o => o.OrderPackages.Count > 0 && o.OrderPackages.Count(p => p.LabOrderDate != null) > 0).Select(so => so.Subject).ToList());
                        newFilterText = newFilterText +  "Subjects With Submitted Orders";
                    }
                    else
                    {
                        this.FlowController.ProjectController.CurrentFlowProject.SubjectList.FilterOnTheseSubjects(CurrentFlowProject.SubjectList.Where(s => s.PackageSummary == "No Packages").ToList());
                        newFilterText = newFilterText +  "Subjects With No Orders";
                    }
                }

                if (rdoSubjectFilterReady.IsChecked == true)
                {
                    this.FlowController.ProjectController.CurrentFlowProject.SubjectList.FilterOnTheseSubjects(CurrentFlowProject.FlowProjectDataContext.Subjects.Where(s => s.Ready == true).ToList());
                    newFilterText = newFilterText + "Subjects Ready";
                }

                //WITH OR WITHOUT IMAGES
                if (rdoNoSubjectFilter.IsChecked == true && rdoWithImages.IsChecked == true)
                {
                    if (((ComboBoxItem)cmbWithWithoutImages.SelectedValue).Content.ToString() == "With Images")
                    {
                        this.FlowController.ProjectController.CurrentFlowProject.SubjectList.FilterOnTheseSubjects(CurrentFlowProject.FlowProjectDataContext.Subjects.Where(s => s.SubjectImages.Count > 0).ToList());
                        newFilterText = newFilterText +  "Subjects With Images";
                    }
                    else if (((ComboBoxItem)cmbWithWithoutImages.SelectedValue).Content.ToString() == "Without Images")
                    {
                        this.FlowController.ProjectController.CurrentFlowProject.SubjectList.FilterOnTheseSubjects(CurrentFlowProject.FlowProjectDataContext.Subjects.Where(s => s.SubjectImages.Count == 0).ToList());
                        newFilterText = newFilterText +  "Subjects Without Images";
                    }
                    else if (((ComboBoxItem)cmbWithWithoutImages.SelectedValue).Content.ToString() == "With Group Images")
                    {
                        this.FlowController.ProjectController.CurrentFlowProject.SubjectList.FilterOnTheseSubjects(CurrentFlowProject.FlowProjectDataContext.Subjects.Where(s => s.SubjectImages.Count > 0 && s.SubjectImages.Any(si => si.GroupImageGuid != null)).ToList());
                        newFilterText = newFilterText + "Subjects With Images";
                    }
                    else if (((ComboBoxItem)cmbWithWithoutImages.SelectedValue).Content.ToString() == "Without Group Images")
                    {
                        this.FlowController.ProjectController.CurrentFlowProject.SubjectList.FilterOnTheseSubjects(CurrentFlowProject.FlowProjectDataContext.Subjects.Where(s => s.SubjectImages.Count == 0 || !s.SubjectImages.Any(si => si.GroupImageGuid != null)).ToList());
                        newFilterText = newFilterText + "Subjects Without Images";
                    }
                }
                else if (rdoWithImages.IsChecked == true)
                {
                    //a subjectFilter was already applied, so lets use "NarrowFilterByTheseSubjectIDs
                    if (((ComboBoxItem)cmbWithWithoutImages.SelectedValue).Content.ToString() == "With Images")
                    {
                        this.FlowController.ProjectController.CurrentFlowProject.SubjectList.NarrowFilterByTheseSubjectIDs(CurrentFlowProject.FlowProjectDataContext.Subjects.Where(s => s.SubjectImages.Count > 0 && CurrentFlowProject.SubjectList.GetDataGridCollectionViewSubjects().Contains(s)).Select(s => s.SubjectID).ToList());
                        newFilterText = newFilterText +  "Subjects With Images";
                    }
                    else if (((ComboBoxItem)cmbWithWithoutImages.SelectedValue).Content.ToString() == "Without Images")
                    {
                        this.FlowController.ProjectController.CurrentFlowProject.SubjectList.NarrowFilterByTheseSubjectIDs(CurrentFlowProject.FlowProjectDataContext.Subjects.Where(s => s.SubjectImages.Count == 0  && CurrentFlowProject.SubjectList.GetDataGridCollectionViewSubjects().Contains(s)).Select(s => s.SubjectID).ToList());
                        newFilterText = newFilterText +  "Subjects Without Images";
                    }
                    if (((ComboBoxItem)cmbWithWithoutImages.SelectedValue).Content.ToString() == "With Group Images")
                    {
                        this.FlowController.ProjectController.CurrentFlowProject.SubjectList.NarrowFilterByTheseSubjectIDs(CurrentFlowProject.FlowProjectDataContext.Subjects.Where(s => s.SubjectImages.Count > 0 && s.SubjectImages.Any(si => si.GroupImageGuid != null) && CurrentFlowProject.SubjectList.GetDataGridCollectionViewSubjects().Contains(s)).Select(s => s.SubjectID).ToList());
                        newFilterText = newFilterText + "Subjects With Images";
                    }
                    else if (((ComboBoxItem)cmbWithWithoutImages.SelectedValue).Content.ToString() == "Without Group Images")
                    {
                        this.FlowController.ProjectController.CurrentFlowProject.SubjectList.NarrowFilterByTheseSubjectIDs(CurrentFlowProject.FlowProjectDataContext.Subjects.Where(s => (s.SubjectImages.Count == 0 || !s.SubjectImages.Any(si => si.GroupImageGuid != null)) && CurrentFlowProject.SubjectList.GetDataGridCollectionViewSubjects().Contains(s)).Select(s => s.SubjectID).ToList());
                        newFilterText = newFilterText + "Subjects Without Images";
                    }

                }

                this.CurrentFlowProject.SubjectList.CurrentFilterName = newFilterText;


                if (this.CurrentFlowProject.SubjectList.DataGridCollectionView.Count == 0)
                {
                    ClearFilter();
                    if (originalSubjectListOrdinalPosition <= this.CurrentFlowProject.SubjectList.RecordCount)
                        this.CurrentFlowProject.SubjectList.CurrentOrdinalPosition = originalSubjectListOrdinalPosition;
                    FlowMessageBox msg = new FlowMessageBox("Filter Results", "The current filter settings produced no matches");
                    msg.CancelButtonVisible = false;
                    msg.ShowDialog();
                }

                

                this.CurrentFlowProject.SubjectList.UpdateFilterSummary();
                if (this.CurrentFlowProject.SubjectList.CurrentItem != null)
                    this.CurrentFlowProject.SubjectList.CurrentItem.RefreshSubjectImages();
            }
            catch (Exception exception)
            {
                logger.ErrorException("Exception occured when applying filter.", exception);

                FlowMessageBox msg = new FlowMessageBox("Error with filter", "There was an error running this filter.\n\nPlease check your filter settings and try again.");
                msg.CancelButtonVisible = false;
                msg.ShowDialog();
            }

            if (this.CurrentFlowProject.FlowProjectDataContext.OrderFilterFilteredSubjects == true)
            {
                this.CurrentFlowProject.FlowProjectDataContext.UpdateFilteredOrderPackages();
                this.CurrentFlowProject.FlowProjectDataContext.UpdateOrderFilterValues();
            }
        }

        private void rdoHoldImages_Checked(object sender, RoutedEventArgs e)
        {

        }
    }
}
