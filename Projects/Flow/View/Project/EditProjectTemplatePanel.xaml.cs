﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Controller;
using Flow.Schema.LinqModel.DataContext;
using Flow.View.Project.Base;
using Flow.View.Dialogs;

namespace Flow.View.Project
{
	/// <summary>
	/// Interaction logic for ProjectTemplatePanel.xaml
	/// </summary>
	public partial class EditProjectTemplatePanel : ProjectPanelBase
	{
		public EditProjectTemplatePanel()
		{
			InitializeComponent();
		}

		protected override void OnSetFlowController()
		{
			this.FlowController.ProjectController.PropertyChanged +=
				new PropertyChangedEventHandler(ProjectController_PropertyChanged);
	
			uxGeneralPanel.ProjectController = this.FlowController.ProjectController;
			uxSubjectFieldPanel.ProjectController = this.FlowController.ProjectController;
			uxSubjectPanel.ProjectController = this.FlowController.ProjectController;
			uxOrganizationPanel.ProjectController = this.FlowController.ProjectController;
			uxImageOptionsPanel.ProjectController = this.FlowController.ProjectController;
			uxEventTriggersPanel.ProjectController = this.FlowController.ProjectController;
            //uxBarcodeSettingsPanel.ProjectController = this.FlowController.ProjectController;

			uxEventTriggersPanel.DataContext = this.FlowController.ProjectController.CurrentProjectTemplate;
            //uxBarcodeSettingsPanel.DataContext = this.FlowController.ProjectController.CurrentProjectTemplate;
		}

		private void btnSaveProjectTemplate_Click(object sender, RoutedEventArgs e)
		{
            if (this.CurrentProjectTemplate == null)
                return;

            string msgText = "";
            if(string.IsNullOrEmpty(this.CurrentProjectTemplate.ProjectTemplateDesc))
            {
                msgText = "You must specify a Description for this project template";
            }

            if(!string.IsNullOrEmpty(msgText))
            {
                FlowMessageBox msg = new FlowMessageBox("Can't Save", msgText);
                msg.CancelButtonVisible = false;
                msg.ShowDialog();
                return;
            }
			uxImageOptionsPanel.SetProjectFilenameFormatFields();
			this.ProjectController.SaveProjectTemplate();
			uxOrganizationPanel.SaveOrgStructureFromTreeView();
			this.ProjectController.FlowMasterDataContext.SubmitChanges();
            this.ProjectController.MainProjectPanel.ShowViewProjectTemplatesPanel();
		}

		private void btnCancelProjectTemplateEdit_Click(object sender, RoutedEventArgs e)
		{
			this.FlowMasterDataContext.Revert();

            if (this.CurrentProjectTemplate != null)
			    this.CurrentProjectTemplate.Revert();

			this.ProjectController.CurrentProjectTemplate = null;

			this.FlowController.Project();
            this.ProjectController.MainProjectPanel.ShowViewProjectTemplatesPanel();
		}

		private void btnNewProjectFromTemplate_Click(object sender, RoutedEventArgs e)
		{
			this.ProjectController.CreateNewProject();
		}

		private void ProjectController_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			// Ensures that the first tab is selected when the selection of a Project Template occurs
			if (e.PropertyName == "CurrentProjectTemplate")
				tabEditProjectTemplate.SelectedIndex = 0;
		}
	
	}
}
