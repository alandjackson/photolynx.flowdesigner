﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.View.Project.Base;

namespace Flow.View.Project
{
	/// <summary>
	/// Interaction logic for ViewProductProgramsPanel.xaml
	/// </summary>
	public partial class ViewFlowCatalogsPanel : ProjectPanelBase
	{
		public ViewFlowCatalogsPanel()
		{
			InitializeComponent();
		}

		protected override void OnSetFlowController()
		{
			base.OnSetFlowController();

			this.DataContext = this.FlowMasterDataContext.FlowCatalogList;
		}
	}
}
