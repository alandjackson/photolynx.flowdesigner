﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Controller;
using Flow.Controls.View.RecordDisplay;
using Flow.Lib.Helpers;
using Flow.Schema.LinqModel.DataContext;
using Flow.View.Project.Base;
using System.Threading;
using System.Data.SqlServerCe;
using Flow.Controller.Project;
using NLog;

namespace Flow.View.Project
{
    /// <summary>
    /// Interaction logic for ProjectsPanel.xaml
    /// </summary>
    public partial class ViewProjectsPanel : ProjectPanelBase
    {
        //ICollectionView _flowProjectListView;

        private static Logger logger = LogManager.GetCurrentClassLogger();

		public ViewProjectsPanel()
        {
            InitializeComponent();
        }

		protected override void OnSetFlowController()
		{
			this.FlowMasterDataContext.FlowProjectList.View.SortDescriptions.Add(new SortDescription("DateModified", ListSortDirection.Descending));

			this.DataContext = this.FlowMasterDataContext;
		}

		private void FlowProjectRecordDisplay_MouseDoubleClick(object sender, MouseButtonEventArgs e)
		{            
			FlowProjectRecordDisplay selectedItem = sender as FlowProjectRecordDisplay;

            logger.Info("Project record display double-clicked: name='{0}' guid='{1}'",
                selectedItem.AssignedFlowProject.FullProjectDisplayName,
                selectedItem.AssignedFlowProject.FlowProjectGuid);

            if (!selectedItem.AssignedFlowProject.EnableProject)
                return;

            if (selectedItem.AssignedFlowProject.inProgress)
                return;

			if (selectedItem != null)
			{
                if (selectedItem.AssignedFlowProject == this.CurrentFlowProject)
                {
                    this.FlowController.Capture();
                    return;
                }

                
                this.ProjectController.Load(selectedItem.AssignedFlowProject);
                if (selectedItem.AssignedFlowProject.FlowProjectDataContext != null)
                {
                    selectedItem.AssignedFlowProject.FlowProjectDataContext.SubjectDataLoaded += delegate
                    {
                        
                        if (this.ProjectController.CurrentFlowProject.SubjectList.Count() > 0)
                            this.ProjectController.CurrentFlowProject.SubjectList.CurrentItem.RefreshSubjectImages();
                        this.FlowController.Capture();
                    };
                }

                
			}
		}

		private void EditProjectButton_Click(object sender, EventArgs<FlowProject> e)
		{
            logger.Info("Edit Project button clicked: name='{0}' guid='{1}'",
                e.Data.FullProjectDisplayName,
                e.Data.FlowProjectGuid);

            if (this.CurrentFlowProject == e.Data)
            {
                this.FlowController.Capture();//init all capture controllers
                this.FlowController.Edit();
                return;
            }
	
			this.ProjectController.Load(e.Data);

            if (e.Data.FlowProjectDataContext != null)
            {
                e.Data.FlowProjectDataContext.SubjectDataLoaded +=
                    delegate
                    {
                        this.FlowController.Capture();//init all capture controllers
                        this.FlowController.Edit();
                        if (this.ProjectController.CurrentFlowProject.SubjectList.Count() > 0)
                            this.ProjectController.CurrentFlowProject.SubjectList.CurrentItem.RefreshSubjectImages();

                    };
            }
		}

		//private void DeleteProjectButton_Click(object sender, EventArgs<FlowProject> e)
		//{

		//}

		private void ArchiveProjectButton_Click(object sender, EventArgs<FlowProject> e)
		{
            logger.Info("Archive Project button clicked: name='{0}' guid='{1}'",
                e.Data.FullProjectDisplayName,
                e.Data.FlowProjectGuid);

            if (this.CurrentFlowProject == e.Data)
            {
                this.ProjectController.ExportProject(e.Data, false);
                return;
            }

            //if (this.CurrentFlowProject != e.Data)
            //{
                this.ProjectController.Load(e.Data);
           // }

                if (this.ProjectController.CurrentFlowProject != null)
                {
                    this.ProjectController.CurrentFlowProject.FlowProjectDataContext.SubjectDataLoaded += delegate
                       {
                           this.ProjectController.ExportProject(e.Data, false);
                       };
                }
		}

        private void SubmitOrdersButton_Click(object sender, EventArgs<FlowProject> e)
        {
            logger.Info("Submit Orders button clicked: name='{0}' guid='{1}'",
                e.Data.FullProjectDisplayName,
                e.Data.FlowProjectGuid);

            if (this.CurrentFlowProject == e.Data)
            {
                this.ProjectController.SubmitOrders(e.Data);
                return;
            }

            //if (this.CurrentFlowProject != e.Data)
            //{
                this.ProjectController.Load(e.Data);
            //}
                if (this.ProjectController.CurrentFlowProject != null)
                {
                    this.ProjectController.CurrentFlowProject.FlowProjectDataContext.SubjectDataLoaded += delegate
                       {
                           this.ProjectController.SubmitOrders(e.Data);
                       };
                }
        }

        private void SubmitOnlineOrderDataButton_Click(object sender, EventArgs<FlowProject> e)
        {
            logger.Info("Online Gallery button clicked: name='{0}' guid='{1}'",
                e.Data.FullProjectDisplayName,
                e.Data.FlowProjectGuid);

            if (this.CurrentFlowProject == e.Data)
            {
                this.ProjectController.SubmitOnlineOrderData(e.Data);
                return;
            }


           // if (this.CurrentFlowProject != e.Data)
            //{
                this.ProjectController.Load(e.Data);
           // }

                if (this.ProjectController.CurrentFlowProject != null)
                {
                    this.ProjectController.CurrentFlowProject.FlowProjectDataContext.SubjectDataLoaded += delegate
                       {
                           this.ProjectController.SubmitOnlineOrderData(e.Data);
                       };
                }
        }

        private void DeleteProject_Click(object sender, EventArgs<FlowProject> e)
        {
            logger.Info("Delete Project button clicked: name='{0}' guid='{1}'",
                e.Data.FullProjectDisplayName,
                e.Data.FlowProjectGuid);

            this.ProjectController.DeleteProject(e.Data);
            this.applyFilter(this.txtSearchAllProjects.Text);
        }

        private bool eventwatchersinited = false;
        private void ArchiveDeleteProject_Click(object sender, EventArgs<FlowProject> e)
        {
            logger.Info("Archive Delete Project button clicked: name='{0}' guid='{1}'",
                e.Data.FullProjectDisplayName,
                e.Data.FlowProjectGuid);

            if (this.CurrentFlowProject == e.Data)
            {
                this.ProjectController.ArchiveDeleteProject(e.Data);
                this.applyFilter(this.txtSearchAllProjects.Text);
                return;
            }


           // if (this.CurrentFlowProject != e.Data)
           // {
                this.ProjectController.Load(e.Data);
           // }

                this.ProjectController.CurrentFlowProject.FlowProjectDataContext.SubjectDataLoaded += delegate
                {
                    this.ProjectController.ArchiveDeleteProject(e.Data);
                    this.applyFilter(this.txtSearchAllProjects.Text);
                    if (!eventwatchersinited)
                    {
                        this.ProjectController.PropertyChanged += new PropertyChangedEventHandler(ProjectController_PropertyChanged);
                        this.FlowMasterDataContext.PropertyChanged += new PropertyChangedEventHandler(FlowMasterDataContext_PropertyChanged);
                        eventwatchersinited = true;
                    }
                };
                this.applyFilter(this.txtSearchAllProjects.Text);
        }

        void FlowMasterDataContext_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "FilteredFlowProjectList")
            {
                this.applyFilter(this.txtSearchAllProjects.Text);
            }
        }

        void ProjectController_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "CurrentFlowProject")
            {
                this.applyFilter(this.txtSearchAllProjects.Text);
            }
        }

		private void UploadProjectButton_Click(object sender, EventArgs<FlowProject> e)
		{
            logger.Info("Upload/Export Project button clicked: name='{0}' guid='{1}'",
                e.Data.FullProjectDisplayName,
                e.Data.FlowProjectGuid);

            if (this.CurrentFlowProject == e.Data)
            {
                this.ProjectController.ExportProject(e.Data, true);
                return;
            }


           // if (this.CurrentFlowProject != e.Data)
           // {
                this.ProjectController.Load(e.Data);
          //  }
                this.ProjectController.CurrentFlowProject.FlowProjectDataContext.SubjectDataLoaded += delegate
                   {
                       this.ProjectController.ExportProject(e.Data, true);
                   };
		}

		private void EditProjectPreferencesButton_Click(object sender, EventArgs<FlowProject> e)
		{
            logger.Info("Edit Project Preferences button clicked: name='{0}' guid='{1}'",
                e.Data.FullProjectDisplayName,
                e.Data.FlowProjectGuid);

			this.ProjectController.EditProjectPreferences(e.Data);
		}

        private void _this_Loaded(object sender, RoutedEventArgs e)
        {
            
        }

        private void _this_MouseEnter(object sender, MouseEventArgs e)
        {
            //if (this.FlowMasterDataContext.PreferenceManager.Permissions.CanModifyExistingProjectPreferences)
            //    ((FlowProjectRecordDisplay)sender).BtnProjectPreferences = true;
            //else
            //    ((FlowProjectRecordDisplay)sender).BtnProjectPreferences = false;

            //if (this.FlowMasterDataContext.PreferenceManager.Permissions.CanExportProjects)
            //{
            //    ((FlowProjectRecordDisplay)sender).BtnExport = true;
            //}
            //else
            //{
            //    ((FlowProjectRecordDisplay)sender).BtnExport = false;
            //}
        }


        public void RefreshBindings()
        {
            this.applyFilter(this.txtSearchAllProjects.Text);
        }

        private void btnSearchAllProjects_Click(object sender, RoutedEventArgs e)
        {
            string term = txtSearchAllProjects.Text.ToLower();
            if (term.Length < 1)
                return;
            List<AllProjectSearchResult> theList = this.ProjectController.AllProjectsSearchResults;
            theList.Clear();

            foreach (FlowProject proj in this.FlowMasterDataContext.FlowProjects)
            {
                proj.FlowMasterDataContext = this.FlowMasterDataContext;
                string connectString = proj.ConnectionString;
                SqlCeConnection cn = new SqlCeConnection(connectString);
                cn.Open();
                SqlCeCommand command = new SqlCeCommand("select FirstName, LastName, TicketCode from Subject where (LOWER(FirstName) like '%" + term + "%' or LOWER(LastName) like '%" + term + "%'  or LOWER(TicketCode) = '" + term + "')", cn);

                SqlCeResultSet rs = command.ExecuteResultSet(new ResultSetOptions());
                while (rs.Read())
                {
                    string first = rs.GetString(0);
                    string last = rs.GetString(1);
                    string ticket = rs.GetString(2);
                    theList.Add(new AllProjectSearchResult(first, last, proj, proj.FlowProjectName, ticket));
                }
                cn.Close();
                cn.Dispose();
            }
            this.ProjectController.AllProjectsSearchResults = theList.OrderBy(p => p.LastName).ToList();
            this.ProjectController.UpdateAllProjectsSearchResults();
            lstSearchResults.ItemsSource = this.ProjectController.AllProjectsSearchResults;
            lstSearchResults.UpdateLayout();
            uxFlowProjectsItemList.ItemsSource = this.FlowMasterDataContext.StudioProjectList;
            uxFlowProjectsItemList.UpdateLayout();
        }

        private void FullName_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {

        }


      

        private void btnOpen_Click(object sender, RoutedEventArgs e)
        {
            FlowProject newFlowProject = ((sender as Button).DataContext as FlowProject);
            if (newFlowProject == this.CurrentFlowProject)
            {
                this.ProjectController.OpenCapture();
            }
            else
            {
                this.ProjectController.Load(newFlowProject);
                //wait for it to load
                if (this.ProjectController.CurrentFlowProject != null)
                {
                    this.ProjectController.CurrentFlowProject.FlowProjectDataContext.SubjectDataLoaded += delegate
                    {
                        this.ProjectController.OpenCapture();
                    };
                }
            }
        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private string filtercol = "project name";
        private bool filterAsc = true;
        private string filterterm = "";
        public void applyFilter(string term)
        {
            if (term == null)
                term = "";
            term = term.ToLower();

            this.FlowMasterDataContext.FilteredFlowProjectList = this.FlowMasterDataContext.FlowProjectList.Where(p =>
              p.FlowProjectName.ToLower().Contains(term) ||
              p.FlowProjectDesc.ToLower().Contains(term) ||
              p.DateCreated.ToShortDateString().ToLower().Contains(term) ||
              term.Length < 1
              ).ToList();


            if (filtercol == "project name")
            {
                if (filterAsc)
                    this.FlowMasterDataContext.FilteredFlowProjectList = this.FlowMasterDataContext.FilteredFlowProjectList.OrderBy(p => p.FlowProjectName).ToList();
                else
                    this.FlowMasterDataContext.FilteredFlowProjectList = this.FlowMasterDataContext.FilteredFlowProjectList.OrderByDescending(p => p.FlowProjectName).ToList();
            }
            if (filtercol == "project desc")
            {
                if (filterAsc)
                    this.FlowMasterDataContext.FilteredFlowProjectList = this.FlowMasterDataContext.FilteredFlowProjectList.OrderBy(p => p.FlowProjectDesc).ThenBy(p => p.FlowProjectName).ToList();
                else
                    this.FlowMasterDataContext.FilteredFlowProjectList = this.FlowMasterDataContext.FilteredFlowProjectList.OrderByDescending(p => p.FlowProjectDesc).ThenBy(p => p.FlowProjectName).ToList();
            }
            if (filtercol == "subject count")
            {
                if (filterAsc)
                    this.FlowMasterDataContext.FilteredFlowProjectList = this.FlowMasterDataContext.FilteredFlowProjectList.OrderBy(p => p.SavedSubjectCount).ThenBy(p => p.FlowProjectName).ToList();
                else
                    this.FlowMasterDataContext.FilteredFlowProjectList = this.FlowMasterDataContext.FilteredFlowProjectList.OrderByDescending(p => p.SavedSubjectCount).ThenBy(p => p.FlowProjectName).ToList();
            }
            if (filtercol == "create date")
            {
                if (filterAsc)
                    this.FlowMasterDataContext.FilteredFlowProjectList = this.FlowMasterDataContext.FilteredFlowProjectList.OrderBy(p => p.DateCreated).ThenBy(p => p.FlowProjectName).ToList();
                else
                    this.FlowMasterDataContext.FilteredFlowProjectList = this.FlowMasterDataContext.FilteredFlowProjectList.OrderByDescending(p => p.DateCreated).ThenBy(p => p.FlowProjectName).ToList();
            }

            if (filtercol == "modify date")
            {
                if (filterAsc)
                    this.FlowMasterDataContext.FilteredFlowProjectList = this.FlowMasterDataContext.FilteredFlowProjectList.OrderBy(p => p.DateModified).ThenBy(p => p.FlowProjectName).ToList();
                else
                    this.FlowMasterDataContext.FilteredFlowProjectList = this.FlowMasterDataContext.FilteredFlowProjectList.OrderByDescending(p => p.DateModified).ThenBy(p => p.FlowProjectName).ToList();
            }
            if (filtercol == "guid")
            {
                if (filterAsc)
                    this.FlowMasterDataContext.FilteredFlowProjectList = this.FlowMasterDataContext.FilteredFlowProjectList.OrderBy(p => p.DateCreated).ThenBy(p => p.FlowProjectName).ToList();
                else
                    this.FlowMasterDataContext.FilteredFlowProjectList = this.FlowMasterDataContext.FilteredFlowProjectList.OrderByDescending(p => p.DateCreated).ThenBy(p => p.FlowProjectName).ToList();
            }

            foreach (FlowProject fp in this.FlowMasterDataContext.FilteredFlowProjectList)
            {
                if (fp.FlowMasterDataContext == null)
                    fp.FlowMasterDataContext = this.FlowMasterDataContext;
            }
            lstSearchResults.ItemsSource = this.FlowMasterDataContext.FilteredFlowProjectList;
            lstSearchResults.UpdateLayout();

            uxFlowProjectsItemList.ItemsSource = this.FlowMasterDataContext.FilteredFlowProjectList;
            uxFlowProjectsItemList.UpdateLayout();
        }
        private void btnClearFilter_Click(object sender, RoutedEventArgs e)
        {
            txtSearchAllProjects.Text = "";
            applyFilter(txtSearchAllProjects.Text);
        }

        private void ProjectColumnHeader_Click(object sender, RoutedEventArgs e)
        {
            if (filtercol == "project name")
                filterAsc = !filterAsc;

            filtercol = "project name";
            applyFilter(txtSearchAllProjects.Text);
        }

        private void PrjectDescColumnHeader_Click(object sender, RoutedEventArgs e)
        {
            if (filtercol == "project desc")
                filterAsc = !filterAsc;

            filtercol = "project desc";
            applyFilter(txtSearchAllProjects.Text);
        }

        private void SubjectCountColumnHeader_Click(object sender, RoutedEventArgs e)
        {
            if (filtercol == "subject count")
                filterAsc = !filterAsc;

            filtercol = "subject count";
            applyFilter(txtSearchAllProjects.Text);
        }

        private void FlowProjectGuidColumnHeader_Click(object sender, RoutedEventArgs e)
        {
            if (filtercol == "guid")
                filterAsc = !filterAsc;

            filtercol = "guid";
            applyFilter(txtSearchAllProjects.Text);
        }

        private void DateCreatedColumnHeader_Click(object sender, RoutedEventArgs e)
        {
            if (filtercol == "create date")
                filterAsc = !filterAsc;

            filtercol = "create date";
            applyFilter(txtSearchAllProjects.Text);
        }

        private void txtSearchAllProjects_KeyUp(object sender, KeyEventArgs e)
        {
            applyFilter(txtSearchAllProjects.Text);
        }

        private void DateModifiedColumnHeader_Click(object sender, RoutedEventArgs e)
        {
            if (filtercol == "modify date")
                filterAsc = !filterAsc;

            filtercol = "modify date";
            applyFilter(txtSearchAllProjects.Text);
        }


        
        private void EditProject_Click(object sender, RoutedEventArgs e)
        {
            this.EditProjectButton_Click(this, new EventArgs<FlowProject>((sender as Button).DataContext as FlowProject));
           
        }

        private void EditProjectPreferences_Click(object sender, RoutedEventArgs e)
        {
            this.EditProjectPreferencesButton_Click(this, new EventArgs<FlowProject>((sender as Button).DataContext as FlowProject));
        }



        private void ArchiveProject_Click(object sender, RoutedEventArgs e)
        {
         
            this.ArchiveProjectButton_Click(this, new EventArgs<FlowProject>((sender as Button).DataContext as FlowProject));
 
        }

        private void SubmitOrders_Click(object sender, RoutedEventArgs e)
        {

            this.SubmitOrdersButton_Click(this, new EventArgs<FlowProject>((sender as Button).DataContext as FlowProject));

        }

        private void UploadProject_Click(object sender, RoutedEventArgs e)
        {

            this.UploadProjectButton_Click(this, new EventArgs<FlowProject>((sender as Button).DataContext as FlowProject));

        }

        private void SubmitOnlineOrderData_Click(object sender, RoutedEventArgs e)
        {

            this.SubmitOnlineOrderDataButton_Click(this, new EventArgs<FlowProject>((sender as Button).DataContext as FlowProject));

        }

        private void btnDeleteProject_Click(object sender, RoutedEventArgs e)
        {

            this.DeleteProject_Click(this, new EventArgs<FlowProject>((sender as Button).DataContext as FlowProject));

        }

        private void btnArchiveProject_Click(object sender, RoutedEventArgs e)
        {

            this.ArchiveDeleteProject_Click(this, new EventArgs<FlowProject>((sender as Button).DataContext as FlowProject));

        }

	}
}
