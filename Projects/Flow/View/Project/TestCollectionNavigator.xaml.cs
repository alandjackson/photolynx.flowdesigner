﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Schema.LinqModel.DataContext;

namespace Flow.View.Project
{
	/// <summary>
	/// Interaction logic for TestCollectionNavigator.xaml
	/// </summary>
	public partial class TestCollectionNavigator : ProjectPanelBase
	{
		public TestCollectionNavigator()
		{
			InitializeComponent();
		}

		protected override void OnSetFlowController()
		{
			base.OnSetFlowController();

			this.FlowProjectManager.Load(1);

			uxSubjectRecords.SetDataContext(this.CurrentFlowProject.FlowProjectDataContext);
		
		}

	}
}
