﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Controller;
using Flow.Controller.Project;
using Flow.Schema.LinqModel.DataContext;
using Flow.View.Project.Base;
using Flow.View.Preferences;
using Flow.Lib;
using Flow.View.Dialogs;

namespace Flow.View.Project
{
	/// <summary>
	/// Interaction logic for NewProjectPanel.xaml
	/// </summary>
	public partial class ProjectGeneralPanel : ProjectPanelBase
	{

		public ProjectGeneralPanel()
		{
			InitializeComponent();
		}

        protected override void OnSetFlowController()
        {
            this.DataContext = this.ProjectController;

            // Populate the Photographer combobox
            cmbPhotographer.ItemsSource =
                from u in this.FlowMasterDataContext.UserList.OrderBy(u => u.UserLastName)
                where u.IsPhotographer == true
                select new
                {
                    PhotographerName = u.FormalFullName,
                    PhotographerID = u.UserID
                };

            // Initialize the Project Date control
           // calProjectDate.DisplayDate = DateTime.Today;
           // calProjectDate.Text = calProjectDate.DisplayDate.ToString();
        }

		/// <summary>
		/// Resets the SelectedValue of comboxes to null
		/// </summary>
		public void RefreshBindings()
		{
            // Populate the Photographer combobox
            cmbPhotographer.ItemsSource =
                from u in this.FlowMasterDataContext.UserList.OrderBy(u => u.UserLastName)
                where u.IsPhotographer == true
                select new
                {
                    PhotographerName = u.FormalFullName,
                    PhotographerID = u.UserID
                };

            cmbStudio.GetBindingExpression(ComboBox.SelectedValueProperty).UpdateTarget();
            cmbOrganization.GetBindingExpression(ComboBox.SelectedValueProperty).UpdateTarget();
            cmbPhotographer.GetBindingExpression(ComboBox.SelectedValueProperty).UpdateTarget();
            DefaultBackgroundThumbnail.DataContext = this.CurrentFlowProject.DefaultGreenScreenBackgroundObject;
            BackgroundThumbnailBig.DataContext = this.CurrentFlowProject.DefaultGreenScreenBackgroundObject;

            cmbSelectedGroupImageFlags.ItemsSource = null;
            cmbSelectedGroupImageFlags.ItemsSource = this.CurrentFlowProject.SelectedGroupImageFlagsNoBlank;

		}

		//private void btnCancel_Click(object sender, RoutedEventArgs e)
		//{
		//    this.FlowController.Project();
		//}

		//private void btnCreate_Click(object sender, RoutedEventArgs e)
		//{
		//    // Get a reference to a new project
		
		//    Flow.Schema.LinqModel.DataContext.ProjectTemplate projectTemplate =
		//        cmbProjectTemplate.SelectedItem as Flow.Schema.LinqModel.DataContext.ProjectTemplate;

		//    this.CurrentFlowProject.FlowCatalogID = projectTemplate.FlowCatalogID;
	
		//    // Set explicitly to avoid strange conversion error
		//    this.CurrentFlowProject.DateCreated = calProjectDate.SelectedDate.Value;

		//    // Create the new project record and all associated project resources
		//    this.ProjectController.GenerateProject(this.CurrentFlowProject, projectTemplate);

		//    // Move control to the DataImport or Capture display
		//    if (chkImportOnCreate.IsChecked.Value)
		//        this.FlowController.DataImport();
		//    else
		//        this.FlowController.Capture();
		//}

        private void btnAddUser_Click(object sender, RoutedEventArgs e)
        {

            if (gridAddPhotographer.Visibility == Visibility.Visible)
            {
                gridAddPhotographer.Visibility = Visibility.Collapsed;
                return;
            }

            gridAddOrg.Visibility = Visibility.Collapsed;
            gridAddPhotographer.Visibility = Visibility.Visible;
            this.txtNewPhotographerFirstName.Text = "";
            this.txtNewPhographertLastName.Text = "";
            this.txtNewPhotographerFirstName.Focus();


            //PreferenceSectionsPanel pnl = (this.DataContext as ProjectController).FlowController.ApplicationPanel.ShowPreferencesPanel();
            //pnl.FlowController = (this.DataContext as ProjectController).FlowController;
            //pnl.ShowUserPanel();
            
        }

        private void btnAddOrganization_Click(object sender, RoutedEventArgs e)
        {
            if (gridAddOrg.Visibility == Visibility.Visible)
            {
                gridAddOrg.Visibility = Visibility.Collapsed;
                return;
            }

            this.txtNewOrgName.Text = "";
            this.cmbNewOrgType.SelectedIndex = 0;
            gridAddPhotographer.Visibility = Visibility.Collapsed;
            gridAddOrg.Visibility = Visibility.Visible;
            this.txtNewOrgName.Focus();

            //PreferenceSectionsPanel pnl = (this.DataContext as ProjectController).FlowController.ApplicationPanel.ShowPreferencesPanel();
            //pnl.FlowController = (this.DataContext as ProjectController).FlowController;
            //pnl.ShowOrganizationPanel();
        }

        private void btnSaveOrg_Click(object sender, RoutedEventArgs e)
        {
            if (txtNewOrgName.Text == null || txtNewOrgName.Text.Length == 0)
            {
                FlowMessageBox msg = new FlowMessageBox("Add Organization", "You must enter an organization name before saving");
                msg.CancelButtonVisible = false;
                msg.ShowDialog();
                return;
            }

            string orgName = txtNewOrgName.Text;
            int orgType = (int)cmbNewOrgType.SelectedValue;
            Organization org = new Organization();
            org.OrganizationName = orgName;
            org.OrganizationTypeID = orgType;
            org.DateCreated = DateTime.Now;
            org.OrganizationGuid = Guid.NewGuid();
            this.FlowMasterDataContext.Organizations.InsertOnSubmit(org);
            this.FlowMasterDataContext.SubmitChanges();
            this.CurrentFlowProject.OrganizationID = org.OrganizationID;
            this.FlowMasterDataContext.RefreshOrgList();

            //this.cmbOrganization.ItemsSource = this.ProjectController.FlowMasterDataContext.SiteOrganizationList;
            this.cmbOrganization.UpdateLayout();
            this.cmbOrganization.SelectedValue = org.OrganizationID;
            gridAddOrg.Visibility = Visibility.Collapsed;
        }

        private void btnSavePhotographer_Click(object sender, RoutedEventArgs e)
        {
            if (txtNewPhotographerFirstName.Text == null || txtNewPhotographerFirstName.Text.Length == 0 ||
               txtNewPhographertLastName.Text == null || txtNewPhographertLastName.Text.Length == 0)
            {
                FlowMessageBox msg = new FlowMessageBox("Add Photographer", "You must enter first and last name before saving");
                msg.CancelButtonVisible = false;
                msg.ShowDialog();
                return;
            }

            string first = txtNewPhotographerFirstName.Text;
            string last = txtNewPhographertLastName.Text;
            User usr = new User();
            usr.UserFirstName = first;
            usr.UserLastName = last;
            usr.IsPhotographer = true;
            this.FlowMasterDataContext.Users.InsertOnSubmit(usr);
            this.FlowMasterDataContext.SubmitChanges();

            this.cmbPhotographer.ItemsSource = from u in this.FlowMasterDataContext.Users.OrderBy(u => u.UserLastName)
                                               where u.IsPhotographer == true
                                               select new
                                               {
                                                   PhotographerName = u.FormalFullName,
                                                   PhotographerID = u.UserID
                                               };
            this.cmbPhotographer.UpdateLayout();
            this.cmbPhotographer.SelectedValue = usr.UserID;

            this.FlowMasterDataContext.RefreshUserList();

            gridAddPhotographer.Visibility = Visibility.Collapsed;
        }

        private void lbxPackageGreenScreen_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.DataContext == null)
                return;
            if ((sender as ListBox).SelectedItem == null)
                return;
            //if (initSelChng == false) { initSelChng = true; return; }


            //string gsBack = ((sender as ListBox).SelectedItem as Flow.Schema.LinqModel.DataContext.FlowMaster.GreenScreenBackground).FullPath;
            this.CurrentFlowProject.DefaultGreenScreenBackgroundObject = ((sender as ListBox).SelectedItem as Flow.Schema.LinqModel.DataContext.FlowMaster.GreenScreenBackground);
            DefaultBackgroundThumbnail.DataContext = this.CurrentFlowProject.DefaultGreenScreenBackgroundObject;
            BackgroundThumbnailBig.DataContext = this.CurrentFlowProject.DefaultGreenScreenBackgroundObject;
        }

        private void btnShowGroupImageFlags_Click(object sender, RoutedEventArgs e)
        {
            cmbAllGroupImageFlags.IsDropDownOpen = !cmbAllGroupImageFlags.IsDropDownOpen;
        }

        private void btnAddGroupImageFlag_Click(object sender, RoutedEventArgs e)
        {
            string v = (sender as Button).DataContext as String;
            this.CurrentFlowProject.SelectedGroupImageFlags.Add(v);
            cmbAllGroupImageFlags.IsDropDownOpen = false;
            cmbSelectedGroupImageFlags.ItemsSource = null;
            cmbSelectedGroupImageFlags.ItemsSource = this.CurrentFlowProject.SelectedGroupImageFlagsNoBlank;
        }
        private void btnRemoveGroupImageFlag_Click(object sender, RoutedEventArgs e)
        {
            string v = (sender as Button).DataContext as String;
            this.CurrentFlowProject.SelectedGroupImageFlags.Remove(v);
            cmbSelectedGroupImageFlags.ItemsSource = null;
            cmbSelectedGroupImageFlags.ItemsSource = this.CurrentFlowProject.SelectedGroupImageFlagsNoBlank;
            //this.NewFlowProject.UpdateSelectedGroupImageFlags();
            //cmbSelectedGroupImageFlags.UpdateLayout();
        }

        private void calProjectDate_SourceUpdated(object sender, DataTransferEventArgs e)
        {
            calDueDate.SelectedDate = calProjectDate.SelectedDate.Value.AddDays(7);
        }

        private void TextBlock_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            string v = (sender as TextBlock).DataContext as String;
            this.CurrentFlowProject.SelectedGroupImageFlags.Add(v);
            cmbAllGroupImageFlags.IsDropDownOpen = false;
            cmbSelectedGroupImageFlags.ItemsSource = null;
            cmbSelectedGroupImageFlags.ItemsSource = this.CurrentFlowProject.SelectedGroupImageFlagsNoBlank;
        }
	}
}
