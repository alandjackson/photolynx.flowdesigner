﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Controller.Project;
using Flow.Controls.Extension;
using Flow.Controls.View.RecordDisplay;
using Flow.Lib.Helpers;
using Flow.Schema;
using Flow.Schema.LinqModel.DataContext;
using Flow.View.Project.Base;

namespace Flow.View.Project
{
	/// <summary>
	/// Interaction logic for ImageOptions.xaml
	/// </summary>
	public partial class ProjectImageOptionsPanel : ProjectPanelBase
	{
		bool _isInitialized = false;

		DependencyPropertyDescriptor dpd =
			DependencyPropertyDescriptor.FromProperty(ComboBox.TextProperty, typeof(ComboBox));

		protected DependencyProperty ImageTypeEditingEnabledProperty = DependencyProperty.Register(
			"ImageTypeEditingEnabled", typeof(bool), typeof(ProjectImageOptionsPanel), new PropertyMetadata(false)
		);

		public bool ImageTypeEditingEnabled
		{
			get { return (bool)this.GetValue(ImageTypeEditingEnabledProperty); }
			set { this.SetValue(ImageTypeEditingEnabledProperty, value); }
		}

		protected DependencyProperty ConfirmCancelPanelVisibilityProperty = DependencyProperty.Register(
			"ConfirmCancelPanelVisibility", typeof(Visibility), typeof(ProjectImageOptionsPanel), new PropertyMetadata(Visibility.Collapsed)
		);

		public Visibility ConfirmCancelPanelVisibility
		{
			get { return (Visibility)this.GetValue(ConfirmCancelPanelVisibilityProperty); }
			set { this.SetValue(ConfirmCancelPanelVisibilityProperty, value); }
		}

		ImageType _currentImageType = null;

		public ProjectImageOptionsPanel()
		{
			InitializeComponent();
			_isInitialized = true;

			dpd.AddValueChanged(cmbFilenameField1, cmbFilenameField_SelectionChanged);
			dpd.AddValueChanged(cmbFilenameField2, cmbFilenameField_SelectionChanged);
			dpd.AddValueChanged(cmbFilenameField3, cmbFilenameField_SelectionChanged);
		}

		private void ProjectPanelBase_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
            UpdateDefaults();
		}

        private bool comboboxesinitiated = false;
        public void UpdateDefaults()
        {
            int imageFileNameFormatTypeID = this.CurrentFlowProject.ImageFormatTypeID;

            string imageFileNameFormatField1 = this.CurrentFlowProject.ImageFormatField1;
            string imageFileNameFormatField2 = this.CurrentFlowProject.ImageFormatField2;
            string imageFileNameFormatField3 = this.CurrentFlowProject.ImageFormatField3;

            if(imageFileNameFormatField1 != null)
                imageFileNameFormatField1 = imageFileNameFormatField1.Replace("~Subject.Last Name", "~Subject.LastName").Replace("~Subject.First Name", "~Subject.FirstName");
            if (imageFileNameFormatField2 != null)
                imageFileNameFormatField2 = imageFileNameFormatField2.Replace("~Subject.Last Name", "~Subject.LastName").Replace("~Subject.First Name", "~Subject.FirstName");
            if (imageFileNameFormatField3 != null)
                imageFileNameFormatField3 = imageFileNameFormatField3.Replace("~Subject.Last Name", "~Subject.LastName").Replace("~Subject.First Name", "~Subject.FirstName");

            if (comboboxesinitiated == false)
            {
                PopulateFilenameFormatComboBoxes(cmbFilenameField1, imageFileNameFormatField1);
                PopulateFilenameFormatComboBoxes(cmbFilenameField2, imageFileNameFormatField2);
                PopulateFilenameFormatComboBoxes(cmbFilenameField3, imageFileNameFormatField3);
                comboboxesinitiated = true;
            }

            foreach (ComboBoxItem separatorField in this.cmbFieldSeparator.Items)
            {
                if (separatorField.Tag.ToString() == this.CurrentFlowProject.ImageFormatSeparatorChar.Trim())
                {
                    this.cmbFieldSeparator.SelectedItem = separatorField;
                    break;
                }
            }

            if (imageFileNameFormatTypeID == ImageFileNameFormatter.IMAGE_FORMAT_TYPE_ORIGINAL)
            {
                rdoFilenameOriginal.IsChecked = true;
            }
            else
            {
                if (imageFileNameFormatTypeID == ImageFileNameFormatter.IMAGE_FORMAT_TYPE_APPEND)
                    rdoFilenamePrefixed.IsChecked = true;
                else
                    rdoFilenameReplaced.IsChecked = true;

                if (imageFileNameFormatField1 != null && !imageFileNameFormatField1.StartsWith("~"))
                    txtFilenameCustom1.Text = imageFileNameFormatField1;

                if (imageFileNameFormatField2 != null)
                {
                    chkFilenameField2.IsChecked = true;

                    if (!imageFileNameFormatField2.StartsWith("~"))
                        txtFilenameCustom2.Text = imageFileNameFormatField2;
                }
                else
                {
                    chkFilenameField2.IsChecked = false;
                    txtFilenameCustom2.Text = "";
                }

                if (imageFileNameFormatField3 != null)
                {
                    chkFilenameField3.IsChecked = true;

                    if (!imageFileNameFormatField3.StartsWith("~"))
                        txtFilenameCustom3.Text = imageFileNameFormatField3;
                }
                else
                {
                    chkFilenameField3.IsChecked = false;
                    txtFilenameCustom3.Text = "";
                }
            }

            UpdateControlStates();
            this.ImageTypeEditButtonStrip.SetSourceCollection<ImageType>(this.FlowMasterDataContext.ImageTypeList);
            this.UpdateLayout();

            
        }

		//public override void SetDataContext(FlowMasterDataContext dataContext, Project Project)
		//{
		//    base.SetDataContext(dataContext, Project);

		//    PopulateFilenameFormatComboBoxes(cmbFilenameField1);
		//    PopulateFilenameFormatComboBoxes(cmbFilenameField2);
		//    PopulateFilenameFormatComboBoxes(cmbFilenameField3);
			
		//    lstCommonTypes.ItemsSource = this.FlowMasterDataContext.ImageTypeList;

		//    this.DataContext = this.CurrentFlowProject;
		//}

		private void PopulateFilenameFormatComboBoxes(ComboBox comboBox, string imageFormatField)
		{
            ComboBoxItem item = new ComboBoxItem();
            item.Content = "Custom";

            comboBox.Items.Add(item);

            item = new ComboBoxItem();
            item.Content = "Photographer";
            item.Tag = "~PhotographerName";

            comboBox.Items.Add(item);

            item = new ComboBoxItem();
            item.Content = "Project Name";
            item.Tag = "~FlowProjectName";

            comboBox.Items.Add(item);

            item = new ComboBoxItem();
            item.Content = "Project Date";
            item.Tag = "~FlowProjectDate";

            comboBox.Items.Add(item);

            //item = new ComboBoxItem();
            //item.Content = "Subject Last Name";
            //item.Tag = "~Subject.Last Name";

            //comboBox.Items.Add(item);

            //item = new ComboBoxItem();
            //item.Content = "Subject First Name";
            //item.Tag = "~Subject.First Name";

            //comboBox.Items.Add(item);


            foreach (ProjectSubjectField subjectField in this.CurrentFlowProject.ProjectSubjectFieldList)
            {
                item = new ComboBoxItem();
                item.Content = subjectField.ProjectSubjectFieldDesc;
                item.Tag = "~Subject." + subjectField.ProjectSubjectFieldDesc;

                comboBox.Items.Add(item);
            }

            if (imageFormatField != null)
            {
                if (imageFormatField.StartsWith("~"))
                {
                    foreach (ComboBoxItem element in comboBox.Items)
                    {
                        if (element.Tag != null && element.Tag.ToString() == imageFormatField)
                        {
                            comboBox.SelectedItem = element;
                            break;
                        }
                    }
                }
            }
		}

///
		#region FILENAME FORMAT CONTROL EVENT HANDLERS
///

		private void rdoFilename_Checked(object sender, RoutedEventArgs e)
		{
			UpdateControlStates();
		}

		private void cmbFieldSeparator_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			UpdateFilenamePreviewText();
		}

		private void chkFilenameField_Checked(object sender, RoutedEventArgs e)
		{
			UpdateControlStates();
		}

		private void cmbFilenameField_SelectionChanged(object sender, EventArgs args)
		{
			UpdateControlStates();
	
			ComboBox source = sender as ComboBox;

			if (source != null && source.Text == "Custom")
			{
				switch (source.Name)
				{
					case "cmbFilenameField1":
						txtFilenameCustom1.Focus();
						txtFilenameCustom1.SelectAll();
						break;
					case "cmbFilenameField2":
						txtFilenameCustom2.Focus();
						txtFilenameCustom2.SelectAll();
						break;
					case "cmbFilenameField3":
						txtFilenameCustom3.Focus();
						txtFilenameCustom3.SelectAll();
						break;
					default:
						break;
				}
			}
		}

		private void txtFilenameCustom_TextChanged(object sender, TextChangedEventArgs e)
		{
			UpdateControlStates();
		}

///
		#endregion
///


		private void UpdateControlStates()
		{
			if (!_isInitialized)
				return;

			if (!chkFilenameField2.IsChecked.Value)
				chkFilenameField3.IsChecked = false;

			lblFieldSeparator.IsEnabled = !rdoFilenameOriginal.IsChecked.Value;
			cmbFieldSeparator.IsEnabled = !rdoFilenameOriginal.IsChecked.Value;

			cmbFilenameField1.IsEnabled = !rdoFilenameOriginal.IsChecked.Value;

			txtFilenameCustom1.IsEnabled =
				!rdoFilenameOriginal.IsChecked.Value	&&
				cmbFilenameField1.Text == "Custom"	
			;

			chkFilenameField2.IsEnabled = !rdoFilenameOriginal.IsChecked.Value;

			cmbFilenameField2.IsEnabled =
				!rdoFilenameOriginal.IsChecked.Value	&&
				chkFilenameField2.IsChecked.Value	
			;

			txtFilenameCustom2.IsEnabled =
				!rdoFilenameOriginal.IsChecked.Value	&&
				chkFilenameField2.IsChecked.Value		&&
				cmbFilenameField2.Text == "Custom"	
			;

			chkFilenameField3.IsEnabled =
				!rdoFilenameOriginal.IsChecked.Value	&&
				chkFilenameField2.IsChecked.Value
			;

			cmbFilenameField3.IsEnabled =
				!rdoFilenameOriginal.IsChecked.Value	&&
				chkFilenameField3.IsChecked.Value
			;

			txtFilenameCustom3.IsEnabled =
				!rdoFilenameOriginal.IsChecked.Value	&&
				chkFilenameField3.IsChecked.Value		&&
				cmbFilenameField3.Text == "Custom"	
			;

			UpdateFilenamePreviewText();
		}

		private void UpdateFilenamePreviewText()
		{
			if (!_isInitialized)
				return;

			string filenamePreview = "0001.jpg";

            string fieldSeparator = "_";
            if (cmbFieldSeparator.SelectedIndex == 0) fieldSeparator = "-";
            if (cmbFieldSeparator.SelectedIndex == 1) fieldSeparator = "_";
            if (cmbFieldSeparator.SelectedIndex == 2) fieldSeparator = "";

			if (!rdoFilenameReplaced.IsChecked.Value)
				filenamePreview = "1234567890" + fieldSeparator + filenamePreview;

			if (!rdoFilenameOriginal.IsChecked.Value)
			{
				string fieldText = "";

				if (chkFilenameField2.IsChecked.Value)
				{
					if (chkFilenameField3.IsChecked.Value)
					{
						fieldText = cmbFilenameField3.Text;

						filenamePreview = SetFieldText(fieldText, txtFilenameCustom3.Text)
								+ fieldSeparator + filenamePreview;
					}

					fieldText = cmbFilenameField2.Text;

					filenamePreview = SetFieldText(fieldText, txtFilenameCustom2.Text)
							+ fieldSeparator + filenamePreview;
				}

				fieldText = cmbFilenameField1.Text;

				filenamePreview = SetFieldText(fieldText, txtFilenameCustom1.Text)
						+ fieldSeparator + filenamePreview;
			}

			lblFilenamePreview.Text = filenamePreview;
		}


		private string SetFieldText(string selection, string custom)
		{
			if (selection == "Custom")
				return (custom == null) ? "" : custom;
			else
				return selection;
		}


		/// <summary>
		/// Sets the five filename format properties of the Project object
		/// </summary>
		public void SetProjectFilenameFormatFields()
		{
            // Reset format fields to defaults
            //this.CurrentFlowProject.ClearFilenameFormatFields();

            this.CurrentFlowProject.ImageFormatSeparatorChar = cmbFieldSeparator.GetTagString();

            if (rdoFilenameOriginal.IsChecked.Value)
            {
                this.CurrentFlowProject.ImageFormatTypeID = ImageFileNameFormatter.IMAGE_FORMAT_TYPE_ORIGINAL;
            }
            else
            {
                this.CurrentFlowProject.ImageFormatTypeID =
                    (rdoFilenamePrefixed.IsChecked.Value)
                        ? ImageFileNameFormatter.IMAGE_FORMAT_TYPE_APPEND
                        : ImageFileNameFormatter.IMAGE_FORMAT_TYPE_REPLACE;

                this.CurrentFlowProject.ImageFormatField1 = (cmbFilenameField1.Text == "Custom")
                    ? txtFilenameCustom1.Text : cmbFilenameField1.GetTagString();

                if (chkFilenameField2.IsChecked.Value)
                {
                    this.CurrentFlowProject.ImageFormatField2 = (cmbFilenameField2.Text == "Custom")
                        ? txtFilenameCustom2.Text : cmbFilenameField2.GetTagString();

                    if (chkFilenameField3.IsChecked.Value)
                    {
                        this.CurrentFlowProject.ImageFormatField3 = (cmbFilenameField3.Text == "Custom")
                            ? txtFilenameCustom3.Text : cmbFilenameField3.GetTagString();
                    }
                    else
                    {
                        this.CurrentFlowProject.ImageFormatField3 = null;
                    }

                }
                else
                {
                    this.CurrentFlowProject.ImageFormatField2 = null;
                }
            }
		}


///
		#region IMAGE TYPE EDITOR
///

		//private void lstImageTypes_SelectionChanged(object sender, SelectionChangedEventArgs e)
		//{
		//    SynchCurrentImageTypeWithList();
		//}

		//private void btnAddImageType_Click(object sender, RoutedEventArgs e)
		//{
		//    _currentImageType = new ImageType();
		//    txtImageType.Text = "";
		//}

		//private void btnSaveImageType_Click(object sender, RoutedEventArgs e)
		//{
		//    _currentImageType.ImageTypeDesc = txtImageType.Text;

		//    if (_currentImageType.ImageTypeID == 0)
		//        this.FlowMasterDataContext.ImageTypeList.Add(_currentImageType);

		//    lstImageTypes.SelectedItem = _currentImageType;
		//}

		//private void btnCancelmageTypeEdit_Click(object sender, RoutedEventArgs e)
		//{
		//    SynchCurrentImageTypeWithList();
		//}

		//private void SynchCurrentImageTypeWithList()
		//{

		//    if (lstImageTypes.SelectedIndex > -1)
		//    {
		//        _currentImageType = lstImageTypes.SelectedItem as ImageType;
		//        txtImageType.Text = _currentImageType.ImageTypeDesc;
		//    }
		//}

///
		#endregion
///


		private void lstImageTypes_Drop(object sender, DragEventArgs e)
		{
			if (e.Data.GetDataPresent(DataFormats.UnicodeText, true))
			{
				ImageType selectedImageType = e.Data.GetData(DataFormats.UnicodeText, true) as ImageType;

				if (selectedImageType != null)
				{
					ProjectImageType newImageType =
						new ProjectImageType(this.CurrentFlowProject.FlowProjectID, selectedImageType);

					this.CurrentFlowProject.ProjectImageTypeList.Add(newImageType);

					e.Effects = ((e.KeyStates & DragDropKeyStates.ControlKey) != 0)
						? DragDropEffects.Copy : DragDropEffects.Move;
				}

				e.Handled = true;
			}
		}

		private void lstCommonTypes_ImageTypeSelectionChanged(object sender, EventArgs<ImageType> e)
		{
			this.FlowMasterDataContext.ImageTypeList.CurrentItem = e.Data;
		}

		private void ImageTypeEditButtonStrip_EditStateChanged(object sender, EditStateChangedEventArgs e)
		{
			switch (e.NewEditState)
			{
				case EditState.View:
					this.ImageTypeEditingEnabled = false;
					this.ConfirmCancelPanelVisibility = Visibility.Collapsed;
					break;
	
				case EditState.Add:
					this.ImageTypeEditingEnabled = true;
					this.FlowMasterDataContext.ImageTypeList.CreateNew();
					break;

				case EditState.Edit:
					this.ImageTypeEditingEnabled = true;
					break;

				case EditState.Delete:
					this.ImageTypeEditingEnabled = false;
					this.ConfirmCancelPanelVisibility = Visibility.Visible;
					break;

				case EditState.Save:
					if (e.OldEditState == EditState.Add)
						this.FlowMasterDataContext.ImageTypeList.CommitNew();
		
					this.ImageTypeEditButtonStrip.EditState = EditState.View;
					break;

				case EditState.Cancel:
					if (e.OldEditState == EditState.Add)
						this.FlowMasterDataContext.ImageTypeList.CancelNew();
					else
						this.FlowMasterDataContext.ImageTypeList.RevertCurrent();


					this.ImageTypeEditButtonStrip.EditState = EditState.View;
					break;
			}
		}

		private void ConfirmDeleteButton_Click(object sender, RoutedEventArgs e)
		{
			this.FlowMasterDataContext.ImageTypeList.DeleteCurrent();
			this.ImageTypeEditButtonStrip.EditState = EditState.View;
		}

		private void CancelDeleteButton_Click(object sender, RoutedEventArgs e)
		{
			this.ImageTypeEditButtonStrip.EditState = EditState.View;
		}

		private void lstImageTypes_ProjectImageTypeRemove(object sender, EventArgs<ProjectImageType> e)
		{
			this.ProjectController.CurrentFlowProject.ProjectImageTypeList.Remove(e.Data);
		}


        internal void RefreshBindings()
        {
            UpdateDefaults();
        }
    }
}