﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using Flow.Config;
using Flow.Controller;
using Flow.Lib;
using Flow.Lib.Log;
using Flow.Properties;
using Flow.Schema;
using StructureMap;
using Flow.Lib.Activation;
using Flow.Controller.Exceptions;
using Flow.Lib.Exceptions;
using Flow.View.Dialogs;
using System.Windows.Threading;
using System.Threading;
using Flow.Lib.Net;
using System.Security.Principal;
using Flow.Controller.Project;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using Flow.Schema.LinqModel.DataContext;
using System.Xml;
using System.Globalization;
using Flow.Lib.Helpers.ImageQuix;
using NetServ.Net.Json;
using Flow.Lib.AsyncWorker;
using System.Diagnostics;
using Flow.Lib.UpdateCheckSync;
using System.Runtime.InteropServices;
using System.Net;
using Flow.Lib.Helpers;
using NLog;
using Flow.Lib.Persister;
using Flow.Lib.FlowError;
using Flow.Controller.Catalog;
using System.Data.SqlServerCe;

namespace Flow
{
    /// <summary>
    /// Interaction logic for AppWindow.xaml
    /// </summary>
    /// 


    public partial class AppWindow : Window
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public IExceptionHandler ExceptionController { get; private set; }

        private NotificationProgressInfo _notificationInfo;
        private NotificationProgressInfo _emailErrorNotificationInfo;
        private NotificationProgressInfo _ProjectTemplateSyncNotificationInfo;
        private NotificationProgressInfo _CatalogSyncNotificationInfo;
        private FlowMessageBox _msg;
        private FlowMessageBox _msg2;
        private FlowMessageBox _msg3;
        private FlowMessageBox _msg4;

        private bool shuttingDown = false;

        private SplashScreen splashScreen;

		public AppWindow()
        {

            // Get Reference to the current Process
            Process thisProc = Process.GetCurrentProcess();
            // Check how many total processes have the same name as the current one
            if (thisProc.ProcessName != "Flow.vshost" && Process.GetProcessesByName(thisProc.ProcessName).Length > 1)
            {
                // If ther is more than one, than it is already running.
                MessageBox.Show("Flow Application is already running.");
                Application.Current.Shutdown();

                return;
            }


            splashScreen = new SplashScreen("images/flow_logo.png");
            //splashScreen.Show(false);

            

            ExceptionController = ObjectFactory.GetInstance<IExceptionHandler>();

            InitializeComponent();
            this.Loaded += new RoutedEventHandler(AppWindow_Loaded);
            //splashScreen.Close(TimeSpan.Zero);
            Hide();
            WindowState = WindowState.Minimized;
            Show();

        }

        public bool finishedCatalogSync = false;
        public bool finishedProjectTemplateSync = false;
        public bool finishedGraphicSync = false;
        void AppWindow_Loaded(object sender, RoutedEventArgs args)
        {

            //Hide();
            
            splashScreen.Show(false);
            
			

			try
            {
                FlowController flow = ObjectFactory.GetInstance<FlowController>();
                
                //string connectionString2 = "Data Source=" + flow.FlowMasterFile + ";Max Database Size=1024;";
                //SqlCeEngine eng = new SqlCeEngine(connectionString2);
                //this.BackupSDF35(eng, flow.FlowMasterFile);
                //eng.Dispose();

               

                logger.Info("About to Copy DBInstall FlowMaster");
                DbVersionManager.CopyNewInstallFlowMaster(flow.FlowMasterFile);
                logger.Info("About to check version of FlowMaster.sdf");
                string connectionString2 = "Data Source=" + flow.FlowMasterFile + ";Max Database Size=1024;";
                SqlCeEngine eng = new SqlCeEngine(connectionString2);
                this.BackupSDF35(eng, flow.FlowMasterFile);
                eng.Dispose();
                logger.Info("Done checking/upgrading FlowMaster.sdf");
                 bool result = flow.Main(AppPanel);
                 if (!result)
                     return;

               

                 // Ensures that the FlowMaster database schema is current
                 string connectionString3 = "Data Source=" + FlowContext.FlowMasterInstallFilePath + ";Max Database Size=1024;";
                 SqlCeEngine eng3 = new SqlCeEngine(connectionString2);
                 this.BackupSDF35(eng3, FlowContext.FlowMasterInstallFilePath);


                 DbVersionManager.SynchFlowMaster(this.AppPanel.FlowController.ProjectController.FlowMasterDataContext, flow.FlowMasterFile);


                 string connectionString = "Data Source=" + FlowContext.FlowProjectTemplateFilePath + ";Max Database Size=1024;";
                 SqlCeEngine enga = new SqlCeEngine(connectionString);
                 this.BackupSDF35(enga, FlowContext.FlowProjectTemplateFilePath);
                 enga.Dispose();

                //if there is no FlowController at this point, app has been closed, so bail
                if (AppPanel.FlowController == null)
                    return;

                AppPanel.FlowController.FlowStartTime = DateTime.Now;
                AppPanel.FlowController.FlowSessionID = TicketGenerator.GenerateTicketString(12);
                NLog.GlobalDiagnosticsContext.Set("SessionID", AppPanel.FlowController.FlowSessionID);
                logger.Info("------------------------------------------------------------");
                logger.Info("Started Flow session ID {0} - version {1}", AppPanel.FlowController.FlowSessionID, FlowContext.GetFlowVersion());
                logger.Info("------------------------------------------------------------");
                flow.Project();

                string LabOrderQueueDir = System.IO.Path.Combine(FlowContext.FlowAppDataDirPath, "LabOrderQueue");
                if (Directory.Exists(LabOrderQueueDir))
                {
                    foreach (string d in Directory.GetDirectories(LabOrderQueueDir))
                    {
                        string tempFile = System.IO.Path.Combine(d, "JobInfoTemp.xml");
                        string nonTempFile = System.IO.Path.Combine(d, "JobInfo.xml");
                        try
                        {
                            if (File.Exists(tempFile))
                                File.Move(tempFile, nonTempFile);
                        }
                        catch (Exception e)
                        {
                            //ignore, no need to error
                        }
                    }

                }

                if (this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.ProjectIQEventIDs != null &&
                    this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.ProjectIQEventIDs.Count > 0)
                {
                    foreach (KeyValuePair<string, string> gal in this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.ProjectIQEventIDs)
                    {
                        if (!this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.FlowProjectGalleries.Any(g => g.EventID == gal.Value))
                        {
                            string customerID = this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.ImageQuixCustomerID;

                            FlowProjectGallery fpg = new FlowProjectGallery();
                            fpg.FlowProjectGuid = new Guid(gal.Key);
                            fpg.EventID = gal.Value;
                            //if (!string.IsNullOrEmpty(fpg.GalleryCode))
                            //    fpg.GalleryURL = "https://vando.imagequix.com/g" + fpg.GalleryCode;
                            //else
                            //{
                            //    fpg.GalleryURL = "https://vando.imagequix.com/gallery.html?id=" + customerID + "&eventid=" + fpg.EventID;
                            //}
                            fpg.DirectShip = false;
                            fpg.WelcomeImage = this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.WelcomeImagePath;
                            fpg.WelcomeText = "Welcome to Online Ordering";
                            this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.FlowProjectGalleries.InsertOnSubmit(fpg);
                        }
                    }

                    //this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.ProjectIQEventIDs = null;
                    this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.SavePreferences();
                }


                if (this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.CompletedOrders != null &&
                    this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.CompletedOrders.Count > 0)
                {
                    foreach (int oldOrderID in this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.CompletedOrders)
                    {
                        if (!this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.PulledIQOrders.Any(p => p.IQOrderID == oldOrderID))
                        {

                            PulledIQOrder pio = new PulledIQOrder();
                            pio.IQOrderID = oldOrderID;
                            this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.PulledIQOrders.InsertOnSubmit(pio);
                        }
                    }

                    this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.CompletedOrders = null;
                    this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.SavePreferences();
                }

                if (this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.IsBasic &&
                    this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.FlowProjectList.Count == 1)
                {
                    this.AppPanel.FlowController.ProjectController.Load(this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.FlowProjectList.First());
                    this.AppPanel.FlowController.ProjectController.OpenCapture();
                }
                //Check for New Network Projects
                //
                //maybe we should delete all network projects from the database and reload them all everytime flow is opened up
                //that would ensure the network projecst are up to date as far as subject count and stuff.

                logger.Info("Checking for new network projects");

                if (!string.IsNullOrEmpty(this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.NetworkProjectsDirectory) &&
                    Directory.Exists(this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.NetworkProjectsDirectory))
                {

                    //delete al local network projects (to pick up any deletions from the server)
                    //if (this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.FlowProjects.Any(p => p.IsNetworkProject == true))
                    //{
                    //    this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.FlowProjects.DeleteAllOnSubmit(
                    //        this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.FlowProjects.Where(p => p.IsNetworkProject == true)
                    //        );
                    //    this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.SubmitChanges();
                    //}
                    //foreach(string file in Directory.GetFiles(this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.ProjectsDirectory))
                    //{
                    //    if (file.EndsWith(".sdf"))
                    //    {
                    //        File.Delete(System.IO.Path.Combine(this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.ProjectsDirectory, file));
                    //    }
                    //}
                    //end delete al local network projects
                    
                    //detach any deleted projects
                    foreach (FlowProject fp in this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.FlowProjects)
                    {
                        if (File.Exists(fp.DatabasePath) && !File.Exists(fp.LocalDBPath))
                        {
                            File.Copy(fp.DatabasePath, fp.LocalDBPath);
                        }

                        fp.FlowMasterDataContext = this.AppPanel.FlowController.ProjectController.FlowMasterDataContext;
                        if (!File.Exists(fp.DatabasePath))
                        {
                            this.AppPanel.FlowController.ProjectController.DeleteProject(fp, "", false, false);
                        }
                    }

                    //logger.Info("Here are the guids currently in the local flowproject table:");
                    //foreach (FlowProject fp in this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.FlowProjects)
                        //logger.Info(fp.FlowProjectGuid.ToString);

                    foreach (DirectoryInfo dir in (new DirectoryInfo(this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.NetworkProjectsDirectory)).GetDirectories())
                    {
                        try
                        {
                           //lets make sure there is an sdf file with the same name as the dir:
                            if (File.Exists(System.IO.Path.Combine(dir.FullName, dir.Name + ".sdf")))
                            {
                                string projectGuid = dir.Name;
                                //logger.Info("looking if network project guid exists in local flowmaster: |" + projectGuid + "|");
                                if (!this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.FlowProjects.Any(fp => fp.FlowProjectGuid == new Guid(projectGuid)))
                                {
                                    //new project!!!
                                    logger.Info("Found a new network project guid that was missing from the local flowmaster: |" + projectGuid + "|");
                                    this.AppPanel.FlowController.ProjectController.AttachNetworkProject(dir);
                                    Thread.Sleep(1000);
                                }
                            }
                        }
                        catch (Exception thisE)
                        {
                            //skip for now
                        }
                    }
                    this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.SubmitChanges();
                    this.AppPanel.ShowMainProjectPanel().ShowViewProjectsPanel().applyFilter("");
                    
                }
                //END Check for New Network Projects
                //
                logger.Info("Done checking for new network projects");


                if (this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.RequireLogin == true)
                {
                    Login login = new Login(this.AppPanel.FlowController.ProjectController.FlowMasterDataContext);
                    if (login.ShowDialog() == false)
                    {
                        shuttingDown = true;
                        this.Close();
                    }

                }

//#if !DEBUG_OFFLINE
                try
                {
                    
                    string pingTestURL = this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.PingTestURL;
                    if (!this.AppPanel.FlowController.IsDayPass || Emailer.CheckForInternetConnection() || Emailer.PingServer(pingTestURL))
                    {

                        //Check if any unsent errors can be sent now
                        _emailErrorNotificationInfo = new NotificationProgressInfo("Unsent Error", "Checking unsent errors...");
                        logger.Info("Checking unsent errors...");
                        UnsentError_Worker();

                        
                        _ProjectTemplateSyncNotificationInfo = new NotificationProgressInfo("Sync Project Templates", "Checking for new Project Templates...");
                        logger.Info("Checking for new Project Templates...");
                        SyncUpdateCheck();
                        finishedProjectTemplateSync = true;
                        this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.SavePreferences();

                        CheckForBasic();

                        if (this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.IsNotBasic)
                        {
                            //Check if any remote syncs are available (like images or graphics on a remote ftp server) 
                            _notificationInfo = new NotificationProgressInfo("Remote Sync", "Checking for updated files...");
                            logger.Info("Checking for updated files...");
                            FlowBackgroundWorkerManager.RunWorker(Sync_Worker, GraphicAndLayougSync_Completed);


                            //Check for updated PLIC Catalog
                            if (this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.CatalogRequest.ImageQuixBaseUrl.Contains("plic.io"))
                            {
                                FlowCatalogController fcc = new FlowCatalogController();
                                fcc.FlowController = this.AppPanel.FlowController;

                                PlicCatalogController controller = new PlicCatalogController(this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.CatalogRequest.ImageQuixBaseUrl, fcc)        ;
                                //controller.forceUpdate = true;
                                //controller.ShowNotification = false;
                                controller.ProcessPlicCatalog();

                            }

                            //Check if any ImageQuix catalog updates are available 
                            //need to call this to initialize the ImageQuixCatalog list
                            try
                            {
                                FlowObservableCollection<ImageQuixCatalog> iqc = flow.ProjectController.FlowMasterDataContext.ImageQuixCatalogList;
                                _CatalogSyncNotificationInfo = new NotificationProgressInfo("Catalog Sync", "Updating Catalog...");
                                logger.Info("Updating Catalog...");
                                FlowBackgroundWorker worker = FlowBackgroundWorkerManager.RunWorker(CatalogSync, CatalogSync_Completed);
                            }
                            catch (Exception ex)
                            {
                                logger.Info("Non Critical Catalog Sync Error: " + ex.Message);
                                //no reall harm if this fails, no need to throw an error
                            }
                            //CatalogSync();

                        }
                    }
                    else
                    {


                        //init this if we are not going to sync backgrounds (otherwise, init after background sync is complete)
                        CheckForBasic();
                        flow.ProjectController.FlowMasterDataContext.initGreenScreenBackgroundList();
                        flow.StartUploader();
                    }
                }
                catch (WebException e)
                {
                    CheckForBasic();
                    logger.Warn("Syncing failed because Flow was started offline.");
                }
//#endif
            }
            catch (Exception e)
            {
				if (Settings.Default.ExceptionContext == "DEBUG")
					throw;
				else
				{
					MessageBox.Show("Error: " + e.ToString());
                    logger.ErrorException("Exception occured.", e);
				}
                
            }

            if (!shuttingDown)
            {
                
                double w = this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.Width;
                double h = this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.Height;
                WindowState ws = this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.DefaultWindowState;
                if (w > 0 && h > 0)
                {
                    if (ws != null)
                        WindowState = ws;
                    else
                        WindowState = WindowState.Normal;

                    this.Width = w;
                    this.Height = h;

                }
                else
                {
                    WindowState = WindowState.Maximized;
                }
                Show();
                splashScreen.Close(TimeSpan.Zero);
            }
        }

        private void CheckForBasic()
        {
            if (this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.IsBasic)
            {
                this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Capture.CaptureTakePicture = true;
                this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Permissions.Permission_CanViewCatalogs = false;
                this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Permissions.Permission_CanViewReports = false;
                this.AppPanel.uxEdit.Visibility = Visibility.Collapsed;
                this.AppPanel.projectButtons.Visibility = Visibility.Collapsed;
                this.AppPanel.btnImportData.Visibility = Visibility.Visible;
                this.AppPanel.uxPreferences.Visibility = Visibility.Visible;
                this.AppPanel.FlowController.ProjectController.MainProjectPanel.new_project_template.Visibility = Visibility.Collapsed;
                this.AppPanel.FlowController.ProjectController.MainProjectPanel.view_project_template.Visibility = Visibility.Collapsed;
                //this.AppPanel.FlowController.ProjectController.MainProjectPanel.onlineOrders.Visibility = Visibility.Collapsed;
                this.AppPanel.FlowController.ProjectController.MainProjectPanel.LabUploadStatusPanel.Visibility = Visibility.Collapsed;
                this.AppPanel.FlowController.ProjectController.MainProjectPanel.FlowChangesWebPage.Visibility = Visibility.Collapsed;

            }
            else
            {
                this.AppPanel.btnImportData.Visibility = Visibility.Collapsed;
                this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Permissions.Permission_CanViewCatalogs = true;
                this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Permissions.Permission_CanViewReports = true;
            }
        }

        void CatalogSync_Completed(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                if (this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogList.Count > 0)
                {
                    //this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.SubmitChanges();
                    this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogList.CurrentItem.RefreshProductList();
                    //Thread.Sleep(1000);

                    foreach (ImageQuixProductGroup pg in this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogList.CurrentItem.ProductGroupList)
                    {
                        pg.RefreshProductList();
                    }
                    this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.ImageQuixCatalogList.CurrentItem.RefreshProductList();

                }
                //this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.SubmitChanges();
            }
            catch (Exception ex)
            {
                logger.Info("Non Critical Catalog Sync Error: " + ex.Message);
                //no reall harm if this fails, no need to throw an error
            }
            logger.Info("DONE with catalog sync");
            finishedCatalogSync = true;
            if (finishedGraphicSync && finishedCatalogSync && finishedProjectTemplateSync) this.AppPanel.FlowController.StartUploader();

            this.IsEnabled = true;
        }

        void GraphicAndLayougSync_Completed(object sender, RunWorkerCompletedEventArgs e)
        {
            this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.initGreenScreenBackgroundList();
            logger.Info("DONE with graphics and layouts sync");
            finishedGraphicSync = true;
            if (finishedGraphicSync && finishedCatalogSync && finishedProjectTemplateSync) this.AppPanel.FlowController.StartUploader();
            
        }

        void Sync_Worker(object sender, DoWorkEventArgs e)
        //void Sync_Worker()
        {
            Thread.Sleep(3000);//wait 3 seconds for the main app to load before we start checking for graphic and layout updates

            try
            {
                RemoteSync RemoteSync = new RemoteSync(this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager, _notificationInfo);
                RemoteSync.Begin();
                string[] newLayoutFiles;
                string[] newGraphicFiles;
                string[] newOverlayFiles;
                string[] newBackgroundFiles;
                string[] newPremiumBackgroundFiles;
                string[] newReportFiles;
                string[] newCustomSettingsFiles;
                string[] newHelpFiles;

                //this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.CanGreenScreen = true;
                if (RemoteSync.CheckFtpServer(out newLayoutFiles, out newGraphicFiles, out newOverlayFiles, out newBackgroundFiles, out newPremiumBackgroundFiles, out newReportFiles, out newCustomSettingsFiles, out newHelpFiles, AppPanel.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.CanGreenScreen))
                {

                    bool result = false;
                    this.Dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                    {
                        _msg = new FlowMessageBox("Layouts and Graphics", "New Layouts and Graphics are available, Click okay to download now");

                        if (_msg.ShowDialog() == true)
                            result = true;
                    }));

                    if (result == false)
                        return;

                    RemoteSync.DoUpdates(newLayoutFiles, newGraphicFiles, newOverlayFiles, newBackgroundFiles, newPremiumBackgroundFiles, newReportFiles, newCustomSettingsFiles, newHelpFiles);
                    this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.GreenScreenBackgroundsRefresh();

                    if (newCustomSettingsFiles != null && newCustomSettingsFiles.Count() > 0)
                        this.AppPanel.FlowController.ProjectController.LoadCustomSettings();
                }

            }
            catch (Exception ex)
            {
                this.Dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                   {
                       FlowMessageBox msg = new FlowMessageBox("Error Connecting to FTP", "There was an error connecting to the ftp server (getting layouts and graphics).\n\n Please check your ftp settings.\n\n" + ex.Message);
                       msg.CancelButtonVisible = false;
                       msg.ShowDialog();
                       logger.ErrorException("There was an error connecting to the FTP server (getting layouts and graphics).", ex);
                   }));
                //throw ex;
            }
            return;
        }

        public string ReplaceLastOccurrence(string Source, string Find, string Replace)
        {
            if (string.IsNullOrEmpty(Source) || string.IsNullOrEmpty(Find) || string.IsNullOrEmpty(Replace))
                return Source;

            if (Source.Length <= Find.Length)
                return Source;
            int Place = Source.LastIndexOf(Find);
            if (Place >= 0)
            {
                string result = Source.Remove(Place, Find.Length).Insert(Place, Replace);
                return result;
            }
            return Source;
        }

        void CatalogSync(object sender, DoWorkEventArgs e)
        //void CatalogSync()
        {
            Thread.Sleep(3000);//wait 3 seconds for the main app to load before we start checking for catalog updates
            FlowMasterDataContext fmdc = this.AppPanel.FlowController.ProjectController.FlowMasterDataContext;

            if (fmdc.PreferenceManager.Upload.CatalogRequest.ImageQuixBaseUrl.Contains(".pud") || fmdc.PreferenceManager.Upload.CatalogRequest.ImageQuixBaseUrl.Contains("plic.io"))
            {
                //this is a PUD, so treat it as such


                return;
            }
            if (fmdc.ImageQuixCatalogList.Count == 0)
            {
                if (fmdc.RefreshRemoteImageQuixCatalogList())
                {

                    if (fmdc.RemoteImageQuixCatalogListDeletes.Count > 0)
                    {

                        foreach (Flow.Lib.Helpers.ImageQuix.ImageQuixCatalogListing listing in fmdc.RemoteImageQuixCatalogListDeletes)
                        {
                            int badFlowCatalogCount = 0;
                            foreach (FlowCatalog flowCatalog in fmdc.FlowCatalogList)
                            {
                                if (flowCatalog.ImageQuixCatalogID == listing.PrimaryKey)
                                {
                                    badFlowCatalogCount++;
                                    //there is a catalog associated with this lab catalog
                                    FlowMessageBox msg = new FlowMessageBox("Deleted Lab Products", "You have a catalog that is referencing a deleted lab catalog.\nYou must redo your catalog.\n" + listing.Label);
                                    msg.CancelButtonVisible = false;
                                    msg.ShowDialog();

                                }
                            }
                            if (badFlowCatalogCount == 0)
                            {
                                ImageQuixCatalog catalog = fmdc.ImageQuixCatalogList.FirstOrDefault(iqcat => iqcat.PrimaryKey == listing.PrimaryKey);
                                if (catalog != null)
                                {
                                    catalog.PrepareForDeletion(fmdc);
                                    fmdc.ImageQuixCatalogList.Remove(catalog);
                                    fmdc.ImageQuixCatalogs.DeleteOnSubmit(catalog);
                                    fmdc.SubmitChanges();
                                }

                                
                            }
                        }
                    }


                    //need to call this to initialize the ImageQuixCatalog list
                    FlowObservableCollection<ImageQuixCatalog> iqc = fmdc.ImageQuixCatalogList;
                    if (fmdc.RemoteImageQuixCatalogList.Count > 0)
                    {
                        bool result = false;
                        this.Dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                        {
                            _msg4 = new FlowMessageBox("New Product Catalog", "There is a Lab Catalog available to download: " + fmdc.RemoteImageQuixCatalogList[0].Label + " \nWould you like to download the catalog now?");

                            if (_msg4.ShowDialog() == true)
                            {
                                this.IsEnabled = false;
                                result = true;
                            }
                        }));

                        if (result == false)
                            return;

                        IShowsNotificationProgress progressNotification = ObjectFactory.GetInstance<IShowsNotificationProgress>();
                        _CatalogSyncNotificationInfo.NotificationProgress = progressNotification;
                        progressNotification.ShowNotification(_CatalogSyncNotificationInfo);


                        fmdc.ImportMarkedRemoteImageQuixCatalogs(_CatalogSyncNotificationInfo, this.Dispatcher);
                        fmdc.ImageQuixCatalogList.CurrentItem.RefreshProductList();
                        
                        _CatalogSyncNotificationInfo.Complete("Finished with catalog sync");
                        
                    }
                }


                


                return;
            }


            foreach(ImageQuixCatalog thisCatalog in fmdc.ImageQuixCatalogList)
            {
                thisCatalog.ResourceURL = ReplaceLastOccurrence(thisCatalog.ResourceURL, ":8080/imagequix", "/catalog");

                string uname = fmdc.PreferenceManager.Upload.CatalogRequest.GetCatalogUsername;
                string pword = fmdc.PreferenceManager.Upload.CatalogRequest.GetCatalogPassword;
                string catalogURL = fmdc.PreferenceManager.Upload.CatalogRequest.ImageQuixCatalogUrl;
                //catalogURL = catalogURL.Replace("catalog", "catalogdump");

                
                string caturl = ReplaceLastOccurrence( thisCatalog.ResourceURL, "catalog", "catalogdump");
 


                if (catalogURL.Contains("demo."))
                    caturl = caturl.Replace("api.", "demo.");

                JsonObject catalogObject = new JsonObject();
                try{
                    catalogObject = ImageQuixUtility.GetImageQuixResourceFromJsonServer(caturl, uname, pword);
                }
                catch (Exception ex)
                {
                    //this.Dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                    //{
                    //    _msg4 = new FlowMessageBox("Updated Product Catalog", "FAILED to find the status for Catalog: " + thisCatalog.Label + " \nThis Catalog may have been deleted?");
                    //    _msg4.CancelButtonVisible = false;
                    //    _msg4.ShowDialog();
                    //}));
                    return;
                }

                string modDate = "1/1/2000";//default a consistant old modDate

                if (catalogObject.Keys.Contains("lastModified") && catalogObject["lastModified"].GetType() != typeof(JsonNull))
                    modDate = ((JsonString)catalogObject["lastModified"]).Value;

                if (thisCatalog.ModifyDate < Convert.ToDateTime(modDate))
                {
                    
                    bool result = false;
                    this.Dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                    {
                        _msg4 = new FlowMessageBox("Updated Product Catalog", "Updates are available for Catalog: " + thisCatalog.Label + " \nWould you like to update the catalog now?");

                        if (_msg4.ShowDialog() == true)
                        {
                            this.IsEnabled = false;
                            result = true;
                        }
                    }));

                    if (result == false)
                        return;

                    IShowsNotificationProgress progressNotification = ObjectFactory.GetInstance<IShowsNotificationProgress>();
                    _CatalogSyncNotificationInfo.NotificationProgress = progressNotification;
                    progressNotification.ShowNotification(_CatalogSyncNotificationInfo);

                    try
                    {
                        JsonArray catalogDef = ImageQuixUtility.GetImageQuixCatalogFromJsonServer(
                       catalogURL,
                       thisCatalog.PrimaryKey, uname, pword
                       );

                        //need to pull the list of catalogs first, and then match the catalog we want to the one in the list so that we get  OptionGroups
                        List<ImageQuixCatalogListing> cats = ImageQuixUtility.GetImageQuixCatalogListFromJsonServer(fmdc.ImageQuixCatalogServerUrl, uname, pword).ToList();
                        //ImageQuixCatalogListing item = new ImageQuixCatalogListing(thisCatalog.Label, thisCatalog.PrimaryKey, modDate, thisCatalog.ResourceURL, "",  null);
                        ImageQuixCatalogListing item = cats.FirstOrDefault(c => c.ResourceURL == thisCatalog.ResourceURL);
                        
                        //JsonArray catalogs = new JsonArray();
                        //catalogs.Add(catalogObject);

                       
                        string CustomerID = "Not Found";
                        try{
                            CustomerID = (fmdc.OrgList.Where(o=>o.OrganizationTypeID == 4).FirstOrDefault()).LabCustomerID;
                        }
                        catch
                        {
                            //do nothing for now
                        }

                        ImageQuixCatalog.CreateImageQuixCatalog(item, catalogDef, fmdc, _CatalogSyncNotificationInfo, this.Dispatcher, CustomerID);

                        foreach (FlowCatalog fc in fmdc.FlowCatalogs)
                        {
                            fc.InitCatalogOptions(fmdc, fc.ImageQuixCatalog);
                            fc.RefreshFlowCatalogOptions();
                        }
                    }
                    catch (Exception ex)
                    {

                        throw ex;
                    }


                    _CatalogSyncNotificationInfo.Complete("Finished with catalog sync");
                    
                   
                }
 
            }

        }


        void UnsentError_Worker()
        //void Sync_Worker()
        {
            
            DataTable errors = this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.UnsentErrors;
            if (errors != null && errors.Rows.Count > 0)
            {
                //check if the server is accessable

                _msg2 = new FlowMessageBox("Unsent Errors", "Errors occured while you were off line. \nNow that you have an internet connection, do you want to send in those errors?");

                if (_msg2.ShowDialog() == false)
                    return;

                foreach (DataRow error in errors.Rows)
                {
                    string ssPath = "";
                    string logFile = "";

                    ProjectEmailer mailer = new ProjectEmailer((string)error["subject"], (string)error["body"]);
                    if (error["screenShot"] != null && error["screenShot"].GetType() != typeof(System.DBNull))
                    {
                        ssPath = (string)error["screenShot"];
                        if(File.Exists(ssPath))
                            mailer.AddAttachment(ssPath);
                    }
                    if (error["logFile"] != null && error["logFile"].GetType() != typeof(System.DBNull))
                    {
                        logFile = (string)error["logFile"];
                        if (File.Exists(logFile))
                            mailer.AddAttachment(logFile);
                    }
                    mailer.Send();
                    mailer.ProgressInfo.Complete("Finished Sending Email");

                    if (error["ErrorStore"] != null && error["ErrorStore"].GetType() != typeof(System.DBNull))
                    {
                        string xmlStore = (string)error["ErrorStore"];
                        WebServiceFlowErrorMessage xmlResults =
                        ObjectSerializer.FromXmlString<WebServiceFlowErrorMessage>(
                        xmlStore);
                        FlowErrorPost FEPost = new FlowErrorPost();
                        string returnMsg = FEPost.Post(xmlResults.FlowErrorStore);
                        FEPost.SendFile(returnMsg, ssPath, xmlResults.FlowErrorStore.ErrorID.ToString());
                        FEPost.SendFile(returnMsg, logFile, xmlResults.FlowErrorStore.ErrorID.ToString());
                    }

                }

                logger.Info("Sent {0} previously unsent error reports.", errors.Rows.Count);

                this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.UnsentErrors.Rows.Clear();
                this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.SavePreferences();
            }
        }

        public bool SyncUpdateCheck()
        {
           
            //if its activated, return true
            ActivationValidator activation = new ActivationValidator(this.AppPanel.FlowController.ActivationKeyFile);
            ActivationStore activationStore = activation.Repository.Load();
            string activationKey = activationStore.ActivationKey;
            string computerId = activationStore.ComputerId;

            

            UpdateCheckStore store = new UpdateCheckStore();
            store.ActivationKey = activationKey.Trim();
            store.ComputerId = computerId.Trim();
            store.FingerPrint = FingerPrint.Value();

            //need to get rid of the 4th number which is the svn version
            string[] vSplit = FlowContext.GetFlowVersion().Split('.');
            store.ClientFlowVersion = vSplit[0] + "." + vSplit[1] + "." + vSplit[2];
            if(is64BitOperatingSystem)
                store.ClientFlowType = "x64";
            else
                store.ClientFlowType = "x86";


            //store.clientProjectTemplates = new List<string[]>();
            //foreach (ProjectTemplate thisTemplate in this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.ProjectTemplateList)
            //{
            //    //store.clientProjectTemplates.Add(new string[] { thisTemplate.ProjectTemplateDesc, thisTemplate.DateCreated.ToString(), thisTemplate.DateModified.ToString() });
            //    store.clientProjectTemplates.Add(new string[] { thisTemplate.ProjectTemplateDesc, thisTemplate.DateCreated.GetDateTimeFormats('G', new CultureInfo("en-US", false))[0]});
            //}

            
            UpdateCheckSync PTSync = new UpdateCheckSync();
            UpdateCheckStore returnStore = PTSync.Sync(store);

            this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.RemoteImageServices = new List<Lib.Preferences.RemoteImageService>();                
            foreach (Flow.Lib.UpdateCheckSync.RemoteImageService ris in returnStore.RemoteImageServices)
            {
                this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.RemoteImageServices.Add(new Lib.Preferences.RemoteImageService(ris.Name, ris.Ftp, ris.Port));
            }

            //this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.RemoteImageServices.Add(new Lib.Preferences.RemoteImageService("Photo Retouch Online", "113.108.248.97", "21"));
            this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.SavePreferences();

            if ((returnStore.ActivationKeyExpireDate < DateTime.Now) || (returnStore.ActivationKeyIsExpired == true))
            {
                this.Dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                {
                    shuttingDown = true;
                    FlowMessageBox msg = new FlowMessageBox("Activation Expired", "Your Flow Activation is no longer valid.\n\nPlease restart flow and enter a valid activation key");
                    msg.CancelButtonVisible = false;
                    msg.ShowDialog();
                    this.Close();
                }));
                
            }

            if (returnStore.NewFlowInstaller != null && returnStore.NewFlowInstaller.Length > 1)
            {
                bool forceUpdate = false;
                if (returnStore.NewFlowInstaller.ToLower().Contains("_f_"))
                    forceUpdate = true;
                FlowMessageBox msgNewInstaller = null;
                if (forceUpdate)
                {
                    msgNewInstaller = new FlowMessageBox("New Installer", "A new version of Flow is available!\nThis is a required update, you must update for flow to work correclty.\n\nWould you like to download and install the newest version of Flow now?");
               
                }
                else
                {
                    msgNewInstaller = new FlowMessageBox("New Installer", "A new version of Flow is available!\nTo get the best performance and newest features, it is highly recommended that you update.\n\nWould you like to download and install the newest version of Flow now?");
                
                }
                 msgNewInstaller.buttonOkay.Content = "Yes";
                msgNewInstaller.buttonCancel.Content = "No";
                if ((bool)msgNewInstaller.ShowDialog())
                {
                    this.AppPanel.IsEnabled = false;
                    string remoteFile = "http://www.flowadmin.com/installers/" + returnStore.NewFlowInstaller.Replace(" ", "%20");
                    string localFile = System.IO.Path.Combine(FlowContext.FlowTempDirPath, returnStore.NewFlowInstaller);

                    NotificationProgressInfo ProgressInfo = new NotificationProgressInfo("Downloading", "Downloading New Installer...");
                    IShowsNotificationProgress progressNotification = ObjectFactory.GetInstance<IShowsNotificationProgress>();
                    ProgressInfo.NotificationProgress = progressNotification;
                    progressNotification.ShowNotification(ProgressInfo);
                    ProgressInfo.NotificationProgress = progressNotification;

                    ProgressInfo.Message = "Downloading New Installer...";
                    ProgressInfo.Update();

                    WebClient webClient = new WebClient();
                    webClient.DownloadProgressChanged += new DownloadProgressChangedEventHandler(
                        delegate(object sender, DownloadProgressChangedEventArgs e)
                        {
                            ProgressInfo.Progress = e.ProgressPercentage;
                            ProgressInfo.Message = "Downloading New Installer... " + e.ProgressPercentage + "%  -- Flow will restart when done";
                            ProgressInfo.Update();
                        });
                    webClient.DownloadFileCompleted += new AsyncCompletedEventHandler(
                        delegate
                        {
                            this.AppPanel.IsEnabled = true;
                            ProgressInfo.Complete("Closing Flow and Launching Installer");
                            System.Diagnostics.Process.Start(@localFile);
                            this.Close();
                        });

                    webClient.DownloadFileAsync(new Uri(remoteFile), @localFile);



                }
                else
                {
                    if (forceUpdate)
                    {
                        shuttingDown = true;
                        this.Close();
                    }
                }
            }

            //load the current Order Form Fields
            List<DataSet> dsOrderForms = returnStore.OrderForms;
            if (dsOrderForms != null)
            {
                this.AppPanel.FlowController.LoadOrderFormFields(dsOrderForms);
            }

            if (!this.AppPanel.FlowController.JustActivated)
            {
                DataSet ds = returnStore.ConfigData;
                if (ds != null)
                {
                    this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Load(ds, false);
                    this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.SavePreferences();
                }

               

                //check for update expire date
                activationStore.ActivatedEnd = returnStore.ActivationKeyExpireDate;
                activation.Repository.Save(activationStore);
            }

            this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.CanGreenScreen = returnStore.CanGreenScreen;
            this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.IsFlowServer = returnStore.IsServerKey;
            this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.IsFlowMaster = returnStore.IsMasterKey;
            this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.IsBasic = returnStore.IsBasic;
            this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.EnableRemoteImageService = !returnStore.DisableRemoteImageService;

            if (string.IsNullOrEmpty(this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.EcommerceAPIURL) &&
                !string.IsNullOrEmpty(returnStore.EcommereAPIUrl))
                this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.EcommerceAPIURL = returnStore.EcommereAPIUrl;

            if (string.IsNullOrEmpty(this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.EcommerceAPIKey) &&
                !string.IsNullOrEmpty(returnStore.EcommereAPIKey))
                this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.EcommerceAPIKey = returnStore.EcommereAPIKey;

            if (this.AppPanel.FlowController.ForceFlowMaster)
                this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.IsFlowMaster = true;
            if (this.AppPanel.FlowController.ForceFlowServer)
                this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.IsFlowServer = true;

        if (returnStore.xmlFiles.Count() > 0)
            {
                bool needsUpdate = false;
                if (returnStore.xmlFiles.Count() != this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.ProjectTemplateList.Count())
                {
                    //either a project template as been added, or deleted on the server
                    needsUpdate = true;
                }
                else
                {
                    //are these new or modified project templates?
                    foreach (XmlDocument xmlDoc in returnStore.xmlFiles)
                    {
                        XmlNode rootNode = xmlDoc.DocumentElement;
                         foreach (XmlNode tableNode in rootNode.ChildNodes)
                         {
                             if (tableNode.Name.Equals("ProjectTemplate"))
                             {
                                 
                                 DateTime ModifyDate = DateTime.Today.AddYears(-15);
                                 DateTime origModifyDateInit = ModifyDate;
                                 Guid PTGuid = new Guid();
                                 foreach (XmlNode childNode in tableNode.ChildNodes)
                                 {
                                     
                                     if (childNode.Name.Equals("DateModified") && childNode.InnerText.Length > 0)
                                         ModifyDate = DateTime.ParseExact(childNode.InnerText, "G", new CultureInfo("en-US", false));
                                     if (childNode.Name.Equals("ProjectTemplateGuid") && childNode.InnerText.Length > 0) PTGuid = new Guid(childNode.InnerText);
                                    
                                 }

                                 bool foundMatch = false;
                                 foreach (ProjectTemplate pt in this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.ProjectTemplateList)
                                 {
                                     if (pt.ProjectTemplateGuid == PTGuid)
                                     {
                                         foundMatch = true;
                                         if (DateTime.Compare((DateTime)pt.DateModified, ModifyDate) < 0)
                                         {
                                             if (pt.DateModified == null && ModifyDate == origModifyDateInit)
                                             {
                                                 //ModifyDate is null, no need to update
                                             }
                                             else
                                             {
                                                 needsUpdate = true;
                                             }
                                         }
                                     }
                                 }
                                 if (!foundMatch)
                                     needsUpdate = true;
                             }
                         }

                    }
                }
                if (needsUpdate)
                {
                    _msg3 = new FlowMessageBox(" New Project Templates Available", "New Project Templates are available and ready to sync to your computer.");

                    if (_msg3.ShowDialog() == false)
                        return false;
                }
                else
                    return false;
            }
            else
            {
                return false;
            }
            //if we made it here, than there are updates to the project templates, so lets just delete them all and reload them


            this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.ProjectTemplateSubjectFields.DeleteAllOnSubmit(this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.ProjectTemplateSubjectFields);
            this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.ProjectTemplateSubjectTypes.DeleteAllOnSubmit(this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.ProjectTemplateSubjectTypes);
            this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.ProjectTemplateUnitScopes.DeleteAllOnSubmit(this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.ProjectTemplateUnitScopes);
            this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.ProjectTemplateEventTriggers.DeleteAllOnSubmit(this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.ProjectTemplateEventTriggers);
            this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.ProjectTemplateImageTypes.DeleteAllOnSubmit(this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.ProjectTemplateImageTypes);
            this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.ProjectTemplateOrganizationalUnitTypes.DeleteAllOnSubmit(this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.ProjectTemplateOrganizationalUnitTypes);
            this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.ProjectTemplates.DeleteAllOnSubmit(this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.ProjectTemplates);
            this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.RefreshProjectTemplateList();
            this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.SubmitChanges();
            ProjectTemplateSyncController.ProcessProjectTemplate(returnStore, _ProjectTemplateSyncNotificationInfo, this.AppPanel.FlowController.ProjectController.FlowMasterDataContext);
            return true;
            
        }

		private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
            if (this.AppPanel.FlowController!= null && this.AppPanel.FlowController.ProjectController.IsUploading)
            {
                MessageBoxResult result = MessageBox.Show("You are currently uploading to the server, are you sure you want to close Flow?", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result == MessageBoxResult.No)
                {
                    e.Cancel = true;
                }
            }

            if (this.AppPanel.FlowController != null && this.AppPanel.FlowController.CaptureController != null && this.AppPanel.FlowController.CaptureController.CaptureDockPanel != null && this.AppPanel.FlowController.CaptureController.SaveLayoutOnClose)
            {
                string layoutFile = System.IO.Path.Combine(FlowContext.FlowConfigDirPath, "DockSettings.xml");
                this.AppPanel.FlowController.CaptureController.CaptureDockPanel.DockMgr.SaveLayout(layoutFile);
            }

            if (this.AppPanel.FlowController != null)
            {
                this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.Width = this.Width;
                this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.Height = this.Height;
                this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.DefaultWindowState = WindowState;
                this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.SavePreferences();
            }

            logger.Info("=================");
            logger.Info("Flow is closing");
            logger.Info("=================");

            if (this.AppPanel.FlowController != null)
            {
                if (this.AppPanel.FlowController.ProjectController.CurrentFlowProject != null &&
                    this.AppPanel.FlowController.ProjectController.CurrentFlowProject.FlowProjectDataContext.UpdateNetworkProject)
                {
                    this.AppPanel.FlowController.ProjectController.CurrentFlowProject.FlowProjectDataContext.UpdateNetworkProject = false;
                    this.AppPanel.FlowController.ProjectController.UpdateNetworkParent();
                }

                if (this.AppPanel.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.IsFlowServer && this.AppPanel.FlowController.ProjectController.CurrentFlowProject != null)
                    this.AppPanel.FlowController.ProjectController.CurrentFlowProject.PrepareForExport();
            }
		}




        static bool is64BitProcess = (IntPtr.Size == 8);
        static bool is64BitOperatingSystem = is64BitProcess || InternalCheckIsWow64();

        [DllImport("kernel32.dll", SetLastError = true, CallingConvention = CallingConvention.Winapi)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool IsWow64Process(
            [In] IntPtr hProcess,
            [Out] out bool wow64Process
        );


        public void BackupSDF35(System.Data.SqlServerCe.SqlCeEngine engine, string dbPath)
        {
            SQLCEVersion fileversion = DetermineVersion(dbPath);
            if (fileversion < SQLCEVersion.SQLCE40)
            {
                string BackupFolder = System.IO.Path.Combine(FlowContext.FlowAppDataDirPath, "BackupSDF35");
                if (!Directory.Exists(BackupFolder))
                    Directory.CreateDirectory(BackupFolder);

                string destFile = System.IO.Path.Combine(BackupFolder, (new FileInfo(dbPath)).Name);
                if (!File.Exists(destFile))
                    File.Copy(dbPath, destFile);
                try
                {
                    engine.Upgrade();
                }
                catch
                {
                    //do nothing
                }
            }
        }

        private enum SQLCEVersion
        {
            SQLCE20 = 0,
            SQLCE30 = 1,
            SQLCE35 = 2,
            SQLCE40 = 3
        }

        private static SQLCEVersion DetermineVersion(string filename)
        {
            var versionDictionary = new Dictionary<int, SQLCEVersion> 
        { 
            { 0x73616261, SQLCEVersion.SQLCE20 }, 
            { 0x002dd714, SQLCEVersion.SQLCE30},
            { 0x00357b9d, SQLCEVersion.SQLCE35},
            { 0x003d0900, SQLCEVersion.SQLCE40}
        };
            int versionLONGWORD = 0;
            try
            {
                using (var fs = new FileStream(filename, FileMode.Open))
                {
                    fs.Seek(16, SeekOrigin.Begin);
                    using (BinaryReader reader = new BinaryReader(fs))
                    {
                        versionLONGWORD = reader.ReadInt32();
                    }
                }
            }
            catch
            {
                throw;
            }
            if (versionDictionary.ContainsKey(versionLONGWORD))
            {
                return versionDictionary[versionLONGWORD];
            }
            else
            {
                throw new ApplicationException("Unable to determine database file version");
            }
        }

        public static bool InternalCheckIsWow64()
        {
            if ((Environment.OSVersion.Version.Major == 5 && Environment.OSVersion.Version.Minor >= 1) ||
                Environment.OSVersion.Version.Major >= 6)
            {
                using (Process p = Process.GetCurrentProcess())
                {
                    bool retVal;
                    if (!IsWow64Process(p.Handle, out retVal))
                    {
                        return false;
                    }
                    return retVal;
                }
            }
            else
            {
                return false;
            }
        }

    }
}
