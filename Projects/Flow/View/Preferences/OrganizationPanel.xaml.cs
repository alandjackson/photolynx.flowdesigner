﻿using System;
using System.Windows;
using System.Windows.Controls;

using Flow.Schema.LinqModel.DataContext;
using Flow.View.Project.ProjectTemplate.Base;
using Flow.View.Dialogs;
using Flow.Controller.Preferences;
using Flow.Lib.Helpers;
using Flow.Controls.View.RecordDisplay;
using Flow.Controller;
using NLog;

namespace Flow.View.Preferences
{
	/// <summary>
	/// Interaction logic for UserPanel.xaml
	/// </summary>
	public partial class OrganizationPanel : ProjectTemplatePanelBase
	{

        public PreferencesController PreferencesController { get; set; }

        public event EventHandler<EventArgs<User>> AssignItemInvoked = delegate { };
		public event EventHandler<EventArgs<User>> DeleteItemInvoked = delegate { };

		protected DependencyProperty EditFieldPanelEnabledProperty = DependencyProperty.Register(
			"EditFieldPanelEnabled", typeof(bool), typeof(OrganizationPanel)
		);

		protected bool EditFieldPanelEnabled
		{
			get { return (bool)this.GetValue(EditFieldPanelEnabledProperty); }
			set { this.SetValue(EditFieldPanelEnabledProperty, value); }
		}

        private Logger logger = LogManager.GetCurrentClassLogger();

        private FlowObservableCollection<Organization> OrgList { get; set; }

        public FlowController FlowController { get; set; }

        public OrganizationPanel()
		{
            InitializeComponent();          
		}

        public void InitButtonStrip()
        {
            OrgList = ((FlowMasterDataContext)DataContext).OrgList;
            this.OrganizationOptionsEditButtonStrip.SetSourceCollection<Organization>(
               this.OrgList
           );

            this.OrganizationOptionsEditButtonStrip.EditStateChanged +=
                new EventHandler<EditStateChangedEventArgs>(CollectionEditButtonStrip_EditStateChanged);

            this.OrganizationOptionsEditButtonStrip.EditState = EditState.View;
        }

        private void cmbOrganization_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.OrgList != null && this.OrgList.CurrentItem != null)
                logger.Info("Organization selection changed: name='{0}' id='{1}'", this.OrgList.CurrentItem.OrganizationName, this.OrgList.CurrentItem.OrganizationID);
        }


		private void ProductRecordDisplay_AssignItemInvoked(object sender, EventArgs<User> e)
		{
			this.AssignItemInvoked(sender, e);
		}

		private void ProductCatalogItemDisplay_DeleteItemInvoked(object sender, EventArgs<User> e)
		{
			this.DeleteItemInvoked(sender, e);
		}

        void CollectionEditButtonStrip_EditStateChanged(object sender, Flow.Controls.View.RecordDisplay.EditStateChangedEventArgs e)
        {
            logger.Info("Organization edit state changed from {0} to {1}", e.OldEditState, e.NewEditState);

            switch (e.OldEditState)
            {
                case EditState.View:

                    //					this.SubjectRecordWrapDisplay.IsReadOnly = false;

                    switch (e.NewEditState)
                    {
                        case EditState.Add:
							this.EditFieldPanelEnabled = true;
							this.OrgList.CreateNew();
                            if (txtOrganizationName.Text.Length < 1)
                                OrganizationOptionsEditButtonStrip.SaveItemButton.IsEnabled = false;
                            if (cmbOrganizationType.SelectedIndex < 0)
                                OrganizationOptionsEditButtonStrip.SaveItemButton.IsEnabled = false;
                            break;

                        case EditState.Delete:
                            this.DeleteConfirmationPrompt.Visibility = Visibility.Visible;
                            break;

                        case EditState.Edit:
							this.EditFieldPanelEnabled = true;
                            if (txtOrganizationName.Text.Length < 1)
                                OrganizationOptionsEditButtonStrip.SaveItemButton.IsEnabled = false;
                            if (cmbOrganizationType.SelectedIndex < 0)
                                OrganizationOptionsEditButtonStrip.SaveItemButton.IsEnabled = false;
							break;

                    }
					break;

                case EditState.Add:
                    switch (e.NewEditState)
                    {
                        case EditState.Save:
                            if (this.OrgList.NewItem != null)
                            {
                                if (this.OrgList.NewItem.IsStudio)
                                {
                                    if (this.txtOrganizationName.Text == null || this.txtOrganizationName.Text.Length < 1 ||
                                        this.txtOrganizationAddressLine1.Text == null || this.txtOrganizationAddressLine1.Text.Length < 1 ||
                                        this.txtOrganizationCity.Text == null || this.txtOrganizationCity.Text.Length < 1 ||
                                        this.txtOrganizationStateCode.Text == null || this.txtOrganizationStateCode.Text.Length < 1 ||
                                        this.txtOrganizationZipCode.Text == null || this.txtOrganizationZipCode.Text.Length < 1 ||
                                        this.txtOrganizationContactName.Text == null || this.txtOrganizationContactName.Text.Length < 1 ||
                                        this.txtOrganizationContactPhone.Text == null || this.txtOrganizationContactPhone.Text.Length < 1 ||
                                        this.txtOrganizationContactEmail.Text == null || this.txtOrganizationContactEmail.Text.Length < 1 ||
                                        this.textBox1.Text == null || this.textBox1.Text.Length < 1 || // Shipping Address Line 1
                                        this.textBox3.Text == null || this.textBox3.Text.Length < 1 || // Shipping City
                                        this.textBox4.Text == null || this.textBox4.Text.Length < 1 || // Shipping State
                                        this.textBox5.Text == null || this.textBox5.Text.Length < 1 || // Shipping Zip
                                        this.txtLabCustomerID.Text == null || this.txtLabCustomerID.Text.Length < 1)
                                    {
                                        FlowMessageBox msg = new FlowMessageBox("Required Fields Missing", "Please fill in required fields before saving. All fields are required for Studio organizations.");
                                        msg.CancelButtonVisible = false;
                                        msg.ShowDialog();
                                        this.OrganizationOptionsEditButtonStrip.EditState = EditState.Add;
                                        break;
                                    }
                                }

                                //this.UserList.CurrentItem..FlowMasterDataContext =
                                //    this.FlowController.ProjectController.FlowMasterDataContext;
                                this.OrgList.CurrentItem.OrganizationGuid = Guid.NewGuid();
                                this.OrgList.CurrentItem.DateCreated = DateTime.Now;
                                this.OrgList.CurrentItem.DateModified = DateTime.Now;

                                this.OrgList.CommitNew();
                                this.FlowController.ProjectController.FlowMasterDataContext.SubmitChanges();
                            }
                            //this.FlowController.ProjectController.FlowMasterDataContext.RefreshOrgList();
                            this.OrganizationOptionsEditButtonStrip.EditState = EditState.View;
                            break;

                        case EditState.Cancel:
                            this.OrgList.CancelNew();
                            this.OrganizationOptionsEditButtonStrip.EditState = EditState.View;
                            break;
                    }
                    break;


                case EditState.Edit:
                    switch (e.NewEditState)
                    {
                        case EditState.Save:
                            this.OrgList.CurrentItem.DateModified = DateTime.Now;
                            this.FlowController.ProjectController.FlowMasterDataContext.SubmitChanges();
                            //this.FlowController.ProjectController.FlowMasterDataContext.RefreshOrgList();
                            this.OrganizationOptionsEditButtonStrip.EditState = EditState.View;
                            break;

                        case EditState.Cancel:
                            this.OrgList.RevertCurrent();
                            this.OrganizationOptionsEditButtonStrip.EditState = EditState.View;
                            break;
                    }

                    break;


                case EditState.Delete:
                    this.DeleteConfirmationPrompt.Visibility = Visibility.Collapsed;
                    break;

            }

           if(e.NewEditState == EditState.View)
				this.EditFieldPanelEnabled = false;

        }

		//void EnableFields(Boolean state)
		//{
		//    //IsReadOnly Values need to be the opposite of <state>
		//    //IsEnable Values need to be = <state>

		//    //User drop down should be Disabled when in edit or add mode
		//    cmbOrganization.IsEnabled = !state;

		//    txtOrganizationName.IsReadOnly = !state;
		//    txtLabCustomerID.IsReadOnly = !state;
		//    txtOrganizationAddressLine1.IsReadOnly = !state;
		//    txtOrganizationAddressLine2.IsReadOnly = !state;
		//    txtOrganizationCity.IsReadOnly = !state;
		//    txtOrganizationStateCode.IsReadOnly = !state;
		//    txtOrganizationZipCode.IsReadOnly = !state;
		//    txtOrganizationContactName.IsReadOnly = !state;
		//    txtOrganizationContactEmail.IsReadOnly = !state;
		//    txtOrganizationContactPhone.IsReadOnly = !state;
		//    cmbOrganizationType.IsEnabled = state;
           
		//}
              
        void ConfirmDeleteButton_Click(object sender, RoutedEventArgs e)
        {
            logger.Info("Clicked Confirm Delete button: organization='{0}' id='{1}'", this.OrgList.CurrentItem.OrganizationName, this.OrgList.CurrentItem.OrganizationID);

            // Check to see if any projects are using the organization we're about to delete
            foreach (FlowProject project in this.FlowController.ProjectController.FlowMasterDataContext.FlowProjects)
            {
                if (this.OrgList.CurrentItem.OrganizationID == project.StudioID ||
                    this.OrgList.CurrentItem.OrganizationID == project.OrganizationID)
                {
                    logger.Warn("Tried to delete an organization that is tied to a project: organization='{0}', project='{1}'",
                        this.OrgList.CurrentItem.OrganizationName, project.FullProjectDisplayName);

                    FlowMessageBox msg = new FlowMessageBox("Can't Delete Organization", "Unable to delete this organization because it is currently assigned to one or more projects.");
                    msg.CancelButtonVisible = false;
                    msg.ShowDialog();

                    this.OrganizationOptionsEditButtonStrip.EditState = EditState.View;

                    return;
                }
            }

            // Save the name and ID so we can log it after deleting
            string deletedOrgName = this.OrgList.CurrentItem.OrganizationName;
            int deletedOrgID = this.OrgList.CurrentItem.OrganizationID;
            
            // Delete the organization and refresh the UI
            this.OrgList.DeleteCurrent();
            this.FlowController.ProjectController.FlowMasterDataContext.SubmitChanges();
            this.FlowController.ProjectController.FlowMasterDataContext.RefreshOrgList();
            InitButtonStrip();
            this.OrganizationOptionsEditButtonStrip.EditState = EditState.View;

            logger.Info("Deleted organization: name='{0}' id='{1}'", deletedOrgName, deletedOrgID);
        }

		void CancelDeleteButton_Click(object sender, RoutedEventArgs e)
		{
            logger.Info("Clicked Cancel Delete button");
			this.OrganizationOptionsEditButtonStrip.EditState = EditState.View;
		}

		private void CopyFromMainAddressButton_Click(object sender, RoutedEventArgs e)
		{
			Button source = sender as Button;

			Organization organization = source.DataContext as Organization;

			organization.CopyMainAddressToShipping();

            logger.Info("Copied main address from shipping");
		}

        private void txtOrganizationName_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if ((txtOrganizationName.Text.Length < 1) || (cmbOrganizationType.SelectedIndex < 0))
                OrganizationOptionsEditButtonStrip.SaveItemButton.IsEnabled = false;
            else
                OrganizationOptionsEditButtonStrip.SaveItemButton.IsEnabled = true;
        }

        private void cmbOrganizationType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((txtOrganizationName.Text.Length < 1) || (cmbOrganizationType.SelectedIndex < 0))
                OrganizationOptionsEditButtonStrip.SaveItemButton.IsEnabled = false;
            else
                OrganizationOptionsEditButtonStrip.SaveItemButton.IsEnabled = true;
        }

		//void CreateProductPackageButton_Click(object sender, RoutedEventArgs e)
		//{
		//    this.FlowController.Product();
		//}

		//void ProductCatalogEditor_AssignItemInvoked(object sender, EventArgs<Product> e)
		//{
		//    //this.UserList.CurrentItem.AssignProduct(e.Data);
		//}

		//void ProductCatalogEditor_DeleteItemInvoked(object sender, EventArgs<ProductPackage> e)
		//{
		//    //this.UserList.CurrentItem.DeleteProductCatalogItem(e.Data);
		//}

	}
}