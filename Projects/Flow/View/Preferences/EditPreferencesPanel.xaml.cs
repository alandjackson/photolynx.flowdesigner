﻿using System;
using System.Windows.Controls;
using System.Windows.Forms;

using Flow.View.Project.ProjectTemplate.Base;
using Flow.Controller.Preferences;
using Flow.Controller;
using Flow.Model.Settings;
using Flow.Lib;
using System.IO;
using Flow.Properties;
using Flow.Schema.LinqModel.DataContext;
using Flow.Controls;
using NLog;

namespace Flow.View.Preferences
{

    /// <summary>
    /// Interaction logic for CapturePreferencesPanel.xaml
    /// </summary>
    public partial class EditPreferencesPanel : ProjectTemplatePanelBase
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public PreferencesController PreferencesController { get; set; }

        public FlowController FlowController { get; set; }


        public EditPreferencesPanel()
        {
            InitializeComponent();
        }


        private void btnSelectFolder_Click(object sender, EventArgs e)
        {

            
            try
            {
                String filePath = SelectExeFile(txtExternalEditor.Text);

                if (filePath != null)
                {
                    txtExternalEditor.Text = filePath;
                    this.FlowController.ProjectController.FlowMasterDataContext
                        .PreferenceManager.Edit.ExternalEditorPath = filePath;

                }
            }
            catch (Exception err)
            {
                logger.ErrorException("Exception occurred.", err);
            }
        }

        private void btnSelectOverlaysFolder_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();
            try
            {
                folderBrowserDialog1.SelectedPath = txtOverlaysDir.Text;
                if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                {
                    //this.FlowController.ProjectController.FlowMasterDataContext
                    //    .PreferenceManager["Capture.HotFolderDirectory"].NativeValue = folderBrowserDialog1.SelectedPath;

                    this.FlowController.ProjectController.FlowMasterDataContext
                        .PreferenceManager.Edit.OverlaysDirectory = folderBrowserDialog1.SelectedPath;

                }
            }
            catch (Exception err)
            {
                logger.ErrorException("Exception occurred.", err);
            }
        }

        private void btnSelectBackgroundsFolder_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();
            try
            {
                folderBrowserDialog1.SelectedPath = txtOverlaysDir.Text;
                if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                {
                    //this.FlowController.ProjectController.FlowMasterDataContext
                    //    .PreferenceManager["Capture.HotFolderDirectory"].NativeValue = folderBrowserDialog1.SelectedPath;

                    this.FlowController.ProjectController.FlowMasterDataContext
                        .PreferenceManager.Edit.OverlaysDirectory = folderBrowserDialog1.SelectedPath;

                }
            }
            catch (Exception err)
            {
                logger.ErrorException("Exception occurred.", err);
            }
        }

        private string SelectExeFile(string initialDirectory)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "exe files (*.exe)|*.exe|All files (*.*)|*.*";
            dialog.InitialDirectory = initialDirectory;
            dialog.Title = "Select an Image Editing Application";
            return (dialog.ShowDialog() == DialogResult.OK)
               ? dialog.FileName : null;
        }

        private void btnSave_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            this.FlowController.ProjectController.FlowMasterDataContext.SavePreferences();
        }

        //public String OverlayPngPath
        //{
        //    get
        //    {
        //        return Convert.ToString(this.FlowController.ProjectController.FlowMasterDataContext
        //              .PreferenceManager["Capture.OverlayPngPath"].NativeValue);
        //    }
        //    set
        //    {
        //        this.FlowController.ProjectController.FlowMasterDataContext
        //               .PreferenceManager["Capture.OverlayPngPath"].NativeValue = value;
        //    }
        //}

        

    }


}
