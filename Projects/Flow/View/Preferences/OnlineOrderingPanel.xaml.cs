﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Controller;
using Flow.Controller.Preferences;
using Flow.Controls.Extension;
using Flow.Controls.View.RecordDisplay;
using Flow.Lib;
using Flow.Lib.Helpers;
using Flow.Lib.Net;
using Flow.Lib.Preferences;
using Flow.Properties;
using Flow.Schema;
using Flow.Schema.LinqModel.DataContext;
using Flow.View.Project.ProjectTemplate.Base;
using StructureMap;
using System.Web;
using System.Net;
using System.IO;
using NetServ.Net.Json;
using Flow.View.Dialogs;
using Flow.Lib.PropertySetter;
using Flow.Lib.Activation;

namespace Flow.View.Preferences
{
	/// <summary>
	/// Interaction logic for ImageOptions.xaml
	/// </summary>
	public partial class OnlineOrderingPanel : ProjectTemplatePanelBase
	{
        public PreferencesController PreferencesController { get; set; }

        public FlowController FlowController { get; set; }

        public FlowMasterDataContext FlowMasterDataContext
        {
            get { return FlowController.ProjectController.FlowMasterDataContext; }
        }
		private PreferenceManager PreferenceManager
			{ get { return this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager; } }

		private ProjectTransferPreferences ProjectTransferPreferences
		{
			get { return this.PreferenceManager.Upload.ProjectTransfer; }
		}

        private NotificationProgressInfo _notificationInfo;


        public OnlineOrderingPanel()
		{
            
//            gridFields.DataContext = this;
		}

        public void Init()
        {
            InitializeComponent();
        }
        void TestUpload_Worker(object sender, DoWorkEventArgs e)
        {
            IFTPClient client;
			if (this.ProjectTransferPreferences.UseSftp)
            {
                client = new SFTPClient(
					this.ProjectTransferPreferences.HostAddress,
					this.ProjectTransferPreferences.HostPort,
					this.ProjectTransferPreferences.UserName,
					this.ProjectTransferPreferences.Password
                );
            }
            else
            {
                client = new FTPClient(
					this.ProjectTransferPreferences.HostAddress,
					this.ProjectTransferPreferences.HostPort,
					this.ProjectTransferPreferences.UserName,
					this.ProjectTransferPreferences.Password
                );
            }

			client.DefaultRemoteDirectory = this.ProjectTransferPreferences.RemoteDirectory;

            client.ProgressInfo = _notificationInfo;

            string status = client.TestConnection();
        }

        private void buttonTest_Click(object sender, RoutedEventArgs e)
        {
            _notificationInfo = new NotificationProgressInfo("Connection Test", "Testing connection...");
            IShowsNotificationProgress progressNotification =
                ObjectFactory.GetInstance<IShowsNotificationProgress>();
            
            _notificationInfo.NotificationProgress = progressNotification;
            progressNotification.ShowNotification(_notificationInfo);

            FlowBackgroundWorkerManager.RunWorker(TestUpload_Worker);
        }

        //void client_CurrentFileUploadProgression(object sender)
        //{
        //    IFTPClient client = sender as IFTPClient;
        //    progressNotification.UpdateNotification(client.ProgressInfo);
        //}

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
			this.FlowController.ProjectController.FlowMasterDataContext.SavePreferences();
        }

        private void txtIQCustomerPW_Loaded(object sender, RoutedEventArgs e)
        {
            this.txtIQCustomerPW.Password = this.PreferenceManager.Upload.OnlineOrder.ImageQuixCustomerPassword;
        }

        private void txtIQCustomerPW_PasswordChanged(object sender, RoutedEventArgs e)
        {
            this.PreferenceManager.Upload.OnlineOrder.ImageQuixCustomerPassword = this.txtIQCustomerPW.Password;
        }

        private void btnGoPLAPI_Click(object sender, RoutedEventArgs e)
        {
            //if (this.FlowController.ProjectController.CurrentFlowProject == null || String.IsNullOrEmpty(this.PreferenceManager.Upload.OnlineOrder.EcommerceAPIURL))
            //{
            //    return;
            //}
            this.FlowController.Project();
            this.FlowController.ProjectController.SubmitOnlineOrders(this.FlowController.ProjectController.CurrentFlowProject);
        }

        private void btnGoIQ_Click(object sender, RoutedEventArgs e)
        {
            this.FlowController.Project();
            this.FlowController.ProjectController.SubmitOnlineOrderData(this.FlowController.ProjectController.CurrentFlowProject);
        }

        private void btnEcommerceSignup_Click(object sender, RoutedEventArgs e)
        {
            string apiKey = (this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.EcommerceAPIKey);
            string apiURL = (this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.EcommerceAPIURL);
            
            bool hasAPIKey = !string.IsNullOrEmpty(apiKey);
            bool siteIsLive = IsSiteLive();

            if (hasAPIKey && !siteIsLive)
            {
                string wizardUrl = apiURL.Replace("/Api", "/embed/flow-wizard");

                this.browserWizard.Navigate(new Uri(wizardUrl, UriKind.Absolute), null, null, "JCart-Api-Key:" + apiKey);
                this.pnlWizardA.Visibility = Visibility.Collapsed;
                this.pnlWizardB.Visibility = Visibility.Visible;
                //return;
            }

            tabSetupAccount.Visibility = Visibility.Visible;
            tabSetupAccount.Focus();
        }

        private bool IsSiteLive()
        {
            
            string apiURL = this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.EcommerceAPIURL;
            string apiKey = this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.EcommerceAPIKey;

            if (String.IsNullOrEmpty(apiKey) || String.IsNullOrEmpty(apiURL)) return false;

            string fullAPI = apiURL + "/GetStudioDetails";

            HttpWebRequest rq = (HttpWebRequest)WebRequest.Create(fullAPI);
            //rq.Headers.Add("JCart-Api-Key", apikey);
            rq.ContentType = "application/x-www-form-urlencoded";
            rq.Headers.Add("JCart-Api-Key", apiKey);
            rq.Timeout = (120000);//try for 120 seconds
            rq.Method = "POST";

            ServicePointManager.ServerCertificateValidationCallback = delegate(
                    Object obj, System.Security.Cryptography.X509Certificates.X509Certificate certificate, System.Security.Cryptography.X509Certificates.X509Chain chain,
                    System.Net.Security.SslPolicyErrors errors)
            {
                return (true);
            };


            //StreamWriter stOut = new
            //StreamWriter(rq.GetRequestStream(),
            //System.Text.Encoding.ASCII);
            //stOut.Write(postData);
            //stOut.Close();

            try
            {
                HttpWebResponse rs = (HttpWebResponse)rq.GetResponse();
                StreamReader reader = new StreamReader(rs.GetResponseStream());
                string jsonText = reader.ReadToEnd();

                reader.Close();
                rs.Close();


                if (jsonText.StartsWith("{"))
                {
                    JsonParser parserPS = new JsonParser(new StringReader(jsonText), true);
                    JsonObject JsonResult = parserPS.ParseObject();
                    if (JsonResult != null && JsonResult.ContainsKey("studio"))
                    {
                        JsonObject jStudio = (JsonObject)JsonResult["studio"];
                        if (jStudio != null && jStudio.ContainsKey("launched"))
                        {
                            if (((JsonBoolean)jStudio["launched"]) == true)
                                return true;
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                //do nothing for now
            }
                   
                


            return false;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

            string APEURL = this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.EcommerceURL;
            Uri ApeUri = new Uri(APEURL);
            string protocal = ApeUri.Scheme;// = APEURL.Split("://")[0]; //ex: http or https
            string domain = ApeUri.Host;//APEURL.Split("://")[1]; //ex: apejuniorstaging.com

            string optional_lab_id = null;
            if(APEURL.Contains("lab_id"))
                optional_lab_id = APEURL.Split("lab_id=")[1]; //ex: 1

            string username = "ape-flow";
            string password = "431ce2dc5fbcaeff07ecb41540c8a4acde5f1872f3162175cc8aa";

            string apiUrl =  APEURL + "/api/studios";
            if(ApeUri.Query.Length > 0)
             apiUrl = APEURL.Replace(ApeUri.Query, "") + "/api/studios";
           

            string newApiUrl = protocal + "://" + txtSubdomain.Text + "." + domain + "/Api/";
           

            string wizardUrl = protocal + "://" + txtSubdomain.Text + "." + domain + "/embed/flow-wizard";

            string tmpApiKey = this.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.EcommerceAPIKey;

           


            string postData = "studio%5Bname%5D=" + HttpUtility.UrlEncode(txtName.Text);
            postData += ("&studio%5Bsubdomain%5D=" + HttpUtility.UrlEncode(txtSubdomain.Text));
            postData += ("&studio%5Binitial_user_email%5D=" + HttpUtility.UrlEncode(txtEmail.Text));
            postData += ("&studio%5Binitial_user_password%5D=" + HttpUtility.UrlEncode(this.FlowController.ActivationKey));
            postData += ("&studio%5Binitial_user_password_confirmation%5D=" + HttpUtility.UrlEncode(this.FlowController.ActivationKey));
            postData += ("&location%5Bname%5D=" + HttpUtility.UrlEncode(txtName.Text));
            postData += ("&location%5Baddress_line1%5D=" + HttpUtility.UrlEncode(txtAddress.Text));
            postData += ("&location%5Bcity%5D=" + HttpUtility.UrlEncode(txtCity.Text));
            postData += ("&location%5Bstate%5D=" + HttpUtility.UrlEncode(txtState.Text));
            postData += ("&location%5Bpostal_code%5D=" + HttpUtility.UrlEncode(txtZip.Text));
            postData += ("&location%5Bcountry%5D=" + HttpUtility.UrlEncode(txtCountry.Text));
            if (optional_lab_id != null)
                postData += ("&studio%5Blab_id%5D=" + HttpUtility.UrlEncode(optional_lab_id));

            HttpWebRequest rq = (HttpWebRequest)WebRequest.Create(apiUrl);
            rq.Credentials = new NetworkCredential(username, password);
            //rq.Headers.Add("JCart-Api-Key", apikey);
            rq.ContentType = "application/x-www-form-urlencoded";
            rq.ContentLength = postData.Length;
            rq.Timeout = (120000);//try for 120 seconds
            rq.Method = "POST";


            ServicePointManager.ServerCertificateValidationCallback = delegate(
            Object obj, System.Security.Cryptography.X509Certificates.X509Certificate certificate, System.Security.Cryptography.X509Certificates.X509Chain chain,
            System.Net.Security.SslPolicyErrors errors)
            {
                return (true);
            };


            StreamWriter stOut = new
            StreamWriter(rq.GetRequestStream(),
            System.Text.Encoding.ASCII);
            stOut.Write(postData);
            stOut.Close();


            HttpWebResponse rs = null;
            int tryCount = 0;
            while (tryCount < 1)
            {
                try
                {
                    rs = (HttpWebResponse)rq.GetResponse();
                    StreamReader reader = new StreamReader(rs.GetResponseStream());
                    string jsonText = reader.ReadToEnd();

                    reader.Close();
                    rs.Close();

                    if (jsonText.StartsWith("{"))
                    {
                        JsonParser parserPS = new JsonParser(new StringReader(jsonText), true);
                        JsonObject JsonResult = parserPS.ParseObject();
                        if (JsonResult != null && JsonResult.ContainsKey("studio"))
                        {
                            JsonObject jStudio = (JsonObject)JsonResult["studio"];
                            if (jStudio != null && jStudio.ContainsKey("api_key"))
                            {
                                string newApiKey = ((JsonString)jStudio["api_key"]).ToString();
                                this.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.EcommerceAPIKey = newApiKey;
                                this.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.EcommerceAPIURL = newApiUrl;
                                this.browserWizard.Navigate(new Uri(wizardUrl, UriKind.Absolute), null, null, "JCart-Api-Key:" + newApiKey);
                                this.pnlWizardA.Visibility = Visibility.Collapsed;
                                this.pnlWizardB.Visibility = Visibility.Visible;

                                //
                                //UPLOAD STATS
                                //
                                PropertySetterStore store = new PropertySetterStore();

                                ActivationValidator activation = new ActivationValidator(this.FlowController.ActivationKeyFile);
                                ActivationStore activationStore = activation.Repository.Load();

                                store.ActivationKey = activationStore.ActivationKey;
                                store.EcommerceAPIUrl = newApiUrl;
                                store.EcommerceAPIKey = newApiKey;

                                PropertySetterPost PropertySetterPost = new PropertySetterPost();
                                string returnMsg = PropertySetterPost.Post(store);
                                //
                                //END UPLOAD STATS
                                //


                            }
                        }
                        //if(uploadedbackgrounds.Contains(bg.FullPath))
                        {
                            //image already exists
                        }
                    }

                    //return jsonText;
                }
                catch (Exception ex)
                {
                    if(ex.Message.Contains("(422)"))
                    {
                        FlowMessageBox msg = new FlowMessageBox("APE Signup Error", "Either your subdomain or email address is not unique, please change it and try again");
                        msg.CancelButtonVisible = false;
                        msg.ShowDialog();
                    }
                    tryCount++;
                }
            }
        }

        private void _this_Loaded(object sender, RoutedEventArgs e)
        {
            HandleAPEWizardVisibility();
            if (this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.EcommerceURL.Contains("myportraitgallery") || this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.EcommerceURL.Contains("lab_id=1")
                || (this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.EcommerceURL.Contains("staging") && this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.EcommerceURL.Contains("lab_id=2")))
            {
                lblNeedAnAccount.Content = "Do you need an online ordering provider? Sign up with My Portrait Gallery (MPG) below:";
                btnEcommerceSignup.Content = "Create MPG Account";
                lblWarningNotLive.Content = "Warning, your MPG site is not live yet.";
                lblAccountSetup.Content = "MPG Account Setup";
                lblSampleDomain.Content = ".MyPortraitGallery.com";
            }
            else
            {
                lblNeedAnAccount.Content = "Do you need an online ordering provider? Sign up with APE below:";
                btnEcommerceSignup.Content = "Create APE Account";
                lblWarningNotLive.Content = "Warning, your APE site is not live yet.";
                lblAccountSetup.Content = "APE Account Setup";
                lblSampleDomain.Content = ".SchoolDayPhoto.com";
            }
        }

        private void Grid_LostFocus(object sender, RoutedEventArgs e)
        {
            HandleAPEWizardVisibility();
        }

        private void HandleAPEWizardVisibility()
        {
            pnlLaunchWarning.Visibility = Visibility.Collapsed;
            if (this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.ShowEcommerceSignupPanel == true)
            {
                bool hasAPIKey = !string.IsNullOrEmpty(this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Upload.OnlineOrder.EcommerceAPIKey);
                if (hasAPIKey && IsSiteLive())
                {
                    tabSetupAccount.Visibility = Visibility.Collapsed;
                    pnlAPESignup.Visibility = Visibility.Collapsed;
                    return;
                }

                if (!hasAPIKey)
                {
                    if (this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.EcommerceURL.Contains("myportraitgallery") || this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.EcommerceURL.Contains("lab_id=1")
               || (this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.EcommerceURL.Contains("staging") && this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.EcommerceURL.Contains("lab_id=2")))
                    {
                        btnEcommerceSignup.Content = "Create MPG Account";
                    }
                    else
                    {
                        btnEcommerceSignup.Content = "Create APE Account";
                    }
                    return;
                } 
                    else
                {
                    if (this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.EcommerceURL.Contains("myportraitgallery") || this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.EcommerceURL.Contains("lab_id=1")
               || (this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.EcommerceURL.Contains("staging") && this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.EcommerceURL.Contains("lab_id=2")))
                    {

                        btnEcommerceSignup.Content = "Configure & Launch MPG Account";
                        pnlLaunchWarning.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        btnEcommerceSignup.Content = "Configure & Launch APE Account";
                        pnlLaunchWarning.Visibility = Visibility.Visible;
                    }
                    return;
                }

                
            }
            else
            {
                tabSetupAccount.Visibility = Visibility.Collapsed;
                pnlAPESignup.Visibility = Visibility.Collapsed;
            }
        }
	}
}