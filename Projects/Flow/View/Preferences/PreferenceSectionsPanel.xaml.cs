﻿using System.Windows;

using Flow.View.Project.ProjectTemplate.Base;

namespace Flow.View.Preferences
{
	/// <summary>
	/// Interaction logic for PreferenceSectionsPanel.xaml
	/// </summary>
	public partial class PreferenceSectionsPanel 
	{
        //protected List<Button> SidebarButtons = new List<Button>();

        //protected ViewProjectsPanel _viewProjectsPanel;
        //public ViewProjectsPanel ViewProjectsPanel { get { return _viewProjectsPanel; } }

        //protected NewProjectPanel _newProjectPanel = null;
        //public NewProjectPanel NewProjectPanel { get { return _newProjectPanel; } }

        //protected ProjectImportPanel _projectImportPanel = null;
        //public ProjectImportPanel ProjectImportPanel { get { return _projectImportPanel; } }

        //protected ViewProjectTemplatesPanel _viewProjectTemplatesPanel = null;
        //public ViewProjectTemplatesPanel ViewProjectTemplatesPanel { get { return _viewProjectTemplatesPanel; } }

        //protected EditProjectTemplatePanel _editProjectTemplatesPanel = null;
        //public EditProjectTemplatePanel EditProjectTemplatePanel { get { return _editProjectTemplatesPanel; } }

        //protected ViewProductCatalogsPanel _viewProductCatalogsPanel = null;
        //public ViewProductCatalogsPanel ViewProductCatalogsPanel { get { return _viewProductCatalogsPanel; } }


		//protected DataImportPanel _dataImportPanel = null;
		//public DataImportPanel DataImportPanel { get { return _dataImportPanel; } }

		//protected TestCollectionNavigator _t = null;
		//public TestCollectionNavigator t { get { return _t; } }

        protected UserPanel _userPanel = null;
        protected OrganizationPanel _organizationPanel = null;
        protected UploadPanel _uploadPanel = null;
        protected OnlineOrderingPanel _onlineOrderingPanel = null;
        protected CapturePreferencesPanel _capturePreferencesPanel = null;
        protected EditPreferencesPanel _editPreferencesPanel = null;
        protected ApplicationPreferencesPanel _applicationPreferencesPanel = null;
        protected LayoutPreferencesPanel _layoutPreferencesPanel = null;
		protected OrdersPreferencesPanel _ordersPreferencesPanel = null;
        protected PermissionsPanel _permissionsPanel = null;
        protected UtilitiesPanel _utilitiesPanel = null;

        public UserPanel UserPanel { get { return _userPanel; } }
        public OrganizationPanel OrganizationPanel { get { return _organizationPanel; } }
        public UploadPanel UploadPanel { get { return _uploadPanel; } }
        public OnlineOrderingPanel OnlineOrderingPanel { get { return _onlineOrderingPanel; } }
        public CapturePreferencesPanel CapturePreferencesPanel { get { return _capturePreferencesPanel; } }
        public EditPreferencesPanel EditPreferencesPanel { get { return _editPreferencesPanel; } }
        public LayoutPreferencesPanel LayoutPreferencesPanel { get { return _layoutPreferencesPanel; } }
        public ApplicationPreferencesPanel ApplicationPreferencesPanel { get { return _applicationPreferencesPanel; } }
		public OrdersPreferencesPanel OrdersPreferencesPanel { get { return _ordersPreferencesPanel; } }
        public PermissionsPanel PermissionsPanel { get { return _permissionsPanel; } }
        public UtilitiesPanel UtilitiesPanel { get { return _utilitiesPanel; } }


        public PreferenceSectionsPanel()
		{
			InitializeComponent();

            //SidebarButtons.Add(btnViewProjects);
            //SidebarButtons.Add(btnCreateNewProject);
            //SidebarButtons.Add(btnViewProjectTemplates);
            //SidebarButtons.Add(btnCreateNewProjectTemplate);
            //SidebarButtons.Add(btnImportProject);
		}

        private void btnUsers_Click(object sender, RoutedEventArgs e)
        {
            ShowUserPanel();
        }

        private void btnPermissions_Click(object sender, RoutedEventArgs e)
        {
            ShowPermissionsPanel();
        }

        private void btnOrganizations_Click(object sender, RoutedEventArgs e)
        {
            ShowOrganizationPanel();
        }

        private void btnCapture_Click(object sender, RoutedEventArgs e)
        {
            ShowCapturePreferencesPanel();
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            ShowEditPreferencesPanel();
        }

        private void btnUpload_Click(object sender, RoutedEventArgs e)
        {
            ShowUploadPanel();
        }

		private void btnOrders_Click(object sender, RoutedEventArgs e)
		{
			ShowOrdersPreferencesPanel();
		}

        private void btnApplication_Click(object sender, RoutedEventArgs e)
        {
            ShowApplicationPreferencesPanel();
        }

        private void btnLayout_Click(object sender, RoutedEventArgs e)
        {
            ShowLayoutPreferencesPanel();
        }

        private void btnUtilities_Click(object sender, RoutedEventArgs e)
        {
            ShowUtilitiesPanel();
        }

        /// <summary>
        ///  Displays The Upload Panel
        /// </summary>
        public void ShowUploadPanel()
        {
            ShowUploadPanel(0);
        }
        public void ShowUploadPanel(int tabIndex)
        {
            _uploadPanel = ShowPanel<UploadPanel>(btnUpload, _uploadPanel);
            _uploadPanel.FlowController = this.FlowController;
            _uploadPanel.tabCtrl.SelectedIndex = tabIndex;
           
        }

        /// <summary>
        ///  Displays The User Panel
        /// </summary>
        public void ShowUserPanel()
        {
            _userPanel = ShowPanel<UserPanel>(btnUsers, _userPanel);
            _userPanel.FlowController = this.FlowController;
            _userPanel.InitButtonStrip();
        }

        /// <summary>
        ///  Displays The Permissions Panel
        /// </summary>
        public void ShowPermissionsPanel()
        {
            _permissionsPanel = ShowPanel<PermissionsPanel>(btnPermissions, _permissionsPanel);
            _permissionsPanel.FlowController = this.FlowController;
            _permissionsPanel.DataContext = this.FlowController.ProjectController.FlowMasterDataContext;
        }

        /// <summary>
        ///  Displays The Utilities Panel
        /// </summary>
        public void ShowUtilitiesPanel()
        {
            _utilitiesPanel = ShowPanel<UtilitiesPanel>(btnUtilities, _utilitiesPanel);
            _utilitiesPanel.FlowController = this.FlowController;
            _utilitiesPanel.DataContext = this.FlowController.ProjectController.FlowMasterDataContext;
        }

        /// <summary>
        /// Displays The Organization Panel
        /// </summary>
		public void ShowOrganizationPanel()
        {
            _organizationPanel = ShowPanel<OrganizationPanel>(btnOrganizations, _organizationPanel);
            _organizationPanel.FlowController = this.FlowController;
            _organizationPanel.InitButtonStrip();
        }

        /// <summary>
        ///  Displays The Capture Preferences Panel
        /// </summary>
        public void ShowCapturePreferencesPanel()
        {
            _capturePreferencesPanel = ShowPanel<CapturePreferencesPanel>(btnCapture, _capturePreferencesPanel);
            _capturePreferencesPanel.FlowController = this.FlowController;

			_capturePreferencesPanel.DataContext = this.FlowController.ProjectController.FlowMasterDataContext;
        }

        /// <summary>
        ///  Displays The Edit Preferences Panel
        /// </summary>
        public void ShowEditPreferencesPanel()
        {
            _editPreferencesPanel = ShowPanel<EditPreferencesPanel>(btnEdit, _editPreferencesPanel);
            _editPreferencesPanel.FlowController = this.FlowController;

            _editPreferencesPanel.DataContext = this.FlowController.ProjectController.FlowMasterDataContext;
        }

        /// <summary>
        ///  Displays The Application Preferences Panel
        /// </summary>
        public void ShowApplicationPreferencesPanel()
        {
            _applicationPreferencesPanel = ShowPanel<ApplicationPreferencesPanel>(btnApplication, _applicationPreferencesPanel);
            _applicationPreferencesPanel.FlowController = this.FlowController;

            _applicationPreferencesPanel.DataContext = this.FlowController.ProjectController.FlowMasterDataContext;
        }

        /// <summary>
        ///  Displays The Application Preferences Panel
        /// </summary>
        public void ShowLayoutPreferencesPanel()
        {
            _layoutPreferencesPanel = ShowPanel<LayoutPreferencesPanel>(btnLayout, _layoutPreferencesPanel);
            _layoutPreferencesPanel.FlowController = this.FlowController;

            _layoutPreferencesPanel.DataContext = this.FlowController.ProjectController.FlowMasterDataContext;
        }

		/// <summary>
		/// Displays the Orders Preferences Panel
		/// </summary>
		public void ShowOrdersPreferencesPanel()
		{
			_ordersPreferencesPanel = ShowPanel<OrdersPreferencesPanel>(btnOrders, _ordersPreferencesPanel);
			_ordersPreferencesPanel.FlowController = this.FlowController;

			_ordersPreferencesPanel.DataContext = this.FlowController.ProjectController.FlowMasterDataContext;
		}


        /// <summary>
        /// Shows panels based upon user selection
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sender"></param>
        /// <param name="existingPnl"></param>
        /// <returns></returns>
        private T ShowPanel<T>(object sender, T existingPnl) where T : ProjectTemplatePanelBase, new()
        {
            //if (sender != null)
            //    EnableSender(sender);

            if (existingPnl == null)
            {
                existingPnl = new T();
                uxContentPanel.Children.Add(existingPnl);
                existingPnl.DataContext = this.FlowController.ProjectController.FlowMasterDataContext;
                
            }
            foreach (UIElement child in uxContentPanel.Children)
            {
                child.Visibility =
                    (child == existingPnl ? Visibility.Visible : Visibility.Hidden);
            }

            return existingPnl;
        }

        private void btnOnlineOrdering_Click(object sender, RoutedEventArgs e)
        {
            ShowOnlineOrderingPanel();
        }

        public void ShowOnlineOrderingPanel()
        {
            _onlineOrderingPanel = ShowPanel<OnlineOrderingPanel>(btnOnlineOrdering, _onlineOrderingPanel);
            _onlineOrderingPanel.FlowController = this.FlowController;
            _onlineOrderingPanel.Init();
            //_onlineOrderingPanel.tabCtrl.SelectedIndex = 0;
        }

        



       

        







///
		#region TEST
///

		//private void btnCollectionNavigatorTest_Click(object sender, RoutedEventArgs e)
		//{
		//    _t = ShowPanel<TestCollectionNavigator>(btnCollectionNavigatorTest, _t);
		//}

///
		#endregion
///


	}
}
