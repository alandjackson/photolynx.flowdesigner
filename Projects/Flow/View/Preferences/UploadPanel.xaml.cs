﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Controller;
using Flow.Controller.Preferences;
using Flow.Controls.Extension;
using Flow.Controls.View.RecordDisplay;
using Flow.Lib;
using Flow.Lib.Helpers;
using Flow.Lib.Net;
using Flow.Lib.Preferences;
using Flow.Properties;
using Flow.Schema;
using Flow.Schema.LinqModel.DataContext;
using Flow.View.Project.ProjectTemplate.Base;
using StructureMap;

namespace Flow.View.Preferences
{
	/// <summary>
	/// Interaction logic for ImageOptions.xaml
	/// </summary>
	public partial class UploadPanel : ProjectTemplatePanelBase
	{
        public PreferencesController PreferencesController { get; set; }

        public FlowController FlowController { get; set; }

		private PreferenceManager PreferenceManager
			{ get { return this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager; } }

		private ProjectTransferPreferences ProjectTransferPreferences
		{
			get { return this.PreferenceManager.Upload.ProjectTransfer; }
		}

        private NotificationProgressInfo _notificationInfo;

		//public string HostName
		//{
		//    get
		//    {
		//        return (string)this.FlowController.ProjectController.FlowMasterDataContext
		//                .PreferenceManager["Upload.ProjectTransferSettings.HostAddress"].NativeValue;
		//        //return Settings.Default.HostName;
		//    }
		//    set
		//    {
		//        this.FlowController.ProjectController.FlowMasterDataContext
		//              .PreferenceManager["Upload.ProjectTransferSettings.HostAddress"].NativeValue = value;
		//    }
		//}

		//public int Port
		//{
		//    get
		//    {
		//        return Convert.ToInt32(this.FlowController.ProjectController.FlowMasterDataContext
		//              .PreferenceManager["Upload.ProjectTransferSettings.HostPort"].NativeValue);
		//    }
		//    set
		//    {
		//        this.FlowController.ProjectController.FlowMasterDataContext
		//            .PreferenceManager["Upload.ProjectTransferSettings.HostPort"].NativeValue = Convert.ToString(value);
		//    }
		//}

		//public string UserName
		//{
		//    get
		//    {
		//        return (string)this.FlowController.ProjectController.FlowMasterDataContext
		//              .PreferenceManager["Upload.ProjectTransferSettings.UserName"].NativeValue;
		//    }
		//    set
		//    {
		//        this.FlowController.ProjectController.FlowMasterDataContext
		//            .PreferenceManager["Upload.ProjectTransferSettings.UserName"].NativeValue = value;
		//    }
		//}

		//public string Password
		//{
		//    get
		//    {
		//        return (string)this.FlowController.ProjectController.FlowMasterDataContext
		//              .PreferenceManager["Upload.ProjectTransferSettings.Password"].NativeValue;
		//    }
		//    set
		//    {
		//        this.FlowController.ProjectController.FlowMasterDataContext
		//            .PreferenceManager["Upload.ProjectTransferSettings.Password"].NativeValue = value;
		//    }
		//}

		//public string RemoteDirectory
		//{
		//    get
		//    {
		//        return (string)this.FlowController.ProjectController.FlowMasterDataContext
		//              .PreferenceManager["Upload.ProjectTransferSettings.RemoteDirectory"].NativeValue;
		//    }
		//    set
		//    {
		//        this.FlowController.ProjectController.FlowMasterDataContext
		//            .PreferenceManager["Upload.ProjectTransferSettings.RemoteDirectory"].NativeValue = value;
		//    }
		//}

		//public bool UseSFTP
		//{
		//    get
		//    {
		//        return Convert.ToBoolean(this.FlowController.ProjectController.FlowMasterDataContext
		//              .PreferenceManager["Upload.ProjectTransferSettings.UseSftp"].NativeValue);
		//    }
		//    set
		//    {
		//        this.FlowController.ProjectController.FlowMasterDataContext
		//              .PreferenceManager["Upload.ProjectTransferSettings.UseSftp"].NativeValue = Convert.ToString(value);
		//        if (value)
		//        {
		//            Port = 22;
		//        }
		//        else
		//        {
		//            Port = 21;
		//        }
		//        txtPort.GetBindingExpression(TextBox.TextProperty).UpdateTarget();
		//        Settings.Default.Save();
		//    }
		//}

        public UploadPanel()
		{
            InitializeComponent();
//            gridFields.DataContext = this;
		}

        void TestUpload_Worker(object sender, DoWorkEventArgs e)
        {
            IFTPClient client;
			if (this.ProjectTransferPreferences.UseSftp)
            {
                client = new SFTPClient(
					this.ProjectTransferPreferences.HostAddress,
					this.ProjectTransferPreferences.HostPort,
					this.ProjectTransferPreferences.UserName,
					this.ProjectTransferPreferences.Password
                );
            }
            else
            {
                client = new FTPClient(
					this.ProjectTransferPreferences.HostAddress,
					this.ProjectTransferPreferences.HostPort,
					this.ProjectTransferPreferences.UserName,
					this.ProjectTransferPreferences.Password
                );
            }

			client.DefaultRemoteDirectory = this.ProjectTransferPreferences.RemoteDirectory;

            client.ProgressInfo = _notificationInfo;

            string status = client.TestConnection();
        }

        private void buttonTest_Click(object sender, RoutedEventArgs e)
        {
            _notificationInfo = new NotificationProgressInfo("Connection Test", "Testing connection...");
            IShowsNotificationProgress progressNotification =
                ObjectFactory.GetInstance<IShowsNotificationProgress>();
            
            _notificationInfo.NotificationProgress = progressNotification;
            progressNotification.ShowNotification(_notificationInfo);

            FlowBackgroundWorkerManager.RunWorker(TestUpload_Worker);
        }

        //void client_CurrentFileUploadProgression(object sender)
        //{
        //    IFTPClient client = sender as IFTPClient;
        //    progressNotification.UpdateNotification(client.ProgressInfo);
        //}

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
			this.FlowController.ProjectController.FlowMasterDataContext.SavePreferences();
        }

        //private void txtIQCustomerPW_Loaded(object sender, RoutedEventArgs e)
        //{
        //    this.txtIQCustomerPW.Password = this.PreferenceManager.Upload.OnlineOrder.ImageQuixCustomerPassword;
        //}

        //private void txtIQCustomerPW_PasswordChanged(object sender, RoutedEventArgs e)
        //{
        //    this.PreferenceManager.Upload.OnlineOrder.ImageQuixCustomerPassword = this.txtIQCustomerPW.Password;
        //}

        private void btnGoPLAPI_Click(object sender, RoutedEventArgs e)
        {
            //if (this.FlowController.ProjectController.CurrentFlowProject == null || String.IsNullOrEmpty(this.PreferenceManager.Upload.OnlineOrder.EcommerceAPIURL))
            //{
            //    return;
            //}
            this.FlowController.Project();
            this.FlowController.ProjectController.SubmitOnlineOrders(this.FlowController.ProjectController.CurrentFlowProject);
        }

        private void btnGoIQ_Click(object sender, RoutedEventArgs e)
        {
            this.FlowController.Project();
            this.FlowController.ProjectController.SubmitOnlineOrderData(this.FlowController.ProjectController.CurrentFlowProject);
        }
	}
}