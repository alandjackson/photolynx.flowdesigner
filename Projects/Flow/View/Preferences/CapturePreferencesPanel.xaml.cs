﻿using System;
using System.Windows.Controls;
using System.Windows.Forms;

using Flow.View.Project.ProjectTemplate.Base;
using Flow.Controller.Preferences;
using Flow.Controller;
using Flow.Model.Settings;
using Flow.Lib;
using System.IO;
using Flow.Properties;
using Flow.Schema.LinqModel.DataContext;
using Flow.Controls;
using NLog;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using ExifLib;
using System.Linq;
using Flow.Lib.Preferences;
using System.Windows;

namespace Flow.View.Preferences
{
    /// <summary>
    /// Interaction logic for CapturePreferencesPanel.xaml
    /// </summary>
    public partial class CapturePreferencesPanel : ProjectTemplatePanelBase
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public PreferencesController PreferencesController { get; set; }

        private FlowController _flowController { get; set; }
        public FlowController FlowController {
            get { return _flowController; }
            set
            {
                _flowController = value;
                {
                    if (_flowController != null)
                    {
                        AllFields = new List<ExifField>();
                        if (_flowController.ProjectController.FlowMasterDataContext.PreferenceManager.Capture.MetaMatchSourceField != null)
                            AllFields.Add(_flowController.ProjectController.FlowMasterDataContext.PreferenceManager.Capture.MetaMatchSourceField);
                    }

                    InitializeComponent();
                    AdjustMethod.Items.Add("Move Lights");
                    AdjustMethod.Items.Add("Change Camera F-Stop");
                }
            }
        }

        public List<ExifField> AllFields { get; set; }

        public CapturePreferencesPanel()
        {
            
           //do init when flowcontorller is set
        }

        //void CapturePreferencesPanel_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        
        
        private void btnSelectFolder_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();
            try
            {
                folderBrowserDialog1.SelectedPath = txtHotFolder.Text;
                if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                {
					//this.FlowController.ProjectController.FlowMasterDataContext
					//    .PreferenceManager["Capture.HotFolderDirectory"].NativeValue = folderBrowserDialog1.SelectedPath;

					this.FlowController.ProjectController.FlowMasterDataContext
						.PreferenceManager.Capture.HotFolderDirectory = folderBrowserDialog1.SelectedPath;
                    
				}
            }
            catch (Exception err)
            {
                logger.ErrorException("Exception occurred.", err);
            }
        }

        private void btnSave_Click(object sender, System.Windows.RoutedEventArgs e)
        {
			this.FlowController.ProjectController.FlowMasterDataContext.SavePreferences();

            if (this.FlowController.CaptureController != null &&
				this.FlowController.CaptureController.HotfolderController != null
			)
            {
                this.FlowController.CaptureController.HotfolderController.ChangeHotfolderWatcher(
					this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Capture.HotFolderDirectory
				);
            }
        }

		//public String OverlayPngPath
		//{
		//    get
		//    {
		//        return Convert.ToString(this.FlowController.ProjectController.FlowMasterDataContext
		//              .PreferenceManager["Capture.OverlayPngPath"].NativeValue);
		//    }
		//    set
		//    {
		//        this.FlowController.ProjectController.FlowMasterDataContext
		//               .PreferenceManager["Capture.OverlayPngPath"].NativeValue = value;
		//    }
		//}

        private void btnSelectPng_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                string selectedFilePath = DialogUtility.SelectFile(
					"Select Overlay PNG File",
                    this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.ProjectsDirectory,
					"PNG File (*.png)|*.png"
                );

                if (!String.IsNullOrEmpty(selectedFilePath))
                {
					this.FlowController.ProjectController.FlowMasterDataContext
						.PreferenceManager.Capture.OverlayPngPath = selectedFilePath;
                }
            }
            catch (Exception err)
            {
                logger.ErrorException("Exception occurred.", err);
            }
        }

        private void btnOrphanedImages_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            this.FlowController.ProjectController.FixOrphanedImages();

        }

        

        private void btnBrowseSampleImage_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            string defaultFolder = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            if (this.FlowController.ProjectController.CurrentFlowProject != null)
                if (Directory.Exists(this.FlowController.ProjectController.CurrentFlowProject.ImageDirPath))
                    if (Directory.GetFiles(this.FlowController.ProjectController.CurrentFlowProject.ImageDirPath,"*.*",SearchOption.TopDirectoryOnly).Length > 0)
                        defaultFolder = this.FlowController.ProjectController.CurrentFlowProject.ImageDirPath;
            try
            {
                string selectedFilePath = DialogUtility.SelectFile(
                    "Select Sample File",
                    defaultFolder,
                    "JPG File (*.jpg)|*.jpg"
                );

                if (!String.IsNullOrEmpty(selectedFilePath))
                {

                    //var encoding = new System.Text.ASCIIEncoding();
                    //var bitmap = new Bitmap(selectedFilePath);
                    AllFields = new List<ExifField>();

                    using (var reader = new ExifReader(selectedFilePath))
                {
                    // Get the image thumbnail (if present)
                    var thumbnailBytes = reader.GetJpegThumbnailBytes();

                    //if (thumbnailBytes == null)
                    //    pictureBoxThumbnail.Image = null;
                    //else
                    //{
                    //    using (var stream = new MemoryStream(thumbnailBytes))
                    //        pictureBoxThumbnail.Image = Image.FromStream(stream);
                    //}

                    var props = Enum.GetValues(typeof(ExifTags)).Cast<ushort>().Select(tagID =>
                    {
                        object val;
                        if (reader.GetTagValue(tagID, out val))
                        {
                            // Special case - some doubles are encoded as TIFF rationals. These
                            // items can be retrieved as 2 element arrays of {numerator, denominator}
                            if (val is double)
                            {
                                int[] rational;
                                if (reader.GetTagValue(tagID, out rational))
                                    val = string.Format("{0} ({1}/{2})", val, rational[0], rational[1]);
                            }
                            if ((tagID == 37510 || tagID == 37500) && val.GetType() == typeof(byte[]))
                                val = System.Text.Encoding.ASCII.GetString(val as Byte[]).Replace("ASCII", "").Trim('\0');
                            AllFields.Add(new ExifField( tagID.ToString(), Enum.GetName(typeof(ExifTags), tagID), RenderTag(val)));
                            return string.Format("{0}: {1}", Enum.GetName(typeof(ExifTags), tagID), RenderTag(val));
                        }

                        return null;

                    }).Where(x => x != null).ToArray();

                    Console.Write(string.Join("\r\n", props));
                }
                   //foreach (var item in bitmap.PropertyItems)
                   //{
                   //    if (item.Type == 1)
                   //        AllFields.Add(new ExifField(item.Id.ToString(), item.Type.ToString(), BitConverter.ToString(item.Value, 0).TrimEnd('\0')));
                   //    else if(item.Type == 2)
                   //        AllFields.Add(new ExifField(item.Id.ToString(), item.Type.ToString(), encoding.GetString(item.Value).TrimEnd('\0')));
                   //    else if (item.Type == 3)
                   //        AllFields.Add(new ExifField(item.Id.ToString(), item.Type.ToString(), BitConverter.ToInt16(item.Value, 0).ToString().TrimEnd('\0')));
                   //    else if (item.Type == 4)
                   //        AllFields.Add(new ExifField(item.Id.ToString(), item.Type.ToString(), BitConverter.ToInt32(item.Value, 0).ToString().TrimEnd('\0')));
                   //    else if (item.Type == 5)
                   //        AllFields.Add(new ExifField(item.Id.ToString(), item.Type.ToString(), encoding.GetString(item.Value).TrimEnd('\0')));
                   //    else if (item.Type == 6)
                   //        AllFields.Add(new ExifField(item.Id.ToString(), item.Type.ToString(), encoding.GetString(item.Value).TrimEnd('\0')));
                   //    else if (item.Type == 7)
                   //        AllFields.Add(new ExifField(item.Id.ToString(), item.Type.ToString(), encoding.GetString(item.Value).TrimEnd('\0')));
                   //    else if (item.Type == 8)
                   //        AllFields.Add(new ExifField(item.Id.ToString(), item.Type.ToString(), encoding.GetString(item.Value).TrimEnd('\0')));
                   //    else if (item.Type == 9)
                   //        AllFields.Add(new ExifField(item.Id.ToString(), item.Type.ToString(), encoding.GetString(item.Value).TrimEnd('\0')));
                   //    else if (item.Type == 10)
                   //        AllFields.Add(new ExifField(item.Id.ToString(), item.Type.ToString(), encoding.GetString(item.Value).TrimEnd('\0')));
                   //}
                   lstAllFilds.ItemsSource = AllFields;
                }
            }
            catch (Exception err)
            {
                logger.ErrorException("Exception occurred.", err);
            }
        }

        private static string RenderTag(object tagValue)
        {
            // Arrays don't render well without assistance.
            var array = tagValue as Array;
            if (array != null)
            {
                // Hex rendering for really big byte arrays (ugly otherwise)
                if (array.Length > 20 && array.GetType().GetElementType() == typeof(byte))
                    return "0x" + string.Join("", array.Cast<byte>().Select(x => x.ToString("X2")).ToArray());

                return string.Join(", ", array.Cast<object>().Select(x => x.ToString()).ToArray());
            }

            return tagValue.ToString();
        }

        private void RadioButton_Checked(object sender, System.Windows.RoutedEventArgs e)
        {

        }

        private void RadioButton_Unchecked(object sender, System.Windows.RoutedEventArgs e)
        {

        }

        private void btnResetCaptureLayout_Click(object sender, RoutedEventArgs e)
        {
            if (_flowController == null || _flowController.CaptureController == null)
                return;
            string layoutFile = System.IO.Path.Combine(FlowContext.FlowConfigDirPath, "DefaultDockSettings.xml");
            if (File.Exists(layoutFile))
            {
                try
                {

                    _flowController.CaptureController.CaptureDockPanel.DockMgr.RestoreLayout(layoutFile);
                    _flowController.CaptureController.CaptureDockPanel.DockMgr.UpdateLayout();
                }
                catch (Exception ex)
                {
                    _flowController.CaptureController.SaveLayoutOnClose = false;
                }
            }
        }

    }
   
}
