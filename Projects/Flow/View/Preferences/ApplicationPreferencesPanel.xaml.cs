﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Controller;
using Flow.Controller.Preferences;
using Flow.Controls.Extension;
using Flow.Controls.View.RecordDisplay;
using Flow.Lib;
using Flow.Lib.Helpers;
using Flow.Lib.Net;
using Flow.Lib.Preferences;
using Flow.Properties;
using Flow.Schema;
using Flow.Schema.LinqModel.DataContext;
using Flow.View.Project.ProjectTemplate.Base;
using StructureMap;
using System.Windows.Forms;
using Flow.View.Dialogs;
using NLog;

namespace Flow.View.Preferences
{
    /// <summary>
    /// Interaction logic for ApplicationPreferencesPanel.xaml
    /// </summary>
    public partial class ApplicationPreferencesPanel : ProjectTemplatePanelBase
    {
        public PreferencesController PreferencesController { get; set; }

        public FlowController FlowController { get; set; }

        private PreferenceManager PreferenceManager
        { get { return this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager; } }

        private ApplicationPreferences ApplicationPreferences
        {
            get { return this.PreferenceManager.Application; }
        }

        private NotificationProgressInfo _notificationInfo;

        private static Logger logger = LogManager.GetCurrentClassLogger();

        public ApplicationPreferencesPanel()
        {
            InitializeComponent();
            //            gridFields.DataContext = this;
            
        }

      

        //void client_CurrentFileUploadProgression(object sender)
        //{
        //    IFTPClient client = sender as IFTPClient;
        //    progressNotification.UpdateNotification(client.ProgressInfo);
        //}

        private void btnSelectFolder_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();
            try
            {
                folderBrowserDialog1.SelectedPath = txtProjectDir.Text;
                if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                {
                    //this.FlowController.ProjectController.FlowMasterDataContext
                    //    .PreferenceManager["Capture.HotFolderDirectory"].NativeValue = folderBrowserDialog1.SelectedPath;

                    this.FlowController.ProjectController.FlowMasterDataContext
                        .PreferenceManager.Application.ProjectsDirectory = folderBrowserDialog1.SelectedPath;

                }
            }
            catch (Exception err)
            {
                logger.ErrorException("Exception occurred.", err);
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            this.FlowController.ProjectController.FlowMasterDataContext.SavePreferences();
        }

        private void btnSelectArchiveFolder_Click(object sender, RoutedEventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();
            try
            {
                folderBrowserDialog1.SelectedPath = txtProjectArchiveDir.Text;
                if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                {
                    //this.FlowController.ProjectController.FlowMasterDataContext
                    //    .PreferenceManager["Capture.HotFolderDirectory"].NativeValue = folderBrowserDialog1.SelectedPath;

                    this.FlowController.ProjectController.FlowMasterDataContext
                        .PreferenceManager.Application.ProjectArchiveDirectory = folderBrowserDialog1.SelectedPath;

                }
            }
            catch (Exception err)
            {
                logger.ErrorException("Exception occurred.", err);
            }
        }

        private void btnUpdatePulledIQOrders_Click(object sender, RoutedEventArgs e)
        {
            if (this.FlowController.ProjectController.CurrentFlowProject != null)
            {
                int count = 0;
                foreach (OrderPackage op in this.FlowController.ProjectController.CurrentFlowProject.AllOnlineOrders)
                {
                    if (!this.FlowController.ProjectController.FlowMasterDataContext.PulledIQOrders.Any(o => o.IQOrderID == op.IQOrderID))
                    {
                        count++;
                        PulledIQOrder pio = new PulledIQOrder();
                        pio.IQOrderID = op.IQOrderID;
                        pio.IQOrderDate = op.IQOrderDate;
                        pio.FlowProjectGuid = this.FlowController.ProjectController.CurrentFlowProject.FlowProjectGuid;
                        this.FlowController.ProjectController.FlowMasterDataContext.PulledIQOrders.InsertOnSubmit(pio);
                        this.FlowController.ProjectController.FlowMasterDataContext.SubmitChanges();
                    }
                }
                FlowMessageBox msg = new FlowMessageBox("Master IQ Pulled Order List", count + " Items have been added to the Master IQ Pulled Order List");
                msg.CancelButtonVisible = false;
                msg.ShowDialog();
            }
            else
            {
                FlowMessageBox msg = new FlowMessageBox("No Project Loaded", "You can not update the master IQ pulled order list until you load a project");
                msg.CancelButtonVisible = false;
                msg.ShowDialog();
            }
        }

        private void btnSelectNetworkProjectsFolder_Click(object sender, RoutedEventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();
            try
            {
                folderBrowserDialog1.SelectedPath = txtNetworkProjectDir.Text;
                if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                {
                    //this.FlowController.ProjectController.FlowMasterDataContext
                    //    .PreferenceManager["Capture.HotFolderDirectory"].NativeValue = folderBrowserDialog1.SelectedPath;

                    this.FlowController.ProjectController.FlowMasterDataContext
                        .PreferenceManager.Application.NetworkProjectsDirectory = folderBrowserDialog1.SelectedPath;

                }
            }
            catch (Exception err)
            {
                logger.ErrorException("Exception occurred.", err);
            }
        }

        private void btnSelectLabOrderQueueFolder_Click(object sender, RoutedEventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();
            try
            {
                folderBrowserDialog1.SelectedPath = this.txtLabOrderQueueDir.Text;
                if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                {
                    //this.FlowController.ProjectController.FlowMasterDataContext
                    //    .PreferenceManager["Capture.HotFolderDirectory"].NativeValue = folderBrowserDialog1.SelectedPath;

                    this.FlowController.ProjectController.FlowMasterDataContext
                        .PreferenceManager.Application.LabOrderQueueDirectory = folderBrowserDialog1.SelectedPath;

                }
            }
            catch (Exception err)
            {
                logger.ErrorException("Exception occurred.", err);
            }
        }
    }
}