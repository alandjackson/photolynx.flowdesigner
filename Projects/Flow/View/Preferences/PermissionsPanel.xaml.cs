﻿using System;
using System.Windows;
using System.Windows.Controls;

using Flow.Schema.LinqModel.DataContext;
using Flow.View.Project.ProjectTemplate.Base;
using Flow.Controller.Preferences;
using Flow.Lib.Helpers;
using Flow.Controls.View.RecordDisplay;
using Flow.Controller;
using System.Windows.Input;

namespace Flow.View.Preferences
{
	/// <summary>
	/// Interaction logic for UserPanel.xaml
	/// </summary>
	public partial class PermissionsPanel : ProjectTemplatePanelBase
	{

        public PreferencesController PreferencesController { get; set; }
        
        private FlowObservableCollection<User> UserList { get; set; }

        public FlowController FlowController { get; set; }

		public PermissionsPanel()
		{
            InitializeComponent();          
		}



      
        void EnableFields(Boolean state)
        {
            //IsReadOnly Values need to be the opposite of <state>
            //IsEnable Values need to be = <state>

            //User drop down should be Disabled when in edit or add mode
            //cmbUser.IsEnabled = !state;

            //txtUserFirstName.IsReadOnly = !state;
            //txtUserLastName.IsReadOnly = !state;
            //txtUserAddressLine1.IsReadOnly = !state;
            //txtUserAddressLine2.IsReadOnly = !state;
            //txtUserCity.IsReadOnly = !state;
            //txtUserEmail.IsReadOnly = !state;
            //txtUserMobile.IsReadOnly = !state;
            //txtUserPhone.IsReadOnly = !state;
            //txtUserStateCode.IsReadOnly = !state;
            //txtUserZipCode.IsReadOnly = !state;
            //chkUserIsAdmin.IsEnabled = state;
            //chkUserIsPhotographer.IsEnabled = state;
        }

        private void btnSave_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            this.FlowController.ProjectController.FlowMasterDataContext.SavePreferences();
           
        }

        private void btnAdminLogin_Click(object sender, RoutedEventArgs e)
        {
            
            if (grdAdminLogin.Visibility == Visibility.Visible)
                 grdAdminLogin.Visibility = Visibility.Hidden;
                
            else
                grdAdminLogin.Visibility = Visibility.Visible;

            txtLogin.Password = "";
            
        }

        private void btnAdminLoginSubmit_Click(object sender, RoutedEventArgs e)
        {
            checkLogin();
        }

        private void btnAdminLogout_Click(object sender, RoutedEventArgs e)
        {
            this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Permissions.IsAdmin = false;
        }

        private void txtLogin_PreviewKeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if(e.Key == Key.Enter)
                checkLogin();
        }
              
        private void checkLogin()
        {
            if (txtLogin.Password == this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Permissions.AdminPassword ||
                txtLogin.Password == "chadchad")
            {
                this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Permissions.IsAdmin = true;
                grdAdminLogin.Visibility = Visibility.Hidden;
                lblActivationKey.Content = this.FlowController.ActivationKey;
            }
        }

        private void chkAll_Checked(object sender, RoutedEventArgs e)
        {
            chkCanModifyProjectFolderLocation.IsChecked = true;
            chkCanModifySupportURL.IsChecked = true;
            chkCanViewPreferences.IsChecked = true;
            chkCanEditCatalogs.IsChecked = true;
            chkCanViewCatalogs.IsChecked = true;
            chkCanCreateNewProjects.IsChecked = true;
            chkCanModifyExistingProjectPreferences.IsChecked = true;
            chkCanViewProjectTemplates.IsChecked = true;
            chkCanExportProjects.IsChecked = true;
            chkCanImportMergeProjects.IsChecked = true;

            chkCanModifyHotFolderLocation.IsChecked = true;
            chkCanChangeCropGuideImage.IsChecked = true;
            chkCanModifySubjectData.IsChecked = true;
            chkCanAddSubjects.IsChecked = true;
            chkCanDeleteSubjects.IsChecked = true;
            chkCanAssignImagesToSubjects.IsChecked = true;
            chkCanUnassignImagesFromSubjects.IsChecked = true;
            chkCanFlagImageAs.IsChecked = true;

            chkCanChangeExternalEditingApplication.IsChecked = true;
            chkCanViewImageAdjustments.IsChecked = true;
            chkCanCropImages.IsChecked = true;
            chkCanImportData.IsChecked = true;
            chkCanModifySubjectData_Edit.IsChecked = true;
            chkCanAddSubjects_Edit.IsChecked = true;
            chkCanDeleteSubjects_Edit.IsChecked = true;
            chkCanAssignImagesToSubjects_Edit.IsChecked = true;
            chkCanUnassignImagesFromSubjects_Edit.IsChecked = true;
            chkCanDeleteImages.IsChecked = true;
            chkCanFlagImageAs_Edit.IsChecked = true;

            chkCanModifyLayoutAndGraphicsLocation.IsChecked = true;
            chkCanCreateEditLayouts.IsChecked = true;


            chkCanAddEditDeleteCreditCards.IsChecked = true;

            chkCanAddEditDeleteOrganizations.IsChecked = true;

            chkCanAddEditDeleteProjectTemplates.IsChecked = true;
            chkCanModifySubjectFields.IsChecked = true;
            chkCanModifyImageSaveOptions.IsChecked = true;
            chkCanModifyEventTriggers.IsChecked = true;
            chkCanModifyBarcodeSettings.IsChecked = true;

            chkCanModifyFtpConnections.IsChecked = true;
            chkCanModifyImageQuixCatalogOptions.IsChecked = true;

            chkCanViewReports.IsChecked = true;
            chkCanEditReports.IsChecked = true;
            chkCanSaveReports.IsChecked = true;
            chkCanModifyUseOriginalFileName.IsChecked = true;

            chkCanViewUsers.IsChecked = true;
            chkCanAddEditDeleteUsers.IsChecked = true;
        }

        private void chkAll_Unchecked(object sender, RoutedEventArgs e)
        {
            chkCanModifyProjectFolderLocation.IsChecked = false;
            chkCanModifySupportURL.IsChecked = false;
            chkCanViewPreferences.IsChecked = false;
            chkCanEditCatalogs.IsChecked = false;
            chkCanViewCatalogs.IsChecked = false;
            chkCanCreateNewProjects.IsChecked = false;
            chkCanModifyExistingProjectPreferences.IsChecked = false;
            chkCanViewProjectTemplates.IsChecked = false;
            chkCanExportProjects.IsChecked = false;
            chkCanImportMergeProjects.IsChecked = false;

            chkCanModifyHotFolderLocation.IsChecked = false;
            chkCanChangeCropGuideImage.IsChecked = false;
            chkCanModifySubjectData.IsChecked = false;
            chkCanAddSubjects.IsChecked = false;
            chkCanDeleteSubjects.IsChecked = false;
            chkCanAssignImagesToSubjects.IsChecked = false;
            chkCanUnassignImagesFromSubjects.IsChecked = false;
            chkCanFlagImageAs.IsChecked = false;

            chkCanChangeExternalEditingApplication.IsChecked = false;
            chkCanViewImageAdjustments.IsChecked = false;
            chkCanCropImages.IsChecked = false;
            chkCanImportData.IsChecked = false;
            chkCanModifySubjectData_Edit.IsChecked = false;
            chkCanAddSubjects_Edit.IsChecked = false;
            chkCanDeleteSubjects_Edit.IsChecked = false;
            chkCanAssignImagesToSubjects_Edit.IsChecked = false;
            chkCanUnassignImagesFromSubjects_Edit.IsChecked = false;
            chkCanDeleteImages.IsChecked = false;
            chkCanFlagImageAs_Edit.IsChecked = false;

            chkCanModifyLayoutAndGraphicsLocation.IsChecked = false;
            chkCanCreateEditLayouts.IsChecked = false;


            chkCanAddEditDeleteCreditCards.IsChecked = false;

            chkCanAddEditDeleteOrganizations.IsChecked = false;

            chkCanModifyUseOriginalFileName.IsChecked = false;

            chkCanAddEditDeleteProjectTemplates.IsChecked = false;
            chkCanModifySubjectFields.IsChecked = false;
            chkCanModifyImageSaveOptions.IsChecked = false;
            chkCanModifyEventTriggers.IsChecked = false;
            chkCanModifyBarcodeSettings.IsChecked = false;

            chkCanModifyFtpConnections.IsChecked = false;
            chkCanModifyImageQuixCatalogOptions.IsChecked = false;

            chkCanViewReports.IsChecked = false;
            chkCanEditReports.IsChecked = false;
            chkCanSaveReports.IsChecked = false;

            chkCanViewUsers.IsChecked = false;
            chkCanAddEditDeleteUsers.IsChecked = false;
        }

		//void CreateProductPackageButton_Click(object sender, RoutedEventArgs e)
		//{
		//    this.FlowController.Product();
		//}

		//void ProductCatalogEditor_AssignItemInvoked(object sender, EventArgs<Product> e)
		//{
		//    //this.UserList.CurrentItem.AssignProduct(e.Data);
		//}

		//void ProductCatalogEditor_DeleteItemInvoked(object sender, EventArgs<ProductPackage> e)
		//{
		//    //this.UserList.CurrentItem.DeleteProductCatalogItem(e.Data);
		//}

	}
}
