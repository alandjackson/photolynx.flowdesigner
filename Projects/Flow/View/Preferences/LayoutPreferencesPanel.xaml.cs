﻿












using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Flow.Controller;
using Flow.Controller.Preferences;
using Flow.Controls.Extension;
using Flow.Controls.View.RecordDisplay;
using Flow.Lib;
using Flow.Lib.Helpers;
using Flow.Lib.Net;
using Flow.Lib.Preferences;
using Flow.Properties;
using Flow.Schema;
using Flow.Schema.LinqModel.DataContext;
using Flow.View.Project.ProjectTemplate.Base;
using StructureMap;
using System.Windows.Forms;
using NLog;

namespace Flow.View.Preferences
{

    /// <summary>
    /// Interaction logic for ApplicationPreferencesPanel.xaml
    /// </summary>
    public partial class LayoutPreferencesPanel : ProjectTemplatePanelBase
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public PreferencesController PreferencesController { get; set; }

        public FlowController FlowController { get; set; }

        private PreferenceManager PreferenceManager
        { get { return this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager; } }

        private LayoutPreferences LayoutPreferences
        {
            get { return this.PreferenceManager.Layout; }
        }

        private NotificationProgressInfo _notificationInfo;



        public LayoutPreferencesPanel()
        {
            InitializeComponent();
            //            gridFields.DataContext = this;
        }

      

        //void client_CurrentFileUploadProgression(object sender)
        //{
        //    IFTPClient client = sender as IFTPClient;
        //    progressNotification.UpdateNotification(client.ProgressInfo);
        //}

        private void btnSelectLayoutsFolder_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();
            try
            {
                folderBrowserDialog1.SelectedPath = txtLayoutDir.Text;
                if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                {
                    //this.FlowController.ProjectController.FlowMasterDataContext
                    //    .PreferenceManager["Capture.HotFolderDirectory"].NativeValue = folderBrowserDialog1.SelectedPath;

                    this.FlowController.ProjectController.FlowMasterDataContext
                        .PreferenceManager.Layout.LayoutsDirectory = folderBrowserDialog1.SelectedPath;

                }
            }
            catch (Exception err)
            {
                logger.ErrorException("Exception occurred.", err);
            }
        }

        private void btnSelectGraphicsFolder_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();
            try
            {
                folderBrowserDialog1.SelectedPath = txtGraphicsDir.Text;
                if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                {
                    //this.FlowController.ProjectController.FlowMasterDataContext
                    //    .PreferenceManager["Capture.HotFolderDirectory"].NativeValue = folderBrowserDialog1.SelectedPath;

                    this.FlowController.ProjectController.FlowMasterDataContext
                        .PreferenceManager.Layout.GraphicsDirectory = folderBrowserDialog1.SelectedPath;

                }
            }
            catch (Exception err)
            {
                logger.ErrorException("Exception occurred.", err);
            }
        }

       

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            this.FlowController.ProjectController.FlowMasterDataContext.SavePreferences();
        }
    }
}
