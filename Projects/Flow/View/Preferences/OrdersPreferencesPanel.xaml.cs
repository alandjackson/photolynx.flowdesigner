﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;

using Flow.Controller;
using Flow.Controller.Preferences;
using Flow.Controls;
using Flow.Lib;
using Flow.Model.Settings;
using Flow.Properties;
using Flow.Schema.LinqModel.DataContext;
using Flow.View.Project.ProjectTemplate.Base;
using StructureMap;
using System.ComponentModel;
using System.Threading;
using Flow.View.Dialogs;

namespace Flow.View.Preferences
{

    /// <summary>
    /// Interaction logic for CapturePreferencesPanel.xaml
    /// </summary>
    public partial class OrdersPreferencesPanel : ProjectTemplatePanelBase
    {
		protected static DependencyProperty CurrentCreditCardTypeProperty = DependencyProperty.Register(
			"CurrentCreditCardType", typeof(CreditCardType), typeof(OrdersPreferencesPanel)
		);

		public CreditCardType CurrentCreditCardType
		{
			get { return (CreditCardType)this.GetValue(CurrentCreditCardTypeProperty); }
			set { this.SetValue(CurrentCreditCardTypeProperty, value); }
		}


        public PreferencesController PreferencesController { get; set; }

        public FlowController FlowController { get; set; }

		private new FlowMasterDataContext FlowMasterDataContext
		{
			get { return this.DataContext as FlowMasterDataContext; }
		}

		public OrdersPreferencesPanel()
        {
            InitializeComponent();
        }

		private void btnAddCreditCardType_Click(object sender, RoutedEventArgs e)
		{
			CreditCardType newType = new CreditCardType("New Type");
	
			this.FlowMasterDataContext.PreferenceManager.Orders.CreditCardTypeCollection.Add(newType);

			lstCreditCardTypes.SelectedItem = newType;
		}

		private void btnCancelCreditCardTypeEdit_Click(object sender, RoutedEventArgs e)
		{
			int selectedIndex = lstCreditCardTypes.SelectedIndex;

			this.FlowMasterDataContext.RestorePreferences();

			lstCreditCardTypes.SelectedIndex = selectedIndex;
		}

		private void btnSaveCreditCardType_Click(object sender, RoutedEventArgs e)
        {
            this.FlowMasterDataContext.SavePreferences();
        }

        private void btnFixPackageSummary_Click(object sender, RoutedEventArgs e)
        {
            if (this.FlowController.ProjectController.CurrentFlowProject == null)
            {
                FlowMessageBox msg = new FlowMessageBox("Update Package Summary", "You must first load a project before you run this");
                msg.CancelButtonVisible = false;
                msg.ShowDialog();
                return;
            }

            this.FlowController.ApplicationPanel.IsEnabled = false;
            FlowBackgroundWorkerManager.RunWorker(MassUpdatePackageSummary, MassUpdatePackageSummary_Complete);

            
        }

        void MassUpdatePackageSummary(object sender, DoWorkEventArgs e)
        {

            
            int allcnt = this.FlowController.ProjectController.CurrentFlowProject.SubjectList.Count;
            int counter = 0;


            IShowsNotificationProgress progressNotification = ObjectFactory.GetInstance<IShowsNotificationProgress>();
            NotificationProgressInfo NotificationInfo = new NotificationProgressInfo("Update Package Summary", "Updating Package Summary......",counter,allcnt,1);
            NotificationInfo.NotificationProgress = progressNotification;
            progressNotification.ShowNotification(NotificationInfo);


            int waitCount = 0;
            string dots = ".";
            while (this.FlowController.ProjectController.CurrentFlowProject.initingSubjects)
            {
                dots += ".";
                NotificationInfo.Update("Waiting for subjects to initialize." + dots);
                waitCount++;
                Thread.Sleep(3000);

                if (waitCount == (4 * 60)) //if its taken longer than 4 minutes, bail
                    this.FlowController.ProjectController.CurrentFlowProject.initingSubjects = false;

            }

            foreach (Subject sub in this.FlowController.ProjectController.CurrentFlowProject.SubjectList)
            {
                counter++;
                NotificationInfo.Update("Updating Package Summary for " + sub.FormalFullName);
                NotificationInfo.Step();
                sub.UpdatePackageSummary();
            }
            NotificationInfo.Update("Finished Updating Package Summary");
            NotificationInfo.ShowDoneButton();
            
            this.FlowController.ProjectController.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();
        }

        void MassUpdatePackageSummary_Complete(object sender, RunWorkerCompletedEventArgs e)
        {
            this.FlowController.ApplicationPanel.IsEnabled = true;
        }
    }

}
