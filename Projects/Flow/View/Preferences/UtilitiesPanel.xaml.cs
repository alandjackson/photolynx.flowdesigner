﻿using System;
using System.Windows;
using System.Windows.Controls;

using Flow.Schema.LinqModel.DataContext;
using Flow.View.Project.ProjectTemplate.Base;
using Flow.Controller.Preferences;
using Flow.Lib;
using Flow.Lib.Helpers;
using Flow.Controls.View.RecordDisplay;
using Flow.Controller;
using Flow.View.Dialogs;
using System.Windows.Input;
using NLog;
using Flow.Lib.AsyncWorker;
using System.ComponentModel;
using System.Windows.Threading;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using StructureMap;
using System.Data;
using System.IO;



namespace Flow.View.Preferences
{
	/// <summary>
	/// Interaction logic for UtilitiesPanel.xaml
	/// </summary>
	public partial class UtilitiesPanel : ProjectTemplatePanelBase
	{
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public PreferencesController PreferencesController { get; set; }
        public FlowController FlowController { get; set; }

		public UtilitiesPanel()
		{
            InitializeComponent();

		}

       
        
        /// <summary>
        /// Save Preferences
        /// </summary>
        private void btnSave_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            logger.Info("Clicked Save Preferences button");

            this.FlowController.ProjectController.FlowMasterDataContext.SavePreferences();
        }

        /// <summary>
        /// Activate the confirmation panel for clearing all orders
        /// </summary>
        private void btnClearAllOrders_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            logger.Info("Clicked Clear All Orders button");

            this.btnClearAllOrders.IsEnabled = false;
            this.pnlConfirmClearAllOrders.Visibility = Visibility.Visible;
        }

        /// <summary>
        /// Clear all orders for the currently loaded project
        /// </summary>
        private void btnConfirmClearAllOrders_Click(object sender, RoutedEventArgs e)
        {
            logger.Info("Clicked Confirm All Clear All Orders button");

            // Cancel with a message if there isn't a project loaded
            if (this.FlowController.ProjectController == null || this.FlowController.ProjectController.CurrentFlowProject == null)
            {
                logger.Info("Not clearing orders because a project is not loaded");

                FlowMessageBox msg = new FlowMessageBox("Cannot Clear Orders", "Orders could not be cleared because no project is loaded.");
                msg.CancelButtonVisible = false;
                msg.ShowDialog();

                this.btnClearAllOrders.IsEnabled = true;
                this.pnlConfirmClearAllOrders.Visibility = Visibility.Collapsed;

                return;
            }

            NotificationProgressInfo progressInfo = new NotificationProgressInfo("Clearing All Orders", "Clearing all orders...");
            progressInfo.Display(true);

            // Remember the current subject
            Subject subject = this.FlowController.ProjectController.CurrentFlowProject.SubjectList.CurrentItem;

            // Clear all orders
            this.FlowController.ProjectController.CurrentFlowProject.ClearAllOrders();

            // Update UI
            this.btnClearAllOrders.IsEnabled = true;
            this.pnlConfirmClearAllOrders.Visibility = Visibility.Collapsed;
            this.FlowController.ProjectController.CurrentFlowProject.RefreshHasIncompleteProducts();
            
            //if (EditContentController != null)
            //    EditContentController.ApplyFilter();

            // Restore the original subject
            this.FlowController.ProjectController.CurrentFlowProject.SubjectList.CurrentItem = subject;

            progressInfo.Complete("Done clearing all orders.");
            logger.Info("Done clearing all orders");
        }

        /// <summary>
        /// Reset Clear All Orders confirmation
        /// </summary>
        private void btnCancelClearAllOrders_Click(object sender, RoutedEventArgs e)
        {
            logger.Info("Clicked Cancel Clear All Orders button");

            this.btnClearAllOrders.IsEnabled = true;
            this.pnlConfirmClearAllOrders.Visibility = Visibility.Collapsed;
        }

        private void btnRefreshExif_Click(object sender, RoutedEventArgs e)
        {
            logger.Info("Clicked Refresh Exif Data button");

            this.btnRefreshExif.IsEnabled = false;
            this.pnlConfirmExifRefresh.Visibility = Visibility.Visible;
        }

        private void btnCancelExifRefresh_Click(object sender, RoutedEventArgs e)
        {
            logger.Info("Clicked Cancel Refresh Exif Data button");

            this.btnRefreshExif.IsEnabled = true;
            this.pnlConfirmExifRefresh.Visibility = Visibility.Collapsed;
        }

        private NotificationProgressInfo progressInfo;

        private void btnConfirmExifRefresh_Click(object sender, RoutedEventArgs e)
        {

            logger.Info("Clicked Confirm Refresh All Exif button");

            // Cancel with a message if there isn't a project loaded
            if (this.FlowController.ProjectController == null || this.FlowController.ProjectController.CurrentFlowProject == null)
            {
                logger.Info("Not clearing orders because a project is not loaded");

                FlowMessageBox msg = new FlowMessageBox("Cannot Refresh Orientation", "Image Orientation could not be refreshed because a project is not loaded.");
                msg.CancelButtonVisible = false;
                msg.ShowDialog();

                this.btnRefreshExif.IsEnabled = true;
                this.pnlConfirmExifRefresh.Visibility = Visibility.Collapsed;

                return;
            }

            progressInfo = new NotificationProgressInfo("Refreshing All Exif Orientation Data", "Refreshing All Exif Orientation Data...");
            progressInfo.Display(true);

            FlowBackgroundWorker worker = FlowBackgroundWorkerManager.RunWorker(RefreshExif, RefreshExif_Completed);

        }
        void RefreshExif_Completed(object sender, RunWorkerCompletedEventArgs e)
        {
            // Update UI
            this.btnRefreshExif.IsEnabled = true;
            this.pnlConfirmExifRefresh.Visibility = Visibility.Collapsed;

            //if (EditContentController != null)
            //    EditContentController.ApplyFilter();


            progressInfo.Complete("Done refreshing Exif Orientation orders.");
            logger.Info("Done refreshing Exif Orientation orders.");
        }
        void RefreshExif(object sender, DoWorkEventArgs e)
         {
           

            // Remember the current subject
            //Subject subject = this.FlowController.ProjectController.CurrentFlowProject.SubjectList.CurrentItem;

            // Refresh Exif
            this.FlowController.ProjectController.CurrentFlowProject.RefreshAllImageExif(progressInfo);

            // Restore the original subject
            //this.FlowController.ProjectController.CurrentFlowProject.SubjectList.CurrentItem = subject;


           


        }

        private void btnRefreshPackages_Click(object sender, RoutedEventArgs e)
        {
            logger.Info("Clicked Refresh Packages button");

            this.btnRefreshPackages.IsEnabled = false;
            this.pnlConfirmPackageRefresh.Visibility = Visibility.Visible;
        }

        private void btnConfirmPackageRefresh_Click(object sender, RoutedEventArgs e)
        {
            logger.Info("Clicked Confirm Refresh All Packages button");

            // Cancel with a message if there isn't a project loaded
            if (this.FlowController.ProjectController == null || this.FlowController.ProjectController.CurrentFlowProject == null)
            {
                logger.Info("Not refreshing packages because a project is not loaded");

                FlowMessageBox msg = new FlowMessageBox("Cannot Refresh Packages", "Packages could not be reassigned because a project is not loaded.");
                msg.CancelButtonVisible = false;
                msg.ShowDialog();

                this.btnRefreshPackages.IsEnabled = true;
                this.pnlConfirmPackageRefresh.Visibility = Visibility.Collapsed;

                return;
            }

            progressInfo = new NotificationProgressInfo("Refreshing All Assigned Packages", "Refreshing All Assogmed Packages...");
            progressInfo.Display(true);

            FlowBackgroundWorker worker = FlowBackgroundWorkerManager.RunWorker(RefreshPackages, RefreshPackages_Completed);
        }

        private void btnCancelPackageRefresh_Click(object sender, RoutedEventArgs e)
        {
            logger.Info("Clicked Cancel Refresh Packages button");

            this.btnRefreshPackages.IsEnabled = true;
            this.pnlConfirmPackageRefresh.Visibility = Visibility.Collapsed;
        }

        void RefreshPackages_Completed(object sender, RunWorkerCompletedEventArgs e)
        {
            // Update UI
            this.btnRefreshPackages.IsEnabled = true;
            this.pnlConfirmPackageRefresh.Visibility = Visibility.Collapsed;

            //if (EditContentController != null)
            //    EditContentController.ApplyFilter();


            progressInfo.Complete("Done refreshing packages.");
            logger.Info("Done refreshing packages.");
        }
        void RefreshPackages(object sender, DoWorkEventArgs e)
        {

            this.RefreshAllPackages(progressInfo);

        }


         public void RefreshAllPackages(NotificationProgressInfo progressInfo)
        {
            if (progressInfo != null)
                progressInfo.Update("Refreshing Packages for Subject {0} of {1}", 0, this.FlowController.ProjectController.CurrentFlowProject.SubjectList.View.Count, 1, true);
            foreach (Subject subject in this.FlowController.ProjectController.CurrentFlowProject.SubjectList.View)
            {
                if (progressInfo != null)
                    progressInfo++;
                foreach (OrderPackage o in subject.SubjectOrder.OrderPackageList)
                {

                    if ((o.LabOrderID != null && o.LabOrderID >= 0) || (o.IQOrderID != null && o.IQOrderID > 0) || (o.WebOrderID != null && o.WebOrderID > 0))
                        continue;

                    //SubjectImage orderedImage = o.SubjectImage;
                    string packageName = o.ProductPackageDesc;
                    string background = o.OrderProducts[0].GreenScreenBackground;
                    ProductPackage newPackage = this.FlowController.ProjectController.CurrentFlowProject.FlowCatalog.GetProductPackage(packageName);
                    if(newPackage != null)
                    {

                        SubjectImage si = o.FirstImage;
                        if (si == null && (o.OrderProducts != null && o.OrderProducts.Count > 0 && o.OrderProducts[0].OrderProductImages != null && o.OrderProducts[0].OrderProductImages.Count > 0))
                        {
                            si = o.OrderProducts[0].OrderProductImages[0].SubjectImage;
                        }

                        subject.RemoveOrderPackage(o);

                        OrderPackage newlyAddedOrderPackage = null;
                        if(si != null)
                        {
                            newlyAddedOrderPackage = subject.AddOrderPackage(newPackage, si);
                        } else
                            newlyAddedOrderPackage = subject.AddOrderPackage(newPackage);

                        if(background != null)
                        {
                            foreach(OrderProduct oprod in newlyAddedOrderPackage.OrderProducts)
                            {
                                oprod.GreenScreenBackground = background;
                            }
                        }
                    }
                    else
                    {
                        this.Dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                        {
                           FlowMessageBox msg = new FlowMessageBox("Missing Package", "The Following Package is missing from your catalog: " + packageName + "\n\nCorrect this package and run this utility again.");
                           msg.CancelButtonVisible = false;
                           msg.ShowDialog();
                           
                       }));
                        return;
                    }
                        
                }
            }
            this.FlowController.ProjectController.CurrentFlowProject.FlowProjectDataContext.SubmitChanges();

        }

         private void btnRemoveOfflineErrors_Click(object sender, RoutedEventArgs e)
         {
             this.FlowController.ProjectController.FlowMasterDataContext.PreferenceManager.Application.UnsentErrorsClear();
             this.FlowController.ProjectController.FlowMasterDataContext.SavePreferences();
         }

         private void btnUpdateCustomReports_Click(object sender, RoutedEventArgs e)
         {

             if (this.FlowController.ProjectController.CurrentFlowProject == null)
             {
                 FlowMessageBox msg = new FlowMessageBox("No Project Loaded", "Please load a project before running this tool");
                 msg.CancelButtonVisible = false;
                 msg.ShowDialog();
                 return;
             }
             this.FlowController.ProjectController.FlowMasterDataContext.CustomReportFields = new ObservableCollection<FieldDetail>();
             //this.FlowController.ProjectController.FlowMasterDataContext.CustomReportFields.Add(new FieldDetail("TicketCodeBarCode", -1));
             //this.FlowController.ProjectController.FlowMasterDataContext.CustomReportFields.Add(new FieldDetail("FormalFullName", -1));
             //this.FlowController.ProjectController.FlowMasterDataContext.CustomReportFields.Add(new FieldDetail("ProjectOrganizationName", -1));
             if (FlowController.ProjectController.CurrentFlowProject != null)
             {
                 int i=0;
                 foreach (ProjectSubjectField fieldName in FlowController.ProjectController.CurrentFlowProject.ProjectSubjectFieldList)
                 {
                     this.FlowController.ProjectController.FlowMasterDataContext.CustomReportFields.Add(new FieldDetail(fieldName.ProjectSubjectFieldDesc, i++));
                 }
             }

             this.FlowController.ProjectController.FlowMasterDataContext.UpdateCustomReportFields();
             this.lstCustomFields.UpdateLayout();

             string exportDB = Path.Combine(FlowContext.FlowTempDirPath, "flowDB.xml");
             if (File.Exists(exportDB))
                 File.Delete(exportDB);

             
             DataSet ds = this.FlowController.ProjectController.CurrentFlowProject.GetCustomReportDataSet(false);

             ds.WriteXml(exportDB, XmlWriteMode.WriteSchema);
             
         }

        


         private void btnBatchArchive_Click(object sender, RoutedEventArgs e)
         {
             List<FlowProject> archiveProjects = new List<FlowProject>();

            foreach (FlowProject item in FlowController.ProjectController.FlowMasterDataContext.FlowProjectList)
            {
                if (item.QueueForArchive)
                {
                    archiveProjects.Add(item);
                }
            }
            if (archiveProjects.Count > 0)
            {
                FlowMessageBox msg = new FlowMessageBox("Archive Prjoect", "Are you sure you want to archive " + archiveProjects.Count + " project(s)");
                if (msg.ShowDialog() == true)
                {
                    this.FlowController.ApplicationPanel.IsEnabled = false;

                    //foreach (FlowProject fp in archiveProjects)
                    //{
                    //    this.FlowController.ProjectController.ArchiveDeleteProject(fp, true);
                    //    System.Threading.Thread.Sleep(8000);
                    //    int j = 0;
                    //    while (this.FlowController.ProjectController.IsExporting)
                    //    {
                    //        j++;
                    //        System.Threading.Thread.Sleep(5000);
                    //        if (j > 5000)
                    //            break;
                    //    }

                    //}
                    //this.FlowController.ApplicationPanel.IsEnabled = true;
                    progressInfo = new NotificationProgressInfo("Project Archive", "Archiving All Projects...", 0, archiveProjects.Count, 1);
                    progressInfo.Display(true);

                    FlowBackgroundWorker worker = FlowBackgroundWorkerManager.RunWorker(ArchiveProjects, ArchiveProjects_Completed);

                }
            }
            else
            {
                FlowMessageBox msg = new FlowMessageBox("Archive Prjoect", "Please select some projects to archive");
                msg.ShowDialog();

            }

         }
         void ArchiveProjects_Completed(object sender, RunWorkerCompletedEventArgs e)
         {
             progressInfo.Complete("Done archiving projects.");
             logger.Info("Done archiving projects.");
             this.FlowController.ApplicationPanel.IsEnabled = true;
         }
         void ArchiveProjects(object sender, DoWorkEventArgs e)
         {

             List<FlowProject> archiveProjects = new List<FlowProject>();

             foreach (FlowProject item in FlowController.ProjectController.FlowMasterDataContext.FlowProjectList)
             {
                 if (item.QueueForArchive)
                 {
                     archiveProjects.Add(item);
                 }
             }

             
                        
             int i = 0;
             foreach (FlowProject fp in archiveProjects)
             {
                 i++;
                 progressInfo.Update("Archiving Project: " + fp.FlowProjectName);
                 progressInfo.Step();
                 //System.Threading.Thread.Sleep(5000);
                 this.Dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>{
                            this.FlowController.ProjectController.ArchiveDeleteProject(fp, true);
                  }));
                 System.Threading.Thread.Sleep(15000);      
             }
                       
         }

         private void chkCheckAll_Checked(object sender, RoutedEventArgs e)
         {
             foreach (FlowProject item in FlowController.ProjectController.FlowMasterDataContext.FlowProjectList)
             {
                 item.QueueForArchive = true;
             }
         }

         private void chkCheckAll_Unchecked(object sender, RoutedEventArgs e)
         {
             foreach (FlowProject item in FlowController.ProjectController.FlowMasterDataContext.FlowProjectList)
             {
                 item.QueueForArchive = false;
                
             }
         }

	}
}
