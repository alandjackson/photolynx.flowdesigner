﻿using System;
using System.Windows;
using System.Windows.Controls;

using Flow.Schema.LinqModel.DataContext;
using Flow.View.Project.ProjectTemplate.Base;
using Flow.View.Dialogs;
using Flow.Controller.Preferences;
using Flow.Lib.Helpers;
using Flow.Controls.View.RecordDisplay;
using Flow.Controller;
using NLog;

namespace Flow.View.Preferences
{
	/// <summary>
	/// Interaction logic for UserPanel.xaml
	/// </summary>
	public partial class UserPanel : ProjectTemplatePanelBase
	{

        public PreferencesController PreferencesController { get; set; }

        public event EventHandler<EventArgs<User>> AssignItemInvoked = delegate { };
		public event EventHandler<EventArgs<User>> DeleteItemInvoked = delegate { };

        private static Logger logger = LogManager.GetCurrentClassLogger();

        private FlowObservableCollection<User> UserList { get; set; }

        public FlowController FlowController { get; set; }


        protected DependencyProperty EditFieldPanelEnabledProperty = DependencyProperty.Register(
            "EditFieldPanelEnabled", typeof(bool), typeof(UserPanel)
        );
        protected bool EditFieldPanelEnabled
        {
            get { return (bool)this.GetValue(EditFieldPanelEnabledProperty); }
            set { this.SetValue(EditFieldPanelEnabledProperty, value); }
        }

		public UserPanel()
		{
            InitializeComponent();          
		}

        public void InitButtonStrip()
        {
            UserList = ((FlowMasterDataContext)DataContext).UserList;
            this.UserOptionsEditButtonStrip.SetSourceCollection<User>(
               this.UserList
           );

            this.UserOptionsEditButtonStrip.EditStateChanged +=
                new EventHandler<EditStateChangedEventArgs>(CollectionEditButtonStrip_EditStateChanged);

            this.UserOptionsEditButtonStrip.EditState = EditState.View;
            EnableFields(false);
        }

        private void cmbUser_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }


		private void ProductRecordDisplay_AssignItemInvoked(object sender, EventArgs<User> e)
		{
			this.AssignItemInvoked(sender, e);
		}

		private void ProductCatalogItemDisplay_DeleteItemInvoked(object sender, EventArgs<User> e)
		{
			this.DeleteItemInvoked(sender, e);
		}

        void CollectionEditButtonStrip_EditStateChanged(object sender, Flow.Controls.View.RecordDisplay.EditStateChangedEventArgs e)
        {
            switch (e.OldEditState)
            {
                case EditState.View:

                    //					this.SubjectRecordWrapDisplay.IsReadOnly = false;

                    switch (e.NewEditState)
                    {
                        case EditState.Add:
                            EditFieldPanelEnabled = true;
                            this.UserList.CreateNew();
                            break;

                        case EditState.Delete:
                            this.DeleteConfirmationPrompt.Visibility = Visibility.Visible;
                            break;

                        case EditState.Edit:
                            EditFieldPanelEnabled = true;
                            break;
                    }
                    break;


                case EditState.Add:
                    switch (e.NewEditState)
                    {
                        case EditState.Save:
                            if (this.txtUserFirstName.Text == null || this.txtUserFirstName.Text.Length < 1 ||
                                this.txtUserLastName.Text == null || this.txtUserLastName.Text.Length < 1)
                            {
                                FlowMessageBox msg = new FlowMessageBox("Required Fields Missing", "Please fill in required fields before saving. First Name and Last Name are required.");
                                msg.CancelButtonVisible = false;
                                msg.ShowDialog();
                                this.UserOptionsEditButtonStrip.EditState = EditState.Add;
                                break;
                            }

                            //this.UserList.CurrentItem..FlowMasterDataContext =
                            //    this.FlowController.ProjectController.FlowMasterDataContext;

                            this.UserList.CommitNew();
                            this.FlowController.ProjectController.FlowMasterDataContext.SubmitChanges();
                            this.UserOptionsEditButtonStrip.EditState = EditState.View;
                            break;

                        case EditState.Cancel:
                            this.UserList.CancelNew();
                            this.UserOptionsEditButtonStrip.EditState = EditState.View;
                            break;
                    }
                    break;


                case EditState.Edit:
                    switch (e.NewEditState)
                    {
                        case EditState.Save:
                            if (this.txtUserFirstName.Text == null || this.txtUserFirstName.Text.Length < 1 ||
                                this.txtUserLastName.Text == null || this.txtUserLastName.Text.Length < 1)
                            {
                                FlowMessageBox msg = new FlowMessageBox("Required Fields Missing", "Please fill in required fields before saving. First Name and Last Name are required.");
                                msg.CancelButtonVisible = false;
                                msg.ShowDialog();
                                this.UserOptionsEditButtonStrip.EditState = EditState.Edit;
                                break;
                            }

                            this.FlowController.ProjectController.FlowMasterDataContext.SubmitChanges();
                            this.UserOptionsEditButtonStrip.EditState = EditState.View;
                            break;

                        case EditState.Cancel:
                            this.UserList.RevertCurrent();
                            this.UserOptionsEditButtonStrip.EditState = EditState.View;
                            break;
                    }

                    break;


                case EditState.Delete:
                    this.DeleteConfirmationPrompt.Visibility = Visibility.Collapsed;
                    break;

            }

           if(e.NewEditState == EditState.View)
               EditFieldPanelEnabled = false;
        }

        void EnableFields(Boolean state)
        {
            //IsReadOnly Values need to be the opposite of <state>
            //IsEnable Values need to be = <state>

            //User drop down should be Disabled when in edit or add mode
            //cmbUser.IsEnabled = !state;

            //txtUserFirstName.IsReadOnly = !state;
            //txtUserLastName.IsReadOnly = !state;
            //txtUserAddressLine1.IsReadOnly = !state;
            //txtUserAddressLine2.IsReadOnly = !state;
            //txtUserCity.IsReadOnly = !state;
            //txtUserEmail.IsReadOnly = !state;
            //txtUserMobile.IsReadOnly = !state;
            //txtUserPhone.IsReadOnly = !state;
            //txtUserStateCode.IsReadOnly = !state;
            //txtUserZipCode.IsReadOnly = !state;
            //chkUserIsAdmin.IsEnabled = state;
            //chkUserIsPhotographer.IsEnabled = state;
        }
              
        void ConfirmDeleteButton_Click(object sender, RoutedEventArgs e)
        {
            // Check to see if any projects are using the user we're about to delete
            foreach (FlowProject project in this.FlowController.ProjectController.FlowMasterDataContext.FlowProjects)
            {
                if (this.UserList.CurrentItem.UserID == project.OwnerUserID)
                {
                    logger.Warn("Tried to delete a user that is set as a photographer on a project: user='{0}', project='{1}'",
                        this.UserList.CurrentItem.FormalFullName, project.FullProjectDisplayName);

                    FlowMessageBox msg = new FlowMessageBox("Can't Delete User", "Unable to delete this user because it is currently assigned as a photographer to one or more projects.");
                    msg.CancelButtonVisible = false;
                    msg.ShowDialog();

                    // Cancel
                    this.UserOptionsEditButtonStrip.EditState = EditState.View;
                    return;
                }
            }

            // Delete the user
            this.UserList.DeleteCurrent();
            this.FlowController.ProjectController.FlowMasterDataContext.SubmitChanges();
            this.UserOptionsEditButtonStrip.EditState = EditState.View;
        }

		void CancelDeleteButton_Click(object sender, RoutedEventArgs e)
		{
			this.UserOptionsEditButtonStrip.EditState = EditState.View;
		}

		//void CreateProductPackageButton_Click(object sender, RoutedEventArgs e)
		//{
		//    this.FlowController.Product();
		//}

		//void ProductCatalogEditor_AssignItemInvoked(object sender, EventArgs<Product> e)
		//{
		//    //this.UserList.CurrentItem.AssignProduct(e.Data);
		//}

		//void ProductCatalogEditor_DeleteItemInvoked(object sender, EventArgs<ProductPackage> e)
		//{
		//    //this.UserList.CurrentItem.DeleteProductCatalogItem(e.Data);
		//}

	}
}