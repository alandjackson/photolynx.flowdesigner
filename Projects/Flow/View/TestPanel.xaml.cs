﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;
using System.Xml.Linq;

using Flow.Controls.ProgressNotificationControl;
using Flow.Lib.Helpers.ImageQuix;

namespace Flow.View
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class TestPanel : UserControl
    {
        #region Private objects
        ProgressNotificationControlData notification1;
        ProgressNotificationControlData notification2;
        ProgressNotificationControlData notification3;
        List<System.Windows.Controls.Image> imgList;
        System.Windows.Forms.Timer timer;
        #endregion

        public TestPanel()
        {
            InitializeComponent();
            pnc1.Visibility = Visibility.Collapsed;
        }

        ~TestPanel()
        {
            notification1 = null;
            notification2 = null;
            notification3 = null;
            imgList = null;
            timer = null;
        }


        private void btnRunTests_Click(object sender, RoutedEventArgs e)
        {
            performUnitTests();
        }

        private void btnShowHideNotifications_Click(object sender, RoutedEventArgs e)
        {
            pnc1.Visibility = (pnc1.Visibility == Visibility.Visible ? Visibility.Collapsed : Visibility.Visible);
        }

        private void btnAction1_Click(object sender, RoutedEventArgs e)
        {
            createNotification1();
        }

        private void btnAction2_Click(object sender, RoutedEventArgs e)
        {
            createNotification2();
        }

        private void btnAction3_Click(object sender, RoutedEventArgs e)
        {
            createNotification3();
        }

        private void btnRemoveAllNotifications_Click(object sender, RoutedEventArgs e)
        {
            killNotification1();
            killNotification2();
            killNotification3();

            // Alternatively, this method can be called to remove all notifications
            // but it does not updates the client interface, only the control.
            //pnc1.RemoveAllNotifications();
        }


        private void btnShowImgAdjust_Click(object sender, RoutedEventArgs e)
        {
            //Window w = new Window();
            System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog();
            ofd.ShowDialog();
            //string dir = System.IO.Directory.GetCurrentDirectory();
            Flow.Schema.LinqModel.DataContext.FlowProjectDataContext data = new Flow.Schema.LinqModel.DataContext.FlowProjectDataContext(ofd.FileName);

            Flow.Controller.Edit.ImageAdjustments.ImageAdjustmentsController controller = new Flow.Controller.Edit.ImageAdjustments.ImageAdjustmentsController(null);

            throw( new NotImplementedException("ImageListContoller.ImageTable have been depreciated."));
            //controller.ImageListContoller.ImageTable = data.Images;
            //PanelDisplay.Children.Add(controller.ImageAdjustmentsPanel);
            
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            if (notification2 != null)
            {
                // increase progress bar value
                notification2.ProgressBarValue += 1;

                // set thumb image at every 20 per cent of the progress bar
                if (notification2.ProgressBarValue % 20 == 0 && Convert.ToInt32(Math.Ceiling(notification2.ProgressBarValue / 20)) <= imgList.Count)
                    notification2.ThumbsImage = imgList[Convert.ToInt32(Math.Ceiling(notification2.ProgressBarValue / 20)) - 1];

                // update notification
                pnc1.UpdateNotification(notification2);

                // clean up memory when progress is finished
                if (notification2.ProgressBarValue >= 100)
                {
                    killNotification2();
                }
            }
        }

        #region Private methods
        private void createNotification1()
        {
            try
            {
                // Create a notification
                if (notification1 == null)
                {
                    notification1 = new ProgressNotificationControlData();
                    notification1.StatusTextTop = "Action 1";
                    notification1.StatusTextBottom = "Doing something...";

                    // create image from application's resource to be set as notification's icon
                    Image img = Util.LoadImageFromResource("../images/camera.png");
                    notification1.IconImage = img;

                    pnc1.CreateNotification(notification1);
                    pnc1.Visibility = Visibility.Visible;
                    btnAction1.Content = "Kill Action 1";
                }
                else
                {
                    killNotification1();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void createNotification2()
        {
            try
            {
                // Create a notification
                if (notification2 == null)
                {
                    notification2 = new ProgressNotificationControlData();
                    notification2.ProgressBarValue = 0;
                    notification2.StatusTextBottom = "Please wait a while...";

                    // create image from application's resource to be set as notification's icon
                    Image img = Util.LoadImageFromResource("../images/camera_edit.png");
                    notification2.IconImage = img;

                    pnc1.CreateNotification(notification2);
                    pnc1.Visibility = Visibility.Visible;
                    btnAction2.Content = "Kill Action 2";

                    // simulate processing
                    SimulateProcessUsingProgressBar();
                }
                else
                {
                    killNotification2();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void createNotification3()
        { 
            try
            {
                // Create a notification
                if (notification3 == null)
                {
                    notification3 = new ProgressNotificationControlData();
                    notification3.StatusTextTop = "Action 3";
                    notification3.StatusTextBottom = "Doing another something...";

                    // create image from application's resource to be set as notification's icon
                    Image img = Util.LoadImageFromResource("../images/camera_link.png");
                    notification3.IconImage = img;

                    pnc1.CreateNotification(notification3);
                    pnc1.Visibility = Visibility.Visible;
                    btnAction3.Content = "Kill Action 3";
                }
                else
                {
                    killNotification3();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void killNotification1()
        {
            if (notification1 != null)
            {
                pnc1.RemoveNotification(notification1);
                notification1 = null;
                btnAction1.Content = "Fire Action 3";
            }
        }

        private void killNotification2()
        {
            if (notification2 != null)
            {
                pnc1.RemoveNotification(notification2);
                notification2 = null;
                imgList = null;
                if (timer != null)
                    timer.Stop();
                timer = null;
                btnAction2.Content = "Fire Action 2";
            }
        }

        private void killNotification3()
        {
            if (notification3 != null)
            {
                pnc1.RemoveNotification(notification3);
                notification3 = null;
                btnAction3.Content = "Fire Action 3";
            }
        }

        private void SimulateProcessUsingProgressBar()
        {
            imgList = new List<Image>();
            imgList.Add(Util.LoadImageFromResource("../images/android1.png"));
            imgList.Add(Util.LoadImageFromResource("../images/M2.jpg"));
            imgList.Add(Util.LoadImageFromResource("../images/cubes.jpg"));
            imgList.Add(Util.LoadImageFromResource("../images/android2.png"));

            timer = new System.Windows.Forms.Timer();
            timer.Interval = 100;
            timer.Tick += new EventHandler(timer_Tick);
            timer.Start();
        }

        private void performUnitTests()
        {
            // Kill all notifications if exist
            killNotification1();
            killNotification2();
            killNotification3();

            // Create all notifications
            createNotification1();
            createNotification2();
            createNotification3();

            // Update notification 1 setting its bottom status text
            if (notification1 != null)
            {
                notification1.StatusTextBottom = "Doing another action";
                pnc1.UpdateNotification(notification1);
            }

            // Update notification 3 setting its thumb image
            if (notification3 != null)
            {
                notification3.ThumbsImage = imgList[3];
                pnc1.UpdateNotification(notification3);
            }

            // Update notification 1 setting its thumb image
            if (notification1 != null)
            {
                notification1.ThumbsImage = imgList[2];
                pnc1.UpdateNotification(notification1);
            }

            // Update notification 2 setting its bottom status text
            if (notification2 != null)
            {
                notification2.StatusTextBottom = "View the progress bar...";
                pnc1.UpdateNotification(notification2);
            }
        }
        #endregion

		private void btnXDocTest_Click(object sender, RoutedEventArgs e)
		{
			//string catFile = @"E:\products.xml";

			//if (File.Exists(catFile))
			//{
			//    Collection<ImageQuixCatalog> cats = new Collection<ImageQuixCatalog>();
	
			//    XDocument doc = XDocument.Load(catFile);
	
			//    foreach(XElement catalogDefinition in doc.Descendants("Catalog"))
			//    {
			//        cats.Add(new ImageQuixCatalog(catalogDefinition));
			//    }
			//}
		}

		private void btnPudImport_Click(object sender, RoutedEventArgs e)
		{
			//string pudFile = @"E:\RipLynx Units.pud";

			//if (File.Exists(pudFile))
			//{
			//    using (Flow.Schema.LinqModel.DataContext.FlowMasterDataContext f = new Flow.Schema.LinqModel.DataContext.FlowMasterDataContext())
			//    {
			//        f.ImportPud(pudFile);
			//    }
			//}
		}

		private void btnIQRequest_Click(object sender, RoutedEventArgs e)
		{

			//string uri = "http://www.testquix.com/flow/iqProducts.php";

			//Collection<ImageQuixCatalogListing> c =
			//    ImageQuixUtility.GetImageQuixCatalogListFromServer(uri);

			//Collection<XElement> catalogs = new Collection<XElement>();


			//foreach (ImageQuixCatalogListing item in c)
			//{
			//    catalogs.Add(ImageQuixUtility.GetImageQuixCatalogFromServer(uri, item.ID));
			//}


			Flow.Schema.LinqModel.DataContext.FlowMasterDataContext dc = new Flow.Schema.LinqModel.DataContext.FlowMasterDataContext();

			string catFile = @"E:\products.xml";

			if (File.Exists(catFile))
			{
				XDocument doc = XDocument.Load(catFile);

				foreach (XElement catalogDefinition in doc.Descendants("Catalog"))
				{
					//Flow.Schema.LinqModel.DataContext.ImageQuixCatalog.CreateImageQuixCatalog(catalogDefinition, dc);

					//Flow.Schema.LinqModel.DataContext.ImageQuixCatalog c =
					//    new Flow.Schema.LinqModel.DataContext.ImageQuixCatalog(catalogDefinition, dc);

					//dc.ImageQuixCatalogList.Add(c);

				}
			}
		
		}

        // DEPREACATED by Tom's new project work.
        //private void btnNewProjectWizard_Click(object sender, RoutedEventArgs e)
        //{
        //    showNewProjectWizard();
        //}

        //private void showNewProjectWizard()
        //{
        //    // Create new project wizard dialog
        //    Flow.NewProjectWizard.WizardDialogBox wizard = null;
        //    try
        //    {
        //        wizard = new Flow.NewProjectWizard.WizardDialogBox();
        //        if ((bool)wizard.ShowDialog())
        //        {
        //            string projectInfoText = "";
        //            foreach (NewProjectWizard.ProjectInfo pi in wizard.ProjectSettings.ProjectInfoList)
        //                projectInfoText += pi.FieldName + ": " + pi.FieldValue + "\n";

        //            MessageBox.Show(projectInfoText, "ProjectSettings.ProjectInfoList", MessageBoxButton.OK, MessageBoxImage.Information);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message, "Wizard Error", MessageBoxButton.OK, MessageBoxImage.Error);
        //        return;
        //    }
        //    finally
        //    {
        //        wizard = null;
        //    }
        //}

    }
}
