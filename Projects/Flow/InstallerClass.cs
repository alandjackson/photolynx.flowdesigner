﻿using System;
using System.Diagnostics;
using System.Windows.Forms;
using System.Collections;
using System.ComponentModel;
using System.Configuration.Install;
using System.Reflection;
using System.IO;
using Microsoft.Win32;

namespace Flow
{
    [RunInstaller(true)]
    public class InstallerClass : System.Configuration.Install.Installer
    {
        public InstallerClass()
            : base()
        {
            // Attach the 'Committed' event.
            this.Committed += new InstallEventHandler(MyInstaller_Committed);
            // Attach the 'Committing' event.
            this.Committing += new InstallEventHandler(MyInstaller_Committing);
        }

        // Event handler for 'Committing' event.
        private void MyInstaller_Committing(object sender, InstallEventArgs e)
        {
            //Console.WriteLine("");
            //Console.WriteLine("Committing Event occurred.");
            //Console.WriteLine("");
        }

        // Event handler for 'Committed' event.
        private void MyInstaller_Committed(object sender, InstallEventArgs e)
        {
            try
            {
                RegistryKey masterKey = Registry.LocalMachine.CreateSubKey
                ("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\App Paths");
                RegistryKey flowRegkey = masterKey.CreateSubKey("Flow.exe");
                flowRegkey.SetValue("", Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\Flow.exe");
                flowRegkey.SetValue("Path", Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\Flow.exe");
                flowRegkey.Close();
                masterKey.Close();

                Directory.SetCurrentDirectory(Path.GetDirectoryName
                (Assembly.GetExecutingAssembly().Location));
                //Process.Start(Path.Combine(Path.GetDirectoryName(
                //  Assembly.GetExecutingAssembly().Location), "Flow.exe"));
            }
            catch
            {
                // Do nothing... 
            }
        }

        // Override the 'Install' method.
        public override void Install(IDictionary savedState)
        {
            base.Install(savedState);
        }

        // Override the 'Commit' method.
        public override void Commit(IDictionary savedState)
        {
            base.Commit(savedState);
        }

        // Override the 'Rollback' method.
        public override void Rollback(IDictionary savedState)
        {
            base.Rollback(savedState);
        }
    }
}
