﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Flow.Controller;
using Flow.Controller.Capture;
using Flow.Controller.Edit;
using Flow.Controller.Edit.ImageAdjustments;
using Flow.Controller.Catalog;
using Flow.Designer;
using Flow.Designer.Controller;
using Flow.Designer.Helpers.SurfaceUi;
//using Flow.Schema.LinqModel.FlowMaster;
using Flow.Designer.Lib.ProjectInterop;
using Flow.Designer.Lib.Service;
using Flow.Designer.Lib.SurfaceUi;
using Flow.Designer.Model;
using Flow.Designer.View;
using Flow.Helpers.FlowController;
using Flow.Lib;
using Flow.Lib.AsyncWorker;
using Flow.Lib.Persister;
using Flow.Model.Capture;
using Flow.Service;

using StructureMap;
using StructureMap.Attributes;
using StructureMap.Configuration.DSL;
using Flow.Controller.Preferences;
using Flow.Lib.Activation;
using System.Windows;
using Flow.Model.Designer;
using Flow.Controller.Exceptions;
using Flow.Lib.Exceptions;

namespace Flow.Config
{
    public static class Bootstrapper
    {
        public static void Initialize()
        {
            LicenseXceed();
            ConfigureStructureMap();
			FlowContext.ClearTempDirectory();
        }


        public static void LicenseXceed()
        {
            Xceed.Wpf.DataGrid.Licenser.LicenseKey = "DGP319WAN7RUH1YJN2A";
        }

        public static void ConfigureStructureMap()
        {
            ObjectFactory.Initialize(x => 
            { 
                x.AddRegistry<FlowRegistry>();
                x.AddRegistry<DesignerRegistry>();
                x.AddRegistry<LibRegistry>();
            });
        }
	}


    public class FlowRegistry : Registry
    {
        public FlowRegistry()
        {
            ForRequestedType<IShowsNotificationProgress>()
                .TheDefaultIsConcreteType<FlowNotificationService>();

            ForRequestedType<ISubjectsRepository>()
                .TheDefaultIsConcreteType<LayoutSubjectsViewPreviewService>();
            ForRequestedType<ILayoutSubjectsPreviewService>()
                .TheDefaultIsConcreteType<LayoutSubjectsViewPreviewService>();


            ForRequestedType<FlowController>()
                .TheDefaultIsConcreteType<FlowController>()
                .CacheBy(InstanceScope.Singleton);

            ForRequestedType<ConfigRepository>()
                .TheDefaultIsConcreteType<ConfigRepository>();

            ForRequestedType<DesignerController>()
                .TheDefaultIsConcreteType<DesignerController>();

            ForRequestedType<Surface>()
                .TheDefaultIsConcreteType<Surface>();
            
            ForRequestedType<Nodes>()
                .TheDefaultIsConcreteType<Nodes>();
            
            ForRequestedType<History>()
                .TheDefaultIsConcreteType<History>();
            
            ForRequestedType<PropertiesPanel>()
                .TheDefaultIsConcreteType<PropertiesPanel>();

            ForRequestedType<HotFolderWatcher>()
                .TheDefaultIsConcreteType<HotFolderWatcher>();
            
            ForRequestedType<BackgroundWorker>()
                .TheDefaultIsConcreteType<BackgroundWorker>();

            ForRequestedType<CaptureController>()
                .TheDefaultIsConcreteType<CaptureController>();
            ForRequestedType<HotFolderController>()
                .TheDefaultIsConcreteType<HotFolderController>();
            ForRequestedType<HotfolderImages>()
                .TheDefaultIsConcreteType<HotfolderImages>();
            ForRequestedType<BackgroundWorker>()
                .TheDefaultIsConcreteType<BackgroundWorker>();
            
            ForRequestedType<CurrentImageController>()
                .TheDefaultIsConcreteType<CurrentImageController>();
            ForRequestedType<SubjectRecordController>()
                .TheDefaultIsConcreteType<SubjectRecordController>();
            ForRequestedType<OrderEntryController>()
                .TheDefaultIsConcreteType<OrderEntryController>();

            ForRequestedType<EditController>()
                .TheDefaultIsConcreteType<EditController>();
            ForRequestedType<OrganizationEditController>()
                .TheDefaultIsConcreteType<OrganizationEditController>();
            ForRequestedType<EditContentController>()
                .TheDefaultIsConcreteType<EditContentController>();

            ForRequestedType<DataImportController>()
                .TheDefaultIsConcreteType<DataImportController>();
            ForRequestedType<ImageAdjustmentsController>()
                .TheDefaultIsConcreteType<ImageAdjustmentsController>();
            ForRequestedType<FlowCatalogController>()
                .TheDefaultIsConcreteType<FlowCatalogController>();
            ForRequestedType<ProductPackageController>()
                .TheDefaultIsConcreteType<ProductPackageController>();


            ForRequestedType<PreferencesController>()
                .TheDefaultIsConcreteType<PreferencesController>();

            ForRequestedType<IExceptionHandler>()
                .TheDefaultIsConcreteType<ExceptionController>()
                .CacheBy(InstanceScope.Singleton);
        }
    }

}
