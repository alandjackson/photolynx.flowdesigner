﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flow.Model.Capture
{
    public class Capture
    {
        public HotfolderImages HotfolderImages { get; set; }
        public CurrentImage CurrentImage { get; set; }

        public Capture()
        {
            HotfolderImages = new HotfolderImages();
            CurrentImage = new CurrentImage();
        }
    }
}
