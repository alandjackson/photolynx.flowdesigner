﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Imaging;
using System.ComponentModel;
using System.IO;
using Flow.Lib.ImageUtils;

namespace Flow.Model
{
    public class HotfolderImage : INotifyPropertyChanged
    {
        protected string _imageFilename = "";
        public string ImageFilename 
        {
            get { return _imageFilename; }
            set { _imageFilename = value; OnPropertyChanged("ImageFilename"); }
        }
        public string ImageName { get { return new FileInfo(ImageFilename).Name; } }

        protected BitmapImage _thumbnail = null;
        public BitmapImage Thumbnail 
        {
            get { return _thumbnail; }
            set { _thumbnail = value; OnPropertyChanged("Thumbnail"); }
        }

        public double BorderWidth
        {
            get
            {
                if (Thumbnail != null)
                    return Thumbnail.Width + 10;
                return 0;
            }
        }

        public HotfolderImage() : this("") { }

        public HotfolderImage(string i)
        {
            ImageFilename = i;

            if (File.Exists(i))
                Thumbnail = ImageLoader.LoadImageThumbnail(i, 100);

            this.PropertyChanged += new PropertyChangedEventHandler(HotfolderImage_PropertyChanged);
        }

        void HotfolderImage_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "ImageFilename")
                OnPropertyChanged("ImageName");
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }
        #endregion


    }
}
