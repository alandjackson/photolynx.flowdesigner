﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

using Flow.Lib;

namespace Flow.Model.Capture
{
    public class HotfolderImages : FlowImageCollection<HotfolderImage>
    {
		protected override void OnCollectionChanged(System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
		{
			base.OnCollectionChanged(e);
		}
	}
}
