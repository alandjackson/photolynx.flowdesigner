﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Imaging;

namespace Flow.Model.Capture
{
    public class RuntimeHotfolderImages : HotfolderImages
    {
        public RuntimeHotfolderImages()
        {
            AddImage(@"C:\Users\ajackson\Development\photolynx\flow\Flow.Controls\Images\DesignTime\000001.JPG");
            AddImage(@"C:\Users\ajackson\Development\photolynx\flow\Flow.Controls\Images\DesignTime\000002.JPG");
        }

        public void AddImage(string imageFilename)
        {
            HotfolderImage image = new HotfolderImage();
            image.ImageFilename = imageFilename;

            BitmapImage bi = new BitmapImage();
            bi.BeginInit();
            bi.DecodePixelWidth = 300;
            bi.UriSource = new Uri(imageFilename);
            bi.EndInit();
            bi.Freeze();

            image.Thumbnail = bi;
            this.Add(image);
        }
    }
}
