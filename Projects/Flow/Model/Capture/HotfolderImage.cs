﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Imaging;
using System.ComponentModel;
using System.IO;
using Flow.Lib.ImageUtils;

using Flow.Lib;

namespace Flow.Model
{
	public class HotfolderImage : FlowImage
	{
		public HotfolderImage() : base() { }
		public HotfolderImage(string i) : base(i) { }
	}
}