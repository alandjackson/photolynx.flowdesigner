﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flow.Model.Settings
{
    public class ApplicationConfig
    {
        public string ProjectsWorkspace { get; set; }
        public string HotfolderDirectory { get; set; }

        public ApplicationConfig()
        {
        }
    }
}
