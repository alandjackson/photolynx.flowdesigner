﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StructureMap;
using Flow.Controller;
using Flow.Designer.Lib.ProjectInterop;
using Flow.Schema.LinqModel.DataContext;
using System.IO;
using System.Windows.Data;
using Flow.Designer.Lib.ImageAdjustments.Model;

namespace Flow.Service
{
    public class ProjectLayoutSubjectsRepository : ISubjectsRepository
    {
        public FlowController FlowController { get; set; }

        public ProjectLayoutSubjectsRepository()
        {
            FlowController = ObjectFactory.GetInstance<FlowController>();
        }

        #region ISubjectsRepository Members

        public string[] GetSubjectFields()
        {
            throw new NotImplementedException();
        }

        public string[] GetImageTags()
        {
            throw new NotImplementedException();
        }

        ListCollectionView SubjectsView
        {
            get
            {
                return FlowController.ProjectController.CurrentFlowProject.
                  SubjectList.View;
            }
        }

        public Flow.Designer.Model.Project.Subject GetSubject(int ndx)
        {
            return ((Subject)FlowController.ProjectController.CurrentFlowProject
                .SubjectList.View.GetItemAt(ndx)).ToDesignerSubject();

            //return FlowController.ProjectController.CurrentFlowProject
            //    .SubjectList[ndx].ToDesignerSubject();
        }

        public int SubjectsCount
        {
            get
            {
                return SubjectsView.Count;
            }
            //get { return FlowController.ProjectController
            //    .CurrentFlowProject.SubjectList.Count; }
        }


        #endregion
    }

    public static class FlowSubjectAdapterExtensions
    {




        public static Flow.Designer.Model.Project.Subject ToDesignerSubject(
             this Flow.Schema.LinqModel.DataContext.Subject src)
        {
            
            //make sure the subject is initialized:
            //object inito = src.SubjectData;
            //

            Flow.Designer.Model.Project.Subject dest =
                new Flow.Designer.Model.Project.Subject();

            //somehow there is a NullReferenceException here sometimes that I cant reproduce.
            //try this fix, if src is null, return empty subject
            if (src == null)
                return dest;

            foreach (SubjectImage i in src.ImageList)
            {
                if (i.IsSuppressed)
                    continue;
                
                dest.SubjectImages.Add(i);
            }


            dest.Fields.Add("Subject ID", src.SubjectID.ToString());
            dest.Fields.Add("Ticket ID", src.TicketCode);
            for (int i = 0; i < src.SubjectData.Count; i++)
            {
                SubjectDatum d = src.SubjectData[i];
                if (d == null) continue;
                if (d.FieldType == typeof(DateTime) && d.Value != null)
                {
                    if (d.Value.ToString().Length > 1)
                        dest.Fields[d.Field] = d.Value == null ? "" : ((DateTime)d.Value).ToShortDateString();
                    else
                        dest.Fields[d.Field] = "";
                }
                else
                    dest.Fields[d.Field] = d.Value == null ? "" : d.Value.ToString();

            }


            if (src.SubjectGuid == null)
            {
                src.SubjectGuid = Guid.NewGuid();
            }

            dest.Fields["SubjectGuid"] = src.SubjectGuid.Value.ToString().Replace("-", "");

            return dest;
        }
    }

}
