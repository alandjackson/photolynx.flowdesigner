﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.Controller;
using StructureMap;
using System.Windows.Data;
using Flow.Designer.Lib.Service;
using Flow.Designer.Model;
using Flow.Designer.Lib.ProjectInterop;
using Flow.Schema.LinqModel.DataContext;
using Flow.Service;
using Flow.Designer.Lib.ProjectRepository;
using Flow.Lib.ImageUtils;
using System.Windows.Forms;

namespace Flow.Model.Designer
{
    public class LayoutSubjectsViewPreviewService : 
        AbstractNotifyPropertyChanged,
        ILayoutSubjectsPreviewService,
        ICurrentSubjectRepository
    {
        public FlowController FlowController { get; set; }

        ListCollectionView SubjectsView
        {
            get
            {
                try
                {
                    if (FlowController.ProjectController.CurrentFlowProject != null)
                        return FlowController.ProjectController.CurrentFlowProject.SubjectList.View;
                    else 
                        return null;
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }
        public LayoutSubjectsViewPreviewService()
        {
            FlowController = ObjectFactory.GetInstance<FlowController>();
            
            if (SubjectsView == null) return;
            SubjectsView.CurrentChanged += new EventHandler(SubjectsView_CurrentChanged);
        }

        void SubjectsView_CurrentChanged(object sender, EventArgs e)
        {
            OnPropertyChanged("CurrentRecordNdx");
            OnSubjectChanged();
        }

        #region ILayoutSubjectsPreviewService Members

        public int CurrentRecordNdx
        {
            get
            {
                if (SubjectsView == null) return 1;
                return SubjectsView.CurrentPosition + 1;
            }
            set
            {
                if (SubjectsView == null) return;
                SubjectsView.MoveCurrentToPosition(value + 1);
            }
        }

        public Flow.Designer.Model.Project.Subject GetCurrentSubject()
        {
            if (SubjectsView == null || ! IsPreviewEnabled) return null;
            return ((Subject)SubjectsView.CurrentItem).ToDesignerSubject();
        }

        protected bool _isPreviewEnabled = false;
        public bool IsPreviewEnabled 
        { 
            get { return _isPreviewEnabled; }
            set
            { 
                _isPreviewEnabled = value;
                OnSubjectChanged();
                //MessageBox.Show("ImageLoaded: " + ImageLoader.ImageLoaded);
            }
        }

        public void MoveFirst()
        {
            if (SubjectsView == null) return;
            SubjectsView.MoveCurrentToFirst();
        }

        public void MoveLast()
        {
            if (SubjectsView == null) return;
            SubjectsView.MoveCurrentToLast();
        }

        public void MoveNext()
        {
            if (SubjectsView == null) return;

            if (CurrentRecordNdx < RecordsCount)
                SubjectsView.MoveCurrentToNext();
        }

        public void MovePrevious()
        {
            if (SubjectsView == null) return;

            if (CurrentRecordNdx > 1)
                SubjectsView.MoveCurrentToPrevious();
        }

        public int RecordsCount
        {
            get
            {
                if (SubjectsView == null) return 1;
                return SubjectsView.Count;
            }
        }

        public event EventHandler SubjectChanged;
        protected void OnSubjectChanged()
        {
            if (SubjectChanged != null)
                SubjectChanged(this, EventArgs.Empty);
        }

        public void UpdateRecordsCount()
        {
            OnPropertyChanged("RecordsCount");
        }


        #endregion

        #region ISubjectsRepository Members

        public Flow.Designer.Model.Project.Subject GetSubject(int ndx)
        {
            if (SubjectsView == null) return null;
            return ((Subject)SubjectsView.GetItemAt(ndx)).ToDesignerSubject();
        }

        public int SubjectsCount
        {
            get { return RecordsCount; }
        }

        public string[] GetSubjectFields()
        {
            return new string[] { };
        }

        public string[] GetImageTags()
        {
            return new string[] { };
        }

        #endregion

        #region ICurrentSubjectRepository Members


        public Flow.Designer.Model.Project.Subject GetSubject()
        {
            return GetCurrentSubject();
        }

        #endregion
    }
}
