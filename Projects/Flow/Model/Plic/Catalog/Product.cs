﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flow.Model.Plic.Catalog
{
    public class Product
    {

        public string Type { get; set; }
        public string Name { get; set; }
        public string SKU { get; set; }
        public string ID { get; set; }
        public string DisplayImageURL { get; set; }
        public string Description { get; set; }
        public int width { get; set; }
        public int height { get; set; }
        public int rotation { get; set; }
        public string Map { get; set; }

        public List<string> Filters { get; set; }
        public List<ImageNode> ImageNodes { get; set; }
        public List<TextNode> TextNodes { get; set; }
        public List<OptionGroup> ProductOptions { get; set; }
        public List<Price> Pricing { get; set; }

        public Product() { }
        public Product(string id, string type)
        {
            ID = id;
            Type = type;
            
            ProductOptions = new List<OptionGroup>();
            ImageNodes = new List<ImageNode>();
            TextNodes = new List<TextNode>();
            Filters = new List<string>();
            Pricing = new List<Price>();
        }


    }
}
