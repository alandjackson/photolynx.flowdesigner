﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flow.Model.Plic.Catalog
{

    public class Price
    {
        public int Amount { get; set; }
        public string UnitOfMeasure { get; set; }

        public List<VariablePrice> VariablePrices { get; set; }

        
        public Price()
        {
            VariablePrices = new List<VariablePrice>();
        }
    }

    public class VariablePrice
    {

        public string Type { get; set; }

        public VariablePrice()
        {

        }

    }
}
