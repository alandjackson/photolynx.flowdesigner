﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flow.Model.Plic.Catalog
{
    public class Filter
    {
        public List<Filter> Filters { get; set; }
        public string ID { get; set; }
        public string Name { get; set; }

        public Filter()
        {

        }
        public Filter(string name, string id)
        {
            Name = name;
            ID = id;
            Filters = new List<Filter>();
        }
    }
}
