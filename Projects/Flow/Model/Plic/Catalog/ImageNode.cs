﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flow.Model.Plic.Catalog
{
    public class ImageNode
    {

        public string Name { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public int Rotation { get; set; }
        public int ID { get; set; }
        public string Type { get; set; }

        public ImageNode() { }
        public ImageNode(string name)
        {
            Name = name;
            Type = "image";

        }
    }
}
