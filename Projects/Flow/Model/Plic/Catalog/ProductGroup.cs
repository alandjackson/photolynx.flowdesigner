﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flow.Model.Plic.Catalog
{
    public class ProductGroup
    {

        public string Name { get; set; }
        public List<Product> Products { get; set; }
        public string ID { get; set; }

        public ProductGroup() { }
        public ProductGroup(string name, string id)
        {
            Name = name;
            ID = id;

            Products = new List<Product>();
        }


    }
}
