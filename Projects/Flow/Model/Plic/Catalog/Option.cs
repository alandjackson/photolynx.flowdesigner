﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flow.Model.Plic.Catalog
{
    public class Option
    {
        public string Name { get; set; }
        public string LabID { get; set; }
        public int Price { get; set; }
        public string Description { get; set; }
        public string SKU { get; set; }

        public Option() { }
        public Option(string name)
        {
            Name = name;
        }
    }
}
