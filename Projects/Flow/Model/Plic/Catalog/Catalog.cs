﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Flow.Model.Plic.Catalog
{
    public class Catalog : INotifyPropertyChanged
    {
        public string Name { get; set; }
        public List<Product> Products { get; set; }
        public List<OptionGroup> OrderOptions { get; set; }
        public List<OptionGroup> ImageOptions { get; set; }
        public List<Filter> Filters { get; set; }

        public int SchemaVersion { get; set; }

        public String LastModifiedDate { get; set; }



        public Catalog()
        {
        }

        public Catalog(string name)
        {
            Name = name;
            Filters = new List<Filter>();
            OrderOptions = new List<OptionGroup>();
            Products = new List<Product>();
            
            SchemaVersion = 1;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public void StatusUpdated()
        {
            NotifyPropertyChanged("Status");
            NotifyPropertyChanged("AllowPublish");
            NotifyPropertyChanged("IsPublishing");
            NotifyPropertyChanged("AllowUpdate");

        }


        
    }
}
