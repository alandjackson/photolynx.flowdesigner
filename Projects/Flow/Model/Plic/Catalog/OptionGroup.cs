﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flow.Model.Plic.Catalog
{

    public class OptionGroup
    {
        public string Type { get; set; }
        public string Name { get; set; }
        public string LabID { get; set; }
        public string Description { get; set; }
        public string Category { get; set; }
        public bool Required { get; set; }
        public bool MulitSelect { get; set; }

        public string DataFieldType { get; set; }

        public List<Option> Options { get; set; }

        public OptionGroup(){ }
        public OptionGroup(string name, string type)
        {
            Type = type;
            Name = name;

            Options = new List<Option>();            
        }
    }
}
