﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Flow.View.Capture;
using Flow.Helpers;
using Flow.ViewModel.Capture;

namespace Flow.Tests
{
    /// <summary>
    /// Summary description for GrayCardPanelTest
    /// </summary>
    [TestClass]
    public class GrayCardPanelTest
    {
        public GrayCardPanelTest()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        //[TestMethod]
        public void TestMethod1()
        {
            var pnl = new GrayCardPanel();
            var vm = new GrayCardViewModel(pnl, new Flow.Lib.Preferences.CapturePreferences() { MrGrayTargetBrightness = "300", MrGrayAdjustmentMethod = "Move Lights" });
            vm.MainImage = new Flow.Lib.FlowImage() { ImageFilename = @"C:\ProgramData\Flow\Projects\3ef7ee0b-92d0-450a-9b76-6f71f9134cf2\Image\subaru-impreza-wrx-00003.jpg" };
            pnl.DataContext = vm;
            var win = UIUtil.WrapInWindow(pnl);
            win.SizeToContent = System.Windows.SizeToContent.Manual;
            win.Width = 800;
            win.Height = 800;
            win.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Stretch;
            win.VerticalContentAlignment = System.Windows.VerticalAlignment.Stretch;
            win.ShowDialog();

        }
    }
}
