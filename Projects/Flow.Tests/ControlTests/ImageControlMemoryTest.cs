﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Media.Imaging;

namespace Flow.Tests.ControlTests
{
    /// <summary>
    /// Summary description for ImageControlMemoryTest
    /// </summary>
    [TestClass]
    public class ImageControlMemoryTest
    {
        public ImageControlMemoryTest()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        //[TestMethod]
        public void ImageControl_loads_images()
        {
            imgControl = new Image();
            imgControl.Width = 300;
            imgControl.Height = 300;
            SetImages();

            var btn = new Button();
            btn.Content = "Refresh";
            btn.Click += new RoutedEventHandler(btn_Click);
            DockPanel.SetDock(btn, Dock.Bottom);

            DockPanel dp = new DockPanel();
            dp.LastChildFill = true;
            dp.Children.Add(btn);
            dp.Children.Add(imgControl);

            Window w = new Window();
            w.SizeToContent = SizeToContent.WidthAndHeight;
            w.Content = dp;
            w.ShowDialog();
        }

        void btn_Click(object sender, RoutedEventArgs e)
        {
            SetImages();
        }

        Image imgControl;
        void SetImages()
        {
            imgControl.Source = new BitmapImage(
                new Uri(@"C:\Users\Public\Pictures\Sample Pictures\Chrysanthemum.jpg"));
        }
    }
}
