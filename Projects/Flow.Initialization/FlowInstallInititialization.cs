﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.IO;
using System.Linq;
using System.Security.AccessControl;

using Flow.Lib;

namespace Flow.Initialization
{
	[RunInstaller(true)]
	public partial class FlowInstallInititialization : Installer
	{
		public FlowInstallInititialization()
		{
			InitializeComponent();
		}

		public override void Commit(IDictionary savedState)
		{
			base.Commit(savedState);

			string commonAppDataDir = FlowContext.FlowAppDataDirPath;

			DirectorySecurity dirSec = Directory.GetAccessControl(commonAppDataDir);
			
			FileSystemAccessRule fsar = new FileSystemAccessRule(
				@"BUILTIN\Users",
				FileSystemRights.Modify,
				InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit,
				PropagationFlags.None,
				AccessControlType.Allow
			);

			dirSec.AddAccessRule(fsar);

			Directory.SetAccessControl(commonAppDataDir, dirSec);


		}

        public override void Install(IDictionary stateSaver)
        {
            base.Install(stateSaver);
        }

        private void Installer1_AfterInstall(object sender, InstallEventArgs e)
        {
            string str = System.Reflection.Assembly.GetExecutingAssembly().Location;
            string accessComponentFile = "AccessDatabaseEngine_x64.exe";


            int indx = str.LastIndexOf('\\');
            str = str.Substring(0, indx);


            System.Diagnostics.Process prc = new System.Diagnostics.Process();
            try
            {
                prc.StartInfo.FileName = str + "\\" + accessComponentFile; ;
                prc.StartInfo.Arguments = "/quiet";
                prc.Start();
                prc.EnableRaisingEvents = true;
                prc.Exited += new EventHandler(prc_Exited);
            }
            catch (Exception ex)
            {
                //System.Windows.Forms.MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        void prc_Exited(object sender, EventArgs e)
        {
            string str = System.Reflection.Assembly.GetExecutingAssembly().Location;
            string accessComponentFile = "AccessDatabaseEngine.exe";
            int indx = str.LastIndexOf('\\');
            str = str.Substring(0, indx);

            string FolderPath = str + "\\" + accessComponentFile;
            if (File.Exists(FolderPath))
            {
                try
                {
                    File.Delete(FolderPath);
                }
                catch (Exception ex)
                {
                }
            }
        }
       
	}
}
