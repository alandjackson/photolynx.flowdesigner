﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Flow.Lib.Net;
using System.Windows.Forms;
using System.Threading;
using System.Diagnostics;

namespace Flow.Lib.Tests
{
    /// <summary>
    /// Summary description for FTPClientUnitTest
    /// </summary>
    [TestClass]
    public class FTPClientUnitTest
    {
        public FTPClientUnitTest()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        //[TestMethod]
        public void FTPClientTest()
        {
            Assert.Inconclusive();

            Debug.Listeners.Add(new TextWriterTraceListener(Console.Out));
            FTPClient client = new FTPClient("127.0.0.1", 21, "ftpaccount", "negativezero");
            
            string[] filelist = new string[4];
            filelist[0] = @"E:\ftproot\fz6-1.pdf";
            filelist[1] = @"E:\ftproot\fz6-2.pdf";
            filelist[2] = @"E:\ftproot\fz6-3.pdf";
            filelist[3] = @"E:\ftproot\fz6-4.pdf";

            client.UploadCompleted += new FTPClientUploadEventHandler(client_UploadCompleted);
            client.CurrentFileUploadCompleted += new FTPClientUploadEventHandler(client_FileUploaded);
            client.CurrentFileUploadProgression += new FTPClientUploadEventHandler(client_CurrentFileUploadProgression);

            client.BeginFileUploads(filelist, "./uploads/test/test/test/");
            
            Thread.Sleep(5000);
            client.PauseUpload();
            Thread.Sleep(5000);
            Debug.WriteLine("Upload paused.");
            client.ResumeUpload();
            Thread.Sleep(5000);
            
            client.WaitForUploadCompletion();
        }

        void client_CurrentFileUploadProgression(object sender)
        {
            FTPClient client = sender as FTPClient;
            Debug.WriteLine(String.Format("Uploading: {0}. {1} of {2} bytes sent.", client.CurrentFileUpload, client.CurrentFileBytesSent, client.CurrentFileBytesTotal));

        }

        void client_FileUploaded(object sender)
        {
            
        }

        void client_UploadCompleted(object sender)
        {
            //MessageBox.Show("Upload complete");
        }
    }
}
