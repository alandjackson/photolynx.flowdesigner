﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Flow.Lib.UnitOfWork;
using System.Threading;

namespace Flow.Lib.Tests
{
    /// <summary>
    /// Summary description for ConcurrentUnitOfWorkProcessorTest
    /// </summary>
    [TestClass]
    public class ConcurrentUnitOfWorkProcessorTest
    {
        public ConcurrentUnitOfWorkProcessorTest()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void ConcurrentUnitOfWorkProcessor_can_submit_batches()
        {
            UnitOfWorkBatch batch = new UnitOfWorkBatch();
            for (int i = 0; i < 10; i++)
            {
                batch.SubmitWork((object a) => 
                { 
                    Console.WriteLine("Starting " + Thread.CurrentThread.ManagedThreadId); 
                    Thread.Sleep(1000); 
                }, null);
            }

            ConcurrentUnitOfWorkProcessor processor = new ConcurrentUnitOfWorkProcessor();
            processor.SubmitBatch(batch);
        }
    }
}
