﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Flow.Lib.Activation;
using System.Diagnostics;

namespace Flow.Lib.Tests.Activation
{
    /// <summary>
    /// Summary description for EncryptionTest
    /// </summary>
    [TestClass]
    public class EncryptionTest
    {
        public EncryptionTest()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void encrypt_decrypt_match()
        {
            Encryption e = new Encryption();
            string start = "Alan Jackson";
            string key = "My Special Key";

            string encrypted = e.Encrypt(start, key);
            string decrypted = e.Decrypt(encrypted, key);
            Assert.AreEqual(start, decrypted);
        }
    }
}
