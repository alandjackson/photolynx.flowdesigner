﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Flow.Lib.Activation;

namespace Flow.Lib.Tests.Activation
{
    /// <summary>
    /// Summary description for ActivationValidatorTest
    /// </summary>
    [TestClass]
    public class ActivationValidatorTest
    {
        public ActivationValidatorTest()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        public class FakeMachineInfo : IMachineInfo
        {
            #region IMachineInfo Members

            public string GenerateMachineId()
            {
                return "12345";
            }

            #endregion
        }

        public class FakeActivationStoreRepository : IActivationStoreRepository
        {
            public ActivationStore StoreToReturn { get; set; }
            public bool WasSaved { get; set; }

            public ActivationStore Load()
            {
                return StoreToReturn == null ? new ActivationStore() : StoreToReturn;
            }

            public void Save(ActivationStore store)
            {
                WasSaved = true;
            }
        }

        [TestMethod]
        public void FeatureIsEnabled_false_to_start()
        {
            FakeActivationStoreRepository fr = new FakeActivationStoreRepository();
            FakeMachineInfo fm = new FakeMachineInfo();

            ActivationValidator validator = new ActivationValidator(fr, fm);
            Assert.IsFalse(validator.FeatureIsEnabled("Flow v0.6+"));
        }

        [TestMethod]
        public void FeatureIsEnabled_true_with_corrent_info()
        {
            FakeMachineInfo fm = new FakeMachineInfo();
            FakeActivationStoreRepository fr = new FakeActivationStoreRepository();
            fr.StoreToReturn = new ActivationStore()
            {
                ActivatedEnd = DateTime.Now.AddDays(1),
                EnabledFeatures = new List<string>(new string[] {"Flow v0.6+"}),
                ComputerId = fm.GenerateMachineId()
            };

            ActivationValidator validator = new ActivationValidator(fr, fm);
            Assert.IsTrue(validator.FeatureIsEnabled("Flow v0.6+"));
        }

        [TestMethod]
        public void FeatureIsEnabled_false_with_last_date_in_future()
        {
            FakeMachineInfo fm = new FakeMachineInfo();
            FakeActivationStoreRepository fr = new FakeActivationStoreRepository();
            fr.StoreToReturn = new ActivationStore()
            {
                ActivatedEnd = DateTime.Now.AddDays(1),
                EnabledFeatures = new List<string>(new string[] { "Flow v0.6+" }),
                ComputerId = fm.GenerateMachineId(),
                LastRanTime = DateTime.Now.AddDays(1)
            };

            ActivationValidator validator = new ActivationValidator(fr, fm);
            Assert.IsFalse(validator.FeatureIsEnabled("Flow v0.6+"));
        }

        [TestMethod]
        public void FeatureIsEnabled_false_with_incorrect_feature_enabled()
        {
            FakeMachineInfo fm = new FakeMachineInfo();
            FakeActivationStoreRepository fr = new FakeActivationStoreRepository();
            fr.StoreToReturn = new ActivationStore()
            {
                ActivatedEnd = DateTime.Now.AddDays(1),
                EnabledFeatures = new List<string>(new string[] { "Flow v0.7+" }),
                ComputerId = fm.GenerateMachineId(),
                LastRanTime = DateTime.MinValue
            };

            ActivationValidator validator = new ActivationValidator(fr, fm);
            Assert.IsFalse(validator.FeatureIsEnabled("Flow v0.6+"));
        }

        [TestMethod]
        public void FeatureIsEnabled_false_with_expiration_date_passed()
        {
            FakeMachineInfo fm = new FakeMachineInfo();
            FakeActivationStoreRepository fr = new FakeActivationStoreRepository();
            fr.StoreToReturn = new ActivationStore()
            {
                ActivatedEnd = DateTime.Now.AddDays(-1),
                EnabledFeatures = new List<string>(new string[] { "Flow v0.6+" }),
                ComputerId = fm.GenerateMachineId(),
                LastRanTime = DateTime.MinValue

            };

            ActivationValidator validator = new ActivationValidator(fr, fm);
            Assert.IsFalse(validator.FeatureIsEnabled("Flow v0.6+"));
        }

        [TestMethod]
        public void FeatureIsEnabled_updates_last_ran_and_saves()
        {
            FakeMachineInfo fm = new FakeMachineInfo();
            FakeActivationStoreRepository fr = new FakeActivationStoreRepository();
            fr.StoreToReturn = new ActivationStore() { LastRanTime = DateTime.MinValue };

            ActivationValidator validator = new ActivationValidator(fr, fm);
            Assert.AreEqual(DateTime.MinValue, fr.StoreToReturn.LastRanTime);
            validator.FeatureIsEnabled("");
            Assert.IsTrue(fr.WasSaved);
            Assert.AreNotEqual(DateTime.MinValue, fr.StoreToReturn.LastRanTime);
        }

    }
}
