﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Flow.Lib.Web;
using System.Windows.Forms;
using Flow.Lib.Activation;

namespace Flow.Lib.Tests.Activation
{
    /// <summary>
    /// Summary description for WebServiceActivatorTest
    /// </summary>
    [TestClass]
    public class WebServiceActivatorTest
    {
        public WebServiceActivatorTest()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        //[TestMethod]
        public void TestMethod1()
        {
            Assert.Inconclusive();
            PostSubmitter submitter = new PostSubmitter
                (@"http://localhost:56596/", "Name", "Alan");
            submitter.Type = PostSubmitter.PostTypeEnum.Post;
            MessageBox.Show("Result: " + submitter.Post());
            //
            // TODO: Add test logic	here
            //
        }

        //[TestMethod]
        public void Activate_works_over_web_service()
        {
            Assert.Inconclusive();
            WebServiceActivator activator = new WebServiceActivator();
            ActivationStore store = new ActivationStore();
            ActivationStore newstore = activator.Activate(store);

            Assert.AreEqual(new DateTime(2009, 9, 1), newstore.ActivatedEnd);
            Assert.IsTrue(newstore.EnabledFeatures.Contains("Flow 1 Beta"));
            
        }
        
    }
}
