﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Flow.Lib.Activation;

namespace Flow.Lib.Tests.Activation
{
    /// <summary>
    /// Summary description for ActivationStoreRepositoryTest
    /// </summary>
    [TestClass]
    public class ActivationStoreRepositoryTest
    {
        public ActivationStoreRepositoryTest()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void Load_Save_match_data()
        {
            ActivationStoreRepository repository = new ActivationStoreRepository();
            repository.ActivationFile = System.IO.Path.GetTempFileName();

            ActivationStore store = new ActivationStore()
            {
                ActivatedEnd = DateTime.Now,
                ActivatedStart = DateTime.Now.AddDays(1),
                ComputerId = "12345"
            };

            repository.Save(store);
            ActivationStore newstore = repository.Load();

            Assert.AreEqual(store.ActivatedEnd, newstore.ActivatedEnd);
            Assert.AreEqual(store.ActivatedStart, newstore.ActivatedStart);
            Assert.AreEqual(store.ComputerId, newstore.ComputerId);

            System.IO.File.Delete(repository.ActivationFile);


        }

        [TestMethod]
        public void Load_initial_data()
        {
            ActivationStoreRepository repository = new ActivationStoreRepository();
            repository.ActivationFile = System.IO.Path.GetTempFileName();

            ActivationStore store = new ActivationStore();
            ActivationStore newstore = repository.Load();

            Assert.AreEqual(store.ActivatedEnd, newstore.ActivatedEnd);
            Assert.AreEqual(store.ActivatedStart, newstore.ActivatedStart);
            Assert.AreEqual(store.ComputerId, newstore.ComputerId);

            System.IO.File.Delete(repository.ActivationFile);
        }
    }
}
