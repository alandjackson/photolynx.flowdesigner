﻿using System;
namespace Flow.Lib.Activation
{
    public interface IActivationStoreRepository
    {
        ActivationStore Load();
        void Save(ActivationStore store);
    }
}
