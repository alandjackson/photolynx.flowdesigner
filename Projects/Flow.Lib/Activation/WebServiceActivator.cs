﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.Lib.Web;
using Flow.Lib.Persister;
using System.Windows;

namespace Flow.Lib.Activation
{
    public class WebServiceActivator
    {
        PostSubmitter PostSubmitter { get; set; }

        public WebServiceActivator()
            : this(new PostSubmitter())
        {
        }

        public WebServiceActivator(PostSubmitter postsubmitter)
        {
            PostSubmitter = postsubmitter;
        }

        public ActivationStore Activate(ActivationStore src)
        {
            WebServiceActivatorMessage sendMsg = new WebServiceActivatorMessage()
            {
                ActivationStore = src
            };

            //PostSubmitter.Url = @"http://localhost:64844";
            PostSubmitter.Url = @"http://www.flowadmin.com/activationservice";
            PostSubmitter.Type = PostSubmitter.PostTypeEnum.Post;
            PostSubmitter.PostItems.Add("WebServiceActivatorMessage",
                ObjectSerializer.ToXmlString(
                new WebServiceActivatorMessage() { ActivationStore = src }));
            string resultMsgStr = PostSubmitter.Post();
            //MessageBox.Show(resultMsgStr);

            WebServiceActivatorMessage resultMsg =
                ObjectSerializer.FromXmlString<WebServiceActivatorMessage>(
                resultMsgStr);

            
            return resultMsg.ActivationStore;
        }
    }

    public class WebServiceActivatorMessage
    {
        public ActivationStore ActivationStore { get; set; }
        public string ErrorMsg { get; set; }
    }
}
