﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;

namespace Flow.Lib.Activation
{
    public class Encryption
    {
        public string Encrypt(string inputString, string key)
        {
            byte[] IVector = new byte[8] { 27, 9, 45, 27, 0, 72, 171, 54 };
            byte[] buffer = Encoding.ASCII.GetBytes(inputString);
            TripleDESCryptoServiceProvider tripleDes = new TripleDESCryptoServiceProvider();
            MD5CryptoServiceProvider MD5 = new MD5CryptoServiceProvider();
            tripleDes.Key = MD5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(key));
            tripleDes.IV = IVector;
            tripleDes.Mode = CipherMode.CBC;
            tripleDes.Padding = PaddingMode.PKCS7;
            ICryptoTransform ITransform = tripleDes.CreateEncryptor();
            return Convert.ToBase64String(ITransform.TransformFinalBlock(buffer, 0, buffer.Length));
        }
        public string Decrypt(string inputString, string key)
        {
            byte[] IVector = new byte[8] { 27, 9, 45, 27, 0, 72, 171, 54 };
            byte[] buffer = Convert.FromBase64String(inputString);
            TripleDESCryptoServiceProvider tripleDes = new TripleDESCryptoServiceProvider();
            MD5CryptoServiceProvider MD5 = new MD5CryptoServiceProvider();
            tripleDes.Key = MD5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(key));
            tripleDes.IV = IVector;
            tripleDes.Mode = CipherMode.CBC;
            tripleDes.Padding = PaddingMode.PKCS7;
            ICryptoTransform ITransform = tripleDes.CreateDecryptor();
            return Encoding.ASCII.GetString(ITransform.TransformFinalBlock(buffer, 0, buffer.Length));
        }
        public static string SecureHash(string inputString)
        {
            var inBytes = UTF8Encoding.UTF8.GetBytes(inputString);
            var outBytes = new MD5CryptoServiceProvider().ComputeHash(inBytes);
            return UTF8Encoding.UTF8.GetString(outBytes);
        }
    }
}
