﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.Lib.Persister;
using System.IO;

namespace Flow.Lib.Activation
{
    public class ActivationStoreRepository : Flow.Lib.Activation.IActivationStoreRepository
    {
        //public string ActivationFile = Path.Combine(
        //    FlowContext.FlowCommonAppDataDirPath, "Activation.dat");
        public string ActivationFile { get; set; }
        public string ActivationDir = FlowContext.FlowCommonAppDataDirPath;
        Encryption _encryption = new Encryption();
        protected string _key = "This is a greate key";

        public ActivationStore Load()
        {
            try
            {
                ActivationStore store =
                    ObjectSerializer.FromXmlString<ActivationStore>(
                    _encryption.Decrypt(
                    File.ReadAllText(ActivationFile, new UTF8Encoding()), _key
                    ).Substring(1));
                return store == null ? new ActivationStore() : store;
            }
            catch (Exception)
            {
                return new ActivationStore();
            }
        }

        public void Save(ActivationStore store)
        {
            File.WriteAllText(ActivationFile,
                _encryption.Encrypt(ObjectSerializer.ToXmlString(store), _key),
                new UTF8Encoding());
        }
    }
}
