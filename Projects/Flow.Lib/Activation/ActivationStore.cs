﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Xml;

namespace Flow.Lib.Activation
{
    public class ActivationStore
    {
        public string ComputerId { get; set; }
        public List<string> EnabledFeatures { get; set; }
        public DateTime ActivatedStart { get; set; }
        public DateTime ActivatedEnd { get; set; }
        public DateTime LastRanTime { get; set; }
        public String ActivationKey { get; set; }
        public String ComputerName { get; set; }
        public DataSet ConfigData { get; set; }
        public DataSet OrderFormData { get; set; }
        public String CustomerName { get; set; }
        public String CustomerId { get; set; }
        public String CustomerEmail { get; set; }
        public String Address { get; set; }
        public String City { get; set; }
        public String State { get; set; }
        public String Zip { get; set; }
        public String ShippingAddress { get; set; }
        public String ShippingCity { get; set; }
        public String ShippingState { get; set; }
        public String ShippingZip { get; set; }
        public String ContactName { get; set; }
        public String CustomerPhone { get; set; }
        public string FingerPrint { get; set; }
    }
}
