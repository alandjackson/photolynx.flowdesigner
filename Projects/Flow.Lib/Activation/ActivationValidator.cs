﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Flow.Lib.Activation
{
    public class ActivationValidator
    {
        public IActivationStoreRepository Repository { get; set; }
        public IMachineInfo MachineInfo { get; set; }
        public String FingerPrintString { get; set; }

        public ActivationValidator(string activationKeyFile) : this(new ActivationStoreRepository(),
            new MachineInfo(), FingerPrint.Value(), activationKeyFile)
        {
        }

        public string DayPassValue()
        {
            int month = int.Parse(DateTime.Today.Month.ToString());
            int day = int.Parse(DateTime.Today.Day.ToString());
            int year = int.Parse(DateTime.Today.Year.ToString());

            string daypass = (((month * month) + (day)).ToString() + ((day * day) + (month)).ToString());
            return daypass;

        }

        public string MultiDayPassValue(int numberOfDays)
        {
            int month = int.Parse(DateTime.Today.Month.ToString());
            int day = int.Parse(DateTime.Today.Day.ToString());
            int year = int.Parse(DateTime.Today.Year.ToString());

            string first = ((month * month) + (day)).ToString();
            string second = ((day * day) + (month)).ToString();
            string third = (((Int32.Parse(first + second) * numberOfDays) + (year * 2222)) * 5).ToString();

            string multidaypass = third; 
            return multidaypass;

        }

        public bool GrantActivation(string featureName, int days)
        {
            ActivationStore store = new ActivationStore();
            store.ActivatedStart = DateTime.Today;
            store.ActivatedEnd = DateTime.Today.AddDays(days);
            try
            {
                store.ComputerId = MachineInfo.GenerateMachineId();
            }
            catch
            {
                store.ComputerId = FingerPrint.Value();
            }
            store.FingerPrint = FingerPrint.Value();
            store.EnabledFeatures = new List<string> { featureName };
            store.LastRanTime = DateTime.Today;
            this.Repository.Save(store);
            return true;
        }


        public ActivationValidator(IActivationStoreRepository repository,
            IMachineInfo machineinfo, string fingerPrint ,string activationKeyFile)
        {
            Repository = repository;
            MachineInfo = machineinfo;
            FingerPrintString = fingerPrint;
            (Repository as ActivationStoreRepository).ActivationFile = activationKeyFile;
        }

        public bool FeatureIsEnabled(string featureName)
        {
            string currentMachineId = "";
            try
            {
                currentMachineId = MachineInfo.GenerateMachineId();
            }
            catch
            {
                currentMachineId = FingerPrint.Value();
            }
            string currentFingerPrint = FingerPrint.Value();
            ActivationStore store = Repository.Load();

            try
            {
                if (store.LastRanTime > DateTime.Now)
                    return false;

                if (store.ComputerId != currentMachineId && store.ComputerId != currentFingerPrint)
                    return false;

                if (store.FingerPrint != null && store.FingerPrint != currentFingerPrint)
                    return false;

                if (!store.EnabledFeatures.Contains(featureName))
                    return false;

                if (store.ActivatedEnd < DateTime.Now)
                    return false;

                return true;
            }
            finally
            {
                store.LastRanTime = DateTime.Now;
                Repository.Save(store);
            }
        }
    }
}
