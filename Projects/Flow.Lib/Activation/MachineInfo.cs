﻿using System;
using System.Text;
using System.Runtime.InteropServices;
using System.Management;
using System.Collections.Generic;

namespace Flow.Lib.Activation
{
    public class MachineInfo : Flow.Lib.Activation.IMachineInfo
    {
        /// <summary>
        /// return Volume Serial Number from hard drive
        /// </summary>
        /// <param name="strDriveLetter">[optional] Drive letter</param>
        /// <returns>[string] VolumeSerialNumber</returns>
        protected string GetVolumeSerial(string strDriveLetter)
        {

            if (strDriveLetter == "" || strDriveLetter == null) strDriveLetter = "C";
            ManagementObject disk =
                new ManagementObject("win32_logicaldisk.deviceid=\"" + strDriveLetter + ":\"");
            disk.Get();
            return disk["VolumeSerialNumber"].ToString();
        }

        /// <summary>
        /// Returns MAC Address from first Network Card in Computer
        /// </summary>
        /// <returns>[string] MAC Address</returns>
        protected IEnumerable<string> GetMACAddresses()
        {
            ManagementClass mc = new ManagementClass("Win32_NetworkAdapterConfiguration");
            ManagementObjectCollection moc = mc.GetInstances();
            string MACAddress = String.Empty;
            foreach (ManagementObject mo in moc)
            {
                if ((bool)mo["IPEnabled"] == true) 
                    yield return mo["MacAddress"].ToString().Replace(":", "");
                mo.Dispose();
            }
        }

        /// <summary>
        /// Return processorId from first CPU in machine
        /// </summary>
        /// <returns>[string] ProcessorId</returns>
        protected string GetCPUId()
        {
            string cpuInfo = String.Empty;
            string temp = String.Empty;
            ManagementClass mc = new ManagementClass("Win32_Processor");
            ManagementObjectCollection moc = mc.GetInstances();
            foreach (ManagementObject mo in moc)
            {
                if (cpuInfo == String.Empty)
                {// only return cpuInfo from first CPU
                    cpuInfo = mo.Properties["ProcessorId"].Value.ToString();
                }
            }
            return cpuInfo;
        }

        public string GenerateMachineId()
        {
            StringBuilder msg = new StringBuilder();
            msg.Append(GetCPUId());
            //foreach (string mac in GetMACAddresses())
            //    msg.Append(mac);
            msg.Append(GetVolumeSerial("C"));
            return msg.ToString();

            //use FingerPrint in the future
            // return (FingerPrint.Value());
        }
    }
}
