﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Media.Imaging;
using Flow.Lib.ImageUtils;
using System.Threading;

namespace Flow.Lib
{
	/// <summary>
	/// Represents a collection of image representations of type FlowImage
	/// or derived from FlowImage
	/// </summary>
	/// <typeparam name="T"></typeparam>
    public class FlowImageCollection<T> : ObservableCollection<T>, IDisposable where T : FlowImage, new()
	{
		public static FlowImageCollection<T> GetFlowImageCollectionFromList(IEnumerable<string> imageFileNameList)
		{
			return new FlowImageCollection<T>(imageFileNameList);
		}

		public FlowImageCollection() { }
	
		public FlowImageCollection(IEnumerable<string> imageFileNameList)
		{
			foreach (string imageFileName in imageFileNameList)
			{
				T image = new T();
				image.ImageFilename = imageFileName;

				this.Add(image);
			}
		}

        public void Dispose()
        {
            System.Windows.Forms.MessageBox.Show("FlowImage.Dispose");
        }
    }
	
	/// <summary>
	/// Represents an image and the image file
	/// </summary>
	public class FlowImage : INotifyPropertyChanged, IDisposable
	{
		private int _orientation;
		public int Orientation { get { return _orientation; } }

        public double CropL;
        public double CropT;
        public double CropW;
        public double CropH;

        public double RotationAngle;

		protected string _imageFilename = "";
		public string ImageFilename
		{
			get { return _imageFilename; }
			set
			{
				_imageFilename = value;


				this.ImageFileExists = File.Exists(_imageFilename);
              
				//if (this.ImageFileExists)
				//    Thumbnail = ImageLoader.LoadImageThumbnail(_imageFilename, 100);
				
				OnPropertyChanged("ImageFilename");
			}
		}

		public string ImageFileName { get { return new FileInfo(ImageFilename).Name; } }

		//protected BitmapImage _thumbnail = null;
		//public BitmapImage Thumbnail
		//{
		//    get { return _thumbnail; }
		//    set { _thumbnail = value; OnPropertyChanged("Thumbnail"); }
		//}

        protected BitmapImage _quickThumbnail = null;
        public BitmapImage QuickThumbnail
        {
            get
            {
                if (_quickThumbnail == null)
                {
                    //					this.ImageFileExists = File.Exists(_imageFilename);

                    _quickThumbnail = ImageLoader.LoadImage(_imageFilename, 25, out _orientation);

                    OnPropertyChanged("QuickThumbnail");
                }

                return _quickThumbnail;
            }
        }

        protected BitmapImage _quickThumbnailCropped = null;
        public BitmapImage QuickThumbnailCropped
        {
            get
            {
                if (_quickThumbnailCropped == null)
                {
                    //					this.ImageFileExists = File.Exists(_imageFilename);

                    _quickThumbnailCropped = ImageLoader.LoadImage(_imageFilename, 25, out _orientation, CropL, CropT, CropW, CropH, RotationAngle);

                    OnPropertyChanged("QuickThumbnailCropped");
                }

                return _quickThumbnailCropped;
            }
        }


		protected BitmapImage _thumbnail = null;
		public BitmapImage Thumbnail
		{
			get
			{
				if (_thumbnail == null)
				{
//					this.ImageFileExists = File.Exists(_imageFilename);

                    _thumbnail = ImageLoader.LoadImage(_imageFilename, 200, out _orientation);

					OnPropertyChanged("Thumbnail");
				}

				return _thumbnail;
			}
		}

        protected BitmapImage _thumbnailCropped = null;
        public BitmapImage ThumbnailCropped
        {
            get
            {
                if (_thumbnailCropped == null)
                {
                    //					this.ImageFileExists = File.Exists(_imageFilename);

                    _thumbnailCropped = ImageLoader.LoadImage(_imageFilename, 200, out _orientation, CropL, CropT, CropW, CropH, RotationAngle);

                    OnPropertyChanged("ThumbnailCropped");
                }

                return _thumbnailCropped;
            }
        }

        protected BitmapImage _intermediateImage = null;
        public BitmapImage IntermediateImage
        {
            get
            {
                if (_intermediateImage == null)
                    _intermediateImage = ImageLoader.LoadImage(_imageFilename, 600, out _orientation);

                return _intermediateImage;
            }
        }

        protected BitmapImage _intermediateImageCropped = null;
        public BitmapImage IntermediateImageCropped
		{
			get
			{
                if (_intermediateImageCropped == null)
                    _intermediateImageCropped = ImageLoader.LoadImage(_imageFilename, 600, out _orientation, CropL, CropT, CropW, CropH, RotationAngle);

                return _intermediateImageCropped;
			}
		}

        public BitmapImage GetProcessingImage()
        {
            return ImageLoader.LoadImage(_imageFilename, out _orientation);
        }

		protected BitmapImage _processingImage = null;
		public BitmapImage ProcessingImage
		{
			get
			{
				if (_processingImage == null)
                    _processingImage = ImageLoader.LoadImage(_imageFilename, out _orientation);

				return _processingImage;
			}
		}

        protected BitmapImage _processingImageCropped = null;
        public BitmapImage ProcessingImageCropped
        {
            get
            {
                if (_processingImageCropped == null)
                    _processingImageCropped = ImageLoader.LoadImage(_imageFilename, 3600, out _orientation, CropL, CropT, CropW, CropH, RotationAngle);

                return _processingImageCropped;
            }
        }

		public double BorderWidth
		{
			get
			{
				if (Thumbnail != null)
					return Thumbnail.Width + 10;
				return 0;
			}
		}

		private bool _imageFileExists = true;
		public bool ImageFileExists
		{
			get { return _imageFileExists; }
			set
			{
				_imageFileExists = value;
				this.OnPropertyChanged("ImageFileExists");
			}
		}


		public FlowImage() : this("") { }

		public FlowImage(string i)
		{
			ImageFilename = i;

			this.PropertyChanged += new PropertyChangedEventHandler(FlowImage_PropertyChanged);
		}

		public void Clear()
		{
            if (_processingImage != null)
            {
                if (_processingImage.StreamSource != null)
                    _processingImage.StreamSource.Dispose();
                _processingImage = null;
            }

            if (_intermediateImage != null)
            {
                if (_intermediateImage.StreamSource != null)
                    _intermediateImage.StreamSource.Dispose();
                _intermediateImage = null;
            }

            if (_thumbnail != null)
            {
                if (_thumbnail.StreamSource != null)
                    _thumbnail.StreamSource.Dispose();
                _thumbnail = null;
            }
            if (_intermediateImageCropped != null)
            {
                if (_intermediateImageCropped.StreamSource != null)
                    _intermediateImageCropped.StreamSource.Dispose();
                _intermediateImageCropped = null;
            }

            if (_thumbnailCropped != null)
            {
                if (_thumbnailCropped.StreamSource != null)
                    _thumbnailCropped.StreamSource.Dispose();
                _thumbnailCropped = null;
            }
		}

        public void Refresh()
        {
            Clear();
             //_processingImage = null;
             //_processingImage = ProcessingImage;

             //_intermediateImage = null;
             //_intermediateImage = IntermediateImage;

             //_thumbnail = null;
             //_thumbnail = Thumbnail;
            
        }

		/// <summary>
		/// Equals override to compare to FlowImage (or subclass type) objects by filename
		/// </summary>
		/// <param name="obj"></param>
		public override bool Equals(object obj)
		{
			if (base.Equals(obj))
				return true;

			FlowImage img = obj as FlowImage;
			if (img != null)
			{
				if (this.ImageFilename == img.ImageFilename)
					return true;
			}

			return false;
		}

		void FlowImage_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			if (e.PropertyName == "ImageFilename")
				OnPropertyChanged("ImageName");
		}


		#region INotifyPropertyChanged Members

		public event PropertyChangedEventHandler PropertyChanged;
		protected void OnPropertyChanged(string propName)
		{
			if (PropertyChanged != null)
				PropertyChanged(this, new PropertyChangedEventArgs(propName));
		}

		#endregion



        public void Dispose()
        {
            return;
            _intermediateImage = null;
            _intermediateImageCropped = null;
            _processingImage = null;
            _quickThumbnail = null;
            _quickThumbnailCropped = null;
            _thumbnail = null;
            _thumbnailCropped = null;
            System.GC.Collect();
            System.GC.WaitForPendingFinalizers();
            //System.Windows.Forms.MessageBox.Show("Dispose");
        }

        public int UpdateOrientation()
        {
            return ImageLoader.GetExifOrientation(this.ImageFilename);
        }
    }
}
