﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Flow.Lib
{
	public static class FlowBackgroundWorkerManager
	{
        public static Flow.Lib.AsyncWorker.FlowBackgroundWorker RunWorker(DoWorkEventHandler doWorkEventHandler)
        {
            return RunWorker(doWorkEventHandler, null);
        }

        public static Flow.Lib.AsyncWorker.FlowBackgroundWorker RunWorker(
            DoWorkEventHandler doWorkEventHandler,
            RunWorkerCompletedEventHandler runWorkerCompletedEventHandler
        )
        {
            return RunWorker( doWorkEventHandler, runWorkerCompletedEventHandler, null);
        }

        public static Flow.Lib.AsyncWorker.FlowBackgroundWorker RunWorker(
			DoWorkEventHandler doWorkEventHandler,
			RunWorkerCompletedEventHandler runWorkerCompletedEventHandler,
			object runWorkerParamter
		)
		{
            Flow.Lib.AsyncWorker.FlowBackgroundWorker worker = new Flow.Lib.AsyncWorker.FlowBackgroundWorker();
			worker.DoWork += new DoWorkEventHandler(doWorkEventHandler);

            

            if (runWorkerCompletedEventHandler != null)
            {
                worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(runWorkerCompletedEventHandler);
            }

            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);

            if (runWorkerParamter != null)
            {
                worker.RunWorkerAsync(runWorkerParamter);
            }
            else
            {
                worker.RunWorkerAsync();
            }

			return worker;
		}

        static void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
                throw new Exception("Exception occurred in background worker.", e.Error);
        }
	}
}
