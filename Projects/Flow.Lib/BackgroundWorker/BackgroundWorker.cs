﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Flow.Lib.AsyncWorker
{
    /// <summary>
    /// Manages a background thread, doing basic stops, starts, and 
    /// state queries.
    /// </summary>
    public class BackgroundWorker
    {
        Thread WorkerThread { get; set; }
        public bool ShouldRun { get; set; }
        public Action DoWork { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public BackgroundWorker()
        {
            ShouldRun = false;
            DoWork = null;
        }

        /// <summary>
        /// Returns true if the thread is running.
        /// </summary>
        public bool IsRunning
        {
            get { return WorkerThread != null && WorkerThread.IsAlive; }
        }

        /// <summary>
        /// Starts the thread
        /// </summary>
        public void Start()
        {
            Start(ApartmentState.MTA);
        }
        public void Start(ApartmentState state)
        {
            if (DoWork == null)
                return;

            ShouldRun = true;
            
            WorkerThread = new Thread(new ThreadStart(DoWork));
            WorkerThread.SetApartmentState(state);
			WorkerThread.IsBackground = true;
            WorkerThread.Start();
        }

        /// <summary>
        /// Stops the thread by setting the stop flag to true and then
        /// waiting for the worker to return.
        /// </summary>
        public void Stop()
        {
            ShouldRun = false;
            if (IsRunning)
                WorkerThread.Join();
        }


    }
}
