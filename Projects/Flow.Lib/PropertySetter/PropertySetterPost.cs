﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.Lib.Web;
using Flow.Lib.Persister;

namespace Flow.Lib.PropertySetter
{
    public class PropertySetterPost
    { 
        PostSubmitter PostSubmitter { get; set; }

        public PropertySetterPost()
            : this(new PostSubmitter())
        {
        }

        public PropertySetterPost(PostSubmitter postsubmitter)
        {
            PostSubmitter = postsubmitter;
        }

        public String Post(PropertySetterStore src)
        {
            WebServicePropertySetterMessage sendMsg = new WebServicePropertySetterMessage()
            {
                PropertySetterStore = src
            };

            //PostSubmitter.Url = @"http://localhost:64844/propertysetter";
            PostSubmitter.Url = @"http://www.flowadmin.com/activationservice/propertysetter";
            PostSubmitter.Type = PostSubmitter.PostTypeEnum.Post;
            PostSubmitter.PostItems.Add("WebServicePropertySetterMessage",
                ObjectSerializer.ToXmlString(
                new WebServicePropertySetterMessage() { PropertySetterStore = src }));
            string resultMsgStr = PostSubmitter.Post();
            //MessageBox.Show(resultMsgStr);

            if (resultMsgStr == null)
                return null;

            //WebServiceFlowOrderMessage resultMsg =
            //    ObjectSerializer.FromXmlString<WebServiceFlowOrderMessage>(
            //    resultMsgStr);


            return resultMsgStr;
        }

    }

    public class WebServicePropertySetterMessage
    {
        public PropertySetterStore PropertySetterStore { get; set; }
        public string ErrorMsg { get; set; }
    }

    
}
