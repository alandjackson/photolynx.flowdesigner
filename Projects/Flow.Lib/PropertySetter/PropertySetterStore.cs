﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flow.Lib.PropertySetter
{
    public class PropertySetterStore
    {
        public String ActivationKey { get; set; }
        public String EcommerceAPIUrl { get; set; }
        public String EcommerceAPIKey { get; set; }
    }
}
