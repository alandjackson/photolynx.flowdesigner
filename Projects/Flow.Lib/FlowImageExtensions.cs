﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Imaging;
using System.Drawing;
using System.IO;
using Flow.Lib.ImageUtils;

namespace Flow.Lib
{
    public static class FlowImageExtensions
    {
        public static BitmapImage ToBitmapImage(this FlowImage image)
        {
            try
            {
            BitmapImage bi = new BitmapImage();
            bi.BeginInit();
            bi.CacheOption = BitmapCacheOption.OnLoad;
            bi.UriSource = new Uri(image.ImageFilename);
           
                bi.Rotation = image.ProcessingImage.Rotation;
           
            bi.EndInit();
            bi.Freeze();
            return bi;
            }
            catch
            {
                //no biggy if rotation fails here
            }
            return null;
        }

        public static Bitmap ToBitmap(this FlowImage image)
        {
            try
            {
                using (Stream s = ImageLoader.ReadImageToStream(image.ImageFilename))
                {
                    Bitmap b = new Bitmap(s);
                    switch (image.ProcessingImage.Rotation)
                    {
                        case Rotation.Rotate90:
                            b.RotateFlip(RotateFlipType.Rotate90FlipNone);
                            break;
                        case Rotation.Rotate180:
                            b.RotateFlip(RotateFlipType.Rotate180FlipNone);
                            break;
                        case Rotation.Rotate270:
                            b.RotateFlip(RotateFlipType.Rotate270FlipNone);
                            break;
                    }
                    return b;
                }
            }
            catch
            {
                //no biggy if rotation fails here
            }
            return null;
        }
    }
}
