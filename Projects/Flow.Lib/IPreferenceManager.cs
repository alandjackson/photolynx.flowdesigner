﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flow.Lib
{
	public interface IPreferenceManager
	{
		T GetPreferenceValue<T>(string preferenceIdentifier);
	}
}
