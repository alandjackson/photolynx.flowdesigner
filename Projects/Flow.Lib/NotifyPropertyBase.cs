﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Flow.Lib
{
	[Serializable]
	public class NotifyPropertyBase : INotifyPropertyChanging, INotifyPropertyChanged
	{
		[field: NonSerialized]
		public event PropertyChangingEventHandler PropertyChanging = delegate { };

		[field: NonSerialized]
		public event PropertyChangedEventHandler PropertyChanged = delegate { };

		protected void SendPropertyChanging(string propertyName)
		{
			this.PropertyChanging(this, new PropertyChangingEventArgs(propertyName));
		}

		protected void SendPropertyChanged(string propertyName)
		{
            if (this.PropertyChanged != null)
			    this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
		}

	}
}
