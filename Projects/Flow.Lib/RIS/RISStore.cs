﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flow.Lib.RIS
{
    public class RISStore
    {
        public String Application { get; set; }
        public String CustomerName { get; set; }
        public String ServiceFTP { get; set; }
        public String ServiceUserName { get; set; }
        public String RemoteFolderName { get; set; }
        public int UpImageCount { get; set; }
        public int DownImageCount { get; set; }
        public String ServicesOrdered { get; set; }

        public String ActivationKey { get; set; }
        public String ProjectName { get; set; }
        public String ApplicationVersion { get; set; }
    }
}
