﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.Lib.Web;
using Flow.Lib.Persister;

namespace Flow.Lib.RIS
{
    public class RISPost
    { 
        PostSubmitter PostSubmitter { get; set; }

        public RISPost()
            : this(new PostSubmitter())
        {
        }

        public RISPost(PostSubmitter postsubmitter)
        {
            PostSubmitter = postsubmitter;
        }

        public String Post(RISStore src)
        {
            WebServiceRISMessage sendMsg = new WebServiceRISMessage()
            {
                RISStore = src
            };

            //PostSubmitter.Url = @"http://localhost:64844/remoteimageservice";
            PostSubmitter.Url = @"http://www.flowadmin.com/activationservice/remoteimageservice";
            PostSubmitter.Type = PostSubmitter.PostTypeEnum.Post;
            PostSubmitter.PostItems.Add("WebServiceRISMessage",
                ObjectSerializer.ToXmlString(
                new WebServiceRISMessage() { RISStore = src }));
            string resultMsgStr = PostSubmitter.Post();
            //MessageBox.Show(resultMsgStr);

            if (resultMsgStr == null)
                return null;

            //WebServiceFlowOrderMessage resultMsg =
            //    ObjectSerializer.FromXmlString<WebServiceFlowOrderMessage>(
            //    resultMsgStr);


            return resultMsgStr;
        }

    }

    public class WebServiceRISMessage
    {
        public RISStore RISStore { get; set; }
        public string ErrorMsg { get; set; }
    }

    
}
