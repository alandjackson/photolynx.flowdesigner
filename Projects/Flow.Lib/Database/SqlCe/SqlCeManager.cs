﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.SqlServerCe;
using System.Linq;
using System.Text;


namespace Flow.Lib.Database.SqlCe
{
	public class SqlCeManager
	{
		private SqlCeConnection _connection = null;
		public SqlCeConnection Connection
		{
			get
			{
				if (_connection == null)
					_connection = new SqlCeConnection();

				return _connection;
			}
		}

		private SqlCeDataAdapter _dataAdapter = null;
		public SqlCeDataAdapter DataAdapter
		{
			get
			{
				if (_dataAdapter == null)
					_dataAdapter = new SqlCeDataAdapter();

				return _dataAdapter;
			}
		}

		private SqlCeCommand _updateCommand = null;

		private DataTable _dataTable = null;


		public SqlCeManager(string connectionString)
		{
			if (!connectionString.StartsWith("Data Source="))
                connectionString = "Data Source=" + connectionString + ";Max Database Size=1024;";
	
			this.Connection.ConnectionString = connectionString;

		}

		public DataTableMapping AddMapping(string sourceTable, string dataSetTable)
		{
			return this.DataAdapter.TableMappings.Add(sourceTable, dataSetTable);
		}

		public void Load(DataSet dataSet, string srcTable)
		{
			SqlCeCommand command = _connection.CreateCommand();
			command.CommandType = CommandType.TableDirect;
			command.CommandText = srcTable;

			_dataAdapter.SelectCommand = command;

			SqlCeCommandBuilder commandBuilder = new SqlCeCommandBuilder(_dataAdapter);

			try
			{
				_dataAdapter.Fill(dataSet, srcTable);
			}
			catch (Exception e)
			{
				throw e;
			}
			finally
			{
				if (_connection.State != ConnectionState.Closed)
					_connection.Close();
			}

			_dataTable = dataSet.Tables[srcTable];

		}

		public void Update(DataRow dataRow)
		{
			if (_updateCommand == null)
				PrepareUpdateCommand(dataRow);
			else
			{
				foreach (DataColumnMapping mapping in _dataAdapter.TableMappings[_dataTable.TableName].ColumnMappings)
				{
					_updateCommand.Parameters["@" + mapping.SourceColumn].Value = dataRow[mapping.DataSetColumn];
				}
			}

			try
			{
				if (_updateCommand.Connection.State == ConnectionState.Closed)
					_updateCommand.Connection.Open();

				_updateCommand.ExecuteNonQuery();
			}
			catch (Exception e)
			{
				//throw e;
                //dont throw it
			}
			finally
			{
				if (_updateCommand.Connection.State != ConnectionState.Closed)
					_updateCommand.Connection.Close();
			}
		}

		public void Update(DataTable dataTable)
		{
			foreach (DataRow dataRow in dataTable.Rows)
			{
				if (_updateCommand == null)
					PrepareUpdateCommand(dataRow);
				else
				{
					foreach (DataColumnMapping mapping in _dataAdapter.TableMappings[_dataTable.TableName].ColumnMappings)
					{
						_updateCommand.Parameters["@" + mapping.SourceColumn].Value = dataRow[mapping.DataSetColumn];
					}
				}

				try
				{
					if (_updateCommand.Connection.State == ConnectionState.Closed)
						_updateCommand.Connection.Open();

					_updateCommand.ExecuteNonQuery();
				}
				catch (Exception e)
				{
					throw e;
				}
				//finally
				//{
				//    if (_updateCommand.Connection.State != ConnectionState.Closed)
				//        _updateCommand.Connection.Close();
				//}
			}

		    if (_updateCommand != null && _updateCommand.Connection.State != ConnectionState.Closed)
		        _updateCommand.Connection.Close();
		}



		private void PrepareUpdateCommand(DataRow dataRow)
		{
			_updateCommand = _connection.CreateCommand();
            _dataTable = dataRow.Table;
			DataColumnMappingCollection columnMappings = _dataAdapter.TableMappings[_dataTable.TableName].ColumnMappings;

			string commandTextTemplate = "UPDATE [{0}] SET {1} WHERE [{2}] = {3};";

			SqlCeParameter primaryKeyParameter = new SqlCeParameter();

			List<string> updateColumnList = new List<string>();

			foreach (DataColumnMapping mapping in columnMappings)
			{
				SqlCeParameter param = new SqlCeParameter(
					"@" + mapping.SourceColumn,
					dataRow[mapping.DataSetColumn]
				);

				if (mapping.DataSetColumn == _dataTable.PrimaryKey[0].ColumnName)
				{
					param.SourceColumn = mapping.SourceColumn;
					primaryKeyParameter = param;
				}
				else
					updateColumnList.Add(
						"[" + mapping.SourceColumn + "] = " + param.ParameterName
					);

				_updateCommand.Parameters.Add(param);
			}

			_updateCommand.CommandText = String.Format(commandTextTemplate,
				_dataTable.TableName,
				String.Join(", ", updateColumnList.ToArray()),
				primaryKeyParameter.SourceColumn,
				primaryKeyParameter.ParameterName
			);

		}

		public void ExecuteCommandBatch(Collection<string> commandQueue)
		{
			SqlCeCommand command = new SqlCeCommand();
			command.Connection = _connection;
			command.CommandType = CommandType.Text;

			if (command.Connection.State != ConnectionState.Open)
				command.Connection.Open();

			foreach (string commandText in commandQueue)
			{
                if (commandText.Length < 2)
                    continue;

				command.CommandText = commandText.Trim();

				try
				{
					command.ExecuteNonQuery();
				}
				catch (Exception e)
				{
					if (command.Connection.State != ConnectionState.Closed)
						command.Connection.Close();

					throw e;
				}
			}

			if (command.Connection.State != ConnectionState.Closed)
				command.Connection.Close();
	
		}

	}
}
