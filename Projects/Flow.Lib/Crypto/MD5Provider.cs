﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.IO;

namespace Flow.Lib.Crypto
{
    public class MD5Provider
    {
        #region privateMembers

        MD5CryptoServiceProvider m_md5Provider;

        #endregion

        #region constructors

        public MD5Provider()
        {
            m_md5Provider = new MD5CryptoServiceProvider();
        }

        #endregion

        #region publicMethods

        public byte[] HashFile(string file)
        {
            FileStream stream = new FileStream(file, FileMode.Open);
            byte[] hash = m_md5Provider.ComputeHash(stream);
            stream.Close();
            return hash;
        }

        #endregion
    }
}
