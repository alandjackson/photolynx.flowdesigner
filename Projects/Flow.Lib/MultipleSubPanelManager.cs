﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using Flow.Lib.Helpers;
using NLog;


namespace Flow.Lib
{
    public class MultipleSubPanelManager
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public event EventHandler<EventArgs<UIElement>> PanelShown = null;
        protected void OnPanelShown(UIElement d) { if (PanelShown != null) PanelShown(this, new EventArgs<UIElement>(d)); }

        protected List<Button> HeaderButtons { get; set; }
        protected Panel ParentPanel { get; set; }
        protected Dictionary<Type, UIElement> ChildPanels { get; set; }

        public MultipleSubPanelManager(List<Button> senderButtons, Panel parentPanel)
        {
            HeaderButtons = senderButtons;
            ParentPanel = parentPanel;
            ChildPanels = new Dictionary<Type, UIElement>();
        }
        //public void ClearChildPanels()
        //{
            
        //    foreach (KeyValuePair<Type, UIElement> kv in ChildPanels)
        //    {
        //       ((UIElement)kv.Value).
        //    }
        //    this.ChildPanels.Clear();
        //}
        public T GetPanel<T>() where T : UIElement
        {
            if (!ChildPanels.ContainsKey(typeof(T)))
                return default(T);
            return (T) ChildPanels[typeof(T)];
        }

        public void KillPanel<T>(object sender) where T : UIElement, new()
        {
            if (ChildPanels.ContainsKey(typeof(T)))
            {
                ChildPanels.Remove(typeof(T));
            }

            return ;
        }

        public T ShowPanel<T>(object sender) where T : UIElement, new()
        {
            if (!ChildPanels.ContainsKey(typeof(T)))
            {
                ChildPanels[typeof(T)] = new T();
                ParentPanel.Children.Add(ChildPanels[typeof(T)]);
            }

            return ShowPanel<T>(sender, (T)ChildPanels[typeof(T)]);
        }

        private T ShowPanel<T>(object sender, T existingPnl) where T : UIElement, new()
        {
            if (sender != null)
                EnableSender(sender);

            // show the test panel
            foreach (UIElement child in ParentPanel.Children)
                child.Visibility = (child == existingPnl ? Visibility.Visible : Visibility.Hidden);

            OnPanelShown(existingPnl);
            logger.Info("Switching to panel {0}", typeof(T).Name);

            return existingPnl;
        }

        private void EnableSender(object sender)
        {
            foreach (Button btn in HeaderButtons)
                btn.IsEnabled = !(sender == btn);
        }

    }
}
