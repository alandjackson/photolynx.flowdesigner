﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using StructureMap;

using Tamir.SharpSsh;

using NLog;

namespace Flow.Lib
{
    public class NotificationProgressInfo : IDisposable, IFtpNotificationProgressInfo
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public string Title { get; set; }
        public string Message { get; set; }
		public string MessageFormat { get; private set; }

        public int Progress { get; set; }

		public decimal Index { get; set; }

		public decimal Count { get; set; }
		
		public int Interval { get; set; }

//		private decimal RealInterval { get { return this.Interval * (100 / this.Count); } }

		private decimal PercentComplete
		{
			get { return this.Index / this.Count * 100; }
		}

		private int NextProgressValue
		{
			get { return this.Progress + this.Interval; }
		}

		public bool IsComplete { get; private set; }

		public bool IsCanceled { get; set; }

        public bool DoneButtonOn { get; private set; }

		public bool AllowCancel { get; set; }

		public IShowsNotificationProgress NotificationProgress { get; set; }

        public static void QuickStatus(string title, string message)
        {
             NotificationProgressInfo ProgressInfo = new NotificationProgressInfo(title, message);
            IShowsNotificationProgress progressNotification = ObjectFactory.GetInstance<IShowsNotificationProgress>();

            progressNotification.ShowNotification(ProgressInfo);
            ProgressInfo.NotificationProgress = progressNotification;
            ProgressInfo.Complete(message);
        }

        public NotificationProgressInfo(string t, string m)
            :this(t,m,-1)
        {
        }

        public NotificationProgressInfo(string t, string m, int p)
        {
            Title = t;
            Message = m;
            Progress = p;
        }

		public NotificationProgressInfo(string title, int startIndex, int count, int interval, string messageFormat)
			: this(title, null, startIndex,  count, interval)
		{
			this.MessageFormat = messageFormat;
			this.Message = String.Format(messageFormat, this.Index, this.Count);

		}

		public NotificationProgressInfo(string title, string message, int startIndex, int count, int interval)
		{
			this.NotificationProgress = ObjectFactory.GetInstance<IShowsNotificationProgress>();

			this.Title = title;
			this.Message = message;
			this.Index = startIndex;
			this.Count = count;
			this.Interval = interval;

		}

        public void Display()
        {
            if (NotificationProgress == null)
            {
                this.NotificationProgress = ObjectFactory.GetInstance<IShowsNotificationProgress>();
            }

			if (this.NotificationProgress != null)
			{
				this.NotificationProgress.ShowNotification(this);
//				this.NotificationProgress.UpdateNotification(this);
                logger.Info("{0} - {1}", this.Title, this.Message);
			}
        }

		public void Display(bool init)
		{
			this.Display();
			this.Progress = -1;
			this.Update();
		}

		public void Update()
		{
			if (this.MessageFormat != null)
				this.Message = String.Format(this.MessageFormat, this.Index, this.Count);
	
			if (this.NotificationProgress != null)
				this.NotificationProgress.UpdateNotification(this);
		}

		public void Update(string message)
		{
			this.Update(message, false);
		}

		public void Update(string message, bool isFormattedMessage)
		{
			if (isFormattedMessage)
			{
				this.MessageFormat = message;
				this.Message = null;
			}
			else
			{
				this.Message = message;
				this.MessageFormat = null;
			}

			this.Update();
		}

		public void Update(string message, int startIndex, int count, int interval, bool isFormattedMessage)
		{
			this.Index = startIndex;
			this.Count = count;
			this.Interval = interval;

			this.Progress = startIndex;

			this.Update(message, isFormattedMessage);
		}

		public void Complete(string message)
		{
			this.Progress = -1;
			this.IsComplete = true;
			this.Update(message);
		}

		public void Step()
		{
			this.Index++;

			if (this.Index == this.Count)
			{
				this.Progress = 100;
			}
			else if(this.PercentComplete > this.NextProgressValue)
			{
				while(this.PercentComplete > this.NextProgressValue)
					this.Progress = this.NextProgressValue;

				this.Update();
			}
		}

		public void Cancel()
		{
			this.IsCanceled = true;
			this.Complete("Operation canceled.");
		}


		public static NotificationProgressInfo operator ++(NotificationProgressInfo info)
		{
			info.Step();
			return info;
		}


		#region IDisposable Members

		public void Dispose()
		{
			this.NotificationProgress = null;
		}

		#endregion

        public void ShowDoneButton()
        {
            this.DoneButtonOn = true;
            this.Update();
        }
    }
}
