﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flow.Lib
{
    public interface IShowsNotificationProgress
    {
        void ShowNotification(NotificationProgressInfo info);
        void UpdateNotification(NotificationProgressInfo info);
    }
}
