﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Xml.Linq;

using Flow.Lib.Net;
using Flow.Lib.Preferences;
using Flow.Lib.Web;
using Flow.Lib.Helpers;
using ICSharpCode.SharpZipLib.Core;

using StructureMap;
using NetServ.Net.Json;
using System.Net;
using System.Xml.Serialization;
using System.Runtime.InteropServices;
using NLog;

namespace Flow.Lib.ImageQuix
{
	public static class ImageQuixUtil
	{

        private static Logger logger = LogManager.GetCurrentClassLogger();

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        static extern EXECUTION_STATE SetThreadExecutionState(EXECUTION_STATE esFlags);

        [FlagsAttribute]
        public enum EXECUTION_STATE : uint
        {
            ES_AWAYMODE_REQUIRED = 0x00000040,
            ES_CONTINUOUS = 0x80000000,
            ES_DISPLAY_REQUIRED = 0x00000002,
            ES_SYSTEM_REQUIRED = 0x00000001
        }

        //no longer prevent the system from sleeping
        static void AllowSleep()
        {
            SetThreadExecutionState(EXECUTION_STATE.ES_CONTINUOUS);
        }

        //prevents the system from sleeping untill the app closes or AllowSleep is called
        static void PreventSleep()
        {
            // Prevent Idle-to-Sleep (monitor not affected)
            SetThreadExecutionState(EXECUTION_STATE.ES_CONTINUOUS | EXECUTION_STATE.ES_AWAYMODE_REQUIRED);
        }

        //just resets the idle clock, must keep calling this periodically to keep awake
        static void KeepSystemAwake()
        {
            SetThreadExecutionState(EXECUTION_STATE.ES_SYSTEM_REQUIRED);
        }


		public static XDeclaration Declaration = new XDeclaration("1.0", "utf-8", null);

		public static string SubmitOrderRequest(string url, XElement custInfoXml)
		{
			PostSubmitter postSubmitter = new PostSubmitter(url);

			// Create the request document
			XDocument requestDoc = new XDocument();
			requestDoc.Declaration = ImageQuixUtil.Declaration;

			XElement orderElement = new XElement("Order");
			orderElement.Add(custInfoXml);

			requestDoc.Add(orderElement);

			// Prepare the request data
			string postData = requestDoc.Declaration.ToString() + requestDoc.ToString();

			// Submit the request, retrieving the result (orderPk)
			string orderID = postSubmitter.PostFileData(postData);
			//orderID = orderID.ZeroPad(8);

			return orderID;
		}

		/// <summary>
		/// Submits a list of subject orders to the ImageQuix processing system
		/// </summary>
		/// <param name="imageDirPath"></param>
		/// <param name="orderDataList"></param>
        public static JsonObject SubmitOrder(
			List<ZipFileMapping> orderFiles,
			XDocument projectDataXDocument,
			XDocument orderXDocument,
            XElement custInfoXml,
			UploadPreferences uploadPreferences,
			string saveOrderExportPath,
            string projectName,
            string reportFileName,
            string orderFormFileName,
            string catalogPackagesFileName,
            JsonArray orderOptionResourceURLs,
            List<KeyValuePair<ZipFileMapping, XElement>> groupImageFileList,
            Emailer mail
		)
		{
            KeepSystemAwake();
            NotificationProgressInfo ProgressInfo = new NotificationProgressInfo("Preparing Order", "Initializing");
            ProgressInfo.NotificationProgress = ObjectFactory.GetInstance<IShowsNotificationProgress>();
            ProgressInfo.AllowCancel = true;
            ProgressInfo.Display(true);

            ProgressInfo.Update("Sending order data to server for validation");

            JsonObject SubmitReturnObject = new JsonObject();

            if (ProgressInfo.IsCanceled)
            {
               return null;
            }

            JsonObject rootJsonOrder = CreateJsonOrder(projectDataXDocument, orderXDocument, custInfoXml, projectName, orderOptionResourceURLs, groupImageFileList);

            if (1 == 1)
            {



               


                //// Create the project data xml field
                projectDataXDocument.Declaration = ImageQuixUtil.Declaration;
                string projectDataFilePath = FlowContext.Combine(FlowContext.FlowTempDirPath, "projectData.xml");
                projectDataXDocument.Save(projectDataFilePath);
                //orderFiles.Add(new ZipFileMapping(projectDataFilePath));

                //// Create the order data xml field
                orderXDocument.Declaration = ImageQuixUtil.Declaration;
                string orderDataFilePath = FlowContext.Combine(FlowContext.FlowTempDirPath, "JSON_ORDER.xml");
                orderXDocument.Save(orderDataFilePath);
                //orderFiles.Add(new ZipFileMapping(orderDataFilePath));

                // Create text File containing json order
                JsonWriter writert = new JsonWriter();
                rootJsonOrder.Write(writert);
                string jsonDatat = writert.ToString().Replace("\\/", "/");
                string jsonDataFilePatht = FlowContext.Combine(FlowContext.FlowTempDirPath, "JSON_ORDER.txt");
                StreamWriter swt = File.CreateText(jsonDataFilePatht);
                swt.Write(jsonDatat);
                swt.Close();
            }


            if (ProgressInfo.IsCanceled)
            {
                return null;
            }

            string orderUri = uploadPreferences.OrderSubmission.ImageQuixOrderSubmissionUrl;
            string username = uploadPreferences.CatalogRequest.GetCatalogUsername;
            string password = uploadPreferences.CatalogRequest.GetCatalogPassword;

            try
            {
                SubmitReturnObject = SubmitJsonOrder(rootJsonOrder, orderUri, username, password);

            }
            catch (Exception e)
            {
                
                throw e;
            }

            ProgressInfo.Update("Done sending order data.");
            ProgressInfo.Complete("Done sending order data.");

            if (ProgressInfo.IsCanceled)
            {
                return null;
            }

            string orderID = SubmitReturnObject["orderID"].ToString();
            //string ftpAddress = SubmitReturnObject["IQ_FtpServer"].ToString();
            //string ftpDirectory = "/";
            //if (ftpAddress.Contains("/"))
            //{
            //    ftpAddress = ftpAddress.Left(ftpAddress.IndexOf("/"));
            //    ftpDirectory = ftpAddress.Right(ftpAddress.Length - ftpAddress.IndexOf("/"));
            //}
            //int ftpPort = 21;

            //if (ftpAddress.Contains(":"))
            //{
            //    ftpPort = Convert.ToInt32((ftpAddress.Split(":")[1]).Split("/")[0]);
            //    ftpAddress = ftpAddress.Split(":")[0];
            //}
            //string ftpUserName = SubmitReturnObject["IQ_user"].ToString();
            //string ftpPassword = SubmitReturnObject["IQ_password"].ToString();
            //bool useSftp = false;
            //if (SubmitReturnObject["IQ_ftpType"].ToString().ToLower() == "sftp")
            //{
            //    useSftp = true;
            //    ftpPort = 22;
            //}



            // Get ftp settings
            string ftpAddress = uploadPreferences.OrderSubmission.ImageQuixFtpAddress;
            int ftpPort = uploadPreferences.OrderSubmission.ImageQuixFtpPort;
            string ftpUserName = uploadPreferences.OrderSubmission.ImageQuixFtpUserName;
            string ftpPassword = uploadPreferences.OrderSubmission.ImageQuixFtpPassword;
            string ftpDirectory = uploadPreferences.OrderSubmission.ImageQuixFtpDirectory;
            bool useSftp = uploadPreferences.OrderSubmission.ImageQuixUseSftp;

            ftpDirectory = ftpDirectory + "/order_" + orderID + "/";

            foreach (KeyValuePair<ZipFileMapping, XElement> kvp in groupImageFileList)
                orderFiles.Add(kvp.Key);

            // Create text File containing json order
            JsonWriter writer = new JsonWriter();
            rootJsonOrder.Write(writer);
            string jsonData = writer.ToString().Replace("\\/", "/");
            string jsonDataFilePath = FlowContext.Combine(FlowContext.FlowTempDirPath, "json_" + orderID + ".txt");
            StreamWriter sw = File.CreateText(jsonDataFilePath);
            sw.Write(jsonData);
            sw.Close();
            orderFiles.Add(new ZipFileMapping(jsonDataFilePath));

            if(reportFileName != null && File.Exists(reportFileName))
                {
                    string customFileName = reportFileName.Replace(".pdf", "_" + orderID + ".pdf");

                    File.Move(reportFileName, customFileName);
                    orderFiles.Add(new ZipFileMapping(customFileName));
                }

            if (orderFormFileName != null && File.Exists(orderFormFileName))
            {
                string customFileName = orderFormFileName.Replace(".pdf", "_" + orderID + ".pdf");

                File.Move(orderFormFileName, customFileName);
                orderFiles.Add(new ZipFileMapping(customFileName));
            }

            if (catalogPackagesFileName != null && File.Exists(catalogPackagesFileName))
            {
                orderFiles.Add(new ZipFileMapping(catalogPackagesFileName));
            }

            if (ProgressInfo.IsCanceled)
            {
                return null;
            }

            string doneFileFilePath = FlowContext.Combine(FlowContext.FlowTempDirPath, "done.txt");
            if(!File.Exists(doneFileFilePath))
                File.Create(doneFileFilePath);



			// upload the zip file;
			// upload a trigger file name {orderPK}.txt

			// If the request is for order submission to an IQ server
            if (saveOrderExportPath == null || saveOrderExportPath.Length < 1)
			{
                orderFiles.Add(new ZipFileMapping(doneFileFilePath));

				int fileCount = orderFiles.Count + 1;

                List<string> fileList = new List<string>();
                String tempOrderFolder = Path.Combine(FlowContext.FlowTempDirPath, "order_" + orderID);
                Directory.CreateDirectory(tempOrderFolder);
                foreach (ZipFileMapping thisFile in orderFiles)
                {
                    string tempFile = Path.Combine(tempOrderFolder, thisFile.DestinationFileName);
                    if(!File.Exists(tempFile))
                    {
                        File.Copy(thisFile.SourceFilePath, tempFile);
                        fileList.Add(tempFile);
                    }
                }


                string result = "";
                NotificationProgressInfo ftpProgress = new NotificationProgressInfo("Upload Order", "Initializing...", 0, fileCount, 1);
                //fileList.Add(doneFileFilePath);

                PreventSleep();
                FlowBackgroundWorkerManager.RunWorker(
                    delegate
                    {
                        IFTPClient ftp;

                        if (useSftp)
                            ftp = new SFTPClient(ftpAddress, ftpPort, ftpUserName, ftpPassword);
                        else
                            ftp = new FTPClient(ftpAddress, ftpPort, ftpUserName, ftpPassword);

                        ftp.ProgressInfo = ftpProgress;
                        ftp.ProgressInfo.AllowCancel = false;
                        ftp.ProgressInfo.Display();

                        result = ftp.BeginFileUploads(fileList.ToArray(), ftpDirectory);
                        if (result.ToLower().Contains("canceled"))
                        {
                            ftp.ProgressInfo.Cancel();
                        }
                        foreach (string thisOne in fileList)
                            File.Delete(thisOne);


                    },
                    delegate
                    {
                        ftpProgress.Complete("");
                        if (mail != null)
                        {
                            string customFileName = orderFormFileName.Replace(".pdf", "_" + orderID + ".pdf");
                            mail.AddAttachment(customFileName);
                            mail.Send();
                              
                        }
                        AllowSleep();
                    }
                );

                if (result.ToLower().Contains("canceled"))
                {
                    return null;
                }
			}
			
			// If the request is for local storage of order file
			else
			{

                //Zip the images and the order xml file
                string orderZipFilePath = FlowContext.Combine(FlowContext.FlowTempDirPath, orderID + ".zip");

                NotificationProgressInfo progressInfo = new NotificationProgressInfo("Create zip file", "Initializing...");
                progressInfo.NotificationProgress = ObjectFactory.GetInstance<IShowsNotificationProgress>();
                progressInfo.Display(true);

                if (progressInfo.IsCanceled)
                    return SubmitReturnObject;

                ZipUtility.CreateArchive(
                    orderZipFilePath,
                    orderFiles,
                    progressInfo
                );

                progressInfo.Complete("Zip file created.");

                // Create trigger file
                string triggerFilePath = FlowContext.Combine(FlowContext.FlowTempDirPath, orderID + "_done.txt");
                File.Create(triggerFilePath);

				FileInfo orderZipFileInfo = new FileInfo(orderZipFilePath);
				string destination = Path.Combine(saveOrderExportPath, orderZipFileInfo.Name);
				File.Move(orderZipFilePath, destination);
			}

            return SubmitReturnObject;
		}

        private static JsonObject CreateJsonOrder(XDocument projectDataXDocument, XDocument orderXDocument, XElement custInfoXml, String projectName, JsonArray orderOptionResourceURls, List<KeyValuePair<ZipFileMapping, XElement>> groupImageFileList)
        {
            JsonObject root = new JsonObject();
            //root.Add("name", DateTime.Now.ToString());
            root.Add("name", projectName);
            root.Add("order-date", DateTime.Today.ToShortDateString());
            root.Add("order-option-selections", orderOptionResourceURls);
            XElement dataRoot = projectDataXDocument.Element("project");

            //customer-information object
            JsonObject cutInfo = new JsonObject();
            {
                if( dataRoot.Attribute("studioID").Value.Length < 1)
                    cutInfo.Add("billing-account-number", "Unknown");
                else
                    cutInfo.Add("billing-account-number", dataRoot.Attribute("studioID").Value);

                cutInfo.Add("billing-company", dataRoot.Attribute("studioName").Value);
                cutInfo.Add("billing-first-name", custInfoXml.Attribute("billingFirstName").Value);
                cutInfo.Add("billing-last-name", custInfoXml.Attribute("billingLastName").Value);
                cutInfo.Add("billing-address-1", custInfoXml.Attribute("billingAddress").Value);
                cutInfo.Add("billing-address-2", null);
                cutInfo.Add("billing-city", custInfoXml.Attribute("billingCity").Value);
                cutInfo.Add("billing-state", custInfoXml.Attribute("billingState").Value);
                cutInfo.Add("billing-zipcode", custInfoXml.Attribute("billingZip").Value);
                cutInfo.Add("billing-country", custInfoXml.Attribute("billingCountry").Value);
                cutInfo.Add("billing-phone-number", custInfoXml.Attribute("phone").Value);
                cutInfo.Add("shipping-first-name", custInfoXml.Attribute("shippingFirstName").Value);
                cutInfo.Add("shipping-last-name", custInfoXml.Attribute("shippingLastName").Value);
                cutInfo.Add("shipping-address-1", custInfoXml.Attribute("shippingAddress").Value);
                cutInfo.Add("shipping-address-2", null);
                cutInfo.Add("shipping-city", custInfoXml.Attribute("shippingCity").Value);
                cutInfo.Add("shipping-state", custInfoXml.Attribute("shippingState").Value);
                cutInfo.Add("shipping-zipcode", custInfoXml.Attribute("shippingZip").Value);
                cutInfo.Add("shipping-country", custInfoXml.Attribute("shippingCountry").Value);
                cutInfo.Add("shipping-phone-number", custInfoXml.Attribute("phone").Value);
                cutInfo.Add("email-address", custInfoXml.Attribute("email").Value.Replace(';', ',').Replace(':', ',').Split(',')[0]);
            }


            root.Add("customer-information", cutInfo);

            JsonArray entries = new JsonArray();
            {
                XElement xmlOrderRoot = orderXDocument.Element("Order");
                //foreach product
                foreach(XElement xmlProduct in xmlOrderRoot.Elements("Product"))
                {
                    //if product-ref is not found, then we cant place the product order
                    if(xmlProduct.Attribute("product-ref").Value.Contains("not found"))
                        continue;

                    JsonObject thisProduct = new JsonObject();
                    thisProduct.Add("product-ref", xmlProduct.Attribute("product-ref").Value);
                    thisProduct.Add("quantity", xmlProduct.Attribute("quantity").Value);
                    JsonArray theseNodes = new JsonArray();
                    //foreach node
                    foreach(XElement xmlNode in xmlProduct.Elements("Node"))
                    {
                        if (xmlNode.Attribute("type").Value != "image")
                            continue;

                        JsonObject thisNode = new JsonObject();
                        //need to create the node-ref based on the product-ref and nodeFK
                        thisNode.Add("node-ref", xmlProduct.Attribute("product-ref").Value + "/node/" + Convert.ToInt32(xmlNode.Attribute("nodeFK").Value));
                        thisNode.Add("type", xmlNode.Attribute("type").Value);

                        if (xmlNode.Attribute("type").Value == "image")
                            thisNode.Add("filename", xmlNode.Attribute("imageFileName").Value);
                        else if (xmlNode.Attribute("type").Value == "text")
                            thisNode.Add("text", xmlNode.Attribute("title").Value);

                        thisNode.Add("crop-x", "0");
                        thisNode.Add("crop-y", "0");
                        thisNode.Add("crop-w", "0");
                        thisNode.Add("crop-h", "0");
                        thisNode.Add("orientation", "0");
                        //thisNode.Add("crop-x", xmlNode.Attribute("cropX").Value);
                        //thisNode.Add("crop-y", xmlNode.Attribute("cropY").Value);
                        //thisNode.Add("crop-w", xmlNode.Attribute("cropWidth").Value);
                        //thisNode.Add("crop-h", xmlNode.Attribute("cropHeight").Value);
                        //thisNode.Add("orientation", xmlNode.Attribute("orientation").Value);

                        theseNodes.Add(thisNode);
                    }
                    thisProduct.Add("nodes", theseNodes);

                    JsonArray theseOptions = new JsonArray();
                    //foreach option
                    foreach(XElement xmlOption in xmlProduct.Elements("Option"))
                    {
                        JsonObject thisOption = new JsonObject();
                        string optionId = xmlOption.Attribute("optionFK").Value;
                        thisOption.Add("option-group-ref", xmlOption.Attribute("option-group-ref").Value);
                        thisOption.Add("option-ref", xmlOption.Attribute("option-ref").Value);

                        theseOptions.Add(thisOption);
                    }
                    thisProduct.Add("option-selections", theseOptions);

                    entries.Add(thisProduct);
                }
            }

            root.Add("entries", entries);




            JsonObject metadata = new JsonObject();

            XElement rootProject = projectDataXDocument.Element("project");
            metadata.Add("guid", rootProject.Attribute("guid").Value);

            JsonArray fields = new JsonArray();
            foreach (XElement xmlfield in rootProject.Element("fields").Elements())
            {
                JsonObject field = new JsonObject();
                field.Add("field", xmlfield.Name.ToString().ToLower());
                fields.Add(field);
            }
            JsonObject imagefieldheader = new JsonObject();
            imagefieldheader.Add("field", "images");
            fields.Add(imagefieldheader);

            metadata.Add("fields", fields);
            JsonArray images = new JsonArray();

            JsonArray records = new JsonArray();
            //List<string> GroupImagenames = new List<string>();
            List<string> uniqueImagenames = new List<string>();
            JsonArray GroupImagefiles = new JsonArray();
            foreach (XElement xmlRecord in rootProject.Element("records").Elements("record"))
            {

                JsonObject record = new JsonObject();
                foreach (XElement xmlfield in rootProject.Element("fields").Elements())
                {
                    string fieldName = xmlfield.Name.ToString();

                    // TODO: CHAD: xmlRecord.Attribute(fieldName) is null causing an 
                    // exception to be thrown that the server cannot be contacted when
                    // ImageQuixUtil.SubmitOrder is called from DesignerController.BatchOrder

                    string value = xmlRecord.Attribute(fieldName) != null
                        ? xmlRecord.Attribute(fieldName).Value
                        : "";
                    record.Add(fieldName.ToLower(), value);

                   
                }
                JsonArray imagefiles = new JsonArray();
                
                foreach (XElement imagefield in xmlRecord.Elements("image"))
                {
                    

                    if(!uniqueImagenames.Contains(imagefield.Attribute("filename").Value))
                    {
                        imagefiles.Add(imagefield.Attribute("filename").Value);

                        uniqueImagenames.Add(imagefield.Attribute("filename").Value);

                        JsonObject thisImage = new JsonObject();
                        //thisImage.Add("filename", imagefield.Attribute("filename").Value);
                        thisImage.Add("name", imagefield.Attribute("filename").Value);
                        thisImage.Add("crop-x", imagefield.Attribute("crop-x").Value);
                        thisImage.Add("crop-y", imagefield.Attribute("crop-y").Value);
                        thisImage.Add("crop-w", imagefield.Attribute("crop-w").Value);
                        thisImage.Add("crop-h", imagefield.Attribute("crop-h").Value);
                        thisImage.Add("orientation", imagefield.Attribute("orientation").Value);
                        JsonArray imageOptions = new JsonArray();
                        foreach (string url in imagefield.Attribute("image-option-selections").Value.Split(","))
                        {
                            if (url.Length > 1)
                            {
                                imageOptions.Add(url);
                            }
                        }
                        thisImage.Add("image-option-selections", imageOptions);
                    
                        images.Add(thisImage);
                    }
                }


               

                //add array of image files to the record
                record.Add("images",imagefiles);
               


                records.Add(record);
            }

            //do the group photos
            foreach (KeyValuePair<ZipFileMapping, XElement> giv in groupImageFileList)
            {
                JsonArray imagefiles = new JsonArray();
                XElement imagefield = giv.Value;
                if (!uniqueImagenames.Contains(imagefield.Attribute("filename").Value))
                {
                    uniqueImagenames.Add(imagefield.Attribute("filename").Value);
                    imagefiles.Add(imagefield.Attribute("filename").Value);

                    //make a record for the "records" array in the metadata section
                    JsonObject Grecord = new JsonObject();
                    foreach (XElement xmlfield in rootProject.Element("fields").Elements())
                    {
                        string fieldName = xmlfield.Name.ToString();
                        if (fieldName.ToLower() == "subjectid")
                            Grecord.Add(fieldName.ToLower(), TicketGenerator.GenerateTicketString(8));
                        else if (fieldName.ToLower() == "lastname")
                            Grecord.Add(fieldName.ToLower(), imagefield.Attribute("description").Value);
                        else if (fieldName.ToLower() == "firstname")
                            Grecord.Add(fieldName.ToLower(), "Group Image");
                        else if (fieldName.ToLower() == "teamname")
                            Grecord.Add(fieldName.ToLower(), imagefield.Attribute("description").Value);
                        else
                            Grecord.Add(fieldName.ToLower(), "");
                    }
                    Grecord.Add("images", imagefiles);
                    records.Add(Grecord);

                    //thisImage is added to the root level list of all images called "image-data"
                    JsonObject thisImage = new JsonObject();
                    //thisImage.Add("filename", imagefield.Attribute("filename").Value);
                    thisImage.Add("name", imagefield.Attribute("filename").Value);
                    thisImage.Add("crop-x", imagefield.Attribute("crop-x").Value);
                    thisImage.Add("crop-y", imagefield.Attribute("crop-y").Value);
                    thisImage.Add("crop-w", imagefield.Attribute("crop-w").Value);
                    thisImage.Add("crop-h", imagefield.Attribute("crop-h").Value);
                    thisImage.Add("orientation", imagefield.Attribute("orientation").Value);
                    JsonArray imageOptions = new JsonArray();
                    thisImage.Add("image-option-selections", imageOptions);
                    images.Add(thisImage);
                }
            }

            metadata.Add("records", records);
            root.Add("image-data", images);

            root.Add("metadata", metadata);

            string JsonString = root.ToString();
            return root;

        }

        public static JsonObject SubmitJsonOrder(JsonObject orderObject, string uri, string username, string password)
        {
            logger.Info("z " + uri);
            HttpWebRequest rq = (HttpWebRequest)WebRequest.Create(uri);
            rq.Credentials = new NetworkCredential(username, password);
            rq.Headers.Add("x-iq-token", "A15M876TJ8");
            rq.Timeout = ((60 * 5) * 1000);//try for 5 minutes (60 seconds * 5 minutes * 1000 = miliseconds)
            rq.Method = "POST";
            rq.ContentType = "application/json";


            JsonWriter writer = new JsonWriter();
            orderObject.Write(writer);
            string postData = writer.ToString().Replace("\\/","/");
            //Console.Write(postData);

            rq.ContentLength = postData.Length;


            StreamWriter stOut = new
            StreamWriter(rq.GetRequestStream(),
            System.Text.Encoding.ASCII);
            stOut.Write(postData);
            stOut.Close();

            HttpWebResponse rs;
            try
            {
                rs = (HttpWebResponse)rq.GetResponse();
                 //HttpWebResponse rs = (HttpWebResponse)rq.GetResponse();
                string orderLocation = rs.Headers["Location"];
                string IQ_FtpServer = rs.Headers["IQ-FTP-SERVER"];
                string IQ_user = rs.Headers["IQ-FTP-USER"];
                string IQ_password = rs.Headers["IQ-FTP-PASSWORD"];
                string IQ_ftpType = rs.Headers["IQ-FTP-TYPE"];

                string orderID = orderLocation.ToString().Split("/").Last();
                JsonObject returnObject = new JsonObject();
                returnObject.Add("orderID", orderID);
                returnObject.Add("orderLocation", orderLocation);
                returnObject.Add("IQ_FtpServer", IQ_FtpServer);
                returnObject.Add("IQ_user", IQ_user);
                returnObject.Add("IQ_password", IQ_password);
                returnObject.Add("IQ_ftpType", IQ_ftpType);

                rs.Close();
                return returnObject;
            }
            catch (WebException e)
            {
                throw e;
            }
           
        }
	}





	///// <summary>
	///// Represents the data necessary to submit an order to the ImageQuix system
	///// </summary>
	//public class ImageQuixOrderData
	//{
	//    public XElement RequestXml { get; set; }
	//    public XElement ProjectDataXml { get; set; }
	//    public List<XElement> OrderXml { get; set; }
	//    public List<string> FileList { get; set; }
	//}

}
