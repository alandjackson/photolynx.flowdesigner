﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Media.Imaging;
using System.IO;
using System.Threading;
using System.Windows;
using System.Windows.Media;
using System.Windows.Controls;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

namespace Flow.Lib.ImageUtils
{
    public class ImageLoader
    {
        protected static int DefaultDisplayHeight = 600;

        public static MemoryStream ReadImageToStream(string path)
        {
            if (!File.Exists(path)) return null;
            byte[] buffer = System.IO.File.ReadAllBytes(path);

            MemoryStream ms1 = new MemoryStream(buffer);
            return ms1;
        }

        public static BitmapImage LoadImage(string imagePath)
        {
            return LoadImage(imagePath, DefaultDisplayHeight);
        }

		public static BitmapImage LoadImage(string imagePath, int thumbSize)
		{
			int rotation;
            return LoadImage(imagePath, thumbSize, out rotation);
		}

        public static BitmapImage LoadImage(string imagePath, out int rotation)
        {
            return LoadImage(imagePath, DefaultDisplayHeight, out rotation);
        }

        public static BitmapImage LoadImage(string imagePath, int thumbSize, out int rotation)
        {
            int rot = 0;
            BitmapImage img = null;
            if (RepeatTry(() => img = LoadImageInternal(imagePath, thumbSize, out rot, 0, 0, 0, 0, 0),
                7, 1000))
            {
                rotation = rot;
                return img;
            }
            else
            {
                rotation = 0;
                return null;
            }
        }

        public static BitmapImage LoadImage(string imagePath, int thumbSize, out int rotation, double CropL, double CropT, double CropW, double CropH, double RotationAngle)
        {
            int rot = 0;
            BitmapImage img = null;
            if (RepeatTry(() => img = LoadImageInternal(imagePath, thumbSize, out rot, CropL, CropT, CropW, CropH, RotationAngle),
                7, 1000))
            {
                rotation = rot;

                if (RotationAngle > 550)
                {
                    TransformedBitmap TempImage = new TransformedBitmap();

                    TempImage.BeginInit();
                    TempImage.Source = img; // MyImageSource of type BitmapImage

                    RotateTransform transform = new RotateTransform(RotationAngle);
                    TempImage.Transform = transform;
                    TempImage.EndInit();
                    img = (BitmapImage)TempImage.Source;

                }

                return img;
            }
            else
            {
                rotation = 0;
                return null;
            }
        }

        public static BitmapImage LoadImageInternal2(string imagePath, int thumbSize, 
            out int rotation, double CropL, double CropT, double CropW, double CropH)
        {
            rotation = 0;

            if (!File.Exists(imagePath))
                return null;

            var source = LoadImageSourceInternal(imagePath, thumbSize, out rotation);
            var image = SourceToImage(source);
            //image.Rotation = ToRotation(rotation);
            //image.SourceRect = getCropArea(image.PixelWidth, image.PixelHeight, rotation,
            //    CropL, CropT, CropW, CropH);
            image.Freeze();
            return image;
        }

        private static BitmapImage SourceToImage(BitmapSource bitmapSource)
        {
            JpegBitmapEncoder encoder = new JpegBitmapEncoder();
            using (MemoryStream memoryStream = new MemoryStream())
            {
                BitmapImage bImg = new BitmapImage();

                encoder.Frames.Add(BitmapFrame.Create(bitmapSource));
                encoder.Save(memoryStream);

                bImg.BeginInit();
                bImg.StreamSource = new MemoryStream(memoryStream.ToArray());
                bImg.CacheOption = BitmapCacheOption.OnLoad;
                bImg.EndInit();
                return bImg;
            }
        }

        public static BitmapSource LoadImageSourceInternal(string imagePath, int thumbSize, out int rotation)
        {
            using (var imageStream = System.IO.File.OpenRead(imagePath))
            {
                BitmapFrame bitmapFrame = BitmapFrame.Create(imageStream, 
                    BitmapCreateOptions.None, BitmapCacheOption.OnLoad);
                rotation = GetRotation(bitmapFrame.Metadata as BitmapMetadata, null);

                if (thumbSize > 0 && bitmapFrame.Thumbnail != null)
                    return bitmapFrame.Thumbnail;
                else
                    return bitmapFrame;
            }
        }

        public static BitmapImage LoadImageInternal(string imagePath, int thumbSize, out int rotation, double CropL, double CropT, double CropW, double CropH, double RotationAngle)
        {
            //set a default in case image isn't found
            rotation = 0;

            if (!File.Exists(imagePath))
                return null;


           



            // load the image, specify CacheOption so the file is not locked
            BitmapImage img = new BitmapImage();
            img.BeginInit();
            if (thumbSize > 0)
                img.DecodePixelHeight = thumbSize;
            img.CacheOption = BitmapCacheOption.OnLoad;
            
            img.UriSource = new Uri(imagePath);
            rotation = Rotate(imagePath, ref img);

           

            if ((CropL + CropT + CropW + CropH) > 0)
            {
                //there are crop values that need to be used
                Bitmap tmpBmp = new Bitmap(imagePath);

 
                if (rotation == 90)
                    tmpBmp.RotateFlip(RotateFlipType.Rotate90FlipNone);
                if (rotation == 180)
                    tmpBmp.RotateFlip(RotateFlipType.Rotate180FlipNone);
                if (rotation == 270)
                    tmpBmp.RotateFlip(RotateFlipType.Rotate270FlipNone);

                Int32Rect rect = getCropArea(tmpBmp, rotation, CropL, CropT, CropW, CropH);
                tmpBmp.Dispose();
                img.SourceRect = rect;
            }

           
            img.EndInit();
            img.Freeze();
           
            
            return img;
        }


       


       

        public static bool RepeatTry(Action a, int times, int sleep)
        {
            for (int i = 0; i < times; i++)
            {
                try
                {
                    a();
                    return true;
                }
                catch (Exception E)
                {
                    Thread.Sleep(sleep);
                }
            }
            return false;
        }

        private static Int32Rect getCropArea(Bitmap iSrc, int rotation, double CropL, double CropT, double CropW, double CropH)
        {
            return getCropArea(iSrc.Width, iSrc.Height, rotation, CropL, CropT, CropW, CropH);
        }

        private static Int32Rect getCropArea(double width, double height, int rotation, double CropL, double CropT, double CropW, double CropH)
        {
            if (CropL < 0) CropL = 0;
            if (CropT < 0) CropT = 0;
            if (CropW < 0) CropW = 0;
            if (CropH < 0) CropH = 0;
            int cropAreaL = (int)(CropL * width);
            int cropAreaT = (int)(CropT * height);
            int cropAreaW = (int)(CropW * width);
            int cropAreaH = (int)(CropH * height);

            if (cropAreaL + cropAreaW > width)
                cropAreaW = (int)width - cropAreaL;
            if (cropAreaT + cropAreaH > height)
                cropAreaH = (int)height - cropAreaT;

            if (rotation == 90)
            {
                return new Int32Rect(
                    cropAreaT,
                    ((int)width - cropAreaL) - cropAreaW,
                    cropAreaH,
                    cropAreaW
                    );
            }

            if (rotation == 270)
            {
                return new Int32Rect(
                    ((int)height - cropAreaT) - cropAreaH,
                    cropAreaL,
                    cropAreaH,
                    cropAreaW
                    );
            }
            
            return new Int32Rect(
                cropAreaL,
                cropAreaT,
                cropAreaW,
                cropAreaH
                );
        }
		/// <summary>
		/// Loads an image file and saves a reduced image to a separate file.
		/// </summary>
		/// <param name="imageFilePath"></param>
		/// <param name="destinationDir"></param>
		public static void SaveReducedImage(string imageFilePath, string destinationDir, int reducedSize)
		{
			FileInfo imageFileInfo = new FileInfo(imageFilePath);

			if (!imageFileInfo.Exists)
				return;

			using (System.Drawing.Image image = System.Drawing.Image.FromFile(imageFilePath))
			{
				decimal ratio = 100m / (decimal)image.Height;

				int conversionWidth = (int)(ratio * (decimal)image.Width);

                System.Drawing.Image thumb = image.GetThumbnailImage(conversionWidth, reducedSize, delegate() { return false; }, IntPtr.Zero);

				thumb.Save(Path.Combine(destinationDir,imageFileInfo.Name));
			}
		}


        /// <summary>
        /// Rotates a BitmapImage according to the Orientation field 
        /// in the EXIF data of the original file
        /// </summary>
        /// <param name="image">a BitmapImage object, passed by reference</param>
        /// <param name="path">a String containing the path to the file in the file system</param>
        private static int Rotate(String path, ref BitmapImage image)
        {
			int rotation = 0;
            if (!File.Exists(path)) return 0;

            BitmapSource img = BitmapFrame.Create(new Uri(path));
            rotation = GetRotation((BitmapMetadata)img.Metadata, image);
            return rotation;
        }

        private static Rotation ToRotation(int rotation)
        {
            switch (rotation % 360)
            {
                case 90: return Rotation.Rotate90;
                case 180: return Rotation.Rotate180;
                case 270: return Rotation.Rotate270;
                default: return Rotation.Rotate0;
            }
        }

        private static int GetRotation(BitmapMetadata meta, BitmapImage image)
        {
            String orientationQuery = "System.Photo.Orientation";
            if ((meta != null) && (meta.ContainsQuery(orientationQuery)))
            {
                object o = meta.GetQuery(orientationQuery);
                if (o != null)
                {
                    switch ((ushort)o)
                    {
                        case 6:
                            if (image != null) image.Rotation = Rotation.Rotate90;
                            return 90;
                            //image.Rotation = Rotation.Rotate90;
                            //rotation = 90;
                            //break;
                        case 3:
                            if (image != null) image.Rotation = Rotation.Rotate180;
                            return 180;
                            //image.Rotation = Rotation.Rotate180;
                            //rotation = 180;
                            //break;
                        case 8:
                            if (image != null) image.Rotation = Rotation.Rotate270;
                            return 270;
                            //image.Rotation = Rotation.Rotate270;
                            //rotation = 270;
                            //break;
                    }
                }
            }
            return 0;
        }



        internal static int GetExifOrientation(string imagePath)
        {
            int rotation = 0;
            if (!File.Exists(imagePath)) return 0;

            using (var imageStream = System.IO.File.OpenRead(imagePath))
            {
                BitmapFrame bitmapFrame = BitmapFrame.Create(imageStream,
                    BitmapCreateOptions.None, BitmapCacheOption.OnLoad);
                rotation = GetRotation(bitmapFrame.Metadata as BitmapMetadata, null);
                imageStream.Close();
                imageStream.Dispose();
                return rotation;
            }
        }
    }
}
