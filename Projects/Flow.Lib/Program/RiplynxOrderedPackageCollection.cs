/* ----------------------------------------------------------------------------
 * $Id: RiplynxOrderedPackageCollection.cs 2989 2008-02-06 21:39:24Z photolyn $
 *
 * --------------------------------------------------------------------------*/

using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Data;
using System.Collections;
using System.Diagnostics;
using System.Collections.Generic;
using PhotoLynx.ImageRender.Primitives;
using NUnit.Framework;

namespace PhotoLynx.ImageRender.RiplynxInterop
{
public class RiplynxOrderedPackageCollection
{
  ArrayList m_ordered_packages = new ArrayList();
  ArrayList m_options = new ArrayList();

  [XmlIgnore()]
  public ArrayList OrderedPackages
  {
    get { return (m_ordered_packages); }
    set { m_ordered_packages = value; }
  }

  [XmlIgnore()]
  public ArrayList Options
  {
    get { return (m_options); }
    set { m_options = value; }
  }

  public RiplynxOrderedPackage [] OrderedPackagesArray
  {
    get
    {
      RiplynxOrderedPackage [] arr = new RiplynxOrderedPackage[m_ordered_packages.Count];
      for (int i = 0; i < m_ordered_packages.Count; i++)
      {
        arr[i] = m_ordered_packages[i] as RiplynxOrderedPackage;
      }
      return (arr);
    }
    set
    {
      m_ordered_packages.Clear();
      foreach (RiplynxOrderedPackage p in value)
      {
        m_ordered_packages.Add(p);
      }
    }

  }

  /// <summary>
  /// Returns the ordered package based off the map
  /// </summary>
  /// <param name="map"></param>
  /// <returns></returns>
  public RiplynxOrderedPackage FindPackage(string map, ArrayList options)
  {
    foreach (RiplynxOrderedPackage ordered_package in m_ordered_packages)
    {
      if (ordered_package.Map == map)
      {
        return (ordered_package);
      }
    }
    return (null);
  }


  /// <summary>
  /// Returns the ordered package based off the map
  /// </summary>
  /// <param name="map"></param>
  /// <returns></returns>
  public RiplynxOrderedPackage FindPackage(string map)
  {
    foreach (RiplynxOrderedPackage ordered_package in m_ordered_packages)
    {
      if (ordered_package.Map == map)
      {
        return (ordered_package);
      }
    }
    return (null);
  }

  public T GetOrderedOptionPrimitive<T>(RiplynxPud pud) where T : class
  {
    List<IPrimitive> primitives = GetOrderedOptionPrimitives(typeof(T), pud);
    if (primitives == null || primitives.Count == 0) return default(T);
    return primitives[0] as T;
  }

  public List<IPrimitive> GetOrderedOptionPrimitives(Type validType, RiplynxPud pud)
  {
    return GetOrderedOptionPrimitives(new List<Type>(new Type[] { validType }), pud);
  }

  public List<IPrimitive> GetOrderedOptionPrimitives(List<Type> validTypes, RiplynxPud pud)
  {
    List<IPrimitive> optionPrimitives = new List<IPrimitive>();

    foreach (RiplynxOrderedPackage ordered_package in OrderedPackages)
    {
      foreach (string option_map in ordered_package.Options)
      {
        if (pud.Units.PrintOptions.ContainsKey(option_map))
        {
          RiplynxOption o = (RiplynxOption)pud.Units.PrintOptions[option_map];
          foreach (IPrimitive primitive in o.Primitive.Primitives)
          {
            IPrimitive rep_primitive;
            if (primitive is NamedPrimitive)
            {
              rep_primitive = (primitive as NamedPrimitive).Primitive;
            }
            else
            {
              rep_primitive = primitive;
            }
            if (validTypes.Contains(rep_primitive.GetType()))
            {
              optionPrimitives.Add(rep_primitive);
            }
          }
        }
      }
    }
    return optionPrimitives;
  }

  /// <summary>
  /// Adds an ordered package to the collection.  Map numbers are kept unique, so the
  /// quantity of an ordered package will go up rather than adding this to the collection.
  /// </summary>
  /// <param name="ordered_package"></param>
  public void Add(RiplynxOrderedPackage ordered_package)
  {
    RiplynxOrderedPackage cur_order = FindPackage(ordered_package.Map);

    if (cur_order == null)
    {
      m_ordered_packages.Add(ordered_package);
    }
    else
    {
      cur_order.Quantity += ordered_package.Quantity;
    }
  }

  public void Add(string map, int quantity)
  {
    Add(new RiplynxOrderedPackage(map, quantity));
  }

  /// <summary>
  /// Loads a ordered package collection for an entire set of records
  /// </summary>
  /// <param name="dt">DataTable containing records with orders</param>
  /// <param name="pf">Field that contains the package string</param>
  /// <returns></returns>
  public static RiplynxOrderedPackageCollection FromData(DataTable dt, string pf)
  {
    RiplynxOrderedPackageCollection col = new RiplynxOrderedPackageCollection();

    foreach (DataRow dr in dt.Rows)
    {
      if (dr[pf] is System.DBNull) continue;

      foreach (RiplynxOrderedPackage order in FromString((string) dr[pf]).OrderedPackages)
      {
        col.Add(order);
      }
    }

    return (col);
  }


  //
  // Writes a string in the form map,option,option-quantity;map,option,oiption-quantity
  // using the information in this collection.
  //
  public override string ToString()
  {
    string msg = "";

    foreach (RiplynxOrderedPackage p in OrderedPackages)
    {
      if (msg != "")
      {
        msg += ";";
      }

      msg += p.ToString();
    }

    return (msg);
  }

  /// <summary>
  /// Parses a string in the form map,option,option-quantity;map,option,oiption-quantity
  /// to an ordered package collection
  /// </summary>
  /// <param name="rep"></param>
  /// <returns></returns>
  public static RiplynxOrderedPackageCollection FromString(string rep)
  {
    RiplynxOrderedPackageCollection col = new RiplynxOrderedPackageCollection();
    return (col.LoadString(rep));
  }

  /// <summary>
  /// Parses a string in the form map,option,option-quantity;map,option,oiption-quantity
  /// to an ordered package collection
  /// </summary>
  /// <param name="rep"></param>
  /// <returns></returns>
  public RiplynxOrderedPackageCollection LoadString(string rep)
  {
    m_ordered_packages.Clear();

    string [] split = rep.Split(';');

    foreach (string s in split)
    {
      RiplynxOrderedPackage package = RiplynxOrderedPackage.FromString(s);
      if (package != null)
      {
        OrderedPackages.Add(package);
      }
    }
    return (this);
  }


  public static RiplynxOrderedPackageCollection FromXmlNode(XmlNode node)
  {
    RiplynxOrderedPackageCollection col = new RiplynxOrderedPackageCollection();

    foreach (XmlNode child_node in node.ChildNodes)
    {
      if (child_node.Name == "unit")
      {
        col.OrderedPackages.Add(RiplynxOrderedPackage.FromXmlNode(child_node));
      }
      if (child_node.Name == "Option")
      {
        col.Options.Add(child_node.Attributes["Map"].Value);
      }
    }

    return (col);
  }
}
}
