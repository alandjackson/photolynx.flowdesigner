/* ----------------------------------------------------------------------------
 * $Id: RiplynxPrograms.cs 2174 2004-10-22 00:56:34Z ajackson $
 *
 * --------------------------------------------------------------------------*/

using System;
using System.IO;
using System.Xml;
using System.Collections;
using System.ComponentModel;

namespace PhotoLynx.ImageRender.RiplynxInterop
{
public class RiplynxPrograms
{
  ArrayList m_programs = new ArrayList();

  public ArrayList Programs
  {
    get { return (m_programs); }
    set { m_programs = value; }
  }


  public static RiplynxPrograms FromXmlNode(XmlNode node)
  {
    RiplynxPrograms prms = new RiplynxPrograms();

    foreach (XmlNode child_node in node.ChildNodes)
    {
      if (child_node.Name == "Program")
      {
        prms.Programs.Add(RiplynxProgram.FromXmlNode(child_node));
      }
    }

    return (prms);
  }
}
}
