/* ----------------------------------------------------------------------------
 * $Id: RiplynxImage.cs 3675 2008-10-30 22:25:26Z photolyn $
 *
 * --------------------------------------------------------------------------*/

using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Threading;
using System.Diagnostics;
using System.Data;
using System.Collections;
using System.Windows.Forms;
using Genitor.Runtime;
using NUnit.Framework;
using Atalasoft.Imaging.Codec;
using PhotoLynx.Common.UI;
using PhotoLynx.Common.Core;
using PhotoLynx.Common.UI.Dialogs;
using PhotoLynx.Common.Core.Legacy.ImDatabase;
using PhotoLynx.ImageRender.Primitives;
using PhotoLynx.ImageRender.Core;
using PhotoLynx.ImageRender.UI;
using PhotoLynx.ImageRender.Renderers;
using System.Collections.Generic;
using PhotoLynx.Common.ImageRender.Core.Util;
using PhotoLynx.GreenScreen2.Renderer;
using PhotoLynx.Common.Core.Util;
using PhotoLynx.ImageRender.Core.Util;
using PhotoLynx.Wrappers;
using PhotoLynx.Common.Core.PlProcess;

namespace PhotoLynx.ImageRender.RiplynxInterop
{


/// <summary>
/// </summary>
public class RiplynxImage
{
  protected static string RenderCacheDir = PhotoLynxImageDirs.PlCacheDir + "\\AdjustedImages";

  string m_name;
  double m_rotate;
  int m_density;
  int m_contrast;
  double m_sharpen;
  int m_red;
  int m_green;
  int m_blue;
  double m_crop_ratio;
  double m_crop_left;
  double m_crop_top;
  double m_crop_height;
  string m_adjustments_xml = "";
  bool m_is_group_image = false;
  double m_group_rotate = 0;
  int m_green_screen_version = 1;
  RiplynxImage m_inverted_adjustments_ri = null;


  public RiplynxImage() : this("") {}

  /// <summary>
  /// </summary>
  public RiplynxImage(string n)
  {
    m_name = n;
    GreenScreenVersion = PhotoLynxSettings.Instance.LocalGreenScreenVersion;
  }

  /// <summary>Applys the riplynx image primitive to the adjustments after inverting them.
  /// This is used in cases where some of the riplynx image adjustments are passed as data
  /// rather than applied to the image.  The adjustment primitives sometimes need to take into
  /// account that the image will be rotated and cropped after the adjustments when they should
  /// be done before.</summary>
  ///// <remarks></remarks>
  ///// <value></value>
  [XmlIgnore()]
  public RiplynxImage InvertedAdjustmentsRi
  {
    get
    {
      if (m_inverted_adjustments_ri == null)
      {
        m_inverted_adjustments_ri = new RiplynxImage();
      }
      return (m_inverted_adjustments_ri);
    }
    set { m_inverted_adjustments_ri = value; }
  }

  public bool IsGroupImage
  {
    get { return (m_is_group_image); }
    set { m_is_group_image = value; }
  }

  public double GroupRotate
  {
    get { return (m_group_rotate); }
    set { m_group_rotate = value; }
  }

  /// <summary>
  /// </summary>
  public string name
  {
    get { return (m_name); }
    set { m_name = value; }
  }

  public int GreenScreenVersion
  {
    get { return m_green_screen_version; }
    set { m_green_screen_version = value; }
  }

  public string adjustments_xml
  {
    get { return (m_adjustments_xml); }
    set { m_adjustments_xml = value; }
  }


  /// <summary>
  /// </summary>
  public double rotate
  {
    get { return (m_rotate); }
    set
    {
      m_rotate = value;

      // Make sure value is between 0 and 359.
      while (m_rotate < 0)
      {
        m_rotate += 360;
      }
      while (m_rotate >= 360)
      {
        m_rotate -= 360;
      }
    }
  }

  /// <summary>
  /// </summary>
  public int density
  {
    get { return (m_density); }
    set { m_density = value; }
  }

  /// <summary>
  /// </summary>
  public int contrast
  {
    get { return (m_contrast); }
    set { m_contrast = value; }
  }

  /// <summary>
  /// </summary>
  public double sharpen
  {
    get { return (m_sharpen); }
    set { m_sharpen = value; }
  }

  /// <summary>
  /// </summary>
  public int red
  {
    get { return (m_red); }
    set { m_red = value; }
  }

  /// <summary>
  /// </summary>
  public int green
  {
    get { return (m_green) ; }
    set { m_green = value; }
  }

  /// <summary>
  /// </summary>
  public int blue
  {
    get { return (m_blue); }
    set { m_blue = value; }
  }

  /// <summary>
  /// </summary>
  public double crop_ratio
  {
    get { return (m_crop_ratio); }
    set { m_crop_ratio = value; }
  }

  /// <summary>
  /// </summary>
  public double crop_left
  {
    get { return (m_crop_left); }
    set { m_crop_left = value; }
  }

  /// <summary>
  /// </summary>
  public double crop_top
  {
    get { return (m_crop_top); }
    set { m_crop_top = value; }
  }

  /// <summary>
  /// </summary>
  public double crop_height
  {
    get { return (m_crop_height); }
    set { m_crop_height = value; }
  }

  /// <summary>Conversion constant to change from ImageXpress to Atalasoft contrast value</summary>
  ///// <remarks></remarks>
  ///// <value></value>
  public double XpressToAtalaContrastConstant
  {
    get { return (0.75); }
  }

  /// <summary>Conversion constant to change from ImageXpress to Atalasoft brightness value</summary>
  ///// <remarks></remarks>
  ///// <value></value>
  public double XpressToAtalaBrightnessConstant
  {
    get { return (-0.42); }
  }
  
  public bool HasAdjustments
  {
    get
    {
      return ! (m_crop_height == 0 && 
          m_crop_ratio == 0 &&
          m_crop_top == 0 &&
          m_crop_left == 0 &&
          m_red == 0 &&
          m_blue == 0 &&
          m_green == 0 &&
          m_rotate == 0 && 
          m_group_rotate == 0 && 
          m_density == 0 &&
          m_contrast == 0 &&
          m_sharpen == 0  && 
          string.IsNullOrEmpty(m_adjustments_xml)
          );
          
    }
  }

  /// <summary>Gets the set of image adjustments to be applied to an image.</summary>
  ///// <remarks></remarks>
  ///// <returns></returns>
  public PrimitiveCollection GetImageAdjustmentCollection()
  {
    Crop crop = null;
    if (m_crop_left != 0 || m_crop_top != 0 || m_crop_height != 0 || m_crop_ratio != 0)
    {
      crop = new Crop();
      crop.X = m_crop_left;
      crop.Y = m_crop_top;
      crop.Height = m_crop_height;
      crop.RatioForceWidth = m_crop_ratio;
    }


    ImageEffects rgb = null;
    if (m_red != 0 || m_blue != 0 || m_green != 0)
    {
      rgb = new ImageEffects(new AdjustChannelCommandWrapper(true, m_red, m_green, m_blue));
    }

    ImageEffects rotate = null;
    if (m_rotate != 0)
    {
      rotate = new ImageEffects(new RotateCommandWrapper(m_rotate));
    }

    ImageEffects group_rotate = null;
    if (m_group_rotate != 0)
    {
      group_rotate = new ImageEffects(new RotateCommandWrapper(m_group_rotate));
    }

    ImageEffects bright = null;
    if (m_density != 0)
    {
      bright = new ImageEffects(new BrightnessContrastCommandWrapper());
      ((BrightnessContrastCommandWrapper) bright.ImageEffect).Brightness = m_density * XpressToAtalaBrightnessConstant;
    }

    ImageEffects contrast_effect = null;
    if (m_contrast != 0)
    {
      contrast_effect = new ImageEffects(new BrightnessContrastCommandWrapper());
      ((BrightnessContrastCommandWrapper) contrast_effect.ImageEffect).Contrast = m_contrast * XpressToAtalaContrastConstant;
    }

    ImageEffects sharpen = null;
    if (m_sharpen != 0)
    {
      sharpen = new ImageEffects(new SharpenCommandWrapper(m_sharpen * 10));
    }

    // Get adjustments collection.
    PrimitiveCollection adjustments_col = null;
    PrimitiveCollection pre_adjustments_col = new PrimitiveCollection();
    if (m_adjustments_xml != null && m_adjustments_xml != "")
    {
      try
      {
        adjustments_col = (PrimitiveCollection) ObjectSerializer.DeserializeString(m_adjustments_xml, typeof(PrimitiveCollection));
      }
      catch
      {
        adjustments_col = null;
      }
    }

    if (GreenScreenVersion == 2)
    {
      adjustments_col = SetupGreenScreenV2(adjustments_col);
    }
    
    // apply the inverted adjustments
    if (adjustments_col != null && m_inverted_adjustments_ri != null)
    {
      PrimitiveCollection new_adjustments = new PrimitiveCollection();
      //new_adjustments.Add(m_inverted_adjustments_ri.GetImageAdjustmentCollection().ConvertData(new InvertTransformationConversiondata()));
      new_adjustments.Add(m_inverted_adjustments_ri.GetImageAdjustmentCollection());
      new_adjustments.Add(adjustments_col);
      new_adjustments.Add(m_inverted_adjustments_ri.GetImageAdjustmentCollection().ConvertData(new InvertTransformationConversiondata()));

      adjustments_col = new_adjustments;
    }

    // Move some adjustments to the front of the list
    if (adjustments_col != null)
    {
      for (int i = 0; i < adjustments_col.Primitives.Count; i++)
      {
        IPrimitive primitive = (IPrimitive) adjustments_col.Primitives[i];
        // Move primitive to pre list if beforeimagematchadjustments is true
        if (primitive is IImageMatchAdjustmentsPosition && ((IImageMatchAdjustmentsPosition) primitive).BeforeImagematchAdjustments)
        {
          pre_adjustments_col.Primitives.Add(primitive);
          adjustments_col.Primitives[i] = null;
        }
      }
    }
    if (pre_adjustments_col.Primitives.Count == 0)
    {
      pre_adjustments_col = null;
    }

    // don't nest adjustments collection if that is the only non null portion.
    if (pre_adjustments_col == null && rotate == null && crop == null && rgb == null && bright == null && contrast_effect == null && sharpen == null && adjustments_col != null && group_rotate == null)
    {
      return (adjustments_col);
    }

    // return collection.
    else
    {
      return new PrimitiveCollection(new IPrimitive[] {pre_adjustments_col,
                                                         rotate,
                                                         crop,
                                                         rgb,
                                                         bright,
                                                         contrast_effect,
                                                         sharpen,
                                                         adjustments_col,
                                                         group_rotate
                                                         });
    }

  }

  private PrimitiveCollection VerifyAdjustments(PrimitiveCollection adjustments_col)
  {
    bool backgroundPrimitiveFound = false;
    if (adjustments_col == null)
    {
      adjustments_col = new PrimitiveCollection();
    }
    else
    {
      List<IPrimitive> toDelete = new List<IPrimitive>();
      foreach (IPrimitive prim in adjustments_col.Primitives)
      {
        if (prim is GreenScreenPrimitive)
        {
          toDelete.Add(prim);
        }
        else if (prim is BackgroundImage)
        {
          backgroundPrimitiveFound = true;
        }
      }

      foreach (IPrimitive prim in toDelete)
      {
        adjustments_col.Primitives.Remove(prim);
      }
      toDelete.Clear();
    }

    if (!backgroundPrimitiveFound)
    {
      adjustments_col.Primitives.Add(new BackgroundImage());
    }
    return adjustments_col;
  }

  private PrimitiveCollection SetupGreenScreenV2(PrimitiveCollection adjustments_col)
  {
    bool backgroundFound = false;
    bool gsPrimFound = false;
    if (adjustments_col == null)
    {
      adjustments_col = new PrimitiveCollection();
    }
    else
    {
      for (int i = 0; i < adjustments_col.Primitives.Count; i++ )
      {
        if (adjustments_col.Primitives[i] is PDIGreenScreenPrimitive)
        {
          ((PDIGreenScreenPrimitive)adjustments_col.Primitives[i]).GreenScreenVersion = 2;
          if (((PDIGreenScreenPrimitive)adjustments_col.Primitives[i]).BackgroundPath.Equals(""))
          {
            ((PDIGreenScreenPrimitive)adjustments_col.Primitives[i]).BackgroundPath = ((PDIGreenScreenPrimitive)adjustments_col.Primitives[i]).ConfigurationXmlBackground;
          }
          gsPrimFound = true;
        }
        else if (adjustments_col.Primitives[i] is BackgroundImage)
        {
          backgroundFound = true;
        }
        else if (adjustments_col.Primitives[i] is GreenScreenPrimitive)
        {//remove gs1 just incase
          adjustments_col.Primitives[i] = null;
        }
      }
    }
    if (!backgroundFound && gsPrimFound)
    {//add backgroundimage only if green screen settings exist
      if (gsPrimFound)
      {
        adjustments_col.Insert(1, new BackgroundImage(""));
      }
      else
      {
        adjustments_col.Insert(0, new BackgroundImage(""));
      }
    }
    if (!gsPrimFound)
    {//do not default render
      //adjustments_col.Insert(0, new PDIGreenScreenPrimitive(""));
    }

    return adjustments_col;
  }

  public PrimitiveCollection Primitive
  {
    get { return (ToPrimitive()); }
  }

  /// <summary>
  /// </summary>
  public PrimitiveCollection ToPrimitive()
  {
    return (ToPrimitive(false, ""));
  }

  /// <summary>
  /// </summary>
  public PrimitiveCollection ToPrimitive(bool use_embedded_profile, string dest_profilename)
  {
    ImageFile imagefile = new ImageFile(m_name);
    imagefile.ConvertEmbeddedProfile = use_embedded_profile;
    imagefile.DestProfilename = dest_profilename;

    PrimitiveCollection col = GetImageAdjustmentCollection();
    col.Insert(0, imagefile);

    GreenScreen2ConversionData convertData = new GreenScreen2ConversionData();
    convertData.GreenScreenVersion = GreenScreenVersion;
    col = (PrimitiveCollection)convertData.ConvertData(col);
    col.IgnoresSourceImage = true;
    return col;
  }

  /// <summary>
  /// </summary>
  public string ToXmlString()
  {
    return (ObjectSerializer.SerializeString(this));

  }

  public bool CollectionMatchesXmlFile(IPrimitive col, string xml_filename)
  {
    if (col == null)
    {
      return (false);
    }
    return (CollectionMatchesXmlFile(ObjectSerializer.SafeSerializeString(col), xml_filename));
  }


  public bool CollectionMatchesXmlFile(string col, string xml_filename)
  {
    if (col == "" || ! File.Exists(xml_filename))
    {
      return (false);
    }

    string str2 = ObjectSerializer.SafeSerializeString(ObjectSerializer.SafeDeserializeFile(xml_filename, typeof(PrimitiveCollection)));
    return (col == str2);
  }

  public bool XmlStringMatchesXmlFile(string xml, string xml_filename)
  {
    string str2 = ObjectSerializer.SerializeString(ObjectSerializer.DeserializeFile(xml_filename, typeof(PrimitiveCollection)));
    return (xml == str2);
  }


  public string GetRenderedCacheFilename(string filename)
  {
    FileInfo fi = new FileInfo(filename);
    return(Path.Combine(Path.Combine(fi.DirectoryName, RiplynxImage.RenderCacheDir), fi.Name));
  }

  public string GetRenderedCacheDataFilename(string filename)
  {
    return (filename + ".adj");
  }

  public void LoadAdjustmentsXmlFromCache()
  {
    string data_filename = GetRenderedCacheDataFilename(GetRenderedCacheFilename(name));
    Trace.WriteLineIf(ImageRender.Switch.TraceInfo, "Loading adjustments xml from file: " + data_filename + ", image name: " + name);
    if (File.Exists(data_filename))
    {
      FileStream fs = new FileStream(data_filename, FileMode.Open);
      StreamReader sr = new StreamReader(fs, System.Text.Encoding.UTF8);
      adjustments_xml = sr.ReadToEnd();
      sr.Close();
      fs.Close();
      //MessageBox.Show(adjustments_xml, "Loading " + name);
    }
    else
    {
      adjustments_xml = "";
    }
  }

  public void SaveAdjustmentsXmlToCache()
  {
    //MessageBox.Show(adjustments_xml, "Saving " + name);
    string img_filename = GetRenderedCacheFilename(name);
    string data_filename = GetRenderedCacheDataFilename(img_filename);
    ValidateCache(adjustments_xml, img_filename, data_filename);
    try
    {
      FileStream fs = new FileStream(data_filename, FileMode.Create);
      StreamWriter sw = new StreamWriter(fs);
      sw.Write(adjustments_xml);
      sw.Close();
      fs.Close();
    }
    catch (Exception e)
    {
      MessageBox.Show("Error writing data file: " + data_filename + ": " + UIQuickies.GetLongErrMsg(e));
    }
  }

  /// <summary>
  /// Renders the riplynx image using the cached adjustments data for the adjustments portion.
  /// </summary>
  public string RenderImageFromCacheData()
  {
    LoadAdjustmentsXmlFromCache();
    return (RenderImage());
  }

  public string RenderImageFromCacheDataWithProgress()
  {
    LoadAdjustmentsXmlFromCache();
    return (RenderImageWithProgress());
  }

  public string RenderImageWithProgress()
  {
    ProgressDialog.ShowProgressDialog(new Thread(new ThreadStart(RenderImageThreadStart)), "Rendering Green Screen", false, false, null);
    return (m_last_rendered_image);
  }

  protected string m_last_rendered_image;
  public void RenderImageThreadStart()
  {
    m_last_rendered_image = RenderImage();
  }

  /// <summary>Deletes all the files from the cached files in the folder this images cache would be stored.</summary>
  ///// <remarks></remarks>
  public void DeleteAllCachedFiles()
  {
    if (name == null || name == "")
    {
      return;
    }

    try
    {

      DirectoryInfo di = new FileInfo(GetRenderedCacheFilename(name)).Directory;
      if (di == null)
      {
        return;
      }

      foreach (FileInfo fi in di.GetFiles())
      {
        fi.Delete();
      }
    }
    catch (Exception)
    {
    }
  }

  public void ValidateCache(PrimitiveCollection adjustments_col, string image_fileanme, string data_filename)
  {
    ValidateCache(ObjectSerializer.SafeSerializeString(adjustments_col), image_fileanme, data_filename);
  }

  public void ValidateCache(string adjustments_col_xml, string image_fileanme, string data_filename)
  {
    if (File.Exists(image_fileanme))
    {
      if (! CollectionMatchesXmlFile(adjustments_col_xml, data_filename))
      {
        File.Delete(image_fileanme);
        File.Delete(data_filename);
      }
    }
  }

  protected static PlConcurrentProcessQueue _renderProcessQueue = null;
  public static PlConcurrentProcessQueue RenderProcessQueue
  {
    get
    {
      if (_renderProcessQueue == null)
      {
        _renderProcessQueue = new PlConcurrentProcessQueue(PhotoLynxSettings.Instance.RiplynxImageConcurrentProcesses.IntValue);
      }
      return _renderProcessQueue;
    }
  }

  public void EndRenderImage()
  {
    if (PhotoLynxSettings.Instance.RiplynxImageExe.BoolValue)
    {
      RenderProcessQueue.End();
    }
  }

  public bool IsRendering
  {
    get
    {
      return RenderProcessQueue.IsRunningActions;
    }
  }

  public void CancelRendering()
  {
    RenderProcessQueue.Cancel();
  }

  public int RenderingQueueSize
  {
    get { return RenderProcessQueue.UndoneActionsCount; }
  }

  protected PrimitiveCollection GetPrimitiveToRender()
  {
    return Primitive;
    //PrimitiveCollection col = Primitive;

    //GreenScreen2ConversionData convertData = new GreenScreen2ConversionData();
    //convertData.GreenScreenVersion = GreenScreenVersion;
    //col = (PrimitiveCollection)convertData.ConvertData(col);

    //return col;
  }

  public Atalasoft.Imaging.AtalaImage RenderImageConcurrentlyToAtalaImage()
  {
    string tempFile = Path.GetTempFileName() + ".png";
    try
    {
      RenderImageToFileConcurrently(tempFile);
      EndRenderImage();
      return new Atalasoft.Imaging.AtalaImage(tempFile);
    }
    finally
    {
      Utilities.SafeDeleteFile(tempFile);
    }
  }

  public string RenderImageToFileConcurrently(string new_filename)
  {
    PrimitiveCollection col = GetPrimitiveToRender();

    // Render riplynx image
    try
    {
      Utilities.CreateDirectory(new FileInfo(new_filename).DirectoryName, true);

      if (PhotoLynxSettings.Instance.RiplynxImageExe.BoolValue)
      {
        RenderProcessQueue.EnqueueAction((Action)delegate
        {
          try
          {
            new ImageRendererData(col, new_filename).RenderImageSpawnProcess();
          }
          catch (Exception e)
          {
            MessageBox.Show("Error Rendering Riplynx Image: " + e.ToString());
          }
        });
      }
      else
      {
        AbstractPrimitiveRenderer.Render(col.Renderer, null, new_filename, new ImageSaveOptions(TiffCompression.NoCompression));
      }
    }
    catch (Exception e)
    {
      MessageBox.Show("Error RiplynxImage.RenderImage: " + name + ". " + e.Message);
      throw (e);

    }
    if (GreenScreenVersion == 2)
    {
      PrimitiveCache.Instance.FlushCache();
    }
    return (new_filename);
  }

  public void Sleep(int ms)
  {
    Thread.Sleep(ms);
  }

  /// <summary>
  /// Renders this riplynx image adjustments on a file.
  /// </summary>
  public string RenderImage()
  {
    string new_filename = GetRenderedCacheFilename(name);
    string new_data_filename = GetRenderedCacheDataFilename(new_filename);

    PrimitiveCollection col = GetPrimitiveToRender();
    PrimitiveCollection adjustments_col = GetImageAdjustmentCollection();

    // Check cache validity
    ValidateCache(adjustments_col, new_filename, new_data_filename);

    // Render riplynx image
    try
    {
      if (!File.Exists(new_filename))
      {
        Utilities.CreateDirectory(new FileInfo(new_filename).DirectoryName, true);

        if (PhotoLynxSettings.Instance.RiplynxImageExe.BoolValue)
        {
          new ImageRendererData(col, new_filename).RenderImageSpawnProcess();
        }
        else
        {
          AbstractPrimitiveRenderer.Render(col.Renderer, null, new_filename, new ImageSaveOptions(TiffCompression.NoCompression));
        }

        // Alan: I don't think render ever returns false since now the save routine throws an error if it can't figure out the 
        //       extension so I am dropping this check
        // drop back to rendering a jpg if the extension doesn't make sense
        //if (!AbstractPrimitiveRenderer.Render(col.Renderer, null, new_filename, new ImageSaveOptions(TiffCompression.NoCompression)))
        //{
        //  new_filename = new_filename + ".jpg";
        //  AbstractPrimitiveRenderer.Render(col.Renderer, null, new_filename);
        //}
        ObjectSerializer.SerializeFile(new_data_filename, adjustments_col);
      }
    }
    catch (Exception e)
    {
        MessageBox.Show("Error RiplynxImage.RenderImage: " + name + ". " + e.Message);
        throw (e);

    }
    adjustments_col.Dispose();
    if (GreenScreenVersion == 2)
    {
      PrimitiveCache.Instance.FlushCache();
    }
    return (new_filename);
  }

  /// <summary>
  /// Deletes the cache data for a filename.
  /// </summary>
  public void DeleteAdjustmentsCache(string src)
  {
    string src_cache_img = GetRenderedCacheFilename(src);
    string src_cache_data = GetRenderedCacheDataFilename(src_cache_img);

    SafeDeleteFile(src_cache_img);
    SafeDeleteFile(src_cache_data);

  }

  /// <summary>
  /// Deletes a file if it exists.  Caches errors so the delete never
  /// throws an exception.
  /// </summary>
  protected void SafeDeleteFile(string filename)
  {
    if (! File.Exists(filename))
    {
      return;
    }

    try
    {
      File.Delete(filename);
    }
    catch
    {
    }
  }

  /// <summary>
  /// Copies the cache data.  Use this when the source filename
  /// is being copied to a new location in order to not have
  /// to rerender the adjustments.
  /// </summary>
  public void CopyAdjustmentsCache(string src, string dest)
  {
    string src_cache_img = GetRenderedCacheFilename(src);
    string src_cache_data = GetRenderedCacheDataFilename(src_cache_img);
    string dest_cache_img = GetRenderedCacheFilename(dest);
    string dest_cache_data = GetRenderedCacheDataFilename(dest_cache_img);

    if (File.Exists(src_cache_img))
    {
      Utilities.CreateDirectory(new FileInfo(dest_cache_img).DirectoryName, true);
      File.Copy(src_cache_img, dest_cache_img, true);
    }

    if (File.Exists(src_cache_data))
    {
      Utilities.CreateDirectory(new FileInfo(dest_cache_data).DirectoryName, true);
      File.Copy(src_cache_data, dest_cache_data, true);
    }
  }


  internal static int GetGreenScreenVersion(XmlNode node)
  {
    string xml;
    if (node.Name == "RiplynxImage")
    {
      xml = node.OuterXml;
    }
    else
    {
      xml = "<RiplynxImage>" + node.InnerXml + "</RiplynxImage>";
    }
    return (ObjectSerializer.DeserializeString(xml, typeof(RiplynxImage)) as RiplynxImage).GreenScreenVersion;
  }

  /// <summary>
  /// </summary>
  public static RiplynxImage FromXmlNode(XmlNode node)
  {
    string xml;
    if (node.Name == "RiplynxImage")
    {
      xml = node.OuterXml;
    }
    else
    {
      xml = "<RiplynxImage>" + node.InnerXml  + "</RiplynxImage>";
    }
    Trace.WriteLineIf(ImageRender.Switch.TraceInfo, "Loading riplynx image xml: " + xml);
    return (ObjectSerializer.DeserializeString(xml, typeof(RiplynxImage)) as RiplynxImage);
  }

  /// <summary>
  /// </summary>
  public static RiplynxImage FromDataRow(DataRow dr)
  {
    return (FromDataRow(dr, CompleteStudentsPeer.Instance));
  }

  /// <summary>
  /// </summary>
  public static int GetSafeInt(object value)
  {
    try
    {
      return ((int) value);
    }
    catch (Exception e)
    {
      Trace.WriteLineIf(ImageRender.Switch.TraceInfo, "Exception getting int: " + e.Message);
      return (0);
    }
  }


  /// <summary>
  /// </summary>
  public static double GetSafeDouble(object value)
  {
    try
    {
      return ((double) value);
    }
    catch (Exception e)
    {
      Trace.WriteLineIf(ImageRender.Switch.TraceInfo, "Exception getting double: " + e.Message);
      return (0);
    }
  }

  public static string GetSafeString(DataRow dr, string fieldname)
  {
    try
    {
      return (GetSafeString(dr[fieldname]));
    }
    catch
    {
      return ("");
    }
  }


  public static string GetSafeString(object value)
  {
    try
    {
      return (value.ToString());
    }
    catch
    {
      return ("");
    }
  }

  public static void PrintDataRow(DataRow dr)
  {
    if (dr != null && dr.Table != null)
    {
      Trace.WriteLineIf(ImageRender.Switch.TraceInfo, "DataRow from table: " + dr.Table.TableName);
      foreach (DataColumn dc in dr.Table.Columns)
      {
        Trace.Write(dc.ColumnName + "(" + dc.DataType + "): ");
        if (dr[dc] == null || dr[dc] == DBNull.Value)
        {
          Trace.WriteLineIf(ImageRender.Switch.TraceInfo, "DBNull");
        }
        else
        {
          Trace.WriteLineIf(ImageRender.Switch.TraceInfo, dr[dc].ToString());
        }
      }
    }
  }

  /// <summary>
  /// </summary>
  public static RiplynxImage FromDataRow(DataRow dr, BasePeer peer)
  {
    string from = "";
    try
    {
      RiplynxImage ri = new RiplynxImage();

      if (peer is CompleteStudentsPeer)
      {
        ri.name = GetSafeString(dr[CompleteStudentsPeer.IMAGE_FILE]);
        ri.rotate = GetSafeDouble(dr[CompleteStudentsPeer.ROTATION]);

        ri.density = (int) GetSafeDouble(dr[CompleteStudentsPeer.DENSITY]);
        ri.contrast = (int) GetSafeDouble(dr[CompleteStudentsPeer.CONTRAST]);
        ri.sharpen = GetSafeDouble(dr[CompleteStudentsPeer.SHARPEN]);
        ri.red = (int) GetSafeDouble(dr[CompleteStudentsPeer.RED]);
        ri.green = (int) GetSafeDouble(dr[CompleteStudentsPeer.GREEN]);
        ri.blue = (int) GetSafeDouble(dr[CompleteStudentsPeer.BLUE]);
        ri.crop_ratio = GetSafeDouble(dr[CompleteStudentsPeer.CROP_RATIO]);
        ri.crop_left = GetSafeDouble(dr[CompleteStudentsPeer.CROP_LEFT]);
        ri.crop_top = GetSafeDouble(dr[CompleteStudentsPeer.CROP_TOP]);
        ri.crop_height = GetSafeDouble(dr[CompleteStudentsPeer.CROP_HEIGHT]);
        ri.adjustments_xml = GetSafeString(dr, CompleteStudentsPeer.ADJUSTMENTS_XML);

        DataRow dr_config = CompleteStudentsPeer.Instance.SafeGetConfigurationRow(dr);
        if (dr_config != null)
        {
          ri.rotate += GetSafeInt(dr_config[ConfigurationPeer.ROTATE]);
          PrintDataRow(dr_config);
          //Trace.WriteLineIf(ImageRender.Switch.TraceInfo, "Adding: rotate" + GetSafeInt(dr_config[ConfigurationPeer.ROTATE]));
        }
        else
        {
          Trace.WriteLineIf(ImageRender.Switch.TraceInfo, "dr_config is null");
        }
      }

      else if (peer is ImageAuxPeer)
      {
        if (dr.Table.Columns.Contains(CompleteStudentsPeer.ROTATION) &&
            dr[CompleteStudentsPeer.ROTATION] != DBNull.Value)
        {
          ri.rotate = (double) GetSafeDouble(dr[CompleteStudentsPeer.ROTATION]);
        }

        ri.density = (int) GetSafeDouble(dr[CompleteStudentsPeer.DENSITY]);
        ri.contrast = (int) GetSafeDouble(dr[CompleteStudentsPeer.CONTRAST]);
        ri.sharpen = GetSafeDouble(dr[CompleteStudentsPeer.SHARPEN]);
        ri.red = (int) GetSafeDouble(dr[CompleteStudentsPeer.RED]);
        ri.green = (int) GetSafeDouble(dr[CompleteStudentsPeer.GREEN]);
        ri.blue = (int) GetSafeDouble(dr[CompleteStudentsPeer.BLUE]);
        ri.crop_ratio = GetSafeDouble(dr[CompleteStudentsPeer.CROP_RATIO]);
        ri.crop_left = GetSafeDouble(dr[CompleteStudentsPeer.CROP_LEFT]);
        ri.crop_top = GetSafeDouble(dr[CompleteStudentsPeer.CROP_TOP]);
        ri.crop_height = GetSafeDouble(dr[CompleteStudentsPeer.CROP_HEIGHT]);
        ri.adjustments_xml = GetSafeString(dr[CompleteStudentsPeer.ADJUSTMENTS_XML]);
      }

      //PrintDataRow(dr);
      //MessageBox.Show("RiplynxImage From Data Row, peer: " + peer.GetType().Name + ", rotate: " + ri.rotate);
      return (ri);
    }
    catch (Exception e)
    {
      string dr_str = "";
      if (dr != null && dr.Table != null)
      {
        foreach (DataColumn dc in dr.Table.Columns)
        {
          dr_str += dc.ColumnName + "(" + dc.DataType + "): " + dr[dc].ToString() + "\n";
          dr_str += "Is null: " + (dr[dc] == DBNull.Value) + "\n";
        }
      }
      throw (new Exception("Error creating image from dr: " + e.Message + "\n" + dr_str + "\n" + e.StackTrace + "\n" + from, e));
    }
  }

}
}
