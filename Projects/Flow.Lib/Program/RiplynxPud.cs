/* ----------------------------------------------------------------------------
 * $Id: RiplynxPud.cs 2878 2007-11-09 18:22:26Z ajackson $
 *
 * --------------------------------------------------------------------------*/

using System;
using System.IO;
using System.Xml;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using NUnit.Framework;
using Atalasoft.Imaging;
using PhotoLynx.Common.Core;
using PhotoLynx.ImageRender.Core;
using PhotoLynx.ImageRender.Primitives;
using PhotoLynx.ImageRender.Renderers;

namespace PhotoLynx.ImageRender.RiplynxInterop
{
  public class RiplynxPud
  {
    RiplynxUnits m_units;

    public RiplynxPud()
    {
    }

    public RiplynxUnits Units
    {
      get { return (m_units); }
      set { m_units = value; }
    }

    public static RiplynxPud FromXmlFile(string filename)
    {
      return (new RiplynxPud().FromXmlFileI(filename));
    }

    public RiplynxPud FromXmlFileI(string filename)
    {
      if (!File.Exists(filename))
      {
        throw (new Exception("Can't load pud, file not found: " + filename));
      }
      StreamReader sr = new StreamReader(filename, System.Text.Encoding.UTF8);
      XmlDocument xml_doc = new XmlDocument();
      xml_doc.Load(sr);
      FromXmlNodeI(xml_doc);
      sr.Close();
      return (this);

    }

    public static RiplynxPud FromXmlNode(XmlNode node)
    {
      return (new RiplynxPud().FromXmlNodeI(node));
    }

    public RiplynxPud FromXmlNodeI(XmlNode node)
    {
      foreach (XmlNode child_node in node.ChildNodes)
      {
        if (child_node.Name == "Units")
        {
          Units = RiplynxUnits.FromXmlNode(child_node);
        }
      }
      return (this);
    }


  }

  [TestFixture]
  public class RiplynxPudTest
  {
    [Ignore("")]
    [Test]
    public void Main()
    {
      RiplynxPud pd = RiplynxPud.FromXmlFile(System.AppDomain.CurrentDomain.BaseDirectory + "\\..\\test_data\\RipLynxPUD.xml");
      PhotoLynx.Common.UI.UIQuickies.ShowInPropertyGrid(pd);
    }

    [Test]
    public void LoadError()
    {
      RiplynxPud pd = RiplynxPud.FromXmlFile(@"C:\Program Files\PhotoLynx\ImageMatch 2000\RipLynx Units.PUD");
    }

  }

  [TestFixture]
  public class RiplynxWalletsTest
  {
    [Test]
    public void Main()
    {
      DateTime dt_start = DateTime.Now;

      AbstractPrimitiveRenderer.PrimitiveRendering += new PrimitiveRenderingEventHandler(AbstractPrimitiveRenderer_PrimitiveRendering);

      Atalasoft.Imaging.Memory.PixelMemoryTracker tracker = Atalasoft.Imaging.Memory.PixelMemoryTracker.Memory;
      tracker.MemoryInUseChanged += new Atalasoft.Imaging.Memory.MemoryUseEventHandler(tracker_MemoryInUseChanged);
      

      RiplynxPud pd = RiplynxPud.FromXmlFile(@"C:\Program Files\PhotoLynx\ImageMatch 2000\RipLynx Units.PUD");
      RiplynxUnit wallet_unit = pd.Units.FindUnit("A4");
      Assert.AreNotEqual(null, wallet_unit);

      ImageFile student_if = new ImageFile(@"C:\Documents and Settings\ajackson\My Documents\home\dev\photolynx\gtd_reference\closed_issues\large_images_crop_screen_problem\jcflash\JC_Bradshaw_0495_03439.jpg");
      RotateImageEffect rotate_if = new RotateImageEffect(-90);
      PrimitiveCollection col = new PrimitiveCollection(student_if, rotate_if);

      IPrimitive unit_primitive = wallet_unit.ToPrimitive(col);
      unit_primitive = new PrimitiveConversionData(300).ConvertData(unit_primitive);

      //IPrimitive unit2_primitive = pd.Units.FindUnit("A1").ToPrimitive(col);
      //unit2_primitive = new PrimitiveConversionData(300).ConvertData(unit2_primitive);

      RepeatingPrimitiveSetImageFileScale rp_conversion = new RepeatingPrimitiveSetImageFileScale();
      unit_primitive = rp_conversion.ConvertData(unit_primitive);

      //QueryPrimitiveCollectionConversionData query_conversion = new QueryPrimitiveCollectionConversionData();
      //query_conversion.SetImagefileScales = true;
      //unit_primitive = query_conversion.ConvertData(unit_primitive);
      //query_conversion.ConvertData(unit2_primitive);
      //Trace.WriteLine(query_conversion.ToString());

      AbstractPrimitiveRenderer.SaveFile(unit_primitive, @"C:\Documents and Settings\ajackson\My Documents\home\dev\photolynx\gtd_reference\closed_issues\large_images_crop_screen_problem\jcflash\wallet1.jpg");
      //AbstractPrimitiveRenderer.SaveFile(unit2_primitive, @"C:\Documents and Settings\ajackson\My Documents\home\dev\photolynx\gtd_reference\closed_issues\large_images_crop_screen_problem\jcflash\wallet2.jpg");

      DateTime dt_end = DateTime.Now;
      TimeSpan ts = dt_end.Subtract(dt_start);
      Trace.WriteLine("Run Tim: " + ts.ToString());
    }

    void AbstractPrimitiveRenderer_PrimitiveRendering(object sender, EventArgs e)
    {
      if (!(sender is ImageFile))
      {
        return;
      }

      Assert.AreNotSame(0, (sender as ImageFile).ScaleSize);

      //MessageBox.Show("Image File Renderering, image scale: " + (sender as ImageFile).ScaleSize);
    }

    long m_max_mem = 0;
    void tracker_MemoryInUseChanged(object sender, Atalasoft.Imaging.Memory.MemoryUseEventArgs e)
    {
      m_max_mem = Math.Max(e.MemoryInUse, m_max_mem);
      Trace.WriteLine("MEMORY IN USE, CHANGE: " + LongBytesToMbString(e.Change) + ", IN USE: " + LongBytesToMbString(e.MemoryInUse) + ", MAX: " + LongBytesToMbString(m_max_mem));
    }

    string LongBytesToMbString(long bytes)
    {
      return (((double)bytes / 1024 / 1024).ToString("0.00") + " MB");
    }
  }


}
