/* ----------------------------------------------------------------------------
 * $Id: RiplynxJob.cs 3726 2009-01-19 16:02:50Z photolyn $
 *
 * --------------------------------------------------------------------------*/

using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Data;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;
using NUnit.Framework;
using PhotoLynx.ImageRender.Primitives;
using PhotoLynx.ImageRender.Core;
using PhotoLynx.Common.Core;
using PhotoLynx.Common.Core.Exports;
using PhotoLynx.Wrappers;
using PhotoLynx.Common.Core.Legacy.ImDatabase;
using PhotoLynx.MvcFramework.Data;

namespace PhotoLynx.ImageRender.RiplynxInterop
{
  public interface IPackageInsertSettings
  {
    List<string> Services { get; set; }
  }

  public class PackageInsertSettings : PlRegData<PackageInsertSettings>, IPackageInsertSettings
  {
    public List<string> Services { get; set; }

    public PackageInsertSettings()
      : base("RipIt!", "PackageInserts")
    {
      Services = new List<string>();
    }

    public override PackageInsertSettings Load()
    {
      string objectStringValue = LoadStringValue();
      return DeserializeObject(objectStringValue);
    }

    protected override PackageInsertSettings DeserializeObject(string objectStringValue)
    {
      PackageInsertSettings newObject = new PackageInsertSettings();

      if (String.IsNullOrEmpty(objectStringValue)) return newObject;

      foreach (string s in objectStringValue.Split('|'))
      {
        newObject.Services.Add(s);
      }
      return newObject;
    }

    public override void Save()
    {
      throw new NotImplementedException();
    }

  }


  public class UnitPrimitive
  {
    IPrimitive m_primitive;
    int m_copies;

    public UnitPrimitive() : this(null, 0) { }

    public UnitPrimitive(IPrimitive primitive, int copies)
    {
      m_primitive = primitive;
      m_copies = copies;
    }

    public IPrimitive Primitive
    {
      get { return (m_primitive); }
      set { m_primitive = value; }
    }

    public int Copies
    {
      get { return (m_copies); }
      set { m_copies = value; }
    }
  }

  public class RiplynxJobPrmInfo
  {
    public string Business { get; set; }
    public string Description { get; set; }

    public static RiplynxJobPrmInfo FromXmlNode(XmlNode node)
    {
      RiplynxJobPrmInfo gr = new RiplynxJobPrmInfo();

      foreach (XmlNode child_node in node.ChildNodes)
      {
        if (child_node.Name == "Description")
        {
          gr.Description = child_node.InnerText;
        }
        else if (child_node.Name == "Business")
        {
          gr.Business = child_node.InnerText;
        }
      }

      return (gr);
    }

  }

  /// <summary>
  /// Represents a job that riplynx uses (passed to it as an xml file).
  /// Contains all of the information for a print job.
  /// References the PUD and PRM file.
  /// </summary>
  public class RiplynxJob
  {
    ArrayList m_records = new ArrayList();
    ArrayList m_group_images = new ArrayList();
    Hashtable m_packager_idbars = new Hashtable();
    ArrayList m_packager_text_lines = new ArrayList();
    string m_type;
    string m_source;
    string m_source_ip;
    string m_print_option;
    string m_name;
    string m_job_name;
    string m_packager_save_dir;
    string m_imagefile_format = "";
    bool m_for_packager = false;
    bool _packageByUnit = false;
    PlImExportData m_export_data = null;
    ProofSetupData m_proof_data = new ProofSetupData();
    int _greenScreenVersion = 0;
    //Hashtable m_record_image_primitives = new Hashtable();

    public RiplynxJob()
    {
      _unitList = new List<RiplynxUnit>();
    }

    public RiplynxJobPrmInfo PrmInfo { get; set; }

    [XmlIgnore()]
    public ArrayList Records
    {
      get { return (m_records); }
      set { m_records = value; }
    }

    [XmlIgnore()]
    public ArrayList GroupImages
    {
      get { return (m_group_images); }
      set { m_group_images = value; }
    }

    //[XmlIgnore()]
    //public Hashtable RecordImagePrimitives
    //{
    //get { return (m_record_image_primitives); }
    //}

    [XmlIgnore()]
    List<RiplynxUnit> _unitList;
    [XmlIgnore()]
    public List<RiplynxUnit> UnitList
    {
      get { return _unitList; }
      set { _unitList = value; }
    }

    public RiplynxRecord[] RecordsArray
    {
      get
      {
        RiplynxRecord[] arr_ru = new RiplynxRecord[m_records.Count];
        for (int i = 0; i < m_records.Count; i++)
        {
          arr_ru[i] = m_records[i] as RiplynxRecord;
        }
        return (arr_ru);
      }
      set
      {
        m_records.Clear();
        foreach (RiplynxRecord rr in value)
        {
          m_records.Add(rr);
        }
      }
    }

    public int GreenScreenVersion
    {
      get { return _greenScreenVersion; }
      set { _greenScreenVersion = value; }
    }

    public string Type
    {
      get { return (m_type); }
      set { m_type = value; }
    }

    public string PackagerDir
    {
      get { return m_packager_save_dir; }
      set { m_packager_save_dir = value; }
    }

    public string ImageFilenameFormat
    {
      get { return m_imagefile_format; }
      set { m_imagefile_format = value; }
    }

    public string JobName
    {
      get { return m_job_name; }
      set { m_job_name = value; }
    }

    public Hashtable PackagerIDBars
    {
      get { return m_packager_idbars; }
      set { m_packager_idbars = value; }
    }

    public ArrayList PackagerTextLines
    {
      get { return m_packager_text_lines; }
      set { m_packager_text_lines = value; }
    }

    public string Source
    {
      get { return (m_source); }
      set { m_source = value; }
    }

    public bool PackageByUnit
    {
      get { return _packageByUnit; }
      set { _packageByUnit = value; }
    }

    public bool ForPackager
    {
      get { return m_for_packager; }
      set { m_for_packager = value; }
    }

    public string SourceIP
    {
      get { return (m_source_ip); }
      set { m_source_ip = value; }
    }

    public PlImExportData ExportData
    {
      get { return m_export_data; }
      set { m_export_data = value; }
    }

    public ProofSetupData ProofData
    {
      get { return m_proof_data; }
      set { m_proof_data = value; }
    }

    public string PrintOption
    {
      get { return (m_print_option); }
      set { m_print_option = value; }
    }

    public string Name
    {
      get { return (m_name); }
      set { m_name = value; }
    }

    public IPackageInsertSettings PackageInsertSettings
    {
      get
      {
        return new PackageInsertSettings().Load();
      }
    }

    /// <summary>
    /// Builds all of the primitives that will render this job
    /// </summary>
    /// <param name="pud_filename"></param>
    /// <returns>An array of arrays of primitives.</returns>
    public List<List<RecordPrimitive>> BuildPrimitives(string pud_filename, 
      int cell_spacing)
    {
      return (BuildPrimitives(pud_filename, cell_spacing, false, ""));
    }

    /// <summary>
    /// Builds all of the primitives that will render this job.
    /// Returns an array of an array of primitives.
    /// </summary>
    /// <param name="pud_filename"></param>
    /// <returns>An array of arrays of primitives.</returns>
    public List<List<RecordPrimitive>> BuildPrimitives(string pud_filename, 
      int cell_spacing, bool use_embedded_profile, string dest_profilename)
    {
      List<List<RecordPrimitive>> m_record_primitives = new List<List<RecordPrimitive>>();

      RiplynxPud pud = RiplynxPud.FromXmlFile(pud_filename);

      foreach (RiplynxRecord rr in Records)
      {
        if (rr.Image != null)
        {
          m_record_primitives.Add(BuildRecordPrimitives(rr, pud, cell_spacing, 
            use_embedded_profile, dest_profilename));
        }
      }

      return (m_record_primitives);
    }




    /// <summary>
    /// Builds the primitives for this record
    /// </summary>
    /// <param name="record"></param>
    /// <param name="pud"></param>
    /// <returns></returns>
    public List<RecordPrimitive> BuildRecordPrimitives(RiplynxRecord record, 
      RiplynxPud pud, int cell_spacing, bool use_embedded_profile, 
      string dest_profilename)
    {
      List<RecordPrimitive> m_primitives = new List<RecordPrimitive>();
      IPrimitive record_primitive = null;
      PrimitiveConversionData record_conversion = 
        new PrimitiveConversionData(record.SafeDataRow);

      // Add passthrough if enabled
      if (record.PassThrough)
      {
        m_primitives.Add(new RecordPrimitive(new ImageFile(record.Image.name)));
      }

      // Add package inserts
      IPackageInsertSettings insertSettings = PackageInsertSettings;
      if (insertSettings != null && record.Image != null)
      {
        // Create record primitive.
        IPrimitive packageInsertPrimitive = record.Image.ToPrimitive(use_embedded_profile, dest_profilename);

        foreach (string serviceFilename in insertSettings.Services)
        {
          ServiceInterop.Service s = ServiceInterop.Service.FromIniFile(serviceFilename, packageInsertPrimitive);
          foreach (IPrimitive p in s.ToPrimitives(record.DataTable))
            m_primitives.Add(new RecordPrimitive(p));
        }
      }

      // Build primitives from the ordered packages
      foreach (RiplynxOrderedPackageCollection ordered_package in record.Packages)
      {
        // Make options collection.
        PrimitiveCollection image_options_col = new PrimitiveCollection();
        PrimitiveCollection package_options_col = new PrimitiveCollection();
        foreach (string option_map in ordered_package.Options)
        {
          if (!pud.Units.PrintOptions.ContainsKey(option_map))
          {
            continue;
          }
          RiplynxOption ro = pud.Units.PrintOptions[option_map] as RiplynxOption;
          if (ro.ApplyTo == RiplynxOption.ApplyTos.Image)
          {
            image_options_col.Add(ro.Primitive);
          }
          else if (ro.ApplyTo == RiplynxOption.ApplyTos.Package)
          {
            package_options_col.Add(ro.Primitive);
          }
        }

        // Create record primitive.
        record_primitive = record.Image.ToPrimitive(use_embedded_profile, dest_profilename);
        if (image_options_col.Primitives.Count != 0)
        {
          record_primitive = new PrimitiveCollection(new IPrimitive[] { record_primitive, image_options_col });
        }

        // Create unit primitives.
        foreach (RiplynxOrderedPackage ordered_unit in ordered_package.OrderedPackages)
        {
          RiplynxUnit unit = pud.Units.FindUnit(ordered_unit.Map);
          if (unit == null)
          {
            Trace.WriteLine("Can't find unit: " + ordered_unit.Map);
            continue;
          }

          IPrimitive[] unit_primitives = unit.ToPrimitives(record_primitive,
                                                            record,
                                                            cell_spacing,
                                                            GroupPrimitives);

          //only need to track information on services
          if (unit.Service == null)
          {
            _unitList.Add(null);
          }
          else
          {
            _unitList.Add(unit);
          }

          foreach (IPrimitive unit_primitive in unit_primitives)
          {
            IPrimitive optioned_primitive;

            // Add package options if they exist
            if (package_options_col.Primitives.Count == 0)
            {
              optioned_primitive = unit_primitive;
            }
            else
            {
              Trace.WriteLine("Adding option collection since it exists");
              optioned_primitive = new PrimitiveCollection(new IPrimitive[] { unit_primitive, package_options_col });
            }

            // Add primitive for quantity.
            // NOTE: this should be fixed so work isn't duplicated.
            for (int i = 0; i < ordered_unit.Quantity; i++)
            {
              m_primitives.Add(new RecordPrimitive(
                record_conversion.ConvertData(optioned_primitive), 
                ordered_unit.Map));
            }
          }
        }
      }

      // Keep track of record primitive
      //if (record_primitive != null)
      //{
      //m_record_image_primitives[m_primitives] = record_primitive;
      //}
      return (m_primitives);
    }

    // Converts the list of group images to a hashtable with
    // the name of the group image as the key and the group image
    // primitive as the value.
    protected Hashtable GroupPrimitives
    {
      get
      {
        Hashtable group_primitives = new Hashtable();
        foreach (GroupRecord gr in m_group_images)
        {
          group_primitives[gr.Name + ".jpg"] = gr.ToPrimitive();

        }
        return (group_primitives);
      }
    }

    public static RiplynxJob FromXmlFile(string filename)
    {
      StreamReader sr = new StreamReader(filename, System.Text.Encoding.Default);
      //StreamReader sr = new StreamReader(filename, System.Text.Encoding.UTF8);

      RiplynxJob rj = RiplynxJob.FromXmlTextReader(sr);
      sr.Close();
      return (rj);
    }

    public static RiplynxJob FromXmlTextReader(TextReader text_reader)
    {
      XmlDocument xml_doc = new XmlDocument();
      xml_doc.Load(text_reader);
      return (RiplynxJob.FromXmlNode(xml_doc));
    }

    public static RiplynxJob FromXmlNode(XmlNode node)
    {
      RiplynxJob rj = new RiplynxJob();


      foreach (XmlNode child_node in node.ChildNodes)
      {
        //Trace.WriteLine("ChildNode: " + child_node.Name);
        if (child_node.Name == "Job")
        {
          rj.Name = child_node.Attributes["Identifier"].Value;
          LoadJobXml(rj, child_node);
        }
      }

      return (rj);
    }

    public static void LoadJobXml(RiplynxJob rj, XmlNode node)
    {
      foreach (XmlNode child_node in node.ChildNodes)
      {
        //Trace.WriteLine("JobChildNode: " + child_node.Name);

        if (child_node.Name == "record")
        {
          if (rj.GreenScreenVersion == 0)
          {
            rj.GreenScreenVersion = RiplynxRecord.GetGreenScreenVersion(child_node);
          }
          rj.Records.Add(RiplynxRecord.FromXmlNode(child_node));
        }
        else if (child_node.Name == "PackagerTextDir")
        {
          rj.PackagerDir = child_node.InnerText;
          rj.ForPackager = true;
        }
        else if (child_node.Name == "PackageByUnit")
        {
          rj.PackageByUnit = Convert.ToBoolean(child_node.InnerText);
        }
        else if (child_node.Name == "ImExportDataXml" && child_node.InnerText != "")
        {
          rj.ExportData = new PlImExportData();
          rj.ExportData = rj.ExportData.DeserializeFromXmlString(child_node.InnerText);
        }
        else if (child_node.Name == "ProofSetupData")
        {
          Console.WriteLine("Proof Setup Xml: " + child_node.OuterXml);
          rj.ProofData = (ProofSetupData)ObjectSerializer.SafeDeserializeString(child_node.OuterXml, typeof(ProofSetupData));
          if (rj.ProofData == null) rj.ProofData = new ProofSetupData();
          ConvertXmlNodesToStrings(rj.ProofData.DestFields);
          ConvertXmlNodesToStrings(rj.ProofData.DuplicateFields);
        }
        else if (child_node.Name == "RipLynxImagenameFormat")
        {
          rj.ImageFilenameFormat = child_node.InnerText;
        }
        else if (child_node.Name == "Name")
        {
          rj.JobName = child_node.InnerText;
        }
        else if (child_node.Name == "GroupRecord")
        {
          GroupRecord gr = GroupRecord.FromXmlNode(child_node);

          // Imagematch replaces ' with _ so do it here also to make sure everything matches.
          gr.Name = gr.Name.Replace("'", "_");

          rj.GroupImages.Add(gr);
        }
        else if (child_node.Name == "PrmInfo")
        {
          rj.PrmInfo = RiplynxJobPrmInfo.FromXmlNode(child_node);
        }
      }

      // fix data table id column (change from string to what it should be, usually int32)
      if (rj.RecordsTable != null)
      {
        FixStudentsTableIdColumn(rj.RecordsTable);
      }

    }

    protected static void ConvertXmlNodesToStrings(ArrayList al)
    {
      Dictionary<int, string> newValues = new Dictionary<int, string>();
      foreach (object o in al)
      {
        if (!(o is XmlNode[])) continue;
        XmlNode[] xn = (XmlNode[])o;
        if (xn.Length == 0 || !(xn[0] is XmlText)) continue;
        XmlText xt = (XmlText)xn[0];
        newValues[al.IndexOf(o)] = xt.Value;
      }

      foreach (int ndx in newValues.Keys)
      {
        al[ndx] = newValues[ndx];
      }
    }

    public static void FixStudentsTableIdColumn(DataTable dt)
    {
      if (dt == null || !dt.Columns.Contains(StudentsPeer.IDENTIFIER)) return;
      if (dt.Columns[StudentsPeer.IDENTIFIER].DataType == StudentsPeer.Instance.PrimaryKeyField.SourceType) return;

      dt.Columns[StudentsPeer.IDENTIFIER].ColumnName = StudentsPeer.IDENTIFIER + "Str";

      dt.Columns.Add(StudentsPeer.IDENTIFIER, StudentsPeer.Instance.PrimaryKeyField.SourceType);
      foreach (DataRow dr in dt.Rows)
      {
        if (StudentsPeer.Instance.PrimaryKeyField.SourceType == typeof(Int32))
        {
          dr[StudentsPeer.IDENTIFIER] = Int32.Parse(dr[StudentsPeer.IDENTIFIER + "Str"].ToString());
        }
        else
        {
          throw new Exception("Can't convert primary key field, unknown type: " + StudentsPeer.Instance.PrimaryKeyField.SourceType);
        }
      }
    }

    public static RiplynxOrderedPackage GetOrderedPackage(RiplynxJob rj, int record, int collection, int package)
    {
      return ((RiplynxOrderedPackage)((RiplynxOrderedPackageCollection)((RiplynxRecord)rj.Records[record]).Packages[collection]).OrderedPackages[package]);
    }

    public static RiplynxOrderedPackageCollection GetPackageCollection(RiplynxJob rj, int record, int collection)
    {
      return ((RiplynxOrderedPackageCollection)((RiplynxRecord)rj.Records[record]).Packages[collection]);
    }

    public static RiplynxRecord GetRecord(RiplynxJob rj, int record)
    {
      return ((RiplynxRecord)rj.Records[record]);
    }


    protected DataTable m_dt_records = null;
    public DataTable RecordsTable
    {
      get
      {
        if (m_dt_records == null)
        {
          DataTable dt = null;
          Trace.WriteLine("RiplynxJob.RecordsTable_get: Importing " + Records.Count + " records");
          foreach (RiplynxRecord record in Records)
          {
            if (record.DataTable == null) continue;
            if (dt == null)
            {
              dt = record.DataTable.Copy();
            }
            else
            {
              dt.ImportRow(record.DataRow);
            }
          }

          if (dt == null)
          {
            dt = new DataTable();
          }
          Trace.WriteLine("RiplynxJob.RecordsTable_get: DataTable rows: " + dt.Rows.Count + ", columns: " + dt.Columns.Count);

          //PhotoLynx.Common.UI.Panels.DataPanel.ShowTableInDialog(dt);
          m_dt_records = dt;
        }

        return m_dt_records;
      }
    }

  }
}


