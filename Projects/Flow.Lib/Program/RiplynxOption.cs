using System;
using PhotoLynx.ImageRender.Primitives;

namespace PhotoLynx.ImageRender.RiplynxInterop
{
//
// Class that contains the information for a self contained
// graphic option that can be applied to an image.  Will
// probably be attached to orders, packages, units, etc so
// packages/units don't have to be created with each combination
// of options that can be ordered - black and white, sepia, logo,
// etc.
public class RiplynxOption : IComparable
{
  public enum ApplyTos
  {
    Image,
    Package,
  }


  protected PrimitiveCollection m_primitive;
  protected string m_map;
  protected string m_description;
  protected double m_cost = 0;
  protected double m_price = 0;
  protected ApplyTos m_apply_to = ApplyTos.Package;

  public RiplynxOption() : this ("", "") { }

  public RiplynxOption(string map, string description) : this(map, description, new PrimitiveCollection()) {}

  public RiplynxOption(string map, string description, PrimitiveCollection primitive)
  {
    m_map = map;
    m_description = description;
    m_primitive = primitive;
  }

  public PrimitiveCollection Primitive
  {
    get { return (m_primitive); }
    set { m_primitive = value; }
  }

  public double Cost
  {
    get { return (m_cost); }
    set { m_cost = value; }
  }

  public double Price
  {
    get { return (m_price); }
    set { m_price = value; }
  }

  public string Map
  {
    get { return (m_map); }
    set { m_map = value; }
  }

  public string Description
  {
    get { return (m_description); }
    set { m_description = value; }
  }

  public ApplyTos ApplyTo
  {
    get { return (m_apply_to); }
    set { m_apply_to = value; }
  }

  public override string ToString()
  {
    return (Map + ": " + Description);
  }

  public int CompareTo(object o)
  {
    if (! (o is RiplynxOption))
    {
      return (0);
    }

    RiplynxOption ro = o as RiplynxOption;
    return (Map.CompareTo(ro.Map));
  }
}
}
