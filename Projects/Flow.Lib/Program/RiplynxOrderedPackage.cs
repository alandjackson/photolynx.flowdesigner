/* ----------------------------------------------------------------------------
 * $Id: RiplynxOrderedPackage.cs 2832 2007-08-30 23:55:32Z ajackson $
 *
 * --------------------------------------------------------------------------*/

using System;
using System.IO;
using System.Xml;
using System.Collections;
using System.Xml.Serialization;
using System.Diagnostics;
using NUnit.Framework;
using PhotoLynx.ImageRender;

namespace PhotoLynx.ImageRender.RiplynxInterop
{
public class RiplynxOrderedPackage
{
  ArrayList m_options;
  string m_package_map;
  int m_quantity;
  string m_packager_text_line;

  public RiplynxOrderedPackage() : this("", 0) {}
  public RiplynxOrderedPackage(string package_map, int quantity) : this(package_map, new ArrayList(), quantity, "") {}
  public RiplynxOrderedPackage(string package_map, int quantity, string text) : this(package_map, new ArrayList(), quantity, text) { }
  public RiplynxOrderedPackage(string package_map, ArrayList options, int quantity) : this(package_map, options, quantity, "") { }

  public RiplynxOrderedPackage(string package_map, ArrayList options, int quantity, string text)
  {
    m_options = options;
    m_package_map = package_map;
    m_quantity = quantity;
    m_packager_text_line = text;
  }

  public ArrayList Options
  {
    get { return (m_options); }
    set { m_options = value; }
  }

  public string Map
  {
    get { return (m_package_map); }
    set { m_package_map = value; }
  }

  public int Quantity
  {
    get { return (m_quantity); }
    set { m_quantity = value; }
  }

  public string PackagerText
  {
    get { return (m_packager_text_line); }
    set { m_packager_text_line = value; }
  }

  /// <summary>
  /// Parses the string map-quantity and returns the object
  /// </summary>
  /// <param name="rep"></param>
  /// <returns></returns>
  public static RiplynxOrderedPackage FromString(string rep)
  {
    try
    {
      string [] split = rep.Split('-');
      if (split.Length != 2)
      {
        return (null);
      }
      string [] maps = split[0].Split(',');
      ArrayList options = new ArrayList();
      for (int i = 1; i < maps.Length; i++)
      {
        options.Add(maps[i]);
      }

      //Trace.WriteLine("RiplynxOrderedPackage.FromString map: " + maps[0] + " quantity: " + split[1] + ", parsed: " + Int32.Parse(split[1]));
      return (new RiplynxOrderedPackage(maps[0], options, Int32.Parse(split[1])));
    }
    catch (Exception e)
    {
      Trace.WriteLineIf(ImageRender.Switch.TraceError, "Can't parse package string: '" + rep + "': " + e);
      return (null);
    }
  }


  public static RiplynxOrderedPackage FromXmlNode(XmlNode node)
  {
    try
    {
      return (new RiplynxOrderedPackage(node.Attributes["Map"].Value, Int32.Parse(node.Attributes["Quantity"].Value),node.Attributes["PackagerTextLine"].Value));
    }
    catch
    {
      //not meant for packager
    }
      return (new RiplynxOrderedPackage(node.Attributes["Map"].Value, Int32.Parse(node.Attributes["Quantity"].Value)));
    
  }

  //
  // Joins all of the string elements in the array using the delimiter specified.
  //
  public string Join(string del, ArrayList array)
  {
    string str = "";
    foreach (string s in array)
    {
      if (str != "")
      {
        str += del;
      }
      str += s;
    }
    return (str);
  }

  //
  // Writes a string in the map,option-map,option format.
  //
  public override string ToString()
  {
    string option_str = Join(",", m_options);
    if (option_str != "")
    {
      option_str = "," + option_str;
    }
    return (m_package_map + option_str + "-" + m_quantity.ToString());
  }

}
}
