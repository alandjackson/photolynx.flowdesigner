/* ----------------------------------------------------------------------------
 * $Id: RiplynxProgram.cs 2174 2004-10-22 00:56:34Z ajackson $
 *
 * --------------------------------------------------------------------------*/

using System;
using System.IO;
using System.Xml;
using System.Collections;
using System.ComponentModel;

namespace PhotoLynx.ImageRender.RiplynxInterop
{
public class RiplynxProgram
{
  RiplynxPackageCollection m_packages = new RiplynxPackageCollection();

  public RiplynxPackageCollection Packages
  {
    get { return (m_packages); }
    set { m_packages = value; }
  }

  public static RiplynxProgram FromXmlNode(XmlNode node)
  {
    RiplynxProgram program = new RiplynxProgram();

    foreach (XmlNode child_node in node.ChildNodes)
    {
      if (child_node.Name == "Package")
      {
        program.Packages.Packages.Add(RiplynxPackage.FromXmlNode(child_node));
      }
    }
    
    return (program);
  }
}
}