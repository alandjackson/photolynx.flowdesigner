/* ----------------------------------------------------------------------------
 * $Id: RiplynxPackage.cs 2174 2004-10-22 00:56:34Z ajackson $
 *
 * --------------------------------------------------------------------------*/

using System;
using System.IO;
using System.Xml;
using System.Collections;
using System.ComponentModel;

namespace PhotoLynx.ImageRender.RiplynxInterop
{

public class RiplynxPackage
{
  string m_map;
  double m_price;
  string m_title;
  string m_description;
  double m_cost;
  bool m_taxed;
  string m_code;
  
  ArrayList m_units = new ArrayList();
  
  public string Map
  {
    get { return (m_map); }
    set { m_map = value; } 
  }
  
  public double Price
  {
    get { return (m_price); }
    set { m_price = value; }
  }
  

  public string Description
  {
    get { return (m_description); }
    set { m_description = value; }
  }
  
  public string Title
  {
    get { return (m_title); }
    set { m_title = value; }
  }

  public double Cost
  {
    get { return (m_cost); }
    set { m_cost = value; }
  }
  
  public bool Taxed
  {
    get { return (m_taxed); }
    set { m_taxed = value; }
  }
  
  public string Code
  {
    get { return (m_code); }
    set { m_code = value; }
  }

  public ArrayList Units
  {
    get { return (m_units); }
    set { m_units = value; }
  }

  public static RiplynxPackage FromXmlNode(XmlNode node)
  {
    RiplynxPackage package = new RiplynxPackage();

    foreach (XmlNode child_node in node.ChildNodes)
    {
      switch (child_node.Name)
      {
        case "Map":
          package.Map = child_node.InnerText;
          break;
        case "Title":
          package.Title = child_node.InnerText;
          break;
        case "Description":
          package.Description = child_node.InnerText;
          break;
        case "Unit":
          package.Units.Add(RiplynxUnit.FromXmlNode(child_node));
          break;
        case "Price":
          package.Price = System.Double.Parse(child_node.InnerText);
          break;
        case "Taxed":
          package.Taxed = System.Boolean.Parse(child_node.InnerText);
          break;
        case "Cost":
          package.Cost = System.Double.Parse(child_node.InnerText);
          break;
        case "Code":
          package.Code = child_node.InnerText;
          break;
      }
    }
    
    return (package);
  }

}
}