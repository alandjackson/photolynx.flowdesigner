/* ----------------------------------------------------------------------------
 * $Id: RipLynxSubUnit.cs 2878 2007-11-09 18:22:26Z ajackson $
 *
 * --------------------------------------------------------------------------*/

using System;
using System.IO;
using System.Xml;
using System.Data;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.ComponentModel;
using NUnit.Framework;
using PhotoLynx.Common.Core;
using PhotoLynx.ImageRender.Primitives;
using PhotoLynx.ImageRender.Core;
using PhotoLynx.ImageRender.UI.Managers;

namespace PhotoLynx.ImageRender.RiplynxInterop
{
[TypeConverter(typeof(ExpandableObjectConverter))]
public class RiplynxSubUnit
{
  public enum UnsharpColorSpaces
  {
    YUV,
    RGB,
  }

  int m_grid;
  int m_cols;
  int m_rows;
  float m_rotate;
  float m_x;
  float m_y;
  float m_width;
  float m_height;
  int m_unsharp_amount;
  int m_unsharp_radius;
  int m_unsharp_threshhold;
  UnsharpColorSpaces m_unsharp_color_spaces;
  bool m_use_unsharp_mask;
  bool m_use_graphics = false;
  string m_graphics_xml = "";

  public RiplynxSubUnit()
  {

  }

  public int Grid
  {
    get { return (m_grid); }
    set { m_grid = value; }
  }

  public int Columns
  {
    get { return (m_cols); }
    set { m_cols = value; }
  }

  public int Rows
  {
    get { return (m_rows); }
    set { m_rows = value; }
  }

  public float Rotate
  {
    get { return (m_rotate); }
    set { m_rotate = value; }
  }

  public float X
  {
    get { return (m_x); }
    set { m_x = value; }
  }

  public float Y
  {
    get { return (m_y); }
    set { m_y = value; }
  }

  public float Width
  {
    get { return (m_width); }
    set { m_width = value; }
  }

  public float Height
  {
    get { return (m_height); }
    set { m_height = value; }
  }


  public int UnsharpAmount
  {
    get { return (m_unsharp_amount); }
    set { m_unsharp_amount = value; }
  }

  public int UnsharpRadius
  {
    get { return (m_unsharp_radius); }
    set { m_unsharp_radius = value; }
  }

  public int UnsharpThreshhold
  {
    get { return (m_unsharp_threshhold); }
    set{ m_unsharp_threshhold = value; }
  }

  public UnsharpColorSpaces UnsharpColorSpace
  {
    get { return (m_unsharp_color_spaces); }
    set { m_unsharp_color_spaces = value; }
  }

  public bool UseUnsharpMask
  {
    get { return (m_use_unsharp_mask); }
    set { m_use_unsharp_mask = value; }
  }

  public bool UseGraphics
  {
    get { return (m_use_graphics); }
    set { m_use_graphics = value; }
  }

  public string GraphicsXml
  {
    get { return (m_graphics_xml); }
    set { m_graphics_xml = value; }
  }

  /// <summary>
  /// Creates a RiplynxSubUnit from the passed xml node, reading
  /// all of the information in manually
  /// </summary>
  /// <param name="node"></param>
  /// <returns></returns>
  public static RiplynxSubUnit FromXmlNode(XmlNode node)
  {
    RiplynxSubUnit su = new RiplynxSubUnit();

    foreach (XmlNode child_node in node.ChildNodes)
    {
      switch (child_node.Name)
      {
        case "Grid":
          su.Grid = Int32.Parse(child_node.InnerText);
          break;
        case "Columns":
          su.Columns = Int32.Parse(child_node.InnerText);
          break;
        case "Rows":
          su.Rows = Int32.Parse(child_node.InnerText);
          break;
        case "X":
          su.X = float.Parse(child_node.InnerText);
          break;
        case "Y":
          su.Y = float.Parse(child_node.InnerText);
          break;
        case "Width":
          su.Width = float.Parse(child_node.InnerText);
          break;
        case "Height":
          su.Height = float.Parse(child_node.InnerText);
          break;
        case "UnsharpAmount":
          su.UnsharpAmount = Int32.Parse(child_node.InnerText);
          break;
        case "UnsharpRadius":
          su.UnsharpRadius = Int32.Parse(child_node.InnerText);
          break;
        case "UnsharpThreshhold":
          su.UnsharpThreshhold = Int32.Parse(child_node.InnerText);
          break;
        case "UnsharpColorspace":
          if (child_node.InnerText == "YUV")
          {
            su.UnsharpColorSpace = UnsharpColorSpaces.YUV;
          }
          else
          {
            su.UnsharpColorSpace = UnsharpColorSpaces.RGB;
          }
          break;
        case "UseUnsharpMask":
          su.UseUnsharpMask = Boolean.Parse(child_node.InnerText);
          break;
        case "Rotate":
          su.Rotate = float.Parse(child_node.InnerText);
          break;
        case "UseSubUnitGraphicsXml":
          su.UseGraphics = Boolean.Parse(child_node.InnerText);
          break;
        case "SubUnitGraphicsXml":
          su.GraphicsXml = child_node.InnerText;
          break;
        default:
          Console.WriteLine("RiplynxSubUnit unhandled xml child: " + child_node.Name);
          break;
      }
    }
    return (su);
  }

  public IPrimitive ToPrimitive(IPrimitive image_primitive)
  {
    return (ToPrimitive(image_primitive, null, 0, null));
  }

  public PackageSubUnit GetPackageSubUnit()
  {
    PackageSubUnit psu = new PackageSubUnit();
    psu.Columns = Columns;
    psu.Rows = Rows;
    psu.Top = Y;
    psu.Left = X;
    psu.SubUnitWidth = Width;
    psu.SubUnitHeight = Height;
    psu.RenderSource = true;

    switch ((int) Rotate % 360)
    {
    case 90:
      psu.RotateFlipType = RotateFlipType.Rotate90FlipNone;
      break;
    case 180:
      psu.RotateFlipType = RotateFlipType.Rotate180FlipNone;
      break;
    case 270:
      psu.RotateFlipType = RotateFlipType.Rotate270FlipNone;
      break;
    }

    return (psu);
  }

  /// <summary>
  /// Searches for and returns a crop primitive inside the primitive collection.
  /// Is not recursive so it does not search collections within the collection.
  /// </summary>
  public Crop CropInPrimitiveCollection(IPrimitive ip)
  {
    if (ip == null || ! (ip is PrimitiveCollection))
    {
      return (null);
    }
    PrimitiveCollection col = ip as PrimitiveCollection;

    foreach (IPrimitive p in col.Primitives)
    {
      if (p is Crop)
      {
        return (p as Crop);
      }
    }
    return (null);
  }

  public PrimitiveCollection AddCropAfterExistingCrop(IPrimitive ip, Crop crop)
  {
    if (ip == null || ! (ip is PrimitiveCollection))
    {
      return (null);
    }
    PrimitiveCollection new_ip = (PrimitiveCollection) (ip as PrimitiveCollection).Clone();
    if (InsertCrop(new_ip, crop))
    {
      return (new_ip);
    }
    return (null);
  }


  public bool InsertCrop(PrimitiveCollection new_ip, Crop crop)
  {
    Trace.WriteLine("RiplynxSubUnit.InsertCrop, primitive collection count: " + new_ip.Primitives.Count);
    Trace.WriteLine(AbstractPrimitive.GetPrimitiveTypeTree(new_ip).ToString());
    //foreach (IPrimitive p in new_ip.PrimitivesArray)
    //{
    //  Trace.WriteLine("\t" + p.GetType().Name);
    //}

    for (int i = 0; i < new_ip.Primitives.Count; i++)
    {
      if (new_ip.Primitives[i] is Crop)
      {
        //new_ip.Primitives.Insert(i + 1, crop);
        new_ip.Primitives.Add(crop);
        return (true);
      }
      if (new_ip.Primitives[i] is PrimitiveCollection)
      {
        if (InsertCrop(new_ip.Primitives[i] as PrimitiveCollection, crop))
        {
          return (true);
        }
      }
    }
    return (false);

  }

  /// <summary>
  /// Builds a primitive that represents this sub unit
  /// </summary>
  ///
  /// <remarks>
  /// Structure:
  ///  -> ContainedPrimitive   (place sub unit in correct location)
  ///    -> RepeatingPrimitive   (Repeate image for grid)
  ///        -> Primitive Collection
  ///            -> Image          (Image to render, adjusted/cropped)
  ///            -> Crop           (To subunit size)
  ///            -> UnsharpMask    (built in sub unit modifier)
  ///            -> Text Overlay   (convert to atalsoft primitive)
  ///            -> Rotate
  ///
  /// </remarks>
  public IPrimitive ToPrimitive(IPrimitive image_primitive, IPrimitive unit_graphics, int cell_spacing, DataRow dr)
  {
    Crop crop = new Crop((m_rotate % 90 == 0 && m_rotate % 180 != 0) ? m_height / m_width : m_width / m_height);

    // Create unit primitive starting with the image primitive
    PrimitiveCollection primitive_collection = new PrimitiveCollection();

    PrimitiveCollection new_ip = AddCropAfterExistingCrop(image_primitive, crop);
    if (new_ip != null)
    {
      primitive_collection.Primitives.Add(new_ip);
    }
    else
    {
      primitive_collection.Primitives.Add(image_primitive);
      primitive_collection.Primitives.Add(crop);
    }


    // Add unit graphics.
    primitive_collection.Primitives.Add(unit_graphics);

    // Add sub unit graphics.
    if (UseGraphics && GraphicsXml != "")
    {
      PrimitiveCollection sub_unit_graphics = null;
      sub_unit_graphics = (PrimitiveCollection) ObjectSerializer.DeserializeString(GraphicsXml, typeof(PrimitiveCollection));
      sub_unit_graphics = (PrimitiveCollection) new PrimitiveConversionData(dr).ConvertData(sub_unit_graphics);
      primitive_collection.Primitives.Add(sub_unit_graphics);
    }

    primitive_collection.Primitives.Add(new ImageEffects(new RotateCommandWrapper(m_rotate)));
    if (m_use_unsharp_mask)
    {
      primitive_collection.Primitives.Add(new ImageEffects(new UnsharpMaskCommandWrapper((double) m_unsharp_amount / 100, m_unsharp_radius, m_unsharp_threshhold)));
    }


    RepeatingPrimitive repeating_primitive = new RepeatingPrimitive(primitive_collection);
    repeating_primitive.Columns = m_cols;
    repeating_primitive.Rows = m_rows;
    repeating_primitive.SpaceColor = Color.White;
    repeating_primitive.Spacing = new SizeF(cell_spacing, cell_spacing);
    repeating_primitive.BaseSize = new SizeF(m_width, m_height);
    repeating_primitive.SizeMethod = PrimitiveSizeMethod.Crop;

    ContainedPrimitive contained_primitive = new ContainedPrimitive(repeating_primitive);
    contained_primitive.X = m_x;
    contained_primitive.Y = m_y;
    contained_primitive.Width = m_width * m_cols;
    contained_primitive.Height = m_height * m_rows;
    contained_primitive.Primitive = repeating_primitive;
    //contained_primitive.AutoCrop = true;
    //contained_primitive.MaintainAspect = true;

    return (contained_primitive);
  }
}

[TestFixture]
public class RiplynxSubUnitTest
{
  public static RiplynxSubUnit CreateTestRiplynxSubUnit()
  {
    RiplynxSubUnit subunit = new RiplynxSubUnit();
    subunit.UseUnsharpMask = false;
    subunit.Width = 3;
    subunit.Height = 5;
    subunit.Rotate = -90;
    subunit.Rows = 1;
    subunit.Columns = 2;
    subunit.X = 0;
    subunit.Y = 0;
    return (subunit);
  }

}
}
