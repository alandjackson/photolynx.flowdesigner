/* ----------------------------------------------------------------------------
 * $Id: RiplynxRecord.cs 3250 2008-04-03 05:11:52Z photolyn $
 *
 * --------------------------------------------------------------------------*/

using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Data;
using System.Drawing;
using System.Collections;
using System.Diagnostics;
using System.Windows.Forms;
using NUnit.Framework;
using Atalasoft.Imaging;
using PhotoLynx.ImageRender.Primitives;
using PhotoLynx.ImageRender.UI.Panels;
using PhotoLynx.Common.Core;

namespace PhotoLynx.ImageRender.RiplynxInterop
{
public class RiplynxRecord
{
  static Font m_font_idbar = new Font("Arial", 18, FontStyle.Regular, GraphicsUnit.Pixel);
  static Font m_font_packager_idbar = new Font("Labox", 18, FontStyle.Regular, GraphicsUnit.Pixel);

  ArrayList m_packages = new ArrayList();
  ArrayList m_duplicates = new ArrayList();
  RiplynxImage m_image;
  DataSet m_ds = new DataSet();
  string m_str_text_overlay;
  string m_str_idbar = "";
  bool m_pass_through = false;
  bool m_split_package = false;

  [XmlIgnore()]
  public ArrayList Packages
  {
    get { return (m_packages); }
  }

  public RiplynxOrderedPackageCollection [] PackageOrdersArray
  {
    get
    {
      RiplynxOrderedPackageCollection [] arr = new RiplynxOrderedPackageCollection[m_packages.Count];
      for (int i = 0; i < m_packages.Count; i++)
      {
        arr[i] = m_packages[i] as RiplynxOrderedPackageCollection;
      }
      return (arr);
    }
    set
    {
      m_packages.Clear();
      foreach (RiplynxOrderedPackageCollection p in value)
      {
        m_packages.Add(p);
      }
    }
  }

  [XmlIgnore()]
  public ArrayList Duplicates
  {
    get { return (m_duplicates); }
  }

  [XmlIgnore()]
  public Hashtable DuplicatePrimitives
  {
    get
    {
      Hashtable ht = new Hashtable();
      foreach (Duplicate d in Duplicates)
      {
        if (d.Image != null)
        {
          ht[d.FieldName] = d.Image.ToPrimitive();
        }
        else
        {
          ht[d.FieldName] = null;
        }
      }
      return (ht);
    }
  }

  public bool SplitPackage
  {
    get { return (m_split_package); }
    set { m_split_package = value; }
  }

  public string IdBarString
  {
    get { return (m_str_idbar); }
    set { m_str_idbar = value; }
  }

  public RiplynxImage Image
  {
    get { return (m_image); }
    set { m_image = value; }
  }

  public DataSet RecordDataSet
  {
    get { return (m_ds); }
    set { m_ds = value; }
  }

  public DataTable DataTable
  {
    get 
    {
      if (RecordDataSet == null || RecordDataSet.Tables.Count == 0) return null;
      return (RecordDataSet.Tables[0]); 
    }
  }

  public DataRow DataRow
  {
    get { return (DataTable.Rows[0]); }
  }

  public DataRow SafeDataRow
  {
    get
    {
      try
      {
        return (DataRow);
      }
      catch
      {
        return (null);
      }
    }
  }

  public string TextOverlayString
  {
    get { return (m_str_text_overlay); }
    set { m_str_text_overlay = value; }
  }

  public bool PassThrough
  {
    get { return (m_pass_through); }
    set { m_pass_through = value; }
  }

  public ArrayList BuildPrimitives()
  {
    return (null);
  }

  internal static int GetGreenScreenVersion(XmlNode node)
  {
    foreach (XmlNode child_node in node.ChildNodes)
    {
      if (child_node.Name == "image" || child_node.Name == "RiplynxImage")
      {
        return RiplynxImage.GetGreenScreenVersion(child_node);
      }
    }
    return 1;
  }

  public static RiplynxRecord FromXmlNode(XmlNode node)
  {
    RiplynxRecord rr = new RiplynxRecord();

    foreach (XmlNode child_node in node.ChildNodes)
    {
      //Trace.WriteLine("Record ChildNode: " + child_node.Name);

      if (child_node.Name == "textoverlay")
      {
        rr.TextOverlayString = child_node.InnerText;
      }

      else if (child_node.Name == "image" || child_node.Name == "RiplynxImage")
      {
        rr.Image = RiplynxImage.FromXmlNode(child_node);
        Trace.WriteLine("Loading riplynx image from xml node, name: " + rr.Image.name);
        //Trace.WriteLine("Node text: " + child_node.InnerText);
      }

      else if (child_node.Name == "package")
      {
        rr.Packages.Add(RiplynxOrderedPackageCollection.FromXmlNode(child_node));
      }

      else if (child_node.Name == "DataSet")
      {
        rr.RecordDataSet = ReadDataSet(child_node);

      }

      else if (child_node.Name == "student")
      {
        rr.IdBarString = BuildIdBarString(child_node);
      }

      else if (child_node.Name == "duplicate")
      {
        rr.Duplicates.Add(Duplicate.FromXmlNode(child_node));
      }

      else if (child_node.Name == "passthrough")
      {
        rr.PassThrough = true;
      }

      else if (child_node.Name == "SplitPackage")
      {
        rr.SplitPackage = (child_node.InnerText == "1");
      }

      else if (child_node.Name == "#text")
      {
      }

      else
      {
      }

    }

    rr.FixDataSet();
    return (rr);
  }

  public static IPrimitive BuildIdBarPrimitive(string idbar)
  {
    return (BuildIdBarPrimitive(idbar, 1));
  }

  protected static int m_idbar_width = 20;
  public static int IdbarWidth
  {
    get { return (m_idbar_width); }
    set { m_idbar_width = value; }
  }

  public static IPrimitive BuildIdBarPrimitive(string idbar, float scale)
  {
    return BuildIdBarPrimitive(idbar, scale, false);
  }

  //
  // Builds the idbar primitive.
  //
  public static IPrimitive BuildIdBarPrimitive(string idbar, float scale, bool forPackager)
  {
    
    Font font;
    if (forPackager)
    {
      font = FontEx.ChangeFontSize(m_font_packager_idbar, m_font_packager_idbar.Size * scale);
    }
    else
    {
      font = FontEx.ChangeFontSize(m_font_idbar, m_font_idbar.Size * scale);
    }
    AtalaImage image = new AtalaImage(10, 10, PixelFormat.Pixel32bppBgra);
    SizeF text_size = image.GetGraphics().MeasureString(idbar, font);
    Trace.WriteLine("Text Size: " + text_size);
    image.Dispose();

    Text text = new Text();
    text.UsePercentBounds = true;
    text.Area = new RectangleF(0, 0, 100, 100);
    text.StrText = idbar;
    text.Font = font;
    text.VertAlign = StringAlignment.Near;

    SizeF idbar_size = new SizeF(Math.Max(1, (float) Math.Ceiling(text_size.Width)), IdbarWidth * scale);

    return (new PrimitiveCollection(new IPrimitive [] {new Blank(idbar_size, Color.White), text}));
  }
  
  /// <summary>
  /// Builds the idbar string from the data attribute in each field child node.
  /// </summary>
  /// <param name="node"></param>
  /// <returns></returns>
  protected static string BuildIdBarString(XmlNode node)
  {
    string idbar = "";

    // add data for each field child node
    foreach (XmlNode child_node in node.ChildNodes)
    {
      if (child_node.Name != "field")
      {
        continue;
      }

      idbar += child_node.Attributes["data"].Value + " - ";
    }

    // remove last " - "
    if (idbar.Length >= 3)
    {
      idbar = idbar.Substring(0, idbar.Length - 3);
    }

    return (idbar);
  }

  protected static DataSet ReadDataSet(XmlNode node)
  {
    DataSet ds = new DataSet();

    foreach (XmlNode child_node in node.ChildNodes)
    {
      if (child_node.Name == "RecordTable")
      {
        ds.Tables.Add(ReadDataTable(child_node));
      }
    }

    return (ds);

  }

  protected static DataTable ReadDataTable(XmlNode node)
  {
    DataTable dt = new DataTable();
    Hashtable ht_data = new Hashtable();

    // read data
    foreach (XmlNode child_node in node.ChildNodes)
    {
      if (child_node.Name != "Field")
      {
        continue;
      }

      string fieldname = "";
      string fieldvalue = "";

      foreach (XmlNode field_node in child_node.ChildNodes)
      {
        if (field_node.Name == "Name")
        {
          fieldname = field_node.InnerText;
        }
        if (field_node.Name == "Value")
        {
          fieldvalue = field_node.InnerText;
        }
      }

      ht_data[fieldname] = fieldvalue;
    }

    // create columns
    foreach (string fieldname in ht_data.Keys)
    {
      dt.Columns.Add(fieldname, typeof(string));
    }

    // create row
    dt.Rows.Add(dt.NewRow());
    foreach (string fieldname in ht_data.Keys)
    {
      dt.Rows[0][fieldname] = ht_data[fieldname];
    }

    return (dt);
  }

  /// <summary>
  /// Modifies the data set to look like the complete students table
  /// loaded from the image match job since the service data is going
  /// to depend on data that is in that format.
  /// </summary>
  protected void FixDataSet()
  {
    if (RecordDataSet.Tables.Count == 0)
    {
      return;
    }
    RecordDataSet.Tables[0].Columns.Add("IAux2", typeof(double));
    RecordDataSet.Tables[0].Columns.Add("IAux3", typeof(double));
    RecordDataSet.Tables[0].Columns.Add("IAux4", typeof(double));
    RecordDataSet.Tables[0].Columns.Add("IAux5", typeof(double));
    RecordDataSet.Tables[0].Columns.Add("IAux6", typeof(double));
    RecordDataSet.Tables[0].Columns.Add("IAux7", typeof(double));
    RecordDataSet.Tables[0].Columns.Add("IAux8", typeof(double));
    RecordDataSet.Tables[0].Columns.Add("IAux10", typeof(double));
    RecordDataSet.Tables[0].Columns.Add("IAux11", typeof(double));
    RecordDataSet.Tables[0].Columns.Add("IAux12", typeof(double));

    if (Image != null)
    {
      RecordDataSet.Tables[0].Rows[0]["Image Name"] = Image.name;
      RecordDataSet.Tables[0].Rows[0]["IAux2"] = Image.density;
      RecordDataSet.Tables[0].Rows[0]["IAux3"] = Image.contrast;
      RecordDataSet.Tables[0].Rows[0]["IAux4"] = Image.sharpen;
      RecordDataSet.Tables[0].Rows[0]["IAux5"] = Image.red;
      RecordDataSet.Tables[0].Rows[0]["IAux6"] = Image.green;
      RecordDataSet.Tables[0].Rows[0]["IAux7"] = Image.blue;
      RecordDataSet.Tables[0].Rows[0]["IAux8"] = Image.crop_ratio;
      RecordDataSet.Tables[0].Rows[0]["IAux10"] = Image.crop_left;
      RecordDataSet.Tables[0].Rows[0]["IAux11"] = Image.crop_top;
      RecordDataSet.Tables[0].Rows[0]["IAux12"] = Image.crop_height;
    }
  }


}

[TestFixture]
public class RiplynxRecordTest
{
  [Ignore("")]
  [Test]
  public void RenderIdBar()
  {
    IPrimitive primitive = RiplynxRecord.BuildIdBarPrimitive("Mark Allignton - TEST MIDDLE  - ALan Jackson - 234-234-2 - Jan 7 2004 1234");

    //PrimitiveSurfacePanel.ShowImageInDialog(primitive.Renderer.AtalaRender(null));
  }

  [Ignore("")]
  [Test]
  public void ContainedPrimitiveTest()
  {

    Blank blank = new Blank(new SizeF(200, 200), Color.Red);

    ContainedPrimitive contained = new ContainedPrimitive(new Blank(new SizeF(100, 100), Color.Blue));
    contained.Rect = new RectangleF(-20, -20, 120, 120);

    PrimitiveCollection col = new PrimitiveCollection(new IPrimitive [] {blank, contained});

    //PrimitiveSurfacePanel.ShowImageInDialog(col.Renderer.AtalaRender(null));
  }
}
}
