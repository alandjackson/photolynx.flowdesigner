/* ----------------------------------------------------------------------------
 * $Id: RiplynxPrm.cs 3123 2008-02-12 21:44:48Z photolyn $
 *
 * --------------------------------------------------------------------------*/

using System;
using System.Collections;
using System.IO;
using System.Xml;
using NUnit.Framework;

namespace PhotoLynx.ImageRender.RiplynxInterop
{
public class RiplynxPrm
{
  RiplynxPrograms m_programs;

  public RiplynxPrograms Programs
  {
    get { return (m_programs); }
    set { m_programs = value; }
  }

  /// <summary>
  /// A PRM always has one program even though there is data support
  /// for more than one.  I don't think Alan Minner knew what he was doing, do you?
  /// </summary>
  public RiplynxProgram Program
  {
    get { return (m_programs.Programs[0] as RiplynxProgram); }
  }

  public ArrayList Packages
  {
    get { return (Program.Packages.Packages); }
  }

  // Gets a list of all the units referenced by the prm.
  public ArrayList UnitsList
  {
    get
    {
      // Use a hashtable to only keep track of unique units
      Hashtable ht_units = new Hashtable();

      // Go through all of the packages and all of the units in each package
      // and make a list of all the unit maps.
      foreach (RiplynxPackage package in Packages)
      {
        foreach (RiplynxUnit unit in package.Units)
        {
          ht_units[unit.Map] = true;
        }

      }

      // Copy the units stored in the hashtable to an arraylist
      ArrayList al_units = new ArrayList();
      foreach (string unit_map in ht_units.Keys)
      {
        al_units.Add(unit_map);
      }

      return (al_units);
    }
  }

  public static RiplynxPrm FromXmlFile(string filename)
  {
    StreamReader sr = new StreamReader(filename, System.Text.Encoding.UTF8);
    XmlDocument xml_doc = new XmlDocument();
    xml_doc.Load(sr);
    RiplynxPrm rp = FromXmlNode(xml_doc);
    sr.Close();
    return (rp);

  }

  public static RiplynxPrm FromXmlNode(XmlNode node)
  {
    RiplynxPrm rp = new RiplynxPrm();

    foreach (XmlNode child_node in node.ChildNodes)
    {
      if (child_node.Name == "Programs")
      {
        rp.Programs = RiplynxPrograms.FromXmlNode(child_node);
      }
    }
    return (rp);
  }
}
}
