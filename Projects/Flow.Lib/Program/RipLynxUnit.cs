/* ----------------------------------------------------------------------------
 * $Id: RipLynxUnit.cs 3490 2008-08-05 05:15:52Z photolyn $
 *
 * --------------------------------------------------------------------------*/

using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Data;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using NUnit.Framework;
using PhotoLynx.ImageRender.Primitives;
using PhotoLynx.ImageRender.Renderers;
using PhotoLynx.Common.Core;
using PhotoLynx.ImageRender.ServiceInterop;
using PhotoLynx.ImageRender.Core;

namespace PhotoLynx.ImageRender.RiplynxInterop
{

public enum RiplynxUnitType
{
  Unit = 0,
  Service = 1,
}

[TypeConverter(typeof(ExpandableObjectConverter))]
public class RiplynxUnit
{
  ArrayList m_sub_units = new ArrayList();

  string m_description;
  string m_short_description;
  double m_x;
  double m_y;
  string m_map;
  RiplynxUnitType m_unit_type;
  Service _service;
  string m_service_name;
  int m_service_rotation = 0;
  bool m_use_text_overlay;
  string m_text_overlay_xml;
  bool m_use_graphics;
  string m_graphics_xml;
  int m_quantity;
  bool m_use_unit_graphics;
  string m_unit_graphics_xml;

  public RiplynxUnit()
  {
  }

  public string Description
  {
    get { return (m_description); }
    set { m_description = value; }
  }

  public string ShortDescription
  {
    get { return (m_short_description); }
    set { m_short_description = value; }
  }

  public Service Service
  {
    get{ return _service;}
    set { _service = value; }
  }

  public double X
  {
    get { return (m_x); }
    set { m_x = value; }
  }

  public double Y
  {
    get { return (m_y); }
    set { m_y = value; }
  }

  public string Map
  {
    get { return (m_map); }
    set { m_map = value; }
  }

  public int UnitType
  {
    get { return ((int) m_unit_type); }
    set { m_unit_type = (RiplynxUnitType) value; }
  }

  [XmlIgnore()]
  public RiplynxUnitType RiplynxUnitType
  {
    get { return (m_unit_type); }
    set { m_unit_type = value; }
  }

  public int ServiceRotation
  {
    get { return (m_service_rotation); }
    set { m_service_rotation = value; }
  }

  public string ServiceName
  {
    get { return (m_service_name); }
    set { m_service_name = value; }
  }

  public bool UseTextOverlay
  {
    get { return (m_use_text_overlay); }
    set { m_use_text_overlay = value; }
  }

  public string TextOverlayXml
  {
    get { return (m_text_overlay_xml); }
    set { m_text_overlay_xml = value; }
  }

  public bool UseGraphics
  {
    get { return (m_use_graphics); }
    set { m_use_graphics = value; }
  }

  public string GraphicsXml
  {
    get { return (m_graphics_xml); }
    set { m_graphics_xml = value; }
  }

  public bool UseUnitGraphics
  {
    get { return (m_use_unit_graphics); }
    set { m_use_unit_graphics = value; }
  }

  public string UnitGraphicsXml
  {
    get { return (m_unit_graphics_xml); }
    set { m_unit_graphics_xml = value; }
  }


  /// <summary>
  /// Gets sets the quantity of unit.  This is used for compatibility with
  /// the program unit definition.
  /// </summary>
  public int Quantity
  {
    get { return (m_quantity); }
    set { m_quantity = value; }
  }

  public ArrayList SubUnits
  {
    get { return (m_sub_units); }
    set { m_sub_units = value; }
  }

  public RiplynxSubUnit [] SubUnitsArray
  {
    get
    {
      RiplynxSubUnit [] arr_ru = new RiplynxSubUnit[m_sub_units.Count];
      for (int i = 0; i < m_sub_units.Count; i++)
      {
        arr_ru[i] = m_sub_units[i] as RiplynxSubUnit;
      }
      return (arr_ru);
    }
  }

  public static DataTable DefaultDataTable
  {
    get
    {
      DataTable dt = new DataTable(FieldString.PreviewTablename);
      dt.Rows.Add(dt.NewRow());
      return (dt);
    }
  }

  /// <summary>
  /// Transforms this unit into a primitive that still needs data conversion,
  /// base image, group image, etc...
  /// </summary>
  public IPrimitive ToBasePrimitive()
  {
    try
    {
      return (ToPrimitive(null));
    }
    catch (Exception e)
    {
      throw (new Exception("Error creating base primitive, unit: " + m_map, e));
    }
  }

  //
  // Transforms this unit into a primitive and then returns the first one in the list.
  // Most units will transform into just one primitive, especially ones without data that
  // could get expanded into lots of pages.
  //
  public IPrimitive ToPrimitive(IPrimitive image_primitive)
  {
    IPrimitive [] primitives = ToPrimitives(image_primitive);
    if (primitives != null && primitives.Length > 0)
    {
      Trace.WriteLine("RiplynxUnit.ToPrimitive: ToPrimitives returned 0 results, returning null");
      return ((IPrimitive) primitives[0]);
    }
    return (null);
  }

  //
  // transforms this unit into primitives without all the crazy data that can go into such a conversion
  //
  public IPrimitive [] ToPrimitives(IPrimitive image_primitive)
  {
    return (ToPrimitives(image_primitive, null, 0, null));
  }


  //
  // Transforms unit to primitive with some usefull options.
  //
  public IPrimitive [] ToPrimitives(IPrimitive image_primitive, RiplynxRecord record, int cell_spacing, Hashtable group_primitives)
  {
    DataRow dr = null;
    DataTable dt = null;
    Hashtable ht_dup = null;

    if (record != null)
    {
      dr = record.DataRow;
      ht_dup = record.DuplicatePrimitives;
      dt = record.DataTable;

      // replace duplicate fields
      foreach (Duplicate d in record.Duplicates)
      {
        if (d.Image == null) continue;

        if (dr.Table.Columns.Contains(d.FieldName))
        {
          dr[d.FieldName] = new FileInfo(d.Image.name).Name;
        }
      }
      if (dr.Table.Columns.Contains("Image Name"))
      {
        dr["Image Name"] = new FileInfo(record.Image.name).Name;
      }

    }
    else
    {
      dt = DefaultDataTable;
    }

    if (this.m_unit_type == RiplynxUnitType.Unit)
    {
      return (new IPrimitive [] {ToUnitPrimitive(image_primitive, dr, cell_spacing)});
    }

    if (m_unit_type == RiplynxUnitType.Service)
    {

      return (ToServicePrimitives(image_primitive, dt, group_primitives, ht_dup));
    }

    return (new IPrimitive[] {});
  }



  public static Service LoadUnitService(string serviceFilename)
  {
    string iniFilename;
    string setupsDir = ServiceSettings.Instance.SetupsDir;

    // Full path, check first for setup file in the setups directory
    if (Path.IsPathRooted(serviceFilename))
    {
      string baseFilename = new FileInfo(serviceFilename).Name;
      if (File.Exists(serviceFilename))
      {
        iniFilename = serviceFilename;
      }
      else
      {
        iniFilename = Path.Combine(setupsDir, baseFilename);
      }
    }
    else
    {
      iniFilename = Path.Combine(setupsDir, serviceFilename);
    }

    if (!File.Exists(iniFilename))
    {
      MessageBox.Show("Error, Service not found: " + serviceFilename);
      return new Service();
    }

    return Service.FromIniFile(iniFilename);

  }

  //
  // Transforms unit to a primitive using the service information.
  //
  protected IPrimitive [] ToServicePrimitives(IPrimitive image_primitive, DataTable dt, Hashtable group_primitives, Hashtable duplicate_primitives)
  {
    Trace.WriteLine("RiplynxUnit.ToServicePrimitives, service name: " + m_service_name);
    _service = LoadUnitService(m_service_name);
    _service.ServiceDataInfo = new ServiceDataInfo(image_primitive, group_primitives, duplicate_primitives, dt);
    //service.ImagePrimitive = image_primitive;
    //service.GroupPrimitives = group_primitives;
    //service.DuplicatePrimitives = duplicate_primitives;
    IPrimitive [] primitives = _service.ToPrimitives(dt);
    Trace.WriteLine("RiplynxUnit.ToServicePrimitives service returned " + primitives.Length + " primitives");

    for (int i = 0; i < primitives.Length; i++)
    {
      primitives[i] = AddUnitGraphic(primitives[i], dt);

      // add rotation
      if (ServiceRotation != 0)
      {
        PrimitiveCollection col = new PrimitiveCollection();
        col.Primitives.Add(primitives[i]);
        col.Primitives.Add(new ImageEffects(new RotateCommandWrapper(ServiceRotation)));
        primitives[i] = col;
      }
    }



    return (primitives);
  }

  //
  // Transforms a normal unit to a primitive.
  //
  protected IPrimitive ToUnitPrimitive(IPrimitive image_primitive, DataRow row, int cell_spacing)
  {
    PrimitiveCollection unit_graphics = null;
    if (GraphicsXml != null && GraphicsXml != "" && UseGraphics)
    {
      unit_graphics = (PrimitiveCollection) ObjectSerializer.DeserializeString(GraphicsXml, typeof(PrimitiveCollection));
      unit_graphics = (PrimitiveCollection) new PrimitiveConversionData(row).ConvertData(unit_graphics);
    }

    PrimitiveCollection col = new PrimitiveCollection();

    col.Add(new Blank((float) m_x, (float) m_y));

    foreach (RiplynxSubUnit subunit in m_sub_units)
    {
      col.Add(subunit.ToPrimitive(image_primitive, unit_graphics, cell_spacing, row));
    }

    AddUnitGraphic(col, row);

    return (col);
  }

  //
  // Adds the unit graphics to the primitive if it is enabled.
  //
  protected IPrimitive AddUnitGraphic(IPrimitive primitive, DataTable dt)
  {
    DataRow row = null;
    if (dt != null && dt.Rows.Count != 0)
    {
      row = dt.Rows[0];
    }
    return (AddUnitGraphic(primitive, row));
  }


  //
  // Adds the unit graphics to the primitive if it is enabled.
  //
  protected IPrimitive AddUnitGraphic(IPrimitive primitive, DataRow row)
  {
    if (UnitGraphicsXml == null || UnitGraphicsXml == "" || ! UseUnitGraphics)
    {
      return (primitive);
    }

    PrimitiveCollection col;
    if (primitive is PrimitiveCollection)
    {
      col = primitive as PrimitiveCollection;
    }
    else
    {
      col = new PrimitiveCollection();
      col.Add(primitive);
    }

    PrimitiveCollection unit_graphics;
    unit_graphics = (PrimitiveCollection) ObjectSerializer.DeserializeString(UnitGraphicsXml, typeof(PrimitiveCollection));
    unit_graphics = (PrimitiveCollection) new PrimitiveConversionData(row).ConvertData(unit_graphics);
    col.Add(unit_graphics);

    return (col);
  }

  //
  // Returns a package unit using the current unit information.
  //
  public PackageUnit GetPackageUnit()
  {
    PackageUnit pu = new PackageUnit();
    foreach (RiplynxSubUnit rsu in SubUnits)
    {
      pu.SubUnits.Add(rsu.GetPackageSubUnit());
    }
    pu.PageWidth = (float) X;
    pu.PageHeight = (float) Y;
    pu.RenderSource = true;

    return (pu);
  }

  //
  // Creates unit from an xml node.
  //
  public static RiplynxUnit FromXmlNode(XmlNode node)
  {
    RiplynxUnit ru = new RiplynxUnit();

    foreach (XmlNode child_node in node.ChildNodes)
    {
      switch (child_node.Name)
      {
        case "SubUnit":
          ru.SubUnits.Add(RiplynxSubUnit.FromXmlNode(child_node));
          break;
        case "Description":
          ru.Description = child_node.InnerText;
          break;
        case "Map":
          ru.Map = child_node.InnerText;
          break;
        case "X":
          if (child_node.InnerText != "" && child_node.InnerText != null)
          {
            ru.X = Double.Parse(child_node.InnerText);
          }
          break;
        case "Y":
          if (child_node.InnerText != "" && child_node.InnerText != null)
          {
            ru.Y = Double.Parse(child_node.InnerText);
          }
          break;
        case "ShortDescription":
          ru.ShortDescription = child_node.InnerText;
          break;
        case "UnitType":
          ru.UnitType = Int32.Parse(child_node.InnerText);
          break;
        case "ServiceName":
          ru.ServiceName = child_node.InnerText;
          break;
        case "ServiceRotation":
          try
          {
            ru.ServiceRotation = Int32.Parse(child_node.InnerText);
          } catch (Exception) {}
          break;
        case "UseTextOverlay":
          ru.UseTextOverlay = Boolean.Parse(child_node.InnerText);
          break;
        case "TextOverlayXml":
          ru.TextOverlayXml = child_node.InnerText;
          break;
        case "UseGraphics":
          ru.UseGraphics = Boolean.Parse(child_node.InnerText);
          break;
        case "GraphicsXml":
          ru.GraphicsXml = child_node.InnerText;
          break;
        case "UseUnitGraphics":
          ru.UseUnitGraphics = Boolean.Parse(child_node.InnerText);
          break;
        case "UnitGraphicsXml":
          ru.UnitGraphicsXml = child_node.InnerText;
          break;
        case "Quantity":
          ru.Quantity = Int32.Parse(child_node.InnerText);
          break;
        case "Title":
        case "Code":
          break;
        default:
          Console.WriteLine("Unhandled RiplynxUnit node: " + child_node.Name);
          break;

      }

    }

    return (ru);
  }
}

  [TestFixture]
  public class GreenScreenRegionShiftTest
  {
    [Test]
    public void Main()
    {
      AbstractPrimitiveRenderer.PrimitiveRendering += new PrimitiveRenderingEventHandler(AbstractPrimitiveRenderer_PrimitiveRendering);

      string pud_filename = @"C:\Program Files\PhotoLynx\RipLynx!\RipLynx Units.PUD";
      RiplynxPud pud = RiplynxPud.FromXmlFile(pud_filename);

      RiplynxUnit unit = pud.Units.FindUnit("AM");

      GreenScreenPrimitive gs = new GreenScreenPrimitive();
      gs.BackgroundColor = GreenScreenPrimitive.BackgroundColors.Blue;
      gs.MinBrightness = 140;
      gs.GreenRatio = 1.2;
      gs.GreenRatioFunction = GreenRatioFunction.Max;

      BackgroundImage bi = new BackgroundImage(@"..\test_data\gs_region_shift\1262bg.jpg");

      RiplynxImage ri = new RiplynxImage();
      ri.name = @"C:\Program Files\PhotoLynx\ImageMatch 2000\test data and images for imagematch\Middle School Images\1\000001.jpg";

      AbstractPrimitiveRenderer.SaveFile(new PrimitiveConversionData(300).ConvertData(unit.ToPrimitive(ri.ToPrimitive())), @"..\test_results\gs_region_shift\original.jpg");

      ri.adjustments_xml = ObjectSerializer.SafeSerializeString(new PrimitiveCollection(gs, bi));
      AbstractPrimitiveRenderer.SaveFile(new PrimitiveConversionData(300).ConvertData(unit.ToPrimitive(ri.ToPrimitive())), @"..\test_results\gs_region_shift\gs_1.jpg");

      //gs.BackgroundColor = GreenScreenPrimitive.BackgroundColors.None;
      gs.DropRgns.Add(new RectangleF(20, 80, 10, 10));
      ri.adjustments_xml = ObjectSerializer.SafeSerializeString(new PrimitiveCollection(gs, bi));
      //AbstractPrimitiveRenderer.SaveFile(new PrimitiveConversionData(300).ConvertData(unit.ToPrimitive(ri.ToPrimitive())), @"..\test_results\gs_region_shift\gs_2.jpg");

      ri.crop_ratio = 0.8;
      ri.crop_left = 8.4;
      ri.crop_top = 5.1;
      ri.crop_height = 65.3;
      ri.adjustments_xml = ObjectSerializer.SafeSerializeString(new PrimitiveCollection(gs, bi));
      AbstractPrimitiveRenderer.SaveFile(new PrimitiveConversionData(300).ConvertData(unit.ToPrimitive(ri.ToPrimitive())), @"..\test_results\gs_region_shift\gs_3.jpg");

      ImageFile image_file = new ImageFile(@"C:\Program Files\PhotoLynx\ImageMatch 2000\test data and images for imagematch\Middle School Images\1\000001.jpg");
      Crop crop = new Crop();
      crop.CropRect = new RectangleF(8.4f, 5.1f, 10f, 65.3f);
      crop.RatioForceWidth = 0.8;
      PrimitiveCollection col = new PrimitiveCollection(image_file, crop, gs, bi);
      AbstractPrimitiveRenderer.SaveFile(new PrimitiveConversionData(300).ConvertData(col), @"..\test_results\gs_region_shift\gs_4.jpg");

    }

    float m_last_ratio = -1;
    void AbstractPrimitiveRenderer_PrimitiveRendering(object sender, PrimitiveRenderingEventArgs args)
    {
      if (!(args.Primitive is GreenScreenPrimitive)) return ;
      GreenScreenPrimitive gs = args.Primitive as GreenScreenPrimitive;
      
      foreach (RectangleF drop_rect in gs.DropRgns)
      {
        Rectangle drop_pixel_rect = RectangleUtils.GetPixelRect(drop_rect, args.Image.Size);
        float ratio = (float)drop_pixel_rect.Width / drop_pixel_rect.Height;
        Trace.WriteLine("\nDROP REGION: " + drop_pixel_rect + ", RATIO: " + ratio + ", LAST: " + m_last_ratio);

        // the green screen is rendered twice, the ratio's should be similar
        if (m_last_ratio > 0)
        {
          float diff = m_last_ratio - ratio;
          Assert.IsTrue(diff < 0.1 && diff > -0.1);
        }
        m_last_ratio = ratio;
      }
    }
  }
}
