/* ----------------------------------------------------------------------------
 * $Id: GroupRecord.cs 2174 2004-10-22 00:56:34Z ajackson $
 *
 * --------------------------------------------------------------------------*/

using System.Xml;
using PhotoLynx.ImageRender.Primitives;

namespace PhotoLynx.ImageRender.RiplynxInterop
{
// Kepps track of information about group images.  They are just a normal
// adjusted image with a name and no data attached.
public class GroupRecord
{
  public string Name = "";
  public RiplynxImage Image = null;

  public GroupRecord() {}

  public static GroupRecord FromXmlNode(XmlNode node)
  {
    GroupRecord gr = new GroupRecord();

    foreach (XmlNode child_node in node.ChildNodes)
    {
      if (child_node.Name == "image")
      {
        gr.Image = RiplynxImage.FromXmlNode(child_node);
      }
      else if (child_node.Name == "GroupName")
      {
        gr.Name = child_node.InnerText;
      }
    }

    return (gr);
  }

  public IPrimitive ToPrimitive()
  {
    if (Image == null)
    {
      return (null);
    }

    return (Image.ToPrimitive());
  }
}
}
