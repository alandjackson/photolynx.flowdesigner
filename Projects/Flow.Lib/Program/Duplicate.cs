/* ----------------------------------------------------------------------------
 * $Id: Duplicate.cs 2684 2007-02-28 06:02:46Z ajackson $
 *
 * --------------------------------------------------------------------------*/

using System;
using System.Diagnostics;
using System.Xml;
using PhotoLynx.ImageRender.Primitives;

namespace PhotoLynx.ImageRender.RiplynxInterop
{
// duplicate image for the same record
public class Duplicate
{
  RiplynxImage m_image;
  string m_fieldname;

  public Duplicate()
  {
  }

  public RiplynxImage Image
  {
    get { return(m_image); }
    set { m_image = value; }
  }

  public string FieldName
  {
    get { return (m_fieldname); }
    set { m_fieldname = value; }
  }

  public static Duplicate FromXmlNode(XmlNode node)
  {
    Duplicate d = new Duplicate();

    foreach (XmlNode child_node in node.ChildNodes)
    {
      if (child_node.Name == "image" || child_node.Name == "RiplynxImage")
      {
        d.Image = RiplynxImage.FromXmlNode(child_node);
      }

      else if (child_node.Name == "field")
      {
        d.FieldName = child_node.InnerText;
      }
    }

    return (d);
  }
}
}
