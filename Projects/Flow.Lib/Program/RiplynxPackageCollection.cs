/* ----------------------------------------------------------------------------
 * $Id: RiplynxPackageCollection.cs 2174 2004-10-22 00:56:34Z ajackson $
 *
 * --------------------------------------------------------------------------*/

using System;
using System.IO;
using System.Xml;
using System.Collections;
using System.ComponentModel;

namespace PhotoLynx.ImageRender.RiplynxInterop
{
public class RiplynxPackageCollection
{
  ArrayList m_packages = new ArrayList();

  public RiplynxPackageCollection()
  {
  }

  public ArrayList Packages
  {
    get { return (m_packages); }
    set { m_packages = value; }
  }

  /// <summary>
  /// Returns a packages indexed by the map
  /// </summary>
  /// <param name="map"></param>
  /// <returns></returns>
  public RiplynxPackage FindPackage(string map)
  {
    foreach (RiplynxPackage p in m_packages)
    {
      if (p.Map == map)
      {
        return (p);
      }
    }
    return (null);
  }

  public static RiplynxPackageCollection FromXmlNode(XmlNode node)
  {

    return (new RiplynxPackageCollection());
  }

}

}
