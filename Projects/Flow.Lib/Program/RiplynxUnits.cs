/* ----------------------------------------------------------------------------
 * $Id: RiplynxUnits.cs 2770 2007-07-13 15:11:22Z ajackson $
 *
 * --------------------------------------------------------------------------*/

using System;
using System.IO;
using System.Xml;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics;
using PhotoLynx.Common.Core;

namespace PhotoLynx.ImageRender.RiplynxInterop
{
[TypeConverter(typeof(ExpandableObjectConverter))]
public class RiplynxUnits
{
  ArrayList m_units = new ArrayList();
  Hashtable m_options = new Hashtable();

  public RiplynxUnits()
  {
  }

  public ArrayList UnitMapList
  {
    get
    {
      ArrayList maps = new ArrayList();
      foreach (RiplynxUnit ru in Units)
      {
        maps.Add(ru.Map);
      }
      return (maps);
    }
  }

  public ArrayList Units
  {
    get { return (m_units); }
    set { m_units = value; }
  }

  public RiplynxUnit [] UnitsArray
  {
    get
    {
      RiplynxUnit [] arr_ru = new RiplynxUnit[m_units.Count];
      for (int i = 0; i < m_units.Count; i++)
      {
        arr_ru[i] = m_units[i] as RiplynxUnit;
      }
      return (arr_ru);
    }
  }

  public Hashtable PrintOptions
  {
    get { return (m_options); }
    set { m_options = value; }
  }

  public string GetInvalidPrintOptions(string package_str)
  {
    string invalid_options = "";
    RiplynxOrderedPackageCollection col = RiplynxOrderedPackageCollection.FromString(package_str);
    foreach (RiplynxOrderedPackage p in col.OrderedPackages)
    {
      foreach (string o in p.Options)
      {
        if (! PrintOptions.ContainsKey(o))
        {
          if (invalid_options != "")
          {
            invalid_options += ", ";
          }
          invalid_options += o;
        }
      }
    }
    return (invalid_options);
  }

  /// <summary>
  /// Returns a unit indexed by the map name.  If map is not unique, returns
  /// the first instance.
  /// </summary>
  /// <param name="map"></param>
  /// <returns></returns>
  public RiplynxUnit FindUnit(string map)
  {
    foreach (RiplynxUnit unit in m_units)
    {
      if (unit.Map == map)
      {
        return (unit);
      }
    }
    return (null);
  }

  public static RiplynxUnits FromXmlNode(XmlNode node)
  {
    RiplynxUnits rus = new RiplynxUnits();

    foreach (XmlNode child_node in node.ChildNodes)
    {
      if (child_node.Name == "Unit")
      {
        RiplynxUnit ru = RiplynxUnit.FromXmlNode(child_node);

        rus.Units.Add(ru);
      }
      if (child_node.Name == "RiplynxOption")
      {
        RiplynxOption ro = ObjectSerializer.DeserializeString(child_node.OuterXml, typeof(RiplynxOption)) as RiplynxOption;
        if (! rus.PrintOptions.ContainsKey(ro.Map))
        {
          rus.PrintOptions.Add(ro.Map, ro);
        }
      }
    }

    return (rus);
  }

}

}
