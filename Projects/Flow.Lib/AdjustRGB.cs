﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Imaging;
using System.Windows;
using Flow.Interop.ImageRender.WIC;

namespace Flow.Interop.ImageRender
{
    public class AdjustRGB
    {
        int _r;
        int _g;
        int _b;

        public AdjustRGB(int r, int g, int b)
        {
            _r = r;
            _g = g;
            _b = b;
        }

        public void Apply(BitmapImage image)
        {
            WICBitmap wicBitmap = new WICBitmap(image);   // WICBitmap type
            try
            {
                using (WICBitmapLock bitmapLock = wicBitmap.Lock())
                {
                    WICBitmapBuffer bitmapBuffer = bitmapLock.Data;
                    unsafe
                    {
                        Int32* pStart = (Int32*)bitmapBuffer.Buffer.ToPointer();
                        Int32* pEnd = pStart + bitmapBuffer.Size / sizeof(Int32);
                        for (; pStart != pEnd; ++pStart)
                        {
                            //           RRGGBB
                            int pixelValue = *pStart;
                            int red = Math.Min(Math.Max(pixelValue & 0x0000FF + _r, 0), 255);
                            pixelValue >>= 8;
                            int green = Math.Min(Math.Max(pixelValue & 0x0000FF + _g, 0), 255);
                            pixelValue >>= 8;
                            int blue = Math.Min(Math.Max(pixelValue & 0x0000FF + _b, 0), 255);
                            *pStart = (red << 16) + (green << 8) + (blue);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failed to manipulate image:\n" + ex.Message);
            }
        }
    }
}
