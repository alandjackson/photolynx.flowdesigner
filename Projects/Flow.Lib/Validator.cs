﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Flow.Lib
{
    public class Validator
    {
        StringBuilder Exceptions { get; set; }

        public Validator()
        {
            Exceptions = new StringBuilder();
        }

        public Validator DirectoryExists(string dirname)
        {
            if (!Directory.Exists(dirname))
                Exceptions.AppendLine("Directory " + dirname + " does not exist");
            return this;
        }

        public void Validate()
        {
            string exceptionStr = Exceptions.ToString();
            if (exceptionStr.Length > 0)
                throw new Exception(exceptionStr);
        }
    }
}
