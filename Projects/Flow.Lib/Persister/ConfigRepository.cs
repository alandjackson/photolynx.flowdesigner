﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Flow.Lib.Helpers;
using Flow.Lib.Persister;

namespace Flow.Lib
{
    public class ConfigRepository
    {
        public IConfigDataPersister DataPersister { get; set; }

        public ConfigRepository(IConfigDataPersister p = null)
        {
            DataPersister = p ?? new XmlAppDataPersister();
        }

        public virtual T Load<T>() where T : class, new()
        {
            return DataPersister.Load<T>();   
        }

        public void Save<T>(T data)
        {
            DataPersister.Save<T>(data);
        }
    }
}
