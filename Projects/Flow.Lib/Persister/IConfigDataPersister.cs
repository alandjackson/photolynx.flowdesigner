﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flow.Lib.Persister
{
    public interface IConfigDataPersister
    {
        T Load<T>() where T : class, new();
        void Save<T>(T data);
    }
}
