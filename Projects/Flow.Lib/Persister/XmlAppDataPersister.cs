﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Flow.Lib.Helpers;

namespace Flow.Lib.Persister
{
    public class XmlAppDataPersister : IConfigDataPersister
    {
        public T Load<T>() where T : class, new()
        {
            string filename = GetConfigFilename<T>();
            if (!File.Exists(filename))
                return new T();
            return ObjectSerializer.FromXmlFile<T>(filename);
        }

        public void Save<T>(T data)
        {
            string filename = GetConfigFilename<T>();
            if (File.Exists(filename))
                File.Delete(filename);
            ObjectSerializer.ToXmlFile(data, filename);
        }

        protected string GetConfigFilename<T>()
        {
            return Path.Combine(GetValidConfigDirectory(), typeof(T).FullName + ".xml");
        }

        protected string GetValidConfigDirectory()
        {
			/// TPM 2009-03-18 : Changed to reflect CommonApplicationFolder directory
			//string appData =
			//    Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
			//string flowData = IOUtil.CombinePaths(appData, "Flow", "Config");

			string flowData = FlowContext.FlowConfigDirPath;

            if (!Directory.Exists(flowData))
                IOUtil.CreateDirectory(flowData, true);

            return flowData;
        }
    }

}
