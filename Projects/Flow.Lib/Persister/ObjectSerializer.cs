﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows;
using System.Xml;
using System.Xml.Serialization;
using System.Text;

namespace Flow.Lib.Persister
{

    /// <summary>
    /// Wrapper function for serializing and deserializing objects
    /// to xml as files or strings
    /// </summary>
    public class ObjectSerializer
    {
        /// <summary>
        /// Serializes object to an xml string.  Return an empty string on error
        /// so this function will never throw an exception.
        /// </summary>
        public static string SafeSerializeString(object o)
        {
            try
            {
                return (SerializeString(o));
            }
            catch (Exception e)
            {
                Trace.WriteLine("Error serializing object: " + e.ToString());
                return ("");
            }
        }

        /// <summary>
        /// Serializes object to an xml string with default option values.
        /// </summary>
        public static string SerializeString(object o)
        {
            return (SerializeString(o, o.GetType(), true));
        }

        /// <summary>
        /// Serializes object to an xml string
        /// </summary>
        public static string SerializeString(object o, bool strip_xml_header)
        {
            return (SerializeString(o, o.GetType(), strip_xml_header));
        }

        public static string ToXmlString(object item)
        {
            XmlSerializer serializer = new XmlSerializer(item.GetType());

            using (MemoryStream ms = new MemoryStream())
            {
                using (XmlTextWriter tw = new XmlTextWriter(ms, Encoding.UTF8))
                {
                    tw.Formatting = Formatting.Indented;
                    serializer.Serialize(tw, item);
                    return UTF8Encoding.UTF8.GetString(ms.ToArray());
                }
            }
        }

        public static void ToXmlFile(object item, string filename)
        {
            XmlSerializer serializer = new XmlSerializer(item.GetType());

            using (FileStream fs = File.OpenWrite(filename))
            {
                using (XmlTextWriter tw = new XmlTextWriter(fs, Encoding.UTF8))
                {
                    tw.Formatting = Formatting.Indented;
                    serializer.Serialize(tw, item);
                }
            }
        }

        public static T FromXmlString<T>(string xmlStr)
        {
            object o;
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            using (MemoryStream ms = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(xmlStr)))
                o = serializer.Deserialize(ms);
            return (T)o;

        }

        public static T FromXmlFile<T>(string filename)
        {
            object o;

            XmlSerializer serializer = new XmlSerializer(typeof(T));
            using (StreamReader sr = File.OpenText(filename))
                o = serializer.Deserialize(sr);
            return (T)o;
        }

        public static T FromXmlStringOrDefault<T>(string xmlStr)
        {
            try
            {
                return FromXmlString<T>(xmlStr);
            }
            catch (Exception)
            {
                return default(T);
            }
        }

        /// <summary>
        /// Serializes object to an xml string
        /// </summary>
        /// <param name="o"></param>
        /// <param name="type"></param>
        /// <param name="strip_xml_header"></param>
        /// <returns></returns>
        public static string SerializeString(object o, System.Type type, bool strip_xml_header)
        {

            string str = "";

            // create new serializer
            XmlSerializer serializer = new XmlSerializer(type);

            // serialize object to a file
            MemoryStream ms = new MemoryStream();

            // create xml text writer
            XmlTextWriter mem_writer = new XmlTextWriter(ms, System.Text.Encoding.UTF8);
            mem_writer.Formatting = Formatting.Indented;

            // get the string of xml data
            serializer.Serialize(mem_writer, o);
            //str = System.Text.ASCIIEncoding.ASCII.GetString(ms.ToArray(), 0, (int)ms.Length);
            str = System.Text.UTF8Encoding.UTF8.GetString(ms.ToArray(), 0, (int)ms.Length);

            // strip out xml header
            if (strip_xml_header)
            {
                str = (new Regex(@"^.*\<\?xml.*?\?\>")).Replace(str, "");
            }

            // close the writer
            mem_writer.Close();

            return (str);

        }

        /// <summary>
        /// Serializes object to xml and writes the xml to a file
        /// </summary>
        public static bool SerializeFile(string filename, object o)
        {
            return (SerializeFile(filename, o, o.GetType()));
        }

        /// <summary>
        /// Serializes object to xml and writes the xml to a file
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="o"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static bool SerializeFile(string filename, object o, System.Type type)
        {
            //Console.WriteLine("Serializing object of type" + type.ToString() + " to file: " + filename);

            // create new serializer
            XmlSerializer serializer = new XmlSerializer(type);

            // serialize object to a file
            FileStream fs = new FileStream(filename, FileMode.Create, FileAccess.Write);
            MemoryStream ms = new MemoryStream();

            // create xml text writer
            XmlTextWriter writer = new XmlTextWriter(fs, System.Text.Encoding.UTF8);

            // write out the xml data
            serializer.Serialize(writer, o);

            // close the writer
            writer.Close();

            return (true);
        }

        /// <summary>
        /// Deserialized objects from a stream.
        /// </summary>
        /// <param name="stream">Incoming xml stream.</param>
        /// <param name="type">Type to deserialize.</param>
        /// <returns></returns>
        public static object DeserializeStream(Stream stream, System.Type type)
        {
            object o;

            XmlSerializer serializer = new XmlSerializer(type);
            XmlReader reader = new XmlTextReader(stream);

            o = (object)serializer.Deserialize(reader);
            reader.Close();
            return (o);

        }

        /// <summary>
        /// Deserializes objects from a string
        /// </summary>
        /// <param name="str"></param>
        /// <param name="type"></param>
        /// <returns>Returns deserialized object or null if deserialization is not successful</returns>
        public static object SafeDeserializeString(string str, System.Type type)
        {
            try
            {
                return (DeserializeString(str, type));
            }
            catch (Exception e)
            {
                Trace.WriteLine("Unable to deserialize string: " + str + ": " + e.ToString());
                return (null);
            }
        }

        //public static void DeserializeStringGeneric<T>(string str)
        //{
        //  object o = DeserializeString(str, typeof(T));
        //  //return (o as typeof(T));
        //}


        /// <summary>
        /// Deserializes objects from a string
        /// </summary>
        /// <param name="str"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static object DeserializeString(string str, System.Type type)
        {
            object o;

            // set up deserialization stream
            XmlSerializer serializer = new XmlSerializer(type);
            MemoryStream ms = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(str));

            // deserizlize stream
            try
            {
                o = DeserializeStream(ms, type);
            }
            catch (Exception e)
            {
                throw (e);
            }
            finally
            {
                ms.Close();
            }
            return (o);
        }

        public static object SafeDeserializeFile(string filename, System.Type type)
        {
            try
            {
                return (DeserializeFile(filename, type));
            }
            catch
            {
                return (null);
            }
        }

        /// <summary>
        /// Deserializes objects from a file
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static object DeserializeFile(string filename, System.Type type)
        {
            object o;
            XmlSerializer serializer = new XmlSerializer(type);
            
            FileStream fs = new FileStream(filename, FileMode.Open);

            // deserizlize stream
            try
            {
                o = DeserializeStream(fs, type);
            }
            catch (Exception e)
            {
                MessageBox.Show("Exception deserializing file: " + e.Message);
                throw (e);
            }
            finally
            {
                //MessageBox.Show("Closing file stream");
                fs.Close();
            }
            return (o);
        }


    }
}

