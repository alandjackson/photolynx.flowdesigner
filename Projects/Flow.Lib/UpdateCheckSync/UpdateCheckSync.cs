﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.Lib.Web;
using Flow.Lib.Persister;

namespace Flow.Lib.UpdateCheckSync
{
    public class UpdateCheckSync
    { 
        PostSubmitter PostSubmitter { get; set; }

        public UpdateCheckSync()
            : this(new PostSubmitter())
        {
        }

        public UpdateCheckSync(PostSubmitter postsubmitter)
        {
            PostSubmitter = postsubmitter;
        }

        public UpdateCheckStore Sync(UpdateCheckStore src)
        {
            WebServiceUpdateCheckMessage sendMsg = new WebServiceUpdateCheckMessage()
            {
                UpdateCheckStore = src
            };

            //PostSubmitter.Url = @"http://localhost:64844/updatecheck";
            PostSubmitter.Url = @"http://www.flowadmin.com/activationservice/updatecheck";
            PostSubmitter.Type = PostSubmitter.PostTypeEnum.Post;
            PostSubmitter.PostItems.Add("WebServiceUpdateCheckMessage",
                ObjectSerializer.ToXmlString(
                new WebServiceUpdateCheckMessage() { UpdateCheckStore = src }));
            string resultMsgStr = PostSubmitter.Post();
            //MessageBox.Show(resultMsgStr);

            if (resultMsgStr == null)
                return null;

            WebServiceUpdateCheckMessage resultMsg =
                ObjectSerializer.FromXmlString<WebServiceUpdateCheckMessage>(
                resultMsgStr);


            return resultMsg.UpdateCheckStore;
        }

    }

    public class WebServiceUpdateCheckMessage
    {
        public UpdateCheckStore UpdateCheckStore { get; set; }
        public string ErrorMsg { get; set; }
    }

    
}
