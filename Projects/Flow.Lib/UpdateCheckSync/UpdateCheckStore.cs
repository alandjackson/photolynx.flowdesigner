﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Data;

namespace Flow.Lib.UpdateCheckSync
{
    public class UpdateCheckStore
    {
        public List<XmlDocument> xmlFiles { get; set; }
        public string ComputerId { get; set; }
        public String ActivationKey { get; set; }
        public DateTime ActivationKeyExpireDate { get; set; }
        public Boolean ActivationKeyIsExpired { get; set; }
        public List<string[]> clientProjectTemplates { get; set; }
        public DataSet OrderFormData { get; set; }
        public String Message { get; set; }
        public DataSet ConfigData { get; set; }
        public List<DataSet> OrderForms { get; set; }
        public Boolean CanGreenScreen { get; set; }
        public String ClientFlowVersion { get; set; }
        public String ClientFlowType { get; set; }
        public String NewFlowInstaller { get; set; }
        public Boolean IsServerKey { get; set; }
        public Boolean IsMasterKey { get; set; }
        public string FingerPrint { get; set; }
        public Boolean IsBasic { get; set; }
        public string EcommereAPIUrl { get; set; }
        public string EcommereAPIKey { get; set; }
        public Boolean DisableRemoteImageService { get; set; }


        private List<RemoteImageService> _remoteImageServices { get; set; }
        public List<RemoteImageService> RemoteImageServices
        {
            get { return _remoteImageServices; }
            set
            {
                _remoteImageServices = value;
            }
        }

    }

    public class RemoteImageService
    {
        public string Name { get; set; }
        public string Ftp { get; set; }
        public string Port { get; set; }

        public RemoteImageService()
        {
        }
        public RemoteImageService(string n, string f, string p)
        {
            Name = n;
            Ftp = f;
            Port = p;
        }
    }
}
