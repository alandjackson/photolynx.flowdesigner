﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Imaging;
using System.Windows;
using Flow.Interop.ImageRender.WIC;
using System.Threading;
using System.Windows.Threading;

namespace Flow.Interop.ImageRender
{
    public class AdjustImage
    {
        public string ImagePath { get; set; }
        public double R { get; set; }
        public double G { get; set; }
        public double B { get; set; }
        public int Brightness { get; set; } //-255 ... 255
        public double Contrast { get; set; }//-1 ... inf(maybe 5)

        public AdjustImage() { }

        public AdjustImage( double rScale, double gScale, double bScale, int brightnessOffset, double contrastScale,
            string imagePath)
        {
            ImagePath = imagePath;
            R = rScale/ 100 + 1;
            G = gScale  / 100 + 1;
            B = bScale / 100 + 1;
            Brightness = brightnessOffset;
            Contrast = contrastScale + 1;
        }

        public void Apply(BitmapImage image)
        {
            try
            {
                WICBitmap wicBitmap = new WICBitmap(image);   // WICBitmap typ
                WICBitmapLock bitmapLock = wicBitmap.Lock();

                WICBitmapBuffer bitmapBuffer = bitmapLock.Data;
                unsafe
                {
                    Int32* pStart = (Int32*)bitmapBuffer.Buffer.ToPointer();
                    Int32* pEnd = pStart + bitmapBuffer.Size / sizeof(Int32);
                    for (; pStart != pEnd; ++pStart)
                    {
                        //           RRGGBB
                        int pixelColor = *pStart;
                        int blue = Math.Min(Math.Max((int)(((pixelColor & 0x0000FF) * B + Brightness) * Contrast), 0), 255);
                        pixelColor >>= 8;
                        int green = Math.Min(Math.Max((int)(((pixelColor & 0x0000FF) * G + Brightness) * Contrast), 0), 255);
                        pixelColor >>= 8;
                        int red = Math.Min(Math.Max((int)(((pixelColor & 0x0000FF) * R + Brightness) * Contrast), 0), 255);

                        *pStart = (red << 16) + (green << 8) + blue;
                    }
                }
                bitmapLock.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Could not apply RGB adjustments.\n" + ex.Message);
            }
            
        }

    }
}
