﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace Flow.Lib.Linq
{
	public static class PredicateBuilder
	{
		public static Expression<Func<T, bool>> Make<T>() { return null; }

		public static Expression<Func<T, bool>> Make<T>(this Expression<Func<T, bool>> predicate)
		{
			return predicate;
		}

		/// <summary>
		/// Prepares an "empty" expression to allow for adding condition chains;
		/// for AND (exclusive) queries, isExclusive = true;
		/// for OR (inclusive) queries, isExclusive = false
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="isExclusive"></param>
		/// <returns></returns>
		public static Expression<Func<T, bool>> Prepare<T>(bool isExclusive)
		{
			return (x) => isExclusive;
		}

		public static Expression<Func<T, bool>> Or<T>(this Expression<Func<T, bool>> expr, Expression<Func<T, bool>> or)
		{
			if (expr == null) return or;
			var invokedExpr = Expression.Invoke(or, expr.Parameters.Cast<Expression>());
			return Expression.Lambda<Func<T, bool>>(Expression.Or(expr.Body, invokedExpr), expr.Parameters);
		}

		public static Expression<Func<T, bool>> And<T>(this Expression<Func<T, bool>> expr, Expression<Func<T, bool>> and)
		{
			if (expr == null) return and;
			var invokedExpr = Expression.Invoke(and, expr.Parameters.Cast<Expression>());
			return Expression.Lambda<Func<T, bool>>(Expression.And(expr.Body, invokedExpr), expr.Parameters);
		}
	}
}