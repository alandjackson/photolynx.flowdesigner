﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Flow.Lib.Helpers;

namespace Flow.Lib.Log
{
    /// <summary>
    /// Basic implementation of a logging framework
    /// </summary>
    public class FlowLog
    {
        protected static FlowLog Instance = new FlowLog();

        protected string LogDir;
        protected static readonly string ERROR_LOG_FILE = "Errors.txt";
        protected static readonly string OUPUT_LOG_FILE = "Out.txt";

        protected FlowLog()
        {
            string appData = System.Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);
            LogDir = IOUtil.CombinePaths(appData, "Flow", "Logs");

            ValidateLogLocations();
        }

        protected void ValidateLogLocations()
        {
            if (!Directory.Exists(LogDir))
                Directory.CreateDirectory(LogDir);

        }

        public void _LogError(string msg)
        {
            WriteToFile(ERROR_LOG_FILE, msg);
        }

        public void _Log(string msg)
        {
            WriteToFile(OUPUT_LOG_FILE, msg);
        }

        protected void WriteToFile(string filename, string msg)
        {
            using (StreamWriter sw = File.AppendText(Path.Combine(LogDir, filename)))
                sw.WriteLine(DateTime.Now.ToString() + ": " + msg);
        }

        public static void LogError(string msg)
        {
            Instance._LogError(msg);
        }

        public static void Log(string msg)
        {
            Instance._Log(msg);
        }

    }
}
