﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mime;
using System.Net.Mail;
using StructureMap;
using Flow.Lib.Exceptions;
using System.Net.NetworkInformation;
using System.Net;

namespace Flow.Lib.Net
{
    public class Emailer
    {
        string TO_EMAIL = "error@flowadmin.com";
        string FROM_EMAIL = "error@flowadmin.com";
        //const string SMTP_SERVER = "smtpout.secureserver.net";
        const string SMTP_SERVER = "mail.flowadmin.com";
        const string SMTP_USER = "flow@flowadmin.com";
        //const string SMTP_PASSWORD = "pawprint$";
        const string SMTP_PASSWORD = "pl$flowadmin";
        const int SMTP_PORT = 3535;

        MailMessage _mail;

        
        public Emailer(string subject, string body) : this(subject, body, true, null, null, null){}

        public Emailer(string subject, string body, bool showProgress) : this(subject, body, showProgress, null, null, null) { }

        public Emailer(string subject, string body, bool showProgress, string to, string from, string cc)
        {
            if (to != null && to.Length > 1)
                TO_EMAIL = to;

            if (from != null && from.Length > 1)
            {
                FROM_EMAIL = from;
            }

            FROM_EMAIL = FROM_EMAIL.Replace(';', ',').Replace(':', ',');
            TO_EMAIL = TO_EMAIL.Replace(';', ',').Replace(':', ',');

            _mail = new MailMessage(FROM_EMAIL, TO_EMAIL, subject, body);

            if (cc != null && cc.Length > 1)
            {
                cc = cc.Replace(';', ',').Replace(':', ',');
                _mail.CC.Add(cc);
            }

            if (!TO_EMAIL.Contains("flowadmin.com"))
                _mail.Bcc.Add("bcc@flowadmin.com");

            if (showProgress)
            {
                ProgressInfo = new NotificationProgressInfo("Sending Error Report", "Sending email...");
                IShowsNotificationProgress progressNotification = ObjectFactory.GetInstance<IShowsNotificationProgress>();

                progressNotification.ShowNotification(ProgressInfo);
                ProgressInfo.NotificationProgress = progressNotification;
            }

        }

        public NotificationProgressInfo ProgressInfo { get; set; }

        public void AddAttachment(string file)
        {
            Attachment data = new Attachment(file, MediaTypeNames.Application.Octet);
            ContentDisposition disposition = data.ContentDisposition;
            disposition.CreationDate = System.IO.File.GetCreationTime(file);
            disposition.ModificationDate = System.IO.File.GetLastWriteTime(file);
            disposition.ReadDate = System.IO.File.GetLastAccessTime(file);
            _mail.Attachments.Add(data);
        }

        public void AsyncSend()
        {
            SmtpClient emailClient = InitClient();
            if (ProgressInfo != null)
            {
                ProgressInfo.Title = "Sending Email";
                ProgressInfo.Message = "Uploading...";
                ProgressInfo.Update();
            }
            emailClient.SendAsync(_mail, null);
        }

        public void Send()
        {
            SmtpClient emailClient = InitClient();
            emailClient.Send(_mail);
            emailClient.Dispose();
        }

        private SmtpClient InitClient()
        {
            SmtpClient emailClient = new SmtpClient(SMTP_SERVER);
            System.Net.NetworkCredential SMTPUserInfo = new System.Net.NetworkCredential(SMTP_USER, SMTP_PASSWORD);
            emailClient.UseDefaultCredentials = false;
            emailClient.Credentials = SMTPUserInfo;
            emailClient.Port = SMTP_PORT;

            emailClient.SendCompleted += new SendCompletedEventHandler(emailClient_SendCompleted);
            return emailClient;
        }

        void emailClient_SendCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            foreach (Attachment a in _mail.Attachments)
            {
                a.Dispose();
            }

            if (e.Error == null)
            {
                if (ProgressInfo != null)
                {
                    ProgressInfo.Complete("Email sent.");
                }
            }
            else
            {
                throw (new EmailException(this, e));
            }
        }

        public static bool CheckForInternetConnection()
        {
            try
            {
                using (var client = new WebClient())
                using (var stream = client.OpenRead("http://www.google.com"))
                {
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        public static bool PingServer(string pingTestURL)

        {
                Ping ping = new Ping();
                //ping www.google.com
                //PingReply pingStatus = ping.Send(IPAddress.Parse("208.69.34.231"));

                try
                {
                    PingReply pingStatus = ping.Send(pingTestURL);
                    if (pingStatus.Status == IPStatus.Success)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception)
                {

                    return false;
                }
        }

        public static bool ConnectionAvailable(string strServer)
        {
            try
            {
                HttpWebRequest reqFP = (HttpWebRequest)HttpWebRequest.Create(strServer);

                HttpWebResponse rspFP = (HttpWebResponse)reqFP.GetResponse();
                if (HttpStatusCode.OK == rspFP.StatusCode)
                {
                    // HTTP = 200 - Internet connection available, server online
                    rspFP.Close();
                    return true;
                }
                else
                {
                    // Other status - Server or connection not available
                    rspFP.Close();
                    return false;
                }
            }
            catch (WebException)
            {
                // Exception - connection not available
                return false;
            }
        }


        public void Close()
        {
            foreach (Attachment a in _mail.Attachments)
                a.Dispose();
        }
    }
}
