﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Rebex.Net;
using System.IO;
using System.Threading;
using StructureMap;
using System.Collections;
using System.Windows;
using Flow.Lib.Helpers;

namespace Flow.Lib.Net
{
    public class FileTransfer
    {

        private string _host;
        private int _port;
        private string _username;
        private string _password;
        private bool _useSftp;
        private NotificationProgressInfo _progressInfo;
        private long _allBytes = 0;
        private long _bytesSent = 0;
        private bool _abortTransfer = false;

        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        public long BytesSent
        {
            get
            {
                return _bytesSent;
            }
        }
        public event EventHandler<EventArgs<long>>
           SentBytesUpdated = delegate { };

        public Sftp sftpClient;
        public Ftp ftpClient;

        
        public bool autoCloseUploadDialog = false;
        public FileTransfer(string host, int port, string username, string password, bool useSftp, NotificationProgressInfo progressInfo)
        {
            _host = host;
            _port = port;
            _username = username;
            _password = password;
            _useSftp = useSftp;
            _progressInfo = progressInfo;

        }

        public bool SendFile(string localFile, string remoteFile)
        {
            string filename = (new FileInfo(localFile)).Name;
            long fsizeInB = (new FileInfo(localFile)).Length;
            long fsizeInMB = (fsizeInB / 1048576);
            logger.Info("Uploading file size (MB): " + fsizeInMB);
            if (_progressInfo != null)
            {
                _progressInfo = new NotificationProgressInfo("Uploading " + filename, "Sending File To Server");
                _progressInfo.Count = 100;
                _progressInfo.Index = 0;
                _progressInfo.NotificationProgress = ObjectFactory.GetInstance<IShowsNotificationProgress>();
                _progressInfo.AllowCancel = true;
                _progressInfo.Display(true);
            }
            
            _allBytes = new FileInfo(localFile).Length;

            if (_useSftp)
            {

                

               
                try
                {

                    if (fsizeInMB < 10)
                    {
                        if (sftpClient.FileExists(remoteFile))
                            sftpClient.DeleteFile(remoteFile);
                    }
                    
                    if (sftpClient == null || !(sftpClient.State == SftpState.Connected || sftpClient.State == SftpState.Ready))
                        Connect();

                    

                    string remoteDir = remoteFile.Replace("/" + (new FileInfo(remoteFile)).Name, "");
                    if(!sftpClient.DirectoryExists(remoteDir))
                            sftpClient.CreateDirectory(remoteDir);
                     //sftpClient.Timeout = 5000;
                     sftpClient.PutFile(localFile, remoteFile);
                    
                }
                catch (Exception ex)
                {
                    logger.Error("error uploading file: " + localFile);
                    logger.Error(ex.Message);

                    //if it is less than 10MB, fail
                    if (fsizeInMB < 10)
                    {
                        if (sftpClient.FileExists(remoteFile))
                            sftpClient.DeleteFile(remoteFile);
                        return false;
                    }

                    if (!ex.Message.ToLower().Contains("aborted"))
                    {
                        //if (!ex.Message.ToLower().Contains("timeout"))
                        //    throw ex;
                        do
                        {
                            Disconnect();
                            try
                            {
                                if (_progressInfo != null)
                                {
                                    _progressInfo.Update("Sending File To Server... re-connecting now... ");
                                }
                                Connect();
                                if (sftpClient.GetConnectionState().Connected)
                                {
                                    if (_progressInfo != null)
                                    {
                                        _progressInfo.Update("Sending File To Server... resuming upload");
                                    }
                                    Thread.Sleep(1000);
                                    long remoteOffset = sftpClient.GetFileLength(remoteFile);

                                    // open the local file for reading 
                                    Stream local = File.OpenRead(localFile);

                                    // resume the transfer if there is more data available 
                                    if (remoteOffset < local.Length)
                                    {
                                        // seek to the offset correspoding to the remote length 
                                        local.Seek(remoteOffset, SeekOrigin.Begin);

                                        // transfer data 
                                        try
                                        {
                                            sftpClient.PutFile(local, remoteFile, remoteOffset, -1);
                                        }
                                        catch (Exception exc)
                                        {
                                            sftpClient.Disconnect();
                                            throw exc;
                                        }
                                    }

                                    // close the local file 
                                    local.Close();
                                }
                            }
                            catch
                            {
                                if ((_progressInfo != null && _progressInfo.IsCanceled) || this._abortTransfer)
                                {
                                    if (_progressInfo != null) _progressInfo.Update("Upload Canceled by user");
                                    sftpClient.AbortTransfer();
                                    this._abortTransfer = false;
                                    break;
                                }

                                //do nothing
                                int x = 10;
                                while (x > 0)
                                {
                                    Thread.Sleep(1000);
                                    if (_progressInfo != null)
                                    {
                                        _progressInfo.Update("Sending File To Server... Connection problems, re-connecting in... " + x);
                                    }
                                    x--;
                                }
                                Thread.Sleep(500);
                            }
                        } while (sftpClient.State == SftpState.Disconnected || !(sftpClient.State == SftpState.Connected || sftpClient.State == SftpState.Ready));
                    }
                }
                if (_progressInfo != null && !_progressInfo.IsCanceled)
                {
                    _progressInfo.Progress = 100;
                    _progressInfo.Update("Finished Uploading " + filename);
                    if (autoCloseUploadDialog)
                        _progressInfo.Complete("Finished Uploading " + filename);
                    else
                        _progressInfo.ShowDoneButton();
                }
            }
            else
            {

               

                try
                {

                    if (fsizeInMB < 10)
                    {
                        if (ftpClient.FileExists(remoteFile))
                            ftpClient.DeleteFile(remoteFile);
                    }
                    
                    if (ftpClient == null || (ftpClient.State == FtpState.Disconnected && !(ftpClient.State == FtpState.Ready)))
                        Connect();
                    
                    ftpClient.Passive = true;

                    string remoteDir = remoteFile.Replace("/" + (new FileInfo(remoteFile)).Name, "");
                    if (!ftpClient.DirectoryExists(remoteDir))
                        ftpClient.CreateDirectory(remoteDir);
                    //ftpClient.Timeout = 5000;
                    ftpClient.PutFile(localFile, remoteFile);
                }
                catch (Exception ex)
                {
                    logger.Error("error uploading file: " + localFile);
                    logger.Error(ex.Message);

                    //if it is less than 10MB, fail
                    if (fsizeInMB < 10)
                    {
                        logger.Error("bailiing on put file attempt");
                        if(ftpClient.FileExists(remoteFile))
                            ftpClient.DeleteFile(remoteFile);
                        return false;
                    }

                    //if its more than 10MB, try and resume
                    if (!ex.Message.ToLower().Contains("aborted"))
                    {
                        //if (!ex.Message.ToLower().Contains("timeout"))
                        //    throw ex;
                        do
                        {

                            Disconnect();
                            try
                            {
                                if (_progressInfo != null)
                                {
                                    _progressInfo.Update("Sending File To Server... re-connecting now... ");
                                }
                                Connect();
                                if (ftpClient.GetConnectionState().Connected)
                                {
                                    if (_progressInfo != null)
                                    {
                                        _progressInfo.Update("Sending File To Server... resuming upload");
                                    }
                                    Thread.Sleep(1000);
                                    long remoteOffset = ftpClient.GetFileLength(remoteFile);

                                    // open the local file for reading 
                                    Stream local = File.OpenRead(localFile);

                                    // resume the transfer if there is more data available 
                                    if (remoteOffset < local.Length)
                                    {
                                        // seek to the offset correspoding to the remote length 
                                        local.Seek(remoteOffset, SeekOrigin.Begin);

                                        // transfer data 
                                        try
                                        {
                                            ftpClient.PutFile(local, remoteFile, remoteOffset, -1);
                                        }
                                        catch(Exception exc)
                                        {
                                            ftpClient.Disconnect();
                                            throw exc;
                                        }
                                    }

                                    // close the local file 
                                    local.Close();
                                }
                            }
                            catch
                            {
                                if ((_progressInfo != null && _progressInfo.IsCanceled) || this._abortTransfer)
                                {
                                    if (_progressInfo != null) _progressInfo.Update("Upload Canceled by user");
                                    ftpClient.Abort();
                                    this._abortTransfer = false;
                                    break;
                                }

                                //do nothing
                                int x = 10;
                                while (x > 0)
                                {
                                    Thread.Sleep(1000);
                                    if (_progressInfo != null)
                                    {
                                        _progressInfo.Update("Sending File To Server... Connection problems, re-connecting in... " + x);
                                    }
                                    x--;
                                }
                                Thread.Sleep(500);
                            }
                        } while (ftpClient.State == FtpState.Disconnected || !ftpClient.GetConnectionState().Connected);
                    }
                }
                if (_progressInfo != null && !_progressInfo.IsCanceled)
                {
                    _progressInfo.Progress = 100;
                    _progressInfo.Update("Finished Uploading " + filename);
                    if (autoCloseUploadDialog)
                        _progressInfo.Complete("Finished Uploading " + filename);
                    else
                        _progressInfo.ShowDoneButton();
                }
            }
            return true;
        }

        public void Connect()
        {
            if (_useSftp)
            {
                if(sftpClient != null && sftpClient.GetConnectionState().Connected)
                    return;
                if (sftpClient == null)
                    sftpClient = new Sftp();
                sftpClient.Connect(_host, _port);
                sftpClient.Login(_username, _password);
                sftpClient.Timeout = 30000;
                sftpClient.TransferProgress += new SftpTransferProgressEventHandler(client_TransferProgress);
            }
            else
            {
                if (ftpClient != null && ftpClient.GetConnectionState().Connected)
                    return;
                if (ftpClient == null)
                    ftpClient = new Ftp();
                ftpClient.Connect(_host, _port);
                ftpClient.Login(_username, _password);
                ftpClient.Timeout = 30000;
                ftpClient.TransferProgress += new FtpTransferProgressEventHandler(client_TransferProgress);
            }
        }

        public void Disconnect()
        {
            if (_useSftp)
            {
                sftpClient.Disconnect();
                sftpClient.Dispose();
                sftpClient = null;
            }
            else
            {
                ftpClient.Disconnect();
                ftpClient.Dispose();
                ftpClient = null;
            }

        }

        /// <summary>
        /// Aborts the current file transfer.
        /// </summary>
        public void AbortTransfer()
        {
            this._abortTransfer = true;
        }

        void client_TransferProgress(object sender, FtpTransferProgressEventArgs e)
        {
            Ftp client = ((Ftp)sender);

            if ((_progressInfo != null && _progressInfo.IsCanceled) || this._abortTransfer)
            {
                if (_progressInfo != null) _progressInfo.Update("Upload Canceled by user");
                client.Abort();
                this._abortTransfer = false;
                return;
            }

            _bytesSent += e.BytesSinceLastEvent;
            SentBytesUpdated(this, new EventArgs<long>(_bytesSent));

            if (_progressInfo != null && _allBytes > 0)
                 _progressInfo.Progress = Convert.ToInt32((((double)_bytesSent / (double)_allBytes) * (double)100));

            if(_progressInfo != null)
                _progressInfo.Update();
        }   


        void client_TransferProgress(object sender, SftpTransferProgressEventArgs e)
        {
            Sftp client = ((Sftp)sender);


            if ((_progressInfo != null && _progressInfo.IsCanceled) || this._abortTransfer)
            {
                if (_progressInfo != null) _progressInfo.Update("Upload Canceled by user");
                client.AbortTransfer();
                this._abortTransfer = false;
                return;
            }

            _bytesSent += e.BytesSinceLastEvent;
            SentBytesUpdated(this, new EventArgs<long>(_bytesSent));

            if (_progressInfo != null && _allBytes > 0)
                _progressInfo.Progress = Convert.ToInt32((((double)_bytesSent / (double)_allBytes) * (double)100));

            if(_progressInfo != null)
                _progressInfo.Update();
        }

    }
}
