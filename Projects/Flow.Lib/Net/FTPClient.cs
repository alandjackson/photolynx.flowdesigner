using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Collections;
using System.Security.Cryptography;
using Flow.Lib.AsyncWorker;
using Flow.Lib.Crypto;
using System.Windows;
using System.Net.NetworkInformation;

namespace Flow.Lib.Net
{
    public class FTPClient : FTPProtocol, IFTPClient
	{
		#region private
		private AsyncCallback m_connectCB;			//	Connect callback method
		private AsyncCallback m_sendCB;				//	Send callback method
		private AsyncCallback m_receiveCB;			//	Receive callback method
		private ManualResetEvent m_send_event;		//	Send threading event
		private ManualResetEvent m_receive_event;	//	Receive threading event
		private StateMaintenance m_state = new StateMaintenance();
		private string m_rnfr_file = "";			//	Rename from file name
		private string m_rnto_file = "";			//	Rename to file name
        private Hashtable m_codeLookUpTable;
        
        private const int m_retryCount = 5;
        private string[] m_uploadList;
        private string m_uploadDirectory;
        private int m_uploadPosition;
        private Thread m_uploadThread;
        private bool m_beingCancelled;
        private long m_bytesSent;
        private long m_bytesTotal;
        
		#endregion

		#region constructors
		/// <summary>
		/// The purpose of this method is to act as a constructor for this class.
		/// </summary>
		/// <param name="host">FTP Server URI</param>
		/*public FTPClient(string host) 
		{
			this.m_uri = host.ToString();

			
		}
        */
		/// <summary>
		/// The purpose of this method is to act as a constructor for this class.
		/// </summary>
		/// <param name="host">FTP Server URI</param>
		/// <param name="user">FTP server user id</param>
		/// <param name="password">FTP server password</param>
		public FTPClient(string host,int port, string username,string password)
		{
            this.m_uri = host.Trim();
            this.m_user = username.Trim();
            this.m_password = password.Trim();
            this.m_connectionPort = port;

            createWorker();
            m_uploadPosition = 0;
            m_uploadList = new string[1] { "" };
		}
		#endregion

        #region publicEvents

        /* Triggers when all files in an upload list are complete.*/
        public event FTPClientUploadEventHandler UploadCompleted;

        /* Triggers when a group of bytes of the current file are uploaded */
        public event FTPClientUploadEventHandler CurrentFileUploadProgression;

        /* Triggers when a file and its md5 are done uploading.*/
        public event FTPClientUploadEventHandler CurrentFileUploadCompleted;

        #endregion

        #region publicProperties

        string _defaultRemoteDirectory;
        public string DefaultRemoteDirectory
        {
            get
            {
                if (_defaultRemoteDirectory == null)
                    return "";
                return _defaultRemoteDirectory;
            }
            set { _defaultRemoteDirectory = value; }
        }

        /*Notification info*/
        public NotificationProgressInfo ProgressInfo { get; set; }

        public string[] CurrentUploadList
        {
            get { return m_uploadList; }
        }

        //Client should not be in charge of threading
        public bool RunAsync { get { return false; } set { } }

        public double PercentageComplete
        {
            get { return m_uploadPosition / (double)m_uploadList.Length; }
        }

        public string CurrentFileUpload
        {
            get { return m_uploadList[m_uploadPosition]; }
        }

        public long CurrentFileBytesSent
        {
            get { return m_bytesSent; }
        }

        public long CurrentFileBytesTotal
        {
            get { return m_bytesTotal; }
        }

        #endregion

        #region privateProperties

        

        #endregion

        #region privateProperties
        /// <summary>
		/// The purpose of this property is to get the return code from the last command.
		/// </summary>
        private int ReturnCode
        {
            get
            {
                return (this.m_state.returncode);
            }
            set
            {
                this.m_state.returncode = value;
            }
        }


        private string ReturnString
		{
			get 
			{
                return (m_codeLookUpTable[this.m_state.returncode].ToString());
			}
		}
		#endregion

		#region privateMethods
		/// <summary>
		/// The purpose of this method is to perform common FTP initialization logic.
		/// </summary>
		private void initialize() 
		{
            try
            {
                m_codeLookUpTable = new Hashtable();

                m_codeLookUpTable.Add(110, "Restart marker reply");
                m_codeLookUpTable.Add(120, "Service ready in x minutes");
                m_codeLookUpTable.Add(125, "Data connection open");
                m_codeLookUpTable.Add(150, "About to open data connection");
                m_codeLookUpTable.Add(200, "Command ok");
                m_codeLookUpTable.Add(202, "Command not implemented");
                m_codeLookUpTable.Add(211, "System status/help reply");
                m_codeLookUpTable.Add(212, "Directory status reply");
                m_codeLookUpTable.Add(213, "File status reply");
                m_codeLookUpTable.Add(214, "Help message text");
                m_codeLookUpTable.Add(215, "Official system type name");
                m_codeLookUpTable.Add(220, "Service ready for new user");
                m_codeLookUpTable.Add(221, "Service closing connection");
                m_codeLookUpTable.Add(225, "Data connection open");
                m_codeLookUpTable.Add(226, "Data connection closing");
                m_codeLookUpTable.Add(227, "Entering passive mode");
                m_codeLookUpTable.Add(230, "User logged in/out");
                m_codeLookUpTable.Add(250, "Requested file action ok");
                m_codeLookUpTable.Add(257, "Pathname created");
                m_codeLookUpTable.Add(331, "Need user password");
                m_codeLookUpTable.Add(332, "Need user account");
                m_codeLookUpTable.Add(350, "Action pending information");
                m_codeLookUpTable.Add(421, "Not available, closing connection");
                m_codeLookUpTable.Add(425, "Can't open data connection");
                m_codeLookUpTable.Add(426, "Connection closed");
                m_codeLookUpTable.Add(450, "File action not taken");
                m_codeLookUpTable.Add(451, "Action aborted, local error");
                m_codeLookUpTable.Add(452, "Insufficient storage/file busy");
                m_codeLookUpTable.Add(500, "Syntax error");
                m_codeLookUpTable.Add(501, "Syntax error in arguments");
                m_codeLookUpTable.Add(502, "Command not implemented");
                m_codeLookUpTable.Add(503, "Bad command squence");
                m_codeLookUpTable.Add(504, "Argument not implemented");
                m_codeLookUpTable.Add(530, "Not logged in");
                m_codeLookUpTable.Add(532, "Need acount for storing files");
                m_codeLookUpTable.Add(550, "Action not taken");
                m_codeLookUpTable.Add(551, "Action aborted, page type unknown");
                m_codeLookUpTable.Add(552, "Exceeded storage allocation");
                m_codeLookUpTable.Add(553, "File name not allowed");

                IPAddress[] addresses = Dns.GetHostAddresses(this.m_uri);
                IPAddress address = addresses[addresses.Length - 1];
                IPEndPoint endPoint = new IPEndPoint(address, m_connectionPort);

                this.m_state.socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                this.m_connectCB = new AsyncCallback(connectCallBack);
                this.m_sendCB = new AsyncCallback(sendCallBack);
                this.m_receiveCB = new AsyncCallback(receiveCallBack);
                this.m_state.command = CONNECT;

                connect(endPoint, this.m_state);

                if (this.m_state.returncode == 0)
                    return;

                switch (user())
                {
                    case (NEED_PASSWORD):
                        if (pass() != LOGGED_IN)
                            throw (new Exception(LOGON_ERROR));

                        break;
                    case (SERVICE_READY):
                        receive(this.m_state);

                        if (this.m_state.returncode == NEED_PASSWORD)
                            if (pass() != LOGGED_IN)
                                throw (new Exception(LOGON_ERROR));

                        break;
                    case (ACTION_OK):
                    case (LOGGED_IN):

                        break;
                    default:
                        throw (new Exception(LOGON_ERROR));
                }

                while ((this.m_state.returncode == LOGGED_IN) | (this.m_state.returncode == ACTION_OK) | (this.m_state.returncode == SERVICE_READY))
                {
                    this.m_state.returncode = 0;

                    receive(this.m_state);				//	Handle multi-part responses
                }
            }
            catch (Exception e)
            {
                throw (e);
            }
		}

		/// <summary>
		/// The purpose of this method is to begin an asynchronous request for a 
		/// socket connection.
		/// </summary>
		/// <param name="endPoint">Network endpoint</param>
		/// <param name="state">Connection state</param>
		/// <returns>FTP return code</returns>
		private int connect(IPEndPoint endPoint,StateMaintenance state) 
		{
			StringBuilder command = new StringBuilder();

			this.m_send_event = new ManualResetEvent(false);
			state.callback = this.m_connectCB;

			command.Append("Attempting connection to FTP server...");
			command.Append(CRLF);

			this.record(command);

			try 
			{
				state.socket.BeginConnect(endPoint, state.callback,state);

				this.m_send_event.WaitOne(TIMEOUT,true);

				receive(this.m_state);

				this.record(this.m_state.response);

				return(this.m_state.returncode);
			}
			catch(Exception) 
			{
				throw(new Exception(CONNECTION_ERROR));
			}
		}

		/// <summary>
		/// The purpose of this method is to handle a call back from a request for an 
		/// asynchronous socket connection.
		/// </summary>
		/// <param name="result">Connection state</param>
		private void connectCallBack(IAsyncResult result) 
		{
			try 
			{
				StateMaintenance state = (StateMaintenance)result.AsyncState;

				state.socket.EndConnect(result);

				this.m_send_event.Set();
			}
			catch(Exception e) 
			{
                //throw(new Exception(CONNECTION_ERROR, e));
			}
		}

		/// <summary>
		/// The purpose of this method is to send commands and data asynchronously.
		/// </summary>
		/// <param name="state">Connection state</param>
		/// <param name="data">Command string</param>
		private void sendCommand(StateMaintenance state,StringBuilder data) 
		{
			sendCommand(state, data.ToString());
		}

		/// <summary>
		/// The purpose of this method is to send commands and data asynchronously.
		/// </summary>
		/// <param name="state">Connection state</param>
		/// <param name="data">Command string</param>
		private void sendCommand(StateMaintenance state,string data) 
		{
			this.m_send_event = new ManualResetEvent(false);
			state.callback =  this.m_sendCB;

			state.command = data;					//	Set command

			this.record(state.command);				//	Record command to log
			this.record(CRLF);

			state.socket.BeginSend(state.bytes,0,state.bytes.Length,SocketFlags.None,state.callback,state);

			this.m_send_event.WaitOne(TIMEOUT,true);
		}

		/// <summary>
		/// The purpose of this method is to handle a call back from an asynchronous 
		/// send.
		/// </summary>
		/// <param name="result">Connection state</param>
		private void sendCallBack(IAsyncResult result) 
		{
			try 
			{
				StateMaintenance state = (StateMaintenance) result.AsyncState;

				int bytes = state.socket.EndSend(result);

				this.m_send_event.Set();
			}
			catch(Exception) { }
		}

		/// <summary>
		/// The purpose of this method is to receive data asynchronously.
		/// </summary>
		/// <param name="state">Connection state</param>
		private void receive(StateMaintenance state) 
		{
			this.m_receive_event = new ManualResetEvent(false);
			state.callback = this.m_receiveCB;

			state.reset();

			try 
			{
				state.socket.BeginReceive(state.buffer,0,state.buffer.Length,0,state.callback,state);

				this.m_receive_event.WaitOne(TIMEOUT,true);

				if(!this.m_state.socket.Equals(state.socket))
					state.socket.Close();			//	Close data transfer socket
			}
			catch(Exception e) 
			{
				while(e != null) 
				{
					Console.WriteLine(e.ToString());

					e = e.InnerException;
				}
			}
		}

		/// <summary>
		/// The purpose of this method is to handle a call back from an asynchronous 
		/// receive.
		/// </summary>
		/// <param name="result">Connection state</param>
		private void receiveCallBack(IAsyncResult result) 
		{
			try 
			{
				StateMaintenance state = (StateMaintenance)result.AsyncState;
				Regex re = new Regex("$");			//	Make commands 5 bytes
				StringBuilder terminate = new StringBuilder();
				string command = re.Replace(state.command,"  ");
				int bytes = state.socket.EndReceive(result);

				re = new Regex(@"\s{1,}");
				command = re.Replace(command,"  ");

				if(bytes > 0) 
					state.response = Encoding.ASCII.GetString(state.buffer,0,bytes);

				terminate.Append(state.buffer[bytes - 2].ToString());
				terminate.Append(state.buffer[bytes - 1].ToString());

				switch(command.Substring(0,5)) 
				{
					case CONNECT:					//	Connect receive handler
						if(state.returncode != SERVICE_READY)
							throw(new Exception(CONNECTION_ERROR));

						if(terminate.ToString() == EOL)
							this.m_receive_event.Set();
						else
							state.socket.BeginReceive(state.buffer,0,state.buffer.Length,0,state.callback,state);

						break;
					case ABOR:						//	Command receive handler
					case ACCT:
					case APPE:
					case CDUP:
					case CWD:
					case DELE:
					case HELP:
					case LIST:
					case MDTM:
					case MKD:
					case MODE:
					case NOOP:
					case PASS:
					case PASV:
					case PORT:
					case PWD:
					case QUIT:
					case REIN:
					case RETR:
					case RMD:
					case RNFR:
					case RNTO:
					case SITE:
					case SIZE:
					case STAT:
					case STOR:
					case SYST:
					case STOU:
					case STRU:
					case TYPE:
					case USER:
						this.record(state.response);

						if(terminate.ToString() == EOL)
							this.m_receive_event.Set();
						else
							state.socket.BeginReceive(state.buffer,0,state.buffer.Length,0,state.callback,state);

						break;
					default:						//	Data receive handler
						if(terminate.ToString() == EOL)
							this.m_receive_event.Set();
						else
							state.socket.BeginReceive(state.buffer,0,state.buffer.Length,0,state.callback,state);

						break;
				}
			}
			catch(Exception) { }					//	Deal with errors later
		}

		/*
		 * Method:			socketConnect
		 * Programmer:		Edmond Woychowsky
		 * Creation Date:	January 25, 2005
		 * Description:		The purpose of this method is to send an create an instance
		 *					of StateMaintenance with a connected socket based upon the
		 *					last PASV or PORT command.
		 */
		/// <summary>
		/// The purpose of this method is to send an create an instance of 
		/// StateMaintenance with a connected socket based upon the last PASV or PORT 
		/// command.
		/// </summary>
		/// <returns>Connection state</returns>
		private StateMaintenance socketConnect() 
		{
			StateMaintenance state = new StateMaintenance();

			try 
			{
				IPAddress address = Dns.GetHostEntry(this.m_uri).AddressList[0];
                IPEndPoint endPoint = new IPEndPoint(address,dataPort());
                //IPEndPoint endPoint = new IPEndPoint(address, this.m_connectionPort);

				state.socket = new Socket(AddressFamily.InterNetwork,SocketType.Stream,ProtocolType.Tcp);

				state.socket.Connect(endPoint);
			}
			catch(Exception e) 
			{
				throw(new Exception(SOCKET_ERROR, e));
			}

			return(state);
		}

        /// <summary>
        /// The purpose of this method is to abort the current operation.
        /// </summary>
        /// <returns>FTP command result</returns>
        private string abor()
        {
            sendCommand(this.m_state, ABOR);			//	Send command
            receive(this.m_state);					//	Receive response

            return (this.m_state.response);
        }

        /// <summary>
        /// The purpose of this method is to append to the end of a file on the remote 
        /// host.
        /// </summary>
        /// <param name="file">File name</param>
        /// <returns>FTP command result</returns>
        private string appe(string file)
        {
            Stream input = File.OpenRead(file);

            appe(file, input);

            input.Close();

            return (this.m_state.response);
        }

        /// <summary>
        /// The purpose of this method is to append to the end of a file on the remote 
        /// host.
        /// </summary>
        /// <param name="localFile">Local file name</param>
        /// <param name="remoteFile">Remote file name</param>
        /// <returns>FTP command result</returns>
        private string appe(string localFile, string remoteFile)
        {
            Stream input = File.OpenRead(localFile);

            appe(remoteFile, input);

            input.Close();

            return (this.m_state.response);
        }

        /// <summary>
        /// The purpose of this method is to append to the end of a file on the remote 
        /// host.
        /// </summary>
        /// <param name="localFile">Local file name</param>
        /// <param name="stream">File stream</param>
        /// <returns>FTP command result</returns>
        private string appe(string remoteFile, Stream stream)
        {
            StateMaintenance state = socketConnect();
            StringBuilder command = new StringBuilder();
            byte[] buffer = new byte[state.BUFFER_SIZE];
            int length = 0;

            command.Append(APPE);					//	Build command
            command.Append(remoteFile);

            sendCommand(this.m_state, command);
            receive(this.m_state);					//	Receive response

            if (this.m_state.returncode == TRANSFER_STARTING)
            {
                stream.Position = 0;				//	Start of stream

                while ((length = stream.Read(buffer, 0, state.BUFFER_SIZE)) > 0)
                    state.socket.Send(buffer, length, 0);

                state.socket.Close();

                receive(this.m_state);				//	Receive response
            }

            return (this.m_state.response);
        }

        /// <summary>
        /// The purpose of this method is to change the current working directory on the 
        /// host to the parent directory.
        /// </summary>
        /// <returns>FTP command result</returns>
        private string cdup()
        {
            sendCommand(this.m_state, CDUP);			//	Send command
            receive(this.m_state);					//	Receive response

            return (this.m_state.response);
        }

        /// <summary>
        /// The purpose of this method is to change the current working directory.
        /// </summary>
        /// <param name="directory">Path to change to</param>
        /// <returns>FTP command result</returns>
        private string cwd(string directory)
        {
            Regex re = new Regex(@"\s{2,}$");		//	Replace multiple whitespaces
            StringBuilder command = new StringBuilder();
            int count = RETRIES;					//	Retry count

            command.Append(re.Replace(CWD, " "));	//	Build command
            command.Append(directory);

            do
            {
                sendCommand(this.m_state, command);
                receive(this.m_state);

                --count;
            } while ((this.m_state.returncode != ACTION_OK) & (count > 0));

            return (this.m_state.response);
        }

        /// <summary>
        /// The purpose of this method is to delete a file on the remote server.
        /// </summary>
        /// <param name="file">Name of file to delete</param>
        /// <returns>FTP command result</returns>
        private string dele(string file)
        {
            StringBuilder command = new StringBuilder();

            command.Append(DELE);					//	Build command
            command.Append(file);

            sendCommand(this.m_state, command);
            receive(this.m_state);					//	Receive response

            return (this.m_state.response);
        }

        /// <summary>
        /// The purpose of this method is to obtain FTP command help from the remote host.
        /// </summary>
        /// <returns>FTP command result</returns>
        private string help()
        {
            return (help(null));
        }

        /// <summary>
        /// The purpose of this method is to obtain FTP command help from the remote host.
        /// </summary>
        /// <param name="command">Command for which to obtain help</param>
        /// <returns>FTP command result</returns>
        private string help(string command)
        {
            StringBuilder helpCommand = new StringBuilder();

            helpCommand.Append(HELP);				//	Build command
            helpCommand.Append(command);

            sendCommand(this.m_state, helpCommand);

            do
            {
                this.m_state.returncode = 0;

                receive(this.m_state);				//	Receive response
            } while (this.m_state.returncode != 0);

            return (this.m_state.response);
        }

        /// <summary>
        /// The purpose of this method is to return a file listing of the current working
        /// directory.
        /// </summary>
        /// <returns>FTP command result</returns>
        private string list()
        {
            StateMaintenance state = socketConnect();

            sendCommand(this.m_state, LIST);

            do
            {
                this.m_state.returncode = 0;

                receive(this.m_state);
            } while ((this.m_state.returncode == TRANSFER_STARTING) | (this.m_state.returncode == FILE_STATUS_OK));

            state.command = DATA;					//	Data receive command
            receive(state);							//	Receive data

            state.socket.Close();
            this.record(state.response);

            return (state.response);
        }

        /// <summary>
        /// The purpose of this method is to return the modification date/time of a file.
        /// </summary>
        /// <param name="file">Remote host file name</param>
        /// <returns>FTP command result</returns>
        private string mdtm(string file)
        {
            StringBuilder command = new StringBuilder();

            command.Append(MDTM);					//	Build command
            command.Append(file);

            sendCommand(this.m_state, command);
            receive(this.m_state);					//	Receive response

            return (this.m_state.response);
        }

        /// <summary>
        /// The purpose of this method is to create a directory on the remote server.
        /// </summary>
        /// <param name="directory">Name of directory to create on the remote host.</param>
        /// <returns>FTP command result</returns>
        private string mkd(string directory)
        {
            StringBuilder command = new StringBuilder();

            command.Append(MKD);					//	Build command
            command.Append(directory);

            sendCommand(this.m_state, command);
            receive(this.m_state);					//	Receive response

            return (this.m_state.response);
        }

        /// <summary>
        /// The purpose of this method is to return a filtered file list.
        /// </summary>
        /// <param name="mask">File name filter</param>
        /// <returns>FTP command result</returns>
        private string nlst(string mask)
        {
            StateMaintenance state = socketConnect();
            StringBuilder command = new StringBuilder();

            command.Append(NLST);					//	Build command
            command.Append(mask);

            sendCommand(this.m_state, command);		//	Send command
            receive(this.m_state);					//	Receive response

            state.command = DATA;					//	Data receive
            receive(state);							//	Receive data

            receive(this.m_state);					//	Receive response
            state.socket.Close();
            this.record(state.response);

            return (state.response);
        }

        /// <summary>
        /// The purpose of this method is to execute an FTP no operation on the remote 
        /// server.
        /// </summary>
        /// <returns>FTP command result</returns>
        private string noop()
        {
            sendCommand(this.m_state, NOOP);			//	Send command
            receive(this.m_state);					//	Receive response

            return (this.m_state.response);
        }

        /// <summary>
        /// The purpose of this method is to send the password to the remote host.
        /// </summary>
        /// <returns>FTP command result</returns>
        private int pass()
        {
            return (pass(this.m_password));
        }

        /// <summary>
        /// The purpose of this method is to send the password to the remote host.
        /// </summary>
        /// <param name="password">Remote host password</param>
        /// <returns>FTP command result</returns>
        private int pass(string password)
        {
            StringBuilder command = new StringBuilder();

            this.m_password = password;

            command.Append(PASS);					//	Build command
            command.Append(password);

            sendCommand(this.m_state, command);

            receive(this.m_state);					//	Receive response

            return (this.m_state.returncode);
        }

        /// <summary>
        /// The purpose of this method is to enter passive transfer mode.
        /// </summary>
        /// <returns>FTP command result</returns>
        private string pasv()
        {
            int count = RETRIES;					//	Retry count

            this.m_state.returncode = 0;

            do
            {
                --count;							//	Decrement try counter

                sendCommand(this.m_state, PASV);		//	Send passive command

                if (this.m_state.returncode != PASSIVE_MODE)
                    receive(this.m_state);			//	Receive response
            } while ((this.m_state.returncode != PASSIVE_MODE) & (count > 0));


            if (this.m_state.returncode != PASSIVE_MODE)
                throw (new Exception(this.m_state.error()));
            else
            {
                Regex re = new Regex(@"^.{0,}\(");	//	Regular expression
                StringBuilder address = new StringBuilder();
                string[] values;					//	Address parts
                string work = re.Replace(this.m_state.response, "");

                re = new Regex(@"\).{0,}\r\n$");

                work = re.Replace(work, "");

                re = new Regex(@",");

                values = re.Split(work);			//	Isolate address/port

                for (int i = 0; i < 4; i++)
                {
                    address.Append(values[i]);

                    if (i != 3)
                        address.Append(".");
                }

                ipAddress = address.ToString();

                dataPort(values[4], values[5]);
            }

            return (this.m_state.response);
        }

        /// <summary>
        /// The purpose of this method is to return the full path of the current working 
        /// directory.
        /// </summary>
        /// <returns>FTP command result</returns>
        private string pwd()
        {
            sendCommand(this.m_state, PWD);			//	Send command
            receive(this.m_state);					//	Receive response

            return (this.m_state.response);
        }

        /// <summary>
        /// The purpose of this method is to send an FTP QUIT command to the remote host.
        /// </summary>
        /// <returns>FTP command result</returns>
        private string quit()
        {
            try
            {
                sendCommand(this.m_state, QUIT);		//	Send command
                receive(this.m_state);				//	Receive response

                this.m_state.close();
            }
            catch (Exception) { }

            return (this.m_state.response);
        }

        /// <summary>
        /// The purpose of this method is to re-initialize the connection to the remote 
        /// server.
        /// </summary>
        /// <returns>FTP command result</returns>
        private string rein()
        {
            sendCommand(this.m_state, REIN);			//	Send command
            receive(this.m_state);					//	Receive response

            return (this.m_state.response);
        }

        /// <summary>
        /// he purpose of this method is to download a file from the remote server.
        /// </summary>
        /// <param name="file">File name to retrieve</param>
        /// <returns>FTP command result</returns>
        private string retr(string file)
        {
            StateMaintenance state = socketConnect();
            StringBuilder command = new StringBuilder();

            command.Append(RETR);					//	Build command
            command.Append(file);

            sendCommand(this.m_state, command);

            do
            {
                this.m_state.returncode = 0;		//	Avoid infinite loop

                receive(this.m_state);				//	Receive response, if any
            } while ((this.m_state.returncode == TRANSFER_STARTING) | (this.m_state.returncode == FILE_STATUS_OK));

            state.command = DATA;					//	Data receive command
            receive(state);							//	Receive data

            state.socket.Close();					//	Receive complete

            return (state.response);
        }

        /// <summary>
        /// The purpose of this method is to delete a directory on the remote server.
        /// </summary>
        /// <param name="directory">Name of the directory to remove</param>
        /// <returns>FTP command result</returns>
        private string rmd(string directory)
        {
            StringBuilder command = new StringBuilder();

            command.Append(RMD);					//	Build command
            command.Append(directory);

            sendCommand(this.m_state, command);
            receive(this.m_state);					//	Receive response

            return (this.m_state.response);
        }

        /// <summary>
        /// The purpose of this method is to specify the rename from part of a file 
        /// rename.
        /// </summary>
        /// <param name="file">Original name of file to rename</param>
        /// <returns>FTP command result</returns>
        private string rnfr(string file)
        {
            StringBuilder command = new StringBuilder();

            this.m_rnfr_file = file;

            command.Append(RNFR);					//	Build command
            command.Append(this.m_rnfr_file);

            sendCommand(this.m_state, command);
            receive(this.m_state);					//	Receive response

            return (this.m_state.response);
        }

        /// <summary>
        /// The purpose of this method is to specify the rename to part of a file rename.
        /// </summary>
        /// <param name="file">New name of file to rename</param>
        /// <returns>FTP command result</returns>
        private string rnto(string file)
        {
            StringBuilder command = new StringBuilder();

            this.m_rnto_file = file;

            command.Append(RNFR);					//	Build command
            command.Append(this.m_rnto_file);

            sendCommand(this.m_state, command);

            do
            {
                receive(this.m_state);				//	Receive response
            } while (this.m_state.returncode == PENDING);

            return (this.m_state.response);
        }

        /// <summary>
        /// The purpose of this method is to issue a site specific command on the remote 
        /// server.
        /// </summary>
        /// <param name="command">The site specific command</param>
        /// <returns>FTP command result</returns>
        private string site(string command)
        {
            StringBuilder siteCommand = new StringBuilder();

            siteCommand.Append(SITE);				//	Build command
            siteCommand.Append(command);

            sendCommand(this.m_state, siteCommand);
            receive(this.m_state);					//	Receive response

            return (this.m_state.response);
        }

        /// <summary>
        /// The purpose of this method is to return the size (in bytes) of the file 
        /// specified.
        /// </summary>
        /// <param name="file">Name of the file on the remote host</param>
        /// <returns>The file size in bytes or zero</returns>
        private int size(string file)
        {
            StringBuilder command = new StringBuilder();
            int fileSize = 0;

            command.Append(SIZE);
            command.Append(file);

            sendCommand(this.m_state, command);
            receive(this.m_state);

            if (this.m_state.returncode == FILE_STATUS)
                fileSize = int.Parse(this.m_state.response.Substring(4));

            if (fileSize < 0)
                fileSize *= -1;

            return (fileSize);
        }

        /// <summary>
        /// The purpose of this method is to obtain the host server's status.
        /// </summary>
        /// <returns>FTP command result</returns>
        private string stat()
        {
            sendCommand(this.m_state, STAT);			//	Send command

            receive(this.m_state);					//	Receive response

            if (this.m_state.returncode == STATUS_OK)
                receive(this.m_state);

            return (this.m_state.response);
        }

        /// <summary>
        /// The purpose of this method is to upload the specified file to the remote 
        /// server.
        /// </summary>
        /// <param name="file">Local/remote file name</param>
        /// <returns>FTP command result</returns>
        private string stor(string file)
        {
            Stream input = File.OpenRead(file);

            stor(file, input);

            input.Close();

            return (this.m_state.response);
        }

        /// <summary>
        /// The purpose of this method is to upload the specified file to the remote 
        /// server.
        /// </summary>
        /// <param name="file">Remote file name</param>
        /// <param name="stream">File stream</param>
        /// <returns>FTP command result</returns>
        private string stor(string file, Stream stream)
        {
            ProgressInfo.Message = String.Format("Uploading {0}.", file);
            ProgressInfo.Update();
            StateMaintenance state = socketConnect();
            StringBuilder command = new StringBuilder();
            byte[] buffer = new byte[state.BUFFER_SIZE];
            int length = 0;

            long size = stream.Length;

            command.Append(STOR);					//	Build command
            command.Append(file);

            sendCommand(this.m_state, command);
            receive(this.m_state);					//	Receive response

            m_bytesSent = 0;

            if (this.m_state.returncode == TRANSFER_STARTING)
            {
                stream.Position = 0;				//	Beginning of stream

                while ((length = stream.Read(buffer, 0, state.BUFFER_SIZE)) > 0)
                {
					if (this.ProgressInfo.IsCanceled)
						return "Operation canceled.";

                    m_bytesSent += length;
                    state.socket.Send(buffer, length, 0);

                    if (CurrentFileUploadProgression != null)
                    {
                        CurrentFileUploadProgression(this);
                    }
                    ProgressInfo.Progress = (int)Math.Floor( (m_bytesSent / (double)size) * 100);
                    ProgressInfo.Update();
                }

                state.socket.Close();

                receive(this.m_state);				//	Receive response
            }

            return (this.m_state.response);
        }

        /// <summary>
        /// The purpose of this method is to upload the specified file to the remote 
        /// server.
        /// </summary>
        /// <param name="file">Local file name</param>
        /// <returns>FTP command result</returns>
        private string stou(string file)
        {
            Stream input = File.OpenRead(file);

            stou(input);

            input.Close();

            return (this.m_state.response);
        }

        /// <summary>
        /// The purpose of this method is to upload the specified file to the remote 
        /// server.
        /// </summary>
        /// <param name="stream">File stream</param>
        /// <returns>FTP command result</returns>
        private string stou(Stream stream)
        {
            StateMaintenance state = socketConnect();
            byte[] buffer = new byte[state.BUFFER_SIZE];
            int length = 0;

            sendCommand(this.m_state, STOU);			//	Send command

            if (this.m_state.returncode == TRANSFER_STARTING)
            {
                stream.Position = 0;				//	Start of stream

                while ((length = stream.Read(buffer, 0, state.BUFFER_SIZE)) > 0)
                    state.socket.Send(buffer, length, 0);

                state.socket.Close();

                receive(this.m_state);				//	Receive response
            }

            return (this.m_state.response);
        }

        /// <summary>
        /// The purpose of this method is to obtain the host server's system type.
        /// </summary>
        /// <returns>FTP command result</returns>
        private string syst()
        {
            sendCommand(this.m_state, SYST);			//	Send command
            receive(this.m_state);					//	Receive response

            return (this.m_state.response);
        }

        /// <summary>
        /// The purpose of this method is to send the user name to the remote host.
        /// </summary>
        /// <returns>Integer FTP command return code</returns>
        private int user()
        {
            return (user(this.m_user));
        }

        /// <summary>
        /// The purpose of this method is to send the user name to the remote host.
        /// </summary>
        /// <param name="userName">Remote host logon id/user name</param>
        /// <returns>Integer FTP command return code</returns>
        private int user(string userName)
        {
            StringBuilder command = new StringBuilder();

            this.m_user = userName;

            command.Append(USER);					//	Build command
            command.Append(this.m_user);

            sendCommand(this.m_state, command);
            receive(this.m_state);					//	Receive response

            return (this.m_state.returncode);
        }

        private void backgroundFileUploader()
        {
            initialize();

            string tempPath = Path.GetTempPath();

            MD5Provider md5 = new MD5Provider();


            NotificationProgressInfo allFilesProgress = new NotificationProgressInfo("Progress of All Files", "Initializing...", 1, m_uploadList.Length, 1);
            allFilesProgress.AllowCancel = true;
            allFilesProgress.Display();

            while (m_uploadPosition < m_uploadList.Length)
            {
                if (allFilesProgress.IsCanceled || ProgressInfo.IsCanceled)
                {
                    m_beingCancelled = true;
                    ProgressInfo.IsCanceled = true;
                    allFilesProgress.IsCanceled = true;
                    userCanceled = true;
                    ProgressInfo.Complete("");
                    allFilesProgress.Complete("");
                    return;
                }

                allFilesProgress.Message = String.Format("Uploading {0} of {1}.", new object[] { m_uploadPosition + 1, m_uploadList.Length });
                allFilesProgress.Update();
                allFilesProgress.Step();

                //if (allFilesProgress.IsCanceled)
                //    return;


                //m_beingCancelled = ProgressInfo.IsCanceled;

                //if (m_beingCancelled)
                //{
                //    ProgressInfo.Complete("Operation canceled");
                //    return;
                //}

                string file = m_uploadList[m_uploadPosition];
                string filename = Path.GetFileName(file);
                Upload(file, this.m_uploadDirectory);
                //FileStream stream;
                //string md5File;
                //int remoteSize;
                //int retryCount = 0;
                //do
                //{
                //    if (m_beingCancelled)
                //    {
                //        return;
                //    }
                //    retryCount++;
                //    if (retryCount > m_retryCount)
                //    {
                //        throw (new Exception("Retry count exceeded."));
                //    }

                //    stream = new FileStream(file, FileMode.Open);
                //    m_bytesTotal = stream.Length;
                //    pasv();
                //    stor(Path.Combine(m_uploadDirectory, filename), stream);
                //    stream.Close();

                //    pasv();
                //    remoteSize = size(Path.Combine(m_uploadDirectory, filename));

                //} while (remoteSize != m_bytesTotal);

                //byte[] md5Hash = md5.HashFile(file);

                //md5File = Path.Combine(tempPath, filename + ".md5");
                //FileStream writeStream = new FileStream(md5File, FileMode.Create);
                //BinaryWriter writer = new BinaryWriter(writeStream);
                //writer.Write(md5Hash);
                //writer.Close();
                //writeStream.Close();

                //stream = new FileStream(md5File, FileMode.Open, FileAccess.Read);
                //pasv();
                //stor(Path.Combine(m_uploadDirectory, Path.GetFileName(md5File)), stream);
                //stream.Close();

                //if (m_beingCancelled)
                //{
                //    return;
                //}
                m_uploadPosition++;
                if (CurrentFileUploadCompleted != null)
                {
                    
                    CurrentFileUploadCompleted(this);
                }
            }
            allFilesProgress.Complete("Transfer completed.");
            quit();

            //ProgressInfo.Message = "Transfer completed.";
            //ProgressInfo.Progress = -1;
            //ProgressInfo.Update();

            ProgressInfo.Complete("Transfer completed.");

            if (UploadCompleted != null)
            {
                UploadCompleted(this);
            }
        }

        private void checkRemotePath()
        {
            try
            {
                initialize();
                mkdirRecurse(m_uploadDirectory);
                quit();
            }
            catch (Exception e)
            {
                throw (e);
            }
        }

        private void mkdirRecurse(string path)
        {
            if (path == "" || path == null)
            {
                return;
            }
            mkdirRecurse(Path.GetDirectoryName(path));
            if (m_beingCancelled)
            {
                return;
            }
            pasv();
            mkd(path);
        }

        private void createWorker()
        {
            m_uploadThread = new Thread(new ThreadStart(backgroundFileUploader));
        }
		#endregion

		#region publicMethods

        public bool PingServer()
        {
            Ping ping = new Ping();

            PingReply pingStatus = ping.Send(m_uri);

            if (pingStatus.Status == IPStatus.Success)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public int GetRemoteFileSize(string directory, string filename)
        {
            initialize();
            int remoteSize = size(Path.Combine(directory, filename));
            quit();
            return remoteSize;
        }

        public void GetRemoteFile(string remoteFile, string localFile)
        {

            FtpWebRequest reqFTP;
            try
            {
                FileStream outputStream = new FileStream(localFile, FileMode.Create);

                reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri("ftp://" + this.m_uri + ":" + this.m_connectionPort + "/" + remoteFile));
                reqFTP.Method = WebRequestMethods.Ftp.DownloadFile;
                reqFTP.UseBinary = true;
                reqFTP.Credentials = new NetworkCredential(this.m_user, this.m_password);
                FtpWebResponse response = (FtpWebResponse)reqFTP.GetResponse();
                Stream ftpStream = response.GetResponseStream();
                long cl = response.ContentLength;
                int bufferSize = 2048;
                int readCount;
                byte[] buffer = new byte[bufferSize];

                readCount = ftpStream.Read(buffer, 0, bufferSize);
                while (readCount > 0)
                {
                    outputStream.Write(buffer, 0, readCount);
                    readCount = ftpStream.Read(buffer, 0, bufferSize);
                }

                ftpStream.Close();
                outputStream.Close();
                response.Close();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }

        public string[] GetRemoteFileList(string directory)
        {
            
            StringBuilder result = new StringBuilder();
            FtpWebRequest reqFTP;
            try
            {
                reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri("ftp://" + this.m_uri + ":" + this.m_connectionPort + "/" + directory + "/"));
                reqFTP.UseBinary = true;
                reqFTP.Credentials = new NetworkCredential(this.m_user, this.m_password);
                reqFTP.Method = WebRequestMethods.Ftp.ListDirectory;
                WebResponse response = reqFTP.GetResponse();
                StreamReader reader = new StreamReader(response.GetResponseStream());

                string line = reader.ReadLine();
                while (line != null)
                {
                    result.Append(line);
                    result.Append("\n");
                    line = reader.ReadLine();
                }
                // to remove the trailing '\n'
                result.Remove(result.ToString().LastIndexOf('\n'), 1);
                reader.Close();
                response.Close();
                return result.ToString().Split('\n');
            }
            catch (Exception ex)
            {
                //System.Windows.Forms.MessageBox.Show(ex.Message);
                return null;
            }
            
        }


        private void Upload(string filename, string remoteDir)
        {
          FileInfo fileInf = new FileInfo(filename);
          string uri = "ftp://" + this.m_uri +  "/" + remoteDir + "/" + fileInf.Name;
          FtpWebRequest reqFTP;
            
          // Create FtpWebRequest object from the Uri provided

          reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri(
                    uri));

          // Provide the WebPermission Credintials

          reqFTP.Credentials = new NetworkCredential(m_user, 
                                                     m_password);
            
          // By default KeepAlive is true, where the control connection is 

          // not closed after a command is executed.

          reqFTP.KeepAlive = false;

          // Specify the command to be executed.

          reqFTP.Method = WebRequestMethods.Ftp.UploadFile;
            
          // Specify the data transfer type.

          reqFTP.UseBinary = true;

          reqFTP.UsePassive = true;

          // Notify the server about the size of the uploaded file

          reqFTP.ContentLength = fileInf.Length;
          long size = fileInf.Length;
          // The buffer size is set to 2kb

          int buffLength = 2048;
          byte[] buff = new byte[buffLength];
          int contentLen;
          int bytesSent = 0;
          int steps = Convert.ToInt32(reqFTP.ContentLength) / buffLength;
          // Opens a file stream (System.IO.FileStream) to read the file to be uploaded
          FileStream fs = fileInf.OpenRead();
           
         int tryCnt = 0;
                while (tryCnt < 5)
                {
                    tryCnt++;
                    try
                    {
                        // Stream to which the file to be upload is written

                        Stream strm = reqFTP.GetRequestStream();
                        
                        // Read from the file stream 2kb at a time

                        contentLen = fs.Read(buff, 0, buffLength);
                       

                        // Till Stream content ends
                        this.ProgressInfo.Update("Transferring File - " + fileInf.Name);

                        while (contentLen != 0)
                        {
                             bytesSent = bytesSent + contentLen;
                             ProgressInfo.Progress = (int)Math.Floor( (bytesSent / (double)size) * 100);
                            this.ProgressInfo.Update();

                            if (ProgressInfo.IsCanceled)
                                return;
                            // Write Content from the file stream to the 

                            // FTP Upload Stream
                            strm.Write(buff, 0, contentLen);
                            contentLen = fs.Read(buff, 0, buffLength);
                        }
                        
                        // Close the file stream and the Request Stream

                        strm.Close();
                        fs.Close();
                      }
                      catch(Exception ex)
                        {
                          if (tryCnt == 5)
                            MessageBox.Show(ex.Message, "Upload Error");
                          continue;
                        }

                    //if we made it here, we didnt throw an error, so no need to retry, so break
                    break;
            }
        }

        public bool userCanceled = false;
        public string BeginFileUploads(string[] fileList)
        {
            return BeginFileUploads(fileList, DefaultRemoteDirectory);
        }

        public string BeginFileUploads(string[] fileList, string destinationDir)
        {
            try
            {

                if (RunAsync && m_uploadThread.IsAlive)
                {
                    return "There is an existing upload. Cancel this upload or wait until it completes.";
                }

                if (ProgressInfo.IsCanceled)
                {
                    userCanceled = true;
                    return "Operation canceled.";
                }
				
				ProgressInfo.Message = "Initializing transfer...";
                ProgressInfo.Progress = 0;
                ProgressInfo.Update();

                m_uploadDirectory = destinationDir;
                m_uploadList = fileList;

                ProgressInfo.Progress = 0;
                ProgressInfo.Update();
                m_uploadPosition = 0;

                checkRemotePath();

                if (ProgressInfo.IsCanceled)
                {
                    userCanceled = true;
                    return "Operation canceled.";
                }

                if (RunAsync)
                {
                    createWorker();
                    m_uploadThread.Start();
                }
                else
                {
                    backgroundFileUploader();
                }
                if (userCanceled)
                {
                    ProgressInfo.Cancel();
                    ProgressInfo.Complete("Operation canceled");
                    return "Operation canceled.";
                }
            }
            catch (Exception e)
            {
                ProgressInfo.Message = "Transfer failed.";
                ProgressInfo.Progress = -1;
                ProgressInfo.Update();
                if (CurrentFileUploadProgression != null)
                {
                    CurrentFileUploadProgression(this);
                }
                quit();
                //throw (e);
                MessageBox.Show("Error Occured while uploading.\nPlease check you ftp settings under Preferences -> Remote Connection");
            }

            if (userCanceled)
            {
                ProgressInfo.Cancel();
                ProgressInfo.Complete("Operation canceled");
                return "Operation canceled.";
            }
            return "Upload started.";
        }

        public string PauseUpload()
        {
            if (RunAsync && !m_uploadThread.IsAlive)
            {
                return "There is no existing upload..";
            }
            m_beingCancelled = true;
            abor();


            ProgressInfo.Message = "Upload paused.";
            ProgressInfo.Update();
            return "Upload paused.";
        }

        public string CancelUpload()
        {
            if (RunAsync && !m_uploadThread.IsAlive)
            {
                return "There is no existing upload..";
            }

            m_beingCancelled = true;
            abor();

            m_uploadList = null;
            m_uploadPosition = -1;

            ProgressInfo.Message = "Upload canceled.";
            ProgressInfo.Update();
            return "Upload canceled.";
        }

        public string ResumeUpload()
        {
            if (RunAsync && m_uploadThread.IsAlive)
            {
                return "There is an existing upload. Cancel this upload or wait until it completes.";
            }
            ProgressInfo.Message = "Resuming upload.";
            ProgressInfo.Update();
            createWorker();

            m_uploadThread.Start();

            return "Upload resumed.";
        }

        public string WaitForUploadCompletion()
        {
            if (RunAsync && !m_uploadThread.IsAlive)
            {
                return "There is no existing upload..";
            }

            m_uploadThread.Join();

            return "Upload completed";
        }

        public string TestConnection()
        {
            try
            {
                string[] tempPath = new string[1];
                tempPath[0] = Path.GetTempFileName();
                FileStream stream = new FileStream(tempPath[0], FileMode.Create);
                byte byteData = 40;
                int byteSize = 500000;
                for( int i = 1; i < byteSize; i++)
                {
                    stream.WriteByte(byteData);
                }
                stream.Close();

                UploadCompleted += new FTPClientUploadEventHandler(FTPClientTest_UploadCompleted);
                initialize();
                BeginFileUploads(tempPath);
            }
            catch (Exception e)
            {
                ProgressInfo.Message = e.Message;
                ProgressInfo.Progress = -1;
                ProgressInfo.Update();
                if (CurrentFileUploadProgression != null)
                {
                    CurrentFileUploadProgression(this);
                }
                return e.Message;
            }
            return "OK";
        }

        void FTPClientTest_UploadCompleted(object sender)
        {
            quit();
            UploadCompleted -= new FTPClientUploadEventHandler(FTPClientTest_UploadCompleted);
        }
		#endregion
	}
}
