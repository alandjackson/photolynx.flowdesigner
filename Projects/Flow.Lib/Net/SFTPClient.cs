﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO;
using System.Security.Cryptography;
using Tamir.SharpSsh;
using Flow.Lib.Crypto;
using System.Net;
using System.Collections;
using System.Net.NetworkInformation;

namespace Flow.Lib.Net
{
    public class SFTPClient : IFTPClient
    {
        #region private
        
        private string[] m_uploadList;
        private string m_uploadDirectory;
        private int m_uploadPosition;
        private Thread m_uploadThread;
        private Sftp m_sftpClient;
        private string m_username;
        private string m_password;
        private int m_port;
        private string m_host;
        private bool m_beingCancelled;
        private long m_bytesSent;
        private long m_bytesTotal;

        #endregion

        #region constructors

        public SFTPClient(string host, int port, string username, string password)
        {
            m_username = username.Trim();
            m_password = password.Trim();
            m_host = host.Trim();
            m_port = port;

            m_uploadThread = new Thread(new ThreadStart(backgroundFileUploader));
        }

        #endregion

        #region publicProperties

        public NotificationProgressInfo ProgressInfo { get; set; }

        /* Bytes sent of current file being uploaded */
        public long CurrentFileBytesSent { get { return m_bytesSent; } }

        /* Total byte size of current file being uploaded */
        public long CurrentFileBytesTotal { get { return m_bytesTotal; } }

        public string DefaultRemoteDirectory { get; set; }

        public string[] CurrentUploadList
        {
            get { return m_uploadList; }
        }

        public double PercentageComplete
        {
            get { return m_uploadPosition / (double)m_uploadList.Length; }
        }

        //Client should not be in charge of threading
        public bool RunAsync { get { return false; } set { } }

        public string CurrentFileUpload
        {
            get { return m_uploadList[m_uploadPosition]; }
        }

        #endregion

        #region publicEvents

        /* Triggers when all files in an upload list are complete.*/
        public event FTPClientUploadEventHandler UploadCompleted;

        /* Triggers when a group of bytes of the current file are uploaded */
        public event FTPClientUploadEventHandler CurrentFileUploadProgression;

        /* Triggers when a file and its md5 are done uploading.*/
        public event FTPClientUploadEventHandler CurrentFileUploadCompleted;

        #endregion

        #region publicMethods

        public bool PingServer()
        {
            Ping ping = new Ping();

            PingReply pingStatus = ping.Send(m_host);

            if (pingStatus.Status == IPStatus.Success)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        public string BeginFileUploads(string[] fileList)
        {
            return BeginFileUploads(fileList, DefaultRemoteDirectory);
        }

        public int GetRemoteFileSize(string directory, string filename)
        {

            if (directory.StartsWith("/"))
                directory = directory.Substring(1);

            string output = "";
            try
            {
                SshExec exec = new SshExec(m_host, m_username);
                exec.Password = m_password;

                exec.Connect();

                string command = "du -b \"" + directory + "/" + filename + "\"";

                output = exec.RunCommand(command);

                exec.Close();
            }
            catch (Exception e)
            {
                throw e;
            }

            int filesize = Convert.ToInt32(output.Split('\t')[0]);
            return filesize;
        }

        public void GetRemoteFile(string remoteFile, string localFile)
        {
            if (remoteFile.StartsWith("/"))
                remoteFile = remoteFile.Substring(1);
            initialize();
            m_sftpClient.Get(remoteFile, localFile);
            quit();

        }

        public string[] GetRemoteFileList(string directory)
        {
            if (directory == null)
                directory = "";

            if (directory.StartsWith("/"))
                directory = directory.Substring(1);

            if (!directory.EndsWith("/"))
                directory += "/";

            string err = null;
            string output = null;

            SshExec exec = new SshExec(m_host, m_username);
            try
            {
                exec.Password = m_password;

                exec.Connect();

                string command = "ls " + directory;
               
                int r = exec.RunCommand(command, ref output, ref err);

                exec.Close();
            }
            catch (Exception e)
            {
                exec.Close();
                throw e;
            }

            //remove trailing \n
            if (output != null && output.Length>0)
            {
                output.Remove(output.ToString().LastIndexOf('\n'), 1);

                string[] result = output.Split('\n');
                return result;
            }

            return null;

        }

        public bool userCanceled = false;
        public string BeginFileUploads(string[] fileList, string destinationDir)
        {
            try
            {

                if (destinationDir.StartsWith("/"))
                    destinationDir = destinationDir.Substring(1);

                ProgressInfo.Message = "Initializing transfer...";
                ProgressInfo.Progress = 0;
                ProgressInfo.Update();

                if (RunAsync && m_uploadThread.IsAlive)
                {
                    return "There is an existing upload. Cancel this upload or wait until it completes.";
                }
                m_uploadDirectory = destinationDir;
                m_uploadList = fileList;

                m_uploadPosition = 0;

                if (ProgressInfo.IsCanceled)
                {
                    userCanceled = true;
                    return "Operation canceled.";
                }

				initialize();

                checkRemotePath();

                quit();

                if (ProgressInfo.IsCanceled)
                {
                    userCanceled = true;
                    return "Operation canceled.";
                }

                if (RunAsync)
                {
                    m_uploadThread = new Thread(new ThreadStart(backgroundFileUploader));
                    m_uploadThread.Start();
                }
                else
                {
                    backgroundFileUploader();
                }

                if (userCanceled)
                {
                    ProgressInfo.Cancel();
                    ProgressInfo.Complete("Operation canceled");
                    return "Operation canceled.";
                }
            }
            catch (Exception e)
            {
                ProgressInfo.Message = e.Message;
                ProgressInfo.Progress = -1;
                ProgressInfo.Update();
                if (CurrentFileUploadProgression != null)
                {
                    CurrentFileUploadProgression(this);
                }
                return e.Message;
            }

            if (userCanceled)
            {
                ProgressInfo.Cancel();
                ProgressInfo.Complete("Operation canceled");
                return "Operation canceled.";
            }

            return "Upload started.";
        }

        public string PauseUpload()
        {
            if (RunAsync && !m_uploadThread.IsAlive)
            {
                return "There is no existing upload.";
            }

            m_beingCancelled = true;
            m_sftpClient.Cancel();

            ProgressInfo.Message = "Upload paused.";
            ProgressInfo.Update();
            return "Upload paused.";
        }

        public string TestConnection()
        {
            try
            {
                ProgressInfo.Message = "Creating test files...";

                string[] tempPath = new string[1];
                tempPath[0] = Path.GetTempFileName();
                //FileStream stream = new FileStream(tempPath[0], FileMode.Create);
                //byte byteData = 40;
                //double byteSize = 1048576;
                string fileContents = new string('0', 1048576);
                File.WriteAllText(tempPath[0], fileContents);
                //for (int i = 1; i < byteSize; i++)
                //{
                //    ProgressInfo.Progress = (int)(i / byteSize);
                //    ProgressInfo.Update();
                //    stream.WriteByte(byteData);
                //}
                //stream.Close();

                UploadCompleted += new FTPClientUploadEventHandler(FTPClientTest_UploadCompleted);
                initialize();
                BeginFileUploads(tempPath);

            }
            catch (Exception e)
            {
                ProgressInfo.Message = e.Message;
                ProgressInfo.Progress = -1;
                ProgressInfo.Update();
                if (CurrentFileUploadProgression != null)
                {
                    CurrentFileUploadProgression(this);
                }
                return e.Message;
            }
            return "OK";
        }

        void FTPClientTest_UploadCompleted(object sender)
        {
            quit();
            UploadCompleted -= new FTPClientUploadEventHandler(FTPClientTest_UploadCompleted);
        }

        public string CancelUpload()
        {
            if (RunAsync && !m_uploadThread.IsAlive)
            {
                return "There is no existing upload.";
            }

            m_beingCancelled = true;
            m_sftpClient.Cancel();
            
            m_uploadList = null;
            m_uploadPosition = -1;

            ProgressInfo.Message = "Upload canceled.";
            ProgressInfo.Update();
            return "Upload canceled.";
        }

        public string ResumeUpload()
        {
            if (RunAsync && m_uploadThread.IsAlive)
            {
                return "There is an existing upload. Cancel this upload or wait until it completes.";
            }
            ProgressInfo.Message = "Resuming upload.";
            ProgressInfo.Update();
            m_uploadThread = new Thread(new ThreadStart(backgroundFileUploader));

            m_uploadThread.Start();

            return "Upload resumed.";
        }

        public string WaitForUploadCompletion()
        {
            if (RunAsync && !m_uploadThread.IsAlive)
            {
                return "There is no existing upload.";
            }

            m_uploadThread.Join();

            return "Upload completed";
        }
        #endregion

        #region privateMethods
        private void backgroundFileUploader()
        {
            try
            {
                initialize();

                string tempPath = Path.GetTempPath();

                MD5Provider md5 = new MD5Provider();
                NotificationProgressInfo allFilesProgress = new NotificationProgressInfo("Progress of All Files", "Initializing...", 1, m_uploadList.Length, 1);
                allFilesProgress.AllowCancel = true;
                allFilesProgress.Display();
                
                while (m_uploadPosition < m_uploadList.Length)
                {

                    if (allFilesProgress.IsCanceled || ProgressInfo.IsCanceled)
                    {
                        m_beingCancelled = true;
                        ProgressInfo.IsCanceled = true;
                        allFilesProgress.IsCanceled = true;
                        userCanceled = true;
                        ProgressInfo.Complete("");
                        allFilesProgress.Complete("");
                        return;
                    }

                    //m_beingCancelled = ProgressInfo.IsCanceled;
					
                    //if (m_beingCancelled)
                    //{
                    //    ProgressInfo.Complete("Operation canceled");
                    //    return;
                    //}
                    string file = m_uploadList[m_uploadPosition];
                    string filename = Path.GetFileName(file);


                    allFilesProgress.Message = String.Format("Uploading {0} of {1}.", new object[] { m_uploadPosition + 1, m_uploadList.Length });
                    allFilesProgress.Update();
                    allFilesProgress.Step();

                    //if (allFilesProgress.IsCanceled)
                    //    return;

                    //
                    //Create MD5Hash file
                    //
                    //byte[] md5Hash = md5.HashFile(file);
                    //string md5File = Path.Combine(tempPath, filename + ".md5");
                    //FileStream writeStream = new FileStream(md5File, FileMode.Create);
                    //BinaryWriter writer = new BinaryWriter(writeStream);
                    //writer.Write(md5Hash);
                    //writer.Close();
                    //writeStream.Close();

                    try
                    {
                        //if (m_beingCancelled)
                        //{
                        //    return;
                        //}
                        ProgressInfo.Message = String.Format("Uploading {0}.", filename);
                        ProgressInfo.Update();
                        if (!m_uploadDirectory.EndsWith("/"))
                            m_uploadDirectory += "/";
                        if (m_uploadDirectory == "/")
                            m_uploadDirectory = "";
                        m_sftpClient.Put(file, m_uploadDirectory + filename);
                        //if (ProgressInfo.IsCanceled)
                        //{
                        //    allFilesProgress.IsCanceled = true;
                        //    return;
                        //}

                        //upload md5hash file
						//ProgressInfo.Message = String.Format("Uploading {0}.", md5File);
                       // ProgressInfo.Update();
                       // m_sftpClient.Put(md5File, Path.Combine(m_uploadDirectory, Path.GetFileName(md5File)));
                    }

                    catch (Exception e)
                    {
                        throw (e);
                    }

                    //if (ProgressInfo.IsCanceled)
                    //    return;

					m_uploadPosition++;
                    if (CurrentFileUploadCompleted != null)
                    {
                        CurrentFileUploadCompleted(this);
                    }
                }

                quit();

				//ProgressInfo.Message = "Transfer completed.";
				//ProgressInfo.Progress = -1;
				//ProgressInfo.Update();
                allFilesProgress.Complete("Transfer completed.");
				ProgressInfo.Complete("Transfer completed.");

                if (CurrentFileUploadCompleted != null)
                {
                    CurrentFileUploadCompleted(this);
                }
            }
            catch (Exception e)
            {
                ProgressInfo.Message = e.Message;
                ProgressInfo.Progress = -1;
                ProgressInfo.Update();
                if (CurrentFileUploadProgression != null)
                {
                    CurrentFileUploadProgression(this);
                }
                return;
            }
        }

        void m_sftpClient_OnTransferProgress(string src, string dst, int transferredBytes, int totalBytes, string message)
        {
            m_bytesSent = transferredBytes;
            m_bytesTotal = totalBytes;
            if (CurrentFileUploadProgression != null)
            {
                CurrentFileUploadProgression(this);
            }
            if (ProgressInfo != null)
            {
                ProgressInfo.Progress = (int)Math.Ceiling((m_bytesSent / (double)totalBytes) * 100);
                ProgressInfo.Update();
            }
        }

        private void checkRemotePath()
        {
            try
            {
                mkdirRecurse(m_uploadDirectory);
            }
            catch (Exception e)
            {
                throw (e);
            }
        }

        private void mkdirRecurse(string path)
        {
            if (path == "" || path == null || path == ".." || path == ".")
            {
                return;
            }
            mkdirRecurse(Path.GetDirectoryName(path));
            if (m_beingCancelled)
            {
                return;
            }

            try
            {
                m_sftpClient.Mkdir(path);
            }
            catch
            {
                //sftp throws an exception when directory exists, fall through.
            }
        }

        private void quit()
        {
            m_sftpClient.Close();
        }

        private void initialize()
        {
            IPAddress address;
            IPHostEntry host;
            string sftpHostIP = null;
            try
            {
                //if the parse works, its a good IP address
                address = IPAddress.Parse(m_host);
                sftpHostIP = m_host;
            }
            catch
            {
                //if IP parse failed, it must be a host name;
                host = Dns.GetHostEntry(m_host);
                sftpHostIP = host.AddressList[host.AddressList.Length - 1].ToString();
            }

            m_sftpClient = new Sftp(sftpHostIP, m_username, m_password);
			m_sftpClient.ProgressInfo = this.ProgressInfo;

            m_sftpClient.OnTransferProgress += new FileTransferEvent(m_sftpClient_OnTransferProgress);
			
			try
            {
                m_sftpClient.Connect(m_port);
				m_sftpClient.ProgressInfo = this.ProgressInfo;
            }
            catch (Exception e)
            {
                throw (e);
            }
        }

        #endregion
    }
}
