﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flow.Lib.Net
{
    public interface IFTPClient
    {
        /* Triggers when all files in an upload list are complete.*/
        event FTPClientUploadEventHandler UploadCompleted;

        /* Triggers when a group of bytes of the current file are uploaded */
        event FTPClientUploadEventHandler CurrentFileUploadProgression;

        /* Triggers when a file and its md5 are done uploading.*/
        event FTPClientUploadEventHandler CurrentFileUploadCompleted;

        /* List of current batch upload files. */
        string[] CurrentUploadList { get; }

        /* Ratio of upload completion. */
        double PercentageComplete { get; }

        bool PingServer();

        /* Bytes sent of current file being uploaded */
        long CurrentFileBytesSent { get; }

        /* Total byte size of current file being uploaded */
        long CurrentFileBytesTotal { get; }

        /* Current uploading file. */
        string CurrentFileUpload { get; }

        /* Run asynchronously. */
        bool RunAsync { get; set; }

        /* Returns a string Array of file in the directory */
        string[] GetRemoteFileList(string directory);

        void GetRemoteFile(string remoteFile, string localFile);

        /* returns the size of a remote file */
        int GetRemoteFileSize(string directory, string filename);

        /*Initializes and begins a batch upload.*/
        string BeginFileUploads(string[] fileList, string destinationDir);

        /*Stops the current batch upload, allowing for a resume.*/
        string PauseUpload();

        /*Cancels the current batch upload.*/
        string CancelUpload();

        /*Resumes a paused upload.*/
        string ResumeUpload();

        /*Wait until batch upload is completed.*/
        string WaitForUploadCompletion();

        /*Tests connection. Returns status string.*/
        string TestConnection();

        /*Notification info*/
        NotificationProgressInfo ProgressInfo {get;set;}

        /*Default remote directory*/
        string DefaultRemoteDirectory { get; set; }
    }
}
