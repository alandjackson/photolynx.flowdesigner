﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flow.Lib.FlowOrder
{
    public class FlowOrderStore
    {
        public int OrderID { get; set; }
        public DateTime OrderDate { get; set; }
        public String ActivationKey { get; set; }
        public String ProjectName { get; set; }
        public String ComputerName { get; set; }
        public String StudioName { get; set; }
        public String FlowVersion { get; set; }
        public Boolean IsImageMatchExport { get; set; }
        public int SubjectCount { get; set; }
        public int ImageCount { get; set; }
        public int PackageCount { get; set; }
    }
}
