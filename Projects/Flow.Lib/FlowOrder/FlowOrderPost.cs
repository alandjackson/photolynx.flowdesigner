﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.Lib.Web;
using Flow.Lib.Persister;

namespace Flow.Lib.FlowOrder
{
    public class FlowOrderPost
    { 
        PostSubmitter PostSubmitter { get; set; }

        public FlowOrderPost()
            : this(new PostSubmitter())
        {
        }

        public FlowOrderPost(PostSubmitter postsubmitter)
        {
            PostSubmitter = postsubmitter;
        }

        public String Post(FlowOrderStore src)
        {
            WebServiceFlowOrderMessage sendMsg = new WebServiceFlowOrderMessage()
            {
                FlowOrderStore = src
            };

            //PostSubmitter.Url = @"http://localhost:64844/floworder";
            PostSubmitter.Url = @"http://www.flowadmin.com/activationservice/floworder";
            PostSubmitter.Type = PostSubmitter.PostTypeEnum.Post;
            PostSubmitter.PostItems.Add("WebServiceFlowOrderMessage",
                ObjectSerializer.ToXmlString(
                new WebServiceFlowOrderMessage() { FlowOrderStore = src }));
            string resultMsgStr = PostSubmitter.Post();
            //MessageBox.Show(resultMsgStr);

            if (resultMsgStr == null)
                return null;

            //WebServiceFlowOrderMessage resultMsg =
            //    ObjectSerializer.FromXmlString<WebServiceFlowOrderMessage>(
            //    resultMsgStr);


            return resultMsgStr;
        }

    }

    public class WebServiceFlowOrderMessage
    {
        public FlowOrderStore FlowOrderStore { get; set; }
        public string ErrorMsg { get; set; }
    }

    
}
