﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flow.Lib.FlowError
{
    public class FlowErrorStore
    {
        public String ComputerId { get; set; }
        public String SupportURL { get; set; }
        public String ProjectName { get; set; }
        public String FlowVersion { get; set; }
        public String FlowEdition { get; set; }
        public String Memory { get; set; }
        public String FlowUpTime { get; set; }
        public String SessionID { get; set; }
        public String Studio { get; set; }
        public String Contact { get; set; }
        public String Email { get; set; }
        public String Phone { get; set; }
        public String UserMessage { get; set; }
        public String ActivationKey { get; set; }
        public DateTime ErrorDate { get; set; }
        public String StackTrace { get; set; }
        public int ErrorID { get; set; }
    }
}
