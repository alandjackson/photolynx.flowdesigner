﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.Lib.Web;
using Flow.Lib.Persister;
using System.IO;
using Flow.Lib.Net;
using Rebex.Net;

namespace Flow.Lib.FlowError
{
    public class FlowErrorPost
    { 
        PostSubmitter PostSubmitter { get; set; }

        public FlowErrorPost()
            : this(new PostSubmitter())
        {
        }

        public FlowErrorPost(PostSubmitter postsubmitter)
        {
            PostSubmitter = postsubmitter;
        }

        public String Post(FlowErrorStore src)
        {
            WebServiceFlowErrorMessage sendMsg = new WebServiceFlowErrorMessage()
            {
                FlowErrorStore = src
            };

            //PostSubmitter.Url = @"http://localhost:64844/flowError";
            PostSubmitter.Url = @"http://www.flowadmin.com/activationservice/flowError";
            PostSubmitter.Type = PostSubmitter.PostTypeEnum.Post;
            PostSubmitter.PostItems.Add("WebServiceFlowErrorMessage",
                ObjectSerializer.ToXmlString(
                new WebServiceFlowErrorMessage() { FlowErrorStore = src }));
            string resultMsgStr = PostSubmitter.Post();
            //MessageBox.Show(resultMsgStr);

            if (resultMsgStr == null)
                return null;

            //WebServiceFlowErrorMessage resultMsg =
            //    ObjectSerializer.FromXmlString<WebServiceFlowErrorMessage>(
            //    resultMsgStr);


            return resultMsgStr;
        }


        public void SendFile(string returnMsg, string filePath, string errorID)
        {
            try
            {
                if (!returnMsg.Contains("--"))
                    return;
                if (!File.Exists(filePath))
                    return;
                string ext = filePath.Split('.').Last();
                string folder1 = errorID.Substring(0, 2);
                string folder2 = errorID.Substring(2, 2);

                string host = returnMsg.Split((new string[] { "--" }), StringSplitOptions.None)[0];
                string user = returnMsg.Split((new string[] { "--" }), StringSplitOptions.None)[1];
                string pass = returnMsg.Split((new string[] { "--" }), StringSplitOptions.None)[2];

                Ftp ftpClient = new Ftp();
                ftpClient.Connect(host, 21);
                ftpClient.Login(user, pass);
                ftpClient.Timeout = 5000;

                string remoteDir = "Errors/" + folder1 + "/" + folder2;
                string remotefile = errorID + "." + ext.ToLower();

                if (!ftpClient.DirectoryExists("Errors"))
                    ftpClient.CreateDirectory("Errors");
                if (!ftpClient.DirectoryExists("Errors/" + folder1))
                    ftpClient.CreateDirectory("Errors/" + folder1);
                if (!ftpClient.DirectoryExists(remoteDir))
                    ftpClient.CreateDirectory(remoteDir);
                ftpClient.PutFile(filePath, remoteDir + "/" + remotefile);
                ftpClient.Disconnect();
            }
            catch (Exception e)
            {
                //do nothing
            }
        }
    }

    public class WebServiceFlowErrorMessage
    {
        public FlowErrorStore FlowErrorStore { get; set; }
        public string ErrorMsg { get; set; }
    }

    
}
