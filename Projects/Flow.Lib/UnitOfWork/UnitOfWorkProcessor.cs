﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.Lib.Log;
using System.Threading;
using NLog;
using System.IO;

namespace Flow.Lib.UnitOfWork
{
    public interface IUnitOfWorkProcessor
    {
        void SubmitBatch(UnitOfWorkBatch batch);
    }

    public class UnitOfWorkProcessor : IUnitOfWorkProcessor
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public void SubmitBatch(UnitOfWorkBatch batch)
        {
            DateTime batchStart = DateTime.Now;

            for (int i = 0; i < batch.WorkItems.Count; i++)
            {
                DateTime itemStart = DateTime.Now;

                batch.WorkItems[i](batch.Params[i]);

                logger.Info("Finished item " + i + " in " +
                    DateTime.Now.Subtract(itemStart).ToString());

                
            }

            if (batch.WhenFinishedAction != null)
                batch.WhenFinishedAction();

            TimeSpan batchFinished = DateTime.Now.Subtract(batchStart);
            logger.Info("Finished batch in " + batchFinished.ToString() +
                ", ave item per second: " +
                batchFinished.TotalSeconds / batch.WorkItems.Count);
        }
    }

    public class ConcurrentUnitOfWorkProcessor : IUnitOfWorkProcessor
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public List<Thread> Threads { get; set; }
        public int ConcurrentProcesses { get; set; }
        Queue<Action<object>> workQueue { get; set; }

        public ConcurrentUnitOfWorkProcessor()
        {
            Threads = new List<Thread>();
            ConcurrentProcesses = 4;
        }

        public void SubmitBatch(UnitOfWorkBatch batch)
        {
            DateTime batchStart = DateTime.Now;
            workQueue = new Queue<Action<object>>();

            foreach (Action<object> a in batch.WorkItems)
                workQueue.Enqueue(a);

            for (int i = 0; i < ConcurrentProcesses; i++)
                StartWorkerThread(batch);

            foreach (Thread t in Threads)
                t.Join();


            TimeSpan batchFinished = DateTime.Now.Subtract(batchStart);
            logger.Info("Finished batch in " + batchFinished.ToString() +
                ", ave item per second: " +
                batchFinished.TotalSeconds / batch.WorkItems.Count);
        }

        protected void StartWorkerThread(UnitOfWorkBatch batch)
        {
            Thread t = new Thread(new ThreadStart(() =>
                {
                    try
                    {
                        while (workQueue.Count > 0)
                        {
                            Action<object> a;
                            lock (workQueue)
                            {
                                a = workQueue.Dequeue();
                            }

                            //new BatchWorker(a, batch.Params[batch.WorkItems.IndexOf(a)])
                            //    .BeginDoWorkAndJoin();


                            DateTime itemStart = DateTime.Now;
                            a(batch.Params[batch.WorkItems.IndexOf(a)]);
                            logger.Info("Finished item in " +
                                DateTime.Now.Subtract(itemStart).ToString());
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Thread Exception: " + e.ToString());
                    }
                }));
            lock(Threads)
            {
                Threads.Add(t);
            }
            t.SetApartmentState(ApartmentState.STA);
            t.Start();
        }
    }

    public class BatchWorker
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public Action<object> Action { get; set; }
        public object Param { get; set; }

        public BatchWorker(Action<object> a, object p) 
        { 
            Action = a; 
            Param = p;
        }

        public void BeginDoWorkAndJoin()
        {
            Thread t = new Thread(new ThreadStart(DoWork));
            t.SetApartmentState(ApartmentState.STA);
            t.Start();
            t.Join();
        }

        public void DoWork()
        {
            DateTime itemStart = DateTime.Now;
            Action(Param);
            logger.Info("Finished item in " +
                DateTime.Now.Subtract(itemStart).ToString());
        }
    }
}
