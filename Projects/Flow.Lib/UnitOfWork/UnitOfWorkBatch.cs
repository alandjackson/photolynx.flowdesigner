﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flow.Lib.UnitOfWork
{
    public class UnitOfWorkBatch
    {
        public List<Action<object>> WorkItems { get; set; }
        public List<object> Params { get; set; }
        public Action WhenFinishedAction { get; set; }

        public UnitOfWorkBatch()
        {
            WorkItems = new List<Action<object>>();
            Params = new List<object>();
            WhenFinishedAction = null;
        }

        public UnitOfWorkBatch SubmitWork(Action<object> a, object p)
        {
            WorkItems.Add(a);
            Params.Add(p);
            return this;
        }

        public UnitOfWorkBatch WhenFinished(Action a)
        {
            WhenFinishedAction = a;
            return this;
        }
    }
}
