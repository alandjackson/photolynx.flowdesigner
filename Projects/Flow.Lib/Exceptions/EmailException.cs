﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using Flow.Lib.Net;

namespace Flow.Lib.Exceptions
{
    public class EmailException : ApplicationException, IPhotoLynxException
    {
        public System.ComponentModel.AsyncCompletedEventArgs EmailEventArgs { get; set; }
        public Emailer Emailer { get; set; }

        public EmailException(Emailer emailer, System.ComponentModel.AsyncCompletedEventArgs e):base()
        {
            EmailEventArgs = e;
            Emailer = emailer;
        }

        #region IPhotoLynxException Members

        public void Handle()
        {
            if (Emailer.ProgressInfo != null)
            {
                Emailer.ProgressInfo.Title = "Could not send email.";
                Emailer.ProgressInfo.Message = EmailEventArgs.Error.Message;
                Emailer.ProgressInfo.Progress = -1;
                Emailer.ProgressInfo.Update();
            }
        }

        #endregion
    }
}
