﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flow.Lib.Exceptions
{
    public interface IPhotoLynxException
    {
        void Handle();
    }
}
