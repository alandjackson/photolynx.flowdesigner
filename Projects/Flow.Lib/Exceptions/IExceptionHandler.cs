﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flow.Lib.Exceptions
{
    public interface IExceptionHandler
    {
        void HandleException(object sender, Exception e);
    }
}
