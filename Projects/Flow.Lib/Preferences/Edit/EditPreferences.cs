﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Xml.Linq;
using System.IO;

namespace Flow.Lib.Preferences
{
    [Serializable]
    public class EditPreferences : PreferenceEntityBase
    {
        public PreferenceManager PreferenceManager { get; set; }

        public string _currentAdjustImageOverlay = null;
        public string CurrentAdjustImageOverlay {
            get { return _currentAdjustImageOverlay; }
            set { this.Set<string>("CurrentAdjustImageOverlay", ref _currentAdjustImageOverlay, value); }
        }

        private string _overlaysDirectory = null;
        public string OverlaysDirectory
        {

            get
            {
                return _overlaysDirectory;
            }
            set
            {
                this.Set<string>("OverlaysDirectory", ref _overlaysDirectory,
                    value.Replace("@CommonApplicationData", System.Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData))
                    );
                if (!Directory.Exists(_overlaysDirectory) && (_overlaysDirectory != null && _overlaysDirectory.Length > 2))
                    Directory.CreateDirectory(_overlaysDirectory);
            }
        }


        private string _reportsDirectory = null;
        public string ReportsDirectory
        {

            get
            {
                return _reportsDirectory;
            }
            set
            {
                this.Set<string>("ReportsDirectory", ref _reportsDirectory,
                    value.Replace("@CommonApplicationData", System.Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData))
                    );
                if (!Directory.Exists(_reportsDirectory) && (_reportsDirectory != null && _reportsDirectory.Length > 2))
                    Directory.CreateDirectory(_reportsDirectory);
            }
        }

        private string _customSettingsDirectory = null;
        public string CustomSettingsDirectory
        {

            get
            {
                return _customSettingsDirectory;
            }
            set
            {
                this.Set<string>("CustomSettingsDirectory", ref _customSettingsDirectory,
                    value.Replace("@CommonApplicationData", System.Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData))
                    );
                if (!Directory.Exists(_customSettingsDirectory) && (_customSettingsDirectory != null && _customSettingsDirectory.Length > 2))
                    Directory.CreateDirectory(_customSettingsDirectory);
            }
        }


        private string _helpDirectory = null;
        public string HelpDirectory
        {

            get
            {
                return _helpDirectory;
            }
            set
            {
                this.Set<string>("HelpDirectory", ref _helpDirectory,
                    value.Replace("@CommonApplicationData", System.Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData))
                    );
                if (!Directory.Exists(_helpDirectory) && (_helpDirectory != null && _helpDirectory.Length > 2))
                    Directory.CreateDirectory(_helpDirectory);
            }
        }

        private string _backgroundsDirectory = null;
        public string BackgroundsDirectory
        {

            get
            {
                string dir =  _backgroundsDirectory ?? Path.Combine(Path.Combine(
                    System.Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "Flow"), "Backgrounds");
                if (!Directory.Exists(dir))
                    Directory.CreateDirectory(dir);
                return dir;
            }
            set
            {
                this.Set<string>("BackgroundsDirectory", ref _backgroundsDirectory,
                    value.Replace("@CommonApplicationData", System.Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData))
                    );
                if (!Directory.Exists(_backgroundsDirectory) && (_backgroundsDirectory != null && _backgroundsDirectory.Length > 2))
                    Directory.CreateDirectory(_backgroundsDirectory);

                this.PremiumBackgroundsDirectory = Path.Combine(value, "Premium");
            }
        }

        private string _premiumBackgroundsDirectory = null;
        public string PremiumBackgroundsDirectory
        {

            get
            {
                if (!Directory.Exists(_premiumBackgroundsDirectory) && (_premiumBackgroundsDirectory != null && _premiumBackgroundsDirectory.Length > 2))
                    Directory.CreateDirectory(_premiumBackgroundsDirectory);

                return _premiumBackgroundsDirectory ?? Path.Combine(Path.Combine(Path.Combine(
                    System.Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "Flow"), "Backgrounds"), "Premium");
            }
            set
            {
                this.Set<string>("PremiumBackgroundsDirectory", ref _premiumBackgroundsDirectory,
                    value.Replace("@CommonApplicationData", System.Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData))
                    );
                if (!Directory.Exists(_premiumBackgroundsDirectory) && (_premiumBackgroundsDirectory != null && _premiumBackgroundsDirectory.Length > 2))
                    Directory.CreateDirectory(_premiumBackgroundsDirectory);
            }
        }
       
        private string _premiumBackgroundPrice = null;
        public string PremiumBackgroundPrice
        {
            get { return _premiumBackgroundPrice; }
            set { this.Set<string>("PremiumBackgroundPrice", ref _premiumBackgroundPrice, value); }
        }
        public decimal PremiumBackgroundPriceDecimal
        {
            get
            {
                decimal returnNumber = 0;
                decimal.TryParse(PremiumBackgroundPrice, out returnNumber);
                return returnNumber;
            }

        }

		private string _externalEditorPath = null;
        public string ExternalEditorPath
        {
            get { return _externalEditorPath; }
            set { this.Set<string>("ExternalEditorPath", ref _externalEditorPath, value); }
        }

        private bool _imageFilterIndexInitialized = false;
        public int _imageFilterIndex = 0;
        public int ImageFilterIndex
        {
            get { return _imageFilterIndex; }
            set { this.Set<int>("ImageFilterIndex", ref _imageFilterIndex, value); }
        }

        private bool _imageFilterBeginDateInitialized = false;
        public DateTime _imageFilterBeginDate = DateTime.Now;
        public DateTime ImageFilterBeginDate
        {
            get { return _imageFilterBeginDate; }
            set { this.Set<DateTime>("ImageFilterBeginDate", ref _imageFilterBeginDate, value); }
        }

        private bool _imageFilterEndDateInitialized = false;
        public DateTime _imageFilterEndDate = DateTime.Now;
        public DateTime ImageFilterEndDate
        {
            get { return _imageFilterEndDate; }
            set { this.Set<DateTime>("ImageFilterEndDate", ref _imageFilterEndDate, value); }
        }

        private bool _imageFilterSourceInitialized = false;
        public String _imageFilterSource = "All";
        public String ImageFilterSource
        {
            get { return _imageFilterSource; }
            set { this.Set<String>("ImageFilterSource", ref _imageFilterSource, value); }
        }

		internal override void Initialize(XElement preferences)
		{
			XElement edit = preferences.Element("Edit");

            if (this.OverlaysDirectory == null)
            {
                if (edit.Element("OverlaysDirectory") == null)
                    this.OverlaysDirectory = "";
                else
                    this.OverlaysDirectory = edit.Element("OverlaysDirectory").Value;

            }

            if (String.IsNullOrEmpty(this.ReportsDirectory))
            {
                this.ReportsDirectory = "@CommonApplicationData\\Flow\\Reports";

            }

            if (String.IsNullOrEmpty(this.CustomSettingsDirectory))
            {
                this.CustomSettingsDirectory = "@CommonApplicationData\\Flow\\CustomSettings";

            }

            if (String.IsNullOrEmpty(this.HelpDirectory))
            {
                this.HelpDirectory = "@CommonApplicationData\\Flow\\Help";

            }

            if (this.BackgroundsDirectory == null)
            {
                if (edit.Element("BackgroundsDirectory") == null)
                    this.BackgroundsDirectory = "";
                else
                    this.BackgroundsDirectory = edit.Element("BackgroundsDirectory").Value;

            }
            if (_premiumBackgroundPrice == null)
                this.PremiumBackgroundPrice = "0";

            if (!_imageFilterIndexInitialized)
            {
                _imageFilterIndexInitialized = true;
                this.ImageFilterIndex = 0;
            }
            //if (!_imageFilterBeginDateInitialized)
            {
                _imageFilterBeginDateInitialized = true;
                //_imageFilterBeginDate = DateTime.Now.AddDays(-14);
                _imageFilterBeginDate = DateTime.Now;
            }
            //if (!_imageFilterEndDateInitialized)
            {
                _imageFilterEndDateInitialized = true;
                _imageFilterEndDate = DateTime.Now;
            }
            if (!_imageFilterSourceInitialized)
            {
                _imageFilterSourceInitialized = true;
                _imageFilterSource = "All";
            }
		}
	}
}
