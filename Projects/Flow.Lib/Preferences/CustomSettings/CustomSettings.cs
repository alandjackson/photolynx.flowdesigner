﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Flow.Lib.Preferences
{
    [Serializable]
    public class CustomSettings : PreferenceEntityBase
    {

       
        private List<string> _payableToFields { get; set; }
        public List<string> PayableToFields
        {
            get { return _payableToFields; }
            set {
                _payableToFields = value;
                SendPropertyChanged("PayableToFields");
            }
        }

        
        private List<string> _allGroupImageFlags { get; set; }
        public List<string> AllGroupImageFlags
        {
            get { return _allGroupImageFlags; }
            set
            {
                _allGroupImageFlags = value;
                SendPropertyChanged("AllGroupImageFlags");
            }
        }


        

 
        public string LabOrderFormLabel { get; set; }

        public bool ShowPayableField { get; set; }
        public bool? ShowCatalogSelection { get; set; }
        public bool ShowProjectTypeField { get; set; }
        public bool ShowGroupImageFlags { get; set; }
        public bool ShowPrincipalField { get; set; }
        public bool? ShowPhotographerField { get; set; }
        public bool ShowDueDateField { get; set; }
        public bool CreateNewProjectShowOrderForm { get; set; }
        public bool Force8x10Crop { get; set; }
        public bool HideTicketCodeAndPackageSummaryInCapture { get; set; }
        public bool UseSubjectExcludeFlag { get; set; }
        public bool? UpdateVisiblefieldsFromMappedDataImport { get; set; }
        public bool? ShowRemoteImageService { get; set; }

        public string DefaultCaptureLayout { get; set; }

        public bool? AlwaysYearbookPose { get; set; }

        public bool? RequireCustomerAddress { get; set; }

        public bool? PafIncludeJsonData { get; set; }

        public bool? EnableIncludeImagesCheckBoxInProjectExport { get; set; }

        internal override void Initialize(XElement xml)
        {

        }
        public void Initialize()
        {
            if (DefaultCaptureLayout == null)
                DefaultCaptureLayout = "";

            if (ShowCatalogSelection == null)
                ShowCatalogSelection = true;

            if (ShowPhotographerField == null)
                ShowPhotographerField = true;

            if (AlwaysYearbookPose == null)
                AlwaysYearbookPose = true;

            if (PafIncludeJsonData == null)
                PafIncludeJsonData = true;



              if (ShowRemoteImageService == null)
                  ShowRemoteImageService = true;

              if (EnableIncludeImagesCheckBoxInProjectExport == null)
                  EnableIncludeImagesCheckBoxInProjectExport = true;

            //force over ride
              //ShowRemoteImageService = false;
            //

            if (RequireCustomerAddress == null)
                RequireCustomerAddress = true;
            if (UpdateVisiblefieldsFromMappedDataImport == null)
                UpdateVisiblefieldsFromMappedDataImport = true;

            if (string.IsNullOrEmpty(LabOrderFormLabel))
            {
                LabOrderFormLabel = "Lab Order Form";
            }
           

            

            if (_payableToFields == null)
            {
                _payableToFields = new List<string>();
                _payableToFields.Add("The Studio");
                _payableToFields.Add("Your School");

            }


            if (_allGroupImageFlags == null)
            {
                _allGroupImageFlags = new List<string>();
                _allGroupImageFlags.Add("Group Flag 1");
                _allGroupImageFlags.Add("Group Flag 2");
                _allGroupImageFlags.Add("Group Flag 3");
                _allGroupImageFlags.Add("Group Flag 4");
                _allGroupImageFlags.Add("Group Flag 5");
                _allGroupImageFlags.Add("Group Flag 6");
            }

            
               

        }









        
    }

   
}
