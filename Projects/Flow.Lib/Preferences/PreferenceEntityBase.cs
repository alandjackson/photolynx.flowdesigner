﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Linq;

using Flow.Lib;

namespace Flow.Lib.Preferences
{
	[Serializable]
	public abstract class PreferenceEntityBase : NotifyPropertyBase
	{
		/// <summary>
		/// Provides property change notification to PreferenceManager elements
		/// </summary>
		protected bool Set<T>(string propertyName, ref T oldValue, T newValue)
        {
			//if (oldValue.Equals(newValue))
			//    return false;

            oldValue = newValue;

            SendPropertyChanged(propertyName);
            return true;
        }

		internal abstract void Initialize(XElement defaults);
	}
}
