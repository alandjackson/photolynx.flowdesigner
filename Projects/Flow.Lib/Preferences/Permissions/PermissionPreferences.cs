﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Linq;

namespace Flow.Lib.Preferences
{
    [Serializable]
    public class PermissionPreferences : PreferenceEntityBase
    {
        public PreferenceManager PreferenceManager { get; set; }

        //
        //These are the main local permissions, the ones that matter
        //

        private bool _isAdmin = false;
        private string _adminPassword = null;
        private bool _canModifyPermissions = false;
        private bool _canModifyProjectFolderLocation = false;
        private bool _canModifySupportURL = false;
        private bool _canViewPreferences = false;
        private bool _canEditCatalogs = false;
        private bool _canViewCatalogs = false;
        private bool _canCreateNewProjects = false;
        private bool _canModifyExistingProjectPreferences = false;
        private bool _canViewProjectTemplates = false;
        private bool _canExportProjects = false;
        private bool _canImportMergeProjects = false;
        private bool _canModifyHotFolderLocation = false;
        private bool _canModifyMrGraySettings = false;
        private bool _canChangeCropGuideImage = false;
        private bool _canModifySubjectData = false;
        private bool _canAddSubjects = false;
        private bool _canDeleteSubjects = false;
        private bool _canAssignImagesToSubjects = false;
        private bool _canUnassignImagesFromSubjects = false;
        private bool _canFlagImageAs = false;
        private bool _canChangeExternalEditingApplication = false;
        private bool _canViewImageAdjustments = false;
        private bool _canCropImages = false;
        private bool _canImportData = false;
        private bool _canDeleteImages = false;
        private bool _canModifyLayoutAndGraphicsLocation = false;
        private bool _canCreateEditLayouts = false;
        private bool _canAddEditDeleteCreditCards = false;
        private bool _canAddEditDeleteOrganizations = false;
        private bool _canModifyUseOriginalFileName = false;
        private bool _canAddEditDeleteProjectTemplates = false;
        private bool _canModifySubjectFields = false;
        private bool _canModifyImageSaveOptions = false;
        private bool _canModifyEventTriggers = false;
        private bool _canModifyBarcodeSettings = false;
        private bool _canModifyFtpConnections = false;
        private bool _canModifyImageQuixCatalogOptions = false;
        private bool _canViewReports = false;
        private bool _canEditReports = false;
        private bool _canSaveReports = false;
        private bool _canViewUsers = false;
        private bool _canAddEditDeleteUsers = false;
        private bool _labOnlyFeatures = false;

        //
        // These are booleans that keep track of which boolean permissions have been initialized
        //
        private bool _isAdminInitialized = false;
        private bool _canModifyPermissionsInitialized = false;
        private bool _canModifyProjectFolderLocationInitialized = false;
        private bool _canModifySupportURLInitialized = false;
        private bool _canViewPreferencesInitialized = false;
        private bool _canEditCatalogsInitialized = false;
        private bool _canViewCatalogsInitialized = false;
        private bool _canCreateNewProjectsInitialized = false;
        private bool _canModifyExistingProjectPreferencesInitialized = false;
        private bool _canViewProjectTemplatesInitialized = false;
        private bool _canExportProjectsInitialized = false;
        private bool _canImportMergeProjectsInitialized = false;
        private bool _canModifyHotFolderLocationInitialized = false;
        private bool _canModifyMrGraySettingsInitialized = false;
        private bool _canChangeCropGuideImageInitialized = false;
        private bool _canModifySubjectDataInitialized = false;
        private bool _canAddSubjectsInitialized = false;
        private bool _canDeleteSubjectsInitialized = false;
        private bool _canAssignImagesToSubjectsInitialized = false;
        private bool _canUnassignImagesFromSubjectsInitialized = false;
        private bool _canFlagImageAsInitialized = false;
        private bool _canChangeExternalEditingApplicationInitialized = false;
        private bool _canViewImageAdjustmentsInitialized = false;
        private bool _canCropImagesInitialized = false;
        private bool _canImportDataInitialized = false;
        private bool _canDeleteImagesInitialized = false;
        private bool _canModifyLayoutAndGraphicsLocationInitialized = false;
        private bool _canCreateEditLayoutsInitialized = false;
        private bool _canAddEditDeleteCreditCardsInitialized = false;
        private bool _canAddEditDeleteOrganizationsInitialized = false;
        private bool _canModifyUseOriginalFileNameInitialized = false;
        private bool _canAddEditDeleteProjectTemplatesInitialized = false;
        private bool _canModifySubjectFieldsInitialized = false;
        private bool _canModifyImageSaveOptionsInitialized = false;
        private bool _canModifyEventTriggersInitialized = false;
        private bool _canModifyBarcodeSettingsInitialized = false;
        private bool _canModifyFtpConnectionsInitialized = false;
        private bool _canModifyImageQuixCatalogOptionsInitialized = false;
        private bool _canViewReportsInitialized = false;
        private bool _canEditReportsInitialized = false;
        private bool _canSaveReportsInitialized = false;
        private bool _canViewUsersInitialized = false;
        private bool _canAddEditDeleteUsersInitialized = false;
        private bool _labOnlyFeaturesInitialized = false;

        //If Admin is changed, all permissions need their change event raised
        public bool IsAdmin
        {
            get { return _isAdmin; }
            set { 
                this.Set<bool>("IsAdmin", ref _isAdmin, value);
                this.Set<bool>("CanModifyPermissions", ref _canModifyPermissions, value);

                SendPropertyChanged("CanModifyProjectFolderLocation");
                SendPropertyChanged("CanModifySupportURL");
                SendPropertyChanged("CanViewPreferences");
                SendPropertyChanged("CanEditCatalogs");
                SendPropertyChanged("CanViewCatalogs");
                SendPropertyChanged("CanCreateNewProjects");
                SendPropertyChanged("CanModifyExistingProjectPreferences");
                SendPropertyChanged("CanViewProjectTemplates");
                SendPropertyChanged("CanExportProjects");
                SendPropertyChanged("CanImportMergeProjects");

                SendPropertyChanged("CanModifyHotFolderLocation");
                SendPropertyChanged("CanModifyMrGraySettings");
                SendPropertyChanged("CanChangeCropGuideImage");
                SendPropertyChanged("CanModifySubjectData");
                SendPropertyChanged("CanAddSubjects");
                SendPropertyChanged("CanDeleteSubjects");
                SendPropertyChanged("CanAssignImagesToSubjects");
                SendPropertyChanged("CanUnassignImagesFromSubjects");
                SendPropertyChanged("CanFlagImageAs");

                SendPropertyChanged("CanChangeExternalEditingApplication");
                SendPropertyChanged("CanViewImageAdjustments");
                SendPropertyChanged("CanCropImages");
                SendPropertyChanged("CanImportData");
                SendPropertyChanged("CanDeleteImages");

                SendPropertyChanged("CanModifyLayoutAndGraphicsLocation");
                SendPropertyChanged("CanCreateEditLayouts");


                SendPropertyChanged("CanAddEditDeleteCreditCards");

                SendPropertyChanged("CanAddEditDeleteOrganizations");

                SendPropertyChanged("CanModifyUseOriginalFileName");

                SendPropertyChanged("CanAddEditDeleteProjectTemplates");
                SendPropertyChanged("CanModifySubjectFields");
                SendPropertyChanged("CanModifyImageSaveOptions");
                SendPropertyChanged("CanModifyEventTriggers");
                SendPropertyChanged("CanModifyBarcodeSettings");

                SendPropertyChanged("CanModifyFtpConnections");
                SendPropertyChanged("CanModifyImageQuixCatalogOptions");

                SendPropertyChanged("CanViewReports");
                SendPropertyChanged("CanEditReports");
                SendPropertyChanged("CanSaveReports");

                SendPropertyChanged("CanViewUsers");
                SendPropertyChanged("CanAddEditDeleteUsers");

                SendPropertyChanged("LabOnlyFeatures");
            }
        }

       
       



        //
        // These are the getters that are used throught the app to enforce the permissions
        //

       public bool CanModifyPermissions
       {
           get { return _isAdmin || _canModifyPermissions; }
       }
        public bool CanModifyProjectFolderLocation
        {
            get { return _isAdmin || _canModifyProjectFolderLocation; }
        }   
        public bool CanModifySupportURL
        {
            get { return _isAdmin ||  _canModifySupportURL; }
        }
        public bool CanViewPreferences
        {
            get { return _isAdmin ||  _canViewPreferences; }
        }
        public bool CanEditCatalogs
        {
            get { return _isAdmin ||  _canEditCatalogs; }
         }
        public bool CanViewCatalogs
        {
            get { return _isAdmin ||  _canViewCatalogs; }
         }
        public bool CanCreateNewProjects
        {
            get { return _isAdmin ||  _canCreateNewProjects; }
        }
        public bool CanModifyExistingProjectPreferences
        {
            get { return _isAdmin ||  _canModifyExistingProjectPreferences; }
        }
        
        public bool CanViewProjectTemplates
        {
            get { return _isAdmin ||  _canViewProjectTemplates; }
        }
        
        public bool CanExportProjects
            {
            get { return _isAdmin ||  _canExportProjects; }
         }
        
        public bool CanImportMergeProjects
            {
            get { return _isAdmin ||  _canImportMergeProjects; }
        }
        
        public bool CanModifyHotFolderLocation
        {
            get { return _isAdmin ||  _canModifyHotFolderLocation; }
        }

        public bool CanModifyMrGraySettings
        {
            get { return _isAdmin || _canModifyMrGraySettings; }
        }
        
        public bool CanChangeCropGuideImage
        {
            get { return _isAdmin ||  _canChangeCropGuideImage; }
        }
        public bool CanModifySubjectData
        {
            get { return _isAdmin ||  _canModifySubjectData; }
        }
        public bool CanAddSubjects
        {
            get { return _isAdmin ||  _canAddSubjects; }
        }
        public bool CanDeleteSubjects
        {
            get { return _isAdmin ||  _canDeleteSubjects; }
        }
        public bool CanAssignImagesToSubjects
        {
            get { return _isAdmin ||  _canAssignImagesToSubjects; }
        }
        public bool CanUnassignImagesFromSubjects
        {
            get { return _isAdmin ||  _canUnassignImagesFromSubjects; }
         }
        public bool CanFlagImageAs
            {
            get { return _isAdmin ||  _canFlagImageAs; }
        }
        
        public bool CanChangeExternalEditingApplication
        {
            get { return _isAdmin ||  _canChangeExternalEditingApplication; }
        }
        public bool CanViewImageAdjustments
        {
            get { return _isAdmin ||  _canViewImageAdjustments; }
        }
        public bool CanCropImages
        {
            get { return _isAdmin ||  _canCropImages; }
        }
        public bool CanImportData
            {
            get { return _isAdmin ||  _canImportData; }
        }

        public bool CanDeleteImages
        {
            get { return _isAdmin ||  _canDeleteImages; }
        }
        
        public bool CanModifyLayoutAndGraphicsLocation
        {
            get { return _isAdmin ||  _canModifyLayoutAndGraphicsLocation; }
        }
        public bool CanCreateEditLayouts
            {
            get { return _isAdmin ||  _canCreateEditLayouts; }
        }
        

        public bool CanAddEditDeleteCreditCards
            {
            get { return _isAdmin ||  _canAddEditDeleteCreditCards; }
        }
        
        public bool CanAddEditDeleteOrganizations
            {
            get { return _isAdmin ||  _canAddEditDeleteOrganizations; }
        }

        public bool CanModifyUseOriginalFileName
            {
                get { return _isAdmin || _canModifyUseOriginalFileName; }
        }

        
        public bool CanAddEditDeleteProjectTemplates
        {
            get { return _isAdmin ||  _canAddEditDeleteProjectTemplates; }
         }
        public bool CanModifySubjectFields
        {
            get { return _isAdmin ||  _canModifySubjectFields; }
        }
        public bool CanModifyImageSaveOptions
        {
            get { return _isAdmin ||  _canModifyImageSaveOptions; }
        }
        public bool CanModifyEventTriggers
        {
            get { return _isAdmin ||  _canModifyEventTriggers; }
        }
        public bool CanModifyBarcodeSettings
            {
            get { return _isAdmin ||  _canModifyBarcodeSettings; }
        }
        
        public bool CanModifyFtpConnections
        {
            get { return _isAdmin ||  _canModifyFtpConnections; }
       }
        public bool CanModifyImageQuixCatalogOptions
            {
            get { return _isAdmin ||  _canModifyImageQuixCatalogOptions; }
         }
        
        public bool CanViewReports
        {
            get { return _isAdmin ||  _canViewReports; }
         }
        public bool CanEditReports
        {
            get { return _isAdmin ||  _canEditReports; }
        }
        public bool CanSaveReports
            {
            get { return _isAdmin ||  _canSaveReports; }
        }
        
        public bool CanViewUsers
        {
            get { return _isAdmin ||  _canViewUsers; }
        }
        public bool CanAddEditDeleteUsers
        {
            get { return _isAdmin ||  _canAddEditDeleteUsers; }
        }

        public bool LabOnlyFeatures
        {
            get { return _labOnlyFeatures; }
        }

        //
        // These are public variables only used by the permissions page, and only these can set the permission variables
        //
        public string AdminPassword
        {
            get { return _adminPassword; }
            set { this.Set<string>("AdminPassword", ref _adminPassword, value); }
        }
        public bool Permission_IsAdmin
        {
            get { return _isAdmin; }
            set { this.Set<bool>("IsAdmin", ref _isAdmin, value); }
        }
        public bool Permission_CanModifyPermissions
        {
            get { return _canModifyPermissions; }
            set { this.Set<bool>("CanModifyPermissions", ref _canModifyPermissions, value); }
        }


        public bool Permission_CanModifyProjectFolderLocation
        {
            get { return _canModifyProjectFolderLocation; }
            set { this.Set<bool>("CanModifyProjectFolderLocation", ref _canModifyProjectFolderLocation, value); }
        }
        public bool Permission_CanModifySupportURL
        {
            get { return _canModifySupportURL; }
            set { this.Set<bool>("CanModifySupportURL", ref _canModifySupportURL, value); }
        }
        public bool Permission_CanViewPreferences
        {
            get { return _canViewPreferences; }
            set { this.Set<bool>("CanViewPreferences", ref _canViewPreferences, value); }
        }
        public bool Permission_CanEditCatalogs
        {
            get { return _canEditCatalogs; }
            set { this.Set<bool>("CanEditCatalogs", ref _canEditCatalogs, value); }
        }
        public bool Permission_CanViewCatalogs
        {
            get { return _canViewCatalogs; }
            set { this.Set<bool>("CanViewCatalogs", ref _canViewCatalogs, value); }
        }
        public bool Permission_CanCreateNewProjects
        {
            get { return _canCreateNewProjects; }
            set { this.Set<bool>("CanCreateNewProjects", ref _canCreateNewProjects, value); }
        }
        public bool Permission_CanModifyExistingProjectPreferences
        {
            get { return _canModifyExistingProjectPreferences; }
            set { this.Set<bool>("CanModifyExistingProjectPreferences", ref _canModifyExistingProjectPreferences, value); }
        }

        public bool Permission_CanViewProjectTemplates
        {
            get { return _canViewProjectTemplates; }
            set { this.Set<bool>("CanViewProjectTemplates", ref _canViewProjectTemplates, value); }
        }

        public bool Permission_CanExportProjects
        {
            get { return _canExportProjects; }
            set { this.Set<bool>("CanExportProjects", ref _canExportProjects, value); }
        }

        public bool Permission_CanImportMergeProjects
        {
            get { return _canImportMergeProjects; }
            set { this.Set<bool>("CanImportMergeProjects", ref _canImportMergeProjects, value); }
        }


        public bool Permission_CanModifyHotFolderLocation
        {
            get { return _canModifyHotFolderLocation; }
            set { this.Set<bool>("CanModifyHotFolderLocation", ref _canModifyHotFolderLocation, value); }
        }

        public bool Permission_CanModifyMrGraySettings
        {
            get { return _canModifyMrGraySettings; }
            set { this.Set<bool>("CanModifyMrGraySettings", ref _canModifyMrGraySettings, value); }
        }

        public bool Permission_CanChangeCropGuideImage
        {
            get { return _canChangeCropGuideImage; }
            set { this.Set<bool>("CanChangeCropGuideImage", ref _canChangeCropGuideImage, value); }
        }
        public bool Permission_CanModifySubjectData
        {
            get { return _canModifySubjectData; }
            set { this.Set<bool>("CanModifySubjectData", ref _canModifySubjectData, value); }
        }
        public bool Permission_CanAddSubjects
        {
            get { return _canAddSubjects; }
            set { this.Set<bool>("CanAddSubjects", ref _canAddSubjects, value); }
        }
        public bool Permission_CanDeleteSubjects
        {
            get { return _canDeleteSubjects; }
            set { this.Set<bool>("CanDeleteSubjects", ref _canDeleteSubjects, value); }
        }
        public bool Permission_CanAssignImagesToSubjects
        {
            get { return _canAssignImagesToSubjects; }
            set { this.Set<bool>("CanAssignImagesToSubjects", ref _canAssignImagesToSubjects, value); }
        }
        public bool Permission_CanUnassignImagesFromSubjects
        {
            get { return _canUnassignImagesFromSubjects; }
            set { this.Set<bool>("CanUnassignImagesFromSubjects", ref _canUnassignImagesFromSubjects, value); }
        }
        public bool Permission_CanFlagImageAs
        {
            get { return _canFlagImageAs; }
            set { this.Set<bool>("CanFlagImageAs", ref _canFlagImageAs, value); }
        }

        public bool Permission_CanChangeExternalEditingApplication
        {
            get { return _canChangeExternalEditingApplication; }
            set { this.Set<bool>("CanChangeExternalEditingApplication", ref _canChangeExternalEditingApplication, value); }
        }
        public bool Permission_CanViewImageAdjustments
        {
            get { return _canViewImageAdjustments; }
            set { this.Set<bool>("CanViewImageAdjustments", ref _canViewImageAdjustments, value); }
        }
        public bool Permission_CanCropImages
        {
            get { return _canCropImages; }
            set { this.Set<bool>("CanCropImages", ref _canCropImages, value); }
        }
        public bool Permission_CanImportData
        {
            get { return _canImportData; }
            set { this.Set<bool>("CanImportData", ref _canImportData, value); }
        }

        public bool Permission_CanDeleteImages
        {
            get { return _canDeleteImages; }
            set { this.Set<bool>("CanDeleteImages", ref _canDeleteImages, value); }
        }

        public bool Permission_CanModifyLayoutAndGraphicsLocation
        {
            get { return _canModifyLayoutAndGraphicsLocation; }
            set { this.Set<bool>("CanModifyLayoutAndGraphicsLocation", ref _canModifyLayoutAndGraphicsLocation, value); }
        }
        public bool Permission_CanCreateEditLayouts
        {
            get { return _canCreateEditLayouts; }
            set { this.Set<bool>("CanCreateEditLayouts", ref _canCreateEditLayouts, value); }
        }


        public bool Permission_CanAddEditDeleteCreditCards
        {
            get { return _canAddEditDeleteCreditCards; }
            set { this.Set<bool>("CanAddEditDeleteCreditCards", ref _canAddEditDeleteCreditCards, value); }
        }

        public bool Permission_CanAddEditDeleteOrganizations
        {
            get { return _canAddEditDeleteOrganizations; }
            set { this.Set<bool>("CanAddEditDeleteOrganizations", ref _canAddEditDeleteOrganizations, value); }
        }

        public bool Permission_CanModifyUseOriginalFileName
        {
            get { return _canModifyUseOriginalFileName; }
            set { this.Set<bool>("CanModifyUseOriginalFileName", ref _canModifyUseOriginalFileName, value); }
        }
        public bool Permission_CanAddEditDeleteProjectTemplates
        {
            get { return _canAddEditDeleteProjectTemplates; }
            set { this.Set<bool>("CanAddEditDeleteProjectTemplates", ref _canAddEditDeleteProjectTemplates, value); }
        }
        public bool Permission_CanModifySubjectFields
        {
            get { return _canModifySubjectFields; }
            set { this.Set<bool>("CanModifySubjectFields", ref _canModifySubjectFields, value); }
        }
        public bool Permission_CanModifyImageSaveOptions
        {
            get { return _canModifyImageSaveOptions; }
            set { this.Set<bool>("CanModifyImageSaveOptions", ref _canModifyImageSaveOptions, value); }
        }
        public bool Permission_CanModifyEventTriggers
        {
            get { return _canModifyEventTriggers; }
            set { this.Set<bool>("CanModifyEventTriggers", ref _canModifyEventTriggers, value); }
        }
        public bool Permission_CanModifyBarcodeSettings
        {
            get { return _canModifyBarcodeSettings; }
            set { this.Set<bool>("CanModifyBarcodeSettings", ref _canModifyBarcodeSettings, value); }
        }

        public bool Permission_CanModifyFtpConnections
        {
            get { return _canModifyFtpConnections; }
            set { this.Set<bool>("CanModifyFtpConnections", ref _canModifyFtpConnections, value); }
        }
        public bool Permission_CanModifyImageQuixCatalogOptions
        {
            get { return _canModifyImageQuixCatalogOptions; }
            set { this.Set<bool>("CanModifyImageQuixCatalogOptions", ref _canModifyImageQuixCatalogOptions, value); }
        }

        public bool Permission_CanViewReports
        {
            get { return _canViewReports; }
            set { this.Set<bool>("CanViewReports", ref _canViewReports, value); }
        }
        public bool Permission_CanEditReports
        {
            get { return _canEditReports; }
            set { this.Set<bool>("CanEditReports", ref _canEditReports, value); }
        }
        public bool Permission_CanSaveReports
        {
            get { return _canSaveReports; }
            set { this.Set<bool>("CanSaveReports", ref _canSaveReports, value); }
        }

        public bool Permission_CanViewUsers
        {
            get { return _canViewUsers; }
            set { this.Set<bool>("CanViewUsers", ref _canViewUsers, value); }
        }
        public bool Permission_CanAddEditDeleteUsers
        {
            get { return _canAddEditDeleteUsers; }
            set { this.Set<bool>("CanAddEditDeleteUsers", ref _canAddEditDeleteUsers, value); }
        }
        public bool Permission_LabOnlyFeatures
        {
            get { return _labOnlyFeatures; }
            set { this.Set<bool>("LabOnlyFeatures", ref _labOnlyFeatures, value); }
        }
          
        //
        // This is where the permissions are initialized from the xml (if they have never been initialized)
        //

        internal override void Initialize(XElement preferences)
        {
            XElement permissions = preferences.Element("Permissions");
            //this._isAdmin = false;
            //this._canModifyPermissions = false;
            if (!_isAdminInitialized)
            {
                _isAdminInitialized = true;
                this.Permission_IsAdmin = Boolean.Parse(permissions.Element("IsAdmin").Value);
            }


            if (!_canModifyPermissionsInitialized)
            {
                _canModifyPermissionsInitialized = true;
                this.Permission_CanModifyPermissions = Boolean.Parse(permissions.Element("CanModifyPermissions").Value);
            }
            if (this.AdminPassword == null)
            {
                this.AdminPassword = permissions.Element("AdminPassword").Value;
            }

            if (!_canModifyProjectFolderLocationInitialized)
            {
                _canModifyProjectFolderLocationInitialized = true;
                this.Permission_CanModifyProjectFolderLocation = Boolean.Parse(permissions.Element("CanModifyProjectFolderLocation").Value);
            }
            if (!_canModifySupportURLInitialized)
            {
                _canModifySupportURLInitialized = true;
                this.Permission_CanModifySupportURL = Boolean.Parse(permissions.Element("CanModifySupportURL").Value);
            }
            if (!_canViewPreferencesInitialized)
            {
                _canViewPreferencesInitialized = true;
                this.Permission_CanViewPreferences = Boolean.Parse(permissions.Element("CanViewPreferences").Value);
            }
            if (!_canEditCatalogsInitialized)
            {
                _canEditCatalogsInitialized = true;
                this.Permission_CanEditCatalogs = Boolean.Parse(permissions.Element("CanEditCatalogs").Value);
            }
            if (!_canViewCatalogsInitialized)
            {
                _canViewCatalogsInitialized = true;
                this.Permission_CanViewCatalogs = Boolean.Parse(permissions.Element("CanViewCatalogs").Value);
            }
            if (!_canCreateNewProjectsInitialized)
            {
                _canCreateNewProjectsInitialized = true;
                this.Permission_CanCreateNewProjects = Boolean.Parse(permissions.Element("CanCreateNewProjects").Value);
            }
            if (!_canModifyExistingProjectPreferencesInitialized)
            {
                _canModifyExistingProjectPreferencesInitialized = true;
                this.Permission_CanModifyExistingProjectPreferences = Boolean.Parse(permissions.Element("CanModifyExistingProjectPreferences").Value);
            }
            if (!_canViewProjectTemplatesInitialized)
            {
                _canViewProjectTemplatesInitialized = true;
                this.Permission_CanViewProjectTemplates = Boolean.Parse(permissions.Element("CanViewProjectTemplates").Value);
            }
            if (!_canExportProjectsInitialized)
            {
                _canExportProjectsInitialized = true;
                this.Permission_CanExportProjects = Boolean.Parse(permissions.Element("CanExportProjects").Value);
            }
            if (!_canImportMergeProjectsInitialized)
            {
                _canImportMergeProjectsInitialized = true;
                this.Permission_CanImportMergeProjects = Boolean.Parse(permissions.Element("CanImportMergeProjects").Value);
            }

            if (!_canModifyHotFolderLocationInitialized)
            {
                _canModifyHotFolderLocationInitialized = true;
                this.Permission_CanModifyHotFolderLocation = Boolean.Parse(permissions.Element("CanModifyHotFolderLocation").Value);
            }
            if (!_canModifyMrGraySettingsInitialized)
            {
                _canModifyMrGraySettingsInitialized = true;
                this.Permission_CanModifyMrGraySettings = Boolean.Parse(permissions.Element("CanModifyMrGraySettings").Value);
            }
            if (!_canChangeCropGuideImageInitialized)
            {
                _canChangeCropGuideImageInitialized = true;
                this.Permission_CanChangeCropGuideImage = Boolean.Parse(permissions.Element("CanChangeCropGuideImage").Value);
            }
            if (!_canModifySubjectDataInitialized)
            {
                _canModifySubjectDataInitialized = true;
                this.Permission_CanModifySubjectData = Boolean.Parse(permissions.Element("CanModifySubjectData").Value);
            }
            if (!_canAddSubjectsInitialized)
            {
                _canAddSubjectsInitialized = true;
                this.Permission_CanAddSubjects = Boolean.Parse(permissions.Element("CanAddSubjects").Value);
            }
            if (!_canDeleteSubjectsInitialized)
            {
                _canDeleteSubjectsInitialized = true;
                this.Permission_CanDeleteSubjects = Boolean.Parse(permissions.Element("CanDeleteSubjects").Value);
            }
            if (!_canAssignImagesToSubjectsInitialized)
            {
                _canAssignImagesToSubjectsInitialized = true;
                this.Permission_CanAssignImagesToSubjects = Boolean.Parse(permissions.Element("CanAssignImagesToSubjects").Value);
            }
            if (!_canUnassignImagesFromSubjectsInitialized)
            {
                _canUnassignImagesFromSubjectsInitialized = true;
                this.Permission_CanUnassignImagesFromSubjects = Boolean.Parse(permissions.Element("CanUnassignImagesFromSubjects").Value);
            }
            if (!_canFlagImageAsInitialized)
            {
                _canFlagImageAsInitialized = true;
                this.Permission_CanFlagImageAs = Boolean.Parse(permissions.Element("CanFlagImageAs").Value);
            }

            if (!_canChangeExternalEditingApplicationInitialized)
            {
                _canChangeExternalEditingApplicationInitialized = true;
                this.Permission_CanChangeExternalEditingApplication = Boolean.Parse(permissions.Element("CanChangeExternalEditingApplication").Value);
            }
            if (!_canViewImageAdjustmentsInitialized)
            {
                _canViewImageAdjustmentsInitialized = true;
                this.Permission_CanViewImageAdjustments = Boolean.Parse(permissions.Element("CanViewImageAdjustments").Value);
            }
            if (!_canCropImagesInitialized)
            {
                _canCropImagesInitialized = true;
                this.Permission_CanCropImages = Boolean.Parse(permissions.Element("CanCropImages").Value);
            }
            if (!_canImportDataInitialized)
            {
                _canImportDataInitialized = true;
                this.Permission_CanImportData = Boolean.Parse(permissions.Element("CanImportData").Value);
            }

            if (!_canDeleteImagesInitialized)
            {
                _canDeleteImagesInitialized = true;
                this.Permission_CanDeleteImages = Boolean.Parse(permissions.Element("CanDeleteImages").Value);
            }

            if (!_canModifyLayoutAndGraphicsLocationInitialized)
            {
                _canModifyLayoutAndGraphicsLocationInitialized = true;
                this.Permission_CanModifyLayoutAndGraphicsLocation = Boolean.Parse(permissions.Element("CanModifyLayoutAndGraphicsLocation").Value);
            }
            if (!_canCreateEditLayoutsInitialized)
            {
                _canCreateEditLayoutsInitialized = true;
                this.Permission_CanCreateEditLayouts = Boolean.Parse(permissions.Element("CanCreateEditLayouts").Value);
            }


            if (!_canAddEditDeleteCreditCardsInitialized)
            {
                _canAddEditDeleteCreditCardsInitialized = true;
                this.Permission_CanAddEditDeleteCreditCards = Boolean.Parse(permissions.Element("CanAddEditDeleteCreditCards").Value);
            }

            if (!_canAddEditDeleteOrganizationsInitialized)
            {
                _canAddEditDeleteOrganizationsInitialized = true;
                this.Permission_CanAddEditDeleteOrganizations = Boolean.Parse(permissions.Element("CanAddEditDeleteOrganizations").Value);
            }
            
            if (!_canModifyUseOriginalFileNameInitialized)
            {
                _canModifyUseOriginalFileNameInitialized = true;
                //this.Permission_CanModifyUseOriginalFileName = Boolean.Parse(permissions.Element("CanModifyUseOriginalFileName").Value);
            }
            if (!_canAddEditDeleteProjectTemplatesInitialized)
            {
                _canAddEditDeleteProjectTemplatesInitialized = true;
                this.Permission_CanAddEditDeleteProjectTemplates = Boolean.Parse(permissions.Element("CanAddEditDeleteProjectTemplates").Value);
            }
            if (!_canModifySubjectFieldsInitialized)
            {
                _canModifySubjectFieldsInitialized = true;
                this.Permission_CanModifySubjectFields = Boolean.Parse(permissions.Element("CanModifySubjectFields").Value);
            }
            if (!_canModifyImageSaveOptionsInitialized)
            {
                _canModifyImageSaveOptionsInitialized = true;
                this.Permission_CanModifyImageSaveOptions = Boolean.Parse(permissions.Element("CanModifyImageSaveOptions").Value);
            }
            if (!_canModifyEventTriggersInitialized)
            {
                _canModifyEventTriggersInitialized = true;
                this.Permission_CanModifyEventTriggers = Boolean.Parse(permissions.Element("CanModifyEventTriggers").Value);
            }
            if (!_canModifyBarcodeSettingsInitialized)
            {
                _canModifyBarcodeSettingsInitialized = true;
                this.Permission_CanModifyBarcodeSettings = Boolean.Parse(permissions.Element("CanModifyBarcodeSettings").Value);
            }

            if (!_canModifyFtpConnectionsInitialized)
            {
                _canModifyFtpConnectionsInitialized = true;
                this.Permission_CanModifyFtpConnections = Boolean.Parse(permissions.Element("CanModifyFtpConnections").Value);
            }
            if (!_canModifyImageQuixCatalogOptionsInitialized)
            {
                _canModifyImageQuixCatalogOptionsInitialized = true;
                this.Permission_CanModifyImageQuixCatalogOptions = Boolean.Parse(permissions.Element("CanModifyImageQuixCatalogOptions").Value);
            }

            if (!_canViewReportsInitialized)
            {
                _canViewReportsInitialized = true;
                this.Permission_CanViewReports = Boolean.Parse(permissions.Element("CanViewReports").Value);
            }
            if (!_canEditReportsInitialized)
            {
                _canEditReportsInitialized = true;
                this.Permission_CanEditReports = Boolean.Parse(permissions.Element("CanEditReports").Value);
            }
            if (!_canSaveReportsInitialized)
            {
                _canSaveReportsInitialized = true;
                this.Permission_CanSaveReports = Boolean.Parse(permissions.Element("CanSaveReports").Value);
            }

            if (!_canViewUsersInitialized)
            {
                _canViewUsersInitialized = true;
                this.Permission_CanViewUsers = Boolean.Parse(permissions.Element("CanViewUsers").Value);
            }
            if (!_canAddEditDeleteUsersInitialized)
            {
                _canAddEditDeleteUsersInitialized = true;
                this.Permission_CanAddEditDeleteUsers = Boolean.Parse(permissions.Element("CanAddEditDeleteUsers").Value);
            }

            if (!_labOnlyFeaturesInitialized)
            {
                _labOnlyFeaturesInitialized = true;
                this.Permission_LabOnlyFeatures = false;
            }
           
        }
    }
}
