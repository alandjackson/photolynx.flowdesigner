﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Linq;
using System.IO;

namespace Flow.Lib.Preferences
{
	[Serializable]
	public class CapturePreferences : PreferenceEntityBase
	{
        public PreferenceManager PreferenceManager { get; set; }

		private bool _useDefaultHotFolderDirectory = false;
		public bool UseDefaultHotFolderDirectory
		{
			get { return _useDefaultHotFolderDirectory; }
			set
			{
				this.Set<bool>("UseDefaultHotFolderDirectory", ref _useDefaultHotFolderDirectory, value);
				this.HotFolderDirectory = FlowContext.FlowHotFolderDirPath;
			}
		}
	
		private string _hotFolderDirectory = null;
		public string HotFolderDirectory
		{
            get { return _hotFolderDirectory; }
            set { this.Set<string>("HotFolderDirectory", ref _hotFolderDirectory, 
                    value.Replace("@CommonApplicationData", System.Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData))
                );
            Directory.CreateDirectory(_hotFolderDirectory);
            }
			
		}

        
       

		private bool _useOverLayPngInitialized = false;
		
		private bool _useOverlayPng = false;
		public bool UseOverlayPng
		{
			get { return _useOverlayPng; }
			set { this.Set<bool>("UseOverlayPng", ref _useOverlayPng, value); }
		}
	
		private bool _useDefaultOverlayPngPath = false;
		public bool UseDefaultOverlayPngPath
		{
			get { return _useDefaultOverlayPngPath; }
			set { this.Set<bool>("UseDefaultOverlayPngPath", ref _useDefaultOverlayPngPath, value); }
		}
	
		private string _overlayPngPath = null;
		public string OverlayPngPath
		{
			get { return _overlayPngPath; }
             set { this.Set<string>("OverlayPngPath", ref _overlayPngPath, 
                    value.Replace("@CommonApplicationData", System.Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData))
                ); }
		}

        private ExifField _metaMatchSourceField = null;
        public ExifField MetaMatchSourceField
        {
            get { return _metaMatchSourceField; }
            set { this.Set<ExifField>("MetaMatchSourceField", ref _metaMatchSourceField, value); }

        }

        private string _metaMatchDestinationField = null;
        public string MetaMatchDestinationField
        {
            get { return _metaMatchDestinationField; }
            set { this.Set<string>("MetaMatchDestinationField", ref _metaMatchDestinationField, value); }

        }

        private bool _metaMatchAutoAssign = false;
        public bool MetaMatchAutoAssign
        {
            get { return _metaMatchAutoAssign; }
            set { this.Set<bool>("MetaMatchAutoAssign", ref _metaMatchAutoAssign, value); }
        }

        private bool _metaMatchAutoPopulate = false;
        public bool MetaMatchAutoPopulate
        {
            get { return _metaMatchAutoPopulate; }
            set { this.Set<bool>("MetaMatchAutoPopulate", ref _metaMatchAutoPopulate, value); }
        }

        private bool _mrGrayShowRGBWarningInitialized = false;

        private bool _mrGrayshowRGBWarning = false;
        public bool MrGrayShowRGBWarning
        {
            get { return _mrGrayshowRGBWarning; }
            set { this.Set<bool>("MrGrayShowRGBWarning", ref _mrGrayshowRGBWarning, value); }
        }

        private string _mrGrayRGBWarningLevel = null;
        public string MrGrayRGBWarningLevel
        {
            get { return _mrGrayRGBWarningLevel; }
            set { this.Set<string>("MrGrayRGBWarningLevel", ref _mrGrayRGBWarningLevel, value); }
        }

        private string _mrGrayTargetBrightness = null;
        public string MrGrayTargetBrightness
        {
            get { return _mrGrayTargetBrightness; }
            set { this.Set<string>("MrGrayTargetBrightness", ref _mrGrayTargetBrightness, value); }
        }

        private string _mrGrayTolerance = null;
        public string MrGrayTolerance
        {
            get { return _mrGrayTolerance; }
            set { this.Set<string>("MrGrayTolerance", ref _mrGrayTolerance, value); }
        }

        private string _mrGrayAdjustmentMethod = null;
        public string MrGrayAdjustmentMethod
        {
            get { return _mrGrayAdjustmentMethod; }
            set { this.Set<string>("MrGrayAdjustmentMethod", ref _mrGrayAdjustmentMethod, value); }
        }


        private string _autoAssignOverride = null;
        public string AutoAssignOverride
        {
            get { return _autoAssignOverride; }
            set { this.Set<string>("AutoAssignOverride", ref _autoAssignOverride, value); }
        }

        public List<String> AutoAssignOverrideChoices
        {
            get
            {
                List<string> newList = new List<string>();
                newList.Add("Always");
                newList.Add("Never");
                newList.Add("Project Specific");

                return newList;
            }
        }

        private string _newTicketScanAction = null;
        public string NewTicketScanAction
        {
            get { return _newTicketScanAction; }
            set { this.Set<string>("NewTicketScanAction", ref _newTicketScanAction, value); }
        }

        public List<String> NewTicketScanActionChoices
        {
            get
            {
                List<string> newList = new List<string>();
                newList.Add("Show Selected Image");
                newList.Add("Show Order Entry");
                newList.Add("Do Nothing");

                return newList;
            }
        }

        private bool _oneImagePerSubject = false;
        public bool OneImagePerSubject
        {
            get { return _oneImagePerSubject; }
            set { this.Set<bool>("OneImagePerSubject", ref _oneImagePerSubject, value); }
        }

        private bool _useReadyFlag = false;
        public bool UseReadyFlag
        {
            get { return _useReadyFlag; }
            set { this.Set<bool>("UseReadyFlag", ref _useReadyFlag, value); }
        }

        private bool _useExcludeSubjectFlag = false;
        public bool UseExcludeSubjectFlag
        {
            get { return _useExcludeSubjectFlag; }
            set { this.Set<bool>("UseExcludeSubjectFlag", ref _useExcludeSubjectFlag, value); }
        }

        private bool _captureTakePicture = false;
        public bool CaptureTakePicture
        {
            get { return _captureTakePicture; }
            set { this.Set<bool>("CaptureTakePicture", ref _captureTakePicture, value); }
        }

        private bool _useHotFolderArchive = false;
        public bool UseHotFolderArchive
        {
            get { return _useHotFolderArchive; }
            set { this.Set<bool>("UseHotFolderArchive", ref _useHotFolderArchive, value); }
        }

        private bool _resetImageNumberingPerSubject = false;
        public bool ResetImageNumberingPerSubject
        {
            get { return _resetImageNumberingPerSubject; }
            set { this.Set<bool>("ResetImageNumberingPerSubject", ref _resetImageNumberingPerSubject, value); }
        }
        
		internal override void Initialize(XElement preferences)
		{
			XElement capture = preferences.Element("Capture");

			if (this.HotFolderDirectory == null)
			{
				this.UseDefaultHotFolderDirectory = Boolean.Parse(capture.Element("UseDefaultHotFolderDirectory").Value);

				this.HotFolderDirectory = (this.UseDefaultHotFolderDirectory)
					? FlowContext.FlowHotFolderDirPath
					: this.HotFolderDirectory = capture.Element("HotFolderDirectory").Value;
				;
			}

			if (!_useOverLayPngInitialized)
			{
				_useOverLayPngInitialized = true;
				this.UseOverlayPng = Boolean.Parse(capture.Element("UseOverlayPng").Value);
			}

			if (this.OverlayPngPath == null)
			{
				this.UseDefaultOverlayPngPath = Boolean.Parse(capture.Element("UseDefaultOverlayPngPath").Value);

				this.OverlayPngPath = (this.UseDefaultOverlayPngPath)
					? FlowContext.FlowDefaultOverlayPngPath
					: capture.Element("OverlayPngPath").Value
				;
			}

            if (!_mrGrayShowRGBWarningInitialized)
            {
                _mrGrayShowRGBWarningInitialized = true;
                this.MrGrayShowRGBWarning = Boolean.Parse(capture.Element("MrGrayShowRGBWarning").Value);
            }

            if (this.MrGrayRGBWarningLevel == null)
                this.MrGrayRGBWarningLevel = capture.Element("MrGrayRGBWarningLevel").Value;
            if (this.MrGrayTargetBrightness == null)
                this.MrGrayTargetBrightness = capture.Element("MrGrayTargetBrightness").Value;
            if (this.MrGrayTolerance == null)
                this.MrGrayTolerance = capture.Element("MrGrayTolerance").Value;
            if (this.MrGrayAdjustmentMethod == null)
                this.MrGrayAdjustmentMethod = capture.Element("MrGrayAdjustmentMethod").Value;

            if (this.AutoAssignOverride == null)
                this.AutoAssignOverride = "Project Specific";
            if (this.NewTicketScanAction == null)
                NewTicketScanAction = "Show Selected Image";


		}
	}
}
