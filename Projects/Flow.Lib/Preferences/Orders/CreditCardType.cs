﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;

using Flow.Lib.Preferences;

namespace Flow.Schema.LinqModel.DataContext
{
	/// <summary>
	/// Represents a credit card type
	/// NOTE: this class is not represented in the FlowMaster database and thus is not a part of the
	/// linq-to-sql system; it has been placed in this assembly and namespace for convenience
	/// </summary>

	[Serializable]
	public class CreditCardType
	{
		public string CreditCardTypeDesc { get; set; }

		public CreditCardType() { }

		public CreditCardType(string creditCardTypeDesc)
		{
			this.CreditCardTypeDesc = creditCardTypeDesc;
		}
	}
}
