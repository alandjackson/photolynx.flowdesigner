﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Linq;

using Flow.Schema.LinqModel.DataContext;

namespace Flow.Lib.Preferences
{
	[Serializable]
	public class OrdersPreferences : PreferenceEntityBase
	{
        public PreferenceManager PreferenceManager { get; set; }

		private ObservableCollection<CreditCardType> _creditCardTypeCollection = null;
		public ObservableCollection<CreditCardType> CreditCardTypeCollection
		{
			get { return _creditCardTypeCollection; }
			private set { _creditCardTypeCollection = value; }
		}

		internal override void Initialize(XElement preferences)
		{
			XElement orders = preferences.Element("Orders");

			if (this.CreditCardTypeCollection == null)
			{
				this.CreditCardTypeCollection = new ObservableCollection<CreditCardType>();

				XElement creditCardTypeCollection = orders.Element("CreditCardTypeCollection");

				foreach (XElement creditCardType in creditCardTypeCollection.Elements("CreditCardType"))
				{
					this.CreditCardTypeCollection.Add(new CreditCardType(creditCardType.Value));
				}
			}
		}

	}
}