﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Linq;
using System.IO;
using Microsoft.Win32;

namespace Flow.Lib.Preferences
{
    [Serializable]
    public class LayoutPreferences : PreferenceEntityBase
    {
        public PreferenceManager PreferenceManager { get; set; }

        public DesignerPreferences Designer { get; set; } = new DesignerPreferences();

        private string _layoutsDirectory = @"@CommonApplicationData\PhotoLynx\LayoutDesigner\Layouts";
        public string LayoutsDirectory
        {

            get
            {
                return _layoutsDirectory;
            }
            set
            {
                this.Set<string>("LayoutsDirectory", ref _layoutsDirectory,
                    value.Replace("@CommonApplicationData", System.Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData))
                    );
                if (!Directory.Exists(_layoutsDirectory))
                    Directory.CreateDirectory(_layoutsDirectory);
            }
        }


        private string _graphicsDirectory = @"@CommonApplicationData\PhotoLynx\LayoutDesigner\Graphics";
        public string GraphicsDirectory
        {
            get
            {
                return _graphicsDirectory;
            }
            set
            {
                this.Set<string>("GraphicsDirectory", ref _graphicsDirectory,
                    value.Replace("@CommonApplicationData", System.Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData))
                    );
                if (!Directory.Exists(_graphicsDirectory))
                    Directory.CreateDirectory(_graphicsDirectory);
            }
        }



        private string _graphicsUnits = "Inches";
        public string GraphicsUnits
        {
            get { return _graphicsUnits; }
            set
            {
                this.Set<string>("GraphicsUnits", ref _graphicsUnits, value);
            }
        }

        public LayoutPreferences LoadFromXmlFile(string xmlFilename)
        {
            XElement layoutPrefs = XDocument.Load(xmlFilename).Root;
            this.Initialize(layoutPrefs);
            return this;
        }

        internal override void Initialize(XElement preferences)
        {
            XElement layout = preferences.Element("Layout");

            LayoutsDirectory = layout.Element("LayoutsDirectory").Value;
            GraphicsDirectory = layout.Element("GraphicsDirectory").Value;
            GraphicsUnits = layout.Element("GraphicsUnits") != null
                ? layout.Element("GraphicsUnits").Value : "Inches";

            Designer = new DesignerPreferences();
            Designer.Initialize(layout);
        }

        public void SaveToXmlFile(string xmlFilename)
        {
            new XDocument(
                new XElement("Preferences",
                CreateXElement())
            ).Save(xmlFilename);
        }

        XElement CreateXElement()
        {
            return 
                new XElement("Layout",
                    new XElement("LayoutsDirectory", LayoutsDirectory),
                    new XElement("GraphicsDirectory", GraphicsDirectory),
                    new XElement("GraphicsUnits", GraphicsUnits),
                    Designer.CreateXElement()
                );
        }

        public void InitializeFromProServicesConfig()
        {
            // initialize graphics and templates directories
            var plLayoutDir = RegistryUtils.ReadStringValue(Registry.CurrentUser,
                "SOFTWARE\\VB and VBA Program Settings\\ProServices\\Default Directory", "Templates Dir");
            if (!string.IsNullOrWhiteSpace(plLayoutDir) && Directory.Exists(plLayoutDir))
            {
                LayoutsDirectory = plLayoutDir;
            }
            var plGraphicsDir = RegistryUtils.ReadStringValue(Registry.CurrentUser,
                "SOFTWARE\\VB and VBA Program Settings\\ProServices\\Default Directory", "Graphics Dir");
            if (!string.IsNullOrWhiteSpace(plGraphicsDir) && Directory.Exists(plGraphicsDir))
            {
                GraphicsDirectory = plGraphicsDir;
            }
            var plOutputDir = RegistryUtils.ReadStringValue(Registry.CurrentUser,
                "SOFTWARE\\VB and VBA Program Settings\\ProServices\\Default Directory", "Print to Image Dir");
            if (!string.IsNullOrWhiteSpace(plOutputDir) && Directory.Exists(plOutputDir))
            {
                Designer.ImageOutputFolderPath = plOutputDir;
            }
        }

        public class RegistryUtils
        {
            public static string ReadStringValue(RegistryKey key, string section, string name)
            {
                using (var subKey = key.OpenSubKey(section))
                {
                    if (subKey == null) return string.Empty;

                    var value = subKey.GetValue(name);
                    return value == null ? string.Empty : value.ToString();
                }
            }
        }

    }
}
