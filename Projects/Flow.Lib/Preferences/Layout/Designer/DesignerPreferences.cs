﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Linq;

namespace Flow.Lib.Preferences
{
	[Serializable]
	public class DesignerPreferences : PreferenceEntityBase
	{
		private bool _useDefaultImageOutputFolderPath = false;
		public bool UseDefaultImageOutputFolderPath
		{
			get { return _useDefaultImageOutputFolderPath; }
			set
			{
				this.Set<bool>("UseDefaultImageOutputFolderPath", ref _useDefaultImageOutputFolderPath, value);
				this.ImageOutputFolderPath = FlowContext.FlowOutputDirPath;
			}
		}

        private bool _useSubjectImageNameInFileName = false;
        public bool UseSubjectImageNameInFileName
        {
            get { return _useSubjectImageNameInFileName; }
            set
            {
                this.Set<bool>("UseSubjectImageNameInFileName", ref _useSubjectImageNameInFileName, value);
                if(value == true)this.Set<bool>("UseSubjectNameInFileName", ref _useSubjectNameInFileName, false);

            }
        }

        private bool _useSubjectNameInFileName = false;
        public bool UseSubjectNameInFileName
        {
            get { return _useSubjectNameInFileName; }
            set
            {
                this.Set<bool>("UseSubjectNameInFileName", ref _useSubjectNameInFileName, value);
                if(value == true)this.Set<bool>("UseSubjectImageNameInFileName", ref _useSubjectImageNameInFileName, false);
            }
        }

		private string _imageOutputFolderPath = @"@CommonApplicationData\PhotoLynx\LayoutDesigner\Output";
		public string ImageOutputFolderPath
		{
			get { return _imageOutputFolderPath; }
			set { this.Set<string>("ImageOutputFolderPath", ref _imageOutputFolderPath, 
                    value.Replace("@CommonApplicationData", System.Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData))
                ); }
		}

		private int _imageDPI = 300;
		public int ImageDPI
		{
			get { return _imageDPI; }
			set { this.Set<int>("ImageDPI", ref _imageDPI, value); }
		}

		private string _imageFormat = "PNG";
		public string ImageFormat
		{
			get { return _imageFormat; }
			set { this.Set<string>("ImageFormat", ref _imageFormat, value); }
		}

		private int _printDestination = 1;
		public int PrintDestination
		{
			get { return _printDestination; }
			set { this.Set<int>("PrintDestination", ref _printDestination, value); }
		}


		internal override void Initialize(XElement layout)
		{
			XElement designer = layout.Element("Designer");
            
			ImageOutputFolderPath = designer.Element("ImageOutputFolderPath").Value;
			ImageDPI = Int32.Parse(designer.Element("ImageDPI").Value);
			ImageFormat = designer.Element("ImageFormat").Value;
			PrintDestination = Int32.Parse(designer.Element("PrintDestination").Value);
            UseSubjectImageNameInFileName = designer.Element("UseSubjectImageNameInFileName").BoolValue();
            UseSubjectNameInFileName = designer.Element("UseSubjectNameInFileName").BoolValue();

        }

        public XElement CreateXElement()
        {
            return
                new XElement("Designer",
                    new XElement("ImageOutputFolderPath", ImageOutputFolderPath),
                    new XElement("ImageDPI", ImageDPI.ToString()),
                    new XElement("ImageFormat", ImageFormat),
                    new XElement("UseSubjectNameInFileName", UseSubjectNameInFileName),
                    new XElement("UseSubjectImageNameInFileName", UseSubjectImageNameInFileName),
                    new XElement("PrintDestination", PrintDestination.ToString())
                );
        }
    }

    public static class XElementExtensions
    {
        public static bool BoolValue(this XElement e)
        {
            if (e == null) return default(bool);
            return bool.Parse(e.Value);
        }
    }
}
