﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Linq;

namespace Flow.Lib.Preferences
{
	[Serializable]
	public class ProjectTransferPreferences : PreferenceEntityBase
	{
        public PreferenceManager PreferenceManager { get; set; }

		private string _hostAddress = null;
		public string HostAddress
		{
            get { 
                if (_hostAddress != null)
                    return _hostAddress.Trim(); 
                return _hostAddress; 
            }
			set { this.Set<string>("HostAddress", ref _hostAddress, value); }
		}

		private int _hostPort = 21;
		public int HostPort
		{
            get { return _hostPort; }
			set { this.Set<int>("HostPort", ref _hostPort, value); }
		}

		private string _userName = null;
		public string UserName
		{
            get
            {
                if (_userName != null)
                    return _userName.Trim();
                return _userName;
            }
			set { this.Set<string>("UserName", ref _userName, value); }
		}

		private string _password = null;
		public string Password
		{
            get
            {
                if (_password != null)
                    return _password.Trim();
                return _password;
            }
			set { this.Set<string>("Password", ref _password, value); }
		}

		private bool _useSftpInitialized = false;
	
		private bool _useSftp = false;
		public bool UseSftp
		{
			get { return _useSftp; }
			set
			{
				this.Set<bool>("UseSftp", ref _useSftp, value);
				this.HostPort = (value) ? 22 : 21;
			}
		}

        private bool _allowEditingInitialized = false;

        private bool _allowEditing = false;
        public bool AllowEditing
        {
            get { return _allowEditing; }
            set
            {
                this.Set<bool>("AllowEditing", ref _allowEditing, value);
            }
        }

		private string _remoteDirectory = null;
		public string RemoteDirectory
		{
            get
            {
                if (_remoteDirectory != null)
                    return _remoteDirectory.Trim().TrimStart('/', '\\');
                return _remoteDirectory;
            }
			set { this.Set<string>("RemoteDirectory", ref _remoteDirectory, value); }
		}

        private string _remoteGraphicsDirectory = null;
        public string RemoteGraphicsDirectory
        {
            get
            {
                if (_remoteGraphicsDirectory != null)
                    return _remoteGraphicsDirectory.Trim().TrimStart('/', '\\');
                return _remoteGraphicsDirectory;
            }
            set { this.Set<string>("RemoteGraphicsDirectory", ref _remoteGraphicsDirectory, value); }
        }

        private string _remoteBackgroundsDirectory = null;
        public string RemoteBackgroundsDirectory
        {
            get
            {
                if (_remoteBackgroundsDirectory != null)
                    return _remoteBackgroundsDirectory.Trim().TrimStart('/', '\\');
                return _remoteBackgroundsDirectory;
            }
            set { this.Set<string>("RemoteBackgroundsDirectory", ref _remoteBackgroundsDirectory, value); }
        }

        private string _remoteLayoutsDirectory = null;
        public string RemoteLayoutsDirectory
        {
            get
            {
                if (_remoteLayoutsDirectory != null)
                    return _remoteLayoutsDirectory.Trim().TrimStart('/', '\\');
                return _remoteLayoutsDirectory;
            }
            set { this.Set<string>("RemoteLayoutsDirectory", ref _remoteLayoutsDirectory, value); }
        }

        private string _remoteOverlaysDirectory = null;
        public string RemoteOverlaysDirectory
        {
            get
            {
                if (_remoteOverlaysDirectory != null)
                    return _remoteOverlaysDirectory.Trim().TrimStart('/', '\\');
                return _remoteOverlaysDirectory;
            }
            set { this.Set<string>("RemoteOverlaysDirectory", ref _remoteOverlaysDirectory, value); }
        }

        private string _remoteReportsDirectory = null;
        public string RemoteReportsDirectory
        {
            get
            {
                if (_remoteReportsDirectory != null)
                    return _remoteReportsDirectory.Trim().TrimStart('/', '\\');
                if (string.IsNullOrEmpty(_remoteReportsDirectory))
                    _remoteReportsDirectory = "Reports";
                return _remoteReportsDirectory;
            }
            set { this.Set<string>("RemoteReportsDirectory", ref _remoteReportsDirectory, value); }
        }

        private string _remoteCustomSettingsDirectory = null;
        public string RemoteCustomSettingsDirectory
        {
            get
            {
                if (_remoteCustomSettingsDirectory != null)
                    return _remoteCustomSettingsDirectory.Trim().TrimStart('/', '\\');

                //use the same path as remote Graphics folder, but replate Graphics with CustomSettings
                if (_remoteGraphicsDirectory != null && _remoteGraphicsDirectory.Contains("Graphics"))
                    return _remoteGraphicsDirectory.Trim().TrimStart('/', '\\').Replace("Graphics", "CustomSettings");
                
                if (string.IsNullOrEmpty(_remoteCustomSettingsDirectory))
                    _remoteCustomSettingsDirectory = "CustomSettings";
                return _remoteCustomSettingsDirectory;
            }
            set { this.Set<string>("RemoteCustomSettingsDirectory", ref _remoteCustomSettingsDirectory, value); }
        }

        private string _remoteHelpDirectory = null;
        public string RemoteHelpDirectory
        {
            get
            {
                if (_remoteHelpDirectory != null)
                    return _remoteHelpDirectory.Trim().TrimStart('/', '\\');

                //use the same path as remote Graphics folder, but replate Graphics with CustomSettings
                if (_remoteGraphicsDirectory != null && _remoteGraphicsDirectory.Contains("Graphics"))
                    return _remoteGraphicsDirectory.Trim().TrimStart('/', '\\').Replace("Graphics", "Help");

                if (string.IsNullOrEmpty(_remoteHelpDirectory))
                    _remoteHelpDirectory = "RemoteHelpDirectory";
                return _remoteHelpDirectory;
            }
            set { this.Set<string>("RemoteHelpDirectory", ref _remoteHelpDirectory, value); }
        }

        private string _remoteImageMatchDirectory = null;
        public string RemoteImageMatchDirectory
        {
            get
            {
                if (_remoteImageMatchDirectory != null)
                    return _remoteImageMatchDirectory.Trim().TrimStart('/', '\\');
                return _remoteImageMatchDirectory;
            }
            set { this.Set<string>("RemoteImageMatchDirectory", ref _remoteImageMatchDirectory, value); }
        }

		internal override void Initialize(XElement upload)
		{
			XElement projectTransfer = upload.Element("ProjectTransfer");

			if(this.HostAddress == null)
				this.HostAddress = projectTransfer.Element("HostAddress").Value;

			if(this.UserName == null)
				this.UserName = projectTransfer.Element("UserName").Value;
	
			if(this.Password == null)
				this.Password = projectTransfer.Element("Password").Value;

			if (!_useSftpInitialized)
			{
				_useSftpInitialized = true;
				this.UseSftp = Boolean.Parse(projectTransfer.Element("UseSftp").Value);
			}

            if (!_allowEditingInitialized)
            {
                _allowEditingInitialized = true;
                this.AllowEditing = false;
            }

            if (this.RemoteDirectory == null && projectTransfer.Element("RemoteDirectory") != null)
				this.RemoteDirectory = projectTransfer.Element("RemoteDirectory").Value;
            if (this.RemoteGraphicsDirectory == null && projectTransfer.Element("RemoteGraphicsDirectory") != null)
                this.RemoteGraphicsDirectory = projectTransfer.Element("RemoteGraphicsDirectory").Value;
            if (this.RemoteLayoutsDirectory == null && projectTransfer.Element("RemoteLayoutsDirectory") != null)
                this.RemoteLayoutsDirectory = projectTransfer.Element("RemoteLayoutsDirectory").Value;
            if (this.RemoteOverlaysDirectory == null && projectTransfer.Element("RemoteOverlaysDirectory") != null)
                this.RemoteOverlaysDirectory = projectTransfer.Element("RemoteOverlaysDirectory").Value;
            if (this.RemoteReportsDirectory == null && projectTransfer.Element("RemoteReportsDirectory") != null)
                this.RemoteReportsDirectory = projectTransfer.Element("RemoteReportsDirectory").Value;
            if (this.RemoteImageMatchDirectory == null && projectTransfer.Element("RemoteImageMatchDirectory") != null)
                this.RemoteImageMatchDirectory = projectTransfer.Element("RemoteImageMatchDirectory").Value;
            if (this.RemoteBackgroundsDirectory == null || this.RemoteBackgroundsDirectory.Length < 2)
                this.RemoteBackgroundsDirectory = "/Backgrounds";

            if (string.IsNullOrEmpty(RemoteReportsDirectory))
                RemoteReportsDirectory = "Reports";
		}
	
	}
}
