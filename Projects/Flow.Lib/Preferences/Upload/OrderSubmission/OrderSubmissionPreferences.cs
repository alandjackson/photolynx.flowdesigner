﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Linq;


namespace Flow.Lib.Preferences
{
	[Serializable]
	public class OrderSubmissionPreferences : PreferenceEntityBase
	{
        public PreferenceManager PreferenceManager { get; set; }

        public string ImageQuixOrderSubmissionUrl
        {
            get { return this.PreferenceManager.Upload.CatalogRequest.ImageQuixBaseUrl + "/serviceorder"; }
        }

		private string _imageQuixFtpAddress = null;
		public string ImageQuixFtpAddress
		{
			get { return _imageQuixFtpAddress; }
			set { this.Set<string>("ImageQuixFtpAddress", ref _imageQuixFtpAddress, value); }
		}

		private int _imageQuixFtpPort = -1;
		public int ImageQuixFtpPort
		{
			get { return _imageQuixFtpPort; }
			set { this.Set<int>("ImageQuixFtpPort", ref _imageQuixFtpPort, value); }
		}

		private string _imageQuixFtpUserName = null;
		public string ImageQuixFtpUserName
		{
			get { return _imageQuixFtpUserName; }
			set { this.Set<string>("ImageQuixFtpUserName", ref _imageQuixFtpUserName, value); }
		}

		private string _imageQuixFtpPassword = null;
		public string ImageQuixFtpPassword
		{
			get { return _imageQuixFtpPassword; }
			set { this.Set<string>("ImageQuixFtpPassword", ref _imageQuixFtpPassword, value); }
		}

		private string _imageQuixFtpDirectory = null;
		public string ImageQuixFtpDirectory
		{
			get { return _imageQuixFtpDirectory; }
			set { this.Set<string>("ImageQuixFtpDirectory", ref _imageQuixFtpDirectory, value); }
		}

		private bool? _imageQuixUseSftp = null;
		public bool ImageQuixUseSftp
		{
			get { return (_imageQuixUseSftp.HasValue) ? _imageQuixUseSftp.Value : false; }
			set
			{
				this.Set<bool?>("ImageQuixUseFtp", ref _imageQuixUseSftp, value);
				this.ImageQuixFtpPort = (this.ImageQuixUseSftp) ? 22 : 21;
			}
		}


		internal override void Initialize(XElement upload)
		{
			XElement orderSubmission = upload.Element("OrderSubmission");

			
			if(this.ImageQuixFtpAddress == null)
				this.ImageQuixFtpAddress = orderSubmission.Element("ImageQuixFtpAddress").Value;
			
			if(this.ImageQuixFtpPort == -1)
				this.ImageQuixFtpPort = Int32.Parse(orderSubmission.Element("ImageQuixFtpPort").Value);
			
			if(this.ImageQuixFtpUserName == null)
				this.ImageQuixFtpUserName = orderSubmission.Element("ImageQuixFtpUserName").Value;
			
			if(this.ImageQuixFtpPassword == null)
				this.ImageQuixFtpPassword = orderSubmission.Element("ImageQuixFtpPassword").Value;
			
			if(this.ImageQuixFtpDirectory == null)
				this.ImageQuixFtpDirectory = orderSubmission.Element("ImageQuixFtpDirectory").Value;

			if (_imageQuixUseSftp == null)
			{
				XElement imageQuixUseSftpElement = orderSubmission.Element("ImageQuixUseSftp");

				if (imageQuixUseSftpElement == null)
					this.ImageQuixUseSftp = false;
				else
					this.ImageQuixUseSftp = Boolean.Parse(imageQuixUseSftpElement.Value);
			}
		}
	
	}
}
