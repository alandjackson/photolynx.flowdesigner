﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Linq;

namespace Flow.Lib.Preferences
{
	[Serializable]
	public class CatalogRequestPreferences : PreferenceEntityBase
	{
        public PreferenceManager PreferenceManager { get; set; }

        private string _imageQuixBaseUrl = null;
        public string ImageQuixBaseUrl
        {
            get { return _imageQuixBaseUrl; }
            set { this.Set<string>("ImageQuixBaseUrl", ref _imageQuixBaseUrl, value); }
        }

		public string ImageQuixCatalogUrl
		{
			get { return _imageQuixBaseUrl +  "/catalog"; }
		}

        private string _getCatalogUsername = null;
        public string GetCatalogUsername
        {
            get { return _getCatalogUsername; }
            set { this.Set<string>("GetCatalogUsername", ref _getCatalogUsername, value); }
        }

        private string _getCatalogPassword = null;
        public string GetCatalogPassword
        {
            get { return _getCatalogPassword; }
            set { this.Set<string>("GetCatalogPassword", ref _getCatalogPassword, value); }
        }

        private bool _allowEditingInitialized = false;
        private bool _allowEditing = false;
        public bool AllowEditing
        {
            get { return _allowEditing; }
            set
            {
                this.Set<bool>("AllowEditing", ref _allowEditing, value);
            }
        }

		internal override void Initialize(XElement upload)
		{
			XElement catalogRequest = upload.Element("CatalogRequest");

			if(this.ImageQuixBaseUrl == null || this.ImageQuixBaseUrl == "")
				this.ImageQuixBaseUrl = "";
            if (this.GetCatalogUsername == null)
                this.GetCatalogUsername = "flow";
            if (this.GetCatalogPassword == null)
                this.GetCatalogPassword = "flow";

            if (!_allowEditingInitialized)
            {
                _allowEditingInitialized = true;
                this.AllowEditing = false;
            }
		}
	}
}
