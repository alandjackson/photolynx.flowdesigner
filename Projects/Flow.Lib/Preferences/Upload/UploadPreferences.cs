﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Linq;

namespace Flow.Lib.Preferences
{
	[Serializable]
	public class UploadPreferences : PreferenceEntityBase
	{
        public PreferenceManager PreferenceManager { get; set; }

		public ProjectTransferPreferences ProjectTransfer { get; set; }
		public CatalogRequestPreferences CatalogRequest { get; set; }
		public OrderSubmissionPreferences OrderSubmission { get; set; }
        public OnlineOrderPreferences OnlineOrder { get; set; }
        

		internal override void Initialize(XElement preferences)
		{
			XElement upload = preferences.Element("Upload");
	
			if(this.ProjectTransfer == null)
				this.ProjectTransfer = new ProjectTransferPreferences();
            this.ProjectTransfer.PreferenceManager = this.PreferenceManager;
			this.ProjectTransfer.Initialize(upload);

			if(this.CatalogRequest == null)
				this.CatalogRequest = new CatalogRequestPreferences();
            this.CatalogRequest.PreferenceManager = this.PreferenceManager;
			this.CatalogRequest.Initialize(upload);

			if(this.OrderSubmission == null)
				this.OrderSubmission = new OrderSubmissionPreferences();
            this.OrderSubmission.PreferenceManager = this.PreferenceManager;
			this.OrderSubmission.Initialize(upload);

            if (this.OnlineOrder == null)
                this.OnlineOrder = new OnlineOrderPreferences();
            this.OnlineOrder.PreferenceManager = this.PreferenceManager;
            this.OnlineOrder.Initialize(upload);
		}
	}
}
