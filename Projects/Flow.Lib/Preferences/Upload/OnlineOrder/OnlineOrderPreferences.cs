﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Flow.Lib.Preferences
{
    [Serializable]
	public class OnlineOrderPreferences : PreferenceEntityBase
    {
        public PreferenceManager PreferenceManager { get; set; }

        private List<int> _completedOrders;
        public List<int> CompletedOrders
        {
            get { return _completedOrders; }
            set { this.Set<List<int>>("CompletedOrders", ref _completedOrders, value); }
        }

        private string _ecommerceAPIURL = "";
        public string EcommerceAPIURL
        {
            get { return _ecommerceAPIURL; }
            set { this.Set<string>("EcommerceAPIURL", ref _ecommerceAPIURL, value); }
        }

        private string _ecommerceAPIKey = "";
        public string EcommerceAPIKey
        {
            get { return _ecommerceAPIKey; }
            set { this.Set<string>("EcommerceAPIKey", ref _ecommerceAPIKey, value); }
        }

        private string _ecommerceUsername = "";
        public string EcommerceUsername
        {
            get { return _ecommerceUsername; }
            set { this.Set<string>("EcommerceUsername", ref _ecommerceUsername, value); }
        }

        private string _ecommercePassword = "";
        public string EcommercePassword
        {
            get { return _ecommercePassword; }
            set { this.Set<string>("EcommercePassword", ref _ecommercePassword, value); }
        }


        private string _imageQuixCustomerID = "";
        public string ImageQuixCustomerID
        {
            get { return _imageQuixCustomerID.ToUpper(); }
            set { this.Set<string>("ImageQuixCustomerID", ref _imageQuixCustomerID, value.ToUpper()); }
        }

        private string _imageQuixCustomerPassword = "";
        public string ImageQuixCustomerPassword
        {
            get { return _imageQuixCustomerPassword; }
            set { this.Set<string>("ImageQuixCustomerPassword", ref _imageQuixCustomerPassword, value); }
        }

        public bool HasImageQuixCustomerCredentials()
        {
            return (_imageQuixCustomerID != null && _imageQuixCustomerID != "" &&
                    _imageQuixCustomerPassword != null && _imageQuixCustomerPassword != "");
        }

        private string _imageQuixApplicationID = "";
        public string ImageQuixApplicationID
        {
            get { return _imageQuixApplicationID; }
            set { this.Set<string>("ImageQuixApplicationID", ref _imageQuixApplicationID, value); }
        }

        public string ImageQuixBaseURL
        {
            //get { return "http://team.imagequix.com"; }
            get { return "http://api.imagequix.com"; }
            //get { return "http://api.dev.imagequix.com"; } 
           
            //get {
            //    if(string.IsNullOrEmpty(this.PreferenceManager.Upload.CatalogRequest.ImageQuixBaseUrl))
            //        return "http://api.imagequix.com";
            //    else
            //    {
            //        Uri uri = new Uri(this.PreferenceManager.Upload.CatalogRequest.ImageQuixBaseUrl);
            //        string host = "http://" + uri.Host;
            //        return host; 
            //    }
            //}
        }

        //
        //private string _imageQuixLoginURL = "https://vando.imagequix.com/rest/cust";
        //
        //private string _imageQuixLoginURL = "http://demo.imagequix.com:8080/osgi/rest/cust";
        public string ImageQuixLoginURL
        {
            get { return ImageQuixBaseURL + "/rest"; }
        }

        public string ImageQuixPublishURL
        {
            get { return ImageQuixBaseURL + "/publisher/rest"; }
        }

        private DateTime _lastGalleryCheck = DateTime.Now.AddYears(-50);
        public DateTime LastGalleryCheck
        {
            get { return _lastGalleryCheck; }
            set { this.Set<DateTime>("LastGalleryCheck", ref _lastGalleryCheck, value); }
        }

        private int _imageQuixEventCounter = 0;
        public int ImageQuixEventCounter
        {
            get { return _imageQuixEventCounter; }
            set { this.Set<int>("ImageQuixEventCounter", ref _imageQuixEventCounter, value); }
        }

        private string _welcomeImagePath = "";
        public string WelcomeImagePath
        {
            get { return _welcomeImagePath; }
            set { this.Set<string>("WelcomeImagePath", ref _welcomeImagePath, value); }
        }

        internal override void Initialize(XElement upload)
        {
            //if (ImageQuixLoginURL == null)
            //ImageQuixLoginURL = "https://vando.imagequix.com/rest/cust";

            if (LastGalleryCheck == null)
                LastGalleryCheck = DateTime.Now.AddYears(-50);

                if (CompletedOrders == null)
                    CompletedOrders = new List<int>();

                if (!(ImageQuixEventCounter >= 0))
                    ImageQuixEventCounter = 0;

                if (WelcomeImagePath == null)
                    WelcomeImagePath = "";
                
        }
    }
}
