﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Linq;

using Flow.Lib.Helpers;
using System.IO;
using Flow.Lib.Net;
using System.Data;
using System.Drawing;

namespace Flow.Lib.Preferences
{
	[Serializable]
	public class ApplicationPreferences : PreferenceEntityBase
	{
        public PreferenceManager PreferenceManager { get; set; }

        public string ProjectLabel_StudioName { get; set; }
        public string ProjectLabel_ProjectTemplate { get; set; }
        public string ProjectLabel_OrganizationName { get; set; }
        public string ProjectLabel_ProjectName { get; set; }
        public string ProjectLabel_EventName { get; set; }
        public string ProjectLabel_CatalogName { get; set; }
        public string ProjectLabel_PhotographerName { get; set; }
        public string ProjectLabel_GreenScreen { get; set; }
        public string ProjectLabel_PhotographyDate { get; set; }
        public string ProjectLabel_Comments { get; set; }
        public string ProjectLabel_PrincipalName { get; set; }
        public string ProjectLabel_DueDate { get; set; }
        public string ProjectLabel_ProjectType { get; set; }
        public string ProjectLabel_PayableTo { get; set; }

        private bool _UseGroupObjectInitialized = false;
        private bool _useGroupObject = false;
        public bool UseGroupObject
        {
            get { return _useGroupObject; }
            set
            {
                this.Set<bool>("UseGroupObject", ref _useGroupObject, value);
            }
        }


        private List<RemoteImageService> _remoteImageServices { get; set; }
        public List<RemoteImageService> RemoteImageServices
        {
            get { return _remoteImageServices; }
            set
            {
                _remoteImageServices = value;
            }
        }

        private DataTable _unsentErrors = null;
        public DataTable UnsentErrors
        {
            get { return _unsentErrors; }
            set
            {
                this.Set<DataTable>("UnsentErrors", ref _unsentErrors, value);
            }
        }
        public string UnsentErrorsCount
        {
            get
            {
                if (UnsentErrors == null)
                    return "0";
                return UnsentErrors.Rows.Count.ToString();
            }
        }
        public void UnsentErrorsClear()
        {
            if (UnsentErrors != null)
                UnsentErrors.Clear();
            SendPropertyChanged("UnsentErrorsCount");
        }
        private string _pingTestURL = null;
        public string PingTestURL
        {
            get { return _pingTestURL; }
            set
            {
                this.Set<string>("PingTestURL", ref _pingTestURL, value);
            }
        }


        private string _supportURL = null;
		public string SupportURL
		{
            get { return _supportURL; }
            set {
				this.Set<string>("SupportURL", ref _supportURL, value);
			}
		}


        private string _ecommerceURL = null;
		public string EcommerceURL
		{
            get { return _ecommerceURL; }
            set {
                this.Set<string>("EcommerceURL", ref _ecommerceURL, value);
			}
		}
        
        public bool ShowEcommerceSignupPanel
        {
            get {
                if (string.IsNullOrEmpty(EcommerceURL))
                    return false;
                return true; 
            }
        }
        

        private string _projectsDirectory = null;
		public string ProjectsDirectory
		{
            get
            {
                return _projectsDirectory;
            }
            set
            {

                this.Set<string>("ProjectsDirectory", ref _projectsDirectory, 
                    value.Replace("@CommonApplicationData", System.Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData))
                    );
            }
		}

        private string _networkProjectsDirectory = null;
        public string NetworkProjectsDirectory
        {
            get
            {
                return _networkProjectsDirectory;
            }
            set
            {

                this.Set<string>("NetworkProjectsDirectory", ref _networkProjectsDirectory, value);
            }
        }

        private string _labOrderQueueDirectory = null;
        public string LabOrderQueueDirectory
        {
            get
            {
                return _labOrderQueueDirectory;
            }
            set
            {

                this.Set<string>("LabOrderQueueDirectory", ref _labOrderQueueDirectory, value);
            }
        }


        private string _projectArchiveDirectory = null;
        public string ProjectArchiveDirectory
        {
            get
            {
                return _projectArchiveDirectory;
            }
            set
            {

                this.Set<string>("ProjectArchiveDirectory", ref _projectArchiveDirectory,
                    value.Replace("@CommonApplicationData", System.Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData))
                    );
            }
        }

        private string _labEmail = null;
        public string LabEmail
        {
            get { return _labEmail; }
            set
            {
                this.Set<string>("LabEmail", ref _labEmail, value);
            }
        }

        private bool _processImagesInBackground = false;
        public bool ProcessImagesInBackground
        {
            get { return _processImagesInBackground; }
            set
            {
                this.Set<bool>("ProcessImagesInBackground", ref _processImagesInBackground, value);
            }
        }

        private bool _canGreenScreen = false;
        public bool CanGreenScreen
        {
            get { return _canGreenScreen; }
            set
            {
                this.Set<bool>("CanGreenScreen", ref _canGreenScreen, value);
            }
        }

        private bool _requireLogin = false;
        public bool RequireLogin
        {
            get { return _requireLogin; }
            set
            {
                this.Set<bool>("RequireLogin", ref _requireLogin, value);
            }
        }

        private bool _IsBasicInitialized = false;
        private bool _isBasic = false;
        public bool IsBasic
        {
            get { return _isBasic; }
            set
            {
                this.Set<bool>("IsBasic", ref _isBasic, value);
            }
        }
        public bool IsNotBasic { 
            get { return !_isBasic; } 
        }

        public bool UsesIQ
        {
            get {
                if (string.IsNullOrEmpty(this.PreferenceManager.Upload.OnlineOrder.ImageQuixCustomerID))
                    return false;
                return true;
            }

        }
        
        private bool _autoPrintLayout = false;
        public bool AutoPrintLayout
        {
            get { return _autoPrintLayout; }
            set
            {
                this.Set<bool>("AutoPrintLayout", ref _autoPrintLayout, value);
            }
        }

        private int _autoPrintLayoutImageCount = 0;
        public int AutoPrintLayoutImageCount
        {
            get { return _autoPrintLayoutImageCount; }
            set
            {
                this.Set<int>("AutoPrintLayoutImageCount", ref _autoPrintLayoutImageCount, value); 
            }
        }

        private bool _CurrentSubjectIndexInitialized = false;
        public int _currentSubjectIndex = 0;
        public int CurrentSubjectIndex
        {
            get { return _currentSubjectIndex; }
            set { this.Set<int>("CurrentSubjectIndex", ref _currentSubjectIndex, value); }
        }

        private bool _searchExactMatch = false;
        public bool SearchExactMatch
        {
            get { return _searchExactMatch; }
            set
            {
                this.Set<bool>("SearchExactMatch", ref _searchExactMatch, value);
            }
        }

        private string _lastCurrentVersion = "";
        public string LastCurrentVersion
        {
            get { return _lastCurrentVersion; }
            set
            {
                this.Set<string>("LastCurrentVersion", ref _lastCurrentVersion, value);
            }
        }

        private bool _serverPorjectsNeedInitialized = true;
        public bool ServerPorjectsNeedInitialized
        {
            get { return _serverPorjectsNeedInitialized; }
            set
            {
                this.Set<bool>("ServerPorjectsNeedInitialized", ref _serverPorjectsNeedInitialized, value);
            }
        }

        private bool _isFlowServer = false;
        public bool IsFlowServer
        {
            get { return _isFlowServer; }
            set
            {
                this.Set<bool>("IsFlowServer", ref _isFlowServer, value);
            }
        }

        public bool IsNotFlowServer
        {
            get { return !IsFlowServer; }
        }

        private bool _isFlowMaster = false;
        public bool IsFlowMaster
        {
            get { return _isFlowMaster; }
            set
            {
                this.Set<bool>("IsFlowMaster", ref _isFlowMaster, value);
            }
        }

        public bool IsNotFlowMaster
        {
            get { return !IsFlowMaster; }
        }

        //key = FlowProjectGuid
        //Value = EventID
        public Dictionary<string, string> ProjectIQEventIDs = new Dictionary<string, string>();

        private bool _showPoseNumber = false;
        public bool ShowPoseNumber
        {
            get { return _showPoseNumber; }
            set
            {
                this.Set<bool>("ShowPoseNumber", ref _showPoseNumber, value);
            }
        }

        private BindingList<string> _recentOrdersPlaced { get; set; }
        public BindingList<string> RecentOrdersPlaced
        {
            get
            {
                return _recentOrdersPlaced;
            }
            set
            {
                _recentOrdersPlaced = value;

            }

        }
        

        private bool _exportDefaultProjectExport = true;
        public bool ExportDefaultProjectExport
        {
            get { return _exportDefaultProjectExport; }
            set
            {
                this.Set<bool>("ExportDefaultProjectExport", ref _exportDefaultProjectExport, value);
            }
        }

        private bool _exportDefaultDataExport = false;
        public bool ExportDefaultDataExport
        {
            get { return _exportDefaultDataExport; }
            set
            {
                this.Set<bool>("ExportDefaultDataExport", ref _exportDefaultDataExport, value);
            }
        }

        private bool _exportDefaultImageMatchExport = false;
        public bool ExportDefaultImageMatchExport
        {
            get { return _exportDefaultImageMatchExport; }
            set
            {
                this.Set<bool>("ExportDefaultImageMatchExport", ref _exportDefaultImageMatchExport, value);
            }
        }

        private bool _exportUploadToLab = false;
        public bool ExportUploadToLab
        {
            get { return _exportUploadToLab; }
            set
            {
                this.Set<bool>("ExportUploadToLab", ref _exportUploadToLab, value);
            }
        }

        private bool _exportSaveLocally = true;
        public bool ExportSaveLocally
        {
            get { return _exportSaveLocally; }
            set
            {
                this.Set<bool>("ExportSaveLocally", ref _exportSaveLocally, value);
            }
        }

        private bool _exportIncludeImages = true;
        public bool ExportIncludeImages
        {
            get { return _exportIncludeImages; }
            set
            {
                this.Set<bool>("ExportIncludeImages", ref _exportIncludeImages, value);
            }
        }

        private bool _exportUncompressed = false;
        public bool ExportUncompressed
        {
            get { return _exportUncompressed; }
            set
            {
                this.Set<bool>("ExportUncompressed", ref _exportUncompressed, value);
            }
        }


        private bool _exportRenderGreenScreen = false;
        public bool ExportRenderGreenScreen
        {
            get { return _exportRenderGreenScreen; }
            set
            {
                this.Set<bool>("ExportRenderGreenScreen", ref _exportRenderGreenScreen, value);
            }
        }
        private bool _exportApplyCrops = true;
        public bool ExportApplyCrops
        {
            get { return _exportApplyCrops; }
            set
            {
                this.Set<bool>("ExportApplyCrops", ref _exportApplyCrops, value);
            }
        }
        private bool _exportUseOriginalFileName = false;
        public bool ExportUseOriginalFileName
        {
            get { return _exportUseOriginalFileName; }
            set
            {
                this.Set<bool>("ExportUseOriginalFileName", ref _exportUseOriginalFileName, value);
            }
        }
        private bool _exportAllImages = false;
        public bool ExportAllImages
        {
            get { return _exportAllImages; }
            set
            {
                this.Set<bool>("ExportAllImages", ref _exportAllImages, value);
            }
        }
        private bool _exportOnlyPrimaryImage = true;
        public bool ExportOnlyPrimaryImage
        {
            get { return _exportOnlyPrimaryImage; }
            set
            {
                this.Set<bool>("ExportOnlyPrimaryImage", ref _exportOnlyPrimaryImage, value);
            }
        }
        private bool _exportGroupPhoto = false;
        public bool ExportGroupPhoto
        {
            get { return _exportGroupPhoto; }
            set
            {
                this.Set<bool>("ExportGroupPhoto", ref _exportGroupPhoto, value);
            }
        }
        private bool _exportPose = true;
        public bool ExportPose
        {
            get { return _exportPose; }
            set
            {
                this.Set<bool>("ExportPose", ref _exportPose, value);
            }
        }

        private string _orderSort1 = "";
        public string OrderSort1
        {
            get { return _orderSort1; }
            set
            {
                this.Set<string>("OrderSort1", ref _orderSort1, value);
            }
        }
        private string _orderSort2 = "";
        public string OrderSort2
        {
            get { return _orderSort2; }
            set
            {
                this.Set<string>("OrderSort2", ref _orderSort2, value);
            }
        }

         private bool _orderSaveLocally = false;
        public bool OrderSaveLocally
        {
            get { return _orderSaveLocally; }
            set
            {
                this.Set<bool>("OrderSaveLocally", ref _orderSaveLocally, value);
            }
        }
         private bool _orderUploadToLab = true;
        public bool OrderUploadToLab
        {
            get { return _orderUploadToLab; }
            set
            {
                this.Set<bool>("OrderUploadToLab", ref _orderUploadToLab, value);
            }
        }
         private bool _orderIncludeAllSubjects = false;
        public bool OrderIncludeAllSubjects
        {
            get { return _orderIncludeAllSubjects; }
            set
            {
                this.Set<bool>("OrderIncludeAllSubjects", ref _orderIncludeAllSubjects, value);
            }
        }
         private bool _orderIncludeSubjectDetailReport = true;
        public bool OrderIncludeSubjectDetailReport
        {
            get { return _orderIncludeSubjectDetailReport; }
            set
            {
                this.Set<bool>("OrderIncludeSubjectDetailReport", ref _orderIncludeSubjectDetailReport, value);
            }
        }
         private bool _reportShowFooter = true;
        public bool ReportShowFooter
        {
            get { return _reportShowFooter; }
            set
            {
                this.Set<bool>("ReportShowFooter", ref _reportShowFooter, value);
            }
        }


         private bool _orderUseOriginalFileNames = true;
        public bool OrderUseOriginalFileNames
        {
            get { return _orderUseOriginalFileNames; }
            set
            {
                this.Set<bool>("OrderUseOriginalFileNames", ref _orderUseOriginalFileNames, value);
            }
        }

        private bool _orderApplyCrops = false;
        public bool OrderApplyCrops
        {
            get { return _orderApplyCrops; }
            set
            {
                this.Set<bool>("OrderApplyCrops", ref _orderApplyCrops, value);
            }
        }

        private bool _orderRenderGreenScreen = true;
        public bool OrderRenderGreenScreen
        {
            get { return _orderRenderGreenScreen; }
            set
            {
                this.Set<bool>("OrderRenderGreenScreen", ref _orderRenderGreenScreen, value);
            }
        }


        private bool _createThumbnails = false;
        public bool CreateThumbnails
        {
            get { return _createThumbnails; }
            set
            {
                this.Set<bool>("CreateThumbnails", ref _createThumbnails, value);
            }
        }

        private bool _enableRemoteImageService = false;
        public bool EnableRemoteImageService
        {
            get { return _enableRemoteImageService; }
            set
            {
                this.Set<bool>("EnableRemoteImageService", ref _enableRemoteImageService, value);
            }
        }

        private string _remoteImageServiceFtp = "";
        public string RemoteImageServiceFtp
        {
            get { return _remoteImageServiceFtp; }
            set
            {
                this.Set<string>("RemoteImageServiceFtp", ref _remoteImageServiceFtp, value);
            }
        }

        private string _remoteImageServiceUser = "";
        public string RemoteImageServiceUser
        {
            get { return _remoteImageServiceUser; }
            set
            {
                this.Set<string>("RemoteImageServiceUser", ref _remoteImageServiceUser, value);
            }
        }

        private string _remoteImageServicePW = "";
        public string RemoteImageServicePW
        {
            get { return _remoteImageServicePW; }
            set
            {
                this.Set<string>("RemoteImageServicePW", ref _remoteImageServicePW, value);
            }
        }

        private bool _forcePUDOrders = false;
        public bool ForcePUDOrders
        {
            get { return _forcePUDOrders; }
            set
            {
                this.Set<bool>("ForcePUDOrders", ref _forcePUDOrders, value);
            }
        }
        private int _thumbSize = 800;
        public int ThumbSize
        {
            get { return _thumbSize; }
            set
            {
                this.Set<int>("ThumbSize", ref _thumbSize, value);
            }
        }

		internal override void Initialize(XElement preferences)
		{
			XElement application = preferences.Element("Application");

            if (this.ThumbSize == null || this.ThumbSize == 0)
                ThumbSize = 800;
			if(this.SupportURL == null)
				this.SupportURL = application.Element("SupportURL").Value;
            if (this.PingTestURL == null)
                this.PingTestURL = "www.google.com";
            if (this.ProjectsDirectory == null)
                this.ProjectsDirectory = application.Element("ProjectsDirectory").Value;
            if (this.ProjectArchiveDirectory == null)
                this.ProjectArchiveDirectory = Path.Combine(System.Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "Flow\\ProjectArchive");
            if (this.LabOrderQueueDirectory == null)
                this.LabOrderQueueDirectory = Path.Combine(System.Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "Flow\\LabOrderQueue");
            if (!Directory.Exists(LabOrderQueueDirectory))
                Directory.CreateDirectory(LabOrderQueueDirectory);

            if (RecentOrdersPlaced == null)
                RecentOrdersPlaced = new BindingList<string>();

            RecentOrdersPlaced.AddingNew += new AddingNewEventHandler(RecentOrdersPlaced_AddingNew);
            
            if (this.UnsentErrors == null || this.UnsentErrors.Rows.Count == 0)
            {
                this.UnsentErrors = new DataTable("Error Email");
                this.UnsentErrors.Columns.Add("subject");
                this.UnsentErrors.Columns.Add("body");
                this.UnsentErrors.Columns.Add("screenShot");
                this.UnsentErrors.Columns.Add("logFile");
                this.UnsentErrors.Columns.Add("ErrorStore");
            }
            //if (this.ProcessImagesInBackground == null)
            //    this.ProcessImagesInBackground = false;

            if (!_UseGroupObjectInitialized)
            {
                UseGroupObject = true;
                _UseGroupObjectInitialized = true;
            }

            if (!_IsBasicInitialized)
            {
                IsBasic = false;
                _IsBasicInitialized = true;
            }

            //IsBasic = true;

            if (AutoPrintLayoutImageCount == 0)
                AutoPrintLayoutImageCount = 1;

            //if (this.AutoPrintLayout == null)
            //this should always start as false
                this.AutoPrintLayout = false;

            //if (this.ProjectsDirectory == null)
            //    this.ProjectsDirectory = FlowContext.FlowProjectsDirPath;
            if (!_CurrentSubjectIndexInitialized)
            {
                _CurrentSubjectIndexInitialized = true;
                this.CurrentSubjectIndex = 0;
            }

            if (ProjectIQEventIDs == null)
                ProjectIQEventIDs = new Dictionary<string, string>();

            if (LastCurrentVersion == null)
                LastCurrentVersion = "";


            //EnableRemoteImageService = false;
            //ForcePUDOrders = true;



            if (ProjectLabel_StudioName == null) { ProjectLabel_StudioName = "Studio"; }
            if (ProjectLabel_ProjectTemplate == null) { ProjectLabel_ProjectTemplate = "Project Template"; }
            if (ProjectLabel_OrganizationName == null) { ProjectLabel_OrganizationName = "Organization"; }
            if (ProjectLabel_ProjectName == null) { ProjectLabel_ProjectName = "Project Name"; }
            if (ProjectLabel_EventName == null) { ProjectLabel_EventName = "Event Name"; }
            if (ProjectLabel_CatalogName == null) { ProjectLabel_CatalogName = "Catalog"; }
            if (ProjectLabel_PhotographerName == null) { ProjectLabel_PhotographerName = "Photographer"; }
            if (ProjectLabel_GreenScreen == null) { ProjectLabel_GreenScreen = "Green Screen"; }
            if (ProjectLabel_PhotographyDate == null) { ProjectLabel_PhotographyDate = "Photography Date"; }
            if (ProjectLabel_Comments == null) { ProjectLabel_Comments = "Comments"; }
            if (ProjectLabel_PrincipalName == null) { ProjectLabel_PrincipalName = "Principal Name"; }
            if (ProjectLabel_DueDate == null) { ProjectLabel_DueDate = "Due Date for Orders"; }
            if (ProjectLabel_ProjectType == null) { ProjectLabel_ProjectType = "Project Type"; }
            if (ProjectLabel_PayableTo == null) { ProjectLabel_PayableTo = "Checks Payable To"; }


		}

        void RecentOrdersPlaced_AddingNew(object sender, AddingNewEventArgs e)
        {
            this.SendPropertyChanged("RecentOrdersPlaced");
        }

        public string GetLocalDatabasePath(Guid projectGuid, bool isNetworkProject, string databaseFileName)
        {
            if (isNetworkProject == true)
                return Path.Combine(this.ProjectsDirectory, databaseFileName);
            return Path.Combine(Path.Combine(this.ProjectsDirectory, projectGuid.ToString()), databaseFileName);
        }

        public string GetFlowProjectDirPath(Guid projectGuid, bool isNetworkProject)
        {
            if (isNetworkProject == true)
                return Path.Combine(this.NetworkProjectsDirectory, projectGuid.ToString());
            return Path.Combine(this.ProjectsDirectory, projectGuid.ToString()); 
        }

        public string GetFlowProjectImageDirPath(Guid projectGuid, bool isNetworkProject)
        { return FlowContext.InitDir(Path.Combine(this.GetFlowProjectDirPath(projectGuid, isNetworkProject), "Image")); }




        public double Width { get; set; }

        public double Height { get; set; }

        public System.Windows.WindowState DefaultWindowState { get; set; }
    }

    [Serializable]
    public class RemoteImageService
    {
        public string Name { get; set; }
        public string Ftp { get; set; }
        public string Port { get; set; }

        public RemoteImageService()
        {
        }
        public RemoteImageService(string n, string f, string p)
        {
            Name = n;
            Ftp = f;
            Port = p;
        }
    }
}