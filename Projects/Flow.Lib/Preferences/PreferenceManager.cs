﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Linq;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Xml.Linq;
using System.Data;

namespace Flow.Lib.Preferences
{
	/// <summary>
	/// Object for managing application Preferences
	/// </summary>
	[Serializable]
	public sealed class PreferenceManager : PreferenceEntityBase
	{
		/// <summary>
		/// Returns an instance of PreferenceManager restored from a serialized instance
		/// </summary>
		/// <param name="data"></param>
		/// <returns></returns>
		public static PreferenceManager GetPreferenceManager(Binary data)
		{
			PreferenceManager retVal;

			using (MemoryStream stream = new MemoryStream(data.ToArray()))
			{
				try
				{
					retVal = (PreferenceManager)(new BinaryFormatter()).Deserialize(stream);
				}
				catch (Exception e)
				{
					retVal = null;
				}
			}

			return retVal;
		}

		public ApplicationPreferences Application { get; set; }

		public CapturePreferences Capture { get; set; }

		public UploadPreferences Upload { get; set; }

		public LayoutPreferences Layout { get; set; }

		public EditPreferences Edit { get; set; }

		public OrdersPreferences Orders { get; set; }

        public PermissionPreferences Permissions { get; set; }

		public PreferenceManager()
		{
			XElement defaultPreferences = XDocument.Load(FlowContext.FlowDefaultPreferencesFilePath).Element("Preferences");
			this.Initialize(defaultPreferences);
		}

		[OnDeserialized()]
		protected void OnDeserializedMethod(StreamingContext context)
		{
			XElement defaultPreferences = XDocument.Load(FlowContext.FlowDefaultPreferencesFilePath).Element("Preferences");
			this.Initialize(defaultPreferences);
		}

		/// <summary>
		/// Instantiates sub-preference objects if they are not loaded from a serialized instance
		/// </summary>
		internal override void Initialize(XElement preferences)
		{
            
			if (this.Application == null)
				this.Application = new ApplicationPreferences();
            this.Application.PreferenceManager = this;
			this.Application.Initialize(preferences);

			if (this.Capture == null)
				this.Capture = new CapturePreferences();
            this.Capture.PreferenceManager = this;
			this.Capture.Initialize(preferences);

			if (this.Upload == null)
				this.Upload = new UploadPreferences();
            this.Upload.PreferenceManager = this;
			this.Upload.Initialize(preferences);

			if (this.Layout == null)
				this.Layout = new LayoutPreferences();
            this.Layout.PreferenceManager = this;
			this.Layout.Initialize(preferences);

			if (this.Edit == null)
				this.Edit = new EditPreferences();
            this.Edit.PreferenceManager = this;
			this.Edit.Initialize(preferences);

			if (this.Orders == null)
				this.Orders = new OrdersPreferences();
            this.Orders.PreferenceManager = this;
			this.Orders.Initialize(preferences);

            if (this.Permissions == null)
                this.Permissions = new PermissionPreferences();
            this.Permissions.PreferenceManager = this;
            this.Permissions.Initialize(preferences);
		}

		/// <summary>
		/// Serializes the PreferenceManager instance and returns the data for
		/// database storage
		/// </summary>
		/// <returns></returns>
		public Binary GetSerializationData()
		{
			Binary data;

			using (MemoryStream stream = new MemoryStream())
			{
				BinaryFormatter formatter = new BinaryFormatter();
				formatter.Serialize(stream, this);
				data = stream.ToArray();
			}

			return data;
		}

        public void Load(DataSet ds)
        {
            Load(ds, true);
        }
        public void Load(DataSet ds, bool fullUpdate)
        {
            //fullUpdate only happen on first activation
            //if fullUpdate is false, than this is the updatecheck, so only force update a few specific fields.

            DataTable dt = ds.Tables[0];

            if (dt.Rows.Count == 0)
                return;

            DataRow dr = dt.Rows[0];
            
            this.Application.SupportURL = Convert.ToString(dr["SupportURL"]);
            this.Application.LabEmail = Convert.ToString(dr["LabEmail"]);

            if(dt.Columns.Contains("RequireLogin"))
                this.Application.RequireLogin = Convert.ToBoolean(dr["RequireLogin"]);

            if (dt.Columns.Contains("EcommerceURL"))
                this.Application.EcommerceURL = Convert.ToString(dr["EcommerceURL"]);

            if (!string.IsNullOrEmpty(Convert.ToString(dr["ProjectLabel_StudioName"])))
                this.Application.ProjectLabel_StudioName = Convert.ToString(dr["ProjectLabel_StudioName"]);
            if (!string.IsNullOrEmpty(Convert.ToString(dr["ProjectLabel_ProjectTemplate"])))
                this.Application.ProjectLabel_ProjectTemplate = Convert.ToString(dr["ProjectLabel_ProjectTemplate"]);
            if (!string.IsNullOrEmpty(Convert.ToString(dr["ProjectLabel_OrganizationName"])))
                this.Application.ProjectLabel_OrganizationName = Convert.ToString(dr["ProjectLabel_OrganizationName"]);
            if (!string.IsNullOrEmpty(Convert.ToString(dr["ProjectLabel_ProjectName"])))
                this.Application.ProjectLabel_ProjectName = Convert.ToString(dr["ProjectLabel_ProjectName"]);
            if (!string.IsNullOrEmpty(Convert.ToString(dr["ProjectLabel_EventName"])))
                this.Application.ProjectLabel_EventName = Convert.ToString(dr["ProjectLabel_EventName"]);
            if (!string.IsNullOrEmpty(Convert.ToString(dr["ProjectLabel_CatalogName"])))
                this.Application.ProjectLabel_CatalogName = Convert.ToString(dr["ProjectLabel_CatalogName"]);
            if (!string.IsNullOrEmpty(Convert.ToString(dr["ProjectLabel_PhotographerName"])))
                this.Application.ProjectLabel_PhotographerName = Convert.ToString(dr["ProjectLabel_PhotographerName"]);
            if (!string.IsNullOrEmpty(Convert.ToString(dr["ProjectLabel_GreenScreen"])))
                this.Application.ProjectLabel_GreenScreen = Convert.ToString(dr["ProjectLabel_GreenScreen"]);
            if (!string.IsNullOrEmpty(Convert.ToString(dr["ProjectLabel_PhotographyDate"])))
                this.Application.ProjectLabel_PhotographyDate = Convert.ToString(dr["ProjectLabel_PhotographyDate"]);
            if (!string.IsNullOrEmpty(Convert.ToString(dr["ProjectLabel_Comments"])))
                this.Application.ProjectLabel_Comments = Convert.ToString(dr["ProjectLabel_Comments"]);
            if (!string.IsNullOrEmpty(Convert.ToString(dr["ProjectLabel_PrincipalName"])))
                this.Application.ProjectLabel_PrincipalName = Convert.ToString(dr["ProjectLabel_PrincipalName"]);
            if (!string.IsNullOrEmpty(Convert.ToString(dr["ProjectLabel_DueDate"])))
                this.Application.ProjectLabel_DueDate = Convert.ToString(dr["ProjectLabel_DueDate"]);
            if (!string.IsNullOrEmpty(Convert.ToString(dr["ProjectLabel_ProjectType"])))
                this.Application.ProjectLabel_ProjectType = Convert.ToString(dr["ProjectLabel_ProjectType"]);
            if (!string.IsNullOrEmpty(Convert.ToString(dr["ProjectLabel_PayableTo"])))
                this.Application.ProjectLabel_PayableTo = Convert.ToString(dr["ProjectLabel_PayableTo"]);

            this.Application.ExportDefaultProjectExport = Convert.ToBoolean(dr["ExportDefaultProjectExport"]);
            this.Application.ExportDefaultDataExport = Convert.ToBoolean(dr["ExportDefaultDataExport"]);
            this.Application.ExportDefaultImageMatchExport = Convert.ToBoolean(dr["ExportDefaultImageMatchExport"]);
            this.Application.ExportUploadToLab = Convert.ToBoolean(dr["ExportUploadToLab"]);
            this.Application.ExportSaveLocally = Convert.ToBoolean(dr["ExportSaveLocally"]);
            this.Application.ExportIncludeImages = Convert.ToBoolean(dr["ExportIncludeImages"]);
            this.Application.ExportUncompressed = Convert.ToBoolean(dr["ExportUncompressed"]);

            this.Application.ExportRenderGreenScreen = Convert.ToBoolean(dr["ExportRenderGreenScreen"]);
            this.Application.ExportApplyCrops = Convert.ToBoolean(dr["ExportApplyCrops"]);
            this.Application.ExportUseOriginalFileName = Convert.ToBoolean(dr["ExportUseOriginalFileName"]);
            this.Application.ExportAllImages = Convert.ToBoolean(dr["ExportAllImages"]);
            this.Application.ExportOnlyPrimaryImage = Convert.ToBoolean(dr["ExportOnlyPrimaryImage"]);
            this.Application.ExportGroupPhoto = Convert.ToBoolean(dr["ExportGroupPhoto"]);
            this.Application.ExportPose = Convert.ToBoolean(dr["ExportPose"]);

            this.Application.OrderSort1 = Convert.ToString(dr["OrderSort1"]);
            this.Application.OrderSort2 = Convert.ToString(dr["OrderSort2"]);
            this.Application.OrderIncludeAllSubjects = Convert.ToBoolean(dr["OrderIncludeAllSubjects"]);
            this.Application.OrderIncludeSubjectDetailReport = Convert.ToBoolean(dr["OrderIncludeSubjectDetailReport"]);
            this.Application.OrderSaveLocally = Convert.ToBoolean(dr["OrderSaveLocally"]);
            this.Application.OrderUploadToLab = Convert.ToBoolean(dr["OrderUploadToLab"]);
            this.Application.OrderUseOriginalFileNames = Convert.ToBoolean(dr["OrderUseOriginalFileNames"]);
            this.Application.OrderApplyCrops = Convert.ToBoolean(dr["OrderApplyCrops"]);
            this.Application.OrderRenderGreenScreen = Convert.ToBoolean(dr["OrderRenderGreenScreen"]);

            
            this.Application.ReportShowFooter = Convert.ToBoolean(dr["ReportShowFooter"]);

            this.Permissions.Permission_CanModifyUseOriginalFileName = Convert.ToBoolean(dr["CanModifyUseOriginalFileName"]);

            this.Permissions.AdminPassword = Convert.ToString(dr["AdminPassword"]);
            //this.Permissions.IsAdmin = Convert.ToBoolean(dr["IsAdmin"]);
            this.Permissions.IsAdmin = false;

            //force these to update everytime (IF THEY ARE SET TO TRUE ON SERVER)
            if (Convert.ToBoolean(dr["CaptureTakePicture"]))
                this.Capture.CaptureTakePicture = Convert.ToBoolean(dr["CaptureTakePicture"]);
            if (Convert.ToBoolean(dr["OneImagePerSubject"]))
                this.Capture.OneImagePerSubject = Convert.ToBoolean(dr["OneImagePerSubject"]);

            if (fullUpdate || this.Upload.CatalogRequest.AllowEditing == false)
            {
                this.Upload.CatalogRequest.ImageQuixBaseUrl = Convert.ToString(dr["ImageQuixBaseUrl"]);
                this.Upload.CatalogRequest.GetCatalogUsername = Convert.ToString(dr["GetCatalogUsername"]);
                this.Upload.CatalogRequest.GetCatalogPassword = Convert.ToString(dr["GetCatalogPassword"]);

                this.Upload.OrderSubmission.ImageQuixFtpAddress = Convert.ToString(dr["ImageQuixFtpAddress"]);
                this.Upload.OrderSubmission.ImageQuixFtpPort = Convert.ToInt32(dr["ImageQuixFtpPort"]);
                this.Upload.OrderSubmission.ImageQuixFtpUserName = Convert.ToString(dr["ImageQuixFtpUserName"]);
                this.Upload.OrderSubmission.ImageQuixFtpPassword = Convert.ToString(dr["ImageQuixFtpPassword"]);
                this.Upload.OrderSubmission.ImageQuixFtpDirectory = Convert.ToString(dr["ImageQuixFtpDirectory"]);
                this.Upload.OrderSubmission.ImageQuixUseSftp = Convert.ToBoolean(dr["ImageQuixUseSftp"]);
            }
            if (fullUpdate || this.Upload.ProjectTransfer.AllowEditing == false)
            {
                this.Upload.ProjectTransfer.HostAddress = Convert.ToString(dr["HostAddress"]);
                this.Upload.ProjectTransfer.HostPort = Convert.ToInt32(dr["HostPort"]);
                this.Upload.ProjectTransfer.UserName = Convert.ToString(dr["UserName"]);
                this.Upload.ProjectTransfer.Password = Convert.ToString(dr["Password"]);
                this.Upload.ProjectTransfer.UseSftp = Convert.ToBoolean(dr["UseSftp"]);
                this.Upload.ProjectTransfer.RemoteDirectory = Convert.ToString(dr["RemoteDirectory"]);
                this.Upload.ProjectTransfer.RemoteGraphicsDirectory = Convert.ToString(dr["RemoteGraphicsDirectory"]);
                this.Upload.ProjectTransfer.RemoteLayoutsDirectory = Convert.ToString(dr["RemoteLayoutsDirectory"]);
                this.Upload.ProjectTransfer.RemoteOverlaysDirectory = Convert.ToString(dr["RemoteOverlaysDirectory"]);
                this.Upload.ProjectTransfer.RemoteReportsDirectory = Convert.ToString(dr["RemoteReportsDirectory"]);
                if (string.IsNullOrEmpty(this.Upload.ProjectTransfer.RemoteReportsDirectory))
                    this.Upload.ProjectTransfer.RemoteReportsDirectory = "Reports";
                this.Upload.ProjectTransfer.RemoteBackgroundsDirectory = Convert.ToString(dr["RemoteBackgroundsDirectory"]);
                this.Upload.ProjectTransfer.RemoteImageMatchDirectory = Convert.ToString(dr["RemoteImageMatchDirectory"]);
            }

            if (fullUpdate)
            {
                this.Application.ProjectsDirectory = Convert.ToString(dr["ProjectsDirectory"]);
                this.Application.ProjectArchiveDirectory = Convert.ToString(dr["ArchiveDirectory"]);

                this.Capture.UseDefaultHotFolderDirectory = Convert.ToBoolean(dr["UseDefaultHotFolderDirectory"]);
                this.Capture.HotFolderDirectory = Convert.ToString(dr["HotFolderDirectory"]);
                this.Capture.UseOverlayPng = Convert.ToBoolean(dr["UseOverlayPng"]);
                this.Capture.UseDefaultOverlayPngPath = Convert.ToBoolean(dr["UseDefaultOverlayPngPath"]);
                this.Capture.OverlayPngPath = Convert.ToString(dr["OverlayPngPath"]);
                this.Capture.OneImagePerSubject = Convert.ToBoolean(dr["OneImagePerSubject"]);
                this.Capture.CaptureTakePicture = Convert.ToBoolean(dr["CaptureTakePicture"]);
                this.Capture.UseHotFolderArchive = Convert.ToBoolean(dr["UseHotFolderArchive"]);
                this.Capture.MrGrayAdjustmentMethod = Convert.ToString(dr["MrGrayAdjustmentMethod"]);
                this.Capture.MrGrayShowRGBWarning = Convert.ToBoolean(dr["MrGrayShowRGBWarning"]);
                this.Capture.MrGrayRGBWarningLevel = Convert.ToString(dr["MrGrayRGBWarningLevel"]);
                this.Capture.MrGrayTargetBrightness = Convert.ToString(dr["MrGrayTargetBrightness"]);
                this.Capture.MrGrayTolerance = Convert.ToString(dr["MrGrayTolerance"]);

                this.Layout.LayoutsDirectory = Convert.ToString(dr["LayoutsDirectory"]);
                this.Layout.GraphicsDirectory = Convert.ToString(dr["GraphicsDirectory"]);
                this.Layout.Designer.UseDefaultImageOutputFolderPath = Convert.ToBoolean(dr["UseDefaultImageOutputFolderPath"]);
                this.Layout.Designer.ImageOutputFolderPath = Convert.ToString(dr["ImageOutputFolderPath"]);
                this.Layout.Designer.ImageDPI = Convert.ToInt32(dr["ImageDPI"]);
                this.Layout.Designer.ImageFormat = Convert.ToString(dr["ImageFormat"]);
                this.Layout.Designer.PrintDestination = Convert.ToInt32(dr["PrintDestination"]);

                this.Edit.ExternalEditorPath = Convert.ToString(dr["ExternalEditorPath"]);
                this.Edit.OverlaysDirectory = Convert.ToString(dr["OverlaysDirectory"]);
                this.Edit.ReportsDirectory = Convert.ToString(dr["ReportsDirectory"]);
                this.Edit.BackgroundsDirectory = Convert.ToString(dr["BackgroundsDirectory"]);
                this.Edit.PremiumBackgroundPrice = Convert.ToString(dr["PremiumBackgroundPrice"]);

                this.Orders.CreditCardTypeCollection.Clear();
                String[] cctypes = Convert.ToString(dr["CreditCardTypes"]).Split(',');
                foreach (string cctype in cctypes)
                {
                    this.Orders.CreditCardTypeCollection.Add(new Flow.Schema.LinqModel.DataContext.CreditCardType(cctype.Trim()));
                }
            

                
                this.Permissions.Permission_CanModifyPermissions = Convert.ToBoolean(dr["CanModifyPermissions"]);
                this.Permissions.Permission_CanModifyProjectFolderLocation = Convert.ToBoolean(dr["CanModifyProjectFolderLocation"]);
                this.Permissions.Permission_CanModifySupportURL = Convert.ToBoolean(dr["CanModifySupportURL"]);
                this.Permissions.Permission_CanViewPreferences = Convert.ToBoolean(dr["CanViewPreferences"]);
                this.Permissions.Permission_CanEditCatalogs = Convert.ToBoolean(dr["CanEditCatalogs"]);
                this.Permissions.Permission_CanViewCatalogs = Convert.ToBoolean(dr["CanViewCatalogs"]);
                this.Permissions.Permission_CanCreateNewProjects = Convert.ToBoolean(dr["CanCreateNewProjects"]);
                this.Permissions.Permission_CanModifyExistingProjectPreferences = Convert.ToBoolean(dr["CanModifyExistingProjectPreferences"]);
                this.Permissions.Permission_CanViewProjectTemplates = Convert.ToBoolean(dr["CanViewProjectTemplates"]);
                this.Permissions.Permission_CanExportProjects = Convert.ToBoolean(dr["CanExportProjects"]);
                this.Permissions.Permission_CanImportMergeProjects = Convert.ToBoolean(dr["CanImportMergeProjects"]);

                this.Permissions.Permission_CanModifyHotFolderLocation = Convert.ToBoolean(dr["CanModifyHotFolderLocation"]);
                this.Permissions.Permission_CanModifyMrGraySettings = Convert.ToBoolean(dr["CanModifyMrGraySettings"]);
                this.Permissions.Permission_CanChangeCropGuideImage = Convert.ToBoolean(dr["CanChangeCropGuideImage"]);
                this.Permissions.Permission_CanModifySubjectData = Convert.ToBoolean(dr["CanModifySubjectData"]);
                this.Permissions.Permission_CanAddSubjects = Convert.ToBoolean(dr["CanAddSubjects"]);
                this.Permissions.Permission_CanDeleteSubjects = Convert.ToBoolean(dr["CanDeleteSubjects"]);
                this.Permissions.Permission_CanAssignImagesToSubjects = Convert.ToBoolean(dr["CanAssignImagesToSubjects"]);
                this.Permissions.Permission_CanUnassignImagesFromSubjects = Convert.ToBoolean(dr["CanUnassignImagesFromSubjects"]);
                this.Permissions.Permission_CanFlagImageAs = Convert.ToBoolean(dr["CanFlagImageAs"]);

                this.Permissions.Permission_CanChangeExternalEditingApplication = Convert.ToBoolean(dr["CanChangeExternalEditingApplication"]);
                this.Permissions.Permission_CanViewImageAdjustments = Convert.ToBoolean(dr["CanViewImageAdjustments"]);
                this.Permissions.Permission_CanCropImages = Convert.ToBoolean(dr["CanCropImages"]);
                this.Permissions.Permission_CanImportData = Convert.ToBoolean(dr["CanImportData"]);
                this.Permissions.Permission_CanDeleteImages = Convert.ToBoolean(dr["CanDeleteImages"]);

                this.Permissions.Permission_CanModifyLayoutAndGraphicsLocation = Convert.ToBoolean(dr["CanModifyLayoutAndGraphicsLocation"]);
                this.Permissions.Permission_CanCreateEditLayouts = Convert.ToBoolean(dr["CanCreateEditLayouts"]);

                this.Permissions.Permission_CanAddEditDeleteCreditCards = Convert.ToBoolean(dr["CanAddEditDeleteCreditCards"]);

                this.Permissions.Permission_CanAddEditDeleteOrganizations = Convert.ToBoolean(dr["CanAddEditDeleteOrganizations"]);

                this.Permissions.Permission_CanModifyUseOriginalFileName = Convert.ToBoolean(dr["CanModifyUseOriginalFileName"]);

                this.Permissions.Permission_CanAddEditDeleteProjectTemplates = Convert.ToBoolean(dr["CanAddEditDeleteProjectTemplates"]);
                this.Permissions.Permission_CanModifySubjectFields = Convert.ToBoolean(dr["CanModifySubjectFields"]);
                this.Permissions.Permission_CanModifyImageSaveOptions = Convert.ToBoolean(dr["CanModifyImageSaveOptions"]);
                this.Permissions.Permission_CanModifyEventTriggers = Convert.ToBoolean(dr["CanModifyEventTriggers"]);
                this.Permissions.Permission_CanModifyBarcodeSettings = Convert.ToBoolean(dr["CanModifyBarcodeSettings"]);

                this.Permissions.Permission_CanModifyFtpConnections = Convert.ToBoolean(dr["CanModifyFtpConnections"]);
                this.Permissions.Permission_CanModifyImageQuixCatalogOptions = Convert.ToBoolean(dr["CanModifyImageQuixCatalogOptions"]);

                this.Permissions.Permission_CanViewReports = Convert.ToBoolean(dr["CanViewReports"]);
                this.Permissions.Permission_CanEditReports = Convert.ToBoolean(dr["CanEditReports"]);
                this.Permissions.Permission_CanSaveReports = Convert.ToBoolean(dr["CanSaveReports"]);

                this.Permissions.Permission_CanViewUsers = Convert.ToBoolean(dr["CanViewUsers"]);
                this.Permissions.Permission_CanAddEditDeleteUsers = Convert.ToBoolean(dr["CanAddEditDeleteUsers"]);
            }
            
        }



	}	// END class

    [Serializable]
    public class ExifField
    {
        public string Id { get; set; }
        public string Type { get; set; }
        public string Value { get; set; }
        public string DisplayString
        {
            get
            {
                return String.Format("{0,-" + (10 - Id.Length) + "}", Id) + " : " + String.Format("{0,-" + (30 - Type.Length) + "}", Type) + " : " + Value;
            }
        }
        public ExifField(string _id, string _type, string _value)
        {
            Id = _id;
            Type = _type;
            Value = _value;
        }
    }

}		// END namespace
