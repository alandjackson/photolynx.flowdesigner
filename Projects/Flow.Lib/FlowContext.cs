﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Flow.Lib.Helpers;
using Flow.Lib.Properties;

using NLog;

namespace Flow.Lib
{
	public static class FlowContext
	{
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public static bool ShowNewFeatures { get; set; }

		public static readonly string FlowAppDataDirPath
			= FlowContext.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "Flow");

		public static readonly string FlowCommonAppDataDirPath
        = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + @"\Flow\";

		public static string FlowConfigDirPath
			{ get { return FlowContext.Combine(FlowAppDataDirPath, "Config"); } }

		public static string FlowDefaultPreferencesFilePath
		{ get { return FlowContext.Combine(FlowConfigDirPath, "DefaultPreferences.xml"); } }

        //public static string FlowMasterFilePath
        //{ get { return FlowContext.Combine(FlowConfigDirPath, "FlowMaster.sdf"); } }

		public static string FlowProjectTemplateFilePath
		{ get { return FlowContext.Combine(FlowConfigDirPath, "FlowProjectTemplate.sdf"); } }

		public static string FlowDbInstallDirPath
		{ get { return Path.Combine(FlowConfigDirPath, "DbInstall"); } }

		public static string FlowMasterInstallFilePath
		{ get { return FlowContext.Combine(FlowDbInstallDirPath, "FlowMaster.sdf"); } }

		public static string FlowProjectTemplateInstallFilePath
		{ get { return FlowContext.Combine(FlowDbInstallDirPath, "FlowProjectTemplate.sdf"); } }

		public static string FlowHotFolderDirPath
			{ get { return FlowContext.Combine(FlowAppDataDirPath, "HotFolder"); } }

		public static string FlowLogsDirPath
			{ get { return FlowContext.Combine(FlowAppDataDirPath, "Logs"); } }

		public static string FlowProjectsDirPath
			{ get { return FlowContext.Combine(FlowAppDataDirPath, "Projects"); } }

		public static string FlowTempDirPath
			{ get { return FlowContext.Combine(FlowAppDataDirPath, "Temp"); } }

		public static string FlowLayoutsDirPath
            { get { return FlowContext.Combine(FlowAppDataDirPath, "Layouts"); } }

        public static string FlowGraphicsDirPath
            { get { return FlowContext.Combine(FlowAppDataDirPath, "Graphics"); } }
        public static string FlowBackgroundsDirPath
        { get { return FlowContext.Combine(FlowAppDataDirPath, "Backgrounds"); } }

		public static string FlowReportsDirPath
		{ get { return FlowContext.Combine(FlowAppDataDirPath, "Reports"); } }

		public static string FlowDefaultOverlayPngPath
		{ get { return FlowContext.Combine(FlowGraphicsDirPath, "crop_overlay.png"); } }

		public static string FlowOutputDirPath
		{ get { return FlowContext.Combine(FlowAppDataDirPath, "Output"); } }

		public static string GetFlowProjectDirPath(Guid projectGuid)
		    { return FlowContext.Combine(FlowProjectsDirPath, projectGuid.ToString()); }

		public static string GetFlowProjectImageDirPath(Guid projectGuid)
		    { return FlowContext.InitDir(Path.Combine(GetFlowProjectDirPath(projectGuid), "Image")); }

		public static string MakeTempSubDirectory(string subDirectoryName)
		{
			return Path.Combine(FlowContext.FlowTempDirPath, subDirectoryName);
		}


        public static string GetFlowVersion()
        {
            return System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }

		public static string FlowVersion
		{
			get { return System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString(); }
		}

		public static void ClearTempDirectory()
		{
			DirectoryInfo dirInfo = new DirectoryInfo(FlowContext.FlowTempDirPath);
            if (!dirInfo.Exists)
            {
                dirInfo.Create();
                logger.Info("Initialized temp directory");
                return;
            }

			dirInfo.Clear();
            logger.Info("Cleared temp directory");
		}

        public static string InitDir(string src)
        {
            if (!Directory.Exists(src))
            {
                Directory.CreateDirectory(src);
                logger.Info("Initialized directory '{0}'", src);
            }

            return src;
        }

		public static string Combine(string string1, string string2)
		{
			string retVal = Path.Combine(string1, string2);

			FileInfo fileInfo = new FileInfo(retVal);

			FlowContext.InitDir(fileInfo.DirectoryName);

			return retVal;
		}

		public static string CombineDir(string string1, string string2)
		{
			string retVal = Path.Combine(string1, string2);

			return FlowContext.InitDir(retVal);
		}
	}
}
