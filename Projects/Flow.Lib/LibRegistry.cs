﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StructureMap.Configuration.DSL;
using Flow.Lib.Persister;
using Flow.Lib.UnitOfWork;
using Flow.Lib.AsyncWorker;
using Flow.Lib.Exceptions;

namespace Flow.Lib
{
    public class LibRegistry : Registry
    {
        public LibRegistry()
        {
            // Flow.lib

            ForRequestedType<ConfigRepository>().TheDefaultIsConcreteType<ConfigRepository>();
            ForRequestedType<HotFolderWatcher>().TheDefaultIsConcreteType<HotFolderWatcher>();
            ForRequestedType<BackgroundWorker>().TheDefaultIsConcreteType<BackgroundWorker>();
            ForRequestedType<BackgroundWorker>().TheDefaultIsConcreteType<BackgroundWorker>();

            ForRequestedType<IConfigDataPersister>()
                .TheDefaultIsConcreteType<XmlAppDataPersister>();
            ForRequestedType<IUnitOfWorkProcessor>()
                .TheDefaultIsConcreteType<UnitOfWorkProcessor>();

            ForRequestedType<UnitOfWorkBatch>()
                .TheDefaultIsConcreteType<UnitOfWorkBatch>();
        }
    }

}
