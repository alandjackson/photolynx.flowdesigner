﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Flow.Lib.Helpers.BarcodeScan
{
	public class BarcodeScanDetector
	{
		public string InputBuffer { get; private set; }

		private string _initiationSequence;
		private string _terminationSequence;
		private string _scanKeyPattern;
		
		public readonly int ScanValueStartIndex;
		public readonly int ScanValueLength;
		public readonly int MaxLength;

		private Regex _scanKeyRegEx;


		public BarcodeScanDetector(
			string initiationSequence,
			string terminationSequence,
			int? scanValueLength
		)
		{
			this.InputBuffer = "";

			_initiationSequence = (initiationSequence == null) ? "" : initiationSequence;
			_terminationSequence = (terminationSequence == null) ? "" : terminationSequence;

			// Eliminate termination characters beyond a carriage return
			int carriageReturnIndex = _terminationSequence.IndexOf("\r");
			if (carriageReturnIndex > -1)
			{
				_terminationSequence = _terminationSequence.Left(carriageReturnIndex + 1);
			}

			this.ScanValueStartIndex = _initiationSequence.Length;
			this.ScanValueLength = scanValueLength.HasValue ? scanValueLength.Value : 0;

			// Assemble the regex pattern for matching input text; this consists of a 
			_scanKeyPattern =
				"^" +
				Regex.Escape(_initiationSequence) +
				"[0-9a-zA-Z]{" + this.ScanValueLength + "}" +
				Regex.Escape(_terminationSequence) +
				"$"
			;

			_scanKeyRegEx = new Regex(_scanKeyPattern);

			this.MaxLength = this.ScanValueStartIndex + this.ScanValueLength + _terminationSequence.Length;
		}

		/// <summary>
		/// Tests the supplied input text against the barcode scan key pattern assigned to the project
		/// </summary>
		/// <param name="inputText"></param>
		public string DetectScan(string inputText)
		{
			if (this.ScanValueLength == 0)
				return null;
			
			string retVal = null;

			// Truncate the input buffer so it is as long as the maximum possible length for a string
			// matching the pattern
			this.InputBuffer = (this.InputBuffer + inputText).Right(this.MaxLength);

			// If the input text matches the pattern
			if (_scanKeyRegEx.IsMatch(this.InputBuffer))
			{
				// Extract the embedded scan value
				retVal = this.InputBuffer.Substring(this.ScanValueStartIndex,this.ScanValueLength);

				// Clear the imput buffer
				this.InputBuffer = "";
			}

			return retVal;
		}

	}	// END class

}	// END namespace
