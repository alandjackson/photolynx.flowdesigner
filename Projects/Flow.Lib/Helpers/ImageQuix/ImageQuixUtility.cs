﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Xml;
using System.Xml.Linq;

using Flow.Lib.Helpers;
using NetServ.Net.Json;
using System.Web.Script.Serialization;
using NLog;

namespace Flow.Lib.Helpers.ImageQuix
{
	public static class ImageQuixUtility
	{


        //public static string CatalogUsername = null;
        //public static string CatalogPassword = null;

        private static Logger logger = LogManager.GetCurrentClassLogger();

		public static XDocument GetImageQuixDocument(string uri)
		{
			HttpWebRequest rq = (HttpWebRequest)WebRequest.Create(uri);
            rq.Timeout = 10000;//only try for 10 seconds
			HttpWebResponse rs = (HttpWebResponse)rq.GetResponse();

			Stream s = rs.GetResponseStream();

			XmlReader rdr = new XmlTextReader(s);

			XDocument retVal = XDocument.Load(rdr);

			s.Close();
            rs.Close();
			return retVal;
		}

        public static JsonArray GetImageQuixJsonDocumentOld(string uri, string nodeName, string uname, string pword)
        {
            logger.Info("v " + uri);
            HttpWebRequest rq = (HttpWebRequest)WebRequest.Create(uri);
            rq.Credentials = new NetworkCredential(uname, pword);
            rq.Headers.Add("x-iq-token", "A15M876TJ8");
            rq.Headers.Add("x-iq-response-details", "fullNested");
            rq.Headers.Add("x-iq-user", uname);
            rq.Timeout = 10000;//only try for 10 seconds
             HttpWebResponse rs;
            try
            {
               rs = (HttpWebResponse)rq.GetResponse();
            }
            catch (WebException e)
            {
                
                throw e;
            }

            //Stream s = rs.GetResponseStream();
            StreamReader reader = new StreamReader(rs.GetResponseStream());
            string jsonText = reader.ReadToEnd();

            reader.Close();
           
            JsonParser parser = new JsonParser(new StringReader(jsonText), true);
            NetServ.Net.Json.JsonParser.TokenType nextToken = parser.NextToken();
            JsonObject rootObject = parser.ParseObject();
            
            JsonArray catalogArray = null;
            try
            {
                if (((JsonArray)rootObject[nodeName]).Count > 0)
                    catalogArray = (JsonArray)rootObject[nodeName];
            }
            catch
            {
                //do nothinig for now
            }

            //JsonObject catalogs = parser.ParseObject();
            rs.Close();
            return catalogArray;
        }


        
        public static JsonArray GetImageQuixJsonDocumentNew(string uri, string nodeName, string uname, string pword)
        {
            logger.Info("w " + uri);
            HttpWebRequest rq = (HttpWebRequest)WebRequest.Create(uri);
            rq.Credentials = new NetworkCredential(uname, pword);
            rq.Headers.Add("x-iq-token", "A15M876TJ8");
            rq.Headers.Add("x-iq-response-details", "fullNested");
            rq.Headers.Add("x-iq-user", uname);
            rq.Timeout = 10000;//only try for 10 seconds
            HttpWebResponse rs;
            try
            {
                rs = (HttpWebResponse)rq.GetResponse();
            }
            catch (WebException e)
            {

                throw e;
            }

            //Stream s = rs.GetResponseStream();
            StreamReader reader = new StreamReader(rs.GetResponseStream());
            string jsonText = reader.ReadToEnd();

            string galleryPostJsonFile = Path.Combine(FlowContext.FlowTempDirPath, nodeName + "_json.txt");
            File.WriteAllText(galleryPostJsonFile, jsonText);


            //var jss = new JavaScriptSerializer();
            //var data = jss.Deserialize<dynamic>(jsonText);
            JsonParser parser = new JsonParser(new StringReader(jsonText), true);
            
            reader.Close();
            //NetServ.Net.Json.JsonParser.TokenType nextToken = parser.NextToken();

            JsonArray catalogArray = null;

            if (parser.NextToken() == NetServ.Net.Json.JsonParser.TokenType.BeginArray)
                catalogArray = parser.ParseArray();
            else if (parser.NextToken() == NetServ.Net.Json.JsonParser.TokenType.BeginObject)
            {
                parser = new JsonParser(new StringReader(jsonText), true);

                JsonObject rootObject = parser.ParseObject();

                try
                {


                    if (((JsonArray)rootObject[nodeName]).Count > 0)
                        catalogArray = (JsonArray)rootObject[nodeName];
                }
                catch
                {
                    //do nothinig for now
                }
            }
            rs.Close();
            return catalogArray;
        }

        public static JsonArray GetImageQuixJsonDocument(string uri, string nodeName, string uname, string pword)
        {
            logger.Info("x " + uri);
            HttpWebRequest rq = (HttpWebRequest)WebRequest.Create(uri);
            rq.Credentials = new NetworkCredential(uname, pword);
            rq.Headers.Add("x-iq-token", "A15M876TJ8");
            rq.Headers.Add("x-iq-response-details", "fullNested");
            rq.Headers.Add("x-iq-user", uname);
            rq.Timeout = 30000;//only try for 30 seconds
            HttpWebResponse rs;
            try
            {
                rs = (HttpWebResponse)rq.GetResponse();
            }
            catch (WebException e)
            {

                throw e;
            }

            //Stream s = rs.GetResponseStream();
            StreamReader reader = new StreamReader(rs.GetResponseStream());
            string jsonText = reader.ReadToEnd();

            string galleryPostJsonFile = Path.Combine(FlowContext.FlowTempDirPath, nodeName + "_json.txt");
            File.WriteAllText(galleryPostJsonFile, jsonText);



            JsonParser parser = new JsonParser(new StringReader(jsonText), true);
            
            reader.Close();
            //NetServ.Net.Json.JsonParser.TokenType nextToken = parser.NextToken();

            JsonArray catalogArray = null;

            if (parser.NextToken() == NetServ.Net.Json.JsonParser.TokenType.BeginArray)
                catalogArray = parser.ParseArray();
            else if (parser.NextToken() == NetServ.Net.Json.JsonParser.TokenType.BeginObject)
            {

                JsonObject rootObject = parser.ParseObject();

                try
                {
                   

                    if (((JsonArray)rootObject[nodeName]).Count > 0)
                        catalogArray = (JsonArray)rootObject[nodeName];
                }
                catch
                {
                    //do nothinig for now
                }
            }



            //JsonObject catalogs = parser.ParseObject();




            parser.Close();
            rs.Close();
            return catalogArray;
        }

        public static JsonObject GetImageQuixJsonResource(string uri, string uname, string pword)
        {
            logger.Info("y " + uri);
            HttpWebRequest rq = (HttpWebRequest)WebRequest.Create(uri);
            rq.Credentials = new NetworkCredential(uname, pword);
            rq.Headers.Add("x-iq-token", "A15M876TJ8");
            rq.Headers.Add("x-iq-response-details", "fullNested");
            rq.Headers.Add("x-iq-user", uname);
            rq.Timeout = 20000;//only try for 10 seconds
            HttpWebResponse rs = (HttpWebResponse)rq.GetResponse();

            //Stream s = rs.GetResponseStream();
            StreamReader reader = new StreamReader(rs.GetResponseStream());
            string jsonText = reader.ReadToEnd();

            reader.Close();
            rs.Close();
            JsonParser parser = new JsonParser(new StringReader(jsonText), true);
            NetServ.Net.Json.JsonParser.TokenType nextToken = parser.NextToken();
            JsonObject rootObject = parser.ParseObject();

            return rootObject;
        }

        //public static Collection<ImageQuixCatalogListing> GetImageQuixCatalogListFromServer(string uri)
        //{
        //    Collection<ImageQuixCatalogListing> catalogList = new Collection<ImageQuixCatalogListing>();
	
        //    XDocument catalogListDef = ImageQuixUtility.GetImageQuixDocument(uri + "?list=catalogs");

        //    foreach(XElement catalogDef in catalogListDef.Descendants("Catalog"))
        //        catalogList.Add(ReadCatalogListing(catalogDef));
			
        //    return catalogList;	
        //}

        public static Collection<ImageQuixCatalogListing> GetImageQuixCatalogListFromJsonServer(string uri, string uname, string pword)
        {

            //uri = "http://184.72.245.5:8080/imagequix/rest/service/4/catalog";
            Collection<ImageQuixCatalogListing> catalogList = new Collection<ImageQuixCatalogListing>();
            JsonArray catalogListDef = null;
            try
            {
                catalogListDef = ImageQuixUtility.GetImageQuixJsonDocument(uri, "catalogs", uname, pword);
            }
            catch (WebException e)
            {
                throw e;
            }
            if (catalogListDef == null)
                return null;

            foreach (JsonObject catalogDef in catalogListDef)
                catalogList.Add(ReadJsonCatalogListing(catalogDef));


            return catalogList;
        }

        public static ImageQuixCatalogListing ReadCatalogListing(XElement catalogDef)
        {
            int pk = (int)catalogDef.Attribute("pk");
            string label = "";
            string modDate = "";
            string sku = "";
            if (catalogDef.Attribute("label") != null)
                label = catalogDef.Attribute("label").NormalizeXAttribute();
            else if (catalogDef.Attribute("title") != null)
                label = catalogDef.Attribute("title").NormalizeXAttribute();

            if (catalogDef.Attribute("lastModified") != null)
                modDate = catalogDef.Attribute("lastModified").NormalizeXAttribute();

            if (catalogDef.Attribute("internalLabID") != null)
                sku = catalogDef.Attribute("internalLabID").NormalizeXAttribute();

            //string label = catalogDef.Attribute("label").NormalizeXAttribute();

            return new ImageQuixCatalogListing(label, pk, modDate, "", sku, null, false);
        }

        public static ImageQuixCatalogListing ReadJsonCatalogListing(JsonObject catalogDef)
        {
            string modDate = "1/1/2000";//default a consistant old modDate
            string resourceURL = "";
            string sku = "";
            bool isRetired = false;
            JsonArray catalogOptionGroups = null;
            int id = Convert.ToInt32(((JsonNumber)catalogDef["id"]).Value);
            string name = ((JsonString)catalogDef["name"]).Value;
            if (catalogDef.Keys.Contains("lastModified") && catalogDef["lastModified"].GetType() != typeof(JsonNull))
                modDate = ((JsonString)catalogDef["lastModified"]).Value;
            if (catalogDef.Keys.Contains("resource"))
                resourceURL = ((JsonString)catalogDef["resource"]).Value;
            if (catalogDef.Keys.Contains("optionGroups"))
                catalogOptionGroups = ((JsonArray)catalogDef["optionGroups"]);
            if (catalogDef.Keys.Contains("internalLabID"))
                sku = ((JsonString)catalogDef["internalLabID"]).Value;
            if (catalogDef.Keys.Contains("retiredOn"))
                isRetired = true;
            return new ImageQuixCatalogListing(name, id, modDate, resourceURL,sku, catalogOptionGroups, isRetired);
        }

        //public static XElement GetImageQuixCatalogFromServer(string uri, int catalogID)
        //{
        //    XDocument catalogDef = GetImageQuixDocument(uri + "?item=catalog&pk=" + catalogID.ToString().PadLeft(8,'0'));

        //    return catalogDef.Descendants("Catalog").FirstOrDefault();
        //}

        public static JsonArray GetImageQuixCatalogFromJsonServer(string uri, int catalogID, string uname, string pword)
        {
            JsonArray catalogDef = GetImageQuixJsonDocument(uri + "dump/" + catalogID.ToString(), "productGroups", uname, pword);
            //JsonArray catalogDef = GetImageQuixJsonDocument(uri + "/" + catalogID.ToString() + "/productgroup", "productGroups", uname, pword);

            return catalogDef;
        }

        public static JsonArray GetImageQuixProductGroupsFromJsonServer(string uri, string uname, string pword)
        {
            JsonArray catalogDef = GetImageQuixJsonDocument(uri, "productGroups", uname, pword);

            return catalogDef;
        }



        public static JsonArray GetImageQuixProductFromJsonServer(string uri, int catalogID, string uname, string pword)
        {
            JsonArray catalogDef = GetImageQuixJsonDocument(uri + "/" + catalogID.ToString() + "/productgroup", "product", uname, pword);

            return catalogDef;
        }

        public static JsonArray GetImageQuixProductFromJsonServer(string uri, string uname, string pword)
        {
            JsonArray catalogDef = GetImageQuixJsonDocument(uri, "products", uname, pword);

            return catalogDef;
        }
        public static JsonObject GetImageQuixResourceFromJsonServer(string uri, string uname, string pword)
        {
            JsonObject catalogDef = GetImageQuixJsonResource(uri, uname, pword);

            return catalogDef;
        }




       
	}

	public class ImageQuixCatalogListing
	{
		public int PrimaryKey { get; private set; }
		public string Label { get; private set; }
		public bool IsMarkedForImport { get; set; }
        public string ModifyDate { get; private set; }
        public string ResourceURL { get; private set; }
        public string SKU { get; private set; }
        public bool isDeleted { get; private set; }
        public JsonArray OptionGroups { get; private set; }
		public ImageQuixCatalogListing() { }

		public ImageQuixCatalogListing(string label, int pk, string modifyDate, string resourceURL, string sku, JsonArray optionGroups, bool retired)
		{
			this.Label = label;
			this.PrimaryKey = pk;
			this.IsMarkedForImport = true;
            this.ModifyDate = modifyDate;
            this.ResourceURL = resourceURL;
            this.OptionGroups = optionGroups;
            this.isDeleted = retired;
            this.SKU = sku;
		}
	}


}
