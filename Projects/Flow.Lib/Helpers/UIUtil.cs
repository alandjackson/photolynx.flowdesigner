﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace Flow.Helpers
{
    public class UIUtil
    {
        public static Window WrapInWindow(UserControl uc)
        {
            Window w = new Window();
            w.Content = uc;
            w.SizeToContent = SizeToContent.WidthAndHeight;
            return w;
        }
    }
}
