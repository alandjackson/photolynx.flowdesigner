﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.Lib.Net;
using System.Windows;
using Flow.Lib.Preferences;
using System.IO;
using Flow.Lib.AsyncWorker;
using StructureMap;
using Rebex.Net;
using NLog;

namespace Flow.Lib
{
    public class RemoteSync
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public PreferenceManager PreferenceManager { get; set; }
        
        public NotificationProgressInfo ProgressInfo { get; set; }

        private int _fileCount = 0;
        private int _curFileCount = 0;

        public RemoteSync(PreferenceManager preferenceManager, NotificationProgressInfo notificationInfo)
        {
            this.PreferenceManager = preferenceManager;
            this.ProgressInfo = notificationInfo;
        }

        public void Begin()
        {
            if (ProgressInfo.IsCanceled)
                return;

            logger.Info("Remote sync checking for updates...");

            ProgressInfo.Message = "Checking For Updates...";
            ProgressInfo.Progress = 0;
            ProgressInfo.Update();

            ProgressInfo.Progress = 0;
            ProgressInfo.Update();


            //CheckForUpdates();
           

        }


        public void DoUpdates(string[] newLayoutFiles, string[] newGraphicFiles, string[] newOverlayFiles, string[] newBackgroundFiles, string[] newPremiumBackgroundFiles, string[] newReportFiles, string[] newCustomSettingsFiles, string[] newHelpFiles)
        {
            logger.Info("Remote sync applying updates");

            // if (MessageBox.Show("New Layouts and Graphics are available, Click okay to download now", "Layouts and Graphics",
            //MessageBoxButton.OKCancel) == MessageBoxResult.Cancel)
            //     return;

            _fileCount = 0;
            if (newLayoutFiles != null)
            {
                logger.Info("Remote sync detected {0} new layout files", newLayoutFiles.Length);
                _fileCount += newLayoutFiles.Length;
            }
            if (newGraphicFiles != null)
            {
                logger.Info("Remote sync detected {0} new graphic files", newGraphicFiles.Length);
                _fileCount += newGraphicFiles.Length;
            }
            if (newOverlayFiles != null)
            {
                logger.Info("Remote sync detected {0} new overlay files", newOverlayFiles.Length);
                _fileCount += newOverlayFiles.Length;
            }
            if (newBackgroundFiles != null)
            {
                logger.Info("Remote sync detected {0} new background files", newBackgroundFiles.Length);
                _fileCount += newBackgroundFiles.Length;
            }
            if (newPremiumBackgroundFiles != null)
            {
                logger.Info("Remote sync detected {0} new premium background files", newPremiumBackgroundFiles.Length);
                _fileCount += newPremiumBackgroundFiles.Length;
            }
            IShowsNotificationProgress progressNotification = ObjectFactory.GetInstance<IShowsNotificationProgress>();
            ProgressInfo.NotificationProgress = progressNotification;
            progressNotification.ShowNotification(ProgressInfo);

            GetRemoteFiles(newLayoutFiles,
                            this.PreferenceManager.Upload.ProjectTransfer.RemoteLayoutsDirectory,
                            this.PreferenceManager.Layout.LayoutsDirectory);
            GetRemoteFiles(newGraphicFiles,
                            this.PreferenceManager.Upload.ProjectTransfer.RemoteGraphicsDirectory,
                            this.PreferenceManager.Layout.GraphicsDirectory);
            GetRemoteFiles(newOverlayFiles,
                            this.PreferenceManager.Upload.ProjectTransfer.RemoteOverlaysDirectory,
                            this.PreferenceManager.Edit.OverlaysDirectory);
            GetRemoteFiles(newReportFiles,
                            this.PreferenceManager.Upload.ProjectTransfer.RemoteReportsDirectory,
                            this.PreferenceManager.Edit.ReportsDirectory);
            GetRemoteFiles(newCustomSettingsFiles,
                            this.PreferenceManager.Upload.ProjectTransfer.RemoteCustomSettingsDirectory,
                            this.PreferenceManager.Edit.CustomSettingsDirectory);
            GetRemoteFiles(newHelpFiles,
                           this.PreferenceManager.Upload.ProjectTransfer.RemoteHelpDirectory,
                           this.PreferenceManager.Edit.HelpDirectory);
            GetRemoteFiles(newBackgroundFiles,
                                this.PreferenceManager.Upload.ProjectTransfer.RemoteBackgroundsDirectory,
                                this.PreferenceManager.Edit.BackgroundsDirectory);
            //GetRemoteFiles(newPremiumBackgroundFiles,
            //                    this.PreferenceManager.Upload.ProjectTransfer.RemoteBackgroundsDirectory + "/Premium",
            //                    Path.Combine(this.PreferenceManager.Edit.BackgroundsDirectory, "Premium"));
                
            ProgressInfo.Complete("Finished Updating");
            logger.Info("Remote sync finished applying updates");
        }

        public bool CheckFtpServer(out string[] newLayoutFiles, out string[] newGraphicFiles, out string[] newOverlayFiles, out string[] newBackgroundFiles, out string[] newPremiumBackgroundFiles, out string[] newReportFiles, out string[] newCustomSettingsFiles, out string[] newHelpFiles, bool canGreenScreen)
        {
            logger.Info("Checking FTP server for updates...");

                if (this.PreferenceManager.Upload.ProjectTransfer.UseSftp)
                {
                        Sftp client = new Sftp();
                        client.Connect(this.PreferenceManager.Upload.ProjectTransfer.HostAddress,this.PreferenceManager.Upload.ProjectTransfer.HostPort);
                        client.Login(this.PreferenceManager.Upload.ProjectTransfer.UserName, this.PreferenceManager.Upload.ProjectTransfer.Password);

                        newLayoutFiles = GetListOfChangedFiles(client,
                        this.PreferenceManager.Upload.ProjectTransfer.RemoteLayoutsDirectory,
                        this.PreferenceManager.Layout.LayoutsDirectory);

                        newGraphicFiles = GetListOfChangedFiles(client,
                                    this.PreferenceManager.Upload.ProjectTransfer.RemoteGraphicsDirectory,
                                    this.PreferenceManager.Layout.GraphicsDirectory);

                        newOverlayFiles = GetListOfChangedFiles(client,
                                    this.PreferenceManager.Upload.ProjectTransfer.RemoteOverlaysDirectory,
                                    this.PreferenceManager.Edit.OverlaysDirectory);

                        newReportFiles = GetListOfChangedFiles(client,
                                        this.PreferenceManager.Upload.ProjectTransfer.RemoteReportsDirectory,
                                        this.PreferenceManager.Edit.ReportsDirectory);

                        newCustomSettingsFiles = GetListOfChangedFiles(client,
                                            this.PreferenceManager.Upload.ProjectTransfer.RemoteCustomSettingsDirectory,
                                            this.PreferenceManager.Edit.CustomSettingsDirectory);
                        newHelpFiles = GetListOfChangedFiles(client,
                                                this.PreferenceManager.Upload.ProjectTransfer.RemoteHelpDirectory,
                                                this.PreferenceManager.Edit.HelpDirectory);

                        if (canGreenScreen)
                        {
                            newBackgroundFiles = GetListOfChangedFiles(client,
                                    this.PreferenceManager.Upload.ProjectTransfer.RemoteBackgroundsDirectory,
                                    this.PreferenceManager.Edit.BackgroundsDirectory);

                            newPremiumBackgroundFiles = GetListOfChangedFiles(client,
                                    this.PreferenceManager.Upload.ProjectTransfer.RemoteBackgroundsDirectory + "/Premium",
                                    Path.Combine(this.PreferenceManager.Edit.BackgroundsDirectory, "Premium"));
                        }
                        else
                        {
                            newBackgroundFiles = null;
                            newPremiumBackgroundFiles = null;
                        }
                        client.Disconnect();
                        client.Dispose();
                    }
                else
                    {
                        Ftp client = new Ftp();
                        client.Connect(this.PreferenceManager.Upload.ProjectTransfer.HostAddress,this.PreferenceManager.Upload.ProjectTransfer.HostPort);
                        client.Login(this.PreferenceManager.Upload.ProjectTransfer.UserName, this.PreferenceManager.Upload.ProjectTransfer.Password);

                        newLayoutFiles = GetListOfChangedFiles(client,
                        this.PreferenceManager.Upload.ProjectTransfer.RemoteLayoutsDirectory,
                        this.PreferenceManager.Layout.LayoutsDirectory);

                        newGraphicFiles = GetListOfChangedFiles(client,
                                    this.PreferenceManager.Upload.ProjectTransfer.RemoteGraphicsDirectory,
                                    this.PreferenceManager.Layout.GraphicsDirectory);

                        newOverlayFiles = GetListOfChangedFiles(client,
                                    this.PreferenceManager.Upload.ProjectTransfer.RemoteOverlaysDirectory,
                                    this.PreferenceManager.Edit.OverlaysDirectory);

                        newReportFiles = GetListOfChangedFiles(client,
                                           this.PreferenceManager.Upload.ProjectTransfer.RemoteReportsDirectory,
                                           this.PreferenceManager.Edit.ReportsDirectory);

                        newCustomSettingsFiles = GetListOfChangedFiles(client,
                                            this.PreferenceManager.Upload.ProjectTransfer.RemoteCustomSettingsDirectory,
                                            this.PreferenceManager.Edit.CustomSettingsDirectory);
                        newHelpFiles = GetListOfChangedFiles(client,
                                               this.PreferenceManager.Upload.ProjectTransfer.RemoteHelpDirectory,
                                               this.PreferenceManager.Edit.HelpDirectory);
                        if (canGreenScreen)
                        {
                            newBackgroundFiles = GetListOfChangedFiles(client,
                                    this.PreferenceManager.Upload.ProjectTransfer.RemoteBackgroundsDirectory,
                                    this.PreferenceManager.Edit.BackgroundsDirectory);

                            newPremiumBackgroundFiles = GetListOfChangedFiles(client,
                                    this.PreferenceManager.Upload.ProjectTransfer.RemoteBackgroundsDirectory + "/Premium",
                                    Path.Combine(this.PreferenceManager.Edit.BackgroundsDirectory, "Premium"));
                        }
                        else
                        {
                            newBackgroundFiles = null;
                            newPremiumBackgroundFiles = null;
                        }
                        client.Disconnect();
                        client.Dispose();
                    }

            if ((newLayoutFiles != null && newLayoutFiles.Length > 0) || (newGraphicFiles != null && newGraphicFiles.Length > 0)
                || (newOverlayFiles != null && newOverlayFiles.Length > 0) || (newBackgroundFiles != null && newBackgroundFiles.Length > 0) || (newPremiumBackgroundFiles != null && newPremiumBackgroundFiles.Length > 0)
                || (newReportFiles != null && newReportFiles.Length > 0) || (newCustomSettingsFiles != null && newCustomSettingsFiles.Length > 0) || (newHelpFiles != null && newHelpFiles.Length > 0))
            {
                return true;
            }

            return false;
        }

        private string[] GetListOfChangedFiles(Sftp client, string remoteDirectory, string localDirectory)
        {
            logger.Info("Getting list of changed SFTP files: remoteDir='{0}' localDir='{1}'", remoteDirectory, localDirectory);

            List<string> NewFiles = new List<string>();
            try
            {
                List<string> extentionList = new List<string>();
                extentionList.Add("jpg");
                extentionList.Add("png");
                extentionList.Add("lyt");
                extentionList.Add("jpeg");
                extentionList.Add("gif");
                extentionList.Add("xml");
                extentionList.Add("pdf");
                extentionList.Add("htm");
                extentionList.Add("html");
                extentionList.Add("chm");

                SftpItemCollection serverFiles = client.GetList("/" + remoteDirectory);
                foreach (SftpItem thisFile in serverFiles)
                {
                    if (thisFile.IsDirectory)
                    {
                        //NewFiles.AddRange(GetListOfChangedFiles(client, thisFile.Path, Path.Combine(localDirectory, thisFile.Name)));
                        foreach (string file in GetListOfChangedFiles(client, thisFile.Path, Path.Combine(localDirectory, thisFile.Name)))
                        {
                            string relpath = thisFile.Path.Replace("/" + remoteDirectory + "/", "");
                            NewFiles.Add(relpath + "/" + file);
                        }
                    }
                    if (thisFile.IsFile)
                    {
                        
                        if(extentionList.Contains(thisFile.Name.ToLower().Split('.').Last()))
                        {
                            FileInfo tmpLocalFile = new FileInfo(Path.Combine(localDirectory, thisFile.Name));
                            if (!tmpLocalFile.Exists)
                            {
                                NewFiles.Add(thisFile.Name);
                            }
                            else
                            {
                                if (thisFile.Size != tmpLocalFile.Length)
                                {
                                    NewFiles.Add(thisFile.Name);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }
            return NewFiles.ToArray();
        }


        private string[] GetListOfChangedFiles(Ftp client, string remoteDirectory, string localDirectory)
        {
            logger.Info("Getting list of changed FTP files: remoteDir='{0}' localDir='{1}'", remoteDirectory, localDirectory);

            List<string> NewFiles = new List<string>();
            try
            {
                List<string> extentionList = new List<string>();
                extentionList.Add("jpg");
                extentionList.Add("png");
                extentionList.Add("lyt");
                extentionList.Add("jpeg");
                extentionList.Add("gif");
                extentionList.Add("xml");
                extentionList.Add("pdf");
                extentionList.Add("htm");
                extentionList.Add("html");
                extentionList.Add("chm");

                if (!remoteDirectory.EndsWith("/"))
                    remoteDirectory = remoteDirectory + "/";

                FtpList serverFiles = client.GetList(remoteDirectory);
                foreach (FtpItem thisFile in serverFiles)
                {
                    if (thisFile.IsDirectory)
                    {
                        foreach(string file in GetListOfChangedFiles(client, Path.Combine(remoteDirectory, thisFile.Path), Path.Combine(localDirectory, thisFile.Name)))
                        {
                            if(file.StartsWith(Path.Combine(remoteDirectory, thisFile.Path)))
                                NewFiles.Add(file);
                            else
                                NewFiles.Add(thisFile.Path + "/" + file);
                        }
                    }
                    if (thisFile.IsFile)
                    {

                        if (extentionList.Contains(thisFile.Name.ToLower().Split('.').Last()))
                        {
                            FileInfo tmpLocalFile = new FileInfo(Path.Combine(localDirectory, thisFile.Name));
                            if (!tmpLocalFile.Exists)
                            {
                                NewFiles.Add(thisFile.Name);
                            }
                            else
                            {
                                if (thisFile.Size != tmpLocalFile.Length)
                                {
                                    NewFiles.Add(thisFile.Name);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }
            return NewFiles.ToArray();
        }

        //private void GetRemoteFiles(string[] files, string remoteDir, string localDir)
        //{
        //    if (!Directory.Exists(localDir))
        //        Directory.CreateDirectory(localDir);

        //    IFTPClient client = ConnectToFtp(this.PreferenceManager);
        //    try
        //    {
        //        foreach (string thisFile in files)
        //        {
        //            _curFileCount++;
        //            ProgressInfo.Message = "Downloading " + thisFile + "...";
        //            ProgressInfo.Progress = (int)Math.Floor((_curFileCount / (double)_fileCount) * 100); 
        //            ProgressInfo.Update();
        //            client.GetRemoteFile(remoteDir + "/" + thisFile, Path.Combine(localDir, thisFile));
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        return;
        //    }
        //}

        private void GetRemoteFiles(string[] files, string remoteDir, string localDir)
        {
            if (files != null && files.Length > 0)
                logger.Info("Getting {2} remote files: remoteDir='{0}' localDir='{1}'", remoteDir, localDir, files.Length);
            else
                return;

            if (!Directory.Exists(localDir))
                Directory.CreateDirectory(localDir);

            if (!remoteDir.EndsWith("/"))
                remoteDir = remoteDir + "/";

            if (this.PreferenceManager.Upload.ProjectTransfer.UseSftp)
            {
                Sftp client = new Sftp();
                client.Connect(this.PreferenceManager.Upload.ProjectTransfer.HostAddress, this.PreferenceManager.Upload.ProjectTransfer.HostPort);
                client.Login(this.PreferenceManager.Upload.ProjectTransfer.UserName, this.PreferenceManager.Upload.ProjectTransfer.Password);

                try
                {
                    foreach (string thisFile in files)
                    {
                        string workingFile = thisFile;
                        if (thisFile.StartsWith(remoteDir.Replace("/","\\")))
                            workingFile = thisFile.Substring(remoteDir.Length);

                        Directory.GetParent(Path.Combine(localDir, workingFile)).Create();

                        _curFileCount++;
                        logger.Info("Downloading file: fileName='{0}'", workingFile);
                        ProgressInfo.Message = "Downloading " + workingFile + "...";
                        ProgressInfo.Progress = (int)Math.Floor((_curFileCount / (double)_fileCount) * 100);
                        ProgressInfo.Update();
                        if (File.Exists(Path.Combine(localDir, workingFile)))
                            File.Delete(Path.Combine(localDir, workingFile));
                        client.GetFile(remoteDir + workingFile, Path.Combine(localDir, workingFile));
                    }
                }
                catch (Exception e)
                {
                    client.Disconnect();
                    client.Dispose();
                    logger.WarnException("Exception occured while syncing remote files, ignoring.", e);
                    return;
                }
                client.Disconnect();
                client.Dispose();
            }
            else
            {
                Ftp client = new Ftp();
                client.Connect(this.PreferenceManager.Upload.ProjectTransfer.HostAddress, this.PreferenceManager.Upload.ProjectTransfer.HostPort);
                client.Login(this.PreferenceManager.Upload.ProjectTransfer.UserName, this.PreferenceManager.Upload.ProjectTransfer.Password);

                try
                {
                    foreach (string thisFile in files)
                    {

                        string workingFile = thisFile;
                        if (thisFile.StartsWith(remoteDir))
                            workingFile = thisFile.Substring(remoteDir.Length);

                        Directory.GetParent(Path.Combine(localDir, workingFile)).Create();

                        _curFileCount++;
                        logger.Info("Downloading file: fileName='{0}'", workingFile);
                        ProgressInfo.Message = "Downloading " + workingFile + "...";
                        ProgressInfo.Progress = (int)Math.Floor((_curFileCount / (double)_fileCount) * 100);
                        ProgressInfo.Update();
                        if (File.Exists(Path.Combine(localDir, workingFile)))
                            File.Delete(Path.Combine(localDir, workingFile));
                        client.GetFile(remoteDir + workingFile, Path.Combine(localDir, workingFile));
                    }
                }
                catch (Exception e)
                {
                    client.Disconnect();
                    client.Dispose();
                    logger.WarnException("Exception occured while syncing remote files, ignoring.", e);
                    return;
                }
                client.Disconnect();
                client.Dispose();
            }
            
        }
        private IFTPClient ConnectToFtp(PreferenceManager preferenceManager)
        {
            IFTPClient client;
            if (preferenceManager.Upload.ProjectTransfer.UseSftp)
            {
                client = new SFTPClient(
                    preferenceManager.Upload.ProjectTransfer.HostAddress,
                    preferenceManager.Upload.ProjectTransfer.HostPort,
                    preferenceManager.Upload.ProjectTransfer.UserName,
                    preferenceManager.Upload.ProjectTransfer.Password
                );
            }
            else
            {
                client = new FTPClient(
                    preferenceManager.Upload.ProjectTransfer.HostAddress,
                    preferenceManager.Upload.ProjectTransfer.HostPort,
                    preferenceManager.Upload.ProjectTransfer.UserName,
                    preferenceManager.Upload.ProjectTransfer.Password
                );
            }
            return client;
        }


    }
}
