﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;

namespace Flow.Lib.Helpers
{
	public static class ExtensionUtility
	{
		/// <summary>
		/// Extension method; specifies a replacement value if the instance is null
		/// </summary>
		/// <param name="value"></param>
		/// <param name="replacementValue"></param>
		/// <returns></returns>
		public static string IfNull(this string value, string replacementValue)
		{
			return (value == null) ? replacementValue : value;
		}

		/// <summary>
		/// Extension method; specifies a replacement value if the instance is null or an empty string
		/// </summary>
		/// <param name="value"></param>
		/// <param name="replacementValue"></param>
		/// <returns>string</returns>
		public static string IfNullOrEmpty(this string value, string replacementValue)
		{
			return (String.IsNullOrEmpty(value)) ? replacementValue : value;
		}

		/// <summary>
		/// Extension method; returns the right n (length) characters of the source string
		/// </summary>
		/// <param name="value"></param>
		/// <param name="length"></param>
		public static string Right(this string value, int length)
		{
			if (value == null || length < 0)
				return null;
	
			int lengthDiff = value.Length - length;

			if (lengthDiff < 1)
				return value;

			return value.Substring(lengthDiff, length);
		}

		/// <summary>
		/// Extension method; returns the right n (length) characters of the source string
		/// </summary>
		/// <param name="value"></param>
		/// <param name="length"></param>
		public static string Left(this string value, int length)
		{
			if (value == null || length < 0)
				return null;

			if (value.Length < length)
				return value;

			return value.Substring(0, length);
		}

		/// <summary>
		/// Extension method; splits a string into a string array separated at instances of the separator sequence
		/// </summary>
		/// <param name="value"></param>
		/// <param name="separator"></param>
		/// <returns></returns>
		public static string[] Split(this string value, string separator)
		{
			return value.Split(new string[] { separator }, StringSplitOptions.None);
		}

		/// <summary>
		/// Replaces special characters with mnemonic representations
		/// </summary>
		/// <param name="value"></param>
		public static string BarcodeEscape(this string value)
		{
			return value
				.Replace("\r", "{CR}")
				.Replace("\n", "{LF}")
				.Replace("\t", "{TAB}")
			;
		}

		/// <summary>
		/// Left-pads the value with zeroes so that the return value's length is equal to the totalLength parameter
		/// </summary>
		/// <param name="value"></param>
		/// <param name="totalWidth"></param>
		/// <returns></returns>
		public static string ZeroPad(this string value, int totalLength)
		{
			return value.PadLeft(totalLength, '0');
		}

		/// <summary>
		/// Left-pads the value with zeroes so that the return value's length is equal to the totalLength parameter
		/// </summary>
		/// <param name="value"></param>
		/// <param name="totalWidth"></param>
		/// <returns></returns>
		public static string ZeroPad(this int value, int totalLength)
		{
			return value.ToString().ZeroPad(totalLength);
		}

		/// <summary>
		/// Left-pads the value with zeroes so that the return value's length is equal to the totalLength parameter
		/// </summary>
		/// <param name="value"></param>
		/// <param name="totalWidth"></param>
		/// <returns></returns>
		public static string ZeroPad(this int? value, int totalLength)
		{
			return (value.HasValue) ? value.Value.ZeroPad(totalLength) : null;
		}

		public static string CurrencyFormat(this decimal value)
		{
			return value.CurrencyFormat("");
		}

		public static string CurrencyFormat(this decimal value, string currencyText)
		{
			return String.Format("{0:" + currencyText + "0.00}", value);
		}

		/// <summary>
		/// Removes xml markup from the source value
		/// </summary>
		/// <param name="attribute"></param>
		/// <returns></returns>
		public static string NormalizeXAttribute(this XAttribute attribute)
		{
			Regex tagPattern = new Regex("<[^>]*>");

			return tagPattern
				.Replace(((string)attribute), "")
				.UriUnescape()
			;
		}

		/// <summary>
		/// Removes xml markup from the source value, and truncates the returned string
		/// </summary>
		/// <param name="attribute"></param>
		/// <returns></returns>
		public static string NormalizeXAttribute(this XAttribute attribute, int length)
		{
			return attribute.NormalizeXAttribute().Left(length);
		}

		public static string UriUnescape(this string value)
		{
			value = Uri.EscapeDataString(value);
			return Uri.UnescapeDataString(value);
		}

		/// <summary>
		/// Adds an XAttribute of the supplied name (and optional value) to the source XElement
		/// </summary>
		/// <param name="element"></param>
		/// <param name="xName"></param>
		/// <param name="value"></param>
		public static void AddNewXAttribute(this XElement element, XName xName, object value)
		{
			element.Add(new XAttribute(xName, value ?? ""));
		}

		//public static void Action<T>(this IEnumerable<T> list, Func<T,bool> func)
		//{
		//    list.Where(func);
		//}

		/// <summary>
		/// Sorts the source IOrderedEnumerable according to the supplied predicate and in the supplied direction
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="list"></param>
		/// <param name="func"></param>
		/// <param name="direction"></param>
		/// <returns></returns>
		public static IOrderedEnumerable<T> OrderBy<T>(this IEnumerable<T> list, Func<T,object> func, ListSortDirection direction)
		{
			return (direction == ListSortDirection.Ascending)
				? list.OrderBy(func)
				: list.OrderByDescending(func);
		}

		/// <summary>
		/// Supplementary sorts the source IOrderedEnumerable according to the supplied predicate and in the supplied direction
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="list"></param>
		/// <param name="func"></param>
		/// <param name="direction"></param>
		/// <returns></returns>
		public static IOrderedEnumerable<T> ThenBy<T>(this IOrderedEnumerable<T> list, Func<T, object> func, ListSortDirection direction)
		{
			return (direction == ListSortDirection.Ascending)
				? list.ThenBy(func)
				: list.ThenByDescending(func);
		}

		/// <summary>
		/// Converts a simple * wildcard search string to a corresponding Regex object
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static Regex ToRegex(this string value)
		{
			if (value == null)
				return null;

			string pattern = Regex.Escape(value);

			pattern = pattern.Replace(@"\*", ".*");

			if(!pattern.StartsWith(".*"))
				pattern = "^" + pattern;

			if(!pattern.EndsWith(".*"))
				pattern += "$";

			return new Regex(pattern, RegexOptions.IgnoreCase);
		}

		/// <summary>
		/// Returns true if the Type is an unsigned numeric
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static bool IsUnsignedNumeric(this Type value)
		{
			return (
				value == typeof(long) ||
				value == typeof(int) ||
				value == typeof(short) ||
				value == typeof(sbyte) ||
				value == typeof(decimal) ||
				value == typeof(float) ||
				value == typeof(double)
			);
		}

		/// <summary>
		/// Returns true if the Type is a signed numeric
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static bool IsSignedNumeric(this Type value)
		{
			return (
				value == typeof(char) ||
				value == typeof(byte) ||
				value == typeof(ushort) ||
				value == typeof(uint) ||
				value == typeof(ulong)
			);
		}

		/// <summary>
		/// Returns true if the Type is numeric
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static bool IsNumeric(this Type value)
		{
			return ExtensionUtility.IsUnsignedNumeric(value) || ExtensionUtility.IsSignedNumeric(value);
		}

		/// <summary>
		/// Determines whether or not a date is within the specified date range
		/// </summary>
		/// <param name="value"></param>
		/// <param name="beginDate"></param>
		/// <param name="endDate"></param>
		/// <returns></returns>
		public static bool IsInRange(this DateTime value, Nullable<DateTime> beginDate, Nullable<DateTime> endDate)
		{
			bool isAfterBegin = !beginDate.HasValue;
			bool isBeforeEnd = !endDate.HasValue;

			if (!isAfterBegin)
				isAfterBegin = (value >= beginDate.Value);

			if (!isBeforeEnd)
				isBeforeEnd = (value < endDate.Value.AddDays(1));

			return isAfterBegin && isBeforeEnd;
		}

		/// <summary>
		/// Attempts to extract the first part of a (supposed) filename
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static string GetFileNameRoot(this string value)
		{
            if(value.Contains("\\"))
                return value.Split('\\').Last().Split('.')[0];

			return value.Split('.')[0];
		}

		/// <summary>
		/// Deletes all child files and directories from the specified directory
		/// </summary>
		public static void Clear(this DirectoryInfo value)
		{
            foreach (FileInfo file in value.GetFiles())
            {
                try
                {
                    file.Delete();
                }
                catch
                {

                }
            }
			foreach (DirectoryInfo dir in value.GetDirectories())
				dir.Delete(true);
		}


		/// <summary>
		/// Generates a DataTable object from the source generic IEnumerable
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="varlist"></param>
		/// <param name="fn"></param>
		/// <returns></returns>
		public delegate object[] CreateRowDelegate<T>(T t);
		public delegate object[] CreateRowDelegate(object t);

		public static DataTable ToDataTable<T>(this IEnumerable<T> source, CreateRowDelegate<T> fn)
		{
			DataTable dtReturn = new DataTable();

			// column names

			PropertyInfo[] oProps = null;

			// Could add a check to verify that there is an element 0

			foreach (T rec in source)
			{

				// Use reflection to get property names, to create table, Only first time, others will follow
				if (oProps == null)
				{
					oProps = ((Type)rec.GetType()).GetProperties();

					foreach (PropertyInfo pi in oProps)
					{
						Type colType = pi.PropertyType; if ((colType.IsGenericType) && (colType.GetGenericTypeDefinition() == typeof(Nullable<>)))
						{
							colType = colType.GetGenericArguments()[0];
						}

						dtReturn.Columns.Add(new DataColumn(pi.Name, colType));
					}
				}

				DataRow dr = dtReturn.NewRow();
				
				foreach (PropertyInfo pi in oProps)
				{
					dr[pi.Name] = pi.GetValue(rec, null) == null ? DBNull.Value : pi.GetValue(rec, null);
				}

				dtReturn.Rows.Add(dr);
			}

			return (dtReturn);
		}

		public static DataTable ToDataTable<T>(this IQueryable<T> source, CreateRowDelegate<T> fn)
		{
			DataTable dtReturn = new DataTable();

			// column names

			PropertyInfo[] oProps = null;

			// Could add a check to verify that there is an element 0

			foreach (T rec in source)
			{

				// Use reflection to get property names, to create table, Only first time, others will follow
				if (oProps == null)
				{
					oProps = ((Type)rec.GetType()).GetProperties();

					foreach (PropertyInfo pi in oProps)
					{
						Type colType = pi.PropertyType; if ((colType.IsGenericType) && (colType.GetGenericTypeDefinition() == typeof(Nullable<>)))
						{
							colType = colType.GetGenericArguments()[0];
						}

						dtReturn.Columns.Add(new DataColumn(pi.Name, colType));
					}
				}

				DataRow dr = dtReturn.NewRow();

				foreach (PropertyInfo pi in oProps)
				{
					dr[pi.Name] = pi.GetValue(rec, null) == null ? DBNull.Value : pi.GetValue(rec, null);
				}

				dtReturn.Rows.Add(dr);
			}

			return (dtReturn);
		}

		public static DataTable ToDataTable(this IQueryable source)
		{
			return source.ToDataTable(q => new object[] { source });
		}

		public static DataTable ToDataTable(this IQueryable source, CreateRowDelegate fn)
		{
			DataTable dtReturn = new DataTable();

			// column names

			PropertyInfo[] oProps = null;

			// Could add a check to verify that there is an element 0

			foreach (object rec in source)
			{

				// Use reflection to get property names, to create table, Only first time, others will follow
				if (oProps == null)
				{
					oProps = ((Type)rec.GetType()).GetProperties();

					foreach (PropertyInfo pi in oProps)
					{
						Type colType = pi.PropertyType; if ((colType.IsGenericType) && (colType.GetGenericTypeDefinition() == typeof(Nullable<>)))
						{
							colType = colType.GetGenericArguments()[0];
						}

						dtReturn.Columns.Add(new DataColumn(pi.Name, colType));
					}
				}

				DataRow dr = dtReturn.NewRow();

				foreach (PropertyInfo pi in oProps)
				{
					dr[pi.Name] = pi.GetValue(rec, null) == null ? DBNull.Value : pi.GetValue(rec, null);
				}

				dtReturn.Rows.Add(dr);
			}

			return (dtReturn);
		}


	} // END class

}	// END namespace
