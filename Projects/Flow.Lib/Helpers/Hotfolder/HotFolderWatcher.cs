﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.Lib.AsyncWorker;
using System.Threading;
using System.IO;
using Flow.Lib.Log;
using Flow.Lib.Helpers;
using NLog;

namespace Flow.Lib
{
    /// <summary>
    /// Watches a directory for incoming and outgoing images.
    /// </summary>
    public class HotFolderWatcher
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

		public event EventHandler<EventArgs<IEnumerable<string>>> ImageFilesAdded = delegate { };
		public event EventHandler<EventArgs<IEnumerable<string>>> ImageFilesRemoved = delegate { };
	
		public event EventHandler<EventArgs<string>> ImageFileAdded = null;
		public event EventHandler<EventArgs<string>> ImageFileRemoved = null;

	
		private string _hotfolderDirectory;
		public string HotfolderDirectory
		{
			get { return _hotfolderDirectory; }
			set
			{
				_hotfolderDirectory = value;
				this.HotfolderDirectoryInfo = new DirectoryInfo(_hotfolderDirectory);
			}
		}
		
		public DirectoryInfo HotfolderDirectoryInfo { get; private set; }

        public Dictionary<string, bool> KnownFiles { get; set; }
        public List<string> ImageFormats { get; set; }

        BackgroundWorker BackgroundWorker;

		public IEnumerable<string> CurrentImageFileList { get; set; }


        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="worker">Manages the worker thread</param>
        /// <param name="formats">List of formats to recognize as images</param>
        public HotFolderWatcher(BackgroundWorker worker)
        {
			BackgroundWorker = worker;
            BackgroundWorker.DoWork = (Action)delegate { HotfolderScanner(); };

            KnownFiles = new Dictionary<string, bool>();

            ImageFormats = new List<string>(new string[] { ".JPG", ".JPEG", ".PNG" });
        }

        /// <summary>
        /// Starts the scan process
        /// </summary>
        public void Start()
        {
			BackgroundWorker.Start();
        }

        /// <summary>
        /// Stops the scan process
        /// </summary>
        public void Stop()
        {
            BackgroundWorker.Stop();
        }

        /// <summary>
        /// Continuously runs the scan, sleeping inbetween scans
        /// </summary>
        public void HotfolderScanner()
        {
            try
            {
                while (BackgroundWorker.ShouldRun)
                {
                    ScanHotfolderForChanges();
                    Thread.Sleep(1000);
                }
            }
            catch (Exception e)
            {
                logger.ErrorException("Exception occured.", e);
            }
        }

/// DEPRECATED; SEE NEW METHOD VERSION BELOW
/*
        /// <summary>
        /// Performs one scan of the directory to check for new or
        /// deleted images.
        /// </summary>
        public void ScanHotfolderForChanges()
        {
            new Validator().DirectoryExists(HotfolderDirectory).Validate();

            // set all of the known files to not found so we can keep track
            // of which files were removed
            List<string> filenames = new List<string>();
            foreach (KeyValuePair<string, bool> kvp in KnownFiles)
                filenames.Add(kvp.Key);

            foreach (string filename in filenames)
                KnownFiles[filename] = false;

            // get list of files from directory
            foreach (string filename in Directory.GetFiles(HotfolderDirectory))
            {
                if (!BackgroundWorker.ShouldRun)
                    return;

                // ignore unknown files
                if (!FilenameIsImage(filename))
                    continue;

                // mark known files as found
                if (KnownFiles.ContainsKey(filename))
                    KnownFiles[filename] = true;

                // found a new file
                else
                {

					//bool fileIsLocked = false;
					//FileStream fileStream = null;

					//try
					//{
					//    fileStream = File.OpenRead(filename);
					//}
					//catch
					//{
					//    fileIsLocked = true;
					//}
					//finally
					//{
					//    fileStream.Close();
					//}

					//if (fileIsLocked)
					//    return;

					KnownFiles.Add(filename, true);
					OnImagefileAdded(filename);
                }
            }

            // check for deleted files

			List<string> removedFiles = new List<string>();
            foreach (string knownFile in KnownFiles.Keys)
            {
                if (!KnownFiles[knownFile])
                {
                    removedFiles.Add(knownFile);
                    OnImagefileRemoved(knownFile);
                }
            }

            // get rid of the known files that were removed
            foreach (string removedFile in removedFiles)
                KnownFiles.Remove(removedFile);
        }
*/

		/// <summary>
		/// Performs one scan of the directory to check for new or
		/// deleted images.
		/// </summary>
		public void ScanHotfolderForChanges()
		{
			new Validator().DirectoryExists(HotfolderDirectory).Validate();


			// Get list of applicable (image) files in HotFolder
			IEnumerable<string> sourceImageFileList =
				this.HotfolderDirectoryInfo
					.GetFiles()
					.Where(f => this.ImageFormats.Contains(f.Extension.ToUpper()))
					.Select(f => f.FullName);

			// If this is the first scan
			if (this.CurrentImageFileList == null)
			{
				this.CurrentImageFileList = sourceImageFileList;

				// Notify listeners of the images currently in the hotfolder
				if(this.CurrentImageFileList.Count() > 0)
					this.OnImagefilesAdded(this.CurrentImageFileList);
			}
			else
			{
				// Get the list of newly added files
				IEnumerable<string> addList =
					sourceImageFileList.Except(this.CurrentImageFileList);

				// Get the list of newly removed files
				IEnumerable<string> removeList
					= this.CurrentImageFileList.Except(sourceImageFileList);

				bool hasChanges = false;

				// If there are new files, notify listeners
				if (addList.Count() > 0)
				{
					this.OnImagefilesAdded(addList);
					hasChanges = true;
				}

				// If there are removed files, notify listeners
				if (removeList.Count() > 0)
				{
					this.OnImagefilesRemoved(removeList);
					hasChanges = true;
				}

				// If the image file list has changed, update the list of current files
				if(hasChanges)
					this.CurrentImageFileList = sourceImageFileList;
			}
		}

        /// <summary>
        /// Checks if an image has a valid extension
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public bool FilenameIsImage(string filename)
        {
			return ImageFormats.Any(f => filename.ToUpper().EndsWith(f.ToUpper()));
			
			//foreach (string format in ImageFormats)
			//    if (filename.ToUpper().EndsWith(format.ToUpper()))
			//        return true;
			//return false;
        }

        /// <summary>
        /// Throws the file added event
        /// </summary>
        /// <param name="filename"></param>
        protected void OnImagefileAdded(string filename)
        {
            if (ImageFileAdded != null)
                ImageFileAdded(this, new EventArgs<string>(filename));
        }

        /// <summary>
        /// Throws the file removed event
        /// </summary>
        /// <param name="filename"></param>
        protected void OnImagefileRemoved(string filename)
        {
            if (ImageFileRemoved != null)
                ImageFileRemoved(this, new EventArgs<string>(filename));
        }

		/// <summary>
		/// Throws the file added event
		/// </summary>
		/// <param name="filename"></param>
		protected void OnImagefilesAdded(IEnumerable<string> filenames)
		{
			this.ImageFilesAdded(this, new EventArgs<IEnumerable<string>>(filenames));
		}

		/// <summary>
		/// Throws the file removed event
		/// </summary>
		/// <param name="filename"></param>
		protected void OnImagefilesRemoved(IEnumerable<string> filenames)
		{
			this.ImageFilesRemoved(this, new EventArgs<IEnumerable<string>>(filenames));
		}

    }
}
