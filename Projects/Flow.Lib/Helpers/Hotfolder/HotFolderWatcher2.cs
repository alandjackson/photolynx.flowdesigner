﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.Lib.AsyncWorker;
using System.Threading;
using System.IO;
using Flow.Lib.Log;
using Flow.Lib.Helpers;

namespace Flow.Lib
{
    /// <summary>
    /// Watches a directory for incoming and outgoing images.
    /// </summary>
    public class HotFolderWatcher
    {
        public string HotfolderDirectory { get; set; }

        public event EventHandler<EventArgs<string>> ImageFileAdded = null;
        public event EventHandler<EventArgs<string>> ImageFileRemoved = null;
        public Dictionary<string, bool> KnownFiles { get; set; }
        public List<string> ImageFormats { get; set; }

		FileSystemWatcher DirectoryWatcher;
		
//		BackgroundWorker BackgroundWorker;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="worker">Manages the worker thread</param>
        /// <param name="formats">List of formats to recognize as images</param>
        public HotFolderWatcher(BackgroundWorker worker, List<string> formats)
        {
			//BackgroundWorker = worker;
			//BackgroundWorker.DoWork = (Action)delegate { HotfolderScanner(); };

			this.DirectoryWatcher = new FileSystemWatcher();
			this.DirectoryWatcher.Path = FlowContext.FlowHotFolderDirPath;
			this.DirectoryWatcher.Filter = "*.jpg";
			this.DirectoryWatcher.EnableRaisingEvents = true;

			this.DirectoryWatcher.Created += new FileSystemEventHandler(DirectoryWatcher_Created);
			this.DirectoryWatcher.Deleted += new FileSystemEventHandler(DirectoryWatcher_Deleted);
			this.DirectoryWatcher.Renamed += new RenamedEventHandler(DirectoryWatcher_Renamed);

            KnownFiles = new Dictionary<string, bool>();
            
            ImageFormats = formats;
        }

		void DirectoryWatcher_Created(object sender, FileSystemEventArgs e)
		{
			if (ImageFileAdded != null)
				ImageFileAdded(this, new EventArgs<string>(e.FullPath));
		}

		void DirectoryWatcher_Deleted(object sender, FileSystemEventArgs e)
		{
			if (ImageFileRemoved != null)
				ImageFileRemoved(this, new EventArgs<string>(e.FullPath));
		}

		void DirectoryWatcher_Renamed(object sender, RenamedEventArgs e)
		{
			throw new NotImplementedException();
		}

		public void Start()
		{
			foreach (string filename in Directory.GetFiles(HotfolderDirectory))
			{
				// ignore unknown files
				if (!FilenameIsImage(filename))
					continue;

				if (ImageFileAdded != null)
					ImageFileAdded(this, new EventArgs<string>(filename));
			}
		}


//        /// <summary>
//        /// Starts the scan process
//        /// </summary>
//        public void Start()
//        {
//            BackgroundWorker.Start();
//        }

//        /// <summary>
//        /// Stops the scan process
//        /// </summary>
//        public void Stop()
//        {
//            BackgroundWorker.Stop();
//        }

//        /// <summary>
//        /// Continuously runs the scan, sleeping inbetween scans
//        /// </summary>
//        public void HotfolderScanner()
//        {
//            try
//            {
//                while (BackgroundWorker.ShouldRun)
//                {
//                    ScanHotfolderForChanges();
//                    Thread.Sleep(1000);
//                }
//            }
//            catch (Exception e)
//            {
//                FlowLog.LogError(e.ToString());
//            }
//        }

//        /// <summary>
//        /// Performs one scan of the directory to check for new or
//        /// deleted images.
//        /// </summary>
//        public void ScanHotfolderForChanges()
//        {
//            new Validator().DirectoryExists(HotfolderDirectory).Validate();

//            // set all of the known files to not found so we can keep track
//            // of which files were removed
//            List<string> filenames = new List<string>();
//            foreach (KeyValuePair<string, bool> kvp in KnownFiles)
//                filenames.Add(kvp.Key);

//            foreach (string filename in filenames)
//                KnownFiles[filename] = false;

//            // get list of files from directory
//            foreach (string filename in Directory.GetFiles(HotfolderDirectory))
//            {
//                if (!BackgroundWorker.ShouldRun)
//                    return;

//                // ignore unknown files
//                if (!FilenameIsImage(filename))
//                    continue;

//                // mark known files as found
//                if (KnownFiles.ContainsKey(filename))
//                    KnownFiles[filename] = true;

//                // found a new file
//                else
//                {
///*
//                    bool fileIsLocked = false;
//                    FileStream fileStream = null;

//                    try
//                    {
//                        fileStream = File.OpenRead(filename);
//                    }
//                    catch
//                    {
//                        fileIsLocked = true;
//                    }
//                    finally
//                    {
//                        fileStream.Close();
//                    }

//                    if (fileIsLocked)
//                        return;
//*/
//                    KnownFiles.Add(filename, true);
//                    OnImagefileAdded(filename);
//                }
//            }

//            // check for deleted files

//            List<string> removedFiles = new List<string>();
//            foreach (string knownFile in KnownFiles.Keys)
//            {
//                if (!KnownFiles[knownFile])
//                {
//                    removedFiles.Add(knownFile);
//                    OnImagefileRemoved(knownFile);
//                }
//            }

//            // get rid of the known files that were removed
//            foreach (string removedFile in removedFiles)
//                KnownFiles.Remove(removedFile);
//        }

		/// <summary>
		/// Checks if an image has a valid extension
		/// </summary>
		/// <param name="filename"></param>
		/// <returns></returns>
		public bool FilenameIsImage(string filename)
		{
			return ImageFormats.Any(f => filename.ToUpper().EndsWith(f.ToUpper()));

			//foreach (string format in ImageFormats)
			//    if (filename.ToUpper().EndsWith(format.ToUpper()))
			//        return true;
			//return false;
		}

        /// <summary>
        /// Throws the file added event
        /// </summary>
        /// <param name="filename"></param>
        protected void OnImagefileAdded(string filename)
        {
            if (ImageFileAdded != null)
                ImageFileAdded(this, new EventArgs<string>(filename));
        }

        /// <summary>
        /// Throws the file removed event
        /// </summary>
        /// <param name="filename"></param>
        protected void OnImagefileRemoved(string filename)
        {
            if (ImageFileRemoved != null)
                ImageFileRemoved(this, new EventArgs<string>(filename));
        }

    }
}
