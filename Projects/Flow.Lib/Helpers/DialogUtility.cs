﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//using Microsoft.Win32;



namespace Flow.Controls
{
	public static class DialogUtility
	{

		public static string SelectFile(string title, string initialDirectory, string filter)
		{
			OpenFileDialog dialog = GetOpenFileDialog(title, initialDirectory, filter);
			dialog.Multiselect = false;

			dialog.ShowDialog();

			return dialog.FileName;
		}

		public static string[] SelectFiles(string title, string initialDirectory, string filter)
		{
			OpenFileDialog dialog = GetOpenFileDialog(title, initialDirectory, filter);
			dialog.Multiselect = true;

			dialog.ShowDialog();

			return dialog.FileNames;
		}

		private static OpenFileDialog GetOpenFileDialog(string title, string initialDirectory, string filter)
		{
			OpenFileDialog dialog = new OpenFileDialog();
			dialog.Title = title;
			dialog.InitialDirectory = initialDirectory;
			dialog.Filter = filter;

			return dialog;
		}

		public static string SaveFile(string title, string initialDirectory, string defaultFileName, string filter)
		{
			SaveFileDialog dialog = new SaveFileDialog();
			dialog.Title = title;
			dialog.InitialDirectory = initialDirectory;
			dialog.FileName = defaultFileName;
			dialog.Filter = filter;

			if (dialog.ShowDialog() == DialogResult.Cancel)
				dialog.FileName = "";

			return dialog.FileName;
		}

		public static string SelectFolder(Environment.SpecialFolder rootFolder, string description)
		{
			FolderBrowserDialog dialog = GetFolderBrowserDialog(rootFolder, description);

            if (dialog.ShowDialog() == DialogResult.Cancel)
                return null;

			return dialog.SelectedPath;
		}

        public static string SelectFolder(string rootFolder, string description)
        {
            FolderBrowserDialog dialog = GetFolderBrowserDialog(rootFolder, description);

            if (dialog.ShowDialog() == DialogResult.Cancel)
                return null;

            return dialog.SelectedPath;
        }

		private static FolderBrowserDialog GetFolderBrowserDialog(Environment.SpecialFolder rootFolder, string description)
		{
			FolderBrowserDialog dialog = new FolderBrowserDialog();
			dialog.RootFolder = rootFolder;
			dialog.Description = description;

			return dialog;
		}

        private static FolderBrowserDialog GetFolderBrowserDialog(string rootFolder, string description)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            dialog.SelectedPath = rootFolder;
            dialog.Description = description;

            return dialog;
        }

	}
}
