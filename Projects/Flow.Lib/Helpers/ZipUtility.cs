﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using ICSharpCode.SharpZipLib.Zip;
using ICSharpCode.SharpZipLib.Core;

namespace Flow.Lib.Helpers
{
	public class ZipUtility
	{
		public static void CreateArchive(string destinationFilePath, string sourceDirPath, bool recursive)
		{
			FastZip zip = new FastZip();

			zip.CreateZip(destinationFilePath, sourceDirPath, recursive, null);
		}

		public static void CreateArchive(string destinationFilePath, IEnumerable<string> fileList, NotificationProgressInfo progressInfo)
		{
			ZipFile zip = ZipFile.Create(destinationFilePath);

			int fileCount = fileList.Count();
			progressInfo.Update("Adding file {0} of {1}", 0, fileCount, 1, true);

			zip.BeginUpdate();
			foreach (string filePath in fileList)
			{
				FileInfo fileInfo = new FileInfo(filePath);
				zip.Add(fileInfo.FullName, CompressionMethod.Stored);

				progressInfo++;
			}
			zip.CommitUpdate();

			zip.Close();
		}

		public static void CreateArchive(string destinationFilePath, IEnumerable<FileInfo> fileList, NotificationProgressInfo progressInfo)
		{
			ZipFile zip = ZipFile.Create(destinationFilePath);

			int fileCount = fileList.Count();
			progressInfo.Update("Adding file {0} of {1}", 0, fileCount, 1, true);

			zip.BeginUpdate();
			foreach (FileInfo fileInfo in fileList)
			{
				zip.Add(fileInfo.FullName, CompressionMethod.Stored);

				progressInfo++;
			}
			zip.CommitUpdate();

			zip.Close();
		}

		public static void CreateArchive(string destinationFilePath, IEnumerable<ZipFileMapping> fileList, NotificationProgressInfo progressInfo)
		{
			ZipFile zip = ZipFile.Create(destinationFilePath);

			int fileCount = fileList.Count();
			progressInfo.Update("Adding file {0} of {1}", 0, fileCount, 1, true);

			zip.BeginUpdate();
			foreach (ZipFileMapping fileInfo in fileList)
			{
				if (progressInfo.IsCanceled)
					return;
	
				//zip.Add(fileInfo.SourceFilePath, fileInfo.DestinationFileName);
                zip.Add(fileInfo.SourceFilePath, CompressionMethod.Stored);

				progressInfo++;
			}
			zip.CommitUpdate();

			zip.Close();
		}

		public static void ExtractArchive(
			string sourceFileName,
			string destinationFileDir,
			FastZip.Overwrite overwrite,
			FastZip.ConfirmOverwriteDelegate overwriteDelegate
		)
		{
			FastZip zip = new FastZip();
			zip.ExtractZip(sourceFileName, destinationFileDir, overwrite, overwriteDelegate, null, null, false);
		}

		/// <summary>
		/// Returns a list of the ZipEntry items (representing files) in the supplied archive
		/// </summary>
		/// <param name="zipFileName"></param>
		/// <returns></returns>
		public static string[] GetArchiveFileList(string zipFileName)
		{
			ZipFile zipFile = new ZipFile(zipFileName);

			return zipFile.Entries.Select(e => e.Name).ToArray();
		}

	}
}
