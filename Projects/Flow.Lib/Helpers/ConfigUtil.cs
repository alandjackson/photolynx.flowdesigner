﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace Flow.Lib.Helpers
{
    public class ConfigUtil
    {
        public static bool GetBoolSetting(string name)
        {
            bool settingVal;
            if (bool.TryParse(ConfigurationManager.AppSettings[name], out settingVal))
                return settingVal;
            return false;

        }
    }
}
