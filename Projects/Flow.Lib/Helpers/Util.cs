﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Resources;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Text.RegularExpressions;

namespace Flow
{
    public class Util
    {
        /// <summary>
        /// Get the image stream at the specified URI inside the application
        /// resource relative to the application package root.
        /// </summary>
        /// <param name="relativeUriString"></param>
        /// <returns>Image</returns>
        public static Image LoadImageFromResource(string relativeUriString)
        {
            Uri uri = new Uri(relativeUriString, UriKind.Relative);
            StreamResourceInfo sri = Application.GetResourceStream(uri);

            // Convert the stream to an Image object.
            BitmapImage bi = new BitmapImage();
            bi.BeginInit();
            bi.StreamSource = sri.Stream;
            bi.EndInit();
            Image img = new Image();
            img.Source = bi;

            return img;
        }

        public static string RemoveSpecialCharacters(string str)
        {
            return Regex.Replace(str, "[^a-zA-Z0-9_.]+", "", RegexOptions.Compiled);
        }
    }
}
