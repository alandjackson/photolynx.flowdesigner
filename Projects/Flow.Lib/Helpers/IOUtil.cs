﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;

namespace Flow.Lib.Helpers
{
    public class IOUtil
    {
        public static void CreateDirectory(string dir)
        {
            CreateDirectory(dir, true);
        }

        public static void CreateDirectory(string dir, bool make_parents)
        {
            CreateDirectory(new DirectoryInfo(dir), make_parents);
        }

        public static void CreateDirectory(DirectoryInfo di, bool make_parents)
        {
            if (di.Exists)
            {
                return;
            }

            if (!di.Parent.Exists && make_parents)
            {
                CreateParent(di);
            }
            di.Create();
        }

        protected static void CreateParent(DirectoryInfo dir)
        {
            DirectoryInfo parent = dir.Parent;
            if (parent != null && !parent.Exists)
            {
                CreateParent(parent);
                parent.Create();
            }
        }

        public static string CombinePaths(params string[] paths)
        {
            string complete_path = "";

            foreach (string p in paths)
            {
                complete_path = Path.Combine(complete_path, p);
            }

            return (complete_path);
        }

		public static void CopyFile(string path1, string path2)
		{
			FileInfo source = new FileInfo(path1);

			if (!source.Exists)
				return;

			FileInfo destination = new FileInfo(path2);
			IOUtil.CreateDirectory(destination.Directory, true);
			destination = null;

			source.CopyTo(path2);
		}

		public static void MoveFile(string path1, string path2)
		{
			FileInfo source = new FileInfo(path1);

			if (!source.Exists)
				return;

			FileInfo destination = new FileInfo(path2);
			IOUtil.CreateDirectory(destination.Directory, true);
			destination = null;

			source.CopyTo(path2, true);
            
			System.GC.Collect();
			System.GC.WaitForPendingFinalizers();

            //sometimes the file is still locked by the os from the copy command,
            //so try again to delete the file 12 times over a 3 second period 
            int tryCount=0;
            while (tryCount++ < 50)
            {
                try
                {
                    source.Delete();
                    break;
                }
                catch (Exception e)
                {
                    if (tryCount > 4)
                    {
                        throw e;
                    }

                    Thread.Sleep(500);
                }
            }
		}

		public static void RenameFile(string path, string newName)
		{
			FileInfo source = new FileInfo(path);

			string destination = Path.Combine(source.DirectoryName, newName);

			source.MoveTo(destination);
		}

        public static Dictionary<String, String> CopyFiles(string path1, string path2, bool renameDups, string dupFileNameAddOnOrig)
		{
            Dictionary<String, String> renamedFiles = new Dictionary<string,string>();

            //make sure the dir exists first
			DirectoryInfo dirInfo = new DirectoryInfo(path1);
            if (!dirInfo.Exists)
                return renamedFiles;

			foreach (FileInfo fileInfo in dirInfo.GetFiles())
			{
				FileInfo destination = new FileInfo(Path.Combine(path2, fileInfo.Name));

                int dupFileNameAddOn = 0;
                bool docopy = true;
                if (destination.Exists && destination.Length == fileInfo.Length)
                {
                    //if the images are the exact same image size, than they are the same, lets skip the copy
                    docopy = false;
                }
                else if (!destination.Exists)
                {
                    //destination does not exist, so copy it
                    docopy = true;
                    renamedFiles.Add(fileInfo.Name, destination.Name);
                }
                else
                {
                    //destination file Exists and is a different file size
                    while (destination.Exists && renameDups)
                    {

                        dupFileNameAddOn++;
                        FileInfo newDest = new FileInfo(Path.Combine(path2, fileInfo.Name.Replace(fileInfo.Extension, "_" + dupFileNameAddOn + fileInfo.Extension)));



                        //if the images are the exact same image size, than they are the same, lets skip the copy
                        if (!newDest.Exists)
                        {
                            renamedFiles.Add(fileInfo.Name, newDest.Name);
                            docopy = true;
                        }
                        else if (newDest.Exists && newDest.Length == fileInfo.Length)
                        {
                            renamedFiles.Add(fileInfo.Name, newDest.Name);
                            docopy = false;
                            break;
                        }

                        destination = newDest;
                    }
                } 

                if(docopy)
                    fileInfo.CopyTo(destination.FullName);
                
			}
            return renamedFiles;
		}

        public static void CopyDirectory(string sourcePath, string destinationPath)
        {
            if (!Directory.Exists(destinationPath))
                Directory.CreateDirectory(destinationPath);

            IOUtil.CopyFiles(sourcePath, destinationPath, false, null);
        }

        public static void CopyDirectoryRecursive(string sourcePath, string destinationPath)
        {
            IOUtil.CopyDirectory(sourcePath, destinationPath);

            string[] subdirectories = Directory.GetDirectories(sourcePath);
            foreach (string subdirectory in subdirectories)  
            {
                string name = Path.GetFileName(subdirectory);
                string dest = Path.Combine(destinationPath, name);
                if(name != "qc" && name != "incoming")
                    CopyDirectoryRecursive(subdirectory, dest);  
            }  
        }

		public static void CopyFiles(string path1, string path2, IEnumerable<string> fileNames)
		{
			IEnumerable<FileInfo> fileInfos = fileNames.Select(f => new FileInfo(Path.Combine(path1, f)));

			foreach (FileInfo source in fileInfos)
			{
				source.CopyTo(Path.Combine(path2, source.Name), true);
			}
		}

		public static void CopyFiles(string path1, string path2, IEnumerable<string> fileNames, NotificationProgressInfo progressInfo)
		{
			DirectoryInfo dirInfo = new DirectoryInfo(path1);
			//int fileCount = dirInfo.GetFiles().Count();
            int fileCount = fileNames.Count();
	
			IEnumerable<FileInfo> fileInfos = fileNames.Select(f => new FileInfo(Path.Combine(path1, f)));

			progressInfo.Update("Copying file {0} of {1}", 0, fileCount, 1, true);

			foreach (FileInfo source in fileInfos)
			{
                if(source.Exists)
				    source.CopyTo(Path.Combine(path2, source.Name), true);
				progressInfo++;
			}
		}


        public static bool IsFileInUse(string filePath)
        {
            return IsFileInUse(new FileInfo(filePath));
        }
        /// <summary>
        /// Checks to see if a file is in use by another process
        /// </summary>
        public static bool IsFileInUse(FileInfo file)
        {
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch (Exception)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }
            return false;
        }

        [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool GetDiskFreeSpaceEx(string lpDirectoryName, out ulong lpFreeBytesAvailable, out ulong lpTotalNumberOfBytes, out ulong lpTotalNumberOfFreeBytes);

        public static ulong DriveFreeBytesAvailable(string pathToCheck)
        {
            if (string.IsNullOrEmpty(pathToCheck))
                throw new ArgumentNullException("pathToCheck");

            if (!pathToCheck.EndsWith("\\"))
                pathToCheck += "\\";

            ulong freeBytesAvailable = 0;
            ulong totalNumberofBytes = 0;
            ulong totalNumberOfFreeBytes = 0;

            if (GetDiskFreeSpaceEx(pathToCheck, out freeBytesAvailable, out totalNumberofBytes, out totalNumberOfFreeBytes))
                return freeBytesAvailable;
            else
                throw new Exception(Marshal.GetLastWin32Error().ToString());
        }

        public static ulong GetDirectorySize(string p)
        {
	        // 1.
	        // Get array of all file names.
	        string[] a = Directory.GetFiles(p, "*.*");

	        // 2.
	        // Calculate total bytes of all files in a loop.
	        long b = 0;
	        foreach (string name in a)
	        {
	            // 3.
	            // Use FileInfo to get length of each file.
	            FileInfo info = new FileInfo(name);
	            b += info.Length;
	        }
	        // 4.
	        // Return total size
	        return (ulong)b;
        }


        public static List<String> DirSearch(string sDir)
        {
            List<String> files = new List<String>();
            try
            {
                foreach (string f in Directory.GetFiles(sDir))
                {
                    files.Add(f);
                }
                foreach (string d in Directory.GetDirectories(sDir))
                {
                    files.AddRange(DirSearch(d));
                }
            }
            catch (System.Exception excpt)
            {
                //MessageBox.Show(excpt.Message);
            }

            return files;
        }
    }
}
