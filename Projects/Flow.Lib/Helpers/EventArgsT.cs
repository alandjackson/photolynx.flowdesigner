﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flow.Lib.Helpers
{
    public class EventArgs<T> : EventArgs
    {
        public T Data { get; set; }

        public EventArgs(T d)
        {
            Data = d;
        }
    }
}
