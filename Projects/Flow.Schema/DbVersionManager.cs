﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.SqlServerCe;
using System.IO;
using System.Linq;
using System.Text;

using Flow.Lib;
using Flow.Lib.Database.SqlCe;
using Flow.Lib.Helpers;
using Flow.Schema.LinqModel.DataContext;

namespace Flow.Schema
{

	/// <summary>
	/// Manages schema updates for the FlowMaster database and individual project databases
	/// </summary>
	public static class DbVersionManager
	{

/// 
		#region FlowMaster Update Management Methods
/// 

		/// <summary>
		/// Checks for new db versions and returns true if a synch is required
		/// </summary>
		private static bool FlowMasterRequiresVersionSynchCheck(FlowMasterDataContext localFlowMaster, string pathToFlowMaster)
		{
				// Test for FlowMaster.sdf in the DbInstall directory
				if (File.Exists(FlowContext.FlowMasterInstallFilePath))
				{
					// Test for an existing installed FlowMaster.sdf file; if exists, synch check is necessary
                    //if (File.Exists(FlowContext.FlowMasterFilePath))
                    if (localFlowMaster.DatabaseExists())
					{
						File.Delete(FlowContext.FlowProjectTemplateFilePath);
						//File.Move(FlowContext.FlowProjectTemplateInstallFilePath, FlowContext.FlowProjectTemplateFilePath);
                        File.Copy(FlowContext.FlowProjectTemplateInstallFilePath, FlowContext.FlowProjectTemplateFilePath);
						return true;
					}


					// This is a fresh install, so copy install files to production directory;
					// version synching is not necessary
					else
					{
						InstallDbFiles(pathToFlowMaster);
						return false;
					}
				}

				// The application has not been freshly installed or re-installed; no version test necessary
				return false;
		}

        public static void CopyNewInstallFlowMaster(string pathToFlowMaster)
        {
            if(!File.Exists(pathToFlowMaster))
                InstallDbFiles(pathToFlowMaster);
        }
		/// <summary>
		/// Performs version update to the FlowMaster database, if a new version is available
		/// </summary>
        public static void SynchFlowMaster(FlowMasterDataContext localFlowMaster, string pathToFlowMaster)
		{
			// If a FlowMaster synch check is needed
            if (FlowMasterRequiresVersionSynchCheck(localFlowMaster, pathToFlowMaster))
			{
                
                SqlCeEngine eng1 = new SqlCeEngine("Data Source=" + FlowContext.FlowMasterInstallFilePath + ";Max Database Size=1024;");
                try
                {
                    eng1.Upgrade();
                }
                catch (Exception ex)
                {
                    //do nothing for now
                }


				// Get the list of all db versions
				List<MasterDbVersion> allDbVersions = FlowMasterDataContext.GetDbVersionHistory(
					FlowContext.FlowMasterInstallFilePath
				);

				//string connectionString = "Data Source=" + FlowContext.FlowMasterFilePath;
                //string connectionString = "Data Source=" + pathToFlowMaster + ";Max Database Size=1024;";
                //SqlCeConnection con = new SqlCeConnection(connectionString);

                //SqlCeEngine eng = new SqlCeEngine(connectionString);
                //try
                //{
                //    eng.Upgrade();
                //}
                //catch (Exception ex2)
                //{
                //    //do nothing for now
                //}

				// Connect to the currently installed FlowMaster database
                //FlowMasterDataContext installedFlowMasterDataContext = new FlowMasterDataContext(con, false);

				//using (FlowMasterDataContext installedFlowMasterDataContext = new FlowMasterDataContext(false))
				//{
					List<MasterDbVersion> newDbVersions = new List<MasterDbVersion>();
					
					// Iterate through all db versions
					foreach (MasterDbVersion version in allDbVersions)
					{
						// See if current installation includes the version
						MasterDbVersion correspondingVersion = localFlowMaster.MasterDbVersions.FirstOrDefault(
							v => v.DbVersionGuid == version.DbVersionGuid
						);

						// If an uninstalled version detected, add to the list
						if (correspondingVersion == null)
						{
							newDbVersions.Add(new MasterDbVersion(version));
						}
					}

					// If any new versions have been detected...
					if (newDbVersions.Count > 0)
					{
						//SqlCeManager sqlCeManager = new SqlCeManager(connectionString);

						// Iterate through the list of uninstalled versions
						foreach (MasterDbVersion newVersion in newDbVersions
							.Where(v => v.DbVersionInstallDate == null)
							.OrderBy(v => v.DbVersionDate)
						)
						{
							// Perform changeset fulfillment (execute version SQL statements
							if (newVersion.ChangeSet != null)
							{
                                foreach (string s in newVersion.ChangeSet.Split("--"))
                                    localFlowMaster.ExecuteCommand(s);
								//Collection<string> commandQueue = new Collection<string>(newVersion.ChangeSet.Split("--"));

								//sqlCeManager.ExecuteCommandBatch(commandQueue);
                                
							}

							// Update the install date to reflect the current date/time
							newVersion.DbVersionInstallDate = DateTime.Now;
						}

						localFlowMaster.MasterDbVersions.InsertAllOnSubmit(newDbVersions);
                        localFlowMaster.SubmitChanges();
					}
				//}

				// Delete the DbInstall dir
				//ClearDbInstallDirectory();
			}
		}

		/// <summary>
		/// Moves installer db files to the production directory (Config)
		/// </summary>
		private static void InstallDbFiles(string pathToFlowMaster)
		{
			// Move the db files from DbInstall to Config
            //File.Move(FlowContext.FlowProjectTemplateInstallFilePath, FlowContext.FlowProjectTemplateFilePath);
            //File.Move(FlowContext.FlowMasterInstallFilePath, FlowContext.FlowMasterFilePath);
            File.Copy(FlowContext.FlowProjectTemplateInstallFilePath, FlowContext.FlowProjectTemplateFilePath,true);
            File.Copy(FlowContext.FlowMasterInstallFilePath, pathToFlowMaster,true);

			// Delete the DbInstall dir
			//ClearDbInstallDirectory();
		}

		/// <summary>
		/// Deletes the DbInstall dir
		/// </summary>
		public static void ClearDbInstallDirectory()
		{
            //if (Directory.Exists(FlowContext.FlowDbInstallDirPath))
            //    Directory.Delete(FlowContext.FlowDbInstallDirPath, true);
		}

/// 
		#endregion
/// 


/// 
		#region Flow Project Update Management Methods
/// 
		
		/// <summary>
		/// Updates the version of the supplied FlowProject if a new schema version is available
		/// </summary>
		/// <param name="flowProject"></param>
		public static void SynchFlowProject(FlowProject flowProject)
		{
            //System.Windows.MessageBox.Show("A");
			DbVersionManager.SynchFlowProject(flowProject.ConnectionString, flowProject.FlowMasterDataContext);
            //System.Windows.MessageBox.Show("B");
		}

		/// <summary>
		/// Updates the version of the supplied FlowProject if a new schema version is available
		/// </summary>
		/// <param name="connectionString"></param>
		/// <param name="flowMasterDataContext"></param>
		public static void SynchFlowProject(string connectionString, FlowMasterDataContext flowMasterDataContext)
		{
           
            
			// Get the full version history from the template db
			List<DbVersion> allDbVersions = FlowProjectDataContext.GetDbVersionHistory();
            
			// Connect to the project db
            FlowProjectDataContext installedFlowProjectDataContext = new FlowProjectDataContext(new SqlCeConnection(connectionString), flowMasterDataContext);
			{
                

                List<DbVersion> installedDbVersions = installedFlowProjectDataContext.DbVersions.ToList();
				List<DbVersion> newDbVersions = new List<DbVersion>();

				// Iterate through the template version history
				foreach (DbVersion version in allDbVersions)
				{
					// Check if the project has been updated to the version
					DbVersion correspondingVersion = installedDbVersions.FirstOrDefault(
						v => v.DbVersionGuid == version.DbVersionGuid
					);

					// If an uninstalled version is detected, add to the list
					if (correspondingVersion == null)
					{
						newDbVersions.Add(new DbVersion(version));
					}
				}

				// If any new versions have been detected...
				if (newDbVersions.Count > 0)
				{
					SqlCeManager sqlCeManager = new SqlCeManager(connectionString);

					foreach (DbVersion newVersion in newDbVersions
						.Where(v => v.DbVersionInstallDate == null)
						.OrderBy(v => v.DbVersionDate)
					)
					{
						// Perform changeset fulfillment (execute SQL statements)
						if (newVersion.ChangeSet != null)
						{
							Collection<string> commandQueue = new Collection<string>(newVersion.ChangeSet.Split("--"));

							sqlCeManager.ExecuteCommandBatch(commandQueue);
						}

						// Update the install date to reflect the current date/time
						newVersion.DbVersionInstallDate = DateTime.Now;
					}

					installedFlowProjectDataContext.DbVersions.InsertAllOnSubmit(newDbVersions);
					installedFlowProjectDataContext.SubmitChanges();
				}

                //if there is a FK dependency on OrderPackge/SubjectImage, get rid of it
                
                string constraintName = installedFlowProjectDataContext.ExecuteQuery<string>("SELECT Constraint_name FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS WHERE (Constraint_Table_Name = 'OrderPackage') AND (Unique_Constraint_Table_Name = 'SubjectImage')", "").FirstOrDefault();
                if(!string.IsNullOrEmpty(constraintName))
                    installedFlowProjectDataContext.ExecuteCommand("ALTER TABLE OrderPackage DROP CONSTRAINT " + constraintName);

                installedFlowProjectDataContext.Connection.Close();
                installedFlowProjectDataContext.Dispose();
			}
		}

/// 
		#endregion
/// 
	
	}	// END class

}		// END namespace
