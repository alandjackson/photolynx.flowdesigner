﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Flow.Schema.LinqModel.DataContext;

namespace Flow.Schema
{
	/// <summary>
	/// A list of Subject field (column) mappings from an external data source to a
	/// Subject field definition, facilitating Subject data (or schema) import
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class ImportColumnPairCollection<T> : FlowObservableCollection<ImportColumnPair<T>>
		where T : ISubjectField, new()
	{

		/// <summary>
		/// Indexer; returns the column mapping whose destination field's display name matches the
		/// supplied key
		/// </summary>
		/// <param name="subjectFieldDisplayName"></param>
		/// <returns></returns>
		public new ImportColumnPair<T> this[string subjectFieldDisplayName]
		{
			get { return this.FirstOrDefault(m => m.DestinationFieldDisplayName == subjectFieldDisplayName); }
		}

		/// <summary>
		/// Indicates whether or not required Last Name subject field has been assigned to a source field
		/// </summary>
		private bool _lastNameFieldUnassigned = true;
		public bool LastNameFieldUnassigned
		{
			get { return _lastNameFieldUnassigned; }
			set
			{
				if (_lastNameFieldUnassigned != value)
				{
					_lastNameFieldUnassigned = value;
					this.SendPropertyChanged("LastNameFieldUnassigned");
					this.SendPropertyChanged("NameFieldsAssigned");
				}
			}
		}

		/// <summary>
		/// Indicates whether or not required First Name subject field has been assigned to a source field
		/// </summary>
		private bool _firstNameFieldUnassigned = true;
		public bool FirstNameFieldUnassigned
		{
			get { return _firstNameFieldUnassigned; }
			set
			{
				if (_firstNameFieldUnassigned != value)
				{
					_firstNameFieldUnassigned = value;
					this.SendPropertyChanged("FirstNameFieldUnassigned");
					this.SendPropertyChanged("NameFieldsAssigned");
				}
			}
		}

		/// <summary>
		/// Indicates whether or both required First/Last Name subject fields have been assigned to source fields
		/// </summary>
		public bool NameFieldsAssigned
		{
			get { return !(this.LastNameFieldUnassigned || this.FirstNameFieldUnassigned); }
		}


		/// <summary>
		/// Specifies fields to serve as record primary keys
		/// </summary>
		private List<ImportColumnPair<T>> _keyFields;
		public List<ImportColumnPair<T>> KeyFields
		{
			get
			{
				if (_keyFields == null)
					_keyFields = new List<ImportColumnPair<T>>();

				return _keyFields;
			}
		}

		/// <summary>
		/// Specifies whether duplicate Subject records (defined as having identical key field values)
		/// are to be ignored (true) or imported (false)
		/// </summary>
		public bool IgnoreDuplicates { get { return this.KeyFields.Count > 0; } }

		/// <summary>
		/// Specifies whether records containing the same key values (but different last/first names)
		/// should be imported
		/// </summary>
		public bool ImportKeyedDuplicates { get; set; }

		/// <summary>
		/// Specifies whether source fields which do not have a matching existing Subject field definition
		/// should be imported (true) or omitted (false)
		/// </summary>
		public bool ImportUnmatched { get; set; }


		#region CONSTRUCTORS

		/// <summary>
		/// Empty constructor
		/// </summary>
		public ImportColumnPairCollection() { }

		/// <summary>
		/// Generates ImportColumnPairs (column mappings) by employing the supplied strategies against the source
		/// schema and the existing Subject field defintions (if any)
		/// </summary>
		/// <param name="dataTable"></param>
		/// <param name="subjectFields"></param>
		/// <param name="strategy"></param>
		public ImportColumnPairCollection(DataTable dataTable, IEnumerable<T> subjectFields, ImportColumnStrategies strategy)
		{
			this.Import(dataTable, subjectFields, strategy, null);
		}

		/// <summary>
		/// Generates ImportColumnPairs (column mappings) by employing the supplied strategies against the source
		/// schema and the existing Subject field defintions (if any)
		/// </summary>
		/// <param name="dataTable"></param>
		/// <param name="subjectFields"></param>
		/// <param name="strategy"></param>
		/// <param name="columnOmissionText"></param>
		public ImportColumnPairCollection(DataTable dataTable, IEnumerable<T> subjectFields, ImportColumnStrategies strategy, string columnOmissionText)
		{
			this.Import(dataTable, subjectFields, strategy, columnOmissionText);
		}

		#endregion


		#region METHODS

		public new void Clear()
		{
			base.Clear();
			this.LastNameFieldUnassigned = true;
			this.FirstNameFieldUnassigned = true;
		}

		/// <summary>
		/// Generates ImportColumnPairs (column mappings) by employing the supplied strategies against the source
		/// schema and the existing Subject field defintions (if any)
		/// </summary>
		/// <param name="dataTable"></param>
		/// <param name="subjectFields"></param>
		/// <param name="strategy"></param>
		public void Import(DataTable dataTable, IEnumerable<T> subjectFields, ImportColumnStrategies strategy)
		{
			this.Import(dataTable, subjectFields, strategy, null);
		}

		/// <summary>
		/// Generates ImportColumnPairs (column mappings) by employing the supplied strategies against the source
		/// schema and the existing Subject field defintions (if any)
		/// </summary>
		/// <param name="dataTable"></param>
		/// <param name="subjectFields"></param>
		/// <param name="strategy"></param>
		/// <param name="columnOmissionText">
		/// Text to be displayed inpertinent UI controls if the source column is being omitted from the import
		/// </param>
		public void Import(DataTable dataTable, IEnumerable<T> subjectFields, ImportColumnStrategies strategy, string columnOmissionText)
		{
			// Iterate through the source data columns
			foreach (DataColumn column in dataTable.Columns)
			{
				// Create the source field in the ImportColumnPair from the actual source column
				T sourceField = new T();
				sourceField.SubjectFieldDisplayName = column.ColumnName;
				sourceField.SubjectFieldDataType = column.DataType;

				T destinationField = default(T);

				// If the manual assign option is NOT selected (meaning no auto-mapping)
				if (strategy != ImportColumnStrategies.ManualAssign)
				{
					// Attempt to find a matching existing field definition
					destinationField = subjectFields.FirstOrDefault(f => f.SubjectFieldDesc == sourceField.SubjectFieldDesc);

					// If no matching field found and all source columns should be provided a destination
					if (destinationField == null && strategy == ImportColumnStrategies.Automap)
						// Clone the source field and assign as the destination
						destinationField = (T)sourceField.Clone();
				}

				// Create the ImportColumn pair, assigning the source and destination fields
				ImportColumnPair<T> newPair = new ImportColumnPair<T>(sourceField, destinationField, subjectFields);

				// If user-defined columnOmissionText is supplied, assign it
				if (columnOmissionText != null)
					newPair.ColumnOmissionText = columnOmissionText;

				this.Add(newPair);
			}
            
			// Inform the UI if the first/last name fields have been assigned/unassigned
			this.LastNameFieldUnassigned = !this.Any(m => m.DestinationFieldDisplayName == "Last Name");
			this.FirstNameFieldUnassigned = !this.Any(m => m.DestinationFieldDisplayName == "First Name");
		}

		/// <summary>
		/// Assigns a destination field (or null) to the supplied ImportColumnPair
		/// </summary>
		/// <param name="mapping"></param>
		/// <param name="subjectField"></param>
		public void AssignDestinationField(ImportColumnPair<T> mapping, ISubjectField subjectField)
		{
			// Attempt to find a mapping which currently has the supplied field assigned as the destination
			ImportColumnPair<T> currentMapping = (subjectField == null) 
				? null
				: this.FirstOrDefault(m => m.DestinationFieldDisplayName == subjectField.SubjectFieldDisplayName);

			// If a mapping currently has the supplied field assigned as destination, set destination field to null
			if (
				currentMapping != null		&&
				mapping != currentMapping	&&
				(ISubjectField)currentMapping.DestinationField == subjectField
			)
			{
				currentMapping.DestinationField = default(T);
			}

			mapping.DestinationField = (T)subjectField;

			// Inform the UI if the first/last name fields have been assigned/unassigned
			this.LastNameFieldUnassigned = !this.Any(m => m.DestinationFieldDisplayName == "Last Name");
			this.FirstNameFieldUnassigned = !this.Any(m => m.DestinationFieldDisplayName == "First Name");

            
		}


        public DataTable RemoveBlankLines(DataTable dataTable)
        {
            int i = 0;
            while (i <= dataTable.Rows.Count - 1)
            {
                bool allEmpty = true;
                foreach (DataColumn dc in dataTable.Columns)
                {
                    if (dataTable.Rows[i][dc.ColumnName].ToString().Trim() != string.Empty)
                    {
                        allEmpty = false;
                    }
                }
                if(allEmpty)
                    dataTable.Rows.RemoveAt(i);
                else
                    i += 1;
            }
            return dataTable;
        }
		/// <summary>
		/// 
		/// </summary>
		/// <param name="dataTable"></param>
		/// <returns></returns>
		public bool TestForDuplicatesByKey(DataTable dataTable)
		{
			if (this.KeyFields.Count == 0 || dataTable.Rows.Count < 2)
				return false;

			string lastNameField =  this.First(p => p.DestinationFieldDisplayName == "Last Name").SourceField.SubjectFieldDisplayName;
			string firstNameField = this.First(p => p.DestinationFieldDisplayName == "First Name").SourceField.SubjectFieldDisplayName;

			DataView view = dataTable.DefaultView;

			List<String> sortColumns = new List<string>();

			foreach (ImportColumnPair<T> pair in this.KeyFields)
			{
				sortColumns.Add("[" + pair.SourceField.SubjectFieldDisplayName + "]");
			}

			sortColumns.Add("[" + lastNameField + "]");
			sortColumns.Add("[" + firstNameField + "]");

			view.Sort = String.Join(",", sortColumns.ToArray());

			DataRowView currentRow = view[0];

			for (int i = 1; i < view.Count; i++)
			{
				bool isDuplicate = true;
				
				DataRowView evalRow = view[i];

				foreach (string keyFieldName in this.KeyFields.Select(k => k.SourceField.SubjectFieldDisplayName))
				{
					isDuplicate = isDuplicate && (currentRow[keyFieldName].Equals(evalRow[keyFieldName]));
				}

				if (isDuplicate)
				{
					if (
						(currentRow[lastNameField] != evalRow[lastNameField]) ||
						(currentRow[firstNameField] != evalRow[firstNameField])
					)
						return true;
				}

				currentRow = evalRow;
			}

			return false;
		}

		#endregion // METHODS

	}	// END class

}	// END namespace
