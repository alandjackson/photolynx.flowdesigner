﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Data.Common;

//using System.Data.OleDb;
//using System.Data.Odbc;
using System.Data.SqlServerCe;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

using Microsoft.VisualBasic.FileIO;

using Flow.Lib;

using Flow.Schema.Utility;
using NPOI.HSSF.UserModel;

namespace Flow.Schema.Import
{
	/// <summary>
	/// Provides objects and methods for managing the importation of external data
	/// into a Flow project
	/// </summary>
	public class FlowImportDataSource
	{

		#region FIELDS AND PROPERTIES

		private readonly string _jetConnectionStringTemplate = "Provider=Microsoft.Jet.OLEDB.4.0;{0};{1}";
		private readonly string _access2007ConnectionStringTemplate = @"Provider=Microsoft.ACE.OLEDB.12.0;{0};Persist Security Info=False;";
        private readonly string _txtConnectionStringTemplate = "Driver={Microsoft Text Driver (*.txt; *.csv)}";

		/// <summary>
		/// Represents the import source type (MS Accces, Excel, etc.)
		/// </summary>
		public FlowImportDataSourceType SourceType { get; private set; }

		public bool IsFlowProjectSource { get { return this.SourceType == FlowImportDataSourceType.FlowProject; } }
		public bool IsMsAccessSource
		{
			get
			{
				return
					this.SourceType == FlowImportDataSourceType.MsAccess ||
					this.SourceType == FlowImportDataSourceType.MsAccess2007;
			}
		}
		public bool IsMsExcelSource { get { return this.SourceType == FlowImportDataSourceType.MsExcel; } }
		public bool IsDelimitedTextSource { get { return this.SourceType == FlowImportDataSourceType.DelimitedText; } }

		private FileInfo _sourceFileInfo = null;
		private FileInfo _tempSourceFileInfo = null;

		/// <summary>
		/// The "DataSource" type connection string expression
		/// </summary>
		private string _dataSource = null;
		public string DataSource
		{
			get
			{
				if (_dataSource == null)
					_dataSource = "Data Source=" +
                        ((this.IsDelimitedTextSource) ? _tempSourceFileInfo.DirectoryName : _sourceFileInfo.FullName);
				
				return _dataSource;
			}
		}

		/// <summary>
		/// The ExtendedProperties portion of the connection string
		/// </summary>
		private string _extendedProperties = "";
		public string ExtendedProperties
		{
			get
			{
				switch (this.SourceType)
				{
					case FlowImportDataSourceType.MsAccess:
						_extendedProperties = "User Id=" + this.UserName + ";Password=" + this.Password + ";";
						break;
					case FlowImportDataSourceType.MsExcel:
						_extendedProperties += "Extended Properties='Excel 8.0;IMEX=1;HDR=" + ((this.HasColumnHeaders) ? "YES';" : "NO';");
						break;
					case FlowImportDataSourceType.DelimitedText:
						_extendedProperties += "Extended Properties='text;IMEX=1;HDR=" + ((this.HasColumnHeaders) ? "Yes" : "No") + ";FMT=" + this.ExtendedPropertiesDelimitationText + "';";
						break;
					default:
						_extendedProperties = "";
						break;
				}

				return _extendedProperties;
			}
		}

		/// <summary>
		/// For certain source types, specifies whether or not the first row of the source
		/// contains column headers (field names);
		/// this is assigned by the user, not inferred
		/// </summary>
		public bool HasColumnHeaders { get; set; }

		public bool IsTabDelimited { get; set; }
		
		private string ExtendedPropertiesDelimitationText
		{
			get { return (this.IsTabDelimited) ? "TabDelimited" : "Delimited"; }
		}

		private string Delimiter
		{
			get { return (this.IsTabDelimited) ? "\t" : ","; }
		}


		/// <summary>
		/// UserName security credential (MS Access only)
		/// </summary>
		private string _userName = "";
		public string UserName
		{
			get { return _userName; }
			set { _userName = (value == null) ? String.Empty : value; }
		}

		/// <summary>
		/// Password security credential (MS Access only)
		/// </summary>
		public string Password { get; set; }

		/// <summary>
		/// The full connection string required to access the data source
		/// </summary>
		private string _connectionString = null;
		public string ConnectionString
		{
			get
			{
				if (this.IsFlowProjectSource)
					_connectionString = this.DataSource;
				else if (this.SourceType == FlowImportDataSourceType.MsAccess2007)
					_connectionString = String.Format(_access2007ConnectionStringTemplate, this.DataSource);
                else if (this.SourceType == FlowImportDataSourceType.DelimitedText)
                    _connectionString = _txtConnectionStringTemplate + ";Dbq='" + this.DataSource + "';Extensions=asc,csv,tab,txt;";
                //else if (this.SourceType == FlowImportDataSourceType.MsExcel)
                //    _connectionString = String.Format(_access2007ConnectionStringTemplate, this.DataSource);
				else
				{
					_connectionString = String.Format(
						_jetConnectionStringTemplate,
							this.DataSource,
							this.ExtendedProperties
					);
				}

				return _connectionString;
			}
		}

		/// <summary>
		/// For data sources which may contain mutiple "tables", the name of the table
		/// from which import data will be drawn
		/// </summary>
		private string _sourceTable = null;
		public string SourceTable
		{
			get
			{
				switch (this.SourceType)
				{
					case FlowImportDataSourceType.FlowProject:
						_sourceTable = "[Subject]";
						break;
					case FlowImportDataSourceType.DelimitedText:
						_sourceTable = _tempSourceFileInfo.Name;
						break;
				}

				return _sourceTable;
			}

			set { _sourceTable = "[" + value + "]"; }
		}

		/// <summary>
		/// The SQL command used for retrieving import source data
		/// </summary>
		public string SelectCommand
		{
			get { return "SELECT * FROM " + this.SourceTable; }
		}

		#endregion


		public FlowImportDataSource(string sourceFilePath)
		{
			_sourceFileInfo = new FileInfo(sourceFilePath);

			switch (_sourceFileInfo.Extension)
			{
				case ".sdf":
					this.SourceType = FlowImportDataSourceType.FlowProject;
					break;
				case ".mdb":
					this.SourceType = FlowImportDataSourceType.MsAccess;
					break;
				case ".accdb":
					this.SourceType = FlowImportDataSourceType.MsAccess2007;
					break;
				case ".xls":
				case ".xlsx":
					this.SourceType = FlowImportDataSourceType.MsExcel;
					break;
				default:
					this.SourceType = FlowImportDataSourceType.DelimitedText;
					_tempSourceFileInfo = this.GetTempSourceFileInfo(_sourceFileInfo);
					break;
			}
		}


		#region METHODS

		/// <summary>
		/// Copies a .csv/.txt source data file to temp location and returns a reference to the
		/// temp file's FileInfo
		/// </summary>
		/// <param name="sourceFileInfo"></param>
		/// <returns></returns>
		private FileInfo GetTempSourceFileInfo(FileInfo sourceFileInfo)
		{
			// Create the name of the temp file (temp dir + guid (minus dashes) + extension)
			string tempFile = FlowContext.Combine(
				FlowContext.FlowTempDirPath,
				Guid.NewGuid().ToString().Replace("-", "") + sourceFileInfo.Extension
			);

			// Copy the source file to the temp file
			File.Copy(sourceFileInfo.FullName, tempFile);

			// Return the FileInfo for the temp file
			return new FileInfo(tempFile);
		}


		/// <summary>
		/// Returns a list of all "tables" in the import source, allowing a user
		/// to specify the source table; valid only for MS Access/Excel sources
		/// </summary>
		/// <returns></returns>
		public Collection<string> GetSourceTableList()
		{
			Collection<string> retVal = null;

			if (this.IsMsAccessSource)
			{
				retVal = new Collection<string>(
					this.GetSchemaInfo("Tables", new string[4] { null, null, null, "Table" })
						.Select(r => r.Field<string>("TABLE_NAME")).ToList()
				);
			}
            else if (this.IsMsExcelSource)
            {
                retVal = new Collection<string>();
                
                HSSFWorkbook _hssfworkbook = new HSSFWorkbook(new FileStream(this._sourceFileInfo.FullName, FileMode.Open));
                int sheetCnt = _hssfworkbook.NumberOfSheets;
                for (int i = 0; i < sheetCnt; i++)
                {
                    retVal.Add(_hssfworkbook.GetSheetAt(i).SheetName);
                }

            }
            else
            {
                throw new Exception("Table enumeration not supported for data source type {" + this.SourceType.ToString() + "}");
            }
			return retVal;
		}

		/// <summary>
		/// Retrieves meta-data from an import source
		/// </summary>
		/// <param name="collectionName"></param>
		/// <param name="restrictions"></param>
		/// <returns></returns>
		private IEnumerable<DataRow> GetSchemaInfo(string collectionName, string[] restrictions)
		{
			DbProviderFactory factory = DbProviderFactories.GetFactory("System.Data.OleDb");
            if(this.SourceType == FlowImportDataSourceType.DelimitedText)
                factory = DbProviderFactories.GetFactory("System.Data.ODBC");

			using (DbConnection connection = factory.CreateConnection())
			{
				connection.ConnectionString = this.ConnectionString;

				connection.Open();

				return connection.GetSchema(collectionName, restrictions).AsEnumerable();
			}
		}

		/// <summary>
		/// Fills the supplied DataTable from the selected import source
		/// </summary>
		/// <param name="dataTable"></param>
		public DataTable Fill(DataTable dataTable)
		{
            if (this.IsDelimitedTextSource)
            {
                //auto detect if tab or comma delimited
                TextFieldParser tempFieldParser = new TextFieldParser(_tempSourceFileInfo.FullName, Encoding.Default);
                string firstLine = tempFieldParser.ReadLine();
                tempFieldParser.Close();
                int tsvColCount = (firstLine.Split("\t".ToCharArray())).Length;
                int csvColCount = (firstLine.Split(",".ToCharArray())).Length;

                if (tsvColCount > csvColCount)
                    this.IsTabDelimited = true;
                else
                    this.IsTabDelimited = false;


                List<string[]> rows = new List<string[]>();

                string[] fieldNames = null;

                using (TextFieldParser parser = new TextFieldParser(_tempSourceFileInfo.FullName, Encoding.Default))
                {
                    parser.Delimiters = new string[] { this.Delimiter };
                    parser.HasFieldsEnclosedInQuotes = true;

                    if (this.HasColumnHeaders)
                        fieldNames = parser.ReadFields();

                    while (!parser.EndOfData)
                    {
                        string[] fieldData = parser.ReadFields();

                        // Ignore any empty rows in the source data
                        //data row must contain at least 2 columns
                        if (fieldData.All(s => String.IsNullOrEmpty(s)) || fieldData.Length < 2)
                            continue;

                        rows.Add(fieldData);
                    }
                }

                List<Type> columnTypes = new List<Type>();

                int rowCount = rows.Count;

                bool columnIsNullable;
                Type columnType;
                Regex zeroPadCheck = new Regex(@"^[0].");

                //if (rows.GroupBy(s => s.Length).Count() > 1)
                //    ;	// Alert user of non-orthogonal data

                for (int i = 0; i < rows[0].Length; i++)
                {
                    try
                    {
                        IEnumerable<string[]> filteredByNull = rows.Where(s => !String.IsNullOrEmpty(s[i]));

                        columnIsNullable = filteredByNull.Count() < rowCount;

                        int tryInt;

                        if (rows.All(s => int.TryParse(s[i], out tryInt)))
                        {
                            if (rows.Any(s => zeroPadCheck.IsMatch(s[i])))
                                columnType = typeof(string);
                            else
                                columnType = columnIsNullable ? typeof(Nullable<int>) : typeof(int);
                        }
                        else
                        {
                            //DateTime tryDate;
                            //if (rows.All(s =>
                            //    DateTime.TryParse(s[i], out tryDate)	||
                            //    DateTime.TryParse(s[i], new CultureInfo("en-GB"), DateTimeStyles.None, out tryDate)
                            //))
                            //{
                            //    columnType = typeof(DateTime);
                            //}
                            //else
                            {
                                long tryLong;
                                if (rows.Any(s => long.TryParse(s[i], out tryLong)))
                                {
                                    columnType = typeof(string);
                                }
                                else
                                {
                                    decimal tryDecimal;
                                    if (rows.All(s => decimal.TryParse(s[i], out tryDecimal)))
                                    {
                                        columnType = columnIsNullable ? typeof(Nullable<decimal>) : typeof(decimal);
                                    }
                                    else
                                    {
                                        columnType = typeof(string);
                                    }
                                }
                            }
                        }

                        columnTypes.Add(columnType);

                    }
                    catch (Exception exc)
                    {
                        Console.Write(exc);
                    }
                }

                for (int i = 0; i < columnTypes.Count; i++)
                {
                    if (columnTypes[i] == typeof(string))
                    {
                        if (this.HasColumnHeaders && fieldNames[i].Contains(this.Delimiter))
                            fieldNames[i] = "\"" + fieldNames[i] + "\"";

                        foreach (string[] row in rows)
                        {
                            row[i] = row[i];
                        }
                    }

                    string columnName = (this.HasColumnHeaders)
                        ? fieldNames[i]
                        : "F" + (i + 1)
                    ;

                    DataColumn column = new DataColumn(columnName, columnTypes[i]);

                    //if (!this.HasColumnHeaders)
                        dataTable.Columns.Add(column);
                }

                this.IsTabDelimited = false;

                using (TextWriter writer = new StreamWriter(_tempSourceFileInfo.FullName, false, Encoding.Default))
                {
                    //if (this.HasColumnHeaders)
                    //{
                    //    writer.WriteLine(String.Join(this.Delimiter, fieldNames));
                    //    int i=0;
                    //    foreach (string name in fieldNames)
                    //        dataTable.Columns[i++].ColumnName = name;

                    //}

                    //int j = 0;
                    foreach (string[] row in rows)
                    {
                        writer.WriteLine(String.Join(this.Delimiter, row));
                        dataTable.Rows.Add(row);
                    }
                }

                
            }
            else if (this.SourceType == FlowImportDataSourceType.MsExcel)
            {
                HSSFWorkbook _hssfworkbook = new HSSFWorkbook(new FileStream(this._sourceFileInfo.FullName, FileMode.Open));
                
                //Getting the sheet
                HSSFSheet _currentSheet = (HSSFSheet)_hssfworkbook.GetSheetAt(0);
                
                int rowCnt = _currentSheet.PhysicalNumberOfRows;
                List<string[]> rows = new List<string[]>();

                string[] fieldNames = null;

                for (int i = 0; i < rowCnt; i++)
                {

                    

                    List<string> values = new List<string>();
                    foreach (HSSFCell cell in _currentSheet.GetRow(i).Cells)
                    {
                        if(cell.CellType ==  NPOI.SS.UserModel.CellType.STRING)
                            values.Add(cell.StringCellValue);
                        else if(cell.CellType ==  NPOI.SS.UserModel.CellType.NUMERIC)
                            values.Add(cell.NumericCellValue.ToString());
                    };
                    string[] fieldData = values.ToArray();

                   

                    // Ignore any empty rows in the source data
                    //data row must contain at least 2 columns
                    if (fieldData.All(s => String.IsNullOrEmpty(s)) || fieldData.Length < 2)
                        continue;

                    if (this.HasColumnHeaders && i == 0)
                    {
                        fieldNames = fieldData;
                        continue;
                    }

                    rows.Add(fieldData);
                }
      

                List<Type> columnTypes = new List<Type>();

                int rowCount = rows.Count;

                bool columnIsNullable;
                Type columnType;
                Regex zeroPadCheck = new Regex(@"^[0].");

                //if (rows.GroupBy(s => s.Length).Count() > 1)
                //    ;	// Alert user of non-orthogonal data

                for (int i = 0; i < rows[0].Length; i++)
                {
                    try
                    {
                        IEnumerable<string[]> filteredByNull = rows.Where(s => !String.IsNullOrEmpty(s[i]));

                        columnIsNullable = filteredByNull.Count() < rowCount;

                        int tryInt;

                        if (rows.All(s => int.TryParse(s[i], out tryInt)))
                        {
                            if (rows.Any(s => zeroPadCheck.IsMatch(s[i])))
                                columnType = typeof(string);
                            else
                                columnType = columnIsNullable ? typeof(Nullable<int>) : typeof(int);
                        }
                        else
                        {
                            //DateTime tryDate;
                            //if (rows.All(s =>
                            //    DateTime.TryParse(s[i], out tryDate)	||
                            //    DateTime.TryParse(s[i], new CultureInfo("en-GB"), DateTimeStyles.None, out tryDate)
                            //))
                            //{
                            //    columnType = typeof(DateTime);
                            //}
                            //else
                            {
                                long tryLong;
                                if (rows.Any(s => long.TryParse(s[i], out tryLong)))
                                {
                                    columnType = typeof(string);
                                }
                                else
                                {
                                    decimal tryDecimal;
                                    if (rows.All(s => decimal.TryParse(s[i], out tryDecimal)))
                                    {
                                        columnType = columnIsNullable ? typeof(Nullable<decimal>) : typeof(decimal);
                                    }
                                    else
                                    {
                                        columnType = typeof(string);
                                    }
                                }
                            }
                        }

                        columnTypes.Add(columnType);

                    }
                    catch (Exception exc)
                    {
                        Console.Write(exc);
                    }
                }

                for (int i = 0; i < columnTypes.Count; i++)
                {
                    if (columnTypes[i] == typeof(string))
                    {
                        if (this.HasColumnHeaders && fieldNames[i].Contains(this.Delimiter))
                            fieldNames[i] = "\"" + fieldNames[i] + "\"";

                        foreach (string[] row in rows)
                        {
                            row[i] = row[i];
                        }
                    }

                    string columnName = (this.HasColumnHeaders)
                        ? fieldNames[i]
                        : "F" + (i + 1)
                    ;

                    DataColumn column = new DataColumn(columnName, columnTypes[i]);

                    //if (!this.HasColumnHeaders)
                    dataTable.Columns.Add(column);
                }

                foreach (string[] row in rows)
                {
                    dataTable.Rows.Add(row);
                }

            }
            else
            {
                DbDataAdapter adapter = DataUtility.GetDataAdapter(this);
                adapter.Fill(dataTable);
            }

            return dataTable;
		}

		#endregion

	}	// END class


	/// <summary>
	/// Represents the datasource (file) types allowed as sources for import into
	/// Flow projects
	/// </summary>
	public enum FlowImportDataSourceType
	{
		FlowProject,
		MsAccess,
		MsAccess2007,
		MsExcel,
		DelimitedText
	}

}	// END namespace
