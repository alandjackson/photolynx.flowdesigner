﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Flow.Schema.LinqModel.DataContext;

namespace Flow.Schema
{
	/// <summary>
	/// Represents a pair (or mapping) of source and destinations columns to facilitate
	/// external data importation; the must implement the ISubjectField interface and must
	/// provide an empty constructor
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class ImportColumnPair<T> : INotifyPropertyChanging, INotifyPropertyChanged
		where T : ISubjectField, new()
	{

		#region EVENTS

		public event PropertyChangingEventHandler PropertyChanging = delegate { };
		protected void OnPropertyChanging(string propertyName)
		{
			this.PropertyChanging(this, new PropertyChangingEventArgs(propertyName));
		}

		public event PropertyChangedEventHandler PropertyChanged = delegate { };
		protected void OnPropertyChanged(string propertyName)
		{
			this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
		}

		#endregion // EVENTS


		#region FIELDS AND PROPERTIES

		/// <summary>
		/// The existing set of Subject field definitions (provided for attempts at auto-mapping);
		/// this is supplied to populate the assignment menu in the UI
		/// </summary>
		public IEnumerable<T> SubjectFields { get; internal set; }

		/// <summary>
		/// Specifies the field from the originating data source
		/// </summary>
		private T _sourceField = default(T);
		public T SourceField
		{
			get { return _sourceField; }
			set { _sourceField = value; }
		}

		/// <summary>
		/// Specifies the destination field to which the source field is mapped
		/// </summary>
		private T _destinationField = default(T);
		public T DestinationField
		{
			get { return _destinationField; }
			set
			{
				_destinationField = value;
	
				this.OnPropertyChanged("DestinationField");
				this.OnPropertyChanged("DestinationFieldDisplayName");

				this.OnPropertyChanged("OmitColumn");
				this.OnPropertyChanged("IncludeColumn");
			}
		}

		/// <summary>
		/// The destination field display name (if a definition field is not assigned
		/// default omission text is returned)
		/// </summary>
		public string DestinationFieldDisplayName
		{
			get { return (_destinationField == null) ? this.ColumnOmissionText : _destinationField.SubjectFieldDisplayName; }
			set
			{
				_destinationField.SubjectFieldDisplayName = value;
				this.OnPropertyChanged("DestinationFieldDisplayName");
			}
		}

		public bool DestinationFieldIsLastName
		{
			get { return this.DestinationField != null && this.DestinationField.SubjectFieldDisplayName == "Last Name"; }
		}

		public bool DestinationFieldIsFirstName
		{
			get { return this.DestinationField != null && this.DestinationField.SubjectFieldDisplayName == "First Name"; }
		}

		public bool DestinationFieldIsName
		{
			get { return this.DestinationFieldIsLastName || this.DestinationFieldIsFirstName; }
		}


		/// <summary>
		/// Specifies whether or not the source column is to be imported
		/// </summary>
		public bool IncludeColumn
		{
			get { return this.DestinationField != null; }
		}

		/// <summary>
		/// Specifies whether or not the source column is to be imported
		/// </summary>
		public bool OmitColumn
		{
			get { return this.DestinationField == null; }
		}

		/// <summary>
		/// The text to be displayed if a source column will be omitted from import
		/// </summary>
		private string _columnOmissionText = "{Omit}";
		public string ColumnOmissionText
		{
			get { return _columnOmissionText; }
			set { _columnOmissionText = value; }
		}

		/// <summary>
		/// Specifies that the field definition (schema) should be imported and the data omitted
		/// </summary>
		private bool _importSchemaOnly = false;
		public bool ImportSchemaOnly
		{
			get { return _importSchemaOnly; }
			set
			{
				_importSchemaOnly = value;

				if (_importSchemaOnly && this.DestinationField != null)
				{
					this.DestinationField.IsKeyField = false;
					this.DestinationField.IsRequiredField = false;
					this.DestinationField.IsScanKeyField = false;
				}

				this.OnPropertyChanged("ImportSchemaOnly");
			}
		}

		#endregion


		#region CONSTRUCTORS

		/// <summary>
		/// Empty constructor
		/// </summary>
		public ImportColumnPair() { }

		/// <summary>
		/// Constructor specifying a source field only (when auto-mapping is disabled)
		/// </summary>
		/// <param name="sourceField"></param>
		public ImportColumnPair(T sourceField, IEnumerable<T> subjectFields)
			: this(sourceField, default(T), subjectFields)
		{
		}

		/// <summary>
		/// Constructor specifying a source and destination field
		/// </summary>
		/// <param name="sourceField"></param>
		/// <param name="destinationField"></param>
		public ImportColumnPair(T sourceField, T destinationField, IEnumerable<T> subjectFields)
		{
			_sourceField = sourceField;
			this.SubjectFields = subjectFields;

			this.DestinationField = destinationField;
		}

		#endregion // CONSTRUCTORS


		#region METHODS

		/// <summary>
		/// Attempts to match the source field to an existing Subject field definition;
		/// if no existing match, the destination field is created from the schema of the source
		/// </summary>
		private void InferDestinationField()
		{
			// Attempt to match the source column and an existing Subject field on description
			T destinationField =
				this.SubjectFields.FirstOrDefault(f => f.SubjectFieldDesc == this.SourceField.SubjectFieldDesc);

			// If no existing field definition exists, copy the source field definition
			if (destinationField == null)
				destinationField = (T)this.SourceField.Clone();

			this.DestinationField = destinationField;
		}

		#endregion // CONSTRUCTORS

	}

	/// <summary>
	/// Strategies for mapping import source columns to destination columns
	/// </summary>
	public enum ImportColumnStrategies
	{
		Automap,
		AutoMapMatchedOnly,
		ManualAssign
	}

}
