﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Linq.Dynamic;
using System.Linq;
using System.Text;

using C1.C1Preview.Export;
using C1.C1Report;
//using C1.C1Report.Util;

using Flow.Controls;
using Flow.Lib;
using Flow.Lib.Helpers;
using Flow.Schema.LinqModel.DataContext;
using System.IO;
using System.Windows;
using C1.C1Preview;
using System.Drawing.Printing;

namespace Flow.Schema.Reports
{
	/// <summary>
	/// 
	/// </summary>
	public static class ReportDataDefinitions
	{
		public static List<string> ReportList { get; private set; }

        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

		public static List<KeyValuePair<string,string>> ExportFormatList { get; private set; }

        public static List<OrderPackage> ApplicablePackages = null;
        public static List<Subject> ApplicableSubjects = null;

		static ReportDataDefinitions()
		{

			ReportDataDefinitions.ReportList = new List<string>(
				new string[]
				{
                    "Catalog Packages",
					"Subject Orders Summary",
                    "All Subjects",
					"Photographed Subjects",
					"Unphotographed Subjects",
                    "Subject Orders Detailed",
                    "Order Form",
                    "Subject Barcodes",
                    "New Ticket Barcodes",
                    "Lab Billing Report"
				}
			);

            foreach (string cr in Directory.GetFiles(FlowContext.FlowReportsDirPath, "*.xml"))
            {
                ReportDataDefinitions.ReportList.Add("> " + (new FileInfo(cr)).Name);
            }
            

			ReportDataDefinitions.ExportFormatList = new List<KeyValuePair<string, string>>(
				new KeyValuePair<string,string>[]
				{
					new KeyValuePair<string,string>("Excel", "Excel Workbook (*.xls)|*.xls"),
					new KeyValuePair<string,string>("PDF", "Adobe PDF (*.pdf)|*.pdf"),
                    //new KeyValuePair<string,string>("HTML", "HTML (*.htm,*.html)|*.htm;*.html")
				}
			);
		}

        public static void GetReportData(FlowReportView reportView, List<OrderPackage> applicablePackages)
		{
            if (applicablePackages != null)
            {
                ApplicableSubjects = applicablePackages.Select(ap => ap.SubjectOrder.Subject).ToList();
                ApplicablePackages = applicablePackages;
            }

            GetReportData(reportView);
        }
		/// <summary>
		/// Returns a DataTable containing data for the requested report
		/// </summary>
		/// <param name="reportName"></param>
		/// <param name="flowProject"></param>
		/// <param name="fields"></param>
		/// <returns></returns>
        private static void GetReportData(FlowReportView reportView)
        {
 
			switch (reportView.ReportName)
			{
				case "Subject Orders Summary":
                    if(ApplicablePackages == null)
                        reportView.DataSource = ReportDataDefinitions.GetSubjectOrdersSummaryReportDataOLD(reportView.FlowProject, reportView.SubjectFieldList);
                    else
					    reportView.DataSource = ReportDataDefinitions.GetSubjectOrdersSummaryReportData(reportView.FlowProject, reportView.SubjectFieldList);
                    
                    ReportDataDefinitions.Sort(reportView);
                    break;

                case "Lab Billing Report":
                    reportView.DataSource = ReportDataDefinitions.GetSubjectProductSummaryBillingReportData(reportView.FlowProject, reportView.SubjectFieldList);
                    ReportDataDefinitions.Sort(reportView);
                    break;

                case "All Subjects":
                    reportView.DataSource = ReportDataDefinitions.GetAllSubjectsReportData(reportView.FlowProject, reportView.SubjectFieldList);
                    ReportDataDefinitions.Sort(reportView);
                    break;

				case "Photographed Subjects":
					reportView.DataSource = ReportDataDefinitions.GetPhotographedSubjectsReportData(reportView.FlowProject, reportView.SubjectFieldList);
                    ReportDataDefinitions.Sort(reportView);
                    break;

				case "Unphotographed Subjects":
					reportView.DataSource = ReportDataDefinitions.GetUnphotographedSubjectsReportData(reportView.FlowProject, reportView.SubjectFieldList);
                    ReportDataDefinitions.Sort(reportView);
                    break;

               
                case "Subject Orders Detailed":
                    reportView.DataSource = ReportDataDefinitions.GetSubjectOrdersDetailedReportData(reportView.FlowProject, reportView.SubjectFieldList, reportView.ShippingOrganization);
                    reportView.DoGroupSort = false;
                    reportView.ShowGroupByValue = false;
                    reportView.ShowReportName = false;
                    break;

                //case "Catalog Packages":
                //    reportView.DataSource = ReportDataDefinitions.GetCatalogPackagesReportData(reportView.FlowProject);
                //    reportView.DoGroupSort = false;
                //    reportView.ShowGroupByValue = false;
                //    reportView.ShowReportName = false;
                //    break;

                case "Subject Barcodes":
                    //reportView.DataSource = ReportDataDefinitions.GetSubjectBarcodesReportData(reportView.FlowProject, reportView.SubjectFieldList);
                    //reportView.DoGroupSort = false;
                    //reportView.ShowGroupByValue = false;
                    //reportView.ShowReportName = false;
                    break;
			}

			
		}

        public static void ExportReport(string defaultFileName, C1PrintDocument document, KeyValuePair<string, string> exportFormat, string reportFileName, bool showOptions)
		{
            if (defaultFileName.StartsWith("> "))
                defaultFileName = defaultFileName.Substring(2);

			if (exportFormat.Key == "Excel") defaultFileName += ".xls";
			else if (exportFormat.Key == "PDF") defaultFileName += ".pdf";
			else defaultFileName += ".htm";

            if (reportFileName == null)
            {
                reportFileName = DialogUtility.SaveFile(
                    "Select export file",
                    Environment.GetFolderPath(Environment.SpecialFolder.Desktop),
                    defaultFileName,
                    exportFormat.Value
                );
            }

			if (!String.IsNullOrEmpty(reportFileName))
			{
                try
                {
                    switch (exportFormat.Key)
                    {
                        case "Excel":
                            ReportDataDefinitions.ExportReportToExcel(document, reportFileName);
                            break;

                        case "PDF":
                            ReportDataDefinitions.ExportReportToPdf(document, reportFileName, showOptions);
                            break;

                        case "HTML":
                            ReportDataDefinitions.ExportReportToHtml(document, reportFileName);
                            break;
                    }
                    
                }
                catch (IOException ex)
                {
                    if (ex.Message.Contains("another process"))
                        MessageBox.Show("Could not export the report because the file exists and is already open in another application.\n\n", "Report Export Failed");
                    else
                        throw;
                }
			}
		}

        public static void ExportCSVReport(string defaultFileName, object DataSource, KeyValuePair<string, string> exportFormat, string reportFileName, bool showOptions)
        {
            

            if (defaultFileName.StartsWith("> "))
                defaultFileName = defaultFileName.Substring(2);


            if (reportFileName == null)
            {
                reportFileName = DialogUtility.SaveFile(
                    "Select export file",
                    Environment.GetFolderPath(Environment.SpecialFolder.Desktop),
                    defaultFileName,
                    exportFormat.Value
                );
            }

            if (!String.IsNullOrEmpty(reportFileName))
            {
                var csv = new StringBuilder();

                if (DataSource.GetType() == typeof(DataTable))
                {

                    string thisrow = "";
                    foreach (DataColumn dc in ((DataTable)DataSource).Columns)
                    {
                        thisrow = thisrow + dc.ColumnName + ",";
                    }
                    thisrow = thisrow.TrimEnd(',');
                    csv.Append(thisrow + Environment.NewLine);

                    foreach (DataRow dr in ((DataTable)DataSource).Rows)
                    {
                        thisrow = "";
                        int i = 0;
                        foreach (DataColumn dc in ((DataTable)DataSource).Columns)
                        {
                            thisrow = thisrow + "\"" + dr[i].ToString().Replace("\n", " -- ") + "\",";
                            i++;
                        }
                        thisrow = thisrow.TrimEnd(',');
                        csv.Append(thisrow + Environment.NewLine);
                    }
                }
                else
                {
                    csv.Append("\"Product\",\"SKU\",\"Cost\",\"Qty\",\"Total\"" + Environment.NewLine);
                    foreach (KeyValuePair<string, Dictionary<string, List<object>>> kvp in (Dictionary<string, Dictionary<string, List<object>>>)DataSource)
                    {
                        
                        foreach (KeyValuePair<string, List<object>> prod in kvp.Value)
                        {
                            string thisrow = "\"" + prod.Key.ToString().Replace("\n", " -- ") + ": " + "\"," + "\"" + prod.Value[1].ToString().Replace("\n", " -- ") + "\"," + "\"" + "$" + prod.Value[2].ToString().Replace("\n", " -- ") + "\"," + "\"" + prod.Value[0].ToString().Replace("\n", " -- ") + "\"," + "\"" + "$" + (Convert.ToDouble(prod.Value[0]) * Convert.ToDouble(prod.Value[2])).ToString("0.00") + "\"";
                            csv.Append(thisrow + Environment.NewLine);
                        }
                    }
                }
                File.WriteAllText(reportFileName, csv.ToString());

            }

            //if (exportFormat.Key == "Excel") defaultFileName += ".xls";
            //else if (exportFormat.Key == "PDF") defaultFileName += ".pdf";
            //else defaultFileName += ".htm";

            //if (reportFileName == null)
            //{
            //    reportFileName = DialogUtility.SaveFile(
            //        "Select export file",
            //        Environment.GetFolderPath(Environment.SpecialFolder.Desktop),
            //        defaultFileName,
            //        exportFormat.Value
            //    );
            //}

            //if (!String.IsNullOrEmpty(reportFileName))
            //{
            //    try
            //    {
            //        switch (exportFormat.Key)
            //        {
            //            case "Excel":
            //                ReportDataDefinitions.ExportReportToExcel(document, reportFileName);
            //                break;

            //            case "PDF":
            //                ReportDataDefinitions.ExportReportToPdf(document, reportFileName, showOptions);
            //                break;

            //            case "HTML":
            //                ReportDataDefinitions.ExportReportToHtml(document, reportFileName);
            //                break;
            //        }

            //    }
            //    catch (IOException ex)
            //    {
            //        if (ex.Message.Contains("another process"))
            //            MessageBox.Show("Could not export the report because the file exists and is already open in another application.\n\n", "Report Export Failed");
            //        else
            //            throw;
            //    }
            //}
        }

		public static void ExportReportToExcel(object document, string reportFileName)
		{
			XlsExporter exporter = new XlsExporter();
			exporter.Document = document;
			exporter.Export(reportFileName);
		}

		public static void ExportReportToPdf(object document, string reportFileName, bool showOptions)
		{
            bool keepTrying = true;
            int tryCount = 0;
            while (keepTrying)
            {
                try
                {
                    tryCount++;
                    //create a jpg of the document
                    //string tempjpg = Path.Combine(FlowContext.FlowTempDirPath, "temp.pdf");


                    (document as C1PrintDocument).Export(reportFileName);

                    
                    //PdfExporter exporter = new PdfExporter();
                    
                    //exporter.ShowOptions = showOptions;
                    //exporter.Document = document;
                    //exporter.Export(reportFileName);
                    keepTrying = false;
                }
                catch (Exception e)
                {
                    logger.Error("FAILED DURING REPORT PDF CREATION, Trying Again : " + tryCount);
                    logger.Error(e.Message);
                    System.Threading.Thread.Sleep(1000);
                    if (tryCount > 5)
                        keepTrying = false;
                }
            }
		}

		public static void ExportReportToHtml(object document, string reportFileName)
		{
			HtmlExporter exporter = new HtmlExporter();
			exporter.Document = document;
			exporter.Export(reportFileName);
		}

		/// <summary>
		/// Dynamically generates a query containing anonymous types with properties specified by the
		/// subjectFields and reportFields parameters; this uses the Dynamic LINQ extension Select method
		/// found in Dynamic.cs (Flow.Lib.Linq)
		/// </summary>
		/// <param name="source"></param>
		/// <param name="subjectFields"></param>
		/// <param name="reportFields"></param>
		/// <returns></returns>
		public static IQueryable Select(this IQueryable source, IEnumerable<ProjectSubjectField> subjectFields, params string[] reportFields)
		{
			List<string> selectorTextList = new List<string>();

			// Add group fields
			foreach(ProjectSubjectField groupField in subjectFields.Where(f => f.IsGroupField))
			{
				selectorTextList.Add(ReportDataDefinitions.GetSubjectFieldSelectorText(groupField));
			}
			
			// Add the SubjectName field
			selectorTextList.Add("SubjectName");

			// Add non-group fields
			foreach (ProjectSubjectField nonGroupField in subjectFields.Where(f => !f.IsGroupField))
			{
				selectorTextList.Add(ReportDataDefinitions.GetSubjectFieldSelectorText(nonGroupField));
			}

			// Add report-specific fields
			if (reportFields != null)
			{
				foreach (string reportField in reportFields)
				{
					selectorTextList.Add(reportField);
				}
			}

			// Concatenate the selector
			string selector = "new(" + String.Join(",", selectorTextList.ToArray()) + ")";

			// Return the results (IQuerayble) of the dynamic query
			return source.Select(selector);
		}

		/// <summary>
		/// Returns a string representing the selector text for a subject field
		/// </summary>
		/// <param name="field"></param>
		/// <returns></returns>
		private static string GetSubjectFieldSelectorText(ProjectSubjectField field)
		{
			return
				"s.SubjectData[\"" + field.ProjectSubjectFieldDisplayName + "\"].Value as " + field.ProjectSubjectFieldDesc;
		}

		/// <summary>
		/// Supplies the DataTable columns with names/captions for use in the target FlowReport
		/// </summary>
		/// <param name="source"></param>
		/// <param name="subjectFields"></param>
		/// <param name="reportFields"></param>
		/// <returns></returns>
		public static DataTable RenameColumns(this DataTable source, IEnumerable<ProjectSubjectField> subjectFields, params KeyValuePair<string,string>[] reportFields)
		{
			List<KeyValuePair<string, string>> renamePairs = new List<KeyValuePair<string, string>>();
	
			// Add group fields
			foreach(ProjectSubjectField groupField in subjectFields.Where(f => f.IsGroupField))
			{
				renamePairs.Add(
					new KeyValuePair<string, string>(groupField.ProjectSubjectFieldDesc, groupField.ProjectSubjectFieldDisplayName)
				);
			}

			// Add the SubjectName field
			renamePairs.Add(new KeyValuePair<string, string>("SubjectName", "Subject Name"));

          
			
			// Add the non-group fields
			foreach (ProjectSubjectField groupField in subjectFields.Where(f => !f.IsGroupField))
			{
				renamePairs.Add(
					new KeyValuePair<string, string>(groupField.ProjectSubjectFieldDesc, groupField.ProjectSubjectFieldDisplayName)
				);
			}
			
			// Add report-specific column names/captions
			if (reportFields != null)
			{
				foreach (KeyValuePair<string,string> reportField in reportFields)
				{
					renamePairs.Add(reportField);
				}
			}

			// Get LINQ-accessible collection for each columns max text length
			EnumerableRowCollection<DataRow> rows = source.AsEnumerable();

            if (source.Rows.Count > 0)
            {
                // Iterate through the list and assign the name/caption pairs to the ColumnName and Caption properties
                // of the corresponding DataTable columns
                for (int i = 0; i < renamePairs.Count; i++)
                {
                    DataColumn column = source.Columns[i];

                    column.ColumnName = renamePairs[i].Key;
                    column.Caption = renamePairs[i].Value;

                    // Find and store the max (textual) item length for column width autodetection
                    int maxTextLength = 0;
                    foreach (DataRow row in rows)
                    {
                        string contents = row[column].ToString();
                        foreach (string line in contents.Split("\n"))
                        {
                            if (line.Length > maxTextLength)
                                maxTextLength = line.Length;
                        }
                    }
                    //int maxTextLength = rows.Max(r => r[column].ToString().Length);

                    if (column.Caption.Length > maxTextLength)
                        maxTextLength = column.Caption.Length;

                    column.ExtendedProperties.Add("MaxTextLength", maxTextLength);

                    if (column.ExtendedProperties.ContainsKey("IsGroupField"))
                    {
                        if (!source.ExtendedProperties.ContainsKey("HasGroupings"))
                            source.ExtendedProperties.Add("HasGroupings", true);
                    }
                    // If the column is a group field, indicate this
                    if (subjectFields.Any(f => f.ProjectSubjectFieldDesc == column.ColumnName && f.IsGroupField))
                    {
                        column.ExtendedProperties.Add("IsGroupField", true);

                        if (!source.ExtendedProperties.ContainsKey("HasGroupings"))
                            source.ExtendedProperties.Add("HasGroupings", true);
                    }

                    // If the column is a numeric type, flag it (for right alignment in the report)
                    if (column.DataType.IsNumeric())
                        column.ExtendedProperties.Add("IsNumeric", true);
                }
            }
			return source;
		}

		/// <summary>
		/// Sorts the data in the report DataTable, first by group fields (if any),
		/// then by column order (left-to-right)
		/// </summary>
		/// <param name="dataSource"></param>
		/// <param name="groupFields"></param>
		private static void Sort(FlowReportView reportView)
		{
			if (reportView.DataSource == null || (reportView.DataSource.Rows.Count < 1))
				return;
	
			List<string> sortFields = new List<string>();

			IEnumerable<ProjectSubjectField> groupFields = reportView.SubjectFieldList.Where(f => f.IsGroupField);

			// Add group fields to the sort list
			if (groupFields != null)
			{
				foreach (ProjectSubjectField groupField in groupFields)
				{
					sortFields.Add(groupField.ProjectSubjectFieldDesc);
				}
			}

			// Retrieve the list of non-group fields
			IEnumerable<DataColumn> nonGroupFields = reportView.DataSource.Columns.Cast<DataColumn>().Where(c =>
				groupFields == null ||
				!groupFields.Any(f => f.ProjectSubjectFieldDesc == c.ColumnName)
			);

			// Add non-grouped fields to the sort list
			foreach (DataColumn column in nonGroupFields)
			{
				sortFields.Add(column.ColumnName);
			}

			// Set the sort on the DataTable's DefaultView
			reportView.DataSource.DefaultView.Sort = String.Join(",", sortFields.ToArray());
		}

/// 
		#region REPORT DEFINITIONS
/// 

		/// <summary>
		/// 
		/// </summary>
		/// <param name="flowProject"></param>
		/// <param name="subjectFields"></param>
		/// <returns></returns>
		public static DataTable GetSubjectOrdersSummaryReportData(FlowProject flowProject, IEnumerable<ProjectSubjectField> subjectFields)
		{
			DataTable table = null;
	
            
            var query = (
                from p in ApplicablePackages
                select new
                {
                    SubjectName = p.SubjectOrder.Subject.FormalFullName,
                    p.SubjectOrder.Subject,
                    ProductPackageDesc = p.ProductPackageDesc,
                    Price = (p.ProductPackage == null) ? 0 : p.ProductPackage.Price,
                    OHistory = p.OrderHistories,
                    OrderPackage = p,
                    IQOrderID = p.IQOrderID,
                    WebOrderID = p.WebOrderID,
                    Notes = p.Notes
                    //OHistory = p.OrderHistoryText
                }
            ).AsQueryable().Select(subjectFields, "ProductPackageDesc", "Price", "IQOrderID", "WebOrderID", "Notes");

			if (query.Count() > 0)
			{
				table = query
					.ToDataTable()
					.RenameColumns(
						subjectFields,
						new KeyValuePair<string, string>("Package", "Package"),
						new KeyValuePair<string, string>("Price", "Price"),
                        new KeyValuePair<string, string>("IQOrderID", "IQOrderID"),
                        new KeyValuePair<string, string>("WebOrderID", "WebOrderID"),
                        new KeyValuePair<string, string>("Notes", "Notes")
					)
				;

				table.Columns["Price"].ExtendedProperties.Add("CalculationExpression", "Sum(Price)");
				table.Columns["Price"].ExtendedProperties.Add("Format", "#.00");
                table.Columns["IQOrderID"].ExtendedProperties.Add("HideColumn", "true");
                table.Columns["WebOrderID"].ExtendedProperties.Add("HideColumn", "true");
                table.Columns["Notes"].ExtendedProperties.Add("HideColumn", "true");
                if (!flowProject.ReportShowPrice)
                    table.Columns.Remove("Price");
			}

			return table;
		}

        public static DataTable GetSubjectOrdersSummaryReportDataOLD(FlowProject flowProject, IEnumerable<ProjectSubjectField> subjectFields)
        {
            DataTable table = null;

            //var query = (
            //    from s in flowProject.SubjectList
            //    from p in s.SubjectOrder.OrderPackageList
            //    select new
            //    {
            //        SubjectName = s.FormalFullName,
            //        s,
            //        ProductPackageDesc = p.ProductPackageDesc,
            //        Price = (p.ProductPackage == null) ? 0 : p.ProductPackage.Price
            //    }
            //).AsQueryable().Select(subjectFields, "ProductPackageDesc", "Price");

            string orderFilter = flowProject.SelectedReportOrderFilter;
            string orderFilterID = "ALL";
            bool onlineOrders = false;
            if (orderFilter == "New Online Orders")
            {
                onlineOrders = true;
                orderFilterID = null;
            }
            if (orderFilter == "New Orders")
            {
                orderFilterID = null;
            }
            if (orderFilter.Contains(" - "))
            {
                orderFilterID = orderFilter.Split(" - ")[0];
            }
            var query = (
                from s in flowProject.SubjectList
                from p in s.SubjectOrder.OrderPackageList
                select new
                {
                    SubjectName = s.FormalFullName,
                    s,
                    ProductPackageDesc = "(" + p.Qty + ") " + p.ProductPackageDesc,
                    Price = (p.ProductPackage == null) ? 0 : p.ProductPackage.Price * (p.Qty),
                    //ProductPackageDesc = p.ProductPackageDesc,
                    //Price = (p.ProductPackage == null) ? 0 : p.ProductPackage.Price,
                    OHistory = p.OrderHistories,
                    OrderPackage = p,
                    IQOrderID = p.IQOrderID,
                    WebOrderID = p.WebOrderID,
                    Notes = p.Notes
                    //OHistory = p.OrderHistoryText
                }
           ).Where(obj => obj.OrderPackage.SubjectOrder.IsMarkedForDeletion != true && ((orderFilterID == null && string.IsNullOrEmpty(obj.OrderPackage.LabOrderID.ToString())) ||
                           (obj.OrderPackage.LabOrderID.ToString() == orderFilterID) ||
                           (orderFilterID == null && obj.OrderPackage.LabOrderID == null) ||
                           (orderFilterID == "ALL"))).AsQueryable().Select(subjectFields, "ProductPackageDesc", "Price", "IQOrderID", "WebOrderID", "Notes");

            //).Where(obj => obj.OrderPackage.SubjectOrder.IsMarkedForDeletion != true && ((!onlineOrders && !obj.OrderPackage.IsOnlineOrder && orderFilterID == null && string.IsNullOrEmpty(obj.OrderPackage.LabOrderID.ToString())) ||
            //              (!onlineOrders && !obj.OrderPackage.IsOnlineOrder && obj.OrderPackage.LabOrderID.ToString() == orderFilterID) ||
            //              (onlineOrders && orderFilterID == null && obj.OrderPackage.IsOnlineOrder && obj.OrderPackage.LabOrderID == null) ||
            //              (!onlineOrders && !obj.OrderPackage.IsOnlineOrder && orderFilterID == "ALL"))).AsQueryable().Select(subjectFields, "ProductPackageDesc", "Product", "Price", "SubjectID", "TicketID", "ShipName", "ShipAddress", "ShipCityState");


            if (query.Count() > 0)
            {
                table = query
                    .ToDataTable()
                    .RenameColumns(
                        subjectFields,
                        new KeyValuePair<string, string>("Package", "Package"),
                        new KeyValuePair<string, string>("Price", "Price"),
                        new KeyValuePair<string, string>("IQOrderID", "IQOrderID"),
                        new KeyValuePair<string, string>("WebOrderID", "WebOrderID"),
                        new KeyValuePair<string, string>("Notes", "Notes")
                    )
                ;

                table.Columns["Price"].ExtendedProperties.Add("CalculationExpression", "Sum(Price)");
                table.Columns["Price"].ExtendedProperties.Add("Format", "#.00");
                table.Columns["IQOrderID"].ExtendedProperties.Add("HideColumn", "true");
                table.Columns["WebOrderID"].ExtendedProperties.Add("HideColumn", "true");
                table.Columns["Notes"].ExtendedProperties.Add("HideColumn", "true");
                if (!flowProject.ReportShowPrice)
                    table.Columns.Remove("Price");
            }

            return table;
        }

        public static DataTable GetSubjectProductSummaryBillingReportData(FlowProject flowProject, IEnumerable<ProjectSubjectField> subjectFields)
		{
			DataTable table = null;
            string orderFilter = flowProject.SelectedReportOrderFilter;
            string orderFilterID = "ALL";
            bool onlineOrders = false;
            if (orderFilter == "New Online Orders")
            {
                onlineOrders = true;
                orderFilterID = null;
            }
            if (orderFilter == "New Orders")
            {
                 orderFilterID = null;
            }
            if (orderFilter.Contains(" - "))
            {
                orderFilterID = orderFilter.Split(" - ")[0];
            }
            var query = (
                from s in flowProject.SubjectList
                from p in s.SubjectOrder.OrderPackageList
                select new
                {
                    SubjectName = s.FormalFullName,
                    s,
                    ProductPackageDesc = p.ProductPackageDesc,
                    Price = (p.ProductPackage == null) ? 0 : p.ProductPackage.Price,
                    OHistory = p.OrderHistories,
                    OrderPackage = p
                    //OHistory = p.OrderHistoryText
                }
            ).Where(obj => obj.OrderPackage.SubjectOrder.IsMarkedForDeletion != true && ((orderFilterID == null && obj.OHistory.Count() == 0) ||
                           (obj.OHistory.Where(oh => oh.OrderID == orderFilterID).Count() > 0) ||
                           (orderFilterID == null && obj.OrderPackage.LabOrderID == null) ||
                           (orderFilterID == "ALL"))).AsQueryable().Select(subjectFields, "ProductPackageDesc", "Price");

			if (query.Count() > 0)
			{
				table = query
					.ToDataTable()
					.RenameColumns(
						subjectFields,
						new KeyValuePair<string, string>("Package", "Package"),
						new KeyValuePair<string, string>("Price", "Price")
					)
				;

				table.Columns["Price"].ExtendedProperties.Add("CalculationExpression", "Sum(Price)");
				table.Columns["Price"].ExtendedProperties.Add("Format", "#.00");
                if (!flowProject.ReportShowPrice)
                    table.Columns.Remove("Price");
			}

			return table;
		}
        private static string getProductList(FlowObservableCollection<OrderProduct> prds)
        {
            string retVal =  "";
            foreach( OrderProduct prd in prds)
            {
                retVal += prd.Quantity + " - " + prd.OrderProductLabel + "\n";
            }
            return retVal + "\n";
        }



        public static DataTable GetCatalogPackagesReportData(FlowProject flowProject)
        {
            
            DataTable table = null;
            var query = (
                from p in flowProject.FlowCatalog.ProductPackages
                //from prd in p.OrderProductList
                select new
                {
                    p
                }
            ).AsQueryable();


            if (query.Count() > 0)
            {
                table = query.ToDataTable();

               
                table.Columns["ProductPackageDesc"].ExtendedProperties.Add("HideDuplicates", "true");
                
            }

            return table;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="flowProject"></param>
        /// <param name="subjectFields"></param>
        /// <returns></returns>
        /// 
        public static DataTable GetSubjectOrdersDetailedReportData_NEW_Broken(FlowProject flowProject, IEnumerable<ProjectSubjectField> subjectFields, Organization shipOrg)
        {
            //if (ApplicableSubjects == null)
            //{
            //    try
            //    {
            //        ApplicableSubjects = flowProject.FlowProjectDataContext.FilteredOrderPackages.Select(s => s.SubjectOrder.Subject).OrderBy(sa => sa.FormalFullName).ToList<Subject>();
            //    }
            //    catch
            //    {
            //        //ignore, not really that important, only ApplicablePackages is used here
            //    }
            //}

            if (ApplicablePackages == null)
            {
                ApplicablePackages = flowProject.FlowProjectDataContext.FilteredOrderPackages.ToList<OrderPackage>();
            }

            string shipName="";
            string shipAddress="";
            string shipCityState="";
            if (shipOrg != null)
            {
                shipName = shipOrg.OrganizationName;
                shipAddress = shipOrg.OrganizationShippingAddressLine1;
                shipCityState = shipOrg.OrganizationShippingCity + ", " + shipOrg.OrganizationShippingStateCode + " " + shipOrg.OrganizationShippingZipCode;
            }
           
            DataTable table = null;

            
            var query = (
                from p in ApplicablePackages
                select new
                {
                    SubjectName = p.SubjectOrder.Subject.FormalFullName,
                    p.SubjectOrder.Subject,
                    ProductPackageDesc = p.ProductPackageDesc,
                    Price = (p.ProductPackage == null) ? 0 : p.ProductPackage.Price,
                    OHistory = p.OrderHistories,
                    Product = getProductList(p.OrderProductList),
                    SubjectID = p.SubjectOrder.Subject.SubjectID,
                    TicketID = p.SubjectOrder.Subject.TicketCode,
                    OrderPackage = p,
                    ShipName = shipName,
                    ShipAddress = shipAddress,
                    ShipCityState = shipCityState,
                    IQID = p.IQOrderID,
                    WebID = p.WebOrderID,
                    s = p.SubjectOrder.Subject
                }
            ).AsQueryable().Select(subjectFields, "ProductPackageDesc", "Product", "Price", "SubjectID", "TicketID", "ShipName", "ShipAddress", "ShipCityState", "IQID", "WebID","s");


            if (query.Count() > 0)
            {
                table = query.ToDataTable();

                table.Columns["Price"].ExtendedProperties.Add("CalculationExpression", "Sum(Price)");
                table.Columns["Price"].ExtendedProperties.Add("Format", "#.00");
                table.Columns["SubjectID"].ExtendedProperties.Add("IsGroupField", "true");
                table.Columns["TicketID"].ExtendedProperties.Add("HideColumn", "true");
                table.Columns["Price"].ExtendedProperties.Add("HideDuplicates", "true");
                table.Columns["ProductPackageDesc"].ExtendedProperties.Add("HideDuplicates", "true");
                table.Columns["ShipName"].ExtendedProperties.Add("HideColumn", "true");
                table.Columns["ShipAddress"].ExtendedProperties.Add("HideColumn", "true");
                table.Columns["ShipCityState"].ExtendedProperties.Add("HideColumn", "true");
                table.Columns["SubjectName"].ExtendedProperties.Add("HideColumn", "true");
                table.Columns["IQID"].ExtendedProperties.Add("HideColumn", "true");
                table.Columns["WebID"].ExtendedProperties.Add("HideColumn", "true");
                //table.Columns["SubjectName"].ExtendedProperties.Add("IsBold", "true");
                //table.Columns["SubjectName"].ExtendedProperties.Add("fontSize", (float)12);
                table.Columns["SubjectName"].ExtendedProperties.Add("IsPageSubTitle", "true");

                foreach (ProjectSubjectField psf in subjectFields)
                {
                    table.Columns[psf.SubjectFieldDesc].ExtendedProperties.Add("HideDuplicates", "true");
                }
                
                table.RenameColumns(
                        subjectFields,
                        new KeyValuePair<string, string>("Package", "Package"),
                        new KeyValuePair<string, string>("Product", "Product"),
                        new KeyValuePair<string, string>("Price", "Price"),
                         new KeyValuePair<string, string>("SubjectID", "SubjectID")
                        
                    )
                ;

                if (!flowProject.ReportShowPrice)
                    table.Columns.Remove("Price");
            }

            return table;
        }

        public static DataTable GetSubjectOrdersDetailedReportData(FlowProject flowProject, IEnumerable<ProjectSubjectField> subjectFields, Organization shipOrg)
        {
            if (ApplicableSubjects == null)
            {
                //ApplicableSubjects = flowProject.FlowProjectDataContext.FilteredOrderPackages.Select(fop=>fop.SubjectOrder.Subject).Distinct().ToList<Subject>();
                ApplicableSubjects = flowProject.SubjectList.OrderBy(s => s.FormalFullName).ToList<Subject>();
            }

            string shipName="";
            string shipAddress="";
            string shipCityState="";
            if (shipOrg != null)
            {
                shipName = shipOrg.OrganizationName;
                shipAddress = shipOrg.OrganizationShippingAddressLine1;
                shipCityState = shipOrg.OrganizationShippingCity + ", " + shipOrg.OrganizationShippingStateCode + " " + shipOrg.OrganizationShippingZipCode;
            }
           
            DataTable table = null;

            string orderFilter = flowProject.SelectedReportOrderFilter;
            string orderFilterID = "ALL";
            bool onlineOrders = false;
            if (orderFilter == "New Online Orders")
            {
                onlineOrders = true;
                orderFilterID = null;
            }
            
            if (orderFilter == "New Orders")
            {
                orderFilterID = null;
            }
            if (orderFilter.Contains(" - "))
            {
                orderFilterID = orderFilter.Split(" - ")[0];
            }
            var query = (
                from s in ApplicableSubjects
                from p in s.SubjectOrder.OrderPackages
                //from prd in p.OrderProductList
                select new
                {
                    SubjectName = s.FormalFullName,
                    s,
                    ProductPackageDesc = "(" + p.Qty + ") " + p.ProductPackageDesc,
                    Price = (p.ProductPackage == null) ? 0 : p.ProductPackage.Price * (p.Qty),
                    OHistory = p.OrderHistories,
                    Product = getProductList(p.OrderProductList),
                    SubjectID = s.SubjectID,
                    TicketID = s.TicketCode,
                    OrderPackage = p,
                    ShipName = shipName,
                    ShipAddress = shipAddress,
                    ShipCityState = shipCityState,
                    IQID = p.IQOrderID,
                    WebID = p.WebOrderID
                }
             ).Where(obj => obj.OrderPackage.SubjectOrder.IsMarkedForDeletion != true && ((orderFilterID == null && string.IsNullOrEmpty(obj.OrderPackage.LabOrderID.ToString())) ||
                           (obj.OrderPackage.LabOrderID.ToString() == orderFilterID) ||
                           (orderFilterID == null && obj.OrderPackage.LabOrderID == null) ||
                           (orderFilterID == "ALL"))).AsQueryable().Select(subjectFields, "ProductPackageDesc", "Product", "Price", "SubjectID", "TicketID", "ShipName", "ShipAddress", "ShipCityState", "IQID", "WebID");


            if (query.Count() > 0)
            {
                table = query.ToDataTable();

                table.Columns["Price"].ExtendedProperties.Add("CalculationExpression", "Sum(Price)");
                table.Columns["Price"].ExtendedProperties.Add("Format", "#.00");
                table.Columns["SubjectID"].ExtendedProperties.Add("IsGroupField", "true");
                table.Columns["TicketID"].ExtendedProperties.Add("HideColumn", "true");
                table.Columns["Price"].ExtendedProperties.Add("HideDuplicates", "true");
                table.Columns["ProductPackageDesc"].ExtendedProperties.Add("HideDuplicates", "true");
                table.Columns["ShipName"].ExtendedProperties.Add("HideColumn", "true");
                table.Columns["ShipAddress"].ExtendedProperties.Add("HideColumn", "true");
                table.Columns["ShipCityState"].ExtendedProperties.Add("HideColumn", "true");
                table.Columns["SubjectName"].ExtendedProperties.Add("HideColumn", "true");
                table.Columns["IQID"].ExtendedProperties.Add("HideColumn", "true");
                table.Columns["WebID"].ExtendedProperties.Add("HideColumn", "true");
                //table.Columns["SubjectName"].ExtendedProperties.Add("IsBold", "true");
                //table.Columns["SubjectName"].ExtendedProperties.Add("fontSize", (float)12);
                table.Columns["SubjectName"].ExtendedProperties.Add("IsPageSubTitle", "true");

                foreach (ProjectSubjectField psf in subjectFields)
                {
                    table.Columns[psf.SubjectFieldDesc].ExtendedProperties.Add("HideDuplicates", "true");
                }
                
                table.RenameColumns(
                        subjectFields,
                        new KeyValuePair<string, string>("Package", "Package"),
                        new KeyValuePair<string, string>("Product", "Product"),
                        new KeyValuePair<string, string>("Price", "Price"),
                         new KeyValuePair<string, string>("SubjectID", "SubjectID")
                        
                    )
                ;

                if (!flowProject.ReportShowPrice)
                    table.Columns.Remove("Price");
            }

            return table;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="flowProject"></param>
        /// <param name="subjectFields"></param>
        /// <returns></returns>
        public static DataTable GetSubjectBarcodesReportData(FlowProject flowProject, IEnumerable<ProjectSubjectField> subjectFields)
        {
            FlowObservableCollection<Subject> tempSubjectList = new FlowObservableCollection<Subject>();
            for (int x = 0; x < flowProject.SubjectList.View.Count; x++)
            {
                Subject sub = (Subject)flowProject.SubjectList.View.GetItemAt(x);
                if (sub.Exclude) continue;
                tempSubjectList.Add(sub);
            }

            var query = (
                 from s in tempSubjectList
                 select new
                 {
                     SubjectName = s.FormalFullName,
                     SubjectID = s.SubjectID,
                     Organization = s.FlowProjectDataContext.FlowProject.Organization.OrganizationName,
                     s
                 }
             ).AsQueryable().Select(subjectFields, "SubjectID", "Organization");

            
            DataTable table = query
                .ToDataTable();

            return table;
        }





		/// <summary>
		/// 
		/// </summary>
		/// <param name="flowProject"></param>
		/// <param name="subjectFields"></param>
		/// <returns></returns>
		public static DataTable GetPhotographedSubjectsReportData(FlowProject flowProject, IEnumerable<ProjectSubjectField> subjectFields)
		{
			var query = (
				from s in flowProject.SubjectList
                from i in s.ImageListNoGroups
				select new
				{
					SubjectName = s.FormalFullName,
                    TicketID = s.TicketCode,
					s,
					ImageFileName = i.ImageFileName
				}
			).AsQueryable().Select(subjectFields, "TicketID", "ImageFileName");

			DataTable table = query
				.ToDataTable()
				.RenameColumns(
                    subjectFields,
                new KeyValuePair<string, string>[2]{new KeyValuePair<string, string>("TicketID", "Ticket ID"),
                new KeyValuePair<string, string>("ImageFileName", "Image Filename")}
				)
			;

			return table;
		}

      
		/// <summary>
		/// 
		/// </summary>
		/// <param name="flowProject"></param>
		/// <param name="subjectFields"></param>
		/// <returns></returns>
		public static DataTable GetUnphotographedSubjectsReportData(FlowProject flowProject, IEnumerable<ProjectSubjectField> subjectFields)
		{
			var query =	(
				from s in flowProject.SubjectList
                from i in s.ImageListNoGroups.DefaultIfEmpty()
				select new
				{
					SubjectName = s.FormalFullName,
                    TicketID = s.TicketCode,
					s,
					i
				}
            ).Where(q => q.i == null).AsQueryable().Select(subjectFields, "TicketID");

			DataTable table = query
				.ToDataTable()
				.RenameColumns(subjectFields, new KeyValuePair<string, string>("TicketID", "Ticket ID"))
			;

			return table;
		}

        public static DataTable GetAllSubjectsReportData(FlowProject flowProject, IEnumerable<ProjectSubjectField> subjectFields)
        {
            FlowObservableCollection<Subject> tempSubjectList = new FlowObservableCollection<Subject>();
            for (int x = 0; x < flowProject.SubjectList.View.Count; x++)
            {
                Subject sub = (Subject)flowProject.SubjectList.View.GetItemAt(x);
                if (sub.Exclude) continue;
                tempSubjectList.Add(sub);
            }
            //flowProject.SubjectList
            var query = (
                from s in tempSubjectList
                select new
                {
                    SubjectName = s.FormalFullName,
                    TicketID = s.TicketCode,
                    s
                }
            ).AsQueryable().Select(subjectFields, "TicketID");

            DataTable table = query
                .ToDataTable()
                .RenameColumns(subjectFields, new KeyValuePair<string, string>("TicketID", "Ticket ID"))
            ;

            return table;
        }

/// 
		#endregion REPORT DEFINITIONS
/// 

	}	// END class
	
}	// END namespace
