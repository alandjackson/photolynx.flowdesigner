﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Data.Linq;
using System.IO;
using System.Linq.Dynamic;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

using C1.C1Report;

using Flow.Lib;
using Flow.Lib.Helpers;
using Flow.Schema.LinqModel.DataContext;

namespace Flow.Schema.Reports
{
	/// <summary>
	/// Represents a storable/restorable report configuration (selected subject fields, grouping levels, etc.)
	/// </summary>
	[Serializable]
	public class FlowReportView : NotifyPropertyBase
	{
        public bool DoGroupSort = true;
        public bool ShowGroupByValue = true;
        public bool ShowReportName = true;
        public bool ShowProjectName = true;


		public static FlowReportView GetFlowReportView(FlowReportViewDef reportViewDef, FlowProject flowProject)
		{
			FlowReportView retVal;
            
			using (MemoryStream stream = new MemoryStream(reportViewDef.Data.ToArray()))
			{
				try
				{
					retVal = (FlowReportView)(new BinaryFormatter()).Deserialize(stream);
					

					retVal.FlowReportViewDef = reportViewDef;

					retVal.FlowProject = flowProject;

					foreach (SubjectFieldProxy proxy in retVal.SubjectFieldProxyList)
					{
						ProjectSubjectField field = flowProject.ProjectSubjectFieldList
							.FirstOrDefault(f => f.ProjectSubjectFieldDesc == proxy.ProjectSubjectField);

						if(field != null)
						{
							field.IsGroupField = proxy.IsGroupField;
							retVal.SubjectFieldList.Add(field);
						}
					}

					retVal.SubjectFieldList.CollectionChanged += new NotifyCollectionChangedEventHandler(retVal.SubjectFieldList_CollectionChanged);
				}
				catch (Exception e)
				{
					retVal = null;
				}
			}

			return retVal;
		}

		/// <summary>
		/// A scaled-down representation of report subject fields for serialization
		/// </summary>
		[Serializable]
		internal class SubjectFieldProxy
		{
			string _projectSubjectField = null;
			internal string ProjectSubjectField
			{
				get { return _projectSubjectField; }
				set { _projectSubjectField = value; }
			}

			bool _isGroupField = false;
			internal bool IsGroupField
			{
				get { return _isGroupField; }
				set { _isGroupField = value; }
			}

			internal SubjectFieldProxy(ProjectSubjectField field)
			{
				this.ProjectSubjectField = field.ProjectSubjectFieldDesc;
				this.IsGroupField = field.IsGroupField;
			}
		}

		[NonSerialized]
		private FlowReportViewDef _flowReportViewDef = null;
		private FlowReportViewDef FlowReportViewDef
		{
			get { return _flowReportViewDef; }
			set { _flowReportViewDef = value; }
		}

		/// <summary>
		/// A list of the selected subject fields (for serialization)
		/// </summary>
		private List<SubjectFieldProxy> SubjectFieldProxyList = null;

		public string _reportName = null;
		public string ReportName
		{
			get { return _reportName; }
			set { _reportName = value; }
		}

		public string _reportViewName = null;
		public string ReportViewName
		{
			get { return _reportViewName; }
			set
			{
				_reportViewName = value;

				this.SendPropertyChanged("ReportViewName");
			}
		}
		public string ReportTitle { get { return (this.ReportViewName ?? this.ReportName); } }

		private OrientationEnum _orientationEnum = OrientationEnum.Portrait;
		public OrientationEnum Orientation
		{
			get { return _orientationEnum; }
			set
			{
				_orientationEnum = value;

				this.SendPropertyChanged("Orientation");
				this.SendPropertyChanged("IsLandscape");
				this.SendPropertyChanged("IsPortrait");
			}
		}

		private bool _isDefault = false;
		internal bool IsDefault
		{
			get { return _isDefault; }
			set { _isDefault = value; }
		}

		public bool IsPortrait
		{
			get { return this.Orientation == OrientationEnum.Portrait; }
			set
			{
				if(value)
					this.Orientation = OrientationEnum.Portrait;
			}
		}

		public bool IsLandscape
		{
			get { return this.Orientation == OrientationEnum.Landscape; }
			set
			{
				if (value)
					this.Orientation = OrientationEnum.Landscape;
			}
		}

		/// <summary>
		/// Represents the first selected subject field in the SubjectFieldList
		/// </summary>
		public ProjectSubjectField SubjectField1
		{
			get
			{
				if (this.SubjectFieldList.Count == 0)
					return null;

				return this.SubjectFieldList[0];
			}
		}

		/// <summary>
		/// Represents the second selected subject field in the SubjectFieldList
		/// </summary>
		public ProjectSubjectField SubjectField2
		{
			get
			{
				if (this.SubjectFieldList.Count < 2)
					return null;

				return this.SubjectFieldList[1];
			}
		}

        public Organization ShippingOrganization { get; set; }

		/// <summary>
		/// The list of subject fields selected for report inclusion
		/// </summary>
		[NonSerialized]
		private ObservableCollection<ProjectSubjectField> _subjectFieldList = null;
		public ObservableCollection<ProjectSubjectField> SubjectFieldList
		{
			get
			{
				if (_subjectFieldList == null)
				{
					_subjectFieldList = new ObservableCollection<ProjectSubjectField>();
//					_subjectFieldList.CollectionChanged += new NotifyCollectionChangedEventHandler(SubjectFieldList_CollectionChanged);
				}

				return _subjectFieldList;
			}
            set
            {
                _subjectFieldList = value;
            }
		}

		/// <summary>
		/// The list of subject fields available (but not selected) for report inclusion
		/// </summary>
		public IEnumerable<string> AvailableSubjectFieldList
		{
			get
			{
				return this.FlowProject.SortableSubjectFieldList.Where(f =>
					f != "[None]" && !this.SubjectFieldList.Any(s => s.ProjectSubjectFieldDisplayName == f)
				);
			}
		}

		/// <summary>
		/// The report data
		/// </summary>
		[NonSerialized]
		private DataTable _dataSource = null;
		public DataTable DataSource
		{
			get { return _dataSource; }
			internal set { _dataSource = value; }
		}

        /// <summary>
        /// The report data
        /// </summary>
        [NonSerialized]
        private List<Field> _detailFields = null;
        public List<Field> DetailFields
        {
            get { return _detailFields; }
            internal set { _detailFields = value; }
        }

		/// <summary>
		/// The project in which the report view resides
		/// </summary>
		[NonSerialized]
		public FlowProject _flowProject = null;
		public FlowProject FlowProject
		{
			get { return _flowProject; }
			private set { _flowProject = value; }
		}


		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="reportName"></param>
		/// <param name="flowProject"></param>
		public FlowReportView(string reportName, FlowProject flowProject)
		{
			this.ReportName = reportName;
			this.FlowProject = flowProject;
			this.ReportViewName = reportName;

			this.IsDefault = true;

			this.SubjectFieldList.CollectionChanged += new NotifyCollectionChangedEventHandler(SubjectFieldList_CollectionChanged);
		}

		/// <summary>
		/// Event handler for CollectionChanged event of SubjectFieldList; sends property notifications
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		void SubjectFieldList_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
		{
			this.SendPropertyChanged("SubjectFieldList");
			this.SendPropertyChanged("AvailableSubjectFieldList");
			this.SendPropertyChanged("SubjectField1");
			this.SendPropertyChanged("SubjectField2");
		}

		/// <summary>
		/// Adds the ProjectSubjectField specified by the supplied description to the list of fields selected
		/// for report inclusion
		/// </summary>
		/// <param name="subjectFieldDesc"></param>
		public void Add(string subjectFieldDesc)
		{
			ProjectSubjectField selectedField = this.FlowProject.ProjectSubjectFieldList
				.FirstOrDefault(f => f.ProjectSubjectFieldDisplayName == subjectFieldDesc);

			if (selectedField != null)
			{
				this.SubjectFieldList.Add(selectedField);

				selectedField.PropertyChanged += new PropertyChangedEventHandler(ProjectSubjectField_PropertyChanged);
			}
		}

		/// <summary>
		/// Removes the ProjectSubjectField from the selected field list
		/// </summary>
		/// <param name="subjectFieldDesc"></param>
		public void Remove(ProjectSubjectField projectSubjectField)
		{
			projectSubjectField.PropertyChanged -= new PropertyChangedEventHandler(ProjectSubjectField_PropertyChanged);

			this.SubjectFieldList.Remove(projectSubjectField);
		}

		/// <summary>
		/// Ensures that the second selected field's IsGroupField property is set to false if the first's is as well
		/// (this ensures grouping priority)
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		void ProjectSubjectField_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			if (e.PropertyName == "IsGroupField")
			{
				ProjectSubjectField sourceField = sender as ProjectSubjectField;

				if (this.SubjectField2 != null)
				{
					if (this.SubjectField1 == sourceField && !this.SubjectField1.IsGroupField)
						this.SubjectField2.IsGroupField = false;
				}
			}
		}

		/// <summary>
		/// Serializes the PreferenceManager instance and returns the data for
		/// database storage
		/// </summary>
		/// <returns></returns>
		public void SetSerializationData(Table<FlowReportViewDef> flowReportViewDefs)
		{
			Binary data;

			this.SubjectFieldProxyList = new List<SubjectFieldProxy>();

			foreach (ProjectSubjectField field in this.SubjectFieldList)
			{
				this.SubjectFieldProxyList.Add(new SubjectFieldProxy(field));
			}

			using (MemoryStream stream = new MemoryStream())
			{
				BinaryFormatter formatter = new BinaryFormatter();
				formatter.Serialize(stream, this);
				data = stream.ToArray();
			}

			if (this.FlowReportViewDef == null)
			{
				this.FlowReportViewDef = new FlowReportViewDef();
				this.FlowReportViewDef.Data = data;

				flowReportViewDefs.InsertOnSubmit(this.FlowReportViewDef);
			}
			else
			{
				this.FlowReportViewDef.Data = data;
			}

			flowReportViewDefs.Context.SubmitChanges();
		}


	}	// END class

}	// END namespace
