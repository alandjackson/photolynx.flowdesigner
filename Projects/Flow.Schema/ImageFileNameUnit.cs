﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

using Flow.Schema.LinqModel.DataContext;

namespace Flow.Schema
{
	/// <summary>
	/// Represents a segment of a formatted image filename, either the value of a database field
	/// or a custom literal string
	/// </summary>
	public class ImageFileNameUnit
	{
		public readonly bool IsSubjectFieldName;
		public readonly string Value;

		public ImageFileNameUnit(bool isSubjectFieldName, string value)
		{
			IsSubjectFieldName = isSubjectFieldName;
			Value = (IsSubjectFieldName) ? value : Regex.Replace(value, @"[^A-Za-z0-9_-]", String.Empty);
		}
	}

}
