﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flow.Schema.LinqModel.DataContext
{
	partial class ProjectEventTrigger
	{
		public EventTriggerType EventTriggerType { get; private set; }
		public EventTrigger EventTrigger { get; private set; }

		public string EventTriggerTypeDesc
		{
			get { return (this.EventTriggerType == null) ? null : this.EventTriggerType.EventTriggerTypeDesc; }
		}

		public string EventTriggerDesc
		{
			get { return (this.EventTrigger == null) ? null : this.EventTrigger.EventTriggerDesc; }
		}

		internal void SetParent(EventTriggerType eventTriggerType, EventTrigger eventTrigger)
		{
			this.EventTriggerType = eventTriggerType;
			this.EventTrigger = eventTrigger;
		}
	}
}
