﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.Linq;
using System.Data.SqlServerCe;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Media.Imaging;

using Flow.Lib;
using Flow.Lib.Database.SqlCe;
using Flow.Lib.Helpers;
using Flow.Schema.Properties;
using Flow.Schema.Utility;


namespace Flow.Schema.LinqModel.DataContext
{
	/// <summary>
	/// Represents a user-defined project-level Subject field
	/// </summary>
	partial class ProjectSubjectField : ISubjectField
	{

        public bool IsEditable { get { return !IsReadOnly; } }

    

        private bool _rememberMe { get; set; }
        public bool RememberMe
        {
            get
            {
                return _rememberMe;
            }
            set
            {
                _rememberMe = value;
            }
        }

        public bool ShowRememberMe { 
            get {
                if (!IsReadOnly && this.SubjectFieldDesc != "FirstName" &&
                     this.SubjectFieldDesc != "LastName" &&
                     this.SubjectFieldDesc != "TicketCode" &&
                     this.SubjectFieldDesc != "PackageSummary")
                    return true;
                else
                    return false;
            } 
        }

        public bool IsPickListField
        {
            get
            {
                if (FlowContext.ShowNewFeatures)
                {
                    //return false;
                    if (this.SubjectFieldDesc == "Teacher" || this.SubjectFieldDesc == "Grade" || this.SubjectFieldDesc == "Class")
                        return true;
                }

                return false;
            }
        }
		#region FIELDS AND PROPERTIES

       

        private string _searchTerm { get; set; }
        public string SearchTerm { 
            get { return _searchTerm; } 
            set {
                _searchTerm = value ;
                SendPropertyChanged("SearchTerm");
            } 
        }

		/// <summary>
		/// The application datatype representation assigned to the Subject user-defined field
		/// </summary>
		public FieldDataType _subjectFieldDataType = null;
		public FieldDataType SubjectFieldDataType
		{
			get
			{
				if (_subjectFieldDataType == null)

					// Conversion through implicit operator implementation
					_subjectFieldDataType = this.FieldDataTypeID;

				return _subjectFieldDataType;
			}

			set
			{
				_subjectFieldDataType = value;
				this.FieldDataTypeID = _subjectFieldDataType.FieldDataTypeID;

				this.SendPropertyChanged("SubjectFieldDataType");
			}
		}

		/// <summary>
		/// The name of the field to be displayed in the UI
		/// </summary>
		public string SubjectFieldDisplayName
		{
			get { return this.ProjectSubjectFieldDisplayName; }
			set
			{
				this.ProjectSubjectFieldDisplayName = value.Normalize();
				this.SendPropertyChanged("SubjectFieldDisplayName");
			}

		}

		/// <summary>
		/// The field name as assigned to the source database table column
		/// </summary>
		public string SubjectFieldDesc { get { return this.ProjectSubjectFieldDesc; } }

		/// <summary>
		/// Indicates that the field is a report grouping field
		/// </summary>
		private bool _isGroupField = false;
		public bool IsGroupField
		{
			get { return _isGroupField; }
			set
			{
				_isGroupField = value;
				this.SendPropertyChanged("IsGroupField");
			}
		}

		#endregion


		#region CONSTRUCTORS

		public ProjectSubjectField(
			string projectSubjectFieldDisplayName,
			FieldDataType fieldDataType,
            int fieldDataSize,
			bool isSystemField,
			bool isKeyField,
			bool isRequiredField,
			bool isScanKeyField,
			bool isProminentField,
			bool isSearchableField,
            bool isShownInEdit,
            bool isReadOnly
		)
		{
			this.ProjectSubjectFieldDisplayName = projectSubjectFieldDisplayName;
			this.SubjectFieldDataType = fieldDataType;
            this.FieldDataSize = fieldDataSize;
			this.IsSystemField = isSystemField;
			this.IsKeyField = isKeyField;
			this.IsRequiredField = isRequiredField;
			this.IsScanKeyField = isScanKeyField;
			this.IsProminentField = isProminentField;
			this.IsSearchableField = isSearchableField;
            this.IsShownInEdit = isShownInEdit;
            this.IsReadOnly = isReadOnly;
            this._rememberMe = false;
		}

		// NOTE: Following constructor appears obsolete; consider for removal
	
		//public ProjectSubjectField(
		//    string projectSubjectFieldDisplayName,
		//    FieldDataType fieldDataType
		//)
		//{
		//    this.ProjectSubjectFieldDisplayName = this.ProjectSubjectFieldDisplayName.FieldNameNormalize();
		//    this.SubjectFieldDataType = fieldDataType;
		//}

		#endregion

		/// <summary>
		/// Invoked when the display name of the field changes, ensuring the source column name
		/// is the normalized derivation of the display name
		/// </summary>
		partial void OnProjectSubjectFieldDisplayNameChanged()
		{
			this.ProjectSubjectFieldDesc = this.ProjectSubjectFieldDisplayName.Replace(" ", "");
		}

		/// <summary>
		/// Invoked when the IsKeyField property is changed, ensuring that the IsRequiredField
		/// property is set true if IsKeyField is true
		/// </summary>
		partial void OnIsKeyFieldChanged()
		{
			if (this.IsKeyField)
				this.IsRequiredField = true;
		}

		/// <summary>
		/// Invoked when the IsRequiredField property is changed; ensuring that IsKeyField
		/// property is set to false if IsRequiredField is false and that IsProminentField
		/// is true if IsRequiredField is true
		/// </summary>
		partial void OnIsRequiredFieldChanged()
		{
			if (this.IsRequiredField)
				this.IsProminentField = true;
			else
				this.IsKeyField = false;
		}

		/// <summary>
		/// Invoked when the IsProminentField property is changed, ensuring that IsRequired and
		/// IsKeyField values are false if IsProminentField is false
		/// </summary>
		partial void OnIsProminentFieldChanged()
		{
			if (!this.IsProminentField)
				this.IsRequiredField = false;
		}


		/// <summary>
		/// Return an ISubjectField representation of the object
		/// </summary>
		/// <returns></returns>
		public ISubjectField Clone()
		{
			return new ProjectSubjectField(
				this.ProjectSubjectFieldDisplayName,
				this.SubjectFieldDataType,
                this.FieldDataSize,
				this.IsSystemField,
				this.IsKeyField,
				this.IsRequiredField,
				this.IsScanKeyField,
				this.IsProminentField,
				this.IsSearchableField,
                this.IsShownInEdit,
                this.IsReadOnly
			);
		}
	}
}
