﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.Linq;
using System.Data.SqlServerCe;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Media.Imaging;

using Flow.Lib;
using Flow.Lib.Database.SqlCe;
using Flow.Lib.Helpers;
using Flow.Schema.Properties;
using Flow.Schema.Utility;


namespace Flow.Schema.LinqModel.DataContext
{
	/// <summary>
	/// Represents a hierarchical organizational unit type applied at the project level
	/// </summary>
	partial class ProjectOrganizationalUnitType
	{

		/// <summary>
		/// List containing the child ProjectOrganizationalUnitTypes
		/// </summary>
		private IEnumerable<ProjectOrganizationalUnitType> _projectOrganizationalUnitTypes = null;
		public IEnumerable<ProjectOrganizationalUnitType> ProjectOrganizationalUnitTypes
		{
			get
			{
				// Select all child units (child ParentID = parent ID)
				return
					_projectOrganizationalUnitTypes
						.Where(ou => ou.ProjectOrganizationalUnitTypeParentID == this.ProjectOrganizationalUnitTypeID);
			}

			private set
			{
				_projectOrganizationalUnitTypes = value;

				// Recursively populate all children
				foreach (ProjectOrganizationalUnitType item in this.ProjectOrganizationalUnitTypes)
				{
					item.SetChildren(_projectOrganizationalUnitTypes);
				}
			}

		}

		/// <summary>
		/// Assigns the children ProjectOrganizationalUnitTypes of the type;
		/// recursively populates all children through the set accessor
		/// </summary>
		/// <param name="childrenItems"></param>
		internal void SetChildren(IEnumerable<ProjectOrganizationalUnitType> childrenItems)
		{
			this.ProjectOrganizationalUnitTypes = childrenItems;
		}

	}
}
