﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Flow.Lib.Helpers;
using System.IO;
using Flow.Schema.LinqModel.DataContext.FlowMaster;

namespace Flow.Schema.LinqModel.DataContext
{
	partial class OrderProduct
	{
		public string OrderProductLabel { get; internal set; }

        public void GreenScreenBackgroundObjectChanged()
        {
            _greenScreenBackgroundObject = null;
            SendPropertyChanged("GreenScreenBackgroundObject");
        }

        //used for a temperoray reference to a package, but we dont want this product to really be linked to the package.
        public OrderPackage TempOrderPackage { get; set; }

        public ProductPackageComposition ProductPackageComposition
        {
            get
            {
                if (this.OrderPackage.ProductPackage != null)
                {
                    if (this.OrderPackage.ProductPackage.ProductPackageCompositionList.Count(comp => comp.ImageQuixProduct.ResourceURL == this.OrderProductResourceURL) > 1)
                    {
                        if (this.OrderPackage.ProductPackage.ProductPackageCompositionList.Any(comp => comp.ImageQuixProduct.ResourceURL == this.OrderProductResourceURL && comp.DisplayLabel == this.Label))
                            return this.OrderPackage.ProductPackage.ProductPackageCompositionList.First(comp => comp.ImageQuixProduct.ResourceURL == this.OrderProductResourceURL && comp.DisplayLabel == this.Label);
                        else
                            return this.OrderPackage.ProductPackage.ProductPackageCompositionList.First(comp => comp.ImageQuixProduct.ResourceURL == this.OrderProductResourceURL);
                    }
                    else if (this.OrderPackage.ProductPackage.ProductPackageCompositionList.Count(comp => comp.ImageQuixProduct.ResourceURL == this.OrderProductResourceURL) == 1)
                    {
                        return this.OrderPackage.ProductPackage.ProductPackageCompositionList.First(comp => comp.ImageQuixProduct.ResourceURL == this.OrderProductResourceURL);
                    }
                    else
                    {
                        return null;
                    }
                }
                else return null;
            }
        }

        private GreenScreenBackground _greenScreenBackgroundObject { get; set; }
        public GreenScreenBackground GreenScreenBackgroundObject
        {
            get
            {
                if (_greenScreenBackgroundObject == null)
                {
                    if (this.GreenScreenBackground != null && File.Exists(this.GreenScreenBackground))
                        _greenScreenBackgroundObject =  new GreenScreenBackground(this.GreenScreenBackground);
                }
                return _greenScreenBackgroundObject;
            }
            set
            {
                if (value == null)
                    _greenScreenBackgroundObject = null;
                else
                {
                    this.GreenScreenBackground = value.FullPath;
                    _greenScreenBackgroundObject = null;
                    this.OrderPackage.SubjectOrder.Subject.FlowProjectDataContext.SubmitChanges();
                }
            }
        }

        public string OrderProductResourceURL { get; internal set; }
		//private FlowObservableCollection<OrderProductImage> _orderProductImageList = null;
		//public FlowObservableCollection<OrderProductImage> OrderProductImageList
		//{
		//    get
		//    {
		//        if (_orderProductImageList == null)
		//            _orderProductImageList = new FlowObservableCollection<OrderProductImage>(this.OrderProductImages);
				
		//        return _orderProductImageList;
		//    }
		//}

		private FlowObservableCollection<OrderProductNode> _orderProductNodeList = null;
		public FlowObservableCollection<OrderProductNode> OrderProductNodeList
		{
			get
			{ 
				if (_orderProductNodeList == null)
					_orderProductNodeList = new FlowObservableCollection<OrderProductNode>(this.OrderProductNodes);

				return _orderProductNodeList;
			}
		}

		private FlowObservableCollection<OrderProductOption> _orderProductOptionList = null;
		public FlowObservableCollection<OrderProductOption> OrderProductOptionList
		{
			get
			{
				if (_orderProductOptionList == null)
				{
					_orderProductOptionList = new FlowObservableCollection<OrderProductOption>(this.OrderProductOptions);

					if (this.OrderPackage != null && this.OrderPackage.ProductPackage != null)
					{
						foreach (OrderProductOption option in _orderProductOptionList)
						{
							ImageQuixOptionGroup optionGroup =
								this.OrderPackage.ProductPackage.FlowCatalog.GetImageQuixOptionGroupByKey(option.ImageQuixOptionGroupPk);
                            if(optionGroup != null)
                            {
							option.OptionGroupLabel = optionGroup.Label;
                            option.OptionGroupResourceURL = optionGroup.ResourceURL;
//							option.OptionList = optionGroup.OptionList;
                            }
						}
					}
				}

				return _orderProductOptionList;
			}
		}

        public void UpdateResourceURL(ImageQuixProduct product)
        {
            if(product != null)
             this.OrderProductResourceURL = product.ResourceURL;
        }
		/// <summary>
		/// 
		/// </summary>
		/// <param name="product"></param>
        public OrderProduct(ImageQuixProduct product, SubjectOrder subjectOrder, string label, string resourceURL)
            : this(product, subjectOrder, "", null, label, resourceURL)
        { }
        public OrderProduct(ImageQuixProduct product, SubjectOrder subjectOrder, string PackageImageType, SubjectImage subImage, string label, string resourceURL)
			: this()
		{
            this.Label = label;
            this.ResourceURL = resourceURL;

			this.ImageQuixProductPk = product.PrimaryKey;

			this.OrderProductLabel = product.Label;

            this.OrderProductResourceURL = product.ResourceURL;

			Subject subject = subjectOrder.Subject;
			SubjectImage selectedImage = null;

			if (subject.HasAssignedImages)
			{
				if (subject.ImageList.CurrentItem == null)
					subject.ImageList.MoveCurrentToFirst();

				selectedImage = subject.ImageList.CurrentItem;
                if (subImage != null)
                {
                    selectedImage = subImage;
                }
                else
                {
                    if (PackageImageType == "Group")
                    {
                        if (!selectedImage.IsGroupImage)
                        {
                            if (subject.ImageList.Count(i => i.IsGroupImage) > 0)
                                selectedImage = subject.ImageList.First(i => i.IsGroupImage);
                        }
                    }
                    if (PackageImageType == "Primary")
                    {
                        if (!selectedImage.IsPrimary)
                        {
                            if (subject.ImageList.Count(i => i.IsPrimary) > 0)
                                selectedImage = subject.ImageList.First(i => i.IsPrimary);
                        }
                    }
                }
			}

			foreach (ImageQuixProductNode node in product.ProductNodeList)
			{
				OrderProductNode orderProductNode = new OrderProductNode();
				orderProductNode.ImageQuixProductNodePk = node.PrimaryKey;
				orderProductNode.ImageQuixProductNodeType = node.Type;
                orderProductNode.ImageQuixProductNodeResourceURL = node.ResourceURL;

				if (node.Type == 1)
					orderProductNode.SubjectImage = selectedImage;

				this.OrderProductNodeList.Add(orderProductNode);
			}

			//if(selectedImage != null)
			//    subject.ImageList.MoveCurrentTo(selectedImage);

		}

		partial void OnLoaded()
		{
			//if (!this.Quantity.HasValue)
			//    this.Quantity = 0;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="flowProjectDataContext"></param>
		/// <returns></returns>
		internal bool RemoveChildren(FlowProjectDataContext flowProjectDataContext)
		{
			flowProjectDataContext.OrderProductNodes.DeleteAllOnSubmit(this.OrderProductNodes);
			this.OrderProductImages.Clear();

			flowProjectDataContext.OrderProductOptions.DeleteAllOnSubmit(this.OrderProductOptions);
			this.OrderProductOptions.Clear();

			return true;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="subjectImage"></param>
		internal void ReplaceUnassignedImage(SubjectImage deletionImage, SubjectImage replacementImage)
		{
			foreach (OrderProductNode orderProductNode in this.OrderProductNodeList.Where(i => i.SubjectImage == deletionImage))
				orderProductNode.SubjectImage = replacementImage;
		}

        public string UniqueProductImageName
        {
            get
            {
                return this.OrderPackage.SubjectOrder.Subject.TicketCode + "_" + this.OrderProductID + ".jpg";
            }
        }

		#region MERGE MEMBERS

		internal string ImageQuixProductPkPadded
		{ get { return this.ImageQuixProductPk.ZeroPad(8); } }
	
		private Guid _orderPackageGuid;
		internal Guid OrderPackageGuid
		{
			get { return _orderPackageGuid; }
			set
			{
				_orderPackageGuid = value;

				this.OrderProductGuid = Guid.NewGuid();

				foreach (OrderProductNode orderProductNode in this.OrderProductNodes)
				{
					orderProductNode.OrderProductGuid = this.OrderProductGuid;
				}

				foreach (OrderProductOption orderProductOption in this.OrderProductOptions)
				{
					orderProductOption.OrderProductGuid = this.OrderProductGuid;
				}
			}
		}

		internal Guid OrderProductGuid { get; private set; }

		/// <summary>
		/// Returns a clone of the source OrderProduct object
		/// </summary>
		/// <returns></returns>
		internal OrderProduct Clone()
		{
			OrderProduct clone = new OrderProduct();

			clone.ImageQuixProductPk = this.ImageQuixProductPk;
			clone.Quantity = this.Quantity;
            clone.GreenScreenBackground = this.GreenScreenBackground;
            
			foreach (OrderProductOption sourceOption in this.OrderProductOptionList)
			{
                
                //ImageQuixOptionGroup optionGroup =
                //                this.OrderPackage.ProductPackage.FlowCatalog.GetImageQuixOptionGroupByKey(sourceOption.ImageQuixOptionGroupPk);



                OrderProductOption rootOption = sourceOption.Clone();
                //rootOption.OptionGroupLabel = optionGroup.Label;
                //rootOption.OptionGroupResourceURL = optionGroup.ResourceURL;

				clone.OrderProductOptions.Add(rootOption);
			}

			foreach(OrderProductNode sourceNode in this.OrderProductNodeList)
			{
				OrderProductNode rootNode = sourceNode.Clone();

				clone.OrderProductNodes.Add(rootNode);
			}

			return clone;
		}

		#endregion


        public void UpdateProductPackageComposition(string p)
        {
            this.OrderProductResourceURL = p;
        }
    }
}
