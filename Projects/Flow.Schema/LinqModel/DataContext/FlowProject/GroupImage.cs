﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.Linq;
using System.Data.SqlServerCe;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Media.Imaging;

using Flow.Lib;
using Flow.Lib.Database.SqlCe;
using Flow.Lib.Helpers;
using Flow.Schema.Properties;
using Flow.Schema.Utility;
using System.Drawing;
using System.Windows.Media;
using System.Windows;
using System.Windows.Threading;
using Flow.Lib.AsyncWorker;


namespace Flow.Schema.LinqModel.DataContext
{
	/// <summary>
	/// Represents an image (file) assigned to a Subject
	/// </summary>
	public partial class GroupImage
	{

		#region FIELDS AND PROPERTIES

        private FlowProjectDataContext _flowProjectDataContext = null;
        public FlowProjectDataContext FlowProjectDataContext
        {
            get { return _flowProjectDataContext; }
            set { _flowProjectDataContext = value; }
        }

		/// <summary>
		/// A class-wrapper representing the graphical image 
		/// </summary>
		FlowImage _flowImage = null;
		public FlowImage FlowImage
		{
			get
			{
                if (_flowImage == null)
                {
                    _flowImage = new FlowImage(this.ImagePath);
                }
				return _flowImage;
			}
			set { _flowImage = value; }
            
		}

		public bool ImageFileExists { get {
            if (!this.FlowImage.ImageFileExists)
            {
                if (File.Exists(this.ImagePath))
                    this.FlowImage.ImageFileExists = true;
            }
           return this.FlowImage.ImageFileExists; 
        
        } }

        public List<SubjectImage> GetSubjectImageList
        {
            get
            {
                return this.FlowProjectDataContext.SubjectImages.Where(si => si.GroupImageGuid == this.GroupImageGuid).ToList();
            }

        }

		/// <summary>
		/// The full filepath of the image file
		/// </summary>
		private string _imagePath = null;
		public string ImagePath
		{
			get
			{
                if (_imagePath == null && this.FlowProjectDataContext != null)
                {
                     _imagePath = Path.Combine(this.FlowProjectDataContext.FlowMasterDataContext.PreferenceManager.Application.GetFlowProjectImageDirPath((Guid)this.FlowProjectGuid, this.FlowProjectDataContext.FlowMasterDataContext.FlowProjectList.CurrentItem.IsNetworkProject == true), this.ImageFileName);
                     
                }
				return _imagePath;
			}
            set { _imagePath = value; }
		}

		

		
        private BitmapImage _updatedThumbnail = null;
        public BitmapImage Thumbnail
        {
            get
            {
                if (this.FlowProjectDataContext.FlowMasterDataContext.PreferenceManager.Application.ProcessImagesInBackground)
                {
                    if (_updatedThumbnail == null)
                    {
                        FlowBackgroundWorker worker = FlowBackgroundWorkerManager.RunWorker(GetThumbnail, GetThumbnail_Completed);

                    }

                    return _updatedThumbnail;
                }
                else
                {
                    BitmapImage tmpImage;

                    if (this.FlowProjectDataContext.FlowMasterDataContext.ShowOriginalFile)
                        tmpImage = this.FlowImage.Thumbnail;
                    else
                    {
                        this.FlowImage.CropL = this.CropL != null ? (double)this.CropL : 0;
                        this.FlowImage.CropT = this.CropT != null ? (double)this.CropT : 0;
                        this.FlowImage.CropW = this.CropW != null ? (double)this.CropW : 0;
                        this.FlowImage.CropH = this.CropH != null ? (double)this.CropH : 0;
                        tmpImage = this.FlowImage.ThumbnailCropped;
                    }

                    this.Orientation = this.FlowImage.Orientation;
                    return tmpImage;
                }

            }
        }

        public void GetThumbnail(object sender, DoWorkEventArgs e)
        {
                 
                BitmapImage tmpImage = new BitmapImage();
               
                if (this.FlowProjectDataContext.FlowMasterDataContext.ShowOriginalFile)
                    tmpImage = this.FlowImage.Thumbnail;
                else
                {
                    this.FlowImage.CropL = this.CropL != null ? (double)this.CropL : 0;
                    this.FlowImage.CropT = this.CropT != null ? (double)this.CropT : 0;
                    this.FlowImage.CropW = this.CropW != null ? (double)this.CropW : 0;
                    this.FlowImage.CropH = this.CropH != null ? (double)this.CropH : 0;
                    tmpImage = this.FlowImage.ThumbnailCropped;
                }

                this.Orientation = this.FlowImage.Orientation;
                //this.Dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                //{
                    _updatedThumbnail = tmpImage;
                    
                //}));
        }
        void GetThumbnail_Completed(object sender, RunWorkerCompletedEventArgs e)
        {
            SendPropertyChanged("Thumbnail");
        }







        private BitmapImage _updatedIntermediateImage = null;
        public BitmapImage IntermediateImage
        {
            get
            {
                if (this.FlowProjectDataContext != null && this.FlowProjectDataContext.FlowMasterDataContext.PreferenceManager.Application.ProcessImagesInBackground)
                {
                    if (_updatedIntermediateImage == null)
                    {
                        FlowBackgroundWorker worker = FlowBackgroundWorkerManager.RunWorker(GetIntermediateImage, GetIntermediateImage_Completed);
                    }

                    return _updatedIntermediateImage;
                }
                else
                {
                    BitmapImage tmpImage;
                    if (this.FlowProjectDataContext != null && this.FlowProjectDataContext.FlowMasterDataContext.ShowOriginalFile)
                        tmpImage = this.FlowImage.IntermediateImage;
                    else
                    {
                        this.FlowImage.CropL = this.CropL != null ? (double)this.CropL : 0;
                        this.FlowImage.CropT = this.CropT != null ? (double)this.CropT : 0;
                        this.FlowImage.CropW = this.CropW != null ? (double)this.CropW : 0;
                        this.FlowImage.CropH = this.CropH != null ? (double)this.CropH : 0;
                        tmpImage = this.FlowImage.IntermediateImageCropped;
                    }

                    this.Orientation = this.FlowImage.Orientation;
                    return tmpImage;
                }
            }
        }

        public void GetIntermediateImage(object sender, DoWorkEventArgs e)
        {

            BitmapImage tmpImage = new BitmapImage();

            if (this.FlowProjectDataContext.FlowMasterDataContext.ShowOriginalFile)
                tmpImage = this.FlowImage.IntermediateImage;
            else
            {
                this.FlowImage.CropL = this.CropL != null ? (double)this.CropL : 0;
                this.FlowImage.CropT = this.CropT != null ? (double)this.CropT : 0;
                this.FlowImage.CropW = this.CropW != null ? (double)this.CropW : 0;
                this.FlowImage.CropH = this.CropH != null ? (double)this.CropH : 0;
                tmpImage = this.FlowImage.IntermediateImageCropped;
            }

            this.Orientation = this.FlowImage.Orientation;
            //this.Dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
            //{
            _updatedIntermediateImage = tmpImage;

            //}));
        }
        void GetIntermediateImage_Completed(object sender, RunWorkerCompletedEventArgs e)
        {
            SendPropertyChanged("IntermediateImage");
        }

        public BitmapImage GetProcessingImage()
        {
            BitmapImage tmpImage = this.FlowImage.GetProcessingImage();
            this.Orientation = this.FlowImage.Orientation;
            return tmpImage;
        }



        private BitmapImage _updatedProcessingImage = null;
        public BitmapImage ProcessingImage
        {
            get
            {
                if (this.FlowProjectDataContext.FlowMasterDataContext.PreferenceManager.Application.ProcessImagesInBackground)
                {
                    if (_updatedProcessingImage == null)
                    {
                        FlowBackgroundWorker worker = FlowBackgroundWorkerManager.RunWorker(GetProcessingImage, GetProcessingImage_Completed);
                    }

                    return _updatedProcessingImage;

                }
                else
                {
                    BitmapImage tmpImage = this.FlowImage.ProcessingImage;
                    this.Orientation = this.FlowImage.Orientation;
                    return tmpImage;
                }

            }
        }

        public void GetProcessingImage(object sender, DoWorkEventArgs e)
        {

            BitmapImage tmpImage = new BitmapImage();


            tmpImage = this.FlowImage.ProcessingImage;


            this.Orientation = this.FlowImage.Orientation;

            _updatedProcessingImage = tmpImage;


        }
        void GetProcessingImage_Completed(object sender, RunWorkerCompletedEventArgs e)
        {
            SendPropertyChanged("ProcessingImage");
        }



		#endregion

        public void AutoCrop()
        {
            //if (auto8x10)
            {

                double thumbWidth = this.GetProcessingImage().Width;
                double thumbHeight = this.GetProcessingImage().Height;
                if (thumbHeight > thumbWidth)
                {//8x10

                    //Image newImage = Image.FromFile(image.ImagePathFullRes);
                    //double sw = newImage.HorizontalResolution;
                    //double sh = newImage.VerticalResolution;
                    //newImage.Dispose();
                    //thumbHeight = sh > sw ? sh : sw;
                    //thumbWidth = sh > sw ? sw : sh;

                    double newH = thumbWidth / .8;

                    this.CropW = 1;
                    this.CropH = newH / thumbHeight;
                    this.CropL = 0;
                    this.CropT = (1 - this.CropH) / 2;
                    this.FlowProjectDataContext.SubmitChanges();
                }
                else if (thumbHeight < thumbWidth)
                {//10x8

                    //Image newImage = Image.FromFile(image.ImagePathFullRes);
                    //double sw = newImage.HorizontalResolution;
                    //double sh = newImage.VerticalResolution;
                    //newImage.Dispose();
                    //thumbHeight = sh < sw ? sh : sw;
                    //thumbWidth = sh < sw ? sw : sh;


                    double newW = thumbHeight / .8;

                    this.CropW = newW / thumbWidth;
                    this.CropH = 1;
                    this.CropL = (1 - this.CropW) / 2;
                    this.CropT = 0;
                    this.FlowProjectDataContext.SubmitChanges();
                }
            }

        }

        public GroupImage(FlowProjectDataContext flowProjectDataContext)
		{
            InitializeNew(flowProjectDataContext);
            this.PropertyChanged +=new PropertyChangedEventHandler(GroupImage_PropertyChanged);
		}

        public void Initialize(FlowProjectDataContext flowProjectDataContext)
        {
            _flowProjectDataContext = flowProjectDataContext;
            this.PropertyChanged += new PropertyChangedEventHandler(GroupImage_PropertyChanged);
        }

        private void GroupImage_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "GroupImageDesc")
            {
                FileInfo origFileInfo = new FileInfo(this.ImagePath);
                string newFileName = ConvertToFileName(this.GroupImageDesc) + origFileInfo.Extension;
                string newPathToFile = Path.Combine(origFileInfo.DirectoryName, newFileName);
                int x=1;
                while (File.Exists(newPathToFile))
                {
                    newFileName = ConvertToFileName(this.GroupImageDesc) + (x++).ToString("000")  + origFileInfo.Extension;
                    newPathToFile = Path.Combine(origFileInfo.DirectoryName, newFileName);
                    if (x > 300)
                        break;
                }

                File.Move(this.ImagePath, newPathToFile);
                if (File.Exists(newPathToFile))
                {
                    this.ImageFileName = newFileName;
                    this.ImagePath = newPathToFile;
                }


                foreach (SubjectImage si in this.GetSubjectImageList)
                {
                    si.ImagePath = this.ImagePath;
                    si.ImageFileName = this.ImageFileName;
                    si.RefreshImage();
                    si.Subject.FlagForMerge = true;
                }
                this.FlowProjectDataContext.SubmitChanges();
            }
        }

        private string ConvertToFileName(string startingName)
        {
            string returnName = startingName.Replace(" ", "_");
            returnName = returnName.Replace("-", "_");
            returnName = Regex.Replace(returnName, "[^A-Za-z0-9_]", "");
            if (returnName.Length == 0) returnName = "GroupPhoto";
            return returnName.Left(26);
            

        }
      
        internal GroupImage InitializeNew(FlowProjectDataContext flowProjectDataContext)
        {
            _flowProjectDataContext = flowProjectDataContext;

            this.DateAssigned = DateTime.Now;
            this.GroupImageGuid = Guid.NewGuid();
            return this;
        }

		partial void OnCreated()
		{
			//if(this.SubjectImageGuid == Guid.Empty)
			//    this.SubjectImageGuid = Guid.NewGuid();

//			this.PrimaryImageAssign += new EventHandler<EventArgs<SubjectImage>>(this.Subject.SubjectImage_PrimaryImageAssign);
		}

		partial void OnLoaded()
		{
	
		}

		public void DisposeImages()
		{
            if (this.FlowImage != null)
                this.FlowImage.Clear();

            this.FlowImage = null;
            this.RefreshImage();
		}

        public void ClearImage()
        {
            _updatedThumbnail = null;
            _updatedIntermediateImage = null;
            _updatedProcessingImage = null;
            this.FlowImage.Clear();
            this.SendPropertyChanged("ProcessingImage");
            this.SendPropertyChanged("IntermediateImage");
            this.SendPropertyChanged("Thumbnail");
        }

        public void RefreshImage()
        {
            _updatedThumbnail = null;
            _updatedIntermediateImage = null;
            _updatedProcessingImage = null;
            this.FlowImage.Refresh();
            this.SendPropertyChanged("ProcessingImage");
            this.SendPropertyChanged("IntermediateImage");
            this.SendPropertyChanged("Thumbnail");
            this.SendPropertyChanged("IntermediateImageCropped");
            this.SendPropertyChanged("ThumbnailCropped");
        }

		internal void IncrementFileName()
		{
			string fileNameRoot = this.ImageFileName.GetFileNameRoot();

			Regex prefix = new Regex(@"^\d{2}_");

			int ordinal = 1;

			if (prefix.IsMatch(fileNameRoot))
			{
				ordinal = Int32.Parse(fileNameRoot.Left(2));
				ordinal++;
			}

			this.ImageFileName = ordinal.ZeroPad(2) + "_" + this.ImageFileName;
		}

		public void SubjectImageType_ExclusiveImageTypeAssign(object sender, EventHandler<EventArgs<SubjectImageType>> e)
		{
//			SubjectImageType assigned

		}


		#region MERGE MEMBERS

        internal GroupImage Clone()
		{
            GroupImage clone = new GroupImage();

            clone.GroupImageGuid = this.GroupImageGuid;
			clone.FlowProjectGuid = this.FlowProjectGuid;
			clone.ImageFileName = this.ImageFileName;
			clone.ImageOriginalFileName = this.ImageOriginalFileName;
			clone.CropT = this.CropT;
			clone.CropL = this.CropL;
			clone.CropW = this.CropW;
			clone.CropH = this.CropH;
			clone.Orientation = this.Orientation;
			clone.DateAssigned = this.DateAssigned;
            clone.GroupImageDesc = this.GroupImageDesc;
            clone.SelectedFlag = this.SelectedFlag;
            clone.AssignedTo = this.AssignedTo;
			return clone;
		}

//		internal Guid SubjectImageGuid { get; private set; }

		#endregion




        public void UpdateGroupImageCrop(double T, double W, double H, double L)
        {
            this.CropT = T;
            this.CropW = W;
            this.CropH = H;
            this.CropL = L;

            //bool backgrounRefresh = true;
            foreach (SubjectImage si in this.GetSubjectImageList)
            {
                si.CropH = H;
                si.CropL = L;
                si.CropT = T;
                si.CropW = W;
                si.ClearImage();
                si.Subject.FlagForMerge = true;
            }
            this.FlowProjectDataContext.SubmitChanges();
        }
    }
}
