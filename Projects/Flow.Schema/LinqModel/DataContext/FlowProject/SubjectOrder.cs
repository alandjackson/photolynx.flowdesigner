﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;

using System.Xml.Linq;

using NLog;

namespace Flow.Schema.LinqModel.DataContext
{
	partial class SubjectOrder
	{
        private static Logger logger = LogManager.GetCurrentClassLogger();

		internal FlowCatalog FlowCatalog { get; set; }


		/// <summary>
		/// 
		/// </summary>
        /// 
        //this only applies to the flow orders, not the IQ online gallery orders
		private FlowObservableCollection<OrderPackage> _orderPackageList;
		public FlowObservableCollection<OrderPackage> OrderPackageList
		{
			get
			{
				if (_orderPackageList == null)
				{
                    //_orderPackageList = new FlowObservableCollection<OrderPackage>(this.OrderPackages.Where(op => op.IsOnlineOrder != true));
                    _orderPackageList = new FlowObservableCollection<OrderPackage>(this.OrderPackages);
					if (this.FlowCatalog != null)
					{
                        foreach (OrderPackage item in this._orderPackageList)
						{
							ProductPackage productPackage = this.FlowCatalog.ProductPackageList
								.FirstOrDefault(i => i.ProductPackageGuid == item.ProductPackageGuid);

							if (productPackage == null)
							{
								productPackage = this.FlowCatalog.ProductPackageList
									.FirstOrDefault(i => i.ProductPackageDesc == item.ProductPackageDesc);

								if (productPackage != null)
								{
									item.ProductPackageGuid = productPackage.ProductPackageGuid;
									this.Subject.FlowProjectDataContext.SubmitChanges();
								}
							}

							item.ProductPackage = productPackage;

							item.TaxFactor = this.FlowCatalog.TaxRate;
						}
					}

					_orderPackageList.CollectionChanged += new NotifyCollectionChangedEventHandler(OrderPackageList_CollectionChanged);
				}

				return _orderPackageList;
			}

			internal set
			{
				_orderPackageList = value;

				this.SendPropertyChanged("OrderPackageList");
			}
		}



        private FlowObservableCollection<OrderPackage> _newOrderPackageList;
        public FlowObservableCollection<OrderPackage> NewOrderPackageList
        {
            get
            {
                if (_newOrderPackageList == null)
                {
                    _newOrderPackageList = new FlowObservableCollection<OrderPackage>();

                    if (this.FlowCatalog != null)
                    {
                        foreach (OrderPackage item in this.OrderPackages.Where(op => op.IsOnlineOrder != true))
                        {
                            if (item.OrderHistories == null || item.OrderHistories.Count < 1)
                            {
                                ProductPackage productPackage = this.FlowCatalog.ProductPackageList
                                    .FirstOrDefault(i => i.ProductPackageGuid == item.ProductPackageGuid);

                                if (productPackage == null)
                                {
                                    productPackage = this.FlowCatalog.ProductPackageList
                                        .FirstOrDefault(i => i.ProductPackageDesc == item.ProductPackageDesc);

                                    if (productPackage != null)
                                    {
                                        item.ProductPackageGuid = productPackage.ProductPackageGuid;
                                        this.Subject.FlowProjectDataContext.SubmitChanges();
                                    }
                                }

                                item.ProductPackage = productPackage;

                                item.TaxFactor = this.FlowCatalog.TaxRate;
                                _newOrderPackageList.Add(item);
                            }
                        }
                    }

                    _newOrderPackageList.CollectionChanged += new NotifyCollectionChangedEventHandler(NewOrderPackageList_CollectionChanged);
                }

                return _newOrderPackageList;
            }

            internal set
            {
                _newOrderPackageList = value;

                this.SendPropertyChanged("NewOrderPackageList");
            }
        }


        private FlowObservableCollection<OrderSubjectOrderOption> _orderSubjectOrderOptionList = null;
        public FlowObservableCollection<OrderSubjectOrderOption> OrderSubjectOrderOptionList
        {
            get
            {
                if (_orderSubjectOrderOptionList == null)
                {
                    //_imageList = new FlowObservableCollection<SubjectImage>(this.SubjectImages);
                    _orderSubjectOrderOptionList = new FlowObservableCollection<OrderSubjectOrderOption>();
                    foreach (OrderSubjectOrderOption oio in this.OrderSubjectOrderOptions.OrderBy(si => si.SubjectOrderID))
                    {

                        if (oio.FlowCatalogOption == null)
                        {
                            if (this.Subject.FlowProjectDataContext.FlowProject.FlowCatalog.FlowCatalogSubjectOrderOptionList.Any(fco => fco.FlowCatalogOptionGuid == oio.FlowCatalogOptionGuid))
                            {
                                oio.FlowCatalogOption = this.Subject.FlowProjectDataContext.FlowProject.FlowCatalog.FlowCatalogSubjectOrderOptionList.First(fco => fco.FlowCatalogOptionGuid == oio.FlowCatalogOptionGuid);
                            }
                        }
                        //if(oio.FlowCatalogOption.ImageQuixCatalogOption.ImageQuixCatalogOptionGroup.Label == "Image Options")\

                        if (oio.FlowCatalogOption != null)
                        {
                            _orderSubjectOrderOptionList.Add(oio);
                        }
                    }
                }

                return _orderSubjectOrderOptionList;
            }


        }

		void OrderPackageList_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
		{
		}

        void NewOrderPackageList_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
        }

		/// <summary>
		/// 
		/// </summary>
		private decimal _orderSubTotal = 0;
		public decimal OrderSubTotal
		{
			get { return _orderSubTotal; }
			set	
			{
				_orderSubTotal = value;
				this.SendPropertyChanged("OrderSubTotal");
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public decimal OrderGrandTotal
		{
			get
			{
				decimal orderGrandTotal = this.OrderPackageList.Sum(op => op.TaxedExpandedPrice * op.Qty);
				this.OrderSubTotal = this.OrderPackageList.Sum(op => (op.ExpandedPrice * op.Qty));

                if (OrderSubjectOrderOptions.Count > 0)
                {
                    orderGrandTotal += OrderSubjectOrderOptions.Sum(oio => oio.TaxedExpandedPrice);
                    this.OrderSubTotal += OrderSubjectOrderOptions.Sum(oio => oio.ExpandedPrice);
                }

                if (this.Subject != null)
                {
                    foreach (SubjectImage si in this.Subject.SubjectImages)
                    {
                        if (si.OrderImageOptions.Count > 0)
                        {
                            orderGrandTotal += si.OrderImageOptions.Sum(oio => oio.TaxedExpandedPrice);
                            this.OrderSubTotal += si.OrderImageOptions.Sum(oio => oio.ExpandedPrice);
                        }
                    }
                }
				this.TaxTotal = orderGrandTotal - this.OrderSubTotal;

				return orderGrandTotal;
			}
		}
        public void ReCalculateGrandTotal()
        {
            this.SendPropertyChanged("OrderGrandTotal");
            this.SendPropertyChanged("BalanceDue");
        }
		/// <summary>
		/// 
		/// </summary>
		private decimal _taxTotal = 0;
		public decimal TaxTotal
		{
			get { return _taxTotal; }
			set
			{
				_taxTotal = value;
				this.SendPropertyChanged("TaxTotal");
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public decimal BalanceDue
		{
			get { return this.OrderGrandTotal - ((this.PaymentAmount.HasValue) ? this.PaymentAmount.Value : 0); }
		}

		/// <summary>
		/// 
		/// </summary>
		public bool PaidByCreditCard
		{
			get { return this.PaymentMethodID == 3; }
		}

		/// <summary>
		/// 
		/// </summary>
		public bool PaidByCheck
		{
			get { return this.PaymentMethodID == 2; }
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="flowCatalog"></param>
		/// <param name="subjectID"></param>
		public SubjectOrder(
			FlowCatalog flowCatalog,
			int subjectID
		) : this()
		{
			this.FlowCatalog = flowCatalog;
			this.SubjectID = subjectID;
			this.ProductOrderDate = DateTime.Now;
			this.PaymentAmount = 0;
	
			if(this.OrderPackageList == null) { };
		}

		/// <summary>
		/// 
		/// </summary>
		partial void OnPaymentAmountChanged()
		{
			this.SendPropertyChanged("BalanceDue");
		}

        public bool Directshipment
        {
            get
            {
                if (Directship == null)
                    return false;
                return (bool)Directship;
            }
            set
            {
                Directship = value;
            }
        }
		/// <summary>
		/// Informs listeners to refresh the binding on PaidByCheck and PaidByCreditCard when the
		/// PaymentMethodID property has changed
		/// </summary>
		partial void OnPaymentMethodIDChanged()
		{
			this.SendPropertyChanged("PaidByCheck");
			this.SendPropertyChanged("PaidByCreditCard");
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="productPackage"></param>
        internal void AddOrderPackage(ProductPackage productPackage)
        {
            AddOrderPackage(productPackage, "", null);
        }
        internal OrderPackage AddOrderPackage(ProductPackage productPackage, string PackageImageType, SubjectImage subImage)
		{
            OrderPackage orderPackage = new OrderPackage(productPackage, this, PackageImageType, subImage, null);
            try
            {
                if (orderPackage.OrderProducts[0].OrderProductNodes[0].SubjectImage.GreenScreenBackground != null)
                {
                    foreach (OrderProduct op in orderPackage.OrderProducts)
                    {
                        op.GreenScreenBackground = orderPackage.OrderProducts[0].OrderProductNodes[0].SubjectImage.GreenScreenBackground;
                    }
                }
            }
            catch (Exception e)
            {
                // Ignore it, but log it
                logger.WarnException("Ignoring a caught exception while adding order package: ", e);
            }

            if (this.OrderPackages.Count > 0)
            {
                orderPackage.ShipmentType = OrderPackages[0].ShipmentType;
                orderPackage.ShippingFirstName = OrderPackages[0].ShippingFirstName;
                orderPackage.ShippingAddress = OrderPackages[0].ShippingAddress;
                orderPackage.ShippingCity = OrderPackages[0].ShippingCity;
                orderPackage.ShippingState = OrderPackages[0].ShippingState;
                orderPackage.ShippingZip = OrderPackages[0].ShippingZip;
            }
            orderPackage.AddDate = DateTime.Now;
			//this.OrderPackageList.Add(orderPackage);
            this.OrderPackages.Add(orderPackage);
            _orderPackageList = null;

			// Check for assignment of the FlowCatalog if not pre-assigned, and assign
			if (this.FlowCatalog == null)
				this.FlowCatalog = productPackage.FlowCatalog;

			orderPackage.TaxFactor = this.FlowCatalog.TaxRate;

			this.SendPropertyChanged("OrderGrandTotal");
            this.SendPropertyChanged("BalanceDue");
            this.SendPropertyChanged("OrderPackages");
            return orderPackage;
		}

        public void RefreshOrderDetails()
        {

            this.SendPropertyChanged("OrderGrandTotal");
            this.SendPropertyChanged("BalanceDue");
            this.SendPropertyChanged("OrderPackages");
            _orderPackageList = null;
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="orderPackage"></param>
		internal void DuplicateOrderPackage(OrderPackage orderPackage)
		{
			orderPackage.Duplicate();
			this.SendPropertyChanged("OrderGrandTotal");
            this.SendPropertyChanged("BalanceDue");
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="orderPackage"></param>
		/// <param name="flowProjectDataContext"></param>
		internal void RemoveOrderPackage(OrderPackage orderPackage, FlowProjectDataContext flowProjectDataContext)
		{
            flowProjectDataContext.SubmitChanges();
            orderPackage.RemoveOrderHistory(flowProjectDataContext);
            flowProjectDataContext.SubmitChanges();
			orderPackage.RemovePackageProducts(flowProjectDataContext);
            flowProjectDataContext.OrderPackages.DeleteOnSubmit(orderPackage);
            if (_orderPackageList != null && _orderPackageList.Contains(orderPackage))
                OrderPackageList = null;
            //object obj = this.OrderPackageList;

            if (!flowProjectDataContext.DeletedOrderPackages.Any(dop => dop.OrderPackageID == orderPackage.OrderPackageID && dop.AddDate == orderPackage.AddDate))
            {
                DeletedOrderPackage dop = new DeletedOrderPackage();
                dop.DeleteDate = DateTime.Now;
                dop.OrderPackageID = orderPackage.OrderPackageID;
                dop.AddDate = orderPackage.AddDate;
                flowProjectDataContext.DeletedOrderPackages.InsertOnSubmit(dop);
            }

            
            
            flowProjectDataContext.SubmitChanges();
			this.SendPropertyChanged("OrderGrandTotal");
            this.SendPropertyChanged("BalanceDue");
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="flowProjectDataContext"></param>
        //internal void ClearOrderPackages(FlowProjectDataContext flowProjectDataContext)
        //{
        //    foreach (OrderPackage orderPackage in this.OrderPackageList.ToList())
        //    {
        //        this.RemoveOrderPackage(orderPackage, flowProjectDataContext);
        //    }
        //}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="subjectImage"></param>
		internal void ReplaceUnassignedImage(SubjectImage deletionImage, SubjectImage replacementImage)
		{
			foreach(OrderPackage orderPackage in this.OrderPackageList)
				orderPackage.ReplaceUnassignedImage(deletionImage, replacementImage);
		}

        
            /// <summary>
		/// Creates an XElement representing to the order information for the parent Subject, for a specific product
        /// (used by the Layout Designer)
		/// </summary>
        internal List<XElement> GetOrderXml(int productPrimaryKey, string defaultImageName, ImageQuixCatalog imageQuixCatalog)
		{
			
			List<XElement> orderElement = new List<XElement>();

            ImageQuixProduct orderProduct = imageQuixCatalog.ProductList.First(s => s.PrimaryKey == productPrimaryKey);
            
		    // Create product element
		    XElement productElement = new XElement("Product");

		    // Add the ImageQuix product pk value
		    productElement.Add(new XAttribute("productFK", orderProduct.PrimaryKey));
            productElement.Add(new XAttribute("product-ref", orderProduct.ResourceURL));
            //if (!orderProduct.Quantity.HasValue)
            //    this.FlowCatalog.VerifyOrderProductQuantity(orderPackage.ProductPackageGuid, orderProduct);
			
		    productElement.Add(new XAttribute("quantity", 1));

		    // Add the ImageQuix node elements (contains the image data)
		    foreach (ImageQuixProductNode orderProductNode in orderProduct.ProductNodeList)
		    {
                if (orderProductNode.NodeType.ToString() != "hidden" && orderProductNode.NodeType.ToString() != "none")
                    productElement.Add(GetNodeXElement(orderProductNode, defaultImageName));
		    }

		    // Add the ImageQuix option elements
		    foreach (ImageQuixOptionGroup orderProductOption in
			    orderProduct.ImageQuixOptionGroups.Where(o => o.PrimaryKey.HasValue)
		    )
		    {
			    XElement optionElement = new XElement("Option");

			    // Assign the option pk attribute
                optionElement.Add(new XAttribute("optionFK", orderProductOption.PrimaryKey));
                optionElement.Add(new XAttribute("option-group-ref", orderProductOption.ResourceURL));
                optionElement.Add(new XAttribute("option-ref", orderProductOption.ResourceURL + "/option/" + orderProductOption.ImageQuixOptions.First(o => o.IsDefault == true).PrimaryKey.ToString()));

			    productElement.Add(optionElement);
		    }

		    orderElement.Add(productElement);
			return orderElement;
		}

        private XElement GetNodeXElement(ImageQuixProductNode orderProductNode, string defaultImageName)
        {
            XElement nodeElement = new XElement("Node");

            AddNewXAttribute(nodeElement, "nodeFK", orderProductNode.PrimaryKey.ToString().PadLeft(8,'0'));
            AddNewXAttribute(nodeElement, "node-ref", orderProductNode.ResourceURL);

            switch (orderProductNode.NodeType)
            {
                // IMAGE
                case ImageQuixProductNodeType.Image:
                    AddNewXAttribute(nodeElement, "imageFileName", defaultImageName);
                    AddNewXAttribute(nodeElement, "type", orderProductNode.NodeType.ToString().ToLower());
                    AddNewXAttribute(nodeElement, "title", "");
                    AddNewXAttribute(nodeElement, "imageSourcePath", "");
                    AddNewXAttribute(nodeElement, "cropX", 0);
                    AddNewXAttribute(nodeElement, "cropY", 0);
                    AddNewXAttribute(nodeElement, "cropWidth", 0);
                    AddNewXAttribute(nodeElement, "cropHeight", 0);
                    AddNewXAttribute(nodeElement, "orientation", 0);
                    break;

                // TEXT
                case ImageQuixProductNodeType.Text:
                    AddNewXAttribute(nodeElement, "imageFileName", "");
                    AddNewXAttribute(nodeElement, "type", orderProductNode.NodeType.ToString().ToLower());
                    AddNewXAttribute(nodeElement, "title", "");
                    AddNewXAttribute(nodeElement, "imageSourcePath", "");
                    AddNewXAttribute(nodeElement, "cropX", 0);
                    AddNewXAttribute(nodeElement, "cropY", 0);
                    AddNewXAttribute(nodeElement, "cropWidth", 0);
                    AddNewXAttribute(nodeElement, "cropHeight", 0);
                    AddNewXAttribute(nodeElement, "orientation", 0);
                    break;

                // NONE
                default:
                    AddNewXAttribute(nodeElement, "imageFileName", "");
                    AddNewXAttribute(nodeElement, "type", orderProductNode.NodeType.ToString().ToLower());
                    AddNewXAttribute(nodeElement, "title", "");
                    AddNewXAttribute(nodeElement, "imageSourcePath", "");
                    AddNewXAttribute(nodeElement, "cropX", "");
                    AddNewXAttribute(nodeElement, "cropY", "");
                    AddNewXAttribute(nodeElement, "cropWidth", "");
                    AddNewXAttribute(nodeElement, "cropHeight", "");
                    AddNewXAttribute(nodeElement, "orientation", "");
                    break;
            }


            return nodeElement;
        }

        private void AddNewXAttribute(XElement element, XName xName, object value)
        {
            element.Add(new XAttribute(xName, value ?? ""));
        }

       
		/// <summary>
		/// Creates an XElement representing to the order information for the parent Subject
		/// </summary>
        internal List<XElement> GetOrderXml(List<OrderPackage> applicableOrderPackages, bool renderGreenScreen, bool useOriginalFileNames)
		{
			//// Create order element
			//XElement orderElement = new XElement("Order");

			//// Create customerinfo element, adding order guid
			//custInfoElement.Attribute("orderIDExternal").Value = this.SubjectOrderGuid.ToString();
			//orderElement.Add(custInfoElement);

			List<XElement> orderElement = new List<XElement>();

			// Iterate through each ordered package
			foreach (OrderPackage orderPackage in this.OrderPackageList)
			{
                if(applicableOrderPackages.Contains(orderPackage))
                {
				    // Iterate through each product in the package
				    foreach (OrderProduct orderProduct in orderPackage.OrderProductList)
				    {
                        if (orderProduct.OrderProductResourceURL == null)
                        {
                            IEnumerable<ImageQuixProduct> iqprods =  this.FlowCatalog.ImageQuixCatalog.ImageQuixProducts.Where(iqp => iqp.PrimaryKey == orderProduct.ImageQuixProductPk);
                            if (iqprods.Count() > 0)
                                orderProduct.UpdateResourceURL(iqprods.First());
                        }
                        if (orderProduct.OrderProductResourceURL.Contains("not found"))
                            continue;//if there is no resourceURL, we can't order this product.

					    // Create product element
					    XElement productElement = new XElement("Product");

					    // Add the ImageQuix product pk value
					    productElement.Add(new XAttribute("productFK", orderProduct.ImageQuixProductPkPadded));
                        productElement.Add(new XAttribute("product-ref", orderProduct.OrderProductResourceURL));
					    if (!orderProduct.Quantity.HasValue)
						    this.FlowCatalog.VerifyOrderProductQuantity(orderPackage.ProductPackageGuid, orderProduct);
    					
					    productElement.Add(new XAttribute("quantity", orderProduct.Quantity.Value));

					    // Add the ImageQuix node elements (contains the image data)
					    foreach (OrderProductNode orderProductNode in orderProduct.OrderProductNodeList)
					    {
                            if(orderProductNode.ImageQuixProductNodeTypeText != "hidden" && orderProductNode.ImageQuixProductNodeTypeText != "none")
                                productElement.Add(orderProductNode.GetNodeXElement(renderGreenScreen, useOriginalFileNames));
					    }

					    // Add the ImageQuix option elements
					    foreach (OrderProductOption orderProductOption in
						    orderProduct.OrderProductOptionList.Where(o => o.ImageQuixOptionPk.HasValue)
					    )
					    {
						    XElement optionElement = new XElement("Option");

						    // Assign the option pk attribute
						    optionElement.Add(new XAttribute("optionFK", orderProductOption.ImageQuixOptionPkPadded));
                            optionElement.Add(new XAttribute("option-group-ref", orderProductOption.OptionGroupResourceURL));
                            optionElement.Add(new XAttribute("option-ref", orderProductOption.OptionGroupResourceURL + "/option/" + orderProductOption.ImageQuixOptionPk));

						    productElement.Add(optionElement);
					    }

					    orderElement.Add(productElement);
				    }
                }
			}

			return orderElement;
		}

		#region MERGE MEMBERS

		private Guid _subjectGuid;
		internal Guid SubjectGuid
		{
			get { return _subjectGuid; }
			set
			{
				_subjectGuid = value;

				this.SubjectOrderGuid = Guid.NewGuid();

				foreach (OrderPackage orderPackage in this.OrderPackageList)
				{
					orderPackage.SubjectOrderGuid = this.SubjectOrderGuid;
				}
			}
		}

		internal Guid SubjectOrderGuid { get; private set; }

		/// <summary>
		/// Returns a clone of the source SubjectOrder object
		/// </summary>
		/// <returns></returns>
		internal SubjectOrder Clone()
		{
			SubjectOrder clone = new SubjectOrder();

			clone.PaymentMethodID = this.PaymentMethodID;
			clone.CreditCardTypeDesc = this.CreditCardTypeDesc;
			clone.CheckNo = this.CheckNo;
			clone.PaymentAmount = this.PaymentAmount;
			clone.ProductOrderDate = this.ProductOrderDate;
           
			return clone;
		}

		/// <summary>
		/// Imports the order data for a subject from a distributed project
		/// </summary>
		/// <param name="subject"></param>
		/// <param name="flowProjectDataContext"></param>
		internal void Merge(Subject subject, FlowCatalog flowCatalog, FlowProjectDataContext flowProjectDataContext)
		{
            logger.Info("Merging Order");
            if (subject.SubjectOrder != null && (subject.SubjectOrder.OrderPackages.Count > 0))
			{
				this.PaymentMethodID = subject.SubjectOrder.PaymentMethodID;
				this.CreditCardTypeDesc = subject.SubjectOrder.CreditCardTypeDesc;
				this.CheckNo = subject.SubjectOrder.CheckNo;
				this.PaymentAmount = subject.SubjectOrder.PaymentAmount;
				this.ProductOrderDate = subject.SubjectOrder.ProductOrderDate;
                
				foreach (OrderPackage sourcePackage in subject.SubjectOrder.OrderPackages)
				{
                    logger.Info("Merging Order - Source package = " + sourcePackage.ProductPackageDesc);
                    //if (this.OrderPackages != null && this.OrderPackages.Any(op => (op.IsOnlineOrder && op.IQOrderID == sourcePackage.IQOrderID)))
                    //{
                        
                    //}

                    //known issue: if an iq order was pulled on 2 different machines around the same time, they will be the same thing but have different "AddDate"
                    // so, they will be seen as unique and mereged in as unique, creating a duplicate order. Not sure how to prevent this right now

                    logger.Info("Merging Order - first lets see if this incoming package matches an existing one");
                    //first lets see if this incoming package matches an existing one
                    if (this.OrderPackages != null && this.OrderPackages.Any(op => op.AddDate == sourcePackage.AddDate))
                    {
                        logger.Info("Merging Order - it matches");
                        OrderPackage theRootPackage = this.OrderPackages.First(op => op.AddDate == sourcePackage.AddDate);
                        if ((theRootPackage.ModifyDate == null && sourcePackage.ModifyDate != null) ||
                           ((theRootPackage.ModifyDate != null && sourcePackage.ModifyDate != null) && (theRootPackage.ModifyDate < sourcePackage.ModifyDate)))
                        {
                            theRootPackage.IQOrderDate = sourcePackage.IQOrderDate;
                            theRootPackage.IQOrderID = sourcePackage.IQOrderID;
                            theRootPackage.WebOrderID = sourcePackage.WebOrderID;
                            theRootPackage.WebOrderDate = sourcePackage.WebOrderDate;
                            theRootPackage.WebOrderAuthorizationToken = sourcePackage.WebOrderAuthorizationToken;
                            theRootPackage.IsALaCarte = sourcePackage.IsALaCarte;
                            theRootPackage.IsOnlineOrder = sourcePackage.IsOnlineOrder;
                            theRootPackage.LabOrderDate = sourcePackage.LabOrderDate;
                            theRootPackage.LabOrderID = sourcePackage.LabOrderID;
                            theRootPackage.ModifyDate = sourcePackage.ModifyDate;
                            theRootPackage.ShipmentType = sourcePackage.ShipmentType;
                            theRootPackage.ShippingAddress = sourcePackage.ShippingAddress;
                            theRootPackage.ShippingCity = sourcePackage.ShippingCity;
                            theRootPackage.ShippingCountry = sourcePackage.ShippingCountry;
                            theRootPackage.ShippingFirstName = sourcePackage.ShippingFirstName;
                            theRootPackage.ShippingLastName = sourcePackage.ShippingLastName;
                            theRootPackage.ShippingState = sourcePackage.ShippingState;
                            theRootPackage.ShippingZip = sourcePackage.ShippingZip;
                            theRootPackage.Notes = sourcePackage.Notes;
                            theRootPackage.Units = sourcePackage.Units;
                            theRootPackage.SubjectImageID = sourcePackage.SubjectImageID;
                            theRootPackage.GreenScreenBackground = sourcePackage.GreenScreenBackground;
                            theRootPackage.Qty = sourcePackage.Qty;

                            flowProjectDataContext.SubmitChanges();
                            continue;
                        }
                    }

                    if(this.FlowCatalog != null)
                        this.FlowCatalog.FlowMasterDataContext = this.Subject.FlowProjectDataContext.FlowMasterDataContext;
                   
                    //no match was found so lets add this package 
                    logger.Info("Merging Order - no match was found, adding new");
					OrderPackage rootPackage = sourcePackage.Clone();
                    
                    ///////////////////////////////////////////////////
                    // Note: the following code is a temporary fix for diverging ProductPackage guids;
                    // this should be removed at a later Flow revision/version
                    logger.Info("Merging Order - diverging quid check");
                    if (flowCatalog != null && !flowCatalog.ProductPackages.Any(p => p.ProductPackageGuid == rootPackage.ProductPackageGuid))
                    {
                        logger.Info("Merging Order - diverging quid check - YUP");
                        ProductPackage productPackageByDesc =
                            flowCatalog.ProductPackages.FirstOrDefault(p => p.ProductPackageDesc == rootPackage.ProductPackageDesc);

                        if (productPackageByDesc != null)
                        {
                            rootPackage.ProductPackageGuid = productPackageByDesc.ProductPackageGuid;
                        }
                    }


                    /////////////////////////////////////////////////////
                    logger.Info("Merging Order - adding an OrderPackage");
                    if (this.OrderPackages != null && !this.OrderPackages.Any(op => op.AddDate == rootPackage.AddDate))
                    {
                        logger.Info("Merging Order - adding an OrderPackage - YUP");
                        
                        this.OrderPackages.Add(rootPackage);
                    }
                    else if(this.OrderPackages != null)
                    {
                        logger.Info("Merging Order - adding an OrderPackage - ELSE");
                        OrderPackage opackage = this.OrderPackages.First(op => op.AddDate == rootPackage.AddDate);
                        foreach (OrderProduct oprod in opackage.OrderProducts)
                        {
                            oprod.GreenScreenBackground = rootPackage.OrderProducts.First(o => o.OrderProductGuid == oprod.OrderProductGuid).GreenScreenBackground;
                        }
                    }
                    
                   
				}

				flowProjectDataContext.SubmitChanges();
			}
		}

		#endregion


        internal void AddCatalogOption(FlowCatalogOption flowCatalogOption)
        {
            if (this.OrderSubjectOrderOptions.Count(oio => oio.FlowCatalogOptionGuid == flowCatalogOption.FlowCatalogOptionGuid) == 0)
            {
                OrderSubjectOrderOption orderSubjectOption = new OrderSubjectOrderOption(flowCatalogOption, this);
                orderSubjectOption.TaxFactor = flowCatalogOption.FlowCatalog.TaxRate;
                orderSubjectOption.FlowCatalogOption = flowCatalogOption;
                orderSubjectOption.FlowCatalogOptionGuid = flowCatalogOption.FlowCatalogOptionGuid;
                orderSubjectOption.DateAdded = DateTime.Now;
                this.OrderSubjectOrderOptions.Add(orderSubjectOption);
                this.Subject.FlowProjectDataContext.OrderSubjectOrderOptions.InsertOnSubmit(orderSubjectOption);
                this._orderSubjectOrderOptionList = null;
                this.SendPropertyChanged("OrderSubjectOrderOptionList");
            }

        }

        internal void RemoveCatalogOption(OrderSubjectOrderOption orderSubjectOrderOption)
        {
            if (this.OrderSubjectOrderOptions.Contains(orderSubjectOrderOption))
            {
                this.Subject.FlowProjectDataContext.OrderSubjectOrderOptions.DeleteOnSubmit(orderSubjectOrderOption);
                this.Subject.FlowProjectDataContext.SubmitChanges();
                this._orderSubjectOrderOptionList = null;
                this.SendPropertyChanged("OrderSubjectOrderOptionList");
            }
        }

        public void UpdateOrderPackages()
        {
            SendPropertyChanged("OrderPackageList");
            //SendPropertyChanged("OrderPackages");
        }
    }
}
