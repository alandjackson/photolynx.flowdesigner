﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Flow.Lib.Helpers;

namespace Flow.Schema.LinqModel.DataContext
{
	partial class OrderImageOption
	{
		public FlowCatalogOption FlowCatalogOption { get; internal set; }
        
        
        //public string OrderHistoryText{
        //    get{
        //        string tmpText = "Order History:";
        //        if (this.OrderHistories.Count() > 0)
        //        {
        //            foreach (OrderHistory oh in this.OrderHistories)
        //            {
        //                tmpText = tmpText + "\n" + oh.OrderID + " - " + oh.OrderSubmittedDate.ToShortDateString();
        //            }
                    
        //        }
        //        else
        //        {
        //            tmpText = "NEW ORDER";
        //        }

        //        return tmpText;
        //    }
        //}

        //public FlowObservableCollection<OrderProduct> _orderProductList;
        //public FlowObservableCollection<OrderProduct> OrderProductList
        //{
        //    get
        //    {
        //        if (_orderProductList == null)
        //        {
        //            _orderProductList = new FlowObservableCollection<OrderProduct>(this.OrderProducts);

        //            if (this.ProductPackage != null)
        //            {
        //                foreach (OrderProduct orderProduct in _orderProductList)
        //                {
        //                    ProductPackageComposition composition =
        //                        this.ProductPackage.ProductPackageCompositionList
        //                            .FirstOrDefault(c => c.ImageQuixProduct.PrimaryKey == orderProduct.ImageQuixProductPk);

        //                    if (composition != null)
        //                    {
        //                        orderProduct.OrderProductLabel = composition.ImageQuixProduct.Label;
        //                        orderProduct.OrderProductResourceURL = composition.ImageQuixProduct.ResourceURL;
        //                    }
        //                    else
        //                    {
        //                        orderProduct.OrderProductLabel = "{ImageQuix Product not found}";
        //                        orderProduct.OrderProductResourceURL = "{ImageQuix Product RescourceURL not found}";
        //                    }
        //                }
        //            }
        //        }

        //        return _orderProductList;
        //    }
        //}

        public bool isOnlineOrder
        {
            get
            {
                if (this.WebOrderID != null && this.WebOrderID > 0) return true;
                else return false;
            }
        }


		private decimal _taxRate = 0;
		internal decimal TaxFactor
		{
			get {
                if (_taxRate == 0 && this.FlowCatalogOption != null)
                    _taxRate = 1 + (this.FlowCatalogOption.FlowCatalog.TaxRate / 100);
                return _taxRate; 
            }
			set { _taxRate = 1 + (value / 100); }
		}
	
		public decimal ExpandedPrice
		{
			get
			{
				if (this.FlowCatalogOption != null)
					return this.FlowCatalogOption.Price;

				return 0;
			}
		}

		public decimal TaxedExpandedPrice
		{
			get
			{
				if (this.FlowCatalogOption == null || !this.FlowCatalogOption.Taxed)
					return this.ExpandedPrice;

				return (this.FlowCatalogOption.Price * this.TaxFactor);
			}
		}

		public string ExpandedPriceDisplayText
		{
			get
			{
                if (this.FlowCatalogOption == null)
                    return "$0.0";

				return this.FlowCatalogOption.Price.CurrencyFormat("$");
			}
		}

        public string TaxedExpandedPriceDisplayText
        {
            get
            {
                if (this.TaxedExpandedPrice == null)
                    return "$0.0";

                return TaxedExpandedPrice.CurrencyFormat("$");
            }
        }

		public string TaxDisplayText
		{
			get { return "(x " + (this.TaxFactor - 1).ToString() + ") Tax"; }
		}

        public OrderImageOption(FlowCatalogOption flowCatalogOption, SubjectImage subjectImage)
			: this()
		{
            this.FlowCatalogOption = flowCatalogOption;
            this.SubjectImage = subjectImage;
            this.SubjectImageID = subjectImage.SubjectImageID;
            this.ResourceURL = flowCatalogOption.ImageQuixCatalogOption.ResourceURL;

            //this.FlowCatalogOption.ImageQuixCatalogOption.Label

            //this.ProductPackage = productPackage;
            //this.ProductPackageGuid = productPackage.ProductPackageGuid;
            //this.ProductPackageDesc = productPackage.ProductPackageDesc;

            //foreach (ProductPackageComposition composition in productPackage.ProductPackageCompositionList)
            //{
            //    ImageQuixProduct product = composition.ImageQuixProduct;

            //    OrderProduct orderProduct = new OrderProduct(product, subjectOrder);
            //    orderProduct.Quantity = composition.Quantity;

            //    this.OrderProductList.Add(orderProduct);

            //    foreach (ImageQuixOptionGroup optionGroup in product.OptionGroupList.Where(g => g.OptionList.HasItems))
            //    {
            //        orderProduct.OrderProductOptionList.Add(new OrderProductOption(optionGroup));
            //    }
            //}
		}

        //internal OrderPackage Duplicate()
        //{
        //    OrderPackage duplicateOrderPackage = new OrderPackage(this.ProductPackage, this.SubjectOrder);
        //    duplicateOrderPackage.AddDate = DateTime.Now;
        //    return duplicateOrderPackage;
        //}

        //internal void RemoveOrderHistory(FlowProjectDataContext flowProjectDataContext)
        //{
        //    flowProjectDataContext.OrderHistories.DeleteAllOnSubmit(this.OrderHistories);
        //    this.OrderProducts.Clear();
        //}

        //internal void RemovePackageProducts(FlowProjectDataContext flowProjectDataContext)
        //{
        //    foreach (OrderProduct orderProduct in this.OrderProductList)
        //    {
        //        orderProduct.RemoveChildren(flowProjectDataContext);
        //        flowProjectDataContext.OrderProducts.DeleteOnSubmit(orderProduct);
        //    }
        //    flowProjectDataContext.SubmitChanges();
        //    //flowProjectDataContext.OrderProducts.DeleteAllOnSubmit(this.OrderProducts);
        //    //this.OrderProducts.Clear();
        //}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="subjectImage"></param>
        //internal void ReplaceUnassignedImage(SubjectImage deletionImage, SubjectImage replacementImage)
        //{
        //    foreach(OrderProduct orderProduct in this.OrderProductList)
        //        orderProduct.ReplaceUnassignedImage(deletionImage, replacementImage);
        //}


		#region MERGE MEMBERS

        //private Guid _subjectOrderGuid;
        //internal Guid SubjectOrderGuid
        //{
        //    get { return _subjectOrderGuid; }
        //    set
        //    {
        //        _subjectOrderGuid = value;

        //        foreach (OrderProduct orderProduct in this.OrderProducts)
        //        {
        //            orderProduct.OrderPackageGuid = this.ProductPackageGuid;
        //        }
        //    }
        //}

        ///// <summary>
        ///// Returns a clone of the source OrderPackage object
        ///// </summary>
        ///// <returns></returns>
        //internal OrderPackage Clone()
        //{
        //    OrderPackage clone = new OrderPackage();

        //    clone.ProductPackageGuid = this.ProductPackageGuid;
        //    clone.ProductPackageDesc = this.ProductPackageDesc;
        //    clone.AddDate = this.AddDate;

        //    foreach (OrderProduct sourceProduct in this.OrderProductList)
        //    {
        //        OrderProduct rootProduct = sourceProduct.Clone();

        //        clone.OrderProducts.Add(rootProduct);
        //    }

        //    return clone;
        //}

		#endregion

	}
}
