﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.Linq;
using System.Data.SqlServerCe;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using System.Xml;
using System.Xml.Linq;

using Flow.Lib;
using Flow.Lib.Database.SqlCe;
using Flow.Lib.Helpers;
using Flow.Lib.ImageQuix;
using Flow.Lib.Web;
using Flow.Lib.Net;
using Flow.Schema.Properties;
using Flow.Schema.Reports;
using Flow.Schema.Utility;

using ICSharpCode.SharpZipLib.Core;

using StructureMap;
using NetServ.Net.Json;
using System.Windows.Threading;
using Flow.Schema.LinqModel.DataContext.FlowMaster;


namespace Flow.Schema.LinqModel.DataContext
{

    partial class OnlineGallery
    {

        public OnlineGallery(Guid flowProjectGuid, string galleryName)
        {
            // TODO: Complete member initialization
            this.FlowProjectGuid = flowProjectGuid;
            this.GalleryName = galleryName;
        }

        //public void UpdateWelcomeImage()
        //{
        //    this.SendPropertyChanged("WelcomeImage");
        //}
    }
		
}	// END namespace
