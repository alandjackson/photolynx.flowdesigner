﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flow.Schema.LinqModel.DataContext
{
	partial class ProjectImageType
	{
		public ProjectImageType(int flowProjectID, ImageType imageType)
			: this()
		{
			this.ProjectImageTypeDesc = imageType.ImageTypeDesc;
			this.IsExclusive = imageType.IsExclusive;
			if (this.SubjectImageTypes == null)
				this.SubjectImageTypes = new System.Data.Linq.EntitySet<SubjectImageType>();

		}

		public string ImageTypeDesc { get { return this.ProjectImageTypeDesc; } }
	}
}
