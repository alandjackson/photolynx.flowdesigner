﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.Schema.LinqModel.DataContext.FlowMaster;
using Flow.Lib;

namespace Flow.Schema.LinqModel.DataContext
{
    public class ProjectGallerySummary : NotifyPropertyBase
    {
        public string ProjectName { get; set; }
        public string GalleryName { get; set; }
        public FlowProject FlowProject { get; set; }
        public int OnlineImageCount { get; set; }
        public string GalleryURL { get; set; }
        public int YearbookPoses { get; set; }
        public string EventID { get; set; }

        public ProjectGallerySummary(string projName, string galleryName, FlowProject flowProject, string galleryURL, int yearbookPoses, int onlineImageCount, string eventID)
        {
            this.ProjectName = projName;
            this.GalleryName = galleryName;
            this.GalleryURL = galleryURL;
            this.FlowProject = flowProject;
            this.OnlineImageCount = onlineImageCount;

            this.YearbookPoses = yearbookPoses;
            this.EventID = eventID;
        }

        private int _newOnlineOrders = 0;
        public int NewOnlineOrders
        {
            get { return _newOnlineOrders; }
            set
            {
                _newOnlineOrders = value;
                SendPropertyChanged("ShowGetOrderButton");
                SendPropertyChanged("NewOnlineOrders");
                SendPropertyChanged("YearbookPoseCount");
                SendPropertyChanged("ShowDoneLabel");
            }
        }

        public List<IQOrder> newOrders { get; set; }
        public List<int> knownOrderIDs { get; set; }
        public List<string> knownYB1ImageNames { get; set; }
        public List<string> knownYB2ImageNames { get; set; }

        private int _yearbookPoseCount = 0;
        public int YearbookPoseCount
        {
            get { return _yearbookPoseCount; }
            set
            {
                _yearbookPoseCount = value;
                SendPropertyChanged("ShowGetOrderButton");
                SendPropertyChanged("NewOnlineOrders");
                SendPropertyChanged("YearbookPoseCount");
                SendPropertyChanged("ShowDoneLabel");
            }
        }

        public List<string> YBPoseList1 { get; set; }
        public List<string> YBPoseList2 { get; set; }

        private bool _showGetOrderButton = false;
        public bool ShowGetOrderButton
        {
            get
            {
                if (NewOnlineOrders + YearbookPoseCount > 0)
                    _showGetOrderButton = true;
                else
                    _showGetOrderButton = false;
                return _showGetOrderButton;
            }
            set
            {
                _showGetOrderButton = value;
                SendPropertyChanged("ShowGetOrderButton");
                SendPropertyChanged("ShowDoneLabel");
            }
        }

        private bool _showDoneLabel = false;
        public bool ShowDoneLabel
        {
            get
            {
                return _showDoneLabel;
            }
            set
            {
                _showDoneLabel = value;
                SendPropertyChanged("ShowDoneLabel");
            }
        }

        private bool _showBlankLabel = true;
        public bool ShowBlankLabel
        {
            get
            {
                if (ShowDoneLabel == false && ShowGetOrderButton == false)
                    _showBlankLabel = true;
                else
                    _showBlankLabel = false;
                return _showBlankLabel;
            }
        }
    }
}
