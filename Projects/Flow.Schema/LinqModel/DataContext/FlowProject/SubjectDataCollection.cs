﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.Linq;
using System.Data.SqlServerCe;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Media.Imaging;

using Flow.Lib;
using Flow.Lib.Database.SqlCe;
using Flow.Lib.Helpers;
using Flow.Schema.Properties;
using Flow.Schema.Utility;


namespace Flow.Schema.LinqModel.DataContext
{
	/// <summary>
	/// A list of field-values (SubjectDatum) corresponding to Subject data fields/values (key-value pairs)
	/// </summary>
	public class SubjectDataCollection : ObservableCollection<SubjectDatum>
	{
		/// <summary>
		/// Notifies listeners that a contained SubjectDatum item has been modified
		/// </summary>
		internal event EventHandler<EventArgs<SubjectDatum>> SubjectDatumChanged = delegate { };

		/// <summary>
		/// Indexer; returns the SubjectDatum item by the specified key (Field property)
		/// </summary>
		/// <param name="field"></param>
		/// <returns>SubjectDatum</returns>
		public SubjectDatum this[string field]
		{
            get { return this.SingleOrDefault(d => d.Field == field || d.FieldDesc == field); }
		}

		/// <summary>
		/// Invoked when a contained SubjectDatum item is modified
		/// the Subject "record" is then marked as "dirty"
		/// </summary>
		private void SubjectDatum_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			this.SubjectDatumChanged(this, new EventArgs<SubjectDatum>(sender as SubjectDatum));
		}

		/// <summary>
		/// Creates and adds a SubjectDatum item with the specified Field (field name) and Value;
		/// assigns an event handler for propagation of data change notification
		/// </summary>
		/// <param name="field"></param>
		/// <param name="value"></param>
		public void Add(ProjectSubjectField projectSubjectField, object value)
		{
			SubjectDatum subjectDatum = new SubjectDatum(projectSubjectField, value);
			subjectDatum.PropertyChanged += new PropertyChangedEventHandler(SubjectDatum_PropertyChanged);

			this.Add(subjectDatum);
		}

		/// <summary>
		/// Sets the fallback value (StoredValue) of the SubjectDatum to the active value (Value); this
		/// is performed after commission of data to the database
		/// </summary>
		internal void CommitChanges()
		{
			foreach (SubjectDatum item in this)
			{
				item.CommitChange();
			}
		}

		/// <summary>
		/// Cancels any unsaved edits made to SubjectDatum items in the collection
		/// </summary>
		internal void Revert()
		{
			foreach (SubjectDatum item in this)
			{
				item.Revert();
			}
		}

		public void Refresh()
		{
			foreach (SubjectDatum item in this)
			{
				item.Refresh();
			}
		}

	}	// END class

}	// END namespace
