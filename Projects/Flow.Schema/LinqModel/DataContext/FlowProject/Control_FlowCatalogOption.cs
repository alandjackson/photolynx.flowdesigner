﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flow.Schema.LinqModel.DataContext
{
	partial class Control_FlowCatalogOption
	{
        public Control_FlowCatalogOption(FlowCatalogOption flowCatalogOption, FlowProject flowProject)
			: this()
		{
            this.FlowCatalogOptionGuid = (Guid)flowCatalogOption.FlowCatalogOptionGuid;
            this.Map = flowCatalogOption.Map;
            this.Price = flowCatalogOption.Price;
            this.Taxed = flowCatalogOption.Taxed;
            this.IQPrimaryKey = flowCatalogOption.IQPrimaryKey;
            this.ImageQuixCatalogOptionPrimaryKey = (int)flowCatalogOption.IQPrimaryKey;

		}
	}
}
