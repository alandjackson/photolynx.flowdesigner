﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Flow.Lib.Helpers;

using System.Xml.Linq;

namespace Flow.Schema.LinqModel.DataContext
{
	partial class OrderProductNode
	{
		internal Guid OrderProductGuid { get; set; }
		internal Guid SubjectImageGuid { get; set; }
        public string ImageQuixProductNodeResourceURL { get; set; }
		public string ImageQuixProductNodeTypeText
		{
			get
			{
				switch (this.ImageQuixProductNodeType)
				{
					case 1:
						return "image";
					case 2:
						return "text";
                    case 3:
                        return "hidden";
					default:
						return "none";
				}
			}
		}

        internal XElement GetNodeXElement(bool renderGreenScreen, bool useOriginalFileNames)
		{
			XElement nodeElement = new XElement("Node");

			nodeElement.AddNewXAttribute("nodeFK", this.ImageQuixProductNodePk.ZeroPad(8));
            nodeElement.AddNewXAttribute("node-ref", this.ImageQuixProductNodeResourceURL);

			switch (this.ImageQuixProductNodeType)
			{
				// IMAGE
				case 1:
                    if (this.SubjectImage.CropH < 0 || double.IsNaN(this.SubjectImage.CropH))
                        this.SubjectImage.CropH = 0;

                    if (renderGreenScreen)
                        nodeElement.AddNewXAttribute("imageFileName", ((this.SubjectImage == null) ? "" : this.SubjectImage.GetDp2FileName(this.OrderProduct.GreenScreenBackground, useOriginalFileNames, renderGreenScreen)));
                    else
                        nodeElement.AddNewXAttribute("imageFileName", ((this.SubjectImage == null) ? "" : this.SubjectImage.Dp2FileName(useOriginalFileNames)));

                    nodeElement.AddNewXAttribute("type", this.ImageQuixProductNodeTypeText);
					nodeElement.AddNewXAttribute("title", "");
					nodeElement.AddNewXAttribute("imageSourcePath", "");
                    double _cropX = double.Parse((this.SubjectImage.CropL * 100).ToString("N5"));
                    double _cropY = double.Parse((this.SubjectImage.CropT * 100).ToString("N5"));
                    double _cropW = double.Parse((this.SubjectImage.CropW * 100).ToString("N5"));
                    double _cropH = double.Parse((this.SubjectImage.CropH * 100).ToString("N5"));
                    nodeElement.AddNewXAttribute("cropX", (_cropX + (_cropW / 2)));
					nodeElement.AddNewXAttribute("cropY", (_cropY + (_cropH / 2)));
					nodeElement.AddNewXAttribute("cropWidth", _cropW);
                    nodeElement.AddNewXAttribute("cropHeight", _cropH);
					nodeElement.AddNewXAttribute("orientation", this.SubjectImage.Orientation);
					break;

				// TEXT
				case 2:
					nodeElement.AddNewXAttribute("imageFileName", "");
					nodeElement.AddNewXAttribute("type", this.ImageQuixProductNodeTypeText);
					nodeElement.AddNewXAttribute("title", this.NodeText);
					nodeElement.AddNewXAttribute("imageSourcePath", "");
					nodeElement.AddNewXAttribute("cropX", "");
					nodeElement.AddNewXAttribute("cropY", "");
					nodeElement.AddNewXAttribute("cropWidth", "");
					nodeElement.AddNewXAttribute("cropHeight", "");
					nodeElement.AddNewXAttribute("orientation", "");
					break;

				// NONE
				default:
					nodeElement.AddNewXAttribute("imageFileName", "");
					nodeElement.AddNewXAttribute("type", this.ImageQuixProductNodeTypeText);
					nodeElement.AddNewXAttribute("title", "");
					nodeElement.AddNewXAttribute("imageSourcePath", "");
					nodeElement.AddNewXAttribute("cropX", "");
					nodeElement.AddNewXAttribute("cropY", "");
					nodeElement.AddNewXAttribute("cropWidth", "");
					nodeElement.AddNewXAttribute("cropHeight", "");
					nodeElement.AddNewXAttribute("orientation", "");
					break;
			}


			return nodeElement;
		}


		#region MERGE MEMBERS

		internal int? MergeSubjectImageID { get; private set; }

		internal OrderProductNode Clone()
		{
			OrderProductNode clone = new OrderProductNode();

			clone.NodeText = this.NodeText;
			clone.ImageQuixProductNodePk = this.ImageQuixProductNodePk;
			clone.ImageQuixProductNodeType = this.ImageQuixProductNodeType;
            clone.ImageQuixProductNodeResourceURL = this.ImageQuixProductNodeResourceURL;
			if (this.ImageQuixProductNodeType == 1 && this.SubjectImageID.HasValue)
			{
				clone.MergeSubjectImageID = this.SubjectImageID.Value;
			}

			return clone;
		}

		#endregion



        public string UniqueNodeImageName
        {
            get
            {
                return this.OrderProduct.OrderPackage.SubjectOrder.Subject.TicketCode + "_" + this.OrderProductNodeID + ".jpg";
            }
        }
    }
}
