using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.Linq;
using System.Data.SqlServerCe;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Media.Imaging;

using Flow.Lib;
using Flow.Lib.Database.SqlCe;
using Flow.Lib.Helpers;
using Flow.Schema.Properties;
using Flow.Schema.Utility;
using System.Windows.Threading;
using Flow.Lib.AsyncWorker;
using NLog;


namespace Flow.Schema.LinqModel.DataContext
{
    /// <summary>
    /// LINQ-to-SQL DataContext for representation of and access to application-level data stored
    /// in a specific Flow project database
    /// </summary>
    partial class FlowProjectDataContext : INotifyPropertyChanged
    {
        public event EventHandler SubjectListChanged = delegate { };
        public event EventHandler UpdateNetworkProjectNeeded = delegate { };
        public int SubjectsInitializedCounter = 0;
        public Boolean NeedPackageSummaryAndTicketCodeUpdated = false;

        static readonly object locker = new object();

        public BitmapImage MissingImage { get; set; }

        /// <summary>
        /// A reference to the application database (FlowMaster) datacontext
        /// </summary>
        private FlowMasterDataContext _flowMasterDataContext = null;
        public FlowMasterDataContext FlowMasterDataContext { get { return _flowMasterDataContext; } }

        private event EventHandler _subjectDataLoaded = delegate { };
        public event EventHandler SubjectDataLoaded
        {
            add
            {
                _subjectDataLoaded = delegate { };
                _subjectDataLoaded += value;
            }
            remove { }
        }

        private static Logger logger = LogManager.GetCurrentClassLogger();



        private bool _updateNetworkProject = false;
        public bool UpdateNetworkProject
        {
            get
            {

                return _updateNetworkProject;
            }
            set
            {
                _updateNetworkProject = value;
                UpdateNetworkProjectNeeded(null, null);
            }
        }
        public new void SubmitChanges()
        {
            //logger.Info("about to submit changes to DB");
            lock (locker)
            {
                if (this.SubjectList != null && this.SubjectList.CurrentItem != null)
                    this.SubjectList.CurrentItem.FlagForMerge = true;

                if (this.FlowProject != null && this.FlowProject.IsNetworkProject == true)
                {
                    UpdateNetworkProject = true;

                }


                int tryCount = 0;
                while (tryCount < 10)
                {
                    try
                    {
                        base.SubmitChanges();
                        tryCount = 100;//its good, bail the while loop
                    }
                    catch (Exception ex)
                    {
                        tryCount++;
                        logger.Info("Failed on Submit Changes: " + ex.Message);
                        Thread.Sleep(1000);
                        base.ChangeConflicts.ResolveAll(RefreshMode.KeepCurrentValues);
                        //base.SubmitChanges();
                    }
                }
            }
            // logger.Info("done submit changes to DB");

        }

        //this will not trigger the UpdateNetworkProject flag
        public new void SubmitChangesLocal()
        {
            lock (locker)
            {
                base.SubmitChanges();
            }
        }
        /// <summary>
        /// Retrieves a list of FlowMaster database schema versions
        /// </summary>
        /// <param name="databaseFilePath"></param>
        public static List<DbVersion> GetDbVersionHistory()
        {
            string connectionString = "Data Source=" + FlowContext.FlowProjectTemplateFilePath + ";Max Database Size=1024;";

            List<DbVersion> versionList;



            using (FlowProjectDataContext dataContext = new FlowProjectDataContext(new SqlCeConnection(connectionString)))
            {
                versionList = new List<DbVersion>(dataContext.DbVersions.OrderBy(v => v.DbVersionDate));
                dataContext.Connection.Close();
                dataContext.Dispose();
            }

            return versionList;
        }

        public static List<ProjectSubjectField> GetProjectSubjectFieldList(FlowProject flowProject)
        {
            List<ProjectSubjectField> fieldList = new List<ProjectSubjectField>();
            if (flowProject != null)
            {
                fieldList = new List<ProjectSubjectField>(flowProject.FlowProjectDataContext.ProjectSubjectFields);
                //using (FlowProjectDataContext dataContext = new FlowProjectDataContext(flowProject.ConnectionString))
                //{
                //    fieldList = new List<ProjectSubjectField>(dataContext.ProjectSubjectFields);
                //}
            }
            return fieldList;
        }


        private FlowObservableCollection<SubjectTicket> _subjectTicketList = null;
        public FlowObservableCollection<SubjectTicket> SubjectTicketList
        {
            get
            {
                if (_subjectTicketList == null)
                {
                    _subjectTicketList = new FlowObservableCollection<SubjectTicket>(this.SubjectTickets);
                }

                return _subjectTicketList;
            }
        }


        private List<String> _flagFilterOptions = null;
        public List<String> FlagFilterOptions
        {
            get
            {
                if (_flagFilterOptions == null)
                {
                    _flagFilterOptions = new List<String>();
                    _flagFilterOptions.Add("is Hold Image");
                    _flagFilterOptions.Add("is not Hold Image");
                    _flagFilterOptions.Add("is Primary Image");
                    _flagFilterOptions.Add("is not Primary Image");
                    foreach (ProjectImageType type in this.ProjectImageTypes)
                    {
                        _flagFilterOptions.Add("is " + type.ProjectImageTypeDesc);
                        _flagFilterOptions.Add("is not " + type.ProjectImageTypeDesc);
                    }

                }
                return _flagFilterOptions;
            }
        }

        private List<String> _imageSourceList = null;
        public List<String> ImageSourceList
        {
            get
            {
                if (_imageSourceList == null)
                {
                    _imageSourceList = new List<String>(this.ImageList.GroupBy(i => i.SourceComputer).Select(g => g.First().SourceComputer));
                    //_imageSourceList.Insert(0, "All");

                    //if (!ImageSourceList.Contains(this.FlowMasterDataContext.PreferenceManager.Edit.ImageFilterSource))
                    //    this.FlowMasterDataContext.PreferenceManager.Edit.ImageFilterSource = "All";
                }
                return _imageSourceList;
            }
        }

        private FlowObservableCollection<OrderFormFieldLocal> _orderFormFieldLocalList = null;
        public FlowObservableCollection<OrderFormFieldLocal> OrderFormFieldLocalList
        {
            get
            {
                if (_orderFormFieldLocalList == null)
                {
                    _orderFormFieldLocalList = new FlowObservableCollection<OrderFormFieldLocal>(this.OrderFormFieldLocals.OrderBy(o => o.SortOrder));
                }

                return _orderFormFieldLocalList;
            }
        }

        /// <summary>
        /// Event invoked when assigned Subject images are detached (unassigned)
        /// </summary>
        public event EventHandler<EventArgs<IEnumerable<SubjectImage>>> SubjectImagesRemoved = delegate { };

        #region FIELDS AND PROPERTIES

        /// <summary>
        /// The FlowProject associated with the datacontext
        /// </summary>
        private FlowProject _flowProject = null;
        public FlowProject FlowProject
        {
            get { return _flowProject; }
            set { _flowProject = value; }
        }


        private SqlCeManager _flowSqlCeManager = null;

        private DataSet _subjectDataSet = null;

        /// <summary>
        /// Local data structure for managing Subject records
        /// </summary>
        private DataTable _subjectDataTable = null;
        internal DataTable SubjectDataTable
        {
            get
            {
                if (_subjectDataTable == null)
                {
                    this.LoadSubjectData();
                }

                return _subjectDataTable;
            }
        }

        // NOTE: consider eliminating the FlowProjectLocal entirely and replacing with new Control_FlowProject entity
        /// <summary>
        /// Flow project meta-data; contains a single row;
        /// ImageSequenceID field provides incrementing value for assignment to image filenames
        /// </summary>
        private FlowProjectLocal _projectLocal = null;
        internal FlowProjectLocal ProjectRecord
        {
            get
            {
                if (_projectLocal == null)
                    _projectLocal = this.FlowProjectLocals.Single(l => l.FlowProjectGuid == _flowProject.FlowProjectGuid);

                return _projectLocal;
            }
        }




        /// <summary>
        /// FlowObservableCollection for generated ProjectSubjectField Linq-to-SQL entity
        /// </summary>
        private FlowObservableCollection<ProjectSubjectField> _projectSubjectFieldList = null;
        public FlowObservableCollection<ProjectSubjectField> ProjectSubjectFieldList
        {
            get
            {
                if (_projectSubjectFieldList == null)
                {
                    _projectSubjectFieldList = new FlowObservableCollection<ProjectSubjectField>(this.ProjectSubjectFields);
                }

                return _projectSubjectFieldList;
            }
        }



        /// <summary>
        /// FlowObservableCollection for generated SubjectField Linq-to-SQL entity
        /// </summary>
        private SubjectObservableCollection _subjectList = null;
        internal SubjectObservableCollection SubjectList
        {
            get
            {
                return _subjectList;
            }
            private set
            {
                _subjectList = value;
                _subjectList.CollectionChanged += delegate { this.SubjectListChanged(this, new EventArgs()); };

                this.SubjectListChanged += new EventHandler(this.FlowProject.OnSubjectListChanged);

                this.SubjectListChanged(this, new EventArgs());

                this.FlowProject.Initialized = true;
            }
        }

        bool SubjectInited = false;
        public void InitSubjectList()
        {

            if (!SubjectInited)
            {
                SubjectInited = true;
                SubjectList = new SubjectObservableCollection(this.Subjects, _flowProject, OnSubjectDataLoaded);
            }
        }

        private void OnSubjectDataLoaded(object sender, EventArgs e)
        {
            _subjectDataLoaded(this, null);
            this.FlowProject.Initialized = true;
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void SendPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        /// <summary>
        /// FlowObservableCollection for generated SubjectField Linq-to-SQL entity
        /// </summary>
        private FlowObservableCollection<SubjectImage> _imageList = null;
        internal FlowObservableCollection<SubjectImage> ImageList
        {
            get
            {
                if (_imageList == null)
                    _imageList = new FlowObservableCollection<SubjectImage>(this.SubjectImages);

                return _imageList;
            }
        }

        /// <summary>
        /// FlowObservableCollection for generated SubjectField Linq-to-SQL entity
        /// </summary>
        private FlowObservableCollection<SubjectOrder> _orderList = null;
        internal FlowObservableCollection<SubjectOrder> OrderList
        {
            get
            {
                if (_orderList == null)
                    _orderList = new FlowObservableCollection<SubjectOrder>(this.SubjectOrders);

                return _orderList;
            }
        }

        private FlowObservableCollection<SubjectImage> _holdImageList = null;
        public FlowObservableCollection<SubjectImage> HoldImageList
        {
            get
            {
                if (_holdImageList == null)
                {
                    try
                    {
                        _holdImageList = new FlowObservableCollection<SubjectImage>(this.SubjectImages.Where(si => si.ImageFlagged == true));
                    }
                    catch (Exception e)
                    {
                        _holdImageList = null;
                    }
                }

                return _holdImageList;
            }
        }
        public String HoldImageCount
        {
            get
            {
                if (HoldImageList == null)
                    return "0";

                return HoldImageList.Count().ToString();
            }
        }
        public void UpdateHoldImageList()
        {
            this.SubmitChanges();
            _holdImageList = new FlowObservableCollection<SubjectImage>(this.SubjectImages.Where(si => si.ImageFlagged == true));
            SendPropertyChanged("HoldImageList");
            SendPropertyChanged("HoldImageCount");

        }

        private string _selectedOrderFilter { get; set; }
        public string SelectedOrderFilter
        {
            get
            {
                if (string.IsNullOrEmpty(_selectedOrderFilter))
                    return "New Orders";
                else
                    return _selectedOrderFilter;
            }
            set
            {
                _selectedOrderFilter = value;
            }
        }

        public void InitOrderFilter()
        {
            OrderFilterCheckAll = true;
            OrderFilterNew = true;
            OrderFilterAll = false;
            OrderFilterPrevious = false;
            OrderFilterPreviousSelection = "";
            OrderFilterWebAndManual = true;
            OrderFilterWeb = false;
            OrderFilterManual = false;
            OrderFilterAllSubjects = true;
            OrderFilterFilteredSubjects = false;
            OrderFilterCurrentSubject = false;
            OrderFilterReadySubject = false;
        }
        public bool OrderFilterCheckAll { get; set; }

        public bool OrderFilterNew { get; set; }
        public bool OrderFilterAll { get; set; }
        public bool OrderFilterPrevious { get; set; }
        public string OrderFilterPreviousSelection { get; set; }

        public bool OrderFilterWebAndManual { get; set; }
        public bool OrderFilterWeb { get; set; }
        public bool OrderFilterManual { get; set; }

        public bool OrderFilterAllSubjects { get; set; }
        public bool OrderFilterFilteredSubjects { get; set; }
        public bool OrderFilterCurrentSubject { get; set; }
        public bool OrderFilterReadySubject { get; set; }

        private FlowObservableCollection<OrderPackage> _filteredOrderPackages { get; set; }
        public FlowObservableCollection<OrderPackage> FilteredOrderPackages
        {
            get
            {
                if (_filteredOrderPackages == null)
                    _filteredOrderPackages = new FlowObservableCollection<OrderPackage>(this.OrderPackages);

                return _filteredOrderPackages;
            }
        }
        public void UpdateFilteredOrderPackages()
        {
            _filteredOrderPackages = null;//clear it first

            if (OrderFilterNew)
                if (FilteredOrderPackages.Any(op => op.LabOrderID == null))
                    _filteredOrderPackages = new FlowObservableCollection<OrderPackage>(FilteredOrderPackages.Where(op => op.LabOrderID == null));
                else
                    _filteredOrderPackages = new FlowObservableCollection<OrderPackage>();

            else if (OrderFilterAll)
                _filteredOrderPackages = new FlowObservableCollection<OrderPackage>(this.OrderPackages);
            else if (OrderFilterPrevious && !string.IsNullOrEmpty(OrderFilterPreviousSelection))
            {
                string labOrderId = OrderFilterPreviousSelection.Split("-")[0].Trim();
                int ilabOrderId = 0;
                Int32.TryParse(labOrderId, out ilabOrderId);
                if (ilabOrderId > 0)
                    _filteredOrderPackages = new FlowObservableCollection<OrderPackage>(FilteredOrderPackages.Where(op => op.LabOrderID == ilabOrderId));
            }
            else
            {
                _filteredOrderPackages = new FlowObservableCollection<OrderPackage>();
                SendPropertyChanged("FilteredOrderPackages");
                return;
            }


            if (OrderFilterWebAndManual)
            {
                //no need to exclude here
            }
            else if (OrderFilterWeb)
            {
                if (FilteredOrderPackages.Any(op => op.IsOnlineOrder))
                    _filteredOrderPackages = new FlowObservableCollection<OrderPackage>(FilteredOrderPackages.Where(op => op.IsOnlineOrder));
                else
                    _filteredOrderPackages = new FlowObservableCollection<OrderPackage>();
            }
            else if (OrderFilterManual)
            {
                if (FilteredOrderPackages.Any(op => !op.IsOnlineOrder))
                    _filteredOrderPackages = new FlowObservableCollection<OrderPackage>(FilteredOrderPackages.Where(op => !op.IsOnlineOrder));
                else
                    _filteredOrderPackages = new FlowObservableCollection<OrderPackage>();
            }
            else
            {
                _filteredOrderPackages = new FlowObservableCollection<OrderPackage>();
                SendPropertyChanged("FilteredOrderPackages");
                return;
            }


            if (OrderFilterAllSubjects)
            {
                //no need to exclude any orders here
            }
            else if (OrderFilterFilteredSubjects)
                if (FilteredOrderPackages.Any(op => this.FlowProject.GetFilteredSubjectList().Contains(op.SubjectOrder.Subject)))
                    _filteredOrderPackages = new FlowObservableCollection<OrderPackage>(FilteredOrderPackages.Where(op => this.FlowProject.GetFilteredSubjectList().Contains(op.SubjectOrder.Subject)));
                else
                    _filteredOrderPackages = new FlowObservableCollection<OrderPackage>();
            else if (OrderFilterCurrentSubject)
                _filteredOrderPackages = new FlowObservableCollection<OrderPackage>(FilteredOrderPackages.Where(op => this.FlowProject.SubjectList.CurrentItem == op.SubjectOrder.Subject));
            else if (OrderFilterReadySubject)
                _filteredOrderPackages = new FlowObservableCollection<OrderPackage>(FilteredOrderPackages.Where(op => op.SubjectOrder.Subject.Ready && this.FlowProject.FlowMasterDataContext.PreferenceManager.Capture.UseReadyFlag == true));
            else
            {
                _filteredOrderPackages = new FlowObservableCollection<OrderPackage>();
                SendPropertyChanged("FilteredOrderPackages");
                return;
            }

            SendPropertyChanged("FilteredOrderPackages");


        }
        public void UpdateOrderPackages()
        {
            _filteredOrderPackages = null;
            UpdateFilteredOrderPackages();
            SendPropertyChanged("OrderPackages");
            SendPropertyChanged("FilteredOrderPackages");


        }

        public void UpdateOrderFilterValues()
        {
            SendPropertyChanged("OrderFilterNew");
            SendPropertyChanged("OrderFilterAll");
            SendPropertyChanged("OrderFilterPrevious");
            SendPropertyChanged("OrderFilterWebAndManual");
            SendPropertyChanged("OrderFilterWeb");
            SendPropertyChanged("OrderFilterManual");
            SendPropertyChanged("OrderFilterAllSubjects");
            SendPropertyChanged("OrderFilterFilteredSubjects");
            SendPropertyChanged("OrderFilterCurrentSubject");
            SendPropertyChanged("OrderFilterReadySubject");
        }

        public void ClearHoldImageList()
        {
            foreach (SubjectImage si in HoldImageList)
            {
                si.ImageFlagged = false;
            }
            UpdateHoldImageList();

        }
        private FlowObservableCollection<GroupImage> _groupimageList = null;
        public FlowObservableCollection<GroupImage> GroupImageList
        {
            get
            {
                if (_groupimageList == null)
                    _groupimageList = new FlowObservableCollection<GroupImage>(this.GroupImages);

                return _groupimageList;
            }
        }
        /// <summary>
        /// Indicates whether or not a barcode scan field has been assigned
        /// </summary>
        private bool? _hasScanKeyField = null;
        public bool? HasScanKeyField
        {
            get
            {
                if (!_hasScanKeyField.HasValue)
                {
                    this.ScanKeyField = this.ProjectSubjectFieldList.FirstOrDefault(f => f.IsScanKeyField);
                    _hasScanKeyField = this.ScanKeyField != null;
                }

                return _hasScanKeyField;
            }
        }

        public ProjectSubjectField ScanKeyField { get; private set; }


        #endregion


        #region CONSTRUCTORS

        /// <summary>
        /// Primary constructor; passes references for the FlowProject and FlowMasterDataContext
        /// </summary>
        /// <param name="flowProject"></param>
        /// <param name="flowMasterDataContext"></param>
        internal FlowProjectDataContext(FlowProject flowProject, FlowMasterDataContext flowMasterDataContext)
            : base(new SqlCeConnection(flowProject.ConnectionString))
        {
            SqlCeEngine e = new SqlCeEngine(flowProject.ConnectionString);
            BackupSDF35(e, flowProject.DatabasePath);
            e.Dispose();



            _flowProject = flowProject;

            _flowMasterDataContext = flowMasterDataContext;



            //DataLoadOptions options = new DataLoadOptions();
            //options.LoadWith<Subject>(s => s.SubjectOrders);
            //options.LoadWith<SubjectOrder>(o => o.OrderPackages);
            //options.LoadWith<OrderPackage>(p => p.OrderProducts);
            //options.LoadWith<OrderProduct>(p => p.OrderProductNodes);
            //options.LoadWith<OrderProduct>(p => p.OrderProductOptions);

#if DEBUG
            // If OutputSqlCeCommandText = true, prints the Linq generated SQL commands to the output window
            if (Settings.Default.OutputSqlCeCommandText)
                this.Log = new DebugTextWriter();
#endif


        }

        /// <summary>
        /// Constuctor invoked when attaching an external project
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="flowMasterDataContext"></param>
        internal FlowProjectDataContext(SqlCeConnection connectionString, FlowMasterDataContext flowMasterDataContext)
            : base(connectionString)
        {
            _flowMasterDataContext = flowMasterDataContext;
            SqlCeEngine eng = new SqlCeEngine(connectionString.ConnectionString);
            BackupSDF35(eng, connectionString.Database);
            eng.Dispose();
        }

        /// <summary>
        /// Constuctor invoked when checking for and installing schema updates
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="flowMasterDataContext"></param>
        internal FlowProjectDataContext(FlowProject flowProject)
            : base(new SqlCeConnection(flowProject.ConnectionString))
        {
        }

        #endregion


        /// <summary>
        /// Notifies listeners when Subject images have been detached (unassigned)
        /// </summary>
        /// <param name="imageList"></param>
        internal void OnSubjectImagesRemoved(IEnumerable<SubjectImage> imageList)
        {
            this.SubjectImagesRemoved(this, new EventArgs<IEnumerable<SubjectImage>>(imageList));
        }


        #region METHODS

        /// <summary>
        /// Loads Subject data into an ADO.NET dataset/datatable
        /// </summary>
        internal void LoadSubjectData()
        {
            // Create the database management object, supplying the connectionstring to the project db
            _flowSqlCeManager = new SqlCeManager(_flowProject.ConnectionString);

            // Ignore any source columns which do not have a matching ProjectSubjectField
            _flowSqlCeManager.DataAdapter.MissingMappingAction = MissingMappingAction.Ignore;

            // Map the Subject database table to the Subject DataTable
            DataTableMapping map = _flowSqlCeManager.AddMapping("Subject", "Subject");

            _subjectDataSet = new DataSet();
            _subjectDataTable = new DataTable("Subject");

            // Create the SubjectID DataColumn and add the the Subject DataTable
            DataColumn _subjectIDColumn = new DataColumn("SubjectID", typeof(int));
            _subjectDataTable.Columns.Add(_subjectIDColumn);
            _subjectDataTable.PrimaryKey = new DataColumn[] { _subjectIDColumn };

            // Map the source SubjectID field to the SubjectID field of the DataTable
            map.ColumnMappings.Add("SubjectID", "SubjectID");

            // Create a DataTable DataColumn for each user-defined subject field
            foreach (ProjectSubjectField field in this.ProjectSubjectFields)
            {

                DataColumn column = new DataColumn(
                    field.ProjectSubjectFieldDisplayName,
                    field.SubjectFieldDataType.NativeType
                );

                column.AllowDBNull = !field.IsRequiredField;

                _subjectDataTable.Columns.Add(column);

                // Map the souce field to the DataTable field
                map.ColumnMappings.Add(field.ProjectSubjectFieldDesc, field.ProjectSubjectFieldDisplayName);
            }

            _subjectDataSet.Tables.Add(_subjectDataTable);

            // Load the data from the source database table into the 
            try
            {
                _flowSqlCeManager.Load(_subjectDataSet, "Subject");
            }
            catch (Exception e)
            {
                this.FlowProject.ProblemLoadingSubjectData = true;
                logger.WarnException("Problem loading subject data - this project may be corrupt: ", e);
                //throw;
            }

            //			_subjectList = new SubjectObservableCollection(this.Subjects, _flowProject);
        }

        /// <summary>
        /// Imports Subject data from an external source into the project
        /// </summary>
        /// <param name="columnMappings"></param>
        /// <param name="sourceTable"></param>
        internal void ImportData(ImportColumnPairCollection<ProjectSubjectField> columnMappings, DataTable sourceTable, NotificationProgressInfo info, Dispatcher dispatcher, string imageDir, string imageField, bool auto8x10)
        {
            // If no key fields are specified, specify last and first name as key
            //if (columnMappings.KeyFields.Count == 0)
            //{
            //    columnMappings.KeyFields.Add(columnMappings["Last Name"]);
            //    columnMappings.KeyFields.Add(columnMappings["First Name"]);
            //}


            info.Update("Sorting...");

            // Create the list of columns to sort and filter out null values by
            List<string> sortColumns = new List<string>();

            string lastNameSourceFieldName =
                columnMappings.FirstOrDefault(m => m.DestinationField != null && m.DestinationField.SubjectFieldDesc == "LastName").SourceField.ProjectSubjectFieldDisplayName;

            string firstNameSourceFieldName =
                columnMappings.FirstOrDefault(m => m.DestinationField != null && m.DestinationField.SubjectFieldDesc == "FirstName").SourceField.ProjectSubjectFieldDisplayName;

            foreach (ImportColumnPair<ProjectSubjectField> keyField in columnMappings.KeyFields)
            {
                sortColumns.Add(keyField.SourceField.ProjectSubjectFieldDisplayName);
            }

            //if (!sortColumns.Any(c => c == lastNameSourceFieldName))
            //    sortColumns.Add(lastNameSourceFieldName);

            //if (!sortColumns.Any(c => c == firstNameSourceFieldName))
            //    sortColumns.Add(firstNameSourceFieldName);

            EnumerableRowCollection<DataRow> prepTable = sourceTable.AsEnumerable();

            // Filter out null values from key and name fields
            //foreach (string sortColumn in sortColumns)
            //{
            //    prepTable = prepTable.Where(r => !String.IsNullOrEmpty(r.Field<string>(sortColumn)));
            //}

            DataView sortedView = prepTable.AsDataView();

            // Sort by key and name fields
            sortedView.Sort = String.Join(", ", sortColumns.Select(c => "[" + c + "]").ToArray());


            //int progressMax = sourceTable.DefaultView.Count * 2 + 1;
            //double progress = 0;
            /// Process the rows

            // Create the control row
            DataRow priorRow = sourceTable.NewRow();
            foreach (ImportColumnPair<ProjectSubjectField> keyField in columnMappings.KeyFields)
            {
                string sourceKeyFieldName = keyField.SourceField.ProjectSubjectFieldDisplayName;

                Type keyFieldDataType = sourceTable.Columns[sourceKeyFieldName].DataType;

                if (keyFieldDataType.IsUnsignedNumeric())
                    priorRow[keyField.SourceField.ProjectSubjectFieldDisplayName] = -1;
                else
                    priorRow[keyField.SourceField.ProjectSubjectFieldDisplayName] = Guid.NewGuid().ToString();
            }

            int recordCount = sourceTable.DefaultView.Count;

            info.Update("Processing record {0} of {1}...", 0, recordCount, 1, true);


            // Iterate through the key-sorted rows of the source table
            foreach (DataRow sourceRow in sortedView.ToTable().Rows)
            {
                info++;

                FilterBuilder<ProjectSubjectField> filterItems = new FilterBuilder<ProjectSubjectField>();
                bool isDuplicateRow = columnMappings.IgnoreDuplicates;

                string imageFileName = "";
                if (imageField != null && imageField.Length > 0 && sourceRow[imageField] != null)
                    imageFileName = sourceRow[imageField].ToString();

                // Compare each of the key fields of the current/prior row
                foreach (ImportColumnPair<ProjectSubjectField> keyField in columnMappings.KeyFields)
                {
                    isDuplicateRow = isDuplicateRow &&
                        sourceRow[keyField.SourceField.ProjectSubjectFieldDisplayName].ToString() == priorRow[keyField.SourceField.ProjectSubjectFieldDisplayName].ToString();

                    filterItems.Add(new KeyValuePair<ProjectSubjectField, object>(
                        keyField.DestinationField,
                        sourceRow[keyField.SourceField.ProjectSubjectFieldDisplayName]
                    ));
                }

                //if (isDuplicateRow && columnMappings.ImportKeyedDuplicates)
                //{
                //    isDuplicateRow = isDuplicateRow &&
                //        sourceRow["Last Name"].ToString() == priorRow["Last Name"].ToString() &&
                //        sourceRow["First Name"].ToString() == priorRow["First Name"].ToString()
                //    ;
                //}

                // If the current row is a duplicate, move to the next row
                bool isMatchingOnTicketCode = false;
                if (columnMappings.KeyFields.Count() == 1 && columnMappings.KeyFields[0].DestinationFieldDisplayName == "Ticket Code")
                    isMatchingOnTicketCode = true;
                if (isDuplicateRow && !isMatchingOnTicketCode)
                    continue;


                // Attempt to locate an existing subject by key fields; create new if none found
                Subject subject = (columnMappings.KeyFields.Count == 0)
                    ? null
                    : _flowProject.FindSubjectByKeyFields(filterItems.Expression)
                ;

                //if (subject == null)
                //{
                //    if (1 == _flowProject.SubjectList.Count(s => s.SubjectData["First Name"].Value.ToString() == sourceRow["First Name"].ToString() && s.SubjectData["Last Name"].Value.ToString() == sourceRow["Last Name"].ToString()))
                //        subject = _flowProject.SubjectList.First(s => s.SubjectData["First Name"].Value.ToString() == sourceRow["First Name"].ToString() && s.SubjectData["Last Name"].Value.ToString() == sourceRow["Last Name"].ToString());
                //}
                bool forceNewTicketCode = false;
                if (subject == null)
                {
                    if (!columnMappings.ImportUnmatched)
                        continue;

                    subject = new Subject(this);
                    forceNewTicketCode = true;
                }
                else
                    subject.IsDirty = true;


                // Iterate through fields and assign source values to Subject data
                foreach (ImportColumnPair<ProjectSubjectField> columnMapping in columnMappings.Where(m => m.IncludeColumn))
                {
                    string destinationFieldName = columnMapping.DestinationField.ProjectSubjectFieldDisplayName;
                    string sourceFieldName = columnMapping.SourceField.ProjectSubjectFieldDisplayName;

                    // Remove leading/trailing whitespace if type is string
                    if (columnMapping.DestinationField.SubjectFieldDataType.NativeType == typeof(string))
                    {
                        //make sure the incoming value is not blank (we dont want to overwrite existing data with blank data)
                        if (sourceRow[sourceFieldName].ToString().Trim().Length > 0)
                            subject.SubjectData[destinationFieldName].Value = sourceRow[sourceFieldName].ToString().Trim();
                    }
                    else
                        subject.SubjectData[destinationFieldName].Value = sourceRow[sourceFieldName];
                }
                if (subject.TicketCode != null && subject.TicketCode.Length > 6)
                    forceNewTicketCode = false;

                if (string.IsNullOrEmpty(subject.TicketCode) || forceNewTicketCode)
                    subject.TicketCode = TicketGenerator.GenerateTicketString(8);


                //subject.UpdatePackageSummary();

                // Insert the base Subject record
                if (subject.IsNew)
                {
                    //  dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                    // {
                    this.Subjects.InsertOnSubmit(subject);
                    //  }));
                }

                //this.SubmitChanges();
                //subject.SaveSubjectData();


                //add image
                if (imageDir != null && imageFileName != null && imageFileName.Length > 0)
                {
                    string thisFile = Path.Combine(imageDir, imageFileName);
                    if (File.Exists(thisFile))
                    {
                        string newImageFileName = "";
                        // Assign the image to the subject
                        this.FlowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                        {
                            newImageFileName = subject.AssignImage(imageFileName, FlowMasterDataContext.LoginID, auto8x10);
                        }));

                        // Create the destination filename
                        string newImageFilePath = Path.Combine(this.FlowProject.ImageDirPath, newImageFileName);

                        // Copy the image to the project's image directory
                        IOUtil.CopyFile(thisFile, newImageFilePath);

                        if (auto8x10)
                        {
                            SubjectImage newsi = subject.SubjectImages.FirstOrDefault(si => si.ImagePathFullRes == newImageFilePath);
                            if (newsi != null)
                                newsi.AutoCrop();

                        }
                        this.SubmitChanges();
                    }
                }
                //SubjectTicket subTicket = new SubjectTicket();
                //subTicket.SubjectID = subject.SubjectID;
                //subTicket.TicketID = TicketGenerator.GenerateTicketString(8);
                //this.SubjectTickets.InsertOnSubmit(subTicket);
                //this.SubmitChanges();

                // Set current row as prior for the next comparison
                priorRow = sourceRow;
            }

            this.SubmitChanges();

            // Refresh the record count in the event that duplicates were ignored
            recordCount = this.Subjects.Count();

            info.Update("Saving record {0} of {1}...", 0, recordCount, 1, true);

            // Synch the DataRow from the SubjectDataCollection
            int badCount = 0;
            foreach (Subject subject in this.Subjects)
            {
                info++;

                if (subject.IsNew || subject.IsDirty)
                {
                    try
                    {
                        subject.SaveSubjectData(false);
                        subject.FlagForMerge = true;
                    }
                    catch (Exception)
                    {

                        badCount++;
                    }
                }
            }
            if (badCount > 0)
            {
                throw new Exception("There were " + badCount + " subjects that were missing required fields.\n\nThese subjects were not imported");
            }
            // Save records in batch to the db
            _flowSqlCeManager.Update(this.SubjectDataTable);

            info.Complete("Import complete.");

            // Populate the obsevable collection
            this.SubjectList = new SubjectObservableCollection(this.Subjects, this.FlowProject, false);

            // Update the barcode scan key field value length, if required
            ProjectSubjectField scanKeyField = this.ProjectSubjectFieldList.FirstOrDefault(f => f.IsScanKeyField);

            if (scanKeyField != null && this.FlowProject.BarcodeScanValueLength.HasValue)
            {
                int maxValueLength = _subjectList
                    .Max(s => ((string)s.SubjectData[scanKeyField.ProjectSubjectFieldDisplayName].Value).Length);

                if (this.FlowProject.BarcodeScanValueLength == null ||
                    this.FlowProject.BarcodeScanValueLength < maxValueLength
                )
                {
                    this.FlowProject.BarcodeScanValueLength = maxValueLength;
                    _flowMasterDataContext.SubmitChanges();
                }
            }

        }

        /// <summary>
        /// Submits changes to Subject data to the database
        /// </summary>
        /// <param name="subject"></param>
        internal void Update(Subject subject)
        {
            _flowSqlCeManager.Update(subject.SubjectDataRow);
        }
        public void UpdateAllSubjectData()
        {
            _flowSqlCeManager.Update(this.SubjectDataTable);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectTemplate"></param>
        internal void InitializeProjectFromTemplate(FlowProject flowProject, ProjectTemplate projectTemplate)
        {
            // Add a new local project record from master project record
            this.FlowProjectLocals.InsertOnSubmit(new FlowProjectLocal(flowProject));

            flowProject.FlowVersion = FlowContext.FlowVersion;

            MasterDbVersion currentMasterDbVersion = _flowMasterDataContext.MasterDbVersions.ToList().LastOrDefault();
            if (currentMasterDbVersion != null)
                flowProject.FlowMasterDBVersion = currentMasterDbVersion.DbVersionNumber;

            DbVersion currentProjectTemplateDbVersion = this.DbVersions.ToList().LastOrDefault();
            if (currentProjectTemplateDbVersion != null)
                flowProject.FlowProjectTemplateVersion = currentProjectTemplateDbVersion.DbVersionNumber;

            _flowMasterDataContext.SubmitChanges();

            // Add control records (metadata for project export/import

            /*
                        Control_Organization organization =
                            _flowMasterDataContext.Organizations.FirstOrDefault(o => o.OrganizationID == flowProject.OrganizationID);

                        this.Control_Organizations.InsertOnSubmit(organization);

                        Control_Organization studio =
                            _flowMasterDataContext.Organizations.FirstOrDefault(o => o.OrganizationID == flowProject.StudioID.Value);

                        this.Control_Organizations.InsertOnSubmit(studio);

                        Control_User user =
                            _flowMasterDataContext.Users.FirstOrDefault(u => u.UserID == flowProject.OwnerUserID);

                        this.Control_Users.InsertOnSubmit(user);

                        Control_FlowProject project = flowProject;

                        this.Control_FlowProjects.InsertOnSubmit(project);
            */

            int projectTemplateID = projectTemplate.ProjectTemplateID;
            flowProject.FlowProjectTemplateGuid = projectTemplate.ProjectTemplateGuid;
            flowProject.FlowProjectTemplateShortDescription = projectTemplate.ShortDescription;

            // Insert the image types
            var imageTypes =
                from i in projectTemplate.ProjectTemplateImageTypes
                where i.ProjectTemplateID == projectTemplateID
                select i;

            foreach (ProjectTemplateImageType item in imageTypes)
            {
                ProjectImageType newItem = new ProjectImageType();

                newItem.ProjectImageTypeDesc = item.ProjectTemplateImageTypeDesc;
                newItem.IsExclusive = item.IsExclusive;

                this.ProjectImageTypes.InsertOnSubmit(newItem);
            }

            // Insert the event triggers
            var eventTriggers =
                from e in projectTemplate.ProjectTemplateEventTriggers
                where e.ProjectTemplateID == projectTemplateID
                select e;

            foreach (ProjectTemplateEventTrigger item in eventTriggers)
            {
                ProjectEventTrigger newItem = new ProjectEventTrigger();

                newItem.EventTriggerID = item.EventTriggerID;
                newItem.Active = item.Active;

                this.ProjectEventTriggers.InsertOnSubmit(newItem);
            }

            // Insert organizational unit types

            var organizationalUnitTypes =
                from ou in projectTemplate.ProjectTemplateOrganizationalUnitTypes
                where ou.ProjectTemplateID == projectTemplateID
                select ou;

            foreach (ProjectTemplateOrganizationalUnitType item in organizationalUnitTypes)
            {
                ProjectOrganizationalUnitType newItem = new ProjectOrganizationalUnitType();

                newItem.ProjectOrganizationalUnitTypeID = item.ProjectTemplateOrganizationalUnitTypeID;
                newItem.ProjectOrganizationalUnitTypeDesc = item.ProjectTemplateOrganizationalUnitTypeDesc;
                newItem.ProjectOrganizationalUnitTypeParentID = item.ProjectTemplateOrganizationalUnitTypeParentID;

                this.ProjectOrganizationalUnitTypes.InsertOnSubmit(newItem);
            }

            // Insert subject types

            var subjectTypes =
                from st in projectTemplate.ProjectTemplateSubjectTypes
                where st.ProjectTemplateID == projectTemplateID
                select st;

            foreach (ProjectTemplateSubjectType item in subjectTypes)
            {
                ProjectSubjectType newItem = new ProjectSubjectType();

                newItem.ProjectSubjectTypeID = item.ProjectTemplateSubjectTypeID;
                newItem.ProjectSubjectTypeDesc = item.ProjectTemplateSubjectTypeDesc;
                newItem.ProjectOrganizationalUnitTypeID = item.ProjectTemplateOrganizationalUnitTypeID;
            }

            string commandTemplate = "ALTER TABLE Subject ADD COLUMN {0} {1};";

            // This collection stores a queue of command strings for
            // adding table columns to be executed in a sequential batch
            Collection<string> commandQueue = new Collection<string>();

            // Add the Last Name field
            this.ProjectSubjectFields.InsertOnSubmit(
                new ProjectSubjectField("Last Name", 1, 128, true, false, true, false, true, projectTemplate.SubjectLastNameIsSearchable, true, false)
            );

            commandQueue.Add(
                string.Format(commandTemplate, "LastName", "nvarchar(64)")
            );

            // Add the First Name field
            this.ProjectSubjectFields.InsertOnSubmit(
                new ProjectSubjectField("First Name", 1, 128, true, false, true, false, true, projectTemplate.SubjectFirstNameIsSearchable, true, false)
            );

            commandQueue.Add(
                string.Format(commandTemplate, "FirstName", "nvarchar(64)")
            );

            // Add the TicketCode field
            this.ProjectSubjectFields.InsertOnSubmit(
                new ProjectSubjectField("Ticket Code", 1, 64, true, false, false, false, true, true, true, true)
            );

            commandQueue.Add(
                string.Format(commandTemplate, "TicketCode", "nvarchar(64)")
            );

            // Add the Package Summary field
            this.ProjectSubjectFields.InsertOnSubmit(
                new ProjectSubjectField("Package Summary", 1, 4001, true, false, false, false, true, true, true, true)
            );

            commandQueue.Add(
                string.Format(commandTemplate, "PackageSummary", "nvarchar(256)")
            );

            // Insert the user-assigned subject fields
            if (projectTemplate.ProjectTemplateSubjectFields.Count() > 0)
            {

                foreach (ProjectTemplateSubjectField field in projectTemplate.ProjectTemplateSubjectFields)
                {
                    // Create the field definition and insert in the ProjectSubjectField table
                    ProjectSubjectField newField = new ProjectSubjectField(
                        field.ProjectTemplateSubjectFieldDisplayName,
                        field.FieldDataTypeID,
                        field.FieldDataSize,
                        false,
                        field.IsKeyField,
                        field.IsRequiredField,
                        field.IsScanKeyField,
                        field.IsProminentField,
                        field.IsSearchableField,
                        field.IsShownInEdit,
                        field.IsReadOnly
                    );

                    this.ProjectSubjectFields.InsertOnSubmit(newField);


                    // Specify the SQL data type for the field
                    string dataType;

                    switch (field.FieldDataTypeID)
                    {
                        case 6:
                            dataType = "bit";
                            break;
                        case 2:
                            dataType = "int";
                            break;
                        case 3:
                            dataType = "decimal";
                            break;
                        case 4:
                            dataType = "datetime";
                            break;
                        case 5:
                            dataType = "money";
                            break;
                        case 1:
                            if (field.FieldDataSize < 4000)
                                dataType = "nvarchar(" + field.FieldDataSize + ")";
                            else
                                dataType = "nvarchar(4000)";
                            break;
                        default:
                            dataType = "nvarchar(128)";
                            break;
                    }

                    // Add the create column command to the queue
                    commandQueue.Add(
                        string.Format(commandTemplate, newField.ProjectSubjectFieldDesc, dataType)
                    );

                }
            }

            // Connect to the database
            if (_flowSqlCeManager == null)
                _flowSqlCeManager = new SqlCeManager(flowProject.ConnectionString);

            // Execute the command batch
            _flowSqlCeManager.ExecuteCommandBatch(commandQueue);


            this.SubmitChanges();
        }

        public void addMoreColumnsToSubjectFields()
        {

            string commandTemplate = "ALTER TABLE Subject ADD COLUMN {0} {1};";

            // This collection stores a queue of command strings for
            // adding table columns to be executed in a sequential batch
            Collection<string> commandQueue = new Collection<string>();



            // Add the TicketCode field
            if (!this.ProjectSubjectFields.Any(psf => psf.ProjectSubjectFieldDisplayName == "Ticket Code"))
            {
                this.ProjectSubjectFields.InsertOnSubmit(
                    new ProjectSubjectField("Ticket Code", 1, 64, true, false, false, false, true, true, true, true)
                );

                commandQueue.Add(
                    string.Format(commandTemplate, "TicketCode", "nvarchar(64)")
                );

            }

            if (!this.ProjectSubjectFields.Any(psf => psf.ProjectSubjectFieldDisplayName == "Package Summary"))
            {
                // Add the Package Summary field
                this.ProjectSubjectFields.InsertOnSubmit(
                    new ProjectSubjectField("Package Summary", 1, 4001, true, false, false, false, true, true, true, true)
                );


                commandQueue.Add(
                    string.Format(commandTemplate, "PackageSummary", "nvarchar(256)")
                );


            }

            if (commandQueue.Count > 0)
            {
                // Connect to the database
                if (_flowSqlCeManager == null)
                    _flowSqlCeManager = new SqlCeManager(FlowProject.ConnectionString);

                // Execute the command batch
                _flowSqlCeManager.ExecuteCommandBatch(commandQueue);
            }

            this.SubmitChanges();
        }

        internal void ClearControlTables()
        {
            this.Control_Users.DeleteAllOnSubmit(this.Control_Users);
            this.Control_Organizations.DeleteAllOnSubmit(this.Control_Organizations);
            this.Control_FlowProjects.DeleteAllOnSubmit(this.Control_FlowProjects);

            this.Control_ProductPackageCompositions.DeleteAllOnSubmit(this.Control_ProductPackageCompositions);
            this.Control_ProductPackages.DeleteAllOnSubmit(this.Control_ProductPackages);
            this.Control_FlowCatalogs.DeleteAllOnSubmit(this.Control_FlowCatalogs);

            this.SubmitChangesLocal();
        }

        /// <summary>
        /// Removes all user-defined fields from the Subject database table;
        /// invoked during import when schema replacement option selected
        /// </summary>
        internal void ClearSubjectTableColumns()
        {
            // Ensure the table has no data before wiping columns out
            if (this.SubjectList.Any())
            {
                this.SubjectList.Clear();
                this.SubmitChanges();
            }

            // Queue the SQL commands for dropping the table columns
            Collection<string> commandQueue = new Collection<string>();
            string commandTemplate = "ALTER TABLE Subject DROP COLUMN {0};";

            foreach (ProjectSubjectField field in this.ProjectSubjectFields)
            {
                commandQueue.Add(String.Format(commandTemplate, field.ProjectSubjectFieldDesc));
            }

            // Connect to the database
            if (_flowSqlCeManager == null)
                _flowSqlCeManager = new SqlCeManager(_flowProject.ConnectionString);

            // Execute command batch
            _flowSqlCeManager.ExecuteCommandBatch(commandQueue);
        }


        internal void BackupSDF35(System.Data.SqlServerCe.SqlCeEngine engine, string dbPath)
        {
            SQLCEVersion fileversion = DetermineVersion(dbPath);
            if (fileversion < SQLCEVersion.SQLCE40)
            {
                string BackupFolder = Path.Combine(FlowContext.FlowAppDataDirPath, "BackupSDF35");
                if (!Directory.Exists(BackupFolder))
                    Directory.CreateDirectory(BackupFolder);

                string destFile = Path.Combine(BackupFolder, (new FileInfo(dbPath)).Name);
                if (!File.Exists(destFile))
                    File.Copy(dbPath, destFile);
                try
                {
                    engine.Upgrade();
                }
                catch
                {
                    //do nothing, if this fails its because its already updated
                }
            }

        }


        private enum SQLCEVersion
        {
            SQLCE20 = 0,
            SQLCE30 = 1,
            SQLCE35 = 2,
            SQLCE40 = 3
        }

        private static SQLCEVersion DetermineVersion(string filename)
        {
            var versionDictionary = new Dictionary<int, SQLCEVersion> 
        { 
            { 0x73616261, SQLCEVersion.SQLCE20 }, 
            { 0x002dd714, SQLCEVersion.SQLCE30},
            { 0x00357b9d, SQLCEVersion.SQLCE35},
            { 0x003d0900, SQLCEVersion.SQLCE40}
        };
            int versionLONGWORD = 0;
            try
            {
                using (var fs = new FileStream(filename, FileMode.Open))
                {
                    fs.Seek(16, SeekOrigin.Begin);
                    using (BinaryReader reader = new BinaryReader(fs))
                    {
                        versionLONGWORD = reader.ReadInt32();
                    }
                }
            }
            catch
            {
                //assume 4.0
                versionLONGWORD = 4000000;
            }
            if (versionDictionary.ContainsKey(versionLONGWORD))
            {
                return versionDictionary[versionLONGWORD];
            }
            else
            {
                throw new ApplicationException("Unable to determine database file version");
            }
        }

        /// <summary>
        /// Adds user-defined fields to the Subject table;
        /// invoked during external data import
        /// </summary>
        internal void CreateSubjectTableColumns()
        {
            // Create queue for SQL add column commands
            Collection<string> commandQueue = new Collection<string>();
            string commandTemplate = "ALTER TABLE Subject ADD COLUMN {0} {1};";

            // Iterate through field definitions and generate add column command
            foreach (ProjectSubjectField field in this.ProjectSubjectFields)
            {
                string dataType;

                switch (field.FieldDataTypeID)
                {
                    case FieldDataType.FIELD_DATA_TYPE_INTEGER:
                        dataType = "int";
                        break;
                    case FieldDataType.FIELD_DATA_TYPE_BOOLEAN:
                        dataType = "bit";
                        break;
                    case FieldDataType.FIELD_DATA_TYPE_DECIMAL:
                        dataType = "decimal";
                        break;
                    case FieldDataType.FIELD_DATA_TYPE_DATETIME:
                        dataType = "datetime";
                        break;
                    case FieldDataType.FIELD_DATA_TYPE_MONEY:
                        dataType = "money";
                        break;
                    case FieldDataType.FIELD_DATA_TYPE_STRING:
                        if (field.FieldDataSize < 4000)
                            dataType = "nvarchar(" + field.FieldDataSize + ")";
                        else
                            dataType = "nvarchar(4000)";
                        break;
                    default:
                        dataType = "nvarchar(128)";
                        break;
                }

                commandQueue.Add(
                    string.Format(commandTemplate, field.ProjectSubjectFieldDesc, dataType)
                );
            }

            // Connect to the database
            if (_flowSqlCeManager == null)
                _flowSqlCeManager = new SqlCeManager(_flowProject.ConnectionString);

            // Execute the command batch
            _flowSqlCeManager.ExecuteCommandBatch(commandQueue);

        }

        #endregion





    }	// END class

}		// END namespace
