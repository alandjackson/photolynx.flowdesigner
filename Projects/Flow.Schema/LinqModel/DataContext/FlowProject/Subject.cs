﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.Linq;
using System.Data.SqlServerCe;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Media.Imaging;
using System.Xml.Linq;

using Flow.Lib;
using Flow.Lib.Database.SqlCe;
using Flow.Lib.Helpers;
using Flow.Schema.Properties;
using Flow.Schema.Utility;

using ICSharpCode.SharpZipLib.Core;
using System.Windows.Threading;
using System.Drawing;


namespace Flow.Schema.LinqModel.DataContext
{
	/// <summary>
	/// Represents a photographic subject
	/// </summary>
	partial class Subject
	{
        public Subject(FlowProjectDataContext flowProjectDataContext)
            : this()
        {
			this.InitializeNew(flowProjectDataContext);
        }

		#region STATIC MEMBERS

		/// <summary>
		/// Creates a new Subject object and sets its subject extended data to an empty data row
		/// </summary>
		/// <param name="flowProjectDataContext"></param>
		public static Subject CreateNewSubject(FlowProject flowProject)
		{
			return new Subject()
			    .InitializeNew(flowProject.FlowProjectDataContext);
		}

		/// <summary>
		/// Creates a new Subject object and assigns data from the supplied DataRow
		/// </summary>
		/// <param name="flowProject"></param>
		/// <param name="dataRow"></param>
		/// <returns></returns>
		public static Subject CreateNewSubject(FlowProject flowProject, DataRow dataRow)
		{
			Subject newSubject = new Subject();
			newSubject.InitializeNew(flowProject.FlowProjectDataContext, dataRow);
			return newSubject;
		}

		#endregion


		#region FIELDS AND PROPERTIES

		/// <summary>
		/// Indexer; return the value of the Subject field specified by the string
		/// </summary>
		/// <param name="fieldName"></param>
		/// <returns>string</returns>
		public string this[string fieldName]
		{
            
			get {
                try
                {
                    if((string)this.SubjectData[fieldName].Value != null)
                        return (string)this.SubjectData[fieldName].Value;
                }
                catch
                {
                    return fieldName;
                }
                return fieldName;
            }
		}

        public SubjectImage PrimaryImage
        {
            get
            {
                return this.SubjectImages.FirstOrDefault(si => si.IsPrimary == true);
            }
        }

        private FlowObservableCollection<OrderImageOption> _orderImageOptionList = null;
        public FlowObservableCollection<OrderImageOption> OrderImageOptionList
        {
            get
            {
                if (_orderImageOptionList == null)
                {
                    _orderImageOptionList = new FlowObservableCollection<OrderImageOption>();
                    foreach (SubjectImage si in this.ImageList)
                    {
                        foreach (OrderImageOption oio in si.OrderImageOptionList)
                        {
                            _orderImageOptionList.Add(oio);
                        }
                    }
                }
                return _orderImageOptionList;
            }
        }
        private FlowObservableCollection<OrderSubjectOrderOption> _subjectOrderOptionList = null;
        public FlowObservableCollection<OrderSubjectOrderOption> SubjectOrderOptionList
        {
            get
            {
                if (_subjectOrderOptionList == null)
                {
                    //_imageList = new FlowObservableCollection<SubjectImage>(this.SubjectImages);
                    _subjectOrderOptionList = new FlowObservableCollection<OrderSubjectOrderOption>();
                    foreach (OrderSubjectOrderOption oio in this.SubjectOrder.OrderSubjectOrderOptions.OrderBy(si => si.SubjectOrderID))
                    {

                        if (oio.FlowCatalogOption == null)
                        {
                            if (this.FlowProjectDataContext.FlowProject.FlowCatalog.FlowCatalogSubjectOrderOptionList.Any(fco => fco.FlowCatalogOptionGuid == oio.FlowCatalogOptionGuid))
                            {
                                oio.FlowCatalogOption = this.FlowProjectDataContext.FlowProject.FlowCatalog.FlowCatalogSubjectOrderOptionList.First(fco => fco.FlowCatalogOptionGuid == oio.FlowCatalogOptionGuid);
                            }
                        }
                        //if(oio.FlowCatalogOption.ImageQuixCatalogOption.ImageQuixCatalogOptionGroup.Label == "Image Options")\

                        if (oio.FlowCatalogOption != null)
                        {
                            _subjectOrderOptionList.Add(oio);
                        }
                    }
                }

                return _subjectOrderOptionList;
            }


        }

        public string TicketCode
        {
            get
            {
                //if(this.SubjectData["Ticket Code"] != null)
                //    return this.SubjectData["Ticket Code"].Value.ToString();

                if (this.GetDataFieldValue("Ticket Code") != null)
                    return this.GetDataFieldValue("Ticket Code");

                return null;
            }
            set
            {
                this.SubjectData["Ticket Code"].Value = value;
            }
        }
		/// <summary>
		/// Specifies that the Subject is provisional (has not yet been saved)
		/// </summary>
		public bool IsNew { get; private set; }

		/// <summary>
		/// Specifies that the Subject data has changed since the last save operation
		/// </summary>
		public bool IsDirty { get; set; }

		/// <summary>
		/// DataContext for accessing the project database
		/// </summary>
        //private FlowProjectDataContext _flowProjectDataContext = null;
        //public FlowProjectDataContext FlowProjectDataContext
        //{
        //    get { return _flowProjectDataContext; }
        //    set { _flowProjectDataContext = value; }
        //}

        private FlowProjectDataContext _flowProjectDataContext = null;
        public FlowProjectDataContext FlowProjectDataContext
        {
            get { return _flowProjectDataContext; }
            set { _flowProjectDataContext = value; }
        }

		/// <summary>
		/// DataRow representing custom field data (SubjectExtended) for the Subject
		/// </summary>
		private DataRow _subjectDataRow = null;
		public DataRow SubjectDataRow { get { 
            if(_subjectDataRow == null)
                _subjectDataRow = _flowProjectDataContext.SubjectDataTable.Rows.Find(this.SubjectID);

            return _subjectDataRow; } }

		/// <summary>
		/// Collection containing field-value pairs (SubjectDatum) representing data
		/// stored in user-defined fields; SubjectDatum items are used for data-binding
		/// to non-grid UI data editing controls
		/// </summary>
		private SubjectDataCollection _subjectData = null;
		public SubjectDataCollection SubjectData
		{
			get
			{
				
                if (_subjectData == null)
					this.SetSubjectData();

				return _subjectData;
			}
		}

        public string PackageSummary { get { return UpdatePackageSummary(); } }
        public string UpdatePackageSummary()
        {
            if (this.IsNew)
                return "No Packages";

                string retVal="";
                try
                {
                    foreach (SubjectOrder so in this.SubjectOrders.Where(subOrd => subOrd.IsMarkedForDeletion == false))
                    {
                        foreach (OrderPackage op in so.OrderPackages)
                        {
                            if (op.IsOnlineOrder)
                                retVal += "*";
                            if(op.Qty > 1)
                                retVal += op.ProductPackageDesc + " (" + op.Qty + "), ";
                            else
                                retVal += op.ProductPackageDesc + ", ";
                        }
                    }
                    if (retVal.Length > 0)
                        retVal = retVal.Remove(retVal.Length - 2, 2);
                    else
                        retVal = "No Packages";

                    this.SubjectData["Package Summary"].Value = retVal.Left(256);
                    //this.FlagForMerge = true;
                    this.SaveSubjectData();
                }
                catch (Exception e)
                {
                    //do nothing
                }
                return retVal;
        }
		/// <summary>
		/// FlowObservableCollection for generated Image Linq-to-SQL entity
		/// </summary>
		private FlowObservableCollection<SubjectImage> _imageList = null;
		public FlowObservableCollection<SubjectImage> ImageList
		{
            get
            {
                if (_imageList == null)
                {
                    //_imageList = new FlowObservableCollection<SubjectImage>(this.SubjectImages);
                    _imageList = new FlowObservableCollection<SubjectImage>();
                    foreach (SubjectImage si in this.SubjectImages.OrderBy(si => si.SubjectImageID))
                    {
                        _imageList.Add(si);
                    }
                }
                int i = 0;
                foreach (SubjectImage si in this.SubjectImages.OrderBy(si => si.SubjectImageID))
                {
                    si.ListIndex = ++i;
                }

                return _imageList;
            }
           
		}

        private FlowObservableCollection<SubjectImage> _imageListNoGroups = null;
        public FlowObservableCollection<SubjectImage> ImageListNoGroups
        {
            get
            {
                if (_imageListNoGroups == null)
                {
                    //_imageList = new FlowObservableCollection<SubjectImage>(this.SubjectImages);
                    _imageListNoGroups = new FlowObservableCollection<SubjectImage>();
                    foreach (SubjectImage si in this.SubjectImages.Where(si => si.IsGroupImage == false).OrderBy(si => si.SubjectImageID))
                    {
                        _imageListNoGroups.Add(si);
                    }
                }
                int i = 0;
                foreach (SubjectImage si in this.SubjectImages.OrderBy(si => si.SubjectImageID))
                {
                    si.ListIndex = ++i;
                }

                return _imageListNoGroups;
            }

        }


        private List<SubjectImage> _filteredImageList = null;
        public List<SubjectImage> FilteredImageList
        {
            get
            {

                _filteredImageList = ImageList.Where(im => im.IsSuppressed != true).OrderBy(si => si.SubjectImageID).ToList();
                int i = 0;
                foreach (SubjectImage si in this._filteredImageList.OrderBy(si => si.SubjectImageID))
                {
                    si.ListIndex = ++i;
                }
                return _filteredImageList;
            }

        }   

		/// <summary>
		/// Indicates if the Subject record has any images assigned
		/// </summary>
		public bool HasAssignedImages
		{
			get { return this.ImageList.HasItems; }
		}

		/// <summary>
		/// Subject name expressed as First, Last for UI display
		/// </summary>
		public string FormalFullName
		{
			get
			{
				return (_subjectDataRow == null)
					? null : _subjectDataRow["Last Name"] + ", " + _subjectDataRow["First Name"];
			}
		}

		/// <summary
		/// Gets the ProductOrder associated with this Subject
		/// </summary>
        private SubjectOrder _subjectOrder = null;
        //public SubjectOrder SubjectOrder { get; private set; }
        public SubjectOrder SubjectOrder
        {
            get
            {
                if (_subjectOrder == null && this.SubjectID > 0)
                {
                    _subjectOrder = this.SubjectOrders.FirstOrDefault(o => !o.IsMarkedForDeletion);

                    if (_subjectOrder == null && this.FlowProjectDataContext.FlowProject.FlowCatalog != null)
                    {
                        _subjectOrder = new SubjectOrder(this.FlowProjectDataContext.FlowProject.FlowCatalog, this.SubjectID);

                        this.FlowProjectDataContext.SubjectOrders.InsertOnSubmit(this.SubjectOrder);
                        this.FlowProjectDataContext.SubmitChanges();
                    }
                    else if (this.FlowProjectDataContext.FlowProject.FlowCatalog != null)
                    {
                        this._subjectOrder.FlowCatalog = this.FlowProjectDataContext.FlowProject.FlowCatalog;
                    }

                }
                //this.SendPropertyChanged("SubjectOrder");
                return _subjectOrder;
            }
            private set { _subjectOrder = value; }
        }

		private string _barcodeFieldValue = null;
		public string BarcodeFieldValue
		{
			get
			{
				if (_flowProjectDataContext.HasScanKeyField.Value && _barcodeFieldValue == null)
					_barcodeFieldValue =
						((string)this.SubjectData[_flowProjectDataContext.ScanKeyField.ProjectSubjectFieldDisplayName].Value)
						.PadLeft(_flowProjectDataContext.FlowProject.BarcodeScanValueLength.Value, '0');
				
				return _barcodeFieldValue;
			}
		}

        //this is no longer use, replaced by TicketCode
        private SubjectTicket _subjectTicket = null;
        private SubjectTicket SubjectTicket
        {
            get
            {

                if (_subjectTicket == null && _flowProjectDataContext != null && _flowProjectDataContext.SubjectTickets.Where(st => st.SubjectID == this.SubjectID).Count() > 0)
                    _subjectTicket = _flowProjectDataContext.SubjectTickets.Where(st => st.SubjectID == this.SubjectID).First();
                return _subjectTicket;
            }
        }

		#endregion


		/// <summary>
		/// Invoked when Subject data is modified through the UI;
		/// marks the Subject "dirty" and notifies listeners that data has changed
		/// </summary>
		private void SubjectData_SubjectDatumChanged(object sender, EventArgs<SubjectDatum> e)
		{
			// If the name fields have changed, ensure UI changes any full name display
			if (e.Data.Field == "Last Name" || e.Data.Field == "First Name")
				this.SendPropertyChanged("FormalFullName");

			// Propagate change to the managing ADO.NET table
            if (!this.IsNew)
			{
				_subjectDataRow[e.Data.Field] = e.Data.Value;

                if(e.Data.Field != "Package Summary")
				    this.IsDirty = e.Data.IsDirty;
			}

			// Notify listeners of a change to Subject data
			this.SendPropertyChanged("SubjectData");
		}

        public void ForcePropertyChange(string propertyName)
        {
            this.SendPropertyChanged(propertyName);
        }

        public void RefreshImageList()
        {
            this._imageList = null;
          
            this.SendPropertyChanged("ImageList");
        }
        public void RefreshSubjectImages()
        {

            foreach (SubjectImage si in this.ImageList)
                si.RefreshImage();

        }
		/// <summary>
		/// Ensures that only a single image assigned to the subject is marked as primary
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		internal void SubjectImage_PrimaryImageAssign(object sender, EventArgs<SubjectImage> e)
		{
			foreach(SubjectImage subjectImage in this.ImageList.Where(i => i.IsPrimary))
			{
				subjectImage.IsPrimary = false;
			}
		}

		/// <summary>
		/// Ensures that an exclusive image type has only a single assignment (per subject)
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		internal void SubjectImageType_ExclusiveImageTypeAssign(object sender, EventArgs<SubjectImageType> e)
		{
			IEnumerable<SubjectImageType> exclusiveImageTypes = this.ImageList.SelectMany(
				i => i.SubjectImageTypeList.Where(t => t.ProjectImageTypeID == e.Data.ProjectImageTypeID)
			);

			foreach (SubjectImageType imageType in exclusiveImageTypes)
			{
				imageType.IsAssigned = false;
			}
		}


		#region METHODS

		partial void OnLoaded()
		{
			
		}

		/// <summary>
		/// Gives the Subject a reference to the project datacontext and assigns
		/// the associated DataRow from the managing ADO.NET DataTable
		/// </summary>
		/// <param name="flowProjectDataContext"></param>
		internal void Initialize(FlowProjectDataContext flowProjectDataContext)
		{



			_flowProjectDataContext = flowProjectDataContext;

            // SPEED UP - try taking this out to speed up project load
			//this.SetProductOrder(flowProjectDataContext.FlowProject.FlowCatalog);

			// Find the managing row for the Subject
			_subjectDataRow = _flowProjectDataContext.SubjectDataTable.Rows.Find(this.SubjectID);

            // SPEED UP - try taking this out to speed up project load
            //this.SetSubjectData();

            if (this.FlowProjectDataContext.NeedPackageSummaryAndTicketCodeUpdated)
            {
                this.setTicketCode();
                this.UpdatePackageSummary();
                this.SaveSubjectData(true);
            }
			// Initialize the ImageList property by invoking the accessor
//			if (this.ImageList == null) { }
		}

        private void setTicketCode()
        {

            if (this.TicketCode == null || this.TicketCode.Length == 0)
            {
                if (this.SubjectTicket == null)
                    this.TicketCode = TicketGenerator.GenerateTicketString(8);
                else
                    this.TicketCode = this.SubjectTicket.TicketID;
               
                
            }
        }

		/// <summary>
		/// Gives the new (provisional) Subject a reference to the project datacontext and creates
		/// a blank DataRow for the managing ADO.NET DataTable; this is invoked when the Subject
		/// object/record is created from a source other than the Xceed datagrid
		/// </summary>
		/// <param name="subjectExtendedDataTable"></param>
		internal Subject InitializeNew(FlowProjectDataContext flowProjectDataContext)
		{
            this.InitializeNew(flowProjectDataContext, null);
            return this;
		}

		/// <summary>
		/// Gives the new (provisional) Subject a reference to the project datacontext and assigns
		/// the supplied DataRow
		/// </summary>
		/// <param name="subjectExtendedDataTable"></param>
		internal Subject InitializeNew(FlowProjectDataContext flowProjectDataContext, DataRow dataRow)
		{
            _flowProjectDataContext = flowProjectDataContext;

            this.DateCreated = DateTime.Now;
            this.SubjectGuid = Guid.NewGuid();
			this.IsNew = true;

			// Copy data from the DataRow to the SubjectDataCollection
            this.SetSubjectData();

            return this;
		}

		/// <summary>
		/// Copies data in the managing ADO.NET DataRow to the SubjectDataCollection
		/// </summary>
		private void SetSubjectData()
		{
            DateTime a = DateTime.Now;
			_subjectData = new SubjectDataCollection();

            if (_flowProjectDataContext == null)
                return;
			if (!this.IsNew && _subjectDataRow == null)
				_subjectDataRow = _flowProjectDataContext.SubjectDataTable.Rows.Find(this.SubjectID);
            DateTime b = DateTime.Now;
			// Iterate through the user-defined fields and add an associated SubjectDatum item
			// to the collection
            
            foreach (ProjectSubjectField field in _flowProjectDataContext.ProjectSubjectFields)
            {
                if (field.SubjectFieldDataType == FieldDataType.FIELD_DATA_TYPE_BOOLEAN)
                {
                    _subjectData.Add(
                        field,
                        (this.IsNew)
                            ? 0
                            : SubjectDataRow[field.ProjectSubjectFieldDisplayName]
                    );
                }
                else
                {
                    _subjectData.Add(
                        field,
                        (this.IsNew)
                            ? null
                            : SubjectDataRow[field.ProjectSubjectFieldDisplayName]
                    );
                }

            }

            DateTime c = DateTime.Now;
         
			// Load the images assigned to the Subject
			//if (!this.IsNew && this.ImageList == null) { };

			// Add an event handler to the collection's SubjectDatumChanged event to notify the
			// Subject object that some data item has been modified
			_subjectData.SubjectDatumChanged += new EventHandler<EventArgs<SubjectDatum>>(SubjectData_SubjectDatumChanged);

			//			this.PropertyChanged(this, new PropertyChangedEventArgs("SubjectData"));
            DateTime d = DateTime.Now;
			this.IsDirty = false;

            int T = (b - a).Milliseconds;
            int U = (c - b).Milliseconds;
            int V = (d - c).Milliseconds;
            return;
		}

        public String GetDataFieldValue(string fieldName)
        {
            if (SubjectDataRow == null)
            {
                if (this.SubjectData[fieldName] != null)
                {
                    if (this.SubjectData[fieldName].Value == null)
                        return "";
                    return this.SubjectData[fieldName].Value.ToString();
                }
            }
            try
            {
                return SubjectDataRow[fieldName].ToString();
            }
            catch
            {
                return "";
            }
        }
		public void Save()
		{
			this.Save(null, true);
		}

        public void Save(bool updateModTime)
        {
            this.Save(null, updateModTime);
        }
		/// <summary>
		/// Saves all changes introduced since the last save operation
		/// </summary>
        public void Save(DataRow xceedDataRow)
        {
            this.Save(xceedDataRow, true);
        }
        public void Save(DataRow xceedDataRow, bool updateModTime)
		{

            if(string.IsNullOrEmpty(this.SubjectData["First Name"].ToString()) || string.IsNullOrEmpty(this.SubjectData["Last Name"].ToString()))
            {
                 
                return;
            }

			if (this.IsNew)
			{
				this.DateCreated = DateTime.Now;

                
                object[] temp = xceedDataRow != null ? xceedDataRow.ItemArray : null;

                //if (_flowProjectDataContext.Subjects.Count(s => s.SubjectGuid == this.SubjectGuid) == 0)
                //{
                try
                {
                    _flowProjectDataContext.SubjectList.Add(this);
                    if (!_flowProjectDataContext.SubjectList.HasItems)
                        _flowProjectDataContext.SubjectList.Add(this);
                }
                catch
                {
                }
                //}

                _flowProjectDataContext.SubmitChanges();

                if (temp != null)
                    xceedDataRow.ItemArray = temp;

				SaveSubjectData(xceedDataRow);
				this.IsNew = false;
                setTicketCode();
                this.SetProductOrder(_flowProjectDataContext.FlowProject.FlowCatalog);

                // Find the managing row for the Subject
               
                _flowProjectDataContext.SubmitChanges();

                _subjectDataRow = _flowProjectDataContext.SubjectDataTable.Rows.Find(this.SubjectID);

                this.FlowProjectDataContext.FlowProject.UpdateProjectSubjectFieldListForSearch();
               
			}
			else if (this.IsDirty)
			{
                if (updateModTime)
				    this.DateModified = DateTime.Now;

				_flowProjectDataContext.SubmitChanges();

				// Store Subject data modifications in database via ADO.NET
				_flowProjectDataContext.Update(this);
			}

			this.IsDirty = false;
            
		}




		/// <summary>
		/// Saves the custom subject data
		/// </summary>

		internal void SaveSubjectData()
		{
			this.SaveSubjectData(null, true);
		}

		internal void SaveSubjectData(bool withCommit)
		{
			this.SaveSubjectData(null, withCommit);
		}

		internal void SaveSubjectData(DataRow xceedDataRow)
		{
			this.SaveSubjectData(xceedDataRow, true);
		}
		
		internal void SaveSubjectData(DataRow xceedDataRow, bool withCommit)
		{
            if (this.SubjectData["First Name"].Value == null || this.SubjectData["Last Name"].Value == null || string.IsNullOrEmpty(this.SubjectData["First Name"].Value.ToString()) || string.IsNullOrEmpty(this.SubjectData["Last Name"].Value.ToString()))
            {
                return;
            }

			if (this.IsNew)
			{
				// If the Subject was created using Flow custom controls, create a new row
				// from the Subject DataTable and populate the row from the SubjectDataCollection
				if (xceedDataRow == null)
				{
					_subjectDataRow = _flowProjectDataContext.SubjectDataTable.NewRow();
					_subjectDataRow["SubjectID"] = this.SubjectID;

					SynchSubjectDataRowFromData();

                    try
                    {
                        _flowProjectDataContext.SubjectDataTable.Rows.Add(_subjectDataRow);
                    }
                    catch (Exception e)
                    {
                        
                        throw new Exception("Error, some of the data is missing a required field.\nThe data did not import correctly\n\n" + e.Message, e);
                    }
				}
				// If the Subject was created using the Xceed datagrid, populate the
				// SubjectDataCollection from the supplied row (xceedDataRow)
				else
				{
					if (_flowProjectDataContext.SubjectDataTable == null) { };
		
					_subjectDataRow = xceedDataRow;
					_subjectDataRow["SubjectID"] = this.SubjectID;
					
					SynchSubjectDataFromDataRow();
                    
				}
			}

			if (withCommit)
			{
				// Store Subject data modifications in database via ADO.NET
				_flowProjectDataContext.Update(this);

				// Modify all fallback (edit cancel) values to match the saved value
				this.SubjectData.CommitChanges();
			}

			this.IsNew = false;
		}

        public void DeleteOrders()
        {
            foreach (SubjectOrder so in this.SubjectOrders)
            {
                so.RefreshOrderDetails();
                foreach (OrderPackage oPackage in so.OrderPackageList)
                {
                    foreach (OrderProduct oProduct in oPackage.OrderProducts)
                    {
                        foreach (OrderProductOption oPOption in oProduct.OrderProductOptions)
                        {
                            _flowProjectDataContext.OrderProductOptions.DeleteOnSubmit(oPOption);
                            //_flowProjectDataContext.SubmitChanges();
                        }

                        foreach (OrderProductNode oPNode in oProduct.OrderProductNodes)
                        {
                            _flowProjectDataContext.OrderProductNodes.DeleteOnSubmit(oPNode);
                           // _flowProjectDataContext.SubmitChanges();
                        }
                        _flowProjectDataContext.OrderProducts.DeleteOnSubmit(oProduct);
                       // _flowProjectDataContext.SubmitChanges();
                    }
                    _flowProjectDataContext.OrderProducts.DeleteAllOnSubmit(oPackage.OrderProducts);
                    so.RemoveOrderPackage(oPackage, this.FlowProjectDataContext);
                    _flowProjectDataContext.OrderHistories.DeleteAllOnSubmit(oPackage.OrderHistories);
                    _flowProjectDataContext.OrderPackages.DeleteOnSubmit(oPackage);
                    //_flowProjectDataContext.SubmitChanges();
                    
                }
                if(so.OrderPackages.Count == 0)
                    _flowProjectDataContext.SubjectOrders.DeleteOnSubmit(so);
              
            }
            _flowProjectDataContext.SubmitChanges();
            //if (this.SubjectOrders != null && this.SubjectOrders.Count > 0)
            //{
            //    _flowProjectDataContext.SubjectOrders.DeleteAllOnSubmit(this.SubjectOrders);
            //    _flowProjectDataContext.SubmitChanges();
            //}

            //if(this.SubjectOrder != null)
            //    _flowProjectDataContext.SubjectOrders.DeleteOnSubmit(this.SubjectOrder);
           
        }
		/// <summary>
		/// 
		/// </summary>
		public void PrepareDelete()
		{
			this.IsDirty = false;

            //this.SubjectOrder = null;
            //this.SubjectOrders = null;
			//this.ClearOrder();

            if (this.SubjectTicket != null)
                _flowProjectDataContext.SubjectTickets.DeleteOnSubmit(this.SubjectTicket);

            this.DeleteSubjectImages();
            if(this.SubjectDataRow != null)
			    _flowProjectDataContext.SubjectDataTable.Rows.Remove(this.SubjectDataRow);
		}

		/// <summary>
		/// Cancels any data modifications since last save operation
		/// </summary>
		public void Revert()
		{
			this.SubjectData.Revert();
		}

		/// <summary>
		///  Updates the SubjectData collection when the SubjectDataRow is modified;
		///  such modifications emanate from the Xceed datagrid
		/// </summary>
		internal void SynchSubjectDataFromDataRow()
		{
			foreach (ProjectSubjectField field in _flowProjectDataContext.ProjectSubjectFields)
			{
				if (_subjectData[field.ProjectSubjectFieldDisplayName].Value != _subjectDataRow[field.ProjectSubjectFieldDisplayName])
					_subjectData[field.ProjectSubjectFieldDisplayName].Value = _subjectDataRow[field.ProjectSubjectFieldDisplayName];
			}
		}

		/// <summary>
		///  Updates the SubjectData collection when the SubjectDataRow is modified;
		///  such modifications emanate from the Xceed datagrid
		/// </summary>
		internal void SynchSubjectDataRowFromData()
		{
			foreach (ProjectSubjectField field in _flowProjectDataContext.ProjectSubjectFields)
			{
                try
                {
                    if (_subjectDataRow[field.ProjectSubjectFieldDisplayName] != _subjectData[field.ProjectSubjectFieldDisplayName].Value)
                        _subjectDataRow[field.ProjectSubjectFieldDisplayName] =
                            (_subjectData[field.ProjectSubjectFieldDisplayName].Value == null)
                                ? DBNull.Value
                                : _subjectData[field.ProjectSubjectFieldDisplayName].Value
                        ;
                }
                catch (Exception e)
                {
                    
                    throw new Exception("error adding a record to the Subject Table, _subjectData items = " + _subjectData.Count() + " and _subjectDataRow columns = " + _subjectDataRow.Table.Columns.Count, e);
                }
			}
		}

		/// <summary>
		/// Assigns an image reference to the Subject and returns the formatted filename
		/// </summary>
		/// <param name="imageFileName"></param>

        public string AssignImage(string imageFileName, string LoginID, bool auto8x10)
        {
            return AssignImage(imageFileName, false, LoginID, auto8x10);
        }

		public string AssignImage(string imageFileName, bool isOrphanedImage, string loginID, bool auto8x10)
		{
			// Generate a new Image object and assign properties
			SubjectImage image = new SubjectImage(this);
            image.SourceComputer =  String.IsNullOrEmpty(loginID) ? System.Environment.MachineName : loginID;
            image.SubjectID = this.SubjectID;
            image.ImageFlagged = false;
			image.FlowProjectGuid = _flowProjectDataContext.FlowProject.FlowProjectGuid;
            this.FlagForMerge = true;
            if (isOrphanedImage)
            {
                image.ImageFileName = imageFileName;
            }
            else
            {
                if(this.FlowProjectDataContext.FlowMasterDataContext.PreferenceManager.Capture.ResetImageNumberingPerSubject)
                {
                    FlowProjectDataContext.ProjectRecord.ImageSequenceID = 0;
                    List<string> ExistingNumbers = new List<string>();
                    foreach (SubjectImage si in this.SubjectImages)
                    {
                        if (si.IsGroupImage) continue;
                        ExistingNumbers.Add(si.ImageFileName.Substring(si.ImageFileName.Length - 9, 5));
                    }
                    if (ExistingNumbers.Count > 0)
                    {
                        ExistingNumbers.Sort();
                        int lastnumber = 0;
                        if(Int32.TryParse(ExistingNumbers.Last(),out lastnumber))
                            FlowProjectDataContext.ProjectRecord.ImageSequenceID = lastnumber;
                    }


                }
                image.ImageFileName = _flowProjectDataContext.FlowProject.GetFormattedImageFileName(this, imageFileName);
                while (File.Exists(Path.Combine(this.FlowProjectDataContext.FlowProject.ImageDirPath, image.ImageFileName)) || this.FlowProjectDataContext.SubjectImages.Any(si => si.ImageFileName == image.ImageFileName))
                    image.ImageFileName = _flowProjectDataContext.FlowProject.GetFormattedImageFileName(this, imageFileName);
            }
			image.ImageOriginalFileName = imageFileName;
            image.DateAssigned = DateTime.Now;
            image.Subject = this;
			// Due to a limitation of LINQ-to-SQL, items of child collections in related entities
			// must also be added to the entity exposed by the managing datacontext

            // Add to the datacontext entity
            if(!_flowProjectDataContext.ImageList.Contains(image))
                _flowProjectDataContext.ImageList.Add(image);

            this.FlowProjectDataContext.SubmitChanges();

			// Add to the local child collection
            if (!this.ImageList.Contains(image))
            {
                //this._imageList.Add(image);

                this._imageList = null;
                this.SendPropertyChanged("ImageList");
            }

            
			image.PrimaryImageAssign += new EventHandler<EventArgs<SubjectImage>>(SubjectImage_PrimaryImageAssign);

			image.IsPrimary = true;

            if (this.FlowProjectDataContext.FlowProject.IsGreenScreen == true)
                image.GreenScreenSettings = this.FlowProjectDataContext.FlowProject.DefaultGSxml;

			this.IsDirty = true;
			this.Save();

            this._flowProjectDataContext.FlowProject.UpdateGalleryCounts();

            //if (this.FlowProjectDataContext != null && this.FlowProjectDataContext.FlowProject != null)
            //    this.FlowProjectDataContext.FlowProject.RefreshHasMissingImages();
            if(ImageList.Count > 0)
                this.SendPropertyChanged("ImageList");

            

			return image.ImagePath;
		}

        public string AssignGroupImage(GroupImage img, string loginID)
        {
            //if this group image has already been assigned, dont do it again.
            if (this.SubjectImages.Any(si => si.IsGroupImage == true && si.GroupImageGuid == img.GroupImageGuid))
                return this.SubjectImages.First(si => si.IsGroupImage == true && si.GroupImageGuid == img.GroupImageGuid).ImagePath;

            // Generate a new Image object and assign properties
            SubjectImage image = new SubjectImage(this);
            image.ImageFlagged = false;
            image.SourceComputer = String.IsNullOrEmpty(loginID) ? System.Environment.MachineName : loginID;
            image.SubjectID = this.SubjectID;
            image.FlowProjectGuid = _flowProjectDataContext.FlowProject.FlowProjectGuid;
            image.ImageFileName = img.ImageFileName;
            image.ImageOriginalFileName = img.ImageOriginalFileName;
            image.DateAssigned = DateTime.Now;
            image.Subject = this;
            image.GroupImageGuid = img.GroupImageGuid;
            image.CropL = img.CropL != null ? (double)img.CropL : 0;
            image.CropT = img.CropT != null ? (double)img.CropT : 0;
            image.CropW = img.CropW != null ? (double)img.CropW : 0;
            image.CropH = img.CropH != null ? (double)img.CropH : 0;


            // Due to a limitation of LINQ-to-SQL, items of child collections in related entities
            // must also be added to the entity exposed by the managing datacontext

            // Add to the datacontext entity
            //_flowProjectDataContext.ImageList.Add(image);
            _flowProjectDataContext.SubjectImages.InsertOnSubmit(image);

            //this.RefreshImageList();

            //image.PrimaryImageAssign += new EventHandler<EventArgs<SubjectImage>>(SubjectImage_PrimaryImageAssign);

            //only assign as primary if there are no other picures
            //if (this.ImageList.Count == 1)
            //{
            //    image.IsPrimary = true;
            //}


            try
            {
                this.IsDirty = true;
                this.Save();
            }
            catch (Exception e)
            {
                //ignore, no need to run Save();
            }

            //if (this.FlowProjectDataContext != null && this.FlowProjectDataContext.FlowProject != null)
            //    this.FlowProjectDataContext.FlowProject.RefreshHasMissingImages();


            this.RefreshImageList();

            return image.ImagePath;

        }

		/// <summary>
		/// Removes the Image record (identified by filename) assigned to the subject
		/// </summary>
		/// <param name="imageFileName">Filename of the assigned image</param>
		public void DetachImage(string imageFileName)
		{
			SubjectImage deletionImage =
				_flowProjectDataContext.SubjectImages.FirstOrDefault(i => i.SubjectID == this.SubjectID && i.ImageFileName == imageFileName);

			if (deletionImage != null)
			{

               

               
               

                //foreach (SubjectOrder so in this.SubjectOrders)
                //{
                //    if (so != null)
                //    {
                //        SubjectImage replacementImage = this.ImageList.FirstOrDefault();
                //        so.ReplaceUnassignedImage(deletionImage, replacementImage);
                //    }
                //}



                List<OrderProductNode> opnodes = deletionImage.OrderProductNodes.ToList();

                if (opnodes.Count > 0)
                {
                    //if (this.ImageList.Count == 1)
                    {
                        //there are order, but no images to assign to the order, so must delete the orders
                        foreach (SubjectOrder order in this.SubjectOrders)
                        {
                            foreach (OrderPackage orderPackage in order.OrderPackageList)
                            {
                                //
                                //We are supposed to remove this image, but it has a package, and there are no other subject images
                                //
                                //order.RemoveOrderPackage(orderPackage, this.FlowProjectDataContext);
                                return;//not going to delete this image because there is an order on it.
                            }
                        }
                    }
                }

                // Remove from the local child collection
                try
                {
                    this.ImageList.Remove(deletionImage);
                }
                catch
                {
                    //its okay if this fails, we will clear the list anyway
                }


                this.Save();

                if (deletionImage.SubjectImageTypes != null)
                {
                    for (int i = 0; i < deletionImage.SubjectImageTypes.Count(); i++)
                    {
                        _flowProjectDataContext.SubjectImageTypes.DeleteOnSubmit(deletionImage.SubjectImageTypes[i]);
                    }

                    deletionImage.SubjectImageTypes.Clear();
                    deletionImage.SubjectImageTypes = null;

                    //_flowProjectDataContext.SubmitChanges();
                }
				
                this.Save();

				// Remove from the datacontext entity
                if (_flowProjectDataContext.SubjectImages.Contains(deletionImage))
                    _flowProjectDataContext.SubjectImages.DeleteOnSubmit(deletionImage);
                if (_flowProjectDataContext.ImageList.Contains(deletionImage))
				    _flowProjectDataContext.ImageList.Remove(deletionImage);

                deletionImage.DisposeImages();

				this.IsDirty = true;
				this.Save();

                if (this.FlowProjectDataContext != null && this.FlowProjectDataContext.FlowProject != null)
                    this.FlowProjectDataContext.FlowProject.RefreshHasMissingImages();

                if (this.ImageList.Count(i => i.IsPrimary) == 0)
                {
                    foreach (SubjectImage si in this.ImageList.Reverse())
                    {

                        if (si.IsGroupImage)
                        {
                            if (this.ImageList.Count(i => i.IsGroupImage == false) == 0)
                            {//only assign group image as primary if there are no non group image photos
                                si.IsPrimary = true;
                                break;
                            }
                        }
                        else
                        {
                            si.IsPrimary = true;
                            break;
                        }

                    }
                    _flowProjectDataContext.SubmitChanges();
                }

                if (!_flowProjectDataContext.DeletedSubjectImages.Any(ds => ds.SubjectImageGuid == deletionImage.SubjectImageGuid))
                {
                    DeletedSubjectImage dsi = new DeletedSubjectImage();
                    dsi.DeleteDate = DateTime.Now;
                    dsi.SubjectImageGuid = deletionImage.SubjectImageGuid;
                    this.FlowProjectDataContext.DeletedSubjectImages.InsertOnSubmit(dsi);
                }

                this._imageList = null;
                SendPropertyChanged("ImageList");

			}
		}

		/// <summary>
		/// Removes the Image records (identified by filename) assigned to the subject
		/// </summary>
		/// <param name="imageList">List of image filenames to detach</param>
		public void DetachImage(IEnumerable<string> imageList)
		{
			foreach (string imageFileName in imageList)
			{
				this.DetachImage(imageFileName);
			}

			this.IsDirty = true;
			this.Save();
		}

		/// <summary>
		/// Removes all image associations from the Subject record (deletes from Image table)
		/// </summary>
		internal void DeleteSubjectImages()
		{
            while (this.SubjectImages.Count > 0)
            {
                SubjectImage thisImage = this.SubjectImages[0];

                if (thisImage.OrderImageOptions.Count > 0)
                {
                    foreach (OrderImageOption o in thisImage.OrderImageOptionList)
                    {
                        this.RemoveCatalogOption(o);
                    }
                }

                DetachImage(thisImage.ImageFileName);
                FileInfo imageFile = new FileInfo(thisImage.ImagePath);

                string newImageFilePath = Path.Combine(
                    this.FlowProjectDataContext.FlowMasterDataContext.PreferenceManager.Capture.HotFolderDirectory,
                    thisImage.ImageOriginalFileName
                );

                IOUtil.MoveFile(thisImage.ImagePath, newImageFilePath);
            }
            this.Save();
			// Notify listeners that the image assignments have been cleared
			_flowProjectDataContext.OnSubjectImagesRemoved(this.SubjectImages);

			// Due to a limitation of LINQ-to-SQL, items of child collections in related entities
			// must also be added to the entity exposed by the managing datacontext

			// Remove from the datacontext entity
			_flowProjectDataContext.SubjectImages.DeleteAllOnSubmit(_flowProjectDataContext.SubjectImages.Where(i => i.SubjectID == this.SubjectID));

			// Remove from the local child collection
			this.ImageList.Clear();
		}

		/// <summary>
		/// Adds a "record" XElement to the supplied "records" XElement (order submission)
		/// </summary>
		/// <param name="productDataElement"></param>
		/// <param name="imageFiles"></param>
        public void SupplyOrderProjectData(XElement recordsElement, List<ZipFileMapping> imageFiles, bool imagesFromLayoutDesigner, List<OrderPackage> applicableOrderPackages, bool renderGreenScreen, bool useOriginalFileNames)
		{
			XElement recordElement = new XElement("record");
            //recordElement.AddNewXAttribute("pk", this.SubjectID);
            //recordElement.AddNewXAttribute("SubjectID", this.SubjectID);
            recordElement.AddNewXAttribute("pk", this.TicketCode);
            recordElement.AddNewXAttribute("SubjectID", this.TicketCode);

            string groupImageDesc = "";
            if (this.ImageList.Count(i => i.IsGroupImage) > 0)
            {
                GroupImage gi = this.ImageList.First(i => i.IsGroupImage).GroupImage;
                if (gi.GroupImageDesc != null && gi.GroupImageDesc.Length > 0)
                    groupImageDesc = gi.GroupImageDesc;
                else
                    groupImageDesc = gi.ImageFileName;
            }

			// Add an attribute for each field-value
			foreach (SubjectDatum datum in this.SubjectData)
			{
                if (datum.Value == null)
                    datum.Value = null;
                // datum.Value = new object();
                
                //if this is a DateTime and the Time is 00:00:00, then just return the data
                if ((datum.Value.GetType() == typeof(DateTime)) && (((DateTime)datum.Value).TimeOfDay.TotalSeconds == 0))
                    recordElement.AddNewXAttribute(datum.FieldDesc, ((DateTime)datum.Value).Date.ToShortDateString());
                else
                    recordElement.AddNewXAttribute(datum.FieldDesc, datum.Value);
              
			}

            recordElement.AddNewXAttribute("GroupImage", groupImageDesc);


            if (applicableOrderPackages == null)
            {
                //for (int i = 0; i < 11; i++)
                //    recordElement.AddNewXAttribute("PackageOrder" + i, "");
            }
            else
            {
                int i = 0;
                string orderSummary = "";
                foreach (OrderPackage thisPackage in this.SubjectOrder.OrderPackageList)
                {
                    //if (i == 10) break;
                    if (applicableOrderPackages.Contains(thisPackage))
                    {
                        i++;
                        //recordElement.AddNewXAttribute("PackageOrder" + i, thisPackage.ProductPackageDesc);
                        if (applicableOrderPackages.Contains(thisPackage))
                        {
                            orderSummary = orderSummary + thisPackage.ProductPackageDesc + ":";
                            foreach (OrderProduct thisProduct in thisPackage.OrderProducts)
                            {
                                orderSummary = orderSummary + "- (" + thisProduct.Quantity + ") - " + thisProduct.OrderProductLabel + ":\n";
                            }
                            orderSummary = orderSummary + "\n";
                        }
                    }
                }
                //while (i < 10)
                //{
                //    i++;
                //    recordElement.AddNewXAttribute("PackageOrder" + i, "");
                //}

                recordElement.AddNewXAttribute("OrderSummary", orderSummary);
            }

            if (imagesFromLayoutDesigner)
            {
                XElement imageElement = new XElement("image");
                imageElement.AddNewXAttribute("filename", this.SubjectGuid.Value.ToString().Replace("-", "").Left(26) + ".jpg");

                imageElement.AddNewXAttribute("crop-x", 0);
                imageElement.AddNewXAttribute("crop-y", 0);
                imageElement.AddNewXAttribute("crop-w", 0);
                imageElement.AddNewXAttribute("crop-h", 0);

                imageElement.AddNewXAttribute("orientation", 0);
                imageElement.AddNewXAttribute("image-option-selections", "");
                recordElement.Add(imageElement);

            }
            else
            {
                List<SubjectImage> images;

                if (imageFiles == null)
                {
                    images = this.ImageList.Distinct().ToList();

                     foreach (SubjectImage si in images)
                        {
                            
                                XElement imageElement = new XElement("image");
                                imageElement.AddNewXAttribute("filename", si.Dp2FileName(useOriginalFileNames));
                                string orderImageOptions ="";
                                if (si.OrderImageOptions.Count > 0)
                                {
                                    foreach (OrderImageOption oio in si.OrderImageOptionList)
                                    {
                                        orderImageOptions += (oio.ResourceURL + ",");
                                    }
                                    if(orderImageOptions.EndsWith(","))
                                        orderImageOptions = orderImageOptions.Substring(0, orderImageOptions.Length - 1);
                                }
                                if (si.CropH < 0 || double.IsNaN(si.CropH))
                                    si.CropH = 0;

                                double _cropX = double.Parse((si.CropL * 100).ToString("N5"));
                                double _cropY = double.Parse((si.CropT * 100).ToString("N5"));
                                double _cropW = double.Parse((si.CropW * 100).ToString("N5"));
                                double _cropH = double.Parse((si.CropH * 100).ToString("N5"));
                                imageElement.AddNewXAttribute("crop-x", (_cropX + (_cropW / 2)));
                                imageElement.AddNewXAttribute("crop-y", (_cropY + (_cropH / 2)));
                                imageElement.AddNewXAttribute("crop-w", _cropW);
                                imageElement.AddNewXAttribute("crop-h", _cropH);

                                if(renderGreenScreen)
                                    imageElement.AddNewXAttribute("orientation", 0);
                                else
                                    imageElement.AddNewXAttribute("orientation", si.Orientation);
                                imageElement.AddNewXAttribute("image-option-selections", orderImageOptions);
                                recordElement.Add(imageElement);
   
                    }

                }
                else
                {
                    // Get a list of the subject images which are assigned to order products
                    // less than 53 seconds

                       // foreach (ZipFileMapping zfp in imageFiles)
                        //{
                            
                           // SubjectImage si = this.FlowProjectDataContext.SubjectImages.First(simg => simg.SubjectImageGuid.ToString().Replace("-","").Contains(zfp.DestinationFileName.Substring(0, 14)));
                            
                            //need a dictionary of current subject's subjectimages mapped to a DestinationFileName (from ZipFileMapping)
                            List<ZipFileMapping> zfms = imageFiles.Where(zfp=> this.SubjectImages.Any(si=>si.SubjectImageGuid.ToString().Replace("-","").Contains(zfp.DestinationFileName.Substring(0, 14)))).ToList();
                            foreach (ZipFileMapping zfp in zfms)
                            {
                                SubjectImage si = this.SubjectImages.First(j => j.SubjectImageGuid.ToString().Replace("-", "").Contains(zfp.DestinationFileName.Substring(0, 14)));
                                XElement imageElement = new XElement("image");
                                imageElement.AddNewXAttribute("filename", zfp.DestinationFileName);
                                string orderImageOptions = "";
                                if (si.OrderImageOptions.Count > 0)
                                {
                                    foreach (OrderImageOption oio in si.OrderImageOptionList)
                                    {
                                        orderImageOptions += (oio.ResourceURL + ",");
                                    }
                                    if (orderImageOptions.EndsWith(","))
                                        orderImageOptions = orderImageOptions.Substring(0, orderImageOptions.Length - 1);
                                }
                                if (si.CropH < 0 || double.IsNaN(si.CropH))
                                    si.CropH = 0;

                                double _cropX = double.Parse((si.CropL * 100).ToString("N5"));
                                double _cropY = double.Parse((si.CropT * 100).ToString("N5"));
                                double _cropW = double.Parse((si.CropW * 100).ToString("N5"));
                                double _cropH = double.Parse((si.CropH * 100).ToString("N5"));
                                imageElement.AddNewXAttribute("crop-x", (_cropX + (_cropW / 2)));
                                imageElement.AddNewXAttribute("crop-y", (_cropY + (_cropH / 2)));
                                imageElement.AddNewXAttribute("crop-w", _cropW);
                                imageElement.AddNewXAttribute("crop-h", _cropH);

                                if (renderGreenScreen)
                                    imageElement.AddNewXAttribute("orientation", 0);
                                else
                                    imageElement.AddNewXAttribute("orientation", si.Orientation);

                                imageElement.AddNewXAttribute("image-option-selections", orderImageOptions);
                                recordElement.Add(imageElement);
                            }
                        //}
                    // Express the image list as a comma-delimited array
                    //recordElement.AddNewXAttribute("Images", String.Join(",",images));
                    }
                }

               

            

            recordElement.AddNewXAttribute("organization_name", this._flowProjectDataContext.FlowProject.Organization.OrganizationName);
            recordElement.AddNewXAttribute("project_name", this._flowProjectDataContext.FlowProject.FlowProjectName);
            recordElement.AddNewXAttribute("project_event", this._flowProjectDataContext.FlowProject.FlowProjectDesc);

            recordsElement.Add(recordElement);
            
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="orderElement"></param>
        internal void SupplyOrderData(XElement orderElement, List<OrderPackage> applicableOrderPackages, bool renderGreenScreen, bool useOriginalFileNames)
		{
            foreach (XElement productElement in this.SubjectOrder.GetOrderXml(applicableOrderPackages, renderGreenScreen, useOriginalFileNames))
			{
				orderElement.Add(productElement);
			}
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="orderElement"></param>
        public void SupplyOrderData(XElement orderElement, int productPrimaryKey, string defaultImageName, ImageQuixCatalog imageQuixCatalog)
        {
            foreach (XElement productElement in this.SubjectOrder.GetOrderXml(productPrimaryKey, defaultImageName, imageQuixCatalog))
            {
                orderElement.Add(productElement);
            }
        }
		/// <summary>
		/// Sets the ProductOrder property, assigning the supplied ProductCatalog
		/// </summary>
		/// <param name="productProgram"></param>
		/// <param name="dataContext"></param>
		public void SetProductOrder(FlowCatalog flowCatalog)
		{

            if (this.SubjectOrder == null && !this.IsNew)
            {
                this.SubjectOrder = this.SubjectOrders.FirstOrDefault(o => !o.IsMarkedForDeletion);

                if (this.SubjectOrder == null)
                {
                    this.SubjectOrder = new SubjectOrder(flowCatalog, this.SubjectID);

                    this.FlowProjectDataContext.SubjectOrders.InsertOnSubmit(this.SubjectOrder);
                    this.FlowProjectDataContext.SubmitChanges();
                }
                else if (flowCatalog != null)
                {
                    this.SubjectOrder.FlowCatalog = flowCatalog;
                }
            }

            this.SendPropertyChanged("SubjectOrder");
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="productCatalogItem"></param>
		public OrderPackage AddOrderPackage(ProductPackage productPackage)
		{
			return this.AddOrderPackage(productPackage, false);
           
		}

        public OrderPackage AddOrderPackage(ProductPackage productPackage, SubjectImage subImage)
        {
            OrderPackage temp = this.AddOrderPackage(productPackage, false, "", subImage);
            return temp;
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="productCatalogItem"></param>
        public OrderPackage AddOrderPackage(ProductPackage productPackage, bool deferSave)
        {
            return this.AddOrderPackage(productPackage, false, "", null);
        }

        public OrderPackage AddOrderPackage(ProductPackage productPackage, bool deferSave, string PackageImageType, SubjectImage subImage)
		 {

             OrderPackage existingPackage = this.SubjectOrder.OrderPackages.FirstOrDefault(p => p.IsOnlineOrder == false && p.LabOrderID == null && p.ProductPackageGuid == productPackage.ProductPackageGuid && (p.FirstImage == subImage || p.FirstImage == this.ImageList.CurrentItem));
             if (existingPackage != null)
             {
                 existingPackage.Qty++;
                 this.UpdatePackageSummary();
                 return existingPackage;
             }


             if (this.SubjectOrder.OrderPackages.Count() == 0)
                 this.SubjectOrder.Directshipment = false;
             OrderPackage op;
            op = this.SubjectOrder.AddOrderPackage(productPackage, PackageImageType, subImage);
            op.Qty = 1;
            this.SendPropertyChanged("SubjectOrder");
            if (!this.SubjectOrders.Contains(this.SubjectOrder))
                this.SubjectOrders.Add(this.SubjectOrder);
            this.UpdatePackageSummary();
            if (!deferSave)
            {   
                this.FlowProjectDataContext.SubmitChanges();
                this.FlowProjectDataContext.FlowMasterDataContext.SubmitChanges();
                this.FlowProjectDataContext.SubmitChanges();
            }
            this.FlowProjectDataContext.UpdateFilteredOrderPackages();
            return op;
            
		}

        public void AddCatalogOption(FlowCatalogOption flowCatalogOption)
        {
            AddCatalogOption(flowCatalogOption, false);
        }
        public void AddCatalogOption(FlowCatalogOption flowCatalogOption, bool deferSave)
        {

            if (flowCatalogOption.IsImageOption)
            {
                this.ImageList.CurrentItem.AddCatalogOption(flowCatalogOption);
            }
            else if (flowCatalogOption.IsSubjectOrderOption)
            {
                this.SubjectOrder.AddCatalogOption(flowCatalogOption);
            }
            this.SubjectOrder.RefreshOrderDetails();
            this.SendPropertyChanged("SubjectOrder");
            if (!this.SubjectOrders.Contains(this.SubjectOrder))
                this.SubjectOrders.Add(this.SubjectOrder);

            if (!deferSave)
            {
                this.FlowProjectDataContext.SubmitChanges();
                this.FlowProjectDataContext.FlowMasterDataContext.SubmitChanges();
            }
        }

        public void RemoveCatalogOption(OrderImageOption orderImageOption)
        {
            RemoveCatalogOption(orderImageOption, true);
        }
        public void RemoveCatalogOption(OrderImageOption orderImageOption, bool addToDelItems)
        {
            
            Guid imageGuid = (Guid)this.SubjectImages.First(si => si.OrderImageOptions.Any(oio => oio.ResourceURL == orderImageOption.ResourceURL)).SubjectImageGuid;
            //this.ImageList.CurrentItem.RemoveCatalogOption(orderImageOption);
            orderImageOption.SubjectImage.RemoveCatalogOption(orderImageOption);
            this.SubjectOrder.RefreshOrderDetails();
            this.SendPropertyChanged("SubjectOrder");

            if (addToDelItems)
            {
                DeletedOrderImageOption del = new DeletedOrderImageOption();
                del.DeleteDate = DateTime.Now;
                del.ResourceURL = orderImageOption.ResourceURL;
                del.SubjectImageGuid = imageGuid;
                this.FlowProjectDataContext.DeletedOrderImageOptions.InsertOnSubmit(del);
            }


        }

        public void RemoveCatalogOption(OrderSubjectOrderOption orderSubjectOrderOption)
        {
            RemoveCatalogOption(orderSubjectOrderOption, true);
        }
        public void RemoveCatalogOption(OrderSubjectOrderOption orderSubjectOrderOption, bool addToDelItems)
        {
            this.SubjectOrder.RemoveCatalogOption(orderSubjectOrderOption);
            this.SubjectOrder.RefreshOrderDetails();
            this.SendPropertyChanged("SubjectOrder");

            if (addToDelItems)
            {
                DeletedSubjectOrderOption del = new DeletedSubjectOrderOption();
                del.DeleteDate = DateTime.Now;
                del.ResourceURL = orderSubjectOrderOption.ResourceURL;
                del.SubjectGuid = this.SubjectGuid;
                this.FlowProjectDataContext.DeletedSubjectOrderOptions.InsertOnSubmit(del);

            }
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="orderPackage"></param>
		public void DuplicateOrderPackage(OrderPackage orderPackage)
		{
			//OrderPackage duplicateOrderPackage = orderPackage.Duplicate();
            AddOrderPackage(orderPackage.ProductPackage, false);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="orderPackage"></param>
		public void RemoveOrderPackage(OrderPackage orderPackage)
		{
			this.SubjectOrder.RemoveOrderPackage(orderPackage, _flowProjectDataContext);
            
			this.FlowProjectDataContext.SubmitChanges();
            this.UpdatePackageSummary();
            if (this.SubjectOrder.OrderPackages.Count() == 0)
                this.SubjectOrder.Directshipment = false;
            this.FlowProjectDataContext.SubmitChanges();
		}

        //public void RefreshImage(string imageFileName)
        //{
        //    SubjectImage refreshImage =
        //        _flowProjectDataContext.SubjectImages.First(i => i.SubjectID == this.SubjectID && i.ImageFileName == imageFileName);

        //    if (refreshImage != null)
        //    {
        //        refreshImage.RefreshImage();
        //    }
            
        //}

		//public void ClearOrder()
		//{
		//    this.ClearOrder(false);
		//}

		/// <summary>
		/// Clears
		/// </summary>
		public void ClearOrder()
		{
			if (this.SubjectOrder == null)
				this.SubjectOrder = this.FlowProjectDataContext.SubjectOrders.FirstOrDefault(o => o.SubjectID == this.SubjectID);

			if (this.SubjectOrder == null)
				return;

			this.SubjectOrder.IsMarkedForDeletion = true;
            this.DeleteOrders();
            
            this.FlowProjectDataContext.SubmitChanges();
			this.SubjectOrder = null;

			this.SetProductOrder(this.FlowProjectDataContext.FlowProject.FlowCatalog);

			this.SendPropertyChanged("SubjectOrder");

            if (this.SubjectOrder.OrderPackages.Count() == 0)
                this.SubjectOrder.Directshipment = false;

            this.FlowProjectDataContext.SubmitChanges();
			//this.SubjectOrder.ClearOrderPackages(_flowProjectDataContext);
			//_flowProjectDataContext.SubjectOrders.DeleteOnSubmit(this.SubjectOrder);
			//this.SubjectOrders.Remove(this.SubjectOrder);
			//this.SubjectOrder = null;
		}

		//public void ClearItemsFromOrder(OrderPackage productPackage)
		//{
		//    this.SubjectOrder.RemoveItem(productPackage, _flowProjectDataContext, true);
		//    this.FlowProjectDataContext.SubmitChanges();
		//}

		#endregion


		#region MERGE MEMBERS

		/// <summary>
		/// Returns a clone of the source Subject object
		/// </summary>
		/// <returns></returns>
		internal Subject Clone(FlowProject rootProject)
		{
			Subject clone = new Subject(rootProject.FlowProjectDataContext);

			clone.FlowProjectGuid = this.FlowProjectGuid;
			clone.SubjectGuid = this.SubjectGuid;
			clone.DateCreated = this.DateCreated;
			clone.DateModified = this.DateModified;

			foreach (SubjectDatum datum in this.SubjectData)
			{
				clone.SubjectData[datum.Field].Value = datum.Value;
			}

			return clone;
		}

		/// <summary>
		/// Imports the elements of the SubjectOrder from the distributed project
		/// </summary>
		/// <param name="subject"></param>
		internal void MergeOrder(Subject subject, FlowCatalog flowCatalog)
		{
			this.SubjectOrder.Merge(subject, flowCatalog, this.FlowProjectDataContext);
		}

		#endregion







        public void ClearGSCache(string GSCache)
        {
            if (!Directory.Exists(GSCache))
                return;

            foreach(SubjectImage si in this.ImageList)
            {
                string imageStart = si.ImageFileName.Substring(0, si.ImageFileName.Length - 4);
                foreach (string f in Directory.GetFiles(GSCache))
                {
                    FileInfo fi = new FileInfo(f);
                    if (fi.Name.StartsWith(imageStart))
                    {
                        File.Delete(Path.Combine(f));
                    }
                }
            }
        }

        internal void UpdateOrderImageOptionList()
        {
            _orderImageOptionList = null;
            this.SendPropertyChanged("OrderImageOptionList");
        }

        public bool HasGroupImage {
            get
            {
                foreach (SubjectImage i in this.SubjectImages)
                    if (i.IsGroupImage) return true;

                return false;
            }
        }
    }	// END class

}	// END namespace
