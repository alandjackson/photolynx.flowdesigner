﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Flow.Lib.Helpers;

namespace Flow.Schema.LinqModel.DataContext
{
	partial class OrderSubjectOrderOption
	{
		public FlowCatalogOption FlowCatalogOption { get; internal set; }
        
        

		private decimal _taxRate = 0;
		internal decimal TaxFactor
		{
			get {
                if (_taxRate == 0 && this.FlowCatalogOption != null)
                    _taxRate = 1 + (this.FlowCatalogOption.FlowCatalog.TaxRate / 100);
                return _taxRate; 
            }
			set { _taxRate = 1 + (value / 100); }
		}
	
		public decimal ExpandedPrice
		{
			get
			{
				if (this.FlowCatalogOption != null)
					return this.FlowCatalogOption.Price;

				return 0;
			}
		}

		public decimal TaxedExpandedPrice
		{
			get
			{
				if (!this.FlowCatalogOption.Taxed)
					return this.ExpandedPrice;

				return (this.FlowCatalogOption.Price * this.TaxFactor);
			}
		}

		public string ExpandedPriceDisplayText
		{
			get
			{
                if (this.FlowCatalogOption == null)
                    return "$0.0";

				return this.FlowCatalogOption.Price.CurrencyFormat("$");
			}
		}

        public string TaxedExpandedPriceDisplayText
        {
            get
            {
                if (this.TaxedExpandedPrice == null)
                    return "$0.0";

                return TaxedExpandedPrice.CurrencyFormat("$");
            }
        }

		public string TaxDisplayText
		{
			get { return "(x " + (this.TaxFactor - 1).ToString() + ") Tax"; }
		}

        public OrderSubjectOrderOption(FlowCatalogOption flowCatalogOption, SubjectOrder subjectOrder)
			: this()
		{
            this.FlowCatalogOption = flowCatalogOption;
            this.SubjectOrder = subjectOrder;
            this.SubjectOrderID = subjectOrder.SubjectOrderID;
            this.ResourceURL = flowCatalogOption.ImageQuixCatalogOption.ResourceURL;

            //this.FlowCatalogOption.ImageQuixCatalogOption.Label

            //this.ProductPackage = productPackage;
            //this.ProductPackageGuid = productPackage.ProductPackageGuid;
            //this.ProductPackageDesc = productPackage.ProductPackageDesc;

            //foreach (ProductPackageComposition composition in productPackage.ProductPackageCompositionList)
            //{
            //    ImageQuixProduct product = composition.ImageQuixProduct;

            //    OrderProduct orderProduct = new OrderProduct(product, subjectOrder);
            //    orderProduct.Quantity = composition.Quantity;

            //    this.OrderProductList.Add(orderProduct);

            //    foreach (ImageQuixOptionGroup optionGroup in product.OptionGroupList.Where(g => g.OptionList.HasItems))
            //    {
            //        orderProduct.OrderProductOptionList.Add(new OrderProductOption(optionGroup));
            //    }
            //}
		}

        //internal OrderPackage Duplicate()
        //{
        //    OrderPackage duplicateOrderPackage = new OrderPackage(this.ProductPackage, this.SubjectOrder);
        //    duplicateOrderPackage.AddDate = DateTime.Now;
        //    return duplicateOrderPackage;
        //}

        //internal void RemoveOrderHistory(FlowProjectDataContext flowProjectDataContext)
        //{
        //    flowProjectDataContext.OrderHistories.DeleteAllOnSubmit(this.OrderHistories);
        //    this.OrderProducts.Clear();
        //}

        //internal void RemovePackageProducts(FlowProjectDataContext flowProjectDataContext)
        //{
        //    foreach (OrderProduct orderProduct in this.OrderProductList)
        //    {
        //        orderProduct.RemoveChildren(flowProjectDataContext);
        //        flowProjectDataContext.OrderProducts.DeleteOnSubmit(orderProduct);
        //    }
        //    flowProjectDataContext.SubmitChanges();
        //    //flowProjectDataContext.OrderProducts.DeleteAllOnSubmit(this.OrderProducts);
        //    //this.OrderProducts.Clear();
        //}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="subjectImage"></param>
        //internal void ReplaceUnassignedImage(SubjectImage deletionImage, SubjectImage replacementImage)
        //{
        //    foreach(OrderProduct orderProduct in this.OrderProductList)
        //        orderProduct.ReplaceUnassignedImage(deletionImage, replacementImage);
        //}


		#region MERGE MEMBERS

        //private Guid _subjectOrderGuid;
        //internal Guid SubjectOrderGuid
        //{
        //    get { return _subjectOrderGuid; }
        //    set
        //    {
        //        _subjectOrderGuid = value;

        //        foreach (OrderProduct orderProduct in this.OrderProducts)
        //        {
        //            orderProduct.OrderPackageGuid = this.ProductPackageGuid;
        //        }
        //    }
        //}

        ///// <summary>
        ///// Returns a clone of the source OrderPackage object
        ///// </summary>
        ///// <returns></returns>
        //internal OrderPackage Clone()
        //{
        //    OrderPackage clone = new OrderPackage();

        //    clone.ProductPackageGuid = this.ProductPackageGuid;
        //    clone.ProductPackageDesc = this.ProductPackageDesc;
        //    clone.AddDate = this.AddDate;

        //    foreach (OrderProduct sourceProduct in this.OrderProductList)
        //    {
        //        OrderProduct rootProduct = sourceProduct.Clone();

        //        clone.OrderProducts.Add(rootProduct);
        //    }

        //    return clone;
        //}

		#endregion

	}
}
