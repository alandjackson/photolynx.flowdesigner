﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flow.Schema.LinqModel.DataContext
{
	partial class Control_FlowCatalog
	{

        public Control_FlowCatalog(FlowProject flowProject)
			: this()
		{
			FlowCatalog assignedCatalog = flowProject.FlowCatalog;

			this.FlowCatalogGuid = assignedCatalog.FlowCatalogGuid.Value;
			this.FlowCatalogDesc = assignedCatalog.FlowCatalogDesc;
			this.FlowCatalogCode = assignedCatalog.FlowCatalogCode;
			this.TaxRate = assignedCatalog.TaxRate;
			this.ImageQuixCatalogPk = assignedCatalog.ImageQuixCatalog.PrimaryKey;

            if (assignedCatalog.ProductPackageList != null)
            {
                foreach (ProductPackage productPackage in assignedCatalog.ProductPackageList)
                {
                    if (productPackage.ProductPackageGuid == new Guid("00000000000000000000000000000000"))
                        productPackage.ProductPackageGuid = Guid.NewGuid();
                    Control_ProductPackage controlProductPackage = new Control_ProductPackage(productPackage, flowProject);
                    flowProject.FlowProjectDataContext.Control_ProductPackages.InsertOnSubmit(controlProductPackage);
                }

                foreach (FlowCatalogOption catOption in assignedCatalog.FlowCatalogOptions)
                {
                    if (flowProject.FlowProjectDataContext.Control_FlowCatalogOptions.Count(fco => fco.FlowCatalogOptionGuid == catOption.FlowCatalogOptionGuid) == 0)
                    {
                        Control_FlowCatalogOption controlFlowCatalogOption = new Control_FlowCatalogOption(catOption, flowProject);
                        flowProject.FlowProjectDataContext.Control_FlowCatalogOptions.InsertOnSubmit(controlFlowCatalogOption);
                    }
                }
            }
		}
	
	}
}
