﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Flow.Lib.Helpers;

namespace Flow.Schema.LinqModel.DataContext
{
	partial class OrderItem
	{
		public ProductPackage ProductPackage { get; internal set; }

		private decimal _taxRate = 0;
		internal decimal TaxFactor
		{
			get { return _taxRate; }
			set { _taxRate = 1 + (value / 100); }
		}
	
		public decimal ExpandedPrice
		{
			get
			{
				if (this.ProductPackage != null)
					return (this.Quantity * this.ProductPackage.Price);

				return 0;
			}
		}

		public decimal TaxedExpandedPrice
		{
			get
			{
				if (!this.ProductPackage.Taxed)
					return this.ExpandedPrice;

//				if (this.ProductPackage != null)
				return (this.Quantity * this.ProductPackage.Price * this.TaxFactor);

//				return 0;
			}
		}

		public string ExpandedPriceDisplayText
		{
			get
			{
				return this.Quantity.ToString() + " @ " + this.ProductPackage.Price.CurrencyFormat("$");
			}
		}

		public string TaxDisplayText
		{
			get { return "(x " + (this.TaxFactor - 1).ToString() + ") Tax"; }
		}

		public OrderItem(ProductPackage productPackage)
			: this()
		{
			this.ProductPackage = productPackage;
			this.Quantity = 1;
			this.ProductPackageID = productPackage.ProductPackageID;
		}

		partial void OnQuantityChanged()
		{
			this.SendPropertyChanged("ExpandedPrice");
			this.SendPropertyChanged("TaxedExpandedPrice");
			this.SendPropertyChanged("ExpandedPriceDisplayText");
		}	

	}
}
