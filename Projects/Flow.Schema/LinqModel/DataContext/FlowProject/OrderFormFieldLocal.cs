﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;

namespace Flow.Schema.LinqModel.DataContext
{
	partial class OrderFormFieldLocal
	{
        public bool IsOptional { get 
        {  
            if (this.IsRequired == true) return false;
            return true;
        } 
        }
	}
}
