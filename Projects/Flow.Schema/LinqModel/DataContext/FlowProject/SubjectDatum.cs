﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.Linq;
using System.Data.SqlServerCe;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Media.Imaging;

using Flow.Lib;
using Flow.Lib.Database.SqlCe;
using Flow.Lib.Helpers;
using Flow.Schema.Properties;
using Flow.Schema.Utility;


namespace Flow.Schema.LinqModel.DataContext
{
	/// <summary>
	/// Represents a field-value for user-defined Subject field;
	/// this is provided to the UI for binding to data editing controls
	/// (with the exception of the Xceed datagrid
	/// </summary>
	public class SubjectDatum : INotifyPropertyChanged, IDataErrorInfo
	{
		#region INotifyPropertyChanged Members

		public event PropertyChangedEventHandler PropertyChanged = delegate { };

		#endregion


		#region FIELDS AND PROPERTIES

        public bool CanUserEdit { get { return !IsLocked; } }
        public bool IsLocked { get; private set; }

		/// <summary>
		/// The name of the user-defined Subject field being represented (ProjectSubjectFieldDisplayName)
		/// </summary>
		public string Field { get; set; }

		/// <summary>
		/// The name of the user-defined Subject field being represented (ProjectSubjectFieldDesc)
		/// </summary>
		public string FieldDesc { get; set; }

		public Type FieldType { get; private set; }

        public int FieldLengthLimit { get; set; }

		public bool IsRequiredField { get; private set; }

		/// <summary>
		/// The active value of the Subject field
		/// </summary>
		private object _value = null;
		public object Value
		{
			get { return _value; }
			set
			{
				object tempValue = (value == null || value.ToString() == String.Empty) ? DBNull.Value : value;

				//if (this.IsRequiredField && tempValue == DBNull.Value)
				//    throw new SystemException(this.Field + " is a required field.");

				if (_value != tempValue)
				{
					_value = tempValue;
					this.PropertyChanged(this, new PropertyChangedEventArgs("Value"));
				}
			}
		}

		public object ComparisonValue
		{
			get { return _value == DBNull.Value ? null : _value; }
		}


		public bool IsValid
		{
			get
			{
				return !(this.IsRequiredField && (this.Value == null || this.Value == DBNull.Value));
			}
		}

		public override bool Equals(object obj)
		{
			if (obj == null)
				return (this.ComparisonValue == null);

			else if (this.ComparisonValue == null)
				return false;

			else
				return this.Value.Equals(obj);
		}

		public bool IsMatch(Regex pattern)
		{
			return (this.Value != DBNull.Value) && pattern.IsMatch((string)this.Value);
		}

		/// <summary>
		/// The fallback value; used for reversion when data edits are cancelled
		/// </summary>
		private object StoredValue { get; set; }
		private object StoredComparisonValue { get { return (this.StoredValue == DBNull.Value) ? null : this.StoredValue; } }

		internal bool IsDirty { get { return this.Value != this.StoredComparisonValue; } }

		#endregion


		#region CONSTRUCTORS

		public SubjectDatum(ProjectSubjectField projectSubjectField)
		{
			this.Field = projectSubjectField.ProjectSubjectFieldDisplayName;
			this.FieldDesc = projectSubjectField.ProjectSubjectFieldDesc;
			this.FieldType = projectSubjectField.SubjectFieldDataType.NativeType;
            this.FieldLengthLimit = projectSubjectField.FieldDataSize;
			this.IsRequiredField = projectSubjectField.IsRequiredField;
            this.IsLocked = projectSubjectField.IsReadOnly;
		}

		public SubjectDatum(ProjectSubjectField projectSubjectField, object value) : this(projectSubjectField)
		{
			_value = value;
			this.StoredValue = value;
		}

		#endregion


		#region METHODS

		/// <summary>
		/// Reverts the SubjectDatum value to the StoredValue (value as of last save/retrieve operation)
		/// </summary>
		internal void Revert()
		{
			this.Value = StoredValue;
		}

		/// <summary>
		/// Finalizes an edit by setting the fallback value (StoredValue) the the active value
		/// </summary>
		internal void CommitChange()
		{
			this.StoredValue = Value;
		}

		internal void Refresh()
		{
			this.PropertyChanged(this, new PropertyChangedEventArgs("Value"));
		}

		#endregion


		#region IDataErrorInfo Members

		public string Error
		{
			get { return null; }
		}

		public string this[string columnName]
		{
			get
			{
				if (!this.IsValid)
					return this.Field + " is a required field.";

				return null;
			}
		}

		#endregion
	}	// END CLASS


	//public class SubjectDatumValueComparer : IComparer
	//{

	//}


}	// END namespace
