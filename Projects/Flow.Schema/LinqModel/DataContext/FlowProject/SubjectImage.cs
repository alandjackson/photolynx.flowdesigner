﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.Linq;
using System.Data.SqlServerCe;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Media.Imaging;

using Flow.Lib;
using Flow.Lib.Database.SqlCe;
using Flow.Lib.Helpers;
using Flow.Schema.Properties;
using Flow.Schema.Utility;
using System.Drawing;
using System.Windows.Media;
using System.Windows;
using System.Windows.Threading;
using Flow.Lib.AsyncWorker;
using Flow.GreenScreen.Renderer;
using Flow.GreenScreen.Data;
using System.Xml.Serialization;
using Flow.Lib.Log;
using NLog;
using System.Diagnostics;
using System.Configuration;


namespace Flow.Schema.LinqModel.DataContext
{

    public enum RenderBgSource
    {
        GsSettings,
        Order
    }


    /// <summary>
    /// Represents an image (file) assigned to a Subject
    /// </summary>
    public partial class SubjectImage 
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        #region FIELDS AND PROPERTIES

        internal event EventHandler<EventArgs<SubjectImage>> PrimaryImageAssign = delegate { };

        private Subject _subject = null;

        public FlowProjectDataContext FlowProjectDataContext { get {
            if (Subject != null)
                return Subject.FlowProjectDataContext;
            else
                return null;
        }
        }

        private int listIndex = 1;
        public int ListIndex
        {
            get
            {
                
                return listIndex;
            }
            set
            {
                listIndex = value;
                SendPropertyChanged("ListIndex");
            }
        }

        //private bool _imageFlagged = false;
        //public bool ImageFlagged
        //{
        //    get
        //    {
        //        return _imageFlagged;
        //    }
        //    set
        //    {
        //        _imageFlagged = value;
        //        SendPropertyChanged("ImageFlagged");
        //    }
        //}

        public bool IsGreenscreen {
        get
            {
                if(!this.ImageFileName.ToLower().EndsWith("png") && string.IsNullOrEmpty(this.GreenScreenSettings))
                    return false;
                return true;
            }
        }

        public bool RemoteImageServiceInProgress
        {
            get
            {
                if (this.RemoteImageServiceStatus == "inprogress")
                    return true;
                return false;
            }
        }

        public bool RemoteImageServiceDone
        {
            get
            {
                if (this.RemoteImageServiceStatus == "done")
                    return true;
                return false;
            }
        }


        public string ToolTip
        {
            get
            {
                return this.ImageFileName + "\n(" + this.SourceComputer + ")";
            }

        }
        partial void OnCropTChanged()
        {
            if (CropT < 0 || double.IsNaN(CropT))
                CropT = 0;
            this.DateModified = DateTime.Now;
        }
        partial void OnCropWChanged()
        {
            if (CropW < 0 || double.IsNaN(CropW))
                CropW = 0;
            this.DateModified = DateTime.Now;
        }
        partial void OnCropHChanged()
        {
            if (CropH < 0 || double.IsNaN(CropH))
                CropH = 0;
            this.DateModified = DateTime.Now;
        }
        partial void OnCropLChanged()
        {
            if (CropL < 0 || double.IsNaN(CropL))
                CropL = 0;
            this.DateModified = DateTime.Now;
        }

        partial void OnDateAssignedChanged()
        {
            this.DateModified = DateTime.Now;
        }

        partial void  OnExistsInOnlineGalleryChanged()
        {
 	        this.DateModified = DateTime.Now;
        }

        partial void OnGreenScreenBackgroundChanged()
        {
            this.DateModified = DateTime.Now;
        }
        partial void OnGreenScreenSettingsChanged()
        {
            this.DateModified = DateTime.Now;
        }
        partial void OnGroupImageGuidChanged()
        {
            this.DateModified = DateTime.Now;
        }
        partial void OnImageFileNameChanged()
        {
            this.DateModified = DateTime.Now;
        }
        partial void OnImageOriginalFileNameChanged()
        {
            this.DateModified = DateTime.Now;
        }
        
        partial void OnOrientationChanged()
        {
            this.DateModified = DateTime.Now;
        }


        public string GetRenderedImageFilename()
        {
            return GetRenderedGreenScreenImagePath(null, RenderBgSource.GsSettings, false);
        }

        public string GetRenderedImageFilename(bool useDefaultBG)
        {
            if(useDefaultBG)
                return GetRenderedGreenScreenImagePath(null, RenderBgSource.GsSettings, false, useDefaultBG);
            else
                return GetRenderedGreenScreenImagePath(null, RenderBgSource.Order, false, useDefaultBG);
        }

        public string GetRenderedPngImageFilename(string bg)
        {
            if (IsPng)
                return ImagePathFullRes;

            if (string.IsNullOrEmpty(GreenScreenSettings))
                return null;

            return GetRenderedGreenScreenImagePath(bg, RenderBgSource.GsSettings, true);
        }

        static Object _renderGreenScreenLock = new Object();

        public void AutoCrop()
        {
            //if (auto8x10)
            {

                double thumbWidth = this.GetProcessingImage().Width;
                double thumbHeight = this.GetProcessingImage().Height;
                if (thumbHeight > thumbWidth)
                {//8x10

                    //Image newImage = Image.FromFile(image.ImagePathFullRes);
                    //double sw = newImage.HorizontalResolution;
                    //double sh = newImage.VerticalResolution;
                    //newImage.Dispose();
                    //thumbHeight = sh > sw ? sh : sw;
                    //thumbWidth = sh > sw ? sw : sh;

                    double newH = thumbWidth / .8;

                    this.CropW = 1;
                    this.CropH = newH / thumbHeight;
                    this.CropL = 0;
                    this.CropT = (1 - this.CropH) / 2;
                    this.FlowProjectDataContext.SubmitChanges();
                }
                else if (thumbHeight < thumbWidth)
                {//10x8

                    //Image newImage = Image.FromFile(image.ImagePathFullRes);
                    //double sw = newImage.HorizontalResolution;
                    //double sh = newImage.VerticalResolution;
                    //newImage.Dispose();
                    //thumbHeight = sh < sw ? sh : sw;
                    //thumbWidth = sh < sw ? sw : sh;


                    double newW = thumbHeight / .8;

                    this.CropW = newW / thumbWidth;
                    this.CropH = 1;
                    this.CropL = (1 - this.CropW) / 2;
                    this.CropT = 0;
                    this.FlowProjectDataContext.SubmitChanges();
                }
            }

        }


        public string GetRenderedGreenScreenImagePath(string OrderedBackground, RenderBgSource bgSource, bool getPng)
        { return GetRenderedGreenScreenImagePath(OrderedBackground, bgSource, getPng, null, false); }

        public string GetRenderedGreenScreenImagePath(string OrderedBackground, RenderBgSource bgSource, bool getPng, bool UseDefaultBG)
        { return GetRenderedGreenScreenImagePath(OrderedBackground, bgSource, getPng, null, UseDefaultBG); }

        public string GetRenderedGreenScreenImagePath(string OrderedBackground, RenderBgSource bgSource, bool getPng, string altImagePath, bool UseDefaultBG)
        {
            if (!File.Exists(ImagePathFullRes))
            {
                logger.Warn("LayoutRender: SubjectImage image path does not exist: {0}", ImagePathFullRes);
                return null;
            }
           

            string gsSettings = GreenScreenSettings;

            // if no gs settings are set, then this is not a gs image
            //if (string.IsNullOrEmpty(gsSettings) && Subject.FlowProjectDataContext.FlowProject.ShowGreenScreen)
            //    gsSettings = ObjectSerializer.SerializeString(new SelectionConfiguration(true) { }, true);

            if (string.IsNullOrEmpty(gsSettings) && !IsPng)
                return ImagePathFullRes;

            // TODO: resolved backgrounds from the gs settings vs gsbackground property
            string bgPath = "";
            if (bgSource == RenderBgSource.GsSettings && !String.IsNullOrEmpty(GreenScreenBackground))
                bgPath = GreenScreenBackground;
            else if (bgSource == RenderBgSource.Order &&  OrderedBackground != null)
                bgPath = OrderedBackground;

            if (UseDefaultBG)
                bgPath = this.Subject.FlowProjectDataContext.FlowProject.DefaultGreenScreenBackground;


            //if there is an order for this image, use the background ordered:
            if (string.IsNullOrEmpty(bgPath) && Subject.SubjectOrder != null && Subject.SubjectOrder.OrderPackages != null && Subject.SubjectOrder.OrderPackages.Count > 0)
            {
                foreach (OrderPackage op in Subject.SubjectOrder.OrderPackages)
                {
                    if (op.FirstImage != null && op.FirstImage.SubjectImageID == this.SubjectImageID)
                        bgPath = op.DefaultBackgound.FullPath;
                }
                //bgPath = this.Subject.FlowProjectDataContext.FlowProject.DefaultGreenScreenBackground;
            }

            if(string.IsNullOrEmpty(bgPath))
                bgPath = GreenScreenBackground;

            // grab the default green screen background if one isn't defined
            if (UseDefaultBG || (string.IsNullOrEmpty(bgPath) && !string.IsNullOrEmpty(Subject.FlowProjectDataContext.FlowProject.DefaultGreenScreenBackground)))
                bgPath = this.Subject.FlowProjectDataContext.FlowProject.DefaultGreenScreenBackground;

            // don't perform dropout of the bg file does not exist
            if (string.IsNullOrEmpty(bgPath) || !File.Exists(bgPath))
                return ImagePathFullRes;

            GreenScreenCache.Initialize(ImagePathFullRes);

            //because we could have multiple rendered images for each subjectImage (different backgrounds for different packages)
            //we need to add an indicator to the filename that represents which background was used
            //this is all temporary, and is only done before sending a rendered image to a lab as part of an order.
            var fi = new FileInfo(ImagePathFullRes);
            string newFileName = fi.Name;


            if (OrderedBackground != null && !getPng)
            {
                var gsBackgrounds = this.Subject.FlowProjectDataContext.FlowMasterDataContext.GreenScreenBackgrounds;

                //get a number to represent the background selected
                int bkgrnIndex = 0;
                if (!string.IsNullOrEmpty(OrderedBackground))
                {
                    try
                    {
                        bkgrnIndex = gsBackgrounds.IndexOf(gsBackgrounds.First(g => g.FileName == ((new FileInfo(OrderedBackground)).Name)));
                    }
                    catch { 
                    //ignore errors, just use bkgrnIndex = 0;
                    }
                }
                bgPath = gsBackgrounds[bkgrnIndex].FullPath;
                newFileName = fi.Name.Replace(fi.Name.GetFileNameRoot(), fi.Name.GetFileNameRoot() + "_" + bkgrnIndex);
            }

            int applyOrientation = Orientation;
            
            string dir = GreenScreenCache.GetCacheDirectory(ImagePathFullRes);
            string renderPath = Path.Combine(dir, newFileName);

            if (renderPath.ToLower().EndsWith(".png"))
                renderPath = renderPath.Replace(".png", ".jpg").Replace(".PNG", ".jpg");

            string imgSrc = ImagePathFullRes;
            if (altImagePath != null && File.Exists(altImagePath))
            {
                imgSrc = altImagePath;
                applyOrientation = 0;
            }

            if (imgSrc.ToLower().EndsWith("png") && !File.Exists(renderPath))
                File.Copy(imgSrc, renderPath);
            if (imgSrc.ToLower().EndsWith("png"))
                gsSettings = null;

            var cacheData = new GreenScreenCacheData()
            {
                ForegroundPath = imgSrc,
                RenderPath = renderPath,
                Orientation = applyOrientation,
                BackgroundPath = bgPath,
                GreenScreenSettings = gsSettings
            };

            lock (_renderGreenScreenLock)
            {
                if (GreenScreenCache.CachedImageRenderExists(cacheData))
                {
                   

                    if (getPng)
                        return renderPath.Replace(".JPG", ".png").Replace(".jpg", ".png");

                    return renderPath;
                }

                RenderGsToFile(cacheData);

                if (File.Exists(renderPath))
                {
                    GreenScreenCache.StoreCachedImageRender(cacheData);
                    if (getPng)
                        return renderPath.Replace(".JPG", ".png").Replace(".jpg", ".png");
                    return renderPath;
                }
            }

            logger.Warn("LayoutRender: SubjectImage image green screen renderPath doesn't exist: {0}", renderPath);
            return "";
        }

        private string RenderGS(string newImageName, string bgPath)
        {

            string dir = GreenScreenCache.GetCacheDirectory(ImagePathFullRes);
            string renderPath = Path.Combine(dir, Path.GetFileName(newImageName));

            var cacheData = new GreenScreenCacheData()
            {
                ForegroundPath = ImagePathFullRes,
                RenderPath = Path.Combine(dir, Path.GetFileName(this.ImageFileName)),
                Orientation = this.Orientation,
                BackgroundPath = bgPath,
                GreenScreenSettings = this.GreenScreenSettings
            };

            lock (_renderGreenScreenLock)
            {
                if (GreenScreenCache.CachedImageRenderExists(cacheData))
                {
                    return renderPath;
                }

                RenderGsToFile(cacheData);

                if (File.Exists(renderPath))
                {
                    GreenScreenCache.StoreCachedImageRender(cacheData);
                    return renderPath;
                }
            }
            return "";
        }

        private static void RenderGsToFile(GreenScreenCacheData cacheData)
        {
            if (ConfigUtil.GetBoolSetting("RENDER_GS_EXTERNAL"))
            {
                logger.Info("RENDER_GS_EXTERNAL");
                string path = "Flow.GreenScreen.exe";
                string args = "";
                string stdin = Flow.Lib.Persister.ObjectSerializer.SerializeString(cacheData);

                logger.Info("Starting Flow.GreenScren");
                ProcessStartInfo psi = new ProcessStartInfo(path, args);
                psi.CreateNoWindow = true;
                psi.UseShellExecute = false;
                psi.RedirectStandardInput = true;
                psi.RedirectStandardOutput = true;
                Process p = Process.Start(psi);
                p.StandardInput.Write(stdin);
                p.StandardInput.Close();
                string errorMsg = p.StandardOutput.ReadToEnd();
                if (!string.IsNullOrEmpty(errorMsg))
                {
                    //if external render caused error, try and do an internal render
                    try
                    {
                        logger.Info("external render caused error, try and do an internal render: " + errorMsg);
                        CPIRenderer.RenderToFile(cacheData);
                        logger.Info("Internal Render Complete - No Errors ");
                    }
                    catch (Exception e)
                    {
                        logger.Error("External GS Render, xml: " + stdin);
                        throw new Exception(e.Message);
                    }
                }
                else
                {
                    logger.Info("External Render Complete - No Errors");
                }

            }
            else
            {
                logger.Info("Render GS Internal");
                CPIRenderer.RenderToFile(cacheData);
            }
        }


        public string GetImagePath(bool renderGreenScreen)
        { return GetImagePath(renderGreenScreen, null); }

        public string GetImagePath(bool renderGreenScreen, string GreenScreenBackground)
        { return GetImagePath(renderGreenScreen, GreenScreenBackground, null); }

        public string GetImagePath(bool renderGreenScreen, string GreenScreenBackground, string altImagePath)
        {
            if (renderGreenScreen)
                return GetRenderedGreenScreenImagePath(GreenScreenBackground, RenderBgSource.Order,false, altImagePath, false);
            else
                return ImagePathFullRes;
        }
        public void UpdateGroupImageCrop()
        {
            if (this.IsGroupImage)
                GroupImage.UpdateGroupImageCrop(this.CropT, this.CropW, this.CropH, this.CropL);
        }
        private GroupImage _groupImage = null;
        public GroupImage GroupImage
        {
            get
            {
                if (IsGroupImage && _groupImage == null)
                {
                    _groupImage = this.Subject.FlowProjectDataContext.GroupImages.Where(gi => gi.GroupImageGuid == this.GroupImageGuid).FirstOrDefault();
                }

                return _groupImage;
            }

        }
        public bool IsGroupImage
        {
            get
            {
                if (this.GroupImageGuid != null)
                    return true;

                return false;
            }
        }

        /// <summary>
        /// A class-wrapper representing the graphical image 
        /// </summary>
        FlowImage _flowImage = null;
        public FlowImage FlowImage
        {
            get
            {
                if (_flowImage == null)
                {
                    _flowImage = new FlowImage(this.ImagePath);
                }
                return _flowImage;
            }
            set { _flowImage = value; }

        }

        public bool ImageFileExists
        {
            get
            {
                if (!this.FlowImage.ImageFileExists)
                {
                    if (File.Exists(this.ImagePath))
                        this.FlowImage.ImageFileExists = true;
                }
                return this.FlowImage.ImageFileExists;

            }
        }


        /// <summary>
        /// The full filepath of the image file
        /// </summary>
        private string _imagePath = null;
        public string ImagePath
        {
            get
            {
                if (_imagePath == null && this.Subject != null && this.Subject.FlowProjectDataContext != null)
                {
                    if (Subject != null)
                        _imagePath = Path.Combine(this.Subject.FlowProjectDataContext.FlowMasterDataContext.PreferenceManager.Application.GetFlowProjectImageDirPath(this.FlowProjectGuid, this.FlowProjectDataContext.FlowMasterDataContext.FlowProjectList.First(fp=>fp.FlowProjectGuid == this.FlowProjectGuid).IsNetworkProject == true), this.ImageFileName);
                    else if (_subject != null)
                        _imagePath = Path.Combine(this.Subject.FlowProjectDataContext.FlowMasterDataContext.PreferenceManager.Application.GetFlowProjectImageDirPath(this.FlowProjectGuid, this.FlowProjectDataContext.FlowMasterDataContext.FlowProjectList.First(fp => fp.FlowProjectGuid == this.FlowProjectGuid).IsNetworkProject == true), this.ImageFileName);

                }
                 if (_imagePath != null)
                 {
                    string miniImagePath = _imagePath.Replace(this.ImageFileName ?? "", "thumbnails\\" + this.ImageFileName);
                    if (File.Exists(miniImagePath))
                        _imagePath = miniImagePath;
                }
                return _imagePath;
            }
            set { _imagePath = value; }
        }

        private string _imagePathFullRes = null;
        public string ImagePathFullRes
        {
            get
            {
                if (ImagePath != null && Directory.GetParent(ImagePath).Name != "thumbnails")
                    return ImagePath;
                if (_imagePathFullRes == null && this.Subject != null && this.Subject.FlowProjectDataContext != null)
                {
                    _imagePathFullRes = Path.Combine(this.Subject.FlowProjectDataContext.FlowMasterDataContext.PreferenceManager.Application.GetFlowProjectImageDirPath(this.FlowProjectGuid, this.Subject.FlowProjectDataContext.FlowMasterDataContext.FlowProjectList.First(fp => fp.FlowProjectGuid == this.FlowProjectGuid).IsNetworkProject == true), this.ImageFileName);                      
                }
                else if (_imagePathFullRes == null && this._subject != null && this._subject.FlowProjectDataContext != null)
                {
                     _imagePathFullRes = Path.Combine(this._subject.FlowProjectDataContext.FlowMasterDataContext.PreferenceManager.Application.GetFlowProjectImageDirPath(this.FlowProjectGuid, this._subject.FlowProjectDataContext.FlowMasterDataContext.FlowProjectList.First(fp => fp.FlowProjectGuid == this.FlowProjectGuid).IsNetworkProject == true), this.ImageFileName);
                }

                return _imagePathFullRes;
            }
            set { _imagePathFullRes = value; }
        }

        /// <summary>
        /// The DP2-safe filename, effectively the image GUID + extension (limited to 31 chars)
        /// </summary>
        ///
        public string GetDp2FileName(string OrderedBackground, bool useOriginal, bool forceJpg)
        {
            //if its a greenscreen background, there could be multiple rendered green screen backgrounds for the same subject image, 
            //so add an _<bkgrnd_index> to the Dp2 Fielname

            //get a number to represent the background selected
            try
            {
                int bkgrnIndex = 0;
                string backgroundFile = "";
                if (!String.IsNullOrEmpty(OrderedBackground))
                {
                    backgroundFile = OrderedBackground;

                }
                else if (this.GreenScreenBackground != null && File.Exists(this.GreenScreenBackground))
                {
                    backgroundFile = this.GreenScreenBackground;
                }
                else if (this.Subject.FlowProjectDataContext.FlowProject.DefaultGreenScreenBackground != null && File.Exists(this.Subject.FlowProjectDataContext.FlowProject.DefaultGreenScreenBackground))
                {
                    backgroundFile = this.Subject.FlowProjectDataContext.FlowProject.DefaultGreenScreenBackground;
                }


                if (this.Subject.FlowProjectDataContext.FlowMasterDataContext.GreenScreenBackgrounds.Any(g => g.FileName == ((new FileInfo(backgroundFile)).Name)))
                {
                    bkgrnIndex = this.Subject.FlowProjectDataContext.FlowMasterDataContext.GreenScreenBackgrounds.IndexOf(this.Subject.FlowProjectDataContext.FlowMasterDataContext.GreenScreenBackgrounds.First(g => g.FileName == ((new FileInfo(backgroundFile)).Name)));
                }
                else
                {
                    logger.Error("SUBJECT: " + this.Subject.FormalFullName + "IS MISSING BACKGROUND IMAGE: " + (new FileInfo(backgroundFile)).Name);
                    throw new Exception("SUBJECT: " + this.Subject.FormalFullName + "IS MISSING BACKGROUND IMAGE: " + (new FileInfo(backgroundFile)).Name);
                }

                string newFileName = Dp2FileName(useOriginal).Replace(Dp2FileName(useOriginal).GetFileNameRoot(), Dp2FileName(useOriginal).GetFileNameRoot().Substring(0, Dp2FileName(useOriginal).GetFileNameRoot().Length - 3) + "_" + bkgrnIndex);

                if (forceJpg && newFileName.ToLower().EndsWith(".png"))
                    newFileName = newFileName.Replace(".png", ".jpg").Replace(".PNG", ".jpg");
                return newFileName;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public string Dp2FileName(bool useOriginal)
        {
            if (useOriginal && !this.IsGroupImage)
            {
                 return this.ImageFileName;
            }
            if (useOriginal && this.IsGroupImage)
            {
                FileInfo origFile = new FileInfo(this.ImageFileName);
                return origFile.Name.Replace(origFile.Extension, "") + "_" + this._SubjectImageGuid.ToString().Substring(0,4) + origFile.Extension;
            }
            //used for most image names for lab orders
            
                //if this is a group image, use the group image guid
                //if (this.IsGroupImage)
                //    return this.ImageFileName;

                FileInfo fileInfo = new FileInfo(this.ImagePathFullRes);
                string extension = fileInfo.Extension;

                string reducedGuid = this.SubjectImageGuid.Value.ToString().Replace("-", "");

                int seq = this.Subject.ImageList.IndexOf(this) + 2;
                if(this.IsPrimary)
                    seq = 1;
                string shortName = seq.ToString("000") + "_" + (reducedGuid + extension).Right(27);
                return shortName;
        
        }

        /// <summary>
        /// A thumbnail representation of the image
        /// </summary>
        //public BitmapImage Thumbnail
        //{
        //    get
        //    {
        //        BitmapImage tmpImage;

        //        if (this.Subject.FlowProjectDataContext.FlowMasterDataContext.ShowOriginalFile)
        //            tmpImage = this.FlowImage.Thumbnail;
        //        else
        //        {
        //            this.FlowImage.CropL = this.CropL;
        //            this.FlowImage.CropT = this.CropT;
        //            this.FlowImage.CropW = this.CropW;
        //            this.FlowImage.CropH = this.CropH;
        //            tmpImage = this.FlowImage.ThumbnailCropped;
        //        }

        //        this.Orientation = this.FlowImage.Orientation;
        //        return tmpImage;
        //    }

        //}
        private bool thumbnailUpdateInProgress = false;
        private bool forceThumbnailBackground = false;
        private BitmapImage _updatedThumbnail = null;
        public BitmapImage qThumb
        {
            get
            {
                forceThumbnailBackground = true;
                return Thumbnail;
            }
        }
        public BitmapImage Thumbnail
        {
            get
            {
                if (this.Subject == null) return null;

                if (!File.Exists(this.ImagePath))
                    return this.FlowProjectDataContext.MissingImage;
                if (forceThumbnailBackground || this.Subject.FlowProjectDataContext.FlowMasterDataContext.PreferenceManager.Application.ProcessImagesInBackground)
                {
                    if (_updatedThumbnail == null && !thumbnailUpdateInProgress)
                    {
                        thumbnailUpdateInProgress = true;
                        FlowBackgroundWorker worker = FlowBackgroundWorkerManager.RunWorker(GetThumbnail, GetThumbnail_Completed);

                    }
                    forceThumbnailBackground = false;
                    //if (_updatedThumbnail == null)
                    //    return this.FlowProjectDataContext.MissingImage;
                    return _updatedThumbnail;
                }
                else
                {
                    BitmapImage tmpImage;
                    this.FlowImage.Clear();
                    if (this.Subject.FlowProjectDataContext.FlowMasterDataContext.ShowOriginalFile)
                        tmpImage = this.FlowImage.Thumbnail;
                    else
                    {
                        if (this.CropW == 1 && this.CropH == 0)
                            this.CropH = 1;

                        if (this.CropH == 1 && this.CropW == 0)
                            this.CropW = 1;

                        this.FlowImage.CropL = this.CropL;
                        this.FlowImage.CropT = this.CropT;
                        this.FlowImage.CropW = this.CropW;
                        this.FlowImage.CropH = this.CropH;
                        this.FlowImage.RotationAngle = this.RotationAngle;

                        tmpImage = this.FlowImage.ThumbnailCropped;
                    }

                    this.Orientation = this.FlowImage.Orientation;
                    _updatedThumbnail = tmpImage;
                    return tmpImage;
                }

            }
        }

        public void GetThumbnail(object sender, DoWorkEventArgs e)
        {
            //_updatedThumbnail = quickThumb();
            //SendPropertyChanged("Thumbnail");
            BitmapImage tmpImage = new BitmapImage();

            if (this.Subject != null && this.Subject.FlowProjectDataContext.FlowMasterDataContext.ShowOriginalFile)
                tmpImage = this.FlowImage.Thumbnail;
            else
            {
                this.FlowImage.CropL = this.CropL;
                this.FlowImage.CropT = this.CropT;
                this.FlowImage.CropW = this.CropW;
                this.FlowImage.CropH = this.CropH;
                tmpImage = this.FlowImage.ThumbnailCropped;
            }

            this.Orientation = this.FlowImage.Orientation;
            //this.Dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
            //{
            _updatedThumbnail = tmpImage;

            //}));
        }
        void GetThumbnail_Completed(object sender, RunWorkerCompletedEventArgs e)
        {
            thumbnailUpdateInProgress = false;
            SendPropertyChanged("Thumbnail");
            SendPropertyChanged("qThumb");
        }


        public BitmapImage quickThumb()
        {

            // BitmapImage bi = new BitmapImage();
            // bi.BeginInit();
            // bi.DecodePixelWidth = 25;
            // bi.CacheOption = BitmapCacheOption.OnLoad;
            // bi.UriSource = new Uri( this.ImagePath.ToString() );
            // bi.SourceRect = new Int32Rect((int)CropL, (int)CropT, (int)CropW, (int)CropH);
            //bi.EndInit();
            //return bi;

            if (this.Subject.FlowProjectDataContext.FlowMasterDataContext.ShowOriginalFile)
                return this.FlowImage.QuickThumbnail;
            else
            {
                this.FlowImage.CropL = this.CropL;
                this.FlowImage.CropT = this.CropT;
                this.FlowImage.CropW = this.CropW;
                this.FlowImage.CropH = this.CropH;
                return this.FlowImage.QuickThumbnailCropped;
            }

        }

        //public BitmapImage IntermediateImage
        //{
        //    get
        //    {
        //        BitmapImage tmpImage;
        //        if (this.Subject.FlowProjectDataContext.FlowMasterDataContext.ShowOriginalFile)
        //            tmpImage = this.FlowImage.IntermediateImage;
        //        else
        //        {
        //            this.FlowImage.CropL = this.CropL;
        //            this.FlowImage.CropT = this.CropT;
        //            this.FlowImage.CropW = this.CropW;
        //            this.FlowImage.CropH = this.CropH;
        //            tmpImage = this.FlowImage.IntermediateImageCropped;
        //        }

        //        this.Orientation = this.FlowImage.Orientation;
        //        return tmpImage;
        //    }
        //}

        private bool intermediateUpdateInProgress = false;
        private bool forceIntermediateBackground = false;
        private BitmapImage _updatedIntermediateImage = null;
        public BitmapImage IntermediateImage
        {
            get
            {
                if (this.Subject == null) return null;

                if (!File.Exists(this.ImagePath))
                    return this.FlowProjectDataContext.MissingImage;
                if (forceIntermediateBackground || this.Subject.FlowProjectDataContext.FlowMasterDataContext.PreferenceManager.Application.ProcessImagesInBackground)
                {
                    if (_updatedIntermediateImage == null && !intermediateUpdateInProgress)
                    {
                        intermediateUpdateInProgress = true;
                        FlowBackgroundWorker worker = FlowBackgroundWorkerManager.RunWorker(GetIntermediateImage, GetIntermediateImage_Completed);

                    }
                    forceIntermediateBackground = false;
                    //if (_updatedIntermediateImage == null)
                    //    return this.FlowProjectDataContext.MissingImage;
                    return _updatedIntermediateImage;
                }
                else
                {
                    BitmapImage tmpImage;
                    if (this.Subject.FlowProjectDataContext.FlowMasterDataContext.ShowOriginalFile)
                        tmpImage = this.FlowImage.IntermediateImage;
                    else
                    {
                        this.FlowImage.CropL = this.CropL;
                        this.FlowImage.CropT = this.CropT;
                        this.FlowImage.CropW = this.CropW;
                        this.FlowImage.CropH = this.CropH;
                        this.FlowImage.RotationAngle = this.RotationAngle;
                        tmpImage = this.FlowImage.IntermediateImageCropped;
                    }

                    this.Orientation = this.FlowImage.Orientation;
                    return tmpImage;
                }
            }
        }

        public void GetIntermediateImage(object sender, DoWorkEventArgs e)
        {
            //_updatedIntermediateImage = quickThumb();
            //SendPropertyChanged("IntermediateImage");

            BitmapImage tmpImage = new BitmapImage();

            if (this.Subject.FlowProjectDataContext.FlowMasterDataContext.ShowOriginalFile)
                tmpImage = this.FlowImage.IntermediateImage;
            else
            {
                this.FlowImage.CropL = this.CropL;
                this.FlowImage.CropT = this.CropT;
                this.FlowImage.CropW = this.CropW;
                this.FlowImage.CropH = this.CropH;
                this.FlowImage.RotationAngle = this.RotationAngle;
                tmpImage = this.FlowImage.IntermediateImageCropped;
            }

            this.Orientation = this.FlowImage.Orientation;
            //this.Dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
            //{
            _updatedIntermediateImage = tmpImage;

            //}));
        }
        void GetIntermediateImage_Completed(object sender, RunWorkerCompletedEventArgs e)
        {
            intermediateUpdateInProgress = false;
            SendPropertyChanged("IntermediateImage");
        }




        /// <summary>
        /// The digital photographic image, in all its glory
        /// </summary>
        //public BitmapImage ProcessingImage
        //{

        //    get 
        //    {
        //        BitmapImage tmpImage = this.FlowImage.ProcessingImage;
        //        this.Orientation = this.FlowImage.Orientation;
        //        return tmpImage;
        //    }
        //}

        public BitmapImage GetProcessingImage() 
        {
            if (this.Subject == null) return null;

            if (!File.Exists(this.ImagePath))
                return this.FlowProjectDataContext.MissingImage;
            
            return FlowImage.GetProcessingImage();
        }

        private bool ProcessingImageUpdateInProgress = false;
        private bool forceProcessingImageBackground = false;
        private BitmapImage _updatedProcessingImage = null;
        public BitmapImage ProcessingImage
        {
            get
            {
                if (this.Subject == null) return null;

                if (!File.Exists(this.ImagePath))
                    return this.FlowProjectDataContext.MissingImage;
                if (forceProcessingImageBackground || this.Subject.FlowProjectDataContext.FlowMasterDataContext.PreferenceManager.Application.ProcessImagesInBackground)
                {
                    forceProcessingImageBackground = false;
                    if (_updatedProcessingImage == null && !ProcessingImageUpdateInProgress)
                    {
                        ProcessingImageUpdateInProgress = true;
                        FlowBackgroundWorker worker = FlowBackgroundWorkerManager.RunWorker(GetProcessingImage, GetProcessingImage_Completed);
                    }
                    forceProcessingImageBackground = false;
                    //if (_updatedIntermediateImage == null)
                    //    return this.FlowProjectDataContext.MissingImage;
                    return _updatedProcessingImage;

                }
                else
                {
                    
                    //return tmpImage;

                    BitmapImage tmpImage;
                    if (this.Subject.FlowProjectDataContext.FlowMasterDataContext.ShowOriginalFile)
                        tmpImage = this.FlowImage.ProcessingImage;
                    else
                    {
                        this.FlowImage.CropL = this.CropL;
                        this.FlowImage.CropT = this.CropT;
                        this.FlowImage.CropW = this.CropW;
                        this.FlowImage.CropH = this.CropH;
                        this.FlowImage.RotationAngle = this.RotationAngle;
                        tmpImage = this.FlowImage.ProcessingImageCropped;
                    }

                    this.Orientation = this.FlowImage.Orientation;
                    return tmpImage;


                }

            }
        }

        public void GetProcessingImage(object sender, DoWorkEventArgs e)
        {

            //_updatedProcessingImage = quickThumb();
            //SendPropertyChanged("ProcessingImage");

            BitmapImage tmpImage = new BitmapImage();


            tmpImage = this.FlowImage.ProcessingImage;


            this.Orientation = this.FlowImage.Orientation;

            _updatedProcessingImage = tmpImage;


        }
        void GetProcessingImage_Completed(object sender, RunWorkerCompletedEventArgs e)
        {
            ProcessingImageUpdateInProgress = false;
            SendPropertyChanged("ProcessingImage");
        }



        private FlowObservableCollection<SubjectImageType> _subjectImageTypeList = null;
        public FlowObservableCollection<SubjectImageType> SubjectImageTypeList
        {
            get
            {
                if (_subjectImageTypeList == null)
                {
                    if (this.SubjectImageTypes.Count == 0)
                    {
                        foreach (ProjectImageType imageType in this.Subject.FlowProjectDataContext.ProjectImageTypes)
                        {
                            SubjectImageType subjectImageType = new SubjectImageType();

                            subjectImageType.SubjectImage = this;
                            subjectImageType.ProjectImageType = imageType;
                            subjectImageType.IsAssigned = false;

                            this.SubjectImageTypes.Add(subjectImageType);

                            if (imageType.IsExclusive)
                                subjectImageType.ExclusiveImageTypeAssign += new EventHandler<EventArgs<SubjectImageType>>(this.Subject.SubjectImageType_ExclusiveImageTypeAssign);
                        }

                        this.Subject.FlowProjectDataContext.SubmitChanges();
                    }

                    _subjectImageTypeList = new FlowObservableCollection<SubjectImageType>(this.SubjectImageTypes);

                }

                return _subjectImageTypeList;
            }
        }

        private bool _isSuppressed = false;
        public bool IsSuppressed
        {
            get { return _isSuppressed; }
            set
            {
                _isSuppressed = value;
                this.SendPropertyChanged("IsSuppressed");
                this.SendPropertyChanged("IsVisible");
            }
        }

        public bool IsVisible { get { return !this.IsSuppressed; } }

        public bool IsNotPrimary { get { return !this.IsPrimary; } }

        partial void OnIsPrimaryChanging(bool value)
        {
            if (value)
            {
                this.PrimaryImageAssign(this, new EventArgs<SubjectImage>(this));
            }
        }

        partial void OnIsPrimaryChanged()
        {
            this.DateModified = DateTime.Now;

            if (this.Subject.FlowProjectDataContext != null && this.IsPrimary)
                this.Subject.FlowProjectDataContext.SubmitChanges();

            this.SendPropertyChanged("IsNotPrimary");
        }


        #endregion


        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="subject"></param>
        public SubjectImage(Subject subject)
            : this()
        {
            _subject = subject;
            this.SubjectImageGuid = Guid.NewGuid();
        }

        private FlowObservableCollection<OrderImageOption> _orderImageOptionList = null;
        public FlowObservableCollection<OrderImageOption> OrderImageOptionList
        {
            get
            {
                if (_orderImageOptionList == null)
                {
                    //_imageList = new FlowObservableCollection<SubjectImage>(this.SubjectImages);
                    _orderImageOptionList = new FlowObservableCollection<OrderImageOption>();
                    foreach (OrderImageOption oio in this.OrderImageOptions.OrderBy(si => si.SubjectImageID))
                    {

                        if (oio.FlowCatalogOption == null && this.FlowProjectDataContext.FlowProject.FlowCatalog != null)
                        {
                            if (this.FlowProjectDataContext.FlowProject.FlowCatalog.FlowCatalogImageOptionList.Any(fco => fco.FlowCatalogOptionGuid == oio.FlowCatalogOptionGuid))
                            {
                                oio.FlowCatalogOption = this.FlowProjectDataContext.FlowProject.FlowCatalog.FlowCatalogImageOptionList.First(fco => fco.FlowCatalogOptionGuid == oio.FlowCatalogOptionGuid);
                            }

                            //backgrounds also need to be shown in this list (for PUD workflow)
                            if (oio.FlowCatalogOption == null)
                            {
                                if (this.FlowProjectDataContext.FlowProject.FlowCatalog.FlowCatalogOptionList.Any(fco => fco.FlowCatalogOptionGuid == oio.FlowCatalogOptionGuid))
                                {
                                    oio.FlowCatalogOption = this.FlowProjectDataContext.FlowProject.FlowCatalog.FlowCatalogOptionList.First(fco => fco.FlowCatalogOptionGuid == oio.FlowCatalogOptionGuid);
                                }
                            }
                        }
                        //if(oio.FlowCatalogOption.ImageQuixCatalogOption.ImageQuixCatalogOptionGroup.Label == "Image Options")\

                        if (oio.FlowCatalogOption != null)
                        {
                            _orderImageOptionList.Add(oio);
                        }
                    }
                }

                return _orderImageOptionList;
            }


        }


        partial void OnCreated()
        {
            //if(this.SubjectImageGuid == Guid.Empty)
            //    this.SubjectImageGuid = Guid.NewGuid();

            //			this.PrimaryImageAssign += new EventHandler<EventArgs<SubjectImage>>(this.Subject.SubjectImage_PrimaryImageAssign);
        }

        partial void OnLoaded()
        {
            if (this.SubjectImageGuid == null || this.SubjectImageGuid == Guid.Empty)
                this.SubjectImageGuid = Guid.NewGuid();

            if (DateAssigned == null && this.ImageFileExists)
                this.DateAssigned = (new FileInfo(this.ImagePath)).CreationTime;

            this.PrimaryImageAssign += new EventHandler<EventArgs<SubjectImage>>(this.Subject.SubjectImage_PrimaryImageAssign);


        }

        public void DisposeImages()
        {
            if (this.FlowImage != null)
                this.FlowImage.Clear();

            this.FlowImage = null;
            this.RefreshImage();
        }

        public void ClearImage()
        {
            _updatedThumbnail = null;
            _updatedIntermediateImage = null;
            _updatedProcessingImage = null;
            this.FlowImage.Clear();
            //this.SendPropertyChanged("ProcessingImage");
            //this.SendPropertyChanged("IntermediateImage");
            //this.SendPropertyChanged("Thumbnail");
        }

        public void RefreshImage()
        {
            RefreshImage(false);
        }
        public void RefreshImage(bool backgroundRefresh)
        {
            _updatedThumbnail = null;
            _updatedIntermediateImage = null;
            _updatedProcessingImage = null;
            _flowImage = null;
            //_imagePath = null;
            //this.FlowImage.Refresh();
            if (backgroundRefresh)
            {
                forceThumbnailBackground = true;
                forceIntermediateBackground = true;
                forceProcessingImageBackground = true;
                object initThumb = Thumbnail;
                object initIntem = IntermediateImage;
                object initProc = ProcessingImage;
            }
            else
            {
                this.SendPropertyChanged("ProcessingImage");
                this.SendPropertyChanged("IntermediateImage");
                this.SendPropertyChanged("Thumbnail");
                this.SendPropertyChanged("IntermediateImageCropped");
                this.SendPropertyChanged("ThumbnailCropped");
            }

            //if (this.ImagePath.Contains("thumbnails"))
            //    File.Delete(ImagePath);
            
        }

        internal void IncrementFileName()
        {
            string fileNameRoot = this.ImageFileName.GetFileNameRoot();

            Regex prefix = new Regex(@"^\d{2}_");

            int ordinal = 1;

            if (prefix.IsMatch(fileNameRoot))
            {
                ordinal = Int32.Parse(fileNameRoot.Left(2));
                ordinal++;
            }

            this.ImageFileName = ordinal.ZeroPad(2) + "_" + this.ImageFileName;
        }

        public void SubjectImageType_ExclusiveImageTypeAssign(object sender, EventHandler<EventArgs<SubjectImageType>> e)
        {
            //			SubjectImageType assigned

        }


        #region MERGE MEMBERS

        private Guid _subjectGuid;
        internal Guid SubjectGuid
        {
            get { return _subjectGuid; }
            set
            {
                _subjectGuid = value;

                // this.SubjectImageGuid = Guid.NewGuid();

                foreach (OrderProductNode orderProductNode in this.OrderProductNodes)
                {
                    orderProductNode.SubjectImageGuid = this.SubjectImageGuid.Value;
                }
            }
        }

        internal SubjectImage Clone()
        {
            SubjectImage clone = new SubjectImage();

            clone.SubjectImageGuid = this.SubjectImageGuid;
            clone.FlowProjectGuid = this.FlowProjectGuid;
            clone.ImageFileName = this.ImageFileName;
            clone.ImageFlagged = false;
            clone.ImageOriginalFileName = this.ImageOriginalFileName;
            clone.Red = this.Red;
            clone.Green = this.Green;
            clone.Blue = this.Blue;
            clone.Sharpen = this.Sharpen;
            clone.Density = this.Density;
            clone.Contrast = this.Contrast;
            clone.CropT = this.CropT;
            clone.CropL = this.CropL;
            clone.CropW = this.CropW;
            clone.CropH = this.CropH;
            clone.Orientation = this.Orientation;
            clone.DateAssigned = this.DateAssigned;
            clone.GroupImageGuid = this.GroupImageGuid;
            clone.GreenScreenBackground = this.GreenScreenBackground;
            clone.GreenScreenSettings = this.GreenScreenSettings;
            clone.SourceComputer = this.SourceComputer;
            clone.ExistsInOnlineGallery = this.ExistsInOnlineGallery;
            return clone;
        }

        //		internal Guid SubjectImageGuid { get; private set; }

        #endregion



        public OrderImageOption AddCatalogOption(FlowCatalogOption flowCatalogOption)
        {
            if (this.OrderImageOptions.Count(oio => oio.FlowCatalogOptionGuid == flowCatalogOption.FlowCatalogOptionGuid) == 0)
            {
                OrderImageOption imageOption = new OrderImageOption(flowCatalogOption, this);
                imageOption.TaxFactor = flowCatalogOption.FlowCatalog.TaxRate;
                imageOption.FlowCatalogOption = flowCatalogOption;
                imageOption.FlowCatalogOptionGuid = flowCatalogOption.FlowCatalogOptionGuid;
                imageOption.ResourceURL = flowCatalogOption.ImageQuixCatalogOption.ResourceURL;
                imageOption.DateAdded = DateTime.Now;
                this.OrderImageOptions.Add(imageOption);
                this.Subject.FlowProjectDataContext.OrderImageOptions.InsertOnSubmit(imageOption);
                this._orderImageOptionList = null;
                this.SendPropertyChanged("OrderImageOptionList");
                this.Subject.UpdateOrderImageOptionList();
                return imageOption;
            }
            return null;

        }

        internal void RemoveCatalogOption(OrderImageOption orderImageOption)
        {
            if (this.OrderImageOptions.Contains(orderImageOption))
            {
                this.FlowProjectDataContext.OrderImageOptions.DeleteOnSubmit(orderImageOption);
                this.FlowProjectDataContext.SubmitChanges();
                this._orderImageOptionList = null;
                this.SendPropertyChanged("OrderImageOptionList");
                this.Subject.UpdateOrderImageOptionList();
            }
        }


        public string GetDropoutImagePath(bool renderIfMissing)
        {
            if (string.IsNullOrEmpty(GreenScreenSettings))
                return this.ImagePath;

            //get the path where we can expect to find the dropout png file
            string dropoutImagePath = Path.Combine(FlowContext.FlowTempDirPath,  this.ImageFileName.Replace((new FileInfo(ImagePath)).Extension, ".png")); //needs to be path to png
            
            //is the png there?
            if (!File.Exists(dropoutImagePath) && renderIfMissing)
            {
                var selectionConfig = SelectionConfiguration.FromXmlStr(GreenScreenSettings);
                Bitmap dropOutBmp = GreenScreenAlgorithm.CreateDropoutImage(new Bitmap(this.ImagePath), selectionConfig);
               dropOutBmp.Save(dropoutImagePath);
            };

            return dropoutImagePath;
        }

        public Bitmap GetDropoutImage(Image img)
        {
            if (String.IsNullOrEmpty(GreenScreenSettings))
                return new Bitmap(img);

            //get the path where we can expect to find the dropout png file
            string dropoutImagePath = Path.Combine(FlowContext.FlowTempDirPath, this.ImageFileName.Replace((new FileInfo(ImagePath)).Extension, ".png")); //needs to be path to png

            //is the png there?
           // if (!File.Exists(dropoutImagePath) && renderIfMissing)
           // {
            var selectionConfig = SelectionConfiguration.FromXmlStr(GreenScreenSettings);
            Bitmap dropOutBmp = GreenScreenAlgorithm.CreateDropoutImage(new Bitmap(img), selectionConfig);
             dropOutBmp.Save(dropoutImagePath);
           // };

                return dropOutBmp;
        }

        public string GetBasicDropoutImagePath(bool renderIfMissing)
        {
            if (string.IsNullOrEmpty(GreenScreenSettings))
                return this.ImagePathFullRes;

            //get the path where we can expect to find the dropout png file
            string dropoutImagePath = Path.Combine(FlowContext.FlowTempDirPath, this.ImageFileName.Replace((new FileInfo(ImagePath)).Extension, ".png")); //needs to be path to png

            //is the png there?
            if (!File.Exists(dropoutImagePath) && renderIfMissing)
            {
                //var selectionConfig = SelectionConfiguration.FromXmlStr(GreenScreenSettings);
                Bitmap dropOutBmp = GreenScreenAlgorithm.RenderDropOutToBitmap(new Bitmap(this.ImagePath), GreenScreenSettings, null);
                dropOutBmp.Save(dropoutImagePath);
            };

            return dropoutImagePath;
        }


        public Bitmap GetBasicDropoutImagePath(float dpi)
        {
            if (string.IsNullOrEmpty(GreenScreenSettings))
                return null;

            //get the path where we can expect to find the dropout png file
            //string dropoutImagePath = Path.Combine(FlowContext.FlowTempDirPath, this.ImageFileName.Replace((new FileInfo(ImagePath)).Extension, ".png")); //needs to be path to png

            //is the png there?
            //if (!File.Exists(dropoutImagePath) && renderIfMissing)
            //{
                //var selectionConfig = SelectionConfiguration.FromXmlStr(GreenScreenSettings);
                Bitmap dropOutBmp = GreenScreenAlgorithm.RenderDropOutToBitmap(new Bitmap(this.ImagePathFullRes), GreenScreenSettings, null);
                dropOutBmp.SetResolution(dpi, dpi);
               // dropOutBmp.Save(dropoutImagePath);
            //};

                return dropOutBmp;
        }


        public void UpdateGS()
        {
            this.SendPropertyChanged("IsGreenscreen");
        }

        internal void MergeImageData(SubjectImage incomingImage, bool forceMerge)
        {
            if (forceMerge || (incomingImage.DateModified != null && (this.DateModified == null || (incomingImage.DateModified > this.DateModified))))
            {
                //merge the fields from incoming to this image
                this.CropH = incomingImage.CropH;
                this.CropL = incomingImage.CropL;
                this.CropT = incomingImage.CropT;
                this.CropW = incomingImage.CropW;
                this.DateModified = incomingImage.DateModified;
                this.ExistsInOnlineGallery = incomingImage.ExistsInOnlineGallery;
                this.GreenScreenBackground = incomingImage.GreenScreenBackground;
                this.GreenScreenSettings = incomingImage.GreenScreenSettings;
                this.GroupImageGuid = incomingImage.GroupImageGuid;
                this.ImageFileName = incomingImage.ImageFileName;
                this.ImageOriginalFileName = incomingImage.ImageOriginalFileName;
                this.IsPrimary = incomingImage.IsPrimary;
                this.Orientation = incomingImage.Orientation;
                this.SourceComputer = incomingImage.SourceComputer;
                this.RotationAngle = incomingImage.RotationAngle;

                //this.Subject.FlagForMerge = true;
            }
        }

        internal void UpdateExifOrientation()
        {
            this.Orientation = FlowImage.UpdateOrientation();
        }

        public bool IsPng { get { if (ImagePathFullRes.ToLower().EndsWith("png")) return true; else return false; } }

        public string RenderTempImage(string OutputDir, bool ApplyCrop, bool renderGreenScreen, string gsBackground, bool returnPng, long jpgQuality, float dpi, bool UseImageNameBGIndex)
        {

            int tryCount = 0;
            while (tryCount < 5)
            {
                try
                {
                    //determin destFileName
                    string destFileName = this.ImageFileName;
                    if (returnPng)
                        destFileName = destFileName.Replace(".JPG", ".png").Replace(".jpg", ".png");
                    string destFile = Path.Combine(OutputDir, destFileName);



                    var gsBackgrounds = this.Subject.FlowProjectDataContext.FlowMasterDataContext.GreenScreenBackgrounds;

                    //get a number to represent the background selected
                    int bkgrnIndex = 0;
                    if (!string.IsNullOrEmpty(gsBackground))
                    {
                        try
                        {
                            bkgrnIndex = gsBackgrounds.IndexOf(gsBackgrounds.First(g => g.FileName == ((new FileInfo(gsBackground)).Name)));
                        }
                        catch
                        {
                            //ignore errors, just use bkgrnIndex = 0;
                        }
                    }

                    //add index to dest file name
                    string extension = Path.GetExtension(destFile);
                    string pathName = Path.GetDirectoryName(destFile);
                    string fileNameOnly = Path.Combine(pathName, Path.GetFileNameWithoutExtension(destFile));

                    //sometimes we dont want a unique bgIndex in the filename
                    string nameWithBG = fileNameOnly + extension;
                    if (UseImageNameBGIndex)
                        nameWithBG = fileNameOnly + "_" + bkgrnIndex + extension;

                    System.Drawing.Imaging.ImageCodecInfo jpgEncoder = GetEncoder(System.Drawing.Imaging.ImageFormat.Jpeg);
                    System.Drawing.Imaging.Encoder myEncoder = System.Drawing.Imaging.Encoder.Quality;
                    System.Drawing.Imaging.EncoderParameters myEncoderParameters = new System.Drawing.Imaging.EncoderParameters(1);
                    System.Drawing.Imaging.EncoderParameter myEncoderParameter = new System.Drawing.Imaging.EncoderParameter(myEncoder, jpgQuality);
                    myEncoderParameters.Param[0] = myEncoderParameter;

                    //string sourceImage = this.ImagePathFullRes;

                    //create Output Dir
                    if (!Directory.Exists(OutputDir))
                        Directory.CreateDirectory(OutputDir);

                    if (File.Exists(destFile)) File.Delete(destFile);

                    if (this.IsPng && returnPng)
                        File.Copy(this.ImagePathFullRes, destFile);
                    else if (renderGreenScreen && (!string.IsNullOrEmpty(this.GreenScreenSettings) || IsPng))
                    {

                        Bitmap bpng;
                        if (IsPng)
                        {
                            bpng = new Bitmap(this.ImagePathFullRes);
                            bpng.SetResolution(dpi, dpi);
                            bpng = GetRotatedBmp(bpng);

                        }
                        else
                        {
                            //pathTopng = GetBasicDropoutImagePath(true);
                            //bpng = GetBasicDropoutImagePath(dpi);
                            string pathTopng = RenderGS(nameWithBG, gsBackground);
                            bpng = new Bitmap(pathTopng);
                            bpng.SetResolution(dpi, dpi);
                        }



                        if (ApplyCrop)
                        {
                            bpng = GetCroppedBmp(bpng);
                            //string pathToCroppedPng = pathTopng.Replace(".png", "_cropped.png");
                            //croppedbmp.Save(pathToCroppedPng, System.Drawing.Imaging.ImageFormat.Png);
                            //pathTopng = pathToCroppedPng;
                        }

                        //else
                        //{
                        //    if (!IsPng)
                        //    {
                        //        Bitmap rotbmp = GetRotatedBmp(pathTopng);
                        //        rotbmp.Save(pathTopng, System.Drawing.Imaging.ImageFormat.Png);
                        //    }
                        //}

                        if (returnPng)
                            bpng.Save(destFile, System.Drawing.Imaging.ImageFormat.Png);
                        else
                        {
                            //add bg to png and call it a jpg



                            destFile = Path.Combine(pathName, nameWithBG);

                            //Bitmap image = bpng;
                            Bitmap bg = new Bitmap(gsBackground);
                            var target = new Bitmap(bpng.Width, bpng.Height);
                            target.SetResolution(dpi, dpi);
                            var graphics = Graphics.FromImage(target);

                            graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.High;
                            graphics.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                            graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

                            float scale = Math.Min(bpng.Width / bg.Width, bpng.Height / bg.Height);
                            var scaleWidth = (int)(bg.Width * scale);
                            var scaleHeight = (int)(bg.Height * scale);

                            graphics.DrawImage(bg, new Rectangle(0, 0, bpng.Width, bpng.Height));
                            graphics.DrawImage(bpng, 0, 0);

                            bpng.Dispose();

                            target.Save(destFile, jpgEncoder, myEncoderParameters);


                        }



                    }
                    else
                    {
                        if (ApplyCrop)
                        {
                            Bitmap croppedbmp = GetCroppedBmp(GetRotatedBmp(new Bitmap(this.ImagePathFullRes)));
                            croppedbmp.Save(destFile, jpgEncoder, myEncoderParameters);
                        }
                        else
                        {

                            File.Copy(this.ImagePathFullRes, destFile);
                        }
                    }

                    return destFile;
                }
                catch (Exception ex)
                {
                    tryCount++;
                    if (tryCount > 5)
                        throw ex;
                    Thread.Sleep(1000);
                }
            }
            return "";
        }


        private System.Drawing.Imaging.ImageCodecInfo GetEncoder(System.Drawing.Imaging.ImageFormat format)
        {
            System.Drawing.Imaging.ImageCodecInfo[] codecs = System.Drawing.Imaging.ImageCodecInfo.GetImageDecoders();
            foreach (System.Drawing.Imaging.ImageCodecInfo codec in codecs)
            {
                if (codec.FormatID == format.Guid)
                {
                    return codec;
                }
            }
            return null;
        }

        private Bitmap GetRotatedBmp(Bitmap img)
        {
            //Bitmap img = new Bitmap(inimg);
            if (this.Orientation == 90)
                img.RotateFlip(RotateFlipType.Rotate90FlipNone);
            if (this.Orientation == 180)
                img.RotateFlip(RotateFlipType.Rotate180FlipNone);
            if (this.Orientation == 270)
                img.RotateFlip(RotateFlipType.Rotate270FlipNone);

            return img;
        }

        private Bitmap GetCroppedBmp(Bitmap img)
        {
            if (((this.CropW > 0) || (this.CropH > 0)))
            {
                img = cropImage(img, getCropArea(img, this.CropL, this.CropT, this.CropH, this.CropW));
            }
            return img;
        }

        private static Bitmap cropImage(Bitmap bmpImage, System.Drawing.Rectangle cropArea)
        {
            //Bitmap bmpImage = new Bitmap(img);
            Bitmap bmpCrop = bmpImage.Clone(cropArea,
            bmpImage.PixelFormat);

            bmpCrop.SetResolution(bmpImage.HorizontalResolution, bmpImage.VerticalResolution);
            bmpImage.Dispose();
           
            return bmpCrop;
        }
        private System.Drawing.Rectangle getCropArea(System.Drawing.Image iSrc, double CropL, double CropT, double CropH, double CropW)
        {
            double width = iSrc.Width;
            double height = iSrc.Height;

            int cropAreaL = (int)(CropL * width);
            int cropAreaT = (int)(CropT * height);
            int cropAreaW = (int)(CropW * width);
            int cropAreaH = (int)(CropH * height);

            if (cropAreaL + cropAreaW > width)
                cropAreaW = (int)width - cropAreaL;
            if (cropAreaT + cropAreaH > height)
                cropAreaH = (int)height - cropAreaT;


            return new System.Drawing.Rectangle(
                cropAreaL,
                cropAreaT,
                cropAreaW,
                cropAreaH
                );
        }
       

        public string GetOrderedBackground()
        {
            if (Subject.SubjectOrder != null)
            {
                foreach (OrderPackage op in Subject.SubjectOrder.OrderPackages)
                {
                    if (op.FirstImage != null && op.FirstImage.SubjectImageID == this.SubjectImageID)
                       return op.DefaultBackgound.FullPath;
                }
            }
            return null;
        }
    }
}
