﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Flow.Lib.Helpers;
using System.IO;
using Flow.Schema.LinqModel.DataContext.FlowMaster;

namespace Flow.Schema.LinqModel.DataContext
{
	partial class OrderPackage
	{
        public bool ShipSeparately
        {
            get
            {
                if (this.ShipmentType == "direct")
                    return true;
                if (this.SubjectOrder.OrderSubjectOrderOptions.Count > 0)
                    return true;

                return false;
            }
        }

        public bool IncludeInOrderChecked { get; set; }

        public SubjectImage SubjectImage
        {
            get
            {
                if (this.SubjectImageID != null)
                    return this.SubjectOrder.Subject.FlowProjectDataContext.ImageList.FirstOrDefault(i => i.SubjectImageID == this.SubjectImageID);
                else
                    return null;

             }

        }
        public SubjectImage FirstImage
        {
            get
            {
                if (this.OrderProducts.Any() && this.OrderProducts.First().OrderProductNodes.Any(node => node.SubjectImage != null))
                    return this.OrderProducts.First().OrderProductNodes.First(node => node.SubjectImage != null).SubjectImage;
                else
                {
                    if (this.OrderProducts.Count >= 2 && this.OrderProducts[1].OrderProductNodes.Any(node => node.SubjectImage != null))
                        return this.OrderProducts[1].OrderProductNodes.First(node => node.SubjectImage != null).SubjectImage;
                    else
                    {
                        if (this.OrderProducts.Count >= 3 && this.OrderProducts[2].OrderProductNodes.Any(node => node.SubjectImage != null))
                            return this.OrderProducts[1].OrderProductNodes.FirstOrDefault(node => node.SubjectImage != null).SubjectImage;
                        else
                        {
                            if (this.SubjectImageID != null && this.SubjectOrder != null)
                                return this.SubjectOrder.Subject.FlowProjectDataContext.ImageList.FirstOrDefault(i=>i.SubjectImageID == this.SubjectImageID);
                            else
                                return null;
                        }
                    }
                }

                
            }
        }
        public void UpdateFirstImage()
        {
            this.SendPropertyChanged("FirstImage");
        }
        private ProductPackage _productPackage { get; set; }
		public ProductPackage ProductPackage { 
            get{
                 if(_productPackage == null)
                 {
                     if (this.SubjectOrder != null && this.SubjectOrder.Subject.FlowProjectDataContext.FlowMasterDataContext.ProductPackages.Any(pp => pp.ProductPackageGuid == this.ProductPackageGuid))
                         _productPackage = this.SubjectOrder.Subject.FlowProjectDataContext.FlowMasterDataContext.ProductPackages.First(pp => pp.ProductPackageGuid == this.ProductPackageGuid);
                     else if (this.SubjectOrder != null && this.SubjectOrder.Subject.FlowProjectDataContext.FlowProject.FlowCatalog != null && this.SubjectOrder.Subject.FlowProjectDataContext.FlowProject.FlowCatalog.ProductPackages.Count(pp => pp.ProductPackageDesc.ToLower() == this.ProductPackageDesc.ToLower()) == 1)
                         _productPackage = this.SubjectOrder.Subject.FlowProjectDataContext.FlowProject.FlowCatalog.ProductPackages.First(pp => pp.ProductPackageDesc.ToLower() == this.ProductPackageDesc.ToLower());
                     else
                     {
                         //throw new Exception("Cant locate Package in flow catalog: " + this.ProductPackageDesc);
                     }
                 }
                    return _productPackage;
            }
            
            internal set{_productPackage = value;} 
        }

        public void DefaultBackgroundChanged()
        {
            this.SendPropertyChanged("DefaultBackgound");
        }


        //private OrderProduct _fristOrderProductCopy {get;set;}
        public OrderProduct FirstOrderProductCopy { get 
        {
            if (this.OrderProductList.Count > 0)
            {
                OrderProduct temp = this.OrderProductList.First().Clone();
                temp.TempOrderPackage = this;
                return temp;
            }
            else return null;
        } }

        public GreenScreenBackground DefaultBackgound
        {
            get{

                if (this.OrderProducts.Count > 0)
                    if (this.OrderProducts[0].GreenScreenBackground != null)
                        if (File.Exists(this.OrderProducts[0].GreenScreenBackground))
                            return new GreenScreenBackground(this.OrderProducts[0].GreenScreenBackground);
                if (this.SubjectOrder == null)
                    return null;

                return this.SubjectOrder.Subject.FlowProjectDataContext.FlowProject.DefaultGreenScreenBackgroundObject;
                
            }
        }

        public string OrderHistoryText{
            get{ 
                string tmpText = "Order History:";
                //if (this.OrderHistories.Count() > 0)
                //{
                //    foreach (OrderHistory oh in this.OrderHistories)
                //    {
                //        tmpText = tmpText + "\n" + oh.OrderID + " - " + oh.OrderSubmittedDate.ToShortDateString();
                //    }
                    
                //}
                if(this.LabOrderID != null)
                    tmpText = tmpText + "\n" + this.LabOrderID + " - " + ((DateTime)this.LabOrderDate).ToShortDateString() + " - " + ShipmentType;
                else
                {
                    tmpText = "NEW ORDER - " + ShipmentType;
                }

                return tmpText;
            }
        }

        public FlowObservableCollection<OrderProduct> _orderProductList;
		public FlowObservableCollection<OrderProduct> OrderProductList
		{
			get
			{
				//if (_orderProductList == null)
				//{
					_orderProductList = new FlowObservableCollection<OrderProduct>(this.OrderProducts);

					if (this.ProductPackage != null)
					{
						foreach (OrderProduct orderProduct in _orderProductList)
						{
							ProductPackageComposition composition =
								this.ProductPackage.ProductPackageCompositionList
									.FirstOrDefault(c => c.ImageQuixProduct.PrimaryKey == orderProduct.ImageQuixProductPk);

                            if (composition != null)
                            {
                                orderProduct.OrderProductLabel = composition.ImageQuixProduct.Label;
                                orderProduct.OrderProductResourceURL = composition.ImageQuixProduct.ResourceURL;
                            }
                            else
                            {
                                orderProduct.OrderProductLabel = "{ImageQuix Product not found}";
                                orderProduct.OrderProductResourceURL = "{ImageQuix Product RescourceURL not found}";
                            }
						}
					}
				//}

				return _orderProductList;
			}
		}

		private decimal _taxRate = 0;
		internal decimal TaxFactor
		{
			get { return _taxRate; }
			set { _taxRate = 1 + (value / 100); }
		}
	
		public decimal ExpandedPrice
		{
			get
			{
                decimal returnPrice = 0;
				if (this.ProductPackage != null)
					returnPrice += this.ProductPackage.Price;

                returnPrice += GreenScreenBackgroundPrice;
                return returnPrice;
			}
		}

        public decimal GreenScreenBackgroundPrice
        {
            get
            {
                decimal gsPrice = 0;

                //only charge once per premium backgound
                List<string> chargedBackgrounds = new List<string>();
                foreach (OrderProduct oprod in this.OrderProducts)
                {
                    if (oprod.GreenScreenBackgroundObject != null && oprod.GreenScreenBackgroundObject.IsPremium)
                    {
                        if (!chargedBackgrounds.Contains(oprod.GreenScreenBackgroundObject.DisplayName))
                        {
                            chargedBackgrounds.Add(oprod.GreenScreenBackgroundObject.DisplayName);
                            gsPrice += this.SubjectOrder.Subject.FlowProjectDataContext.FlowMasterDataContext.PreferenceManager.Edit.PremiumBackgroundPriceDecimal;
                        }
                    }
                }
                return gsPrice;
            }
        }

        public void ReCalculatePrice()
        {
            this.SendPropertyChanged("ExpandedPrice");
            this.SendPropertyChanged("ExpandedPriceDisplayText");
        }

		public decimal TaxedExpandedPrice
		{
			get
			{
                decimal returnPrice = 0;
				if (this.ProductPackage != null && !this.ProductPackage.Taxed)
					returnPrice += this.ExpandedPrice;
                else
                    returnPrice += (this.ExpandedPrice * this.TaxFactor);
                return returnPrice;
			}
		}

		public string ExpandedPriceDisplayText
		{
			get
			{
                decimal returnPrice = 0;
                if (this.ProductPackage != null)
                    returnPrice += this.ProductPackage.Price;

                returnPrice += GreenScreenBackgroundPrice;

                return 1.ToString() + " @ " + returnPrice.CurrencyFormat("$");
			}
		}

		public string TaxDisplayText
		{
			get { return "(x " + (this.TaxFactor - 1).ToString() + ") Tax"; }
		}

        public OrderPackage(ProductPackage productPackage, SubjectOrder subjectOrder)
            : this(productPackage, subjectOrder, "", null, null) { }


        public OrderPackage(ProductPackage productPackage, SubjectOrder subjectOrder, string PackageImageType, SubjectImage subImage, string background)
			: this()
		{
            if(subImage != null)
                this.SubjectImageID = subImage.SubjectImageID;
			this.ProductPackage = productPackage;
			this.ProductPackageGuid = productPackage.ProductPackageGuid;
			this.ProductPackageDesc = productPackage.ProductPackageDesc;
            if (this.ShipmentType == null)
                this.ShipmentType = "pickup";

            if (subjectOrder.Directship == true)
            {
                this.ShipmentType = "direct";
                this.ShippingFirstName = subjectOrder.ShippingName;
                this.ShippingAddress = subjectOrder.ShippingAddress;
                this.ShippingCity = subjectOrder.ShippingCity;
                this.ShippingState = subjectOrder.ShippingState;
                this.ShippingZip = subjectOrder.ShippingZipCode;
            }



            this.AddDate = DateTime.Now;
            this.ModifyDate = DateTime.Now;

			foreach (ProductPackageComposition composition in productPackage.ProductPackageCompositionList)
			{
				ImageQuixProduct product = composition.ImageQuixProduct;

                OrderProduct orderProduct = new OrderProduct(product, subjectOrder, PackageImageType, subImage, composition.DisplayLabel, composition.ImageQuixProduct.ResourceURL);
				orderProduct.Quantity = composition.Quantity;

                if (!string.IsNullOrEmpty(background))
                    orderProduct.GreenScreenBackground = background;

				this.OrderProductList.Add(orderProduct);

				foreach (ImageQuixOptionGroup optionGroup in product.OptionGroupList.Where(g => g.OptionList.HasItems))
				{
					orderProduct.OrderProductOptionList.Add(new OrderProductOption(optionGroup));
				}
			}
            this.Qty = 1;
		}

        internal OrderPackage Duplicate()
		{
			OrderPackage duplicateOrderPackage = new OrderPackage(this.ProductPackage, this.SubjectOrder);
            duplicateOrderPackage.AddDate = DateTime.Now;
            return duplicateOrderPackage;
		}

        internal void RemoveOrderHistory(FlowProjectDataContext flowProjectDataContext)
        {
            flowProjectDataContext.OrderHistories.DeleteAllOnSubmit(this.OrderHistories);
            this.OrderHistories.Clear();
        }

		internal void RemovePackageProducts(FlowProjectDataContext flowProjectDataContext)
		{
            flowProjectDataContext.SubmitChanges();
			foreach (OrderProduct orderProduct in this.OrderProducts)
			{
				orderProduct.RemoveChildren(flowProjectDataContext);
			}
            flowProjectDataContext.SubmitChanges();
            flowProjectDataContext.OrderProducts.DeleteAllOnSubmit(this.OrderProducts);
            flowProjectDataContext.SubmitChanges();
			//flowProjectDataContext.OrderProducts.DeleteAllOnSubmit(this.OrderProducts);
			this.OrderProducts.Clear();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="subjectImage"></param>
		internal void ReplaceUnassignedImage(SubjectImage deletionImage, SubjectImage replacementImage)
		{
			foreach(OrderProduct orderProduct in this.OrderProductList)
				orderProduct.ReplaceUnassignedImage(deletionImage, replacementImage);
		}


		#region MERGE MEMBERS

		private Guid _subjectOrderGuid;
		internal Guid SubjectOrderGuid
		{
			get { return _subjectOrderGuid; }
			set
			{
				_subjectOrderGuid = value;

				foreach (OrderProduct orderProduct in this.OrderProducts)
				{
					orderProduct.OrderPackageGuid = this.ProductPackageGuid;
				}
			}
		}

		/// <summary>
		/// Returns a clone of the source OrderPackage object
		/// </summary>
		/// <returns></returns>
		internal OrderPackage Clone()
		{
			OrderPackage clone = new OrderPackage();

			clone.ProductPackageGuid = this.ProductPackageGuid;
			clone.ProductPackageDesc = this.ProductPackageDesc;
            clone.AddDate = this.AddDate;
            clone.IQOrderDate = this.IQOrderDate;
            clone.IQOrderID = this.IQOrderID;
            clone.WebOrderID = this.WebOrderID;
            clone.WebOrderAuthorizationToken = this.WebOrderAuthorizationToken;
            clone.WebOrderDate = this.WebOrderDate;
            clone.IsALaCarte = this.IsALaCarte;
            clone.IsOnlineOrder = this.IsOnlineOrder;
            clone.LabOrderDate = this.LabOrderDate;
            clone.LabOrderID = this.LabOrderID;
            clone.ModifyDate = this.ModifyDate;
            clone.ShipmentType = this.ShipmentType;
            clone.ShippingAddress = this.ShippingAddress;
            clone.ShippingCity = this.ShippingCity;
            clone.ShippingCountry = this.ShippingCountry;
            clone.ShippingFirstName = this.ShippingFirstName;
            clone.ShippingLastName = this.ShippingLastName;
            clone.ShippingState = this.ShippingState;
            clone.ShippingZip = this.ShippingZip;
            clone.Notes = this.Notes;
            clone.Units = this.Units;
            clone.SubjectImageID = this.SubjectImageID;
            clone.GreenScreenBackground = this.GreenScreenBackground;
            clone.Qty = this.Qty;

			foreach (OrderProduct sourceProduct in this.OrderProductList)
			{
				OrderProduct rootProduct = sourceProduct.Clone();

				clone.OrderProducts.Add(rootProduct);
			}

			return clone;
		}

		#endregion


        public string IQOrderDateShort{

            get{
                return ((DateTime)IQOrderDate).ToShortDateString();

        }
        }

        public string OrderType
        {
            get
            {
                if (this.WebOrderID != null || this.IQOrderID != null)
                    return "Web";
                else
                    return "Manual";
            }
        }

        public void UpdateShipmentType()
        {
            this.SendPropertyChanged("OrderHistoryText");
        }

        public void UpdatePackageOptions()
        {
            
            this.SendPropertyChanged("FirstOrderProductCopy");
        }
    }
}
