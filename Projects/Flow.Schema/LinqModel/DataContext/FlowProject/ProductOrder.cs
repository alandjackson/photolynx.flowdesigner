﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;

namespace Flow.Schema.LinqModel.DataContext
{
	partial class ProductOrder
	{
		internal FlowCatalog FlowCatalog { get; set; }

		private FlowObservableCollection<OrderItem> _orderItemList;
		public FlowObservableCollection<OrderItem> OrderItemList
		{
			get
			{
				if (_orderItemList == null)
				{
					_orderItemList = new FlowObservableCollection<OrderItem>(this.OrderItems);

					foreach (OrderItem item in this.OrderItemList)
					{
						item.ProductPackage = this.FlowCatalog.ProductPackageList
							.FirstOrDefault(i => i.ProductPackageID == item.ProductPackageID);

						item.TaxFactor = this.FlowCatalog.TaxRate;
					}

					_orderItemList.CollectionChanged += new NotifyCollectionChangedEventHandler(OrderItemList_CollectionChanged);
				}

				return _orderItemList;
			}
		}

		private decimal _orderSubTotal = 0;
		public decimal OrderSubTotal
		{
			get { return _orderSubTotal; }
			set	
			{
				_orderSubTotal = value;
				this.SendPropertyChanged("OrderSubTotal");
			}
		}

		public decimal OrderGrandTotal
		{
			get
			{
				decimal orderGrandTotal = this.OrderItemList.Sum(op => op.TaxedExpandedPrice);

				this.OrderSubTotal = this.OrderItemList.Sum(op => op.ExpandedPrice);
	
				this.TaxTotal = orderGrandTotal - this.OrderSubTotal;

				return orderGrandTotal;
			}
		}

		private decimal _taxTotal = 0;
		public decimal TaxTotal
		{
			get { return _taxTotal; }
			set
			{
				_taxTotal = value;
				this.SendPropertyChanged("TaxTotal");
			}
		}

		public decimal BalanceDue
		{
			get { return this.OrderGrandTotal - ((this.PaymentAmount.HasValue) ? this.PaymentAmount.Value : 0); }
		}

		public ProductOrder(
			FlowCatalog flowCatalog,
			int subjectID
		) : this()
		{
			this.FlowCatalog = flowCatalog;
			this.SubjectID = subjectID;
			this.ProductOrderDate = DateTime.Now;
			this.PaymentAmount = 0;
	
			if(this.OrderItemList == null) { };
		}

		void OrderItemList_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
		{
		}

		partial void OnPaymentAmountChanged()
		{
			this.SendPropertyChanged("BalanceDue");
		}

		internal void AddItem(ProductPackage productPackage)
		{
			OrderItem orderItem = this.OrderItemList
				.SingleOrDefault(oi => oi.ProductPackageID == productPackage.ProductPackageID);

			if (orderItem == null)
			{
				orderItem = new OrderItem(productPackage);
				this.OrderItemList.Add(orderItem);

				// Check for assignment of the FlowCatalog if not pree-assigned, and assign
				if (this.FlowCatalog == null)
					this.FlowCatalog = productPackage.FlowCatalog;

				orderItem.TaxFactor = this.FlowCatalog.TaxRate;
			}
			else
				orderItem.Quantity++;

			this.SendPropertyChanged("OrderGrandTotal");
		}

		internal void RemoveItem(ProductPackage productPackage, FlowProjectDataContext flowProjectDataContext, bool clearItems)
		{
			OrderItem orderItem = this.OrderItemList
				.SingleOrDefault(oi => oi.ProductPackageID == productPackage.ProductPackageID);

			if (orderItem == null)
				return;
	
			if (clearItems || orderItem.Quantity == 1)
			{
				flowProjectDataContext.OrderItems.DeleteOnSubmit(orderItem);
				this.OrderItemList.Remove(orderItem);
			}
			else
				orderItem.Quantity--;

			this.SendPropertyChanged("OrderGrandTotal");
		}




	}
}
