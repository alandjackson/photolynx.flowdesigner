﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.Linq;
using System.Data.SqlServerCe;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Media.Imaging;

using Flow.Lib;
using Flow.Lib.Database.SqlCe;
using Flow.Lib.Helpers;
using Flow.Schema.Properties;
using Flow.Schema.Utility;


namespace Flow.Schema.LinqModel.DataContext
{
	/// <summary>
	/// Flow project metadata; contains a single row maintaining  project-level references from the
	/// application database (FlowMaster)
	/// </summary>
	partial class FlowProjectLocal
	{
		public FlowProjectLocal(FlowProject flowProject)
		{
			this.FlowProjectID = flowProject.FlowProjectID;
			this.FlowProjectGuid = flowProject.FlowProjectGuid;
			this.FlowProjectTemplateID = flowProject.FlowProjectTemplateID;
			this.OrganizationID = flowProject.OrganizationID;
			this.OwnerUserID = flowProject.OwnerUserID;
			this.ImageSequenceID = 0;
            this._FlowProjectTemplateGuid = flowProject.FlowProjectTemplateGuid;
            this._FlowProjectTemplateShortDescription = flowProject.FlowProjectTemplateShortDescription;
		}
	}
}
