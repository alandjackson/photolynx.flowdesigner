﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.Linq;
using System.Data.SqlServerCe;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Media.Imaging;

using Flow.Lib;
using Flow.Lib.Database.SqlCe;
using Flow.Lib.Helpers;
using Flow.Schema.Properties;
using Flow.Schema.Utility;
using System.Drawing;
using System.Windows.Media;
using System.Windows;
using System.Windows.Threading;
using Flow.Lib.AsyncWorker;


namespace Flow.Schema.LinqModel.DataContext
{
	/// <summary>
	/// Represents an image (file) assigned to a Subject
	/// </summary>
	public partial class Group
	{

        private FlowProjectDataContext _flowProjectDataContext = null;
        public FlowProjectDataContext FlowProjectDataContext
        {
            get { return _flowProjectDataContext; }
            set { _flowProjectDataContext = value; }
        }

        public Group(FlowProjectDataContext flowProjectDataContext)
		{
            _flowProjectDataContext = flowProjectDataContext;
		}

        private List<Subject> _subjects { get; set; }
        public List<Subject> Subjects
        {
            get
            {
                _subjects = new List<Subject>();
                foreach (GroupSubject gs in this._flowProjectDataContext.GroupSubjects.Where(gs => gs.GroupGuid == this._GroupGuid))
                {
                    Subject s = _flowProjectDataContext.SubjectList.FirstOrDefault(su => su.SubjectGuid == gs.SubjectGuid);
                    if (s != null) _subjects.Add(s);
                }
                return _subjects;
            }
        }

        private List<GroupImage> _groupImages { get; set; }
        public List<GroupImage> GroupImages
        {
            get
            {
                _groupImages = new List<GroupImage>();
                foreach (GroupGroupImage gi in this._flowProjectDataContext.GroupGroupImages.Where(gi => gi.GroupGuid == this._GroupGuid))
                {
                    GroupImage g = _flowProjectDataContext.GroupImageList.FirstOrDefault(gig => gig.GroupImageGuid == gi.GroupImageGuid);
                    if (g != null) _groupImages.Add(g);
                }
                return _groupImages;
            }
        }

        public void AssignGroupImage(GroupImage gImage)
        {
            if (!_flowProjectDataContext.GroupGroupImages.Any(ggi => ggi.GroupGuid == this._GroupGuid && ggi.GroupImageGuid == gImage.GroupImageGuid))
            {
                GroupGroupImage g = new GroupGroupImage();
                g.GroupImageGuid = (Guid)gImage.GroupImageGuid;
                g.GroupGuid = (Guid)this._GroupGuid;
                _flowProjectDataContext.GroupGroupImages.InsertOnSubmit(g);
                _flowProjectDataContext.SubmitChanges();
                this.SendPropertyChanged("GroupImages");
                UpdateGroupSubjects();
            }
        }

        public void AssignSubject(Subject sub)
        {
            if (!_flowProjectDataContext.GroupSubjects.Any(gs => gs.GroupGuid == this._GroupGuid && gs.SubjectGuid == sub.SubjectGuid))
            {
                GroupSubject s = new GroupSubject();
                s.SubjectGuid = (Guid)sub.SubjectGuid;
                s.GroupGuid = (Guid)this._GroupGuid;
                _flowProjectDataContext.GroupSubjects.InsertOnSubmit(s);
                AddImagesToSubject(sub);
                if (string.IsNullOrEmpty(this.Attribute1) && sub.SubjectData["Class"].Value != null)
                    this.Attribute1 = sub.SubjectData["Class"].Value as String;
                if (string.IsNullOrEmpty(this.Attribute2) && sub.SubjectData["Teacher"].Value != null)
                    this.Attribute2 = sub.SubjectData["Teacher"].Value as String;
                else if (!string.IsNullOrEmpty(this.Attribute2) && sub.SubjectData["Teacher"].Value != null && this.Attribute2 != sub.SubjectData["Teacher"].Value as String)
                    this.Attribute2 += ", " + sub.SubjectData["Teacher"].Value as String;

                _flowProjectDataContext.SubmitChanges();
                this.SendPropertyChanged("Subjects");
            }
        }

        public void RemoveSubject(Subject sub)
        {
            if (_flowProjectDataContext.GroupSubjects.Any(gs => gs.GroupGuid == this._GroupGuid && gs.SubjectGuid == sub.SubjectGuid))
            {
                GroupSubject s = _flowProjectDataContext.GroupSubjects.FirstOrDefault(gs => gs.GroupGuid == this._GroupGuid && gs.SubjectGuid == sub.SubjectGuid);
                if(s!=null)
                {
                    RemoveImagesFromSubject(sub);
                    _flowProjectDataContext.GroupSubjects.DeleteOnSubmit(s);
                    _flowProjectDataContext.SubmitChanges();
                    this.SendPropertyChanged("Subjects");
                }
            }
        }

        public void RemoveGroupImage(GroupImage gi)
        {
            if (_flowProjectDataContext.GroupGroupImages.Any(ggi => ggi.GroupGuid == this._GroupGuid && ggi.GroupImageGuid == gi.GroupImageGuid))
            {
                GroupGroupImage s = _flowProjectDataContext.GroupGroupImages.FirstOrDefault(ggi => ggi.GroupGuid == this._GroupGuid && ggi.GroupImageGuid == gi.GroupImageGuid);
                if (s != null)
                {
                    RemoveImageFromAllSubjects(gi);
                    _flowProjectDataContext.GroupGroupImages.DeleteOnSubmit(s);
                    _flowProjectDataContext.SubmitChanges();
                    this.SendPropertyChanged("GroupImages");
                }
            }
        }

        public void UpdateGroupSubjects()
        {
            foreach (Subject s in this.Subjects)
            {
                AddImagesToSubject(s);
            }
        }

        private void RemoveImageFromAllSubjects(GroupImage g)
        {
            foreach (Subject s in Subjects)
            {
                s.DetachImage(g.ImageFileName);
            }
        }

        private void RemoveImagesFromSubject(Subject s)
        {
            foreach (GroupGroupImage gi in this._flowProjectDataContext.GroupGroupImages.Where(gi => gi.GroupGuid == this._GroupGuid))
            {
                GroupImage g = _flowProjectDataContext.GroupImageList.FirstOrDefault(gig => gig.GroupImageGuid == gi.GroupImageGuid);
                s.DetachImage(g.ImageFileName);
            }
        }
        private void AddImagesToSubject(Subject s)
        {
            foreach (GroupGroupImage gi in this._flowProjectDataContext.GroupGroupImages.Where(gi => gi.GroupGuid == this._GroupGuid))
            {
                GroupImage g = _flowProjectDataContext.GroupImageList.FirstOrDefault(gig => gig.GroupImageGuid == gi.GroupImageGuid);
                s.AssignGroupImage(g, _flowProjectDataContext.FlowMasterDataContext.LoginID);
            }
        }

		#region MERGE MEMBERS

        internal Group Clone()
		{
            Group clone = new Group();

            clone.GroupGuid = this.GroupGuid;
			clone.GroupDesc = this.GroupDesc;
            clone.Attribute1 = this.Attribute1;
            clone.Attribute2 = this.Attribute2;
            clone.Attribute3 = this.Attribute3;
            clone.Attribute4 = this.Attribute4;
            clone.Attribute5 = this.Attribute5;
			return clone;
		}

//		internal Guid SubjectImageGuid { get; private set; }

		#endregion




    }
}
