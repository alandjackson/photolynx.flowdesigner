﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Flow.Lib.Helpers;

namespace Flow.Schema.LinqModel.DataContext
{
	partial class SubjectImageType
	{
		public event EventHandler<EventArgs<SubjectImageType>> ExclusiveImageTypeAssign = delegate { };
		
		partial void OnLoaded()
		{
			if (this.ProjectImageType.IsExclusive)
			{
				this.ExclusiveImageTypeAssign += new EventHandler<EventArgs<SubjectImageType>>(this.SubjectImage.Subject.SubjectImageType_ExclusiveImageTypeAssign);				
			}
		}

		partial void OnIsAssignedChanging(bool value)
		{
			if (value && this.ProjectImageType.IsExclusive)
			{
				this.ExclusiveImageTypeAssign(this, new EventArgs<SubjectImageType>(this));
			}
		}

		partial void OnIsAssignedChanged()
		{
			this.SubjectImage.Subject.FlowProjectDataContext.SubmitChanges();
		}
	}
}
