﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

using Flow.Lib.Helpers;

namespace Flow.Schema.LinqModel.DataContext
{
	partial class OrderProductOption
	{
		//public string OptionGroupLabel { get; internal set; }
        public string OptionGroupResourceURL { get; internal set; }
		public string ImageQuixOptionPkPadded
		{ get { return this.ImageQuixOptionPk.ZeroPad(8); } }

		private bool OptionGroupIsMandatory { get; set; }

		public int ImageQuixOptionPkDisplay
		{
			get
			{
				int returnValue = this.ImageQuixOptionPk.HasValue ? this.ImageQuixOptionPk.Value : -1;
                return returnValue;
			}
			set
			{
				this.ImageQuixOptionPk = ((value == -1) ? null : (int?)value);
				this.SendPropertyChanged("ImageQuixOptionPkDisplay");
			}
		}

		//private ObservableCollection<ImageQuixOption> _optionList;
		//public ObservableCollection<ImageQuixOption> OptionList
		//{
		//    get { return _optionList; }
		//    internal set
		//    {
		//        _optionList = new ObservableCollection<ImageQuixOption>(value.ToList());

		//        if (!this.OptionGroupIsMandatory)
		//        {
		//            ImageQuixOption nullOption = new ImageQuixOption();
		//            nullOption.PrimaryKey = null;
		//            nullOption.Label = "<None>";

		//            _optionList.Insert(0, nullOption);
		//        }
		//    }
		//}

		public OrderProductOption(ImageQuixOptionGroup optionGroup)
			: this()
		{
			this.ImageQuixOptionGroupPk = optionGroup.PrimaryKey.Value;
			this.OptionGroupLabel = optionGroup.Label;
            this.OptionGroupResourceURL = optionGroup.ResourceURL;
			this.OptionGroupIsMandatory = optionGroup.IsMandatory;
            
			// Set the option to the default value
            //this.ImageQuixOptionPkDisplay = Convert.ToInt32(optionGroup.OptionList[0].PrimaryKey);
            foreach (ImageQuixOption op in optionGroup.OptionList)
            {
                if (op.IsDefault == true)
                {
                    this.ImageQuixOptionPkDisplay = Convert.ToInt32(op.PrimaryKey);
                    break;
                }
            }
			//this.ImageQuixOptionPkDisplay = optionGroup.OptionDisplayList[0].ImageQuixOptionID;
			
//			this.OptionList = optionGroup.OptionList;
		}

		#region MERGE MEMBERS

		internal Guid OrderProductGuid { get; set; }

		/// <summary>
		/// Returns a clone of the source OrderProductOption object
		/// </summary>
		/// <returns></returns>
		internal OrderProductOption Clone()
		{
			OrderProductOption clone = new OrderProductOption();

			clone.ImageQuixOptionGroupPk = this.ImageQuixOptionGroupPk;
			clone.ImageQuixOptionPk = this.ImageQuixOptionPk;
            clone.OptionGroupLabel = this.OptionGroupLabel;
            clone.Map = this.Map;
			return clone;   
		}

		#endregion

	}
}
