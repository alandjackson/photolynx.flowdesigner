﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flow.Schema.LinqModel.DataContext
{
	partial class Control_ProductPackage
	{
		public Control_ProductPackage(ProductPackage productPackage, FlowProject flowProject)
			: this()
		{
			this.ProductPackageGuid = productPackage.ProductPackageGuid;
			this.ProductPackageDesc = productPackage.ProductPackageDesc;
			this.ProductPackageTitle = productPackage.ProductPackageTitle;
			this.ProductPackageCode = productPackage.ProductPackageCode;
			this.Price = productPackage.Price;
			this.Cost = productPackage.Cost;
			this.Taxed = productPackage.Taxed;
			this.Special = productPackage.Special;
			this.Map = productPackage.Map;
            this.ShowInOnlinePricesheet = productPackage.ShowInOnlinePricesheet;
            this.DigitalDownload = productPackage.DigitalDownload;
            this.IsAnyXPackage = productPackage.IsAnyXPackage;
            this.IsALaCarte = productPackage.IsALaCarte;
            this.MaxProductsAllowed = productPackage.MaxProductsAllowed;
            this.ImageURL = productPackage.ImageURL;

			foreach (ProductPackageComposition composition in productPackage.ProductPackageCompositionList)
			{
				Control_ProductPackageComposition controlComposition = new Control_ProductPackageComposition();

				controlComposition.ProductPackageCompositionID = composition.ProductPackageCompositionID;
				controlComposition.ProductPackageGuid = this.ProductPackageGuid;
				controlComposition.ImageQuixProductPk = composition.ImageQuixProduct.PrimaryKey;
				controlComposition.Quantity = composition.Quantity;
                controlComposition.Layout = composition.Layout;
				flowProject.FlowProjectDataContext.Control_ProductPackageCompositions.InsertOnSubmit(controlComposition);
			}

		}
	}
}
