﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.Linq;
using System.Data.SqlServerCe;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Media.Imaging;

using Flow.Lib;
using Flow.Lib.Database.SqlCe;
using Flow.Lib.Helpers;
using Flow.Schema.Properties;
using Flow.Schema.Utility;


namespace Flow.Schema.LinqModel.DataContext
{
	/// <summary>
	/// Represents an image (file) assigned to a Subject
	/// </summary>
	public partial class Image
	{

		#region FIELDS AND PROPERTIES

		private Subject _subject = null;

		/// <summary>
		/// A class-wrapper representing the graphical image 
		/// </summary>
		FlowImage _flowImage = null;
		private FlowImage FlowImage
		{
			get
			{
				if (_flowImage == null)
					_flowImage = new FlowImage(this.ImagePath);

				return _flowImage;
			}
			set { _flowImage = value; }
		}

		public bool ImageFileExists { get { return this.FlowImage.ImageFileExists; } }

		/// <summary>
		/// The full filepath of the image file
		/// </summary>
		private string _imagePath = null;
		public string ImagePath
		{
			get
			{
				if (_imagePath == null)
					_imagePath = Path.Combine(FlowContext.GetFlowProjectImageDirPath(this.FlowProjectGuid), this.ImageFileName);

				return _imagePath;
			}
		}

		/// <summary>
		/// A thumbnail representation of the image
		/// </summary>
		public BitmapImage Thumbnail
		{
			get { return this.FlowImage.Thumbnail; }
		}

		public BitmapImage IntermediateImage
		{
			get { return this.FlowImage.IntermediateImage; }
		}

		/// <summary>
		/// The digital photographic image, in all its glory
		/// </summary>
		public BitmapImage ProcessingImage
		{
			get { return this.FlowImage.ProcessingImage; }
		}

		#endregion


		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="subject"></param>
		public Image(Subject subject)
		{
			_subject = subject;
		}

		public void DisposeImages()
		{
			if (this.FlowImage != null)
				this.FlowImage.Clear();

			this.FlowImage = null;
		}

	}
}
