﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Data;

using Flow.Lib.Helpers;
using Flow.Lib.Linq;
using Flow.Schema.Utility;

using Xceed.Wpf.DataGrid;
using Flow.Lib;
using StructureMap;
using System.Windows.Threading;
using Flow.Lib.AsyncWorker;
using System.IO;
using NLog;


namespace Flow.Schema.LinqModel.DataContext
{

	/// <summary>
	/// Represents a "navigable", UI-observable collection of project Subjects
	/// </summary>
	public class SubjectObservableCollection : FlowObservableCollection<Subject>
	{
        private static Logger logger = LogManager.GetCurrentClassLogger();

		#region FIELDS AND PROPERTIES

		public event EventHandler SubjectDataLoaded = delegate { };
		public event EventHandler SortCompleted = delegate { };
			
		private FlowProject _flowProject = null;

		private DataTable DataTable
		{
			get { return _flowProject.FlowProjectDataContext.SubjectDataTable; }
		}

		private IList _dataTableList = null;
		public IList Table
		{
			get
			{
				if (_dataTableList == null)
					_dataTableList = ((IListSource)this.DataTable).GetList();

				return _dataTableList;
			}
		}

		/// <summary>
		/// Represents a specialized view on an underlying data structure for use by the Xceed datagrid
		/// </summary>
		private DataGridCollectionView _dataGridCollectionView = null;
        public void RefreshDataGridCollectionView() { _dataGridCollectionView = null; }
		public DataGridCollectionView DataGridCollectionView
		{
			get
			{
				if (_dataGridCollectionView == null)
				{
					_dataGridCollectionView = new DataGridCollectionView(this.Table);

					_dataGridCollectionView.ItemProperties.Remove(_dataGridCollectionView.ItemProperties["Formal Name"]);
                    _dataGridCollectionView.ItemProperties.Remove(_dataGridCollectionView.ItemProperties["SubjectID"]);

					// Event handler assignments
					_dataGridCollectionView.CurrentChanged += new EventHandler(DataGridCollectionView_CurrentChanged);

					_dataGridCollectionView.NewItemCreated += new EventHandler<DataGridItemEventArgs>(DataGridCollectionView_NewItemCreated);
					_dataGridCollectionView.CancelingNewItem += new EventHandler<DataGridItemHandledEventArgs>(DataGridCollectionView_CancelingNewItem);
					_dataGridCollectionView.CommittingNewItem += new EventHandler<DataGridCommittingNewItemEventArgs>(DataGridCollectionView_CommittingNewItem);

					_dataGridCollectionView.EditCommitted += new EventHandler<DataGridItemEventArgs>(DataGridCollectionView_EditCommitted);

					_dataGridCollectionView.RemovingItem += new EventHandler<DataGridRemovingItemEventArgs>(DataGridCollectionView_RemovingItem);
                    
                    
					//((INotifyCollectionChanged)_dataGridCollectionView.SortDescriptions).CollectionChanged +=
					//    new NotifyCollectionChangedEventHandler(DataGridCollectionView_SortDescriptionsChanged);
				}
	
				return _dataGridCollectionView;
			}
		}

        private string _currentFilterName { get; set; }
        public string CurrentFilterName
        {
            get
            {
                if (_currentFilterName == null)
                    _currentFilterName = "No Filter Set";
                return _currentFilterName;
            }
            set{
                _currentFilterName = value;
                this.SendPropertyChanged("CurrentFilterName");
            }
        }
        public string FilterSummary
        {
            get { 
                if(_dataGridCollectionView != null)
                    return CurrentFilterName + " (" + _dataGridCollectionView.Count + " out of " + this.Count() + " Subjects)";
                return CurrentFilterName + " (" + this.Count() + " out of " + this.Count() + " Subjects)";
            
            }
        }
        public void UpdateFilterSummary()
        {
            this.SendPropertyChanged("FilterSummary");
        }

		private IOrderedEnumerable<int> _filteredSubjectIdList = null;

		public bool IsFilteredByCriterion { get { return _filteredByCriterionSubjectIdList == null; } }
		private IEnumerable<int> _filteredByCriterionSubjectIdList = null;

		public bool IsFilteredByImagesAssigned { get { return _filteredByImagesAssignedSubjectIdList == null; } }
		private IEnumerable<int> _filteredByImagesAssignedSubjectIdList = null;

		private bool _isSorting = false;
		private bool _isSavingItem = false;
		private bool _isDeleting = false;
        private bool _isFilteredOnImages = false;
        private bool _isFilteredOnNoImages = false;
        private Nullable<DateTime> _filterBeginDate;
        private Nullable<DateTime> _filterEndDate;
        private String _filterImageSource;

		private string _currentCriterion = null;
		public string CurrentCriterion
		{
			get { return _currentCriterion; }
			private set
			{
				_currentCriterion = value;
				this.SendPropertyChanged("CurrentCriterion");
			}
		}
		//		bool _isFilteredOnImages = false;


		#endregion // FIELDS AND PROPERTIES


		/// <summary>
		/// Contructor; withLoad defaulted to true (load Subject data from db, in separate thread)
		/// </summary>
		/// <param name="subjectList"></param>
		/// <param name="flowProject"></param>
		public SubjectObservableCollection(IEnumerable<Subject> subjectList, FlowProject flowProject)
			: this(subjectList, flowProject, true) { }
	
		/// <summary>
		/// Constructor; withLoad is false for data import as the data is already loaded
		/// </summary>
		/// <param name="subjectList"></param>
		/// <param name="flowProject"></param>
		public SubjectObservableCollection(IEnumerable<Subject> subjectList, FlowProject flowProject, bool withLoad)
			: base(subjectList)
		{
			_flowProject = flowProject;

			if(withLoad)
				FlowBackgroundWorkerManager.RunWorker(
					this.LoadSubjects,
					LoadDataWorker_Completed,
					flowProject
				);
		}


        Dispatcher _dispatcher;
		/// <summary>
		/// Constructor; allows for a callback method once the data load operation is complete
		/// </summary>
		/// <param name="subjectList"></param>
		/// <param name="flowProject"></param>
		/// <param name="method"></param>
        public SubjectObservableCollection(IEnumerable<Subject> subjectList, FlowProject flowProject, EventHandler method)
            : base(subjectList)
        {
            
			_flowProject = flowProject;
            _dispatcher = Dispatcher.CurrentDispatcher;
			this.SubjectDataLoaded += new EventHandler(method);

            object initlist = flowProject.FlowProjectDataContext.ProjectSubjectFieldList;
            object initlist2 = flowProject.FlowProjectDataContext.SubjectTicketList;

            if (this._flowProject.MainDispatcher == null)
            {
                this._flowProject.MainDispatcher = _dispatcher;
                FlowBackgroundWorker wrker = FlowBackgroundWorkerManager.RunWorker(
                    this.LoadSubjects,
                    LoadDataWorker_Completed,
                    flowProject
                );
            }
            else
            {
                LoadSubjects(flowProject);
                this._flowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                  {
                      LoadDataWorker_Completed();
                  }));
            }
		}

        /// <summary>
        /// Performs operations after the subject data loading worker thread is complete
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
		void LoadDataWorker_Completed(object sender, RunWorkerCompletedEventArgs e)
        {
            LoadDataWorker_Completed();
            this.SubjectDataLoaded(this, null);
            this.SendPropertyChanged("CurrentItem");
        }

        private void LoadDataWorker_Completed()
        {
            this.RemovingItem += new EventHandler<EventArgs<Subject>>(FlowObservableCollection_RemovingItem);
            this.ItemRemoved += new EventHandler(FlowObservableCollection_ItemRemoved);

            // Sends messages to refresh UI from subject data
            //if (this.HasItems)
            //    this.CurrentItem.SubjectData.Refresh();

            
        }

        public void RaiseSubjectDataLoaded()
        {

            this.SubjectDataLoaded(this, null);
            this.SendPropertyChanged("CurrentItem");
        }
        /// <summary>
        /// Initializes each Subject item in the collection, loading and assigning the user-defined field data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
		void LoadSubjects(object sender, DoWorkEventArgs e)
        {
            FlowProject flowProject = (FlowProject)e.Argument;

            LoadSubjects(flowProject);
 
		}

        private void LoadSubjects(FlowProject flowProject)
        {
            if (flowProject.LoadingProgressInfo != null)
            {
                flowProject.LoadingProgressInfo.Title = "Initializing Subjects...";
                flowProject.LoadingProgressInfo.Update("{0} of {1}",
                    0,
                    this.Count,
                    1, true);
            }
            else
            {
                flowProject.LoadingProgressInfo = new NotificationProgressInfo(
                    "Initializing Subjects...",
                    0,
                    this.Count,
                    1,
                    "{0} of {1}"
                );
                flowProject.LoadingProgressInfo.Display();
            }


            this._flowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
            {
                this._flowProject.RegisterImageListChangeEventHandler();
                 
            }));

            foreach (Subject item in this)
            {

                //this just makes sure that all subjects have a primary image, it should not be needed
                //
                //this._flowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                //    {
                //        object subImgeList = item.ImageList;
                //        if (item.ImageList.HasItems && !item.ImageList.Any(i => i.IsPrimary))
                //        {
                //            //this._flowProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                //            //{

                //            item.ImageList.Last().IsPrimary = true;

                //            //}));
                //        }
                //    }));

               
                item.Initialize(flowProject.FlowProjectDataContext);
                flowProject.LoadingProgressInfo++;

            }


            //we released a verion in September 2011 that orphaned images that came from a group image when the group image was renamed.
            //the bug was live for a few weeks before it was fixed. This is a temporary patch to fix any of the potential orphaned images.
            //should remove this in 2012.
            //foreach (GroupImage gi in this._flowProject.FlowProjectDataContext.GroupImages)
            //{
            //    gi.FlowProjectDataContext = this._flowProject.FlowProjectDataContext;
            //    foreach (SubjectImage si in gi.GetSubjectImageList)
            //    {
            //        si.ImagePath = gi.ImagePath;
            //        si.ImageFileName = gi.ImageFileName;
            //    }
            //}
            flowProject.LoadingProgressInfo.Complete("Project " + flowProject.FlowProjectName + " loaded.");
        }

		#region EVENT HANDLERS

		/// <summary>
		/// Invoked when the item current of the ICollectionView is changing;
		/// if the current (prior to operation) item is dirty, the item is saved
		/// </summary>
		protected override void CollectionView_CurrentChanging(object sender, CurrentChangingEventArgs e)
		{
			// Upon moving to another Subject, see if current Subject has been edited and save if so
			if (this.CurrentItem != null && this.CurrentItem.IsDirty)
				this.CurrentItem.Save();

            //GC.Collect();
            //release images for the current (previous) subject
            //lets not dispose of images, Keep them around for better performance - CHAD 10-5-2010
            //if (this.CurrentItem != null && this.CurrentItem.ImageList.Count() > 0)
            //{
            //    foreach (SubjectImage img in this.CurrentItem.ImageList)
            //    {
            //        img.DisposeImages();
            //    }
            //}

			base.CollectionView_CurrentChanging(sender, e);
		}

		/// <summary>
		/// Invoked after the item current of the ICollectionView changes;
		/// the Xceed DataGridCollectionView is synched to correspond to the current item
		/// </summary>
		protected override void CollectionView_CurrentChanged(object sender, EventArgs e)
		{
			if (this.NewItem == null)
			{
				if (this.CurrentItem != null && !_isSorting)
					// NOTE: moving by position may be problematic with respect to sorting, grouping, filtering
                    if (this.View.CurrentPosition >= 0 && this.View.CurrentPosition < this.DataGridCollectionView.Count)
                    {
                        this.DataGridCollectionView.MoveCurrentToPosition(this.View.CurrentPosition);
                    }

				_isSorting = false;

				base.CollectionView_CurrentChanged(sender, e);
			}

			this.SendPropertyChanged("CurrentSubject");
		}

		/// <summary>
		/// Invoked when an item is being removed from the base FlowObservableCollection;
		/// deletes the Xceed datagrid row associated with the Subject being deleted
		/// </summary>
		private void FlowObservableCollection_RemovingItem(object sender, EventArgs<Subject> e)
		{
			if (!_isDeleting)
			{
				_isDeleting = true;

				// The DataGridCollectionView may not have been instantiated yet, so test
				if (_dataGridCollectionView != null)
					_dataGridCollectionView.Remove(e.Data.SubjectDataRow);

				_isDeleting = false;
			}
		}


		/// <summary>
		/// Invoked after an item has been removed from the underlyging FlowObservableCollection
		/// </summary>
		private void FlowObservableCollection_ItemRemoved(object sender, EventArgs e)
		{
			_flowProject.FlowProjectDataContext.SubmitChanges();
		}


		/// <summary>
		/// Invoked when the the record current of the Xceed datagrid has changed
		/// </summary>
		private void DataGridCollectionView_CurrentChanged(object sender, EventArgs e)
		{
			if (_dataGridCollectionView.CurrentItem != null)
			{
				int currentSubjectID = (int)(((DataRowView)_dataGridCollectionView.CurrentItem).Row["SubjectID"]);

				Subject subject = this.FindSubjectByID(currentSubjectID);

				if(subject != null)
					this.View.MoveCurrentTo(subject);
			}
		}

		/// <summary>
		/// Invoked when the "new item row" in the Xceed datagrid is edited;
		/// a new Subject item is created in correspondence
		/// </summary>
		private void DataGridCollectionView_NewItemCreated(object sender, DataGridItemEventArgs e)
		{
			this.CreateNew();
		}

		/// <summary>
		/// Invoked when the user cancels cration of a new row in the Xceed datagrid
		/// </summary>
		void DataGridCollectionView_CancelingNewItem(object sender, DataGridItemHandledEventArgs e)
		{
			this.NewItem = null;
		}

		/// <summary>
		/// Invoked when the new item row of the Xceed datagrid is committing (finalized);
		/// the corresponding Subject object is saved
		/// </summary>
		private void DataGridCollectionView_CommittingNewItem(object sender, DataGridCommittingNewItemEventArgs e)
		{
			if (_isSavingItem)
				return;

			_isSavingItem = true;
			
			System.Data.DataRow newDataRow = ((DataRowView)e.Item).Row;
			newDataRow["SubjectID"] = 0;

			foreach (DataColumn column in newDataRow.Table.Columns)
			{
				if (!column.AllowDBNull && newDataRow.IsNull(column))
				{
                    
					_dataGridCollectionView.CancelNew();
					e.Cancel = true;
                    _isSavingItem = false;
					return;
				}
			}


			this.NewItem.Save(newDataRow);
			_isSavingItem = false;
		}

		/// <summary>
		/// Invoked after the new item row of the Xceed datagrid has been committed;
		/// the item current of the SubjectObservableCollection is synched to that represented
		/// by the committed row
		/// </summary>
		private void DataGridCollectionView_EditCommitted(object sender, DataGridItemEventArgs e)
		{
			System.Data.DataRow editRow = ((DataRowView)e.Item).Row;

			if (editRow == this.CurrentItem.SubjectDataRow)
			{
				this.CurrentItem.SynchSubjectDataFromDataRow();
			}

			if (this.CurrentItem.IsDirty)
				this.CurrentItem.Save();
		}

		/// <summary>
		/// Deletes the Subject item related to the Xceed datagrid row being deleted
		/// </summary>
		private void DataGridCollectionView_RemovingItem(object sender, DataGridRemovingItemEventArgs e)
		{
			if (!_isDeleting)
			{
				_isDeleting = true;

				//System.Data.DataRow deletionDataRow = ((DataRowView)_dataGridCollectionView.SourceItems[e.Index]).Row;
				//int deletionSubjectID = (int)(deletionDataRow["SubjectID"]);

				//this.DeleteSubject(deletionSubjectID);

				this.DeleteCurrent();

				// Prevent the DataGridCollectionView from deleting the item; this will be
				// handled by the SubjectObservableCollection and the target Subject
				e.Cancel = true;

				_isDeleting = false;
			}
		}

		#endregion // EVENT HANDLERS


		#region PUBLIC METHODS

		/// <summary>
		/// Creates a new Subject object, which becomes current of the collection
		/// </summary>
		/// <returns>Subject</returns>
		public new Subject CreateNew()
		{
			this.NewItem = new Subject(_flowProject.FlowProjectDataContext);
			return this.CurrentItem;
		}

		/// <summary>
		/// Creates a new Subject object from the supplied DataRow (Xceed datagrid)
		/// </summary>
		/// <param name="dataRow"></param>
		/// <returns>Subject</returns>
		public Subject CreateNew(System.Data.DataRow dataRow)
		{
			this.NewItem = Subject.CreateNewSubject(_flowProject, dataRow);
			return this.CurrentItem;
		}

		/// <summary>
		/// Cancels (eliminates) the new item (Subject) reference
		/// </summary>
		public override void CancelNew()
		{
			this.NewItem = null;
		}

		/// <summary>
		/// Saves the current item and ensures that the Xceed datagrid is in synch
		/// </summary>
		/// <param name="subjectID"></param>
		public void SaveCurrent()
		{
			this.CurrentItem.Save();
			this.DataGridCollectionView.MoveCurrentTo(this.CurrentItem.SubjectDataRow);
		}

		/// <summary>
		/// Removes a Subject from the collection by SubjectID
		/// </summary>
		/// <param name="subjectID"></param>
		public void DeleteSubject(int subjectID)
		{
			DeleteSubject(this.FindSubjectByID(subjectID));
		}

		/// <summary>
		/// Removes the supplied Subject from the collection
		/// </summary>
		/// <param name="subject"></param>
		public void DeleteSubject(Subject subject)
		{
			int currentIndex = this.View.IndexOf(subject);
            subject.DeleteOrders();
            _flowProject.FlowProjectDataContext.SubmitChanges();
			subject.PrepareDelete();
            _flowProject.FlowProjectDataContext.SubmitChanges();

            //there should not be any, but just in case, lets delete them
            foreach (SubjectOrder so in this._flowProject.FlowProjectDataContext.SubjectOrders.Where(s=>s.SubjectID == subject.SubjectID))
            {
                this._flowProject.FlowProjectDataContext.SubjectOrders.DeleteOnSubmit(so);
            }
			    
            this.Remove(subject);
            this._flowProject.FlowProjectDataContext.SubmitChanges();
            if (!this._flowProject.FlowProjectDataContext.DeletedSubjects.Any(ds => ds.SubjectGuid == subject.SubjectGuid))
            {
                DeletedSubject ds = new DeletedSubject();
                ds.DeleteDate = DateTime.Now;
                ds.SubjectGuid = subject.SubjectGuid;
                this._flowProject.FlowProjectDataContext.DeletedSubjects.InsertOnSubmit(ds);
            }

			this.MoveCurrentToPosition(currentIndex);
		}

		/// <summary>
		/// Deletes the item current of the collection from the collection
		/// </summary>
		public override void DeleteCurrent()
		{
			this.DeleteSubject(this.CurrentItem);
            logger.Info("Deleted current subject");
		}

		/// <summary>
		/// Finds the DataRow in the Linq-to-SQL base Subject table corresponding to the supplied SubjectID
		/// </summary>
		/// <param name="subjectID"></param>
		/// <returns>DataRow</returns>
		public System.Data.DataRow FindItemByID(int subjectID)
		{
			return this.DataTable.Rows.Find(subjectID);
		}

		/// <summary>
		/// Returns the Subject object in the collection by SubjectID
		/// </summary>
		/// <param name="subjectID"></param>
		/// <returns>Subject</returns>
		public Subject FindSubjectByID(int subjectID)
		{
			return this.SingleOrDefault(s => s.SubjectID == subjectID);
		}

		/// <summary>
		/// Removes a field from view of the Xceed datagrid by removing its reference in the underlying view
		/// </summary>
		/// <param name="propertyName"></param>
		public void RemoveDataGridItemProperty(string propertyName)
		{
			if(_dataGridCollectionView != null)
				_dataGridCollectionView.ItemProperties.Remove(_dataGridCollectionView.ItemProperties[propertyName]);
		}

		/// <summary>
		/// Performs sorting of Subject records for UI display;
		/// the datasources for the XCeed datagrid and the custom subject record dipslays
		/// are sorted separately and require synchronization
		/// </summary>
		/// <param name="sortItems"></param>
		public void Sort(SortDescriptionCollection sortItems, bool fromDataGrid)
		{
			_isSorting = fromDataGrid;
            IsSorted = true;
			SortDescription subjectIdSortItem = new SortDescription("SubjectID", ListSortDirection.Ascending);

			Subject currentSubject = this.CurrentItem;

			// If the sort is not initiated from the xceed grid, sort the data supplying the datagrid
			if (!fromDataGrid)
			{
				using (this.DataGridCollectionView.DeferRefresh())
				{
					// Remove existing SortDescriptions
					this.DataGridCollectionView.SortDescriptions.Clear();

					// If sort items have been supplied...
					if (sortItems.Count > 0)
					{
						// Invert the list and add to the xceed datasource's SortDescriptions list
						foreach (SortDescription sortItem in sortItems)
						{
							this.DataGridCollectionView.SortDescriptions.Add(sortItem);
						}

						// Add a final sort on SubjectID to ensure synchronization 
						this.DataGridCollectionView.SortDescriptions.Add(subjectIdSortItem);
					}
				}
			}


			// Sort the data supplying the SubjectDataCollection
			using (this.View.DeferRefresh())
			{
				this.View.SortDescriptions.Clear();

				if (sortItems.Count > 0)
				{
					foreach (SortDescription sortItem in sortItems)
					{
						this.View.SortDescriptions.Add(
							this.GetSubjectDatumSortDescription(sortItem)
						);
					}

					this.View.SortDescriptions.Add(subjectIdSortItem);
				}
			}

			this.SortCompleted(this, null);
		}

		/// <summary>
		/// Returns a SortDesctiption for sorting on Subject fields
		/// </summary>
		/// <param name="sortItem"></param>
		/// <returns></returns>
		private SortDescription GetSubjectDatumSortDescription(SortDescription sortItem)
		{
			SortDescription retVal = new SortDescription();

			retVal.PropertyName = "SubjectData[" + sortItem.PropertyName + "].ComparisonValue";
			retVal.Direction = sortItem.Direction;
			return retVal;
		}

		/// <summary>
		/// Attempts to locate a record containing a barcode scan key matching the supplied criterion
		/// </summary>
		/// <param name="criterion"></param>
		public IEnumerable<Subject> PerformBarcodeLookup(string criterion)
		{
            logger.Info("Performing barcode lookup: criterion='{0}'", criterion);

			int criterionLength = criterion.Length;

			if (_flowProject.FlowProjectDataContext.HasScanKeyField.Value)
			{
				return this.Where(s => s.BarcodeFieldValue == criterion.ToUpper());
			}

			// If no scan key has been assigned, handle (?)
			else
			{
				// throw error
			}

			return null;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="criterion"></param>
		public SubjectSearchResult ApplyFilter(string criterion, string fieldDesc)
		{
			if (criterion == null)
				_filteredByCriterionSubjectIdList = null;
			else
				_filteredByCriterionSubjectIdList = this.GetQueryResultIndices(criterion, false, fieldDesc);

			SubjectSearchResult result = this.ApplyFilter();

			if (result == SubjectSearchResult.HasResults)
				this.CurrentCriterion = criterion;

			return result;
		}


        /// <summary>
        /// 
        /// </summary>
        public SubjectSearchResult ApplyTemporaryFilter()
        {
            return ApplyImageFilter(false,true, _filterBeginDate, _filterEndDate, _filterImageSource);
        }

        /// <summary>
        /// 
        /// </summary>
        public SubjectSearchResult RestoreFilter()
        {
            return ApplyImageFilter(_isFilteredOnNoImages, _isFilteredOnImages, _filterBeginDate, _filterEndDate, _filterImageSource);
        }

        public SubjectSearchResult FilterOnTheseSubjects(List<Subject> subjects)
        {
            _filteredByCriterionSubjectIdList = subjects.Select(s => s.SubjectID).AsEnumerable();
            return this.ApplyFilter();
        }

        public SubjectSearchResult FilterOnTheseImages(List<SubjectImage> subjectImages)
        {
            _filteredByCriterionSubjectIdList = subjectImages.Select(si => si.Subject).Select(s => s.SubjectID).AsEnumerable();
            foreach (Subject sub in subjectImages.Select(si => si.Subject).ToList())
            {
                foreach (SubjectImage subImage in sub.SubjectImages)
                {
                    if (!subjectImages.Contains(subImage))
                    {
                        subImage.IsSuppressed = true;
                    }
                }
            }
            return this.ApplyFilter();
        }

        public SubjectSearchResult ApplyFieldFilter(ProjectSubjectField psf)
        {
            if (psf.SearchTerm != null)
            {
                _filteredByImagesAssignedSubjectIdList = this
                       .Where(s =>
                           s.SubjectData[psf.ProjectSubjectFieldDesc].Value.ToString().Contains(psf.SearchTerm)
                       )
                       .Select(s => s.SubjectID)
                       .AsEnumerable();

                return this.ApplyFilter();
            }
            else
                return this.ApplyFilter(false, false, null, null, null);
        }
		/// <summary>
		/// 
		/// </summary>
		/// <param name="isFilteredOnImages"></param>
		public SubjectSearchResult ApplyFilter(bool isFilteredOnNoImagems, bool isFilteredOnImages, Nullable<DateTime> beginDate, Nullable<DateTime> endDate, String imgSource)
		{
            return new SubjectSearchResult();
            _isFilteredOnImages = isFilteredOnImages;
            _isFilteredOnNoImages = isFilteredOnNoImagems;
            _filterBeginDate = beginDate;
            _filterEndDate = endDate;
            _filterImageSource = imgSource;
            return ApplyImageFilter(isFilteredOnNoImagems,isFilteredOnImages, beginDate, endDate, imgSource);
		}

        //public SubjectSearchResult ApplyMissingImageFilter()
        //{
        //    _filteredByImagesAssignedSubjectIdList = this
        //          .Where(s =>
        //              s.HasAssignedImages &&
        //              (
        //                  s.ImageList.Any(i => !File.Exists(i.ImagePath))
        //              )
        //          )
        //          .Select(s => s.SubjectID)
        //          .AsEnumerable();

        //    SubjectSearchResult result = this.ApplyFilter();
        //    return result;
        //}
        private SubjectSearchResult ApplyImageFilter(bool isFilteredOnNoImages, bool isFilteredOnImages, Nullable<DateTime> beginDate, Nullable<DateTime> endDate, String imgSource)
        {
            return new SubjectSearchResult();

            if (imgSource == "All")
                imgSource = null;

            if (imgSource != null && imgSource.Length > 1)
            { isFilteredOnImages = true; isFilteredOnNoImages = false; }

            if (isFilteredOnImages == false && isFilteredOnNoImages == false)
			{
				_filteredByImagesAssignedSubjectIdList = null;
			}
            else if (isFilteredOnNoImages)
            {
                //this means, we are filtering on subjects with no Images
                //_filteredByImagesAssignedSubjectIdList = this._flowProject.FlowProjectDataContext.SubjectList
                _filteredByImagesAssignedSubjectIdList = this
                    .Where(s =>
                        s.HasAssignedImages == false
                    )
                    .Select(s => s.SubjectID)
                    .AsEnumerable();

            }
            else
            {
                //this means, we are filtering on subjects with Images
                //_filteredByImagesAssignedSubjectIdList = this._flowProject.FlowProjectDataContext.SubjectList
                _filteredByImagesAssignedSubjectIdList = this
                    .Where(s =>
                        s.HasAssignedImages &&
                        (
                            (imgSource != null && imgSource.Length > 0 && s.ImageList.Any(i => i.SourceComputer == imgSource)) ||
                            (imgSource == null || imgSource.Length == 0)
                        ) &&
                        (
                            !(beginDate.HasValue || endDate.HasValue) ||
                            s.ImageList.Any(i => i.DateAssigned.Value.IsInRange(beginDate, endDate))
                        ) 
                    )
                    .Select(s => s.SubjectID)
                    .AsEnumerable();
            }

//            this.ApplyFilter();

            SubjectSearchResult result = this.ApplyFilter();

            foreach (SubjectImage subjectImage in this._flowProject.FlowProjectDataContext.ImageList)
            {
                subjectImage.IsSuppressed = !(
                    !isFilteredOnImages ||
                    !(beginDate.HasValue || endDate.HasValue) ||
                    subjectImage.DateAssigned.Value.IsInRange(beginDate, endDate)
                ) ||
                (
                    (imgSource != null && imgSource.Length > 0 && subjectImage.SourceComputer != imgSource) 
                )
                ;
            }
            //foreach (Subject subject in this.View)
            //{
            //    foreach (SubjectImage subjectImage in subject.ImageList)
            //    {
            //        cnter++;
            //        subjectImage.IsSuppressed = !(
            //            !isFilteredOnImages  ||
            //            !(beginDate.HasValue || endDate.HasValue) ||
            //            subjectImage.DateAssigned.Value.IsInRange(beginDate, endDate)
            //        );
            //    }
            //}

			return result;
        }

		/// <summary>
		/// Clears the applied filters
		/// </summary>
		public void ClearFilter()
		{
			_filteredSubjectIdList = null;
			this.CurrentCriterion = null;

            this._isFilteredOnImages = false;
            this._isFilteredOnNoImages = false;

			this.DataGridCollectionView.Filter = null;
			this.View.Filter = null;

			this.SendPropertyChanged("RecordCount");
			this.SendPropertyChanged("IsFiltered");

            
		}
	
		/// <summary>
		/// Returns a list of Subjects filtered by the supplied criterion (applied across searchable fields)
		/// </summary>
		/// <param name="criterion"></param>
		/// <param name="limitToProminentFields">Specified that only 'Prominent' fields should be searched</param>
		public IEnumerable<Subject> GetQueryResults(string criterion, bool limitToProminentFields, string fieldDesc)
		{
			IEnumerable<int> _filterItems = this.GetQueryResultIndices(criterion, limitToProminentFields, fieldDesc);

			return this.Join(
				_filterItems,
				s => s.SubjectID,
				i => i,
				(s, i) => s
			);
		}

        public IEnumerable<Subject> GetQueryResultsForDataGridItems(string criterion, bool limitToProminentFields, string fieldDesc)
        {
          DataTable dt = ((DataGridCollectionView.SourceCollection) as DataView).ToTable();


            IEnumerable<int> _filterItems = this.DataGridCollectionView.Cast<DataRowView>()
                .AsQueryable()
                .Where(this.GetPredicate(criterion, limitToProminentFields, fieldDesc))
                .Select(r => (int)r["SubjectID"])
            ;

            return this.Join(
                _filterItems,
                s => s.SubjectID,
                i => i,
                (s, i) => s
            );
        }

        public IEnumerable<Subject> GetDataGridCollectionViewSubjects()
        {
            DataTable dt = ((DataGridCollectionView.SourceCollection) as DataView).ToTable();


            IEnumerable<int> _filterItems = this.DataGridCollectionView.Cast<DataRowView>()
                .AsQueryable()
                .Select(r => (int)r["SubjectID"])
            ;

            return this.Join(
                _filterItems,
                s => s.SubjectID,
                i => i,
                (s, i) => s
            );
        }


		public void ApplyResolutionFilter()
		{
			this.ClearFilter();

			IEnumerable<int> results = this.Where(s =>
				s.SubjectOrder != null &&
				s.SubjectOrder.OrderPackageList.HasItems &&
				(
					s.SubjectOrder.OrderPackageList.Any(pk =>
						pk.OrderProductList.Any(pr =>
							pr.OrderProductNodeList.Any(n =>
								n.ImageQuixProductNodeType == 1 &&
								n.SubjectImageID == null
							)
						)
					)
				)
			).Select(s => s.SubjectID);

			// Store the result indices in a hashset for performance
			HashSet<int> hash = new HashSet<int>(results);

			// Apply the filter against the Xceed datagrid
			this.DataGridCollectionView.Filter =
				(r) => hash.Contains((int)((DataRowView)r)["SubjectID"]);

			// Apply the filter against the custom subject display elements
			this.View.Filter =
				(s) => hash.Contains(((Subject)s).SubjectID);


			this.SendPropertyChanged("RecordCount");
			this.SendPropertyChanged("IsFiltered");

			// Set the CurrentItem to the first item in the search results
			this.View.MoveCurrentToFirst();
		}

		public void ApplyMissingImageFilter()
		{
			this.ClearFilter();

			IEnumerable<int> results = this.Where(s =>
				s.ImageList.Any(i => !i.ImageFileExists)
			).Select(s => s.SubjectID);

			// Store the result indices in a hashset for performance
			HashSet<int> hash = new HashSet<int>(results);

			// Apply the filter against the Xceed datagrid
			this.DataGridCollectionView.Filter =
				(r) => hash.Contains((int)((DataRowView)r)["SubjectID"]);

			// Apply the filter against the custom subject display elements
			this.View.Filter =
				(s) => hash.Contains(((Subject)s).SubjectID);


			this.SendPropertyChanged("RecordCount");
			this.SendPropertyChanged("IsFiltered");

			// Set the CurrentItem to the first item in the search results
			this.View.MoveCurrentToFirst();
		}

		/// <summary>
		/// Applies the current filter against the pertinent data views
		/// </summary>
		/// <returns></returns>
		public SubjectSearchResult ApplyFilter()
		{
			IOrderedEnumerable<int> results = null;

			// If the text filter is applied...
			if (_filteredByImagesAssignedSubjectIdList != null)
			{
				// Set the results to the list of matched subject IDs (by text filter)
				results = _filteredByImagesAssignedSubjectIdList.OrderBy(i => i);

				// If the image filter is applied...
				if (_filteredByCriterionSubjectIdList != null)
					
					// Find the union of the text and image filter results
					results = results.Intersect(_filteredByCriterionSubjectIdList).OrderBy(i => i);
			}

			// If the image filter is applied...
			else if (_filteredByCriterionSubjectIdList != null)

				// Set the results to the list of matched subject IDs (by image filter)
				results = _filteredByCriterionSubjectIdList.OrderBy(i => i);
			
			// If no filters are applied
			else
			{
				// Reset the UI filters
				this.ClearFilter();
				return SubjectSearchResult.Cleared;
			}

			int resultCount = results.Count();

            //if (resultCount == 0)
            //    return SubjectSearchResult.NoResults;

			// Store the result indices in a hashset for performance
			HashSet<int> hash = new HashSet<int>(results);

			// Apply the filter against the Xceed datagrid
			this.DataGridCollectionView.Filter =
				(r) => hash.Contains((int)((DataRowView)r)["SubjectID"]);

			// Apply the filter against the custom subject display elements
			this.View.Filter =
				(s) => hash.Contains(((Subject)s).SubjectID);


			this.SendPropertyChanged("RecordCount");
			this.SendPropertyChanged("IsFiltered");

			// Set the CurrentItem to the first item in the search results
			//this.View.MoveCurrentToFirst();

			return SubjectSearchResult.HasResults;
		}

        public void NarrowFilterByTheseSubjectIDs(List<int> subjectIDs)
        {
            // Store the result indices in a hashset for performance
            HashSet<int> hash = new HashSet<int>(subjectIDs);

            // Apply the filter against the Xceed datagrid
            this.DataGridCollectionView.Filter =
                (r) => hash.Contains((int)((DataRowView)r)["SubjectID"]);

            // Apply the filter against the custom subject display elements
            this.View.Filter =
                (s) => hash.Contains(((Subject)s).SubjectID);


            this.SendPropertyChanged("RecordCount");
            this.SendPropertyChanged("IsFiltered");
        }
		/// <summary>
		/// Returns a list of SubjectIDs for subject records which meet the query criteria
		/// </summary>
		/// <param name="criterion"></param>
		/// <param name="limitToProminentFields"></param>
		/// <returns></returns>
		public IEnumerable<int> GetQueryResultIndices(string criterion, bool limitToProminentFields, string fieldDesc)
		{
            if (criterion == "" && fieldDesc != "All Fields")
                return this.DataTable.DefaultView
                .Cast<DataRowView>()
                .AsQueryable()
                .Where(i=> string.IsNullOrEmpty(i[fieldDesc] as string))
                .Select(r => (int)r["SubjectID"])
                ;
            else
			    return this.DataTable.DefaultView
				    .Cast<DataRowView>()
				    .AsQueryable()
                    .Where(this.GetPredicate(criterion, limitToProminentFields, fieldDesc))
				    .Select(r => (int)r["SubjectID"])
			    ;
		}

		/// <summary>
		/// Builds a predicate for searching/filtering on custom Subject fields
		/// </summary>
		/// <param name="criterion"></param>
		/// <param name="limitToProminentFields"></param>
		/// <returns></returns>
		private Expression<Func<DataRowView, bool>> GetPredicate(string criterion, bool limitToProminentFields, string fieldDesc)
		{
			// Instantiate the predicate
			Expression<Func<DataRowView, bool>> predicate = PredicateBuilder.Prepare<DataRowView>(false);

			// Limit fields search to those marked searchable, and furthermore prominent fields (if specified)
            var  searchableFields = _flowProject.ProjectSubjectFieldList.Where(
                 f => (f.FieldDataTypeID == 1 && f.IsSearchableField &&
                 (f.IsProminentField || !limitToProminentFields))
             );

            if(fieldDesc != null && fieldDesc != "All Fields")
                searchableFields = _flowProject.ProjectSubjectFieldList.Where(
                 f => f.FieldDataTypeID == 1 && f.ProjectSubjectFieldDesc == fieldDesc
             );

			// Prepare the Regex expression 
			bool isWildcard = false;
			Regex wildcardCriterion = null;

			isWildcard = criterion.Contains("*");
            if (!isWildcard && criterion.Length > 0 && !this._flowProject.FlowMasterDataContext.PreferenceManager.Application.SearchExactMatch)
            {
                criterion = "*" + criterion + "*";
                isWildcard = true;
            }

            
    			wildcardCriterion = criterion.ToRegex();

			// Iterate through the list of searchable fields and add the field to the filter condition
			foreach (ProjectSubjectField field in searchableFields)
			{
				string fieldName = field.ProjectSubjectFieldDisplayName;

				switch (field.FieldDataTypeID)
				{
					// int
					case FieldDataType.FIELD_DATA_TYPE_INTEGER:
						int tryInt;
						if (!isWildcard && Int32.TryParse(criterion, out tryInt))
							predicate = predicate.Or(r => r[fieldName].Equals(tryInt));

						break;

					// decimal and money
					case FieldDataType.FIELD_DATA_TYPE_DECIMAL:
					case FieldDataType.FIELD_DATA_TYPE_MONEY:
						decimal tryDecimal;
						if (!isWildcard && Decimal.TryParse(criterion, out tryDecimal))
							predicate = predicate.Or(r => r[fieldName].Equals(tryDecimal));

						break;

					// DateTime
					case FieldDataType.FIELD_DATA_TYPE_DATETIME:
						DateTime tryDateTime;
						if (!isWildcard && DateTime.TryParse(criterion, out tryDateTime))
							predicate = predicate.Or(r => r[fieldName].Equals(tryDateTime));

						break;

					// string
					default:
                        if (!isWildcard && criterion == "")
                            predicate = predicate.Or(r => r[fieldName].Equals(""));
                        else
                        {
                            predicate = predicate.Or(r =>
                                r[fieldName] != DBNull.Value &&
                                wildcardCriterion.IsMatch((string)r[fieldName])
                            );
                        }
						break;
				}
			}

			return predicate;
		}


		#endregion // PUBLIC METHODS



        public bool IsSorted = false;


    }	// END class


	public enum SubjectSearchResult
	{
		HasResults,
		NoResults,
		Cleared
	}




}	// END namespace
