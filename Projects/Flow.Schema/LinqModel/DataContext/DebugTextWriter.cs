﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.Linq;
using System.Data.SqlServerCe;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Media.Imaging;

using Flow.Lib;
using Flow.Lib.Database.SqlCe;
using Flow.Lib.Helpers;
using Flow.Schema.Properties;
using Flow.Schema.Utility;


namespace Flow.Schema.LinqModel.DataContext
{
	/// <summary>
	/// Provides text to output (debug);
	/// can be toggled on/off by changing the project setting "OutputSqlCeCommandText"
	/// </summary>
	class DebugTextWriter : System.IO.TextWriter
	{
		public override void Write(char[] buffer, int index, int count)
		{
			System.Diagnostics.Debug.Write(new String(buffer, index, count));
		}

		public override void Write(string value)
		{
			System.Diagnostics.Debug.Write(value);
		}

		public override System.Text.Encoding Encoding
		{
			get { return System.Text.Encoding.Default; }
		}
	}
}
