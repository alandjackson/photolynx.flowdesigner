﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

using Flow.Lib.Helpers;
using Flow.Lib.Helpers.ImageQuix;
using NetServ.Net.Json;
using System.Text.RegularExpressions;

namespace Flow.Schema.LinqModel.DataContext
{
	partial class ImageQuixCatalogOption
	{
        //private FlowObservableCollection<ImageQuixOptionCommand> _optionCommandList = null;
        //public FlowObservableCollection<ImageQuixOptionCommand> OptionCommandList
        //{
        //    get
        //    {
        //        if (_optionCommandList == null)
        //            _optionCommandList = new FlowObservableCollection<ImageQuixOptionCommand>(this.ImageQuixOptionCommands);

        //        return _optionCommandList;
        //    }
        //}
	
		public ImageQuixCatalogOptionGroup Parent { get; set; }
	    
        //public ImageQuixCatalogOption(XElement optionDef, ImageQuixOptionGroup parentGroup)
        //    : this()
        //{
        //    this.Parent = parentGroup;

        //    ImageQuixCatalogListing listing = ImageQuixUtility.ReadCatalogListing(optionDef);
        //    this.PrimaryKey = listing.PrimaryKey;
        //    this.Label = listing.Label;
        //    this.ImageQuixOptionGroupPk = parentGroup.PrimaryKey;
        //    this.ResourceURL = listing.ResourceURL;

        //    parentGroup.OptionList.Add(this);

        //    foreach (XElement optionCommandDef in optionDef.Elements("OptionCommand"))
        //    {
        //        this.OptionCommandList.Add(new ImageQuixOptionCommand(optionCommandDef, this));
        //    }		
        //}

        public ImageQuixCatalogOption(JsonObject optionDef, ImageQuixCatalogOptionGroup parentGroup, string defaultOptionRef, string CustomerID)
            : this()
        {
            if (optionDef.ContainsKey("internalLabID") && (optionDef["internalLabID"].JsonTypeCode != JsonTypeCode.Null))
            {
                //this.SKU = (((JsonString)optionDef["internal-lab-id"]).Value);
                string internalLabId = (((JsonString)optionDef["internalLabID"]).Value);
                bool showProduct = true;
                if (internalLabId.Contains("["))
                {
                    showProduct = false;
                    var pattern = @"\[(.*?)\]";
                    var matches = Regex.Matches(internalLabId, pattern);

                    foreach (Match m in matches)
                    {
                        if (m.Groups[1].Value == CustomerID)
                        {
                            showProduct = true;
                            internalLabId = internalLabId.Split("[")[0];
                        }
                    }
                }

                if (!showProduct)
                {
                    return;
                }

                this.SKU = internalLabId;
            }

            this.Parent = parentGroup;
            this.PrimaryKey = Convert.ToInt32(((JsonNumber)optionDef["id"]).Value);
            this.Label = (((JsonString)optionDef["name"]).Value);
            this.ResourceURL = (((JsonString)optionDef["resource"]).Value);
            if (this.ResourceURL == defaultOptionRef)
                this.IsDefault = true;
            this.Price = Convert.ToDecimal(((JsonNumber)optionDef["price"]).Value);
            this.ImageQuixCatalogOptionGroupPk = parentGroup.PrimaryKey;
            this.ImageQuixCatalogOptionGroupID = parentGroup.ImageQuixCatalogOptionGroupID;

            

            JsonArray aSets = ((JsonArray)optionDef["attributeSets"]);
            foreach (JsonObject attributeSet in aSets)
            {
                JsonArray attribues = ((JsonArray)attributeSet["attributes"]);
                foreach (JsonObject attribute in attribues)
                {
                    string aName = (((JsonString)attribute["attributeName"]).Value);
                    string aValue = (((JsonString)attribute["value"]).Value);
                    if (aName == "appliesTo")
                        this.AppliesTo = aValue;
                }
            }


           
        }
	}
}
