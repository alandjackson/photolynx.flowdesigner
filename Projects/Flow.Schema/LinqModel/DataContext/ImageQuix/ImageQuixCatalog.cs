﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Xml.Linq;

using Flow.Lib.Helpers;
using Flow.Lib.Helpers.ImageQuix;
using NetServ.Net.Json;
using Flow.Lib;
using System.Windows.Threading;

namespace Flow.Schema.LinqModel.DataContext
{
    partial class ImageQuixCatalog : IImageQuixProductParent
    {
        public FlowMasterDataContext FlowMasterDataContext { get; set; }


        private FlowObservableCollection<ImageQuixCatalogOptionGroup> _catalogOptionGroupList = null;
        public FlowObservableCollection<ImageQuixCatalogOptionGroup> CatalogOptionGroupList
        {
            get
            {
                if (_catalogOptionGroupList == null)
                {
                    //_catalogOptionGroupList = new FlowObservableCollection<ImageQuixCatalogOptionGroup>();
                    //foreach (ImageQuixCatalogOptionGroup grp in this.FlowMasterDataContext.ImageQuixCatalogOptionGroups.Where(og=>og.ImageQuixCatalogID == this.ImageQuixCatalogID))
                    //{
                    //    _catalogOptionGroupList.Add(grp);
                    //}
                    //if (_catalogOptionGroupList == null)

                    //_catalogOptionGroupList = new FlowObservableCollection<ImageQuixCatalogOptionGroup>(this.ImageQuixCatalogOptionGroups.Where(i=>i.IsImageOption != true));
                    _catalogOptionGroupList = new FlowObservableCollection<ImageQuixCatalogOptionGroup>(this.ImageQuixCatalogOptionGroups);
                }

                return _catalogOptionGroupList;
            }
        }

        //public void UpdateProductsForUI()
        //{
        //    this.SendPropertyChanged("ProductList");
        //    this.SendPropertyChanged("ImageQuixProductGroups");
        //    this.SendPropertyChanged("ProductGroupList");
        //}
        private FlowObservableCollection<ImageQuixCatalogOption> _catalogOptionList = null;
        public FlowObservableCollection<ImageQuixCatalogOption> CatalogOptionList
        {
            get
            {
                if (_catalogOptionList == null)
                {
                    _catalogOptionList = new FlowObservableCollection<ImageQuixCatalogOption>();
                    foreach(ImageQuixCatalogOptionGroup og in CatalogOptionGroupList)
                    {
                        foreach (ImageQuixCatalogOption opt in og.ImageQuixCatalogOptions)
                        {
                            _catalogOptionList.Add(opt);
                        }
                    }
                }

                return _catalogOptionList;
            }

            
        }
        public void RefreshCatalogOptionList()
        {

                _catalogOptionList = null;
                _catalogOptionGroupList = null;
                this.SendPropertyChanged("CatalogOptionList");
        }

        private FlowObservableCollection<ImageQuixProductGroup> _productGroupList = null;
        public FlowObservableCollection<ImageQuixProductGroup> ProductGroupList
        {
            get
            {
                if (_productGroupList == null)
                    _productGroupList = new FlowObservableCollection<ImageQuixProductGroup>(this.ImageQuixProductGroups);

                return _productGroupList;
            }
        }

        private FlowObservableCollection<ImageQuixProduct> _productList = null;
        public FlowObservableCollection<ImageQuixProduct> ProductList
        {
            get
            {
                if (_productList == null)
                    _productList = new FlowObservableCollection<ImageQuixProduct>(this.ImageQuixProducts);

                return _productList;
            }

            private set
            {
                _productList = value;
                this.SendPropertyChanged("ProductList");
            }
        }

        public void RefreshProductList()
        {
            _productGroupList = null;
            _productList = null;
            SendPropertyChanged("ImageQuixProductGroups");
            SendPropertyChanged("ProductList");
            
        }

        public ImageQuixCatalog(ImageQuixCatalogListing listing, FlowMasterDataContext flowMasterDataContext)
            : this()
        {
            this.FlowMasterDataContext = flowMasterDataContext;

            this.PrimaryKey = listing.PrimaryKey;

            int catalogIndex = flowMasterDataContext.ImageQuixCatalogList.Count(c => c.PrimaryKey == this.PrimaryKey) + 1;
            this.Label = listing.Label;// +" (" + catalogIndex + ")";
            this.ModifyDate = Convert.ToDateTime(listing.ModifyDate);
            this.ResourceURL = listing.ResourceURL;
        }

        /// <summary>
        /// Removes all child objects from the catalog prior to deletion
        /// </summary>
        public void PrepareForDeletion(FlowMasterDataContext flowMasterDataContext)
        {
            if (this.FlowMasterDataContext == null)
                FlowMasterDataContext = flowMasterDataContext;

            //foreach (ImageQuixCatalogOptionGroup oGroup in this.CatalogOptionGroupList)
            //{
            //    foreach (ImageQuixCatalogOption o in oGroup.OptionList)
            //    {
            //        flowMasterDataContext.ImageQuixCatalogOptions.DeleteOnSubmit(o);
            //    }
            //    flowMasterDataContext.ImageQuixCatalogOptionGroups.DeleteOnSubmit(oGroup);
            //}
            flowMasterDataContext.ImageQuixCatalogOptions.DeleteAllOnSubmit(flowMasterDataContext.ImageQuixCatalogOptions);
            flowMasterDataContext.ImageQuixCatalogOptionGroups.DeleteAllOnSubmit(flowMasterDataContext.ImageQuixCatalogOptionGroups);

            foreach (ImageQuixProduct product in this.ProductList)
            {
                foreach (ImageQuixOptionGroup optionGroup in product.OptionGroupList)
                {
                    foreach (ImageQuixOption option in optionGroup.OptionList)
                    {
                        flowMasterDataContext.ImageQuixOptionCommands.DeleteAllOnSubmit(option.OptionCommandList);
                    }

                    flowMasterDataContext.ImageQuixOptions.DeleteAllOnSubmit(optionGroup.OptionList);
                }

                flowMasterDataContext.ImageQuixOptionGroups.DeleteAllOnSubmit(product.OptionGroupList);
                flowMasterDataContext.ImageQuixProductNodes.DeleteAllOnSubmit(product.ProductNodeList);
            }

            

            foreach (ImageQuixProductGroup productGroup in this.ProductGroupList)
            {
                this.ImageQuixProducts.Clear();
            }

            flowMasterDataContext.ImageQuixProducts.DeleteAllOnSubmit(this.ProductList);
            this.ProductList.Clear();

            flowMasterDataContext.ImageQuixProductGroups.DeleteAllOnSubmit(this.ProductGroupList);
        }

        

        /// <summary>
        /// Creates a new ImageQuix catalog from the defining xml fragment
        /// </summary>
        /// <param name="catalogDef"></param>
        /// <param name="flowMasterDataContext"></param>
        public static void CreateImageQuixCatalog(ImageQuixCatalogListing parentCatalog, JsonArray catalogDef, FlowMasterDataContext flowMasterDataContext, NotificationProgressInfo progressInfo, Dispatcher dispatcher, String CustomerID)
        {
            string uname = flowMasterDataContext.PreferenceManager.Upload.CatalogRequest.GetCatalogUsername;
            string pword = flowMasterDataContext.PreferenceManager.Upload.CatalogRequest.GetCatalogPassword;

            string serviceURL = flowMasterDataContext.PreferenceManager.Upload.CatalogRequest.ImageQuixBaseUrl;

            //dont create a new catalog if it exists, lets just update it.
            ImageQuixCatalog newItem = null;
            foreach (ImageQuixCatalog cat in flowMasterDataContext.ImageQuixCatalogList)
            {
                if (progressInfo != null)
                {
                    progressInfo.Progress++;
                    progressInfo.Update();
                }
                if (cat.PrimaryKey == parentCatalog.PrimaryKey)
                {
                    //if the new modifydate is older or the same as the old one, lets bail - no update needed
                    if (cat.ModifyDate >= Convert.ToDateTime(parentCatalog.ModifyDate))
                    {
                        //return;
                        //lets update anyway, the user might have deleted unused products
                    }

                    newItem = cat;
                    newItem.FlowMasterDataContext = flowMasterDataContext;
                    newItem.ModifyDate = Convert.ToDateTime(parentCatalog.ModifyDate);
                    newItem.Label = parentCatalog.Label;
                }
            }
            //if newItem is null, than we have a new catalog, lets create it
            if (newItem == null)
            {
                newItem = new ImageQuixCatalog(parentCatalog, flowMasterDataContext);
                dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                {
                    flowMasterDataContext.ImageQuixCatalogList.Add(newItem);
                }));
            }
            
            //if we made it hear, either we added a new catalog, or the ModifyDate changed on an existing catalog
            flowMasterDataContext.SubmitChanges();


            //do the catalog options:
            if (parentCatalog.OptionGroups != null && parentCatalog.OptionGroups.Count > 0)
            {
                newItem.processOptionGroups(parentCatalog.OptionGroups, dispatcher, CustomerID);
            }

            //Check for options on the main service url
            //JsonArray serviceDef = null;
            string caturl = ReplaceLastOccurrence(parentCatalog.ResourceURL, "catalog", "catalogdump");

            JsonArray serviceDef = ImageQuixUtility.GetImageQuixJsonDocument(
                    caturl,
                    "optionGroups", uname, pword
                );
            if (serviceDef != null && serviceDef.Count > 0)
            {
                newItem.processOptionGroups(serviceDef, dispatcher, CustomerID);
            }


            JsonArray serviceDef2 = ImageQuixUtility.GetImageQuixJsonDocument(
                    serviceURL,
                    "optionGroups", uname, pword
                );
            if (serviceDef2 != null && serviceDef2.Count > 0)
            {
                newItem.processOptionGroups(serviceDef2, dispatcher, CustomerID);
            }

            flowMasterDataContext.alltempProdIDs = new List<string>();

            foreach (JsonObject productGroupDef in catalogDef)
            {
                if (progressInfo != null)
                {
                    progressInfo.Progress++;
                    progressInfo.Update();
                }
                //ImageQuixCatalog newItem = new ImageQuixCatalog(productGroupDef, flowMasterDataContext);

                //flowMasterDataContext.ImageQuixCatalogList.Add(newItem);

                //flowMasterDataContext.SubmitChanges();

                int id = Convert.ToInt32(((JsonNumber)productGroupDef["id"]).Value);
                string resourceUrl = (((JsonString)productGroupDef["resource"]).Value);
                JsonObject productGroups = (JsonObject)productGroupDef["productGroupRef"];
                JsonObject products = (JsonObject)productGroupDef["productRef"];

                ImageQuixProductGroup newPG = null;
                ImageQuixCatalogListing productGroupListing = ImageQuixUtility.ReadJsonCatalogListing(productGroupDef);
                int foundMatch = 0;
                foreach (ImageQuixProductGroup group in newItem.ImageQuixProductGroups)
                {
                    if (group.PrimaryKey == productGroupListing.PrimaryKey)
                    {

                        //group.ParentProductGroup = newPG.ParentProductGroup;
                        group.ImageQuixCatalog = newItem;
                        group.Label = productGroupListing.Label;
                        group.ResourceURL = productGroupListing.ResourceURL;
                        newPG = group;
                        foundMatch = 1;
                        break;
                    }
                }
                if (foundMatch == 0)
                {
                    newPG = new ImageQuixProductGroup(productGroupDef, newItem);
                    dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                    {
                        newItem.ProductGroupList.Add(newPG);
                    }));

                }
                flowMasterDataContext.SubmitChanges();
                
                newPG.ProcessProductGroup(productGroupDef, uname, pword, progressInfo, dispatcher, CustomerID, flowMasterDataContext);


                

                // Assign the node IDs to the option commands (where applicable)
                foreach (ImageQuixOptionCommand optionCommand in flowMasterDataContext.ImageQuixOptionCommands)
                {
                    if (progressInfo != null)
                    {
                        progressInfo.Progress++;
                        progressInfo.Update();
                    }
                    ImageQuixProductNode productNode = flowMasterDataContext.ImageQuixProductNodes
                        .FirstOrDefault(n => n.PrimaryKey == optionCommand.NodePrimaryKey);

                    optionCommand.ImageQuixProductNodeID = (productNode == null)
                        ? null
                        : (int?)productNode.ImageQuixProductNodeID
                    ;
                }

                flowMasterDataContext.SubmitChanges();

                // Load the product list for display in the UI
                //newItem.ProductList = new FlowObservableCollection<ImageQuixProduct>(newItem.ImageQuixProducts);
                newItem.RefreshCatalogOptionList();
                newItem.RefreshProductList();

            }

            //Any products get removed?
            foreach (ImageQuixProduct p in newItem.ImageQuixProducts)
            {
                if (!flowMasterDataContext.alltempProdIDs.Contains(p.ResourceURL))
                {
                    //this item is missing from the catalog
                    if(p.RetiredOn == null)
                        p.RetiredOn = DateTime.Now.AddDays(-1);
                }
            }


        }


        public static string ReplaceLastOccurrence(string Source, string Find, string Replace)
        {
            if (Source.Length <= Find.Length)
                return Source;
            int Place = Source.LastIndexOf(Find);
            if (Place >= 0)
            {
                string result = Source.Remove(Place, Find.Length).Insert(Place, Replace);
                return result;
            }
            return Source;
        }

        public void processOptionGroups(JsonArray optionGroups, Dispatcher dispatcher, string CustomerID)
        {
            foreach (JsonObject optionGroup in optionGroups)
            {
                string defaultOptionRef = null;
                if ((optionGroup["defaultOptionRef"]) != JsonNull.Null)
                    defaultOptionRef = ((JsonString)optionGroup["defaultOptionRef"]).Value;
                else
                    defaultOptionRef = null;

                ImageQuixCatalogOptionGroup newOG = null;
                Boolean foundMatch = false;
                foreach (ImageQuixCatalogOptionGroup og in this.CatalogOptionGroupList)
                {
                    if (og.PrimaryKey == Convert.ToInt32(((JsonNumber)optionGroup["id"]).Value))
                    {
                        foundMatch = true;

                        og.Parent = this;
                        og.ImageQuixCatalogID = this.ImageQuixCatalogID;
                        og.PrimaryKey = Convert.ToInt32(((JsonNumber)optionGroup["id"]).Value);
                        og.Label = (((JsonString)optionGroup["name"]).Value);
                        og.ResourceURL = (((JsonString)optionGroup["resource"]).Value);
                        //og.IsMandatory = (((JsonBoolean)optionGroup["isMandatory"]).Value);
                        if ((((JsonString)optionGroup["requiredness"]).Value == "required"))
                            og.IsMandatory = true;
                        else
                            og.IsMandatory = false;
                        newOG = og;
                    }

                }
                if (!foundMatch)
                {
                    newOG = new ImageQuixCatalogOptionGroup(optionGroup, this);
                    dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                    {
                        this.FlowMasterDataContext.ImageQuixCatalogOptionGroups.InsertOnSubmit(newOG);
                        this.CatalogOptionGroupList.Add(newOG);
                    }));

                }
                this.FlowMasterDataContext.SubmitChanges();
                newOG.processOptions(optionGroup, defaultOptionRef, CustomerID);

                this.FlowMasterDataContext.SubmitChanges();

            }
        }
    }

	/// <summary>
	/// 
	/// </summary>
	public interface IImageQuixProductParent { }

}
