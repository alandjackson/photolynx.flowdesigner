﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

using Flow.Lib.Helpers;
using Flow.Lib.Helpers.ImageQuix;
using NetServ.Net.Json;

namespace Flow.Schema.LinqModel.DataContext
{
	partial class ImageQuixOption
	{
		private FlowObservableCollection<ImageQuixOptionCommand> _optionCommandList = null;
		public FlowObservableCollection<ImageQuixOptionCommand> OptionCommandList
		{
			get
			{
				if (_optionCommandList == null)
					_optionCommandList = new FlowObservableCollection<ImageQuixOptionCommand>(this.ImageQuixOptionCommands);

				return _optionCommandList;
			}
		}
	
		public ImageQuixOptionGroup Parent { get; set; }
	
		public ImageQuixOption(XElement optionDef, ImageQuixOptionGroup parentGroup)
			: this()
		{
			this.Parent = parentGroup;

            ImageQuixCatalogListing listing = ImageQuixUtility.ReadCatalogListing(optionDef);
            this.PrimaryKey = listing.PrimaryKey;
            this.Label = listing.Label;
			this.ImageQuixOptionGroupPk = parentGroup.PrimaryKey;
            this.ResourceURL = listing.ResourceURL;

			parentGroup.OptionList.Add(this);

			foreach (XElement optionCommandDef in optionDef.Elements("OptionCommand"))
			{
				this.OptionCommandList.Add(new ImageQuixOptionCommand(optionCommandDef, this));
			}		
		}

        public ImageQuixOption(JsonObject optionDef, ImageQuixOptionGroup parentGroup, string defaultOptionRef)
            : this()
        {
            this.Parent = parentGroup;
            this.PrimaryKey = Convert.ToInt32(((JsonNumber)optionDef["id"]).Value);
            this.Label = (((JsonString)optionDef["name"]).Value);
            this.ResourceURL = (((JsonString)optionDef["resource"]).Value);
            if (this.ResourceURL == defaultOptionRef)
                this.IsDefault = true;
            this.ImageQuixOptionGroupPk = parentGroup.PrimaryKey;

            

           
        }
	}
}
