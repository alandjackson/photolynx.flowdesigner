﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

using Flow.Lib.Helpers;
using Flow.Lib.Helpers.ImageQuix;
using NetServ.Net.Json;

namespace Flow.Schema.LinqModel.DataContext
{
	partial class ImageQuixProductNode
	{
		public ImageQuixProductNodeType NodeType { get { return (ImageQuixProductNodeType)this.Type; } }

		public ImageQuixProductNode(XElement productNodeDef, ImageQuixProduct parentProduct)
			: this()
		{
			this.ImageQuixProduct = parentProduct;

            ImageQuixCatalogListing listing = ImageQuixUtility.ReadCatalogListing(productNodeDef);
            this.PrimaryKey = listing.PrimaryKey;
            this.Label = listing.Label;
            this.ResourceURL = listing.ResourceURL;
 

			string nodeType = productNodeDef.Attribute("type").Value;
			switch (nodeType)
			{
				case "image":
					this.Type = 1;
					break;
				case "text":
					this.Type = 2;
					break;
                case "hidden":
                    this.Type = 3;
                    break;
				default:
					this.Type = 0;
					break;
			}

			this.Width = (double?)productNodeDef.Attribute("w");
			this.Height = (double?)productNodeDef.Attribute("h");
			this.X = (double?)productNodeDef.Attribute("x");
			this.Y = (double?)productNodeDef.Attribute("y");

			//this.NodeID = (int)productNodeDef.Attribute("nodeid");
			//this.NodeTypeFk = (int)productNodeDef.Attribute("nodetypefk");
			//this.ProductFk = (int)productNodeDef.Attribute("productfk");

			//this.ImageFile = productNodeDef.Attribute("label").NormalizeXAttribute();
			//this.ImageMask = productNodeDef.Attribute("label").NormalizeXAttribute();

			//this.SortOrder = (int)productNodeDef.Attribute("sortorder");
			//this.SourceFk = (string)productNodeDef.Attribute("sourcefk");
			//this.SourceTypeFk = (int)productNodeDef.Attribute("sourcetypefk");
			//this.StatusFk = (int)productNodeDef.Attribute("statusfk");
		}

        public ImageQuixProductNode(JsonObject productNodeDef, ImageQuixProduct parentProduct)
            : this()
        {
            this.ImageQuixProduct = parentProduct;

            this.PrimaryKey = Convert.ToInt32(((JsonNumber)productNodeDef["id"]).Value);
            this.Label = (((JsonString)productNodeDef["name"]).Value);
            this.ResourceURL = (((JsonString)productNodeDef["resource"]).Value);

            this.Width = Convert.ToDouble(((JsonNumber)productNodeDef["width"]).Value);
            this.Height = Convert.ToDouble(((JsonNumber)productNodeDef["height"]).Value);
            this.X = Convert.ToDouble(((JsonNumber)productNodeDef["x"]).Value);
            this.Y = Convert.ToDouble(((JsonNumber)productNodeDef["y"]).Value);

            string nodeType = (((JsonString)productNodeDef["type"]).Value);
            switch (nodeType)
            {
                case "image":
                    this.Type = 1;
                    break;
                case "text":
                    this.Type = 2;
                    break;
                case "hidden":
                    this.Type = 3;
                    break;
                default:
                    this.Type = 0;
                    break;
            }
        }

	}

	public enum ImageQuixProductNodeType
	{
		None = 0,
		Image = 1,
		Text = 2,
        Hidden = 3
	}


}
