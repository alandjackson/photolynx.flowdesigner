﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Flow.Schema.LinqModel.DataContext
{
	/// <summary>
	/// Describes a pricing tier (the price of a product for a given quantity)
	/// </summary>

	public class ImageQuixPricingTier
	{
		[XmlAttribute("min")]	
		public int MinQuantity { get; set; }

		[XmlAttribute("max")]
		public int MaxQuantity { get; set; }
	
		[XmlAttribute("price")]
		public decimal Price { get; set; }

		public ImageQuixPricingTier() { }
	
		public ImageQuixPricingTier(decimal price)
		{
			this.MinQuantity = 1;
			this.MaxQuantity = Int32.MaxValue;
			this.Price = price;
		}

		public ImageQuixPricingTier(int minQuantity, decimal price)
		{
			this.MinQuantity = minQuantity;
			this.MaxQuantity = Int32.MaxValue;
			this.Price = price;
		}

		public bool IsInRange(int quantity)
		{
			return (this.MinQuantity <= quantity) && (quantity <= this.MaxQuantity);
		}


		/// <summary>
		/// Returns a list of pricing tiers from a source string; the source string is a comma-delimited list
		/// of tuples (min quantity [int], price [decimal]); e.g.: 1,1.60,5,1.35,...
		/// </summary>
		/// <param name="priceString"></param>
		/// <returns></returns>
		public static ImageQuixPricingTierList GetPricingTierList(string priceString)
		{
			if (String.IsNullOrEmpty(priceString)) return null;

			IOrderedEnumerable<ImageQuixPricingTier> retVal = null;

			ImageQuixPricingTierList valueList = new ImageQuixPricingTierList();

			string[] values = priceString.Split(',');

			// If this is a single-priced product...
			if (values.Length == 1)
			{
				decimal tryDecimal;

				// If the value is a valid decimal, create the tier
				if (Decimal.TryParse(values[0], out tryDecimal))
				{
					valueList.Add(new ImageQuixPricingTier(tryDecimal));
					retVal = valueList.OrderBy(t => t.MinQuantity);
				}

				else
				{
					// throw error
				}

			}
			
			// If this is a multi-tiered 
			else
			{
				// There should be an even number of items in the source list...
				if (values.Length % 2 == 1)
				{
					// throw error
				}
				else
				{
					// Iterate through the min quantity/price pairs
					for (int i = 0; i < values.Length; i += 2)
					{
						// Test for valid min quantity [int]
						int tryInt;
						if (Int32.TryParse(values[i], out tryInt))
						{
							// Test for valid price [decimal]
							decimal tryDecimal;
							if (Decimal.TryParse(values[i + 1], out tryDecimal))
							{
								// Add the pricing tier to the list
								valueList.Add(new ImageQuixPricingTier(tryInt, tryDecimal));
							}
							else
							{
								// throw error
							}
						}
						else
						{
							// throw error
						}
					}

					// Order the list by min price
					retVal = valueList.OrderBy(t => t.MinQuantity);

					// Iterate through the tiers, assigning the min value -1 of each tier
					// as the max value of the prior tier to define the range
					for (int i = 1; i < retVal.Count(); i++)
					{
						retVal.ElementAt(i - 1).MaxQuantity = retVal.ElementAt(i).MinQuantity - 1;
					}
				}
			}

			return new ImageQuixPricingTierList(retVal);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pricingTiers"></param>
		/// <returns></returns>
		public static byte[] Serialize(ImageQuixPricingTierList pricingTiers)
		{
			XmlSerializer xs = new XmlSerializer(typeof(ImageQuixPricingTierList));

			MemoryStream ms = new MemoryStream();
			UTF8Encoding ec = new UTF8Encoding();

			XmlTextWriter xw = new XmlTextWriter(ms, ec);

			xs.Serialize(xw, pricingTiers);

			return ms.GetBuffer().ToArray(); 
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="data"></param>
		/// <returns></returns>
		public static ImageQuixPricingTierList Deserialize(byte[] data)
		{
			if (data == null) return null;

			XmlSerializer xs = new XmlSerializer(typeof(ImageQuixPricingTierList));

			XmlTextReader xr = new XmlTextReader(
				new MemoryStream(data)
			);

			object obj = xs.Deserialize(xr);

			return obj as ImageQuixPricingTierList;
		}


	}

	public class ImageQuixPricingTierList : List<ImageQuixPricingTier>
	{
		public ImageQuixPricingTierList() : base() { }
		public ImageQuixPricingTierList(IEnumerable<ImageQuixPricingTier> pricingTiers) : base(pricingTiers) { }
	}

}
