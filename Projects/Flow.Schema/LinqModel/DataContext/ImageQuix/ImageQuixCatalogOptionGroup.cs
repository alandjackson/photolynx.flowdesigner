﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Xml.Linq;

using Flow.Lib.Helpers;
using NetServ.Net.Json;
using System.Data.Linq;
using System.Text.RegularExpressions;

namespace Flow.Schema.LinqModel.DataContext
{
	partial class ImageQuixCatalogOptionGroup
	{
        //FlowMasterDataContext FlowMasterDataContect {get;set;}

		private FlowObservableCollection<ImageQuixCatalogOption> _optionList = null;
		public FlowObservableCollection<ImageQuixCatalogOption> OptionList
		{
			get
			{
                //if (_optionList == null)
               // {
                //    if (this.ImageQuixOptions != null && this.ImageQuixOptions.Count() > 0)
                //        _optionList = new FlowObservableCollection<ImageQuixOption>(this.ImageQuixOptions.OrderBy(g => g.Label));
                //    else
                        _optionList = new FlowObservableCollection<ImageQuixCatalogOption>();
                        if (this.Parent != null)
                        {
                            
                            foreach (ImageQuixCatalogOption grp in this.Parent.FlowMasterDataContext.ImageQuixCatalogOptions.Where(o => o.ImageQuixCatalogOptionGroupID == this.ImageQuixCatalogOptionGroupID))
                            {
                                _optionList.Add(grp);
                            }
                        }
               // }

				return _optionList;
			}
		}

		private List<ImageQuixCatalogOption> _optionDisplayList = null;
		public List<ImageQuixCatalogOption> OptionDisplayList
		{
			get
			{
				if (_optionDisplayList == null)
				{
					_optionDisplayList = new List<ImageQuixCatalogOption>(this.OptionList.OrderBy(o => o.Label).ToList());

					if (!(bool)this.IsMandatory)
					{
						ImageQuixCatalogOption nullOption = new ImageQuixCatalogOption();
						nullOption.PrimaryKey = -1;
						nullOption.Label = "<None>";
                       
						_optionDisplayList.Insert(0, nullOption);
					}
				}

                return _optionDisplayList;
			}
		}

		public ImageQuixCatalog Parent { get; set; }


        //public ImageQuixCatalogOptionGroup(XElement optionGroupDef, ImageQuixCatalog parentCatalog)
        //    : this()
        //{
        //    this.Parent = parentCatalog;

        //    this.Label = optionGroupDef.Attribute("title").NormalizeXAttribute(128);
        //    this.PrimaryKey = (int)optionGroupDef.Attribute("pk");
        //    this.IsMandatory = (bool)optionGroupDef.Attribute("isMandatory");

        //    parentProduct.OptionGroupList.Add(this);

        //    foreach (XElement optionDef in optionGroupDef.Elements("Option"))
        //    {
        //        new ImageQuixOption(optionDef, this);
        //    }
        //}

        public ImageQuixCatalogOptionGroup(JsonObject optionGroupDef, ImageQuixCatalog parentCatalog)
            : this()
        {
            this.Parent = parentCatalog;
            this.ImageQuixCatalogID = parentCatalog.ImageQuixCatalogID;
            this.PrimaryKey = Convert.ToInt32(((JsonNumber)optionGroupDef["id"]).Value);
            this.Label = (((JsonString)optionGroupDef["name"]).Value);
            this.ResourceURL = (((JsonString)optionGroupDef["resource"]).Value);
            //this.IsMandatory = (((JsonBoolean)optionGroupDef["isMandatory"]).Value);
            if ((((JsonString)optionGroupDef["requiredness"]).Value == "required"))
                this.IsMandatory = true;
            else
                this.IsMandatory = false;



        }
        public void processOptions(JsonObject optionGroupDef, string defaultOptionRef, string CustomerID)
        {
            JsonArray options = (JsonArray)optionGroupDef["options"];
            foreach (JsonObject option in options)
            {
                ImageQuixCatalogOption newOption = null;
                Boolean foundMatch = false;
                foreach (ImageQuixCatalogOption opt in this.OptionList)
                {
                    if (opt.PrimaryKey == Convert.ToInt32(((JsonNumber)option["id"]).Value))
                    {
                        foundMatch = true;
                        opt.Parent = this;
                        opt.PrimaryKey = Convert.ToInt32(((JsonNumber)option["id"]).Value);
                        opt.Price = Convert.ToDecimal(((JsonNumber)option["price"]).Value);
                        opt.Label = (((JsonString)option["name"]).Value);
                        opt.ResourceURL = (((JsonString)option["resource"]).Value);
                        opt.ImageQuixCatalogOptionGroupPk = this.PrimaryKey;
                        opt.ImageQuixCatalogOptionGroupID = this.ImageQuixCatalogOptionGroupID;
                        if (option.ContainsKey("internalLabID") && (option["internalLabID"].JsonTypeCode != JsonTypeCode.Null))
                        {
                            //opt.SKU = (((JsonString)option["internal-lab-id"]).Value);
                            string internalLabId = (((JsonString)option["internalLabID"]).Value);
                            bool showProduct = true;
                            if (internalLabId.Contains("["))
                            {
                                showProduct = false;
                                var pattern = @"\[(.*?)\]";
                                var matches = Regex.Matches(internalLabId, pattern);

                                foreach (Match m in matches)
                                {
                                    if (m.Groups[1].Value == CustomerID)
                                    {
                                        showProduct = true;
                                        internalLabId = internalLabId.Split("[")[0];
                                    }
                                }
                            }

                            if (!showProduct)
                            {
                                return;
                            }

                            opt.SKU = internalLabId;
                        }
                        JsonArray aSets = ((JsonArray)option["attributeSets"]);
                        foreach(JsonObject attributeSet in aSets)
                        {
                            JsonArray attribues = ((JsonArray)attributeSet["attributes"]);
                            foreach (JsonObject attribute in attribues)
                            {
                                string aName = (((JsonString)attribute["attributeName"]).Value);
                                string aValue = (((JsonString)attribute["value"]).Value);
                                if (aName == "appliesTo")
                                    opt.AppliesTo = aValue;
                            }
                        }

                        if ((defaultOptionRef == null) || (opt.ResourceURL == defaultOptionRef))
                        {
                            defaultOptionRef = opt.ResourceURL;
                            opt.IsDefault = true;
                        }
                        newOption = opt;
                    }
                }
                if (!foundMatch)
                {
                    newOption = new ImageQuixCatalogOption(option, this, defaultOptionRef, CustomerID);
                    this.Parent.FlowMasterDataContext.ImageQuixCatalogOptions.InsertOnSubmit(newOption);
                    this.OptionList.Add(newOption);
                }
                this.Parent.FlowMasterDataContext.SubmitChanges();
            }

        }

	}
}
