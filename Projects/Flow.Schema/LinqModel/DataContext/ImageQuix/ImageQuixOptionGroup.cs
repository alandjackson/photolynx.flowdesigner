﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Xml.Linq;

using Flow.Lib.Helpers;
using NetServ.Net.Json;
using System.Data.Linq;

namespace Flow.Schema.LinqModel.DataContext
{
	partial class ImageQuixOptionGroup
	{
		private FlowObservableCollection<ImageQuixOption> _optionList = null;
		public FlowObservableCollection<ImageQuixOption> OptionList
		{
			get
			{
                //if (_optionList == null)
               // {
                //    if (this.ImageQuixOptions != null && this.ImageQuixOptions.Count() > 0)
                //        _optionList = new FlowObservableCollection<ImageQuixOption>(this.ImageQuixOptions.OrderBy(g => g.Label));
                //    else
                        _optionList = new FlowObservableCollection<ImageQuixOption>(this.ImageQuixOptions);
               // }

				return _optionList;
			}
		}

		private List<ImageQuixOption> _optionDisplayList = null;
		public List<ImageQuixOption> OptionDisplayList
		{
			get
			{
				if (_optionDisplayList == null)
				{
					_optionDisplayList = new List<ImageQuixOption>(this.OptionList.OrderBy(o => o.Label).ToList());

					if (!this.IsMandatory)
					{
						ImageQuixOption nullOption = new ImageQuixOption();
						nullOption.PrimaryKey = -1;
						nullOption.Label = "<None>";

						_optionDisplayList.Insert(0, nullOption);
					}
				}

                return _optionDisplayList;
			}
		}

		public ImageQuixProduct Parent { get; set; }


		public ImageQuixOptionGroup(XElement optionGroupDef, ImageQuixProduct parentProduct)
			: this()
		{
			this.Parent = parentProduct;

			this.Label = optionGroupDef.Attribute("title").NormalizeXAttribute(128);
			this.PrimaryKey = (int)optionGroupDef.Attribute("pk");
			this.IsMandatory = (bool)optionGroupDef.Attribute("isMandatory");

			parentProduct.OptionGroupList.Add(this);

			foreach (XElement optionDef in optionGroupDef.Elements("Option"))
			{
				new ImageQuixOption(optionDef, this);
			}
		}

        public ImageQuixOptionGroup(JsonObject optionGroupDef, ImageQuixProduct parentProduct)
            : this()
        {
            this.Parent = parentProduct;

            this.PrimaryKey = Convert.ToInt32(((JsonNumber)optionGroupDef["id"]).Value);
            this.Label = (((JsonString)optionGroupDef["name"]).Value);
            this.ResourceURL = (((JsonString)optionGroupDef["resource"]).Value);
            if ((((JsonString)optionGroupDef["requiredness"]).Value == "required"))
                this.IsMandatory = true;
            else
                this.IsMandatory = false;
            //this.IsMandatory = (((JsonBoolean)optionGroupDef["isMandatory"]).Value);


        }
        public void processOptions(JsonObject optionGroupDef ,string defaultOptionRef)
        {
            JsonArray options = (JsonArray)optionGroupDef["options"];
            foreach (JsonObject option in options)
            {
                ImageQuixOption newOption = null;
                Boolean foundMatch = false;
                foreach (ImageQuixOption opt in this.OptionList)
                {
                    if (opt.PrimaryKey == Convert.ToInt32(((JsonNumber)option["id"]).Value))
                    {
                        foundMatch = true;
                        opt.Parent = this;
                        opt.PrimaryKey = Convert.ToInt32(((JsonNumber)option["id"]).Value);
                        opt.Label = (((JsonString)option["name"]).Value);
                        opt.ResourceURL = (((JsonString)option["resource"]).Value);
                        opt.ImageQuixOptionGroupPk = this.PrimaryKey;
                        if ((defaultOptionRef == null) || (opt.ResourceURL == defaultOptionRef))
                        {
                            defaultOptionRef = opt.ResourceURL;
                            opt.IsDefault = true;
                        }
                        newOption = opt;
                    }
                }
                if (!foundMatch)
                {
                    newOption = new ImageQuixOption(option, this, defaultOptionRef);
                    this.OptionList.Add(newOption);
                }
                //this.ImageQuixProduct.ImageQuixCatalog.FlowMasterDataContext.SubmitChanges();
            }

        }

	}
}
