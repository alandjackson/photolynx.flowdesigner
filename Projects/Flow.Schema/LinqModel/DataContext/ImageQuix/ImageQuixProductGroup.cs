﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Xml.Linq;

using Flow.Lib.Helpers;
using Flow.Lib.Helpers.ImageQuix;
using NetServ.Net.Json;
using Flow.Lib;
using System.Windows.Threading;
using System.Text.RegularExpressions;

namespace Flow.Schema.LinqModel.DataContext
{
	partial class ImageQuixProductGroup : IImageQuixProductParent
	{
		private FlowObservableCollection<ImageQuixProduct> _productList = null;
		public FlowObservableCollection<ImageQuixProduct> ProductList
		{
			get
			{
                if (_productList == null)
                    //_productList = new FlowObservableCollection<ImageQuixProduct>(this.ImageQuixProducts);
					_productList = new FlowObservableCollection<ImageQuixProduct>(this.ImageQuixProducts.Where(p=>p.IsRetired == false));

				return _productList;
			}
		}

		private ImageQuixProductGroup _parentProductGroup = null;
		public ImageQuixProductGroup ParentProductGroup
		{
			get { return _parentProductGroup; }
			private set
			{
				_parentProductGroup = value;
				this.ImageQuixProductGroupParentID = _parentProductGroup.ImageQuixProductGroupID;
			}
		}

		public ImageQuixProductGroup(XElement productGroupDef, IImageQuixProductParent parent, string CustomerID)
			: this()
		{
			if (parent is ImageQuixCatalog)
				this.ImageQuixCatalog = parent as ImageQuixCatalog;
			else
			{
				this.ParentProductGroup = parent as ImageQuixProductGroup;
				this.ImageQuixCatalog = this.ParentProductGroup.ImageQuixCatalog;
			}

            ImageQuixCatalogListing listing = ImageQuixUtility.ReadCatalogListing(productGroupDef);
            this.PrimaryKey = listing.PrimaryKey;
            this.Label = listing.Label;
            this.ResourceURL = listing.ResourceURL;
            //this.PrimaryKey = (int)productGroupDef.Attribute("pk");
            //this.Label = productGroupDef.Attribute("label").NormalizeXAttribute();
			//this.SortOrder = (int)productGroupDef.Attribute("sortorder");
			//this.SourceFk = (string)productGroupDef.Attribute("sourcefk");
			//this.StatusFk = (int)productGroupDef.Attribute("statusfk");

			this.ImageQuixCatalog.ProductGroupList.Add(this);

			foreach (XElement productDef in productGroupDef.Elements("Product"))
			{
				new ImageQuixProduct(productDef, this, CustomerID);
			}

			foreach (XElement productSubGroupDef in productGroupDef.Elements("ProductGroup"))
			{
				new ImageQuixProductGroup(productSubGroupDef, this, CustomerID);
			}		
		}
        public ImageQuixProductGroup(JsonArray productGroupDefs, IImageQuixProductParent parent)
            : this()
        {
            if (productGroupDefs == null || productGroupDefs.Count < 1)
                return;

            string uname = this.ImageQuixCatalog.FlowMasterDataContext.PreferenceManager.Upload.CatalogRequest.GetCatalogUsername;
            string pword = this.ImageQuixCatalog.FlowMasterDataContext.PreferenceManager.Upload.CatalogRequest.GetCatalogPassword;
            

            ////this.PrimaryKey = (int)productGroupDef.Attribute("pk");
            ////this.Label = productGroupDef.Attribute("label").NormalizeXAttribute();
            ////this.SortOrder = (int)productGroupDef.Attribute("sortorder");
            ////this.SourceFk = (string)productGroupDef.Attribute("sourcefk");
            ////this.StatusFk = (int)productGroupDef.Attribute("statusfk");

            //this.ImageQuixCatalog.ProductGroupList.Add(this);
            foreach (JsonObject productGroupDef in productGroupDefs)
            {
                if (parent is ImageQuixCatalog)
                    this.ImageQuixCatalog = parent as ImageQuixCatalog;
                else
                {
                    this.ParentProductGroup = parent as ImageQuixProductGroup;
                    this.ImageQuixCatalog = this.ParentProductGroup.ImageQuixCatalog;
                }

                ImageQuixCatalogListing listing = ImageQuixUtility.ReadJsonCatalogListing(productGroupDef);
                this.PrimaryKey = listing.PrimaryKey;
                this.Label = listing.Label;
                this.ResourceURL = listing.ResourceURL;

                JsonObject productGroups = (JsonObject)productGroupDef["productGroups"];
                JsonObject products = (JsonObject)productGroupDef["products"];

                foreach (JsonString collectionUrl in productGroups.Values)
                {
                    

                    JsonArray productGroupElemets = ImageQuixUtility.GetImageQuixProductGroupsFromJsonServer(collectionUrl.ToString(), uname, pword);
                    new ImageQuixProductGroup(productGroupElemets, parent);
                }

                //foreach (JsonString collectionUrl in products.Values)
                //{
                //    JsonArray productElemets = ImageQuixUtility.GetImageQuixProductGroupsFromJsonServer(collectionUrl.ToString());
                //    new ImageQuixProduct(productElemets, parent);
                //}
            }

            //foreach (XElement productDef in productGroupDef.Elements("Product"))
            //{
            //    new ImageQuixProduct(productDef, this);
            //}

            //foreach (XElement productSubGroupDef in productGroupDef.Elements("ProductGroup"))
            //{
            //    new ImageQuixProductGroup(productSubGroupDef, this);
            //}
        }

        public ImageQuixProductGroup(JsonObject productGroupDef, IImageQuixProductParent parent)
            : this()
        {
            if (parent is ImageQuixCatalog)
                this.ImageQuixCatalog = parent as ImageQuixCatalog;
            else
            {
                this.ParentProductGroup = parent as ImageQuixProductGroup;
                this.ImageQuixCatalog = this.ParentProductGroup.ImageQuixCatalog;
            }

            ImageQuixCatalogListing listing = ImageQuixUtility.ReadJsonCatalogListing(productGroupDef);
            this.PrimaryKey = listing.PrimaryKey;
            this.Label = listing.Label;
            this.ResourceURL = listing.ResourceURL;   
        }

        public void ProcessProductGroup(JsonObject productGroupDef, string username, string password, NotificationProgressInfo progressInfo, Dispatcher dispatcher, string CustomerID, FlowMasterDataContext flowmasterdatacontext)
        {
            if (progressInfo != null)
            {
                progressInfo.Progress++;
                progressInfo.Update();
            }

            int id = Convert.ToInt32(((JsonNumber)productGroupDef["id"]).Value);

            JsonArray productGroups = (JsonArray)productGroupDef["productGroups"];
            JsonArray products = (JsonArray)productGroupDef["products"];



            foreach (JsonObject productDef in productGroups)
            {
                if (progressInfo != null)
                {
                    progressInfo.Progress++;
                    progressInfo.Update();
                }

                //JsonArray productGroupElemets = ImageQuixUtility.GetImageQuixProductGroupsFromJsonServer(collectionUrl.ToString(), username, password);
                //if (productGroupElemets != null && productGroupElemets.Count > 0)
               // {
                //    foreach (JsonObject productDef in productGroupElemets)
                  //  {
                        //I dont think this code ever gets reached
                        //new ImageQuixProductGroup(productDef, this);
                this.ProcessProductGroup(productDef, username, password, progressInfo, dispatcher, CustomerID, flowmasterdatacontext);
                 //   }
               // }

            }

            foreach (JsonObject productDef in products)
            {
                //JsonArray productElemets = ImageQuixUtility.GetImageQuixProductFromJsonServer(collectionUrl.ToString(), username, password);
               // if (productElemets != null && productElemets.Count > 0)
               // {
                //    foreach (JsonObject productDef in productElemets)
                //    {
                        if (progressInfo != null)
                        {
                            progressInfo.Progress++;
                            progressInfo.Update();
                        }
                        //lets get the complete productDef from the resource url
                        string productUrl = Convert.ToString(((JsonString)productDef["resource"]).Value);
                        flowmasterdatacontext.alltempProdIDs.Add(productUrl);
                        //JsonObject productObject = ImageQuixUtility.GetImageQuixResourceFromJsonServer(productUrl, username, password);
                        JsonObject productObject = productDef;
                        ImageQuixProduct newProd = null; 
                        Boolean foundMatch = false;
                        foreach (ImageQuixProduct prod in this.ImageQuixProducts)
                        {
                            if (prod.PrimaryKey == Convert.ToInt32(((JsonNumber)productObject["id"]).Value))
                            {
                                prod.ImageQuixCatalog = this.ImageQuixCatalog;
                                prod.ImageQuixProductGroup = this;

                                prod.PrimaryKey = Convert.ToInt32(((JsonNumber)productObject["id"]).Value);
                                prod.Label = (((JsonString)productObject["name"]).Value);
                                prod.ResourceURL = (((JsonString)productObject["resource"]).Value);

                                prod.Width = Convert.ToDouble(((JsonNumber)productObject["width"]).Value);
                                prod.Height = Convert.ToDouble(((JsonNumber)productObject["height"]).Value);

                                prod.Map = ImageQuixProduct.ExtractMapValue(this.Label);
                                if (productObject.ContainsKey("retiredOn") && (productObject["retiredOn"].JsonTypeCode != JsonTypeCode.Null))
                                {
                                    prod.RetiredOn = DateTime.Parse((((JsonString)productObject["retiredOn"]).Value));
                                }
                                if (productObject.ContainsKey("internalLabID") && (productObject["internalLabID"].JsonTypeCode != JsonTypeCode.Null))
                                {
                                    //prod.SKU = (((JsonString)productObject["internal-lab-id"]).Value);
                                    string internalLabId = (((JsonString)productObject["internalLabID"]).Value);
                                    bool showProduct = true;
                                    if (internalLabId.Contains("["))
                                    {
                                        showProduct = false;
                                        var pattern = @"\[(.*?)\]";
                                        var matches = Regex.Matches(internalLabId, pattern);

                                        foreach (Match m in matches)
                                        {
                                            if (m.Groups[1].Value == CustomerID)
                                            {
                                                showProduct = true;
                                                internalLabId = internalLabId.Split("[")[0];
                                            }
                                        }
                                    }

                                    //prod.RetiredOn = null;
                                    if (!showProduct)
                                    {
                                        prod.RetiredOn = DateTime.Now;
                                        continue;
                                    }

                                    prod.SKU = internalLabId;
                                }
                                string priceString = (((JsonNumber)productObject["price"]).Value).ToString();

                                ImageQuixPricingTierList pricingTiers = ImageQuixPricingTier.GetPricingTierList(priceString);

                                if (pricingTiers != null)
                                {
                                    ImageQuixPricingTier baseTier = pricingTiers.FirstOrDefault(t => t.MinQuantity == 1);

                                    if (baseTier != null)
                                        prod.Price = baseTier.Price;

                                    prod.PricingTierList = new ObservableCollection<ImageQuixPricingTier>(pricingTiers);

                                    prod.PricingTiers = ImageQuixPricingTier.Serialize(pricingTiers);
                                }
                                newProd = prod;
                                foundMatch = true;
                                break;
                            }
                        }
                        if (!foundMatch)
                        {
                            newProd = new ImageQuixProduct(productObject, this, CustomerID);
                            dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                            {
                                if (newProd.Label != null)
                                {
                                    //this.ImageQuixProducts.Add(newProd);
                                    //_productList = null;
                                    //this.ProductList.Add(newProd);
                                }
                            }));
                            
                        }
                        this.ImageQuixCatalog.FlowMasterDataContext.SubmitChanges();
                        if (newProd.Label != null)
                        {
                            newProd.processNodes(productObject, dispatcher);
                            newProd.processOptionGroups(productObject, dispatcher, CustomerID);
                        }
                       
                   // }
               // }
            }

        }





        public void RefreshProductList()
        {
            _productList = null;
            SendPropertyChanged("ProductList");
        }
    }



}
