﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

using Flow.Lib.Helpers;
using NetServ.Net.Json;

namespace Flow.Schema.LinqModel.DataContext
{
	partial class ImageQuixOptionCommand
	{
		internal int NodePrimaryKey { get; private set; }

		public ImageQuixOptionCommand(XElement optionCommandDef, ImageQuixOption parentOption)
			: this()
		{
			this.JobText = optionCommandDef.Attribute("jobtext").NormalizeXAttribute(128);
			this.PrimaryKey = (int)optionCommandDef.Attribute("pk");

			this.NodePrimaryKey = (int)optionCommandDef.Attribute("nodefk");

			string bounds = (string)optionCommandDef.Attribute("bounds");

			if (bounds == "")
				this.HasBounds = false;
			else
			{
				decimal[] boundsValues = bounds.Split(',').Select(v => Convert.ToDecimal(v)).ToArray();

				this.X = boundsValues[0];
				this.Y = boundsValues[1];
				this.Width = boundsValues[2];
				this.Height = boundsValues[3];
			}
		}

        public ImageQuixOptionCommand(JsonObject optionCommandDef, ImageQuixOption parentOption)
            : this()
        {
            //this.PrimaryKey = Convert.ToInt32(((JsonNumber)optionDef["id"]).Value);
            //this.Label = (((JsonString)optionDef["name"]).Value);

            //this.JobText = optionCommandDef.Attribute("jobtext").NormalizeXAttribute(128);
            //this.PrimaryKey = (int)optionCommandDef.Attribute("pk");

            //this.NodePrimaryKey = (int)optionCommandDef.Attribute("nodefk");

            //string bounds = (string)optionCommandDef.Attribute("bounds");

            //if (bounds == "")
            //    this.HasBounds = false;
            //else
            //{
            //    decimal[] boundsValues = bounds.Split(',').Select(v => Convert.ToDecimal(v)).ToArray();

            //    this.X = boundsValues[0];
            //    this.Y = boundsValues[1];
            //    this.Width = boundsValues[2];
            //    this.Height = boundsValues[3];
            //}
        }

	}
}
