﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

using Flow.Lib.Helpers;
using Flow.Lib.Helpers.ImageQuix;
using NetServ.Net.Json;
using Flow.Lib;
using System.Windows.Threading;

namespace Flow.Schema.LinqModel.DataContext
{
	partial class ImageQuixProduct
	{

        public string catalogItemRef { get; set; }
        public int tempItemID { get; set; }

        public bool IsRetired
        {
            get
            {
                if (this.RetiredOn != null && this.RetiredOn < DateTime.Now)
                    return true;
                return false;
            }

        }
		private FlowObservableCollection<ImageQuixProductNode> _productNodeList = null;
		public FlowObservableCollection<ImageQuixProductNode> ProductNodeList
		{
			get
			{
				if (_productNodeList == null)
					_productNodeList = new FlowObservableCollection<ImageQuixProductNode>(this.ImageQuixProductNodes);

				return _productNodeList;
			}
		}

		private FlowObservableCollection<ImageQuixOptionGroup> _optionGroupList = null;
		public FlowObservableCollection<ImageQuixOptionGroup> OptionGroupList
		{
			get
			{
                if (_optionGroupList == null)
                {
                    try
                    {
                        _optionGroupList = new FlowObservableCollection<ImageQuixOptionGroup>(this.ImageQuixOptionGroups);
                    }
                    catch (Exception ex)
                    {
                        if (ex.GetType() == typeof(System.AccessViolationException))
                        {
                            //do nothing for now
                        }
                    }
                }

				return _optionGroupList;
			}
		}

		private ObservableCollection<ImageQuixPricingTier> _pricingTierList = null;
		public ObservableCollection<ImageQuixPricingTier> PricingTierList
		{
			get
			{
				if (_pricingTierList == null)
				{
					if (this.PricingTiers != null)
						_pricingTierList = new ObservableCollection<ImageQuixPricingTier>(
							ImageQuixPricingTier.Deserialize(this.PricingTiers.ToArray())
						);
				}

				return _pricingTierList;
			}

			set { _pricingTierList = value; }
		}


		public ImageQuixProduct(XElement productDef, ImageQuixProductGroup parentGroup, string CustomerID)
			: this()
		{
			this.ImageQuixCatalog = parentGroup.ImageQuixCatalog;
			this.ImageQuixProductGroup = parentGroup;

            ImageQuixCatalogListing listing = ImageQuixUtility.ReadCatalogListing(productDef);
            this.PrimaryKey = listing.PrimaryKey;
            this.Label = listing.Label;
            this.ResourceURL = listing.ResourceURL;
            this.SKU = listing.SKU;
            //this.PrimaryKey = (int)productDef.Attribute("pk");
            //this.Label = productDef.Attribute("label").NormalizeXAttribute(128);
			this.Width = (double?)productDef.Attribute("w");
			this.Height = (double?)productDef.Attribute("h");
			//this.Map = ImageQuixProduct.ExtractMapValue(this.Label);
            //this.Map = listing.
            //if (productDef.Attribute("retiredOn") != null)
            //    this.RetiredOn = DateTime.Parse((string)productDef.Attribute("retiredOn"));
          


			string priceString = (string)productDef.Attribute("price");

			ImageQuixPricingTierList pricingTiers = ImageQuixPricingTier.GetPricingTierList(priceString);

			if (pricingTiers != null)
			{
				ImageQuixPricingTier baseTier = pricingTiers.FirstOrDefault(t => t.MinQuantity == 1);

				if (baseTier != null)
					this.Price = baseTier.Price;

				this.PricingTierList = new ObservableCollection<ImageQuixPricingTier>(pricingTiers);

				this.PricingTiers = ImageQuixPricingTier.Serialize(pricingTiers);
			}

			this.PriceType = (string)productDef.Attribute("pricetype");

			parentGroup.ProductList.Add(this);

			foreach (XElement productNodeDef in productDef.Elements("Node"))
			{
				this.ProductNodeList.Add(new ImageQuixProductNode(productNodeDef, this));
			}

			foreach (XElement optionGroupDef in productDef.Elements("OptionGroup"))
			{
				new ImageQuixOptionGroup(optionGroupDef, this);
			}

		}


        public ImageQuixProduct(JsonObject productDef, ImageQuixProductGroup parentGroup, string CustomerID)
            : this()
        {

            if (productDef == null)
                return;

            if (productDef.ContainsKey("internalLabID") && (productDef["internalLabID"].JsonTypeCode != JsonTypeCode.Null))
            {
                string internalLabId = (((JsonString)productDef["internalLabID"]).Value);
                bool showProduct = true;
                if (internalLabId.Contains("["))
                {
                    showProduct = false;
                    var pattern = @"\[(.*?)\]";
                    var matches = Regex.Matches(internalLabId, pattern);

                    foreach (Match m in matches)
                    {
                        if (m.Groups[1].Value == CustomerID)
                        {
                            showProduct = true;
                            internalLabId = internalLabId.Split("[")[0];
                        }
                    }
                }

                if (!showProduct)
                {
                    return;
                }

                this.SKU = internalLabId;
                
            }
            this.ImageQuixCatalog = parentGroup.ImageQuixCatalog;
            this.ImageQuixProductGroup = parentGroup;

            this.PrimaryKey = Convert.ToInt32(((JsonNumber)productDef["id"]).Value);
            this.Label = (((JsonString)productDef["name"]).Value);
            this.ResourceURL = (((JsonString)productDef["resource"]).Value);

            this.Width = Convert.ToDouble(((JsonNumber)productDef["width"]).Value);
            this.Height = Convert.ToDouble(((JsonNumber)productDef["height"]).Value);

            this.Map = ImageQuixProduct.ExtractMapValue(this.Label);
            if (productDef.ContainsKey("retiredOn") && (productDef["retiredOn"].JsonTypeCode != JsonTypeCode.Null))
            {
                this.RetiredOn = DateTime.Parse((((JsonString)productDef["retiredOn"]).Value));
            }

            string priceString = (((JsonNumber)productDef["price"]).Value).ToString();

            ImageQuixPricingTierList pricingTiers = ImageQuixPricingTier.GetPricingTierList(priceString);

            if (pricingTiers != null)
            {
                ImageQuixPricingTier baseTier = pricingTiers.FirstOrDefault(t => t.MinQuantity == 1);

                if (baseTier != null)
                    this.Price = baseTier.Price;

                this.PricingTierList = new ObservableCollection<ImageQuixPricingTier>(pricingTiers);

                this.PricingTiers = ImageQuixPricingTier.Serialize(pricingTiers);
            }

            //this.PriceType = (((JsonString)productDef["pricetype"]).Value);

        }
        public void processNodes(JsonObject productDef, Dispatcher dispatcher)
        {

            JsonArray nodes = (JsonArray)productDef["nodes"];
            foreach (JsonObject node in nodes)
            {

                ImageQuixProductNode newNode = null;
                Boolean foundMatch = false;
                foreach (ImageQuixProductNode thisNode in this.ProductNodeList)
                {
                    if (thisNode.PrimaryKey == Convert.ToInt32(((JsonNumber)node["id"]).Value))
                    {
                        foundMatch = true;

                        thisNode.ImageQuixProduct = this;

                        thisNode.PrimaryKey = Convert.ToInt32(((JsonNumber)node["id"]).Value);
                        thisNode.Label = (((JsonString)node["name"]).Value);
                        thisNode.ResourceURL = (((JsonString)node["resource"]).Value);

                        thisNode.Width = Convert.ToDouble(((JsonNumber)node["width"]).Value);
                        thisNode.Height = Convert.ToDouble(((JsonNumber)node["height"]).Value);
                        thisNode.X = Convert.ToDouble(((JsonNumber)node["x"]).Value);
                        thisNode.Y = Convert.ToDouble(((JsonNumber)node["y"]).Value);

                        string nodeType = (((JsonString)node["type"]).Value);
                        switch (nodeType)
                        {
                            case "image":
                                thisNode.Type = 1;
                                break;
                            case "text":
                                thisNode.Type = 2;
                                break;
                            case "hidden":
                                thisNode.Type = 3;
                                break;
                            default:
                                thisNode.Type = 0;
                                break;
                        }
                        newNode = thisNode;
                        break;
                    }
                }
                if (!foundMatch)
                {
                    newNode = new ImageQuixProductNode(node, this);
                    dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                    {
                        this.ProductNodeList.Add(newNode);
                    }));
                    
                }

                //this.ImageQuixCatalog.FlowMasterDataContext.SubmitChanges();
            }

        }
        public void processOptionGroups(JsonObject productDef, Dispatcher dispatcher, string CustomerID)
        {
            JsonArray optionGroups = (JsonArray)productDef["optionGroups"];
            foreach (JsonObject optionGroup in optionGroups)
            {
                if (optionGroup.ContainsKey("internalLabID") && (optionGroup["internalLabID"].JsonTypeCode != JsonTypeCode.Null))
                {
                    //prod.SKU = (((JsonString)productObject["internal-lab-id"]).Value);
                    string internalLabId = (((JsonString)optionGroup["internalLabID"]).Value);
                    bool showProduct = true;
                    if (internalLabId.Contains("["))
                    {
                        showProduct = false;
                        var pattern = @"\[(.*?)\]";
                        var matches = Regex.Matches(internalLabId, pattern);

                        foreach (Match m in matches)
                        {
                            if (m.Groups[1].Value == CustomerID)
                            {
                                showProduct = true;
                                internalLabId = internalLabId.Split("[")[0];
                            }
                        }
                    }

                    if (!showProduct)
                    {
                        continue;
                    }

                }

                string defaultOptionRef = null;
                if ((optionGroup["defaultOptionRef"]) != JsonNull.Null)
                    defaultOptionRef = ((JsonString)optionGroup["defaultOptionRef"]).Value;
                else
                    defaultOptionRef = null;

                ImageQuixOptionGroup newOG = null; 
                Boolean foundMatch = false;
                foreach (ImageQuixOptionGroup og in this.OptionGroupList)
                {
                    if (og.PrimaryKey == Convert.ToInt32(((JsonNumber)optionGroup["id"]).Value))
                    {
                        foundMatch = true;

                        og.Parent = this;

                        og.PrimaryKey = Convert.ToInt32(((JsonNumber)optionGroup["id"]).Value);
                        og.Label = (((JsonString)optionGroup["name"]).Value);
                        og.ResourceURL = (((JsonString)optionGroup["resource"]).Value);
                        //og.IsMandatory = (((JsonBoolean)optionGroup["isMandatory"]).Value);
                        if ((((JsonString)optionGroup["requiredness"]).Value == "required"))
                            og.IsMandatory = true;
                        else
                            og.IsMandatory = false;
                        newOG = og;
                    }

                }
                if (!foundMatch)
                {
                     JsonArray options = (JsonArray)optionGroup["options"];
                     JsonObject option = options.FirstOrDefault() as JsonObject;


                     if (option != null && option.ContainsKey("internalLabID") && (option["internalLabID"].JsonTypeCode != JsonTypeCode.Null))
                        {
                            //prod.SKU = (((JsonString)productObject["internal-lab-id"]).Value);
                            string internalLabId = (((JsonString)option["internalLabID"]).Value);
                            bool showProduct = true;
                            if (internalLabId.Contains("["))
                            {
                                showProduct = false;
                                var pattern = @"\[(.*?)\]";
                                var matches = Regex.Matches(internalLabId, pattern);

                                foreach (Match m in matches)
                                {
                                    if (m.Groups[1].Value == CustomerID)
                                    {
                                        showProduct = true;
                                        internalLabId = internalLabId.Split("[")[0];
                                    }
                                }
                            }

                            if (!showProduct)
                            {
                                continue;
                            }

                        }
                     
                    newOG = new ImageQuixOptionGroup(optionGroup, this);
                    dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                    {
                        this.OptionGroupList.Add(newOG);
                    }));
                   
                }
                newOG.processOptions(optionGroup, defaultOptionRef);
                //this.ImageQuixCatalog.FlowMasterDataContext.SubmitChanges();

            }
        }
 

		public static string ExtractMapValue(string value)
		{
			if(Regex.IsMatch(value, @"^\w\w-"))
				return value.Substring(0,2);

			return null;
		}

	}
}
