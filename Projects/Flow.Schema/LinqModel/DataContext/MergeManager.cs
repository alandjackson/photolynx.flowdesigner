﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Threading;

using Flow.Lib;
using Flow.Lib.Helpers;

using ICSharpCode.SharpZipLib.Zip;
using NLog;
using Flow.Lib.AsyncWorker;
using System.Threading;
using System.Windows;

namespace Flow.Schema.LinqModel.DataContext
{
	public class MergeManager
	{
		public event EventHandler MergeCompleted = delegate { };

        public event EventHandler MergeProjectReady = delegate { };

		public string ArchiveFilePath { get; set; } // For PAF file merges
        public string SourceDirectory { get; set; } // For uncompressed folder merges
		public FlowProject RootProject { get; set; }
		public FlowMasterDataContext FlowMasterDataContext { get; set; }

		private string MergeTempDirPath { get; set; }
		private string MergeTempImageDirPath { get; set; }
		private string ProjectGuidString { get; set; }
		private FlowProject MergeProject { get; set; }

        private bool forceImageMerge = false;


        private static Logger logger = LogManager.GetCurrentClassLogger();

		public NotificationProgressInfo ProgressInfo { get; set; }

		public void Merge()
		{
            
			this.ProgressInfo.Display(true);
            DbVersionManager.SynchFlowProject(RootProject);
			// Extract the archive to a temp directory
			this.ProjectGuidString = this.RootProject.FlowProjectGuid.ToString();
			this.MergeTempDirPath = FlowContext.MakeTempSubDirectory(this.ProjectGuidString);
            this.MergeProjectReady += new EventHandler(Merge_ImportProjectReady);

            if (this.ArchiveFilePath != null) // We're merging a PAF or SDF file
            {
                if (this.ArchiveFilePath.EndsWith(".sdf")) // We're merging an SDF
                {
                    
                    logger.Info("Merging SDF file: fileName='{0}'", this.ArchiveFilePath);

                    if (!Directory.Exists(this.MergeTempDirPath))
                        Directory.CreateDirectory(this.MergeTempDirPath);
                    string newSdfFile = Path.Combine(this.MergeTempDirPath, this.ProjectGuidString + ".sdf");
                    if (File.Exists(newSdfFile))
                        File.Delete(newSdfFile);

                    File.Copy(this.ArchiveFilePath, newSdfFile);
                    forceImageMerge = false;
                    this.MergeProjectReady(null, null);
                }
                else // We're merging a PAF
                {
                    logger.Info("Unpacking PAF file for merge: fileName='{0}'", this.ArchiveFilePath);

                    forceImageMerge = true;

                    this.ProgressInfo.Update("Unpacking project file.... (This could take a few minutes)");

                    bool unpacking = true;

                    FlowBackgroundWorkerManager.RunWorker(
                        delegate
                        {
                            ZipUtility.ExtractArchive(this.ArchiveFilePath, this.MergeTempDirPath, FastZip.Overwrite.Always, null);
                        },
                        delegate
                        {
                            unpacking = false;
                        });



                    



                    string dots = ".";
                    FlowBackgroundWorkerManager.RunWorker(
                      delegate
                      {
                          while (unpacking)
                          {

                              if (dots == ".............")
                                  dots = ".";
                              RootProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                              {
                                  this.ProgressInfo.Update("Unpacking project file (This could take a few minutes) " + dots);
                              }));
                              dots += ".";
                              Thread.Sleep(1000);
                          }
                      },
                      delegate
                      {
                          this.ProgressInfo.Update("Done unpacking project file");
                          this.MergeProjectReady(null, null);
                      });
                }
            }
            else // We're merging an uncompressed project folder
            {
                logger.Info("Merging an uncompressed project folder: sourceDirectory='{0}'", this.SourceDirectory);

                forceImageMerge = true;

                this.ProgressInfo.Update("Copying project files.... (This could take a few minutes)");

                bool copying = true;

                FlowBackgroundWorkerManager.RunWorker(
                    delegate
                    {
                        IOUtil.CopyDirectoryRecursive(this.SourceDirectory, this.MergeTempDirPath);
                    },
                    delegate
                    {
                        copying = false;
                    });


                string dots = ".";
                FlowBackgroundWorkerManager.RunWorker(
                  delegate
                  {
                      while (copying)
                      {

                          if (dots == ".............")
                              dots = ".";
                          RootProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                          {
                              this.ProgressInfo.Update("Copying project files (This could take a few minutes) " + dots);
                          }));
                          dots += ".";
                          Thread.Sleep(1000);
                      }
                  },
                  delegate
                  {
                      this.ProgressInfo.Update("Done copying project files");
                      this.MergeProjectReady(null, null);
                  });
            }
		}

        private void Merge_ImportProjectReady(object sender, EventArgs e)
        {
            this.MergeTempImageDirPath = FlowContext.CombineDir(this.MergeTempDirPath, "Image");


            if (1 == 2 && this.RootProject.Initialized)
            {
                this.Merge_RootProjectLoaded(null, null);
            }
            else
            {
                // Connect to the root project

                this.RootProject.Connect(this.FlowMasterDataContext);
                DbVersionManager.SynchFlowProject(this.RootProject);
                //this.RootProject.FlowProjectDataContext.addMoreColumnsToSubjectFields();
                //this.RootProject.FlowProjectDataContext.SubjectDataLoaded += new EventHandler(Merge_RootProjectLoaded);
                this.RootProject.FlowProjectDataContext.InitSubjectList();
                this.RootProject.FlowProjectDataContext.SubmitChanges();



                //lets handle layouts and graphics in this folder
                string projLayouts = Path.Combine(this.MergeTempDirPath, "Layouts");
                string projGraphics = Path.Combine(this.MergeTempDirPath, "Graphics");
                string defaultLayout = "";
                if (Directory.Exists(projLayouts))
                {
                    foreach (string file in Directory.GetFiles(projLayouts))
                    {
                        //if (string.IsNullOrEmpty(this.ProjectController.CustomSettings.DefaultCaptureLayout)) ;
                        //{
                        //    this.FlowController.ProjectController.CustomSettings.DefaultCaptureLayout = file;
                        //}
                        string destFile = Path.Combine(FlowContext.FlowLayoutsDirPath, new FileInfo(file).Name);
                        if (!File.Exists(destFile))
                            File.Copy(file, destFile);
                    }
                }

                if (Directory.Exists(projGraphics))
                {
                    foreach (string file in Directory.GetFiles(projGraphics))
                    {
                        string destFile = Path.Combine(FlowContext.FlowGraphicsDirPath, new FileInfo(file).Name);
                        if (!File.Exists(destFile))
                            File.Copy(file, destFile);
                    }
                }


                this.Merge_RootProjectLoaded(null, null);
            }
        }

		private void Merge_RootProjectLoaded(object sender, EventArgs e)
		{
			string mergeProjectDatabasePath = FlowContext.Combine(this.MergeTempDirPath, this.ProjectGuidString + ".sdf");

			this.MergeProject =
				new FlowProject(mergeProjectDatabasePath, this.FlowMasterDataContext);
            DbVersionManager.SynchFlowProject(this.MergeProject);
            this.MergeProject.FlowProjectDataContext.addMoreColumnsToSubjectFields();
			this.MergeProject.FlowProjectDataContext.SubjectDataLoaded += new EventHandler(Merge_MergeProjectLoaded);
            this.MergeProject.FlowProjectDataContext.InitSubjectList();
            this.MergeProject.FlowProjectDataContext.SubmitChanges();

            logger.Info("Merge root project loaded");
		}

		private void Merge_MergeProjectLoaded(object sender, EventArgs e)
		{
            this.ProgressInfo.Update("Processing subject {0} of {1}", 0, this.MergeProject.SubjectListMerge.Count, 1, true);

			FlowBackgroundWorkerManager.RunWorker(
				Merge_MergeSubjects,
				Merge_Complete
			);
		}

		private void Merge_MergeSubjects(object sender, DoWorkEventArgs e)
		{
             try
             {
             
                 try{

                    RootProject.MainDispatcher.BeginInvoke(DispatcherPriority.Normal, (Action)(() =>
                        {
                            logger.Debug("Apply Control Tables");
                            this.FlowMasterDataContext.ApplyControlTables(MergeProject.LocalDBPath, false, RootProject);
                            logger.Debug("Done Apply Control Tables");
                        }));

                 }
                 catch (Exception ex)
                 {
                     logger.ErrorException("Error in Merge Flow Catalog", ex);
                     throw ex;
                 }

                 //try
                 //{
                 //     User photographer = null;
                 //     string photographerLastName = "";
                 //     string photographerFirstName = "";
                 //     if (MergeProject.OwnerUserID != null && MergeProject.FlowProjectDataContext.Control_Users.Any(p => p.UserID == MergeProject.OwnerUserID))
                 //     {
                 //         photographerLastName = MergeProject.FlowProjectDataContext.Control_Users.FirstOrDefault(p => p.UserID == MergeProject.OwnerUserID).UserLastName;
                 //         photographerFirstName = MergeProject.FlowProjectDataContext.Control_Users.FirstOrDefault(p => p.UserID == MergeProject.OwnerUserID).UserFirstName;
                 //     }


                 //     if (FlowMasterDataContext.Users.Any(p => p.UserFirstName == photographerFirstName && p.UserLastName == photographerLastName))
                 //    {
                 //       photographer = FlowMasterDataContext.Users.FirstOrDefault(p => p.UserFirstName == photographerFirstName && p.UserLastName == photographerLastName);
                 //    }

                 //    if ((MergeProject.Photographer.FormalFullName != RootProject.Photographer.FormalFullName) && photographer != null)
                 //    {
                 //        RootProject.OwnerUserID = photographer.UserID;
                 //    }
                 //    else if ((MergeProject.Photographer.FormalFullName != RootProject.Photographer.FormalFullName) && photographer == null)
                 //    {
                 //        photographer = MergeProject.Photographer.clone();
                 //        RootProject.FlowMasterDataContext.Users.InsertOnSubmit(photographer);
                 //        RootProject.FlowMasterDataContext.SubmitChanges();
                 //        RootProject.OwnerUserID = photographer.UserID;
                 //    }

                 //}
                 //catch (Exception ex)
                 //{
                 //    logger.ErrorException("Error in Merge Photographer", ex);
                 //    throw ex;
                 //}


            try
            {

                //lets first merge project settings
                RootProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                {
                    logger.Info("merge settings");
                        Control_FlowProject merge_proj = MergeProject.FlowProjectDataContext.Control_FlowProjects.FirstOrDefault();
                        Control_FlowCatalog merge_cat = MergeProject.FlowProjectDataContext.Control_FlowCatalogs.FirstOrDefault();
                        Control_Organization merge_org = MergeProject.FlowProjectDataContext.Control_Organizations.FirstOrDefault();

                        FlowProject root_proj = FlowMasterDataContext.FlowProjectList.FirstOrDefault(p => p.FlowProjectGuid == merge_proj.FlowProjectGuid);
               
                        if(merge_proj != null)
                        {
                            logger.Info("merge settings - proj gs");
                            if (merge_proj.IsGreenScreen != root_proj.IsGreenScreen)
                                root_proj.IsGreenScreen = merge_proj.IsGreenScreen;
                            if (merge_proj.DefaultGreenScreenBackground != root_proj.DefaultGreenScreenBackground)
                                root_proj.DefaultGreenScreenBackground = merge_proj.DefaultGreenScreenBackground;

                            logger.Info("merge settings - proj name and stuff");
                            if (merge_proj.FlowProjectName != root_proj.FlowProjectName)
                                root_proj.FlowProjectName = merge_proj.FlowProjectName;
                            if (!string.IsNullOrEmpty(merge_proj.FlowProjectDesc) && merge_proj.FlowProjectDesc != root_proj.FlowProjectDesc)
                                root_proj.FlowProjectDesc = merge_proj.FlowProjectDesc;
                            if (merge_proj.DateCreated != root_proj.DateCreated)
                                root_proj.DateCreated = merge_proj.DateCreated;
                   
                        }

                        logger.Info("merge settings - Cat");
                        if (merge_cat.FlowCatalogGuid != null && root_proj.FlowCatalog == null)
                            root_proj.FlowCatalog = this.FlowMasterDataContext.FlowCatalogList.FirstOrDefault(fc => fc.FlowCatalogGuid == merge_cat.FlowCatalogGuid);
                        else if (merge_cat.FlowCatalogGuid != null && root_proj.FlowCatalog != null && merge_cat.FlowCatalogGuid != root_proj.FlowCatalog.FlowCatalogGuid)
                            root_proj.FlowCatalog = this.FlowMasterDataContext.FlowCatalogList.FirstOrDefault(fc => fc.FlowCatalogGuid == merge_cat.FlowCatalogGuid);

                        logger.Info("merge settings - Org");
                        if (merge_org.OrganizationGuid == root_proj.Organization.OrganizationGuid)
                        {
                            Organization root_org = FlowMasterDataContext.Organizations.FirstOrDefault(or => or.OrganizationGuid == merge_org.OrganizationGuid);
                            if (root_org != null)
                            {
                                if (string.IsNullOrEmpty(merge_org.OrganizationAddressLine1))
                                    root_org.OrganizationAddressLine1 = merge_org.OrganizationAddressLine1;
                                if (string.IsNullOrEmpty(merge_org.OrganizationAddressLine2))
                                    root_org.OrganizationAddressLine2 = merge_org.OrganizationAddressLine2;
                                if (string.IsNullOrEmpty(merge_org.OrganizationCity))
                                    root_org.OrganizationCity = merge_org.OrganizationCity;
                                if (string.IsNullOrEmpty(merge_org.OrganizationStateCode))
                                    root_org.OrganizationStateCode = merge_org.OrganizationStateCode;
                                if (string.IsNullOrEmpty(merge_org.OrganizationZipCode))
                                    root_org.OrganizationZipCode = merge_org.OrganizationZipCode;

                                if (string.IsNullOrEmpty(merge_org.OrganizationShippingAddressLine1))
                                    root_org.OrganizationShippingAddressLine1 = merge_org.OrganizationShippingAddressLine1;
                                if (string.IsNullOrEmpty(merge_org.OrganizationShippingAddressLine2))
                                    root_org.OrganizationShippingAddressLine2 = merge_org.OrganizationShippingAddressLine2;
                                if (string.IsNullOrEmpty(merge_org.OrganizationShippingCity))
                                    root_org.OrganizationShippingCity = merge_org.OrganizationShippingCity;
                                if (string.IsNullOrEmpty(merge_org.OrganizationShippingStateCode))
                                    root_org.OrganizationShippingStateCode = merge_org.OrganizationShippingStateCode;
                                if (string.IsNullOrEmpty(merge_org.OrganizationShippingZipCode))
                                    root_org.OrganizationShippingZipCode = merge_org.OrganizationShippingZipCode;


                                if (string.IsNullOrEmpty(merge_org.OrganizationContactEmail))
                                    root_org.OrganizationContactEmail = merge_org.OrganizationContactEmail;
                                if (string.IsNullOrEmpty(merge_org.OrganizationContactName))
                                    root_org.OrganizationContactName = merge_org.OrganizationContactName;
                                if (string.IsNullOrEmpty(merge_org.OrganizationContactPhone))
                                    root_org.OrganizationContactPhone = merge_org.OrganizationContactPhone;
                            }
                        }


                        //lets first merge the gallery
                        if (MergeProject.FlowProjectDataContext.OnlineGalleries.Any())
                        {
                            logger.Debug("Merging Online Galleries");

                            OnlineGallery incomingGallery = MergeProject.FlowProjectDataContext.OnlineGalleries.First();
                            OnlineGallery rootGallery = null;
                            if (RootProject.FlowProjectDataContext.ProjectGalleries.Any())
                            {
                                rootGallery = RootProject.FlowProjectDataContext.OnlineGalleries.First();
                            }

                            if (rootGallery == null)
                                RootProject.FlowProjectDataContext.OnlineGalleries.InsertOnSubmit(incomingGallery);
                            else if (incomingGallery.GalleryCode != null && rootGallery.GalleryCode == null)
                            {
                                RootProject.FlowProjectDataContext.OnlineGalleries.DeleteOnSubmit(rootGallery);
                                RootProject.FlowProjectDataContext.OnlineGalleries.InsertOnSubmit(incomingGallery);
                            }
                            else if (incomingGallery.GalleryCode != null)
                            {
                                if ((rootGallery.LastModifyDate == null && incomingGallery.LastModifyDate != null) || ((rootGallery.LastModifyDate != null && incomingGallery.LastModifyDate != null) && (incomingGallery.LastModifyDate > rootGallery.LastModifyDate)))
                                {
                                    RootProject.FlowProjectDataContext.OnlineGalleries.DeleteOnSubmit(rootGallery);
                                    RootProject.FlowProjectDataContext.OnlineGalleries.InsertOnSubmit(incomingGallery);
                                }
                            }
                        }
                        if (MergeProject.FlowProjectDataContext.ProjectGalleries.Any())
                        {
                            logger.Debug("Merging IQ Galleries");

                            ProjectGallery incomingGallery = MergeProject.FlowProjectDataContext.ProjectGalleries.First();
                            ProjectGallery rootGallery = null;
                            if (RootProject.FlowProjectDataContext.ProjectGalleries.Any())
                            {
                                rootGallery = RootProject.FlowProjectDataContext.ProjectGalleries.First();
                            }

                            if (rootGallery == null)
                                RootProject.FlowProjectDataContext.ProjectGalleries.InsertOnSubmit(incomingGallery);
                            else if (incomingGallery.GalleryCode != null && rootGallery.GalleryCode == null)
                            {
                                RootProject.FlowProjectDataContext.ProjectGalleries.DeleteOnSubmit(rootGallery);
                                RootProject.FlowProjectDataContext.ProjectGalleries.InsertOnSubmit(incomingGallery);
                            }
                            else if (incomingGallery.GalleryCode != null)
                            {
                                if ((rootGallery.LastModifyDate == null && incomingGallery.LastModifyDate != null) || ((rootGallery.LastModifyDate != null && incomingGallery.LastModifyDate != null) && (incomingGallery.LastModifyDate > rootGallery.LastModifyDate)))
                                {
                                    RootProject.FlowProjectDataContext.ProjectGalleries.DeleteOnSubmit(rootGallery);
                                    RootProject.FlowProjectDataContext.ProjectGalleries.InsertOnSubmit(incomingGallery);
                                }
                            }

                        }
                    }));
                    }
                catch (Exception ex)
                {
                    logger.ErrorException("Error in Merge Galleries", ex);
                    throw ex;
                }
             

           

            // Copy all image files to the root project directory
            Dictionary<String, String> renamedFiles = IOUtil.CopyFiles(this.MergeTempImageDirPath, this.RootProject.ImageDirPath, true, "_A");


            try
            {
                
                if (renamedFiles.Count() > 0)
                {
                    logger.Debug("Merge: Copy all new files to Images dir");
                    foreach (GroupImage img in this.MergeProject.FlowProjectDataContext.GroupImageList)
                    {
                        if (renamedFiles.ContainsKey(img.ImageFileName))
                        {
                            img.ImageFileName = renamedFiles[img.ImageFileName];
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.ErrorException("Error in Renaming files", ex);
                throw ex;
            }
            logger.Debug("Merging Group Images");
            try
            {
                foreach (GroupImage img in this.MergeProject.FlowProjectDataContext.GroupImageList)
                {
                    this.RootProject.MergeGroupImages(img, this.MergeTempDirPath);
                }
            }
            catch (Exception ex)
            {
                logger.ErrorException("Error in Merge Group Images", ex);
                throw ex;
            }

            logger.Debug("Merging Subjects");
            try
            {
                try
                {
                    logger.Debug("Merging Subjects - Init Subject List");
                    this.RootProject.FlowProjectDataContext.InitSubjectList();
                    logger.Debug("Merging Subjects - Done with Init Subject List");
                }
                catch (Exception ex)
                {
                    logger.Error("Merge in init subject list\n" + ex.Message);
                    throw ex;
                }

                foreach (Subject subject in this.MergeProject.SubjectListMerge)
                {
                    int tryCount = 0;
                    bool keepTrying = true;
                    while (keepTrying)
                    {
                        try
                        {
                            tryCount++;
                            logger.Debug("Merging Subjects - " + subject.FormalFullName + " - " + subject.TicketCode);
                            if (renamedFiles.Count() > 0)
                            {
                                logger.Debug("Merging Subjects - Dealing with renamed files");
                                foreach (SubjectImage img in subject.ImageList)
                                {
                                    if (renamedFiles.ContainsKey(img.ImageFileName))
                                    {
                                        img.ImageFileName = renamedFiles[img.ImageFileName];
                                    }
                                }
                                logger.Debug("Merging Subjects - Done with renamed files");
                            }

                            logger.Debug("Merging Subjects - The Beef");
                            //Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                            //{
                            this.RootProject.MergeSubject(subject, this.MergeTempDirPath, forceImageMerge);
                            //}));
                            logger.Debug("Merging Subjects - Done with The Beef");
                            this.ProgressInfo++;
                            keepTrying = false;//we did it, we can exit loop
                        }
                        catch(Exception exc)
                        {
                            //ignore any errors and try again
                            
                            logger.Info("IGNORING ERROR, TRYING AGAIN: " + exc.Message);
                            Thread.Sleep(500);
                            if(tryCount > 5)
                            {
                                logger.Error("BAD ERROR, Could not merge subject");
                                throw exc;
                                keepTrying = false;
                            }
                            
                        }
                    }
                        
                    
                }
            }
            catch (Exception ex)
            {
                logger.ErrorException("Error in Merge Subjects", ex);
                throw ex;
            }




            try
            {
                //get rid of OrderPackages that need deleted
                if (this.MergeProject.FlowProjectDataContext.DeletedOrderPackages.Count() > 0)
                {
                    logger.Debug("Merge: Sync Deleted Packages");
                    List<DeletedOrderPackage> dops = this.MergeProject.FlowProjectDataContext.DeletedOrderPackages.GroupBy(p => p.OrderPackageID).Select(g => g.First()).ToList();
                    foreach (DeletedOrderPackage dop in dops)
                    {
                        if (this.RootProject.FlowProjectDataContext.OrderPackages.Any(op => op.OrderPackageID == dop.OrderPackageID && op.AddDate == dop.AddDate))
                        {
                            if (RootProject.MainDispatcher != null)
                            {
                                //RootProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                               // {
                                    OrderPackage deleteOrderPackage = this.RootProject.FlowProjectDataContext.OrderPackages.First(op => op.OrderPackageID == dop.OrderPackageID && op.AddDate == dop.AddDate);
                                    deleteOrderPackage.SubjectOrder.RemoveOrderPackage(deleteOrderPackage, this.RootProject.FlowProjectDataContext);
                               // }));
                            }
                            else
                            {
                                OrderPackage deleteOrderPackage = this.RootProject.FlowProjectDataContext.OrderPackages.First(op => op.OrderPackageID == dop.OrderPackageID && op.AddDate == dop.AddDate);
                                deleteOrderPackage.SubjectOrder.RemoveOrderPackage(deleteOrderPackage, this.RootProject.FlowProjectDataContext);
                            }


                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.ErrorException("Error in Merge Sync Deleted Pacakges", ex);
                throw ex;
            }

            try
            {
                //get rid of OrderOptions that need deleted
                if (this.MergeProject.FlowProjectDataContext.DeletedOrderPackages.Count() > 0)
                {
                    logger.Debug("Merge: Sync Deleted OrderImageOptions");
                    List<DeletedOrderImageOption> dops = this.MergeProject.FlowProjectDataContext.DeletedOrderImageOptions.ToList();
                    foreach (DeletedOrderImageOption dop in dops)
                    {
                        if (this.RootProject.FlowProjectDataContext.OrderImageOptions.Any(op => op.SubjectImage.SubjectImageGuid == dop.SubjectImageGuid && op.ResourceURL == dop.ResourceURL))
                        {
                            //logger.Debug("Merge: Sync Deleted OrderImageOptions SubjectImageGuid - " + dop.SubjectImageGuid);
                            //logger.Debug("Merge: Sync Deleted OrderImageOptions Resource URL- " + dop.ResourceURL);

                            if (RootProject.MainDispatcher != null)
                            {
                                logger.Debug("Merge: Sync Deleted OrderImageOptions A");
                                //RootProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                                //{
                                    logger.Debug("Merge: Sync Deleted OrderImageOptions Begin");
                                    OrderImageOption deleteOrderImageOption = this.RootProject.FlowProjectDataContext.OrderImageOptions.First(op => op.SubjectImage.SubjectImageGuid == dop.SubjectImageGuid && op.ResourceURL == dop.ResourceURL);
                                    logger.Debug("Merge: Sync Deleted OrderImageOptions ImageGuid - " + deleteOrderImageOption.SubjectImage.SubjectImageGuid);
                                    deleteOrderImageOption.SubjectImage.Subject.RemoveCatalogOption(deleteOrderImageOption, false);
                                    RootProject.FlowProjectDataContext.SubmitChanges();
                                    logger.Debug("Merge: Sync Deleted OrderImageOptions End");
                                //}));
                                logger.Debug("Merge: Sync Deleted OrderImageOptions B");
                                Thread.Sleep(3000);
                            }
                            else
                            {
                                //logger.Debug("Merge: Sync Deleted OrderImageOptions B");
                                OrderImageOption deleteOrderImageOption = this.RootProject.FlowProjectDataContext.OrderImageOptions.First(op => op.SubjectImage.SubjectImageGuid == dop.SubjectImageGuid && op.ResourceURL == dop.ResourceURL);
                                deleteOrderImageOption.SubjectImage.Subject.RemoveCatalogOption(deleteOrderImageOption, false);
                                RootProject.FlowProjectDataContext.SubmitChanges();
                            }


                        }
                    }
                }

                //get rid of OrderOptions that need deleted
                if (this.MergeProject.FlowProjectDataContext.DeletedSubjectOrderOptions.Count() > 0)
                {
                    logger.Debug("Merge: Sync Deleted SubjectOrderOptions");
                    List<DeletedSubjectOrderOption> dops = this.MergeProject.FlowProjectDataContext.DeletedSubjectOrderOptions.ToList();
                    foreach (DeletedSubjectOrderOption dop in dops)
                    {
                        if (this.RootProject.FlowProjectDataContext.OrderSubjectOrderOptions.Any(op => op.SubjectOrder.Subject.SubjectGuid == dop.SubjectGuid && op.ResourceURL == dop.ResourceURL))
                        {
                            if (RootProject.MainDispatcher != null)
                            {
                               // RootProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                               // {
                                    OrderSubjectOrderOption deleteOrderSubjectOrderOption = this.RootProject.FlowProjectDataContext.OrderSubjectOrderOptions.First(op => op.SubjectOrder.Subject.SubjectGuid == dop.SubjectGuid && op.ResourceURL == dop.ResourceURL);
                                    deleteOrderSubjectOrderOption.SubjectOrder.Subject.RemoveCatalogOption(deleteOrderSubjectOrderOption, false);
                               // }));
                            }
                            else
                            {
                                OrderSubjectOrderOption deleteOrderSubjectOrderOption = this.RootProject.FlowProjectDataContext.OrderSubjectOrderOptions.First(op => op.SubjectOrder.Subject.SubjectGuid == dop.SubjectGuid && op.ResourceURL == dop.ResourceURL);
                                deleteOrderSubjectOrderOption.SubjectOrder.Subject.RemoveCatalogOption(deleteOrderSubjectOrderOption, false);
                            }


                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.ErrorException("Error in Merge Sync Deleted Order Options", ex);
                throw ex;
            }


            try
            {
                //get rid of subjectimages that need deleted
                if (this.MergeProject.FlowProjectDataContext.DeletedSubjectImages.Count() > 0)
                {
                    logger.Debug("Merge: Sync Deleted Images");
                    List<DeletedSubjectImage> dsis = this.MergeProject.FlowProjectDataContext.DeletedSubjectImages.GroupBy(p => p.SubjectImageGuid).Select(g => g.First()).ToList();
                    logger.Info("Merge: found images to delete - " + dsis.Count());
                    int i = 0;
                    foreach (DeletedSubjectImage dsi in dsis)
                    {
                        logger.Info("Merge: deleting image number " + i++);
                        if (this.RootProject.FlowProjectDataContext.ImageList.Any(si => si.SubjectImageGuid == dsi.SubjectImageGuid))
                        {

                            if (RootProject.MainDispatcher != null)
                            {
                               // RootProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                                // {
                                     SubjectImage deleteSubjectImage = this.RootProject.FlowProjectDataContext.ImageList.First(si => si.SubjectImageGuid == dsi.SubjectImageGuid);
                                     this.RootProject.FlowProjectDataContext.OrderImageOptions.DeleteAllOnSubmit(this.RootProject.FlowProjectDataContext.OrderImageOptions.Where(oio => oio.SubjectImage.SubjectImageGuid == deleteSubjectImage.SubjectImageGuid));
                                     this.RootProject.FlowProjectDataContext.SubmitChanges();
                                     deleteSubjectImage.Subject.DetachImage(deleteSubjectImage.ImageFileName);
                               //  }));
                            }
                            else
                            {
                                // RootProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                                // {
                                    SubjectImage deleteSubjectImage = this.RootProject.FlowProjectDataContext.ImageList.First(si => si.SubjectImageGuid == dsi.SubjectImageGuid);
                                    deleteSubjectImage.Subject.DetachImage(deleteSubjectImage.ImageFileName);
                                // }));
                            }

                            //if (!this.RootProject.FlowProjectDataContext.DeletedSubjectImages.Any(si => si.SubjectImageGuid == dsi.SubjectImageGuid))
                            //{
                            //    DeletedSubjectImage newDSI = new DeletedSubjectImage();
                            //    newDSI.SubjectImageGuid = dsi.SubjectImageGuid;
                            //    newDSI.DeleteDate = dsi.DeleteDate;

                            //    this.RootProject.FlowProjectDataContext.DeletedSubjectImages.InsertOnSubmit(newDSI);
                            //}
                        }

                        if (this.RootProject.FlowProjectDataContext.GroupImageList.Any(si => si.GroupImageGuid == dsi.SubjectImageGuid))
                        {

                            if (RootProject.MainDispatcher != null)
                            {
                               // RootProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                               // {
                                    GroupImage deleteSubjectImage = this.RootProject.FlowProjectDataContext.GroupImageList.First(si => si.GroupImageGuid == dsi.SubjectImageGuid);
                                    DeleteGroupImage(deleteSubjectImage);
                               // }));
                            }
                            else
                            {
                                GroupImage deleteSubjectImage = this.RootProject.FlowProjectDataContext.GroupImageList.First(si => si.GroupImageGuid == dsi.SubjectImageGuid);
                                DeleteGroupImage(deleteSubjectImage);
                            }
                        }

                       
                    }
                }
            }
            catch (Exception ex)
            {
                logger.ErrorException("Error in Merge Sync Deleted Images", ex);
                throw ex;
            }

            //get rid of subjects that need deleted
            try{
                if (this.MergeProject.FlowProjectDataContext.DeletedSubjects.Count() > 0)
                {
                    logger.Debug("Merge: Sync Deleted Subjects");
                    try
                    {
                        List<DeletedSubject> dss = this.MergeProject.FlowProjectDataContext.DeletedSubjects.GroupBy(p => p.SubjectGuid).Select(g => g.First()).ToList();
                        foreach (DeletedSubject ds in dss)
                        {
                            if (this.RootProject.FlowProjectDataContext.Subjects.Any(si => si.SubjectGuid == ds.SubjectGuid))
                            {
                                if (RootProject.MainDispatcher != null)
                                {
                                    RootProject.MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                                     {
                                    Subject deleteSubject = this.RootProject.FlowProjectDataContext.Subjects.First(si => si.SubjectGuid == ds.SubjectGuid);
                                    this.RootProject.SubjectList.DeleteSubject(deleteSubject);
                                      }));
                                }
                                else
                                {
                                    Subject deleteSubject = this.RootProject.FlowProjectDataContext.Subjects.First(si => si.SubjectGuid == ds.SubjectGuid);
                                    this.RootProject.SubjectList.DeleteSubject(deleteSubject);
                                }




                                //if (!this.RootProject.FlowProjectDataContext.DeletedSubjects.Any(si => si.SubjectGuid == ds.SubjectGuid))
                                //{
                                //    DeletedSubject newDS = new DeletedSubject();
                                //    newDS.SubjectGuid = ds.SubjectGuid;
                                //    newDS.DeleteDate = ds.DeleteDate;

                                //    this.RootProject.FlowProjectDataContext.DeletedSubjects.InsertOnSubmit(newDS);
                                //}
                            }
                        }
                    }
                    catch (Exception except)
                    {
                        //do nothing, if an error happened, it didnt get deleted.
                    }
                }
            }
            catch (Exception ex)
            {
                logger.ErrorException("Error in Merge Sync Deleted Sbuject", ex);
                throw ex;
            }

           

             }
             catch (Exception exp)
             {
                 logger.Error("Error in Merge for {0} \n {1}", this.RootProject.FlowProjectName, exp.Message);
                 throw exp;
             }
            logger.Info("Begin Merge for {0}", this.RootProject.FlowProjectName);


          

		}

        private void DeleteGroupImage(GroupImage img)
        {
            this.RootProject.FlowProjectDataContext.GroupImageList.Remove(img);
            this.RootProject.FlowProjectDataContext.GroupImages.DeleteOnSubmit(img);
            this.RootProject.FlowProjectDataContext.SubmitChanges();

            if (!this.RootProject.FlowProjectDataContext.DeletedSubjectImages.Any(ds => ds.SubjectImageGuid == img.GroupImageGuid))
            {
                DeletedSubjectImage dsi = new DeletedSubjectImage();
                dsi.DeleteDate = DateTime.Now;
                dsi.SubjectImageGuid = img.GroupImageGuid;
                this.RootProject.FlowProjectDataContext.DeletedSubjectImages.InsertOnSubmit(dsi);
            }
        }

		private void Merge_Complete(object sender, RunWorkerCompletedEventArgs e)
		{
			//foreach (Subject subject in this.RootProject.SubjectList)
			//{
			//    subject.Save();
			//}
            if (this.FlowMasterDataContext.PreferenceManager.Application.IsFlowServer)
            {
                this.ProgressInfo.Complete("Merge completed.");
            }
            else
            {
                this.ProgressInfo.Update("Merge completed.");
                this.ProgressInfo.ShowDoneButton();
            }
            logger.Info("Merge complete");
			FlowContext.ClearTempDirectory();
			this.MergeCompleted(this, new EventArgs());
		}

       
    }
}
