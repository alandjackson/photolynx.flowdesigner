﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.Linq;
using System.Data.SqlServerCe;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Media.Imaging;

using Flow.Lib;
using Flow.Lib.Database.SqlCe;
using Flow.Lib.Helpers;
using Flow.Schema.Properties;
using Flow.Schema.Utility;


namespace Flow.Schema.LinqModel.DataContext
{
	public class FilterBuilder<T> : List<KeyValuePair<T, object>> where T : ISubjectField
	{
		private string _expression = null;
		public string Expression
		{
			get
			{
				if (_expression == null)
				{
					string[] filterItems = new string[this.Count];

					for (int i = 0; i < this.Count; i++)
					{
						KeyValuePair<T, object> filterItem = this[i];

                        if (filterItem.Key != null)
                        {
                            string qualifier = (filterItem.Key.SubjectFieldDataType.RequiresQuoteQualifier)
                                    ? "'" : "";

                            string filterItemValue = filterItem.Value.ToString().Trim().Replace("'", "''");

                            filterItems[i] = "([" + filterItem.Key.SubjectFieldDisplayName + "] = " +
                                qualifier + filterItemValue + qualifier + ")";
                        }
					}

					_expression = String.Join(" AND ", filterItems);
				}

				return _expression;
			}
		}
	}
}
