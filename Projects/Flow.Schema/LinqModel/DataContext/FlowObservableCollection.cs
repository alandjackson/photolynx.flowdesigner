﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data.Linq;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Data;

using Flow.Lib.Helpers;
using System.Windows.Threading;

namespace Flow.Schema.LinqModel.DataContext
{

	/// <summary>
	/// Class wrapper for auto-generated Linq-to-SQL Table{T} and EntitySet{T} entities;
	/// this class automates synchronization of bound list controls to the
	/// source "list structure" by adding/deleting items to both the bound instance
	/// of this class and the entity from which it is based
	/// <typeparam name="T"></typeparam>
	public class FlowObservableCollection<T> : ObservableCollection<T>, INavigable
		where T : class, INotifyPropertyChanged, INotifyPropertyChanging, new()
	{

		#region STATIC FIELDS

		private static readonly Type GenericTableType = typeof(Table<T>);
		private static readonly Type GenericEntitySetType = typeof(EntitySet<T>);

		#endregion // STATIC FIELDS


		#region EVENTS

		public event EventHandler<EventArgs<T>> CurrentChanged = delegate { };
		public event EventHandler<EventArgs<T>> RemovingItem = delegate { };
		public event EventHandler ItemRemoved = delegate { };

		#endregion // EVENTS


		#region FIELDS AND PROPERTIES

		private Type _linqCollectionType = null;

		private Type _linqCollectionItemType = null;
		private PropertyInfo _descPropertyInfo = null;

		private FlowObservableCollectionItemComparer<T> _comparer = null;

        //public NotifyCollectionChangedEventHandler CollectionChanged;

		public T this[string itemDesc]
		{
			get
			{
				if (_linqCollectionItemType == null)
				{
					_linqCollectionItemType = typeof(T);
					_descPropertyInfo = _linqCollectionItemType.GetProperty(_linqCollectionItemType.Name + "Desc");
				}

				foreach (T item in this)
				{
					if (_descPropertyInfo.GetValue(item, null).ToString() == itemDesc)
						return item;
				}

				return null;
			}
		}


		/// <summary>
		/// Represents an item to be (provisionally) added to the collection;
		/// once the item is added this property if nullified
		/// </summary>
		private T _newItem = null;

		public T NewItem
		{
			get { return _newItem; }
			protected set
			{
				_newItem = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("CurrentItem"));
			}
		}

		/// <summary>
		/// Represents the base IEnumerable element supplying collection items
		/// </summary>
		private IEnumerable<T> _linqCollection = null;
		public IEnumerable<T> LinqCollection
		{
			get { return _linqCollection; }
		}

		private ListCollectionView _collectionView = null;
		public ListCollectionView View
		{
			get
			{
				if (_collectionView == null)
				{
					_collectionView = new ListCollectionView(this);

					_collectionView.CurrentChanging += new CurrentChangingEventHandler(CollectionView_CurrentChanging);
					_collectionView.CurrentChanged += new EventHandler(CollectionView_CurrentChanged);
				}

				return _collectionView;
			}
		}

		/// <summary>
		/// Provides a filtered encapsulation of the collection
		/// </summary>
		private ICollectionView _filteredCollectionView = null;
		public ICollectionView FilteredView
		{
			get
			{
				if (_filteredCollectionView == null)
				{
					_filteredCollectionView = CollectionViewSource.GetDefaultView(this);
					_filteredCollectionView.CurrentChanging += new CurrentChangingEventHandler(CollectionView_CurrentChanging);
					_filteredCollectionView.CurrentChanged += new EventHandler(CollectionView_CurrentChanged);
				}

				return _filteredCollectionView;
			}
		}

		/// <summary>
		/// Represents the item considered "current" of the collection (having focus);
		/// if a provisional (new) item is instantiated, it is returned;
		/// otherwise the current item of the ICollectionView (if any) is returned
		/// </summary>
		public T CurrentItem
		{
			get { return (this.NewItem == null) ? (T)this.View.CurrentItem : this.NewItem; }
			set { if(value != null) this.View.MoveCurrentTo(value); }
		}

		public bool HasItems
		{
			get { return this.RecordCount > 0; }
		}

		public bool IsEmpty
		{
			get { return this.RecordCount == 0; }
		}

		public bool IsFiltered
		{
			get { return this.View.Filter != null; }
		}

		#endregion // FIELDS AND PROPERTIES


		#region CONSTRUCTORS

		/// <summary>
		/// Constructs a new (empty) FlowObservableCollection
		/// </summary>
		/// <param name="linqCollection">The base entity containing data items to be bound</param>
		public FlowObservableCollection()
		{
			_linqCollection = new EntitySet<T>();
		}
		
		/// <summary>
		/// Constructs a new FlowObservableCollection populated from the supplied IEnumerable element
		/// </summary>
		/// <param name="linqCollection"></param>
		public FlowObservableCollection(IEnumerable<T> linqCollection)
			: base(linqCollection)
		{
			_linqCollection = linqCollection;
			_linqCollectionType = _linqCollection.GetType();

			_comparer = new FlowObservableCollectionItemComparer<T>();
		}

		#endregion // CONSTRUCTORS


		#region EVENT HANDLERS

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected virtual void CollectionView_CurrentChanging(object sender, CurrentChangingEventArgs e)
		{
			// NO BASE IMPLEMENTATION; PROVIDED FOR DERIVED CLASSES
		}

		/// <summary>
		/// Event raised when current item of collection view changes
		/// </summary>
		protected virtual void CollectionView_CurrentChanged(object sender, EventArgs e)
		{
			this.SendPropertyChanged("CurrentItem");
			this.SendPropertyChanged("CurrentOrdinalPosition");
			this.CurrentChanged(this, new EventArgs<T>(this.CurrentItem));
		}

		#endregion // CONSTRUCTORS


		#region METHODS

		protected void SendPropertyChanged(string propertyName)
		{
			this.OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
		}


		/// <summary>
		/// Removes all items from the collection (and the collection's source)
		/// </summary>
		protected override void ClearItems()
		{
			if (_linqCollectionType == FlowObservableCollection<T>.GenericTableType)
			{
				Table<T> entities = _linqCollection as Table<T>;
				// Mark all items from the source Table<T> for deletion
				entities.DeleteAllOnSubmit(entities);
			}

			else
			{
				EntitySet<T> entities = _linqCollection as EntitySet<T>;
				// Remove all items from the EntitySet<T>
                if (entities != null)
				    entities.Clear();
			}

			base.ClearItems();
		}

		//protected override void OnCollectionChanged(System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
		//{
		//    try
		//    {
		//        base.OnCollectionChanged(e);
		//    }
		//    catch(Exception exc) {}
		//}


		///// <summary>
		///// Overrides for CollectionChanged event to handle cross-thread- UI updates
		///// </summary>
		//public new NotifyCollectionChangedEventHandler CollectionChanged; 
		//protected override void OnCollectionChanged(System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
		//{
		//    // Be nice - use BlockReentrancy like MSDN said
		//    using (BlockReentrancy())
		//    {
		//        System.Collections.Specialized.NotifyCollectionChangedEventHandler eventHandler = CollectionChanged;
		//        if (eventHandler == null)
		//            return;

		//        Delegate[] delegates = eventHandler.GetInvocationList();
		//        // Walk thru invocation list
		//        foreach (System.Collections.Specialized.NotifyCollectionChangedEventHandler handler in delegates)
		//        {
		//            DispatcherObject dispatcherObject = handler.Target as DispatcherObject;
		//            // If the subscriber is a DispatcherObject and different thread
		//            if (dispatcherObject != null && dispatcherObject.CheckAccess() == false)
		//            {
		//                // Invoke handler in the target dispatcher's thread
		//                dispatcherObject.Dispatcher.Invoke(DispatcherPriority.DataBind, handler, this, e);
		//            }
		//            else // Execute handler as is
		//                handler(this, e);
		//        }
		//    }
		//}

		/// <summary>
		/// Inserts an item into the collection (and its source)
		/// </summary>
		/// <param name="index"></param>
		/// <param name="item"></param>
		protected override void InsertItem(int index, T item)
		{
			bool wasEmpty = this.IsEmpty;

			base.InsertItem(index, item);

			// If the collection was empty prior to insert, notify listeners that HasItems is true
			if (wasEmpty)
				this.SendPropertyChanged("HasItems");

			if(_linqCollectionType == FlowObservableCollection<T>.GenericTableType)
			{
				// Add the item to the source Table<T>
				(_linqCollection as Table<T>).InsertOnSubmit(item);
			}
			else
			{
				// Add the item to the source EntitySet<T>
				(_linqCollection as EntitySet<T>).Add(item);
			}

			// Nullify the (now inserted) NewItem object and set the inserted item to current
			this.NewItem = null;
			try { this.CurrentItem = item; } catch (Exception e) { }

			// Notify listeners of change to RecordCount
			this.SendPropertyChanged("RecordCount");
		}


		/// <summary>
		/// Removes the item at the supplied index from the collection (and its source)
		/// </summary>
		/// <param name="index"></param>
		protected override void RemoveItem(int index)
		{
			// Get reference to item at supplied index
			T item = this.ElementAt(index);

			// Notify listeners that an item is about to be removed
			this.RemovingItem(this, new EventArgs<T>(item));

			if (_linqCollectionType == FlowObservableCollection<T>.GenericTableType)
			{
				// Mark the item in the source Table<T> for delete
				(_linqCollection as Table<T>).DeleteOnSubmit(item);
			}
			else
			{
				// Remove the item from the source EntitySet<T>
				(_linqCollection as EntitySet<T>).Remove(item);
			}
	
			base.RemoveItem(index);

			// Notify listeners that the item has been removed
			this.ItemRemoved(this, new EventArgs());

			// If the collection is now empty, notify listeners that HasItems is false
			if (this.IsEmpty)
				this.SendPropertyChanged("HasItems");

			// If the collection has items, move current to the item which is now at the index
			else
				this.MoveCurrentToPosition(index);

			// Notify listeners that RecordCount has changed
			this.SendPropertyChanged("RecordCount");
		}

		/// <summary>
		/// Creates a new item of type T; the current item in the collection is set to this provisional item
		/// </summary>
		public virtual void CreateNew()
		{
			this.NewItem = new T();
		}

		/// <summary>
		/// Cancels the provisional new item before it is committed
		/// </summary>
		public virtual void CancelNew()
		{
			this.NewItem = null;
		}

		/// <summary>
		/// Adds the provisional new item to the collection and nullifies the NewItem property
		/// </summary>
		public virtual void CommitNew()
		{
			this.Add(this.NewItem);
			this.NewItem = null;
		}

		/// <summary>
		/// Reverts the current item to it's state prior to any unsaved modifications
		/// </summary>
		public void RevertCurrent()
		{
			if (_linqCollectionType == FlowObservableCollection<T>.GenericTableType)
			{
				(_linqCollection as Table<T>).GetOriginalEntityState(this.CurrentItem);
			}
		}

		/// <summary>
		/// Deletes the item current of the collection
		/// </summary>
		public virtual void DeleteCurrent()
		{
			this.Remove(this.CurrentItem);
		}

		/// <summary>
		/// Compares an item's description to the descriptions of items in the list and
		/// returns true if there are any duplicates
		/// </summary>
		/// <param name="item"></param>
		public bool ContainsDesc(T item)
		{
			return this.Contains<T>(item, _comparer);
		}

		/// <summary>
		/// Filters the collection based on the supplied criteria express as a Predicate object
		/// </summary>
		/// <param name="filter"></param>
		public void ApplyFilter(Predicate<object> filter )
		{
			this.FilteredView.Filter = filter;
		}


		#endregion // METHODS


		#region INavigable Members

		public int CurrentPosition
		{
			get { return (this.NewItem == null) ? this.View.CurrentPosition : -1; }
		}

		public int CurrentOrdinalPosition
		{
			get { return this.CurrentPosition + 1; }
			set { this.MoveCurrentToPosition(value - 1); }
		}

		public int AbsolutePosition
		{
			get { return this.View.CurrentPosition; }
		}

		//private int _recordCount = 0;
		//public int RecordCount
		//{
		//    get { return _recordCount; }
		//    protected set
		//    {
		//        _recordCount = value;

		//        this.SendPropertyChanged("RecordCount");
		//    }
		//}

		public int RecordCount
		{
			get { return this.View.Count; }
		}

		public void MoveCurrentToFirst()
		{
			if(this.HasItems && this.View.CurrentPosition != 0)
				this.View.MoveCurrentToFirst();
		}

		public void MoveCurrentToPrevious()
		{
			if (this.HasItems)
			{
				if (this.View.CurrentPosition > 0)
					this.View.MoveCurrentToPrevious();
				else
					this.View.MoveCurrentToLast();
			}
		}

		public void MoveCurrentToNext()
		{
			if (this.HasItems)
			{
				if (this.CurrentOrdinalPosition < this.RecordCount)
					this.View.MoveCurrentToNext();
				else
					this.View.MoveCurrentToFirst();
			}
		}

		public void MoveCurrentToLast()
		{
			if (this.HasItems && this.CurrentOrdinalPosition < this.RecordCount)
				this.View.MoveCurrentToLast();
		}

		public void MoveCurrentToPosition(int position)
		{
			if (this.HasItems)
			{
				if (position < this.RecordCount)
					this.View.MoveCurrentToPosition(position);
				else
					this.View.MoveCurrentToLast();
			}
		}

		public void MoveCurrentTo(object item)
		{
			T typedItem = (T)item;

			if(this.Contains(typedItem))
				this.View.MoveCurrentTo(item);
		}

		#endregion // INavigable Members

	}	// END class



	/// <summary>
	/// Compares two items of the same type, determining equality by comparison of a property named
	/// with the convention {Type}Desc where {Type} represents the name of the supplied type T
	/// </summary>
	/// <typeparam name="T"></typeparam>
	internal class FlowObservableCollectionItemComparer<T> : IEqualityComparer<T>
		where T : class, INotifyPropertyChanged, INotifyPropertyChanging
	{
		private PropertyInfo _descColumnPropertyInfo = null;

		internal FlowObservableCollectionItemComparer()
		{
			Type itemType = typeof(T);
			_descColumnPropertyInfo = itemType.GetProperty(itemType.Name + "Desc");
		}
		
		public bool Equals(T x, T y)
		{
			if (Object.ReferenceEquals(x, y)) return true;

			if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
				return false;

			string xDesc = _descColumnPropertyInfo.GetValue(x, null).ToString();
			string yDesc = _descColumnPropertyInfo.GetValue(y, null).ToString();

			return xDesc == yDesc;
		}

		public int GetHashCode(T item)
		{
			if (Object.ReferenceEquals(item, null)) return 0;

			string itemDesc = _descColumnPropertyInfo.GetValue(item, null).ToString();

			return itemDesc == null ? 0 : item.GetHashCode();
		}
	}


}	// END namespace
