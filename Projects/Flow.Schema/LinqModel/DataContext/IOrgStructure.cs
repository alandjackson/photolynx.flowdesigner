﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flow.Schema.LinqModel.DataContext
{
	public interface IOrgStructure
	{
		int OrgStructureID { get; set; }
		int RelationalID { get; set; }
		int UnitTypeID { get; set; }
		int UnitScopeID { get; }
		int? UnitTypeParentID { get; set; }
	}
}
