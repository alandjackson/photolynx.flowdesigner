﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flow.Schema.LinqModel.DataContext
{
	/// <summary>
	/// Represents a "navigable" type of data structure
	/// </summary>
	public interface INavigable
	{
		int CurrentPosition { get; }
		int CurrentOrdinalPosition { get; }
		int RecordCount { get; }

		bool IsFiltered { get; }

		void MoveCurrentToFirst();
		void MoveCurrentToPrevious();
		void MoveCurrentToNext();
		void MoveCurrentToLast();
		void MoveCurrentToPosition(int position);
		void MoveCurrentTo(object item);
	}
}
