﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.Linq;
using System.Data.SqlServerCe;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Media.Imaging;

using Flow.Lib;
using Flow.Lib.Database.SqlCe;
using Flow.Lib.Helpers;
using Flow.Schema.Properties;
using Flow.Schema.Utility;


namespace Flow.Schema.LinqModel.DataContext
{
	/// <summary>
	/// Represents a human role as a member of an organization;
	/// e.g., student, teacher, and vice-principal are SubjectTypes of a school Organization
	/// </summary>
	partial class SubjectType
	{
		/// <summary>
		/// Attempts to convert the supplied parameter to a ProjectTemplateSubjectType object
		/// </summary>
		/// <param name="item"></param>
		/// <returns>ProjectTemplateSubjectType</returns>
		public static ProjectTemplateSubjectType ConvertFrom(object item)
		{
			if (item is ProjectTemplateSubjectType)
				return (ProjectTemplateSubjectType)item;

			// NOTE: this can be implemented as an implicit conversion
			if (item is SubjectType)
			{
				ProjectTemplateSubjectType castItem = new ProjectTemplateSubjectType();
				castItem.ProjectTemplateSubjectTypeDesc = (item as SubjectType).SubjectTypeDesc;

				return castItem;
			}

			return null;
		}
	}
}
