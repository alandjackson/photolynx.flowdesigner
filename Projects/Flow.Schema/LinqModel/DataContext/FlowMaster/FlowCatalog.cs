﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Xml;

namespace Flow.Schema.LinqModel.DataContext
{
	partial class FlowCatalog
	{
		public FlowMasterDataContext FlowMasterDataContext { get; set; }

        private bool _allowCatalogEditing { get; set; }
        public bool AllowCatalogEditing
        {
            get
            {
                return _allowCatalogEditing;
            }
            set
            {
                _allowCatalogEditing = value;
                this.SendPropertyChanged("AllowCatalogEditing");
                this.SendPropertyChanged("CatalogEditingLocked");
            }
        }
        public bool CatalogEditingLocked { get { return !AllowCatalogEditing; } }

        public void RefreshPackageList()
        {
            ProductPackage tpp = ProductPackageList.CurrentItem;
            _productPackageList = null;
            this.SendPropertyChanged("ProductPackageList");
            this.SendPropertyChanged("ProductPackageListSorted");
            ProductPackageList.CurrentItem = tpp;
            
        }

		private ImageQuixCatalog _assignedImageQuixCatalog = null;
		public ImageQuixCatalog AssignedImageQuixCatalog
		{
			get
			{
                if (_assignedImageQuixCatalog == null && this.FlowMasterDataContext != null)
					_assignedImageQuixCatalog = this.FlowMasterDataContext.ImageQuixCatalogList
						.FirstOrDefault(c => c.ImageQuixCatalogID == this.ImageQuixCatalogID);

				return _assignedImageQuixCatalog;
			}
		}

		private FlowObservableCollection<ProductPackage> _productPackageList;
		public FlowObservableCollection<ProductPackage> ProductPackageList
		{
			get
			{
				if (_productPackageList == null)
				{
                    _productPackageList = new FlowObservableCollection<ProductPackage>(this.ProductPackages);
				}

                return _productPackageList;
			}
		}

        public List<ProductPackage> ProductPackageListSorted
        {
            get
            {
                return ProductPackageList.OrderBy(a=>a.IsALaCarte).ToList();
            }
        }

		private List<ImageQuixOptionGroup> _optionGroupList = null;
		public List<ImageQuixOptionGroup> OptionGroupList
		{
			get
			{
				//return this.FlowMasterDataContext.OptionGroupList;

                if (_optionGroupList == null)
                {
                    _optionGroupList = this.FlowMasterDataContext.OptionGroupList;

                    //this.FlowMasterDataContext.ImageQuixOptionGroups.ToList();

                    //_optionGroupList = new List<ImageQuixOptionGroup>(
                    //    this.FlowMasterDataContext.ImageQuixOptionGroups
                    //);
                }

                return _optionGroupList;
			}
		}
        private FlowObservableCollection<FlowCatalogOption> _flowCatalogOptionList = null;
        public FlowObservableCollection<FlowCatalogOption> FlowCatalogOptionList
        {
            get
            {
                if (_flowCatalogOptionList == null)
                {
                    if (_flowCatalogOptionList == null)
                        _flowCatalogOptionList = new FlowObservableCollection<FlowCatalogOption>(this.FlowCatalogOptions);
                }

                return _flowCatalogOptionList;
            }
        }

        private FlowObservableCollection<FlowCatalogOption> _flowCatalogImageOptionList = null;
        public FlowObservableCollection<FlowCatalogOption> FlowCatalogImageOptionList
        {
            get
            {
                if (_flowCatalogImageOptionList == null)
                {
                    if (_flowCatalogImageOptionList == null)
                        _flowCatalogImageOptionList = new FlowObservableCollection<FlowCatalogOption>(this.FlowCatalogOptions.Where(fco => (fco.ImageQuixCatalogOption.ImageQuixCatalogOptionGroup.Label == "Image Options" || fco.ImageQuixCatalogOption.ImageQuixCatalogOptionGroup.IsImageOption) && fco.ImageQuixCatalogOption.AppliesTo != "Background" ));
                }

                return _flowCatalogImageOptionList;
            }
        }


        private FlowObservableCollection<FlowCatalogOption> _flowCatalogSubjectOrderOptionList = null;
        public FlowObservableCollection<FlowCatalogOption> FlowCatalogSubjectOrderOptionList
        {
            get
            {
                if (_flowCatalogSubjectOrderOptionList == null)
                {
                    if (_flowCatalogSubjectOrderOptionList == null)
                        _flowCatalogSubjectOrderOptionList = new FlowObservableCollection<FlowCatalogOption>(this.FlowCatalogOptions.Where(fco => fco.ImageQuixCatalogOption.ImageQuixCatalogOptionGroup.Label == "Subject Order Options"));
                }

                return _flowCatalogSubjectOrderOptionList;
            }
        }
        private FlowObservableCollection<FlowCatalogOption> _flowCatalogOrderOptionList = null;
        public FlowObservableCollection<FlowCatalogOption> FlowCatalogOrderOptionList
        {
            get
            {
                if (_flowCatalogOrderOptionList == null)
                {
                    if (_flowCatalogOrderOptionList == null)
                        _flowCatalogOrderOptionList = new FlowObservableCollection<FlowCatalogOption>(this.FlowCatalogOptions.Where(fco => fco.ImageQuixCatalogOption.ImageQuixCatalogOptionGroup.Label == "Order Options"));
                }
                
                return _flowCatalogOrderOptionList;
            }
        }

        private FlowObservableCollection<FlowCatalogOption> _flowCatalogShippingOptionList = null;
        public FlowObservableCollection<FlowCatalogOption> FlowCatalogShippingOptionList
        {
            get
            {
                if (_flowCatalogShippingOptionList == null)
                {
                    if (_flowCatalogShippingOptionList == null)
                        _flowCatalogShippingOptionList = new FlowObservableCollection<FlowCatalogOption>(this.FlowCatalogOptions.Where(fco => fco.ImageQuixCatalogOption.ImageQuixCatalogOptionGroup.Label.ToLower().Contains("ship")));
                }

                return _flowCatalogShippingOptionList;
            }
        }

        private FlowObservableCollection<FlowCatalogOption> _flowCatalogPackagingOptionList = null;
        public FlowObservableCollection<FlowCatalogOption> FlowCatalogPackagingOptionList
        {
            get
            {
                if (_flowCatalogPackagingOptionList == null)
                {
                    if (_flowCatalogPackagingOptionList == null)
                        _flowCatalogPackagingOptionList = new FlowObservableCollection<FlowCatalogOption>(this.FlowCatalogOptions.Where(fco => fco.ImageQuixCatalogOption.ImageQuixCatalogOptionGroup.Label.ToLower().Contains("packaging")));
                }

                return _flowCatalogPackagingOptionList;
            }
        }
		//private Dictionary<int,ImageQuixOptionObservableCollection> _optionSets = null;
		//public Dictionary<int, ImageQuixOptionObservableCollection> OptionSets
		//{
		//    get
		//    {
		//        if (_optionSets == null)
		//        {
		//            _optionSets = new Dictionary<int, ImageQuixOptionObservableCollection>();
		
		//            IEnumerable<ImageQuixOptionGroup> groups =
		//                this.FlowMasterDataContext.ImageQuixOptionGroups.Where(g => g.ImageQuixOptions.Count > 0);
		
		//            foreach (ImageQuixOptionGroup group in groups)
		//            {
		//                List<ImageQuixOption> options = group.ImageQuixOptions.ToList();

		//                if (!group.IsMandatory)
		//                {
		//                    ImageQuixOption nullOption = new ImageQuixOption();
		//                    nullOption.PrimaryKey = null;
		//                    nullOption.Label = "<None>";

		//                    options.Insert(0, nullOption);
		//                }

		//                _optionSets.Add(
		//                    group.PrimaryKey.Value,
		//                    new ImageQuixOptionObservableCollection(options)
		//                );
		//            }
		//        }

		//        return _optionSets;
		//    }
		//}

		/// <summary>
		/// Indicates if no FlowProjects assign this catalog
		/// </summary>
		public bool IsAssigned
		{
			get {
                if (this.FlowMasterDataContext == null)
                    return false;
                return this.FlowMasterDataContext.FlowProjectList.Any(p => p.FlowCatalogID == this.FlowCatalogID); }
		}

		/// <summary>
		/// Indicates if no FlowProjects assign this catalog
		/// </summary>
		public bool IsUnassigned
		{
			get { return !this.IsAssigned; }
		}

		/// <summary>
		/// Constructor; used for adding a FlowCatalog transferred via remote project attachment
		/// </summary>
		/// <param name="flowProject"></param>
		/// <param name="sourceCatalog"></param>
		/// <param name="sourceImageQuixCatalog"></param>
		public FlowCatalog(FlowProjectDataContext importProjectDataContext, Control_FlowCatalog sourceCatalog, ImageQuixCatalog sourceImageQuixCatalog)
			: this()
		{
			this.FlowCatalogGuid = sourceCatalog.FlowCatalogGuid;
			this.FlowCatalogDesc = sourceCatalog.FlowCatalogDesc;
			this.FlowCatalogCode = sourceCatalog.FlowCatalogCode;
			this.TaxRate = sourceCatalog.TaxRate;
			this.ImageQuixCatalogID = sourceImageQuixCatalog.ImageQuixCatalogID;

			foreach (Control_ProductPackage sourcePackage in importProjectDataContext.Control_ProductPackages)
			{
				ProductPackage productPackage = new ProductPackage(importProjectDataContext, sourcePackage, sourceImageQuixCatalog);
				this.ProductPackages.Add(productPackage);
			}

            foreach (Control_FlowCatalogOption sourceOption in importProjectDataContext.Control_FlowCatalogOptions)
            {
                FlowCatalogOption fco = new FlowCatalogOption(importProjectDataContext, sourceOption, sourceImageQuixCatalog, this);
                if(fco.ImageQuixCatalogOptionID != null && fco.ImageQuixCatalogOptionID >0)
                    this.FlowCatalogOptions.Add(fco);
            }
		}


        /// <summary>
        /// Constructor; used for duplicating a Flow Catalog
        /// </summary>
        /// <param name="sourceCatalog"></param>
        public FlowCatalog(FlowCatalog sourceCatalog)
            : this()
        {
            this.FlowCatalogGuid = this.FlowCatalogGuid = Guid.NewGuid(); 
            this.FlowCatalogDesc = sourceCatalog.FlowCatalogDesc + " (Copy)";
            this.FlowCatalogCode = sourceCatalog.FlowCatalogCode;
            this.TaxRate = sourceCatalog.TaxRate;
            this.ImageQuixCatalogID = sourceCatalog.ImageQuixCatalogID;

            foreach (ProductPackage sourcePackage in sourceCatalog.ProductPackages)
            {
                ProductPackage productPackage = new ProductPackage(sourcePackage);
                this.ProductPackages.Add(productPackage);
            }

            foreach (FlowCatalogOption sourceOption in sourceCatalog.FlowCatalogOptions)
            {
                FlowCatalogOption fco = new FlowCatalogOption(sourceOption);
                if (fco.ImageQuixCatalogOptionID != null && fco.ImageQuixCatalogOptionID > 0)
                    this.FlowCatalogOptions.Add(fco);
            }
        }

       
		/// <summary>
		/// 
		/// </summary>
		partial void OnCreated()
		{
			//if(this.FlowCatalogGuid == Guid.Empty)
			//    this.FlowCatalogGuid = Guid.NewGuid();
            
		}

        public void Select()
        {
            if (this.FlowMasterDataContext != null)
            {
                this.FlowMasterDataContext.FlowCatalogList.CurrentItem = this;
                RefreshFlowCatalogOptions();
            }
        }

		/// <summary>
		/// 
		/// </summary>
		partial void OnLoaded()
		{
			if(!this.FlowCatalogGuid.HasValue || this.FlowCatalogGuid.Value == Guid.Empty)
				this.FlowCatalogGuid = Guid.NewGuid();

            
		}


		/// <summary>
		/// 
		/// </summary>
		public void PrepareDelete()
		{
            this.FlowMasterDataContext.FlowCatalogOptions.DeleteAllOnSubmit(this.FlowCatalogOptions);
			foreach (ProductPackage productPackage in this.ProductPackageList.ToList())
			{
				this.DeleteProductPackage(productPackage);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="productPackage"></param>
		public void DeleteProductPackage(ProductPackage productPackage)
		{
			if (productPackage.ProductPackageCompositionList.HasItems)
			{
				this.FlowMasterDataContext.ProductPackageCompositions.DeleteAllOnSubmit(productPackage.ProductPackageCompositionList);
				productPackage.ProductPackageCompositionList.Clear();
			}

			// This prevents an error involving newly added items in relationships
			try	{ this.FlowMasterDataContext.ProductPackages.DeleteOnSubmit(productPackage);	}
			catch { }
			
			this.ProductPackageList.Remove(productPackage);
			this.FlowMasterDataContext.SubmitChanges();
		}

		internal ImageQuixOptionGroup GetImageQuixOptionGroupByKey(int? primaryKey)
		{
			return this.FlowMasterDataContext.ImageQuixOptionGroups.FirstOrDefault(g => g.PrimaryKey.Value == primaryKey.Value);
		}

		internal ImageQuixProduct GetImageQuixProductByKey(int primaryKey)
		{
			return this.FlowMasterDataContext.ImageQuixProducts.FirstOrDefault(p => p.PrimaryKey == primaryKey);
		}

		/// <summary>
		/// Ensures that the order product quantity matches the catalog composition quantity for the product
		/// </summary>
		/// <param name="guid"></param>
		/// <param name="orderProduct"></param>
		internal void VerifyOrderProductQuantity(Guid guid, OrderProduct orderProduct)
		{
			ProductPackage productPackage = this.ProductPackageList.FirstOrDefault(p => p.ProductPackageGuid == guid);

			if (productPackage != null)
			{
				ProductPackageComposition composition =
					productPackage.ProductPackageCompositionList.FirstOrDefault(
						c => c.ImageQuixProduct.PrimaryKey == orderProduct.ImageQuixProductPk
					);

				if (composition != null)
					orderProduct.Quantity = composition.Quantity;
			}
		}

        public void InitCatalogOptions(FlowMasterDataContext flowMasterDataContext, ImageQuixCatalog imageQuixCatalog)
        {
            foreach (ImageQuixCatalogOptionGroup icog in imageQuixCatalog.CatalogOptionGroupList)
            {
                foreach (ImageQuixCatalogOption ico in flowMasterDataContext.ImageQuixCatalogOptions.Where(op => op.ImageQuixCatalogOptionGroupID == icog.ImageQuixCatalogOptionGroupID))
                {
                    if (this.FlowCatalogOptions.Count(o => o.ImageQuixCatalogOptionID == ico.ImageQuixCatalogOptionID) == 0)
                    {
                        FlowCatalogOption fco = new FlowCatalogOption();
                        fco.ImageQuixCatalogOptionID = ico.ImageQuixCatalogOptionID;
                        fco.Price = (decimal)ico.Price * (decimal)2;
                        fco.Taxed = false;
                        fco.ImageQuixCatalogOption = ico;
                        fco.FlowCatalogID = this.FlowCatalogID;
                        fco.FlowCatalog = this;
                        if (flowMasterDataContext.IsPUDConfiguration)
                            fco.Map = ico.ResourceURL;
                        else
                            fco.Map = "";
                        fco.FlowCatalogOptionGuid = Guid.NewGuid();
                        fco.IQPrimaryKey = ico.PrimaryKey;
                        if (!ico.FlowCatalogOptions.Any(f => f.FlowCatalogOptionGuid == fco.FlowCatalogOptionGuid))
                            ico.FlowCatalogOptions.Add(fco);
                        if (!this.FlowCatalogOptions.Any(f => f.FlowCatalogOptionGuid == fco.FlowCatalogOptionGuid))
                            this.FlowCatalogOptions.Add(fco);
                        if(!flowMasterDataContext.FlowCatalogOptions.Any(f => f.FlowCatalogOptionGuid == fco.FlowCatalogOptionGuid))
                            flowMasterDataContext.FlowCatalogOptions.InsertOnSubmit(fco);
                        flowMasterDataContext.SubmitChanges();
                    }
                }
            }
            RefreshFlowCatalogOptions();
        }

        public void RefreshFlowCatalogOptions()
        {
            _flowCatalogImageOptionList = null;
            this.SendPropertyChanged("FlowCatalogImageOptionList");
            _flowCatalogOrderOptionList = null;
            this.SendPropertyChanged("FlowCatalogOrderOptionList");
            _flowCatalogOptionList = null;
            this.SendPropertyChanged("FlowCatalogOptionList");
        }

        public void WritePRMData(string prmFile)
        {

              
                //////////////////////////
                // Create prmFile content
                //////////////////////////
                XmlDocument prmXml = new XmlDocument();
                XmlDeclaration dec = prmXml.CreateXmlDeclaration("1.0", null, null);
                prmXml.AppendChild(dec);// Create the root element
                XmlElement programs = prmXml.CreateElement("Programs");
                prmXml.AppendChild(programs);
                {
                    XmlElement bus = prmXml.CreateElement("Business");
                    bus.InnerText = "Flow Job";
                    programs.AppendChild(bus);

                    XmlElement date = prmXml.CreateElement("Date");
                    date.InnerText = DateTime.Now.ToShortDateString();
                    programs.AppendChild(date);

                    XmlElement time = prmXml.CreateElement("Time");
                    time.InnerText = DateTime.Now.ToShortTimeString();
                    programs.AppendChild(time);

                    XmlElement program = prmXml.CreateElement("Program");
                    {
                        XmlElement code = prmXml.CreateElement("Code");
                        code.InnerText = "";
                        program.AppendChild(code);

                        XmlElement desc = prmXml.CreateElement("Description");
                        desc.InnerText = "";
                        program.AppendChild(desc);

                        XmlElement tax = prmXml.CreateElement("Tax");
                        {
                            XmlElement include = prmXml.CreateElement("Include");
                            include.InnerText = "False";
                            tax.AppendChild(include);

                            XmlElement rate = prmXml.CreateElement("Rate");
                            rate.InnerText = "0";
                            tax.AppendChild(rate);

                        }
                        program.AppendChild(tax);
                        //this is where the work begins.....
                        try
                        {
                            foreach (ProductPackage pp in this.ProductPackages)
                            {

                                bool isPackage = true;
                                //bool isSpecialField = Convert.ToBoolean(xn["IsSpecialField"].InnerText);
                                //bool isOption = Convert.ToBoolean(xn["IsOption"].InnerText);

                                //BubbleDataSummary bubSum = new BubbleDataSummary();
                                //bubSum.BubbleID = Convert.ToInt16(xn["BubbleID"].InnerText);

                                //if (isSpecialField)
                                //{
                                //    bubSum.isSpecial = true;
                                //    if (xn["ViewText"] != null) bubSum.Value = xn["ViewText"].InnerText;
                                //}
                                //else if (isOption)
                                //{
                                //    bubSum.isOption = true;
                                //}
                                //else if (isPackage)
                                //{

                                    //bubSum.isPackage = true;
                                    //if (xn["ViewText"] != null) bubSum.Value = xn["ViewText"].InnerText;

                                    XmlElement package = prmXml.CreateElement("Package");
                                    {

                                        XmlElement pCode = prmXml.CreateElement("Code");
                                        pCode.InnerText = "";
                                        package.AppendChild(pCode);

                                        XmlElement pdesc = prmXml.CreateElement("Description");
                                        if (pp.ProductPackageDesc != null) pdesc.InnerText = pp.ProductPackageDesc;
                                        package.AppendChild(pdesc);

                                        XmlElement pPrice = prmXml.CreateElement("Price");
                                        pPrice.InnerText = pp.Price.ToString();
                                        package.AppendChild(pPrice);

                                        XmlElement pCost = prmXml.CreateElement("Cost");
                                        pCost.InnerText = pp.Cost.ToString();
                                        package.AppendChild(pCost);

                                        XmlElement pTaxed = prmXml.CreateElement("Taxed");
                                        if(pp.Taxed)
                                            pTaxed.InnerText = "True";
                                        else
                                            pTaxed.InnerText = "False";
                                        package.AppendChild(pTaxed);

                                        XmlElement pSpecial = prmXml.CreateElement("Special");
                                        pSpecial.InnerText = "";
                                        package.AppendChild(pSpecial);

                                        XmlElement pMap = prmXml.CreateElement("Map");
                                        if (pp.Map != null && pp.Map.Length > 0) pMap.InnerText = pp.Map;
                                        package.AppendChild(pMap);

                                        XmlElement pTitle = prmXml.CreateElement("Title");
                                        if (pp.ProductPackageDesc != null) pTitle.InnerText = pp.ProductPackageDesc;
                                        package.AppendChild(pTitle);

                                        XmlElement pComm = prmXml.CreateElement("Commission");
                                        pComm.InnerText = "0";
                                        package.AppendChild(pComm);


                                        
                                        foreach (ProductPackageComposition comp in pp.ProductPackageCompositionList)
                                        {
                                            //if (comp.ImageQuixProduct.Map != null)
                                            //{
                                                XmlElement unit = prmXml.CreateElement("Unit");
                                                {
                                                    XmlElement uCode = prmXml.CreateElement("Code");
                                                    uCode.InnerText = "";
                                                    unit.AppendChild(uCode);

                                                    XmlElement udesc = prmXml.CreateElement("Description");
                                                    if (comp.ImageQuixProduct.Label != null) udesc.InnerText = comp.ImageQuixProduct.Label;
                                                    unit.AppendChild(udesc);

                                                    XmlElement uTitle = prmXml.CreateElement("Title");
                                                    if (comp.ImageQuixProduct.Label != null) uTitle.InnerText = comp.ImageQuixProduct.Label;
                                                    unit.AppendChild(uTitle);

                                                    XmlElement uMap = prmXml.CreateElement("Map");
                                                    if (comp.ImageQuixProduct.Map != null) uMap.InnerText =comp.ImageQuixProduct.Map;
                                                    unit.AppendChild(uMap);

                                                    XmlElement uQty = prmXml.CreateElement("Quantity");
                                                    uQty.InnerText = comp.Quantity.ToString();
                                                    unit.AppendChild(uQty);
                                                }
                                                package.AppendChild(unit);
                                            //}
                                        }

                                    //}
                                    program.AppendChild(package);
                                }
                                //BubbleValues.Add(bubSum);
                            }
                        }
                        catch (Exception)
                        {

                            throw;
                        }
                    }

                    programs.AppendChild(program);
                }
                //////////////////////////


                //write to out file (prmXml to prmFile)
                prmXml.Save(prmFile);

            }

        public ProductPackage GetProductPackage(string packageName)
        {
            foreach (ProductPackage pp in this.ProductPackageList)
            {
                if (pp.ProductPackageDesc == packageName)
                    return pp;
            }
            return null;
        }
    }
    

	/// <summary>
	/// 
	/// </summary>
	public class ImageQuixOptionObservableCollection : List<ImageQuixOption>
	{
		public ImageQuixOptionObservableCollection(IEnumerable<ImageQuixOption> options) : base(options) { }
	}

    
}
