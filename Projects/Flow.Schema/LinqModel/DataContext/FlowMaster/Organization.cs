﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

using Flow.Lib.Helpers;

namespace Flow.Schema.LinqModel.DataContext
{
	/// <summary>
	/// Represents an Organization providing subjects for a photographic session
	/// </summary>
    /// 
    [Serializable]
	partial class Organization
	{
		public bool IsStudio { get { return this.OrganizationTypeID == 4; } }

        public Dictionary<string, string> Fields
        {
            get
            {
                Dictionary<string, string> temp = new Dictionary<string, string>();
                temp.Add("Guid", OrganizationGuid.ToString());
                 temp.Add("Name", OrganizationName);
                 temp.Add("AddressLine1", OrganizationAddressLine1);
                 temp.Add("AddressLine2", OrganizationAddressLine2);
                 temp.Add("City", OrganizationCity);
                 temp.Add("StateCode", OrganizationStateCode);
                 temp.Add("ZipCode", OrganizationZipCode);
                 temp.Add("ContactName", OrganizationContactName);
                 temp.Add("ContactEmail", OrganizationContactEmail);
                 temp.Add("ContactPhone", OrganizationContactPhone);
                 temp.Add("TypeID", OrganizationTypeID.ToString());
                 temp.Add("DateCreated", DateCreated.ToString());
                 temp.Add("DateModified", DateModified.ToString());
                 temp.Add("LabCustomerID", LabCustomerID);
                
                return temp;
            }
        }
		partial void OnCreated()
		{
			//if(this.OrganizationGuid == Guid.Empty)
			//    this.OrganizationGuid = Guid.NewGuid();
		}

		partial void  OnLoaded()
		{
			if (this.OrganizationGuid == Guid.Empty)
				this.OrganizationGuid = Guid.NewGuid();
		}

        public string Organizationabbreviation {
            get
            {
                string temp = this.OrganizationName.Replace(" ", "").ToLower();
                if (temp.Length > 16)
                {
                    temp = temp.Substring(0, 16);
                }
                return temp;
            }
            }
		public void CopyMainAddressToShipping()
		{
			this.OrganizationShippingAddressLine1 = this.OrganizationAddressLine1;
			this.OrganizationShippingAddressLine2 = this.OrganizationAddressLine2;
			this.OrganizationShippingCity = this.OrganizationCity;

			if(this.OrganizationStateCode != null)
				this.OrganizationShippingStateCode = this.OrganizationStateCode.Trim();

			if (this.OrganizationZipCode != null)
				this.OrganizationShippingZipCode = this.OrganizationZipCode.Trim();
		}

		/// <summary>
		/// Gets an XElement representing a studio's information for order submission
		/// </summary>
		/// <returns></returns>
		public XElement GetCustomerInfo()
		{
			XElement custInfo = new XElement("CustomerInfo");

			custInfo.AddNewXAttribute("custIDExternal",this.LabCustomerID);
			custInfo.AddNewXAttribute("orderDate", DateTime.Today.ToString("MMDDYYYY"));
			custInfo.AddNewXAttribute("orderIDExternal", "");
			custInfo.AddNewXAttribute("orderSourceFK", "3");
			custInfo.AddNewXAttribute("billingFirstName", this.OrganizationContactName);
			custInfo.AddNewXAttribute("billingLastName", "");
			custInfo.AddNewXAttribute("billingAddress", this.OrganizationAddressLine1);
			custInfo.AddNewXAttribute("billingCity", this.OrganizationCity);
			custInfo.AddNewXAttribute("billingState", this.OrganizationStateCode);
			custInfo.AddNewXAttribute("billingZip", this.OrganizationZipCode);
			custInfo.AddNewXAttribute("billingCountry", "US");
			custInfo.AddNewXAttribute("shippingFirstName", this.OrganizationContactName);
			custInfo.AddNewXAttribute("shippingLastName", "");
			custInfo.AddNewXAttribute("shippingAddress", this.OrganizationShippingAddressLine1);
			custInfo.AddNewXAttribute("shippingCity", this.OrganizationShippingCity);
			custInfo.AddNewXAttribute("shippingState", this.OrganizationShippingStateCode);
			custInfo.AddNewXAttribute("shippingZip", this.OrganizationShippingZipCode);
			custInfo.AddNewXAttribute("shippingCountry", "US");
			custInfo.AddNewXAttribute("email", this.OrganizationContactEmail);
			custInfo.AddNewXAttribute("phone", this.OrganizationContactPhone);
			custInfo.AddNewXAttribute("notes", "");

			return custInfo;
		}

		public static implicit operator Organization(Control_Organization sourceOrganization)
		{
			Organization retVal = new Organization();

			retVal.OrganizationGuid = sourceOrganization.OrganizationGuid;
			retVal.OrganizationName = sourceOrganization.OrganizationName;
            retVal.OrganizationAddressLine1 = sourceOrganization.OrganizationAddressLine1;
            retVal.OrganizationAddressLine2 = sourceOrganization.OrganizationAddressLine2;
			retVal.OrganizationCity = sourceOrganization.OrganizationCity;
			retVal.OrganizationStateCode = sourceOrganization.OrganizationStateCode;
            retVal.OrganizationZipCode = sourceOrganization.OrganizationZipCode;
            retVal.OrganizationContactName = sourceOrganization.OrganizationContactName;
            retVal.OrganizationContactEmail = sourceOrganization.OrganizationContactEmail;
            retVal.OrganizationContactPhone = sourceOrganization.OrganizationContactPhone;
			retVal.OrganizationTypeID = sourceOrganization.OrganizationTypeID;
			retVal.DateCreated = sourceOrganization.DateCreated;
			retVal.DateModified = sourceOrganization.DateModified;
            retVal.LabCustomerID = sourceOrganization.LabCustomerID;
            retVal.OrganizationShippingAddressLine1 = sourceOrganization.OrganizationShippingAddressLine1;
            retVal.OrganizationShippingAddressLine2 = sourceOrganization.OrganizationShippingAddressLine2;
            retVal.OrganizationShippingCity = sourceOrganization.OrganizationShippingCity;
            retVal.OrganizationShippingStateCode = sourceOrganization.OrganizationShippingStateCode;
            retVal.OrganizationShippingZipCode = sourceOrganization.OrganizationShippingZipCode;
            
			return retVal;
		}

		public static implicit operator Control_Organization(Organization sourceOrganization)
		{
			Control_Organization retVal = new Control_Organization();

            retVal.OrganizationGuid = sourceOrganization.OrganizationGuid;
            retVal.OrganizationName = sourceOrganization.OrganizationName;
            retVal.OrganizationAddressLine1 = sourceOrganization.OrganizationAddressLine1;
            retVal.OrganizationAddressLine2 = sourceOrganization.OrganizationAddressLine2;
            retVal.OrganizationCity = sourceOrganization.OrganizationCity;
            retVal.OrganizationStateCode = sourceOrganization.OrganizationStateCode;
            retVal.OrganizationZipCode = sourceOrganization.OrganizationZipCode;
            retVal.OrganizationContactName = sourceOrganization.OrganizationContactName;
            retVal.OrganizationContactEmail = sourceOrganization.OrganizationContactEmail;
            retVal.OrganizationContactPhone = sourceOrganization.OrganizationContactPhone;
            retVal.OrganizationTypeID = sourceOrganization.OrganizationTypeID;
            retVal.DateCreated = sourceOrganization.DateCreated;
            retVal.DateModified = sourceOrganization.DateModified;
            retVal.LabCustomerID = sourceOrganization.LabCustomerID;
            retVal.OrganizationShippingAddressLine1 = sourceOrganization.OrganizationShippingAddressLine1;
            retVal.OrganizationShippingAddressLine2 = sourceOrganization.OrganizationShippingAddressLine2;
            retVal.OrganizationShippingCity = sourceOrganization.OrganizationShippingCity;
            retVal.OrganizationShippingStateCode = sourceOrganization.OrganizationShippingStateCode;
            retVal.OrganizationShippingZipCode = sourceOrganization.OrganizationShippingZipCode;

			return retVal;
		}	
	}
}
