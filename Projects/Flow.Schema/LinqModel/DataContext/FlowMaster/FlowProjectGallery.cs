﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.Linq;
using System.Data.SqlServerCe;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using System.Xml;
using System.Xml.Linq;

using Flow.Lib;
using Flow.Lib.Database.SqlCe;
using Flow.Lib.Helpers;
using Flow.Lib.ImageQuix;
using Flow.Lib.Web;
using Flow.Lib.Net;
using Flow.Schema.Properties;
using Flow.Schema.Reports;
using Flow.Schema.Utility;

using ICSharpCode.SharpZipLib.Core;

using StructureMap;
using NetServ.Net.Json;
using System.Windows.Threading;
using Flow.Schema.LinqModel.DataContext.FlowMaster;


namespace Flow.Schema.LinqModel.DataContext
{

    partial class FlowProjectGallery
    {

        //private int _newOnlineOrders = 0;
        //public int NewOnlineOrders
        //{
        //    get { return _newOnlineOrders; }
        //    set
        //    {
        //        _newOnlineOrders = value;
        //        SendPropertyChanged("ShowGetOrderButton");
        //        SendPropertyChanged("NewOnlineOrders");
        //        SendPropertyChanged("ShowDoneLabel");
        //    }
        //}

        //public List<IQOrder> newOrders { get; set; }


        //private int _yearbookPoseCount = 0;
        //public int YearbookPoseCount
        //{
        //    get { return _yearbookPoseCount; }
        //    set
        //    {
        //        _yearbookPoseCount = value;
        //        SendPropertyChanged("ShowGetOrderButton");
        //        SendPropertyChanged("NewOnlineOrders");
        //        SendPropertyChanged("ShowDoneLabel");
        //    }
        //}

        //public List<string> YBPoseList1 { get; set; }
        //public List<string> YBPoseList2 { get; set; }

        //private bool _showGetOrderButton = false;
        //public bool ShowGetOrderButton
        //{
        //    get
        //    {
        //        if (NewOnlineOrders + YearbookPoseCount > 0)
        //            _showGetOrderButton = true;
        //        else
        //            _showGetOrderButton = false;
        //        return _showGetOrderButton;
        //    }
        //    set
        //    {
        //        _showGetOrderButton = value;
        //        SendPropertyChanged("ShowGetOrderButton");
        //        SendPropertyChanged("ShowDoneLabel");
        //    }
        //}

        //private bool _showDoneLabel = false;
        //public bool ShowDoneLabel
        //{
        //    get
        //    {
        //        return _showDoneLabel;
        //    }
        //    set
        //    {
        //        _showDoneLabel = value;
        //        SendPropertyChanged("ShowDoneLabel");
        //    }
        //}

        //private bool _showBlankLabel = true;

        //partial void OnPickupShipChanged()
        //{
        //    if (PickupShip == true)
        //    {
        //        DirectShip = false;
        //        CustomerChoiceShip = false;
        //    }
        //}

        //partial void OnDirectShipChanged()
        //{
        //    if (DirectShip == true)
        //    {
        //        PickupShip = false;
        //        CustomerChoiceShip = false;
        //    }
        //}

        //partial void OnCustomerChoiceShipChanged()
        //{
        //    if (CustomerChoiceShip == true)
        //    {
        //        PickupShip = false;
        //        DirectShip = false;
        //    }
        //}

        //public bool ShowBlankLabel
        //{
        //    get
        //    {
        //        if (ShowDoneLabel == false && ShowGetOrderButton == false)
        //            _showBlankLabel = true;
        //        else
        //            _showBlankLabel = false;
        //        return _showBlankLabel;
        //    }
        //}
      
        //public FlowProjectGallery(Guid flowProjectGuid, string galleryName)
        //{
        //    // TODO: Complete member initialization
        //    this.FlowProjectGuid = flowProjectGuid;
        //    this.GalleryName = galleryName;
        //}

        //public void UpdateWelcomeImage()
        //{
        //    this.SendPropertyChanged("WelcomeImage");
        //}
    }
		
}	// END namespace
