﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.Linq;
using System.Data.SqlServerCe;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Data;
using System.Windows.Media.Imaging;

using Flow.Lib;
using Flow.Lib.Database.SqlCe;
using Flow.Lib.Helpers;
using Flow.Schema.Properties;
using Flow.Schema.Utility;


namespace Flow.Schema.LinqModel.DataContext
{
	partial class ProjectTemplate
	{

		#region FIELDS AND PROPERTIES

		/// <summary>
		/// FlowObservableCollection for generated ProjectTemplateEventTrigger Linq-to-SQL enti
		/// </summary>
		private FlowObservableCollection<ProjectTemplateEventTrigger> _projectTemplateEventTriggerList = null;
		public FlowObservableCollection<ProjectTemplateEventTrigger> ProjectTemplateEventTriggerList
		{
			get
			{
				if (_projectTemplateEventTriggerList == null)
				{
                    
					_projectTemplateEventTriggerList = new FlowObservableCollection<ProjectTemplateEventTrigger>(this.ProjectTemplateEventTriggers);
					//_projectTemplateEventTriggerList.View.GroupDescriptions
					//    .Add(new PropertyGroupDescription("EventTriggerTypeID"));

                    foreach (ProjectTemplateEventTrigger projectTemplateEventTrigger in _projectTemplateEventTriggerList)
                    {
                        EventTrigger eventTrigger = this.FlowMasterDataContext.EventTriggers.FirstOrDefault(et => et.EventTriggerID == projectTemplateEventTrigger.EventTriggerID);

                        EventTriggerType eventTriggerType =
                        this.FlowMasterDataContext.EventTriggerTypes
                            .FirstOrDefault(ett => ett.EventTriggerTypeID == eventTrigger.EventTriggerTypeID);

                        projectTemplateEventTrigger.SetParent(eventTriggerType, eventTrigger);
                    }


				}

				return _projectTemplateEventTriggerList;
			}
		}
        public void UpdateProjectTemplateTriggers()
        {
            this.SendPropertyChanged("ProjectTemplateEventTriggerList");
        }
		/// <summary>
		/// FlowObservableCollection for generated ProjectTemplateSubjectField Linq-to-SQL entity
		/// </summary>
		private FlowObservableCollection<ProjectTemplateSubjectField> _projectTemplateSubjectFieldList = null;
		public FlowObservableCollection<ProjectTemplateSubjectField> ProjectTemplateSubjectFieldList
		{
			get
			{
				if (_projectTemplateSubjectFieldList == null)
				{
					_projectTemplateSubjectFieldList = new FlowObservableCollection<ProjectTemplateSubjectField>(this.ProjectTemplateSubjectFields);
					_projectTemplateSubjectFieldList.RemovingItem += new EventHandler<EventArgs<ProjectTemplateSubjectField>>(ProjectTemplateSubjectFieldList_RemovingItem);
				}

				return _projectTemplateSubjectFieldList;
			}
		}

		void ProjectTemplateSubjectFieldList_RemovingItem(object sender, EventArgs<ProjectTemplateSubjectField> e)
		{
			if (this.FlowMasterDataContext.ProjectTemplateSubjectFields.Contains(e.Data))
				this.FlowMasterDataContext.ProjectTemplateSubjectFields.DeleteOnSubmit(e.Data);
		}

		/// <summary>
		/// FlowObservableCollection for generated ProjectTemplateImageType Linq-to-SQL entity
		/// </summary>
		private FlowObservableCollection<ProjectTemplateImageType> _projectTemplateImageTypeList = null;
		public FlowObservableCollection<ProjectTemplateImageType> ProjectTemplateImageTypeList
		{
			get
			{
				if (_projectTemplateImageTypeList == null)
					_projectTemplateImageTypeList = new FlowObservableCollection<ProjectTemplateImageType>(this.ProjectTemplateImageTypes);

				return _projectTemplateImageTypeList;
			}
		}

		/// <summary>
		/// Represents a hierarchical organizational structure to be applied to inheriting projects
		/// </summary>
		private ProjectTemplateOrganizationalUnitType _organizationalStructure = null;
		public ProjectTemplateOrganizationalUnitType OrganizationalStructure
		{
			get
			{
				// If an organizational structure is defined
				if (this.ProjectTemplateOrganizationalUnitTypes.Count > 0)
				{
					// Select the root unit type
					_organizationalStructure = this.ProjectTemplateOrganizationalUnitTypes
						.First(ou => !ou.ProjectTemplateOrganizationalUnitTypeParentID.HasValue);

					// Populate the hierarchy
					_organizationalStructure.SetChildren(this.ProjectTemplateOrganizationalUnitTypes);
				}

				return _organizationalStructure;
			}
		}

		private FlowMasterDataContext _flowMasterDataContext = null;
		public FlowMasterDataContext FlowMasterDataContext
		{
			get { return _flowMasterDataContext; }
			set
			{
				_flowMasterDataContext = value;
				this.SynchEventTriggers();
			}
		}

		/// <summary>
		/// Indicates that the ProjectTemplate is provisional (has not yet been saved)
		/// </summary>
		public bool IsNew { get { return this.ProjectTemplateID == 0; } }

		/// <summary>
		/// The product program assigned to the project template (if any)
		/// </summary>
		private FlowCatalog _flowCatalog = null;
		public FlowCatalog FlowCatalog
		{
			get
			{
				if (_flowCatalog == null)
					_flowCatalog = _flowMasterDataContext.FlowCatalogs.SingleOrDefault(c => (int?)c.FlowCatalogID == this.FlowCatalogID);

				return _flowCatalog;
			}

			set
			{
				_flowCatalog = value;
				this.FlowCatalogID = _flowCatalog.FlowCatalogID;
			}
		}

		/// <summary>
		/// Displays the barcode scan intialization sequence, with special characters replaced with mnemonics
		/// </summary>
		public string BarcodeScanInitSequenceDisplay
		{
			get
			{
				return (this.BarcodeScanInitSequence == null)
					? null
					: this.BarcodeScanInitSequence.BarcodeEscape();
			}
		}

		partial void OnBarcodeScanInitSequenceChanged()
		{
			this.SendPropertyChanged("BarcodeScanInitSequenceDisplay");
		}

		/// <summary>
		/// Displays a textual description of the barcode value pattern
		/// </summary>
		public string BarcodeScanValueLengthDisplay
		{
			get
			{
				return (this.BarcodeScanValueLength.HasValue)
					? this.BarcodeScanValueLength.ToString() + "-character alphanumeric value"
					: null;
			}
		}

		partial void OnBarcodeScanValueLengthChanged()
		{
			this.SendPropertyChanged("BarcodeScanValueLengthDisplay");
		}

		/// <summary>
		/// Displays the barcode scan termination sequence, with special characters replaced with mnemonics
		/// </summary>
		public string BarcodeScanTermSequenceDisplay
		{
			get
			{
				return (this.BarcodeScanTermSequence == null)
					? null
					: this.BarcodeScanTermSequence.BarcodeEscape();
			}
		}

		partial void OnBarcodeScanTermSequenceChanged()
		{
			this.SendPropertyChanged("BarcodeScanTermSequenceDisplay");
		}

		#endregion


		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="flowMasterDataContext"></param>
		public ProjectTemplate(FlowMasterDataContext flowMasterDataContext)
			: this()
		{
			this.FlowMasterDataContext = flowMasterDataContext;
		}


		#region METHODS

		/// <summary>
		/// Ensures that the Active value for the ProjectTemplateEventTrigger is assigned
		/// the Active value of the related EventTrigger (which is bound to the representative UI control)
		/// </summary>
		/// <param name="flowMasterDataContext"></param>
		private void SynchEventTriggers()
		{
			// Iterate through the EventTriggers
			foreach (EventTrigger eventTrigger in this.FlowMasterDataContext.EventTriggers)
			{
				EventTriggerType eventTriggerType =
					this.FlowMasterDataContext.EventTriggerTypes
						.FirstOrDefault(ett => ett.EventTriggerTypeID == eventTrigger.EventTriggerTypeID);

				ProjectTemplateEventTrigger projectTemplateEventTrigger =
					this.ProjectTemplateEventTriggerList
						.FirstOrDefault(p => p.EventTriggerID == eventTrigger.EventTriggerID);

				if (projectTemplateEventTrigger == null)
				{
					projectTemplateEventTrigger = (ProjectTemplateEventTrigger)eventTrigger;
                    this.FlowMasterDataContext.ProjectTemplateEventTriggers.InsertOnSubmit(projectTemplateEventTrigger);
					this.ProjectTemplateEventTriggerList.Add(projectTemplateEventTrigger);
				}

				projectTemplateEventTrigger.SetParent(eventTriggerType, eventTrigger);
			}

			this.ProjectTemplateEventTriggerList.View.GroupDescriptions.Add(new PropertyGroupDescription("EventTriggerTypeDesc"));
            
		}

		public ProjectTemplateEventTrigger GetEventTrigger(string eventTriggerDesc)
		{
			return this.ProjectTemplateEventTriggerList.Join(
				this.FlowMasterDataContext.EventTriggers.Where(e => e.EventTriggerDesc == eventTriggerDesc),
				p => p.EventTriggerID,
				e => e.EventTriggerID,
				(p,e) => p

			).FirstOrDefault();
		}

		/// <summary>
		/// Resets the ProjectTemplate lists (clearing UI bindings)
		/// </summary>
		public void Revert()
		{
			_projectTemplateSubjectFieldList = null;
			_projectTemplateImageTypeList = null;
		}

		/// <summary>
		/// Sets image filename formats to default values
		/// </summary>
		public void ClearFilenameFormatFields()
		{
			this.ImageFormatTypeID = 1;
			this.ImageFormatSeparatorChar = null;
			this.ImageFormatField1 = null;
			this.ImageFormatField2 = null;
			this.ImageFormatField3 = null;
		}

		#endregion // METHODS



        public void UpdateImageOptions()
        {
            
            this.SendPropertyChanged("ProjectTemplateImageTypeList");
        }
    }	// END class

}	// END namespace
