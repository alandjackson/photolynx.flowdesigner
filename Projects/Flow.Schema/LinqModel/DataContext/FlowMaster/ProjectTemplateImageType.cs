﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.Linq;
using System.Data.SqlServerCe;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Media.Imaging;

using Flow.Lib;
using Flow.Lib.Database.SqlCe;
using Flow.Lib.Helpers;
using Flow.Schema.Properties;
using Flow.Schema.Utility;


namespace Flow.Schema.LinqModel.DataContext
{
	/// <summary>
	/// Represents a photographic image "type", such as a "yearbook pose"
	/// </summary>
	partial class ProjectTemplateImageType
	{
		/// <summary>
		/// Creates a new ProjectTemplateImageType item from an existing ImageType item
		/// </summary>
		/// <param name="projectTemplateID"></param>
		/// <param name="imageType"></param>
		public ProjectTemplateImageType(int projectTemplateID, ImageType imageType)
		{
			this.ProjectTemplateID = projectTemplateID;
			this.ProjectTemplateImageTypeDesc = imageType.ImageTypeDesc;
			this.IsExclusive = imageType.IsExclusive;
		}

		public string ImageTypeDesc { get { return this.ProjectTemplateImageTypeDesc; } }
	}
}
