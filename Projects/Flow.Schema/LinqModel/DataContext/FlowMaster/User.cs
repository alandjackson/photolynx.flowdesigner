﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Collections.Specialized;

namespace Flow.Schema.LinqModel.DataContext
{
	/// <summary>
	/// Represents an application user
	/// </summary>
	partial class User
	{
        //flags
        enum FLAG
        {
            photographer = 0,
            administrator = 1,
            
        }

        public Boolean IsAdministrator
        {
            
            get {

                BitArray bits = new BitArray(new int[]{this.UserFlags});
                return bits.Get((int)FLAG.administrator);    
            }
            set {
                BitArray bits = new BitArray(new int[]{this.UserFlags});
                bits[(int)FLAG.administrator] = value;
                int[] array = new int[1];
                bits.CopyTo(array, 0);
                this.UserFlags = array[0];   
            }
        }

        public Boolean IsPhotographer
        {
            get
            {

                BitArray bits = new BitArray(new int[] { this.UserFlags });
                return bits.Get((int)FLAG.photographer);
            }
            set
            {
                BitArray bits = new BitArray(new int[] { this.UserFlags });
                bits[(int)FLAG.photographer] = value;
                int[] array = new int[1];
                bits.CopyTo(array, 0);
                this.UserFlags = array[0]; 
            }
        }

        public string FormalFullName
        {
            get { return this.UserLastName + ", " + this.UserFirstName; }
        }


        public User  clone()
        {
            User sourceUser = this;
            User retVal = new User();

            retVal.UserGuid = sourceUser.UserGuid;
            //			retVal.UserTypeID = sourceUser.UserTypeID;
            retVal.UserLastName = sourceUser.UserLastName;
            retVal.UserFirstName = sourceUser.UserFirstName;
            retVal.UserAddressLine1 = sourceUser.UserAddressLine1;
            retVal.UserAddressLine2 = sourceUser.UserAddressLine2;
            retVal.UserCity = sourceUser.UserCity;
            retVal.UserStateCode = sourceUser.UserStateCode;
            retVal.UserZipCode = sourceUser.UserZipCode;
            retVal.UserZipPlusFour = sourceUser.UserZipPlusFour;
            retVal.UserPhone = sourceUser.UserPhone;
            retVal.UserMobile = sourceUser.UserMobile;
            retVal.UserEmail = sourceUser.UserEmail;
            //retVal.UserFlags = sourceUser.UserFlags;
            return retVal;
        }

		public static implicit operator User(Control_User sourceUser)
		{
            if (sourceUser == null) return null;

			User retVal = new User();

			retVal.UserGuid = sourceUser.UserGuid;
//			retVal.UserTypeID = sourceUser.UserTypeID;
			retVal.UserLastName = sourceUser.UserLastName;
			retVal.UserFirstName = sourceUser.UserFirstName;
			retVal.UserAddressLine1 = sourceUser.UserAddressLine1;
			retVal.UserAddressLine2 = sourceUser.UserAddressLine2;
			retVal.UserCity = sourceUser.UserCity;
			retVal.UserStateCode = sourceUser.UserStateCode;
			retVal.UserZipCode = sourceUser.UserZipCode;
			retVal.UserZipPlusFour = sourceUser.UserZipPlusFour;
			retVal.UserPhone = sourceUser.UserPhone;
			retVal.UserMobile = sourceUser.UserMobile;
			retVal.UserEmail = sourceUser.UserEmail;
            //retVal.UserFlags = sourceUser.UserFlags;
			return retVal;
		}

		public static implicit operator Control_User(User sourceUser)
		{
			Control_User retVal = new Control_User();
            if (sourceUser == null) return retVal;

			retVal.UserGuid = sourceUser.UserGuid;
//			retVal.UserTypeID = sourceUser.UserTypeID;
			retVal.UserLastName = sourceUser.UserLastName;
			retVal.UserFirstName = sourceUser.UserFirstName;
			retVal.UserAddressLine1 = sourceUser.UserAddressLine1;
			retVal.UserAddressLine2 = sourceUser.UserAddressLine2;
			retVal.UserCity = sourceUser.UserCity;
			retVal.UserStateCode = sourceUser.UserStateCode;
			retVal.UserZipCode = sourceUser.UserZipCode;
			retVal.UserZipPlusFour = sourceUser.UserZipPlusFour;
			retVal.UserPhone = sourceUser.UserPhone;
			retVal.UserMobile = sourceUser.UserMobile;
			retVal.UserEmail = sourceUser.UserEmail;
            //retVal.UserFlag = sourceUser.UserFlags;

			return retVal;
		}

      
    }
}
