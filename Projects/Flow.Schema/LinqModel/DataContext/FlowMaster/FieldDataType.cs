﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.Linq;
using System.Data.SqlServerCe;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Media.Imaging;

using Flow.Lib;
using Flow.Lib.Database.SqlCe;
using Flow.Lib.Helpers;
using Flow.Schema.Properties;
using Flow.Schema.Utility;


namespace Flow.Schema.LinqModel.DataContext
{
	/// <summary>
	/// Represents a data type available for assignment to user-defined Subject fields
	/// </summary>
	partial class FieldDataType
	{
		public const int FIELD_DATA_TYPE_STRING = 1;
		public const int FIELD_DATA_TYPE_INTEGER = 2;
		public const int FIELD_DATA_TYPE_DECIMAL = 3;
		public const int FIELD_DATA_TYPE_DATETIME = 4;
		public const int FIELD_DATA_TYPE_MONEY = 5;
		public const int FIELD_DATA_TYPE_BOOLEAN = 6;

		/// <summary>
		/// Represents the system Type represented by the FieldDataType ID value
		/// </summary>
		/// <param name="fieldDataTypeID"></param>
		/// <returns></returns>
		private static Type GetNativeTypeFromFieldDataTypeID(int fieldDataTypeID)
		{
			switch (fieldDataTypeID)
			{
				case FIELD_DATA_TYPE_STRING:
					return typeof(string);
				case FIELD_DATA_TYPE_INTEGER:
					return typeof(int);
				case FIELD_DATA_TYPE_DECIMAL:
				case FIELD_DATA_TYPE_MONEY:
					return typeof(decimal);
				case FIELD_DATA_TYPE_DATETIME:
					return typeof(DateTime);
				case FIELD_DATA_TYPE_BOOLEAN:
					return typeof(bool);
				default:
					return typeof(string);
			}
		}

		/// <summary>
		/// Returns the system Type represented by the object
		/// </summary>
		private Type _nativeType = null;
		public Type NativeType
		{
			get
			{
				if (_nativeType == null)
					_nativeType = FieldDataType.GetNativeTypeFromFieldDataTypeID(this.FieldDataTypeID);

				return _nativeType;
			}
		}

		public bool IsString { get { return this.FieldDataTypeID == FIELD_DATA_TYPE_STRING; } }
		public bool IsInteger { get { return this.FieldDataTypeID == FIELD_DATA_TYPE_INTEGER; } }
		public bool IsDecimal { get { return this.FieldDataTypeID == FIELD_DATA_TYPE_DECIMAL; } }
		public bool IsMoney { get { return this.FieldDataTypeID == FIELD_DATA_TYPE_MONEY; } }
		public bool IsDateTime { get { return this.FieldDataTypeID == FIELD_DATA_TYPE_DATETIME; } }
		public bool IsBoolean { get { return this.FieldDataTypeID == FIELD_DATA_TYPE_BOOLEAN; } }


		/// <summary>
		/// Indicates if the Type requires quoted identifiers for database operations
		/// </summary>
		public bool RequiresQuoteQualifier
		{
			get { return this.NativeType == typeof(string) || this.NativeType == typeof(DateTime); }
		}

		#region IMPLICIT CONVERSIONS

		public static implicit operator FieldDataType(Type type)
		{
			return FlowMasterDataContext.DataTypes.GetFieldDataType(dt => dt.NativeType == type);
		}

		public static implicit operator Type(FieldDataType fieldDataType)
		{
			return fieldDataType.NativeType;
		}

		public static implicit operator FieldDataType(int fieldDataTypeID)
		{
			return FlowMasterDataContext.DataTypes.GetFieldDataType(dt => dt.FieldDataTypeID == fieldDataTypeID);
		}

		public static implicit operator int(FieldDataType fieldDataType)
		{
			return fieldDataType.FieldDataTypeID;
		}

		public override bool Equals(object obj)
		{
			FieldDataType comparisonType = obj as FieldDataType;

			if (comparisonType != null)
				return this.FieldDataTypeID == comparisonType.FieldDataTypeID;

			return false;
		}

		#endregion


	}	// END class

}	// END namespace
