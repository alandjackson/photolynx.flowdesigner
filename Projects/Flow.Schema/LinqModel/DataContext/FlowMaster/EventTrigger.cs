﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.Linq;
using System.Data.SqlServerCe;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Media.Imaging;

using Flow.Lib;
using Flow.Lib.Database.SqlCe;
using Flow.Lib.Helpers;
using Flow.Schema.Properties;
using Flow.Schema.Utility;


namespace Flow.Schema.LinqModel.DataContext
{
	/// <summary>
	/// Represents a user preference which specifies an action to take when a specific event occurs
	/// </summary>
	partial class EventTrigger
	{
		public static explicit operator ProjectTemplateEventTrigger(EventTrigger item)
		{
			ProjectTemplateEventTrigger castItem = new ProjectTemplateEventTrigger();
			castItem.EventTriggerID = item.EventTriggerID;
			castItem.EventTriggerTypeID = item.EventTriggerTypeID;
			castItem.Active = item._Active;

			return castItem;
		}
	}
}
