﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.Linq;
using System.Data.SqlServerCe;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Media.Imaging;

using Flow.Lib;
using Flow.Lib.Database.SqlCe;
using Flow.Lib.Helpers;
using Flow.Schema.Properties;
using Flow.Schema.Utility;


namespace Flow.Schema.LinqModel.DataContext
{
	/// <summary>
	/// Represents a user-defined Subject field; the list of SubjectFields provides
	/// a base list for selection when defining project templates
	/// </summary>
	partial class SubjectField
	{
		/// <summary>
		/// When the field display name is set, set the desc (specifying the database table field name)
		/// by normalizing the display name
		/// </summary>
		partial void OnSubjectFieldDisplayNameChanged()
		{
			this.SubjectFieldDesc = this.SubjectFieldDisplayName.Replace(" ", "");
		}

		/// <summary>
		/// Converts a SubjectField item to a ProjectTemplateSubjectField
		/// </summary>
		/// <param name="item"></param>
		/// <returns></returns>
		public static explicit operator ProjectTemplateSubjectField(SubjectField item)
		{
			ProjectTemplateSubjectField castItem = new ProjectTemplateSubjectField();
			castItem.ProjectTemplateSubjectFieldDisplayName = item.SubjectFieldDisplayName;
			castItem.ProjectTemplateSubjectFieldDesc = item.SubjectFieldDesc;
			castItem.FieldDataTypeID = item.FieldDataTypeID;
            castItem.FieldDataSize = item.FieldDataSize;
            //if (castItem.FieldDataTypeID == 1)
            //    castItem.FieldDataSize = 512;
            //if (castItem.FieldDataTypeID == 2)
            //    castItem.FieldDataSize = 4;
			castItem.IsRequiredField = item.IsRequiredField;

			return castItem;
		}
	}
}
