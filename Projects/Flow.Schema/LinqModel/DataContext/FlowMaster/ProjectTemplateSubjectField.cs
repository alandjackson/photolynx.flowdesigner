﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.Linq;
using System.Data.SqlServerCe;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Media.Imaging;

using Flow.Lib;
using Flow.Lib.Database.SqlCe;
using Flow.Lib.Helpers;
using Flow.Schema.Properties;
using Flow.Schema.Utility;


namespace Flow.Schema.LinqModel.DataContext
{
	/// <summary>
	/// Represents user-defined Subject fields applied at the project template level
	/// </summary>
	partial class ProjectTemplateSubjectField : ISubjectField
	{

		#region FIELDS AND PROPERTIES

		/// <summary>
		/// The application datatype representation assigned to the Subject user-defined field
		/// </summary>
		public FieldDataType _subjectFieldDataType = null;
		public FieldDataType SubjectFieldDataType
		{
			get
			{
				if (_subjectFieldDataType == null)

					// Conversion through implicit operator implementation
					_subjectFieldDataType = this.FieldDataTypeID;

				return _subjectFieldDataType;
			}

			set
			{
				_subjectFieldDataType = value;
				this.FieldDataTypeID = _subjectFieldDataType.FieldDataTypeID;

				this.SendPropertyChanged("SubjectFieldDataType");
			}
		}

		/// <summary>
		/// The name of the field to be displayed in the UI
		/// </summary>
		public string SubjectFieldDisplayName
		{
			get { return this.ProjectTemplateSubjectFieldDisplayName; }
			set
			{
				this.ProjectTemplateSubjectFieldDisplayName = value.Normalize();
				this.SendPropertyChanged("SubjectFieldDisplayName");
			}

		}

		/// <summary>
		/// The field name as assigned to the source database table column
		/// </summary>
		public string SubjectFieldDesc { get { return this.ProjectTemplateSubjectFieldDesc; } }

		#endregion


		/// <summary>
		/// Creates a new ProjectTemplateSubjectField item from an existing SubjectField item
		/// </summary>
		/// <param name="ProjectTemplateID"></param>
		/// <param name="subjectField"></param>
		public ProjectTemplateSubjectField(SubjectField subjectField)
		{
			this.ProjectTemplateSubjectFieldDisplayName = subjectField.SubjectFieldDisplayName;
			this.FieldDataTypeID = subjectField.FieldDataTypeID;
            this.FieldDataSize = subjectField.FieldDataSize;
            if(this.FieldDataTypeID == 1)
                this.FieldDataSize = 256;
            //if (this.FieldDataTypeID == 2)
            //    this.FieldDataSize = 4;

			this.IsSystemField = subjectField.IsSystemField;
			this.IsRequiredField = subjectField.IsRequiredField;
			this.IsScanKeyField = subjectField.IsScanKeyField;
			this.IsProminentField = subjectField.IsProminentField;
			this.IsSearchableField = subjectField.IsSearchableField;
		}

		/// <summary>
		/// Creates a new ProjectTemplateSubjectField item from an existing SubjectField item
		/// </summary>
		/// <param name="ProjectTemplateID"></param>
		/// <param name="subjectField"></param>
		public ProjectTemplateSubjectField(int projectTemplateID, SubjectField subjectField)
		{
			this.ProjectTemplateID = projectTemplateID;
            this.FieldDataSize = subjectField.FieldDataSize;
            //if (this.FieldDataTypeID == 1)
            //    this.FieldDataSize = 512;
            //if (this.FieldDataTypeID == 2)
            //    this.FieldDataSize = 4;
			this.ProjectTemplateSubjectFieldDisplayName = subjectField.SubjectFieldDisplayName;
			this.FieldDataTypeID = subjectField.FieldDataTypeID;
			this.IsSystemField = subjectField.IsSystemField;
			this.IsRequiredField = subjectField.IsRequiredField;
			this.IsScanKeyField = subjectField.IsScanKeyField;
			this.IsProminentField = subjectField.IsProminentField;
			this.IsSearchableField = subjectField.IsSearchableField;
		}

		/// <summary>
		/// When the field display name is set, set the desc (specifying the database table field name)
		/// by normalizing the display name
		/// </summary>
		partial void OnProjectTemplateSubjectFieldDisplayNameChanged()
		{
			this.ProjectTemplateSubjectFieldDesc = this.ProjectTemplateSubjectFieldDisplayName.Replace(" ", "");
		}


		/// <summary>
		/// Invoked when the IsKeyField property is changed, ensuring that the IsRequiredField
		/// property is set true if IsKeyField is true
		/// </summary>
		partial void OnIsKeyFieldChanged()
		{
			if (this.IsKeyField)
				this.IsRequiredField = true;
		}

		/// <summary>
		/// Invoked when the IsRequiredField property is changed; ensuring that IsKeyField
		/// property is set to false if IsRequiredField is false and that IsProminentField
		/// is true if IsRequiredField is true
		/// </summary>
		partial void OnIsRequiredFieldChanged()
		{
			if (this.IsRequiredField)
				this.IsProminentField = true;
			else
				this.IsKeyField = false;
		}

		/// <summary>
		/// Invoked when the IsProminentField property is changed, ensuring that IsRequired and
		/// IsKeyField values are false if IsProminentField is false
		/// </summary>
		partial void OnIsProminentFieldChanged()
		{
			if (!this.IsProminentField)
				this.IsRequiredField = false;
		}

		/// <summary>
		/// Return an ISubjectField representation of the object
		/// </summary>
		/// <returns></returns>
		public ISubjectField Clone()
		{
			return new ProjectSubjectField(
				this.ProjectTemplateSubjectFieldDisplayName,
				this.SubjectFieldDataType,
                this.FieldDataSize,
				this.IsSystemField,
				this.IsKeyField,
				this.IsRequiredField,
				this.IsScanKeyField,
				this.IsProminentField,
				this.IsSearchableField,
                this.IsShownInEdit,
                this.IsReadOnly
			);
		}	
	}
}
