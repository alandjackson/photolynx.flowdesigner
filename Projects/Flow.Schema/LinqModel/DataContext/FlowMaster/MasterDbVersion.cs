﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flow.Schema.LinqModel.DataContext
{
	partial class MasterDbVersion : IDbVersion
	{
		public MasterDbVersion(MasterDbVersion source)
		{
			this.DbVersionGuid = source.DbVersionGuid;
			this.DbVersionNumber = source.DbVersionNumber;
			this.DbVersionDate = source.DbVersionDate;
			this.DbVersionComments = source.DbVersionComments;
			this.ChangeSet = source.ChangeSet;
		}
	}
}
