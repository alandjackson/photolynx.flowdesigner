﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;

namespace Flow.Schema.LinqModel.DataContext
{
	partial class ProductPackage
	{

        public bool HasProducts
        {
            get { if (this._ProductPackageCompositions.Count > 0)return true; else return false; }
        }
        public bool HasNoProducts
        {
            get { if (this._ProductPackageCompositions.Count > 0)return false; else return true; }
        }

        public bool IsNotALaCarte
        {
            get { return !IsALaCarte; }
        }

		private FlowObservableCollection<ProductPackageComposition> _productPackageCompositionList = null;
		public FlowObservableCollection<ProductPackageComposition> ProductPackageCompositionList
		{
			get
			{
				if (_productPackageCompositionList == null)
					_productPackageCompositionList = new FlowObservableCollection<ProductPackageComposition>(this.ProductPackageCompositions);

				return _productPackageCompositionList;
			}
		}

        public string DigitalDownloadMessage
        {
            get
            {
                if (this.DigitalDownload.StartsWith("400") || this.DigitalDownload.StartsWith("800") || this.DigitalDownload.StartsWith("1200") )
                    return "* This package includes a " + this.DigitalDownload + " digital download";
                return "";
            }
        }

		public ProductPackage(FlowProjectDataContext importProjectDataContext, Control_ProductPackage sourcePackage, ImageQuixCatalog sourceImageQuixCatalog)
			: this()
		{
			this.ProductPackageGuid = sourcePackage.ProductPackageGuid;
			this.ProductPackageDesc = sourcePackage.ProductPackageDesc;
			this.ProductPackageTitle = sourcePackage.ProductPackageTitle;
			this.ProductPackageCode = sourcePackage.ProductPackageCode;
			this.Price = sourcePackage.Price;
			this.Cost = sourcePackage.Cost;
			this.Taxed = sourcePackage.Taxed;
			this.Special = sourcePackage.Special;
			this.Map = sourcePackage.Map;
            this.ShowInOnlinePricesheet = sourcePackage.ShowInOnlinePricesheet;
            this.DigitalDownload = sourcePackage.DigitalDownload;
            this.IsAnyXPackage = sourcePackage.IsAnyXPackage;
            this.MaxProductsAllowed = sourcePackage.MaxProductsAllowed;
            this.IsALaCarte = sourcePackage.IsALaCarte;
            this.ImageURL = sourcePackage.ImageURL;

			foreach (Control_ProductPackageComposition sourceComposition in importProjectDataContext
				.Control_ProductPackageCompositions.Where(c => c.ProductPackageGuid == sourcePackage.ProductPackageGuid)
			)
			{
				ImageQuixProduct product = sourceImageQuixCatalog.ImageQuixProducts.FirstOrDefault(p => p.PrimaryKey == sourceComposition.ImageQuixProductPk);

				if (product == null)
				{
					// Error
				}
				else
				{
					ProductPackageComposition composition = new ProductPackageComposition();

					composition.ImageQuixProductID = product.ImageQuixProductID;
					composition.Quantity = sourceComposition.Quantity;
                    composition.Layout = sourceComposition.Layout;
                    composition.PLICID = sourceComposition.PLICID;
					this.ProductPackageCompositions.Add(composition);
				}
			}
		}


        public ProductPackage(ProductPackage sourcePackage)
            : this()
        {
            this.ProductPackageGuid = Guid.NewGuid();
            this.ProductPackageDesc = sourcePackage.ProductPackageDesc;
            this.ProductPackageTitle = sourcePackage.ProductPackageTitle;
            this.ProductPackageCode = sourcePackage.ProductPackageCode;
            this.Price = sourcePackage.Price;
            this.Cost = sourcePackage.Cost;
            this.Taxed = sourcePackage.Taxed;
            this.Special = sourcePackage.Special;
            this.Map = sourcePackage.Map;
            this.ShowInOnlinePricesheet = sourcePackage.ShowInOnlinePricesheet;
            this.DigitalDownload = sourcePackage.DigitalDownload;
            this.IsAnyXPackage = sourcePackage.IsAnyXPackage;
            this.MaxProductsAllowed = sourcePackage.MaxProductsAllowed;
            this.IsALaCarte = sourcePackage.IsALaCarte;
            this.ImageURL = sourcePackage.ImageURL;

            foreach (ProductPackageComposition sourceComposition in sourcePackage.ProductPackageCompositions
            )
            {
                ImageQuixProduct product = sourceComposition.ImageQuixProduct;

                if (product == null)
                {
                    // Error
                }
                else
                {
                    ProductPackageComposition composition = new ProductPackageComposition();

                    composition.ImageQuixProductID = product.ImageQuixProductID;
                    composition.Quantity = sourceComposition.Quantity;
                    composition.Layout = sourceComposition.Layout;
                    composition.PLICID = sourceComposition.PLICID;
                    this.ProductPackageCompositions.Add(composition);
                }
            }
        }

		partial void OnCreated()
		{
			//if (this.ProductPackageGuid == Guid.Empty)
			//{
			//    this.ProductPackageGuid = Guid.NewGuid();
			//    this.ProductPackageTitle = "";
			//    this.ProductPackageCode = "";
			//    this.Map = "";
			//}
            if (this.ProductPackageGuid == Guid.Empty)
                this.ProductPackageGuid = Guid.NewGuid();
            if(this.ProductPackageTitle == null)
                this.ProductPackageTitle = "";
            if (this.ProductPackageCode == null)
                this.ProductPackageCode = "";
			this.SendPropertyChanged("ProductPackageDesc");
			this.SendPropertyChanged("IsValid");
            this.Map = "";
            this.ShowInOnlinePricesheet = true;
            this.DigitalDownload = "None";
		}

		public bool IsValid
		{
			get
			{
				return !String.IsNullOrEmpty(this.ProductPackageDesc);
			}
		}

		//partial void OnProductPackageDescChanging(string value)
		//{
		//    if (String.IsNullOrEmpty(value))
		//        throw new SystemException("Description is a required field.");
		//}

		partial void OnProductPackageDescChanged()
		{
			this.SendPropertyChanged("IsValid");
		}

		//partial void OnMapChanging(string value)
		//{
		//    if (String.IsNullOrEmpty(value))
		//        throw new SystemException("Map is a required field.");
		//}

		//partial void OnMapChanged()
		//{
		//    this.SendPropertyChanged("IsValid");
		//}



		public void Select()
		{
			// Check for null is a temporary hack; FlowCatalog not set for items with zero compositions for
			// some reason
			if(this.FlowCatalog != null)
				this.FlowCatalog.ProductPackageList.CurrentItem = this;
		}

		public void AddItemToPackage(ImageQuixProduct item)
		{
			ProductPackageComposition composition = this.ProductPackageCompositionList.FirstOrDefault(
				c => c.ImageQuixProductID == item.ImageQuixProductID
			);
            
			if (1==1 || composition == null)
			{
				composition = new ProductPackageComposition(this, item);
				this.ProductPackageCompositionList.Add(composition);
				this.FlowCatalog.FlowMasterDataContext.SubmitChanges();
				this.UpdateCost();
			}
			else
				composition++;

		}

		public void RemoveItemFromPackage(ImageQuixProduct item)
		{
			ProductPackageComposition composition = this.ProductPackageCompositionList.FirstOrDefault(
				c => c.ImageQuixProductID == item.ImageQuixProductID
			);

			if (composition == null)
				return;

			if ((--composition).Quantity == 0)
				this.DeleteComposition(composition);
		}

		public void DeleteComposition(ProductPackageComposition composition)
		{
			this.FlowCatalog.FlowMasterDataContext.ProductPackageCompositions.DeleteOnSubmit(composition);
			this.ProductPackageCompositionList.Remove(composition);

			this.FlowCatalog.FlowMasterDataContext.SubmitChanges();
		}

		internal void ProductPackageComposition_ExpandedPriceChanged(object sender, EventArgs e)
		{
			this.UpdateCost();
		}

		private void UpdateCost()
		{
			this.Cost = this.ProductPackageCompositionList.Sum(c => c.ExpandedPrice);
		}

       
	
	}	// END class

}	// END namespace
