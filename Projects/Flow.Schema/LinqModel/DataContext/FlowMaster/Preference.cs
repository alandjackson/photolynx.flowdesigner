﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.Linq;
using System.Data.SqlServerCe;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Media.Imaging;
using System.Xml.Linq;

using Flow.Lib;
using Flow.Lib.Database.SqlCe;
using Flow.Lib.Helpers;
using Flow.Lib.Helpers.ImageQuix;
using Flow.Schema.Properties;
using Flow.Schema.Utility;

namespace Flow.Schema.LinqModel.DataContext
{
	partial class Preference
	{
		public event EventHandler<EventArgs<Preference>> PreferenceChanged = delegate { };
	
		public string PreferenceIdentifier
		{
			get
			{
				return
					this.PreferenceContext + "." +
					((this.PreferenceGroup == null) ? "" : this.PreferenceGroup + ".") +
					this.PreferenceDesc
				;
			}
		}

		public T GetValue<T>()
		{
			if (this.NativeValue == null)
				return default(T);

			return (T)this.NativeValue;
		}

		public object NativeValue
		{
			get
			{
				if (this.Value == null)
					return null;
	
				switch (this.PreferenceDataType)
				{
					case FieldDataType.FIELD_DATA_TYPE_STRING:
						return this.Value;
					
					case FieldDataType.FIELD_DATA_TYPE_INTEGER:
						return Int32.Parse(this.Value);

					case FieldDataType.FIELD_DATA_TYPE_DECIMAL:
					case FieldDataType.FIELD_DATA_TYPE_MONEY:
						return Decimal.Parse(this.Value);

					case FieldDataType.FIELD_DATA_TYPE_DATETIME:
						return DateTime.Parse(this.Value);

					case FieldDataType.FIELD_DATA_TYPE_BOOLEAN:
						return Boolean.Parse(this.Value);

					default:
						throw new Exception("Preference data type {" + this.PreferenceDataType.ToString() + "} does not exist.");
				}
			}

			set
			{
				this.Value = (value == null) ? null : value.ToString();

				//if (this.PreferenceDataType == FieldDataType.FIELD_DATA_TYPE_BOOLEAN)
				//    this.Value = value.ToString();
				//else
				//    this.Value = (value == null) ? null : (string)value;

				this.SendPropertyChanged("NativeValue");
				this.PreferenceChanged(this, new EventArgs<Preference>(this));
			}
		}

	}
}
