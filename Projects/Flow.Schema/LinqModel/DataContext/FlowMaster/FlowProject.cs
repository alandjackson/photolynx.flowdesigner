﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.Linq;
using System.Data.SqlServerCe;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using System.Xml;
using System.Xml.Linq;
using System.Web.Script.Serialization;

using Flow.Lib;
using Flow.Lib.Database.SqlCe;
using Flow.Lib.Helpers;
using Flow.Lib.ImageQuix;
using Flow.Lib.Web;
using Flow.Lib.Net;
using Flow.Schema.Properties;
using Flow.Schema.Reports;
using Flow.Schema.Utility;

using Newtonsoft.Json;

using ICSharpCode.SharpZipLib.Core;

using StructureMap;
using NetServ.Net.Json;
using System.Windows.Threading;
using Flow.Schema.LinqModel.DataContext.FlowMaster;
using NLog;


namespace Flow.Schema.LinqModel.DataContext
{
	/// <summary>
	/// Represents a Flow project; most project-level data items reside in the associated
	/// project database, which is accessed through th FlowProjectDataContext object
	/// </summary>
	partial class FlowProject
	{
        
		#region FIELDS AND PROPERTIES

        public Dispatcher MainDispatcher { get; set; }

        public bool ProblemLoadingSubjectData = false;
        public bool ProblemImportingFlowCatalog = false;

        private static Logger logger = LogManager.GetCurrentClassLogger();
        
		public event EventHandler<EventArgs<bool>> HasIncompleteProductsChanged = delegate { };
		public event EventHandler<EventArgs<bool>> HasMissingImagesChanged = delegate { };

        public Boolean NeedsCatalog { get { if (this.FlowCatalog != null) return false; else return true; } }

        private bool _queueForArchive { get; set; }
        public bool QueueForArchive { 
            get
            {
                return _queueForArchive;
            }
            set
            {
                _queueForArchive = value;
                SendPropertyChanged("QueueForArchive");
            }

        }

        partial void OnDateCreatedChanged()
        {
            DueDate = DateCreated.AddDays(7);
            SendPropertyChanged("DueDate");
        }


        //private List<String> _uniqueTeachers { get; set; }
        public List<String> UniqueTeachers
        {
            get
            {
                if (this.ProjectSubjectFieldList.Any(psf => psf.ProjectSubjectFieldDesc == "Teacher"))
                {
                    if(this.SubjectList != null)
                        return this.SubjectList.Select(s => s.SubjectData["Teacher"].Value as string).Distinct().OrderBy(s=> s).ToList<String>();

                }
                return null;
            }
        }
        public List<String> UniqueGrades
        {
            get
            {
                if (this.ProjectSubjectFieldList.Any(psf => psf.ProjectSubjectFieldDesc == "Grade"))
                {
                    if (this.SubjectList != null)
                        return this.SubjectList.Select(s => s.SubjectData["Grade"].Value as string).Distinct().OrderBy(s => s).ToList<String>();

                }
                return null;
            }
        }
        public List<String> UniqueClasses
        {
            get
            {
                if (this.ProjectSubjectFieldList.Any(psf => psf.ProjectSubjectFieldDesc == "Class"))
                {
                    if (this.SubjectList != null)
                        return this.SubjectList.Select(s => s.SubjectData["Class"].Value as string).Distinct().OrderBy(s => s).ToList<String>();

                }
                return null;
            }
        }

        private List<String> _selectedGroupImageFlags { get; set; }
        public List<String> SelectedGroupImageFlags
        {
            get
            {
                if (_selectedGroupImageFlags == null)
                {
                    _selectedGroupImageFlags = new List<string>();
                    _selectedGroupImageFlags.Add("");
                    if (!String.IsNullOrEmpty(this.GroupImageFlags))
                    {
                        foreach (string s in this.GroupImageFlags.Split(";"))
                        {
                            if(s.Length>0)
                            _selectedGroupImageFlags.Add(s);
                        }
                    }
                }
                return _selectedGroupImageFlags;
            }
        }

        public List<String> SelectedGroupImageFlagsNoBlank
        {
            get
            {
                //_selectedGroupImageFlags = null;
                List<string> tempList = new List<string>(SelectedGroupImageFlags);
                while (tempList.Count > 0 && tempList[0] == "")
                { tempList.RemoveAt(0); }
                return tempList;
            }
        }

        public void UpdateSelectedGroupImageFlags()
        {
            SendPropertyChanged("SelectedGroupImageFlags");
            SendPropertyChanged("SelectedGroupImageFlagsNoBlank");

        }


        static readonly object locker = new object();

        public enum ShipMethods
        {
            Direct = 2,
            Studio = 0,
            School = 1,
        }

        
        private Dictionary<int, string> _userShippingMethods = null;
        public Dictionary<int, string> UserShippingMethods
        {
            get
            {
                if (this.ShippingMethod == null)
                    this.ShippingMethod = 0;

                if (_userShippingMethods == null)
                {
                    _userShippingMethods = new Dictionary<int, string>();
                    _userShippingMethods.Add(2, "Ship To Studio");
                    _userShippingMethods.Add(3, "Ship To School");
                }
                this.FlowMasterDataContext.SubmitChanges();
                return _userShippingMethods;
            }

        }

        private Dictionary<int, string> _allShippingMethods = null;
        public Dictionary<int, string> AllShippingMethods
        {
            get
            {
                if (_allShippingMethods == null)
                {
                    _allShippingMethods = new Dictionary<int, string>();
                    _allShippingMethods.Add(1, "Ship To Customer");
                    _allShippingMethods.Add(2, "Ship To Studio");
                    _allShippingMethods.Add(3, "Ship To School");
                }
                return _allShippingMethods;
            }

        }

        public Organization Studio
        {
            get
            {
                return this.FlowMasterDataContext.Organizations.First(o => o.OrganizationID == this.StudioID);
            }
        }

        //private List<OrderPackage> _onlineOrders
        public List<OrderPackage> AllOnlineOrders
        {

            get {
                return this.FlowProjectDataContext.OrderPackages.Where(o => o.IsOnlineOrder == true).ToList();
            }
        }

        private bool didOrderHistoryCheck = false;
        public List<OrderPackage> NewOnlineOrders
        {

            get
            {
                if (!didOrderHistoryCheck)
                {
                    List<OrderPackage> newOrders = this.FlowProjectDataContext.OrderPackages.Where(o => o.IsOnlineOrder == true && o.LabOrderID == null).OrderByDescending(or=>or.IQOrderDate).ToList();
                    foreach (OrderPackage op in newOrders)
                    {
                        if (op.OrderHistories != null && op.OrderHistories.Count > 0)
                        {
                            op.LabOrderDate = op.OrderHistories[0].OrderSubmittedDate;
                            op.LabOrderID = Convert.ToInt32(op.OrderHistories[0].OrderID);
                            this.FlowProjectDataContext.SubmitChanges();
                        }
                    }
                    didOrderHistoryCheck = true;
                }
                return this.FlowProjectDataContext.OrderPackages.Where(o => o.IsOnlineOrder == true && o.LabOrderID == null).ToList();
            }
        }

        public void UpdateRemoteImageServiceLists()
        {
            SendPropertyChanged("RemoteImageServiceImagesNew");
            SendPropertyChanged("RemoteImageServiceImagesInProgress");
            SendPropertyChanged("RemoteImageServiceImagesDone");
        }
        public List<SubjectImage> RemoteImageServiceImagesNew
        {

            get
            {
                List<SubjectImage> newImages = this.FlowProjectDataContext.SubjectImages.Where(o => o.RemoteImageService == true && (o.RemoteImageServiceStatus == null || o.RemoteImageServiceStatus == "new")).ToList();
                return newImages;

            }
        }

        public List<SubjectImage> RemoteImageServiceImagesInProgress
        {

            get
            {
                List<SubjectImage> newImages = this.FlowProjectDataContext.SubjectImages.Where(o => o.RemoteImageService == true && o.RemoteImageServiceStatus != null && (o.RemoteImageServiceStatus == "inprogress")).ToList();
                return newImages;

            }
        }

        public List<SubjectImage> RemoteImageServiceImagesDone
        {

            get
            {
                List<SubjectImage> newImages = this.FlowProjectDataContext.SubjectImages.Where(o => o.RemoteImageService == true && o.RemoteImageServiceStatus != null && (o.RemoteImageServiceStatus == "done")).ToList();
                return newImages;

            }
        }

        public List<OrderPackage> SubmittedOnlineOrders
        {

            get
            {
                return this.FlowProjectDataContext.OrderPackages.Where(o => o.IsOnlineOrder == true && o.LabOrderID != null).OrderByDescending(or=>or.LabOrderDate).ToList();
            }
        }

        public void UpdateListOfOnlineOrders()
        {
            SendPropertyChanged("AllOnlineOrders");
            SendPropertyChanged("NewOnlineOrders");
            SendPropertyChanged("SubmittedOnlineOrders");
            SendPropertyChanged("HasNewOnlineOrders");
            SendPropertyChanged("HasOnlinePickupOrders");
        }

        public void UpdateOrderHistoryMasterList()
        {
            SendPropertyChanged("HistoryDropDownList");
            SendPropertyChanged("HistoryDropDownListMini");
        }
        public bool HasNewOnlineOrders
        {
            get
            {
                if (NewOnlineOrders.Count > 0)
                    return true;
                else
                    return false;
            }
        }

        public bool HasOnlinePickupOrders
        {
            get
            {
                if (NewOnlineOrders.Any(n=>n.ShipmentType == "pickup"))
                    return true;
                else
                    return false;
            }
        }


        
        public List<OrderPackage> AllFlowOrders
        {

            get
            {
                return this.FlowProjectDataContext.OrderPackages.Where(o => o.IsOnlineOrder == false).ToList();
            }
        }

        private bool didFlowOrderHistoryCheck = false;
        public List<OrderPackage> NewFlowOrders
        {

            get
            {
                if (!didFlowOrderHistoryCheck)
                {
                    List<OrderPackage> newFlowOrders = this.FlowProjectDataContext.OrderPackages.Where(o => o.IsOnlineOrder == false && o.LabOrderID == null).ToList();
                    foreach (OrderPackage op in newFlowOrders)
                    {
                        if (op.OrderHistories != null && op.OrderHistories.Count > 0)
                        {
                            op.LabOrderDate = op.OrderHistories[0].OrderSubmittedDate;
                            op.LabOrderID = Convert.ToInt32(op.OrderHistories[0].OrderID);
                            this.FlowProjectDataContext.SubmitChanges();
                        }
                    }
                    didFlowOrderHistoryCheck = true;
                }
                return this.FlowProjectDataContext.OrderPackages.Where(o => o.IsOnlineOrder == false && o.LabOrderID == null).ToList();
            }
        }

        public List<OrderPackage> AllFlowOrdersInFilterView
        {

            get
            {
                List<OrderPackage> returnList = new List<OrderPackage>();
                foreach (Subject s in this.SubjectList.GetDataGridCollectionViewSubjects())
                {

                    if (s.SubjectOrder.OrderPackages.Any(o => o.IsOnlineOrder == false))
                        foreach (OrderPackage op in s.SubjectOrder.OrderPackages.Where(o => o.IsOnlineOrder == false))
                            returnList.Add(op);
                }
                return returnList;
            }
        }

        public List<OrderPackage> NewFlowOrdersInFilterView
        {

            get
            {
                List<OrderPackage> returnList = new List<OrderPackage>();
                foreach (Subject s in this.SubjectList.GetDataGridCollectionViewSubjects())
                {
                    
                    if (s.SubjectOrder.OrderPackages.Any(o => o.IsOnlineOrder == false && o.LabOrderID == null))
                        foreach (OrderPackage op in s.SubjectOrder.OrderPackages.Where(o => o.IsOnlineOrder == false && o.LabOrderID == null))
                            returnList.Add(op);
                }
                return returnList;
                }
        }

        public List<OrderPackage> SubmittedFlowOrders
        {

            get
            {
                return this.FlowProjectDataContext.OrderPackages.Where(o => o.IsOnlineOrder == false && o.LabOrderID != null).ToList();
            }
        }

        public void UpdateListOfFlowOrders()
        {
            SendPropertyChanged("AllFlowOrders");
            SendPropertyChanged("NewFlowOrders");
            SendPropertyChanged("SubmittedFlowOrders");
            SendPropertyChanged("HasNewFlowOrders");
            SendPropertyChanged("HasFlowPickupOrders");
        }

        public bool HasNewFlowOrders
        {
            get
            {
                if (NewFlowOrders.Count > 0)
                    return true;
                else
                    return false;
            }
        }

        public bool HasNewFlowPickupOrders
        {
            get
            {
                if (NewFlowOrders.Any(n => n.ShipmentType == null || n.ShipmentType == "pickup"))
                    return true;
                else
                    return false;
            }
        }

        public bool HasAnyFlowPickupOrders
        {
            get
            {
                if (AllFlowOrders.Any(n => n.ShipmentType == null || n.ShipmentType == "pickup"))
                    return true;
                else
                    return false;
            }
        }

        public string DateCreatedShort { get { return this.DateCreated.ToShortDateString(); } }
        public string DateModifiedShort { get { return this.DateModified == null ? "" : ((DateTime)this.DateModified).ToShortDateString(); } }

		private FlowMasterDataContext _flowMasterDataContext = null;
		public FlowMasterDataContext FlowMasterDataContext
		{
			get { return _flowMasterDataContext; }
			set { _flowMasterDataContext = value; }
		}

		private FlowProjectDataContext _flowProjectDataContext = null;
		public FlowProjectDataContext FlowProjectDataContext
		{
			get { return _flowProjectDataContext; }
		}

        public NotificationProgressInfo LoadingProgressInfo { get; set; }
        public bool Initialized { get; set; }

		public string FullProjectName
		{
			get
			{
				if (!String.IsNullOrEmpty(this.FlowProjectDesc))
					return this.FlowProjectName;
				return this.FlowProjectName + "_" + this.FlowProjectDesc.IfNull(this.FlowProjectName);
			}
		}

        public Boolean ShowGreenScreen{
        get{
            if ((bool)FlowMasterDataContext.PreferenceManager.Application.CanGreenScreen && (bool)this.IsGreenScreen)
                return true;

            return false;
        }}

        int lastliveImagecnt = 0;
        public int GalleryLiveImageCount
        {
            get
            {
                lock (locker)
                {
                    try
                    {
                        lastliveImagecnt = this.FlowProjectDataContext.SubjectImages.Count(s => s.ExistsInOnlineGallery == true);
                    }
                    catch
                    {
                        //ignore
                    }

                    return lastliveImagecnt;
                }
            }
        }
        public int GalleryNewImageCount
        {
            get
            {
                lock (locker)
                {
                    try
                    {
                        int liveImages = this.FlowProjectDataContext.SubjectImages.Count(s => s.ExistsInOnlineGallery == true);
                        int allImages = this.FlowProjectDataContext.SubjectImages.Count();
                        return allImages - liveImages;
                    }
                    catch (Exception e)
                    {
                        //try 1 more time
                        Thread.Sleep(500);
                        try
                        {
                            int liveImages = this.FlowProjectDataContext.SubjectImages.Count(s => s.ExistsInOnlineGallery == true);
                            int allImages = this.FlowProjectDataContext.SubjectImages.Count();
                            return allImages - liveImages;
                        }
                        catch (Exception ex)
                        {
                            //try 1 more time
                            return 0;
                        }
                    }
                }
            }
        }
        public void UpdateGalleryCounts()
        {
            this.SendPropertyChanged("GalleryLiveImageCount");
            this.SendPropertyChanged("GalleryNewImageCount");
        }
		public string FullProjectDisplayName
		{
			get
			{
				if (String.IsNullOrEmpty(this.FlowProjectDesc))
					return this.FlowProjectName;

				return this.FlowProjectName + " - " + this.FlowProjectDesc;
			}
		}


		private string FlowProjectGuid10
		{
			get { return this.FlowProjectGuid.ToString().Replace("-", "").Right(9); }
		}

        private string FlowProjectTemplateGuid10
        {
            get { return this.FlowProjectTemplateGuid.ToString().Replace("-", "").Right(9); }
        }
		/// <summary>
		/// Returns the path to the project directory
		/// </summary>
		private string _flowProjectDirPath = null;
		public string DirPath
		{
			get
			{
                if (_flowProjectDirPath == null)
					_flowProjectDirPath = this.FlowMasterDataContext.PreferenceManager.Application.GetFlowProjectDirPath(this.FlowProjectGuid, this.IsNetworkProject == true);

				return _flowProjectDirPath;
			}
		}

        public Boolean EnableProject
        {
            get
            {
                if (this.IsNetworkProject == true)
                {
                    if (Directory.Exists(FlowMasterDataContext.PreferenceManager.Application.NetworkProjectsDirectory))
                        return true;
                    else
                        return false;
                }
                return true;
            }
        }

        public string ProjectToolTip
        {
            get
            {
                if (this.IsNetworkProject == true)
                {
                    if (Directory.Exists(FlowMasterDataContext.PreferenceManager.Application.NetworkProjectsDirectory))
                        return "Open Network Project";
                    else
                        return "Network Project is Disabled. Path Not Found";
                }
                return "Open Project";
            }
        }

        private string _flowProjecDBPath = null;
        public string LocalDBPath
        {
            get
            {
                if (this.MergeDatabasePath != null)
                    return this.MergeDatabasePath;
                if (_flowProjecDBPath == null)
                    _flowProjecDBPath = this.FlowMasterDataContext.PreferenceManager.Application.GetLocalDatabasePath(this.FlowProjectGuid, (this.IsNetworkProject == true), this.FlowProjectGuid.ToString() + ".sdf");

                return _flowProjecDBPath;
            }
        }

		/// <summary>
		/// Returns the path to the project image directory
		/// </summary>
		public string ImageDirPath
		{
            get { return this.FlowMasterDataContext.PreferenceManager.Application.GetFlowProjectImageDirPath(this.FlowProjectGuid, this.IsNetworkProject == true); }
		}


        /// <summary>
        /// Returns a Project Name that is safe for using in a file or folder name
        /// </summary>
        public string FileSafeProjectName
        {
            get { return Regex.Replace(this.FlowProjectName, "[^a-zA-Z0-9_.-]", "_"); }
        }

		/// <summary>
		/// Returns the full filepath to the project database file
		/// </summary>
		public string DatabasePath
		{
			get
			{
				return (this.MergeDatabasePath == null)
					? Path.Combine(this.DirPath, (this.FlowProjectGuid.ToString() + ".sdf"))
					: this.MergeDatabasePath
				;
			}
		}

		internal string MergeDatabasePath { get; set; }

		/// <summary>
		/// Returns the connection string for accessing the project database (via FlowProjectDataContext)
		/// </summary>
        public string ConnectionString
        {
            get { return "Data Source=" + this.LocalDBPath + ";Max Database Size=1024;File Mode = 'shared read';"; }
        }

        public string ConnectionStringReadOnly
        {
            get { return "Data Source=" + this.LocalDBPath + ";Max Database Size=1024;File Mode = 'read only';Temp Path=" + System.IO.Path.GetTempPath(); }
        }

        public string NetworkConnectionString
        {
            get { return "Data Source=" + this.DatabasePath + ";Max Database Size=1024;File Mode = 'shared read';"; }
        }
        public string NetworkConnectionStringReadOnly
        {
            get
            {
                string tempdbFile = Path.Combine(FlowContext.FlowTempDirPath, new FileInfo(this.DatabasePath).Name);
                if (File.Exists(tempdbFile)) File.Delete(tempdbFile);
                File.Copy(this.DatabasePath, tempdbFile);
                return "Data Source=" + tempdbFile + ";Max Database Size=1024;File Mode = 'read only';Temp Path=" + System.IO.Path.GetTempPath();
            }
        }

		/// <summary>
		/// Indicates if the FlowProject object is "new" by testing for an empty Guid value
		/// </summary>
		public bool IsNew { get { return this.FlowProjectGuid == Guid.Empty; } }

		public bool IsValid
		{
			get
			{
				return
					this.StudioID.HasValue	&&
					(!this.IsNew || this.FlowProjectTemplateID.HasValue) &&
					this.OrganizationID > 0 &&
					!String.IsNullOrEmpty(this.FlowProjectName) &&
					!String.IsNullOrEmpty(this.FlowProjectDesc);
			}
		}

		partial void OnStudioIDChanged()
		{
			this.SendPropertyChanged("IsValid");
		}
	
		partial void OnFlowProjectTemplateIDChanged()
		{
			this.SendPropertyChanged("IsValid");
		}

		partial void OnOrganizationIDChanged()
		{
			this.SendPropertyChanged("IsValid");
		}

		partial void OnFlowProjectNameChanged()
		{
			this.SendPropertyChanged("IsValid");
		}

		partial void OnFlowProjectDescChanged()
		{
			this.SendPropertyChanged("IsValid");
		}

		partial void OnOwnerUserIDChanged()
		{
			this.SendPropertyChanged("IsValid");
		}


		/// <summary>
		/// An object for converting image filenames based on formatting preferences
		/// </summary>
		public ImageFileNameFormatter _imageFileNameFormatter = null;

		private ProjectTemplate _projectTemplate = null;
		public ProjectTemplate ProjectTemplate
		{
			get
			{
				if (_projectTemplate == null)
					_projectTemplate = _flowMasterDataContext.ProjectTemplateList
                        .FirstOrDefault(t => t.ProjectTemplateID == this.FlowProjectTemplateID || t.ProjectTemplateGuid == this.FlowProjectTemplateGuid);

				return _projectTemplate;
			}
		}

        public List<string> ProjectSubjectFieldListForSearch
        {
            get
            {
                List<string> tempList = new List<string>();
                tempList.Add("All Fields");
                foreach (ProjectSubjectField psf in this.FlowProjectDataContext.ProjectSubjectFields)
                {
                    if (psf.IsShownInEdit || psf.IsProminentField)
                        tempList.Add(psf.ProjectSubjectFieldDesc);
                }
                return tempList;
            }
        }
        public List<string> ProjectSubjectFieldListForSearchReplace
        {
            get
            {
                List<string> tempList = new List<string>();
                foreach (ProjectSubjectField psf in this.FlowProjectDataContext.ProjectSubjectFields)
                {
                    if (psf.IsShownInEdit || psf.IsProminentField)
                        tempList.Add(psf.ProjectSubjectFieldDesc);
                }
                return tempList;
            }
        }

        public void UpdateProjectSubjectFieldListForSearch()
        {
            this.SendPropertyChanged("ProjectSubjectFieldListForSearch");
        }

		/// <summary>
		/// FlowObservableCollection wrapper for list of user-defined project Subject fields
		/// </summary>
		private FlowObservableCollection<ProjectSubjectField> _projectSubjectFieldList = null;
		public FlowObservableCollection<ProjectSubjectField> ProjectSubjectFieldList
		{
			get
			{
				if (_flowProjectDataContext != null && _projectSubjectFieldList == null)
					_projectSubjectFieldList = new FlowObservableCollection<ProjectSubjectField>(_flowProjectDataContext.ProjectSubjectFields);

				return _projectSubjectFieldList;
			}
		}

		/// <summary>
		/// Retrieves a list 
		/// </summary>
		private ObservableCollection<string> _sortableSubjectFieldList = null;
		public ObservableCollection<string> SortableSubjectFieldList
		{
			get
			{
				if (_sortableSubjectFieldList == null)
				{
					_sortableSubjectFieldList = new ObservableCollection<string>(
						FlowProjectDataContext.GetProjectSubjectFieldList(this).Where(f => !(
							f.ProjectSubjectFieldDisplayName == "Last Name" ||
							f.ProjectSubjectFieldDisplayName == "First Name"
						)).Select(f => f.ProjectSubjectFieldDisplayName)
					);

					_sortableSubjectFieldList.Insert(0, "[None]");

					this.SendPropertyChanged("SortableSubjectFieldList");
				}

				return _sortableSubjectFieldList;
			}
		}


		/// <summary>
		/// FlowObservableCollection for generated ProjectEventTrigger Linq-to-SQL enti
		/// </summary>
		private FlowObservableCollection<ProjectEventTrigger> _projectEventTriggerList = null;
		public FlowObservableCollection<ProjectEventTrigger> ProjectEventTriggerList
		{
			get
			{
				if (_projectEventTriggerList == null)
				{
					_projectEventTriggerList = new FlowObservableCollection<ProjectEventTrigger>(this.FlowProjectDataContext.ProjectEventTriggers);

					foreach (EventTrigger eventTrigger in this.FlowMasterDataContext.EventTriggers)
					{
						EventTriggerType eventTriggerType =
							this.FlowMasterDataContext.EventTriggerTypes
								.FirstOrDefault(ett => ett.EventTriggerTypeID == eventTrigger.EventTriggerTypeID);

						ProjectEventTrigger projectEventTrigger =
							this._projectEventTriggerList
								.FirstOrDefault(p => p.EventTriggerID == eventTrigger.EventTriggerID);

                        if (projectEventTrigger == null)
                        {
                            projectEventTrigger = new ProjectEventTrigger();
                            projectEventTrigger.EventTriggerID = eventTrigger.EventTriggerID;
                            //pet.EventTriggerID = eventTrigger.EventTriggerID;
                            this.FlowProjectDataContext.ProjectEventTriggers.InsertOnSubmit(projectEventTrigger);
                            this.FlowProjectDataContext.SubmitChanges();
                            _projectEventTriggerList = new FlowObservableCollection<ProjectEventTrigger>(this.FlowProjectDataContext.ProjectEventTriggers);

                            //this._projectEventTriggerList.Add(projectEventTrigger);
                        }
						    projectEventTrigger.SetParent(eventTriggerType, eventTrigger);
					}

                    _projectEventTriggerList = new FlowObservableCollection<ProjectEventTrigger>(_projectEventTriggerList.Where(pet => pet.EventTriggerDesc != null));
					// Create a group description for grouping in the UI
					_projectEventTriggerList.View.GroupDescriptions.Add(new PropertyGroupDescription("EventTriggerTypeDesc"));
                   
				}

                return _projectEventTriggerList;
			}
		}

		private FlowObservableCollection<ProjectImageType> _projectImageTypeList = null;
		public FlowObservableCollection<ProjectImageType> ProjectImageTypeList
		{
			get
			{
                try
                {
                    if (_projectImageTypeList == null)
                        if (this.FlowProjectDataContext != null)
                            _projectImageTypeList = new FlowObservableCollection<ProjectImageType>(this.FlowProjectDataContext.ProjectImageTypes);
                }
                catch (Exception)
                {

                    return _projectImageTypeList;
                }

				return _projectImageTypeList;
			}
		}

        private Group _currentGroup { get; set; }
        public Group CurrentGroup
        {
            get
            {
                return _currentGroup;
            }
            set
            {
                _currentGroup = value;
                this.SendPropertyChanged("CurrentGroup");
            }
        }


        private FlowObservableCollection<SubjectTicket> _subjectTicketList = null;
        public FlowObservableCollection<SubjectTicket> SubjectTicketList
        {
            get
            {
                if (_subjectTicketList == null)
                    if (this.FlowProjectDataContext != null)
                        _subjectTicketList = new FlowObservableCollection<SubjectTicket>(this.FlowProjectDataContext.SubjectTickets);

                return _subjectTicketList;
            }
        }


		/// <summary>
		/// FlowObservableCollection wrapper for project Subjects
		/// </summary>
        public SubjectObservableCollection SubjectList
        {
            get { return (_flowProjectDataContext == null) ? null : _flowProjectDataContext.SubjectList; }
        }


        public FlowObservableCollection<Group> GroupList
        {
            get { return (_flowProjectDataContext == null) ? null : new FlowObservableCollection<Group>(_flowProjectDataContext.Groups); }
        }
        public void RefreshGroupList()
        {
            this.SendPropertyChanged("GroupList");
        }

        /// <summary>
        /// Gets the list of subjects with the current filter applied.
        /// </summary>
        /// <returns></returns>
        public List<Subject> GetFilteredSubjectList()
        {
            List<Subject> filteredSubjectList = new List<Subject>();
            foreach (DataRowView d in this.SubjectList.DataGridCollectionView)
                filteredSubjectList.Add(this.SubjectList.First(sub => sub.SubjectID == (d["SubjectID"] as int?)));
            return filteredSubjectList;
        }

         public List<Subject> SubjectListMerge
        {
            get {
                List<Subject> tempList = (_flowProjectDataContext == null) ? null : _flowProjectDataContext.SubjectList.Where(s => s.FlagForMerge == true).ToList();
                //if there are subjects flaged for merge, only do those, otherwise do all subjects
                if (tempList != null && tempList.Count > 0)
                    return tempList;
                else
                    return (_flowProjectDataContext == null) ? null : _flowProjectDataContext.SubjectList.ToList();
            }
        }
        

		internal void OnSubjectListChanged(object sender, EventArgs e)
		{
			this.SendPropertyChanged("HasSubjects");
			this.SendPropertyChanged("SubjectList");

		}

		/// <summary>
		/// The number of Subject items in the project
		/// </summary>
		public int SubjectCount
		{
			get { return (this.SubjectList == null) ? 0 : this.SubjectList.Count; }
		}


		/// <summary>
		/// Indicates whether or not any Subject items are assigned to the project
		/// </summary>
		public bool HasSubjects
		{
			get
			{
				if (_flowProjectDataContext == null)
					return false;

				return this.SubjectCount > 0;
			}
		}

		/// <summary>
		/// FlowObservableCollection wrapper for list of all images assigned to project Subjects
		/// </summary>
		public FlowObservableCollection<SubjectImage> ImageList
		{
			get { return (_flowProjectDataContext == null) ? null : _flowProjectDataContext.ImageList; }
		}

        /// <summary>
        /// FlowObservableCollection wrapper for list of all images assigned to project Subjects
        /// </summary>
        public FlowObservableCollection<SubjectOrder> OrderList
        {
            get { return (_flowProjectDataContext == null) ? null : _flowProjectDataContext.OrderList; }
        }


		private Organization _organization = null;
		public Organization Organization
		{
			get
			{
				if (_organization == null)
					_organization = _flowMasterDataContext.Organizations.FirstOrDefault(o => o.OrganizationID == this.OrganizationID);

				return _organization;
			}
		}

		private Organization _customer = null;
		public Organization Customer
		{
			get
			{
				if (_customer == null)
					_customer = _flowMasterDataContext.Organizations.FirstOrDefault(o => o.OrganizationID == this.StudioID.Value);

				return _customer;
			}
		}

		public bool HasIncompleteProducts
		{
			get
			{
                //bool rValue =
                //    this.FlowProjectDataContext.SubjectOrders.AsEnumerable().Any(o => !o.IsMarkedForDeletion && 
                //        o.OrderPackageList.Any(
                //            pk => pk.OrderProductList.Any(
                //                pr => pr.OrderProductNodeList.Any(
                //                    n => n.ImageQuixProductNodeType == 1 && n.SubjectImageID == null
                //                )
                //            )
                //        )
                //    );
                //return rValue;
                return false;
			}				
//				.OrderProductNodes.Any(n => n.ImageQuixProductNodeType == 1 && n.SubjectImageID == null); }
		}

		public bool HasMissingImages
		{
			get {
                return false;
                //bool rValue = this.ImageList.Any(i => !i.ImageFileExists);
                //return rValue;
            }
		}

		public void RefreshHasIncompleteProducts()
		{
			this.SendPropertyChanged("HasIncompleteProducts");
			this.HasIncompleteProductsChanged(this, new EventArgs<bool>(this.HasIncompleteProducts));
		}

		public void RefreshHasMissingImages()
		{
			this.SendPropertyChanged("HasMissingImages");
			this.HasMissingImagesChanged(this, new EventArgs<bool>(this.HasMissingImages));
		}


		/// <summary>
		/// The User (representing the photographer) assigned to the project
		/// </summary>
		private User _photographer = null;
		public User Photographer
		{
			get
			{
				if (_photographer == null)
					_photographer = _flowMasterDataContext.Users.FirstOrDefault(u => u.UserID == this.OwnerUserID);

				return _photographer;
			}
		}

		/// <summary>
		/// Represents the abstract organizational structure of the Organization containing the Subjects
		/// of the photographic session; the structure is defined by hierarchical ProjectOrganizationUnitType
		/// elements
		/// </summary>
		private ProjectOrganizationalUnitType _organizationalStructure = null;
		public ProjectOrganizationalUnitType OrganizationalStructure
		{
			get
			{
				// If a structure is defined (and thus has unit types)
				if (_flowProjectDataContext.ProjectOrganizationalUnitTypes.Count() > 0)
				{
					// Select the root unit type
					_organizationalStructure = _flowProjectDataContext.ProjectOrganizationalUnitTypes
						.First(ou => !ou.ProjectOrganizationalUnitTypeParentID.HasValue);

					// Populate the hierarchy
					_organizationalStructure.SetChildren(_flowProjectDataContext.ProjectOrganizationalUnitTypes);
				}

				return _organizationalStructure;
			}
		}

		/// <summary>
		/// The product program assigned to the project (if any)
		/// </summary>
		private FlowCatalog _flowCatalog = null;
		public FlowCatalog FlowCatalog
		{
			get
			{
				if (_flowCatalog == null)
				{
                    try
                    {
                        MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                          {
                              _flowCatalog = _flowMasterDataContext.FlowCatalogs.SingleOrDefault(p => (int?)p.FlowCatalogID == this.FlowCatalogID);

                              // If the FlowCatalog has been deleted
                              if (_flowCatalog == null && this.FlowCatalogID.HasValue && this.FlowMasterDataContext.FlowCatalogs.Any(c => c.FlowCatalogID == this.FlowCatalogID))
                              {
                                  // Mark all orders for deletion
                                  foreach (SubjectOrder subjectOrder in this.FlowProjectDataContext.SubjectOrders)
                                  {
                                      subjectOrder.IsMarkedForDeletion = true;
                                      foreach (OrderPackage op in subjectOrder.OrderPackages)
                                          subjectOrder.RemoveOrderPackage(op, this.FlowProjectDataContext);
                                  }

                                  // Remove the FlowCatalog reference
                                  this.FlowCatalogID = null;

                                  // Save changes
                                  this.Save();
                              }
                          }));
                    }
                    catch (Exception e)
                    {
                        return null;
                    }
				}

				return _flowCatalog;
			}

			set
			{
				_flowCatalog = value;
                if (_flowCatalog != null)
                {
                    this.FlowCatalogID = _flowCatalog.FlowCatalogID;
                    _flowMasterDataContext.SubmitChanges();

                    if (this.SubjectList != null)
                    {
                        foreach (Subject subject in this.SubjectList)
                        {
                            if (subject.FlowProjectDataContext == null)
                                subject.FlowProjectDataContext = this.FlowProjectDataContext;

                            subject.SetProductOrder(_flowCatalog);
                            subject.FlagForMerge = true;
                        }
                    }

                    this.SendPropertyChanged("FlowCatalog");
                    this.SendPropertyChanged("HasAssignedFlowCatalog");
                }
			}
		}

		public bool HasAssignedFlowCatalog
		{
			get { return this.FlowCatalog != null; }
		}


		/// 
		#region REPORTS
		///

		/// <summary>
		/// 
		/// </summary>
		private List<FlowReportView> _flowReportViewList = null;
		private List<FlowReportView> FlowReportViewList
		{
			get
			{
				if (_flowReportViewList == null)
					this.RestoreReportViews();

				return _flowReportViewList;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public IQueryable FilteredFlowReportViewList
		{
			get
			{
				List<FlowReportView> flowReportViewList =
					this.FlowReportViewList.Where(r => r.ReportName == this.SelectedReportName).ToList();

				flowReportViewList.Insert(0, new FlowReportView(this.SelectedReportName, this));

				return flowReportViewList
					.Select(r => new
						{
							ReportViewDesc = (r.IsDefault) ? "[Default View]" : r.ReportViewName,
							FlowReportView = r
						}
					)
					.AsQueryable();
			}
		}

		/// <summary>
		/// Returns a list of all subject reports for
		/// </summary>
		public IEnumerable<FlowReportView> SubjectOrderReportsList
		{
			get
			{
                return this.FlowReportViewList.Where(v => v.ReportName == "Subject Orders Summary");
			}
		}

		/// <summary>
		/// 
		/// </summary>
		private string _selectedReportName = "Subject Orders Summary";
		public string SelectedReportName
		{
			get { return _selectedReportName; }
			set
			{
				_selectedReportName = value;
				this.SendPropertyChanged("FilteredFlowReportViewList");
                this.SendPropertyChanged("SelectedReportName");
			}
		}

        /// <summary>
        /// 
        /// </summary>
        private string _selectedReportOrderFilter = "New Orders";
        public string SelectedReportOrderFilter
        {
            get { return _selectedReportOrderFilter; }
            set
            {
                _selectedReportOrderFilter = value;
            }
        }

        private ProjectSubjectField _selectedPageBreakField = null;
        public ProjectSubjectField SelectedPageBreakField
        {
            get { return _selectedPageBreakField; }
            set
            {
                _selectedPageBreakField = value;
            }
        }

             /// <summary>
        /// 
        /// </summary>
        /// 
        private bool _isReportShowFooter = true;
        public bool IsReportShowFooter
        {
            get { return _isReportShowFooter; }
            set
            {
                _isReportShowFooter = value;
                this.SendPropertyChanged("IsReportShowFooter");
            }
        }

        private bool _isReportStackSorted = true;
        public bool IsReportStackSorted
        {
            get { return _isReportStackSorted; }
            set
            {
                _isReportStackSorted = value;
                this.SendPropertyChanged("IsNOTReportStackSorted");
            }
        }
        public bool IsNOTReportStackSorted
        {
            get { return !_isReportStackSorted; }
        }

        public int _subjectCopies = 1;
        public int SubjectCopies
        {
            get { return _subjectCopies; }
            set
            {
                _subjectCopies = value;
                this.SendPropertyChanged("SubjectCopies");
            }
        }

        public int _customReportStackSortHowManyUp = 30;
        public int CustomReportStackSortHowManyUp
        {
            get { return _customReportStackSortHowManyUp; }
            set
            {
                _customReportStackSortHowManyUp = value;
                this.SendPropertyChanged("CustomReportStackSortHowManyUp");
            }
        }

        private bool _reportShowPrice = true;
        public bool ReportShowPrice
        {
            get { return _reportShowPrice; }
            set
            {
                _reportShowPrice = value;
            }
        }

        private bool _reportShowBarCodes = true;
        public bool ReportShowBarCodes
        {
            get { return _reportShowBarCodes; }
            set
            {
                _reportShowBarCodes = value;
            }
        }

        private bool _reportShowProducts = false;
        public bool ReportShowProducts
        {
            get { return _reportShowProducts; }
            set
            {
                _reportShowProducts = value;
            }
        }

        private bool _reportShowImageOptions = false;
        public bool ReportShowImageOptions
        {
            get { return _reportShowImageOptions; }
            set { _reportShowImageOptions = value; }
        }

         /// <summary>
        /// 
        /// </summary>
        private int _newTicketBatchSize = 20;
        public int NewTicketBatchSize
        {
            get { return _newTicketBatchSize; }
            set
            {
                _newTicketBatchSize = value;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        private string _selectedReportSubjectBarcodesLayout = "10-Up";
        public string SelectedReportSubjectBarcodesLayout
        {
            get { return _selectedReportSubjectBarcodesLayout; }
            set
            {
                _selectedReportSubjectBarcodesLayout = value;
            }
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="flowReportView"></param>
		public void SaveReportView(FlowReportView flowReportView)
		{
			flowReportView.IsDefault = false;
			this.FlowReportViewList.Add(flowReportView);
			this.SaveReportViews();
		}
	
		/// <summary>
		/// 
		/// </summary>
		public void SaveReportViews()
		{
			foreach (FlowReportView flowReportView in this.FlowReportViewList.Where(v => !v.IsDefault))
			{
				flowReportView.SetSerializationData(this.FlowProjectDataContext.FlowReportViewDefs);
			}

			this.SendPropertyChanged("FilteredFlowReportViewList");
		}

		/// <summary>
		/// 
		/// </summary>
		public void RestoreReportViews()
		{
			_flowReportViewList = new List<FlowReportView>();

			foreach (FlowReportViewDef flowReportViewDef in this.FlowProjectDataContext.FlowReportViewDefs)
			{
				_flowReportViewList.Add(FlowReportView.GetFlowReportView(flowReportViewDef, this));
			}

			this.SendPropertyChanged("FilteredFlowReportViewList");
		}

		/// 
		#endregion REPORTS
		///



        /// <summary>
        /// Returns a list of values for the Order History Dropdown
        /// </summary>
        public List<String> HistoryDropDownList
        {
            get
            {
                List<String> returnList = new List<string>();
                returnList.Add("New Orders");
                returnList.Add("All Orders");
                returnList.Add("Current Filter (New Orders)");
                returnList.Add("Current Filter (All Orders)");
                foreach (OrderPackage pkg in this.FlowProjectDataContext.OrderPackages.Where(p=>p.LabOrderID != null && p.LabOrderID > 0).GroupBy(p => p.LabOrderID).Select(g => g.First()).ToList())
                {

                    string thisItem = pkg.LabOrderID + " - " + pkg.LabOrderDate;
                    if (!returnList.Contains(thisItem))
                            returnList.Add(thisItem);

                }
                //foreach (OrderHistoryMaster history in this.FlowProjectDataContext.OrderHistoryMasters)
                //{
                   
                //string thisItem = history.OrderID + " - " + history.OrderSubmittedDate;
                //if (!returnList.Contains(thisItem))
                //    if(this.FlowProjectDataContext.OrderPackages.Any(p=>p.LabOrderID == Convert.ToInt32(history.OrderID)))
                //        returnList.Add(thisItem);

                //}
                return returnList;
            }
        }


        public List<String> HistoryDropDownListMini
        {
            get
            {
                List<String> returnList = new List<string>();
                returnList.Add("");
                foreach (OrderPackage pkg in this.FlowProjectDataContext.OrderPackages.Where(p => p.LabOrderID != null && p.LabOrderID > 0).GroupBy(p => p.LabOrderID).Select(g => g.First()).ToList())
                {

                    string thisItem = pkg.LabOrderID + " - " + pkg.LabOrderDate;
                    if (!returnList.Contains(thisItem))
                        returnList.Add(thisItem);

                }
                //foreach (OrderHistoryMaster history in this.FlowProjectDataContext.OrderHistoryMasters)
                //{

                //string thisItem = history.OrderID + " - " + history.OrderSubmittedDate;
                //if (!returnList.Contains(thisItem))
                //    if(this.FlowProjectDataContext.OrderPackages.Any(p=>p.LabOrderID == Convert.ToInt32(history.OrderID)))
                //        returnList.Add(thisItem);

                //}
                return returnList;
            }
        }
        /// <summary>
        /// Returns a list of values for the Order History Dropdown
        /// </summary>
        public List<String> ReportSubjectBarcodesLayoutDropDownList
        {
            get
            {
                List<String> returnList = new List<string>();
                returnList.Add("8-Up");
                returnList.Add("10-Up");
                returnList.Add("30-Up");
                //foreach (OrderHistoryMaster history in this.FlowProjectDataContext.OrderHistoryMasters)
                //{

                //    string thisItem = history.OrderID + " - " + history.OrderSubmittedDate;
                //    if (!returnList.Contains(thisItem))
                //        returnList.Add(thisItem);

                //}
                return returnList;
            }
        }

		/// <summary>
		/// Displays the barcode scan intialization sequence, with special characters replaced with mnemonics
		/// </summary>
		public string BarcodeScanInitSequenceDisplay
		{
			get
			{
				return (this.BarcodeScanInitSequence == null)
					? null
					: this.BarcodeScanInitSequence.BarcodeEscape();
			}
		}

		/// <summary>
		/// Displays a textual description of the barcode value pattern
		/// </summary>
		public string BarcodeScanValueLengthDisplay
		{
			get
			{
				return (this.BarcodeScanValueLength.HasValue)
					? this.BarcodeScanValueLength.ToString() + "-character alphanumeric value"
					: null;
			}
		}

		/// <summary>
		/// Displays the barcode scan termination sequence, with special characters replaced with mnemonics
		/// </summary>
		public string BarcodeScanTermSequenceDisplay
		{
			get
			{
				return (this.BarcodeScanTermSequence == null)
					? null
					: this.BarcodeScanTermSequence.BarcodeEscape();
			}
		}

		#endregion // FIELDS AND PROPERTIES


		#region CONSTRUCTORS

		/// <summary>
		/// Constructor; invoked for connection to an unattached project database file during project merge
		/// </summary>
		/// <param name="mergeDatabasePath"></param>
		/// <param name="flowMasterDataContext"></param>
		internal FlowProject(string mergeDatabasePath, FlowMasterDataContext flowMasterDataContext) : this()
		{
			this.MergeDatabasePath = mergeDatabasePath;
	
			_flowMasterDataContext = flowMasterDataContext;
            _flowProjectDataContext = new FlowProjectDataContext(new SqlCeConnection("Data Source=" + this.MergeDatabasePath + ";Max Database Size=1024;"), _flowMasterDataContext);
			_flowProjectDataContext.FlowProject = this;

			Control_FlowCatalog sourceFlowCatalog = _flowProjectDataContext.Control_FlowCatalogs.FirstOrDefault();
			if (sourceFlowCatalog != null)
			{
				FlowCatalog projectCatalog =
					_flowMasterDataContext.FlowCatalogs.FirstOrDefault(c => c.FlowCatalogGuid == sourceFlowCatalog.FlowCatalogGuid);

				if(projectCatalog == null)
				{
					this.FlowCatalog = 
						_flowMasterDataContext.FlowCatalogs.FirstOrDefault(c => c.FlowCatalogDesc == sourceFlowCatalog.FlowCatalogDesc);
				}
				else
				{
					this.FlowCatalog = projectCatalog;
				}
			}

            
		}
		
		#endregion


		#region METHODS

		/// <summary>
		/// Initializes a new project
		/// </summary>
		/// <param name="flowMasterDataContext"></param>
		internal void Initialize(FlowMasterDataContext flowMasterDataContext)
		{
			this.FlowProjectGuid = Guid.NewGuid();
			_flowMasterDataContext = flowMasterDataContext;
		}

        public string ProjectTemplateDisplayName
        {
            get
            {
                ProjectTemplate projT = this.FlowMasterDataContext.ProjectTemplateList.FirstOrDefault(pt => pt.ProjectTemplateGuid == this._FlowProjectTemplateGuid);
                if (projT != null)
                {
                    return projT.ProjectTemplateDesc;
                }
                else
                    return "";
            }
        }

       
		/// <summary>
		/// Copies all templated data elements stored in the supplied ProjectTemplate item
		/// to the FlowProject (for new projects only)
		/// </summary>
		/// <param name="projectTemplate"></param>
		public void InitializeProjectFromTemplate(ProjectTemplate projectTemplate)
		{
			this.Connect();

            _flowProjectDataContext.InitializeProjectFromTemplate(this, projectTemplate);
		}

		/// <summary>
		/// Instantiates the FlowProjectDataContext object thus establishing a connection
		/// to the project database
		/// </summary>
		/// <param name="flowMasterDataContext"></param>
		/// <returns>FlowProject</returns>
		public FlowProject Connect(FlowMasterDataContext flowMasterDataContext)
		{
			_flowMasterDataContext = flowMasterDataContext;
			
			this.Connect();

			return this;
		}

        public void RegisterImageListChangeEventHandler()
        {

            this.ImageList.CollectionChanged += delegate
           {
               //this.RefreshHasMissingImages();
           };

        }


		/// <summary>
		/// Instantiates the FlowProjectDataContext object thus establishing a connection
		/// to the project database
		/// </summary>
		/// <param name="flowMasterDataContext"></param>
		/// <returns>FlowProject</returns>
		private FlowProject Connect()
		{
           
            try
            {
                _flowProjectDataContext = new FlowProjectDataContext(this, _flowMasterDataContext);
            }
            catch
            {
                Thread.Sleep(5000);
                _flowProjectDataContext = new FlowProjectDataContext(this, _flowMasterDataContext);
            }
			_imageFileNameFormatter = new ImageFileNameFormatter(this);
            
			// Initialize the first Subject

			

			//if (this.SubjectList.CurrentItem != null && this.SubjectList.CurrentItem.SubjectData == null) { }

			//if (this.SubjectList.CurrentItem != null) { }

			return this;
		}

		/// <summary>
		/// Saves all project data modified since the last save operation
		/// </summary>
		public void Save()
		{
			if (_flowProjectDataContext != null)
				_flowProjectDataContext.SubmitChanges();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="columnMappings"></param>
		/// <param name="sourceTable"></param>
		public void ImportData(ImportColumnPairCollection<ProjectSubjectField> columnMappings, DataTable sourceTable, NotificationProgressInfo info, Dispatcher dispatcher, string imageDir, string imageField, bool auto8x10)
		{
			_flowProjectDataContext.ImportData(columnMappings, sourceTable, info, dispatcher, imageDir, imageField, auto8x10);
			this.SendPropertyChanged("ProjectSubjectFieldList");
		}

		///// <summary>
		///// 
		///// </summary>
		///// <param name="flowMasterDataContext"></param>
		//public void PrepareForExport(FlowMasterDataContext flowMasterDataContext)
		//{
		//    this.Connect(flowMasterDataContext);
        //    this.FlowProjectDataContext.SubjectDataLoaded += delegate { this.PrepareForExport(); };
		//}

		/// <summary>
		/// 
		/// </summary>
		public void PrepareForExport()
		{
            lock (locker)
            {
                this.FlowProjectDataContext.ClearControlTables();

                Control_Organization organization =
                    _flowMasterDataContext.Organizations.FirstOrDefault(o => o.OrganizationID == this.OrganizationID);
                if (organization.OrganizationGuid == new Guid())
                {
                    organization.OrganizationGuid = Guid.NewGuid();
                    FlowMasterDataContext.SubmitChanges();
                }

                this.FlowProjectDataContext.Control_Organizations.InsertOnSubmit(organization);

                Control_Organization studio =
                    _flowMasterDataContext.Organizations.FirstOrDefault(o => o.OrganizationID == this.StudioID.Value);
                if (studio.OrganizationGuid == new Guid())
                {
                    studio.OrganizationGuid = Guid.NewGuid();
                    FlowMasterDataContext.SubmitChanges();
                }
                this.FlowProjectDataContext.Control_Organizations.InsertOnSubmit(studio);

                if (this.OwnerUserID > 0)
                {
                    Control_User user =
                        _flowMasterDataContext.Users.FirstOrDefault(u => u.UserID == this.OwnerUserID);

                    this.FlowProjectDataContext.Control_Users.InsertOnSubmit(user);
                }

                Control_FlowProject project = this;

                this.FlowProjectDataContext.Control_FlowProjects.InsertOnSubmit(project);

                if (this.FlowCatalog != null)
                {
                    Control_FlowCatalog catalog = new Control_FlowCatalog(this);
                    this.FlowProjectDataContext.Control_FlowCatalogs.InsertOnSubmit(catalog);
                }

                foreach (Subject subject in this.FlowProjectDataContext.Subjects.Where(s => s.SubjectGuid == null || s.FlowProjectGuid == null))
                {
                    if (!subject.SubjectGuid.HasValue)
                    {
                        subject.SubjectGuid = Guid.NewGuid();
                        subject.FlagForMerge = true;
                    }

                    if (!subject.FlowProjectGuid.HasValue)
                        subject.FlowProjectGuid = this.FlowProjectGuid;
                }

                this.FlowProjectDataContext.SubmitChangesLocal();

                if (this.FlowProjectDataContext.Connection.State != ConnectionState.Closed)
                    this.FlowProjectDataContext.Connection.Close();
            }
            
		}

		/// <summary>
		/// Exports project data for import by external applications
		/// </summary>
        public DataTable GetExportData(ProjectImageType imageType, bool groupPhotoAsSeperatePose, out IEnumerable<string> imageFileList, out List<string> BackgroundFileList, bool useAllImages, List<Subject> FilteredSubjectList)
		{
            List<string> _backgroundFileList = new List<string>();

			DataTable subjectDataTable = new DataTable();

			foreach (ProjectSubjectField field in this.ProjectSubjectFieldList)
			{
				DataColumn column = new DataColumn(field.ProjectSubjectFieldDisplayName, typeof(string));
				subjectDataTable.Columns.Add(column);
			}

            //subjectDataTable.Columns.Add(new DataColumn("TicketID", typeof(string)));

			DataColumn projectColumn;

			projectColumn = new DataColumn("Project Name");
			subjectDataTable.Columns.Add(projectColumn);

			projectColumn = new DataColumn("Event Name");
			subjectDataTable.Columns.Add(projectColumn);

			projectColumn = new DataColumn("Event Date");
			subjectDataTable.Columns.Add(projectColumn);

			projectColumn = new DataColumn("Event Comments");
			subjectDataTable.Columns.Add(projectColumn);

            projectColumn = new DataColumn("Project Guid");
            subjectDataTable.Columns.Add(projectColumn);

            bool skipProjPhotographer = true;
            if (!subjectDataTable.Columns.Contains("Photographer"))
            {
                projectColumn = new DataColumn("Photographer");
                subjectDataTable.Columns.Add(projectColumn);
                skipProjPhotographer = false;
            }

			projectColumn = new DataColumn("Organization Name");
			subjectDataTable.Columns.Add(projectColumn);

			projectColumn = new DataColumn("Organization Address");
			subjectDataTable.Columns.Add(projectColumn);

			projectColumn = new DataColumn("Organization City");
			subjectDataTable.Columns.Add(projectColumn);

			projectColumn = new DataColumn("Organization State/Province");
			subjectDataTable.Columns.Add(projectColumn);

			projectColumn = new DataColumn("Organization Zip Code");
			subjectDataTable.Columns.Add(projectColumn);

            projectColumn = new DataColumn("Organization Shipping Address");
            subjectDataTable.Columns.Add(projectColumn);

            projectColumn = new DataColumn("Organization Shipping City");
            subjectDataTable.Columns.Add(projectColumn);

            projectColumn = new DataColumn("Organization Shipping State/Province");
            subjectDataTable.Columns.Add(projectColumn);

            projectColumn = new DataColumn("Organization Shipping Zip Code");
            subjectDataTable.Columns.Add(projectColumn);

			projectColumn = new DataColumn("Studio Lab Customer ID");
			subjectDataTable.Columns.Add(projectColumn);

			projectColumn = new DataColumn("Image Name");
			subjectDataTable.Columns.Add(projectColumn);

            projectColumn = new DataColumn("Date Assigned");
            subjectDataTable.Columns.Add(projectColumn);

			projectColumn = new DataColumn("Image Type");
			subjectDataTable.Columns.Add(projectColumn);

            projectColumn = new DataColumn("Packages");
            subjectDataTable.Columns.Add(projectColumn);

            projectColumn = new DataColumn("GSBackground");
            subjectDataTable.Columns.Add(projectColumn);

            projectColumn = new DataColumn("Group Image");
            subjectDataTable.Columns.Add(projectColumn);

            projectColumn = new DataColumn("Source Computer");
            subjectDataTable.Columns.Add(projectColumn);

			//Organization organization = this.FlowMasterDataContext.OrgList.FirstOrDefault(o => o.OrganizationID == this.OrganizationID);

			IEnumerable<SubjectImage> imageList;

            bool filterOn = false;
            List<Subject> tmpSubjects = this.SubjectList.ToList();
            if (FilteredSubjectList != null)
            {
                tmpSubjects = FilteredSubjectList;
                filterOn = true;
            }

            //always include GroupImage
            if (useAllImages)
            {
                imageList = tmpSubjects.SelectMany(s =>
                    s.ImageList
                );
            }
			else if (imageType == null)
			{
				imageList = tmpSubjects.SelectMany(s =>
					s.ImageList.Where(i => i.IsPrimary || i.IsGroupImage)
				);
			}
			else
			{
				imageList = tmpSubjects.SelectMany(s =>
					s.ImageList.Where(i =>
						(i.SubjectImageTypeList.Any(t => t.ProjectImageTypeID == imageType.ProjectImageTypeID && t.IsAssigned) || i.IsGroupImage)
					)
				);
			}

            if (filterOn)
                imageList = imageList.Where(i => i.IsSuppressed == false);

            var subjectList = tmpSubjects.ToList().Join(
                imageList,
                s => s.SubjectID,
                i => i.SubjectID,
                (s, i) => new { s.SubjectData, i.ImageFileName, i.DateAssigned, s.SubjectOrders, s, i.IsGroupImage, i.IsPrimary, i.SubjectImageTypes, i.SourceComputer }
            );

            //if (FilteredSubjectList != null)
            //{
            //    subjectList = FilteredSubjectList.Join(
            //        imageList,
            //        s => s.SubjectID,
            //        i => i.SubjectID,
            //        (s, i) => new { s.SubjectData, i.ImageFileName, s.SubjectOrders, s, i.IsGroupImage, i.IsPrimary }
            //    );
            //}
			

			
			foreach (var subject in subjectList)
			{
                if ((!groupPhotoAsSeperatePose && subject.IsGroupImage) && !subject.IsPrimary)
                    continue;
				DataRow row = subjectDataTable.NewRow();

				foreach (ProjectSubjectField field in this.ProjectSubjectFieldList)
				{
					row[field.ProjectSubjectFieldDisplayName] = subject.SubjectData[field.ProjectSubjectFieldDisplayName].Value;
				}
                //row["TicketID"] = subject.s.TicketCode;

                string greenScreenBackground = "";
                string packages = "";
                Subject trueSubject = subject.s;
                foreach (SubjectOrder so in trueSubject.SubjectOrders)
                {
                    if (!so.IsMarkedForDeletion)
                    {
                        foreach (OrderPackage op in so.OrderPackageList)
                        {
                            greenScreenBackground = "";
                            if (op.OrderProducts.Any(prod => prod.OrderProductNodes.Any(node => node.SubjectImage != null && node.SubjectImage.ImageFileName == subject.ImageFileName)))
                            {
                                if (this.IsGreenScreen == true && op.DefaultBackgound != null && String.IsNullOrEmpty(greenScreenBackground))
                                {
                                    _backgroundFileList.Add(op.DefaultBackgound.FullPath);
                                    greenScreenBackground = op.DefaultBackgound.FileName;
                                }

                                //if (op.ProductPackage != null)
                                //    packages = packages + op.ProductPackage.Map + "-1;";
                                string options = "";

                                if (op.OrderProducts.Count > 0 && op.OrderProducts[0].OrderProductOptions.Count > 0)
                                {
                                    foreach (OrderProductOption opo in op.OrderProducts[0].OrderProductOptions)
                                    {
                                        //(this.FlowCatalog.FlowCatalogOrderOptionList.Select(oo=> oo.ImageQuixCatalogOption).Where(oo=> oo.ResourceURL == oio.ResourceURL) as ImageQuixCatalogOption).PrimaryKey.ToString())
                                        if (this.FlowMasterDataContext.ImageQuixOptions.Any(iqo => iqo.PrimaryKey == opo.ImageQuixOptionPk))
                                        {
                                            logger.Info("about to map an IQ Option");
                                            ImageQuixOption iqopt = this.FlowMasterDataContext.ImageQuixOptions.First(iqo => iqo.PrimaryKey == opo.ImageQuixOptionPk);
                                            logger.Info("done map an IQ Option");
                                            if(this.isDp2)
                                                options += ("," + iqopt.Label);
                                            else
                                                options += ("," + iqopt.ResourceURL);
                                        }
                                    }

                                }

                                logger.Info("about to match a subject image: " + subject.ImageFileName);
                                SubjectImage subjectImage = subject.s.SubjectImages.First(i=>i.ImageFileName == subject.ImageFileName);
                                logger.Info("done match a subject image: " + subject.ImageFileName);
                                if (subjectImage.OrderImageOptions.Count > 0)
                                {
                                    foreach (OrderImageOption oio in subjectImage.OrderImageOptions)
                                    {
                                       
                                        if (this.isDp2)
                                        {
                                            logger.Info("about to map an Flow Catalog Option");
                                            FlowCatalogOption fco = this.FlowCatalog.FlowCatalogOptions.FirstOrDefault(oo => oo.FlowCatalogOptionGuid == oio.FlowCatalogOptionGuid);
                                            logger.Info("done map an Flow Catalog Option");
                                            if (fco != null)
                                            {
                                                if (string.IsNullOrEmpty(fco.Map))
                                                    options += ("," + fco.ImageQuixCatalogOption.Label);
                                                else
                                                    options += ("," + fco.Map);
                                            }
                                            else
                                            {
                                                logger.Error("FAILED TO MAP A FLOW CATALOG OPTION: " + subjectImage.Subject.FormalFullName + " - option: " + oio.FlowCatalogOptionGuid);
                                            }
                                        }
                                        else
                                            options += ("," + oio.ResourceURL);
                                    }
                                   
                                }
                                if (!string.IsNullOrEmpty(greenScreenBackground))
                                {
                                    if (this.FlowMasterDataContext.ImageQuixCatalogOptions.Any(o => o.Label == greenScreenBackground))
                                    {
                                        logger.Info("about to map an IQ Catalog Option");
                                        options += ("," + this.FlowMasterDataContext.ImageQuixCatalogOptions.First(o => o.Label == greenScreenBackground).ResourceURL);
                                        logger.Info("done map an IQ Catalog Option");
                                    }
                                    else
                                    {
                                        options += ("," + "nomap");
                                    }
                                }
                                logger.Info("building packages list");
                                try
                                {
                                    if (op.ProductPackage.IsAnyXPackage)
                                    {
                                        foreach (string u in op.Units.Split(";"))
                                        {
                                            string id = u.Split("-")[0];
                                            if (id == op.ProductPackage.Map)
                                                continue;
                                            packages = packages + id + "-1;";
                                        }
                                    }
                                    else
                                    {
                                        if (op.ProductPackage == null)
                                            packages = packages + "MissingPackage" + options + "-1;";
                                        else
                                            packages = packages + op.ProductPackage.Map + options + "-" + op.Qty + ";";
                                    }
                                }
                                catch (Exception ex)
                                {
                                    logger.Error("Not throwing an exception - Failed to Access ProductPackage for " + op.ProductPackageDesc);
                                    packages = packages + "MissingPackage" + options + "-1;";
                                }
                            }
                        }
                    }
                }
                if(packages.Length > 0)
                    packages.Remove(packages.Length - 1);

				row["Project Name"] = this.FlowProjectName;
				row["Event Name"] = this.FlowProjectDesc;
				row["Event Date"] = this.DateCreated;
				row["Event Comments"] = this.Comments;
                row["Project Guid"] = this.FlowProjectGuid;
                if (!skipProjPhotographer)
                {
                    if (this.Photographer == null)
                        row["Photographer"] = this.FlowMasterDataContext.LoginID;
                    else
                        row["Photographer"] = this.Photographer.FormalFullName;
                }
				row["Organization Name"] = this.Organization.OrganizationName;
                row["Organization Address"] = this.Organization.OrganizationAddressLine1;
                row["Organization City"] = this.Organization.OrganizationCity;
                row["Organization State/Province"] = (this.Organization.OrganizationStateCode == null)
                    ? null
                    : this.Organization.OrganizationStateCode.Trim()
                    ;
                row["Organization Zip Code"] = (this.Organization.OrganizationZipCode == null)
					? null
                    : this.Organization.OrganizationZipCode.Trim()
				;
                row["Organization Shipping Address"] = this.Organization.OrganizationShippingAddressLine1;
                row["Organization Shipping City"] = this.Organization.OrganizationShippingCity;
                row["Organization Shipping State/Province"] = (this.Organization.OrganizationShippingStateCode == null)
                    ? null
                    : this.Organization.OrganizationShippingStateCode.Trim()
                    ;
                row["Organization Shipping Zip Code"] = (this.Organization.OrganizationShippingZipCode == null)
                    ? null
                    : this.Organization.OrganizationShippingZipCode.Trim()
                ;
                row["Studio Lab Customer ID"] = this.Studio.LabCustomerID;
				row["Image Name"] = subject.ImageFileName;
                if(subject.DateAssigned == null)
                {
                    row["Date Assigned"] = "";
                } else{
                    string timeString = ((DateTime)subject.DateAssigned).ToString("yyyy/MM/dd - HH:mm:ss");

                    row["Date Assigned"] = timeString;
                }
                string ImageTypes = "";

                if (subject.IsPrimary)
                   ImageTypes += "Primary Image, ";
                foreach (SubjectImageType sit in subject.SubjectImageTypes)
                {
                    if(sit.IsAssigned)
                    {
                        logger.Info("about to map a Project Image type: " + sit.ProjectImageTypeID);
                        string desc = this.FlowProjectDataContext.ProjectImageTypes.First(it=>it.ProjectImageTypeID == sit.ProjectImageTypeID).ProjectImageTypeDesc;
                        logger.Info("done map a Project Image type: " + sit.ProjectImageTypeID);
                        ImageTypes += (desc + ", ");
                    }
                }
                if (ImageTypes.Length > 0)
                    ImageTypes = ImageTypes.Remove(ImageTypes.Length - 2, 2);

                //else
                //    row["Image Type"] = (imageType == null) ? "" : imageType.ProjectImageTypeDesc;

                row["Image Type"] = ImageTypes;

                row["Packages"] = packages;
                row["GSBackground"] = greenScreenBackground;
                if (!groupPhotoAsSeperatePose)
                {
                    if (subject.s.ImageList.Count(subi => subi.IsGroupImage) > 0)
                    {
                        logger.Info("about to map a Group Image");
                        row["Group Image"] = subject.s.ImageList.First(subi => subi.IsGroupImage).ImageFileName;
                        logger.Info("done map a Group Image");
                    }
                }
                row["Source Computer"] = subject.SourceComputer;
				subjectDataTable.Rows.Add(row);
			}

			imageFileList = imageList.Select(i => i.ImageFileName);
            BackgroundFileList = _backgroundFileList;

			return subjectDataTable;
		}

        public DataTable GetLimitedExportData(List<SubjectImagePlus> SubjectImageList, List<OrderPackage> CurrentOrderPackages, bool renderGreenscreen)
        {
           
            DataTable subjectDataTable = new DataTable();

            foreach (ProjectSubjectField field in this.ProjectSubjectFieldList)
            {
                try
                {
                    DataColumn column = new DataColumn(field.ProjectSubjectFieldDisplayName, typeof(string));
                    subjectDataTable.Columns.Add(column);
                }
                catch (Exception e)
                {
                    throw e;
                }
            }

            //subjectDataTable.Columns.Add(new DataColumn("TicketID", typeof(string)));

            DataColumn projectColumn;

            projectColumn = new DataColumn("Project Name");
            subjectDataTable.Columns.Add(projectColumn);

            projectColumn = new DataColumn("Event Name");
            subjectDataTable.Columns.Add(projectColumn);

            projectColumn = new DataColumn("Event Date");
            subjectDataTable.Columns.Add(projectColumn);

            projectColumn = new DataColumn("Event Comments");
            subjectDataTable.Columns.Add(projectColumn);

            projectColumn = new DataColumn("Project Guid");
            subjectDataTable.Columns.Add(projectColumn);

            bool skipProjPhotographer = true;
            if (!subjectDataTable.Columns.Contains("Photographer"))
            {
                projectColumn = new DataColumn("Photographer");
                subjectDataTable.Columns.Add(projectColumn);
                skipProjPhotographer = false;
            }

            projectColumn = new DataColumn("Organization Name");
            subjectDataTable.Columns.Add(projectColumn);

            projectColumn = new DataColumn("Organization Address");
            subjectDataTable.Columns.Add(projectColumn);

            projectColumn = new DataColumn("Organization City");
            subjectDataTable.Columns.Add(projectColumn);

            projectColumn = new DataColumn("Organization State/Province");
            subjectDataTable.Columns.Add(projectColumn);

            projectColumn = new DataColumn("Organization Zip Code");
            subjectDataTable.Columns.Add(projectColumn);

            projectColumn = new DataColumn("Organization Shipping Address");
            subjectDataTable.Columns.Add(projectColumn);

            projectColumn = new DataColumn("Organization Shipping City");
            subjectDataTable.Columns.Add(projectColumn);

            projectColumn = new DataColumn("Organization Shipping State/Province");
            subjectDataTable.Columns.Add(projectColumn);

            projectColumn = new DataColumn("Organization Shipping Zip Code");
            subjectDataTable.Columns.Add(projectColumn);

            projectColumn = new DataColumn("Studio Lab Customer ID");
            subjectDataTable.Columns.Add(projectColumn);

            projectColumn = new DataColumn("Image Name");
            subjectDataTable.Columns.Add(projectColumn);

            projectColumn = new DataColumn("Image Type");
            subjectDataTable.Columns.Add(projectColumn);

            projectColumn = new DataColumn("Packages");
            subjectDataTable.Columns.Add(projectColumn);

            projectColumn = new DataColumn("GSBackground");
            subjectDataTable.Columns.Add(projectColumn);

            projectColumn = new DataColumn("Group Image");
            subjectDataTable.Columns.Add(projectColumn);

            projectColumn = new DataColumn("Source Computer");
            subjectDataTable.Columns.Add(projectColumn);

            //Organization organization = this.FlowMasterDataContext.OrgList.FirstOrDefault(o => o.OrganizationID == this.OrganizationID);

            IEnumerable<SubjectImage> imageList;

            //always include GroupImage
            //if (useAllImages)
            //{
            //    imageList = this.SubjectList.SelectMany(s =>
            //        s.ImageList
            //    );
            //}
            //else if (imageType == null)
            //{
            //    imageList = this.SubjectList.SelectMany(s =>
            //        s.ImageList.Where(i => i.IsPrimary || i.IsGroupImage)
            //    );
            //}
            //else
            //{
            //    imageList = this.SubjectList.SelectMany(s =>
            //        s.ImageList.Where(i =>
            //            (i.SubjectImageTypeList.Any(t => t.ProjectImageTypeID == imageType.ProjectImageTypeID && t.IsAssigned) || i.IsGroupImage)
            //        )
            //    );
            //}

            //var subjectList = this.SubjectList.ToList().Join(
            //    imageList,
            //    s => s.SubjectID,
            //    i => i.SubjectID,
            //    (s, i) => new { s.SubjectData, i.ImageFileName, s.SubjectOrders, s, i.IsGroupImage, i.IsPrimary }
            //);


            foreach (SubjectImagePlus si in SubjectImageList)
            {
                try{
                //if ((!groupPhotoAsSeperatePose && subject.IsGroupImage) && !subject.IsPrimary)
                //    continue;
                
                //
                
                DataRow row = subjectDataTable.NewRow();

                foreach (ProjectSubjectField field in this.ProjectSubjectFieldList)
                {
                    row[field.ProjectSubjectFieldDisplayName] = si.SubjectImage.Subject.SubjectData[field.ProjectSubjectFieldDisplayName].Value;
                }
                //row["TicketID"] = si.Subject.TicketCode;

                string greenScreenBackground = "";
                string packages = "";
                //Subject trueSubject = subject.s;
                string imageName = si.NewImageName;
                string ImageTypes = "";

                if (si.SubjectImage.IsPrimary)
                    ImageTypes += "Primary Image, ";
                foreach (SubjectImageType sit in si.SubjectImage.SubjectImageTypes)
                {
                    if (sit.IsAssigned)
                    {
                        string desc = this.FlowProjectDataContext.ProjectImageTypes.First(it => it.ProjectImageTypeID == sit.ProjectImageTypeID).ProjectImageTypeDesc;
                        ImageTypes += (desc + ", ");
                    }
                }
                if (ImageTypes.Length > 0)
                    ImageTypes = ImageTypes.Remove(ImageTypes.Length - 2, 2);

                

                string computerName = si.SubjectImage.SourceComputer;
                    try{
                    List<OrderPackage> limitedOPs =  CurrentOrderPackages.Where(op => (!this.IsGreenScreen == true || (this.IsGreenScreen == true && op.OrderProducts.Count > 0 && op.OrderProducts[0].GreenScreenBackground == si.Background))
                    && op.SubjectOrder.Subject == si.SubjectImage.Subject
                    && op.OrderProducts.Any(prod => prod.OrderProductNodes != null
                        && prod.OrderProductNodes.Count > 0
                        && prod.OrderProductNodes.Any(node => (!si.IsRendered && node.SubjectImage != null
                            && node.SubjectImage.ImageFileName == si.SubjectImage.ImageFileName
                            && (node.OrderProduct.ProductPackageComposition != null && !node.OrderProduct.ProductPackageComposition.Renderlayout))
                        || (si.IsRendered && node.OrderProduct.UniqueProductImageName == si.NewImageName)
                        )
                    )
                    ).ToList();


                    if (limitedOPs != null && limitedOPs.Count > 0)
                        {
                foreach (OrderPackage op in limitedOPs)
                {
                    
                    greenScreenBackground = "";
                    if (!CurrentOrderPackages.Any(o => o.OrderPackageID == op.OrderPackageID))
                        continue;
                    imageName = si.NewImageName;

                    if (this.IsGreenScreen==true && !renderGreenscreen && op.DefaultBackgound != null && String.IsNullOrEmpty(greenScreenBackground))
                    {
                        //_backgroundFileList.Add(op.DefaultBackgound.FullPath);
                        greenScreenBackground = op.DefaultBackgound.FileName;
                        //if(renderGreenscreen && !si.IsRendered)
                        //   imageName = op.OrderProducts[0].OrderProductNodes.First(n => n.ImageQuixProductNodeType == 1).SubjectImage.GetDp2FileName(greenScreenBackground);
                    }


                    if (op.ProductPackage != null)
                    {
                        string options = "";

                        if (op.OrderProducts.Count > 0 && op.OrderProducts[0].OrderProductOptions.Count > 0)
                        {
                            foreach (OrderProductOption opo in op.OrderProducts[0].OrderProductOptions)
                            {
                                //(this.FlowCatalog.FlowCatalogOrderOptionList.Select(oo=> oo.ImageQuixCatalogOption).Where(oo=> oo.ResourceURL == oio.ResourceURL) as ImageQuixCatalogOption).PrimaryKey.ToString())
                                if (this.FlowMasterDataContext.ImageQuixOptions.Any(iqo => iqo.PrimaryKey == opo.ImageQuixOptionPk))
                                {
                                    options += ("," + this.FlowMasterDataContext.ImageQuixOptions.First(iqo => iqo.PrimaryKey == opo.ImageQuixOptionPk).ResourceURL);
                                }
                            }

                        }
                       

                        if (si.SubjectImage.OrderImageOptions.Count > 0)
                        {
                            foreach (OrderImageOption oio in si.SubjectImage.OrderImageOptions)
                            {
                                //(this.FlowCatalog.FlowCatalogOrderOptionList.Select(oo=> oo.ImageQuixCatalogOption).Where(oo=> oo.ResourceURL == oio.ResourceURL) as ImageQuixCatalogOption).PrimaryKey.ToString())
                                options += ("," + oio.ResourceURL);
                            }
                           
                        }
                        if (!string.IsNullOrEmpty(greenScreenBackground))
                        {
                            if (this.FlowMasterDataContext.ImageQuixCatalogOptions.Any(o => o.Label == greenScreenBackground))
                            {
                                options += ("," + this.FlowMasterDataContext.ImageQuixCatalogOptions.First(o => o.Label == greenScreenBackground).ResourceURL);
                            }
                            else
                            {
                                options += ("," + "nomap");
                            }
                        }
                        if (op.ProductPackage.IsAnyXPackage)
                        {
                            foreach (string u in op.Units.Split(";"))
                            {
                                string id = u.Split("-")[0];
                                if (id == op.ProductPackage.Map)
                                    continue;
                                packages = packages + id + "-1;";
                            }
                        }
                        else
                        {
                            packages = packages + op.ProductPackage.Map + options + "-" + op.Qty + ";";
                        }

                    }
                }
                        }
                    }
                    catch (Exception ex)
                    {
                        
                        throw ex;
                    }


                if (packages.Length > 0)
                    packages.Remove(packages.Length - 1);

                row["Project Name"] = this.FlowProjectName;
                row["Event Name"] = this.FlowProjectDesc;
                row["Event Date"] = this.DateCreated;
                row["Event Comments"] = this.Comments;
                row["Project Guid"] = this.FlowProjectGuid;
                if(!skipProjPhotographer)
                    row["Photographer"] = this.Photographer.FormalFullName;
                row["Organization Name"] = this.Organization.OrganizationName;
                row["Organization Address"] = this.Organization.OrganizationAddressLine1;
                row["Organization City"] = this.Organization.OrganizationCity;
                row["Organization State/Province"] = (this.Organization.OrganizationStateCode == null)
                    ? null
                    : this.Organization.OrganizationStateCode.Trim()
                    ;
                row["Organization Zip Code"] = (this.Organization.OrganizationZipCode == null)
                    ? null
                    : this.Organization.OrganizationZipCode.Trim()
                ;
                row["Organization Shipping Address"] = this.Organization.OrganizationShippingAddressLine1;
                row["Organization Shipping City"] = this.Organization.OrganizationShippingCity;
                row["Organization Shipping State/Province"] = (this.Organization.OrganizationShippingStateCode == null)
                    ? null
                    : this.Organization.OrganizationShippingStateCode.Trim()
                    ;
                row["Organization Shipping Zip Code"] = (this.Organization.OrganizationShippingZipCode == null)
                    ? null
                    : this.Organization.OrganizationShippingZipCode.Trim()
                ;
                row["Studio Lab Customer ID"] = this.Studio.LabCustomerID;
                row["Image Name"] = imageName;
                row["Image Type"] = ImageTypes;
                row["Packages"] = packages;
                row["GSBackground"] = greenScreenBackground;
                row["Group Image"] = "";
                //if (!groupPhotoAsSeperatePose)
                //{
                if (si.SubjectImage.Subject.ImageList.Count(subi => subi.IsGroupImage) > 0)
                    row["Group Image"] = si.SubjectImage.Subject.ImageList.First(subi => subi.IsGroupImage).ImageFileName;
                //}

                row["Source Computer"] = computerName;

                subjectDataTable.Rows.Add(row);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        

            //imageFileList = imageList.Select(i => i.ImageFileName);
            //BackgroundFileList = _backgroundFileList;

            return subjectDataTable;
        }


        public DataTable GetBasicDataExport(List<Subject> filteredSubjectList)
        {
            DataTable subjectDataTable = new DataTable();

            foreach (ProjectSubjectField field in this.ProjectSubjectFieldList)
            {
                if (field.ProjectSubjectFieldDisplayName != "SubjectID" &&
                    field.ProjectSubjectFieldDisplayName != "Package Summary")
                {
                    DataColumn column = new DataColumn(field.ProjectSubjectFieldDisplayName, typeof(string));

                    subjectDataTable.Columns.Add(column);
                }
                

            }

            DataColumn projectColumn;

            projectColumn = new DataColumn("Images");
            subjectDataTable.Columns.Add(projectColumn);

            projectColumn = new DataColumn("Packages");
            subjectDataTable.Columns.Add(projectColumn);


            List<Subject> subjectList = (filteredSubjectList == null) ? this.SubjectList.ToList() : filteredSubjectList;

            foreach (Subject subject in subjectList)
            {

                DataRow row = subjectDataTable.NewRow();

                foreach (ProjectSubjectField field in this.ProjectSubjectFieldList)
                {
                    if (field.ProjectSubjectFieldDisplayName != "SubjectID" &&
                    field.ProjectSubjectFieldDisplayName != "Package Summary")
                    {
                        row[field.ProjectSubjectFieldDisplayName] = subject.SubjectData[field.ProjectSubjectFieldDisplayName].Value;
                    }
                }
                List<String> images = new List<string>();
                foreach (SubjectImage si in subject.ImageList)
                    images.Add(si.ImageFileName);
                row["Images"] = String.Join(", ", images);

                List<String> packages = new List<string>();
                if(subject.SubjectOrder != null)
                {
                    foreach (OrderPackage op in subject.SubjectOrder.OrderPackages)
                        packages.Add(op.ProductPackageDesc);
                    row["Packages"] = String.Join(", ", packages);
                }


                subjectDataTable.Rows.Add(row);
            }
            return subjectDataTable;
        }

		/// <summary>
		/// Clears user-defined Subject field definitions (for data import)
		/// </summary>
		public void ClearSubjectTableColumns()
		{
			if (_flowProjectDataContext != null)
				_flowProjectDataContext.ClearSubjectTableColumns();
		}

		/// <summary>
		/// Creates user-defined Subject field defintions (from an external schema, during data import)
		/// </summary>
		public void CreateSubjectTableColumns()
		{
			if (_flowProjectDataContext != null)
				_flowProjectDataContext.CreateSubjectTableColumns();

			_flowProjectDataContext.LoadSubjectData();
		}

		/// <summary>
		/// Attempts to find a Subject based on supplied criteria
		/// </summary>
		/// <param name="keyFields"></param>
		public Subject FindSubjectByKeyFields(string filterExpression)
		{
			// Find all Subject rows matching criteria (should be unique)
			DataRow[] searchRows = this.FlowProjectDataContext.SubjectDataTable.Select(filterExpression);

			// If no matching Subject, return null
			if (searchRows == null || searchRows.Length == 0)
				return null;

			// Get the SubjectID for the Subject
			int subjectID = (int)searchRows[0]["SubjectID"];

			// Return the associated Subject
			return this.SubjectList.FirstOrDefault(s => s.SubjectID == subjectID);
		}

		/// <summary>
		/// Returns a formatted image filename based on filename formatting preferences
		/// </summary>
		/// <param name="subject"></param>
		/// <param name="originalImageFileName"></param>
		/// <returns>string</returns>
		public string GetFormattedImageFileName(Subject subject, string originalImageFileName)
		{
			return _imageFileNameFormatter.GetFormattedImageFileName(subject, originalImageFileName);
		}

		/// <summary>
		/// Returns a sequencing ID for application to assigned image filenames
		/// </summary>
		/// <returns>string</returns>
		public string GetNewImageSequenceID()
		{
			// Increment the image sequence ID; no need to issue SubmitChanges on datacontext
			// since this will be performed when the image is assigned to a subject record
			int currentImageSequenceID = ++this.FlowProjectDataContext.ProjectRecord.ImageSequenceID;

			return currentImageSequenceID.ToString().PadLeft(5, '0');
		}

		/// <summary>
		/// Returns the event trigger setting value corresponding to the supplied event trigger id
		/// </summary>
		/// <param name="eventTriggerID"></param>
		/// <returns>bool</returns>
		public bool GetEventTriggerSetting(int eventTriggerID)
		{
			ProjectEventTrigger eventTrigger = this.FlowProjectDataContext.ProjectEventTriggers
				.FirstOrDefault(et => et.EventTriggerID == eventTriggerID);

            if (eventTriggerID == 9)
            {
                //this is the auto assign hot folder image, so check for override
                if (this.FlowMasterDataContext.PreferenceManager.Capture.AutoAssignOverride == "Always")
                    return true;
                if (this.FlowMasterDataContext.PreferenceManager.Capture.AutoAssignOverride == "Never")
                    return false;
            }
            if (eventTrigger == null)
                return false;
				//throw new Exception("There is no Event Trigger with an ID of {" + eventTriggerID.ToString() + "} assigned to this project."); 

			return eventTrigger.Active;
		}

        internal void MergeGroupImages(GroupImage groupImage, string mergeTempImageDirPath)
        {
            // Attempt to locate and existing assignment of the image
            GroupImage rootGroupImage = this.FlowProjectDataContext.GroupImages
                .FirstOrDefault(i => i.GroupImageGuid == groupImage.GroupImageGuid);

            // If the image is not assigned to the subject record in the root project
            if (rootGroupImage == null)
            {
                // Clone the image record
                rootGroupImage = groupImage.Clone();

                // If an image with the same filename already exists in the project, rename
                //if (this.FlowProjectDataContext.GroupImages.Any(i => i.ImageFileName == rootGroupImage.ImageFileName))
                //{
                //    string currentFilePath = Path.Combine(mergeTempImageDirPath, rootGroupImage.ImageFileName);
                //    rootGroupImage.IncrementFileName();
                //    IOUtil.MoveFile(currentFilePath, rootGroupImage.ImageFileName);
                //}

                // Add the newly generated image record
                this.FlowProjectDataContext.GroupImages.InsertOnSubmit(rootGroupImage);

                //this.FlowProjectDataContext.GroupImageList.Add(rootGroupImage);
                //this.Save();
            }
            else
            {
                rootGroupImage.GroupImageDesc = groupImage.GroupImageDesc;
            }
            this.FlowProjectDataContext.SubmitChanges();
        }
		/// <summary>
		/// Merges a subject's data from a distibuted project into the consolidated (root) project
		/// </summary>
		/// <param name="subject"></param>
		internal void MergeSubject(Subject subject, string mergeTempImageDirPath, bool forceImageMerge)
		{
            //try
            //{
            //    this.FlowProjectDataContext.InitSubjectList();
            //}
            //catch (Exception ex)
            //{
            //    logger.Error("Merge in init subject list\n" + ex.Message);
            //    throw ex;
            //}

            Subject rootSubject = null;
            logger.Info("Find root subject");
            try
            {
                
                rootSubject = this.FlowProjectDataContext.SubjectList.FirstOrDefault(s => s.SubjectGuid == subject.SubjectGuid);
                if(rootSubject == null)
                    rootSubject = this.FlowProjectDataContext.SubjectList.FirstOrDefault(s => s.TicketCode == subject.TicketCode);
            }
            catch (Exception ex)
            {
                logger.Error("determining the root subject Part A\n" + ex.Message);
                throw ex;
            }


            //if(rootSubject == null)
            //    rootSubject = this.FlowProjectDataContext.Subjects.FirstOrDefault(s => s.TicketCode == subject.TicketCode);

            try
            {
                if (rootSubject == null)
                {
                    //////////////////////////////////////////////////////////
                    // NOTE: the following code is a temporary fix for the divergent subject guid issue;
                    // it should be removed at the earliest possible time
                    //rootSubject = this.FlowProjectDataContext.SubjectList.FirstOrDefault(s =>
                    //    s.SubjectID == subject.SubjectID &&
                    //    s["Last Name"] == subject["Last Name"] &&
                    //    s["First Name"] == subject["First Name"]
                    //);

                    if (rootSubject == null)
                    {
                        //////////////////////////////////////////////////////////

                        logger.Info("root subject is null, so lets copy the incoming subject to the rootproject");
                        rootSubject = subject.Clone(this);
                        if (rootSubject.TicketCode == null || rootSubject.TicketCode.Length < 1)
                            rootSubject.TicketCode = TicketGenerator.GenerateTicketString(8);

                        this.FlowProjectDataContext.Subjects.InsertOnSubmit(rootSubject);


                        this.FlowProjectDataContext.SubmitChanges();
                        rootSubject.SaveSubjectData();
                        // this.FlowProjectDataContext.SubmitChanges();
                        //this.SubjectList.Add(rootSubject);
                        //rootSubject.Save();


                        ///////////////////////////////////////////////////////////
                    }
                    else
                    {
                        logger.Info("found subject match in root project");
                        subject.SubjectGuid = rootSubject.SubjectGuid;
                        subject.FlowProjectDataContext.SubmitChanges();
                    }
                    ///////////////////////////////////////////////////////////
                }
                else
                {
                    

                    bool incomingDataIsNewer = false;

                    if (rootSubject.DateModified == null && subject.DateModified != null)
                    {
                        incomingDataIsNewer = true;
                        
                    }

                    if ((rootSubject.DateModified != null && subject.DateModified != null) && (DateTime.Compare((DateTime)rootSubject.DateModified, (DateTime)subject.DateModified) < 0))
                    {
                        incomingDataIsNewer = true;
                    }

                    if (incomingDataIsNewer)
                    {
                        logger.Info("incoming Data Is Newer");
                        //int tryCount = 0;
                       // bool tryAgain = true;
                       // while (tryAgain)
                        //{
                            //tryCount++;
                            try
                            {
                                foreach (SubjectDatum datum in subject.SubjectData)
                                {
                                    if (datum.Value == null || (datum.FieldType == typeof(string) && datum.Value.ToString().Length == 0))
                                        continue;

                                    rootSubject.SubjectData[datum.Field].Value = datum.Value;
                                }
                                rootSubject.Ready = subject.Ready;
                                rootSubject.Exclude = subject.Exclude;
                                rootSubject.DateModified = subject.DateModified;
                                rootSubject.Save(false);
                                //tryAgain = false;
                            }
                            catch (Exception exc)
                            {
                               // System.Windows.MessageBox.Show("There was an error that would have caused a crash, gona try 4 times");
                               // if(tryCount > 4)
                               //     tryAgain = false;

                               // Thread.Sleep(1000);
                                
                            }
                        //}
                    }
                    else
                    {
                        logger.Info("incoming Data Is OLDER (dont import it)");
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("determining the root subject Part B\n" + ex.Message);
                throw ex;
            }
            
			// Import subject images from the merge database
            logger.Info("Merging Subject Images");
            try
            {
               // MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                              // {
                                   foreach (SubjectImage subjectImage in subject.SubjectImages)
                                   {


                                       // Attempt to locate and existing assignment of the image
                                       SubjectImage rootSubjectImage = null;

                                       rootSubjectImage = rootSubject.ImageList
                                            .FirstOrDefault(i => i.SubjectImageGuid == subjectImage.SubjectImageGuid);


                                       try
                                       {
                                           // If the image is not assigned to the subject record in the root project
                                           if (rootSubjectImage == null)
                                           {
                                               // Clone the image record
                                               rootSubjectImage = subjectImage.Clone();
                                               rootSubject.FlagForMerge = true;
                                               // If an image with the same filename already exists in the project, rename
                                               //if(this.FlowProjectDataContext.SubjectImages.Any(i => i.ImageFileName == rootSubjectImage.ImageFileName))
                                               //{
                                               //    string currentFilePath = Path.Combine(mergeTempImageDirPath, rootSubjectImage.ImageFileName);
                                               //    rootSubjectImage.IncrementFileName();
                                               //    IOUtil.MoveFile(currentFilePath, rootSubjectImage.ImageFileName);
                                               //}

                                               // Add the newly generated image record
                                               //rootSubject.SubjectImages.Add(rootSubjectImage);

                                               //rootSubject.ImageList.Add(rootSubjectImage);
                                               rootSubjectImage.SubjectID = rootSubject.SubjectID;
                                               this.FlowProjectDataContext.SubjectImages.InsertOnSubmit(rootSubjectImage);
                                               //this.Save();
                                           }
                                       }
                                       catch (Exception ex)
                                       {
                                           logger.Error("Locating or Creating a Subject Image: " + subjectImage.ImageFileName + "\n" + ex.Message);
                                           throw ex;
                                       }

                                       rootSubjectImage.Subject = rootSubject;
                                       try
                                       {
                                           rootSubjectImage.MergeImageData(subjectImage, forceImageMerge);
                                       }
                                       catch (Exception ex)
                                       {
                                           logger.Error("Merging Subject Image Data: " + subjectImage.ImageFileName + "\n" + ex.Message);
                                           throw ex;
                                       }

                                       //MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                                                   //{
                                                       rootSubject.FlowProjectDataContext.SubmitChanges();
                                                   //}));
                                       //if the subjectImage has any image options, copy them over

                                       try
                                       {
                                           if (subjectImage.OrderImageOptions.Count > 0)
                                           {
                                               foreach (OrderImageOption oio in subjectImage.OrderImageOptions)
                                               {
                                                   if (!rootSubjectImage.OrderImageOptions.Any(o => o.ResourceURL == oio.ResourceURL))
                                                   {

                                                       //OrderImageOption newoio = new OrderImageOption();
                                                       FlowCatalogOption op = this.FlowCatalog.FlowCatalogOptions.FirstOrDefault(p => p.FlowCatalogOptionGuid == oio.FlowCatalogOptionGuid);
                                                       if (op == null)
                                                           continue;
                                                      // MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                                                       //{
                                                           OrderImageOption newoio = rootSubjectImage.AddCatalogOption(op);

                                                           //newoio.ResourceURL = oio.ResourceURL;
                                                           //newoio.TaxFactor = oio.TaxFactor;
                                                           //newoio.FlowCatalogOptionGuid = oio.FlowCatalogOptionGuid;
                                                           //newoio.SubjectImageID = rootSubjectImage.SubjectImageID;
                                                           //newoio.SubjectImage = rootSubjectImage;
                                                           newoio.DateAdded = oio.DateAdded;
                                                           newoio.LabOrderDate = oio.LabOrderDate;
                                                           newoio.LabOrderID = oio.LabOrderID;
                                                           newoio.WebOrderDate = oio.WebOrderDate;
                                                           newoio.WebOrderID = oio.WebOrderID;


                                                           rootSubject.SubjectOrder.RefreshOrderDetails();
                                                           this.SendPropertyChanged("SubjectOrder");
                                                           if (!rootSubject.SubjectOrders.Contains(rootSubject.SubjectOrder))
                                                               rootSubject.SubjectOrders.Add(rootSubject.SubjectOrder);

                                                           //rootSubjectImage.OrderImageOptions.Add(newoio);
                                                           //rootSubjectImage.FlowProjectDataContext.OrderImageOptions.InsertOnSubmit(newoio);
                                                           rootSubject.FlowProjectDataContext.SubmitChanges();
                                                           rootSubject.UpdateOrderImageOptionList();
                                                       //}));
                                                   }
                                               }
                                           }
                                       }
                                       catch (Exception ex)
                                       {
                                           logger.Error("Merging Order Image Options: " + subjectImage.ImageFileName + "\n" + ex.Message);
                                           throw ex;
                                       }



                                   }
                                   rootSubject.FlowProjectDataContext.SubmitChanges();
                               //}));
            }
            catch (Exception ex)
            {
                logger.Error("Iterate Subject Images: \n" + ex.Message);
                throw ex;
            }

            logger.Info("Merging Order Data");
			// Merge order data
            try
            {
                rootSubject.SetProductOrder(this.FlowCatalog);
            }
            catch (Exception ex)
            {
                logger.Error("Set Product Order: \n" + ex.Message);
                throw ex;
            }
            try{

			    rootSubject.MergeOrder(subject, this.FlowCatalog);

            }
            catch (Exception ex)
            {
                logger.Error("Merging Subject Order: " + rootSubject.TicketCode + "\n" + ex.Message);
                //throw ex;
                
            }

            Dictionary<int, int> imageMappingList = null;
            IEnumerable<OrderProductNode> assignedImageNodes = null;
            try
            {
                // Find all order product nodes which have an image assigned
                assignedImageNodes = rootSubject.SubjectOrder.OrderPackages
                    .SelectMany(pk => pk.OrderProducts
                        .SelectMany(pr => pr.OrderProductNodes.Where(n => n.MergeSubjectImageID.HasValue)
                    )
                );

                // Map the source image IDs to the root image IDs
                imageMappingList =
                    subject.ImageList.ToList().Join(
                        rootSubject.SubjectImages.ToList(),
                        i => i.SubjectImageGuid,
                        r => r.SubjectImageGuid,
                        (i, r) => new { Key = i.SubjectImageID, Value = r.SubjectImageID }
                    ).ToDictionary(k => k.Key, k => k.Value);

               
            }
            catch (Exception ex)
            {
                logger.Error("Mapping Source IMage IDs: " + rootSubject.TicketCode + "\n" + ex.Message);
                //throw ex; //ignore the error for now
            }

            try{
			    // Assign the corresponding subject image id to the node
			    foreach (OrderProductNode rootNode in assignedImageNodes)
			    {
                    //MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                    //                {
                                        if (imageMappingList.Keys.Contains(rootNode.MergeSubjectImageID.Value))
                                            rootNode.SubjectImageID = imageMappingList[rootNode.MergeSubjectImageID.Value];
                     //               }));
			    }
            }
            catch (Exception ex)
            {
                logger.Error("Assigning Nodes in Order Merge: " + rootSubject.TicketCode + "\n" + ex.Message);
                throw ex;
            }

            //go over the images and copy over any image tags
            logger.Info("Merging Subject Image Tags");

            try
            {
                // MainDispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                //               {
                foreach (SubjectImage subjectImage in subject.SubjectImages)
                {
                   
                                   // Attempt to locate an existing assignment of the image
                                   SubjectImage rootSubjectImage = rootSubject.SubjectImages
                                       .FirstOrDefault(i => i.SubjectImageGuid == subjectImage.SubjectImageGuid);
                                   if (rootSubjectImage != null)
                                   {
                                       //I just need to initialize this list
                                       object trash = rootSubjectImage.SubjectImageTypeList;

                                       if (subjectImage.IsPrimary)
                                           rootSubjectImage.IsPrimary = true;

                                       if (subjectImage.SubjectImageTypes != null)
                                       {
                                           foreach (SubjectImageType sit in subjectImage.SubjectImageTypes.Where(it => it.IsAssigned == true))
                                           {
                                               if (rootSubjectImage.SubjectImageTypes != null && rootSubjectImage.SubjectImageTypes.Any(si => si.ProjectImageType.ImageTypeDesc == sit.ProjectImageType.ImageTypeDesc))
                                                   rootSubjectImage.SubjectImageTypes.First(si => si.ProjectImageType.ImageTypeDesc == sit.ProjectImageType.ImageTypeDesc).IsAssigned = true;
                                           }
                                       }
                                   }
                               
                }
                      //         }));
            }
            catch (Exception ex)
            {
                logger.Error("Merging Subject Image Image types: " + rootSubject.TicketCode + "\n" + ex.Message);
                throw ex;
            }
            //make sure there are not 2 primary images:

            try
            {
                if (rootSubject.SubjectImages.Count(si => si.IsPrimary == true) > 1)
                {
                    List<SubjectImage> all = rootSubject.SubjectImages.Where(si => si.IsPrimary == true).ToList();
                    foreach (SubjectImage si in all)
                    {
                        si.IsPrimary = false;
                    }
                    //if there are any in the list that are not a group image, make the last one the primary
                    //otherwise, just make the last one the primary (regardless if its a group image
                    if (all.Any(si => si.IsGroupImage == false))
                        all.Last(si => si.IsGroupImage == false).IsPrimary = true;
                    else
                        all.Last().IsPrimary = true;

                }
                                                                                                                          
            }
            catch (Exception ex)
            {
                logger.Error("primary image flag integrety check: " + rootSubject.TicketCode + "\n" + ex.Message);
                throw ex;
            }

            try
            {
                if (this.FlowMasterDataContext.PreferenceManager.Application.IsFlowServer)
                {
                    rootSubject.FlagForMerge = false;
                }
			        
                rootSubject.FlowProjectDataContext.SubmitChanges();
            }
            catch (Exception ex)
            {
                logger.Error("Submitting Changes In Flow Project Merge Subject: " + rootSubject.TicketCode + "\n" + ex.Message);
                throw ex;
            }
		}

       

        public void RefreshAllImageExif(NotificationProgressInfo progressInfo)
		{
            if(progressInfo != null)
                progressInfo.Update("Refreshing Images for Subject {0} of {1}", 0, this.SubjectList.View.Count, 1, true);
				foreach (Subject subject in this.SubjectList.View)
				{
                    if (progressInfo != null)
                        progressInfo++;
                    foreach (SubjectImage i in subject.SubjectImages)
                    {
                        //i.RefreshImage();
                        //object o = i.Thumbnail;
                        int old = i.Orientation;
                        i.UpdateExifOrientation();
                        if (i.Orientation != old)
                        {
                            subject.DateModified = DateTime.Now;
                            subject.FlagForMerge = true;
                        }
                        //i.FlowImage.Refresh();
                        //i.RefreshImage();

                    }
				}
                this.FlowProjectDataContext.SubmitChanges();

		}


		/// <summary>
		/// 
		/// </summary>
		public void ClearAllOrders()
		{
			if (this.SubjectList.IsFiltered)
			{
                logger.Info("Clearing all orders... (subject list filtered)");

				List<SubjectImage> subjectImages = new List<SubjectImage>();

				foreach (Subject subject in this.SubjectList.View)
				{
                    try
                    {
                        if(subject.SubjectOrder.OrderPackages != null && subject.SubjectOrder.OrderPackages.Count() > 0)
                        {
                            subject.DateModified = DateTime.Now;
                            subject.FlagForMerge = true;
                        }
                        subject.ClearOrder();
                        subject.UpdatePackageSummary();
                        subjectImages.AddRange(subject.ImageList);
                    }
                    catch (Exception ex)
                    {
                        logger.ErrorException(String.Format("Failed to clear orders in subject '{0}'", subject.FormalFullName), ex);
                        throw;
                    }
				}

				IEnumerable<OrderProductNode> nodes = this.FlowProjectDataContext.OrderProductNodes.ToList()
					.Where(n => subjectImages.Contains(n.SubjectImage));

				foreach (OrderProductNode node in nodes)
				{
					node.SubjectImage = null;
//					this.FlowProjectDataContext.OrderProductNodes.DeleteOnSubmit(node);
				}
			}
			else
			{
                logger.Info("Clearing all orders... (subject list not filtered)");

				foreach (Subject subject in this.SubjectList)
				{
                    try
                    {
                        if (subject.SubjectOrder.OrderPackages != null && subject.SubjectOrder.OrderPackages.Count() > 0)
                        {
                            subject.DateModified = DateTime.Now;
                            subject.FlagForMerge = true;
                        }

                        subject.ClearOrder();
                        subject.UpdatePackageSummary();
                    }
                    catch (Exception ex)
                    {
                        logger.ErrorException(String.Format("Failed to clear orders in subject '{0}'", subject.FormalFullName), ex);
                        throw;
                    }
				}

				foreach (OrderProductNode node in this.FlowProjectDataContext.OrderProductNodes)
				{
					node.SubjectImage = null;
				}
//				this.FlowProjectDataContext.OrderProductNodes.DeleteAllOnSubmit(this.FlowProjectDataContext.OrderProductNodes);
			}

			this.Save();

			this.RefreshHasIncompleteProducts();

            logger.Info("Done clearing all orders");
		}

		/// <summary>
		/// Adds the supplied ProductPackage to the orders of all subjects in the project
		/// </summary>
		/// <param name="productPackage"></param>
        public void AddProductPackageToAllSubjects(ProductPackage productPackage, string PackageImageType)
		{
			using (
				this.LoadingProgressInfo = new NotificationProgressInfo(
					"Adding Packages...",
					0,
					this.SubjectList.View.Count,
					1,
					"{0} of {1}"
				)
			)
			{
				this.LoadingProgressInfo.Display();

                int x = 0;
				foreach (Subject subject in this.SubjectList.View)
				{
                    if (subject.ImageList != null && subject.ImageList.Count > 0)
                    {
                        x++;
                        subject.SetProductOrder(this.FlowCatalog);
                        subject.AddOrderPackage(productPackage, true, PackageImageType, null);
                        subject.UpdatePackageSummary();
                        subject.FlagForMerge = true;
                    }
					this.LoadingProgressInfo++;
				}

				this.LoadingProgressInfo.Update("Saving packages...");

				this.Save();

				this.LoadingProgressInfo.Complete("Packages added to " + x + " subjects");
			}
		}


        public IOrderedEnumerable<Subject> GetOrderSubjects(bool includeAllSubjects, string sortField1, string sortField2, string orderFilter, out List<OrderPackage> applicableOrderPackages)
        {

            applicableOrderPackages = new List<OrderPackage>();

            // Ensure the assigned FlowCatalog has the FlowMasterDataContext property assigned
            if (this.FlowCatalog != null)
                this.FlowCatalog.FlowMasterDataContext = _flowMasterDataContext;

           

            // Filter on subjects against which order records exist
            IEnumerable<Subject> subjectsWithOrderItems = this.SubjectList.Where(s => s.SubjectOrders.Count > 0);

        

            foreach (Subject subject in subjectsWithOrderItems)
            {
               
                subject.SetProductOrder(this.FlowCatalog);
            }


            subjectsWithOrderItems = subjectsWithOrderItems.Where(
                s => s.SubjectOrder.OrderPackageList.HasItems
            );


            IEnumerable<Subject> subjectsWithNewOrderItems = subjectsWithOrderItems.Where(
                s => s.SubjectOrder.NewOrderPackageList.HasItems
            );



           string oldOrderId = null;

            //
            //determine which subjects are getting ordered
            //
            //IOrderedEnumerable<Subject> applicableSubjects = (
            //    includeAllSubjects
            //        ? this.SubjectList.Where(s => s.ImageList.HasItems)
            //        : subjectsWithOrderItems
            //).OrderBy(s => true);

            //default is only subjects with OrderItems
            IOrderedEnumerable<Subject> applicableSubjects = subjectsWithOrderItems.OrderBy(s => true);
            if (orderFilter == "Current Filter Set")
                {
                    applicableSubjects = this.GetFilteredSubjectList().Where(s => s.ImageList.HasItems).OrderBy(s => true);
                }
            else if (includeAllSubjects)
            {
                //They want to include all subjects, so lets do it
                    applicableSubjects = this.SubjectList.Where(s => s.ImageList.HasItems).OrderBy(s => true);
            }
            else
            {
                //are we submitting a new order or an old order
                if (orderFilter == "New Orders")
                {
                    applicableSubjects = subjectsWithNewOrderItems.OrderBy(s => true);
                }
                if (orderFilter.Contains(" - "))
                {
                    oldOrderId = orderFilter.Split(" - ")[0];
                }
            }

            List<Subject> applicableSubjectsOverride = new List<Subject>();

            //determine which packages are getting ordered
            
            foreach (Subject thisSubject in applicableSubjects)
            {
                foreach (SubjectOrder so in thisSubject.SubjectOrders)
                {
                    if (!so.IsMarkedForDeletion)
                    {
                        foreach (OrderPackage op in so.OrderPackageList)
                        {
                            if (orderFilter == "New Orders")
                            {
                                if ((op.OrderHistories == null) || (op.OrderHistories.Count < 1))
                                    applicableOrderPackages.Add(op);
                            }
                            else if (oldOrderId != null && oldOrderId.Length > 1)
                            {
                                //applicable packages are those part of an old orderId
                                foreach (OrderHistory oh in op.OrderHistories)
                                {
                                    if (oh.OrderID == oldOrderId)
                                    {
                                        applicableOrderPackages.Add(op);
                                        applicableSubjectsOverride.Add(thisSubject);
                                    }
                                }
                            }
                            else
                            {
                                applicableOrderPackages.Add(op);
                            }
                        }
                    }
                }
            }

            //if this has a value, it means that we are resubmitting an old order
            if (applicableSubjectsOverride.Count() > 0)
            {
                applicableSubjects = applicableSubjects.Where(s => applicableSubjectsOverride.Contains(s)).OrderBy(s => true);
            }




            //
            // Perform sort operations on subjects
            //
            if (sortField1 != "[None]")
            {
                applicableSubjects = applicableSubjects.ThenBy(s => s.SubjectData[sortField1].ComparisonValue);

                if (sortField2 != "[None]")
                    applicableSubjects = applicableSubjects.ThenBy(s => s.SubjectData[sortField2].ComparisonValue);
            }

            applicableSubjects = applicableSubjects
                .ThenBy(s => s.SubjectData["Last Name"].ComparisonValue)
                .ThenBy(s => s.SubjectData["First Name"].ComparisonValue);


            return applicableSubjects;
        }

       
		/// <summary>
		/// Submits the project order after loading the project;
		/// calls the submission method after subject data is loaded
		/// </summary>
		/// <param name="flowMasterDataContext"></param>
		//public void SubmitOrder(FlowMasterDataContext flowMasterDataContext, bool includeAllSubjects)
		//{
		//    this.Connect(flowMasterDataContext);
        //    this.FlowProjectDataContext.SubjectDataLoaded += delegate { this.SubmitOrder(includeAllSubjects); };
		//}

		/// <summary>
		/// Submits the project order
		/// </summary>
        public Boolean SubmitOrder(bool includeAllSubjects, string saveOrderExportPath, string sortField1, string sortField2, string orderFilter, string reportFileName, string orderFormFileName, string catalogPackagesFileName, IOrderedEnumerable<Subject> applicableSubjects, List<OrderPackage> applicablePackages, bool renderGreenScreen, bool useOriginalFileNames, out string Message)
		{
            
            Message = "";
            this.LoadingProgressInfo = new NotificationProgressInfo("Preparing Order", "Gathering Images and Data...");
            this.LoadingProgressInfo.NotificationProgress = ObjectFactory.GetInstance<IShowsNotificationProgress>();
            this.LoadingProgressInfo.AllowCancel = true;
            this.LoadingProgressInfo.Display(true);

            if (this.LoadingProgressInfo.IsCanceled)
            {
                Message = "Submit Order Cancelled by user.";
                return false;
            }
            Organization assignedStudio = _flowMasterDataContext.StudioList.FirstOrDefault(s => s.OrganizationID == this.StudioID.Value);
	
            // Retrieve the XElement containing the studio info
            XElement custInfoXml = assignedStudio.GetCustomerInfo();


            // Create the product data xml doc
            XDocument productDataXDocument = this.GenerateProjectDataXml();
            XElement recordsXElement = productDataXDocument.Element("project").Element("records");

            // Create the order data xml doc
            XDocument orderXDocument = new XDocument();
            XElement orderElement = new XElement("Order");
  
            //determin which images to include:

            FlowObservableCollection<SubjectImage> tempImageList = ImageList;

            List<ZipFileMapping> imageFileList = null;

            List<GroupImage> tempGroupImages = null;
            List<KeyValuePair<ZipFileMapping, XElement>> groupImageFileList = new List<KeyValuePair<ZipFileMapping, XElement>>();

            if (renderGreenScreen)
            {
                this.LoadingProgressInfo.Update("Rendering Green Screen Images... (This could take a while)");
            }
            // Retrieve the list of image files involved in this order
            // Add all images in project to list if include all subjects selected
            if (includeAllSubjects)
            {
                //imageFileList = this.ImageList
                //    .Select(i => new ZipFileMapping(FlowContext.Combine(this.ImageDirPath, i.GetImagePath(renderGreenScreen)), i.Dp2FileName))
                //    .Distinct(new ZipFileMappingComparer()).ToList()
                //;

                imageFileList = new List<ZipFileMapping>();
                foreach (SubjectImage si in this.ImageList)
                {
                    if (si.Subject.SubjectOrder.OrderPackages.Count(op => op.IsOnlineOrder != true) == 0)
                    {
                        ZipFileMapping zf = new ZipFileMapping(FlowContext.Combine(this.ImageDirPath, si.GetImagePath(renderGreenScreen)), si.Dp2FileName(useOriginalFileNames));
                        if (!imageFileList.Contains(zf))
                            imageFileList.Add(zf);
                        continue;
                    }
                    foreach (OrderPackage op in si.Subject.SubjectOrder.OrderPackages.Where(op=>op.IsOnlineOrder != true))
                    {
                        foreach (OrderProduct prod in op.OrderProducts)
                        {
                            foreach (OrderProductNode node in prod.OrderProductNodes)
                            {
                                ZipFileMapping zfm;
                                if (renderGreenScreen && prod.GreenScreenBackground != null && File.Exists(prod.GreenScreenBackground))
                                    zfm = new ZipFileMapping(FlowContext.Combine(this.ImageDirPath, node.SubjectImage.GetImagePath(renderGreenScreen, prod.GreenScreenBackground)), node.SubjectImage.GetDp2FileName(prod.GreenScreenBackground, useOriginalFileNames, renderGreenScreen));
                                else
                                    zfm = new ZipFileMapping(FlowContext.Combine(this.ImageDirPath, node.SubjectImage.GetImagePath(renderGreenScreen)), node.SubjectImage.Dp2FileName(useOriginalFileNames));


                                if (!imageFileList.Contains(zfm))
                                    imageFileList.Add(zfm);
                            }
                        }
                    }
                }


                tempGroupImages = this.FlowProjectDataContext.GroupImageList.Distinct().ToList();
            }
            else
            {
               
                //foreach(OrderProductNode opn in this.FlowProjectDataContext.OrderProductNodes)
                //{
                //    if(applicablePackages.Contains(opn.OrderProduct.OrderPackage))
                //    {
                //        thisList.Add(opn);
                //    }
                //}
                FlowObservableCollection<OrderProductNode> thisList = new FlowObservableCollection<OrderProductNode>();
                foreach (OrderPackage op in applicablePackages)
                {
                    foreach (OrderProduct oprod in op.OrderProducts)
                    {
                        foreach (OrderProductNode opn in oprod.OrderProductNodes)
                            thisList.Add(opn);
                    }
                }

                //imageFileList =
                //    this.ImageList.Join(
                //        thisList,
                //        si => si.SubjectImageID,
                //        on => on.SubjectImageID,
                //        (si, on) => si 
                //    )
                //    .Distinct()
                //    .Where( si => applicableSubjects.Contains(si.Subject))
                //    .Select(si => new ZipFileMapping(FlowContext.Combine(this.ImageDirPath, si.GetImagePath(renderGreenScreen, si.Subject.SubjectOrder.)), si.GetDp2FileName()))
                //    .Distinct(new ZipFileMappingComparer())
                //    .ToList();

                 //get a list of images for this order
                //List<SubjectImage> OrderSubjectImages = new List<SubjectImage>();
                imageFileList = new List<ZipFileMapping>();
                int counter = 0;
                int all = applicablePackages.Count;
                foreach (OrderPackage op in applicablePackages)
                {
                    counter++;
                    foreach (OrderProduct prod in op.OrderProducts)
                    {
                        foreach (OrderProductNode node in prod.OrderProductNodes)
                        {
                            if (node.SubjectImage != null && node.ImageQuixProductNodeType == 1) // 1 is the image nodetype
                            {
                                ZipFileMapping zfm;
                                if (renderGreenScreen && prod.GreenScreenBackground != null && File.Exists(prod.GreenScreenBackground))
                                {
                                    this.LoadingProgressInfo.Update(counter + " of " + all + " - Rendering Green Screen Images... " + node.SubjectImage.ImageFileName);
                                    zfm = new ZipFileMapping(FlowContext.Combine(this.ImageDirPath, node.SubjectImage.GetImagePath(renderGreenScreen, prod.GreenScreenBackground)), node.SubjectImage.GetDp2FileName(prod.GreenScreenBackground, useOriginalFileNames, renderGreenScreen));
                                }
                                else if (renderGreenScreen && node.SubjectImage.GreenScreenBackground != null && File.Exists(node.SubjectImage.GreenScreenBackground))
                                {
                                    this.LoadingProgressInfo.Update(counter + " of " + all + " - Rendering Green Screen Images... " + node.SubjectImage.ImageFileName);
                                    zfm = new ZipFileMapping(FlowContext.Combine(this.ImageDirPath, node.SubjectImage.GetImagePath(renderGreenScreen, node.SubjectImage.GreenScreenBackground)), node.SubjectImage.GetDp2FileName(node.SubjectImage.GreenScreenBackground, useOriginalFileNames, renderGreenScreen));
                                }
                                else if (renderGreenScreen && this.DefaultGreenScreenBackground != null && File.Exists(this.DefaultGreenScreenBackground))
                                {
                                    this.LoadingProgressInfo.Update(counter + " of " + all + " - Rendering Green Screen Images... " + node.SubjectImage.ImageFileName);
                                    zfm = new ZipFileMapping(FlowContext.Combine(this.ImageDirPath, node.SubjectImage.GetImagePath(renderGreenScreen, this.DefaultGreenScreenBackground)), node.SubjectImage.GetDp2FileName(this.DefaultGreenScreenBackground, useOriginalFileNames, renderGreenScreen));
                                }
                                else if (renderGreenScreen && this.FlowProjectDataContext.FlowMasterDataContext.GreenScreenBackgrounds.Count > 0 && this.FlowProjectDataContext.FlowMasterDataContext.GreenScreenBackgrounds[0] != null && File.Exists(this.FlowProjectDataContext.FlowMasterDataContext.GreenScreenBackgrounds[0].FullPath))
                                {
                                    this.LoadingProgressInfo.Update(counter + " of " + all + " - Rendering Green Screen Images... " + node.SubjectImage.ImageFileName);
                                    zfm = new ZipFileMapping(FlowContext.Combine(this.ImageDirPath, node.SubjectImage.GetImagePath(renderGreenScreen, this.FlowProjectDataContext.FlowMasterDataContext.GreenScreenBackgrounds[0].FullPath)), node.SubjectImage.GetDp2FileName(this.FlowProjectDataContext.FlowMasterDataContext.GreenScreenBackgrounds[0].FullPath, useOriginalFileNames, renderGreenScreen));
                                }
                                else
                                    zfm = new ZipFileMapping(FlowContext.Combine(this.ImageDirPath, node.SubjectImage.GetImagePath(renderGreenScreen)), node.SubjectImage.Dp2FileName(useOriginalFileNames));


                                if (!imageFileList.Any(a => a.DestinationFileName == zfm.DestinationFileName))
                                    imageFileList.Add(zfm);
                            }
                            else
                            {
                                bool oops = true;
                            }
                        }
                    }
                }


                tempGroupImages =
                    this.ImageList
                    .Where(si => applicableSubjects.Contains(si.Subject) && si.IsGroupImage)
                    .Select(i => i.GroupImage)
                    .Distinct()
                    .ToList();
            }

            if (this.LoadingProgressInfo.IsCanceled)
            {
                Message = "Submit Order Cancelled by user.";
                return false;
            }
            foreach(GroupImage gi in tempGroupImages)
            {
                string groupImageDesc = "";
                if (gi.GroupImageDesc != null && gi.GroupImageDesc.Length > 0)
                    groupImageDesc = gi.GroupImageDesc;
                else
                    groupImageDesc = gi.ImageFileName;

                XElement imageElement = new XElement("image");
                imageElement.AddNewXAttribute("filename", gi.ImageFileName);
                imageElement.AddNewXAttribute("description", groupImageDesc);
                string orderImageOptions = "";
               
                if (gi.CropH  != null && (gi.CropH < 0 || double.IsNaN((double)gi.CropH)))
                    gi.CropH = 0;
                if (gi.CropL == null) gi.CropL = 0;
                if (gi.CropT == null) gi.CropT = 0;
                if (gi.CropW == null) gi.CropW = 0;
                if (gi.CropH == null) gi.CropH = 0;

                double _cropX = double.Parse(((double)gi.CropL * 100).ToString("N5"));
                double _cropY = double.Parse(((double)gi.CropT * 100).ToString("N5"));
                double _cropW = double.Parse(((double)gi.CropW * 100).ToString("N5"));
                double _cropH = double.Parse(((double)gi.CropH * 100).ToString("N5"));
                imageElement.AddNewXAttribute("crop-x", (_cropX + (_cropW / 2)));
                imageElement.AddNewXAttribute("crop-y", (_cropY + (_cropH / 2)));
                imageElement.AddNewXAttribute("crop-w", _cropW);
                imageElement.AddNewXAttribute("crop-h", _cropH);

                if(renderGreenScreen)
                    imageElement.AddNewXAttribute("orientation", 0);
                else
                    imageElement.AddNewXAttribute("orientation", gi.Orientation);

                imageElement.AddNewXAttribute("image-option-selections", orderImageOptions);

                groupImageFileList.Add(new KeyValuePair<ZipFileMapping, XElement>(new ZipFileMapping(FlowContext.Combine(this.ImageDirPath, gi.ImagePath), gi.ImageFileName), imageElement));
            }

            if (this.LoadingProgressInfo.IsCanceled)
            {
                Message = "Submit Order Cancelled by user.";
                return false;
            }
			// Create the xml elements and image file list for each order
			int subjectCount = applicableSubjects.Count();
			this.LoadingProgressInfo.Update("Generating subject order XML {0} of {1}", 0, subjectCount, 1, true);
			foreach (Subject subject in applicableSubjects)
			{
				if (this.LoadingProgressInfo.IsCanceled)
				{
                    Message = "Canceled by User";
				    return false;
                }

                subject.SupplyOrderProjectData(recordsXElement, imageFileList, false, applicablePackages, renderGreenScreen, useOriginalFileNames);

				//if((!includeAllSubjects) || subjectsWithOrderItems.Contains(subject))
                subject.SupplyOrderData(orderElement, applicablePackages, renderGreenScreen, useOriginalFileNames);

               

				this.LoadingProgressInfo++;
			}

			orderXDocument.Add(orderElement);



            if (this.LoadingProgressInfo.IsCanceled)
            {
                Message = "Submit Order Cancelled by user.";
                return false;
            }

			this.LoadingProgressInfo.Complete("Order preparation complete.");

            JsonArray orderOptionResourceURLs = new JsonArray();
            foreach (OrderOrderOption ooo in this.FlowProjectDataContext.OrderOrderOptions)
            {
                orderOptionResourceURLs.Add(ooo.ResourceURL);
            }
            foreach (OrderShippingOption ooo in this.FlowProjectDataContext.OrderShippingOptions)
            {
                orderOptionResourceURLs.Add(ooo.ResourceURL);
            }
            foreach (OrderPackagingOption ooo in this.FlowProjectDataContext.OrderPackagingOptions)
            {
                orderOptionResourceURLs.Add(ooo.ResourceURL);
            }

            this.LoadingProgressInfo.Complete("Done preparing Order Info.");
            


            Emailer mail = null;
            try
            {
                if (this.FlowMasterDataContext.PreferenceManager.Application.LabEmail != null && this.FlowMasterDataContext.PreferenceManager.Application.LabEmail.Length > 2)
                {
                    string studioemail = null;
                    if (FlowMasterDataContext.Organizations.First(o => o.OrganizationID == (int)this.StudioID).OrganizationContactEmail != null &&
                        FlowMasterDataContext.Organizations.First(o => o.OrganizationID == (int)this.StudioID).OrganizationContactEmail.Length > 1)
                            studioemail = FlowMasterDataContext.Organizations.First(o => o.OrganizationID == (int)this.StudioID).OrganizationContactEmail;

                    string studioName = FlowMasterDataContext.Organizations.First(o => o.OrganizationID == (int)this.StudioID).OrganizationName;
                    mail = new Emailer("Flow Product Order Uploaded - " + studioName + " -- DO NOT REPLY",
                        "New Flow Product Order Uploaded\n\nStudio: " + studioName + "\nProject: " + this.FlowProjectName + "\n\n",
                        false,
                        this.FlowMasterDataContext.PreferenceManager.Application.LabEmail,
                        "flow@flowadmin.com",
                        studioemail);
                    
                    
                }
            }
            catch (Exception)
            {

                Message = "There was a problem sending email notification to the Lab regarging this order";
                return false;
            }


            JsonObject SubmitReturnObject;
            try
            {
                
                SubmitReturnObject = ImageQuixUtil.SubmitOrder(
                        imageFileList,
                        productDataXDocument,
                        orderXDocument,
                        custInfoXml,
                        _flowMasterDataContext.PreferenceManager.Upload,
                        saveOrderExportPath,
                        this.FlowProjectName,
                        reportFileName,
                        orderFormFileName,
                        catalogPackagesFileName,
                        orderOptionResourceURLs,
                        groupImageFileList,
                        mail 
                    );
            }
            catch (Exception e)
            {
                
                 Message = "There was an error connecting to the order server.\nThe server may be down or your connection setting may be incorrect.\nPlease check your connection settings.\nPreferences -> Remote Connection -> Lab Catalog\n\n" + e.Message;
                return false;
            }
            
            //
            //take care of marking orders as submitted for Order History
            //
            try
            {
                string orderID = SubmitReturnObject["orderID"].ToString();
                //string orderLocation = SubmitReturnObject["orderLocation"].ToString();
                DateTime submitTimeStamp = DateTime.Now;

                OrderHistoryMaster historyMaster = new OrderHistoryMaster();
                historyMaster.OrderID = orderID;
                historyMaster.OrderSubmittedDate = submitTimeStamp;
                FlowProjectDataContext.OrderHistoryMasters.InsertOnSubmit(historyMaster);


                foreach (OrderPackage thisOrderpackage in applicablePackages)
                {
                    if (applicablePackages.Contains(thisOrderpackage))
                    {
                        OrderHistory orderHistory = new OrderHistory();
                        orderHistory.OrderID = orderID;
                        orderHistory.OrderPackageID = thisOrderpackage.OrderPackageID;
                        orderHistory.OrderSubmittedDate = submitTimeStamp;
                        thisOrderpackage.OrderHistories.Add(orderHistory);
                    }
                }

                FlowProjectDataContext.SubmitChanges();

                //send email
                //try
                //{
                //    if (this.FlowMasterDataContext.PreferenceManager.Application.LabEmail != null && this.FlowMasterDataContext.PreferenceManager.Application.LabEmail.Length > 2)
                //    {
                //        string studioName = FlowMasterDataContext.Organizations.First(o => o.OrganizationID == (int)this.StudioID).OrganizationName;
                //        Emailer mail = new Emailer("Flow Product Order Uploaded - " + studioName,
                //            "New Flow Product Order Uploaded\n\nStudio: " + studioName + "\nProject: " + this.FlowProjectName + "\n\n",
                //            false,
                //            this.FlowMasterDataContext.PreferenceManager.Application.LabEmail,
                //            "flow@flowadmin.com");

                //        string customFileName = orderFormFileName.Replace(".pdf", "_" + orderID + ".pdf");
                //        mail.AddAttachment(customFileName);
                //        mail.Send();
                //    }
                //}
                //catch (Exception)
                //{
                    
                //     Message="There was a problem sending email notification to the Lab regarging this order";
                //    return false;
                //}
            }
            catch (Exception e)
            {
                //Show an Error Indicating that the order didnt post
                Message="There was a problem posting the order, order did not post";
                return false;
            }
            return true;
		}

      

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public XDocument GenerateProjectDataXml()
		{
			XDocument projectDataXDocument = new XDocument();
	
			// Create the project element
			XElement projectElement = new XElement("project");

			// Create the attributes for the project element
            string guidValue = this.FlowProjectTemplateShortDescription;
            if (guidValue == null || guidValue.Length < 1)
                guidValue = this.FlowProjectTemplateGuid10;
			projectElement.AddNewXAttribute("guid", guidValue);
			projectElement.AddNewXAttribute("studioID", this.Customer.LabCustomerID);
			projectElement.AddNewXAttribute("studioName", this.Customer.OrganizationName);

			// Create the fields element
			XElement fieldsElement = new XElement("fields");

			// Add the field element for the pk (primary key) field
			XElement pkFieldElement = new XElement("SubjectID");
			pkFieldElement.AddNewXAttribute("key","primary");
			fieldsElement.Add(pkFieldElement);

			// Iterate through the subject field list...
			foreach (ProjectSubjectField field in this.ProjectSubjectFieldList)
			{
				// Add the field element, assigning the fieldname as the element name
				XElement fieldElement = new XElement(field.ProjectSubjectFieldDesc);

				fieldsElement.Add(fieldElement);
			}
            //for(int i=1;i<11;i++)
            //    fieldsElement.Add(new XElement("PackageOrder" + i));
            fieldsElement.Add(new XElement("OrderSummary"));

            

            fieldsElement.Add(new XElement("GroupImage"));

			projectElement.Add(fieldsElement);

			// Create the records element
			projectElement.Add(new XElement("records"));

            fieldsElement.Add(new XElement("organization_name"));
            fieldsElement.Add(new XElement("project_name"));
            fieldsElement.Add(new XElement("project_event"));

			projectDataXDocument.Add(projectElement);

			return projectDataXDocument;
		}

		public void RefreshPropertyBinding(string propertyName)
		{
			this.SendPropertyChanged(propertyName);
		}


		#endregion // METHODS


		#region IMPLICIT CONVERSIONS

		/// <summary>
		/// Converts a project control record to a proper FlowProject object
		/// </summary>
		/// <param name="sourceProject"></param>
		/// <returns></returns>
		public static implicit operator FlowProject(Control_FlowProject sourceProject)
		{
			FlowProject retVal = new FlowProject();

			retVal.FlowProjectGuid = sourceProject.FlowProjectGuid;
			retVal.OrganizationID = sourceProject.OrganizationID;
			retVal.StudioID = sourceProject.StudioID;
			retVal.OwnerUserID = sourceProject.OwnerUserID;
			retVal.FlowProjectName = sourceProject.FlowProjectName;
			retVal.FlowProjectType = sourceProject.FlowProjectType;
			retVal.FlowProjectDesc = sourceProject.FlowProjectDesc;
			retVal.ImageFormatTypeID = sourceProject.ImageFormatTypeID;
			retVal.ImageFormatSeparatorChar = sourceProject.ImageFormatSeparatorChar;
			retVal.ImageFormatField1 = sourceProject.ImageFormatField1;
			retVal.ImageFormatField2 = sourceProject.ImageFormatField2;
			retVal.ImageFormatField3 = sourceProject.ImageFormatField3;
			retVal.Comments = sourceProject.Comments;
			retVal.DateCreated = sourceProject.DateCreated;
			retVal.DateModified = sourceProject.DateModified;
            retVal.FlowVersion = sourceProject.FlowVersion;
            retVal.FlowMasterDBVersion = sourceProject.FlowMasterDBVersion;
            retVal.FlowProjectTemplateVersion = sourceProject.FlowProjectTemplateVersion;
            retVal.FlowProjectTemplateGuid = sourceProject.FlowProjectTemplateGuid;
            retVal.FlowProjectTemplateShortDescription = sourceProject.FlowProjectTemplateShortDescription;
            retVal.IsGreenScreen = sourceProject.IsGreenScreen;
            retVal.ShippingMethod = sourceProject.ShippingMethod;
            retVal.DefaultGreenScreenBackground = sourceProject.DefaultGreenScreenBackground;
            retVal.DueDate = sourceProject.DueDate;
            retVal.PrincipalName = sourceProject.PrincipalName;
            retVal.ProjectType = sourceProject.ProjectType;
            retVal.PayableTo = sourceProject.PayableTo;
            retVal.GroupImageFlags = sourceProject.GroupImageFlags;

			return retVal;
		}

		/// <summary>
		/// Converts a project control record to a proper FlowProject object
		/// </summary>
		/// <param name="sourceProject"></param>
		/// <returns></returns>
		public static implicit operator Control_FlowProject(FlowProject sourceProject)
		{
			Control_FlowProject retVal = new Control_FlowProject();

			retVal.FlowProjectGuid = sourceProject.FlowProjectGuid;
			retVal.OrganizationID = sourceProject.OrganizationID;
			retVal.StudioID = sourceProject.StudioID;
			retVal.OwnerUserID = sourceProject.OwnerUserID;
			retVal.FlowProjectName = sourceProject.FlowProjectName;
			retVal.FlowProjectType = sourceProject.FlowProjectType;
			retVal.FlowProjectDesc = sourceProject.FlowProjectDesc;
			retVal.ImageFormatTypeID = sourceProject.ImageFormatTypeID;
			retVal.ImageFormatSeparatorChar = sourceProject.ImageFormatSeparatorChar;
			retVal.ImageFormatField1 = sourceProject.ImageFormatField1;
			retVal.ImageFormatField2 = sourceProject.ImageFormatField2;
			retVal.ImageFormatField3 = sourceProject.ImageFormatField3;
			retVal.Comments = sourceProject.Comments;
			retVal.DateCreated = sourceProject.DateCreated;
			retVal.DateModified = sourceProject.DateModified;
            retVal.FlowVersion = sourceProject.FlowVersion;
            retVal.FlowMasterDBVersion = sourceProject.FlowMasterDBVersion;
            retVal.FlowProjectTemplateVersion = sourceProject.FlowProjectTemplateVersion;
            retVal.FlowProjectTemplateGuid = sourceProject.FlowProjectTemplateGuid;
            retVal.FlowProjectTemplateShortDescription = sourceProject.FlowProjectTemplateShortDescription;
            retVal.IsGreenScreen = sourceProject.IsGreenScreen;
            retVal.ShippingMethod = sourceProject.ShippingMethod;
            retVal.DefaultGreenScreenBackground = sourceProject.DefaultGreenScreenBackground;
            retVal.DueDate = sourceProject.DueDate;
            retVal.PrincipalName = sourceProject.PrincipalName;
            retVal.ProjectType = sourceProject.ProjectType;
            retVal.PayableTo = sourceProject.PayableTo;
            retVal.GroupImageFlags = sourceProject.GroupImageFlags;
			return retVal;
		}

		#endregion



        public bool inProgress { get; set; }

        
        private GreenScreenBackground _defaultGreenScreenBackgroundObject{get;set;}
        public GreenScreenBackground DefaultGreenScreenBackgroundObject
        {
            get
            {
                if (_defaultGreenScreenBackgroundObject == null)
                {
                    if (!String.IsNullOrEmpty(this.DefaultGreenScreenBackground) && File.Exists(this.DefaultGreenScreenBackground))
                        _defaultGreenScreenBackgroundObject = new GreenScreenBackground(this.DefaultGreenScreenBackground);
                }
                return _defaultGreenScreenBackgroundObject;
            }
            set
            {
                _defaultGreenScreenBackgroundObject = value;
                this.DefaultGreenScreenBackground = _defaultGreenScreenBackgroundObject.FullPath;
            }

        }

        public string DefaultGSxml =
            @"﻿<?xml version=""1.0"" encoding=""utf-8""?>\r\n<SelectionConfiguration xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">\r\n  <ForegroundTopLeft>\r\n    <X>0</X>\r\n    <Y>0</Y>\r\n  </ForegroundTopLeft>\r\n  <ForegroundScale>1</ForegroundScale>\r\n  <ForegroundClip>\r\n    <FillRule>Nonzero</FillRule>\r\n    <Figures />\r\n  </ForegroundClip>\r\n  <RenderedClipNegative>\r\n    <FillRule>Nonzero</FillRule>\r\n    <Figures />\r\n  </RenderedClipNegative>\r\n  <ColorAndBackgroundConfiguration>\r\n    <BackgroundDefinitionRed>86</BackgroundDefinitionRed>\r\n    <BackgroundDefinitionGreen>191</BackgroundDefinitionGreen>\r\n    <BackgroundDefinitionBlue>121</BackgroundDefinitionBlue>\r\n    <BackgroundCorrection>0</BackgroundCorrection>\r\n    <ControlPoints>\r\n      <Point>\r\n        <X>76</X>\r\n        <Y>1</Y>\r\n      </Point>\r\n      <Point>\r\n        <X>105</X>\r\n        <Y>87</Y>\r\n      </Point>\r\n    </ControlPoints>\r\n    <DropColors>\r\n      <Tolerance>24</Tolerance>\r\n      <ColorsSerializationString />\r\n    </DropColors>\r\n    <KeepColors>\r\n      <Tolerance>24</Tolerance>\r\n      <ColorsSerializationString />\r\n    </KeepColors>\r\n    <BackgroundImagePath />\r\n    <ProcessDropout>true</ProcessDropout>\r\n    <SoftenEdges>true</SoftenEdges>\r\n  </ColorAndBackgroundConfiguration>\r\n  <RotateFlipType>RotateNoneFlipNone</RotateFlipType>\r\n</SelectionConfiguration>";


        public bool initingSubjects { get; set; }

        public bool ExportInProgress { get; set; }




        public bool isDp2 { 
            get
            {
                if (this.FlowMasterDataContext.PreferenceManager.Upload.CatalogRequest.ImageQuixBaseUrl.ToLower().Contains(".pud") ||
                    this.FlowMasterDataContext.PreferenceManager.Upload.CatalogRequest.ImageQuixBaseUrl.ToLower().Contains("plic.io"))
                    return false;

                return true;
            }
        }

        public bool isPUD
        {
            get
            {
                if (this.FlowMasterDataContext.PreferenceManager.Upload.CatalogRequest.ImageQuixBaseUrl.ToLower().Contains(".pud"))
                    return true;

                return false;
            }
        }

        public bool isPLIC
        {
            get
            {
                if (this.FlowMasterDataContext.PreferenceManager.Upload.CatalogRequest.ImageQuixBaseUrl.ToLower().Contains("plic.io"))
                    return true;

                return false;
            }
        }

        public ProjectGallery Gallery {
            get
            {
               return  this.FlowProjectDataContext.ProjectGalleries.FirstOrDefault();
            }
            }

        public OnlineGallery OnlineGallery
        {
            get
            {
                return this.FlowProjectDataContext.OnlineGalleries.FirstOrDefault();
            }
        }

        public int OrderPackageCount {
            get
            {
                int rvalue = 0;
                foreach (Subject s in this.SubjectList)
                {
                    if (s.SubjectOrder != null && s.SubjectOrder.OrderPackageList != null)
                        rvalue += s.SubjectOrder.OrderPackageList.Count();
                }
                return rvalue;
            }
        }

        public void Disconnect()
        {
            _flowProjectDataContext.Dispose();
        }

        public Subject LastSubjectViewed { get; set; }

        public DataSet GetCustomReportDataSet(bool GeneratePrimaryThumbs)
        {
            //
            //Define columns for ALL tables
            //

            DataTable dt = new DataTable("Subject");
            DataTable dt2 = new DataTable("SubjectImage");
            DataTable dt3 = new DataTable("SubjectOrder");
            DataTable dt4 = new DataTable("GroupImages");

            dt.Columns.Add("TicketCodeBarCode");
            dt.Columns.Add("FormalFullName");
            dt.Columns.Add("ProjectOrganizationName");
            dt.Columns.Add("ProjectName");
            dt.Columns.Add("EventName");
            dt.Columns.Add("PhotoDate");
            dt.Columns.Add("DueDate");
            dt.Columns.Add("CatalogName");
            dt.Columns.Add("Photographer");
            dt.Columns.Add("PrimaryImagePath");
            dt.Columns.Add("PrimaryImageName");
            dt.Columns.Add("PrimaryImage");

            dt2.Columns.Add("ImageFileName");
            dt2.Columns.Add("ImagePath");
            dt2.Columns.Add("IsGreenScreen");
            dt2.Columns.Add("GreenScreenBackground");
            dt2.Columns.Add("TicketCodeBarCode");
            dt2.Columns.Add("FormalFullName");
            dt2.Columns.Add("ProjectOrganizationName");
            dt2.Columns.Add("ProjectName");

            dt3.Columns.Add("LabOrderID");
            dt3.Columns.Add("PackageName");
            dt3.Columns.Add("PackageProducts");
            dt3.Columns.Add("PackagePrice");
            dt3.Columns.Add("PackageCost");
            dt3.Columns.Add("IsOnlineOrder");
            dt3.Columns.Add("TicketCodeBarCode");
            dt3.Columns.Add("FormalFullName");
            dt3.Columns.Add("ProjectOrganizationName");
            dt3.Columns.Add("ProjectName");

            dt4.Columns.Add("ImageDesc");
            dt4.Columns.Add("ImageName");
            dt4.Columns.Add("ImagePath");
            dt4.Columns.Add("AssignedTo");
            
            foreach (ProjectSubjectField fieldName in this.ProjectSubjectFieldList)
            {
                if (!dt.Columns.Contains(fieldName.ProjectSubjectFieldDesc))
                    dt.Columns.Add(fieldName.ProjectSubjectFieldDesc);
                if (!dt2.Columns.Contains(fieldName.ProjectSubjectFieldDesc))
                    dt2.Columns.Add(fieldName.ProjectSubjectFieldDesc);
                if (!dt3.Columns.Contains(fieldName.ProjectSubjectFieldDesc))
                    dt3.Columns.Add(fieldName.ProjectSubjectFieldDesc);
            }


            //Subject Table
            List<Subject> origSubjectList = new List<Subject>();
            foreach (Subject sub in this.SubjectList.View)
            {
                int x = 0;
                if (sub.Exclude)
                    continue;
                while (x++ < this.SubjectCopies)
                {
                    origSubjectList.Add(sub);
                }
            }


            List<Subject> applicableSubjects = new List<Subject>();
            if (IsReportStackSorted && this.SubjectCopies == 1)
            {
                int itemsInList = origSubjectList.Count;
                int iterationsNeeded = Convert.ToInt32(Math.Ceiling((double)itemsInList / CustomReportStackSortHowManyUp));
                for (int x = 0; x < iterationsNeeded; x++)
                {
                    for (int j = 0; j < CustomReportStackSortHowManyUp; j++)
                    {
                        int targetIndex = x + (j * (iterationsNeeded));
                        if (targetIndex < itemsInList)
                            applicableSubjects.Add((Subject)origSubjectList[targetIndex]);
                        else
                            applicableSubjects.Add(null);
                    }
                }

            }
            else if (IsReportStackSorted && this.SubjectCopies > 1)
            {
                int itemsInList = origSubjectList.Count;
                int iterationsNeeded = Convert.ToInt32(Math.Ceiling((double)itemsInList / CustomReportStackSortHowManyUp));

                Subject[] subjects = new Subject[itemsInList + CustomReportStackSortHowManyUp];

                int cnt = 0;
                int page = 0;
                int startpagePos = 0;
                int curpagePos = 0;
                foreach (Subject s in origSubjectList)
                {
                    int targetindex = curpagePos + (page * CustomReportStackSortHowManyUp);
                    subjects[targetindex] = s;

                    cnt++;
                    if (cnt > 0 && cnt % this.SubjectCopies == 0)
                    {
                        page++;
                        if (page == iterationsNeeded)
                        {
                            page = 0;
                            startpagePos += this.SubjectCopies;
                        }

                        curpagePos = startpagePos;


                    }
                    else
                        curpagePos++;
                }
                applicableSubjects = subjects.ToList();
                while (applicableSubjects.Last() == null)
                    applicableSubjects.RemoveAt(applicableSubjects.Count - 1);


            }
            else
            {
                applicableSubjects = origSubjectList;
            }

            string folderpath = Path.Combine(FlowContext.FlowTempDirPath, "ReportImages");
            if (!Directory.Exists(folderpath))
                Directory.CreateDirectory(folderpath);


            foreach (Subject sub in applicableSubjects)
            {
                
                if (sub != null)
                {
                    DataRow dr = dt.NewRow();
                    dr["TicketCodeBarCode"] = "*.NST." + sub.TicketCode + "*";
                    dr["FormalFullName"] = sub.FormalFullName;
                    dr["ProjectOrganizationName"] = this.Organization.OrganizationName;
                    dr["ProjectName"] = this.FlowProjectName;
                    dr["EventName"] = this.FlowProjectDesc;
                    dr["PhotoDate"] = this.DateCreated.ToString();
                    dr["DueDate"] = this.DueDate.ToString();
                    dr["CatalogName"] = (this.FlowCatalog == null) ? "" : this.FlowCatalog.FlowCatalogDesc;
                    dr["Photographer"] = this.FlowMasterDataContext.LoginID;

                    foreach (ProjectSubjectField fieldName in this.ProjectSubjectFieldList)
                    {
                        dr[fieldName.ProjectSubjectFieldDesc] = sub.SubjectDataRow[fieldName.ProjectSubjectFieldDisplayName];
                    }
                    foreach (SubjectImage i in sub.SubjectImages)
                    {
                        if (i.IsPrimary)
                        {
                           
                            string tempPath = Path.Combine(folderpath, sub.TicketCode + ".jpg");

                           
                            
                            dr["PrimaryImageName"] = i.ImageFileName;

                            if (GeneratePrimaryThumbs)
                            {
                                //byte[] data;
                                JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                                encoder.Frames.Add(BitmapFrame.Create(i.Thumbnail));
                                using (FileStream ms = new FileStream(tempPath, FileMode.Create))
                                {
                                    encoder.Save(ms);

                                }
                                dr["PrimaryImagePath"] = tempPath;
                            }
                            else
                            {
                                dr["PrimaryImagePath"] = i.ImagePathFullRes;
                            }


                            //dr["PrimaryImage"] = Convert.ToBase64String(data);
                        }
                    }
                    
                    dt.Rows.Add(dr);
                }
                
            }


            //Images
            foreach (SubjectImage si in this.ImageList)
            {
                DataRow dr2 = dt2.NewRow();

                dr2["ImageFileName"] = si.ImageFileName;
                dr2["ImagePath"] = si.ImagePathFullRes;
                dr2["IsGreenScreen"] = si.IsGreenscreen;
                dr2["GreenScreenBackground"] = si.GreenScreenBackground;

                dr2["TicketCodeBarCode"] = "*.NST." + si.Subject.TicketCode + "*";
                dr2["FormalFullName"] = si.Subject.FormalFullName;
                dr2["ProjectOrganizationName"] = this.Organization.OrganizationName;
                dr2["ProjectName"] = this.FlowProjectName;

                foreach (ProjectSubjectField fieldName in this.ProjectSubjectFieldList)
                {
                    dr2[fieldName.ProjectSubjectFieldDesc] = si.Subject.SubjectDataRow[fieldName.ProjectSubjectFieldDisplayName];
                }

                dt2.Rows.Add(dr2);
            }

            //Orders
            foreach (SubjectOrder so in this.OrderList)
            {
                foreach (OrderPackage op in so.OrderPackages)
                {
                    DataRow dr3 = dt3.NewRow();

                    dr3["LabOrderID"] = op.LabOrderID;
                    dr3["PackageName"] = op.ProductPackageDesc;

                    List<string> products = new List<string>();
                    decimal cost = 0;
                    foreach (OrderProduct oprod in op.OrderProducts)
                    {
                        products.Add(oprod.Label + " - qty: " + oprod.Quantity);
                        oprod.UpdateProductPackageComposition(oprod.ResourceURL);
                        if (oprod.ProductPackageComposition != null)
                            cost += oprod.ProductPackageComposition.ImageQuixProduct.Price;
                    }

                    dr3["PackageProducts"] = string.Join(", ", products.ToArray());
                    dr3["PackagePrice"] = op.ExpandedPriceDisplayText;
                    dr3["PackageCost"] = "$" + cost.ToString();
                    dr3["IsOnlineOrder"] = op.IsOnlineOrder;


                    dr3["TicketCodeBarCode"] = "*.NST." + so.Subject.TicketCode + "*";
                    dr3["FormalFullName"] = so.Subject.FormalFullName;
                    dr3["ProjectOrganizationName"] = this.Organization.OrganizationName;
                    dr3["ProjectName"] = this.FlowProjectName;

                    foreach (ProjectSubjectField fieldName in this.ProjectSubjectFieldList)
                    {
                        dr3[fieldName.ProjectSubjectFieldDesc] = so.Subject.SubjectDataRow[fieldName.ProjectSubjectFieldDisplayName];
                    }
                    dt3.Rows.Add(dr3);
                }
            }


            foreach (GroupImage gi in this.FlowProjectDataContext.GroupImages)
            {
                DataRow dr4 = dt4.NewRow();
                dr4["ImageDesc"] = gi.GroupImageDesc;
                dr4["ImageName"] = gi.ImageFileName;
                dr4["ImagePath"] = gi.ImagePath;
                dr4["AssignedTo"] = gi.AssignedTo;
                dt4.Rows.Add(dr4);
            }

            DataSet ds = new DataSet();
            ds.Tables.Add(dt);
            ds.Tables.Add(dt2);
            ds.Tables.Add(dt3);
            ds.Tables.Add(dt4);

            return ds;
        }

        public void GenerateExportData()
        {
            string exportJsonFile = Path.Combine(this.DirPath, this.FileSafeProjectName + ".json");
            string exportXmlFile = Path.Combine(this.DirPath, this.FileSafeProjectName + ".xml");

            if (File.Exists(exportJsonFile)) File.Delete(exportJsonFile);
            if (File.Exists(exportXmlFile)) File.Delete(exportXmlFile);

            JsonObject mainRoot = new JsonObject();
            JsonObject root = new JsonObject();
            mainRoot.Add("Project", root);

            if (this.ProjectTemplate == null)
            {
                root.Add("ProjectType", this.ProjectTemplateDisplayName);
            }
            else
            {
                root.Add("ProjectType", this.ProjectTemplate.ProjectTemplateDesc);
            }

            root.Add(FlowMasterDataContext.PreferenceManager.Application.ProjectLabel_OrganizationName.Replace(" ", "").Replace("(", "").Replace(")", "").Replace("#", "").Replace("'", ""), this.Organization.OrganizationName);
            root.Add(FlowMasterDataContext.PreferenceManager.Application.ProjectLabel_ProjectName.Replace(" ", "").Replace("(", "").Replace(")", "").Replace("#", "").Replace("'", ""), this.FlowProjectName);
            root.Add(FlowMasterDataContext.PreferenceManager.Application.ProjectLabel_EventName.Replace(" ", "").Replace("(", "").Replace(")", "").Replace("#", "").Replace("'", ""), this.FlowProjectDesc);
            root.Add(FlowMasterDataContext.PreferenceManager.Application.ProjectLabel_PrincipalName.Replace(" ", "").Replace("(", "").Replace(")", "").Replace("#", "").Replace("'", ""), this.PrincipalName);
            string catName = (this.FlowCatalog == null) ? "" : this.FlowCatalog.FlowCatalogDesc;
            root.Add(FlowMasterDataContext.PreferenceManager.Application.ProjectLabel_CatalogName.Replace(" ", "").Replace("(", "").Replace(")", "").Replace("#", "").Replace("'", ""), catName);
            root.Add(FlowMasterDataContext.PreferenceManager.Application.ProjectLabel_PayableTo.Replace(" ", "").Replace("(", "").Replace(")", "").Replace("#", "").Replace("'", ""), this.PayableTo);
            root.Add(FlowMasterDataContext.PreferenceManager.Application.ProjectLabel_PhotographyDate.Replace(" ", "").Replace("(", "").Replace(")", "").Replace("#", "").Replace("'", ""), this.DateCreated.ToString());
            root.Add(FlowMasterDataContext.PreferenceManager.Application.ProjectLabel_DueDate.Replace(" ", "").Replace("(", "").Replace(")", "").Replace("#", "").Replace("'", ""), this.DueDate.ToString());
            root.Add(FlowMasterDataContext.PreferenceManager.Application.ProjectLabel_Comments.Replace(" ", "").Replace("(", "").Replace(")", "").Replace("#", "").Replace("'", ""), this.Comments);

            foreach (OrderFormFieldLocal off in this.FlowProjectDataContext.OrderFormFieldLocals)
            {
                if(!root.ContainsKey(off.FieldName))
                    root.Add(off.FieldName, off.SelectedValue);
            }
            JsonArray jSubjects = new JsonArray();
            root.Add("Subjects", jSubjects);
            foreach (Subject s in this.SubjectList)
            {
                JsonObject jSub = new JsonObject();
                jSubjects.Add(jSub);
                foreach (SubjectDatum d in s.SubjectData)
                {
                    if (d.Value == null)
                        continue;
                    if(d.Value.GetType() == typeof(string))
                        jSub.Add(d.FieldDesc, d.Value as string);
                    if (d.Value.GetType() == typeof(bool))
                    {
                        if(d.Value as bool? == true)
                            jSub.Add(d.FieldDesc, true);
                        else
                            jSub.Add(d.FieldDesc, false);

                    }
                }
                JsonArray jImages = new JsonArray();
                jSub.Add("Images", jImages);
                int poseCount = 0;
                foreach (SubjectImage i in s.ImageList)
                {
                    poseCount++;
                    if (i.IsGroupImage) continue;
                    
                    JsonObject jImg = new JsonObject();
                    jImages.Add(jImg);
                    jImg.Add("FileName", i.ImageFileName);
                    jImg.Add("Photographer", i.SourceComputer);
                    jImg.Add("DateTaken", i.DateAssigned.ToString());
                    jImg.Add("PoseNumber", poseCount);
                    jImg.Add("IsPrimary", i.IsPrimary);
                    jImg.Add("CropX", i.CropL);
                    jImg.Add("CropY", i.CropT);
                    jImg.Add("CropW", i.CropW);
                    jImg.Add("CropH", i.CropH);
                    jImg.Add("Rotation", i.Orientation);
                    foreach (SubjectImageType t in i.SubjectImageTypes.Where(sit => sit.IsAssigned))
                    {
                        jImg.Add(t.ProjectImageType.ImageTypeDesc, true); 
                    }
                }

                JsonArray jGroupImages = new JsonArray();
                jSub.Add("GroupImages", jGroupImages);
                poseCount = 0;
                foreach (SubjectImage i in s.ImageList)
                {
                    poseCount++;
                    if (i.IsGroupImage)
                    {
                        JsonObject jGImg = new JsonObject();
                        jGroupImages.Add(jGImg);
                        jGImg.Add("FileName", i.ImageFileName);
                        jGImg.Add("Photographer", i.SourceComputer);
                        jGImg.Add("DateTaken", i.DateAssigned.ToString());
                        jGImg.Add("PoseNumber", poseCount);
                        jGImg.Add("Description", i.GroupImage.GroupImageDesc);
                        jGImg.Add("CropX", i.CropL);
                        jGImg.Add("CropY", i.CropT);
                        jGImg.Add("CropW", i.CropW);
                        jGImg.Add("CropH", i.CropH);
                        jGImg.Add("Rotation", i.Orientation);
                        jGImg.Add("GroupImageFlag",i.GroupImage.SelectedFlag);
                        jGImg.Add("AssignedTo", i.GroupImage.AssignedTo);
                        foreach (SubjectImageType t in i.SubjectImageTypes.Where(sit => sit.IsAssigned))
                        {
                            jGImg.Add(t.ProjectImageType.ImageTypeDesc, true);
                        }
                    }
                }
            }


            //write to files
            NetServ.Net.Json.JsonWriter writert = new NetServ.Net.Json.JsonWriter();
            mainRoot.Write(writert);
            string jsonDatat = writert.ToString().Replace("\\/", "/");

            System.IO.File.WriteAllText(exportJsonFile, jsonDatat);

            try
            {
                XmlDocument doc = JsonConvert.DeserializeXmlNode(jsonDatat);
                XDocument xdoc = XDocument.Parse(doc.InnerXml);
                xdoc.Save(exportXmlFile);
            }
            catch
            {
                logger.Error("FAILED TO CONVERT JSON to XML - export project");
                //not gonna throw the error, just no xml this time ;-)
            }

        }





        public void UploadedImage(SubjectImage si)
        {
            logger.Info("a");
            //SubjectImage si = this.ImageList.FirstOrDefault(i => i.SubjectImageGuid == siguid);
            logger.Info("b");
            si.ExistsInOnlineGallery = true;
            logger.Info("c");
            si.Subject.FlagForMerge = true;
            logger.Info("d");
        }
    }	// END class

    public class ZipFileMappingComparer : IEqualityComparer<ZipFileMapping>
        {

        IEqualityComparer<ZipFileMapping> Members;

        public bool Equals(ZipFileMapping x, ZipFileMapping y)
            {

                if ((x.DestinationFileName == y.DestinationFileName))
                {

                    return true;
                }

                else
                {

                    return false;
                }

            }
        public int GetHashCode(ZipFileMapping obj)
            {

                if (Object.ReferenceEquals(obj, null)) return 0;
                int DestinationFileName = obj.DestinationFileName == null ? 0 : obj.DestinationFileName.GetHashCode();

                return DestinationFileName;
            }
        }

    public class SubjectImagePlus
    {
        public SubjectImage SubjectImage { get; private set; }
        public string Background { get; private set; }
        public string NewImageName { get; private set; }
        public string NewImagePath { get; private set; }
        public bool IsRendered { get; private set; }

        public SubjectImagePlus(SubjectImage si, string background, string newImageName, string newImagePath, bool isRendered)
        {
            SubjectImage = si;
            Background = background;
            NewImageName = newImageName;
            NewImagePath = newImagePath;
            IsRendered = isRendered;
        }
    }
}	// END namespace
