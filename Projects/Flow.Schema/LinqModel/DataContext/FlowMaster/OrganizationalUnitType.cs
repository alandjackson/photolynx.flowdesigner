﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.Linq;
using System.Data.SqlServerCe;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Media.Imaging;

using Flow.Lib;
using Flow.Lib.Database.SqlCe;
using Flow.Lib.Helpers;
using Flow.Schema.Properties;
using Flow.Schema.Utility;


namespace Flow.Schema.LinqModel.DataContext
{
	/// <summary>
	/// Represents a constituent structural unit of an Organization
	/// e.g., a class is a strutural unit of a school, the school itself
	/// being considered the root unit
	/// </summary>
	partial class OrganizationalUnitType
	{
		/// <summary>
		/// Attempts to convert the supplied parameter to a ProjectTemplateOrganizationalUnitType object
		/// </summary>
		/// <param name="item"></param>
		/// <returns>ProjectTemplateOrganizationalUnitType</returns>
		public static ProjectTemplateOrganizationalUnitType ConvertFrom(object item)
		{
			if (item is ProjectTemplateOrganizationalUnitType)
				return (ProjectTemplateOrganizationalUnitType)item;

			// NOTE: this can be implemented as an implicit conversion
			if (item is OrganizationalUnitType)
			{
				ProjectTemplateOrganizationalUnitType castItem = new ProjectTemplateOrganizationalUnitType();
				castItem.ProjectTemplateOrganizationalUnitTypeDesc = (item as OrganizationalUnitType).OrganizationalUnitTypeDesc;

				return castItem;
			}

			return null;
		}
	}
}
