﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.Linq;
using System.Data.SqlServerCe;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Media.Imaging;

using Flow.Lib;
using Flow.Lib.Database.SqlCe;
using Flow.Lib.Helpers;
using Flow.Schema.Properties;
using Flow.Schema.Utility;


namespace Flow.Schema.LinqModel.DataContext
{
	partial class ProjectTemplateEventTrigger
	{
		public EventTriggerType EventTriggerType { get; private set; }
		public EventTrigger EventTrigger { get; private set; }

		public string EventTriggerTypeDesc
		{
			get { return (this.EventTriggerType == null) ? null : this.EventTriggerType.EventTriggerTypeDesc; }
		}

		public string EventTriggerDesc
		{
			get { return (this.EventTrigger == null) ? null : this.EventTrigger.EventTriggerDesc; }
		}

		internal void SetParent(EventTriggerType eventTriggerType, EventTrigger eventTrigger)
		{
			this.EventTriggerType = eventTriggerType;
			this.EventTrigger = eventTrigger;
		}
	}
}
