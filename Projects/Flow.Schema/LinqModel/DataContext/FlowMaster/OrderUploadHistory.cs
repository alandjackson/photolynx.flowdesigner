﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flow.Schema.LinqModel.DataContext
{
    partial class OrderUploadHistory
    {

        public Boolean IsProjectExport
        {
            get
            {
                if (this.IsImageMatchExport != true && this.IsRemoteImageServiceUpload != true && this.IsImageUpload != true && (this.LabOrderId == null || this.LabOrderId == "0"))
                    return true;

                return false;
            }
        }

        public Boolean IsLabOrder
        {
            get
            {
                if (this.IsImageMatchExport != true && this.IsRemoteImageServiceUpload != true && this.IsImageUpload != true && this.LabOrderId != null && this.LabOrderId != "0")
                    return true;

                return false;
            }
        }

        public string IconImagePath
        {
            get
            {
                if (IsImageMatchExport == true)
                    return " ..\\..\\Images\\export2.png";
                else if (IsProjectExport)
                    return " ..\\..\\Images\\package_export_48.png";
                else
                    return " ..\\..\\Images\\shoppingcart_full_48.png";
               
            }
        }
        public string UploadType
        {
            get
            {
                if (IsImageMatchExport == true)
                    return "ImageMatch Export";
                else if (IsProjectExport)
                    return "Project Export";
                else if (IsRemoteImageServiceUpload)
                    return "Remote Image Service";
                else if (IsImageUpload)
                    return "Image Upload";
                else
                    return "Lab Order";
            }
        }

    }
}
