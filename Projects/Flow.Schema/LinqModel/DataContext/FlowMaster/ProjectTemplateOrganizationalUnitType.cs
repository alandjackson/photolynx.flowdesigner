﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.Linq;
using System.Data.SqlServerCe;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Media.Imaging;

using Flow.Lib;
using Flow.Lib.Database.SqlCe;
using Flow.Lib.Helpers;
using Flow.Schema.Properties;
using Flow.Schema.Utility;


namespace Flow.Schema.LinqModel.DataContext
{
	/// <summary>
	/// Represents a hierarchical organizational unit type applied at the project template level
	/// </summary>
	partial class ProjectTemplateOrganizationalUnitType
	{
		/// <summary>
		/// List containing the child ProjectTemplateOrganizationalUnitTypes
		/// </summary>
		private IEnumerable<ProjectTemplateOrganizationalUnitType> _projectTemplateOrganizationalUnitTypes = null;
		public IEnumerable<ProjectTemplateOrganizationalUnitType> ProjectTemplateOrganizationalUnitTypes
		{
			get
			{
				// Select all child units (child ParentID = parent ID)
				return
					_projectTemplateOrganizationalUnitTypes
						.Where(ou => ou.ProjectTemplateOrganizationalUnitTypeParentID == this.ProjectTemplateOrganizationalUnitTypeID);
			}

			private set
			{
				_projectTemplateOrganizationalUnitTypes = value;

				// Recursively populate all children
				foreach (ProjectTemplateOrganizationalUnitType item in this.ProjectTemplateOrganizationalUnitTypes)
				{
					item.SetChildren(_projectTemplateOrganizationalUnitTypes);
				}
			}

		}

		/// <summary>
		/// Assigns the children ProjectTemplateOrganizationalUnitTypes of the type;
		/// recursively populates all children through the set accessor
		/// </summary>
		/// <param name="childrenItems"></param>
		internal void SetChildren(IEnumerable<ProjectTemplateOrganizationalUnitType> childrenItems)
		{
			this.ProjectTemplateOrganizationalUnitTypes = childrenItems;
		}

	}
}
