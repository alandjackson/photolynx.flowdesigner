﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.Linq;
using System.Data.SqlServerCe;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Media.Imaging;
using System.Xml.Linq;

using Flow.Lib;
using Flow.Lib.Database.SqlCe;
using Flow.Lib.Helpers;
using Flow.Schema.Properties;
using Flow.Schema.Utility;

namespace Flow.Schema.LinqModel.DataContext
{
	partial class ProductSubUnit
	{
		public ProductSubUnit(XElement productSubUnitDef)
		{
			this.ProductSubUnitDesc = (string)productSubUnitDef.Attribute("Description");	
		}
	}
}
