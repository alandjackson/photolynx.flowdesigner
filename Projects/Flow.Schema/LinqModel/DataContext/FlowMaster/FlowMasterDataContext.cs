using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.Linq;
using System.Data.SqlServerCe;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Media.Imaging;
using System.Xml.Linq;

using Flow.Lib;
using Flow.Lib.Database.SqlCe;
using Flow.Lib.Helpers;
using Flow.Lib.Helpers.ImageQuix;
using Flow.Lib.Preferences;
using Flow.Schema.Properties;
using Flow.Schema.Utility;
using System.Windows.Threading;
using NetServ.Net.Json;
using Flow.Schema.LinqModel.DataContext.FlowMaster;
using NLog;
using System.Net;

namespace Flow.Schema.LinqModel.DataContext
{

    /// <summary>
    /// LINQ-to-SQL DataContext for representation of and access to application-level data stored
    /// in the FlowMaster database
    /// </summary>
    partial class FlowMasterDataContext : INotifyPropertyChanged
    {

        static readonly object locker = new object();


        #region STATIC MEMBERS

        public List<string> alltempProdIDs { get; set; }

        public string LoginID { get; set; }

        /// <summary>
        /// Sets the path for the |DataDirectory| token in the data context's connection path;
        /// the path reference is stored in the FlowMasterConnectionString setting in the project Settings
        /// </summary>
        static FlowMasterDataContext()
        {
            // Set the directory which contains the FlowMaster database
            AppDomain.CurrentDomain.SetData("DataDirectory", FlowContext.FlowConfigDirPath);




            //init the greenscreenbackgrounds


            //FlowMasterDataContext.DataTypes = new FlowObservableCollection<FieldDataType>(new FlowMasterDataContext().FieldDataTypes);
            //FlowMasterDataContext.DefaultDataType = FlowMasterDataContext.DataTypes.First(dt => dt.NativeType == typeof(string));
        }

        /// <summary>
        /// Represents the set of data types available for user-defined fields
        /// </summary>
        private static FlowObservableCollection<FieldDataType> _dataTypes = null;
        public static FlowObservableCollection<FieldDataType> DataTypes
        {
            get
            {
                if (_dataTypes == null)
                {
                    //_dataTypes = new FlowObservableCollection<FieldDataType>(new FlowMasterDataContext().FieldDataTypes);
                    FlowMasterDataContext f = new FlowMasterDataContext();
                    _dataTypes = new FlowObservableCollection<FieldDataType>(f.FieldDataTypes);
                    f.Connection.Close();
                    f.Dispose();
                }

                return _dataTypes;
            }
        }

        /// <summary>
        /// Represents the default data type for cases when a user-defined field's type can not be inferred
        /// </summary>
        private static FieldDataType _defaultDataType;
        public static FieldDataType DefaultDataType
        {
            get
            {
                if (_defaultDataType == null)
                    _defaultDataType = FlowMasterDataContext.DataTypes.First(dt => dt.NativeType == typeof(string));

                return _defaultDataType;
            }
        }


        /// <summary>
        /// Retrieves a list of FlowMaster database schema versions
        /// </summary>
        /// <param name="databaseFilePath"></param>
        public static List<MasterDbVersion> GetDbVersionHistory(string databaseFilePath)
        {
            string connectionString = "Data Source=" + databaseFilePath + ";Max Database Size=1024;";
            SqlCeConnection con = new SqlCeConnection(connectionString);
            List<MasterDbVersion> versionList;

            // Retrieve the list of db versions in order of date (ascending)
            using (FlowMasterDataContext dataContext = new FlowMasterDataContext(con, false))
            {
                versionList = new List<MasterDbVersion>(dataContext.MasterDbVersions.OrderBy(v => v.DbVersionDate));
                dataContext.Connection.Close();
                dataContext.Dispose();
            }

            return versionList;
        }

        public FlowProject GetExistingProject(string sourceFileName)
        {
            string[] archiveFileList;
            if (sourceFileName.EndsWith(".paf"))
                archiveFileList = ZipUtility.GetArchiveFileList(sourceFileName);
            else
                archiveFileList = Directory.GetFiles(sourceFileName);

            string projectDatabaseFileEntry = archiveFileList.FirstOrDefault(e => e.EndsWith(".sdf"));

            if (projectDatabaseFileEntry == null)
                throw new Exception("Invalid archive file; missing database entry.");

            Guid projectGuid = new Guid(ExtensionUtility.GetFileNameRoot(projectDatabaseFileEntry));

            return this.FlowProjectList.FirstOrDefault(p => p.FlowProjectGuid == projectGuid);
        }

        public FlowProject GetExistingProjectUncompressed(string sourceDirName)
        {
            string[] sourceFileList = Directory.GetFiles(sourceDirName);

            string projectDatabaseFileEntry = Path.GetFileName(sourceFileList.FirstOrDefault(e => e.EndsWith(".sdf")));

            if (projectDatabaseFileEntry == null)
                throw new Exception("Invalid archive directory; missing database entry.");

            Guid projectGuid = new Guid(ExtensionUtility.GetFileNameRoot(projectDatabaseFileEntry));

            return this.FlowProjectList.FirstOrDefault(p => p.FlowProjectGuid == projectGuid);
        }

        private static Logger logger = LogManager.GetCurrentClassLogger();

        #endregion // STATIC MEMBERS

        public new void SubmitChanges()
        {
            lock (locker)
            {
                ChangeSet changeSet = base.GetChangeSet();
                if (this.PreferenceManager.Application.IsFlowServer)
                {
                    foreach (object o in changeSet.Deletes)
                    {
                        if (o.GetType() == typeof(ProductPackage) || o.GetType() == typeof(ProductPackageComposition) || o.GetType() == typeof(FlowCatalog) || o.GetType() == typeof(FlowCatalogOption))
                        {
                            this.PreferenceManager.Application.ServerPorjectsNeedInitialized = true;
                            this.SavePreferencesNoSubmit();
                        }
                    }
                    foreach (object o in changeSet.Inserts)
                    {
                        if (o.GetType() == typeof(ProductPackage) || o.GetType() == typeof(ProductPackageComposition) || o.GetType() == typeof(FlowCatalog) || o.GetType() == typeof(FlowCatalogOption))
                        {
                            this.PreferenceManager.Application.ServerPorjectsNeedInitialized = true;
                            this.SavePreferencesNoSubmit();
                        }
                    }
                    foreach (object o in changeSet.Updates)
                    {
                        if (o.GetType() == typeof(ProductPackage) || o.GetType() == typeof(ProductPackageComposition) || o.GetType() == typeof(FlowCatalog) || o.GetType() == typeof(FlowCatalogOption))
                        {
                            this.PreferenceManager.Application.ServerPorjectsNeedInitialized = true;
                            this.SavePreferencesNoSubmit();
                        }
                    }
                }

                try
                {
                    base.SubmitChanges();
                }
                catch
                {

                    Thread.Sleep(3000);
                    base.ChangeConflicts.ResolveAll(RefreshMode.KeepCurrentValues);
                    base.SubmitChanges();
                }
            }

        }

        public bool IsPLICConfiguration
        {
            get
            {
                if (this.PreferenceManager.Upload.CatalogRequest.ImageQuixBaseUrl.ToLower().Contains("plic.io"))
                    return true;
                if (this.PreferenceManager.Application.ForcePUDOrders)
                    return true;
                return false;
            }
        }

        public bool IsPUDConfiguration
        {
            get
            {
                if (this.PreferenceManager.Upload.CatalogRequest.ImageQuixBaseUrl.ToLower().Contains(".pud"))
                    return true;
                if (this.PreferenceManager.Application.ForcePUDOrders)
                    return true;
                return false;
            }
        }

        public bool IsDP2Configuration
        {
            get
            {
                return !IsPUDConfiguration;
            }
        }

        private List<GreenScreenBackground> _greenScreenBackgrounds { get; set; }
        public List<GreenScreenBackground> GreenScreenBackgrounds
        {
            get
            {
                if (_greenScreenBackgrounds == null)
                {
                    initGreenScreenBackgroundList();
                }
                return _greenScreenBackgrounds;
            }
        }

        public void initGreenScreenBackgroundList()
        {
            _greenScreenBackgrounds = new List<GreenScreenBackground>();
            foreach (string filePath in Directory.GetFiles(this.PreferenceManager.Edit.BackgroundsDirectory, "*.jpg", SearchOption.AllDirectories))
            {
                _greenScreenBackgrounds.Add((new GreenScreenBackground(filePath)));
            }
            foreach (string filePath in Directory.GetFiles(this.PreferenceManager.Edit.BackgroundsDirectory, "*.png", SearchOption.AllDirectories))
            {
                _greenScreenBackgrounds.Add((new GreenScreenBackground(filePath)));
            }
        }
        public void GreenScreenBackgroundsRefresh()
        {
            this._greenScreenBackgrounds = null;
            this.SendPropertyChanged("GreenScreenBackgrounds");
        }
        public List<List<GreenScreenBackground>> ListOfBackgroundLists
        {
            get
            {
                List<List<GreenScreenBackground>> temp = new List<List<GreenScreenBackground>>();
                temp.Add(GreenScreenBackgrounds);
                return temp;
            }
        }

        public void UpdateOverlayImage() { _overlayImage = null; SendPropertyChanged("OverlayImage"); }
        private BitmapImage _overlayImage = null;
        public BitmapImage OverlayImage
        {
            get
            {
                if (_overlayImage == null)
                {
                    string overlayDir = PreferenceManager.Edit.OverlaysDirectory;
                    string overlay = PreferenceManager.Edit.CurrentAdjustImageOverlay;
                    string overlayLocation = (overlay == null)
                        ? overlay = Directory.GetFiles(overlayDir)[0]
                        : Path.Combine(overlayDir, overlay);
                    _overlayImage = new BitmapImage();
                    if (File.Exists(overlayLocation))
                    {
                        _overlayImage.BeginInit();
                        _overlayImage.UriSource = new Uri(overlayLocation);
                        _overlayImage.DecodePixelHeight = 300;
                        _overlayImage.EndInit();
                    }
                }
                return _overlayImage;
            }
        }

        private PreferenceManager _preferenceManager = null;
        public PreferenceManager PreferenceManager
        {
            get
            {
                if (_preferenceManager == null)
                {
                    Serialization preferencesSerialization = this.PreferencesSerialization;

                    if (preferencesSerialization == null)
                    {
                        _preferenceManager = new PreferenceManager();

                        preferencesSerialization = new Serialization();
                        preferencesSerialization.SerializationKey = "Preferences";
                        preferencesSerialization.SerializationData = _preferenceManager.GetSerializationData();

                        this.Serializations.InsertOnSubmit(preferencesSerialization);

                        this.SubmitChanges();
                    }
                    else
                    {
                        _preferenceManager = PreferenceManager.GetPreferenceManager(preferencesSerialization.SerializationData);

                        // If an error occurred and the prefs did not deserialize, reset to defaults
                        // This error needs to be communicated at some point
                        if (_preferenceManager == null)
                        {
                            _preferenceManager = new PreferenceManager();
                            this.SubmitChanges();
                        }
                    }
                }

                return _preferenceManager;
            }

        }

        private Serialization PreferencesSerialization
        {
            get { return this.Serializations.FirstOrDefault(s => s.SerializationKey == "Preferences"); }
        }

        public void RestorePreferences()
        {
            _preferenceManager = PreferenceManager.GetPreferenceManager(this.PreferencesSerialization.SerializationData);

            // If an error occurred and the prefs did not deserialize, reset to defaults
            // This error needs to be communicated at some point
            if (_preferenceManager == null)
            {
                _preferenceManager = new PreferenceManager();
                this.SubmitChanges();
            }

            this.SendPropertyChanged("PreferenceManager");

            this.CustomReportFields = new ObservableCollection<FieldDetail>();
            this.CustomReportFields.Add(new FieldDetail("TicketCodeBarCode", -1));
            this.CustomReportFields.Add(new FieldDetail("FormalFullName", -1));
            this.CustomReportFields.Add(new FieldDetail("ProjectOrganizationName", -1));
            this.CustomReportFields.Add(new FieldDetail("ProjectName", -1));
        }

        public bool ShowOriginalFile = false;
        public bool ProcessImagesInBackground = false;

        #region METHODS

        /// <summary>
        /// Constructor; uses the default directory for connection and requests initialization
        /// of the ProjectTemplateList
        /// </summary>
        public FlowMasterDataContext() : this(true) { }

        /// <summary>
        /// Constructor; uses the default directory for connection
        /// </summary>
        /// <param name="initProjectTemplateList"></param>
        public FlowMasterDataContext(bool initProjectTemplateList) : this(new SqlCeConnection("Data Source = " + AppDomain.CurrentDomain.GetData("PathToFlowMaster")), initProjectTemplateList) { }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="initProjectTemplateList"></param>
        public FlowMasterDataContext(SqlCeConnection connectionString, bool initProjectTemplateList)
            : base(connectionString)
        {
           
            
            //this.UpdateIQGalleryIDs();


            // Initialize the PreferenceManager
            //			if(this.PreferenceManager == null) {};

            // If the ProjectTemplateList will be loaded if connecting to the default FlowMaster instance;
            // it will not be loaded when connecting to the installer version under the DbInstall dir
            if (initProjectTemplateList)
            {
                foreach (ProjectTemplate projectTemplate in this.ProjectTemplateList)
                {
                    projectTemplate.FlowMasterDataContext = this;
                }




                this.FlowProjectList.CollectionChanged += delegate
                {
                    this.SendPropertyChanged("StudioProjectList");
                    this.SendPropertyChanged("NoStudioProjectsAvailable");
                };

                this.OrgList.CollectionChanged += delegate
                {
                    this.SendPropertyChanged("OrgList");
                    this.SendPropertyChanged("StudioList");
                    this.SendPropertyChanged("SchoolList");
                    this.SendPropertyChanged("StudioFilterList");
                    this.SendPropertyChanged("SiteOrganizationList");
                };

                this.UserList.CollectionChanged += delegate
                {
                    this.SendPropertyChanged("UserList");
                    this.SendPropertyChanged("PhotographerList");
                };

                this.StudioList.CollectionChanged += delegate
                {
                    this.SendPropertyChanged("StudioList");
                    this.SendPropertyChanged("StudioFilterList");
                };
            }


        }

        public void RefreshOrgList()
        {
            //_orgList = new FlowObservableCollection<Organization>(this.Organizations.OrderBy(o => o.OrganizationName).ToList());
            _orgList = new FlowObservableCollection<Organization>(this.Organizations);
            this.SendPropertyChanged("OrgList");
            this.SendPropertyChanged("StudioList");
            this.SendPropertyChanged("SchoolList");
            this.SendPropertyChanged("StudioFilterList");
            this.SendPropertyChanged("SiteOrganizationList");
        }

        public void RefreshUserList()
        {
            _userList = null;
            this.SendPropertyChanged("UserList");
            this.SendPropertyChanged("PhotographerList");
        }

        public void SavePreferences()
        {
            SavePreferencesNoSubmit();
            this.SubmitChanges();
        }
        public void SavePreferencesNoSubmit()
        {
            this.PreferencesSerialization.SerializationData = this.PreferenceManager.GetSerializationData();
        }

        /// <summary>
        /// Saves any application-level data changes to the FlowMaster database
        /// </summary>
        /// <param name="currentFlowProject"></param>
        /// <param name="projectTemplate"></param>
        public void SaveProject(FlowProject currentFlowProject, ProjectTemplate projectTemplate)
        {
            // If this is a new Flow Project, set for insertion
            if (currentFlowProject.IsNew)
            {
                currentFlowProject.Initialize(this);

                if (projectTemplate != null)
                {
                    // Set image filename format fields
                    currentFlowProject.ImageFormatTypeID = projectTemplate.ImageFormatTypeID;
                    currentFlowProject.ImageFormatSeparatorChar = projectTemplate.ImageFormatSeparatorChar;
                    currentFlowProject.ImageFormatField1 = projectTemplate.ImageFormatField1;
                    currentFlowProject.ImageFormatField2 = projectTemplate.ImageFormatField2;
                    currentFlowProject.ImageFormatField3 = projectTemplate.ImageFormatField3;

                    // Set barcode scan settings
                    currentFlowProject.BarcodeScanInitSequence = projectTemplate.BarcodeScanInitSequence;
                    currentFlowProject.BarcodeScanValueLength = projectTemplate.BarcodeScanValueLength;
                    currentFlowProject.BarcodeScanTermSequence = projectTemplate.BarcodeScanTermSequence;
                }

                this.FlowProjectList.Add(currentFlowProject);
            }

            // Save the record
            this.SubmitChanges();
        }


        /// <summary>
        /// Cancels any unsaved modifications to FlowMaster entities
        /// </summary>
        public void Revert()
        {
            this.Refresh(RefreshMode.OverwriteCurrentValues);

            _subjectFieldList = null;
            _organizationalUnitTypeList = null;
            _subjectTypeList = null;
            _imageTypeList = null;
            _flowProjectList = null;
        }


        /// <summary>
        /// Attaches a Flow project which has been extracted from a project archive file
        /// and returns a reference to the new attached project
        /// </summary>
        /// <param name="projectFilePath"></param>
        /// <returns>FlowProject</returns>
        public FlowProject AttachProject(string projectFilePath, bool isNetworkProject)
        {
            string connectionString = "Data Source=" + projectFilePath + ";Max Database Size=1024;";

            
            
            DbVersionManager.SynchFlowProject(connectionString, this);

            FlowProjectDataContext importProjectDataContext = new FlowProjectDataContext(new SqlCeConnection(connectionString), this);

            // Synch assigned Organization reference
            Control_Organization projectOrganizationRef =
                importProjectDataContext.Control_Organizations.FirstOrDefault(o => o.OrganizationTypeID != 4);

            Organization organization = null;

            if (projectOrganizationRef != null)
            {
                organization = this.Organizations.FirstOrDefault(o => o.OrganizationGuid == projectOrganizationRef.OrganizationGuid);

                if (organization == null)
                {
                    organization = projectOrganizationRef;
                    this.Organizations.InsertOnSubmit(organization);
                    this.SubmitChanges();
                    this._orgList = null;
                    this.SendPropertyChanged("OrgList");
                }
            }
            else
                logger.Warn("Importing project: no organization is set!");

            // Synch assigned Studio reference
            Control_Organization projectStudioRef =
                importProjectDataContext.Control_Organizations.FirstOrDefault(o => o.OrganizationTypeID == 4);

            Organization studio = null;

            if (projectStudioRef != null)
            {
                studio = this.Organizations.FirstOrDefault(o => o.OrganizationGuid == projectStudioRef.OrganizationGuid);
                if (studio == null)
                    studio = this.Organizations.FirstOrDefault(o => projectStudioRef.LabCustomerID != null && projectStudioRef.LabCustomerID.Length > 0 && o.LabCustomerID == projectStudioRef.LabCustomerID && o.OrganizationTypeID == 4);
                if (studio == null)
                    studio = this.Organizations.FirstOrDefault(o => o.OrganizationName == projectStudioRef.OrganizationName && o.OrganizationTypeID == 4);
                //if (projectStudioRef.OrganizationGuid != studio.OrganizationGuid)
                //{
                //    projectStudioRef.OrganizationGuid = studio.OrganizationGuid;

                //}

                if (studio == null)
                {
                    studio = projectStudioRef;
                    this.Organizations.InsertOnSubmit(studio);
                    this.SubmitChanges();
                    this._orgList = null;
                    this.SendPropertyChanged("OrgList");
                }
            }
            else
                logger.Warn("Importing project: no studio is set!");

            // Synch assigned Owner (User) reference
            Control_User projectUserRef =
                importProjectDataContext.Control_Users.FirstOrDefault();

            User user = null;

            if (projectUserRef != null)
            {
                user = this.Users.FirstOrDefault(u => u.UserGuid == projectUserRef.UserGuid);

                if (user == null)
                {
                    user = projectUserRef;
                    this.Users.InsertOnSubmit(user);
                    this.SubmitChanges();
                }
            }
            else
                logger.Warn("Importing project: no user is set!");

            // Insert master project record
            Control_FlowProject projectRef = importProjectDataContext.Control_FlowProjects.FirstOrDefault();

            if (organization != null)
                projectRef.OrganizationID = organization.OrganizationID;

            if (studio != null)
                projectRef.StudioID = studio.OrganizationID;

            if (user != null)
                projectRef.OwnerUserID = user.UserID;

            FlowProject project = projectRef;
            project.FlowMasterDataContext = this;
            project.IsNetworkProject = (isNetworkProject == true);
            this.FlowProjectList.Add(project);
            this.SubmitChanges();

            // TODO: This is probably unnecessary
            if (!isNetworkProject)
                DbVersionManager.SynchFlowProject(project);

            Control_FlowCatalog sourceCatalog = importProjectDataContext.Control_FlowCatalogs.FirstOrDefault();

            FlowCatalog flowCatalog = null;

            if (sourceCatalog != null)
            {
                if (this.FlowCatalogList.Any(c => c.FlowCatalogGuid == sourceCatalog.FlowCatalogGuid))
                {
                    flowCatalog = this.FlowCatalogList.First(c => c.FlowCatalogGuid == sourceCatalog.FlowCatalogGuid);
                }
                else
                {

                    ///////////////////////////////////////////////////
                    // The following is temporary code to fix an issue with regenerating catalog GUIDs;
                    // this should be retired for a later release of Flow;
                    ///////////////////////////////////////////////////

                    //flowCatalog = this.FlowCatalogList.FirstOrDefault(c => c.FlowCatalogDesc == sourceCatalog.FlowCatalogDesc);

                    //if (flowCatalog == null)
                    //{

                    // End temporary code


                    ImageQuixCatalog sourceImageQuixCatalog = this.ImageQuixCatalogList.FirstOrDefault(c => c.PrimaryKey == sourceCatalog.ImageQuixCatalogPk);

                    if (sourceImageQuixCatalog == null)
                    {
                        project.ProblemImportingFlowCatalog = true;
                        // Attempt to retrieve the IQ catalog?

                    }
                    else
                    {
                        flowCatalog = new FlowCatalog(importProjectDataContext, sourceCatalog, sourceImageQuixCatalog);
                        flowCatalog.InitCatalogOptions(this, this.ImageQuixCatalogs.FirstOrDefault(c => c.ImageQuixCatalogID == flowCatalog.ImageQuixCatalogID));
                        flowCatalog.RefreshFlowCatalogOptions();
                        this.SubmitChanges();
                        if (!this.FlowCatalogs.Any(f => f.FlowCatalogGuid == flowCatalog.FlowCatalogGuid))
                            this.FlowCatalogs.InsertOnSubmit(flowCatalog);

                        this.SubmitChanges();
                        this._flowCatalogList = null;
                        this.SubmitChanges();



                        //_flowCatalogList = null;

                        SendPropertyChanged("FlowCatalogList");
                        SendPropertyChanged("FlowCatalogs");

                    }


                    //} // Remove this brace with the aforementioned temporary code
                }


                if (flowCatalog != null && File.Exists(project.DatabasePath))
                {
                    // && !(project.IsNetworkProject == true)
                    FlowProjectDataContext fpdc = new FlowProjectDataContext(project);
                    project.FlowCatalog = flowCatalog;
                    fpdc.SubmitChanges();
                    this.SubmitChanges();
                }
            }

            if (projectOrganizationRef != null && organization != null)
                projectOrganizationRef.OrganizationID = organization.OrganizationID;

            if (projectStudioRef != null && studio != null)
                projectStudioRef.OrganizationID = studio.OrganizationID;
            if (user != null)
                projectUserRef.UserID = user.UserID;

            projectRef.FlowProjectID = project.FlowProjectID;

            importProjectDataContext.SubmitChanges();

            return project;
        }



        public void ApplyControlTables(string projectFilePath, bool isNetworkProject, FlowProject destProject)
        {
            lock (locker)
            {
                string connectionString = "Data Source=" + projectFilePath + ";Max Database Size=1024;";

                DbVersionManager.SynchFlowProject(connectionString, this);

                FlowProjectDataContext importProjectDataContext = new FlowProjectDataContext(new SqlCeConnection(connectionString), this);

                // Synch assigned Organization reference
                Control_Organization projectOrganizationRef =
                    importProjectDataContext.Control_Organizations.FirstOrDefault(o => o.OrganizationTypeID != 4);


                Organization organization =
                    this.Organizations.FirstOrDefault(o => o.OrganizationGuid == projectOrganizationRef.OrganizationGuid);

                if (organization == null)
                {
                    organization = projectOrganizationRef;
                    this.Organizations.InsertOnSubmit(organization);
                    this.SubmitChanges();
                }

                // Synch assigned Studio reference
                Control_Organization projectStudioRef =
                    importProjectDataContext.Control_Organizations.FirstOrDefault(o => o.OrganizationTypeID == 4);

                Organization studio = null;

                if (projectStudioRef != null)
                {
                    studio = this.Organizations.FirstOrDefault(o => o.OrganizationGuid == projectStudioRef.OrganizationGuid);
                    if (studio == null)
                        studio = this.Organizations.FirstOrDefault(o => projectStudioRef.LabCustomerID != null && projectStudioRef.LabCustomerID.Length > 0 && o.LabCustomerID == projectStudioRef.LabCustomerID && o.OrganizationTypeID == 4);
                    if (studio == null)
                        studio = this.Organizations.FirstOrDefault(o => o.OrganizationName == projectStudioRef.OrganizationName && o.OrganizationTypeID == 4);
                    //if (projectStudioRef.OrganizationGuid != studio.OrganizationGuid)
                    //{
                    //    projectStudioRef.OrganizationGuid = studio.OrganizationGuid;
                    //}
                    if (studio == null)
                    {
                        studio = projectStudioRef;
                        this.Organizations.InsertOnSubmit(studio);
                        this.SubmitChanges();
                    }
                }

                // Synch assigned Owner (User) reference
                Control_User projectUserRef =
                    importProjectDataContext.Control_Users.FirstOrDefault();

                User user = null;
                try
                {
                    user =
                        this.Users.FirstOrDefault(u => u.UserGuid == projectUserRef.UserGuid);
                }
                catch
                {
                    user = null;
                }

                if (user == null && projectUserRef != null)
                {
                    user = projectUserRef;
                    this.Users.InsertOnSubmit(user);
                    this.SubmitChanges();
                }

                // Insert master project record
                Control_FlowProject projectRef = importProjectDataContext.Control_FlowProjects.FirstOrDefault();
                projectRef.OrganizationID = organization.OrganizationID;

                if (studio != null)
                    projectRef.StudioID = studio.OrganizationID;

                if(user != null)
                    projectRef.OwnerUserID = user.UserID;

                //FlowProject project = projectRef;
                destProject.Comments = projectRef.Comments;
                destProject.DateCreated = projectRef.DateCreated;
                destProject.DateModified = projectRef.DateModified;
                destProject.DefaultGreenScreenBackground = projectRef.DefaultGreenScreenBackground;
                destProject.FlowMasterDBVersion = projectRef.FlowMasterDBVersion;
                destProject.FlowProjectDesc = projectRef.FlowProjectDesc;
                destProject.FlowProjectGuid = projectRef.FlowProjectGuid;
                destProject.FlowProjectName = projectRef.FlowProjectName;
                destProject.ImageFormatField1 = projectRef.ImageFormatField1;
                destProject.ImageFormatField2 = projectRef.ImageFormatField2;
                destProject.ImageFormatField3 = projectRef.ImageFormatField3;
                destProject.ImageFormatSeparatorChar = projectRef.ImageFormatSeparatorChar;
                destProject.IsGreenScreen = projectRef.IsGreenScreen;

                //these are special and need to reference the control tables, because ids on the client may not be the same

                destProject.OrganizationID = projectRef.OrganizationID;
                destProject.StudioID = projectRef.StudioID;
                destProject.OwnerUserID = projectRef.OwnerUserID;

                destProject.FlowMasterDataContext = this;
                destProject.IsNetworkProject = (isNetworkProject == true);
                //this.FlowProjectList.Add(project);
                this.SubmitChanges();

                if (this.PreferenceManager.Application.IsNotFlowMaster)
                {

                    Control_FlowCatalog sourceCatalog = importProjectDataContext.Control_FlowCatalogs.FirstOrDefault();

                    FlowCatalog flowCatalog = null;

                    if (sourceCatalog != null)
                    {

                        if (this.FlowCatalogList.Any(c => c.FlowCatalogGuid == sourceCatalog.FlowCatalogGuid))
                        {

                            flowCatalog = this.FlowCatalogList.First(c => c.FlowCatalogGuid == sourceCatalog.FlowCatalogGuid);
                            flowCatalog.PrepareDelete();
                            this.FlowCatalogs.DeleteOnSubmit(flowCatalog);
                            this.FlowCatalogList.Remove(flowCatalog);
                            //this.FlowCatalogs.DeleteOnSubmit(flowCatalog);
                            this.SubmitChanges();
                            flowCatalog = null;
                        }

                        if (flowCatalog == null)
                        {

                            ///////////////////////////////////////////////////
                            // The following is temporary code to fix an issue with regenerating catalog GUIDs;
                            // this should be retired for a later release of Flow;
                            ///////////////////////////////////////////////////

                            //flowCatalog = this.FlowCatalogList.FirstOrDefault(c => c.FlowCatalogDesc == sourceCatalog.FlowCatalogDesc);

                            //if (flowCatalog == null)
                            //{

                            // End temporary code


                            ImageQuixCatalog sourceImageQuixCatalog = this.ImageQuixCatalogList.FirstOrDefault(c => c.PrimaryKey == sourceCatalog.ImageQuixCatalogPk);

                            if (sourceImageQuixCatalog == null)
                            {
                                destProject.ProblemImportingFlowCatalog = true;
                                // Attempt to retrieve the IQ catalog?

                            }
                            else
                            {
                                flowCatalog = new FlowCatalog(importProjectDataContext, sourceCatalog, sourceImageQuixCatalog);
                                //this.FlowCatalogList.Add(flowCatalog);
                                flowCatalog.InitCatalogOptions(this, this.ImageQuixCatalogs.FirstOrDefault(c => c.ImageQuixCatalogID == flowCatalog.ImageQuixCatalogID));
                                flowCatalog.RefreshFlowCatalogOptions();

                                flowCatalog.ImageQuixCatalog = sourceImageQuixCatalog;
                                flowCatalog.ImageQuixCatalogID = sourceImageQuixCatalog.ImageQuixCatalogID;

                                flowCatalog.FlowMasterDataContext = this;
                                this.SubmitChanges();
                                if (!this.FlowCatalogs.Any(c => c.FlowCatalogGuid == flowCatalog.FlowCatalogGuid))
                                    this.FlowCatalogs.InsertOnSubmit(flowCatalog);
                                this.SubmitChanges();
                                this._flowCatalogList = null;
                                this.SubmitChanges();



                                //_flowCatalogList = null;

                                SendPropertyChanged("FlowCatalogList");
                                SendPropertyChanged("FlowCatalogs");

                            }


                            //} // Remove this brace with the aforementioned temporary code
                        }


                        if (flowCatalog != null && File.Exists(destProject.DatabasePath))
                        {
                            // && !(project.IsNetworkProject == true)
                            FlowProjectDataContext fpdc = new FlowProjectDataContext(destProject);
                            destProject.FlowCatalog = flowCatalog;
                            fpdc.SubmitChangesLocal();
                            this.SubmitChanges();
                        }
                    }
                }
                projectOrganizationRef.OrganizationID = organization.OrganizationID;

                if (projectStudioRef != null && studio != null)
                    projectStudioRef.OrganizationID = studio.OrganizationID;

                if(user != null)
                    projectUserRef.UserID = user.UserID;

                projectRef.FlowProjectID = destProject.FlowProjectID;

                importProjectDataContext.SubmitChangesLocal();
                importProjectDataContext.Connection.Close();
                importProjectDataContext.Dispose();
            }
            //return project;
        }


        /// <summary>
        /// Deletes the FlowProject record for the specified project from the application
        /// database (within the scope of a project export operation)
        /// </summary>
        /// <param name="flowProject"></param>
        public void DetachProject(FlowProject flowProject)
        {
            this.FlowProjectList.Remove(flowProject);

            this.SubmitChanges();
            _filteredFlowProjectList = null;

            this.SendPropertyChanged("FilteredFlowProjectList");
            this.SendPropertyChanged("StudioProjectList");

        }

        #endregion // METHODS


        ///
        #region FlowObservableCollection Implementations
        ///


        private ObservableCollection<OnlineGallery> _onlineGalleryList = null;
        public ObservableCollection<OnlineGallery> OnlineGalleryList
        {
            get
            {
                if (_onlineGalleryList == null)
                    _onlineGalleryList = new ObservableCollection<OnlineGallery>();

                return _onlineGalleryList;
            }
        }


        /// <summary>
        /// FlowObservableCollection for generated FlowProject Linq-to-SQL entity
        /// </summary>
        private FlowObservableCollection<ProjectTemplate> _projectTemplateList = null;
        public FlowObservableCollection<ProjectTemplate> ProjectTemplateList
        {
            get
            {
                if (_projectTemplateList == null)
                    _projectTemplateList = new FlowObservableCollection<ProjectTemplate>(this.ProjectTemplates);

                return _projectTemplateList;
            }
        }
        public void RefreshProjectTemplateList()
        {
            _projectTemplateList = null;
            SendPropertyChanged("ProjectTemplateList");
        }

        private List<OrderUploadHistory> _orderUploadHistoryList = null;
        public List<OrderUploadHistory> OrderUploadHistoryList
        {
            get
            {
                if (_orderUploadHistoryList == null)
                {
                    _orderUploadHistoryList = new List<OrderUploadHistory>(this.OrderUploadHistories.Where(h => h.ProjectName != null && h.UploadDate != null));
                }
                _orderUploadHistoryList.Reverse();
                return _orderUploadHistoryList;
            }
        }
        public bool OrderUploadHistoryListHasItems
        {
            get
            {
                if (OrderUploadHistoryList == null || OrderUploadHistoryList.Count == 0)
                    return false;
                return true;
            }
        }
        public bool OrderUploadHistoryListHasNoItems
        {
            get
            {
                if (OrderUploadHistoryList == null || OrderUploadHistoryList.Count == 0)
                    return true;
                return false;
            }
        }
        private List<OrderUploadHistory> _orderUploadHistoryQueueList = null;
        public List<OrderUploadHistory> OrderUploadHistoryQueueList
        {
            get
            {
                if (_orderUploadHistoryQueueList == null)
                {
                    try
                    {
                        return this.OrderUploadHistories.Where(h => h.ProjectName != null && h.UploadDate == null).ToList();
                        
                    }
                    catch
                    {
                        //do nothing
                    }
                }
                return new List<OrderUploadHistory>();
            }
        }
        public bool OrderUploadHistoryQueueListHasItems
        {
            get
            {
                if (OrderUploadHistoryQueueList == null || OrderUploadHistoryQueueList.Count == 0)
                    return false;
                return true;
            }
        }
        public bool OrderUploadHistoryQueueListHasNoItems
        {
            get
            {
                if (OrderUploadHistoryQueueList == null || OrderUploadHistoryQueueList.Count == 0)
                    return true;
                return false;
            }
        }
        public void RefreshOrderUploadHistoryList()
        {
            _orderUploadHistoryList = null;
            _orderUploadHistoryQueueList = null;
            SendPropertyChanged("OrderUploadHistoryList");
            SendPropertyChanged("OrderUploadHistoryQueueList");

            SendPropertyChanged("OrderUploadHistoryListHasItems");
            SendPropertyChanged("OrderUploadHistoryListHasNoItems");
            SendPropertyChanged("OrderUploadHistoryQueueListHasItems");
            SendPropertyChanged("OrderUploadHistoryQueueListHasNoItems");
        }

        /// <summary>
        /// FlowObservableCollection for generated FlowProject Linq-to-SQL entity
        /// </summary>
        private FlowObservableCollection<FlowProject> _flowProjectList = null;
        public FlowObservableCollection<FlowProject> FlowProjectList
        {
            get
            {
                if (_flowProjectList == null)
                    _flowProjectList = new FlowObservableCollection<FlowProject>(this.FlowProjects);

                return _flowProjectList;
            }
        }

        private List<FlowProject> _filteredFlowProjectList = null;
        public List<FlowProject> FilteredFlowProjectList
        {
            get
            {
                if (_filteredFlowProjectList == null)
                    _filteredFlowProjectList = new List<FlowProject>(this.FlowProjects);

                return _filteredFlowProjectList;
            }
            set
            {
                _filteredFlowProjectList = value;
            }
        }
        /// <summary>
        /// FlowObservableCollection for generated FlowProject Linq-to-SQL entity
        /// </summary>
        private FlowObservableCollection<OrderFormField> _orderFormFieldList = null;
        public FlowObservableCollection<OrderFormField> OrderFormFieldList
        {
            get
            {
                if (_orderFormFieldList == null)
                    _orderFormFieldList = new FlowObservableCollection<OrderFormField>(this.OrderFormFields);

                return _orderFormFieldList;
            }
        }

        /// <summary>
        /// The studio selected for application wide filtering (restriction to studio's projects)
        /// </summary>
        private Organization _selectedStudio = null;
        public Organization SelectedStudio
        {
            get {
                if (_selectedStudio == null)
                    _selectedStudio = this.StudioList.FirstOrDefault();
                return _selectedStudio; 
            }
            set
            {
                _selectedStudio = value;
                this.SendPropertyChanged("SelectedStudio");
                this.SendPropertyChanged("StudioProjectList");
                this.SendPropertyChanged("NoStudioProjectsAvailable");
            }
        }

        /// <summary>
        /// The list of projects specific to the selected studio
        /// </summary>
        public List<FlowProject> StudioProjectList
        {
            get
            {
                List<FlowProject> studioProjectList =
                    this.FlowProjectList
                    .Where(p =>
                        this.SelectedStudio == null ||
                        (p.StudioID.HasValue && p.StudioID.Value == this.SelectedStudio.OrganizationID)
                    ).OrderBy(p => p.FlowProjectDesc).ToList();

                return studioProjectList;
            }
        }

        /// <summary>
        /// Indicates if the selected studio has any projects
        /// </summary>
        public bool NoStudioProjectsAvailable
        {
            get { return this.StudioProjectList.Count == 0; }
        }

        /// <summary>
        /// FlowObservableCollection for generated SubjectField Linq-to-SQL entity
        /// </summary>
        private FlowObservableCollection<SubjectField> _subjectFieldList = null;
        public FlowObservableCollection<SubjectField> SubjectFieldList
        {
            get
            {
                if (_subjectFieldList == null)
                    _subjectFieldList = new FlowObservableCollection<SubjectField>(this.SubjectFields);

                return _subjectFieldList;
            }
        }

        public List<string> SubjectFieldDescs
        {
            get
            {
                List<string> temp = new List<string>();
                //temp.Add("FirstName");
                //temp.Add("LastName");
                temp.Add("TicketCode");
                //temp.AddRange(this.SubjectFields.Select(i => i.SubjectFieldDesc).ToList());
                return temp;
            }
        }

        /// <summary>
        /// Represents the set of Users in the Database
        /// </summary>
        private FlowObservableCollection<User> _userList = null;
        public FlowObservableCollection<User> UserList
        {
            get
            {
                if (_userList == null)
                    _userList = new FlowObservableCollection<User>(this.Users);

                return _userList;
            }
        }


        public FlowObservableCollection<User> PhotographerList
        {
            get { return new FlowObservableCollection<User>(this.UserList.Where(u => u.IsPhotographer).OrderBy(u => u.UserLastName)); }
        }

        /// <summary>
        /// Represents the set of Organizations in the Database
        /// </summary>
        private FlowObservableCollection<Organization> _orgList = null;
        public FlowObservableCollection<Organization> OrgList
        {
            get
            {
                if (_orgList == null)
                    _orgList = new FlowObservableCollection<Organization>(this.Organizations);
                //_orgList = new FlowObservableCollection<Organization>(this.Organizations.OrderBy(o => o.OrganizationName).ToList());

                return _orgList;
            }
        }

        /// <summary>
        /// Represents the current Organization
        /// </summary>
        public FlowObservableCollection<Organization> GetOrganization(int orgId)
        {
            return new FlowObservableCollection<Organization>(this.Organizations.Where(o => o.OrganizationID == orgId));
        }
        /// <summary>
        /// Represents the set of Organizations in the Database
        /// </summary>
        private FlowObservableCollection<Organization> _siteOrganizationList = null;
        public FlowObservableCollection<Organization> SiteOrganizationList
        {
            get
            {
                //if (_siteOrganizationList == null)
                //_siteOrganizationList = new FlowObservableCollection<Organization>(this.Organizations.Where(o => o.OrganizationTypeID != 4).OrderBy(o => o.OrganizationName).ToList());
                _siteOrganizationList = new FlowObservableCollection<Organization>(this.Organizations.Where(o => o.OrganizationTypeID != 4));
                return _siteOrganizationList;
            }
        }

        /// <summary>
        /// Represents the set of Studios in the Database
        /// </summary>
        public FlowObservableCollection<Organization> StudioList
        {
            get
            {
                return new FlowObservableCollection<Organization>(this.Organizations.Where(o => o.OrganizationTypeID == 4));
            }
        }

        /// <summary>
        /// A list of studios for filtering selection, with a null option for no selection (no filter)
        /// </summary>
        public IQueryable StudioFilterList
        {
            get
            {
                List<Organization> studioFilterList = this.StudioList.ToList();
                studioFilterList.Insert(0, null);

                return studioFilterList.Select(s =>
                    new
                    {
                        StudioName = ((s == null) ? "[No studio selected]" : s.OrganizationName),
                        Studio = s
                    }
                ).AsQueryable();
            }
        }

        /// <summary>
        /// Represents the set of Organization Types in the Database
        /// </summary>
        private FlowObservableCollection<OrganizationType> _orgTypeList = null;
        public FlowObservableCollection<OrganizationType> OrgTypeList
        {
            get
            {
                if (_orgTypeList == null)
                    _orgTypeList = new FlowObservableCollection<OrganizationType>(this.OrganizationTypes);

                return _orgTypeList;
            }
        }

        /// <summary>
        /// FlowObservableCollection for generated OrganizationalUnitType Linq-to-SQL entity
        /// </summary>
        private FlowObservableCollection<OrganizationalUnitType> _organizationalUnitTypeList;
        public FlowObservableCollection<OrganizationalUnitType> OrganizationalUnitTypeList
        {
            get
            {
                if (_organizationalUnitTypeList == null)
                    _organizationalUnitTypeList = new FlowObservableCollection<OrganizationalUnitType>(this.OrganizationalUnitTypes);

                return _organizationalUnitTypeList;
            }
        }

        /// <summary>
        /// FlowObservableCollection for generated SubjectType Linq-to-SQL entity
        /// </summary>
        private FlowObservableCollection<SubjectType> _subjectTypeList;
        public FlowObservableCollection<SubjectType> SubjectTypeList
        {
            get
            {
                if (_subjectTypeList == null)
                    _subjectTypeList = new FlowObservableCollection<SubjectType>(this.SubjectTypes);

                return _subjectTypeList;
            }
        }

        /// <summary>
        /// FlowObservableCollection for generated ImageType Linq-to-SQL entity
        /// </summary>
        private FlowObservableCollection<ImageType> _imageTypeList;
        public FlowObservableCollection<ImageType> ImageTypeList
        {
            get
            {
                if (_imageTypeList == null)
                    _imageTypeList = new FlowObservableCollection<ImageType>(this.ImageTypes);

                return _imageTypeList;
            }
        }

        /// <summary>
        /// FlowObservableCollection for generated ProductCatalog Linq-to-SQL entity
        /// </summary>
        private FlowObservableCollection<FlowCatalog> _flowCatalogList;
        public FlowObservableCollection<FlowCatalog> FlowCatalogList
        {
            get
            {
                if (_flowCatalogList == null)
                {
                    _flowCatalogList = new FlowObservableCollection<FlowCatalog>(this.FlowCatalogs);

                    // Set the FlowMasterDataContext property for each catalog in the list
                    foreach (FlowCatalog item in _flowCatalogList)
                    {

                        item.FlowMasterDataContext = this;
                        if (item.ImageQuixCatalog != null)
                        {
                            try
                            {
                                item.InitCatalogOptions(this, item.ImageQuixCatalog);
                            }
                            catch (Exception e)
                            {
                                logger.Debug("had an error that I skipped: " + e.Message);
                            }
                        }

                    }
                }

                return _flowCatalogList;
            }
        }

        public List<ImageQuixOptionGroup> OptionGroupList
        {
            get
            {
                return this.ImageQuixOptionGroups.ToList();
            }

        }

        /// <summary>
        /// FlowObservableCollection for generated ProductCatalogItem Linq-to-SQL entity
        /// </summary>
        private FlowObservableCollection<ProductPackage> _productPackageList;
        public FlowObservableCollection<ProductPackage> ProductPackageList
        {
            get
            {
                if (_productPackageList == null)
                    _productPackageList = new FlowObservableCollection<ProductPackage>(this.ProductPackages);

                return _productPackageList;
            }
        }

        /// <summary>
        /// FlowObservableCollection for generated ImageQuixCatalog Linq-to-SQL entity
        /// </summary>
        private FlowObservableCollection<ImageQuixCatalog> _imageQuixCatalogList;
        public FlowObservableCollection<ImageQuixCatalog> ImageQuixCatalogList
        {
            get
            {
                if (_imageQuixCatalogList == null)
                    _imageQuixCatalogList = new FlowObservableCollection<ImageQuixCatalog>(this.ImageQuixCatalogs);

                return _imageQuixCatalogList;
            }
        }
        public void UpdateImageQuixCatalogList()
        {
            _imageQuixCatalogList = null;
            this.SendPropertyChanged("ImageQuixCatalogList");
        }
        /// <summary>
        /// FlowObservableCollection for generated PaymentMethod Linq-to-SQL entity
        /// </summary>
        private FlowObservableCollection<PaymentMethod> _paymentMethodList = null;
        public FlowObservableCollection<PaymentMethod> PaymentMethodList
        {
            get
            {
                if (_paymentMethodList == null)
                    _paymentMethodList = new FlowObservableCollection<PaymentMethod>(this.PaymentMethods);

                return _paymentMethodList;
            }
        }


        ///
        #endregion
        ///


        ///
        #region ImageQuix
        ///

        // DEPRECATED IN FAVOR OF DATABASE STORAGE OF VALUE (PREFERENCES)
        public string ImageQuixCatalogServerUrl
        {
            get { return this.PreferenceManager.Upload.CatalogRequest.ImageQuixCatalogUrl; }
        }
        public string CatalogUsername
        {
            get { return this.PreferenceManager.Upload.CatalogRequest.GetCatalogUsername; }
        }
        public string CatalogPassword
        {
            get { return this.PreferenceManager.Upload.CatalogRequest.GetCatalogPassword; }
        }

        /// <summary>
        /// 
        /// </summary>
        private ObservableCollection<ImageQuixCatalogListing> _remoteImageQuixCatalogList = null;
        public ObservableCollection<ImageQuixCatalogListing> RemoteImageQuixCatalogList
        {
            get
            {
                if (_remoteImageQuixCatalogList == null)
                    _remoteImageQuixCatalogList = new ObservableCollection<ImageQuixCatalogListing>();

                return _remoteImageQuixCatalogList;
            }
        }


        private ObservableCollection<ImageQuixCatalogListing> _remoteImageQuixCatalogListDeletes = null;
        public ObservableCollection<ImageQuixCatalogListing> RemoteImageQuixCatalogListDeletes
        {
            get
            {
                if (_remoteImageQuixCatalogListDeletes == null)
                    _remoteImageQuixCatalogListDeletes = new ObservableCollection<ImageQuixCatalogListing>();

                return _remoteImageQuixCatalogListDeletes;
            }
        }


        /// <summary>
        /// Retrieves the current list of ImageQuix catalogs from the IQ server
        /// </summary>
        public Boolean RefreshRemoteImageQuixCatalogList()
        {
            Collection<ImageQuixCatalogListing> IQCatalogs = null;
            //string catalogurl = this.ImageQuixCatalogServerUrl.Replace("catalog", "catalogdump");
            string catalogurl = this.ImageQuixCatalogServerUrl;
            try
            {
                IQCatalogs = ImageQuixUtility.GetImageQuixCatalogListFromJsonServer(catalogurl, CatalogUsername, CatalogPassword);

            }
            catch (Exception e)
            {
                //throw e;

            }


            if (IQCatalogs == null)
                return false;
            foreach (ImageQuixCatalogListing listing in IQCatalogs)
            {
                try
                {
                    if (listing.isDeleted)
                    {
                        if (!this.RemoteImageQuixCatalogList.Any(l => l.PrimaryKey == listing.PrimaryKey))
                            this.RemoteImageQuixCatalogList.Remove(listing);

                        if (!this.RemoteImageQuixCatalogListDeletes.Any(l => l.PrimaryKey == listing.PrimaryKey))
                            this.RemoteImageQuixCatalogListDeletes.Add(listing);

                        continue;
                    }
                    if (!this.RemoteImageQuixCatalogList.Any(l => l.PrimaryKey == listing.PrimaryKey))
                        this.RemoteImageQuixCatalogList.Add(listing);
                }
                catch
                {
                    //ignore
                }
            }
            return true;
        }

        /// <summary>
        /// Retrieves the specified ImageQuix catalogs from the IQ server
        /// and saves them locally
        /// </summary>
        public void ImportMarkedRemoteImageQuixCatalogs(NotificationProgressInfo notificationInfo, Dispatcher dispatcher)
        {



            foreach (ImageQuixCatalogListing item in this.RemoteImageQuixCatalogList.Where(r => r.IsMarkedForImport))
            {

                JsonArray catalogDef = ImageQuixUtility.GetImageQuixCatalogFromJsonServer(
                    this.ImageQuixCatalogServerUrl,
                    item.PrimaryKey, CatalogUsername, CatalogPassword
                );

                JsonWriter writer = new JsonWriter();
                catalogDef.Write(writer);
                string postData = writer.ToString().Replace("\\/", "/");

                //write post data to temp folder
                string iqcat = Path.Combine(FlowContext.FlowTempDirPath, "IQ_Catalog.txt");
                File.WriteAllText(iqcat, postData);



                string CustomerID = "Not Found";
                try
                {
                    CustomerID = (this.OrgList.Where(o => o.OrganizationTypeID == 4).FirstOrDefault()).LabCustomerID;
                }
                catch
                {
                    //do nothing for now
                }


                ImageQuixCatalog.CreateImageQuixCatalog(item, catalogDef, this, notificationInfo, dispatcher, CustomerID);
                foreach (FlowCatalog fc in this.FlowCatalogs)
                {
                    fc.InitCatalogOptions(this, fc.ImageQuixCatalog);
                    fc.RefreshFlowCatalogOptions();
                }
                ImageQuixCatalog iqc = this.ImageQuixCatalogList.CurrentItem;
                if (iqc != null)
                    iqc.RefreshProductList();
                this.SendPropertyChanged("ImageQuixProductGroups");
                this.SendPropertyChanged("ImageQuixCatalogList");



            }
        }

        /// <summary>
        /// Deletes the ImageQuixCatalog current of the ImageQuixCatalogList collection
        /// </summary>
        public void DeleteCurrentImageQuixCatalog()
        {
            if (this.ImageQuixCatalogList != null && this.ImageQuixCatalogList.CurrentItem != null)
            {
                this.ImageQuixCatalogList.CurrentItem.PrepareForDeletion(this);

                this.ImageQuixCatalogList.DeleteCurrent();
                this.SubmitChanges();
            }
        }


        //nothing special here, this just alows me to put a break point here so I can see when Submit Changes is called.
        // public void SubmitChanges(){}

        public void DeleteUnusedProducts()
        {
            //foreach (FlowCatalog flowCatalog in this.FlowCatalogList)
            //{
            //    foreach (ProductPackage productPackage in this.ProductPackageList.ToList())
            //    {
            //        this.DeleteProductPackage(productPackage);
            //    }
            //}

            foreach (ImageQuixCatalog catalog in this.ImageQuixCatalogList)
            {
                foreach (ImageQuixProduct product in catalog.ProductList)
                {
                    bool prodInUse = false;
                    foreach (FlowCatalog flowCatalog in this.FlowCatalogList)
                    {
                        foreach (ProductPackage productPackage in this.ProductPackageList.ToList())
                        {
                            if (productPackage.ProductPackageCompositions.Where(s => s.ImageQuixProduct == product).Count() > 0)
                                prodInUse = true;
                        }
                    }

                    if (!prodInUse)
                    {
                        foreach (ImageQuixOptionGroup optionGroup in product.OptionGroupList)
                        {
                            foreach (ImageQuixOption option in optionGroup.OptionList)
                            {
                                this.ImageQuixOptionCommands.DeleteAllOnSubmit(option.OptionCommandList);
                            }

                            this.ImageQuixOptions.DeleteAllOnSubmit(optionGroup.OptionList);
                        }

                        this.ImageQuixOptionGroups.DeleteAllOnSubmit(product.OptionGroupList);
                        this.ImageQuixProductNodes.DeleteAllOnSubmit(product.ProductNodeList);
                        this.ImageQuixProducts.DeleteOnSubmit(product);

                    }
                }

                this.SubmitChanges();
                catalog.RefreshProductList();

                //foreach (ImageQuixProductGroup productGroup in catalog.ProductGroupList)
                //{
                //    catalog.ImageQuixProducts.Clear();
                //}

                //this.ImageQuixProducts.DeleteAllOnSubmit(catalog.ProductList);
                //catalog.ProductList.Clear();

                //this.ImageQuixProductGroups.DeleteAllOnSubmit(catalog.ProductGroupList);
            }

        }

        /// <summary>
        /// 
        /// </summary>
        public void ClearCatalogs()
        {
            foreach (FlowCatalog flowCatalog in this.FlowCatalogList)
            {
                flowCatalog.FlowMasterDataContext = this;
                flowCatalog.PrepareDelete();
            }

            this.FlowCatalogList.Clear();
            this.SubmitChanges();


            foreach (ImageQuixCatalog catalog in this.ImageQuixCatalogList)
            {
                catalog.PrepareForDeletion(this);
            }

            this.ImageQuixCatalogList.Clear();
            this.ImageQuixCatalogs.DeleteAllOnSubmit(this.ImageQuixCatalogs);
            this.SubmitChanges();
        }


        ///
        #endregion
        ///



        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;


        protected void SendPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        private List<ProjectGallerySummary> _liveFlowProjectGalleries { get; set; }
        public List<ProjectGallerySummary> LiveFlowProjectGalleries
        {
            get
            {
                if (_liveFlowProjectGalleries == null)
                {
                    GenerateFlowGalleryList();
                }

                return _liveFlowProjectGalleries.OrderBy(g => g.GalleryName).ToList();
            }
        }

        //public void UpdateIQGalleryIDs()
        //{
        //    string getGalleryIdurlbase = this.PreferenceManager.Upload.OnlineOrder.ImageQuixBaseURL + "/catalog/rest/flow/event/";

        //    foreach (FlowProject proj in this.FlowProjects)
        //    {
        //        try
        //        {
        //            proj.FlowMasterDataContext = this;
        //            string connectString = proj.ConnectionString;
        //            if (proj.IsNetworkProject == true)
        //                connectString = proj.NetworkConnectionString;
        //            SqlCeConnection cn = new SqlCeConnection(connectString);
        //            cn.Open();
        //            SqlCeCommand command = new SqlCeCommand("select EventID, GalleryCode from ProjectGallery", cn);
        //            SqlCeResultSet rs = command.ExecuteResultSet(new ResultSetOptions());
        //            List<KeyValuePair<string, string>> gals = new List<KeyValuePair<string, string>>();
        //            while (rs.Read())
        //            {

        //                string eventID = "";
        //                string galleryCode = "";
        //                if (!rs.IsDBNull(0))
        //                    eventID = rs.GetString(0);
        //                if (!rs.IsDBNull(1))
        //                    galleryCode = rs.GetString(1);




        //                if (!string.IsNullOrEmpty(eventID) && eventID.Contains("-"))
        //                {
        //                    gals.Add(new KeyValuePair<string, string>(eventID, galleryCode));
        //                }
        //            }
        //            rs.Close();
        //            foreach (KeyValuePair<string, string> gal in gals)
        //            {
        //                string eventID = gal.Key;
        //                string galleryCode = gal.Value;
        //                string newGalleryID = "";

        //                string apiUrl = getGalleryIdurlbase + eventID;
        //                string JsonResponse = GetJson(apiUrl, "", "", "");
        //                if (!string.IsNullOrEmpty(JsonResponse))
        //                {
        //                    newGalleryID = JsonResponse;
        //                }

        //                int tempGalId = 0;
        //                if (!string.IsNullOrEmpty(newGalleryID) && Int32.TryParse(newGalleryID, out tempGalId) && tempGalId > 0)
        //                {
        //                    string galleryURL = "https://vando.imagequix.com/g" + newGalleryID;

        //                    SqlCeCommand updatecommand = new SqlCeCommand("update ProjectGallery set EventID = '" + newGalleryID + "', GalleryCode = '" + newGalleryID + "', GalleryURL = '" + galleryURL + "' where EventID = '" + eventID + "'", cn);
        //                    updatecommand.ExecuteNonQuery();
        //                }

        //            }
        //            cn.Close();
        //        }
        //        catch (Exception e)
        //        {

        //        }
        //    }
        //}

        private string GetJson(string uri, string custId, string custPw, string postData)
        {
            logger.Info("u " + uri);
            HttpWebRequest rq = (HttpWebRequest)WebRequest.Create(uri);
            rq.Credentials = new NetworkCredential(custId, custPw);
            rq.Headers.Add("x-iq-token", "A15M876TJ8");
            rq.Headers.Add("x-iq-user", custId);
            rq.Timeout = (15000);//try for 15 seconds
            rq.Method = "GET";

            HttpWebResponse rs = null;
            int tryCount = 0;
            while (tryCount < 5)
            {
                try
                {
                    rs = (HttpWebResponse)rq.GetResponse();

                    StreamReader reader = new StreamReader(rs.GetResponseStream());
                    string jsonText = reader.ReadToEnd();

                    reader.Close();
                    rs.Close();
                    return jsonText;

                }
                catch (WebException e)
                {

                    tryCount++;

                    //if (tryCount == 5)
                    //    throw e;
                }
            }


            return null;
        }

        public void GenerateFlowGalleryList()
        {
            _liveFlowProjectGalleries = new List<ProjectGallerySummary>();
            foreach (FlowProject proj in this.FlowProjects)
            {
                try
                {
                    proj.FlowMasterDataContext = this;
                    ProjectGallerySummary ProjectGallerySummary = null;
                    string connectString = proj.ConnectionStringReadOnly;
                    if (proj.IsNetworkProject == true)
                        connectString = proj.NetworkConnectionStringReadOnly;


                    SqlCeConnection cn = new SqlCeConnection(connectString);
                    cn.Open();
                    SqlCeCommand command = new SqlCeCommand("select GalleryName, GalleryURL, YearbookPoses, GalleryCode from ProjectGallery", cn);
                    SqlCeResultSet rs = command.ExecuteResultSet(new ResultSetOptions());
                    while (rs.Read())
                    {
                        string galleryName = "";
                        string galleryURL = "";
                        int yearbookPoses = 0;
                        string eventID = "";

                        if (!rs.IsDBNull(0))
                            galleryName = rs.GetString(0);
                        if (!rs.IsDBNull(1))
                            galleryURL = rs.GetString(1);
                        if (!rs.IsDBNull(2))
                            yearbookPoses = rs.GetInt32(2);
                        if (!rs.IsDBNull(3))
                            eventID = rs.GetString(3);
                        if (string.IsNullOrEmpty(galleryURL) && !string.IsNullOrEmpty(eventID))
                            galleryURL = "https://vando.imagequix.com/g" + eventID;


                        if (!string.IsNullOrEmpty(galleryURL))
                            ProjectGallerySummary = new ProjectGallerySummary(proj.FlowProjectName, galleryName, proj, galleryURL, yearbookPoses, 0, eventID);
                    }

                    if (ProjectGallerySummary != null)
                    {
                        List<int> knownOrderIDs = new List<int>();
                        SqlCeCommand command2 = new SqlCeCommand("select distinct(IQOrderID) from OrderPackage where IQOrderID is not null", cn);
                        SqlCeResultSet rs2 = command2.ExecuteResultSet(new ResultSetOptions());
                        while (rs2.Read())
                        {
                            knownOrderIDs.Add(rs2.GetInt32(0));
                        }
                        rs.Close();
                        ProjectGallerySummary.knownOrderIDs = knownOrderIDs;

                        List<string> knownYB1ImageNames = new List<string>();
                        SqlCeCommand command3 = new SqlCeCommand("select a.ImageFileName from SubjectImage a, SubjectImageType b, ProjectImageType c where a.SubjectImageID = b.SubjectImageID AND b.ProjectImageTypeID = c.ProjectImageTypeID AND c.ProjectImageTypeDesc = 'Yearbook Pose' AND b.IsAssigned = 1", cn);
                        SqlCeResultSet rs3 = command3.ExecuteResultSet(new ResultSetOptions());
                        while (rs3.Read())
                        {
                            if (!knownYB1ImageNames.Contains(rs3.GetString(0)))
                                knownYB1ImageNames.Add(rs3.GetString(0));
                        }
                        rs.Close();
                        ProjectGallerySummary.knownYB1ImageNames = knownYB1ImageNames;

                        List<string> knownYB2ImageNames = new List<string>();
                        SqlCeCommand command4 = new SqlCeCommand("select a.ImageFileName from SubjectImage a, SubjectImageType b, ProjectImageType c where a.SubjectImageID = b.SubjectImageID AND b.ProjectImageTypeID = c.ProjectImageTypeID AND c.ProjectImageTypeDesc = 'Yearbook Pose 2' AND b.IsAssigned = 1", cn);
                        SqlCeResultSet rs4 = command4.ExecuteResultSet(new ResultSetOptions());
                        while (rs4.Read())
                        {
                            if (!knownYB2ImageNames.Contains(rs4.GetString(0)))
                                knownYB2ImageNames.Add(rs4.GetString(0));
                        }
                        rs.Close();
                        ProjectGallerySummary.knownYB2ImageNames = knownYB2ImageNames;

                        _liveFlowProjectGalleries.Add(ProjectGallerySummary);
                    }



                    cn.Close();
                    cn.Dispose();
                }
                catch (Exception e)
                {
                    //ignore all exceptions for now
                }
            }
        }
        //public List<ProjectGallery> LiveFlowProjectGalleries
        //{
        //    get
        //    {
        //        if (FlowProjectGalleries != null &&
        //            FlowProjectGalleries.Any(f => f.GalleryURL.Contains("vando")))
        //        {
        //            return FlowProjectGalleries.Where(f => f.GalleryURL.Contains("vando")).ToList();
        //        }
        //        else
        //            return new List<FlowProjectGallery>();
        //    }
        //}
        public void RefreshGalleryList()
        {
            //_liveFlowProjectGalleries = null;
            this.SendPropertyChanged("FlowProjectGalleries");
            this.SendPropertyChanged("LiveFlowProjectGalleries");
        }

        public void RefreshFlowCatalogList()
        {
            _flowCatalogList = null;
            this.SendPropertyChanged("FlowCatalogList");
        }

        public string TemplayoutFolder { get; set; }
        private List<string> _layoutFiles { get; set; }
        public List<string> LayoutFiles
        {
            get
            {
                if (_layoutFiles == null)
                {
                    if (String.IsNullOrEmpty(TemplayoutFolder))
                        TemplayoutFolder = this.PreferenceManager.Layout.LayoutsDirectory;

                    if (TemplayoutFolder.Length < this.PreferenceManager.Layout.LayoutsDirectory.Length)
                        TemplayoutFolder = this.PreferenceManager.Layout.LayoutsDirectory;

                    _layoutFiles = new List<string>();
                    _layoutFiles.Add("");
                    //_layoutFiles.Add("None");
                    if (Directory.Exists(TemplayoutFolder))
                    {
                        _layoutFiles.Add(".. (up a directory)");
                        foreach (string di in Directory.GetDirectories(TemplayoutFolder))
                            _layoutFiles.Add("[" + (new DirectoryInfo(di)).Name + "]");
                        _layoutFiles.Add("--------------------------");

                        foreach (string file in Directory.GetFiles(TemplayoutFolder, "*.lyt"))
                            _layoutFiles.Add(file.Replace(this.PreferenceManager.Layout.LayoutsDirectory, "").TrimStart('\\'));
                    }

                }
                return _layoutFiles;
            }
        }
        public void UpdateLayouts()
        {
            _layoutFiles = null;
            this.SendPropertyChanged("LayoutFiles");
        }

        public void RefreshFilteredProjectList()
        {
            _filteredFlowProjectList = null;
            this.SendPropertyChanged("FilteredFlowProjectList");
        }

        public bool UploadQueueHasItems
        {
            get
            {
                try
                {
                    if (this.OrderUploadHistoryQueueList != null && this.OrderUploadHistoryQueueList.Count() > 0)
                        return true;
                }
                catch
                {
                    return false;
                }

                return false;
            }
        }

        public ObservableCollection<FieldDetail> CustomReportFields { get; set; }


        public void UpdateCustomReportFields()
        {

            SendPropertyChanged("CustomReportFields");
        }


    }	// END class

    public class FieldDetail
    {
        public string Name { get; set; }
        public int Index { get; set; }

        public FieldDetail(string name, int index)
        {
            Name = name;
            Index = index;
        }
    }


}	// END namespace