﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flow.Schema.LinqModel.DataContext.FlowMaster
{
    public class IQOrder
    {
        public int OrderID { get; set; }
        public string EventID { get; set; }

        public IQOrder(int orderID, string eventID)
        {
            this.OrderID = orderID;
            this.EventID = eventID;
        }

    }
}
