﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Flow.Lib.Helpers;
using System.IO;

namespace Flow.Schema.LinqModel.DataContext
{
	partial class ProductPackageComposition
	{
		public event EventHandler ExpandedPriceUpdated = delegate { };
	
         public string DisplayLabel
        {
            get
            {
                if (Renderlayout)
                {
                    string lytName = Path.GetFileNameWithoutExtension(Layout);
                    //return lytName + " - (" + this.ImageQuixProduct.Width + "x" + this.ImageQuixProduct.Height +")";
                    return lytName;
                }
                return this.ImageQuixProduct.Label;
            }

        }
         public void UpdateDisplayLabel()
         {
             this.SendPropertyChanged("DisplayLabel");
         }
		public decimal ExpandedPrice
		{
			get { return this.ImageQuixProduct.Price * (decimal)this.Quantity; }
		}
		
		public ProductPackageComposition(ProductPackage package, ImageQuixProduct product)
		{
			this.ProductPackage = package;
			this.ImageQuixProduct = product;
			this.Quantity = 1;
            this.Layout = "";
			this.ExpandedPriceUpdated += new EventHandler(this.ProductPackage.ProductPackageComposition_ExpandedPriceChanged);
		}

        public bool Renderlayout
        {
            get
            {
                if (string.IsNullOrEmpty(this.Layout) || this.Layout == "None")
                    return false;
                return true;
            }

        }

		partial void OnLoaded()
		{
			this.ExpandedPriceUpdated += new EventHandler(this.ProductPackage.ProductPackageComposition_ExpandedPriceChanged);
		}

		partial void OnQuantityChanged()
		{
			this.ExpandedPriceUpdated(this, null);
		}

		public static ProductPackageComposition operator ++(ProductPackageComposition composition)
		{
            if (composition.ProductPackage.FlowCatalog.FlowMasterDataContext != null)
            {
                composition.Quantity++;
                composition.ProductPackage.FlowCatalog.FlowMasterDataContext.SubmitChanges();
            }
			return composition;
		}

		public static ProductPackageComposition operator --(ProductPackageComposition composition)
		{
            if (composition.ProductPackage.FlowCatalog.FlowMasterDataContext != null)
            {
                composition.Quantity--;

                if (composition.Quantity > 0)
                    composition.ProductPackage.FlowCatalog.FlowMasterDataContext.SubmitChanges();
            }

			return composition;
		}

        private bool _expandLayouts = false;
        public bool ExpandLayouts { get { return _expandLayouts; } set { _expandLayouts = value; SendPropertyChanged("ExpandLayouts"); } }
    }
}
