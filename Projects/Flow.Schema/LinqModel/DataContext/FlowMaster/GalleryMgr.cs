﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Flow.Schema.LinqModel.DataContext.FlowMaster
{
    public class GalleryMgr : INotifyPropertyChanged
    {

        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged = delegate { };

        public string ProjectName { get; set; }
        public Guid ProjectGuid { get; set; }
        public string EventID { get; set; }
        public string GallerySite { get; set; }
        //public int SubjectsOnline { get; set; }
        //public int ImagesOnline { get; set; }
        //public int TotalOnlineOrders { get; set; }
        private int _newOnlineOrders = 0;
        public int NewOnlineOrders { 
            get { return _newOnlineOrders; }
            set
            {
                _newOnlineOrders = value;
                SendPropertyChanged("NewOnlineOrders");
                
            } 
        }

        public IEnumerable<IQOrder> newOrders { get; set; }

        private bool _showGetOrderButton = false;
        public bool ShowGetOrderButton
        {
            get
            {
                if (NewOnlineOrders > 0)
                    _showGetOrderButton = true;
                else
                    _showGetOrderButton = false;
                return _showGetOrderButton;
            }
            set
            {
                _showGetOrderButton = value;
                SendPropertyChanged("ShowGetOrderButton");
                SendPropertyChanged("ShowDoneLabel");
            }
        }

        private bool _showDoneLabel = false;
        public bool ShowDoneLabel
        {
            get
            {
                return _showDoneLabel;
            }
            set
            {
                _showDoneLabel = value;
                SendPropertyChanged("ShowDoneLabel");
                SendPropertyChanged("ShowDoneLabel");
            }
        }

        private bool _showBlankLabel = true;
        public bool ShowBlankLabel
        {
            get
            {
                if (ShowDoneLabel == false && ShowGetOrderButton == false)
                    _showBlankLabel = true;
                else
                    _showBlankLabel = false;
                return _showBlankLabel;
            }
        }

        public void SendPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
