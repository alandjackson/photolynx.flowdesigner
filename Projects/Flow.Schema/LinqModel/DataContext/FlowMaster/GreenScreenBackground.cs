﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Imaging;
using System.IO;

namespace Flow.Schema.LinqModel.DataContext.FlowMaster
{
    public class GreenScreenBackground
    {
        private string _fullPath { get; set; }
        public string FullPath { get {return _fullPath;}  }

        private string _displayName { get; set; }
        public string DisplayName { get { return _displayName; } }

        private string _fileName { get; set; }
        public string FileName { get { return _fileName; } }

        private bool _isPremium { get; set; }
        public bool IsPremium { get { return _isPremium; } }

        private BitmapImage _backgroundImage { get; set; }
        public BitmapImage BackgroundImage { get { return _backgroundImage; } }

        

        public GreenScreenBackground(string pathToFile)
        {
            _fullPath = pathToFile;
            _fileName = (new FileInfo(pathToFile)).Name;
            _displayName = _fileName.Split(".".ToCharArray())[0];
           
            if (pathToFile.ToLower().Contains("premium")) _isPremium = true; else _isPremium = false;

            try
            {
                _backgroundImage = new BitmapImage();
                _backgroundImage.BeginInit();
                _backgroundImage.UriSource = new Uri(pathToFile);
                _backgroundImage.DecodePixelWidth = 200;
                _backgroundImage.EndInit();
            }
            catch
            {
                //do nothing
            }
        }
    }
}
