﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.Linq;
using System.Data.SqlServerCe;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Media.Imaging;
using System.Xml.Linq;

using Flow.Lib;
using Flow.Lib.Database.SqlCe;
using Flow.Lib.Helpers;
using Flow.Lib.Helpers.ImageQuix;
using Flow.Schema.Properties;
using Flow.Schema.Utility;

namespace Flow.Schema.LinqModel.DataContext
{
	public class PreferenceManager : IPreferenceManager
	{
		public event EventHandler<EventArgs<Preference>> PreferenceChanged = delegate { };
	
		private FlowMasterDataContext _flowMasterDataContext = null;
	
		private FlowObservableCollection<Preference> _preferenceList = null;
		public FlowObservableCollection<Preference> PreferenceList
		{
			get
			{
				if (_preferenceList == null)
				{
					_preferenceList = new FlowObservableCollection<Preference>(_flowMasterDataContext.Preferences);

					foreach (Preference preference in _preferenceList)
					{
						preference.PreferenceChanged += new EventHandler<EventArgs<Preference>>(Preference_PreferenceChanged);
					}
				}

				return _preferenceList;
			}
		}

		void Preference_PreferenceChanged(object sender, EventArgs<Preference> e)
		{
			this.PreferenceChanged(this, e);
		}
	
		public PreferenceManager(FlowMasterDataContext flowMasterDataContext)
		{
			_flowMasterDataContext = flowMasterDataContext;

			this.Capture = new CapturePreferenceContext(this);
			this.Upload = new UploadPreferenceContext(this);
            this.Layout = new LayoutDesignerPreferenceContext(this);
		}

		public Preference this[string preferenceIdentifier]
		{
			get { return GetPreference(preferenceIdentifier); }
		}

		public Preference GetPreference(string preferenceIdentifier)
		{
			Preference preference = this.PreferenceList.FirstOrDefault(p => p.PreferenceIdentifier == preferenceIdentifier);

			return preference;
		}

		public T GetPreferenceValue<T>(string preferenceIdentifier)
		{
			return this.GetPreference(preferenceIdentifier).GetValue<T>();
		}

		public void SavePreferences()
		{
			_flowMasterDataContext.SubmitChanges();
		}

		public CapturePreferenceContext Capture { get; private set; }

		public UploadPreferenceContext Upload { get; private set; }

        public LayoutDesignerPreferenceContext Layout{ get; private set; }


		public class CapturePreferenceContext : PreferenceContextBase
		{
			public string HotFolderDirectory
			{
				get { return (string)this.PreferenceManager[this["HotFolderDirectory"]].NativeValue; }
				set
				{
					this.PreferenceManager[this["HotFolderDirectory"]].NativeValue = value;
					this.SendNotifyPropertyChanged("HotFolderDirectory");
				}
			}

			public bool UseOverlayPng
			{
				get { return (bool)this.PreferenceManager[this["UseOverlayPng"]].NativeValue; }
				set
				{
					this.PreferenceManager[this["UseOverlayPng"]].NativeValue = value;
					this.SendNotifyPropertyChanged("UseOverlayPng");
				}
			}

			public string OverlayPngPath
			{
				get { return (string)this.PreferenceManager[this["OverlayPngPath"]].NativeValue; }
				set
				{
					this.PreferenceManager[this["OverlayPngPath"]].NativeValue = value;
					this.SendNotifyPropertyChanged("OverlayPngPath");
				}
			}

			public CapturePreferenceContext(PreferenceManager preferenceManager)
				: base(preferenceManager)
			{
				this.Description = "Capture";
			}

		}

		public class UploadPreferenceContext : PreferenceContextBase
		{
			public ProjectTransferSettingsPreferenceGroup ProjectTransferSettings { get; set; }
			public CatalogRequestPreferenceGroup CatalogRequest { get; set; }
			public OrderSubmissionPreferenceGroup OrderSubmission { get; set; }

			public UploadPreferenceContext(PreferenceManager preferenceManager) : base(preferenceManager)
			{
				this.ProjectTransferSettings = new ProjectTransferSettingsPreferenceGroup(preferenceManager);
				this.CatalogRequest = new CatalogRequestPreferenceGroup(preferenceManager);
				this.OrderSubmission = new OrderSubmissionPreferenceGroup(preferenceManager);
			}

			public class CatalogRequestPreferenceGroup : PreferenceContextBase
			{
				public string ImageQuixCatalogUrl
				{
					get { return (string)this.PreferenceManager[this["ImageQuixCatalogUrl"]].NativeValue; }
					set
					{
						this.PreferenceManager[this["ImageQuixCatalogUrl"]].NativeValue = value;
						this.SendNotifyPropertyChanged("ImageQuixCatalogUrl");
					}
				}

				public CatalogRequestPreferenceGroup(PreferenceManager preferenceManager)
					: base(preferenceManager)
				{
					this.Description = "Upload.CatalogRequest";
				}
			}


			public class OrderSubmissionPreferenceGroup : PreferenceContextBase
			{
				public string ImageQuixOrderSubmissionUrl
				{
					get { return (string)this.PreferenceManager[this["ImageQuixOrderSubmissionUrl"]].NativeValue; }
					set
					{
						this.PreferenceManager[this["ImageQuixOrderSubmissionUrl"]].NativeValue = value;
						this.SendNotifyPropertyChanged("ImageQuixOrderSubmissionUrl");
					}
				}

				public string ImageQuixFtpAddress
				{
					get { return (string)this.PreferenceManager[this["ImageQuixFtpAddress"]].NativeValue; }
					set
					{
						this.PreferenceManager[this["ImageQuixFtpAddress"]].NativeValue = value;
						this.SendNotifyPropertyChanged("ImageQuixFtpAddress");
					}
				}

				public int ImageQuixFtpPort
				{
					get { return (int)this.PreferenceManager[this["ImageQuixFtpPort"]].NativeValue; }
					set
					{
						this.PreferenceManager[this["ImageQuixFtpPort"]].NativeValue = value;
						this.SendNotifyPropertyChanged("ImageQuixFtpPort");
					}
				}

				public string ImageQuixFtpUserName
				{
					get { return (string)this.PreferenceManager[this["ImageQuixFtpUserName"]].NativeValue; }
					set
					{
						this.PreferenceManager[this["ImageQuixFtpUserName"]].NativeValue = value;
						this.SendNotifyPropertyChanged("ImageQuixFtpUserName");
					}
				}

				public string ImageQuixFtpPassword
				{
					get { return (string)this.PreferenceManager[this["ImageQuixFtpPassword"]].NativeValue; }
					set
					{
						this.PreferenceManager[this["ImageQuixFtpPassword"]].NativeValue = value;
						this.SendNotifyPropertyChanged("ImageQuixFtpPassword");
					}
				}

				public string ImageQuixFtpDirectory
				{
					get { return (string)this.PreferenceManager[this["ImageQuixFtpDirectory"]].NativeValue; }
					set
					{
						this.PreferenceManager[this["ImageQuixFtpDirectory"]].NativeValue = value;
						this.SendNotifyPropertyChanged("ImageQuixFtpDirectory");
					}
				}



				public OrderSubmissionPreferenceGroup(PreferenceManager preferenceManager)
					: base(preferenceManager)
				{
					this.Description = "Upload.OrderSubmission";
				}
			}




			public class ProjectTransferSettingsPreferenceGroup : PreferenceContextBase
			{
				public string HostAddress
				{
					get { return (string)this.PreferenceManager[this["HostAddress"]].NativeValue; }
					set
					{
						this.PreferenceManager[this["HostAddress"]].NativeValue = value;
						this.SendNotifyPropertyChanged("HostAddress");
					}
				}
				public int HostPort
				{
					get { return (int)this.PreferenceManager[this["HostPort"]].NativeValue; }
					private set
					{
						this.PreferenceManager[this["HostPort"]].NativeValue = value;
						this.SendNotifyPropertyChanged("HostPort");
					}
				}
				public string UserName
				{
					get { return (string)this.PreferenceManager[this["UserName"]].NativeValue; }
					set
					{
						this.PreferenceManager[this["UserName"]].NativeValue = value;
						this.SendNotifyPropertyChanged("UserName");
					}
				}
				public string Password
				{
					get { return (string)this.PreferenceManager[this["Password"]].NativeValue; }
					set
					{
						this.PreferenceManager[this["Password"]].NativeValue = value;
						this.SendNotifyPropertyChanged("Password");
					}
				}
				public string RemoteDirectory
				{
					get { return (string)this.PreferenceManager[this["RemoteDirectory"]].NativeValue; }
					set
					{
						this.PreferenceManager[this["RemoteDirectory"]].NativeValue = value;
						this.SendNotifyPropertyChanged("RemoteDirectory");
					}
				}
				public bool UseSftp
				{
					get { return (bool)this.PreferenceManager[this["UseSftp"]].NativeValue; }
					set
					{
						this.PreferenceManager[this["UseSftp"]].NativeValue = value;

						this.HostPort = (value) ? 22 : 21;
						
						this.SendNotifyPropertyChanged("UseSftp");
					}
				}
				
				public ProjectTransferSettingsPreferenceGroup(PreferenceManager preferenceManager) : base(preferenceManager)
				{
					this.Description = "Upload.ProjectTransferSettings";
				}
			}

		}


        public class LayoutDesignerPreferenceContext : PreferenceContextBase
        {
            public LayoutDesignerPreferenceGroup LayoutDesignerSettings { get; set; }

            public LayoutDesignerPreferenceContext(PreferenceManager preferenceManager)
                : base(preferenceManager)
            {
                this.LayoutDesignerSettings = new LayoutDesignerPreferenceGroup(preferenceManager);
            }


            public class LayoutDesignerPreferenceGroup : PreferenceContextBase
            {
                public string ImageOutputFolderPath
                {
                    get { return (string)this.PreferenceManager[this["ImageOutputFolderPath"]].NativeValue; }
                    set
                    {
                        this.PreferenceManager[this["ImageOutputFolderPath"]].NativeValue = value;
                        this.SendNotifyPropertyChanged("ImageOutputFolderPath");
                    }
                }
                public int ImageDPI
                {
                    get { return (int)this.PreferenceManager[this["ImageDPI"]].NativeValue; }
                    set
                    {
                        this.PreferenceManager[this["ImageDPI"]].NativeValue = value;
                        this.SendNotifyPropertyChanged("ImageDPI");
                    }
                }
                public string ImageFormat
                {
                    get { return (string)this.PreferenceManager[this["ImageFormat"]].NativeValue; }
                    set
                    {
                        this.PreferenceManager[this["ImageFormat"]].NativeValue = value;
                        this.SendNotifyPropertyChanged("ImageFormat");
                    }
                }
                public int PrintDestination
                {
                    get { return (int)this.PreferenceManager[this["PrintDestination"]].NativeValue; }
                    set
                    {
                        this.PreferenceManager[this["PrintDestination"]].NativeValue = value;
                        this.SendNotifyPropertyChanged("PrintDestination");
                    }
                }
               
                public LayoutDesignerPreferenceGroup(PreferenceManager preferenceManager)
                    : base(preferenceManager)
                {
                    this.Description = "Layout.LayoutDesignerSettings";
                }
            }

        }	
	
	
	}


	public abstract class PreferenceContextBase  : INotifyPropertyChanged
	{
		protected PreferenceManager PreferenceManager { get; set; }

		public string Description { get; protected set; }

		protected string this[string preferenceName]
		{
			get { return this.Description + "." + preferenceName; }
		}

		public event PropertyChangedEventHandler PropertyChanged;

		public PreferenceContextBase(PreferenceManager preferenceManager)
		{
			this.PreferenceManager = preferenceManager;
		}

		protected void SendNotifyPropertyChanged(string propertyName)
		{
			this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
		}
	}

}
