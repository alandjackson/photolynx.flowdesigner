﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data.Linq;
using System.Linq;
using System.Text;

namespace Flow.Schema.LinqModel.DataContext
{
	partial class FlowCatalogOption
	{
        //Every time a project is loaded, thes values need to be set to show if this option is used in the current project
        private bool _useOption = false;
        public bool UseOption
        {
            get
            {
                return _useOption;
            }
            set
            {
                _useOption = value;
                SendPropertyChanged("UseOption");
            }
        }

        public bool IsCustomizableOption
        {
            get
            {
                if (IsSubjectOrderOption || IsImageOption)
                    return true;
                else
                    return false;
            }
        }

        public bool IsSubjectOrderOption
        {
            get
            {
                if (this.ImageQuixCatalogOption.ImageQuixCatalogOptionGroup.Label.ToLower() == "subject order options")
                    return true;
                else
                    return false;
            }
        }

        public bool IsImageOption
        {
            get
            {
                if (this.ImageQuixCatalogOption.ImageQuixCatalogOptionGroup.Label.ToLower() == "image options" || this.ImageQuixCatalogOption.ImageQuixCatalogOptionGroup.IsImageOption)
                    return true;
                else
                    return false;
            }
        }

        public bool IsOrderOption
        {
            get
            {
                if (this.ImageQuixCatalogOption.ImageQuixCatalogOptionGroup.Label.ToLower() == "order options")
                    return true;
                else
                    return false;
            }
        }

       public FlowCatalogOption(FlowProjectDataContext importProjectDataContext, Control_FlowCatalogOption sourceCatalogOption, ImageQuixCatalog sourceImageQuixCatalog, FlowCatalog flowCatalog)
			: this()
		{

            this.FlowCatalogOptionGuid = sourceCatalogOption.FlowCatalogOptionGuid;
            this.Map = sourceCatalogOption.Map;
            this.Price = sourceCatalogOption.Price;
            this.Taxed = sourceCatalogOption.Taxed;
            this.IQPrimaryKey = sourceCatalogOption.IQPrimaryKey;

            if(sourceImageQuixCatalog.CatalogOptionList.Any(co=>co.PrimaryKey == this.IQPrimaryKey))
            {
                this.ImageQuixCatalogOptionID = sourceImageQuixCatalog.CatalogOptionList.First(co => co.PrimaryKey == this.IQPrimaryKey).ImageQuixCatalogOptionID;
                this.ImageQuixCatalogOption = sourceImageQuixCatalog.CatalogOptionList.First(co => co.PrimaryKey == this.IQPrimaryKey);
            }

           
		}

       public FlowCatalogOption(FlowCatalogOption sourceCatalogOption)
           : this()
       {

           this.FlowCatalogOptionGuid = Guid.NewGuid();
           this.Map = sourceCatalogOption.Map;
           this.Price = sourceCatalogOption.Price;
           this.Taxed = sourceCatalogOption.Taxed;
           this.IQPrimaryKey = sourceCatalogOption.IQPrimaryKey;

           this.ImageQuixCatalogOptionID = sourceCatalogOption.ImageQuixCatalogOptionID;
           this.ImageQuixCatalogOption = sourceCatalogOption.ImageQuixCatalogOption;


       }
	}
}
