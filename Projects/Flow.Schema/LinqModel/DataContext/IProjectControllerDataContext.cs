﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Flow.Schema.LinqModel.DataContext;

namespace Flow.Lib.Project
{
	public interface IProjectControllerDataContext
	{
		FlowMasterDataContext FlowMasterDataContext { get; }
		FlowProject CurrentFlowProject { get; set; }
		ProjectTemplate CurrentProjectTemplate { get; set; }
	}
}
