﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.Schema.Properties;
using Flow.Lib;
using System.IO;
using Flow.Lib.Helpers;
using Flow.Schema.LinqModel.DataContext;

namespace Flow.Schema.LinqModel.FlowMaster
{
    public class FlowMasterRepository
    {
        public FlowMasterDataContext BuildStandardDataContext()
        {
            return new FlowMasterDataContext();
        }


        public FlowMasterDataContext BuildCommonUserDataContext()
        {
            string cnnString = Settings.Default.FlowMasterConnectionString;

            string usersDataPath = GetFilename(cnnString.Replace("|DataDirectory|", 
                FlowContext.FlowCommonAppDataDirPath));
            string programDataPath = GetFilename(cnnString.Replace("|DataDirectory|", 
                AppDomain.CurrentDomain.BaseDirectory));

            // check to see if user file exists
            if (!File.Exists(usersDataPath))
            {
                // make sure program file exists
                if (!File.Exists(programDataPath))
                    throw new FileNotFoundException("Flow Master source database not found", programDataPath);

                // make sure user app dir exists
                IOUtil.CreateDirectory(new FileInfo(usersDataPath).Directory, true);

                // copy to user space
                File.Copy(programDataPath, usersDataPath);
            }

            return new FlowMasterDataContext("Data Source=" + usersDataPath);
        }

        protected string GetFilename(string cnnString)
        {
            int ndx = cnnString.IndexOf("Data Source=");
            if (ndx < 0 || ndx >= cnnString.Length) return "";

            int lastNdx = cnnString.IndexOf(";", ndx + 12);

            if (lastNdx < 0 || lastNdx >= cnnString.Length)
                return cnnString.Substring(ndx + 12);
            else
                return cnnString.Substring(ndx + 12, lastNdx - ndx);

        }

    }
}
