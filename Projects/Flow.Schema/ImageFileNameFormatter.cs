﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

using Flow.Lib.Helpers;
using Flow.Schema.LinqModel.DataContext;

namespace Flow.Schema
{
	/// <summary>
	/// Manages the formatting of image filenames
	/// </summary>
	public class ImageFileNameFormatter
	{
		public static readonly int IMAGE_FORMAT_TYPE_ORIGINAL = 1;
		public static readonly int IMAGE_FORMAT_TYPE_APPEND = 2;
		public static readonly int IMAGE_FORMAT_TYPE_REPLACE = 3;
	
		
		private FlowProject _flowProject = null;

		private int _formatTypeID = IMAGE_FORMAT_TYPE_ORIGINAL;

		private string _separatorChar = "-";

		private ImageFileNameUnit _unit1 = null;
		private ImageFileNameUnit _unit2 = null;
		private ImageFileNameUnit _unit3 = null;

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="flowProject"></param>
		public ImageFileNameFormatter(FlowProject flowProject)
		{
			_flowProject = flowProject;

			_formatTypeID = _flowProject.ImageFormatTypeID;

			_separatorChar = _flowProject.ImageFormatSeparatorChar.IfNull("-");

			if (_flowProject.ImageFormatTypeID != IMAGE_FORMAT_TYPE_ORIGINAL)
			{
				if (_flowProject.ImageFormatField1 != null)
				{
					_unit1 = CreateImageFileNameUnit(_flowProject.ImageFormatField1);
                }
				if (_flowProject.ImageFormatField2 != null)
				{
					_unit2 = CreateImageFileNameUnit(_flowProject.ImageFormatField2);
                }
				if (_flowProject.ImageFormatField3 != null)
                {
					_unit3 = CreateImageFileNameUnit(_flowProject.ImageFormatField3);
				}
				
			}
		}

		/// <summary>
		/// Returns the formatted filename of the supplied filename
		/// </summary>
		/// <param name="subject"></param>
		/// <param name="originalImageFileName"></param>
		/// <returns>string</returns>
		public string GetFormattedImageFileName(Subject subject, string originalImageFileName)
		{
            //this._flowProject.
			
            // Get the next image sequencing ID
			string retVal = _flowProject.GetNewImageSequenceID();

			// Find the last '.' character, specifying the extension delimiter
			int extensionDelimiterIndex = originalImageFileName.LastIndexOf('.');

			// Append the extension to the sequence ID, creating the base for the filename
			retVal += originalImageFileName.Substring(extensionDelimiterIndex);

			// Remove the extension from the original filename
			originalImageFileName = originalImageFileName.Remove(extensionDelimiterIndex);

			// If the original filename is not to be replaced, prepend it to the formatted filename
			if (_formatTypeID != IMAGE_FORMAT_TYPE_REPLACE)
			{
				retVal = originalImageFileName + _separatorChar + retVal;
			}

			// If the formatted filename includes external items (data field-values or custom strings)...
			if(_formatTypeID != IMAGE_FORMAT_TYPE_ORIGINAL)
			{
				// Prepend filename units, where applicable;
				// process in reverse order (3,2,1) so the addtional items are in the correct right-to-left order
				this.SetFileNameFromUnit(ref retVal, _unit3, subject);
				this.SetFileNameFromUnit(ref retVal, _unit2, subject);
				this.SetFileNameFromUnit(ref retVal, _unit1, subject);
			}

			// Replace spaces; temp fix for fact that separator is stored as nchar(2) in the db, resulting in a trailing space
			retVal = retVal.Replace(" ", "");
	
			return retVal;
		}

		/// <summary>
		/// Creates an ImageFileNameUnit (represents a data-driven portion of a filename)
		/// </summary>
		/// <param name="imageFileNameFormatField"></param>
		/// <returns>ImageFileNameUnit</returns>
		private ImageFileNameUnit CreateImageFileNameUnit(string imageFileNameFormatField)
		{
			ImageFileNameUnit retVal = null;
	
			// If the value represents a database field
			if(imageFileNameFormatField.StartsWith("~"))
			{
				// Remove the database field indicator (~)
				imageFileNameFormatField = imageFileNameFormatField.Remove(0, 1);

				switch (imageFileNameFormatField)
				{
					// The name of the photographer assigned to the project
					case "PhotographerName":
                        try
                        {
                            if (!string.IsNullOrEmpty(this._flowProject.FlowMasterDataContext.LoginID))
                                retVal = new ImageFileNameUnit(
                                false,
                                this._flowProject.FlowMasterDataContext.LoginID
                                );
                            else
                                retVal = new ImageFileNameUnit(
                                    false,
                                    _flowProject.Photographer.UserLastName + _flowProject.Photographer.UserFirstName
                                );
                        }
                        catch
                        {
                            if(!string.IsNullOrEmpty(this._flowProject.FlowMasterDataContext.LoginID))
                                retVal = new ImageFileNameUnit(
                                false,
                                this._flowProject.FlowMasterDataContext.LoginID
                                );
                            else
                                retVal = new ImageFileNameUnit(
                                    false,
                                    "UnknownPhotographer"
                                );
                        }

						break;

					// The name of the project
					case "FlowProjectName":
						retVal = new ImageFileNameUnit(false, _flowProject.FlowProjectName);
						break;

					// The date the project was created
					// NOTE: this may need to be changed to the date the project begins
					case "FlowProjectDate":
						retVal = new ImageFileNameUnit(false, _flowProject.DateCreated.ToString("yyyyMMdd"));
						break;
				
					// Assume that some Subject field-value is to be included in the filename
					default:
						string[] tokens = imageFileNameFormatField.Split('.');

						// If Subject is not specified, let everybody know
						if (tokens.Length != 2 || tokens[0] != "Subject")
						{
							throw new Exception(
								String.Format("Invalid image filename format field reference: {0}", imageFileNameFormatField)
							);
						}

						retVal = new ImageFileNameUnit(true, tokens[1]);
						break;
				}
	
			}

			// If the value represents a Custom value (a literal
			else
			{
				retVal =  new ImageFileNameUnit(false, imageFileNameFormatField);
			}

			return retVal;
		}

		/// <summary>
		/// Prepends a formatting unit, either a Subject field-value or a custom string
		/// </summary>
		/// <param name="newFileName"></param>
		/// <param name="unit"></param>
		/// <param name="subject"></param>
		private void SetFileNameFromUnit(ref string newFileName, ImageFileNameUnit unit, Subject subject)
		{
			if (unit != null)
			{
				string unitValue = (unit.IsSubjectFieldName)
					// Unit is a Subject field-value, replace invalid characters
					? Regex.Replace(subject[unit.Value], @"[^A-Za-z0-9_-]", String.Empty)
					// Unit is a custom value (literal)
					: unit.Value;

				// Prepend the value
				newFileName = unitValue + _separatorChar + newFileName;
			}
		}

	}

}
