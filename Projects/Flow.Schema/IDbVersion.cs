﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flow.Schema
{
	public interface IDbVersion
	{
		Guid DbVersionGuid { get; set; }
	}

	public class DbVersionComparer : IEqualityComparer<IDbVersion>
	{
		public bool Equals(IDbVersion x, IDbVersion y)
		{
			// Check whether the compared objects reference the same data.
			if (Object.ReferenceEquals(x, y)) return true;

			// Check whether any of the compared objects is null.
			if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
				return false;

			return x.DbVersionGuid == y.DbVersionGuid;
		}

		// If Equals() returns true for a pair of objects,
		// GetHashCode must return the same value for these objects.

		public int GetHashCode(IDbVersion item)
		{
			// Check whether the object is null.
			if (Object.ReferenceEquals(item, null)) return 0;

			return item.DbVersionGuid.GetHashCode();
		}

	}
	


}
