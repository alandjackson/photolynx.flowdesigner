﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Data.Common;

using System.Data.OleDb;
using System.Data.SqlServerCe;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

using Flow.Schema.Import;
using Flow.Schema.LinqModel.DataContext;
using System.Data.Odbc;

namespace Flow.Schema.Utility
{
	/// <summary>
	/// Provides a set of static methods for connection to external data sources
	/// </summary>
	public static class DataUtility
	{
		/// <summary>
		/// A RegEx expression for normalizing (removing invalid characters from) a provisional database
		/// field name
		/// </summary>
		public static string FieldNameNormalPattern = @"![A-Za-z0-9_- ]";

		/// <summary>
		/// Extension method for normalizing field names
		/// </summary>
		/// <param name="text"></param>
		/// <returns></returns>
		public static string FieldNameNormalize (this string text)
		{
			return Regex.Replace(text, DataUtility.FieldNameNormalPattern, String.Empty);
		}

		/// <summary>
		/// Returns a data adapter for filling a data structure (DataTable, etc.) from a data source
		/// </summary>
		/// <param name="dataSource"></param>
		/// <returns></returns>
		public static DbDataAdapter GetDataAdapter(FlowImportDataSource dataSource)
		{
			DbDataAdapter retVal = null;

			switch (dataSource.SourceType)
			{
				case FlowImportDataSourceType.FlowProject:
					retVal = new SqlCeDataAdapter(dataSource.SelectCommand, dataSource.ConnectionString);
					break;
                case FlowImportDataSourceType.DelimitedText:
                    retVal = new OdbcDataAdapter(dataSource.SelectCommand, dataSource.ConnectionString);
                    break;
				default:
					retVal = new OleDbDataAdapter(dataSource.SelectCommand, dataSource.ConnectionString);
					break;
			}

			// Retrieves schema information from the data source for application to the
			// target data structure
			retVal.MissingSchemaAction = MissingSchemaAction.Add;

			return retVal;
		}

		/// <summary>
		/// Extension method for returning the FieldDataType by (system) Type or FieldDataTypeID as specified in
		/// the supplied lambda expression
		/// </summary>
		/// <param name="dataTypes"></param>
		/// <param name="del"></param>
		public static FieldDataType GetFieldDataType(this FlowObservableCollection<FieldDataType> dataTypes, Func<FieldDataType,bool> del)
		{
			return dataTypes
				.Where(del)
				.DefaultIfEmpty(FlowMasterDataContext.DefaultDataType)
				.First()
			;
		}

	}
}
