﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flow.Schema.Interface
{
	public interface ICustomField
	{
		string FieldDesc { get; }
		int FieldDataTypeID { get; }
		Type FieldDataType { get; }
	}
}
