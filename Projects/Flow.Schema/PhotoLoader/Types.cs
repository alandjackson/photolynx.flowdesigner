﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flow.Schema.PhotoLoader
{
    public enum SourceType
    {
        LocalDisk
    }

    public enum DisplayOptions
    {
        Preview,
        FullResolution
    }
}
