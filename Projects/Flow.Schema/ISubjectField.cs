﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

using Flow.Schema.LinqModel.DataContext;

namespace Flow.Schema
{
	/// <summary>
	/// Represents a generic implementation of user-defined Subject fields in the application
	/// </summary>
	public interface ISubjectField : INotifyPropertyChanging, INotifyPropertyChanged
	{
		string SubjectFieldDisplayName { get; set; }
		string SubjectFieldDesc { get; }
		FieldDataType SubjectFieldDataType { get; set; }
		bool IsKeyField { get; set; }
		bool IsRequiredField { get; set; }
		bool IsScanKeyField { get; set; }
		bool IsProminentField { get; set; }
		bool IsSearchableField { get; set; }
		ISubjectField Clone();
	}
}
