using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Configuration;
using Activation.Web.Service.Activation;

namespace Activation.Web.Controllers
{
    public class ActivationController : Controller
    {
        //
        // GET: /Activation/
        [ValidateInput(false)]
        public void Index()
        {
            if (Request.HttpMethod == "POST")
            {
                try
                {
                    string featureName =
                        ConfigurationManager.AppSettings["FeatureName"];
                    DateTime expiration = DateTime.Parse(
                        ConfigurationManager.AppSettings["Expiration"]);

                    WebServiceActivatorMessage srcMsg =
                        ObjectSerializer.FromXmlString<WebServiceActivatorMessage>(
                        Request.Form["WebServiceActivatorMessage"]);

                    if (! srcMsg.ActivationStore.EnabledFeatures.Contains(featureName))
                        srcMsg.ActivationStore.EnabledFeatures.Add(featureName);
                    srcMsg.ActivationStore.ActivatedEnd = expiration;

                    Response.Write(ObjectSerializer.ToXmlString(srcMsg));
                }
                catch (Exception e)
                {
                    Response.Write("Exception: " + e.Message);
                }
            }
            else
            {       
                Response.Write("PhotoLynx Flow Activation Service");
            }
        }
    }
}
