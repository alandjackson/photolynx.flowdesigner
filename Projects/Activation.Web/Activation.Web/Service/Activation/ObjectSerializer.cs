﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Serialization;
using System.Text;

namespace Activation.Web.Service.Activation
{
    /// <summary>
    /// Wrapper function for serializing and deserializing objects
    /// to xml as files or strings
    /// </summary>
    public class ObjectSerializer
    {
        public static string ToXmlString(object item)
        {
            XmlSerializer serializer = new XmlSerializer(item.GetType());

            using (MemoryStream ms = new MemoryStream())
            {
                using (XmlTextWriter tw = new XmlTextWriter(ms, Encoding.UTF8))
                {
                    tw.Formatting = Formatting.Indented;
                    serializer.Serialize(tw, item);
                    return UTF8Encoding.UTF8.GetString(ms.ToArray());
                }
            }
        }


        public static T FromXmlString<T>(string xmlStr)
        {
            object o;
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            using (MemoryStream ms = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(xmlStr)))
                o = serializer.Deserialize(ms);
            return (T)o;

        }
    }
}


