﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Activation.Web.Service.Activation
{
    public class WebServiceActivatorMessage
    {
        public ActivationStore ActivationStore { get; set; }
        public string ErrorMsg { get; set; }
    }
}
