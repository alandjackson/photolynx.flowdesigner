﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Activation.Web.Service.Activation
{
    public class ActivationStore
    {
        public string ComputerId { get; set; }
        public List<string> EnabledFeatures { get; set; }
        public DateTime ActivatedStart { get; set; }
        public DateTime ActivatedEnd { get; set; }
        public DateTime LastRanTime { get; set; }
    }
}
