﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using NUnit.Framework;
using System.Data.Linq;

namespace Flow.UnitTests2.Unit
{
    [TestFixture]
    public class LinqTest
    {
        [Test]
        public void MainTest()
        {
            DataContext db = new DataContext("SqlCe/FlowProject.sdf");
            Table<Flow.Schema.LinqModel.Subject> Subjects = db.GetTable<Flow.Schema.LinqModel.Subject>();
            var q = from c in Subjects select c;
            foreach (var cust in q)
                Console.WriteLine("Id: {0}, Name: {1} {2}", cust.SubjectId, cust.FirstName, cust.LastName);
        }
    }
}
