﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using Flow.Designer.Model;
using Flow.Lib;
using System.Windows.Media.Effects;
using Microsoft.Samples.CustomControls;
using Flow.Designer.Model.Project;
using Flow.Designer.Model.Designer;

namespace Flow.Designer.View
{
    /// <summary>
    /// Interaction logic for ImagePropertiesPanel.xaml
    /// </summary>
    public partial class ImagePropertiesPanel : UserControl
    {
        private string GraphicsDir;

       
        
        public ImagePropertiesPanel()
        {
            InitializeComponent(); 
        }

        public ImagePropertiesPanel(ImageTypes ImageTypes, string GraphicsDir)
        {
            this.GraphicsDir = GraphicsDir;
            InitializeComponent();
            GetImageTypes(ImageTypes);
        }

        protected LayoutImage LayoutImage
        {
            get
            {
                if (DataContext == null || !(DataContext is PropertiesPanelModel))
                    return null;
                return (LayoutImage) ((PropertiesPanelModel)DataContext).LayoutItem;
            }
        }

        private void GetImageTypes(ImageTypes ImageTypes)
        {
            if (ImageTypes != null)
            foreach (string key in ImageTypes.Fields.Keys)
            {
                cmbImageTypes.Items.Add(key);
            }
        }
        private void uxBrowseImage_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.InitialDirectory = FlowContext.InitDir(GraphicsDir);
            dlg.Title = "Select an image to add.";
            dlg.Filter = "Image Files (*.jpg, *.jpeg, *.png, *.gif, *.bmp)|*.jpg;*.gif;*.bmp;*.png;*.jpeg|All Files (*.*)|*.*";
            if (dlg.ShowDialog() != true)
                return;

            LayoutImage.ImageFilename = dlg.FileName;

        }


        private void uxStaticImage_Checked(object sender, RoutedEventArgs e)
        {
            LayoutImage.IsStaticImage = true;
        }

        private void uxDynamicImage_Checked(object sender, RoutedEventArgs e)
        {
            LayoutImage.IsDynamicImage = true;
        }

        private void uxSelectStrokeColor_Click(object sender, RoutedEventArgs e)
        {
            ColorPickerDialog dlg = new ColorPickerDialog();
            dlg.StartingColor = LayoutImage.StrokeColor;
            if (dlg.ShowDialog() != true)
                return;
            LayoutImage.StrokeColor = dlg.SelectedColor;
        }

        private void uxSelectDropShadowColor_Click(object sender, RoutedEventArgs e)
        {
            ColorPickerDialog dlg = new ColorPickerDialog();
            dlg.StartingColor = LayoutImage.DropShadow.Color;
            if (dlg.ShowDialog() != true)
                return;
            LayoutImage.DropShadow.Color = dlg.SelectedColor;
        }

        private void cmbImageTypes_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //if(cmbImageTypes.SelectedValue != null)
            //    LayoutImage.CurrentSubjectImageType = cmbImageTypes.SelectedValue.ToString();
        }

        private void uxImageSize_Click(object sender, RoutedEventArgs e)
        {
            if (sender == ux8x10)
                SetLayoutElementSize(new Size(8, 10));
            else if (sender == ux10x8)
                SetLayoutElementSize(new Size(10, 8));
            else if (sender == ux5x7)
                SetLayoutElementSize(new Size(5, 7));
            else if (sender == ux7x5)
                SetLayoutElementSize(new Size(7, 5));
            else if (sender == ux35x5)
                SetLayoutElementSize(new Size(3.5, 5));
            else if (sender == ux5x35)
                SetLayoutElementSize(new Size(5, 3.5));
            else if (sender == ux175x25)
                SetLayoutElementSize(new Size(1.75, 2.5));
            else if (sender == ux25x175)
                SetLayoutElementSize(new Size(2.5, 1.75));
        }

        protected void SetLayoutElementSize(Size size)
        {
            if (!(DataContext is PropertiesPanelModel)) return;
            var dc = (PropertiesPanelModel)DataContext;
            if (!(dc.LayoutItem is AbstractLayoutElement)) return;
            var element = (AbstractLayoutElement) dc.LayoutItem;
            element.Width = size.Width * 96;
            element.Height = size.Height * 96;
        }

        private void uxAllSubjectImages_Checked(object sender, RoutedEventArgs e)
        {
            LayoutImage.IsAllSubjectImages = true;
        }
    }
}
