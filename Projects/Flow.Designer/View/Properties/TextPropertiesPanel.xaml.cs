﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Samples.CustomControls;
using Flow.Designer.Model;
using Flow.Designer.Model.Designer;


namespace Flow.Designer.View
{
    /// <summary>
    /// Interaction logic for TextPropertiesPanel.xaml
    /// </summary>
    public partial class TextPropertiesPanel : UserControl
    {
        public TextPropertiesPanel()
            : this(new string[0])
        {
        }

        public TextPropertiesPanel(string [] subjectFields)
        {
            InitializeComponent();

            foreach (string subjectField in subjectFields)
                uxDataFieldSelector.Items.Add(subjectField);

            Loaded += TextPropertiesPanel_Loaded;
            uxDataFieldSelector.SelectionChanged += uxDataFieldSelector_SelectionChanged;
            DataContextChanged += TextPropertiesPanel_DataContextChanged;
        }

        void TextPropertiesPanel_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            object data = this.DataContext;
        }

        void uxDataFieldSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // make sure a data field was selected
            if (uxDataFieldSelector.SelectedIndex <= 0) return;

            // make sure there is a valid layout text bound to this control
            LayoutText lt = LayoutText;
            if (lt == null) return;

            // get rid of initial, default text if that is all that is there
            if (lt.Text == "New Text")
                lt.Text = "";

            lt.Text += string.Format("[{0}]", uxDataFieldSelector.SelectedItem.ToString());

            // Reset the selection
            uxDataFieldSelector.SelectedIndex = 0;
        }

        public LayoutText LayoutText
        {
            get
            {
                if (DataContext == null || !(DataContext is PropertiesPanelModel))
                    return null;
                return (DataContext as PropertiesPanelModel).LayoutItem as LayoutText;
            }
        }

        void TextPropertiesPanel_Loaded(object sender, RoutedEventArgs e)
        {

            //foreach (FontFamily ff in System.Windows.Media.Fonts.SystemFontFamilies)
            //{
            //    if (ff.ToString().StartsWith("BC"))
            //    {
            //        int i = 1;
            //    }
            //}

            //string fontsfolder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Fonts);
            //uxFontFamilies.ItemsSource = System.Windows.Media.Fonts.GetFontFamilies(fontsfolder).OrderBy(f => f.ToString());

            //this is the orginal
            //uxFontFamilies.ItemsSource = Fonts.SystemFontFamilies.OrderBy(f => f.ToString());




            List<FontFamily> ffamilies = System.Windows.Media.Fonts.SystemFontFamilies.ToList();//.OrderBy(f => f.ToString());

            System.Drawing.Text.InstalledFontCollection fontsCollection = new System.Drawing.Text.InstalledFontCollection();
            foreach (System.Drawing.FontFamily theFam in fontsCollection.Families)
            {

                if (theFam.Name.ToLower().StartsWith("bc") || theFam.Name.ToLower().Contains("barcode"))
                {

                    if (theFam.IsStyleAvailable(System.Drawing.FontStyle.Regular))
                    {
                        System.Drawing.Font font = new System.Drawing.Font(theFam.Name, 10);
                        FontFamily mfont = new FontFamily(font.Name);
                        if (!ffamilies.Contains(mfont))
                            ffamilies.Add(mfont);
                    }
                    else if (theFam.IsStyleAvailable(System.Drawing.FontStyle.Bold))
                    {
                        System.Drawing.Font font = new System.Drawing.Font(theFam.Name, 10, System.Drawing.FontStyle.Bold);
                        FontFamily mfont = new FontFamily(font.Name);
                        if (!ffamilies.Contains(mfont))
                            ffamilies.Add(mfont);
                    }
                    
                }
            }
            uxFontFamilies.ItemsSource = ffamilies.OrderBy(f => f.ToString());

        }

        private void uxSelectColor_Click(object sender, RoutedEventArgs e)
        {
            ColorPickerDialog dlg = new ColorPickerDialog();
            dlg.StartingColor = LayoutText.Color;
            if (dlg.ShowDialog() != true)
                return;
            LayoutText.Color = dlg.SelectedColor;
        }

        private void uxSelectDropShadowColor_Click(object sender, RoutedEventArgs e)
        {
            ColorPickerDialog dlg = new ColorPickerDialog();
            dlg.StartingColor = LayoutText.DropShadow.Color;
            if (dlg.ShowDialog() != true)
                return;
            LayoutText.DropShadow.Color = dlg.SelectedColor;
        }


        private void uxSelectStrokeColor_Click(object sender, RoutedEventArgs e)
        {
            ColorPickerDialog dlg = new ColorPickerDialog();
            dlg.StartingColor = LayoutText.StrokeColor;
            if (dlg.ShowDialog() != true)
                return;
            LayoutText.StrokeColor = dlg.SelectedColor;
        }

        private void uxLeftAlignment_Click(object sender, RoutedEventArgs e)
        {
            LayoutText.TextAlignment = TextAlignment.Left;
        }

        private void uxCenterAlignment_Click(object sender, RoutedEventArgs e)
        {
            LayoutText.TextAlignment = TextAlignment.Center;
        }

        private void uxRightAlignment_Click(object sender, RoutedEventArgs e)
        {
            LayoutText.TextAlignment = TextAlignment.Right;
        }

    }
}
