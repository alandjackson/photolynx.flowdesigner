﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using Flow.Designer.Model;
using Flow.Lib;
using System.Windows.Media.Effects;
using Microsoft.Samples.CustomControls;
using Flow.Designer.Model.Project;
using Flow.Designer.Lib.Converters;
using Flow.Designer.Model.Designer;

namespace Flow.Designer.View.Properties
{
    /// <summary>
    /// Interaction logic for ImagePropertiesPanel.xaml
    /// </summary>
    public partial class NodePropertiesPanel : UserControl
    {

        public NodePropertiesPanel()
        {
            this.DataContextChanged += new DependencyPropertyChangedEventHandler(NodePropertiesPanel_DataContextChanged);
            InitializeComponent(); 
            //this.Resources[
        }

        void NodePropertiesPanel_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (Resources["PixelGraphicsUnitConverter"] == null
                || !(Resources["PixelGraphicsUnitConverter"] is PixelGraphicsUnitConverter)
                || !(e.NewValue is PropertiesPanelModel))
                return;

            var conv = (PixelGraphicsUnitConverter) this.Resources["PixelGraphicsUnitConverter"];
            var m = (PropertiesPanelModel) e.NewValue;

            conv.GraphicsUnits = m.GraphicsUnits;
        }




    }
}
