﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Samples.CustomControls;
using Flow.Designer.Model.Designer;
using Flow.Designer.Model;

namespace Flow.Designer.View
{
    /// <summary>
    /// Interaction logic for SurfacePropertiesPanel.xaml
    /// </summary>
    public partial class SurfacePropertiesPanel : UserControl
    {
        public SurfacePropertiesPanel()
        {
            InitializeComponent();            
        }

        
        private void uxEditBackgroundColor_Click(object sender, RoutedEventArgs e)
        {
            ColorPickerDialog cPicker = new ColorPickerDialog();

            bool? dialogResult = cPicker.ShowDialog();
            if (dialogResult != null && (bool)dialogResult == true)
            {
                PropertiesPanelModel model = this.DataContext as PropertiesPanelModel;
                if (model != null && model.LayoutItem is Layout)
                {
                    ((Layout)model.LayoutItem).BackgroundColor = cPicker.SelectedColor;
                }
            }
        }
    }

    public class ColorStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            if (value == null) return null;

            if (value is Color)
            {
                return value.ToString();
            }
            throw new NotImplementedException("ColorStringConverter can't convert type: " + value.GetType().Name);
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
                return Colors.White;

            if (value is string && targetType == typeof(System.Windows.Media.Color))
            {
                var converter = new System.Drawing.ColorConverter();

                if (! converter.IsValid(value)) return Colors.White;

                System.Drawing.Color c = (System.Drawing.Color)converter.ConvertFromString(value.ToString());
                System.Windows.Media.Color mc = System.Windows.Media.Color.FromArgb(c.A, c.R, c.G, c.B);
                return mc;
            }


            throw new NotImplementedException("ColorStringConverter can't convert type: " 
                + value.GetType().Name 
                + " to " + targetType.FullName);
        }
    }
}
