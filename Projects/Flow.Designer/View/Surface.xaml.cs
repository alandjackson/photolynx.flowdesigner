﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Flow.Designer.Model;
using Flow.Designer.Helpers.SurfaceUi;
using Flow.Lib.Helpers;
using System.IO;
using Microsoft.Win32;
using Flow.Designer.Model.Designer;
using System.ComponentModel;
using Flow.Lib;
using Flow.Lib.Persister;
using Flow.Designer.Lib.SurfaceUi;
using Flow.Designer.Lib.ProjectInterop;
using StructureMap;
using Flow.Designer.Lib.Service;
using Flow.Designer.Lib.Service.SnapToService;
using Flow.Lib.Preferences;
using Flow.Designer.Lib.Converters;
using Flow.Controls;
using Flow.Controls.View.Dialogs;

namespace Flow.Designer.View
{
    /// <summary>
    /// Interaction logic for Surface.xaml
    /// </summary>
    public partial class Surface : UserControl
    {
        public event EventHandler SurfaceLoaded = null;
        protected void OnSurfaceLoaded() { if (SurfaceLoaded != null) SurfaceLoaded(this, EventArgs.Empty); }

        LayoutItemAligner LayoutItemAligner { get; set; }

        public LayoutPreferences LayoutPreferences { get; set; }

        public string TempLayoutDir { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="mes"></param>
        /// <param name="transformer"></param>
        public Surface() : this (ObjectFactory.GetInstance<LayoutItemAligner>()) {}

        public Surface(LayoutItemAligner aligner)
        {
            // TODO: use structuremap auto wiring
            LayoutItemAligner = aligner;

            OModel = null;
            SurfaceLayoutItems = new Dictionary<ILayoutItem, FrameworkElement[]>();
            BoundLayoutItems = new Dictionary<FrameworkElement, ILayoutItem>();

            InitializeComponent();

            uxLoadBtnMenu.PlacementTarget = uxLoadbtn;
            uxSaveBtnMenu.PlacementTarget = uxSave;

            uxLoadbtn.Click += new RoutedEventHandler(uxLoadbtn_Click);
            DataContextChanged += new DependencyPropertyChangedEventHandler(Surface_DataContextChanged);
            Loaded += new RoutedEventHandler(Surface_Loaded);
            uxLayoutCanvas.Loaded += new RoutedEventHandler(uxLayoutCanvas_Loaded);
            uxViewGridPanel.DataContextChanged += new DependencyPropertyChangedEventHandler(uxViewGridPanel_DataContextChanged);
        }

        void uxViewGridPanel_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (uxViewGridPanel.Resources["PixelGraphicsUnitConverter"] == null
                || !(uxViewGridPanel.Resources["PixelGraphicsUnitConverter"] is PixelGraphicsUnitConverter)
                || !(e.NewValue is ViewGridPanelModel))
                return;

            var conv = (PixelGraphicsUnitConverter)uxViewGridPanel.Resources["PixelGraphicsUnitConverter"];
            var m = (ViewGridPanelModel)e.NewValue;

            conv.GraphicsUnits = m.GraphicsUnits;
            VerifySubmitOrderSettings();
        }

        void Surface_KeyDown(object sender, KeyEventArgs e)
        {
            //Window.GetWindow(this).Title = string.Format(
            //    "uxLayoutCanvas has focus: {0}", uxLayoutCanvas.IsFocused);

            if (!uxProperties.IsKeyboardFocusWithin)
            {
                if (e.Key == Key.Delete)
                {
                    Designer.DeleteSelectedItems();
                    e.Handled = true;
                }
                else if (e.Key == Key.Z && IsCtrl(e))
                {
                    Designer.Undo();
                    e.Handled = true;
                }
                else if (e.Key == Key.Y && IsCtrl(e))
                {
                    Designer.Redo();
                    e.Handled = true;
                }
                else if (e.Key == Key.Up && e.KeyboardDevice.Modifiers == ModifierKeys.Shift)
                {
                    Designer.OffsetSelectedItems(0, -1);
                    e.Handled = true;
                }
                else if (e.Key == Key.Down && e.KeyboardDevice.Modifiers == ModifierKeys.Shift)
                {
                    Designer.OffsetSelectedItems(0, 1);
                    e.Handled = true;
                }
                else if (e.Key == Key.Left && e.KeyboardDevice.Modifiers == ModifierKeys.Shift)
                {
                    Designer.OffsetSelectedItems(-1, 0);
                    e.Handled = true;
                }
                else if (e.Key == Key.Right && e.KeyboardDevice.Modifiers == ModifierKeys.Shift)
                {
                    Designer.OffsetSelectedItems(1, 0);
                    e.Handled = true;
                }
            }

        }


        protected bool IsCtrl(KeyEventArgs e)
        {
            return e.KeyboardDevice.Modifiers == ModifierKeys.Control;
        }



        public ILayoutSubjectsPreviewService PreviewService
        {
            get
            {
                return (ILayoutSubjectsPreviewService)uxPreviewPanel.DataContext;
            }
            set
            {
                uxPreviewPanel.DataContext = value;
                uxPreviewPrevRecord.Click += new RoutedEventHandler(
                    (object sender, RoutedEventArgs e) => 
                    { value.MovePrevious(); });
                uxPreviewNextRecord.Click += new RoutedEventHandler(
                    (object sender, RoutedEventArgs e) => 
                    { value.MoveNext(); });
                uxPreviewFirstRecord.Click += new RoutedEventHandler(
                    (object sender, RoutedEventArgs e) => 
                    { value.MoveFirst(); });
                uxPreviewLastRecord.Click += new RoutedEventHandler(
                    (object sender, RoutedEventArgs e) => 
                    { value.MoveLast(); });
            }
        }
        

        public Flow.Designer.Controller.DesignerController Designer { get; set; }
        public Dictionary<FrameworkElement, ILayoutItem> BoundLayoutItems { get; set; }

        public Dictionary<ILayoutItem, FrameworkElement []> SurfaceLayoutItems { get; set; }

        public double CanvasWidth { get { return uxLayoutCanvas.ActualWidth; } }
        public double CanvasHeight { get { return uxLayoutCanvas.ActualHeight; } }
        public Canvas RenderVisual { get { return uxLayoutCanvas; } }

        protected ObservableDesignerModel OModel { get; set; }
        public DesignerModel Model
        {
            get { return OModel.Model; }
            set 
            {
                if (OModel == null)
                {
                    OModel = new ObservableDesignerModel(value);
                    OModel.LayoutChanged += new EventHandler<EventArgs<Layout>>(OModel_LayoutChanged);
                    OModel.SelectedLayoutItemsChanged += new EventHandler<EventArgs<ILayoutItem[]>>(OModel_SelectedLayoutItemsChanged);
                    UpdateSurfaceProperties();
                }
                else
                    OModel.Model = value; 
            }
        }

        void OModel_SelectedLayoutItemsChanged(object sender, EventArgs<ILayoutItem[]> e)
        {
            if (SelectionAdorner == null) return;

            SelectionAdorner.RefreshSelectedElements(
                SelectableLayout.SelectedItems.ToArray(), 
                SurfaceLayoutItems);
        }

        public bool SuspendLayoutBinding = false;
        void OModel_LayoutChanged(object sender, EventArgs<Layout> e)
        {
            if (! SuspendLayoutBinding)
                RebuildLayoutElements();
        }

        public void RebuildLayoutElements()
        {
            SelectableLayout.SuspendPropertyNotifying("*");
            SelectableLayout.SelectedItem = null;
            SelectableLayout.Layout = SelectableLayout.Layout.Copy() as Layout;
            SelectableLayout.ResumePropertyNotifying("*", false);
            OModel.Layout = SelectableLayout.Layout;

            UpdateSurfaceProperties();
            CreateLayoutSurfaceElements();

            OModel.Model.OnLayoutRebindNeeded();
            SelectableLayout.SelectedItem = null;
        }

        protected void UpdateSurfaceProperties()
        {
            Size layoutSize = new Size(Layout.LayoutWidth * 96, Layout.LayoutHeight * 96);
            if (uxLayoutSurface.Width != layoutSize.Width)
                uxLayoutSurface.Width = layoutSize.Width;
            if (uxLayoutSurface.Height != layoutSize.Height)
                uxLayoutSurface.Height = layoutSize.Height;
            uxLayoutCanvas.Background = new SolidColorBrush(Layout.BackgroundColor);

            VerifySubmitOrderSettings();
            
        }

        public SelectableLayout SelectableLayout
        {
            get { return Model.SelectableLayout; }
            set { Model.SelectableLayout = value; }
        }

        public Layout Layout
        {
            get { return SelectableLayout.Layout; }
            set { SelectableLayout.Layout = value; }
        }

        void CreateLayoutSurfaceElements()
        {
            // remove all of the old ones
            ClearSurfaceElements();

            // create the new ones
            foreach (ILayoutItem li in Layout.LayoutItems)
                AddLayoutItem(li);
        }

        /// <summary>
        /// Called when the layout changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void _layout_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "LayoutItems")
            {
                // Make sure all elements are accounted for in the layout
                foreach (ILayoutItem li in Layout.LayoutItems)
                {
                    if (!SurfaceLayoutItems.ContainsKey(li))
                        AddLayoutItem(li);
                }
            }
            VerifySubmitOrderSettings();
           
        }

        public GridAdorner GridAdorner { get; set; }

        SnapToService CanvasEdgeMoveSnapToSurface { get; set; }

        void uxLayoutCanvas_Loaded(object sender, RoutedEventArgs e)
        {

        }

        void Surface_Loaded(object sender, RoutedEventArgs e)
        {
            SurfaceLoad();
        }

        void SurfaceLoad()
        {
            Window.GetWindow(this).Closing += new CancelEventHandler(Surface_Closing);

            // add the grid
            if (AdornerLayer.GetAdornerLayer(uxLayoutCanvas) == null)
                return;

            GridAdorner = new GridAdorner(uxLayoutCanvas) { ZoomValue = uxZoom.Value }; 
            AdornerLayer.GetAdornerLayer(uxLayoutCanvas).Add(GridAdorner);
            uxViewGridPanel.DataContext = new ViewGridPanelModel() { GridAdorner = GridAdorner, GraphicsUnits = Model.GraphicsUnits };

            // create the snap to service
            CanvasEdgeMoveSnapToSurface = new SnapToService(10,
                new CanvasSnapLocationProvider(uxLayoutCanvas),
                new GridAdornerSnapLocationProvider(GridAdorner));
            uxSnapPanel.DataContext = CanvasEdgeMoveSnapToSurface;

            InitSelector();
            VerifySubmitOrderSettings();

            OnSurfaceLoaded();
        }

        SurfaceSelectionAdorner SelectionAdorner;
        protected void InitSelector()
        {
            SurfaceSelectionBand selector = new SurfaceSelectionBand(this);
            selector.AreaSelected += new EventHandler<EventArgs<Rect>>(selector_AreaSelected);
            SelectionAdorner = new SurfaceSelectionAdorner(this, 
                CanvasEdgeMoveSnapToSurface, new NullSnapToService());
        }

        void selector_AreaSelected(object sender, EventArgs<Rect> e)
        {
            List<ILayoutItem> selectedItems = new List<ILayoutItem>();
            foreach (ILayoutItem item in SurfaceLayoutItems.Keys.ToArray())
            {
                foreach (FrameworkElement fe in SurfaceLayoutItems[item])
                {
                    if (RectIntersectsElement(e.Data, fe))
                    {
                        selectedItems.Add(item);
                        break;
                    }
                }
            }
            SelectableLayout.SelectItems(selectedItems.ToArray());
        }

        public bool RectIntersectsElement(Rect r, FrameworkElement f)
        {
            Rect fr = new Rect(Canvas.GetLeft(f), Canvas.GetTop(f), f.Width, f.Height);
            return r.IntersectsWith(fr);
        }


        void Surface_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            throw new ApplicationException("Don't set the data context!!!");
        }

        public FrameworkElement GetFrameworkElement(ILayoutItem item)
        {
            foreach (FrameworkElement fe in BoundLayoutItems.Keys)
                if (BoundLayoutItems[fe] == item)
                    return fe;
            return null;
        }

        private void uxNew_Click(object sender, RoutedEventArgs e)
        {
            Designer.NewLayout();
            VerifySubmitOrderSettings();
        }

        private void uxPrint_Click(object sender, RoutedEventArgs e)
        {
            Designer.Print();
            VerifySubmitOrderSettings();
            //this.uxPreviewEnabled.IsChecked = true;
        }

        private void uxBatchPrint_Click(object sender, RoutedEventArgs e)
        {
            if (Designer.renderingImages == false)
            {
                Designer.BatchPrint(null, false, false, "");
                VerifySubmitOrderSettings();
            }
            else
            {
                FlowMessageDialog msg = new FlowMessageDialog("Batch Layout Render", "Layouts are currently being rendered, please wait for current task to finish.");
                msg.CancelButtonVisible = false;
                msg.ShowDialog();

            }
        }

       


        private void uxNewImg_Click(object sender, RoutedEventArgs e)
        {
            Designer.NewImage();
        }

        private void uxNewTxt_Click(object sender, RoutedEventArgs e)
        {
            Designer.NewText();
        }

        private void uxSaveAs_Click(object sender, RoutedEventArgs e)
        {
            SaveLayoutAs();
        }

        private bool SaveLayoutAs()
        {
            SaveFileDialog dlg = new SaveFileDialog();

            if (String.IsNullOrEmpty(TempLayoutDir))
                TempLayoutDir = LayoutPreferences.LayoutsDirectory;

            dlg.InitialDirectory = FlowContext.InitDir(TempLayoutDir);
            dlg.Filter = "Layout Files (*.lyt) | *.lyt";
            if (dlg.ShowDialog() != true)
                return false;

            SaveLayout(dlg.FileName);
            LoadedLayoutFile = new FileInfo(dlg.FileName);
            return true;
        }

        private void uxSaveExisting_Click(object sender, RoutedEventArgs e)
        {
            if (LoadedLayoutFile == null)
                SaveLayoutAs();
            else
            {
                ObjectSerializer.SerializeFile(LoadedLayoutFile.FullName, Layout);
                VerifySubmitOrderSettings();
            }
        }

        protected void SaveLayout(string filename)
        {
            ObjectSerializer.SerializeFile(filename, Layout);
            VerifySubmitOrderSettings();
        }

        protected FileInfo _loadedLayoutFile = null;
        public FileInfo LoadedLayoutFile
        {
            get { return _loadedLayoutFile; }
            set
            {
                _loadedLayoutFile = value;
                if (value != null)
                {
                    uxSaveExisting.Header = "Save..." + value.Name;
                    LayoutDockContent.Title = "Layout ::   " + value.Name;
                }
                else
                {
                    uxSaveExisting.Header = "Save";
                    LayoutDockContent.Title = "Layout";
                }
            }

        }

        private void uxLoadFromLocationBtn_Click(object sender, RoutedEventArgs e)
        {
            var dlg = new OpenFileDialog();
            dlg.InitialDirectory = LayoutPreferences.LayoutsDirectory;
            dlg.Filter = "Layout Files (*.lyt) | *.lyt";
            if (dlg.ShowDialog() != true)
                return;

            LoadLayoutFromFile(new FileInfo(dlg.FileName));
        }
 
        void layoutItem_Click(object sender, RoutedEventArgs e)
        {
            MenuItem mi = (MenuItem)sender;
            FileInfo selectedFile = (FileInfo)mi.DataContext;
            if(selectedFile.Name.StartsWith(".."))
            {
                TempLayoutDir = Directory.GetParent(TempLayoutDir).FullName;
                LoadLayoutsMenu();
                uxLoadbtn.ContextMenu.IsOpen = true;
            }
            else if (selectedFile.Name.StartsWith("-----"))
            {
                LoadLayoutsMenu();
                uxLoadbtn.ContextMenu.IsOpen = true;
            }
            else if (selectedFile.Name.StartsWith("["))
            {
                TempLayoutDir = System.IO.Path.Combine(selectedFile.DirectoryName, selectedFile.Name.Substring(1, selectedFile.Name.Length-2));
                LoadLayoutsMenu();
                uxLoadbtn.ContextMenu.IsOpen = true;
            }
            else
                LoadLayoutFromFile((FileInfo)mi.DataContext);

        }

        public bool VerifyExistingLayoutSaved()
        {
            // save a new layout
            if (LoadedLayoutFile == null)
            {
                if (Layout.LayoutItems.Count == 0)
                    return true;

                var saveNow = MessageBox.Show("Current layout has not been saved, save it now?", "Save Layout", MessageBoxButton.YesNoCancel);
                if (saveNow == MessageBoxResult.Yes)
                    return SaveLayoutAs();
                return saveNow == MessageBoxResult.No;
            }

            // save an existing layout
            string origLayout = File.Exists(LoadedLayoutFile.FullName) ?
                ObjectSerializer.SerializeString(ObjectSerializer.FromXmlFile<Layout>(LoadedLayoutFile.FullName)) :
                null;
            string curLayout = ObjectSerializer.SerializeString(Layout);
            if (origLayout == curLayout)
                return true;

            var saveExisting = MessageBox.Show("Layout " + LoadedLayoutFile.Name + " has been modified, save it now?", "Save layout?", MessageBoxButton.YesNoCancel);
            if (saveExisting == MessageBoxResult.Yes)
            {
                ObjectSerializer.SerializeFile(LoadedLayoutFile.FullName, Layout);
                return true;
            }

            return saveExisting == MessageBoxResult.No;
        }

        public void LoadLayoutFromFile(FileInfo fi)
        {
            if (!VerifyExistingLayoutSaved()) return;

            //TODO: prompt for save existing layout if not saved
            LoadedLayoutFile = fi;
            Layout = ObjectSerializer.FromXmlFile<Layout>(LoadedLayoutFile.FullName);
            SelectableLayout.SelectedItem = null;
            VerifySubmitOrderSettings();
        }

        void Surface_Closing(object sender, CancelEventArgs e)
        {
            if (!VerifyExistingLayoutSaved())
            {
                e.Cancel = true;
            }
        }

        private void uxLoad_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            LoadLayoutsMenu();
        }

        public void AddLayoutItem(ILayoutItem li)
        {
            li.AddToCanvas(uxLayoutCanvas, LayoutRenderQuality.Medium,
                (FrameworkElement fe) =>
                {
                    new LayoutLocationBinding().SetBinding(fe, li);
                    BoundLayoutItems.Add(fe, li);
                }, 
                (FrameworkElement[] fes) =>
                { SurfaceLayoutItems.Add(li, fes); });
        }

        public void ClearSurfaceElements()
        {
            foreach (FrameworkElement fe in BoundLayoutItems.Keys)
            {
                uxLayoutCanvas.Children.Remove(fe);
            }
            SurfaceLayoutItems = new Dictionary<ILayoutItem, FrameworkElement[]>();
            BoundLayoutItems = new Dictionary<FrameworkElement, ILayoutItem>();
        }

        public void RemoveLayoutElement(FrameworkElement i)
        {
            uxLayoutCanvas.Children.Remove(i);
        }

        protected void LoadLayoutsMenu()
        {
            if (String.IsNullOrEmpty(TempLayoutDir))
                TempLayoutDir = LayoutPreferences.LayoutsDirectory;

            if (TempLayoutDir.Length < LayoutPreferences.LayoutsDirectory.Length)
                TempLayoutDir = LayoutPreferences.LayoutsDirectory;

            new LoadLayoutsListService()
                .ItemsToRemoveAre(uxLoadBtnMenu.Items)
                .WhenNoFilesFound(() =>
                {
                    MenuItem noItems = new MenuItem();
                    noItems.Header = "No Layouts Found";
                    uxLoadBtnMenu.Items.Add(noItems);
                })
                .ForEachFileFound((FileInfo layoutFile) =>
                {
                    MenuItem layoutItem = new MenuItem();
                    layoutItem.DataContext = layoutFile;
                    layoutItem.Header = layoutFile.Name;
                    layoutItem.Background = new SolidColorBrush(Colors.DarkGray);
                    layoutItem.Click += new RoutedEventHandler(layoutItem_Click);
                    uxLoadBtnMenu.Items.Add(layoutItem);
                })
                .LoadLayouts(TempLayoutDir);
        }

        void uxSave_Click(object sender, RoutedEventArgs e)
        {
            uxSaveBtnMenu.IsOpen = true;
        }

        void uxLoadbtn_Click(object sender, RoutedEventArgs e)
        {
            LoadLayoutsMenu();
            uxLoadBtnMenu.IsOpen = true;
        }

        private void uxZoomIn_Click(object sender, RoutedEventArgs e)
        {
            uxZoom.Value = uxZoom.Value + uxZoom.Value * .5;
        }

        private void uxZoomOut_Click(object sender, RoutedEventArgs e)
        {
            uxZoom.Value = uxZoom.Value - uxZoom.Value * .5;
        }

        private void uxSentToFront_Click(object sender, RoutedEventArgs e)
        {
            // The model and surface are backwards, the last item on the list is on top
            SelectableLayout.MoveSelectedToBack();
        }

        private void uxSendToBack_Click(object sender, RoutedEventArgs e)
        {
            // The model and surface are backwards, the last item on the list is on top
            SelectableLayout.MoveSelectedToFront();
        }

        private void uxAlignLefts_Click(object sender, RoutedEventArgs e)
        {
            LayoutItemAligner.AlignLefts(SelectableLayout.SelectedItems.ToArray());
        }

        private void uxAlignRights_Click(object sender, RoutedEventArgs e)
        {
            LayoutItemAligner.AlignRights(SelectableLayout.SelectedItems.ToArray());
        }

        private void uxAlignTops_Click(object sender, RoutedEventArgs e)
        {
            LayoutItemAligner.AlignTops(SelectableLayout.SelectedItems.ToArray());
        }

        private void uxAlignBottoms_Click(object sender, RoutedEventArgs e)
        {
            LayoutItemAligner.AlignBottoms(SelectableLayout.SelectedItems.ToArray());
        }

        private void uxAlignHMiddles_Click(object sender, RoutedEventArgs e)
        {
            LayoutItemAligner.AlignHMiddles(SelectableLayout.SelectedItems.ToArray());
        }

        private void uxAlignVMiddles_Click(object sender, RoutedEventArgs e)
        {
            LayoutItemAligner.AlignVMiddles(SelectableLayout.SelectedItems.ToArray());
        }

        private void uxZoom1_1_Click(object sender, RoutedEventArgs e)
        {
            uxZoom.Value = 1;
        }

        private void uxPrintSetup_Click(object sender, RoutedEventArgs e)
        {
            Designer.PrintSetup();
        }

        private void uxZoom_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (GridAdorner != null)
                GridAdorner.ZoomValue = uxZoom.Value;
        }





        //private ExportMethod SelectedExportMethod { get; set; }

        public string SaveOrderExportPath
        {
            get { return (string)this.GetValue(SaveOrderExportPathProperty); }
            set { this.SetValue(SaveOrderExportPathProperty, value); }
        }

        protected static DependencyProperty SaveOrderExportPathProperty =
            DependencyProperty.Register("SaveOrderExportPath", typeof(string), typeof(Surface));

       

        protected static DependencyProperty ExportButtonEnabledProperty =
           DependencyProperty.Register("ExportButtonEnabled", typeof(bool), typeof(Surface));

        public bool ExportButtonEnabled
        {
            get { return (bool)this.GetValue(ExportButtonEnabledProperty); }
            set { this.SetValue(ExportButtonEnabledProperty, value); }
        }


        private void SelectedExportMethod_Changed(object sender, RoutedEventArgs e)
        {
            if (!this.IsInitialized)
                return;

            RadioButton source = sender as RadioButton;

            if (source != null)
            {
                //this.SetExportMethod();
            }
        }

        private void btnSelectSaveOrderExportPath_Click(object sender, RoutedEventArgs e)
        {
            this.SaveOrderExportPath = DialogUtility.SelectFolder(Environment.SpecialFolder.Desktop, "Select destination folder.");
            VerifySubmitOrderSettings();
            //this.ExportButtonEnabled = !String.IsNullOrEmpty(this.SaveOrderExportPath);
        }

        private void ConfirmOrderButton_Click(object sender, RoutedEventArgs e)
        {
            if (Designer.renderingImages == false)
            {
                //temp folder will be used for preparing order
                string tmpFolder = System.IO.Path.Combine(FlowContext.FlowTempDirPath, "tmpLayoutOrder");
                if (Directory.Exists(tmpFolder))
                    Directory.Delete(tmpFolder,true);
                Directory.CreateDirectory(tmpFolder);

                //user may choose to save local instead of upload to server
                string saveOrderExportPath = "";
                if (rdoSaveOrder.IsChecked == true)
                {
                    saveOrderExportPath = txtSaveExportDestination.Text;
                    if (!Directory.Exists(saveOrderExportPath))
                        return;
                }

                if (Directory.Exists(tmpFolder))
                {
                    Designer.BatchPrint(tmpFolder, true, true, saveOrderExportPath);
                    this.SubmitOrderConfirmation.Visibility = Visibility.Collapsed;
                }
            }
            else
            {
                FlowMessageDialog msg = new FlowMessageDialog("Batch Layout Render", "Layouts are currently being rendered, please wait for current task to finish.");
                msg.CancelButtonVisible = false;
                msg.ShowDialog();
            }
        }

        private void uxSubmitOrder_Click(object sender, RoutedEventArgs e)
        {
            VerifySubmitOrderSettings();
            if(uxSubmitOrder.IsEnabled == true)
                this.SubmitOrderConfirmation.Visibility = Visibility.Visible;

            long workingSet = System.Diagnostics.Process.GetCurrentProcess().WorkingSet64;
            //long VirtualMemorySize = System.Diagnostics.Process.GetCurrentProcess().VirtualMemorySize64;
            //long PrivateMemorySize = System.Diagnostics.Process.GetCurrentProcess().PrivateMemorySize64;

            //if (workingSet > 367001600)//350MB
            if (workingSet > 524288000)//500MB
            {
                FlowMessageDialog msg = new FlowMessageDialog("WARNING - Batch Layout Order Submit", "WARNING!\n\nTo ensure all layouts are rendered and submitted successfully,\nYou should only submit a batch order if you have recently restarted Flow.");
                msg.CancelButtonVisible = false;
                msg.ShowDialog();
            }
        }

        private void CancelOrderButton_Click(object sender, RoutedEventArgs e)
        {
            this.SubmitOrderConfirmation.Visibility = Visibility.Collapsed;
        }

        private void rdoSubmitOrder_Click(object sender, RoutedEventArgs e)
        {
            VerifySubmitOrderSettings();
        }

    

        private void VerifySubmitOrderSettings()
        {

            //make sure there is flowProject loaded
            if (this.Designer == null || this.Designer.FlowProject == null)
            { 
                this.ConfirmOrderButton.IsEnabled = false; 
                uxSubmitOrder.IsEnabled = false; 
                return; 
            }

            //make sure there is a product associated with the current layout
            if (this.Layout.CatalogProductKey < 1)
            { this.ConfirmOrderButton.IsEnabled = false; uxSubmitOrder.IsEnabled = false;  return; }

            uxSubmitOrder.IsEnabled = true;

            if (rdoSaveOrder.IsChecked == true && (txtSaveExportDestination.Text == null || txtSaveExportDestination.Text.Length < 1))
            { this.ConfirmOrderButton.IsEnabled = false; return; }

            this.ConfirmOrderButton.IsEnabled = true;

        }

        private void rdoSaveOrder_Click(object sender, RoutedEventArgs e)
        {
            VerifySubmitOrderSettings();
        }

        private void uxUpDirectory_Click(object sender, RoutedEventArgs e)
        {
            TempLayoutDir = Directory.GetParent(TempLayoutDir).FullName;
            LoadLayoutsMenu();
            uxLoadbtn.ContextMenu.IsOpen = true;
        }

    }


}
