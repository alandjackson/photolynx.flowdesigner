﻿using Flow.Designer.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Flow.Designer.View
{
    /// <summary>
    /// Interaction logic for AppWindow.xaml
    /// </summary>
    public partial class AppWindow : Window
    {
        DesignerController DesignerController { get; set; }


        public AppWindow()
        {
            InitializeComponent();
            //Loaded += AppWindow_Loaded;
        }

        //private void AppWindow_Loaded(object sender, RoutedEventArgs e)
        //{
        //    DesignerController = new DesignerController() { CanCreateEditLayouts = true }
        //        .Main(null, null, this)
        //        //.Activate()
        //        ;

        //}
    }
}
