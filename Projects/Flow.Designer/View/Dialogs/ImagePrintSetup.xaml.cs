using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Forms;
using Flow.Designer.Model.Settings;
using Flow.Schema.LinqModel.DataContext;
using Flow.Designer.Controller;

namespace Flow.Designer.View.Dialogs
{
    /// <summary>
    /// Interaction logic for ImagePrintSetup.xaml
    /// </summary>
    public partial class ImagePrintSetup : System.Windows.Controls.UserControl
    {
        //public FlowDesignerDataContext FlowDesignerDataContext { get; set; }
        //public FlowMasterDataContext FlowMasterDataContext { get; set; }
        public ImagePrintSetup()
        {
            //FlowDesignerDataContext = StructureMap.ObjectFactory.GetInstance<FlowDesignerDataContext>();

            //DataContext = new ImagePrintSetupViewModel();
            InitializeComponent();
            DataContextChanged += new DependencyPropertyChangedEventHandler(ImagePrintSetup_DataContextChanged);
            uxImageFormats.Items.Clear();
            uxImageFormats.Items.Add("JPG");
            uxImageFormats.Items.Add("PNG");
        }

        public PrintSettings ViewModel { get { return DataContext as PrintSettings; } }

        void ImagePrintSetup_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            PrintSettings ps = e.NewValue as PrintSettings;
            if (ps == null) return;

            uxPrintToImage.IsChecked = (ps.PrintDestination == PrintSettings.PrintDestiations.Image);
            uxPrintToPrinter.IsChecked = (ps.PrintDestination == PrintSettings.PrintDestiations.WindowsPrinter);
        }

        private void uxBrowsePrintDir_Click(object sender, RoutedEventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                uxPrintDir.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void uxPrintToImage_Checked(object sender, RoutedEventArgs e)
        {
            //if (uxPrintToImage.IsChecked == true)
            //    FlowDesignerDataContext.LayoutPreferences.Designer.PrintDestination = 1;
        }

        private void uxPrintToPrinter_Checked(object sender, RoutedEventArgs e)
        {
            //if (uxPrintToPrinter.IsChecked == true)
            //    FlowDesignerDataContext.LayoutPreferences.Designer.PrintDestination = 0;

        }

        private void uxPrinterSetup_Clicked(object sender, RoutedEventArgs e)
        {
            PrintSettingsRepository.PrintDialog.ShowDialog();
        }

        private void uxOk_Click(object sender, RoutedEventArgs e)
        {
            PrintSettings ps = this.DataContext as PrintSettings;
            if (ps == null) return;

            if (uxPrintToImage.IsChecked == true) ps.PrintDestination = PrintSettings.PrintDestiations.Image;
            if (uxPrintToPrinter.IsChecked == true) ps.PrintDestination = PrintSettings.PrintDestiations.WindowsPrinter;

            Window.GetWindow(this).DialogResult = true;
        }

        private void UseSubjectNameCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            // only one of these can be checked at a time
            if (UseSubjectNameCheckBox.IsChecked == true && UseRealSubjectNameCheckBox.IsChecked == true)
            {
                UseRealSubjectNameCheckBox.IsChecked = false;
            }
        }

        private void UseRealSubjectNameCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            // only one of these can be checked at a time
            if (UseSubjectNameCheckBox.IsChecked == true && UseRealSubjectNameCheckBox.IsChecked == true)
            {
                UseSubjectNameCheckBox.IsChecked = false;
            }
        }
    }
}
