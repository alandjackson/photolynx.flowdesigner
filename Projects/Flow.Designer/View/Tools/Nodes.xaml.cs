﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Flow.Lib.Helpers;
using Flow.Designer.Model;
using Flow.Designer.Controller;

namespace Flow.Designer.View
{
    /// <summary>
    /// Interaction logic for Nodes.xaml
    /// </summary>
    public partial class Nodes : UserControl
    {
        public event SelectionChangedEventHandler NodeSelectionChanged = null;


        public Nodes()
        {
            InitializeComponent();
        }

        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ListBox lb = sender as ListBox;
            if (lb.SelectedItems.Count == 1 && lb.SelectedItem is LayoutGroup)
            {
                LayoutGroup lg = lb.SelectedItem as LayoutGroup;
                foreach (ILayoutItem c in lg.Children)
                    lb.SelectedItems.Add(c);
            }
            else
            {
                if (NodeSelectionChanged != null)
                    NodeSelectionChanged(this, e);
            }

        }

        public int SelectedNodes
        {
            get { return uxNodesList.SelectedItems.Count; }
        }

        private void uxRenameNode_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Rename Sender data context: " + (sender as MenuItem).DataContext);

        }

        protected List<ILayoutItem> SelectedItems
        {
            get 
            {
                List<ILayoutItem> items = new List<ILayoutItem>();
                foreach (object o in uxNodesList.SelectedItems)
                    if (o is ILayoutItem) items.Add(o as ILayoutItem);
                return items;
            }
        }

        private void uxGroupNodes_Click(object sender, RoutedEventArgs e)
        {
        }



    }
}
