﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.Designer.Model;

namespace Flow.Designer.Lib.Service
{
    public class SurfaceGrid : AbstractNotifyPropertyChanged
    {
        public bool IsGridEnabled { get; set; }
        public int GridSize { get; set; }
    }
}
