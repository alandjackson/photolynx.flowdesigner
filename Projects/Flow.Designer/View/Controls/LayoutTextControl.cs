﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Markup;
using System.Windows.Media;
using System.Globalization;
using Flow.Designer.Model;
using System.Windows.Controls;
using System.ComponentModel;

namespace Flow.Designer.View.Controls
{
    public class LayoutTextControl : FrameworkElement, IAddChild
    {
        public DockPanel DropShadowDockPanel { get; set; }

        #region Private Fields

        private FormattedText _formattedText;
        private Geometry _textGeometry;
        private Geometry _textHighlightGeometry;

        #endregion

        public bool BoxOutline { get; set; }    

        #region Private Methods

        public LayoutTextControl()
        {
            DataContextChanged += LayoutTextControl_DataContextChanged;
            BoxOutline = false;
        }

        void LayoutTextControl_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (!(DataContext is LayoutText))
                return;

            var lt = ((LayoutText)DataContext);
            lt.PropertyChanged += lt_PropertyChanged;

            lt_PropertyChanged(this, new PropertyChangedEventArgs("DropShadowEnabled"));
        }

        void lt_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var lt = ((LayoutText)DataContext);

            if (e.PropertyName == "DropShadowEnabled")
                DropShadowDockPanel.Effect = lt.DropShadowEnabled ? lt.DropShadow : null;
        }

        /// <summary>
        /// Invoked when a dependency property has changed. Generate a new FormattedText object to display.
        /// </summary>
        /// <param name="d">OutlineText object whose property was updated.</param>
        /// <param name="e">Event arguments for the dependency property.</param>
        private static void OnOutlineTextInvalidated(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((LayoutTextControl)d).CreateText();
        }

        #endregion


        #region FrameworkElement Overrides

        /// <summary>
        /// OnRender override draws the geometry of the text and optional highlight.
        /// </summary>
        /// <param name="drawingContext">Drawing context of the OutlineText control.</param>
        protected override void OnRender(DrawingContext drawingContext)
        {

            CreateText();

            // Scale barcode fonts to fill the entire vertical space
            if (Font.Source.ToUpper().StartsWith("BC "))
            {
                drawingContext.PushTransform(new ScaleTransform(1.0, ActualHeight / _formattedText.Height));
            }

            // Draw the outline based on the properties that are set.
            drawingContext.DrawGeometry(null, new Pen(Stroke, StrokeThickness), _textGeometry);
            drawingContext.DrawGeometry(Fill, null, _textGeometry);

            if (_textHighlightGeometry != null && BoxOutline)
            {
                drawingContext.DrawGeometry(null, new Pen(Stroke, StrokeThickness), _textHighlightGeometry);
            }

            if (Font.Source.ToUpper().StartsWith("BC "))
            {
                drawingContext.Pop();
            }
        }


        /// <summary>
        /// Create the outline geometry based on the formatted text.
        /// </summary>
        public void CreateText()
        {
            FontStyle fontStyle = FontStyles.Normal;
            FontWeight fontWeight = FontWeights.Medium;

            if (Bold == true) fontWeight = FontWeights.Bold;
            if (Italic == true) fontStyle = FontStyles.Italic;

            // Font Size must always be greater than zero
            if (FontSize <= 0) FontSize = 16;

            _formattedText = CreateFormattedText(fontStyle, fontWeight, FontSize);
            if (AutoSizeFont && ActualWidth > 0 && _formattedText.Width > 0)
            {
                // calculate how much to scale the ratio to fit in the width
                double fontSizeRatioWidth = ActualWidth / _formattedText.Width * .95;
                double fontSizeRatioHeight = ActualHeight / _formattedText.Height * .95;
                double newFontSize = FontSize * (fontSizeRatioWidth < fontSizeRatioHeight
                    ? fontSizeRatioWidth : fontSizeRatioHeight);
                _formattedText = CreateFormattedText(fontStyle, fontWeight, newFontSize);
            }

            // Build the geometry object that represents the text.
            _textGeometry = _formattedText.BuildGeometry(new Point(0, 0));

            if (BoxOutline)
                _textHighlightGeometry = _formattedText.BuildHighlightGeometry(new Point(0, 0));

            this.MinHeight = _formattedText.Height;
        }

        private FormattedText CreateFormattedText(FontStyle fontStyle, FontWeight fontWeight, double textFontSize)
        {
            // Create the formatted text based on the properties set.
            FormattedText formattedText = new FormattedText(
                Text,
                CultureInfo.GetCultureInfo("en-us"),
                System.Windows.FlowDirection.LeftToRight,
                new Typeface(Font, fontStyle, fontWeight, FontStretches.Normal),
                textFontSize,
                Brushes.Black // This brush does not matter since we use the geometry of the text. 
                );
            formattedText.TextAlignment = TextAlignment;
            if (ActualWidth > 0)
                formattedText.MaxTextWidth = ActualWidth;
            return formattedText;
        }

        #endregion

        #region DependencyProperties

        /// <summary>
        /// Specifies whether the font should display Bold font weight.
        /// </summary>
        public bool Bold
        {
            get
            {
                return (bool)GetValue(BoldProperty);
            }

            set
            {
                SetValue(BoldProperty, value);
            }
        }

        /// <summary>
        /// Identifies the Bold dependency property.
        /// </summary>
        public static readonly DependencyProperty BoldProperty = DependencyProperty.Register(
            "Bold",
            typeof(bool),
            typeof(LayoutTextControl),
            new FrameworkPropertyMetadata(
                false,
                FrameworkPropertyMetadataOptions.AffectsRender,
                new PropertyChangedCallback(OnOutlineTextInvalidated),
                null
                )
            );

        public TextAlignment TextAlignment
        {
            get { return (TextAlignment) GetValue(TextAlignmentProperty); }
            set { SetValue(TextAlignmentProperty, value); }
        }

        /// <summary>
        /// Identifies the TextAlignment dependency property.
        /// </summary>
        public static readonly DependencyProperty TextAlignmentProperty = DependencyProperty.Register(
            "TextAlignment",
            typeof(TextAlignment),
            typeof(LayoutTextControl),
            new FrameworkPropertyMetadata(
                TextAlignment.Left,
                FrameworkPropertyMetadataOptions.AffectsRender,
                new PropertyChangedCallback(OnOutlineTextInvalidated),
                null
                )
            );
        

        /// <summary>
        /// Specifies the brush to use for the fill of the formatted text.
        /// </summary>
        public Brush Fill
        {
            get
            {
                return (Brush)GetValue(FillProperty);
            }

            set
            {
                SetValue(FillProperty, value);
            }
        }

        /// <summary>
        /// Identifies the Fill dependency property.
        /// </summary>
        public static readonly DependencyProperty FillProperty = DependencyProperty.Register(
            "Fill",
            typeof(Brush),
            typeof(LayoutTextControl),
            new FrameworkPropertyMetadata(
                new SolidColorBrush(Colors.LightSteelBlue),
                FrameworkPropertyMetadataOptions.AffectsRender,
                new PropertyChangedCallback(OnOutlineTextInvalidated),
                null
                )
            );

        /// <summary>
        /// The font to use for the displayed formatted text.
        /// </summary>
        public FontFamily Font
        {
            get
            {
                return (FontFamily)GetValue(FontProperty);
            }

            set
            {
                SetValue(FontProperty, value);
            }
        }

        /// <summary>
        /// Identifies the Font dependency property.
        /// </summary>
        public static readonly DependencyProperty FontProperty = DependencyProperty.Register(
            "Font",
            typeof(FontFamily),
            typeof(LayoutTextControl),
            new FrameworkPropertyMetadata(
                new FontFamily("Arial"),
                FrameworkPropertyMetadataOptions.AffectsRender,
                new PropertyChangedCallback(OnOutlineTextInvalidated),
                null
                )
            );

        /// <summary>
        /// The current font size.
        /// </summary>
        public double FontSize
        {
            get
            {
                return (double)GetValue(FontSizeProperty);
            }

            set
            {
                SetValue(FontSizeProperty, value);
            }
        }

        /// <summary>
        /// Identifies the FontSize dependency property.
        /// </summary>
        public static readonly DependencyProperty FontSizeProperty = DependencyProperty.Register(
            "FontSize",
            typeof(double),
            typeof(LayoutTextControl),
            new FrameworkPropertyMetadata(
                 (double)48.0,
                 FrameworkPropertyMetadataOptions.AffectsRender,
                 new PropertyChangedCallback(OnOutlineTextInvalidated),
                 null
                 )
            );


        /// <summary>
        /// Specifies whether the font should display Italic font style.
        /// </summary>
        public bool Italic
        {
            get
            {
                return (bool)GetValue(ItalicProperty);
            }

            set
            {
                SetValue(ItalicProperty, value);
            }
        }

        /// <summary>
        /// Identifies the Italic dependency property.
        /// </summary>
        public static readonly DependencyProperty ItalicProperty = DependencyProperty.Register(
            "Italic",
            typeof(bool),
            typeof(LayoutTextControl),
            new FrameworkPropertyMetadata(
                 false,
                 FrameworkPropertyMetadataOptions.AffectsRender,
                 new PropertyChangedCallback(OnOutlineTextInvalidated),
                 null
                 )
            );

        /// <summary>
        /// Specifies whether the font should display Italic font style.
        /// </summary>
        public bool AutoSizeFont
        {
            get
            {
                return (bool)GetValue(AutoSizeFontProperty);
            }

            set
            {
                SetValue(AutoSizeFontProperty, value);
            }
        }

        /// <summary>
        /// Identifies the AutoSizeFont dependency property.
        /// </summary>
        public static readonly DependencyProperty AutoSizeFontProperty = DependencyProperty.Register(
            "AutoSizeFont",
            typeof(bool),
            typeof(LayoutTextControl),
            new FrameworkPropertyMetadata(
                 false,
                 FrameworkPropertyMetadataOptions.AffectsRender,
                 new PropertyChangedCallback(OnOutlineTextInvalidated),
                 null
                 )
            );

        /// <summary>
        /// Specifies whether the font should display Underline font style.
        /// </summary>
        public bool Underline
        {
            get
            {
                return (bool)GetValue(UnderlineProperty);
            }

            set
            {
                SetValue(UnderlineProperty, value);
            }
        }

        /// <summary>
        /// Identifies the Underline dependency property.
        /// </summary>
        public static readonly DependencyProperty UnderlineProperty = DependencyProperty.Register(
            "Underline",
            typeof(bool),
            typeof(LayoutTextControl),
            new FrameworkPropertyMetadata(
                 false,
                 FrameworkPropertyMetadataOptions.AffectsRender,
                 new PropertyChangedCallback(OnOutlineTextInvalidated),
                 null
                 )
            );

        /// <summary>
        /// Specifies the brush to use for the stroke and optional hightlight of the formatted text.
        /// </summary>
        public Brush Stroke
        {
            get
            {
                return (Brush)GetValue(StrokeProperty);
            }

            set
            {
                SetValue(StrokeProperty, value);
            }
        }

        /// <summary>
        /// Identifies the Stroke dependency property.
        /// </summary>
        public static readonly DependencyProperty StrokeProperty = DependencyProperty.Register(
            "Stroke",
            typeof(Brush),
            typeof(LayoutTextControl),
            new FrameworkPropertyMetadata(
                 new SolidColorBrush(Colors.Teal),
                 FrameworkPropertyMetadataOptions.AffectsRender,
                 new PropertyChangedCallback(OnOutlineTextInvalidated),
                 null
                 )
            );

        /// <summary>
        ///     The stroke thickness of the font.
        /// </summary>
        public ushort StrokeThickness
        {
            get
            {
                return (ushort)GetValue(StrokeThicknessProperty);
            }

            set
            {
                SetValue(StrokeThicknessProperty, value);
            }
        }

        /// <summary>
        /// Identifies the StrokeThickness dependency property.
        /// </summary>
        public static readonly DependencyProperty StrokeThicknessProperty = DependencyProperty.Register(
            "StrokeThickness",
            typeof(ushort),
            typeof(LayoutTextControl),
            new FrameworkPropertyMetadata(
                 (ushort)0,
                 FrameworkPropertyMetadataOptions.AffectsRender,
                 new PropertyChangedCallback(OnOutlineTextInvalidated),
                 null
                 )
            );

        /// <summary>
        /// Specifies the text string to display.
        /// </summary>
        public string Text
        {
            get
            {
                return (string)GetValue(TextProperty);
            }

            set
            {
                SetValue(TextProperty, value);
            }
        }

        /// <summary>
        /// Identifies the Text dependency property.
        /// </summary>
        public static readonly DependencyProperty TextProperty = DependencyProperty.Register(
            "Text",
            typeof(string),
            typeof(LayoutTextControl),
            new FrameworkPropertyMetadata(
                 "",
                 FrameworkPropertyMetadataOptions.AffectsRender,
                 new PropertyChangedCallback(OnOutlineTextInvalidated),
                 null
                 )
            );

        public void AddChild(Object value)
        {

        }

        public void AddText(string value)
        {
            Text = value;
        }

        #endregion
    }
}


