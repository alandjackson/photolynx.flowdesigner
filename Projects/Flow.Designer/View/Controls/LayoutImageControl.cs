﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using Flow.Designer.Model;
using System.Windows;
using System.Windows.Media.Imaging;
using System.IO;
using System.Windows.Media.Effects;
using Flow.Lib.ImageUtils;
using Flow.Designer.Lib.Service;
using System.ComponentModel;
using System.Windows.Media;
using Flow.Lib.Log;
using NLog;
using System.Configuration;
using Flow.Schema.LinqModel.DataContext;

namespace Flow.Designer.View.Controls
{
    public class LayoutImageControl : Image
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        ILoadImageService LoadImageService { get; set; }
        public DockPanel BlurDockPanel { get; set; }
        public DockPanel DropShadowDockPanel { get; set; }
        public DockPanel BwDockPanel { get; set; }
        public DockPanel SepiaDockPanel { get; set; }
        public Border ImageBorder { get; set; }
        public LayoutRenderQuality Quality { get; set; }

        

        public LayoutImageControl() : this(new LoadImageService()) { }
        
        public LayoutImageControl(ILoadImageService l)
        {
            LoadImageService = l;
            DataContextChanged += new DependencyPropertyChangedEventHandler(
                    FlowImage_DataContextChanged);

            if (ImgScalingHighQualityAlias)
            {
                RenderOptions.SetBitmapScalingMode(this, BitmapScalingMode.HighQuality);
                RenderOptions.SetEdgeMode(this, EdgeMode.Aliased);
            }
        }

        bool liEventTriggerRegistered = false;
        void FlowImage_DataContextChanged(object sender, 
            DependencyPropertyChangedEventArgs e)
        {
            if (!(this.DataContext is LayoutImage))
                return;

            LayoutImage li = (LayoutImage)DataContext;
           // if (liEventTriggerRegistered == false)
           // {
                li.PropertyChanged +=
                    new PropertyChangedEventHandler(li_PropertyChanged);
                liEventTriggerRegistered = true;
           // }


            UpdateImageSource();

            logger.Info("ImageSource Updated, about to notify property change events");

            li_PropertyChanged(this, new PropertyChangedEventArgs("DropShadowEnabled"));
            li_PropertyChanged(this, new PropertyChangedEventArgs("BlurEnabled"));
            li_PropertyChanged(this, new PropertyChangedEventArgs("IsSepia"));
            li_PropertyChanged(this, new PropertyChangedEventArgs("IsBW"));

        }

        void li_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            var li = ((LayoutImage)DataContext);

            if (e.PropertyName == "RenderImageFilename")
                UpdateImageSource();
            if (e.PropertyName == "BlurEnabled")
                UpdateBitmapEffect(li.BlurEnabled, li.Blur, BlurDockPanel);
            if (e.PropertyName == "DropShadowEnabled")
                UpdateBitmapEffect(li.DropShadowEnabled, li.DropShadow, DropShadowDockPanel);
            if (e.PropertyName == "IsSepia")
                UpdateBitmapEffect(li.IsSepia, li.Sepia, SepiaDockPanel);
            if (e.PropertyName == "IsBW")
                UpdateBitmapEffect(li.IsBW, li.GrayScale, BwDockPanel);
        }

        
        void UpdateBitmapEffect(bool enabled, Effect effect, DockPanel dp)
        {
            if (enabled)
                dp.Effect = effect;
            else
                dp.Effect = null;
        }

        protected LayoutImage _lastImageSource = null;

        static bool? _imgScalingHighQualityAlias = null;
        public static bool ImgScalingHighQualityAlias
        {
            get
            {
                if (_imgScalingHighQualityAlias == null)
                {
                    bool value;
                    if (Boolean.TryParse(ConfigurationManager.AppSettings["IMAGE_SCALING_HIGH_QUALITY_ALIAS"], out value))
                        _imgScalingHighQualityAlias = value;

                }
                return _imgScalingHighQualityAlias.HasValue ? _imgScalingHighQualityAlias.Value : false;
            }
        }

        static int? _maxHighQualityImageResize = null;
        public static int MaxHighQualityImageResize
        {
            get
            {
                if (_maxHighQualityImageResize == null)
                {
                    int value;
                    if (Int32.TryParse(ConfigurationManager.AppSettings["MAX_HIGH_QUALITY_IMAGE_AUTO_RESIZE"], out value))
                        _maxHighQualityImageResize = value;

                }
                return _maxHighQualityImageResize.HasValue ? _maxHighQualityImageResize.Value : 0;
            }
        }

        int _controlHeightAtLoad = 0;
        void UpdateImageSource()
        {
            
            LayoutImage dc = ((LayoutImage)DataContext);

            if (dc.UseOrderedImage && dc.SubjectImage != null)
            {
                SubjectImage si = dc.SubjectImage;
                if (si.Subject.SubjectOrder.OrderPackages.Count > 0)
                {
                    if (si.Subject.SubjectOrder.OrderPackageList.Any(op => op.OrderProducts.Any(prod => prod.OrderProductNodes.Any(opi => opi.SubjectImage.SubjectImageGuid == si.SubjectImageGuid))))
                    { }
                    else
                    {
                        SubjectImage anotherImage = si.Subject.SubjectOrder.OrderPackages[0].OrderProducts[0].OrderProductNodes[0].SubjectImage;

                        dc.OrderedSubjectImage = anotherImage;
                    }
                }

            }

            var rimage = "";
            Canvas rcanvas = new Canvas();
            if (dc.IsAllSubjectImages && dc.Subject != null)
                //rimage = dc.RenderImageFilenameNew;
                
                    rcanvas = dc.RenderImageFilenameNew;
                
            else
                rimage = dc.RenderImageFilename;

            logger.Info("LayoutImageControl.UpdateImageSource, rimage: {0}", rimage);

            if (rcanvas.Children.Count < 1 && (string.IsNullOrEmpty(rimage)
                || !File.Exists(rimage)))
            {
                logger.Warn("LayoutImageControl.UpdateImageSource file does not exist");
                this.Source = null;
                return;
            }

            // use the height of the image control to determine what size to load the image at
            double highSize = dc.Height * dc.Dpi / 96;
            
            // adjust the detected height for crop
            if (dc.Crop.CropW != 0 && dc.Crop.CropH != 0)
                highSize = highSize / (dc.Crop.CropW);

            // make sure the detected size is lower than the max
            if (highSize > MaxHighQualityImageResize)
                highSize = 0;

            double lowSize = highSize > 600 ? 600 : highSize;
            int loadedImageHeight = Quality == LayoutRenderQuality.High ? (int)Math.Ceiling(highSize) : (int)Math.Ceiling(lowSize);
            BitmapSource imageSource;
            if (rimage != "")
                imageSource = LoadImageService.LoadImage(rimage, loadedImageHeight);
            else
            {
                imageSource = rcanvas.ToRenderTargetBitmap(96);
                rcanvas.Children.Clear();
            }

            Int32Rect cropRect = dc.Crop.ToInt32Rect(imageSource);
            logger.Info("LayoutImageControl.UpdateImageSource load image done, source is null: {0}, is cropped: {1}",
                imageSource == null, cropRect.Width > 0 || cropRect.Height > 0);

            logger.Info("Crop W:" + cropRect.Width + " AND Crop H: " + cropRect.Height);

            if ((cropRect.Width == 0 || cropRect.Height == 0) ||  dc.alreadyCropped)
            {
                logger.Info("No Crop Needed, set the Source");
                this.Source = imageSource;
                logger.Info("Source has been set");
            }
            else
            {
                logger.Info("Crop is needed, apply crop to source");
                this.Source = new CroppedBitmap(imageSource, cropRect);
                logger.Info("Source has been set with crop");
            }

            
            logger.Info("about to run GC");
            GC.Collect();
            logger.Info("about to Wait for GC Finalizers");
            GC.WaitForPendingFinalizers();
            logger.Info("GC complete");
                
        }

        ///// <summary>
        ///// Specifies the brush to use for the stroke and optional hightlight of the formatted text.
        ///// </summary>
        //public Brush Stroke
        //{
        //    get { return (Brush)GetValue(StrokeProperty); }

        //    set { SetValue(StrokeProperty, value); }
        //}

        ///// <summary>
        ///// Identifies the Stroke dependency property.
        ///// </summary>
        //public static readonly DependencyProperty StrokeProperty = 
        //    DependencyProperty.Register(
        //    "Stroke",
        //    typeof(Brush),
        //    typeof(LayoutImageControl),
        //    new FrameworkPropertyMetadata(
        //         new SolidColorBrush(Colors.Teal),
        //         FrameworkPropertyMetadataOptions.AffectsRender,
        //         null,
        //         null
        //         )
        //    );

        ///// <summary>
        /////     The stroke thickness of the font.
        ///// </summary>
        //public ushort StrokeThickness
        //{
        //    get
        //    {
        //        return (ushort)GetValue(StrokeThicknessProperty);
        //    }

        //    set
        //    {
        //        SetValue(StrokeThicknessProperty, value);
        //    }
        //}

        ///// <summary>
        ///// Identifies the StrokeThickness dependency property.
        ///// </summary>
        //public static readonly DependencyProperty StrokeThicknessProperty = DependencyProperty.Register(
        //    "StrokeThickness",
        //    typeof(ushort),
        //    typeof(LayoutImageControl),
        //    new FrameworkPropertyMetadata(
        //         (ushort)0,
        //         FrameworkPropertyMetadataOptions.AffectsRender,
        //         null,
        //         null
        //         )
        //    );

    }
}
