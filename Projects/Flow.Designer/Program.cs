﻿using System;
using System.Windows;
using Flow.Designer.Controller;
using Flow.Designer.View;
using Flow.Lib.Persister;
using Flow.Designer.Model.Print;
using System.IO;
using NLog;

namespace Flow.Designer
{
    public class Program
    {
        public static string LoadLayoutFilename { get; set; }
        public static string LoadImagematchDatabaseFilename { get; set; }
            

        [STAThread]
        public static int Main(string[] args)
        {
            try
            {
                LogManager.GetCurrentClassLogger().Info($"Started Layout Designer {typeof(Program).Assembly.GetName().Version.ToString()}, with arguments [{string.Join(", ", args)}]");

                Run(args);
                return 0;
            }
            catch (Exception e)
            {
                // If running as a normal app, show exception in message box
                if (args.Length == 0)
                {
                    MessageBox.Show(e.ToString());
                }
                // If running at the command line, write exception to standard out
                else
                {
                    Console.WriteLine(e.ToString());
                }
                return 1;
            }
        }

        public static void InitializeTypes()
        {
            StructureMap.ObjectFactory.Initialize(x =>
            {
                x.AddRegistry<DesignerRegistry>();
                x.AddRegistry<Flow.Lib.LibRegistry>();
            });
        }

        public static void Run(string[] args)
        {
            InitializeTypes();
            if (ProcessArguments(args)) return;
            InitializeApplication().Run();
        }

        /// <summary>
        /// Processes the command line arguments. Returns true if the launching of the
        /// GUI application should be skipped.
        /// </summary>
        public static bool ProcessArguments(string[] args)
        {
            if (args.Length > 1 && args[0].ToUpper() == "PRINT")
            {
                ObjectSerializer.FromXmlFile<FlowRenderInfo>(args[1]).RenderToImage();
                return true;
            }
            foreach (var arg in args)
            {
                if (arg.ToUpper().EndsWith(".LYT"))
                {
                    LoadLayoutFilename = arg;
                }
                if (arg.ToUpper().EndsWith(".MDB"))
                {
                    LoadImagematchDatabaseFilename = arg;
                }
            }
            return false;
        }

        public static Application InitializeApplication()
        {
            var appWindow = new AppWindow();
            appWindow.Title = "Layout Designer " + typeof(Program).Assembly.GetName().Version.ToString();
            appWindow.WindowState = WindowState.Maximized;
            appWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            appWindow.Loaded += (s, e) =>
            {
                var DesignerController = new DesignerController() { CanCreateEditLayouts = true }.Main(null, appWindow);

                if(! string.IsNullOrEmpty(LoadLayoutFilename) && File.Exists(LoadLayoutFilename))
                {
                    var fi = new FileInfo(LoadLayoutFilename);
                    DesignerController.Surface.LoadLayoutFromFile(fi);
                    DesignerController.FlowDesignerDataContext.LayoutPreferences.LayoutsDirectory = fi.DirectoryName;
                }
            };

            var app = new Application();
            app.Resources.MergedDictionaries.Add(new ResourceDictionary()
            {
                Source = new Uri("ExpressiveDefault.xaml", UriKind.Relative)
            });

            app.MainWindow = appWindow;
            app.ShutdownMode = ShutdownMode.OnMainWindowClose;
            app.Startup += (s, e) =>
            {
                app.MainWindow.Show();
            };
            return app;
        }
    }
}
