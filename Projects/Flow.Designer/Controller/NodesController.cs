﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.Designer.View;
using Flow.Designer.Model.Designer;
using Flow.Lib.Helpers;
using System.Collections;
using System.Windows;

namespace Flow.Designer.Controller
{
    public class NodesController
    {
        protected DesignerController DesignerController { get; set; }
        ObservableDesignerModel _omodel;
        Nodes _pnl;

        public NodesController(Nodes pnl, DesignerModel model,
            DesignerController dc)
            : this(pnl, new ObservableDesignerModel(model), dc) { }

        public NodesController(Nodes pnl, ObservableDesignerModel omodel,
            DesignerController dc)
        {
            DesignerController = dc;

            _omodel = omodel;
            _omodel.Model.LayoutRebindNeeded += new EventHandler(_omodel_LayoutRebindNeeded);
            _omodel.LayoutChanged += new EventHandler<EventArgs<Flow.Designer.Model.Layout>>(_omodel_LayoutChanged);
            _omodel.SelectedLayoutItemsChanged += new EventHandler<EventArgs<Flow.Designer.Model.ILayoutItem[]>>(_omodel_SelectedLayoutItemsChanged);
            _pnl = pnl;
            _pnl.DataContext = _omodel.Layout;
            _pnl.uxNodesList.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(uxNodesList_SelectionChanged);
            _pnl.uxDeleteNode.Click += new RoutedEventHandler(DeleteNode_Click);
            _pnl.uxDuplicateNode.Click += new RoutedEventHandler(DuplicateNode_Click);
        }

        void _omodel_LayoutRebindNeeded(object sender, EventArgs e)
        {
            UpdateDataContext();
        }

        private void DeleteNode_Click(object sender, RoutedEventArgs e)
        {
            DesignerController.DeleteSelectedItems();
        }

        private void DuplicateNode_Click(object sender, RoutedEventArgs e)
        {
            DesignerController.DuplicateSelectedItems();
        }


        void uxNodesList_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (_isUpdatedItems)
                return;

            _isUpdatedItems = true;
            SetSelectedItems(_pnl.uxNodesList.SelectedItems, _omodel.SelectableLayout.SelectedItems);
            _isUpdatedItems = false;
        }

        void _omodel_SelectedLayoutItemsChanged(object sender, EventArgs<Flow.Designer.Model.ILayoutItem[]> e)
        {
            if (_isUpdatedItems)
                return;

            _isUpdatedItems = true;
            SetSelectedItems(_omodel.SelectableLayout.SelectedItems, _pnl.uxNodesList.SelectedItems);
            _isUpdatedItems = false;
        }

        bool _isUpdatedItems = false;
        void SetSelectedItems(IList srcList, IList dstList)
        {
            dstList.Clear();
            foreach (object o in srcList)
                dstList.Add(o);
        }

        void _omodel_LayoutChanged(object sender, EventArgs<Flow.Designer.Model.Layout> e)
        {
            UpdateDataContext();
        }

        protected void UpdateDataContext()
        {
            _pnl.DataContext = _omodel.Model.SelectableLayout.Layout;
        }



    }
}
