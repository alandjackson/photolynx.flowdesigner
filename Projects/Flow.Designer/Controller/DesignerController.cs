﻿using Flow.Designer.Lib.ProjectInterop;
using Flow.Designer.Lib.ProjectRepository;
using Flow.Designer.Lib.Service;
using Flow.Designer.Model;
using Flow.Designer.Model.Designer;
using Flow.Designer.Model.Project;
using Flow.Designer.Model.Settings;
using Flow.Designer.View;
using Flow.Designer.View.Dialogs;
using Flow.Helpers;
using Flow.Lib;
using Flow.Lib.Helpers;
using Flow.Lib.ImageQuix;
using Flow.Lib.Persister;
using Flow.Lib.Preferences;
using Flow.Lib.UnitOfWork;
using Flow.Schema.LinqModel.DataContext;
using ICSharpCode.SharpZipLib.Core;
using Microsoft.Win32;
using NetServ.Net.Json;
using StructureMap;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Xml.Linq;


namespace Flow.Designer.Controller
{
    public class DesignerController : IFlowController<DesignerController>
    {
        public ConfigRepository ConfigRepository { get; set; }

        public HistoryController HistoryController { get; set; }
        public PropertiesController PropertiesController { get; set; }

        //public FlowMasterDataContext FlowMasterDataContext { get { return FlowDesignerDataContext.FlowMasterDataContext; } set { FlowDesignerDataContext.FlowMasterDataContext = value; } }
        public FlowDesignerDataContext FlowDesignerDataContext { get; set; } 
        public FlowProject FlowProject { get; set; }

        public Surface Surface { get; set; }
        public PropertiesPanel Properties { get; set; }
        public Nodes Nodes { get; set; }

        public List<KeyPair> CatalogProducts { get; set; }

        protected string currentImageDestination = null;

        protected DesignerModel DesignerModel { get; set; } = new DesignerModel();

        public SelectableLayout SelectableLayout 
        { 
            get { return DesignerModel.SelectableLayout; } 
        }
        protected Layout Layout 
        { 
            get { return SelectableLayout.Layout; } 
            set { SelectableLayout.Layout = value; } 
        }
        protected NodesController NodesController { get; set; }
        protected ILayoutSubjectsPreviewService LayoutSubjectsPreviewService { get; set; }
        protected LayoutSaveImageService LayoutSaveImageService { get; set; }
        PrintSettingsRepository PrintSettingsRepository { get; set; }

        public bool renderingImages = false;
        IUnitOfWorkProcessor UnitOfWorkProcessor { get; set; }

        public DesignerController()
            : this (new Surface(), new Nodes(), new History(), new PropertiesPanel(),
                  new ConfigRepository(), new LayoutSubjectsPreviewService(new AbstractSubjectsRepository()), 
                  new LayoutSaveImageService(), new UnitOfWorkProcessor(), new PrintSettingsRepository())
        {
        }
        
        public DesignerController(
            Surface s, 
            Nodes n, 
            History h,
            PropertiesPanel p, 
            ConfigRepository cr,
            ILayoutSubjectsPreviewService previewService,
            LayoutSaveImageService saveImageService,
            IUnitOfWorkProcessor workProcessor,
            PrintSettingsRepository settingsRepository)
        {
            FlowDesignerDataContext = ObjectFactory.GetInstance<FlowDesignerDataContext>();

            PrintSettingsRepository = settingsRepository;
            LayoutSaveImageService = saveImageService;
            UnitOfWorkProcessor = workProcessor;

            // IOC: create from factory
            PropertiesController = new PropertiesController(
                s.uxProperties, DesignerModel);
            PropertiesController.DesignerController = this;

            NodesController = new NodesController(s.uxNodes, DesignerModel, this);
            LayoutSubjectsPreviewService = previewService;
            Nodes = n;

            Surface = s;
            Surface.Designer = this;
            Surface.Model = DesignerModel;
            Surface.DataContextChanged += 
                new DependencyPropertyChangedEventHandler(
                    Surface_DataContextChanged);
            
            Surface.PreviewService = previewService;

            
            previewService.SubjectChanged +=
                new EventHandler((object sender, EventArgs e) =>
                {
                    if (!this.IsActive) return;

                    Surface.SuspendLayoutBinding = true;
                    DesignerModel.SuspendPropertyNotifying("*");
                    DesignerModel.DesignerDynamicFields = Flow.Designer.Model.Project.DesignerDynamicFields.Build(this.FlowProject);
                    DesignerModel.ImageTypes = GetImageTypes();
                    DesignerModel.Subject = ((ICurrentSubjectRepository)sender).GetSubject();
                    DesignerModel.ResumePropertyNotifying("*", false);
                    Surface.SuspendLayoutBinding = false;
                    Surface.RebuildLayoutElements();
                });

            // IOC: create from factory
            HistoryController = new HistoryController(new ObservableDesignerModel(DesignerModel));

            ConfigRepository = cr;

            Properties = p;

            Surface.SurfaceLoaded += (object sender, EventArgs args) => { LoadUserState(); };
        }

        protected ImageTypes GetImageTypes()
        {
            return ImageTypes.Build(FlowDesignerDataContext);
        }

        void Surface_DataContextChanged(object sender, 
            DependencyPropertyChangedEventArgs e)
        {
            Nodes.DataContext = Layout;
        }

        protected bool IsActive { get; set; }

        public void Deactivate()
        {
            IsActive = false;

            SaveUserState();
        }

        /// <summary>
        /// Saves the designer state between runs
        /// </summary>
        protected void SaveUserState()
        {
            var state = new PersistedDesignerState { GridSize = Surface.GridAdorner.GridSize };
            ConfigRepository.Save<PersistedDesignerState>(state);
        }

        /// <summary>
        /// Loads the state saved between designer runs
        /// </summary>
        protected void LoadUserState()
        {
            try
            {
                var state = ConfigRepository.Load<PersistedDesignerState>();
                Surface.GridAdorner.GridSize = state.GridSize;
            }
            catch (Exception e)
            {
                MessageBox.Show("Error loading designer state: " + e.ToString());
            }
        }



        public DesignerController Activate()
        {
            // this will throw the property changed event and refresh the UI
            Surface.PreviewService.UpdateRecordsCount();
            
            Flow.Designer.Model.Project.DesignerDynamicFields temp = new Flow.Designer.Model.Project.DesignerDynamicFields();
            if (this.FlowProject !=null)
            {
                temp.Fields = this.FlowProject.Organization.Fields;
                DesignerModel.DesignerDynamicFields = temp;
            }

            DesignerModel.Subject = Surface.PreviewService.GetCurrentSubject();
            DesignerModel.ImageTypes = GetImageTypes();

            IsActive = true;
            return this;
        }

        public bool CanCreateEditLayouts
        {
            set
            {
                Visibility newVis = value ? Visibility.Visible : Visibility.Hidden;

                Surface.uxNew.Visibility = newVis;
                Surface.uxSave.Visibility = newVis;
                Surface.uxNodes.uxDeleteNode.Visibility = newVis;
                Surface.uxNodes.uxDuplicateNode.Visibility = newVis;
                Surface.spTools.Visibility = newVis;
            }
        }


        /// <summary>
        /// Shows the designer and any necessary tool windows
        /// </summary>
        public DesignerController Main(FlowProject fp, ContentControl parent)
        {
            parent.Content = Surface;
            this.FlowProject = fp;

            Surface.LayoutPreferences = FlowDesignerDataContext.LayoutPreferences;
            DesignerModel.GraphicsUnits = FromString(FlowDesignerDataContext.LayoutPreferences.GraphicsUnits);
            DesignerModel.ImageTypes = ImageTypes.Build(FlowDesignerDataContext);

            //if (FlowMasterDataContext != null)
            //{
            //    CatalogProducts = new List<KeyPair>();
            //    if (FlowMasterDataContext != null && this.FlowMasterDataContext.ImageQuixCatalogList.Count > 0)
            //    {
            //        foreach (ImageQuixProduct prod in this.FlowMasterDataContext.ImageQuixCatalogList[0].ImageQuixProducts)
            //        {
            //            CatalogProducts.Add(new KeyPair(prod.PrimaryKey, prod.Label));
            //        }
            //    }
            //}

            PropertiesController.Initialize();
            return this;
        }

        protected GraphicsUnits FromString(string str)
        {
            switch (str)
            {
                case "Inches":
                    return GraphicsUnits.Inches;
                case "Pixels":
                    return GraphicsUnits.Pixels;
                case "Centimeters":
                    return GraphicsUnits.Centimeters;
            }
            throw new Exception("Unknown graphics units string: " + str);
        }

        /// <summary>
        /// Lets the user start a new layout
        /// </summary>
        public void NewLayout()
        {
            if (!Surface.VerifyExistingLayoutSaved()) return;
            Surface.LoadedLayoutFile = null;
            Layout = new Layout();
            SelectableLayout.SelectedItem = null;
        }

        public void NewImage()
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.InitialDirectory = FlowContext.InitDir(FlowDesignerDataContext.LayoutPreferences.GraphicsDirectory);
            dlg.Title = "Select an image to add.";
            dlg.Filter = "Image Files (*.jpg, *.jpeg, *.png, *.gif, *.bmp)|*.jpg;*.gif;*.bmp;*.png;*.jpeg|All Files (*.*)|*.*";
            if (dlg.ShowDialog() != true)
                return;

            ILayoutItem li = new LayoutImage(dlg.FileName);
            Layout.LayoutItems.Add(li);
            SelectableLayout.SelectedItem = li;
        }

        public void NewText()
        {
            ILayoutItem li = new LayoutText("New Text");
            Layout.LayoutItems.Add(li);
            SelectableLayout.SelectedItem = li;
        }

        public void Print()
        {
            PrintSettings ps = PrintSettingsRepository.Load(FlowDesignerDataContext.LayoutPreferences.Designer);
            LayoutSubjectsPreviewService.IsPreviewEnabled = true;
            if (ps.PrintDestination == PrintSettings.PrintDestiations.WindowsPrinter)
            {
                worker_DoWork(this, new DoWorkEventArgs(new ThreadSafeLayout(Layout)));
            }
            else
            {
                StaBackgroundWorker worker = new StaBackgroundWorker();
                worker.DoWork += new DoWorkEventHandler(worker_DoWork);
                worker.RunWorkerAsync(new ThreadSafeLayout(Layout));
            }
        }

        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            Layout l = (e.Argument as ThreadSafeLayout).ToLayout();
            if (l == null)
                throw new Exception("Designer.Print layout argument is null");

            LayoutSaveImageService.FlowMasterDataContext = FlowDesignerDataContext;
            PrintSettings ps = PrintSettingsRepository.Load(FlowDesignerDataContext.LayoutPreferences.Designer);
            if (ps.PrintDestination == PrintSettings.PrintDestiations.Image)
            {
                if (string.IsNullOrEmpty(currentImageDestination))
                    currentImageDestination = ps.ImageOutputFolderPath;

                if (!InitImagePrint(currentImageDestination))
                    return;
            }

            var ProgressInfo = new NotificationProgressInfo("Rendering Layout", "Begin Rendering");
            ProgressInfo.AllowCancel = false;
            IShowsNotificationProgress progressNotification = ObjectFactory.GetInstance<IShowsNotificationProgress>();
            progressNotification.ShowNotification(ProgressInfo);

            ProgressInfo.Progress = 10;
            progressNotification.UpdateNotification(ProgressInfo);

            if (ps.PrintDestination == PrintSettings.PrintDestiations.Image)
            {
                string fileNameText = ps.UseSubjectImageNameInFileName ? l.GetOriginalFilename(LayoutSubjectsPreviewService) : "";
                fileNameText = string.IsNullOrWhiteSpace(fileNameText) && ps.UseSubjectNameInFileName ? l.GetSubjectNameForFileName(LayoutSubjectsPreviewService) : fileNameText;
                string filename = LayoutSaveImageService.GetNextOutputFilename(currentImageDestination, fileNameText);
                l.GenerateImageFile(ps.ImageDPI, filename);
            }
            else if (ps.PrintDestination == PrintSettings.PrintDestiations.WindowsPrinter)
            {
                l.SendToPrinter(PrintSettingsRepository.PrintDialog);
            }
            ProgressInfo.Progress = 100;
            progressNotification.UpdateNotification(ProgressInfo);

            ProgressInfo.Complete("Complete");
            progressNotification.UpdateNotification(ProgressInfo);
        }

        public void BatchOrder(string tempDestination, bool useSubjectGuidFileName, string saveOrderExportPath)
        {
            throw new NotImplementedException();

            //List<ZipFileMapping> imageFileList = new List<ZipFileMapping>();
            //List<string> allFileNames = new List<string>();
            //foreach(string thisFile in Directory.GetFiles(tempDestination))
            //{
            //    if (thisFile.ToLower().EndsWith(".jpg"))
            //    {
            //        FileInfo thisFileInfo = new FileInfo(thisFile);
            //        imageFileList.Add(new ZipFileMapping(thisFileInfo));
            //        allFileNames.Add(thisFileInfo.Name);
            //    }
            //}

            ////Organization assignedStudio = this.FlowMasterDataContext.StudioList.FirstOrDefault(s => s.OrganizationID == this.FlowProject.StudioID.Value);
            //var assignedStudio = FlowDesignerDataContext.Organization;
            //XElement custInfoXml = assignedStudio.GetCustomerInfo();


            //// Create the product data xml doc
            //XDocument productDataXDocument = this.FlowProject.GenerateProjectDataXml();
            //XElement recordsXElement = productDataXDocument.Element("project").Element("records");

            //// Create the order data xml doc
            //XDocument orderXDocument = new XDocument();
            //XElement orderElement = new XElement("Order");
            ////orderElement.AddNewXAttribute("pk", orderID);

            //List<Flow.Schema.LinqModel.DataContext.Subject> applicableSubjects = new List<Flow.Schema.LinqModel.DataContext.Subject>();
            //foreach (Flow.Schema.LinqModel.DataContext.Subject thisSubject in this.FlowProject.SubjectList.View)
            //{
            //    string defaultImageName = thisSubject.SubjectGuid.Value.ToString().Replace("-", "").Left(26) + ".jpg";
            //    if (allFileNames.Contains(defaultImageName))
            //    {
            //        //this is one of the Subjects used
            //        applicableSubjects.Add(thisSubject);
            //        thisSubject.SupplyOrderProjectData(recordsXElement, imageFileList, true, null,false, false);

            //        //if((!includeAllSubjects) || subjectsWithOrderItems.Contains(subject))
            //        thisSubject.SupplyOrderData(orderElement, this.Layout.CatalogProductKey, defaultImageName, this.FlowMasterDataContext.ImageQuixCatalogList[0]);

            //    }
            //}
            //orderXDocument.Add(orderElement);

            //try
            //{
            //    JsonObject SubmitReturnObject = ImageQuixUtil.SubmitOrder(
            //           imageFileList,
            //           productDataXDocument,
            //           orderXDocument,
            //           custInfoXml,
            //           this.FlowMasterDataContext.PreferenceManager.Upload,
            //           saveOrderExportPath,
            //           this.FlowProject.FlowProjectName,
            //           null, null, null,new JsonArray(), new List<KeyValuePair<ZipFileMapping,XElement>>(), null
            //       );
            //}
            //catch (Exception)
            //{
            //    this.Surface.Dispatcher.Invoke((Action) delegate 
            //    {
            //        Flow.Controls.View.Dialogs.FlowMessageDialog msg = 
            //            new Flow.Controls.View.Dialogs.FlowMessageDialog("Problem Connecting", 
            //                "There was an error connecting to the order server.\n" +
            //                "The server may be down or your connection setting may be incorrect.\n" +
            //                "Please check your connection settings.\n" +
            //                "Preferences -> Remote Connection -> Lab Catalog");
            //        msg.CancelButtonVisible = false;
            //        msg.ShowDialog();
            //    });
            //    return;
            //}

        }
        public void BatchPrint(string tempDestination, bool useSubjectGuidFileName, bool sendOrder, string saveOrderExportPath)
        {
            LayoutSubjectsPreviewService.IsPreviewEnabled = true;
            renderingImages = true;

            currentImageDestination = FlowDesignerDataContext.LayoutPreferences.Designer.ImageOutputFolderPath;
            if (tempDestination != null)
                currentImageDestination = tempDestination;

            PrintSettings ps = PrintSettingsRepository.Load(FlowDesignerDataContext.LayoutPreferences.Designer);
            if (ps.PrintDestination == PrintSettings.PrintDestiations.Image)
            {
                if (!InitImagePrint(currentImageDestination))
                    return;
            }


            LayoutSaveImageService.FlowMasterDataContext = this.FlowDesignerDataContext;
            
            var ProgressInfo = 
                new NotificationProgressInfo("Layouts Rendering", 
                    "Rendering " + 
                    LayoutSubjectsPreviewService.RecordsCount + 
                    " Layouts");
            ProgressInfo.AllowCancel = true;
            IShowsNotificationProgress progressNotification = 
                ObjectFactory.GetInstance<IShowsNotificationProgress>();
            progressNotification.ShowNotification(ProgressInfo);

            LayoutRenderBatchService worker = new LayoutRenderBatchService(ps,
                ObjectSerializer.ToXmlString(Layout),
                DesignerModel.ImageTypes,
                DesignerModel.DesignerDynamicFields,
                LayoutSubjectsPreviewService,
                LayoutSaveImageService,
                UnitOfWorkProcessor,
                PrintSettingsRepository,
                currentImageDestination,
                useSubjectGuidFileName);
            //worker.FlowMasterDataContext = this.FlowMasterDataContext;
            worker.CancelWorker = false;

            worker.ProgressChanged += new ProgressChangedEventHandler(
                (object sender, ProgressChangedEventArgs e) =>
                {
                    ProgressInfo.Message = e.UserState.ToString();
                    ProgressInfo.Progress = e.ProgressPercentage;
                   
                    progressNotification.UpdateNotification(ProgressInfo);

                    if (ProgressInfo.IsCanceled)
                    {
                        renderingImages = false;
                        worker.CancelWorker = true;
                       
                    }

                     if (ProgressInfo.Message == "Rendering Layouts Complete")
                    {

                        if (sendOrder && !ProgressInfo.IsCanceled)
                         {
                             ProgressInfo.Update("Sending Order");
                             BatchOrder(tempDestination, useSubjectGuidFileName, saveOrderExportPath);
                         }
                        ProgressInfo.Complete("Rendering Layouts Complete");
                        progressNotification.UpdateNotification(ProgressInfo);
                       
                        renderingImages = false;
                        
                       
                    }
                });

            if (ps.PrintDestination == PrintSettings.PrintDestiations.WindowsPrinter)
            {
                worker.RunWorker();
            }
            else
            {
                worker.RunWorkerAsync();
            }
        }

        public void PrintSetup()
        {
            ImagePrintSetup printSetupDlg = new ImagePrintSetup();
            printSetupDlg.DataContext = PrintSettingsRepository.Load(FlowDesignerDataContext.LayoutPreferences.Designer);
            Window w = UIUtil.WrapInWindow(printSetupDlg);
            w.Title = "Print Settings";
            if (w.ShowDialog() != true)
                return;

            PrintSettingsRepository.Save((PrintSettings)printSetupDlg.DataContext, FlowDesignerDataContext.LayoutPreferences.Designer);
            FlowDesignerDataContext.SavePreferences();
            //repository.Save((PrintSettings)printSetupDlg.DataContext);
        }

        void PrintDlg_FileOk(object sender, CancelEventArgs e)
        {
            SaveFileDialog dlg = sender as SaveFileDialog;
            string filename = dlg.FileName;
            if (new FileInfo(filename).Extension.Length == 0)
                filename += ".jpg";
        }

        /// <summary>
        /// Deletes the selected objects
        /// </summary>
        public void DeleteSelectedItems()
        {
            ILayoutItem[] items = SelectableLayout.SelectedItems.ToArray();
            SelectableLayout.SelectedItems.Clear();
            foreach (ILayoutItem item in items)
                Layout.LayoutItems.Remove(item);
        }

        public void OffsetSelectedItems(double x, double y)
        {
            foreach (ILayoutItem item in SelectableLayout.SelectedItems)
            {
                var location = item.Location;
                location.Offset(x, y);
                item.Location = location;
            }
        }


        /// <summary>
        /// Creates a copy of the selected objects
        /// </summary>
        public void DuplicateSelectedItems()
        {
            SelectableLayout.Layout.SuspendPropertyNotifying("*");
            ILayoutItem[] items = SelectableLayout.SelectedItems.ToArray();
            foreach (ILayoutItem item in items)
                Layout.LayoutItems.Add(item.Translate(10, 10));
            SelectableLayout.Layout.ResumePropertyNotifying("*", false);
            SelectableLayout.Layout.ResumePropertyNotifying(
                "LayoutItems", true);
            
        }

        public void Undo()
        {
            HistoryController.Undo();
        }

        public void Redo()
        {
            HistoryController.Redo();
        }


        protected bool InitImagePrint(string printFolder)
        {
            try
            {
                IOUtil.CreateDirectory(printFolder);
                return true;
            }
            catch (Exception)
            {
                MessageBox.Show("Failed to create image output folder path '" +
                    printFolder + "'" + 
                    " please correct the print settings\n\n" + 
                    "Print job has been aborted!");
                return false;
            }
        }
    }
}
