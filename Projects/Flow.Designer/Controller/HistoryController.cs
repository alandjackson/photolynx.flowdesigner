﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.Designer.Model;
using Flow.Designer.Lib.Collections;
using Flow.Designer.View;
using Flow.Designer.Model.Designer;
using Flow.Lib.Helpers;
using System.ComponentModel;
using Flow.Lib.Persister;

namespace Flow.Designer.Controller
{
    public class HistoryController
    {
        public ObservableDesignerModel Model { get; set; }
        public UndoRedoStack<Layout> History { get; set; }
        protected Layout WatchedLayout { get; set; }

        public HistoryController(ObservableDesignerModel m)
        {
            History = new UndoRedoStack<Layout>();

            Model = m;
            Model.LayoutChanged += 
                new EventHandler<EventArgs<Layout>>(Model_LayoutChanged);
            TakeSnapshot();

            WatchedLayout = Model.Layout;
            WatchedLayout.LayoutItemPropertyChaged += 
                new EventHandler(WatchedLayout_LayoutItemPropertyChaged);
        }

        void WatchedLayout_LayoutItemPropertyChaged(object sender, EventArgs e)
        {
            TakeSnapshot();
        }


        void Model_LayoutChanged(object sender, Flow.Lib.Helpers.EventArgs<Layout> e)
        {
            if (WatchedLayout != Model.Layout)
            {
                WatchedLayout.LayoutItemPropertyChaged -= new
                    EventHandler(WatchedLayout_LayoutItemPropertyChaged);
                WatchedLayout = Model.Layout;
                WatchedLayout.LayoutItemPropertyChaged += new 
                    EventHandler(WatchedLayout_LayoutItemPropertyChaged);
            }

            TakeSnapshot();
        }

        bool _snapshotsEnabled = true;
        void TakeSnapshot()
        {
            if (! _snapshotsEnabled)
                return;

            if (IsModelChangedFromTop)
                History.Push((Layout)Model.Layout.Clone());
        }

        protected bool IsModelChangedFromTop
        {
            get
            {
                if (History.Top == null)
                    return true;

                return ! ObjectSerializer.ToXmlString(History.Top).Equals(
                      ObjectSerializer.ToXmlString(Model.Layout));
            }
        }

        public void Undo()
        {
            if (History.Undo())
                LoadTop();
        }

        /// <summary>
        /// Sets the current layout to the current top of the history stack
        /// </summary>
        public void LoadTop()
        {
            _snapshotsEnabled = false;
            Model.Layout = History.Top;
            _snapshotsEnabled = true;
        }

        public void Redo()
        {
            if (History.Redo())
                LoadTop();
        }



    }
}
