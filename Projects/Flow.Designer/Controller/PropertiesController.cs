﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.Designer.Model.Designer;
using System.ComponentModel;
using Flow.Designer.Model;
using Flow.Designer.View;
using System.Windows;
using Flow.Designer.View.Properties;
using Flow.Lib.Helpers;
using Flow.Schema.LinqModel.DataContext;

namespace Flow.Designer.Controller
{
    public class PropertiesController
    {
        ObservableDesignerModel _omodel;
        PropertiesPanel _pnl;

        public DesignerController DesignerController { get; set; }

        public Dictionary<Type, UIElement> PropertyContentPanels { get; set; }
        ILayoutItem _currentItem = null;

        public PropertiesController(PropertiesPanel pnl, DesignerModel model)
        {
            _omodel = new ObservableDesignerModel(model);
            _omodel.SelectedLayoutItemsChanged += 
                new EventHandler<EventArgs<ILayoutItem[]>>(_omodel_SelectedLayoutItemsChanged);

            _pnl = pnl;
            _pnl.Loaded += new RoutedEventHandler(_pnl_Loaded);

            PropertyContentPanels = new Dictionary<Type, UIElement>();
            PropertyContentPanels.Add(typeof(LayoutText), new TextPropertiesPanel());
        }

        public void Initialize()
        {
            PropertyContentPanels[typeof(LayoutText)] =
                new TextPropertiesPanel(GetFields());
        }

        string[] GetFields()
        {

            var fields = FlowProjectDataContext.GetProjectSubjectFieldList(DesignerController.FlowProject);
            return fields.Select(s => s.SubjectFieldDisplayName).ToArray();
        }

        void _pnl_Loaded(object sender, RoutedEventArgs e)
        {
            ShowContentProperties(null);
        }

        void _omodel_SelectedLayoutItemsChanged(object sender, EventArgs<ILayoutItem[]> e)
        {
            if (e.Data.Length != 1)
                ShowContentProperties(null);
            else
                ShowContentProperties(e.Data[0]);
            
        }

        void ShowContentProperties(ILayoutItem li)
        {
            // default to the layout surface
            if (li == null)
                li = _omodel.Model.SelectableLayout.Layout;

            if (li == _currentItem) return;

            UIElement contentPnl = GetContentPanel(li.GetType());
            if (contentPnl is FrameworkElement)
                ((FrameworkElement)contentPnl).DataContext = new PropertiesPanelModel(this) 
                    { LayoutItem = li, GraphicsUnits = _omodel.Model.GraphicsUnits };

            _pnl.uxContentPanel.Children.Clear();
            _pnl.uxContentPanel.Children.Add(contentPnl);
            //_pnl.uxContentPanel.Children.Add(new OrderPropertiesPanel(DesignerController.CatalogProducts));
            _currentItem = li;
        }

        UIElement GetContentPanel(Type t)
        {
            if (!PropertyContentPanels.ContainsKey(t))
            {
                PropertyContentPanels[t] = CreateContentPanel(t);
                _pnl.uxContentPanel.Children.Add(PropertyContentPanels[t]);
            }

           
            return PropertyContentPanels[t];
        }

        UIElement CreateContentPanel(Type t)
        {
            if (t == null || t == typeof(Layout))
                return new SurfacePropertiesPanel();
            else if (t == typeof(LayoutImage))
                return new ImagePropertiesPanel(
                    Flow.Designer.Model.Project.ImageTypes.Build(DesignerController.FlowDesignerDataContext), 
                    DesignerController.FlowDesignerDataContext.LayoutPreferences.GraphicsDirectory);
            else if (t == typeof(LayoutText))
                return new TextPropertiesPanel();
            else if (t == typeof(LayoutGroup))
                return new GroupPropertiesPanel();

            throw new Exception("Can't create properties panel for type: " + t.Name);
                
        }
    }
}
