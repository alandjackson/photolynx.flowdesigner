﻿using Flow.Lib.Persister;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.Designer.Lib.Service;
using Flow.Designer.Controller;
using System.IO;
using Flow.Designer.Model.Project;
using System.Data;

namespace Flow.Designer.Model.Print
{
    public class FlowRenderInfo
    {
        public string FlowLayoutFilename { get; set; }
        public System.Data.DataSet DataSet { get; set; }
        public string SubjectImageFilename { get; set; }
        public string OutputFilename { get; set; }
        public int Dpi { get; set; }

        public void RenderToImage()
        {
            var db = StructureMap.ObjectFactory.GetInstance<FlowDesignerDataContext>();
            db.LayoutPreferences.Designer.ImageDPI = Dpi;
            db.LayoutPreferences.Designer.ImageFormat = System.IO.Path.GetExtension(OutputFilename).Substring(1);

            var layoutInDefaultDirectory = Path.Combine(db.LayoutPreferences.LayoutsDirectory, FlowLayoutFilename);
            if (! File.Exists(FlowLayoutFilename) && File.Exists(layoutInDefaultDirectory))
            {
                FlowLayoutFilename = layoutInDefaultDirectory;
            }

            Layout l = ObjectSerializer.FromXmlFile<Layout>(FlowLayoutFilename);


            l.Subject = BuildSubject();
            //l.ImageTypes = this.ImageTypes;
            //l.DesignerDynamicFields = this.DesignerDynamicFields;

            l.GenerateImageFile(Dpi, OutputFilename);            
        }

        public Subject BuildSubject()
        {
            var s = new Subject();
            if (! string.IsNullOrWhiteSpace(SubjectImageFilename))
            {
                s.Images = new List<string>(new string[] { SubjectImageFilename });
            }
            if (DataSet != null && DataSet.Tables.Count > 0 && DataSet.Tables[0].Rows.Count > 0)
            {
                foreach (DataColumn dc in DataSet.Tables[0].Columns)
                {
                    s.Fields[dc.ColumnName] = DataSet.Tables[0].Rows[0][dc].ToSafeString();
                }
            }
            return s;
        }
    }

    public static class ObjectExtensions
    {
        public static string ToSafeString(this object o)
        {
            if (o == null) return string.Empty;
            return o.ToString();
        }
    }
}
