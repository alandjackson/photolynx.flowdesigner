﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.ComponentModel;
using System.Xml.Serialization;
using Flow.Lib.Persister;
using Flow.Designer.Model.Project;
using Flow.Designer.Lib.Service;
using Flow.Lib.Helpers;

namespace Flow.Designer.Model
{
    public class AbstractNotifyPropertyChanged : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }


        #endregion
    }

    public class SuspendableNotifyPropertyChanged : AbstractNotifyPropertyChanged
    {
        HashSet<string> _suspendedProps = new HashSet<string>();

        public virtual void SuspendPropertyNotifying(string propname)
        {
            _suspendedProps.Add(propname);
        }

        public virtual void ResumePropertyNotifying(string propname, bool throwEvent)
        {
            if (_suspendedProps.Contains(propname))
                _suspendedProps.Remove(propname);

            if (throwEvent)
                OnPropertyChanged(propname);
        }

        protected override void OnPropertyChanged(string propName)
        {
            if (_suspendedProps.Contains(propName) ||
                _suspendedProps.Contains("*"))
                return;

            base.OnPropertyChanged(propName);
        }
    }

    public class AbstractLayoutElement : SuspendableNotifyPropertyChanged, ILayoutItem
    {
        protected Rect _location;
        public Rect Location 
        { 
            get { return _location; } 
            set 
            {
                if (_location.Equals(value))
                    return;

                var propsChanged = new List<string>();
                propsChanged.Add("Location");
                if (_location.Width != value.Width)
                    propsChanged.Add("Width");
                if (_location.Height != value.Height)
                    propsChanged.Add("Height");
                if (_location.Height != value.Y)
                    propsChanged.Add("Y");
                if (_location.Height != value.X)
                    propsChanged.Add("X");

                _location = value;
                propsChanged.Each(s => OnPropertyChanged(s));
            } 
        }

        protected int _dpi = 96;
        public int Dpi
        {
            get { return _dpi; }
            set { if (_dpi == value) return; _dpi = value; OnPropertyChanged("Dpi"); }
        }

        protected double _rotateAngle = 0;
        public double RotateAngle
        {
            get { return _rotateAngle; }
            set
            {
                if (_rotateAngle == value) { return; }
                _rotateAngle = value;
                OnPropertyChanged("RotateAngle");
            }
        }

        [XmlIgnore()]
        public Layout Layout { get; set; }

        protected Subject _subject;
        [XmlIgnore()]
        public Subject Subject
        {
            get { return _subject; }
            set
            {
                _subject = value;
                OnPropertyChanged("Subject");
            }
        }

        protected DesignerDynamicFields _designerDynamicFields;
        [XmlIgnore()]
        public DesignerDynamicFields DesignerDynamicFields
        {
            get { return _designerDynamicFields; }
            set { _designerDynamicFields = value;
                OnPropertyChanged("DesignerDynamicFields");
            }
        }

        protected Flow.Schema.LinqModel.DataContext.SubjectImage _orderedSubjectImage;
        [XmlIgnore()]
        public Flow.Schema.LinqModel.DataContext.SubjectImage OrderedSubjectImage
        {
            get { return _orderedSubjectImage; }
            set { if (_orderedSubjectImage != value) { _orderedSubjectImage = value; OnPropertyChanged("OrderedSubjectImage"); } }
        }

        [XmlIgnore()]
        public TextSubstitution TextSubstitution
        {
            get { return TextSubstitutionFactory.Build(Subject, DesignerDynamicFields); }
        }


        protected ImageTypes _imageTypes;
        [XmlIgnore()]
        public ImageTypes ImageTypes
        {
            get { return _imageTypes; }
            set
            {
                _imageTypes = value;
                OnPropertyChanged("ImageTypes");
            }
        }

        protected string _itemName;
        public string ItemName
        { 
            get { return _itemName; } 
            set { _itemName = value; OnPropertyChanged("ItemName"); } 
        }



        public double Width
        {
            get { return Location.Width; }
            set
            {
                if (Width == value)
                    return;
                Location = new Rect(Location.X, Location.Y, 
                    Math.Abs(value), Location.Height);
            }
        }

        public double Height
        {
            get { return Location.Height; }
            set
            {
                if (Height == value)
                    return;
                Location = new Rect(Location.X, Location.Y,
                    Location.Width, Math.Abs(value));
            }
        }

        public double X
        {
            get { return Location.X; }
            set
            {
                if (X == value)
                    return;
                Location = new Rect(value, Location.Y,
                    Location.Width, Location.Height);
            }
        }

        public double Y
        {
            get { return Location.Y; }
            set
            {
                if (Y == value)
                    return;
                Location = new Rect(Location.X, value,
                    Location.Width, Location.Height);
            }
        }

        [XmlIgnore()]
        public ILayoutItem Parent { get; set; }

        public AbstractLayoutElement()
        {
            ItemName = "";
            Parent = null;
        }


        #region ILayoutItem Members

        public virtual FrameworkElement[] CreateFrameworkElements(LayoutRenderQuality quality)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region ICloneable Members

        public object Clone()
        {
            return Copy(); 
        }

        #endregion

        #region ICopyable<AbstractLayoutElement> Members

        public virtual ILayoutItem Copy()
        {
            throw new NotImplementedException("Copy not implemented for: " + GetType().Name);
        }

        public ILayoutItem Translate(double dx, double dy)
        {
            ILayoutItem copy = this.Copy();
            copy.Location = new Rect(Location.X + dx, Location.Y + dy,
                 Location.Width, Location.Height);
            return copy;
        }

        #endregion
    }
}
