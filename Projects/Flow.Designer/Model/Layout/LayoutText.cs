﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.ComponentModel;
using System.Windows.Media.Imaging;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Xml.Serialization;
using System.Reflection;
using Flow.Designer.View.Controls;
using Flow.Designer.Lib.Service;
using StructureMap;
using Flow.Lib.Persister;
using Flow.Designer.Model.Project;
using System.Windows.Media.Effects;

namespace Flow.Designer.Model
{
    public class LayoutText : AbstractLayoutElement, INotifyPropertyChanged
    {
        protected string _text = "";
        public string Text
        {
            get { return _text; }
            set
            {
                if (value == _text) return;
                _text = value;
                OnPropertyChanged("Text");
                OnPropertyChanged("RenderText");
            }
        }

        public string RenderText
        {
            get 
            {
                if (!Text.Contains('['))
                    return Text;
                return TextSubstitution.Render(Text); 
            }
        }

        protected FontFamily _fontFamily = new FontFamily("Arial");
        [XmlIgnore()]
        public FontFamily FontFamily
        {
            get { return _fontFamily; }
            set
            {
                if (value == _fontFamily) return;
                _fontFamily = value;
                OnPropertyChanged("FontFamily");
            }
        }

        public string FontFamilyName
        {
            get { return FontFamily.Source; }
            set 
            { 
                FontFamily = new FontFamily(value); 
                OnPropertyChanged("FontFamilyName"); 
            }
        }

        protected Color _color = Colors.Black;
        public Color Color
        {
            get { return _color; }
            set { _color = value; OnPropertyChanged("Color"); OnPropertyChanged("ColorBrush"); }
        }

        protected Color _strokeColor = Colors.White;
        public Color StrokeColor
        {
            get { return _strokeColor; }
            set { _strokeColor = value; OnPropertyChanged("StrokeColor"); OnPropertyChanged("StrokeColorBrush"); }
        }

        protected ushort _strokeThickness = 0;
        public ushort StrokeThickness
        {
            get { return _strokeThickness; }
            set { _strokeThickness = value; OnPropertyChanged("StrokeThickness"); }
        }

        protected double _fontSize = 12;
        public double FontSize
        {
            get { return _fontSize; }
            set { if (_fontSize == value) return; _fontSize = value; OnPropertyChanged("FontSize"); }
        }

        protected bool _autoSizeFont = false;
        public bool AutoSizeFont
        {
            get { return _autoSizeFont; }
            set { if (_autoSizeFont == value) return; _autoSizeFont = value; OnPropertyChanged("AutoSizeFont"); }
        }

        [XmlIgnore()]
        public Brush ColorBrush
        {
            get { return ColorToBrush(Color); }
            set { Color = BrushToColor(value); }
        }

        [XmlIgnore()]
        public Brush StrokeColorBrush
        {
            get { return ColorToBrush(StrokeColor); }
            set { StrokeColor = BrushToColor(value); }
        }

        protected TextAlignment _textAlignment = TextAlignment.Left;
        public TextAlignment TextAlignment
        {
            get { return _textAlignment; }
            set 
            { 
                _textAlignment = value; 
                OnPropertyChanged("TextAlignment"); 
            }
        }

        protected bool _bold = false;
        public bool Bold
        {
            get { return _bold; }
            set
            {
                _bold = value;
                OnPropertyChanged("Bold");
            }
        }

        protected bool _italic = false;
        public bool Italic
        {
            get { return _italic; }
            set
            {
                _italic = value;
                OnPropertyChanged("Italic");
            }
        }

        protected bool _dropshadowEnabled = false;
        public bool DropShadowEnabled
        {
            get { return _dropshadowEnabled; }
            set
            {
                _dropshadowEnabled = value;
                OnPropertyChanged("DropShadowEnabled");
            }
        }

        protected DropShadowEffect _dropshadow;
        public DropShadowEffect DropShadow
        {
            get { return _dropshadow; }
            set { _dropshadow = value; OnPropertyChanged("DropShadow"); }
        }



        public LayoutText()
            : this("") { }

        public LayoutText(string t)
        {
            Text = t;
            Location = new Rect(50, 50, 150, 50);

            DropShadow = new DropShadowEffect();
            DropShadow.Color = Colors.Black;
            DropShadow.Direction = 320;
            DropShadow.ShadowDepth = 8;
            DropShadow.BlurRadius = 5;
            DropShadow.Opacity = 0.4;

            PropertyChanged += LayoutText_PropertyChanged;
        }

        void LayoutText_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Subject")
                OnPropertyChanged("RenderText");

        }

        public override string ToString()
        {
            return "Text: " + Text;
        }

        public override FrameworkElement[] CreateFrameworkElements(LayoutRenderQuality quality)
        {

            LayoutTextControl t = new LayoutTextControl();
            t.BeginInit();
            t.Style = null;
            t.SetBinding(LayoutTextControl.TextProperty, 
                new Binding("RenderText"));
            t.SetBinding(LayoutTextControl.FontProperty, 
                new Binding("FontFamily"));
            t.SetBinding(LayoutTextControl.FillProperty, 
                new Binding("ColorBrush"));
            t.SetBinding(LayoutTextControl.FontSizeProperty, 
                new Binding("FontSize"));
            t.SetBinding(LayoutTextControl.StrokeThicknessProperty, 
                new Binding("StrokeThickness"));
            t.SetBinding(LayoutTextControl.StrokeProperty, 
                new Binding("StrokeColorBrush"));
            t.SetBinding(LayoutTextControl.TextAlignmentProperty, 
                new Binding("TextAlignment"));
            t.SetBinding(LayoutTextControl.BoldProperty,
                new Binding("Bold"));
            t.SetBinding(LayoutTextControl.ItalicProperty,
                new Binding("Italic"));
            t.SetBinding(LayoutTextControl.AutoSizeFontProperty,
                new Binding("AutoSizeFont"));



            t.DropShadowDockPanel = new DockPanel() { LastChildFill = true };
            t.DropShadowDockPanel.Children.Add(t);

            var rotation = new RotateTransform(this.RotateAngle);
            t.DropShadowDockPanel.RenderTransform = rotation;
            t.DropShadowDockPanel.RenderTransformOrigin = new Point(0.5, 0.5);

            this.PropertyChanged += (s, e) =>
            {
                if (e.PropertyName == "RotateAngle")
                    rotation.Angle = RotateAngle;
            };

            try
            {
                t.DataContext = this;
            }
            catch (Exception e)
            {
                //throw e;
            }
            
            t.EndInit();

            return new FrameworkElement[] { (FrameworkElement)t.DropShadowDockPanel };
        }

        protected Brush ColorToBrush(Color c)
        {
            Brush b = new SolidColorBrush(c);
            b.Freeze();
            return b;
        }

        protected Color BrushToColor(Brush b)
        {
            if (!(b is SolidColorBrush))
                throw new Exception("Unknown brush: " + b.GetType().Name);
            return ((SolidColorBrush)b).Color;
        }

        public override ILayoutItem Copy()
        {
            return ObjectSerializer.FromXmlString<LayoutText>(
                ObjectSerializer.ToXmlString(this));
        }


    }
}
