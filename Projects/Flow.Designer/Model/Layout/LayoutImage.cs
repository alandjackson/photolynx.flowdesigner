using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.IO;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using Flow.Designer.View.Controls;
using StructureMap;
using Flow.Designer.Lib.Service;
using Flow.Designer.Model.Project;
using System.Xml.Serialization;
using System.Windows.Data;
using System.Windows.Media.Effects;
using System.Windows.Media;
using Flow.Lib.Persister;
using System.ComponentModel;
using Flow.Designer.Lib.ImageAdjustments.Model;
using Flow.Schema.LinqModel.DataContext;
using Flow.Designer.Lib.ProjectInterop;
using Flow.Lib.Log;
using NLog;
using System.Globalization;
using Flow.Lib;

namespace Flow.Designer.Model
{
    public class LayoutImage : AbstractLayoutElement
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public bool alreadyCropped { get; set; }

        public LayoutImage()
            : this("")
        {
        }

        public LayoutImage(string filename)
        {
            if (!UriParser.IsKnownScheme("pack"))
            {
                System.Windows.Application app = new System.Windows.Application();
            }

            ImageFilename = filename;
            Location = new Rect(50, 50, 50, 50);
            Blur = new BlurEffect();
            Blur.KernelType = KernelType.Gaussian;
            DropShadow = new DropShadowEffect();
            DropShadow.Color = Colors.Black;
            DropShadow.Direction = 320;
            DropShadow.ShadowDepth = 8;
            DropShadow.BlurRadius = 5;
            DropShadow.Opacity = 0.4;
            Sepia = new ShaderEffectLibrary.ColorToneEffect() { Toned = .245, Desaturation = .95 };
            Sepia.Freeze();

            GrayScale = new ShaderEffectLibrary.ColorToneEffect() { Toned = 0, Desaturation = 1 };
            GrayScale.Freeze();

            PropertyChanged += new PropertyChangedEventHandler(
                LayoutImage_PropertyChanged);

            
        }

        void LayoutImage_PropertyChanged(object sender, 
            PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Subject")
            {
                OnPropertyChanged("RenderImageFilename");
                OnPropertyChanged("Crop");
            }

        }

        protected ImageColorMode _imageColorMode = ImageColorMode.Color;
        public ImageColorMode ImageColorMode
        {
            get { return _imageColorMode; }
            set
            {
                _imageColorMode = value;
                OnPropertyChanged("ImageColorMode");
                OnPropertyChanged("IsColor");
                OnPropertyChanged("IsBW");
                OnPropertyChanged("IsSepia");
            }
        }

        [XmlIgnore()]
        public bool IsColor 
        { 
            get { return ImageColorMode == ImageColorMode.Color; }
            set { if (value) ImageColorMode = ImageColorMode.Color; } 
        }
        [XmlIgnore()]
        public bool IsBW
        {
            get { return ImageColorMode == ImageColorMode.BW; }
            set { if (value) ImageColorMode = ImageColorMode.BW; }
        }
        [XmlIgnore()]
        public bool IsSepia
        {
            get { return ImageColorMode == ImageColorMode.Sepia; }
            set { if (value) ImageColorMode = ImageColorMode.Sepia; }
        }

        protected string _imageFilename = "";
        public string ImageFilename 
        { 
            get { return _imageFilename; }
            set 
            { 
                _imageFilename = value; 
                OnPropertyChanged("ImageFilename"); 
                OnPropertyChanged("RenderImageFilename"); 
            } 
        }

        public enum ImageSourceType
        {
            Dynamic,
            Static,
            DynamicAll,
        }
        protected ImageSourceType _imageSource = ImageSourceType.Static;
        public ImageSourceType ImageSource
        {
            get { return _imageSource; }
            set
            {
                _imageSource = value;
                OnPropertyChanged("ImageSource");
                OnPropertyChanged("IsDynamicImage");
                OnPropertyChanged("IsStaticImage");
                OnPropertyChanged("RenderImageFilename");
                OnPropertyChanged("Crop");
            }
        }

        public bool IsStaticImage
        {
            get { return _imageSource == ImageSourceType.Static; }
            set
            {
                if (value)
                    ImageSource = ImageSourceType.Static;
            }
        }
        public bool IsDynamicImage
        {
            get { return _imageSource == ImageSourceType.Dynamic; }
            set
            {
                if (value)
                    ImageSource = ImageSourceType.Dynamic;
            }
        }

        public bool IsAllSubjectImages
        {
            get { return _imageSource == ImageSourceType.DynamicAll; }
            set
            {
                if (value)
                    ImageSource = ImageSourceType.DynamicAll;
            }
        }

        protected bool _useCrop = true;
        public bool UseCrop
        {
            get { return _useCrop; }
            set 
            {
                if (_useCrop == value) return;
                _useCrop = value;
                OnPropertyChanged("UseCrop");
                OnPropertyChanged("RenderImageFilename");
            }
        }

        protected bool _useOrderedImage = false;
        public bool UseOrderedImage
        {
            get { return _useOrderedImage; }
            set { if (_useOrderedImage != value) { _useOrderedImage = value; OnPropertyChanged("UseOrderedImage"); } }
        }

        protected bool _transBG = false;
        public bool TransBG
        {
            get { return _transBG; }
            set
            {
                if (_transBG == value) return;
                _transBG = value;
                OnPropertyChanged("TransBG");
                OnPropertyChanged("RenderImageFilename");
            }
        }

        protected int _subjectImageNdx = 0;
        public int SubjectImageNdx
        {
            get { return _subjectImageNdx; }
            set
            {
                _subjectImageNdx = value;
                CurrentSubjectImageType = "Proof " + (value + 1);
            }
        }

        protected string _currentSubjectImageType = "Proof 1";
        public string CurrentSubjectImageType
        {
            get {return _currentSubjectImageType; }
            set
            {
                _currentSubjectImageType = value;
                 OnPropertyChanged("CurrentSubjectImageType");
                OnPropertyChanged("RenderImageFilename");
                OnPropertyChanged("Crop");
            }
        }

        protected double _opacity = 1;
        public double Opacity
        {
            get { return _opacity; }
            set { _opacity = value; OnPropertyChanged("Opacity"); }
        }

        public SubjectImage SubjectImage
        {
            get
            {
                if (!IsDynamicImage || Subject == null)
                    return null;

                if (Subject != null && Subject.SubjectImages.Count > 0)
                    return Subject.SubjectImages[0];

                if (ImageTypes == null)
                    return null;
                return GetSubjectImageFromType();
            }
        }

        public string RenderImageFilename
        {
            get
            {
                alreadyCropped = false;
                string rImageFilename;

                SubjectImage si;
                if (UseOrderedImage && this.Layout.OrderedSubjectImage != null)
                    si = this.Layout.OrderedSubjectImage;
                else
                    si = SubjectImage;

                //SubjectImage si = SubjectImage;
                if (si != null)
                {
                    rImageFilename = si.ImagePath;
                    //rImageFilename = string.Empty;
                    //alreadyCropped = true;
                    //string gsBackground = si.Subject.FlowProjectDataContext.FlowProject.DefaultGreenScreenBackground;
                    //if (UseOrderedImage)
                    //{
                    //    string orderedbg = si.GetOrderedBackground();
                    //    if (!string.IsNullOrEmpty(orderedbg))
                    //        gsBackground = orderedbg;
                    //}
                    //rImageFilename = si.RenderTempImage(FlowContext.FlowTempDirPath, UseCrop, true, gsBackground, TransBG, 85, 300, false);


                    //if (TransBG == true)
                    //    rImageFilename = si.GetRenderedPngImageFilename(null) ?? ImageFilename;
                    //else
                    //{

                    //    if (UseOrderedImage && this.Layout.OrderedSubjectImage == null)
                    //    {
                    //        if (si.Subject.SubjectOrder.OrderPackages.Count > 0)
                    //        {
                    //            if (si.Subject.SubjectOrder.OrderPackageList.Any(op => op.OrderProducts.Any(prod => prod.OrderProductNodes.Any(opi => opi.SubjectImage.SubjectImageGuid == si.SubjectImageGuid))))
                    //            {
                    //                //temp crop
                    //                string tempcrop = "";
                    //                if (UseCrop)
                    //                    tempcrop = CreateTempCrop(si);

                    //                alreadyCropped = true;
                    //                //tempcrop

                    //                if (string.IsNullOrEmpty(si.GreenScreenSettings) && !si.IsPng)
                    //                    alreadyCropped = false;//only gs or pngs will be cropped in here

                    //                rImageFilename = si.GetRenderedGreenScreenImagePath(null, RenderBgSource.Order, false, tempcrop, false) ?? ImageFilename;
                    //            }
                    //            else
                    //            {
                    //                SubjectImage anotherImage = si.Subject.SubjectOrder.OrderPackages[0].OrderProducts[0].OrderProductNodes[0].SubjectImage;
                    //                string tempcrop = "";
                    //                if (UseCrop)
                    //                    tempcrop = CreateTempCrop(anotherImage);

                    //                //tempcrop

                    //                if (string.IsNullOrEmpty(si.GreenScreenSettings) && !si.IsPng)
                    //                    alreadyCropped = false;//only gs or pngs will be cropped in here
                    //                rImageFilename = anotherImage.GetRenderedGreenScreenImagePath(null, RenderBgSource.Order, false, tempcrop, false) ?? ImageFilename;
                    //                //rImageFilename = anotherImage.GetRenderedImageFilename() ?? anotherImage.ImageFileName;

                    //            }
                    //        }
                    //        else
                    //        {
                    //            //temp crop
                    //            //string tempcrop = "";
                    //            //if (UseCrop)
                    //            //    tempcrop = CreateTempCrop(si);

                    //            alreadyCropped = true;
                    //            //tempcrop

                    //            //if (string.IsNullOrEmpty(si.GreenScreenSettings) && !si.IsPng)
                    //            //    alreadyCropped = false;//only gs or pngs will be cropped in here

                    //            rImageFilename = si.GetRenderedGreenScreenImagePath(null, RenderBgSource.Order, false, tempcrop, true) ?? ImageFilename;
                               

                    //            //rImageFilename = si.GetRenderedImageFilename(true) ?? ImageFilename;//
                    //        }
                    //    }
                    //    else
                    //    {
                    //        //temp crop
                    //        string tempcrop = "";
                    //        if (UseCrop)
                    //            tempcrop = CreateTempCrop(si);

                    //        alreadyCropped = true;
                    //        //tempcrop

                    //        if (string.IsNullOrEmpty(si.GreenScreenSettings) && !si.IsPng)
                    //            alreadyCropped = false;//only gs or pngs will be cropped in here
                    //        rImageFilename = si.GetRenderedGreenScreenImagePath(null, RenderBgSource.GsSettings, false, tempcrop, !UseOrderedImage) ?? ImageFilename;
                    //        //rImageFilename = si.GetRenderedImageFilename(true) ?? ImageFilename;//
                    //    }
                    //}
                    bool useTempImage = false;

                    //this is a total hack for ICON to test if slow renders because of images being on a newtork drive are faster if we copy them local first
                    //begin hack
                    if (File.Exists(Path.Combine(Flow.Lib.FlowContext.FlowAppDataDirPath, "renderlayoutswithtempfiles.txt")))
                        useTempImage = true;
                    if (useTempImage)
                    {
                        string tempImage = Path.Combine(Flow.Lib.FlowContext.FlowTempDirPath, Flow.Lib.Helpers.TicketGenerator.GenerateTicketString(5) + "_" + new FileInfo(rImageFilename).Name);
                        try
                        {
                            if (!File.Exists(tempImage))
                                File.Copy(rImageFilename, tempImage);
                            rImageFilename = tempImage;
                        }
                        catch (Exception e)
                        {
                            //no need to crash, if we cant get a temp image, we will source the original
                        }
                    }
                    //end hack
                }
                else
                    rImageFilename = ImageFilename;

                //return rImageFilename;

                if (rImageFilename.Contains('['))
                    rImageFilename = TextSubstitution.Render(rImageFilename);

                logger.Info("LayoutImage.RenderImageFilename_get: {0}, subject image null: {1}", rImageFilename, si == null);
                return rImageFilename;
            }
        }


        private System.Drawing.Rectangle getCropArea(System.Drawing.Image iSrc, double CropL, double CropT, double CropH, double CropW)
        {
            double width = iSrc.Width;
            double height = iSrc.Height;

            int cropAreaL = (int)(CropL * width);
            int cropAreaT = (int)(CropT * height);
            int cropAreaW = (int)(CropW * width);
            int cropAreaH = (int)(CropH * height);

            if (cropAreaL + cropAreaW > width)
                cropAreaW = (int)width - cropAreaL;
            if (cropAreaT + cropAreaH > height)
                cropAreaH = (int)height - cropAreaT;


            return new System.Drawing.Rectangle(
                cropAreaL,
                cropAreaT,
                cropAreaW,
                cropAreaH
                );
        }
        private static System.Drawing.Image cropImage(System.Drawing.Image img, System.Drawing.Rectangle cropArea)
        {
            System.Drawing.Bitmap bmpImage = new System.Drawing.Bitmap(img);
            System.Drawing.Bitmap bmpCrop = bmpImage.Clone(cropArea,
            bmpImage.PixelFormat);
            bmpImage.Dispose();
            return (System.Drawing.Image)(bmpCrop);
        }

        private string CreateTempCrop(Schema.LinqModel.DataContext.SubjectImage si)
        {
                string newImagePath = si.ImagePathFullRes;
                logger.Info("About to apply a crop");
                System.Drawing.Image img = System.Drawing.Image.FromFile(newImagePath);
                if (si.Orientation == 90)
                    img.RotateFlip(System.Drawing.RotateFlipType.Rotate90FlipNone);
                if (si.Orientation == 180)
                    img.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone);
                if (si.Orientation == 270)
                    img.RotateFlip(System.Drawing.RotateFlipType.Rotate270FlipNone);

                if (((si.CropW > 0) || (si.CropH > 0)))
                    img = cropImage(img, getCropArea(img, si.CropL, si.CropT, si.CropH, si.CropW));
                string tempExportPath = System.IO.Path.Combine(FlowContext.FlowTempDirPath);
                if (!Directory.Exists(tempExportPath))
                    Directory.CreateDirectory(tempExportPath);
                newImagePath = System.IO.Path.Combine(tempExportPath, (new FileInfo(newImagePath)).Name);

                if (!File.Exists(newImagePath))
                {
                    if (newImagePath.EndsWith(".png"))
                        img.Save(newImagePath, System.Drawing.Imaging.ImageFormat.Png);
                    else
                        img.Save(newImagePath, System.Drawing.Imaging.ImageFormat.Jpeg);
                }

                img.Dispose();
                logger.Info("Done apply a crop");
                return newImagePath;
        }

        public Canvas RenderImageFilenameNew
        {
            get
            {
                System.Threading.Thread.Sleep(1000);
                string rImageFilename = "";
                if (IsDynamicImage)
                {
                    SubjectImage si;
                    if (UseOrderedImage && Layout != null && Layout.OrderedSubjectImage != null)
                        si = Layout.OrderedSubjectImage;
                    else
                        si = SubjectImage;

                    //SubjectImage si = SubjectImage;
                    if (si != null)
                    {
                        if (TransBG == true)
                            rImageFilename = si.GetRenderedPngImageFilename(null) ?? ImageFilename;
                        else
                        {
                            rImageFilename = si.GetRenderedImageFilename() ?? ImageFilename;
                        }
                        bool useTempImage = false;

                        //this is a total hack for ICON to test if slow renders because of images being on a newtork drive are faster if we copy them local first
                        //begin hack
                        if (File.Exists(Path.Combine(Flow.Lib.FlowContext.FlowAppDataDirPath, "renderlayoutswithtempfiles.txt")))
                            useTempImage = true;
                        if (useTempImage)
                        {
                            string tempImage = Path.Combine(Flow.Lib.FlowContext.FlowTempDirPath, Flow.Lib.Helpers.TicketGenerator.GenerateTicketString(5) + "_" + new FileInfo(rImageFilename).Name);
                            try
                            {
                                if (!File.Exists(tempImage))
                                    File.Copy(rImageFilename, tempImage);
                                rImageFilename = tempImage;
                            }
                            catch (Exception e)
                            {
                                //no need to crash, if we cant get a temp image, we will source the original
                            }
                        }
                        //end hack
                    }
                    else
                        rImageFilename = ImageFilename;

                    //return rImageFilename;

                    if (rImageFilename.Contains('['))
                        rImageFilename = TextSubstitution.Render(rImageFilename);

                    logger.Info("LayoutImage.RenderImageFilename_get: {0}, subject image null: {1}", rImageFilename, si == null);

                    
                }
                else if(IsAllSubjectImages && Subject != null)
                {
                    Canvas returnCanvas = new Canvas();
                    //
                    string tempAllImages = Path.Combine(Flow.Lib.FlowContext.FlowTempDirPath, Flow.Lib.Helpers.TicketGenerator.GenerateTicketString(5) + "_" + Subject.Fields["Ticket ID"] + ".png");
                    int ImageCount = Subject.SubjectImages.Count;
                    double imgAllowedWidth = this.Width / ImageCount;

                    Canvas mainCanvas = new Canvas();
                    mainCanvas.Width = this.Width;
                    mainCanvas.Height = this.Height;
                    mainCanvas.HorizontalAlignment = HorizontalAlignment.Center;

                    Grid grd = new Grid();
                    grd.Width = this.Width;
                    grd.Height = this.Height;
                    grd.HorizontalAlignment = HorizontalAlignment.Center;
                    mainCanvas.Children.Add(grd);

                    StackPanel mainPnl = new StackPanel();
                    mainPnl.Orientation = Orientation.Horizontal;
                    mainPnl.HorizontalAlignment = HorizontalAlignment.Center;
                    //mainPnl.Width = this.Width;
                    mainPnl.Height = this.Height;
                    grd.Children.Add(mainPnl);

                    double left = 0;

                    double totalW = 0;
                    foreach (SubjectImage si in Subject.SubjectImages)
                    {
                        totalW += si.Thumbnail.Width;
                    }


                    int cnt = 0;
                        foreach (SubjectImage si in Subject.SubjectImages)
                        {

                            double AvailableWPerc = si.Thumbnail.Width / totalW;

                            imgAllowedWidth = this.Width * AvailableWPerc;

                            cnt++;

                            StackPanel pnl = new StackPanel();
                            pnl.Orientation = Orientation.Vertical;
                            pnl.HorizontalAlignment = HorizontalAlignment.Center;
                            




                            //First do Pose#
                           
                            
                            
                                //Label lbl = new Label();
                                //lbl.Foreground = new SolidColorBrush(Colors.Blue);
                                //lbl.Content = "Pose " + cnt;
                                //lbl.Margin = new Thickness(0, 0, 0, 0);
                                //lbl.VerticalAlignment = VerticalAlignment.Top;
                                //lbl.HorizontalAlignment = HorizontalAlignment.Center;
                                ////lbl.HorizontalAlignment = HorizontalAlignment.Center;
                            int fsize = 18;
                            if (ImageCount < 5)
                                fsize = 18;
                            else if (ImageCount < 8)
                                fsize = 14;
                            else if (ImageCount < 12)
                                fsize = 10;
                            else
                                fsize = 7;

                                Image dynamicImageA = new Image();
                                //dynamicImageA.Width = imgAllowedWidth - 16;
                                dynamicImageA.Margin = new Thickness(0, 0, 0, 0);
                                dynamicImageA.VerticalAlignment = VerticalAlignment.Top;
                                dynamicImageA.HorizontalAlignment = HorizontalAlignment.Center;
                                 var visual = new DrawingVisual();
                                using (DrawingContext drawingContext = visual.RenderOpen())
                                {
                                    drawingContext.DrawText(
                                        new FormattedText("Pose " + cnt, CultureInfo.InvariantCulture, FlowDirection.LeftToRight,
                                            new Typeface("Arial"), fsize, Brushes.Blue), new Point(0, 0));
                                }
                                var image = new DrawingImage(visual.Drawing);
                                
                                dynamicImageA.Source = image;

                                pnl.Children.Add(dynamicImageA);
                            

                            

                            //Next Do Image
                            //SubjectImage si = SubjectImage;
                            if (si != null)
                            {
                                if (TransBG == true)
                                    rImageFilename = si.GetRenderedPngImageFilename(null) ?? ImageFilename;
                                else
                                {
                                    rImageFilename = si.GetRenderedImageFilename() ?? ImageFilename;
                                }
                            }
                            else
                                rImageFilename = ImageFilename;

                            //return rImageFilename;

                            if (rImageFilename.Contains('['))
                                rImageFilename = TextSubstitution.Render(rImageFilename);

                            // Create Image and set its width and height  
                            Image dynamicImage = new Image();
                            dynamicImage.Stretch = Stretch.Uniform;
                            dynamicImage.MaxWidth = imgAllowedWidth - 16;
                            dynamicImage.Height = this.Height - 52;
                            dynamicImage.Margin = new Thickness(8, 4, 8, 0);
                            dynamicImage.VerticalAlignment = VerticalAlignment.Center;
                            dynamicImage.HorizontalAlignment = HorizontalAlignment.Center;
                            // Create a BitmapSource  
                            //BitmapImage bitmapt = new BitmapImage();
                            //bitmapt.BeginInit();
                            //bitmapt.UriSource = new Uri(rImageFilename);
                            //bitmapt.EndInit();

                            // Set Image.Source  
                            dynamicImage.Source = si.IntermediateImage;
                            // Add Image to Window  
                            pnl.Children.Add(dynamicImage);
                            
                            
                            //lastly do image name

                            //Label lbl2 = new Label();
                            //lbl2.Foreground = new SolidColorBrush(Colors.Black);
                            //lbl2.Content = si.ImageFileName;
                            //lbl2.Margin = new Thickness(0, 4, 0, 0);
                            //lbl2.VerticalAlignment = VerticalAlignment.Bottom;
                            ////lbl2.TextAlignment = TextAlignment.Center;
                            //lbl2.HorizontalAlignment = HorizontalAlignment.Center;
                            int fsize2 = 10;
                            if (ImageCount < 5)
                                fsize2 = 10;
                            else if (ImageCount < 8)
                                fsize2 = 8;
                            else
                                fsize2 = 6;
                            //pnl.children.add(lbl2);

                            Image dynamicImageB = new Image();
                            //dynamicImageB.Width = imgAllowedWidth - 16;
                            dynamicImageB.Margin = new Thickness(0, 4, 0, 0);
                            dynamicImageB.VerticalAlignment = VerticalAlignment.Bottom;
                            dynamicImageB.HorizontalAlignment = HorizontalAlignment.Center;
                            var visual2 = new DrawingVisual();
                            using (DrawingContext drawingContext2 = visual2.RenderOpen())
                            {
                                drawingContext2.DrawText(
                                    new FormattedText(si.ImageFileName, CultureInfo.InvariantCulture, FlowDirection.LeftToRight,
                                        new Typeface("Arial"), fsize2, Brushes.Black), new Point(0, 0));
                            }
                            var image2 = new DrawingImage(visual2.Drawing);

                            dynamicImageB.Source = image2;

                            pnl.Children.Add(dynamicImageB);


                           

                            mainPnl.Children.Add(pnl);

                            left += imgAllowedWidth;
                        }
                    
                    //save mainCanvas to TempAllImages
                       // ExportToPng(new Uri(tempAllImages), mainCanvas);

                    //set return image
                   // rImageFilename = tempAllImages;
                        returnCanvas = mainCanvas;
                    //}));
                    return returnCanvas;
                
                }
                return null;
                //return rImageFilename;
            }
        }


        public void ExportToPng(Uri path, Canvas surface)
        {
            if (path == null) return;

            // Save current canvas transform
            Transform transform = surface.LayoutTransform;
            // reset current transform (in case it is scaled or rotated)
            surface.LayoutTransform = null;

            // Get the size of canvas
            Size size = new Size(surface.Width, surface.Height);
            // Measure and arrange the surface
            // VERY IMPORTANT
            surface.Measure(size);
            surface.Arrange(new Rect(size));

            // Create a render bitmap and push the surface to it
            RenderTargetBitmap renderBitmap =
              new RenderTargetBitmap(
                (int)size.Width,
                (int)size.Height,
                96d,
                96d,
                PixelFormats.Pbgra32);
            renderBitmap.Render(surface);

            // Create a file stream for saving image
            using (FileStream outStream = new FileStream(path.LocalPath, FileMode.Create))
            {
                // Use png encoder for our data
                PngBitmapEncoder encoder = new PngBitmapEncoder();
                // push the rendered bitmap to it
                encoder.Frames.Add(BitmapFrame.Create(renderBitmap));
                // save the data to the stream
                encoder.Save(outStream);
            }

            // Restore previously saved layout
            surface.LayoutTransform = transform;
        }
        public void ExportToJpg(Uri path, Canvas surface)
        {
            if (path == null) return;

            // Save current canvas transform
            Transform transform = surface.LayoutTransform;
            // reset current transform (in case it is scaled or rotated)
            surface.LayoutTransform = null;

            // Get the size of canvas
            Size size = new Size(surface.Width, surface.Height);
            // Measure and arrange the surface
            // VERY IMPORTANT
            surface.Measure(size);
            surface.Arrange(new Rect(size));

            // Create a render bitmap and push the surface to it
            RenderTargetBitmap renderBitmap =
              new RenderTargetBitmap(
                (int)size.Width,
                (int)size.Height,
                96d,
                96d,
                PixelFormats.Pbgra32);
            renderBitmap.Render(surface);

            // Create a file stream for saving image
            using (FileStream outStream = new FileStream(path.LocalPath, FileMode.Create))
            {
                // Use png encoder for our data
                JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                // push the rendered bitmap to it
                encoder.Frames.Add(BitmapFrame.Create(renderBitmap));
                // save the data to the stream
                encoder.Save(outStream);
            }

            // Restore previously saved layout
            surface.LayoutTransform = transform;
        }
        public Crop Crop
        {
            get
            {
                if (!UseCrop) return new Crop();

                if (this.UseOrderedImage == true && this.OrderedSubjectImage != null)
                    return this.OrderedSubjectImage.GetCrop();

                SubjectImage si = SubjectImage;
                if (si != null)
                    return si.GetCrop();
                return new Crop();
            }
        }

        protected bool _blurEnabled = false;
        public bool BlurEnabled
        {
            get { return _blurEnabled; }
            set { _blurEnabled = value; OnPropertyChanged("BlurEnabled"); }
        }

        protected bool _dropshadowEnabled = false;
        public bool DropShadowEnabled 
        { 
            get { return _dropshadowEnabled; } 
            set 
            { 
                _dropshadowEnabled = value; 
                OnPropertyChanged("DropShadowEnabled"); 
            } 
        }

        protected DropShadowEffect _dropshadow;
        public DropShadowEffect DropShadow
        {
            get { return _dropshadow; }
            set { _dropshadow = value; OnPropertyChanged("DropShadow"); }
        }

        [XmlIgnore()]
        public Effect Sepia { get; set; }

        [XmlIgnore()]
        public Effect GrayScale { get; set; }

        protected BlurEffect _blur;
        public BlurEffect Blur
        {
            get { return _blur; }
            set { _blur = value; OnPropertyChanged("Blur"); }
        }

        protected System.Windows.Media.Color _strokeColor = System.Windows.Media.Colors.White;
        public System.Windows.Media.Color StrokeColor
        {
            get { return _strokeColor; }
            set { _strokeColor = value; OnPropertyChanged("StrokeColor"); OnPropertyChanged("StrokeColorBrush"); }
        }

        protected ushort _strokeThickness = 0;
        public ushort StrokeThickness
        {
            get { return _strokeThickness; }
            set { _strokeThickness = value; OnPropertyChanged("StrokeThickness"); }
        }

        [XmlIgnore()]
        public Brush StrokeColorBrush
        {
            get { return ColorToBrush(StrokeColor); }
            set { StrokeColor = BrushToColor(value); }
        }

        public override string ToString()
        {
            if (!string.IsNullOrEmpty(ImageFilename))
                return "Image: " + new FileInfo(ImageFilename).Name;

            return base.ToString();
        }

        public override FrameworkElement[] CreateFrameworkElements(LayoutRenderQuality quality)
        {
            logger.Info("Creating layout image control");
            LayoutImageControl i = new LayoutImageControl() { Quality = quality };
            i.SetBinding(Image.OpacityProperty, new Binding("Opacity"));

            i.ImageBorder = new Border();
            i.ImageBorder.Child = i;
            i.ImageBorder.BorderBrush = Brushes.Black;
            i.ImageBorder.HorizontalAlignment = HorizontalAlignment.Center;
            i.ImageBorder.VerticalAlignment = VerticalAlignment.Center;
            i.ImageBorder.BorderThickness = new Thickness(3);
            i.ImageBorder.SetBinding(Border.BorderThicknessProperty,
                new Binding("StrokeThickness"));
            i.ImageBorder.SetBinding(Border.BorderBrushProperty,
                new Binding("StrokeColorBrush"));

            i.BwDockPanel = new DockPanel();
            i.BwDockPanel.LastChildFill = true;
            i.BwDockPanel.Children.Add(i.ImageBorder);

            i.SepiaDockPanel = new DockPanel();
            i.SepiaDockPanel.LastChildFill = true;
            i.SepiaDockPanel.Children.Add(i.BwDockPanel);

            i.DropShadowDockPanel = new DockPanel();
            i.DropShadowDockPanel.LastChildFill = true;
            i.DropShadowDockPanel.Children.Add(i.SepiaDockPanel);

            i.BlurDockPanel = new DockPanel();
            i.BlurDockPanel.LastChildFill = true;
            i.BlurDockPanel.Children.Add(i.DropShadowDockPanel);

            try
            {
                i.DataContext = this;
                logger.Info("LayoutImage DataContext was set");
                i.ImageBorder.DataContext = this;
                logger.Info("Image Border DataContext was set");
            }
            catch (Exception e)
            {
                logger.Error("There was an Error setting LayoutImage DataContext in LayoutImage");
                //throw e;
            }



            logger.Info("about to set Rotation for layoutImage");
            var rotation = new RotateTransform(this.RotateAngle);
            i.BlurDockPanel.RenderTransform = rotation;
            i.BlurDockPanel.RenderTransformOrigin = new Point(0.5, 0.5);

            logger.Info("Adding PropertyChanged Event Handler for RotateAngle");
            this.PropertyChanged += (s, e) =>
            {
                if (e.PropertyName == "RotateAngle")
                    rotation.Angle = RotateAngle;
            };


            return new FrameworkElement[] { i.BlurDockPanel };
        }

        public override ILayoutItem Copy()
        {
            LayoutImage li = (LayoutImage) 
                ObjectSerializer.FromXmlString<LayoutImage>(
                ObjectSerializer.ToXmlString(this));
            li.Parent = Parent;
            li.Subject = Subject;
            return li;
        }

        private SubjectImage GetSubjectImageFromType()
        {
            //first of all, if this is supposed to use the Order Image, lets use it and ignore everthing else
            if (UseOrderedImage == true && Layout != null && Layout.OrderedSubjectImage != null)
                return Layout.OrderedSubjectImage;

            if (CurrentSubjectImageType == "Group Photo")
                return Subject.SubjectImages.Where(si => si.IsGroupImage).FirstOrDefault();

            if (CurrentSubjectImageType == "Primary Image")
                return Subject.SubjectImages.Where(si => si.IsPrimary).FirstOrDefault();

            int proofNum;
            if (CurrentSubjectImageType.Length > 6
                && CurrentSubjectImageType.StartsWith("Proof ")
                && int.TryParse(CurrentSubjectImageType.Substring(6), out proofNum))
                return Subject.SubjectImages.Where(si => !si.IsGroupImage).ElementAtOrDefault(proofNum - 1);

            int currentSubjectImagemTypeId = ImageTypes.GetFieldValue(CurrentSubjectImageType);
            if (currentSubjectImagemTypeId < 0)
                return null;

            if ((currentSubjectImagemTypeId > 5555550 && currentSubjectImagemTypeId < 5555599))
            {
                //These are true ImageTypes
                int i = 0;
                if (Subject != null && Subject.SubjectImages != null)
                {
                    foreach (SubjectImage thisOne in Subject.SubjectImages)
                    {
                        if (thisOne.GetImageImageTypeList().Contains(currentSubjectImagemTypeId - 5555550))
                        {
                            return Subject.SubjectImages[i];
                        }
                        i++;
                    }
                }
            }
            return null;
        }

        protected Brush ColorToBrush(System.Windows.Media.Color c)
        {
            Brush b = new SolidColorBrush(c);
            b.Freeze();
            return b;
        }

        protected System.Windows.Media.Color BrushToColor(Brush b)
        {
            if (!(b is SolidColorBrush))
                throw new Exception("Unknown brush: " + b.GetType().Name);
            return ((SolidColorBrush)b).Color;
        }

    }

    public enum ImageColorMode
    {
        Color,
        BW,
        Sepia,
    }


}
