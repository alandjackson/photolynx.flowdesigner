﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Collections.Specialized;
using Flow.Lib.Persister;
using Flow.Designer.Model.Project;
using System.Windows.Media;
using System.Windows;
using Flow.Designer.Lib.ProjectInterop;
using Flow.Designer.Lib.Service;
using Flow.Schema.LinqModel.DataContext;

namespace Flow.Designer.Model
{
    [XmlInclude(typeof(Layout))]
    [XmlInclude(typeof(LayoutImage))]
    [XmlInclude(typeof(LayoutGroup))]
    [XmlInclude(typeof(LayoutText))]
    public class Layout : AbstractLayoutElement 
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public Layout()
        {
            LayoutWidth = 8;
            LayoutHeight = 10;
            LayoutItems = new ObservableCollection<ILayoutItem>();
            this.PropertyChanged += new PropertyChangedEventHandler(Layout_PropertyChanged);
        }

        void Layout_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Subject")
                foreach (ILayoutItem li in LayoutItems)
                    li.Subject = Subject;
            if (e.PropertyName == "DesignerDynamicFields")
                foreach (ILayoutItem li in LayoutItems)
                    li.DesignerDynamicFields = DesignerDynamicFields;
            if (e.PropertyName == "ImageTypes")
                foreach (ILayoutItem li in LayoutItems)
                    li.ImageTypes = ImageTypes;

            foreach (ILayoutItem li in LayoutItems)
                li.Layout = this;
        }

        protected Color _backgroundColor = Colors.White;
        public Color BackgroundColor
        {
            get { return _backgroundColor; }
            set { _backgroundColor = value; OnPropertyChanged("BackgroundColor"); }
        }

        protected int _catalogProductKey;
        public int CatalogProductKey
        {
            get { return _catalogProductKey; }
            set { _catalogProductKey = value; OnPropertyChanged("CatalogProductKey"); }
        }

        public event EventHandler LayoutItemPropertyChaged = null;
        protected void OnLayoutItemPropertyChanged()
        {
            if (LayoutItemPropertyChaged != null)
                LayoutItemPropertyChaged(this, EventArgs.Empty);
        }

        protected double _width;
        public double LayoutWidth { get { return _width; } 
            set { _width = Math.Abs(value); OnPropertyChanged("LayoutWidth"); } }
        protected double _height;
        public double LayoutHeight { get { return _height; } 
            set { _height = Math.Abs(value); OnPropertyChanged("LayoutHeight"); } }

        protected ObservableCollection<ILayoutItem> _layoutItems = null;

        [XmlIgnore()]
        public ObservableCollection<ILayoutItem> LayoutItems
        {
            get { return _layoutItems; }
            set 
            { 
                if (_layoutItems != null)
                    _layoutItems.CollectionChanged -= 
                        new NotifyCollectionChangedEventHandler(
                            _layoutItems_CollectionChanged); 

                _layoutItems = value;
                OnPropertyChanged("LayoutItems");
                _layoutItems.CollectionChanged += 
                    new NotifyCollectionChangedEventHandler(
                        _layoutItems_CollectionChanged);
                RefreshCollectionItemChangedEvents();
            }
        }

        void _layoutItems_CollectionChanged(object sender, 
            NotifyCollectionChangedEventArgs e)
        {
            OnPropertyChanged("LayoutItems");
            RefreshCollectionItemChangedEvents();
        }

        ILayoutItem[] WatchedItems { get; set; }

        /// <summary>
        /// Sets up events to throw changed events for each item in the
        /// collection of layout items
        /// </summary>
        protected void RefreshCollectionItemChangedEvents()
        {
            if (WatchedItems != null)
                foreach (ILayoutItem item in WatchedItems)
                    item.PropertyChanged -=
                        new PropertyChangedEventHandler(item_PropertyChanged);

            WatchedItems = LayoutItems.ToArray();
            foreach (ILayoutItem item in WatchedItems)
                item.PropertyChanged += 
                    new PropertyChangedEventHandler(item_PropertyChanged);
        }

        void item_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnLayoutItemPropertyChanged();
            //OnPropertyChanged("LayoutItems");
            //OnPropertyChanged("LayoutItemProperty");
        }

        public object[] LayoutItemsArray
        {
            get { return LayoutItems.ToArray(); }
            set 
            {
                ILayoutItem[] items = new ILayoutItem[value.Length];
                for (int i = 0; i < value.Length; i++)
                    items[i] = (ILayoutItem)value[i];
                LayoutItems = new ObservableCollection<ILayoutItem>(
                    new List<ILayoutItem>(items)); 
            }
        }

        public override ILayoutItem Copy()
        {
            Layout l =  ObjectSerializer.FromXmlString<Layout>(
                ObjectSerializer.ToXmlString(this));
            l.Subject = Subject;
            l.DesignerDynamicFields = DesignerDynamicFields;
            l.ImageTypes = ImageTypes;
            return l;
        }


        public string GetOriginalFilename(ILayoutSubjectsPreviewService previewService)
        {
            foreach (ILayoutItem item in LayoutItems)
            {
                LayoutImage image = item as LayoutImage;
                if (image == null) continue;

                if (!image.IsDynamicImage) continue;

                if (previewService != null && previewService.IsPreviewEnabled != true)
                    previewService.IsPreviewEnabled = true;

                if (image.SubjectImage != null && image.SubjectImage.ImageFileName != null)
                    return image.SubjectImage.ImageFileName;
            }
            return "";
        }

        public string GetSubjectNameForFileName(ILayoutSubjectsPreviewService previewService)
        {
            foreach (ILayoutItem item in LayoutItems)
            {
                LayoutImage image = item as LayoutImage;
                if (image == null) continue;

                if (!image.IsDynamicImage) continue;

                if (previewService != null && previewService.IsPreviewEnabled != true)
                    previewService.IsPreviewEnabled = true;

                if (image.Subject != null)
                    return image.Subject.Fields["Last Name"].Replace(" ", "") + "_" + image.Subject.Fields["First Name"].Replace(" ", "") + "_" + image.Subject.Fields["Ticket ID"] + ".jpg";
            }
            return "";
        }
        

    }

    public class ThreadSafeLayout
    {
        public string LayoutXml { get; set; }
        public Flow.Designer.Model.Project.Subject Subject { get; set; }
        public Flow.Designer.Model.Project.DesignerDynamicFields DesignerDynamicFields { get; set; }
        public ImageTypes ImageTypes { get; set; }

        public ThreadSafeLayout(Layout l)
        {
            LayoutXml = ObjectSerializer.ToXmlString(l);
            Subject = l.Subject;
            DesignerDynamicFields = l.DesignerDynamicFields;
            ImageTypes = l.ImageTypes;
        }

        public Layout ToLayout()
        {
            Layout l = ObjectSerializer.FromXmlString<Layout>(LayoutXml);
            l.Subject = Subject;
            l.DesignerDynamicFields = DesignerDynamicFields;
            l.ImageTypes = ImageTypes;
            return l;
        }
    }

}
