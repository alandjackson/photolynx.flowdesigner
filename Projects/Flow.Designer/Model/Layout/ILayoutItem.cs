﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.ComponentModel;
using Flow.Designer.Model.Project;
using Flow.Designer.Lib.Service;

namespace Flow.Designer.Model
{
    public interface ICopyable<T>
    {
        T Copy();
    }

    public interface ILayoutItem : INotifyPropertyChanged, ICloneable, ICopyable<ILayoutItem>
    {
        int Dpi { get; set; }
        string ItemName { get; set; }
        Rect Location { get; set; }
        ILayoutItem Parent { get; set; }
        FrameworkElement[] CreateFrameworkElements(LayoutRenderQuality quality);
        ILayoutItem Translate(double dx, double dy);
        Subject Subject { get; set; }
        DesignerDynamicFields DesignerDynamicFields { get; set; }
        ImageTypes ImageTypes { get; set; }
        //KeyValuePair<int,string> CatalogProduct { get; set; }

        Layout Layout { get; set; }

    }
}
