﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Xml.Serialization;

namespace Flow.Designer.Model
{
    [XmlInclude(typeof(LayoutImage))]
    [XmlInclude(typeof(LayoutGroup))]
    [XmlInclude(typeof(LayoutText))]
    public class LayoutGroup : AbstractLayoutElement
    {
        [XmlIgnore()]
        public ObservableCollection<ILayoutItem> Children { get; set; }

        public LayoutGroup()
            : this(new List<ILayoutItem>()) { }


        public LayoutGroup(List<ILayoutItem> c)
        {
            Children = new ObservableCollection<ILayoutItem>(c);
            foreach (ILayoutItem li in Children)
                li.Parent = this;
        }

        public object[] LayoutItemsArray
        {
            get { return Children.ToArray(); }
            set
            {
                ILayoutItem[] items = new ILayoutItem[value.Length];
                for (int i = 0; i < value.Length; i++)
                    items[i] = (ILayoutItem)value[i];
                Children = new ObservableCollection<ILayoutItem>(new List<ILayoutItem>(items));
            }
        }


        public override string ToString()
        {
            if (!string.IsNullOrEmpty(ItemName))
                return ItemName;
            return "Node Group (" + Children.Count + " Nodes)";
        }
    }
}
