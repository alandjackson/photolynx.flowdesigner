﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.Lib.Helpers;
using System.ComponentModel;

namespace Flow.Designer.Model.Designer
{
    public class ObservableDesignerModel
    {
        public event EventHandler<EventArgs<ILayoutItem[]>> SelectedLayoutItemsChanged;
        public event EventHandler<EventArgs<Layout>> LayoutChanged;

        public ObservableDesignerModel(DesignerModel model)
        {
            Model = model;
        }

        protected void OnSelectedLayoutItemsChanged(ILayoutItem[] selectedItems)
        {
            if (SelectedLayoutItemsChanged != null)
                SelectedLayoutItemsChanged(this, new EventArgs<ILayoutItem[]>(selectedItems));
        }

        protected void OnLayoutChanged(Layout layout)
        {
            Layout = layout;
            if (LayoutChanged != null)
                LayoutChanged(this, new EventArgs<Layout>(layout));
        }

        protected DesignerModel _model = null;
        public DesignerModel Model
        {
            get { return _model; }
            set
            {
                if (_model != null)
                    _model.PropertyChanged -=
                        new PropertyChangedEventHandler(_model_PropertyChanged);

                _model = value;
                SelectableLayout = _model.SelectableLayout;
                _model.PropertyChanged +=
                    new PropertyChangedEventHandler(_model_PropertyChanged);
            }
        }

        void _model_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "SelectableLayout")
                SelectableLayout = _model.SelectableLayout;
        }

        protected SelectableLayout _selectableLayout = null;
        public SelectableLayout SelectableLayout
        {
            get { return _selectableLayout; }
            set
            {
                // clear previous events
                if (_selectableLayout != null)
                    _selectableLayout.PropertyChanged -=
                        new PropertyChangedEventHandler(_selectableLayout_PropertyChanged);

                // setup new events
                _selectableLayout = value;
                Layout = _selectableLayout.Layout;
                _selectableLayout.PropertyChanged +=
                    new PropertyChangedEventHandler(_selectableLayout_PropertyChanged);

                if (_selectableLayout != Model.SelectableLayout)
                    Model.SelectableLayout = _selectableLayout;
            }
        }

        void _selectableLayout_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "SelectedItems")
                OnSelectedLayoutItemsChanged(_selectableLayout.SelectedItems.ToArray());
            else if (e.PropertyName == "Layout")
                OnLayoutChanged(SelectableLayout.Layout);
        }

        protected Layout _layout = null;
        public Layout Layout
        {
            get { return _layout; }
            set
            {
                if (_layout == value) return;

                if (_layout != null)
                    _layout.PropertyChanged -= new PropertyChangedEventHandler(_layout_PropertyChanged);

                _layout = value;
                _layout.PropertyChanged += new PropertyChangedEventHandler(_layout_PropertyChanged);

                if (SelectableLayout.Layout != _layout)
                {
                    SelectableLayout.Layout = _layout;
                    SelectableLayout.SelectedItem = null;
                }
            }
        }

        void _layout_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnLayoutChanged(_layout);

        }
    }

}
