﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.Designer.Model.Settings;
using Flow.Designer.Controller;

namespace Flow.Designer.Model.Designer
{
    public class PropertiesPanelModel
    {
        public ILayoutItem LayoutItem { get; set; }
        public GraphicsUnits GraphicsUnits { get; set; }
        public PropertiesController PropertiesController { get; set; }

        public bool ShowNewFeatures { get; set; }

        public PropertiesPanelModel(PropertiesController propertiesController)
        {
            PropertiesController = propertiesController;
            ShowNewFeatures = Flow.Lib.FlowContext.ShowNewFeatures;
            
        }
    }
}
