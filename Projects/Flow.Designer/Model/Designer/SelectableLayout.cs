﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Collections.Specialized;
using Flow.Designer.Model.Designer;

namespace Flow.Designer.Model
{
    public class SelectableLayout : SuspendableNotifyPropertyChanged
    {
        protected Layout _layout;
        public Layout Layout 
        { 
            get { return _layout; }
            set 
            { 
                _layout = value; 
                OnPropertyChanged("Layout"); 
                //SelectedItem = null; 
            } 
        }
        protected ObservableCollection<ILayoutItem> _selectedItems;
        public ObservableCollection<ILayoutItem> SelectedItems 
        {
            get { return _selectedItems; }
            set 
            { 
                _selectedItems = value; OnPropertyChanged("SelectedItems");
                _selectedItems.CollectionChanged += 
                    new NotifyCollectionChangedEventHandler(_selectedItems_CollectionChanged);
            } 
        }
        public void SelectItems(ILayoutItem[] items)
        {
            SelectedItems = new ObservableCollection<ILayoutItem>(new List<ILayoutItem>(items));
        }

        void _selectedItems_CollectionChanged(object sender, 
            NotifyCollectionChangedEventArgs e)
        {
            if (sender == _selectedItems)
                OnPropertyChanged("SelectedItems");
        }

        public ILayoutItem SelectedItem
        {
            get
            {
                if (SelectedItems.Count != 1)
                    return null;
                return SelectedItems[0];
            }
            set
            {
                if (SelectedItems.Count == 1 && SelectedItems[0] == value) return;

                SuspendPropertyNotifying("SelectedItems");
                SelectedItems.Clear();
                if (value != null)
                    SelectedItems.Add(value);
                ResumePropertyNotifying("SelectedItems", true);
            }
        }

        public SelectableLayout()
        {
            SelectedItems = new ObservableCollection<ILayoutItem>();
            Layout = new Layout();
        }

        public void MoveSelectedToFront()
        {
            Layout.SuspendPropertyNotifying("LayoutItems");
            ILayoutItem[] selItems = this.SelectedItems.ToArray();
            for (int i = selItems.Length - 1; i >= 0; i--)
            {
                ILayoutItem selItem = selItems[i];
                Layout.LayoutItems.Remove(selItem);
                Layout.LayoutItems.Insert(0, selItem);
            }
            Layout.ResumePropertyNotifying("LayoutItems", true);
        }

        public void MoveSelectedToBack()
        {
            Layout.SuspendPropertyNotifying("LayoutItems");
            ILayoutItem[] selItems = this.SelectedItems.ToArray();
            foreach (ILayoutItem selItem in selItems)
            {
                Layout.LayoutItems.Remove(selItem);
                Layout.LayoutItems.Add(selItem);
            }
            Layout.ResumePropertyNotifying("LayoutItems", true);
        }


    }
}
