﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.Designer.Helpers.SurfaceUi;
using Flow.Designer.Model.Settings;

namespace Flow.Designer.Model.Designer
{
    public class ViewGridPanelModel
    {
        public GridAdorner GridAdorner { get; set; }
        public GraphicsUnits GraphicsUnits { get; set; }
    }
}
