﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flow.Designer.Model
{
    public class SubjectsDesignerViewModel
    {
        public int SubjectsCount { get; set; }
        public int CurrentRecordNdx { get; set; }
        public Dictionary<string, string> CurrentRecordFields { get; set; }
        public Dictionary<string, string> CurrentRecordImages { get; set; }
        public int CurrentRecordProofCount { get; set; }
    }

    public interface ISubjectsDesignerViewModelRepository
    {
        SubjectsDesignerViewModel GetViewModel();
    }

    public class TestSubjectsDesignerViewModelRepository : ISubjectsDesignerViewModelRepository
    {
        #region ISubjectsDesignerViewModelRepository Members

        public SubjectsDesignerViewModel GetViewModel()
        {
            SubjectsDesignerViewModel model = new SubjectsDesignerViewModel();
            model.SubjectsCount = 100;
            model.CurrentRecordNdx = 10;
            model.CurrentRecordFields = new Dictionary<string, string>();
            model.CurrentRecordFields["First Name"] = "Alan";
            model.CurrentRecordFields["Last Name"] = "Jackson";
            model.CurrentRecordImages["Primary"] = @"C:\Users\Admin\Pictures\Sydney's Camera\DSCF1537.JPG";
            model.CurrentRecordImages["Group"] = @"C:\Users\Admin\Pictures\Sydney's Camera\DSCF1495.JPG";
            model.CurrentRecordImages["Proof1"] = @"C:\Users\Admin\Pictures\Sydney's Camera\DSCF1546.JPG";
            model.CurrentRecordImages["Proof2"] = @"C:\Users\Admin\Pictures\Sydney's Camera\DSCF1547.JPG";
            return model;
        }

        #endregion
    }
}
