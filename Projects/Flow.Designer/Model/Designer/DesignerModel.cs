﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using Flow.Designer.Model.Project;
using Flow.Designer.Model.Settings;

namespace Flow.Designer.Model.Designer
{

    /// <summary>
    /// Acts as the backend model for the entire designer portion of the application
    /// </summary>
    public class DesignerModel : SuspendableNotifyPropertyChanged
    {
        public GraphicsUnits GraphicsUnits { get; set; }

        public event EventHandler LayoutRebindNeeded;
        public void OnLayoutRebindNeeded()
        {
            if (LayoutRebindNeeded != null)
                LayoutRebindNeeded(this, EventArgs.Empty);
        }


        protected SelectableLayout _selectableLayout;
        public SelectableLayout SelectableLayout
        {
            get { return _selectableLayout; }
            set 
            { 
                _selectableLayout = value; 
                _selectableLayout.PropertyChanged 
                    += new PropertyChangedEventHandler(_selectableLayout_PropertyChanged); 
            }
        }

        public override void ResumePropertyNotifying(string propname, bool throwEvent)
        {
            base.ResumePropertyNotifying(propname, throwEvent);
            SelectableLayout.Layout.ResumePropertyNotifying(propname, throwEvent);
        }
        public override void SuspendPropertyNotifying(string propname)
        {
            base.SuspendPropertyNotifying(propname);
            SelectableLayout.Layout.SuspendPropertyNotifying(propname);
        }


        protected Subject _subject;
        public Subject Subject
        {
            get { return _subject; }
            set
            {
                _subject = value;
                OnPropertyChanged("Subject");
                SelectableLayout.Layout.Subject = value;
            }
        }

        protected DesignerDynamicFields _designerDynamicFields;
        public DesignerDynamicFields DesignerDynamicFields
        {
            get { return _designerDynamicFields; }
            set
            {
                _designerDynamicFields = value;
                OnPropertyChanged("DesignerDynamicFields");
                SelectableLayout.Layout.DesignerDynamicFields = value;
            }
        }

        protected ImageTypes _imageTypes;
        public ImageTypes ImageTypes
        {
            get { return _imageTypes; }
            set
            {
                _imageTypes = value;
                OnPropertyChanged("ImageTypes");
                SelectableLayout.Layout.ImageTypes = value;
            }
        }
        void _selectableLayout_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnPropertyChanged("SelectableLayout." + e.PropertyName);
        }

        public DesignerModel()
        {
            SelectableLayout = new SelectableLayout();
        }

    }
}
