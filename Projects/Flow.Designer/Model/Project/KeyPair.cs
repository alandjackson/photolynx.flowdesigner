﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flow.Designer.Model.Project
{
    [Serializable]
    public class KeyPair
    {
        public int Key { get; set; }
        public string Value { get; set; }

        public KeyPair(int key, string value)
        {
            this.Key = key;
            this.Value = value;
        }

        public KeyPair()
        {
        }
    }
}
