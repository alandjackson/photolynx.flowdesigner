﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.Schema.LinqModel.DataContext;

namespace Flow.Designer.Model.Project
{
    public class DesignerDynamicFields
    {
        public Dictionary<string, string> Fields { get; set; }

        public DesignerDynamicFields()
        {
            Fields = new Dictionary<string, string>();
        }

        public string GetFieldValue(string fieldname)
        {
            if (!Fields.ContainsKey(fieldname))
                throw new ArgumentException("Subject does not contain field: " + fieldname);

            return Fields[fieldname];
        }

        public static DesignerDynamicFields Build(FlowProject fp)
        {
            DesignerDynamicFields fields = new Flow.Designer.Model.Project.DesignerDynamicFields();
            if (fp != null)
            {
                var orgFields = fp.Organization.Fields;
                foreach (string key in orgFields.Keys)
                    fields.Fields["Organization." + key] = orgFields[key];
                fields.Fields["Project.Name"] = fp.FlowProjectName;
                fields.Fields["Project.EventName"] = fp.FlowProjectDesc;
                if(fp.DueDate != null)
                    fields.Fields["Project.DueDate"] = ((DateTime)fp.DueDate).ToString("D");
                fields.Fields["Project.PhotographyDate"] = fp.DateCreated.ToString("D");
                fields.Fields["Project.PayableTo"] = fp.PayableTo;
            }
            return fields;
        }

    }
}
