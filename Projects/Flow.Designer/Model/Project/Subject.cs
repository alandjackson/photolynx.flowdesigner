﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.Schema.LinqModel.DataContext;
using System.IO;

namespace Flow.Designer.Model.Project
{
    public class Subject
    {
        public Dictionary<string, string> Fields { get; set; }
        public List<SubjectImage> SubjectImages { get; set; }


        public Subject()
        {
            SubjectImages = new List<SubjectImage>();
            Fields = new Dictionary<string, string>();
        }

        public List<string> Images
        {
            get
            {
                List<string> images = new List<string>();
                foreach (SubjectImage si in SubjectImages)
                    images.Add(si.ImagePathFullRes);
                return images;
            }
            set
            {
                SubjectImages = new List<SubjectImage>();
                if (value == null) return;

                foreach (string image in value)
                {
                    var fi = new FileInfo(image);
                    SubjectImages.Add(new SubjectImage() { ImagePath = fi.FullName, ImageFileName = fi.Name });
                }
            }
        }

        public string GetFieldValue(string fieldname)
        {
            if (!Fields.ContainsKey(fieldname))
                throw new ArgumentException("Subject does not contain field: " + fieldname);

            return Fields[fieldname];
        }

        public string GetImageFilename(int ndx)
        {
            if (ndx < 0 || ndx >= SubjectImages.Count)
                throw new ArgumentException("Image index out of bounds: " + ndx);

            return SubjectImages[ndx].ImagePathFullRes;
        }
    }
}
