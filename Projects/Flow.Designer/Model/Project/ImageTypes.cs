﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.Schema.LinqModel.DataContext;
using Flow.Designer.Controller;

namespace Flow.Designer.Model.Project
{
    public class ImageTypes
    {
        public Dictionary<string, int> Fields { get; set; }

        public int GetFieldValue(string imageType)
        {
            if (!Fields.ContainsKey(imageType))
                throw new ArgumentException("Image Types does not contain imageTypeId: " + imageType);

            return Fields[imageType];
        }

        public static ImageTypes  Build(FlowDesignerDataContext db)
        {
            Flow.Designer.Model.Project.ImageTypes tempIT = new Flow.Designer.Model.Project.ImageTypes();
            Dictionary<string, int> tempDictionary = new Dictionary<string, int>();

            int i = 0;
            while (i++ < 120)
            {
                tempDictionary.Add("Proof " + i, i);
            }
            tempDictionary.Add("Primary Image", 9999999);
            tempDictionary.Add("Group Photo", 8888888);
            foreach (ProjectImageType tempType in db.GetImageTypes())
            {
                tempDictionary.Add(tempType.ProjectImageTypeDesc, 5555550 + tempType.ProjectImageTypeID);
            }
            tempIT.Fields = tempDictionary;
            return tempIT;
        }
    }
}