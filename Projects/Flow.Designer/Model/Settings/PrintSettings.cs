﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.Designer.Properties;
using Flow.Schema.LinqModel.DataContext;
using System.Windows.Controls;
using Flow.Lib.Preferences;
using Flow.Designer.Controller;

namespace Flow.Designer.Model.Settings
{
    public class PrintSettings
    {
        public enum PrintDestiations
        {
            WindowsPrinter = 0,
            Image = 1
        }

        public PrintDestiations PrintDestination { get; set; }
        public string ImageFormat { get; set; }
        public string ImageOutputFolderPath { get; set; }
        public int ImageDPI { get; set; }
        public bool UseSubjectImageNameInFileName { get; set; }
        public bool UseSubjectNameInFileName { get; set; }
        
        public PrintSettings()
        {
            ImageFormat = "jpg";
            ImageOutputFolderPath = "output";
            PrintDestination = PrintDestiations.Image;
        }
    }

    public class PrintSettingsRepository : Flow.Designer.Model.Settings.IPrintSettingsRepository
    {
        Flow.Designer.Properties.Settings DesignerSettings { get; set; }

        protected static System.Windows.Controls.PrintDialog _printDialog = null;
        public static System.Windows.Controls.PrintDialog PrintDialog { get { return (_printDialog = _printDialog ?? new System.Windows.Controls.PrintDialog()); } }

        public PrintSettingsRepository()
        {
            DesignerSettings = Flow.Designer.Properties.Settings.Default;
        }

        public void Save(PrintSettings ps, DesignerPreferences designerPrefs)
        {
            designerPrefs.PrintDestination = (int)ps.PrintDestination;
            designerPrefs.ImageFormat = ps.ImageFormat;
            designerPrefs.ImageDPI = ps.ImageDPI;
            designerPrefs.ImageOutputFolderPath = ps.ImageOutputFolderPath;
            designerPrefs.UseSubjectImageNameInFileName = ps.UseSubjectImageNameInFileName;
            designerPrefs.UseSubjectNameInFileName = ps.UseSubjectNameInFileName;
        }

        public PrintSettings Load()
        {
            return Load(StructureMap.ObjectFactory.GetInstance<FlowDesignerDataContext>().LayoutPreferences.Designer);
        }


        public PrintSettings Load(DesignerPreferences designerPrefs)
        {
            PrintSettings ps = new PrintSettings();

            int tmp = designerPrefs.PrintDestination;
            if (tmp == 0) ps.PrintDestination = PrintSettings.PrintDestiations.WindowsPrinter;
            else if (tmp == 1) ps.PrintDestination = PrintSettings.PrintDestiations.Image;
            ps.ImageFormat = designerPrefs.ImageFormat;
            ps.ImageOutputFolderPath = designerPrefs.ImageOutputFolderPath;
            ps.ImageDPI = designerPrefs.ImageDPI;
            ps.UseSubjectImageNameInFileName = designerPrefs.UseSubjectImageNameInFileName;
            ps.UseSubjectNameInFileName = designerPrefs.UseSubjectNameInFileName;
            return ps;
        }
    }
}
