﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flow.Designer.Model.Settings
{
    public enum GraphicsUnits
    {
        Inches,
        Centimeters,
        Pixels
    }
}
