﻿using Flow.Lib.Preferences;
using System;
namespace Flow.Designer.Model.Settings
{
    public interface IPrintSettingsRepository
    {
        PrintSettings Load();
        PrintSettings Load(DesignerPreferences designerPrefs);
        void Save(PrintSettings ps, DesignerPreferences designerPrefs);
    }
}
