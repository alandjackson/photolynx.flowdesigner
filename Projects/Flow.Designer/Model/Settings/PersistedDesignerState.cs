﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flow.Designer.Model.Settings
{
    /// <summary>
    /// The state of the designer saved between runs
    /// </summary>
    public class PersistedDesignerState
    {
        public double GridSize { get; set; }

        public PersistedDesignerState()
        {
            GridSize = 50;
        }
    }
}
