﻿using Flow.Designer.Lib.ProjectInterop;
using Flow.Designer.Lib.ProjectRepository;
using Flow.Designer.Lib.Service;
using Flow.Designer.Model;
using Flow.Designer.Model.Designer;
using Flow.Designer.Model.Project;
using Flow.Designer.Model.Settings;
using Flow.Designer.View;
using Flow.Designer.View.Dialogs;
using Flow.Helpers;
using Flow.Lib;
using Flow.Lib.Helpers;
using Flow.Lib.ImageQuix;
using Flow.Lib.Persister;
using Flow.Lib.Preferences;
using Flow.Lib.UnitOfWork;
using Flow.Schema.LinqModel.DataContext;
using ICSharpCode.SharpZipLib.Core;
using Microsoft.Win32;
using NetServ.Net.Json;
using StructureMap;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Xml.Linq;

namespace Flow.Designer.Controller
{
    public class FlowDesignerDataContext
    {
        public string LayoutPreferencesXmlFilename { get; set; }

        public FlowDesignerDataContext()
        {
            LayoutPreferences = new LayoutPreferences();
            InitializePreferences();
            LoadPreferences();
        }

        public IEnumerable<ProjectImageType> GetImageTypes()
        {
            //var db = FlowMasterDataContext;
            //if (db != null &&
            //    db.FlowProjectList != null &&
            //    db.FlowProjectList.CurrentItem != null &&
            //    db.FlowProjectList.CurrentItem.ProjectImageTypeList != null)
            //{
            //    foreach (ProjectImageType tempType in db.FlowProjectList.CurrentItem.ProjectImageTypeList)
            //    {
            //        yield return tempType;
            //    }
            //}
            return new ProjectImageType[0];
        }

        public Organization Organization { get; set; } = new Organization();

        // TODO: Persist layout preferences on open/close app
        public LayoutPreferences LayoutPreferences { get; set; }

        /// <summary>
        /// Makes sure the preferences dir/file exists and creates it if it doesn't
        /// </summary>
        public void InitializePreferences()
        {
            var prefsDir = Path.Combine(
                Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData),
                "PhotoLynx", "LayoutDesigner", "Config");
            LayoutPreferencesXmlFilename = Path.Combine(prefsDir, "LayoutPreferences.xml");

            if (!Directory.Exists(prefsDir))
            {
                IOUtil.CreateDirectory(prefsDir, true);
            }

            if (!File.Exists(LayoutPreferencesXmlFilename))
            {
                LayoutPreferences.InitializeFromProServicesConfig();
                SavePreferences();
            }
        }

        public void SavePreferences()
        {
            LayoutPreferences.SaveToXmlFile(LayoutPreferencesXmlFilename);
        }

        public void LoadPreferences()
        {
            LayoutPreferences.LoadFromXmlFile(LayoutPreferencesXmlFilename);
        }
    }
}