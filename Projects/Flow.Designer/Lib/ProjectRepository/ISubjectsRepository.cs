﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.Designer.Model.Project;

namespace Flow.Designer.Lib.ProjectInterop
{

    public interface ISubjectsRepository
    {
        //IQueryable<Subject> GetSubjects();
        Subject GetSubject(int ndx);
        int SubjectsCount { get; }
        string[] GetSubjectFields();
        string[] GetImageTags();


    }

    public class AbstractSubjectsRepository : ISubjectsRepository
    {
        public int SubjectsCount
        {
            get
            {
                return 0;
            }
        }

        public string[] GetImageTags()
        {
            return new string[0];
        }

        public Subject GetSubject(int ndx)
        {
            return new Subject();
        }

        public string[] GetSubjectFields()
        {
            return new string[0];
        }
    }

    //public class BlankSubjectsRepositry
    //{
    //    public 
    //}
}
