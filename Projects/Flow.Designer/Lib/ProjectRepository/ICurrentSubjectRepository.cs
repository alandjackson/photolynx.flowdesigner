﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.Designer.Model.Project;

namespace Flow.Designer.Lib.ProjectRepository
{
    public interface ICurrentSubjectRepository
    {
        event EventHandler SubjectChanged;
        Subject GetSubject();
    }
}
