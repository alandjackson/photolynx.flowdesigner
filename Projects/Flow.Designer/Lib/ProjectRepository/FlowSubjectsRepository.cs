﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.Designer.Lib.ImageAdjustments.Model;
using Flow.Schema.LinqModel.DataContext;

namespace Flow.Designer.Lib.ProjectInterop
{
    class FlowSubjectsRepository
    {
    }

    public static class FlowSubjectAdapterExtensions
    {
        public static Crop GetCrop(this SubjectImage i)
        {
            return new Crop()
            {
                CropH = i.CropH,
                CropW = i.CropW,
                CropT = i.CropT,
                CropL = i.CropL
            };

        }

        public static List<int> GetImageImageTypeList(this SubjectImage subjectImage)
        {
            List<int> assignedImageTypes = new List<int>();
            foreach (SubjectImageType sit in subjectImage.SubjectImageTypeList)
            {
                if (sit.SubjectImageID == subjectImage.SubjectImageID && sit.IsAssigned)
                    assignedImageTypes.Add(sit.ProjectImageTypeID);
            }
            return assignedImageTypes;
        }
    }
}
