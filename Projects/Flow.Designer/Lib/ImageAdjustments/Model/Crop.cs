﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Flow.Designer.Lib.ImageAdjustments.Model
{
    public class Crop
    {
        public double CropL { get; set; }
        public double CropT { get; set; }
        public double CropW { get; set; }
        public double CropH { get; set; }

        public Int32Rect ToInt32Rect(ImageSource iSrc)
        {
            double width = iSrc.Width;
            double height = iSrc.Height;

            if (iSrc is BitmapSource)
            {
                width = ((BitmapSource)iSrc).PixelWidth;
                height = ((BitmapSource)iSrc).PixelHeight;
            }
            int cropAreaL = (int)(CropL * width);
            int cropAreaT = (int)(CropT * height);
            int cropAreaW = (int)(CropW * width);
            int cropAreaH = (int)(CropH * height);

            if (cropAreaL + cropAreaW > width)
                cropAreaW = (int)width - cropAreaL;
            if (cropAreaT + cropAreaH > height)
                cropAreaH = (int)height - cropAreaT;


            return new Int32Rect(
                cropAreaL,
                cropAreaT,
                cropAreaW,
                cropAreaH
                );
        }
    }
}
