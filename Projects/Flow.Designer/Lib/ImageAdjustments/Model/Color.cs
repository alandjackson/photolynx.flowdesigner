﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flow.Designer.Lib.ImageAdjustments.Model
{
    public class Color
    {
        public string ImagePath { get; set; }
        public double R { get; set; }
        public double G { get; set; }
        public double B { get; set; }
        public int Brightness { get; set; } //-255 ... 255
        public double Contrast { get; set; }//-1 ... inf(maybe 5)
    }
}
