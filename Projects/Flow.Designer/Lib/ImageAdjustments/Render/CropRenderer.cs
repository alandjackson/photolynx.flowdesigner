﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.Designer.Lib.ImageAdjustments.Model;
using System.Windows.Media.Imaging;
using System.Windows;

namespace Flow.Designer.Lib.Render
{
    public static class CropRenderer
    {
        public static BitmapSource ApplyToImage(this Crop c, BitmapSource bs)
        {
            return new CroppedBitmap(bs, new Int32Rect((int)c.CropL, (int)c.CropT, (int)c.CropW, (int)c.CropH));
        }
    }
}
