﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Imaging;
using System.Windows.Media;

namespace Flow.Designer.Lib.ImageAdjustments.Render
{
    public class GrayscaleRenderer
    {
        public BitmapSource Apply(BitmapSource bs)
        {
            FormatConvertedBitmap f = new FormatConvertedBitmap();
            f.BeginInit();
            f.DestinationFormat = PixelFormats.Gray32Float;
            f.EndInit();
            return f;
        }
    }
}
