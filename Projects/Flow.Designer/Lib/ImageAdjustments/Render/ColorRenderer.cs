﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.Designer.Lib.ImageAdjustments.Model;
using System.Windows.Media.Imaging;
using Flow.Designer.Lib.ImageAdjustments.WIC;

namespace Flow.Designer.Lib.ImageAdjustments.Render
{
    public static class ColorRenderer
    {

        public static BitmapImage Apply(this Color c, BitmapImage image)
        {
            try
            {
                WICBitmap wicBitmap = new WICBitmap(image);   // WICBitmap typ
                WICBitmapLock bitmapLock = wicBitmap.Lock();

                WICBitmapBuffer bitmapBuffer = bitmapLock.Data;
                unsafe
                {
                    Int32* pStart = (Int32*)bitmapBuffer.Buffer.ToPointer();
                    Int32* pEnd = pStart + bitmapBuffer.Size / sizeof(Int32);
                    for (; pStart != pEnd; ++pStart)
                    {
                        //           RRGGBB
                        int pixelColor = *pStart;
                        int blue = Math.Min(Math.Max((int)(((pixelColor & 0x0000FF) * c.B + c.Brightness) * c.Contrast), 0), 255);
                        pixelColor >>= 8;
                        int green = Math.Min(Math.Max((int)(((pixelColor & 0x0000FF) * c.G + c.Brightness) * c.Contrast), 0), 255);
                        pixelColor >>= 8;
                        int red = Math.Min(Math.Max((int)(((pixelColor & 0x0000FF) * c.R + c.Brightness) * c.Contrast), 0), 255);

                        *pStart = (red << 16) + (green << 8) + blue;
                    }
                }
                bitmapLock.Dispose();
                return image;
            }
            catch (Exception)
            {
                return null;
            }

        }

    }
}
