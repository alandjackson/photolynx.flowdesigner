﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using Flow.Designer.Model;
using System.ComponentModel;
using System.Windows.Controls;

namespace Flow.Designer.Lib.SurfaceUi
{
    public class LayoutLocationBinding
    {
        protected FrameworkElement FrameworkElement { get; set; }
        protected ILayoutItem LayoutElement { get; set; }

        public void SetBinding(FrameworkElement fe, ILayoutItem le)
        {
            FrameworkElement = fe;
            LayoutElement = le;

            FrameworkElement.LayoutUpdated += 
                new EventHandler(FrameworkElement_LayoutUpdated);
            LayoutElement.PropertyChanged += 
                new PropertyChangedEventHandler(layoutElement_PropertyChanged);
        }

        bool _updatingProperty = false;
        void layoutElement_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName != "Location")
                return;

            if (_updatingProperty)
                return;
            _updatingProperty = true;

            Canvas.SetLeft(FrameworkElement, LayoutElement.Location.X);
            Canvas.SetTop(FrameworkElement, LayoutElement.Location.Y);
            FrameworkElement.Width = LayoutElement.Location.Width;
            FrameworkElement.Height = LayoutElement.Location.Height;

            _updatingProperty = false;
        }

        void FrameworkElement_LayoutUpdated(object sender, EventArgs e)
        {
            if (_updatingProperty)
                return;
            _updatingProperty = true;

            LayoutElement.Location = new Rect(Canvas.GetLeft(FrameworkElement), Canvas.GetTop(FrameworkElement),
                FrameworkElement.Width, FrameworkElement.Height);

            _updatingProperty = false;
        }
    }

}
