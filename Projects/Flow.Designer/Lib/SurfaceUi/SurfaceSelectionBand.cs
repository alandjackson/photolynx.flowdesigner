﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.Designer.View;
using System.Windows.Shapes;
using System.Windows.Media;
using System.Windows;
using System.Windows.Controls;
using Flow.Lib.Helpers;

namespace Flow.Designer.Lib.SurfaceUi
{
    public class SurfaceSelectionBand
    {
        public event EventHandler<EventArgs<Rect>> AreaSelected;
        protected void OnAreaSelected(Rect rect)
        {
            if (AreaSelected != null)
                AreaSelected(this, new EventArgs<Rect>(rect));
        }

        protected Rectangle DragRectangle { get; set; }
        protected bool _dragInProgress = false;
        protected Point _startPoint;

        //protected Surface _surface;
        //protected Surface Surface
        //{
        //    get { return _surface; }
        //    set
        //    {
        //        _surface = value;
        //        _surface.uxLayoutCanvas.MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(uxLayoutCanvas_MouseLeftButtonDown);
        //        _surface.MouseLeftButtonUp += new System.Windows.Input.MouseButtonEventHandler(uxLayoutCanvas_MouseLeftButtonUp);
        //        _surface.MouseMove += new System.Windows.Input.MouseEventHandler(uxLayoutCanvas_MouseMove);
        //    }
        //}

        protected Panel SelectionControl { get; set; }
        protected FrameworkElement BoundsControl { get; set; }

        public SurfaceSelectionBand(Surface s) : this(s.uxLayoutCanvas, s)
        {

        }

        public SurfaceSelectionBand(Panel selControl, FrameworkElement bControl)
        {
            if (selControl != null)
            {
                SelectionControl = selControl;
                SelectionControl.MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(uxLayoutCanvas_MouseLeftButtonDown);
            }
            if (bControl != null)
            {
                BoundsControl = bControl;
                BoundsControl.MouseLeftButtonUp += new System.Windows.Input.MouseButtonEventHandler(uxLayoutCanvas_MouseLeftButtonUp);
                BoundsControl.MouseMove += new System.Windows.Input.MouseEventHandler(uxLayoutCanvas_MouseMove);
            }

            //Surface = s;

            DragRectangle = new Rectangle();
            DragRectangle.Visibility = System.Windows.Visibility.Hidden;
            DragRectangle.RadiusX = 3;
            DragRectangle.RadiusY = 3;
            DragRectangle.Stroke = new SolidColorBrush(Colors.Black);
            DragRectangle.StrokeThickness = 1;
            DragRectangle.Fill = new SolidColorBrush(Colors.Blue);
            DragRectangle.Fill.Opacity = .20;

            if (selControl != null)
            {
                selControl.Children.Add(DragRectangle);
            }

            //Surface.uxLayoutCanvas.Children.Add(DragRectangle);

        }

        void uxLayoutCanvas_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            // BUG: Max calculations mess up the rect when moving negative
            // TODO: bound the rect to the top/left edges of the canvas
            if (_dragInProgress)
            {
                Point p = e.GetPosition(SelectionControl);

                double w = p.X - _startPoint.X;
                double h = p.Y - _startPoint.Y;
                double x = _startPoint.X;
                double y = _startPoint.Y;
                double wMax = SelectionControl.ActualWidth - x;
                double hMax = SelectionControl.ActualHeight - y;
                if (w < 0)
                {
                    x = p.X;
                    w = -w;
                }
                if (w > wMax)
                    w = wMax;
                if (h < 0)
                {
                    y = p.Y;
                    h = -h;
                }
                if (h > hMax)
                    h = hMax;                

                Canvas.SetLeft(DragRectangle, x);
                Canvas.SetTop(DragRectangle, y);
                DragRectangle.Width = w;
                DragRectangle.Height = h;
            }
        }

        void uxLayoutCanvas_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (!_dragInProgress)
                return;

            _dragInProgress = false;
            DragRectangle.Visibility = Visibility.Hidden;

            OnAreaSelected(new Rect(Canvas.GetLeft(DragRectangle), Canvas.GetTop(DragRectangle),
                DragRectangle.Width, DragRectangle.Height));
        }

        void uxLayoutCanvas_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            _dragInProgress = true;
            _startPoint = e.GetPosition(SelectionControl);
            Canvas.SetLeft(DragRectangle, _startPoint.X);
            Canvas.SetTop(DragRectangle, _startPoint.Y);
            DragRectangle.Width = 0;
            DragRectangle.Height = 0;
            DragRectangle.Visibility = Visibility.Visible;
        }



        void uxLayoutCanvas_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
        }

    }
}
