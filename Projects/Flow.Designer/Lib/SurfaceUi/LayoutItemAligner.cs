﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.Designer.Model;
using System.Windows;

namespace Flow.Designer.Lib.SurfaceUi
{
    public class LayoutItemAligner
    {
        public void AlignLefts(ILayoutItem [] items)
        {
            AlignItems(items, (Rect baseLocation, Rect itemLocation) =>
            {
                return new Rect(baseLocation.X,
                    itemLocation.Y, itemLocation.Width, itemLocation.Height);
            });
        }

        public void AlignRights(ILayoutItem[] items)
        {
            AlignItems(items, (Rect baseLocation, Rect itemLocation) => 
            { 
                return new Rect(
                    baseLocation.X + baseLocation.Width - itemLocation.Width,
                    itemLocation.Y, itemLocation.Width, itemLocation.Height);
            });
        }

        public void AlignHMiddles(ILayoutItem[] items)
        {
            AlignItems(items, (Rect baseLocation, Rect itemLocation) =>
            {
                return new Rect(
                    baseLocation.X + baseLocation.Width / 2 - itemLocation.Width / 2,
                    itemLocation.Y, itemLocation.Width, itemLocation.Height);
            });
        }

        public void AlignTops(ILayoutItem[] items)
        {
            AlignItems(items, (Rect baseLocation, Rect itemLocation) =>
            {
                return new Rect(itemLocation.X,
                    baseLocation.Y, itemLocation.Width, itemLocation.Height);
            });
        }

        public void AlignBottoms(ILayoutItem[] items)
        {
            AlignItems(items, (Rect baseLocation, Rect itemLocation) =>
            {
                return new Rect(itemLocation.X,
                    baseLocation.Y + baseLocation.Height - itemLocation.Height, itemLocation.Width, itemLocation.Height);
            });
        }

        public void AlignVMiddles(ILayoutItem[] items)
        {
            AlignItems(items, (Rect baseLocation, Rect itemLocation) =>
            {
                return new Rect(itemLocation.X,
                    baseLocation.Y + baseLocation.Height / 2 - itemLocation.Height / 2, itemLocation.Width, itemLocation.Height);
            });
        }


        public delegate Rect AlignmentProcessDelegate(Rect baseLocation, Rect itemLocation);

        void AlignItems(ILayoutItem[] items, AlignmentProcessDelegate alignProcess)
        {
            if (items == null || items.Length <= 1)
                return;

            ILayoutItem baseItem = items[items.Length - 1];
            foreach (ILayoutItem item in items)
            {
                if (item == baseItem)
                    continue;
                item.Location = alignProcess(baseItem.Location, item.Location);
            }
        }

    }
}
