﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using Flow.Designer.Model;
using System.Windows.Controls;
using System.Windows.Data;

namespace Flow.Designer.Helpers.SurfaceUi
{
    public class TextFrameworkElementCreator
    {
        public FrameworkElement CreateElement(LayoutText text)
        {
            Label tb = new Label();
            tb.SetBinding(Label.ContentProperty, new Binding("Text"));

            //TextBox nameTextBox = new TextBox();
            //Binding nameTextBinding = new Binding("Name");
            //nameTextBinding.Source = person;
            //nameTextBox.SetBinding(TextBox.TextProperty, nameTextBinding);


            return tb;
        }
    }
}
