﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.Designer.Model.Project;

namespace Flow.Designer.Lib.Filters
{
    public static class SubjectFilters
    {
        /// <summary>
        /// Filters The query by ProductID
        /// </summary>
        public static Subject ElementAt(this IQueryable<Subject> qry, long ndx)
        {
            return qry.ElementAt(ndx);
            //return qry.ToList()[(int)ndx];
        }
    }
}
