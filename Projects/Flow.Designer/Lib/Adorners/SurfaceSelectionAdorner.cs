﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Documents;
using Flow.Designer.Helpers.SurfaceUi;
using System.Windows;
using Flow.Designer.Model;
using Flow.Designer.View;
using System.Windows.Controls;
using Flow.Designer.Lib.Service.SnapToService;

namespace Flow.Designer.Lib.SurfaceUi
{
    public class SurfaceSelectionAdorner
    {
        ILayoutItem[] SelectedItems = null;
        protected bool _dragInProgress = false;
        protected bool _moveInProgress = false;
        protected UIElement _moveElement = null;
        protected bool _sizeInProgress = false;
        protected FrameworkElement _sizeElement = null;
        protected Point _startPoint;
        Surface Surface;
        public ISnapToService MoveSnapToService { get; set; }
        public ISnapToService SizeSnapToService { get; set; }

        public SurfaceSelectionAdorner(Surface s) 
            : this (s, null, null) { }

        public SurfaceSelectionAdorner(Surface s, ISnapToService msts, ISnapToService ssts)
        {
            MoveSnapToService = msts ?? new NullSnapToService();
            SizeSnapToService = ssts ?? new NullSnapToService();
            s.MouseMove += new System.Windows.Input.MouseEventHandler(MouseMove);
            s.MouseLeftButtonUp += new System.Windows.Input.MouseButtonEventHandler(MouseLeftButtonUp);
            Surface = s;
        }

        void MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if (!_dragInProgress)
                return;

            if (_moveInProgress)
            {
                Point endPoint = e.GetPosition(Surface.uxLayoutCanvas);
                double dx = endPoint.X - _startPoint.X;
                double dy = endPoint.Y - _startPoint.Y;
                double x = Canvas.GetLeft(_moveElement);
                double y = Canvas.GetTop(_moveElement);

                Canvas.SetLeft(_moveElement, MoveSnapToService.SnapX(x + dx));
                Canvas.SetTop(_moveElement, MoveSnapToService.SnapY(y + dy));

                // set new start point
                _startPoint = endPoint;
            }
            else if (_sizeInProgress)
            {
                Point endPoint = e.GetPosition(Surface.uxLayoutCanvas);
                double dx = endPoint.X - _startPoint.X;
                double dy = endPoint.Y - _startPoint.Y;
                double x = _sizeElement.Width;
                double y = _sizeElement.Height;

                _sizeElement.Width = Math.Max(1, SizeSnapToService.SnapX(x + dx));
                _sizeElement.Height = Math.Max(1, SizeSnapToService.SnapY(y + dy));

                // set new start point
                _startPoint = endPoint;
            }
        }

        void MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            _dragInProgress = true;
            _startPoint = e.GetPosition(Surface.uxLayoutCanvas);

            if (sender is RectangleAdorner)
            {
                _moveInProgress = true;
                _moveElement = (sender as RectangleAdorner).AdornedElement;
            }
            else if (sender is CircleAdorner)
            {
                _sizeInProgress = true;
                _sizeElement = (FrameworkElement) (sender as CircleAdorner).AdornedElement;
            }
        }

        void MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            _dragInProgress = false;
            _moveInProgress = false;
            _sizeInProgress = false;
        }


        public void RefreshSelectedElements(ILayoutItem[] newSelectedItems, 
            Dictionary<ILayoutItem, FrameworkElement[]> surfaceLayoutItems)
        {
            List<ILayoutItem> adornedSelectedItems = new List<ILayoutItem>();

            // remove the previous adorners
            if (SelectedItems != null)
                foreach (ILayoutItem li in SelectedItems)
                    if (surfaceLayoutItems.ContainsKey(li))
                        foreach (FrameworkElement fe in surfaceLayoutItems[li])
                            RemoveSelectionAdorners(fe);

            // add in the new adorners
            SelectedItems = newSelectedItems;
            if (newSelectedItems != null)
                foreach (ILayoutItem li in SelectedItems)
                    if (surfaceLayoutItems.ContainsKey(li))
                        foreach (FrameworkElement fe in surfaceLayoutItems[li])
                            AddSelectionAdorners(fe);
        }

        protected void AddSelectionAdorners(FrameworkElement i)
        {
            AdornerLayer layer = AdornerLayer.GetAdornerLayer(i);
            if (layer == null)
                return;

            RectangleAdorner ra = new RectangleAdorner(i);
            layer.Add(ra);
            ra.MouseLeftButtonUp += new System.Windows.Input.MouseButtonEventHandler(MouseLeftButtonUp);
            ra.MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(MouseLeftButtonDown);
            ra.MouseMove += new System.Windows.Input.MouseEventHandler(MouseMove);


            CircleAdorner a = new CircleAdorner(i);
            layer.Add(a);
            a.MouseLeftButtonUp += new System.Windows.Input.MouseButtonEventHandler(MouseLeftButtonUp);
            a.MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(MouseLeftButtonDown);
            a.MouseMove += new System.Windows.Input.MouseEventHandler(MouseMove);

            ra.RenderTransform = i.RenderTransform;
            a.RenderTransform = i.RenderTransform;
        }

        protected void RemoveSelectionAdorners(FrameworkElement i)
        {
            AdornerLayer layer = AdornerLayer.GetAdornerLayer(i);
            if (layer == null || layer.GetAdorners(i) == null)
                return;
            foreach (Adorner a in layer.GetAdorners(i))
            {
                a.MouseLeftButtonUp -= new System.Windows.Input.MouseButtonEventHandler(MouseLeftButtonUp);
                a.MouseLeftButtonDown -= new System.Windows.Input.MouseButtonEventHandler(MouseLeftButtonDown);
                a.MouseMove -= new System.Windows.Input.MouseEventHandler(MouseMove);
                layer.Remove(a);
            }
        }
    }
}
