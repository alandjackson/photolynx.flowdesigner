using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Documents;
using System.Windows;
using System.Windows.Media;
using Flow.Designer.Lib.Service.SnapToService;
using System.ComponentModel;


namespace Flow.Designer.Helpers.SurfaceUi
{
    public class GridAdornerSnapLocationProvider : ISnapLocationProvider
    {
        GridAdorner GridAdorner { get; set; }

        public GridAdornerSnapLocationProvider(GridAdorner adorner)
        {
            GridAdorner = adorner;
        }

        public double[] XSnapLocations
        {
            get { return GridAdorner.XGridLines; }
        }

        public double[] YSnapLocations
        {
            get { return GridAdorner.YGridLines; }
        }
    }

    // Adorners must subclass the abstract base class Adorner.
    public class GridAdorner : Adorner, INotifyPropertyChanged
    {
        public static double MIN_GRID_SIZE = 0.01;

        protected bool _isGridEnabled;
        public bool IsGridEnabled { get { return _isGridEnabled; } set { _isGridEnabled = value; InvalidateVisual(); OnPropertyChanged("IsGridEnabled"); } }
        protected double _gridSize;
        public double GridSize 
        { 
            get { return _gridSize; } 
            set 
            {
                if (value < MIN_GRID_SIZE)
                    throw new ArgumentOutOfRangeException("GridSize", "Grid size must be greater than " + MIN_GRID_SIZE);
                if (_gridSize == value)
                    return;
                _gridSize = value; 
                InvalidateVisual(); 
                OnPropertyChanged("GridSize"); 
            } 
        }

        public double ZoomValue { get; set; }

        // Be sure to call the base class constructor.
        public GridAdorner(UIElement adornedElement)
            : base(adornedElement)
        {
            IsGridEnabled = true;
            GridSize = 20;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }


        protected Rect LastDrawRect { get; set; }
        public double[] XGridLines
        {
            get
            {
                List<double> lines = new List<double>();
                if (IsGridEnabled)
                    for (double x = LastDrawRect.Left; x <= LastDrawRect.Right; x += GridSize)
                        lines.Add(x);
                return lines.ToArray();

            }
        }
        public double[] YGridLines
        {
            get
            {
                List<double> lines = new List<double>();
                if (IsGridEnabled)
                    for (double y = LastDrawRect.Top; y <= LastDrawRect.Bottom; y += GridSize)
                        lines.Add(y);
                return lines.ToArray();

            }
        }

        // A common way to implement an adorner's rendering behavior is to override the 
        // OnRender method, which is called by the layout system as part of a rendering 
        // pass.
        protected override void OnRender(DrawingContext drawingContext)
        {
            if (!IsGridEnabled)
                return;



            // get the area to draw the grid on and save it
            Rect drawRect = new Rect(this.AdornedElement.DesiredSize);
            FrameworkElement fe = AdornedElement as FrameworkElement;
            if (fe != null)
                drawRect = new Rect(new Size(fe.ActualWidth, fe.ActualHeight));
            LastDrawRect = drawRect;


            // Some arbitrary drawing implements.
            SolidColorBrush renderBrush = new SolidColorBrush(Colors.CadetBlue);
            renderBrush.Opacity = 0.0;
            Pen renderPen = new Pen(new SolidColorBrush(Color.FromArgb(255, 115, 115, 115)), .25 / ZoomValue);


            //for (double y = drawRect.Top; y <= drawRect.Bottom; y += GridSize)
            foreach (double y in YGridLines)
                drawingContext.DrawLine(renderPen, new Point(drawRect.Left, y), new Point(drawRect.Right, y));
            //for (double x = drawRect.Left; x <= drawRect.Right; x += GridSize)
            foreach (double x in XGridLines)
                drawingContext.DrawLine(renderPen, new Point(x, drawRect.Top), new Point(x, drawRect.Bottom));

        }
    }
}
