﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Documents;
using System.Windows;
using System.Windows.Media;

namespace Flow.Designer.Helpers.SurfaceUi
{
    // Adorners must subclass the abstract base class Adorner.
    public class CircleAdorner : Adorner
    {
        // Be sure to call the base class constructor.
        public CircleAdorner(UIElement adornedElement)
            : base(adornedElement)
        {
        }

        // A common way to implement an adorner's rendering behavior is to override the 
        // OnRender method, which is called by the layout system as part of a rendering 
        // pass.
        protected override void OnRender(DrawingContext drawingContext)
        {
            Rect adornedElementRect = new Rect(this.AdornedElement.DesiredSize);

            // Some arbitrary drawing implements.
            SolidColorBrush renderBrush = new SolidColorBrush(Colors.Orange);
            renderBrush.Opacity = 1.0;
            Pen renderPen = new Pen(new SolidColorBrush(Color.FromArgb(255,100,149,237)),.75);
            double renderRadius = 5.0;

            // Draw a circle at each corner.
            drawingContext.DrawEllipse(renderBrush, renderPen, 
                adornedElementRect.TopLeft, renderRadius, renderRadius);
            drawingContext.DrawEllipse(renderBrush, renderPen, 
                adornedElementRect.TopRight, renderRadius, renderRadius);
            drawingContext.DrawEllipse(renderBrush, renderPen, 
                adornedElementRect.BottomLeft, renderRadius, renderRadius);
            drawingContext.DrawEllipse(renderBrush, renderPen, 
                adornedElementRect.BottomRight, renderRadius, renderRadius);
        }
    }
}
