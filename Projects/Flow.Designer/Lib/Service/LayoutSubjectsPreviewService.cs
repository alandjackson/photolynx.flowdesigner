﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.Designer.Model.Project;
using StructureMap;
using Flow.Designer.Model;
using Flow.Designer.Lib.Service;
using Flow.Designer.Lib.ProjectRepository;

namespace Flow.Designer.Lib.ProjectInterop
{
    public class LayoutSubjectsPreviewService : 
        AbstractNotifyPropertyChanged, 
        ICurrentSubjectRepository, 
        ILayoutSubjectsPreviewService
    {
        public ISubjectsRepository Repository { get; set; }

        public LayoutSubjectsPreviewService(ISubjectsRepository r)
        {
            Repository = r;
        }

        public void UpdateRecordsCount()
        {
            OnPropertyChanged("RecordsCount");
        }

        public int RecordsCount
        {
            get { return Repository.SubjectsCount; }
        }

        protected bool _isPreviewEnabled = false;
        public bool IsPreviewEnabled
        {
            get { return _isPreviewEnabled; }
            set 
            { 
                _isPreviewEnabled = value; 
                OnPropertyChanged("IsPreviewEnabled"); 
                OnSubjectChanged(); 
            }
        }

        protected int _curRecNdx = 1;
        public int CurrentRecordNdx
        {
            get { return _curRecNdx; }
            set
            {
                _curRecNdx = value; OnPropertyChanged("CurrentRecordNdx"); 
                OnSubjectChanged();
            }
        }

        public void MoveFirst()
        {
            CurrentRecordNdx = 1;
        }

        public void MovePrevious()
        {
            CurrentRecordNdx = Math.Max(CurrentRecordNdx - 1, 1);
        }

        public void MoveNext()
        {
            CurrentRecordNdx = Math.Min(CurrentRecordNdx + 1, RecordsCount);
        }

        public void MoveLast()
        {
            CurrentRecordNdx = RecordsCount;
        }

        public Subject GetCurrentSubject()
        {
            return Repository.GetSubject(CurrentRecordNdx - 1);
        }

        #region ISubjectRepository Members

        public event EventHandler SubjectChanged;
        protected void OnSubjectChanged()
        {
            if (SubjectChanged != null)
                SubjectChanged(this, EventArgs.Empty);
        }

        Subject ICurrentSubjectRepository.GetSubject()
        {
            if (IsPreviewEnabled)
                return GetCurrentSubject();
            else
                return new Subject();
        }

        #region ISubjectsRepository Members

        public Flow.Designer.Model.Project.Subject GetSubject(int ndx)
        {
            return Repository.GetSubject(ndx);
        }

        public int SubjectsCount
        {
            get { return Repository.SubjectsCount; }
        }

        public string[] GetSubjectFields()
        {
            return Repository.GetSubjectFields();
        }

        public string[] GetImageTags()
        {
            return Repository.GetImageTags();
        }

        #endregion


        #endregion

    }
}
