﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using Flow.Designer.Model;
using System.Windows.Media;
using System.Windows;
using Flow.Lib.Log;
using NLog;

namespace Flow.Designer.Lib.Service
{
    public class LayoutCanvasAdapter
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public Canvas LayoutToCanvas(Layout layout, int dpi, LayoutRenderQuality quality)
        {
            logger.Info("Converting layout to canvas, dpi: {0}, quality: {1}", dpi, quality);

            Canvas canvas = new Canvas();
            canvas.BeginInit();
            canvas.Width = layout.LayoutWidth * dpi;
            canvas.Height = layout.LayoutHeight * dpi;
            canvas.Background = new SolidColorBrush(layout.BackgroundColor);
            foreach (ILayoutItem li in layout.LayoutItems)
            {
                logger.Info("adding Layout Item: " + li.ItemName);
                li.Dpi = dpi;
                li.AddToCanvas(canvas, quality, (FrameworkElement fe) =>
                {
                    fe.Measure(new Size(canvas.Width, canvas.Height));
                    fe.Arrange(new Rect(0, 0, canvas.Width, canvas.Height));
                    fe.UpdateLayout();
                });
            }
            logger.Info("Done adding Layout Items");
            canvas.EndInit();
            canvas.Measure(new Size(canvas.Width, canvas.Height));
            canvas.Arrange(new Rect(0, 0, canvas.Width, canvas.Height));
            canvas.UpdateLayout();
            logger.Info("Done doing LayoutToCanvas");
            return canvas;
        }
    }

}
