﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.Designer.Model;
using StructureMap;
using System.Windows.Controls;
using System.Windows.Threading;

namespace Flow.Designer.Lib.Service
{
    public static class LayoutRenderServiceExtensions
    {
        public static void GenerateImageFile(this Layout layout, int dpi,
            string filename)
        {
            ObjectFactory.GetInstance<LayoutRenderService>()
                .GenerateImageFile(layout, dpi, filename);
        }

        public static void SendToPrinter(this Layout layout, PrintDialog dlg)
        {
            ObjectFactory.GetInstance<LayoutRenderService>().SendToPrinter(layout, dlg);
        }

        public static void SendToPrinter(this Layout layout, bool showDialog)
        {
            ObjectFactory.GetInstance<LayoutRenderService>().SendToPrinter(layout, showDialog);
        }
    }
}
