﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.Designer.Model;
using System.Windows;
using System.Windows.Controls;
using NLog;

namespace Flow.Designer.Lib.Service
{
    public static class LayoutItemExtensions
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public static void AddToCanvas(this ILayoutItem li, Canvas c,
            LayoutRenderQuality quality,
            Action<FrameworkElement> action)
        {
            li.AddToCanvas(c, quality, action, null);
        }

        public static void AddToCanvas(this ILayoutItem li, Canvas c,
            LayoutRenderQuality quality,
            Action<FrameworkElement> action,
            Action<FrameworkElement[]> action2)
        {
            FrameworkElement[] fes = li.CreateFrameworkElements(quality);
            logger.Info("adding Framework Elements");
            if (action2 != null) action2(fes);
            foreach (FrameworkElement fe in fes)
            {
                logger.Info("adding Framework Element: " + fe.Name);
                c.Children.Add(fe);

                fe.HorizontalAlignment = HorizontalAlignment.Left;
                fe.VerticalAlignment = VerticalAlignment.Top;
                fe.SetValue(Canvas.TopProperty, (double)li.Location.Top);
                fe.SetValue(Canvas.LeftProperty, (double)li.Location.Left);
                fe.SetValue(Canvas.WidthProperty, (double)li.Location.Width);
                fe.SetValue(Canvas.HeightProperty, (double)li.Location.Height);

                if (action != null) action(fe);
            }
            logger.Info("Done adding Framework Elements");
        }
    }

}
