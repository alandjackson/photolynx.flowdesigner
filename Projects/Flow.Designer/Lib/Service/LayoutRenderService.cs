﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.Designer.Model;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.IO;
using StructureMap;
using Flow.Designer.Model.Settings;
using System.Windows.Threading;
using Flow.Lib.Log;
using NLog;

namespace Flow.Designer.Lib.Service
{
    public class LayoutRenderService
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public void GenerateImageFile(Layout layout, int dpi, string filename)
        {
            logger.Info("Starting layout render to file: " + filename);

            var canvas = GetCanvas(layout, dpi);

            Viewbox viewbox = new Viewbox();
            viewbox.Child = canvas; //control to render
            viewbox.Measure(new System.Windows.Size(canvas.Width, canvas.Height));
            viewbox.Arrange(new Rect(0, 0, canvas.Width, canvas.Height));
            viewbox.UpdateLayout();
            viewbox.GenerateImageFile(filename);



            //GetCanvas(layout).GenerateImageFile(filename);
            

            
            logger.Info("Finished render to file");
        }

        public void SendToPrinter(Layout layout, PrintDialog dlg)
        {
            SendToPrinter(layout, dlg, false);
        }

        public void SendToPrinter(Layout layout, bool showDialog)
        {
            SendToPrinter(layout, new PrintDialog(), showDialog);
        }

        public void SendToPrinter(Layout layout, PrintDialog dlg, bool showDialog)
        {
            if (!showDialog || dlg.ShowDialog() == true)
            {
                using (var imgms = new MemoryStream())
                {
                    dlg.PrintVisual(GetImageWrappedCanvas(layout, imgms), "Flow Layout");
                }
            }
        }

        protected Canvas GetCanvas(Layout layout, int dpi)
        {
            return layout.ToCanvas(dpi, LayoutRenderQuality.High);
        }

        protected Canvas GetImageWrappedCanvas(Layout layout, MemoryStream ms)
        {



            return GetImageWrappedCanvasRenderedBarcodes(layout, ms);


            //bool hasTextEffects = false;
            //foreach (ILayoutItem li in layout.LayoutItems)
            //{
            //    if (li.GetType() == typeof(LayoutText))
            //    {
            //        LayoutText lt = (LayoutText)li;
            //        if (lt.DropShadowEnabled || lt.StrokeThickness > 0)
            //            hasTextEffects = true;
            //    }
            //}
            //if(hasTextEffects)
            //    return GetImageWrappedCanvasRendered(layout, ms);
            //else
            //    return GetImageWrappedCanvasUnRendered(layout, ms);

        }
        protected Canvas GetImageWrappedCanvasUnRendered(Layout layout, MemoryStream ms)
        {
            var layoutElement = layout.ToCanvas(300, LayoutRenderQuality.High);
            return layoutElement;

        }

        protected Canvas GetImageWrappedCanvasRenderedBarcodes(Layout layout, MemoryStream ms)
        {

            List<LayoutText> barcodeLayouts = new List<LayoutText>();
            foreach (ILayoutItem li in layout.LayoutItems)
            {
                if (li.GetType() == typeof(LayoutText))
                {
                    LayoutText lt = (LayoutText)li;
                    if (lt.FontFamily.FamilyNames.Any(fn => fn.Value.ToLower().Contains("bar")) || lt.FontFamily.FamilyNames.Any(fn => fn.Value.ToLower().Contains("bc")))
                    {
                        barcodeLayouts.Add(lt);
                    }
                }
            }

            //remove the barcode text from the layout
            //we will add it back in after we render the other elements to jpg
            foreach(LayoutText lt in barcodeLayouts)
                layout.LayoutItems.Remove(lt);

            int dpi = 96;
            var layoutElement = layout.ToCanvas(300, LayoutRenderQuality.High);

            
            var renderer = new FrameworkElementRenderService(null);
            renderer.RenderToJpegStream(layoutElement, 300, ms);
            ms.Position = 0;

            Canvas canvas = new Canvas();
            canvas.BeginInit();
            canvas.Width = layout.LayoutWidth * dpi;
            canvas.Height = layout.LayoutHeight * dpi;
            canvas.Background = new SolidColorBrush(layout.BackgroundColor);

            var bmp = new BitmapImage();
            bmp.BeginInit();
            bmp.StreamSource = ms;
            bmp.EndInit();

            var fe = new Image();
            canvas.Children.Add(fe);

            fe.BeginInit();
            fe.Source = bmp;

            fe.HorizontalAlignment = HorizontalAlignment.Left;
            fe.VerticalAlignment = VerticalAlignment.Top;
            fe.SetValue(Canvas.TopProperty, (double)0);
            fe.SetValue(Canvas.LeftProperty, (double)0);
            fe.SetValue(Canvas.WidthProperty, (double)canvas.Width);
            fe.SetValue(Canvas.HeightProperty, (double)canvas.Height);
            fe.Measure(new Size(canvas.Width, canvas.Height));
            fe.Arrange(new Rect(0, 0, canvas.Width, canvas.Height));
            fe.EndInit();
            fe.UpdateLayout();

            foreach(LayoutText lt in barcodeLayouts)
            {
                lt.AddToCanvas(canvas,LayoutRenderQuality.High,null);
            }

            canvas.EndInit();
            canvas.Measure(new Size(canvas.Width, canvas.Height));
            canvas.Arrange(new Rect(0, 0, canvas.Width, canvas.Height));
            canvas.UpdateLayout();

            return canvas;


        }

        protected Canvas GetImageWrappedCanvasRendered(Layout layout, MemoryStream ms)
        {
            int dpi = 96;
            var layoutElement = layout.ToCanvas(300, LayoutRenderQuality.High);

            var renderer = new FrameworkElementRenderService(null);
            renderer.RenderToJpegStream(layoutElement, 300, ms);
            ms.Position = 0;

            Canvas canvas = new Canvas();
            canvas.BeginInit();
            canvas.Width = layout.LayoutWidth * dpi;
            canvas.Height = layout.LayoutHeight * dpi;
            canvas.Background = new SolidColorBrush(layout.BackgroundColor);

            var bmp = new BitmapImage();
            bmp.BeginInit();
            bmp.StreamSource = ms;
            bmp.EndInit();

            var fe = new Image();
            canvas.Children.Add(fe);

            fe.BeginInit();
            fe.Source = bmp;

            fe.HorizontalAlignment = HorizontalAlignment.Left;
            fe.VerticalAlignment = VerticalAlignment.Top;
            fe.SetValue(Canvas.TopProperty, (double)0);
            fe.SetValue(Canvas.LeftProperty, (double)0);
            fe.SetValue(Canvas.WidthProperty, (double)canvas.Width);
            fe.SetValue(Canvas.HeightProperty, (double)canvas.Height);
            fe.Measure(new Size(canvas.Width, canvas.Height));
            fe.Arrange(new Rect(0, 0, canvas.Width, canvas.Height));
            fe.EndInit();
            fe.UpdateLayout();

            canvas.EndInit();
            canvas.Measure(new Size(canvas.Width, canvas.Height));
            canvas.Arrange(new Rect(0, 0, canvas.Width, canvas.Height));
            canvas.UpdateLayout();

            return canvas;


        }
    }
}
