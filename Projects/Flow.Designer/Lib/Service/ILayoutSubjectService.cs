﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.Designer.Model.Project;
using Flow.Designer.Lib.ProjectRepository;

namespace Flow.Designer.Lib.Service
{
    public interface ILayoutSubjectService
    {
        event EventHandler SubjectChanged;
        ICurrentSubjectRepository SubjectRepository { get; set; }
        Subject GetSubject();
    }

}
