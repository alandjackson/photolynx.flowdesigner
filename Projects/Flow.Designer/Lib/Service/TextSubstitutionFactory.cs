﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.Designer.Model.Project;

namespace Flow.Designer.Lib.Service
{
    public class TextSubstitutionFactory
    {
        public static TextSubstitution Build(Subject subject, DesignerDynamicFields designerDynamicFields)
        {
            TextSubstitution textSubstitution = new TextSubstitution();

            Dictionary<string, string> substitutionStrings = new Dictionary<string, string>();

            if (subject != null)
            {
                foreach (string key in subject.Fields.Keys)
                {
                    if(!substitutionStrings.ContainsKey(key))
                        substitutionStrings.Add(key, subject.Fields[key]);
                }

                foreach (string key in subject.Fields.Keys)
                {
                    if (!substitutionStrings.ContainsKey("Subject." + key))
                    substitutionStrings.Add("Subject." + key, subject.Fields[key]);
                }

                if (designerDynamicFields != null)
                {
                    foreach (string key in designerDynamicFields.Fields.Keys)
                    {
                        if (!substitutionStrings.ContainsKey(key))
                            substitutionStrings.Add(key, designerDynamicFields.Fields[key]);
                    }
                }

                if (subject.SubjectImages != null && subject.SubjectImages.Count > 0)
                {
                    if (!substitutionStrings.ContainsKey("Image.Name"))
                        substitutionStrings.Add("Image.Name", subject.SubjectImages[0].ImageFileName);
                }
                if (subject.SubjectImages != null)
                {
                    for (int i = 0; i < subject.SubjectImages.Count; i++)
                    {
                        if (!substitutionStrings.ContainsKey("Image" + (i + 1).ToString() + ".Name"))
                            substitutionStrings.Add("Image" + (i+1).ToString() + ".Name", subject.SubjectImages[i].ImageFileName);
                    }
                }

            }

            textSubstitution.Variables = substitutionStrings;

            return textSubstitution;
        }
    }
}
