﻿using System;
using Flow.Designer.Lib.ProjectInterop;
namespace Flow.Designer.Lib.Service
{
    public interface ILayoutSubjectsPreviewService: ISubjectsRepository
    {
        int CurrentRecordNdx { get; set; }
        Flow.Designer.Model.Project.Subject GetCurrentSubject();
        bool IsPreviewEnabled { get; set; }
        void MoveFirst();
        void MoveLast();
        void MoveNext();
        void MovePrevious();
        int RecordsCount { get; }
        event EventHandler SubjectChanged;
        void UpdateRecordsCount();
    }
}
