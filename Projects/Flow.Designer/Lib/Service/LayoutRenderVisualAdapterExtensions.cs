﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StructureMap;
using System.Windows.Controls;
using Flow.Designer.Model;
using System.Windows.Media.Imaging;
using System.IO;
using System.Windows.Media;

namespace Flow.Designer.Lib.Service
{
    public static class LayoutRenderVisualAdapterExtensions
    {
        public static Canvas ToCanvas(this Layout layout, int dpi)
        {
            return layout.ToCanvas(dpi, LayoutRenderQuality.High);
        }

        
        public static Canvas ToCanvas(this Layout layout, int dpi, LayoutRenderQuality quality)
        {
            return ObjectFactory.GetInstance<LayoutCanvasAdapter>()
                .LayoutToCanvas(layout, dpi, quality);
        }

        public static byte[] ToPngBytes(this Canvas canvas)
        {
            RenderTargetBitmap r = canvas.ToRenderTargetBitmap();
            PngBitmapEncoder encoder = new PngBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(r));
            using (MemoryStream ms = new MemoryStream())
            {
                encoder.Save(ms);
                return ms.ToArray();
            }
        }

        public static RenderTargetBitmap ToRenderTargetBitmap(this Canvas canvas, int sourceDpi = 300, int finalDpi = 300)
        {
            Viewbox v = new Viewbox();
            v.Child = canvas;
            canvas.Measure(new System.Windows.Size(double.PositiveInfinity, double.PositiveInfinity));
            v.Measure(canvas.DesiredSize);
            v.Arrange(new System.Windows.Rect(new System.Windows.Point(), canvas.DesiredSize));
            v.UpdateLayout();

            //going from 96dpi to finalDpi
            double multiplier = (double)finalDpi / (double)sourceDpi;
            int width = (int)(canvas.Width * multiplier);
            int height = (int)(canvas.Height * multiplier);
            RenderTargetBitmap r = new RenderTargetBitmap(width, height, finalDpi, finalDpi, PixelFormats.Pbgra32);
            r.Render(v);
            return r;
        }

    }
}
