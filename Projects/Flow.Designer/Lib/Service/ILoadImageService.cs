﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Imaging;

namespace Flow.Designer.Lib.Service
{
    public interface ILoadImageService
    {
        BitmapImage LoadImage(string path, int displayHeight);
    }
}
