﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.Designer.Model;
using Flow.Designer.Lib.ProjectInterop;
using System.ComponentModel;
using System.Threading;
using Flow.Lib.Log;
using Flow.Lib.UnitOfWork;
using StructureMap;
using Flow.Lib.Persister;
using Flow.Designer.Model.Settings;
using Flow.Schema.LinqModel.DataContext;
using Flow.Designer.Model.Project;
using System.IO;
using Flow.Lib.Exceptions;
using System.Windows.Threading;
using NLog;

namespace Flow.Designer.Lib.Service
{
    public class StaBackgroundWorker
    {
        public bool WorkerReportsProgress { get; set; }
        public bool WorkerSupportsCancellation { get; set; }
        public event DoWorkEventHandler DoWork;
        protected void OnDoWork(object argument) 
        {
            if (DoWork != null)
                DoWork(this, new DoWorkEventArgs(argument));
        }
        public event ProgressChangedEventHandler ProgressChanged;
        protected void OnProgressChanged(int p, object s) 
        {
            if (ProgressChanged != null)
                ProgressChanged(this, new ProgressChangedEventArgs(p, s));
        }
        public void RunWorker()
        {
            OnDoWork(null);
        }
        public void RunWorkerAsync()
        {
            RunWorkerAsync(null);
        }
        protected Thread Thread { get; set; }
        public void RunWorkerAsync(object argument)
        {
            Thread startThread = Thread.CurrentThread;

            Thread t = new Thread(new ThreadStart(() => 
            {
                try
                {
                    OnDoWork(argument);
                }
                catch (Exception e)
                {
                    Dispatcher.FromThread(startThread).Invoke((Action) delegate 
                    {
                        ObjectFactory.GetInstance<IExceptionHandler>().HandleException(this, e);
                    });
                }
            }));
            t.IsBackground = true;
            t.SetApartmentState(ApartmentState.STA);
            t.Start();
            Thread = t;
        }
        public void Wait()
        {
            Thread.Join();
        }
        public void ReportProgress(int percentage, object state)
        {
            OnProgressChanged(percentage, state);
        }

    }

    public class LayoutRenderBatchService : StaBackgroundWorker
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        ImageTypes ImageTypes { get; set; }
        Flow.Designer.Model.Project.DesignerDynamicFields DesignerDynamicFields { get; set; }
        string LayoutXml { get; set; }
        ISubjectsRepository SubjectsRepository { get; set; }
        LayoutSaveImageService LayoutSaveImageService { get; set; }
        IUnitOfWorkProcessor UnitOfWorkProcessor { get; set; }
        PrintSettingsRepository PrintSettingsRepository { get; set; }
        //public FlowMasterDataContext FlowMasterDataContext { get; set; }
        public bool CancelWorker { get; set; }
        string _currentImageDestination { get; set; }
        bool _useSubjectGuidFileName { get; set; }
        PrintSettings PrintSettings { get; set; }
        public LayoutRenderBatchService(PrintSettings ps,
            string lx, 
            ImageTypes imageTypes,
            Flow.Designer.Model.Project.DesignerDynamicFields designerDynamicFields,
            ISubjectsRepository subjects,
            LayoutSaveImageService saveImageService,
            IUnitOfWorkProcessor workProcessor,
            PrintSettingsRepository settingsRepository,
            string currentImageDestination,
            bool useSubjectGuidFileName)
        {
            PrintSettings = ps;
            this._currentImageDestination = currentImageDestination;
            this._useSubjectGuidFileName = useSubjectGuidFileName;
            this.ImageTypes = imageTypes;
            this.DesignerDynamicFields = designerDynamicFields;
            PrintSettingsRepository = settingsRepository;
            WorkerReportsProgress = true;
            WorkerSupportsCancellation = true;
            UnitOfWorkProcessor = workProcessor;

            LayoutXml = lx;
            SubjectsRepository = subjects;
            LayoutSaveImageService = saveImageService;

            DoWork += new DoWorkEventHandler(LayoutRenderBatchService_DoWork);
        }

        void LayoutRenderBatchService_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                UnitOfWorkBatch batch = ObjectFactory.GetInstance<UnitOfWorkBatch>();
                PrintSettings ps = PrintSettings;
                int dpi = ps.ImageDPI;

                for (int i = 0; i < SubjectsRepository.SubjectsCount; i++)
                {
                    string _filename = "";
                    batch.SubmitWork((object param) =>
                    {

                        int j = (int)((object [] )param)[0];
                        string saveFilename = (string)((object[])param)[1];
                        ReportProgress(ToPercentage(j, SubjectsRepository.SubjectsCount),
                            "Rendering layout " + j + " of " + SubjectsRepository.SubjectsCount);

                        if (e.Cancel || CancelWorker)
							return;

                        Layout l = ObjectSerializer.FromXmlString<Layout>(LayoutXml);
                        l.Subject = SubjectsRepository.GetSubject(j);
                        l.ImageTypes = this.ImageTypes;
                        l.DesignerDynamicFields = this.DesignerDynamicFields;
                         

                        string fileNameText = ps.UseSubjectImageNameInFileName ? l.GetOriginalFilename(null) : "";
                        fileNameText = string.IsNullOrWhiteSpace(fileNameText) && ps.UseSubjectNameInFileName ? l.GetSubjectNameForFileName(null) : fileNameText;
                        
                        if ((ps.UseSubjectImageNameInFileName || ps.UseSubjectNameInFileName) && fileNameText.Length > 0)
                            saveFilename = Path.Combine((new FileInfo(saveFilename)).Directory.FullName, fileNameText);
                        
                        
                        if (_useSubjectGuidFileName)
                        {
                            _filename = Left((l.Subject.Fields["SubjectGuid"].ToString()), 26) + ".jpg";
                            saveFilename = Path.Combine((new FileInfo(saveFilename)).Directory.FullName, _filename);
                        }

                        if (ps.PrintDestination == PrintSettings.PrintDestiations.Image)
                            l.GenerateImageFile(dpi, saveFilename);
                        else if (ps.PrintDestination == PrintSettings.PrintDestiations.WindowsPrinter)
                        {
                            if (PrintSettingsRepository.PrintDialog != null)
                                l.SendToPrinter(PrintSettingsRepository.PrintDialog);
                            else
                                l.SendToPrinter(false);

                        }

                    }, new object[] { i, LayoutSaveImageService.GetNextOutputFilename(_currentImageDestination, _filename) });
                }

                

                batch.WhenFinished(() =>
                {
                    this.ReportProgress(100, "Rendering Layouts Complete");
                });

                UnitOfWorkProcessor.SubmitBatch(batch);
            }
            catch (Exception err)
            {
                logger.ErrorException("Exception occured.", err);
                this.ReportProgress(0, "Error: " + err.Message);
                
            }
        }

        protected string Left(string value, int length)
        {
            if (value == null || length < 0)
                return null;

            if (value.Length < length)
                return value;

            return value.Substring(0, length);
        }

        protected int ToPercentage(int v, int t)
        {
            return (int) ((double)v / t * 100);
        }
    }





}
