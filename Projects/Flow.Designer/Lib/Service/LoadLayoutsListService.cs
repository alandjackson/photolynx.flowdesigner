﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Flow.Lib;
using System.Windows.Controls;
using Flow.Schema.LinqModel.DataContext;

namespace Flow.Designer.Lib.Service
{

    public class LoadLayoutsListService
    {
        protected ItemCollection ItemsToRemove { get; set; }
        protected Action NoFilesAction { get; set; }
        protected Action<FileInfo> FileFoundAction { get; set; }

      
        public LoadLayoutsListService ItemsToRemoveAre(ItemCollection ic)
        {
            ItemsToRemove = ic;
            return this;
        }

        public LoadLayoutsListService WhenNoFilesFound(Action action)
        {
            NoFilesAction = action;
            return this;
        }

        public LoadLayoutsListService ForEachFileFound(Action<FileInfo> action)
        {
            FileFoundAction = action;
            return this;
        }


        public void LoadLayouts(String LayoutsDirectory)
        {
            if (ItemsToRemove != null)
                while (ItemsToRemove.Count > 0)
                    ItemsToRemove.RemoveAt(0);

            //DirectoryInfo layoutsDir = new DirectoryInfo(FlowContext.FlowLayoutsDirPath);
            DirectoryInfo layoutsDir = new DirectoryInfo(LayoutsDirectory);
            
            FileInfo[] layoutFiles = new FileInfo[0];
            List<FileInfo> tempFiles = new List<FileInfo>();
            if (layoutsDir.Exists)
            {
                tempFiles.Add(new FileInfo(".. (up a directory)"));
                foreach (DirectoryInfo di in layoutsDir.GetDirectories())
                    tempFiles.Add(new FileInfo(Path.Combine(di.Parent.FullName, "[" + di.Name + "]")));
                tempFiles.Add(new FileInfo("--------------------------"));
                tempFiles.AddRange(layoutsDir.GetFiles("*.lyt"));
            }
            layoutFiles = tempFiles.ToArray();

            // no layouts found
            if (layoutFiles.Length == 0)
            {
                if (NoFilesAction != null)
                    NoFilesAction();
                return;
            }

            // load all of the layouts
            foreach (FileInfo layoutFile in layoutFiles)
            {
                if (FileFoundAction != null)
                    FileFoundAction(layoutFile);
            }
        }

    }

    public class DisplayableFile
    {
        public FileInfo FileInfo { get; set; }
        public DisplayableFile(FileInfo fi) { FileInfo = fi; }
        public override string ToString()
        {
            return FileInfo == null ? "" : FileInfo.Name;
        }
    }

}
