﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.Designer.Model.Project;
using Flow.Designer.Lib.ProjectRepository;

namespace Flow.Designer.Lib.Service
{
    public class LayoutSubjectService : ILayoutSubjectService
    {
        ICurrentSubjectRepository _subjectRepository = null;
        public ICurrentSubjectRepository SubjectRepository
        {
            get { return _subjectRepository; }
            set
            {
                _subjectRepository = value;
                _subjectRepository.SubjectChanged += new EventHandler((object sender, EventArgs e) => { OnSubjectChanged(); });
            }
        }

        public event EventHandler SubjectChanged;
        protected void OnSubjectChanged()
        {
            if (SubjectChanged != null)
                SubjectChanged(this, EventArgs.Empty);
        }

        public Subject GetSubject()
        {
            if (SubjectRepository == null)
                throw new ApplicationException(
                    "LayoutSubjectService repository never set, can't use service");

            return SubjectRepository.GetSubject();
        }
    }
}
