﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.Designer.Model.Settings;
using System.Windows.Media.Imaging;
using System.Windows;
using System.Windows.Shapes;
using Tamir.SharpSsh.java.io;
using System.IO;
using System.Windows.Media;

namespace Flow.Designer.Lib.Service
{
    public class FrameworkElementRenderService
    {
        public IPrintSettingsRepository PrintSettingsRepository { get; set; }

        public FrameworkElementRenderService(IPrintSettingsRepository repository = null)
        {
            PrintSettingsRepository = repository;
        }

        public void GenerateImageFile(FrameworkElement canvas, string filename)
        {
            int dpi = 300;
            if (PrintSettingsRepository != null)
            {
                PrintSettings settings = PrintSettingsRepository.Load();
                filename = System.IO.Path.ChangeExtension(filename, GetExtension(settings.ImageFormat));
                dpi = settings.ImageDPI;
            }


            using (FileStream fs = System.IO.File.OpenWrite(filename))
            {
                RenderToImageStream(canvas, dpi, fs, GetEncoder(new FileInfo(filename).Extension));
            }
        }

        public void RenderToJpegStream(FrameworkElement canvas, int dpi, Stream strm)
        {
            RenderToImageStream(canvas, dpi, strm, new JpegBitmapEncoder());
        }

        public void RenderToImageStream(FrameworkElement canvas, int dpi, Stream strm, BitmapEncoder encoder)
        {
            int width = (int)(canvas.ActualWidth > 0 ?
                canvas.ActualWidth : canvas.Width);
            int height = (int)(canvas.ActualHeight > 0 ?
                canvas.ActualHeight : canvas.Height);

            RenderTargetBitmap rtb = new RenderTargetBitmap(
                width, height, dpi, dpi,
                PixelFormats.Pbgra32);
            rtb.Render(canvas);

            encoder.Frames.Add(BitmapFrame.Create(rtb));
            encoder.Save(strm);
        }

        protected DrawingVisual WrapVisualInVisualBrush(Visual target, int width, int height)
        {
            DrawingVisual dv = new DrawingVisual();
            using (DrawingContext ctx = dv.RenderOpen())
            {
                VisualBrush vb = new VisualBrush(target);
                ctx.DrawRectangle(vb, null, new Rect(new Point(), new Size(width, height)));
            }
            return dv;
        }

        protected string GetExtension(string type)
        {
            return "." + type.ToLower();
        }

        protected BitmapEncoder GetEncoder(string type)
        {
            if (type.StartsWith("."))
                type = type.Substring(1);

            switch (type.ToUpper())
            {
                case "JPG":
                    return new JpegBitmapEncoder();
                case "JPEG":
                    return new JpegBitmapEncoder();
                case "TIFF":
                    return new TiffBitmapEncoder();
                case "BMP":
                    return new BmpBitmapEncoder();
                case "PNG":
                    return new PngBitmapEncoder();
                default:
                    throw new ApplicationException("GetEncoder: Unknown image type: " + type);
            }
        }
    }

}
