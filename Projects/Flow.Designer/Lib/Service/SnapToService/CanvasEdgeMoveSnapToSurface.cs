﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;

namespace Flow.Designer.Lib.Service.SnapToService
{
    public interface ISnapLocationProvider
    {
        double[] XSnapLocations { get; }
        double[] YSnapLocations { get; }
    }

    public class CanvasSnapLocationProvider : ISnapLocationProvider
    {
        public Canvas Canvas { get; set; }

        public CanvasSnapLocationProvider(Canvas c)
        {
            Canvas = c;
        }

        public double[] XSnapLocations
        {
            get { return new double [] { 0, Canvas.Width }; }
        }

        public double[] YSnapLocations
        {
            get { return new double [] { 0, Canvas.Height }; }
        }
    }

    public class SnapToService : ISnapToService
    {
        public double SnapToDistance = 10;

        EdgeSnapToService _yEdgeSnapToService;
        EdgeSnapToService _xEdgeSnapToService;

        public List<ISnapLocationProvider> SnapLocationProviders { get; set; }

        public bool IsSnapEnabled { get; set; }

        public SnapToService()
            : this(10) { }

        public SnapToService(double snapToDistance, params ISnapLocationProvider [] providers)
        {
            IsSnapEnabled = true;

            SnapLocationProviders = new List<ISnapLocationProvider>(providers);
            SnapToDistance = snapToDistance;

            _yEdgeSnapToService = new EdgeSnapToService(SnapToDistance);
            _xEdgeSnapToService = new EdgeSnapToService(SnapToDistance);
        }

        #region ISnapToService Members

        public void Start()
        {
            _yEdgeSnapToService.Start();
            _xEdgeSnapToService.Start();
        }

        protected double[] XSnapLocations
        {
            get
            {
                List<double> snapLocations = new List<double>();
                foreach (ISnapLocationProvider provider in SnapLocationProviders)
                    snapLocations.AddRange(provider.XSnapLocations);
                return snapLocations.ToArray();
            }
        }

        protected double[] YSnapLocations
        {
            get
            {
                List<double> snapLocations = new List<double>();
                foreach (ISnapLocationProvider provider in SnapLocationProviders)
                    snapLocations.AddRange(provider.YSnapLocations);
                return snapLocations.ToArray();
            }
        }


        public double SnapX(double src)
        {
            if (!IsSnapEnabled)
                return src;

            return _xEdgeSnapToService.SnapEdgeWithState(src, XSnapLocations);
        }

        public double SnapY(double src)
        {
            if (!IsSnapEnabled)
                return src;

            return _yEdgeSnapToService.SnapEdgeWithState(src, YSnapLocations);
        }

        #endregion
    }
}
