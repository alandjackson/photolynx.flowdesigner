﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flow.Designer.Lib.Service.SnapToService
{
    public interface ISnapToService
    {
        void Start();
        double SnapX(double src);
        double SnapY(double src);
    }
}
