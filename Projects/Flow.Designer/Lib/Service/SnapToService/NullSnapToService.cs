﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flow.Designer.Lib.Service.SnapToService
{
    public class NullSnapToService : ISnapToService
    {
        public void Start() { }
        public double SnapX(double src) { return src; }
        public double SnapY(double src) { return src; }

    }
}
