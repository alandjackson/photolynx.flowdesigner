﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flow.Designer.Lib.Service.SnapToService
{
    public class EdgeSnapToService
    {
        protected double? _unsnapped;
        protected double _snappedTo;
        protected double SnapToDistance;

        public EdgeSnapToService(double snapToDistance)
        {
            SnapToDistance = snapToDistance;
        }

        public void Start()
        {
            _unsnapped = null;
        }

        public double SnapEdgeWithState(double src, params double[] snapLocations)
        {
            double closest = GetClosestLocation(src, snapLocations);
            return SnapEdgeWithState(src, closest);
        }

        /// <summary>
        /// Returns the value from the list that is closts to the src
        /// </summary>
        /// <param name="src"></param>
        /// <param name="snapLocations"></param>
        /// <returns></returns>
        protected double GetClosestLocation(double src, double[] snapLocations)
        {
            if (snapLocations.Length == 0)
                throw new ArgumentException("Must include at least one snap location");

            double closest = snapLocations[0];
            for (int i = 1; i < snapLocations.Length; i++)
                if (Math.Abs(closest - src) > Math.Abs(snapLocations[i] - src))
                    closest = snapLocations[i];
            return closest;
        }

        public double SnapEdgeWithState(double src, double snapLocation)
        {
            if (_unsnapped != null && _snappedTo == snapLocation)
            {
                _unsnapped += (src - _snappedTo);
                double newVal = SnapEdge((double)_unsnapped, snapLocation);
                if (newVal == _unsnapped)
                    _unsnapped = null;
                return newVal;
            }
            else
            {
                double newVal = SnapEdge(src, snapLocation);
                if (newVal != src)
                {
                    _unsnapped = src;
                    _snappedTo = newVal;
                }
                return newVal;
            }
        }

        protected double SnapEdge(double src, double value)
        {
            if (src >= value - SnapToDistance && src <= value + SnapToDistance)
                return value;
            return src;
        }
    }

}
