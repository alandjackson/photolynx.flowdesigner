﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flow.Designer.Lib.Service
{
    public class TextSubstitution
    {
        public Dictionary<string, string> Variables { get; set; }

        public TextSubstitution()
        {
            Variables = new Dictionary<string, string>();
        }

        public string Render(string src)
        {
            foreach (string var in Variables.Keys)
                src = src.Replace("[" + var + "]", Variables[var]);
            return src;
        }
    }
}
