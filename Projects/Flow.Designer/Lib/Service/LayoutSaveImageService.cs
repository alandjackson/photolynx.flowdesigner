﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.Designer.Properties;
using System.IO;
using Flow.Designer.Model.Settings;
using StructureMap;
using Flow.Schema.LinqModel.DataContext;
using Flow.Designer.Controller;

namespace Flow.Designer.Lib.Service
{
    public class LayoutSaveImageService
    {
        Settings DesignerSettings { get; set; }
        public FlowDesignerDataContext FlowMasterDataContext { get; set; }
        public LayoutSaveImageService()
        {
            DesignerSettings = Settings.Default;
        }

        public string GetNextOutputFilename(string currentImageDestination, string fileNameText)
        {
            string filename = "";
            do
            {
                filename = BuildOutputFilename(currentImageDestination, fileNameText);
                IncrementNextOutputFilename();
            } while (File.Exists(filename));

            return filename;
        }

        public string BuildOutputFilename(string currentImageDestination, string fileNameText)
        {
            PrintSettingsRepository repository = ObjectFactory.GetInstance<PrintSettingsRepository>();
            PrintSettings ps = repository.Load(FlowMasterDataContext.LayoutPreferences.Designer);
            string filename = "";
            //if (!new FileInfo(ps.ImageOutputFolderPath).Directory.Exists)
            //    return filename;
            if (fileNameText.Length < 1)
            {
                filename = BuildOutputFilename(
                currentImageDestination,
                DesignerSettings.OutputPrefix,
                DesignerSettings.OutputNdx,
                DesignerSettings.OutputNdxDigits,
                ".jpg");
            }
            else
            {
                filename = Path.Combine(currentImageDestination, fileNameText);
            }

           

            if (!new FileInfo(filename).Directory.Exists)
                new FileInfo(filename).Directory.Create();

            return filename;
        }

        public string BuildOutputFilename(string folder, string prefix,
            long ndx, int digits, string ext)
        {
            string filename = prefix + ndx.ToString(
                new String('0', digits)) + ext;

            return Path.Combine(folder, filename);
        }

        public void IncrementNextOutputFilename()
        {
            DesignerSettings.OutputNdx = DesignerSettings.OutputNdx + 1;
            DesignerSettings.Save();
        }



    }
}
