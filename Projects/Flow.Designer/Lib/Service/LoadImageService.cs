﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.Lib.ImageUtils;
using System.Windows.Media.Imaging;

namespace Flow.Designer.Lib.Service
{
    public class LoadImageService : ILoadImageService
    {
        public BitmapImage LoadImage(string path, int displayHeight)
        {
            return ImageLoader.LoadImage(path, displayHeight);
        }
    }
}
