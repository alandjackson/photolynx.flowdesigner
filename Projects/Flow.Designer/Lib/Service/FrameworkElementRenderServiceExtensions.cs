﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using StructureMap;

namespace Flow.Designer.Lib.Service
{
    public static class FrameworkElementRenderServiceExtensions
    {
        public static void GenerateImageFile(this FrameworkElement canvas,
            string filename)
        {
            ObjectFactory.GetInstance<FrameworkElementRenderService>()
                .GenerateImageFile(canvas, filename);
        }
    }
}
