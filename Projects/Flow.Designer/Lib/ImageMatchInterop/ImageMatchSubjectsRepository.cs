﻿using Flow.Designer.Lib.ProjectInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.Designer.Model.Project;
using Flow.Designer.Lib.Service;

namespace Flow.Designer.Lib.ImageMatchInterop
{
    class ImageMatchSubjectsRepository : ILayoutSubjectsPreviewService
    {
        public int CurrentRecordNdx
        {
            get
            {
                return 1;
            }

            set
            {
            }
        }

        public bool IsPreviewEnabled
        {
            get
            {
                return false;
            }

            set
            {
            }
        }

        public int RecordsCount
        {
            get
            {
                return 0;
            }
        }

        public int SubjectsCount
        {
            get
            {
                return 0;
            }
        }

        public event EventHandler SubjectChanged;

        public Subject GetCurrentSubject()
        {
            return null;
        }

        public string[] GetImageTags()
        {
            return new string[0];
        }

        public Subject GetSubject(int ndx)
        {
            return null;
        }

        public string[] GetSubjectFields()
        {
            return new string[0];
        }

        public void MoveFirst()
        {
        }

        public void MoveLast()
        {
        }

        public void MoveNext()
        {
        }

        public void MovePrevious()
        {
        }

        public void UpdateRecordsCount()
        {
        }
    }
}
