﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Collections.ObjectModel;

namespace Flow.Designer.Lib.Collections
{
    public class UndoRedoStack<T> where T : ICloneable, INotifyPropertyChanged
    {
        public ObservableCollection<T> Items { get; set; }
        public List<T> RemovedItems { get; set; }

        public UndoRedoStack()
        {
            Items = new ObservableCollection<T>();
            RemovedItems = new List<T>();
        }

        public T Top
        {
            get
            {
                if (Items.Count > 0)
                    return Items[Items.Count - 1];
                return default(T);
            }
        }

        public void Push(T item)
        {
            Items.Add(item);
            RemovedItems = new List<T>();
        }

        public bool Undo()
        {
            if (Items.Count <= 1)
                return false;
            T top = Top;
            RemovedItems.Add(top);
            Items.Remove(top);
            return true;
        }

        public bool Redo()
        {
            if (RemovedItems.Count == 0)
                return false;

            T lastRemoved = RemovedItems[RemovedItems.Count - 1];
            RemovedItems.Remove(lastRemoved);
            Items.Add(lastRemoved);
            return true;
        }
    }
}
