﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using Flow.Designer.Model;

namespace Flow.Designer.Helpers
{
    public class ConvertParentToIndent : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (value == null ? 0 : 16);
        }

        public object  ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotSupportedException("Not supported - ConvertBack should never be called in a OneWay Binding.");
        }

        #endregion
    }
}


    //public class ConvertLevelToIndent : IValueConverter
    //{
    //    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    //    {
    //        return (int)value * 16;
    //    }
        
    //    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    //    {
    //        throw new NotSupportedException(”Not supported - ConvertBack should never be called in a OneWay Binding.”);
    //    }
    //}}
