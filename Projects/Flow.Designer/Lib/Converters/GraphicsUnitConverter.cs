﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using Flow.Designer.Model.Settings;

namespace Flow.Designer.Lib.Converters
{
    /// <summary>
    /// Converts pixel values to a graphics unit
    /// </summary>
    public class GraphicsUnitsShortConverter : IValueConverter
    {
        public GraphicsUnits GraphicsUnits { get; set; }

        public GraphicsUnitsShortConverter()
        {
            GraphicsUnits = GraphicsUnits.Pixels;
        }

        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (!(value is GraphicsUnits))
                return "";

            switch ((GraphicsUnits)value)
            {
                case GraphicsUnits.Inches:
                    return "in";
                case GraphicsUnits.Centimeters:
                    return "cm";
                case GraphicsUnits.Pixels:
                    return "px";
            }
            return "";
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }


    /// <summary>
    /// Converts pixel values to a graphics unit
    /// </summary>
    public class PixelGraphicsUnitConverter : IValueConverter
    {
        public GraphicsUnits GraphicsUnits { get; set; }

        public PixelGraphicsUnitConverter()
        {
            GraphicsUnits = GraphicsUnits.Pixels;
        }

        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
                return 0;

            double v;
            if (value is Double ||  value is double)
                v = (double)value;
            else if (value is Int32 || value is int)
                v = (double)(int)value;
            else
                return 0;

            switch (GraphicsUnits)
            {
                case GraphicsUnits.Pixels:
                    return v;
                case GraphicsUnits.Inches:
                    return Math.Round(v / 96, 3);
                case GraphicsUnits.Centimeters:
                    return Math.Round(v / 96 * 2.54, 3);
            }
            
            return 0;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
                return 0;

            if (!(value is string))
                throw new Exception("PixelGraphicsUnitConverter can't ConvertBack when value isn't a string (" + value.GetType().Name + ")");

            double v;
            if (!double.TryParse((string)value, out v))
                return 0;

            switch (GraphicsUnits)
            {
                case GraphicsUnits.Pixels:
                    return v;
                case GraphicsUnits.Inches:
                    return Math.Round(v * 96, 2);
                case GraphicsUnits.Centimeters:
                    return Math.Round(v * 96 / 2.54);
            }

            return 0;

        }

        #endregion
    }
}
