﻿using Flow.Lib.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace Flow.Designer.Lib.Application
{
    public class ExceptionHandler : IExceptionHandler
    {
        public void HandleException(object sender, Exception e)
        {
            MessageBox.Show(e.ToString(), "Application Exception");
        }
    }
}
