﻿using Flow.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flow.Designer.Lib.Application
{
    public class ShowsNotificationProgress : IShowsNotificationProgress
    {
        public void ShowNotification(NotificationProgressInfo info)
        {
        }

        public void UpdateNotification(NotificationProgressInfo info)
        {
        }
    }
}
