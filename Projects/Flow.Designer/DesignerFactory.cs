﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StructureMap;
using Flow.Designer.Lib.SurfaceUi;
using StructureMap.Attributes;
using Flow.Designer.Lib.Service;
using StructureMap.Configuration.DSL;
using Flow.Designer.Model.Settings;
using Flow.Designer.Lib.ProjectInterop;
using Flow.Designer.Controller;
using Flow.Designer.View;
using Flow.Designer.Lib.ImageMatchInterop;
using Flow.Lib.Exceptions;
using Flow.Lib;
using Flow.Designer.Lib.Application;

namespace Flow.Designer
{
    public class DesignerRegistry : Registry
    {
        public DesignerRegistry()
        {

            // Flow.Lib Interface, Designer Implementation
            ForRequestedType<IShowsNotificationProgress>().TheDefaultIsConcreteType<ShowsNotificationProgress>();
            ForRequestedType<IExceptionHandler>().TheDefaultIsConcreteType<ExceptionHandler>().CacheBy(InstanceScope.Singleton);


            ForRequestedType<ISubjectsRepository>().TheDefaultIsConcreteType<ImageMatchSubjectsRepository>();
            ForRequestedType<ILayoutSubjectsPreviewService>().TheDefaultIsConcreteType<ImageMatchSubjectsRepository>();

            ForRequestedType<FlowDesignerDataContext>().TheDefaultIsConcreteType<FlowDesignerDataContext>()
                .CacheBy(InstanceScope.Singleton);

            ForRequestedType<DesignerController>().TheDefaultIsConcreteType<DesignerController>();
            ForRequestedType<Surface>().TheDefaultIsConcreteType<Surface>();
            ForRequestedType<Nodes>().TheDefaultIsConcreteType<Nodes>();
            ForRequestedType<History>().TheDefaultIsConcreteType<History>();
            ForRequestedType<PropertiesPanel>().TheDefaultIsConcreteType<PropertiesPanel>();

            ForRequestedType<LayoutItemAligner>().TheDefaultIsConcreteType<LayoutItemAligner>()
                .CacheBy(InstanceScope.Singleton);

            ForRequestedType<ILayoutSubjectService>().TheDefaultIsConcreteType<LayoutSubjectService>()
                .CacheBy(InstanceScope.Singleton);

            ForRequestedType<FrameworkElementRenderService>().TheDefaultIsConcreteType<FrameworkElementRenderService>();
            ForRequestedType<IPrintSettingsRepository>().TheDefaultIsConcreteType<PrintSettingsRepository>();
            
            
            ForRequestedType<LayoutCanvasAdapter>().TheDefaultIsConcreteType<LayoutCanvasAdapter>();
            ForRequestedType<LayoutRenderService>().TheDefaultIsConcreteType<LayoutRenderService>();

            
        }
    }
}
