﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using Microsoft.Win32;
using System.IO;
using System.Threading;
using System.Collections;
using System.Drawing;
using System.Windows.Media;
using System.Collections.ObjectModel;
using Flow.GreenScreen.RunTest;
using Flow.GreenScreen.Panels;
using Flow.GreenScreen.Data;
using Flow.GreenScreen.Managers;
using Flow.GreenScreen.Managers.Panels;
//using PhotoLynx.GreenScreen2.Util;
//using PhotoLynx.GreenScreen2.Renderer;
//using PhotoLynx.GreenScreen2.Panels;
//using PhotoLynx.GreenScreen2.Data;
//using PhotoLynx.GreenScreen2.Dialogs;
//using PhotoLynx.GreenScreen2.Managers.Panels;
//using PhotoLynx.GreenScreen2.Managers;

namespace Driver
{
    class Program
    {
        // Basic application that shows the Green Screen editor panel
        [STAThread]
        static void Main(string[] args)
        {
            //try
            //{
                Program p = new Program();
                p.RunCPIPanel(args);
            //}
            //catch (Exception e)
            //{
            //    Console.WriteLine(e.ToString());
            //    throw;
            //}
        }

        private void RunCPIPanel(string[] args)
        {
            var app = new Application();
            app.Startup += new StartupEventHandler(app_Startup);
            app.Run();
        }

        GreenScreenPanel pnl;
        string[] subImages = new string[] {
                @"C:\ProgramData\Flow\Projects\37fff1fa-d2c8-4049-b6fa-d2c8068cdbf2\Image\gs2-test-test-IMG_1412-00033-00050.jpg",
                Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "..\\..\\images\\gs.jpg")
            };

        void app_Startup(object sender, StartupEventArgs e)
        {
            CPIGreenScreen.Util.CPISettings.Instance.BackgroundFolder =
                @"C:\ProgramData\Flow\Backgrounds";
            pnl = new GreenScreenPanel(VariantType.CPI);
            //pnl.Data.SetImageFiles(new string[0]);
            pnl.Data.SetImageFiles(subImages);
            //pnl.Data.SetImageFiles(new string [] {
            //    @"C:\ProgramData\Flow\Projects\39ab2fda-26f2-4503-b785-64a2c0fe6c85\Image\asdf-asdf-00002.jpg"
            //});

            //pnl.ShowDialog();

            pnl.Initialize();

            pnl.Data.SetConfigurationXml = new PanelData.SetConfigurationXmlDelegate(SetConfigXml);
            pnl.Data.GetConfigurationXml = new PanelData.GetConfigurationXmlDelegate(GetConfigXml);

            pnl.InitializeWindow().Show();

            var sbj = new SubjectDisplay();
            sbj.ClearSubjectButton.Click += new RoutedEventHandler(ClearSubjectButton_Click);
            sbj.LoadSubject1Button.Click += new RoutedEventHandler(LoadSubject1Button_Click);
            sbj.LoadSubject2Button.Click += new RoutedEventHandler(LoadSubject2Button_Click);
            sbj.LoadSubject2Button.Visibility = subImages.Length > 1 ? Visibility.Visible : Visibility.Hidden;
            sbj.LoadSubject3Button.Click += new RoutedEventHandler(LoadSubject3Button_Click);
            sbj.LoadSubject3Button.Visibility = subImages.Length > 2 ? Visibility.Visible : Visibility.Hidden;
            sbj.NextButton.Click += new RoutedEventHandler(NextButton_Click);
            sbj.PrevButton.Click += new RoutedEventHandler(PrevButton_Click);
            sbj.Show();
        }

        Dictionary<string, string> GsSettings = new Dictionary<string, string>();

        string GetConfigXml(string image)
        {
            //MessageBox.Show("GetConfigXml: " + image);
            return GsSettings.ContainsKey(image) ? GsSettings[image] : "";
        }

        void SetConfigXml(string image, string xml, SaveGsSettingsTo applyTo)
        {
            var cpiSettings = (ManagerTracker.SettingsPanelManager as CPISettingsPanelManager);
            GsSettings[image] = xml;
        }

        int ndx = 0;
        void PrevButton_Click(object sender, RoutedEventArgs e)
        {
            pnl.SelectedIndex = --ndx;
        }

        void NextButton_Click(object sender, RoutedEventArgs e)
        {
            pnl.SelectedIndex = ++ndx;
        }

        void LoadSubject1Button_Click(object sender, RoutedEventArgs e)
        {
            pnl.Data.SetImageFiles(subImages);
            pnl.SelectedIndex = 0;
            ndx = 0;
        }

        void LoadSubject2Button_Click(object sender, RoutedEventArgs e)
        {
            pnl.Data.SetImageFiles(subImages);
            pnl.SelectedIndex = 1;
            ndx = 0;
        }

        void LoadSubject3Button_Click(object sender, RoutedEventArgs e)
        {
            pnl.Data.SetImageFiles(new string[] {
                @"C:\Users\Public\Pictures\Sample Pictures\Chrysanthemum.jpg",
                @"C:\Users\Public\Pictures\Sample Pictures\Desert.jpg",
                @"C:\Users\Public\Pictures\Sample Pictures\Hydrangeas.jpg"
            });
            pnl.SelectedIndex = 1;
            ndx = 1;
        }

        void ClearSubjectButton_Click(object sender, RoutedEventArgs e)
        {
            pnl.Data.SetImageFiles(new string[0]);
            ndx = 0;
        }
    }
}
