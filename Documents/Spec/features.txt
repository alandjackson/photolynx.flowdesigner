Application
============

Synopsis
--------
Flow is an application to be used by photographers to capture images and subject data, edit the data and images, produce graphics (such as id cards and proof sheets).  The primary use of the application will be for school photographers to shoot a school or sports event, match images with students, enter packages, and send their work back to a lab/studio to be processed.

Each shoot or job will be broken down into a project that consists of some settings and a set of images matched with some data.

.Notes
link:InitialUserMeetingNotes.html[Initial User Meeting Notes from 11/12/2008]

Application Componants
----------------------

Layout Designer
~~~~~~~~~~~~~~~
This section of the application will allow the user to arrange graphics and text on a sheet to produce id cards, proof order sheets, or anything else they desire.

Schema
~~~~~~
Each job will have a set of subject fields, layouts, and other necessary data to allow the capture and editing of data to be as streamlined as possible.  Some items will be automatically downloaded from the web so the lab can control certain aspects (pud file, etc)

Capture
~~~~~~~
This portion of the application will have a similar workflow to our current Camlynx application.  The user will shoot images that will show up in a hotfolder, possibly scan a barcode to search for a record or type in new record information, and save images to a record.

Print
~~~~~
Either at the capture or edit stage the user needs the ability to print out items for a specific subject (id card, proof sheet) or the entire project (mugbook, etc).

View / Edit
~~~~~~~~~~~
The user needs to be able to manipulate the data (new records, shift, group) and the images (crop, correct) before sending to the lab.

Upload
~~~~~~
The system needs an end to end upload module that can upload an entire project (images, data, settings) to an ftp server and verify all of the data got there correctly.  It also needs to be able reupload the project after retakes - sending just the new data and images.

Misc
----
Onsite printing
- per record templates (id card, proof sheet)
- per job templates (mugbook)