﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ICSharpCode.SharpZipLib.Core
{
	public struct ZipFileMapping
	{
		public FileInfo SourceFileInfo { get; private set; }
		public string SourceFilePath { get { return SourceFileInfo.FullName; } }
		public string DestinationFileName { get; private set; }


		public ZipFileMapping(string sourceFilePath)
			: this()
		{
			SourceFileInfo = new FileInfo(sourceFilePath);
			DestinationFileName = SourceFileInfo.Name;
		}
	
		public ZipFileMapping(string sourceFilePath, string destinationFileName)
			: this()
		{
			SourceFileInfo = new FileInfo(sourceFilePath);
			DestinationFileName = destinationFileName;
		}

		public ZipFileMapping(FileInfo sourceFileInfo)
			: this()
		{
			SourceFileInfo = sourceFileInfo;
			DestinationFileName = SourceFileInfo.Name;
		}

		public ZipFileMapping(FileInfo sourceFileInfo, string destinationFileName)
			: this()
		{
			SourceFileInfo = sourceFileInfo;
			DestinationFileName = destinationFileName;
		}


	}
}
