//  
//   Rebex Sample Code License
// 
//   Copyright (c) 2009, Rebex CR s.r.o. www.rebex.net, 
//   All rights reserved.
// 
//   Permission to use, copy, modify, and/or distribute this software for any
//   purpose with or without fee is hereby granted
// 
//   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//   EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//   HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
//   OTHER DEALINGS IN THE SOFTWARE.
// 

using System;
using System.Collections;
using Rebex.Net;
using Rebex.Security.Certificates;

namespace Rebex.Samples.WinFormClient
{
	public class RequestHandler : ICertificateRequestHandler
	{
		private static Hashtable _chosenCertificates = new Hashtable();

		public CertificateChain Request(TlsSocket socket, DistinguishedName[] issuers)
		{
			string serverCertificateFingerprint = BitConverter.ToString(
				socket.ServerCertificate.LeafCertificate.GetCertHash());
			
			if (_chosenCertificates.Contains(serverCertificateFingerprint))
				return _chosenCertificates[serverCertificateFingerprint] as CertificateChain;

			CertificateStore my = new CertificateStore ("MY");
			Certificate[] certs;

			if (issuers.Length > 0)
				certs = my.FindCertificates
					(
					issuers,
					CertificateFindOptions.IsTimeValid |
					CertificateFindOptions.HasPrivateKey |
					CertificateFindOptions.ClientAuthentication
					);
			else
				certs = my.FindCertificates
					(
					CertificateFindOptions.IsTimeValid |
					CertificateFindOptions.HasPrivateKey |
					CertificateFindOptions.ClientAuthentication
					);

			if (certs.Length == 0)
				return null;

			RequesetHandlerForm rhForm = new RequesetHandlerForm();
			rhForm.LoadData(certs);

			if (rhForm.ShowDialog() == System.Windows.Forms.DialogResult.OK) 
			{
				CertificateChain chain = null;
				
				if (rhForm.Certificate != null)
					chain = CertificateChain.BuildFrom(rhForm.Certificate);

				// save chosen certificate for the server in static HashTable
				_chosenCertificates.Add(serverCertificateFingerprint, chain);

				return chain;
			}

			return null;
		}
	}
}
