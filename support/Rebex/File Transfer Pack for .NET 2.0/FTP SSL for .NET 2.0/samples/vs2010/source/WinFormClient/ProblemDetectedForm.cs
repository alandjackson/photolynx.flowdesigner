//  
//   Rebex Sample Code License
// 
//   Copyright (c) 2009, Rebex CR s.r.o. www.rebex.net, 
//   All rights reserved.
// 
//   Permission to use, copy, modify, and/or distribute this software for any
//   purpose with or without fee is hereby granted
// 
//   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//   EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//   HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
//   OTHER DEALINGS IN THE SOFTWARE.
// 

using System;
using System.IO;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Rebex.Net;

namespace Rebex.Samples
{
	/// <summary>
	/// Problem handling form.
	/// </summary>
	public class ProblemDetectedForm : System.Windows.Forms.Form
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		private System.Windows.Forms.Label lblMessage;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnSkip;
		private System.Windows.Forms.Button btnRename;
		private System.Windows.Forms.Button btnRetry;
		private System.Windows.Forms.Button btnOverwrite;
		private System.Windows.Forms.Button btnOverwriteAll;
		private System.Windows.Forms.Button btnOverwriteOlder;
		private System.Windows.Forms.Button btnOverwriteDiffSize;
		private System.Windows.Forms.Button btnSkipAll;
		private System.Windows.Forms.Button btnFollowLink;

		// event arguments
		private FtpBatchTransferProblemDetectedEventArgs _arguments;

		// list of problem types the user chose to skip
		private readonly Hashtable _skipProblemTypes = new Hashtable();
		
		// true if the user chose to overwrite all existing files
		private bool _overwriteAll;

		// true if the user chose to overwrite all older files
		private bool _overwriteOlder;
		
		// true if the user chose to overwrite all files with a different size
		private bool _overwriteDifferentSize;

		public ProblemDetectedForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
		}

		/// <summary>
		/// Initialize the form for using it again in new batch transfer.
		/// </summary>
		/// <remarks>
		/// Clears the user's chosen default actions.
		/// </remarks>
		public void Initialize()
		{
			_skipProblemTypes.Clear();
			_overwriteAll = false;
			_overwriteOlder = false;
			_overwriteDifferentSize = false;
		}

		/// <summary>
		/// Shows the form as a modal dialog box.
		/// The form's buttons are shown according to possible actions taken from the given event argument.
		/// </summary>
		/// <param name="e">Event argument describing the type of a problem.</param>
		public void ShowModal(Control form, FtpBatchTransferProblemDetectedEventArgs e)
		{
			if (e == null)
				throw new ArgumentNullException("e");

			// if the user chose to skip this problem type, skip it
			if (_skipProblemTypes.ContainsKey(e.ProblemType))
			{
				e.Action = FtpBatchTransferAction.Skip;
				return;
			}

			// if the problem is FileExists check, whether the user have chosen any default action already
			if (e.ProblemType == FtpBatchTransferProblemType.FileExists)
			{
				if (_overwriteAll)
				{
					e.Action = FtpBatchTransferAction.Overwrite;
					return;
				}

				if (_overwriteDifferentSize)
				{
					e.Action = FtpBatchTransferAction.OverwriteIfDifferentSize;
					return;
				}

				if (_overwriteOlder)
				{
					e.Action = FtpBatchTransferAction.OverwriteIfOlder;
					return;
				}

				// format the text according to FtpTransferState (Downloading or Uploading)
				string messageFormat = "Overwrite file: {0}\n\t{1} Bytes, {2}\n\nwith file: {3}\n\t{4} Bytes, {5}";
				if (e.State == FtpTransferState.Downloading)
				{
					lblMessage.Text = string.Format(messageFormat,
						e.LocalPath, e.LocalFileLength, e.LocalFileModified,
						e.RemotePath, e.RemoteFileLength, e.RemoteFileModified);
				}
				else
				{
					lblMessage.Text = string.Format(messageFormat,
						e.RemotePath, e.RemoteFileLength, e.RemoteFileModified,
						e.LocalPath, e.LocalFileLength, e.LocalFileModified);
				}

				lblMessage.TextAlign = ContentAlignment.MiddleLeft;
			}
			else
			{
				lblMessage.Text = e.Exception.Message;
				lblMessage.TextAlign = ContentAlignment.MiddleCenter;
			}

			// store the event arguments for later use at button click handler
			_arguments = e;

			// only enable buttons that represent a possible action
			btnCancel.Enabled = e.IsActionPossible(FtpBatchTransferAction.ThrowException);
			btnSkip.Enabled = e.IsActionPossible(FtpBatchTransferAction.Skip);
			btnSkipAll.Enabled = e.IsActionPossible(FtpBatchTransferAction.Skip);
			btnRetry.Enabled = e.IsActionPossible(FtpBatchTransferAction.Retry);
			btnRename.Enabled = e.IsActionPossible(FtpBatchTransferAction.Rename);
			btnOverwrite.Enabled = e.IsActionPossible(FtpBatchTransferAction.Overwrite);
			btnOverwriteAll.Enabled = e.IsActionPossible(FtpBatchTransferAction.Overwrite);
			btnOverwriteOlder.Enabled = e.IsActionPossible(FtpBatchTransferAction.OverwriteIfOlder);
			btnOverwriteDiffSize.Enabled = e.IsActionPossible(FtpBatchTransferAction.OverwriteIfDifferentSize);
			btnFollowLink.Enabled = e.IsActionPossible(FtpBatchTransferAction.FollowLink);

			// select default action button
			SetDefaultActionButton(e.Action);

			ShowDialog(form);
		}

		/// <summary>
		/// Selects the button according to default action.
		/// </summary>
		/// <param name="action">Default FtpBatchTransferAction.</param>
		private void SetDefaultActionButton(FtpBatchTransferAction action)
		{
			switch (action)
			{
				case FtpBatchTransferAction.ThrowException: btnCancel.Select(); break;
				case FtpBatchTransferAction.Skip: btnSkipAll.Select(); break;
				case FtpBatchTransferAction.Retry: btnRetry.Select(); break;
				case FtpBatchTransferAction.Rename: btnRename.Select(); break;
				case FtpBatchTransferAction.Overwrite: btnOverwriteAll.Select(); break;
				case FtpBatchTransferAction.OverwriteIfOlder: btnOverwriteOlder.Select(); break;
				case FtpBatchTransferAction.OverwriteIfDifferentSize: btnOverwriteDiffSize.Select(); break;
				case FtpBatchTransferAction.FollowLink: btnFollowLink.Select(); break; 
			}			
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if ( disposing )
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnSkip = new System.Windows.Forms.Button();
			this.btnRename = new System.Windows.Forms.Button();
			this.btnRetry = new System.Windows.Forms.Button();
			this.btnOverwrite = new System.Windows.Forms.Button();
			this.btnOverwriteAll = new System.Windows.Forms.Button();
			this.btnOverwriteOlder = new System.Windows.Forms.Button();
			this.btnOverwriteDiffSize = new System.Windows.Forms.Button();
			this.btnSkipAll = new System.Windows.Forms.Button();
			this.btnFollowLink = new System.Windows.Forms.Button();
			this.lblMessage = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// btnCancel
			// 
			this.btnCancel.Location = new System.Drawing.Point(8, 112);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(128, 24);
			this.btnCancel.TabIndex = 0;
			this.btnCancel.Text = "&Cancel";
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// btnSkip
			// 
			this.btnSkip.Location = new System.Drawing.Point(296, 80);
			this.btnSkip.Name = "btnSkip";
			this.btnSkip.Size = new System.Drawing.Size(128, 24);
			this.btnSkip.TabIndex = 1;
			this.btnSkip.Text = "&Skip";
			this.btnSkip.Click += new System.EventHandler(this.btnSkip_Click);
			// 
			// btnRename
			// 
			this.btnRename.Location = new System.Drawing.Point(8, 144);
			this.btnRename.Name = "btnRename";
			this.btnRename.Size = new System.Drawing.Size(128, 24);
			this.btnRename.TabIndex = 2;
			this.btnRename.Text = "&Rename...";
			this.btnRename.Click += new System.EventHandler(this.btnRename_Click);
			// 
			// btnRetry
			// 
			this.btnRetry.Location = new System.Drawing.Point(296, 144);
			this.btnRetry.Name = "btnRetry";
			this.btnRetry.Size = new System.Drawing.Size(128, 24);
			this.btnRetry.TabIndex = 3;
			this.btnRetry.Text = "Re&try";
			this.btnRetry.Click += new System.EventHandler(this.btnRetry_Click);
			// 
			// btnOverwrite
			// 
			this.btnOverwrite.Location = new System.Drawing.Point(8, 80);
			this.btnOverwrite.Name = "btnOverwrite";
			this.btnOverwrite.Size = new System.Drawing.Size(128, 24);
			this.btnOverwrite.TabIndex = 4;
			this.btnOverwrite.Text = "&Overwrite";
			this.btnOverwrite.Click += new System.EventHandler(this.btnOverwrite_Click);
			// 
			// btnOverwriteAll
			// 
			this.btnOverwriteAll.Location = new System.Drawing.Point(144, 80);
			this.btnOverwriteAll.Name = "btnOverwriteAll";
			this.btnOverwriteAll.Size = new System.Drawing.Size(144, 24);
			this.btnOverwriteAll.TabIndex = 5;
			this.btnOverwriteAll.Text = "Overwrite &All";
			this.btnOverwriteAll.Click += new System.EventHandler(this.btnOverwriteAll_Click);
			// 
			// btnOverwriteOlder
			// 
			this.btnOverwriteOlder.Location = new System.Drawing.Point(144, 112);
			this.btnOverwriteOlder.Name = "btnOverwriteOlder";
			this.btnOverwriteOlder.Size = new System.Drawing.Size(144, 24);
			this.btnOverwriteOlder.TabIndex = 6;
			this.btnOverwriteOlder.Text = "Overwrite All Ol&der";
			this.btnOverwriteOlder.Click += new System.EventHandler(this.btnOverwriteOlder_Click);
			// 
			// btnOverwriteDiffSize
			// 
			this.btnOverwriteDiffSize.Location = new System.Drawing.Point(144, 144);
			this.btnOverwriteDiffSize.Name = "btnOverwriteDiffSize";
			this.btnOverwriteDiffSize.Size = new System.Drawing.Size(144, 24);
			this.btnOverwriteDiffSize.TabIndex = 7;
			this.btnOverwriteDiffSize.Text = "Overwrite &If Size Differs";
			this.btnOverwriteDiffSize.Click += new System.EventHandler(this.btnOverwriteDiffSize_Click);
			// 
			// btnSkipAll
			// 
			this.btnSkipAll.Location = new System.Drawing.Point(296, 112);
			this.btnSkipAll.Name = "btnSkipAll";
			this.btnSkipAll.Size = new System.Drawing.Size(128, 24);
			this.btnSkipAll.TabIndex = 8;
			this.btnSkipAll.Text = "S&kip All";
			this.btnSkipAll.Click += new System.EventHandler(this.btnSkipAll_Click);
			// 
			// btnFollowLink
			// 
			this.btnFollowLink.Location = new System.Drawing.Point(8, 176);
			this.btnFollowLink.Name = "btnFollowLink";
			this.btnFollowLink.Size = new System.Drawing.Size(128, 24);
			this.btnFollowLink.TabIndex = 9;
			this.btnFollowLink.Text = "Follow &Link";
			this.btnFollowLink.Click += new System.EventHandler(this.btnFollowLink_Click);
			// 
			// lblMessage
			// 
			this.lblMessage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.lblMessage.Location = new System.Drawing.Point(8, 8);
			this.lblMessage.Name = "lblMessage";
			this.lblMessage.Size = new System.Drawing.Size(424, 64);
			this.lblMessage.TabIndex = 11;
			this.lblMessage.Text = "Message";
			this.lblMessage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// ProblemDetectedForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(434, 208);
			this.Controls.Add(this.lblMessage);
			this.Controls.Add(this.btnFollowLink);
			this.Controls.Add(this.btnSkipAll);
			this.Controls.Add(this.btnOverwriteDiffSize);
			this.Controls.Add(this.btnOverwriteOlder);
			this.Controls.Add(this.btnOverwriteAll);
			this.Controls.Add(this.btnOverwrite);
			this.Controls.Add(this.btnRetry);
			this.Controls.Add(this.btnRename);
			this.Controls.Add(this.btnSkip);
			this.Controls.Add(this.btnCancel);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Name = "ProblemDetectedForm";
			this.ShowInTaskbar = false;
			this.Text = "Transfer Problem Detected";
			this.ResumeLayout(false);

		}
		#endregion

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			_arguments.Action = FtpBatchTransferAction.ThrowException;
			Close();
		}

		private void btnSkip_Click(object sender, System.EventArgs e)
		{
			_arguments.Action = FtpBatchTransferAction.Skip;
			Close();
		}

		private void btnSkipAll_Click(object sender, System.EventArgs e)
		{
			// add the problem to the table of automatically skipped problems
			_skipProblemTypes.Add(_arguments.ProblemType, null);

			_arguments.Action = FtpBatchTransferAction.Skip;
			Close();
		}

		private void btnRetry_Click(object sender, System.EventArgs e)
		{
			_arguments.Action = FtpBatchTransferAction.Retry;
			Close();
		}

		private void btnRename_Click(object sender, System.EventArgs e)
		{
			// initialize the renaming form
			NewNameForm formNewName = new NewNameForm();

			// set the current file name
			string oldName = Path.GetFileName(_arguments.LocalPath);
			formNewName.FileName = oldName;

			// show the form
			DialogResult result = formNewName.ShowDialog(this);

			// get the new name
			string newName = formNewName.FileName;

			// check whether the user clicked on OK and insert something nonempy and something else
			if (result != DialogResult.OK || newName.Length == 0 || newName == oldName)
				return;

			// set the appropriate action and new name to the event arguments
			_arguments.Action = FtpBatchTransferAction.Rename;
			_arguments.NewName = newName;
			Close();
		}

		private void btnOverwrite_Click(object sender, System.EventArgs e)
		{
			_arguments.Action = FtpBatchTransferAction.Overwrite;
			Close();
		}

		private void btnOverwriteAll_Click(object sender, System.EventArgs e)
		{
			_arguments.Action = FtpBatchTransferAction.Overwrite;
			_overwriteAll = true;
			Close();
		}

		private void btnOverwriteOlder_Click(object sender, System.EventArgs e)
		{
			_arguments.Action = FtpBatchTransferAction.OverwriteIfOlder;
			_overwriteOlder = true;
			Close();
		}

		private void btnOverwriteDiffSize_Click(object sender, System.EventArgs e)
		{
			_arguments.Action = FtpBatchTransferAction.OverwriteIfDifferentSize;
			_overwriteDifferentSize = true;
			Close();
		}

		private void btnFollowLink_Click(object sender, System.EventArgs e)
		{
			_arguments.Action = FtpBatchTransferAction.FollowLink;
			Close();
		}
	}
}
