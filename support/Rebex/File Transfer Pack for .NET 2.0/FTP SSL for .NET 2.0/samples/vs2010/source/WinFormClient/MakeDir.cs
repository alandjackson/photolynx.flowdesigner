//  
//   Rebex Sample Code License
// 
//   Copyright (c) 2009, Rebex CR s.r.o. www.rebex.net, 
//   All rights reserved.
// 
//   Permission to use, copy, modify, and/or distribute this software for any
//   purpose with or without fee is hereby granted
// 
//   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//   EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//   HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
//   OTHER DEALINGS IN THE SOFTWARE.
// 

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Rebex.Samples.WinFormClient
{
	/// <summary>
	/// make dir dialog
	/// </summary>
	public class MakeDir : System.Windows.Forms.Form
	{
		private System.Windows.Forms.TextBox tbDir;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.Button btnCancel;
		private System.ComponentModel.Container components=null;

		private string m_strDirectory=null;        // directory name

		public string Directory
		{
			get {return m_strDirectory;}
			set {m_strDirectory=value;}
		}

		public MakeDir()
		{
			InitializeComponent();
		}

		// -------------------------------------------------------------------------
		/// <summary>
		/// clean up any resources being used
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if (disposing)
			{
				if (components!=null)
				{
						components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.tbDir = new System.Windows.Forms.TextBox();
			this.btnOK = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// tbDir
			// 
			this.tbDir.Location = new System.Drawing.Point(16, 16);
			this.tbDir.Name = "tbDir";
			this.tbDir.Size = new System.Drawing.Size(176, 20);
			this.tbDir.TabIndex = 0;
			this.tbDir.Text = "";
			// 
			// btnOK
			// 
			this.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnOK.Location = new System.Drawing.Point(200, 16);
			this.btnOK.Name = "btnOK";
			this.btnOK.TabIndex = 1;
			this.btnOK.Text = "OK";
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnCancel.Location = new System.Drawing.Point(280, 16);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.TabIndex = 2;
			this.btnCancel.Text = "Cancel";
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// MakeDir
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(368, 56);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.btnCancel,
																		  this.btnOK,
																		  this.tbDir});
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.KeyPreview = true;
			this.Name = "MakeDir";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Create folder";
			this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MakeDir_KeyPress);
			this.ResumeLayout(false);

		}
		#endregion

		// -------------------------------------------------------------------------
		/// <summary>
		/// form confirmation by clicking 'ok' button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnOK_Click(object sender, System.EventArgs e)
		{
			m_strDirectory=tbDir.Text;
			this.Close();
		}

		// -------------------------------------------------------------------------
		/// <summary>
		/// form cancellation
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}

		// -------------------------------------------------------------------------
		/// <summary>
		/// global key handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void MakeDir_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			// when 'enter' is pressed the form is cinfirmed

			if (e.KeyChar=='\r')
			{
				m_strDirectory=tbDir.Text;
				this.Close();
			}

			// when 'esc' is pressed the form is closed

			if ((int)(e.KeyChar)==Convert.ToChar(Keys.Escape))
			{
				this.Close();
			}
		}
	}
}
