'  
'   Rebex Sample Code License
' 
'   Copyright (c) 2009, Rebex CR s.r.o. www.rebex.net, 
'   All rights reserved.
' 
'   Permission to use, copy, modify, and/or distribute this software for any
'   purpose with or without fee is hereby granted
' 
'   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
'   EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
'   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
'   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
'   HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
'   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
'   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
'   OTHER DEALINGS IN THE SOFTWARE.
' 

Imports System
Imports System.IO
Imports System.Threading
Imports Rebex.Net

Module GetPut

    ' This sample is a simple utility to upload/download a file to/from
    ' a FTP server using asynchronous methods of the Ftp class.
    Sub Main()
        Dim args() As String = Environment.GetCommandLineArgs()
        Dim method As String = Nothing

        If args.Length = 6 Then
            Select Case args(1).ToLower()
                Case "get"
                    method = "get"
                Case "put"
                    method = "put"
            End Select
        End If

        If method Is Nothing Then
            ' display syntax if wrong number of parameters or unsupported operation
            Console.WriteLine("GetPutAsync - uploads or downloads a file from a FTP server")
            Console.WriteLine("Syntax: GetPutAsync get|put unsecure|secure|implicit hostname remotepath localpath")
            Console.WriteLine("Example: GetPutAsync get unsecure ftp.microsoft.com /developr/readme.txt readme.txt")
            Return
        End If

        Dim security As FtpSecurity = DirectCast(System.Enum.Parse(GetType(FtpSecurity), args(2), True), FtpSecurity)
        Dim hostname As String = args(3)
        Dim remotePath As String = args(4)
        Dim localPath As String = args(5)

        ' create Ftp object and set event handlers
        Dim ftp As New Ftp
        AddHandler ftp.TransferProgress, AddressOf TransferProgress
        AddHandler ftp.ResponseRead, AddressOf ResponseRead
        AddHandler ftp.CommandSent, AddressOf CommandSent

        Try
            ' set SSL parameters to accept all server certificates...
            ' do not do this in production code, server certificates should
            ' be verified - use CertificateVerifier.Default instead
            Dim par As TlsParameters = New TlsParameters
            par.CommonName = hostname
            par.CertificateVerifier = CertificateVerifier.AcceptAll

            ' connect to the server
            ftp.Connect(hostname, Ftp.DefaultPort, par, security)


            ' ask for username and password and login
            Console.Write("Username: ")
            Dim username As String = Console.ReadLine()
            Console.Write("Password: ")
            Dim password As String = Console.ReadLine()
            ftp.Login(username, password)
            Console.WriteLine()

            ' begin asynchronous transfer
            Dim asyncResult As IAsyncResult
            If method = "get" Then
                asyncResult = ftp.BeginGetFile(remotePath, localPath, New AsyncCallback(AddressOf MyCallback), Nothing)
            Else
                asyncResult = ftp.BeginPutFile(localPath, remotePath, New AsyncCallback(AddressOf MyCallback), Nothing)
            End If

            ' do something else here...

            ' wait for the transfer to end
            asyncResult.AsyncWaitHandle.WaitOne()

            ' get the result
            Dim bytes As Long
            If method = "get" Then
                bytes = ftp.EndGetFile(asyncResult)
            Else
                bytes = ftp.EndPutFile(asyncResult)
            End If
            ' disconnect from the server
            ftp.Disconnect()
        Catch e As FtpException
            ' output error description to the console
            Console.WriteLine("'{0}' error: {1}", e.Status, e)
        Finally
            ' release all resources
            ftp.Dispose()
        End Try
    End Sub

    Public Sub MyCallback(ByVal asyncResult As IAsyncResult)
        Console.WriteLine(ControlChars.Lf + "Transfer finished.")
    End Sub

    Public Sub TransferProgress(ByVal sender As Object, ByVal e As FtpTransferProgressEventArgs)
        If e.State <> FtpTransferState.None Then
            Console.WriteLine(ControlChars.Cr + "Transferred {0} bytes...", e.BytesTransferred)
        End If
    End Sub

    Public Sub ResponseRead(ByVal sender As Object, ByVal e As FtpResponseReadEventArgs)
        Console.WriteLine(e.Response)
    End Sub

    Public Sub CommandSent(ByVal sender As Object, ByVal e As FtpCommandSentEventArgs)
        Console.WriteLine("-> " + e.Command)
    End Sub

End Module
