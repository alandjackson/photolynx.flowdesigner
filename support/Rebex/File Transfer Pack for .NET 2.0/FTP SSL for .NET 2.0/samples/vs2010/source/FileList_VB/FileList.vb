'  
'   Rebex Sample Code License
' 
'   Copyright (c) 2009, Rebex CR s.r.o. www.rebex.net, 
'   All rights reserved.
' 
'   Permission to use, copy, modify, and/or distribute this software for any
'   purpose with or without fee is hereby granted
' 
'   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
'   EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
'   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
'   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
'   HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
'   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
'   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
'   OTHER DEALINGS IN THE SOFTWARE.
' 

Imports System
Imports System.Net
Imports Rebex.Net

Module FileList

    ' This sample retrieves the list of files from a FTP server.
    ' It demonstrates the usage of FtpList class.
    Sub Main()
        Dim args() As String = Environment.GetCommandLineArgs()
        If args.Length <> 2 Then
            ' display syntax if wrong number of parameters or unsupported operation
            Console.WriteLine("FileList - retrieves file list from a FTP server")
            Console.WriteLine("Syntax: FileList [username:[password]@]hostname/remotepath")
            Console.WriteLine("Example: FileList ftp.rebex.net/pub/example/")
            Return
        End If

        ' get username and password from URI
        Dim uri As New Uri("ftp://" + args(1))

        Dim username As String
        Dim password As String
        If uri.UserInfo.Length = 0 Then
            Console.Write("Username: ")
            username = Console.ReadLine()
            Console.Write("Password: ")
            password = Console.ReadLine()
            If password.Length = 0 Then
                password = Nothing
            End If
        Else
            Dim userInfo As String() = uri.UserInfo.Split(":"c)
            username = userInfo(0)
            If userInfo.Length > 1 Then
                password = userInfo(1)
            Else
                Console.Write("Password: ")
                password = Console.ReadLine()
            End If
        End If

        ' create Ftp object and connect to the server
        Dim ftp As New Ftp
        ftp.Connect(uri.Host, uri.Port)

        Try
            ftp.Secure()
            Console.WriteLine("Connection is secured using TLS/SSL.")
        Catch ex As TlsException
            Console.WriteLine("Connection is not secure: {0}", ex.Message)
        End Try

        ' login with username and password
        ftp.Login(username, password)

        ftp.ChangeDirectory(uri.AbsolutePath)
        Dim list As FtpList = ftp.GetList()

        Dim i As Integer
        For i = 0 To list.Count - 1
            Dim item As FtpItem = list(i)
            If item.IsSymlink Then
                Console.Write("s ")
            ElseIf item.IsDirectory Then
                Console.Write("d ")
            Else
                Console.Write("- ")
            End If
            Console.Write(item.Modified.ToString("u").Substring(0, 16))
            Console.Write(item.Size.ToString().PadLeft(10, " "c))
            Console.Write(" {0}", item.Name)
            If item.IsSymlink Then
                Console.Write((" -> " + item.SymlinkPath))
            End If
            Console.WriteLine()
        Next i

        ' disconnect from the server
        ftp.Disconnect()
    End Sub

End Module
