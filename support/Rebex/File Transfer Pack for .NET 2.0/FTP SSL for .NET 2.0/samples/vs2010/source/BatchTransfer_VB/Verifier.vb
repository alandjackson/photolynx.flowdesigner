'  
'   Rebex Sample Code License
' 
'   Copyright (c) 2009, Rebex CR s.r.o. www.rebex.net, 
'   All rights reserved.
' 
'   Permission to use, copy, modify, and/or distribute this software for any
'   purpose with or without fee is hereby granted
' 
'   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
'   EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
'   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
'   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
'   HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
'   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
'   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
'   OTHER DEALINGS IN THE SOFTWARE.
' 

Imports System
Imports System.Collections
Imports System.Text
Imports Rebex.Net
Imports Rebex.Security.Certificates


Public Class Verifier
    Implements ICertificateVerifier

    Private Shared ReadOnly acceptedCertificates As Hashtable = New Hashtable()

    Public Function Verify(ByVal socket As TlsSocket, ByVal commonName As String, ByVal certificateChain As CertificateChain) As TlsCertificateAcceptance Implements ICertificateVerifier.Verify

        Dim res As ValidationResult = certificateChain.Validate(commonName, 0)

        If res.Valid Then
            Return TlsCertificateAcceptance.Accept
        End If
        Dim status As ValidationStatus = res.Status

        Dim values As ValidationStatus() = CType([Enum].GetValues(GetType(ValidationStatus)), ValidationStatus())

        Dim showAddIssuerCaToTrustedCaStore As Boolean
        showAddIssuerCaToTrustedCaStore = False
        Dim sb As New StringBuilder
        Dim i As Integer
        For i = 0 To values.Length - 1
            If (status And values(i)) <> 0 Then
                status = status Xor values(i)

                Dim problem As String

                Select Case values(i)
                    Case ValidationStatus.TimeNotValid
                        problem = "Server certificate has expired or is not valid yet."
                    Case ValidationStatus.Revoked
                        problem = "Server certificate has been revoked."
                    Case ValidationStatus.UnknownCa
                        problem = "Server certificate was issued by an unknown authority."
                    Case ValidationStatus.RootNotTrusted
						problem = "Server certificate was issued by an untrusted authority."
						If Not (certificateChain.RootCertificate Is Nothing) Then
							showAddIssuerCaToTrustedCaStore = True
						End If
					Case ValidationStatus.IncompleteChain
						problem = "Server certificate does not chain up to a trusted root authority."
					Case ValidationStatus.Malformed
						problem = "Server certificate is malformed."
					Case ValidationStatus.CnNotMatch
						problem = "Server hostname does not match the certificate."
					Case ValidationStatus.UnknownError
						problem = String.Format("Error {0:x} encountered while validating server's certificate.", res.ErrorCode)
					Case Else
						problem = values(i).ToString()
				End Select

				sb.AppendFormat("{0}" + ControlChars.Cr + ControlChars.Lf, problem)
			End If
        Next i

        Dim cert As Certificate = certificateChain.LeafCertificate

        Dim certFingerprint As String = BitConverter.ToString(cert.GetCertHash())

        If acceptedCertificates.Contains(certFingerprint) Then
            Return TlsCertificateAcceptance.Accept
        End If

        Dim certForm As New VerifierForm
        certForm.Problem = sb.ToString()
        certForm.Hostname = cert.GetCommonName()
        certForm.Subject = cert.GetSubjectName()
        certForm.Issuer = cert.GetIssuerName()
        certForm.ShowAddIssuerToTrustedCaStoreButton = showAddIssuerCaToTrustedCaStore
        certForm.ValidFrom = cert.GetEffectiveDate().ToString()
        certForm.ValidTo = cert.GetExpirationDate().ToString()

        certForm.ShowDialog()

        ' add certiticate of the issuer CA to truster authorities store
        If certForm.AddIssuerCertificateAuthothorityToTrustedCaStore Then
            Dim trustedCaStore As New CertificateStore(CertificateStoreName.Root)
            Dim rootCertificate As Certificate
            rootCertificate = certificateChain.RootCertificate

            trustedCaStore.AddCertificate(rootCertificate)
        End If

        If certForm.Accepted Then
            acceptedCertificates.Add(certFingerprint, cert)
            Return TlsCertificateAcceptance.Accept
        End If

        If (res.Status And ValidationStatus.TimeNotValid) <> 0 Then
            Return TlsCertificateAcceptance.Expired
        End If
        If (res.Status And ValidationStatus.Revoked) <> 0 Then
            Return TlsCertificateAcceptance.Revoked
        End If
        If (res.Status And (ValidationStatus.UnknownCa Or ValidationStatus.RootNotTrusted Or ValidationStatus.IncompleteChain)) <> 0 Then
            Return TlsCertificateAcceptance.UnknownAuthority
        End If
        If (res.Status And (ValidationStatus.Malformed Or ValidationStatus.UnknownError)) <> 0 Then
            Return TlsCertificateAcceptance.Other
        End If
        Return TlsCertificateAcceptance.Bad
    End Function 'Verify
End Class 'Verifier 
