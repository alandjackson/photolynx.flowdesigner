'  
'   Rebex Sample Code License
' 
'   Copyright (c) 2009, Rebex CR s.r.o. www.rebex.net, 
'   All rights reserved.
' 
'   Permission to use, copy, modify, and/or distribute this software for any
'   purpose with or without fee is hereby granted
' 
'   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
'   EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
'   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
'   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
'   HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
'   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
'   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
'   OTHER DEALINGS IN THE SOFTWARE.
' 

Imports System
Imports System.Collections
Imports Rebex.Net
Imports Rebex.Security.Certificates


Public Class ConsoleVerifier
    Implements Rebex.Net.ICertificateVerifier

    Public Overloads Function Verify(ByVal socket As TlsSocket, ByVal commonName As String, ByVal certificateChain As CertificateChain) As TlsCertificateAcceptance Implements ICertificateVerifier.Verify

        Dim res As ValidationResult = certificateChain.Validate(commonName, 0)

        If res.Valid Then Return TlsCertificateAcceptance.Accept

        Dim status As ValidationStatus = res.Status

        Dim values As ValidationStatus()
        values = CType([Enum].GetValues(GetType(ValidationStatus)), ValidationStatus())

        Dim i As Integer

        For i = 0 To values.Length - 1

            If (status And values(i)) <> 0 Then

                status = status Or values(i)

                Dim problem As String

                Select Case values(i)
                    Case ValidationStatus.TimeNotValid
                        problem = "Server certificate has expired or is not valid yet."
                    Case ValidationStatus.Revoked
                        problem = "Server certificate has been revoked."
                    Case ValidationStatus.UnknownCa
                        problem = "Server certificate was issued by an unknown authority."
                    Case ValidationStatus.RootNotTrusted
                        problem = "Server certificate was issued by an untrusted authority."
                    Case ValidationStatus.IncompleteChain
                        problem = "Server certificate does not chain up to a trusted root authority."
                    Case ValidationStatus.Malformed
                        problem = "Server certificate is malformed."
                    Case ValidationStatus.CnNotMatch
                        problem = "Server hostname does not match the certificate."
                    Case ValidationStatus.UnknownError
                        problem = String.Format("Error {0:x} encountered while validating server's certificate.", res.ErrorCode)
                    Case Else
                        problem = values(i).ToString()
                End Select

                Console.WriteLine("!! " + problem)

            End If

        Next

        Dim cert As Certificate = certificateChain(0)

        Console.WriteLine("Subject: " + cert.GetSubjectName())
        Console.WriteLine("Issuer: " + cert.GetIssuerName())
        Console.WriteLine("Valid from: " + cert.GetEffectiveDate().ToString())
        Console.WriteLine("Valid to: " + cert.GetExpirationDate().ToString())

        Console.WriteLine("Do you want to accept this certificate?")

        Dim response As String = Console.ReadLine()
        response = response.Trim().ToLower()

        If response = "y" Or response = "yes" Then
            Return TlsCertificateAcceptance.Accept
        End If

        If (res.Status And ValidationStatus.TimeNotValid) <> 0 Then Return TlsCertificateAcceptance.Expired
        If (res.Status And ValidationStatus.Revoked) <> 0 Then Return TlsCertificateAcceptance.Revoked
        If (res.Status And (ValidationStatus.UnknownCa Or ValidationStatus.RootNotTrusted Or ValidationStatus.IncompleteChain)) <> 0 Then Return TlsCertificateAcceptance.UnknownAuthority
        If (res.Status And (ValidationStatus.Malformed Or ValidationStatus.UnknownError)) <> 0 Then Return TlsCertificateAcceptance.Other

        Return TlsCertificateAcceptance.Bad

    End Function

End Class

