'  
'   Rebex Sample Code License
' 
'   Copyright (c) 2009, Rebex CR s.r.o. www.rebex.net, 
'   All rights reserved.
' 
'   Permission to use, copy, modify, and/or distribute this software for any
'   purpose with or without fee is hereby granted
' 
'   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
'   EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
'   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
'   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
'   HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
'   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
'   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
'   OTHER DEALINGS IN THE SOFTWARE.
' 

''' <summary>
''' Miscellaneous constants.
''' </summary>
Public Module Constants
    Sub New()
    End Sub
    ''' <summary>
    ''' Directory delimiter.
    ''' </summary>
    Public Const PathDelimiter As Char = "/"c
End Module

''' <summary>
''' Page URL parameters.
''' </summary>
Public Module UrlParameter
    Sub New()
    End Sub
    ''' <summary>
    ''' URL parameter for current directory.
    ''' </summary>
    Public Const CurrentFolder As String = "currentDir"

    ''' <summary>
    ''' URL parameter for command type.
    ''' </summary>
    Public Const Command As String = "command"

    ''' <summary>
    ''' First URL parameter for command argument value.
    ''' </summary>
    Public Const UrlCommandArgument1 As String = "cmdArg1"

    ''' <summary>
    ''' Second URL parameter for command argument value.
    ''' </summary>
    Public Const UrlCommandArgument2 As String = "cmdArg2"

    ''' <summary>
    ''' URL parameter for return URL when logging in.
    ''' </summary>
    Public Const ReturnUrl As String = "returnUrl"
End Module

''' <summary>
''' Values for <see cref="UrlParameter.Command"/> web page argument.
''' </summary>
Public Module UrlParameterCommand
    Sub New()
    End Sub
    ''' <summary>
    ''' Lists current directory.
    ''' </summary>
    ''' <remarks>
    ''' Expects no argument. Lists <see cref="UrlParameter.CurrentFolder"/> directory.
    ''' </remarks>
    Public Const List As String = "list"

    ''' <summary>
    ''' Downloads a file.
    ''' </summary>
    ''' <remarks>
    ''' Expects <see cref="UrlParameter.UrlCommandArgument1"/> as an absolute path to the file.
    ''' </remarks>
    Public Const Download As String = "download"

    ''' <summary>
    ''' Deletes a file.
    ''' </summary>
    ''' <remarks>
    ''' Expects <see cref="UrlParameter.UrlCommandArgument1"/> as an absolute path to the file.
    ''' </remarks>
    Public Const DeleteFile As String = "deleteFile"

    ''' <summary>
    ''' Creates a new directory.
    ''' </summary>
    ''' <remarks>
    ''' Expects <see cref="UrlParameter.UrlCommandArgument1"/> as a name of the new directory.
    ''' </remarks>
    Public Const CreateDirectory As String = "createDirectory"

    ''' <summary>
    ''' Deletes a directory.
    ''' </summary>
    ''' <remarks>
    ''' Expects <see cref="UrlParameter.UrlCommandArgument1"/> as an absolute path to the directory.
    ''' </remarks>
    Public Const DeleteDirectory As String = "deleteDirectory"

    ''' <summary>
    ''' Renames file or directory.
    ''' </summary>
    ''' <remarks>
    ''' Expects <see cref="UrlParameter.UrlCommandArgument1"/> as a name of the original file.
    ''' Expects <see cref="UrlParameter.UrlCommandArgument2"/> as a new file name.
    ''' </remarks>
    Public Const Rename As String = "rename"

    ''' <summary>
    ''' Resolves a symlink.
    ''' </summary>
    ''' <remarks>
    ''' Expects <see cref="UrlParameter.UrlCommandArgument1"/> as an absolute path to the symlink.
    ''' </remarks>
    Public Const Symlink As String = "symlink"

    ''' <summary>
    ''' Clears saved connection information.
    ''' </summary>
    ''' <remarks>
    ''' Expects no argument.
    ''' </remarks>
    Public Const LogOut As String = "logout"
End Module

''' <summary>
''' Determines the image to show.
''' </summary>
Public Enum IconType
    File
    Folder
    Symlink
    Rename
    Delete
    DirectoryUp
    AddFolder
    UploadFile
    Refresh
End Enum
