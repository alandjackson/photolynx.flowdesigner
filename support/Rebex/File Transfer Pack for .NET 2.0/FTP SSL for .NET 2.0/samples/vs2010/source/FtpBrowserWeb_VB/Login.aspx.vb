'  
'   Rebex Sample Code License
' 
'   Copyright (c) 2009, Rebex CR s.r.o. www.rebex.net, 
'   All rights reserved.
' 
'   Permission to use, copy, modify, and/or distribute this software for any
'   purpose with or without fee is hereby granted
' 
'   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
'   EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
'   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
'   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
'   HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
'   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
'   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
'   OTHER DEALINGS IN THE SOFTWARE.
' 

Imports System
Imports System.Web.UI
Imports Rebex.Net

Partial Public Class Login
    Inherits Page
    ''' <summary>
    ''' Gets the return URL for redirect when connect was successful.
    ''' </summary>
    Private ReadOnly Property ReturnUrl() As String
        Get
            If Not String.IsNullOrEmpty(Request(UrlParameter.ReturnUrl)) Then
                Return Request(UrlParameter.ReturnUrl)
            End If

            Return String.Format("Default.aspx?{0}={1}", UrlParameter.Command, UrlParameterCommand.List)
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        ' if LogOut command was specified
        If Request(UrlParameter.Command) = UrlParameterCommand.LogOut Then
            Logout()
        End If

        If Not Page.IsPostBack Then
			txtPort.Text = Ftp.DefaultPort.ToString()
			' fill the Security DropDownList
			ddlSecurity.Items.Add(New ListItem("No security", [Enum].GetName(GetType(FtpSecurity), FtpSecurity.Unsecure)))
			ddlSecurity.Items.Add(New ListItem("Explicit TLS or SSL", [Enum].GetName(GetType(FtpSecurity), FtpSecurity.Explicit)))
			ddlSecurity.Items.Add(New ListItem("Implicit TLS or SSL", [Enum].GetName(GetType(FtpSecurity), FtpSecurity.Implicit)))
            ddlSecurity.DataBind()
            trSecurity.Visible = True
        End If
    End Sub

    ''' <summary>
    ''' Handles the 'Connect' button click.
    ''' </summary>
    Protected Sub Connect(ByVal sender As Object, ByVal e As EventArgs)
        CheckAndSaveConnectionInfo()
    End Sub

    ''' <summary>
    ''' Checks entered connection values.
    ''' If connection values are valid (it is possible to connect and log in to the remote server) saves them and 
    ''' redirects to the URL specified in HttpRequest or the Default URL.
    ''' </summary>
    Private Sub CheckAndSaveConnectionInfo()
        Dim connectionInfo As New ConnectionInfo()

        ' set connection info values
        connectionInfo.ServerName = txtServerName.Text
        connectionInfo.Port = Convert.ToInt32(txtPort.Text)
        connectionInfo.UserName = txtUserName.Text
        connectionInfo.Password = txtPassword.Text
		connectionInfo.Security = DirectCast([Enum].Parse(GetType(FtpSecurity), ddlSecurity.SelectedValue, True), FtpSecurity)
		connectionInfo.AcceptAllCertificates = chkAcceptAllCertificates.Checked

        ' create new instance of a Ftp class
        Dim ftp As New Ftp()

        Try
            ' connect to the FTP server
			Dim pars As New TlsParameters()

			If (chkAcceptAllCertificates.Checked) Then
				pars.CertificateVerifier = CertificateVerifier.AcceptAll
			Else
				pars.CertificateVerifier = CertificateVerifier.Default
			End If

			ftp.Connect(connectionInfo.ServerName, connectionInfo.Port, pars, connectionInfo.Security)

            ' log in
            ftp.Login(connectionInfo.UserName, connectionInfo.Password)

            ' disconnect from the FTP server
            ftp.Disconnect()

            ' connection was successful, save the connection information
            connectionInfo.Save()

            ' redirect to the original URL
            Response.Redirect(ReturnUrl)
        Catch ex As FtpException
            ' show error message
			lblError.Text = ex.Message
        Catch ex As TlsException
            ' show error message
            lblError.Text = ex.Message
		Catch ex As Exception
			' show error message
			lblError.Text = ex.Message
		Finally
			' dispose the Ftp object
			ftp.Dispose()
        End Try
    End Sub

    ''' <summary>
    ''' Clears connection information.
    ''' </summary>
    ''' <remarks>
    ''' In ASP.NET environment we have to perform ConnectToFtp - ExecuteCommnand - DisconnectFromFtp sequence on each request
    ''' because FTP connection will not survive between seaparate requests.
    ''' </remarks>
    Private Sub Logout()
        ' delete connection from session
        ConnectionInfo.Clear()
    End Sub
End Class
