'  
'   Rebex Sample Code License
' 
'   Copyright (c) 2009, Rebex CR s.r.o. www.rebex.net, 
'   All rights reserved.
' 
'   Permission to use, copy, modify, and/or distribute this software for any
'   purpose with or without fee is hereby granted
' 
'   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
'   EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
'   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
'   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
'   HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
'   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
'   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
'   OTHER DEALINGS IN THE SOFTWARE.
' 

Imports System
Imports System.IO
Imports System.Drawing
Imports System.Collections
Imports System.ComponentModel
Imports System.Windows.Forms
Imports Rebex.Net


''' <summary>
''' Problem handling form.
''' </summary>
Public Class ProblemDetectedForm
    Inherits System.Windows.Forms.Form


    ' event arguments
    Private _arguments As FtpBatchTransferProblemDetectedEventArgs

    ' list of problem types the user chose to skip
    Private ReadOnly _skipProblemTypes As New Hashtable

    ' true if the user chose to overwrite all existing files
    Private _overwriteAll As Boolean

    ' true if the user chose to overwrite all older files
    Private _overwriteOlder As Boolean

    ' true if the user chose to overwrite all files with a different size
    Private _overwriteDifferentSize As Boolean

    ''' <summary>
    ''' Initialize the form for using it again in new batch transfer.
    ''' </summary>
    ''' <remarks>
    ''' Clears the user's chosen default actions.
    ''' </remarks>
    Public Sub Initialize()
        _skipProblemTypes.Clear()
        _overwriteAll = False
        _overwriteOlder = False
        _overwriteDifferentSize = False
    End Sub

    ''' <summary>
    ''' Shows the form as a modal dialog box.
    ''' The form's buttons are shown according to possible actions taken from the given event argument.
    ''' </summary>
    ''' <param name="e">Event argument describing the type of a problem.</param>
    Public Sub ShowModal(ByVal form As Control, ByVal e As FtpBatchTransferProblemDetectedEventArgs)
        If e Is Nothing Then
            Throw New ArgumentNullException("e")
        End If

        ' if the user chose to skip this problem type, skip it
        If _skipProblemTypes.ContainsKey(e.ProblemType) Then
            e.Action = FtpBatchTransferAction.Skip
            Return
        End If

        ' if the problem is FileExists check, whether the user have chosen any default action already
        If e.ProblemType = FtpBatchTransferProblemType.FileExists Then
            If _overwriteAll Then
                e.Action = FtpBatchTransferAction.Overwrite
                Return
            End If

            If _overwriteDifferentSize Then
                e.Action = FtpBatchTransferAction.OverwriteIfDifferentSize
                Return
            End If

            If _overwriteOlder Then
                e.Action = FtpBatchTransferAction.OverwriteIfOlder
                Return
            End If

            ' format the text according to FtpTransferState (Downloading or Uploading)
            Dim messageFormat As String = "Overwrite file: {0}" & Chr(10) & "" & Chr(9) & "{1} Bytes, {2}" & Chr(10) & Chr(10) & "with file: {3}" & Chr(10) & "" & Chr(9) & "{4} Bytes, {5}"
            If e.State = FtpTransferState.Downloading Then
                lblMessage.Text = String.Format(messageFormat, e.LocalPath, e.LocalFileLength, e.LocalFileModified, e.RemotePath, e.RemoteFileLength, _
                 e.RemoteFileModified)
            Else
                lblMessage.Text = String.Format(messageFormat, e.RemotePath, e.RemoteFileLength, e.RemoteFileModified, e.LocalPath, e.LocalFileLength, _
                 e.LocalFileModified)
            End If

            lblMessage.TextAlign = ContentAlignment.MiddleLeft
        Else
            lblMessage.Text = e.Exception.Message
            lblMessage.TextAlign = ContentAlignment.MiddleCenter
        End If

        ' store the event arguments for later use at button click handler
        _arguments = e

        ' only enable buttons that represent a possible action
        btnCancel.Enabled = e.IsActionPossible(FtpBatchTransferAction.ThrowException)
        btnSkip.Enabled = e.IsActionPossible(FtpBatchTransferAction.Skip)
        btnSkipAll.Enabled = e.IsActionPossible(FtpBatchTransferAction.Skip)
        btnRetry.Enabled = e.IsActionPossible(FtpBatchTransferAction.Retry)
        btnRename.Enabled = e.IsActionPossible(FtpBatchTransferAction.Rename)
        btnOverwrite.Enabled = e.IsActionPossible(FtpBatchTransferAction.Overwrite)
        btnOverwriteAll.Enabled = e.IsActionPossible(FtpBatchTransferAction.Overwrite)
        btnOverwriteOlder.Enabled = e.IsActionPossible(FtpBatchTransferAction.OverwriteIfOlder)
        btnOverwriteDiffSize.Enabled = e.IsActionPossible(FtpBatchTransferAction.OverwriteIfDifferentSize)
        btnFollowLink.Enabled = e.IsActionPossible(FtpBatchTransferAction.FollowLink)

		' select default action button
		SetDefaultActionButton(e.Action)

        ShowDialog(form)
    End Sub

	' Selects the button according to default action.
	Private Sub SetDefaultActionButton(ByVal action As FtpBatchTransferAction)
		Select Case action
			Case FtpBatchTransferAction.ThrowException : btnCancel.Select()
			Case FtpBatchTransferAction.Skip : btnSkipAll.Select()
			Case FtpBatchTransferAction.Retry : btnRetry.Select()
			Case FtpBatchTransferAction.Rename : btnRename.Select()
			Case FtpBatchTransferAction.Overwrite : btnOverwriteAll.Select()
			Case FtpBatchTransferAction.OverwriteIfOlder : btnOverwriteOlder.Select()
			Case FtpBatchTransferAction.OverwriteIfDifferentSize : btnOverwriteDiffSize.Select()
			Case FtpBatchTransferAction.FollowLink : btnFollowLink.Select()
		End Select
	End Sub


#Region "Windows Form Designer generated code"
	Public Sub New()

		'This call is required by the Windows Form Designer.
		InitializeComponent()

		'Add any initialization after the InitializeComponent() call

	End Sub

	'Form overrides dispose to clean up the component list.
	Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If Not (components Is Nothing) Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(disposing)
	End Sub

	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.  
	'Do not modify it using the code editor.
	Friend WithEvents lblMessage As System.Windows.Forms.Label
	Friend WithEvents btnCancel As System.Windows.Forms.Button
	Friend WithEvents btnSkip As System.Windows.Forms.Button
	Friend WithEvents btnRename As System.Windows.Forms.Button
	Friend WithEvents btnRetry As System.Windows.Forms.Button
	Friend WithEvents btnOverwrite As System.Windows.Forms.Button
	Friend WithEvents btnOverwriteAll As System.Windows.Forms.Button
	Friend WithEvents btnOverwriteOlder As System.Windows.Forms.Button
	Friend WithEvents btnOverwriteDiffSize As System.Windows.Forms.Button
	Friend WithEvents btnSkipAll As System.Windows.Forms.Button
	Friend WithEvents btnFollowLink As System.Windows.Forms.Button
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.btnCancel = New System.Windows.Forms.Button
        Me.btnSkip = New System.Windows.Forms.Button
        Me.btnRename = New System.Windows.Forms.Button
        Me.btnRetry = New System.Windows.Forms.Button
        Me.btnOverwrite = New System.Windows.Forms.Button
        Me.btnOverwriteAll = New System.Windows.Forms.Button
        Me.btnOverwriteOlder = New System.Windows.Forms.Button
        Me.btnOverwriteDiffSize = New System.Windows.Forms.Button
        Me.btnSkipAll = New System.Windows.Forms.Button
        Me.btnFollowLink = New System.Windows.Forms.Button
        Me.lblMessage = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(8, 112)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(128, 24)
        Me.btnCancel.TabIndex = 0
        Me.btnCancel.Text = "&Cancel"
        '
        'btnSkip
        '
        Me.btnSkip.Location = New System.Drawing.Point(296, 80)
        Me.btnSkip.Name = "btnSkip"
        Me.btnSkip.Size = New System.Drawing.Size(128, 24)
        Me.btnSkip.TabIndex = 1
        Me.btnSkip.Text = "&Skip"
        '
        'btnRename
        '
        Me.btnRename.Location = New System.Drawing.Point(8, 144)
        Me.btnRename.Name = "btnRename"
        Me.btnRename.Size = New System.Drawing.Size(128, 24)
        Me.btnRename.TabIndex = 2
        Me.btnRename.Text = "&Rename..."
        '
        'btnRetry
        '
        Me.btnRetry.Location = New System.Drawing.Point(296, 144)
        Me.btnRetry.Name = "btnRetry"
        Me.btnRetry.Size = New System.Drawing.Size(128, 24)
        Me.btnRetry.TabIndex = 3
        Me.btnRetry.Text = "Re&try"
        '
        'btnOverwrite
        '
        Me.btnOverwrite.Location = New System.Drawing.Point(8, 80)
        Me.btnOverwrite.Name = "btnOverwrite"
        Me.btnOverwrite.Size = New System.Drawing.Size(128, 24)
        Me.btnOverwrite.TabIndex = 4
        Me.btnOverwrite.Text = "&Overwrite"
        '
        'btnOverwriteAll
        '
        Me.btnOverwriteAll.Location = New System.Drawing.Point(144, 80)
        Me.btnOverwriteAll.Name = "btnOverwriteAll"
        Me.btnOverwriteAll.Size = New System.Drawing.Size(144, 24)
        Me.btnOverwriteAll.TabIndex = 5
        Me.btnOverwriteAll.Text = "Overwrite &All"
        '
        'btnOverwriteOlder
        '
        Me.btnOverwriteOlder.Location = New System.Drawing.Point(144, 112)
        Me.btnOverwriteOlder.Name = "btnOverwriteOlder"
        Me.btnOverwriteOlder.Size = New System.Drawing.Size(144, 24)
        Me.btnOverwriteOlder.TabIndex = 6
        Me.btnOverwriteOlder.Text = "Overwrite All Ol&der"
        '
        'btnOverwriteDiffSize
        '
        Me.btnOverwriteDiffSize.Location = New System.Drawing.Point(144, 144)
        Me.btnOverwriteDiffSize.Name = "btnOverwriteDiffSize"
        Me.btnOverwriteDiffSize.Size = New System.Drawing.Size(144, 24)
        Me.btnOverwriteDiffSize.TabIndex = 7
        Me.btnOverwriteDiffSize.Text = "Overwrite &If Size Differs"
        '
        'btnSkipAll
        '
        Me.btnSkipAll.Location = New System.Drawing.Point(296, 112)
        Me.btnSkipAll.Name = "btnSkipAll"
        Me.btnSkipAll.Size = New System.Drawing.Size(128, 24)
        Me.btnSkipAll.TabIndex = 8
        Me.btnSkipAll.Text = "S&kip All"
        '
        'btnFollowLink
        '
        Me.btnFollowLink.Location = New System.Drawing.Point(8, 176)
        Me.btnFollowLink.Name = "btnFollowLink"
        Me.btnFollowLink.Size = New System.Drawing.Size(128, 24)
        Me.btnFollowLink.TabIndex = 9
        Me.btnFollowLink.Text = "Follow &Link"
        '
        'lblMessage
        '
        Me.lblMessage.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblMessage.Location = New System.Drawing.Point(8, 8)
        Me.lblMessage.Name = "lblMessage"
        Me.lblMessage.Size = New System.Drawing.Size(424, 64)
        Me.lblMessage.TabIndex = 11
        Me.lblMessage.Text = "Message"
        Me.lblMessage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ProblemDetectedForm
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(434, 208)
        Me.Controls.Add(Me.lblMessage)
        Me.Controls.Add(Me.btnFollowLink)
        Me.Controls.Add(Me.btnSkipAll)
        Me.Controls.Add(Me.btnOverwriteDiffSize)
        Me.Controls.Add(Me.btnOverwriteOlder)
        Me.Controls.Add(Me.btnOverwriteAll)
        Me.Controls.Add(Me.btnOverwrite)
        Me.Controls.Add(Me.btnRetry)
        Me.Controls.Add(Me.btnRename)
        Me.Controls.Add(Me.btnSkip)
        Me.Controls.Add(Me.btnCancel)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "ProblemDetectedForm"
        Me.ShowInTaskbar = False
        Me.Text = "Transfer Problem Detected"
        Me.ResumeLayout(False)

    End Sub
#End Region

	Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
		_arguments.Action = FtpBatchTransferAction.ThrowException
		Close()
	End Sub

	Private Sub btnSkip_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSkip.Click
		_arguments.Action = FtpBatchTransferAction.Skip
		Close()
	End Sub

	Private Sub btnSkipAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSkipAll.Click
		' add the problem to the table of automatically skipped problems
		_skipProblemTypes.Add(_arguments.ProblemType, Nothing)

		_arguments.Action = FtpBatchTransferAction.Skip
		Close()
	End Sub

	Private Sub btnRetry_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRetry.Click
		_arguments.Action = FtpBatchTransferAction.Retry
		Close()
	End Sub

	Private Sub btnRename_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRename.Click
		' initialize the renaming form
		Dim formNewName As New NewNameForm

		' set the current file name
		Dim oldName As String = Path.GetFileName(_arguments.LocalPath)
		formNewName.FileName = oldName

		' show the form
		Dim result As DialogResult = formNewName.ShowDialog(Me)

		' get the new name
		Dim newName As String = formNewName.FileName

		' check whether the user clicked on OK and insert something nonempy and something else
		If result <> DialogResult.OK OrElse newName.Length = 0 OrElse newName = oldName Then
			Return
		End If

		' set the appropriate action and new name to the event arguments
		_arguments.Action = FtpBatchTransferAction.Rename
		_arguments.NewName = newName
		Close()
	End Sub

	Private Sub btnOverwrite_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOverwrite.Click
		_arguments.Action = FtpBatchTransferAction.Overwrite
		Close()
	End Sub

	Private Sub btnOverwriteAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOverwriteAll.Click
		_arguments.Action = FtpBatchTransferAction.Overwrite
		_overwriteAll = True
		Close()
	End Sub

	Private Sub btnOverwriteOlder_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOverwriteOlder.Click
		_arguments.Action = FtpBatchTransferAction.OverwriteIfOlder
		_overwriteOlder = True
		Close()
	End Sub

	Private Sub btnOverwriteDiffSize_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOverwriteDiffSize.Click
		_arguments.Action = FtpBatchTransferAction.OverwriteIfDifferentSize
		_overwriteDifferentSize = True
		Close()
	End Sub

	Private Sub btnFollowLink_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFollowLink.Click
		_arguments.Action = FtpBatchTransferAction.FollowLink
		Close()
	End Sub
End Class

