//  
//   Rebex Sample Code License
// 
//   Copyright (c) 2009, Rebex CR s.r.o. www.rebex.net, 
//   All rights reserved.
// 
//   Permission to use, copy, modify, and/or distribute this software for any
//   purpose with or without fee is hereby granted
// 
//   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//   EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//   HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
//   OTHER DEALINGS IN THE SOFTWARE.
// 

using System;
using System.IO;
using System.Net;
using Rebex.Net;

namespace Rebex.Samples.FtpWebRequest
{
	/// <summary>
	/// This sample demonstrates using FtpWebRequest class to download
	/// file from a FTP, HTTP or HTTPS server.
	/// </summary>
	class FtpRequest
	{

		[STAThread]
		static void Main(string[] args)
		{
			if (args.Length < 2)
			{
				// display syntax if wrong number of parameters
				Console.WriteLine("FtpWebRequest - download file from a Uri.");
				Console.WriteLine("Syntax: FtpWebRequest uri localpath");
				Console.WriteLine("Example: FtpWebRequest ftp://ftp.rebex.net/pub/example/readme.txt readme.txt [username [password]]");
				return;
			}

			// register the component for the FTP prefix
			WebRequest.RegisterPrefix ("ftp://", Rebex.Net.FtpWebRequest.Creator);

			// create web request for the given URI
			WebRequest request = WebRequest.Create (args[0]);

			// support for user name and password from arguments
			if(args.Length>2)
			{
				// local variables
				string userName="";
				string password="";
				
				switch(args.Length)
				{
					case 3 :
						userName = args[2];				// get the user name
						break;
					case 4 :
						userName = args[2];				// get the user name
						password = args[3];				// get the password
						break;
				}

				if(password.Trim()=="")
				{
					Console.Write("Password: ");
					password = Console.ReadLine();		// get the password	
				}
				// support for authentification
				request.Credentials = new NetworkCredential(userName, password);
			}
			
			// get and read web response
			WebResponse response = request.GetResponse();
			Stream stream = response.GetResponseStream();

			Stream local = File.Create (args[1]);

			byte[] buffer = new byte[1024];
			int n;

			Console.WriteLine("Downloading...");			
			
			do
			{
				n = stream.Read (buffer, 0, buffer.Length);
				local.Write (buffer, 0, n);
			} while (n > 0);

			// close both streams
			stream.Close();
			local.Close();

			Console.WriteLine("Download finished.");
		}
	}
}
