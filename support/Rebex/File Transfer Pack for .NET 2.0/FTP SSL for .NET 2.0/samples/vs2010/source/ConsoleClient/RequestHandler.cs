//  
//   Rebex Sample Code License
// 
//   Copyright (c) 2009, Rebex CR s.r.o. www.rebex.net, 
//   All rights reserved.
// 
//   Permission to use, copy, modify, and/or distribute this software for any
//   purpose with or without fee is hereby granted
// 
//   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//   EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//   HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
//   OTHER DEALINGS IN THE SOFTWARE.
// 

using System;
using Rebex.Net;
using Rebex.Security.Certificates;

namespace Rebex.Samples.ConsoleClient
{
	public class RequestHandler : ICertificateRequestHandler
	{
		public CertificateChain Request(TlsSocket socket, DistinguishedName[] issuers)
		{
			CertificateStore my = new CertificateStore ("MY");
			Certificate[] certs;

			if (issuers.Length > 0)
				certs = my.FindCertificates
					(
					issuers,
					CertificateFindOptions.IsTimeValid |
					CertificateFindOptions.HasPrivateKey |
					CertificateFindOptions.ClientAuthentication
					);
			else
				certs = my.FindCertificates
					(
					CertificateFindOptions.IsTimeValid |
					CertificateFindOptions.HasPrivateKey |
					CertificateFindOptions.ClientAuthentication
					);

			if (certs.Length == 0)
				return null;

			Console.WriteLine("Select certificate:");
			for (int i = 0; i <= certs.Length; i++) 
			{
				if (i == 0) 
				{
					Console.WriteLine(string.Format("{0}. [Nothing, skip this step]", i));
					continue;
				}

				Console.WriteLine(string.Format("{0}. {1}", i, certs[i-1].GetSubjectName()));
			}

			while (true) 
			{
				Console.Write(string.Format("Select certificate [0 - {0}]: ", certs.Length));

				int certIndex = -1;

				try 
				{
					certIndex = int.Parse(Console.ReadLine());
				} 
				catch 
				{
					Console.WriteLine("ERROR: Wrong input!");
					continue;
				}

				if (certIndex > 0 && certIndex <= certs.Length) 
				{
					return CertificateChain.BuildFrom(certs[certIndex]);
				}
				else 
				{
					if (certIndex == 0)
						break;

					Console.WriteLine(string.Format("ERROR: You must enter number between 0 and {0}.", certs.Length));
				}
			}

			return null;
		}
	}
}
