'  
'   Rebex Sample Code License
' 
'   Copyright (c) 2009, Rebex CR s.r.o. www.rebex.net, 
'   All rights reserved.
' 
'   Permission to use, copy, modify, and/or distribute this software for any
'   purpose with or without fee is hereby granted
' 
'   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
'   EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
'   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
'   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
'   HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
'   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
'   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
'   OTHER DEALINGS IN THE SOFTWARE.
' 


Imports System
Imports System.Drawing
Imports System.Collections
Imports System.ComponentModel
Imports System.Windows.Forms
Imports System.Xml
Imports System.IO
Imports Rebex.Net

' -------------------------------------------------------------------------
'' <summary>
'' connection dialog
'' </summary>
Public Class Connection
    Inherits System.Windows.Forms.Form
    Private components As System.ComponentModel.Container = Nothing
    Private groupBox1 As System.Windows.Forms.GroupBox
    Private cbPassive As System.Windows.Forms.CheckBox
    Private tbPassword As System.Windows.Forms.TextBox
    Private label4 As System.Windows.Forms.Label
    Private tbLogin As System.Windows.Forms.TextBox
    Private label3 As System.Windows.Forms.Label
    Private tbPort As System.Windows.Forms.TextBox
    Private label2 As System.Windows.Forms.Label
    Private tbHost As System.Windows.Forms.TextBox
    Private label1 As System.Windows.Forms.Label
    Private groupBox2 As System.Windows.Forms.GroupBox
    Private label7 As System.Windows.Forms.Label
    Private cbProxyType As System.Windows.Forms.ComboBox
    Private tbProxyPort As System.Windows.Forms.TextBox
    Private label6 As System.Windows.Forms.Label
    Private WithEvents cbProxy As System.Windows.Forms.CheckBox
    Private tbProxy As System.Windows.Forms.TextBox
    Private label5 As System.Windows.Forms.Label
    Private WithEvents btnConnect As System.Windows.Forms.Button
    Private tbProxyLogin As System.Windows.Forms.TextBox
    Private label8 As System.Windows.Forms.Label
    Private tbProxyPassword As System.Windows.Forms.TextBox
    Private label9 As System.Windows.Forms.Label
    Private label10 As System.Windows.Forms.Label
    Private sbMessage As System.Windows.Forms.StatusBar
    Private label11 As System.Windows.Forms.Label
    Private WithEvents cbUseLargeBuffers As System.Windows.Forms.CheckBox
    Private groupBox3 As System.Windows.Forms.GroupBox
    Private label12 As System.Windows.Forms.Label
    Private WithEvents cbSecurity As System.Windows.Forms.ComboBox
    Private label13 As System.Windows.Forms.Label
    Private WithEvents cbTLS As System.Windows.Forms.CheckBox
    Private label14 As System.Windows.Forms.Label
    Private cbSuite As System.Windows.Forms.ComboBox
    Private WithEvents cbSSL As System.Windows.Forms.CheckBox
    Private label16 As System.Windows.Forms.Label
    Private tbCertificatePath As System.Windows.Forms.TextBox
    Friend WithEvents btnCertificatePath As System.Windows.Forms.Button
    Private openFileDialog As System.Windows.Forms.OpenFileDialog
    Private label17 As System.Windows.Forms.Label

    ' CONSTANTS
    Public ANONYMOUSLOGIN As String = "anonymous"
    Public ANONYMOUSPASSWORD As String = "guest@127.0.0.1"
    Public PROXYWHENSECURE() As FtpProxyType = {FtpProxyType.Socks4, FtpProxyType.Socks4a, FtpProxyType.Socks5, FtpProxyType.HttpConnect}

    Public Shared ReadOnly ConfigFile As String = Path.Combine(Environment.GetFolderPath(System.Environment.SpecialFolder.LocalApplicationData), "Rebex\FTP SSL\WinFormClient.xml")


    Public _ok As Boolean = False ' form was confirmed

    Public ReadOnly Property OK() As Boolean
        Get
            Return _ok
        End Get
    End Property

    ' host name   
    Public Property Host() As String
        Get
            Return tbHost.Text
        End Get
        Set(ByVal Value As String)
            tbHost.Text = Value
        End Set
    End Property

    ' port of the host
    Public Property Port() As Integer
        Get
            Dim temp As Integer

            Try
                temp = Int32.Parse(tbPort.Text)

            Catch
                Return 0 ' def. value if error occures
            End Try

            Return temp
        End Get
        Set(ByVal Value As Integer)
            tbPort.Text = Value.ToString()
        End Set
    End Property

    ' proxy server
    Public Property Proxy() As String
        Get
            Return tbProxy.Text
        End Get
        Set(ByVal Value As String)
            tbProxy.Text = Value
        End Set
    End Property

    ' proxy login name   
    Public Property ProxyLogin() As String
        Get
            Return tbProxyLogin.Text
        End Get
        Set(ByVal Value As String)
            tbProxyLogin.Text = Value
        End Set
    End Property

    ' proxy password
    Public Property ProxyPassword() As String
        Get
            Return tbProxyPassword.Text
        End Get
        Set(ByVal Value As String)
            tbProxyPassword.Text = Value
        End Set
    End Property

    ' is proxy enabled?   
    Public Property ProxyEnabled() As Boolean

        Get
            Return cbProxy.Checked
        End Get
        Set(ByVal Value As Boolean)
            If Value Then
                tbProxy.Enabled = True
                tbProxyPort.Enabled = True
                cbProxy.Checked = True
                cbProxyType.Enabled = True
                tbProxyLogin.Enabled = True
                tbProxyPassword.Enabled = True
            Else
                tbProxy.Enabled = False
                tbProxyPort.Enabled = False
                cbProxy.Checked = False
                cbProxyType.Enabled = False
                tbProxyLogin.Enabled = False
                tbProxyPassword.Enabled = False
            End If
        End Set
    End Property

    ' proxy port
    Public Property ProxyPort() As Integer
        Get
            Dim temp As Integer

            Try
                temp = Int32.Parse(tbProxyPort.Text)

            Catch
                Return 0 ' def. value if error occures
            End Try

            Return temp
        End Get
        Set(ByVal Value As Integer)
            tbProxyPort.Text = Value.ToString()
        End Set
    End Property

    ' login name
    Public Property Login() As String
        Get
            If Not (Not (tbLogin.Text Is Nothing) AndAlso tbLogin.Text.Length > 0) Then
                Return ANONYMOUSLOGIN
            Else
                Return tbLogin.Text
            End If
        End Get

        Set(ByVal Value As String)
            tbLogin.Text = Value
        End Set
    End Property

    ' password
    Public Property Password() As String
        Get
            If Not (Not (tbLogin.Text Is Nothing) AndAlso tbLogin.Text.Length > 0) Then
                Return ANONYMOUSPASSWORD
            Else
                Return tbPassword.Text
            End If
        End Get

        Set(ByVal Value As String)
            tbPassword.Text = Value
        End Set
    End Property

    ' passive mode
    Public Property Passive() As Boolean
        Get
            Return cbPassive.Checked
        End Get
        Set(ByVal Value As Boolean)
            cbPassive.Checked = Value
        End Set
    End Property

    ' use large buffers
    Public Property UseLargeBuffers() As Boolean
        Get
            Return cbUseLargeBuffers.Checked
        End Get
        Set(ByVal Value As Boolean)
            cbUseLargeBuffers.Checked = Value
        End Set
    End Property

    ' proxy type
    Public Property ProxyType() As FtpProxyType
        Get
            Select Case cbProxyType.SelectedIndex
                Case 1
                    Return FtpProxyType.Socks4a
                Case 2
                    Return FtpProxyType.Socks5
                Case 3
                    Return FtpProxyType.HttpConnect
                Case 4
                    Return FtpProxyType.FtpSite
                Case 5
                    Return FtpProxyType.FtpUser
                Case 6
                    Return FtpProxyType.FtpOpen
                Case Else
                    Return FtpProxyType.Socks4
            End Select
        End Get
        Set(ByVal Value As FtpProxyType)
            Select Case Value
                Case FtpProxyType.Socks4a
                    cbProxyType.SelectedIndex = 1
                Case FtpProxyType.Socks5
                    cbProxyType.SelectedIndex = 2
                Case FtpProxyType.HttpConnect
                    cbProxyType.SelectedIndex = 3
                Case FtpProxyType.FtpSite
                    cbProxyType.SelectedIndex = 4
                Case FtpProxyType.FtpUser
                    cbProxyType.SelectedIndex = 5
                Case FtpProxyType.FtpOpen
                    cbProxyType.SelectedIndex = 6
                Case Else
                    cbProxyType.SelectedIndex = 0
            End Select
        End Set
    End Property

    ' security type
    Public Property SecurityType() As FtpSecurity
        Get
            Select Case cbSecurity.SelectedIndex
                Case 1
                    Return FtpSecurity.Explicit
                Case 2
                    Return FtpSecurity.Implicit
                Case Else
                    Return FtpSecurity.Unsecure
            End Select
        End Get
        Set(ByVal Value As FtpSecurity)
            Select Case Value
                Case FtpSecurity.Explicit
                    cbSecurity.SelectedIndex = 1
                Case FtpSecurity.Implicit
                    cbSecurity.SelectedIndex = 2
                Case Else
                    cbSecurity.SelectedIndex = 0
            End Select
        End Set
    End Property


    ' certificate path        
    Public Property CertificatePath() As String
        Get
            Return tbCertificatePath.Text
        End Get
        Set(ByVal Value As String)
            tbCertificatePath.Text = Value
        End Set
    End Property

    ' allowed suite        
    Public Property AllowedSuite() As TlsCipherSuite
        Get
            If cbSuite.SelectedIndex = 0 Then
                Return TlsCipherSuite.All
            Else
                Return TlsCipherSuite.Secure
            End If
        End Get
        Set(ByVal Value As TlsCipherSuite)
            If Value = TlsCipherSuite.All Then
                cbSuite.SelectedIndex = 0
            Else
                cbSuite.SelectedIndex = 1
            End If
        End Set
    End Property

    ' protocol
    Public Property Protocol() As TlsVersion
        Get
            Dim value As TlsVersion = 0
            If cbTLS.Checked Then
                value = value Or TlsVersion.TLS10
            End If
            If cbSSL.Checked Then
                value = value Or TlsVersion.SSL30
            End If
            Return value
        End Get
        Set(ByVal Value As TlsVersion)
            cbTLS.Checked = (Value And TlsVersion.TLS10) <> 0
            cbSSL.Checked = (Value And TlsVersion.SSL30) <> 0
        End Set
    End Property

    ' clear command channel
    Public Property ClearCommandChannel() As Boolean
        Set(ByVal Value As Boolean)
            cbCCC.Checked = Value
        End Set
        Get
            Return cbCCC.Checked
        End Get
    End Property


    ' log level
    Public Property LogLevel() As Rebex.LogLevel
        Get
            Select Case cbLogLevel.SelectedIndex
                Case 1
                    Return Rebex.LogLevel.Debug
                Case 2
                    Return Rebex.LogLevel.Verbose
                Case 3
                    Return Rebex.LogLevel.Error
                Case Else
                    Return Rebex.LogLevel.Info
            End Select
        End Get
        Set(ByVal Value As Rebex.LogLevel)
            Select Case Value
                Case Rebex.LogLevel.Debug
                    cbLogLevel.SelectedIndex = 1
                Case Rebex.LogLevel.Verbose
                    cbLogLevel.SelectedIndex = 2
                Case Rebex.LogLevel.Error
                    cbLogLevel.SelectedIndex = 3
                Case Else
                    cbLogLevel.SelectedIndex = 0
            End Select
        End Set
    End Property

    ' -------------------------------------------------------------------------
    '' <summary>
    '' validity check
    '' </summary>
    '' <returns>true (everything is ok)</returns>
    Public Function ValidateForm() As Boolean
        ' ftp host
        If tbHost.Text Is Nothing OrElse tbHost.Text.Length <= 0 Then
            sbMessage.Text = "ftp host is a required field"
            Return False
        End If

        ' ftp port
        If tbPort.Text Is Nothing OrElse tbPort.Text.Length <= 0 Then
            sbMessage.Text = "ftp port is a required field"
            Return False
        End If

        ' ftp port [decimal]
        Dim badFtpPort As Boolean = False

        Try
            Dim n As Integer
            n = Integer.Parse(tbPort.Text)
            If n = 0 Then
                badFtpPort = True
            End If

        Catch
            badFtpPort = True
        End Try

        If badFtpPort Then
            sbMessage.Text = "ftp port must be > 0"
            Return False
        End If

        ' ----------------
        ' proxy check-outs
        ' ----------------
        If cbProxy.Checked Then
            ' proxy host
            If tbProxy.Text Is Nothing OrElse tbProxy.Text.Length <= 0 Then
                sbMessage.Text = "proxy host is a required field when proxy is enabled"
                Return False
            End If

            ' proxy port
            If tbProxyPort.Text Is Nothing OrElse tbProxyPort.Text.Length <= 0 Then
                sbMessage.Text = "proxy port is a required field when proxy is enabled"
                Return False
            End If

            ' proxy port [decimal]
            Dim badProxyPort As Boolean = False

            Try
                Dim n As Integer = Integer.Parse(tbPort.Text)
                If n = 0 Then
                    badProxyPort = True
                End If

            Catch
                badProxyPort = True
            End Try

            If badProxyPort Then
                sbMessage.Text = "proxy port must be > 0"
                Return False
            End If

            ' when secure - check proxy type
            If SecurityType <> 0 Then
                Dim ok As Boolean = False

                Dim i As Integer
                For i = 0 To PROXYWHENSECURE.Length - 1
                    If PROXYWHENSECURE(i) = ProxyType Then
                        ok = True
                    End If
                Next i
                If Not ok Then
                    MessageBox.Show("choose SOCKS4 or HTTP Connect when security is on", "proxy error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Return False
                End If
            End If
        End If

        Return True
    End Function

    ' -------------------------------------------------------------------------
    '' <summary>
    '' load a config file and set appropritate values for site
    '' </summary>
    Private Sub LoadConfig()
        ' set default values for site
        Me.Host = Nothing
        Me.Port = Ftp.DefaultPort
        Me.Login = ""
        Me.Password = ""
        Me.Passive = True
        Me.Proxy = Nothing
        Me.ProxyEnabled = False
        Me.ProxyPort = 3128
        Me.ProxyLogin = Nothing
        Me.ProxyPassword = Nothing
        Me.ProxyType = FtpProxyType.Socks4
        Me.LogLevel = Rebex.LogLevel.Info
        Me.ClearCommandChannel = False
        Me.SecurityType = FtpSecurity.Unsecure
        Me.AllowedSuite = TlsCipherSuite.All

        ' read config file

        Dim xml As New XmlDocument
        If Not File.Exists(ConfigFile) Then
            Exit Sub
        End If

        xml.Load(ConfigFile)

        Me.Proxy = Nothing

        Dim config As XmlElement = xml("configuration")
        Dim key As XmlNode
        For Each key In config.ChildNodes
            Dim item As String = Nothing
            Dim name As String = Nothing
            If Not key.Attributes("value") Is Nothing Then item = key.Attributes("value").Value
            If Not key.Attributes("name") Is Nothing Then name = key.Attributes("name").Value
            If Not key("value") Is Nothing Then item = key("value").InnerText
            If Not key("name") Is Nothing Then name = key("name").InnerText

            If Not name Is Nothing Then
                If item Is Nothing Then item = ""

                Select Case name
                    Case "host"
                        Me.Host = item
                    Case "login"
                        Me.Login = item
                    Case "port"
                        Try
                            Me.Port = Int32.Parse(item)
                        Catch
                            Me.Port = Ftp.DefaultPort           ' default
                        End Try
                    Case "password"
                        Me.Password = item
                    Case "passive"
                        Me.Passive = IIf(item.ToLower() = "false", False, True)
                    Case "largebuffers"
                        Me.UseLargeBuffers = IIf(item.ToLower() = "false", False, True)
                    Case "proxy"
                        Me.Proxy = item
                    Case "proxylogin"
                        Me.ProxyLogin = item
                    Case "proxypassword"
                        Me.ProxyPassword = item
                    Case "proxyenabled"
                        Me.ProxyEnabled = IIf(item.ToLower() = "false", False, True)
                    Case "clearcommandchannel"
                        Me.ClearCommandChannel = IIf(item.ToLower() = "false", False, True)
                    Case "certificatepath"
                        Me.CertificatePath = item
                    Case "proxyport"
                        Try
                            Me.ProxyPort = Int32.Parse(item)
                        Catch
                            Me.ProxyPort = 3128                         ' default
                        End Try
                    Case "proxytype"
                        Try
                            Me.ProxyType = CType([Enum].Parse(GetType(FtpProxyType), item), FtpProxyType)
                        Catch
                            Me.ProxyType = FtpProxyType.Socks4                          ' default
                        End Try
                    Case "loglevel"
                        Try
                            Me.LogLevel = CType([Enum].Parse(GetType(Rebex.LogLevel), item), Rebex.LogLevel)
                        Catch
                            Me.LogLevel = Rebex.LogLevel.Info                            ' default
                        End Try
                    Case "securitytype"
                        Try
                            Me.SecurityType = CType([Enum].Parse(GetType(FtpSecurity), item), FtpSecurity)
                        Catch
                            Me.SecurityType = FtpSecurity.Unsecure
                        End Try
                    Case "allowedsuite"
                        Try
                            Me.AllowedSuite = CType([Enum].Parse(GetType(TlsCipherSuite), item), TlsCipherSuite)
                        Catch
                            Me.AllowedSuite = TlsCipherSuite.All
                        End Try
                    Case "protocol"
                        Try
                            Me.Protocol = CType([Enum].Parse(GetType(TlsVersion), item, True), TlsVersion)
                        Catch
                            Me.Protocol = TlsVersion.SSL30 Or TlsVersion.TLS10                          ' default
                        End Try
                End Select
            End If
        Next key
    End Sub


    Private Shared Sub AddKey(ByVal xml As XmlDocument, ByVal config As XmlElement, ByVal name As String, ByVal val As String)
        Dim key As XmlElement = xml.CreateElement("key")
        config.AppendChild(key)
        Dim atrName As XmlAttribute = xml.CreateAttribute("name")
        atrName.Value = name
        Dim atrVal As XmlAttribute = xml.CreateAttribute("value")
        atrVal.Value = val
        key.Attributes.Append(atrName)
        key.Attributes.Append(atrVal)
    End Sub

    ' -------------------------------------------------------------------------
    '' <summary>
    '' save site values into config file
    '' </summary>
    Private Sub SaveConfig()
        Dim xml As XmlDocument = New XmlDocument
        Dim config As XmlElement = xml.CreateElement("configuration")
        xml.AppendChild(config)

        AddKey(xml, config, "host", Me.Host)
        AddKey(xml, config, "login", Me.Login)
        AddKey(xml, config, "port", Me.Port.ToString())
        AddKey(xml, config, "password", Me.Password)
        AddKey(xml, config, "passive", Me.Passive.ToString())
        AddKey(xml, config, "largebuffers", Me.UseLargeBuffers.ToString())
        AddKey(xml, config, "proxyenabled", Me.ProxyEnabled.ToString())
        AddKey(xml, config, "proxy", Me.Proxy)
        AddKey(xml, config, "proxyport", Me.ProxyPort.ToString())
        AddKey(xml, config, "proxytype", Me.ProxyType.ToString())
        AddKey(xml, config, "proxylogin", Me.ProxyLogin)
        AddKey(xml, config, "proxypassword", Me.ProxyPassword)
        AddKey(xml, config, "loglevel", Me.LogLevel.ToString())
        AddKey(xml, config, "securitytype", Me.SecurityType.ToString())
        AddKey(xml, config, "allowedsuite", Me.AllowedSuite.ToString())
        AddKey(xml, config, "protocol", Me.Protocol.ToString())
        AddKey(xml, config, "clearcommandchannel", Me.ClearCommandChannel.ToString())
        AddKey(xml, config, "certificatepath", Me.CertificatePath)

        Dim configPath As String = Path.GetDirectoryName(ConfigFile)
        If Not Directory.Exists(configPath) Then
            Directory.CreateDirectory(configPath)
        End If
        xml.Save(ConfigFile)
    End Sub


#Region "Windows Form Designer generated code"

    ' -------------------------------------------------------------------------
    '' <summary>
    '' constructor
    '' </summary>
    Public Sub New()
        InitializeComponent()

        LoadConfig()
    End Sub

    ' -------------------------------------------------------------------------
    '' <summary>
    '' clean up any resources being used
    '' </summary>
    Protected Overloads Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If

        MyBase.Dispose(disposing)
    End Sub

    ' -------------------------------------------------------------------------
    '' <summary>
    '' Required method for Designer support - do not modify
    '' the contents of this method with the code editor.
    '' </summary>
    Friend WithEvents cbCCC As System.Windows.Forms.CheckBox
    Friend WithEvents label18 As System.Windows.Forms.Label
    Friend WithEvents cbLogLevel As System.Windows.Forms.ComboBox
    Private Sub InitializeComponent()
        Me.groupBox1 = New System.Windows.Forms.GroupBox
        Me.cbPassive = New System.Windows.Forms.CheckBox
        Me.tbPassword = New System.Windows.Forms.TextBox
        Me.label4 = New System.Windows.Forms.Label
        Me.tbLogin = New System.Windows.Forms.TextBox
        Me.label3 = New System.Windows.Forms.Label
        Me.tbPort = New System.Windows.Forms.TextBox
        Me.label2 = New System.Windows.Forms.Label
        Me.tbHost = New System.Windows.Forms.TextBox
        Me.label1 = New System.Windows.Forms.Label
        Me.label11 = New System.Windows.Forms.Label
        Me.groupBox2 = New System.Windows.Forms.GroupBox
        Me.label10 = New System.Windows.Forms.Label
        Me.tbProxyPassword = New System.Windows.Forms.TextBox
        Me.label9 = New System.Windows.Forms.Label
        Me.tbProxyLogin = New System.Windows.Forms.TextBox
        Me.label8 = New System.Windows.Forms.Label
        Me.label7 = New System.Windows.Forms.Label
        Me.cbProxyType = New System.Windows.Forms.ComboBox
        Me.tbProxyPort = New System.Windows.Forms.TextBox
        Me.label6 = New System.Windows.Forms.Label
        Me.cbProxy = New System.Windows.Forms.CheckBox
        Me.tbProxy = New System.Windows.Forms.TextBox
        Me.label5 = New System.Windows.Forms.Label
        Me.btnConnect = New System.Windows.Forms.Button
        Me.sbMessage = New System.Windows.Forms.StatusBar
        Me.groupBox3 = New System.Windows.Forms.GroupBox
        Me.btnCertificatePath = New System.Windows.Forms.Button
        Me.tbCertificatePath = New System.Windows.Forms.TextBox
        Me.label16 = New System.Windows.Forms.Label
        Me.cbCCC = New System.Windows.Forms.CheckBox
        Me.cbSuite = New System.Windows.Forms.ComboBox
        Me.label14 = New System.Windows.Forms.Label
        Me.cbSSL = New System.Windows.Forms.CheckBox
        Me.cbTLS = New System.Windows.Forms.CheckBox
        Me.label13 = New System.Windows.Forms.Label
        Me.cbSecurity = New System.Windows.Forms.ComboBox
        Me.label12 = New System.Windows.Forms.Label
        Me.label17 = New System.Windows.Forms.Label
        Me.openFileDialog = New System.Windows.Forms.OpenFileDialog
        Me.label18 = New System.Windows.Forms.Label
        Me.cbLogLevel = New System.Windows.Forms.ComboBox
        Me.cbUseLargeBuffers = New System.Windows.Forms.CheckBox
        Me.groupBox1.SuspendLayout()
        Me.groupBox2.SuspendLayout()
        Me.groupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'groupBox1
        '
        Me.groupBox1.Controls.Add(Me.cbUseLargeBuffers)
        Me.groupBox1.Controls.Add(Me.cbPassive)
        Me.groupBox1.Controls.Add(Me.tbPassword)
        Me.groupBox1.Controls.Add(Me.label4)
        Me.groupBox1.Controls.Add(Me.tbLogin)
        Me.groupBox1.Controls.Add(Me.label3)
        Me.groupBox1.Controls.Add(Me.tbPort)
        Me.groupBox1.Controls.Add(Me.label2)
        Me.groupBox1.Controls.Add(Me.tbHost)
        Me.groupBox1.Controls.Add(Me.label1)
        Me.groupBox1.Controls.Add(Me.label11)
        Me.groupBox1.Location = New System.Drawing.Point(8, 8)
        Me.groupBox1.Name = "groupBox1"
        Me.groupBox1.Size = New System.Drawing.Size(400, 132)
        Me.groupBox1.TabIndex = 18
        Me.groupBox1.TabStop = False
        Me.groupBox1.Text = "FTP settings"
        '
        'cbPassive
        '
        Me.cbPassive.Checked = True
        Me.cbPassive.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cbPassive.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.cbPassive.Location = New System.Drawing.Point(235, 48)
        Me.cbPassive.Name = "cbPassive"
        Me.cbPassive.Size = New System.Drawing.Size(149, 24)
        Me.cbPassive.TabIndex = 18
        Me.cbPassive.Text = "Passive mode"
        '
        'tbPassword
        '
        Me.tbPassword.Location = New System.Drawing.Point(112, 72)
        Me.tbPassword.Name = "tbPassword"
        Me.tbPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.tbPassword.Size = New System.Drawing.Size(100, 20)
        Me.tbPassword.TabIndex = 17
        '
        'label4
        '
        Me.label4.Location = New System.Drawing.Point(8, 75)
        Me.label4.Name = "label4"
        Me.label4.Size = New System.Drawing.Size(100, 23)
        Me.label4.TabIndex = 16
        Me.label4.Text = "Password:"
        '
        'tbLogin
        '
        Me.tbLogin.Location = New System.Drawing.Point(112, 48)
        Me.tbLogin.Name = "tbLogin"
        Me.tbLogin.Size = New System.Drawing.Size(100, 20)
        Me.tbLogin.TabIndex = 15
        '
        'label3
        '
        Me.label3.Location = New System.Drawing.Point(8, 51)
        Me.label3.Name = "label3"
        Me.label3.Size = New System.Drawing.Size(100, 23)
        Me.label3.TabIndex = 14
        Me.label3.Text = "Login name:"
        '
        'tbPort
        '
        Me.tbPort.Location = New System.Drawing.Point(296, 24)
        Me.tbPort.Name = "tbPort"
        Me.tbPort.Size = New System.Drawing.Size(64, 20)
        Me.tbPort.TabIndex = 13
        Me.tbPort.Text = "21"
        '
        'label2
        '
        Me.label2.Location = New System.Drawing.Point(232, 27)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(56, 23)
        Me.label2.TabIndex = 12
        Me.label2.Text = "Port:"
        '
        'tbHost
        '
        Me.tbHost.Location = New System.Drawing.Point(112, 24)
        Me.tbHost.Name = "tbHost"
        Me.tbHost.Size = New System.Drawing.Size(100, 20)
        Me.tbHost.TabIndex = 11
        '
        'label1
        '
        Me.label1.Location = New System.Drawing.Point(8, 27)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(100, 23)
        Me.label1.TabIndex = 10
        Me.label1.Text = "Host name:"
        '
        'label11
        '
        Me.label11.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.label11.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.label11.Location = New System.Drawing.Point(114, 99)
        Me.label11.Name = "label11"
        Me.label11.Size = New System.Drawing.Size(270, 30)
        Me.label11.TabIndex = 31
        Me.label11.Text = "Leave login/password empty for anonymous login."
        '
        'groupBox2
        '
        Me.groupBox2.Controls.Add(Me.label10)
        Me.groupBox2.Controls.Add(Me.tbProxyPassword)
        Me.groupBox2.Controls.Add(Me.label9)
        Me.groupBox2.Controls.Add(Me.tbProxyLogin)
        Me.groupBox2.Controls.Add(Me.label8)
        Me.groupBox2.Controls.Add(Me.label7)
        Me.groupBox2.Controls.Add(Me.cbProxyType)
        Me.groupBox2.Controls.Add(Me.tbProxyPort)
        Me.groupBox2.Controls.Add(Me.label6)
        Me.groupBox2.Controls.Add(Me.cbProxy)
        Me.groupBox2.Controls.Add(Me.tbProxy)
        Me.groupBox2.Controls.Add(Me.label5)
        Me.groupBox2.Location = New System.Drawing.Point(8, 347)
        Me.groupBox2.Name = "groupBox2"
        Me.groupBox2.Size = New System.Drawing.Size(400, 128)
        Me.groupBox2.TabIndex = 19
        Me.groupBox2.TabStop = False
        Me.groupBox2.Text = "Proxy settings"
        '
        'label10
        '
        Me.label10.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.label10.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.label10.Location = New System.Drawing.Point(232, 72)
        Me.label10.Name = "label10"
        Me.label10.Size = New System.Drawing.Size(160, 48)
        Me.label10.TabIndex = 30
        Me.label10.Text = "Leave login/password fields empty when not needed."
        '
        'tbProxyPassword
        '
        Me.tbProxyPassword.Location = New System.Drawing.Point(112, 96)
        Me.tbProxyPassword.Name = "tbProxyPassword"
        Me.tbProxyPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.tbProxyPassword.Size = New System.Drawing.Size(104, 20)
        Me.tbProxyPassword.TabIndex = 29
        '
        'label9
        '
        Me.label9.Location = New System.Drawing.Point(8, 99)
        Me.label9.Name = "label9"
        Me.label9.Size = New System.Drawing.Size(100, 23)
        Me.label9.TabIndex = 28
        Me.label9.Text = "Password:"
        '
        'tbProxyLogin
        '
        Me.tbProxyLogin.Location = New System.Drawing.Point(112, 72)
        Me.tbProxyLogin.Name = "tbProxyLogin"
        Me.tbProxyLogin.Size = New System.Drawing.Size(104, 20)
        Me.tbProxyLogin.TabIndex = 27
        '
        'label8
        '
        Me.label8.Location = New System.Drawing.Point(8, 75)
        Me.label8.Name = "label8"
        Me.label8.Size = New System.Drawing.Size(100, 23)
        Me.label8.TabIndex = 26
        Me.label8.Text = "Login name:"
        '
        'label7
        '
        Me.label7.Location = New System.Drawing.Point(8, 51)
        Me.label7.Name = "label7"
        Me.label7.Size = New System.Drawing.Size(88, 23)
        Me.label7.TabIndex = 25
        Me.label7.Text = "Proxy type:"
        '
        'cbProxyType
        '
        Me.cbProxyType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbProxyType.Items.AddRange(New Object() {"Socks4", "Socks4a", "Socks5", "HttpConnect", "FtpSite", "FtpUser", "FtpOpen"})
        Me.cbProxyType.Location = New System.Drawing.Point(112, 48)
        Me.cbProxyType.Name = "cbProxyType"
        Me.cbProxyType.Size = New System.Drawing.Size(104, 21)
        Me.cbProxyType.TabIndex = 24
        '
        'tbProxyPort
        '
        Me.tbProxyPort.Location = New System.Drawing.Point(296, 24)
        Me.tbProxyPort.Name = "tbProxyPort"
        Me.tbProxyPort.Size = New System.Drawing.Size(64, 20)
        Me.tbProxyPort.TabIndex = 23
        Me.tbProxyPort.Text = "21"
        '
        'label6
        '
        Me.label6.Location = New System.Drawing.Point(232, 27)
        Me.label6.Name = "label6"
        Me.label6.Size = New System.Drawing.Size(56, 23)
        Me.label6.TabIndex = 22
        Me.label6.Text = "Port:"
        '
        'cbProxy
        '
        Me.cbProxy.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.cbProxy.Location = New System.Drawing.Point(296, 48)
        Me.cbProxy.Name = "cbProxy"
        Me.cbProxy.Size = New System.Drawing.Size(88, 24)
        Me.cbProxy.TabIndex = 21
        Me.cbProxy.Text = "Use proxy"
        '
        'tbProxy
        '
        Me.tbProxy.Location = New System.Drawing.Point(112, 24)
        Me.tbProxy.Name = "tbProxy"
        Me.tbProxy.Size = New System.Drawing.Size(104, 20)
        Me.tbProxy.TabIndex = 20
        '
        'label5
        '
        Me.label5.Location = New System.Drawing.Point(8, 27)
        Me.label5.Name = "label5"
        Me.label5.Size = New System.Drawing.Size(100, 23)
        Me.label5.TabIndex = 19
        Me.label5.Text = "Proxy:"
        '
        'btnConnect
        '
        Me.btnConnect.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnConnect.Location = New System.Drawing.Point(316, 483)

        Me.btnConnect.Name = "btnConnect"
        Me.btnConnect.Size = New System.Drawing.Size(88, 23)
        Me.btnConnect.TabIndex = 20
        Me.btnConnect.Text = "Connect"
        '
        'sbMessage
        '
        Me.sbMessage.Location = New System.Drawing.Point(0, 512)
        Me.sbMessage.Name = "sbMessage"
        Me.sbMessage.Size = New System.Drawing.Size(416, 19)
        Me.sbMessage.TabIndex = 21
        '
        'groupBox3
        '
        Me.groupBox3.Controls.Add(Me.btnCertificatePath)
        Me.groupBox3.Controls.Add(Me.tbCertificatePath)
        Me.groupBox3.Controls.Add(Me.label16)
        Me.groupBox3.Controls.Add(Me.cbCCC)
        Me.groupBox3.Controls.Add(Me.cbSuite)
        Me.groupBox3.Controls.Add(Me.label14)
        Me.groupBox3.Controls.Add(Me.cbSSL)
        Me.groupBox3.Controls.Add(Me.cbTLS)
        Me.groupBox3.Controls.Add(Me.label13)
        Me.groupBox3.Controls.Add(Me.cbSecurity)
        Me.groupBox3.Controls.Add(Me.label12)
        Me.groupBox3.Controls.Add(Me.label17)
        Me.groupBox3.Location = New System.Drawing.Point(8, 141)
        Me.groupBox3.Name = "groupBox3"
        Me.groupBox3.Size = New System.Drawing.Size(400, 200)
        Me.groupBox3.TabIndex = 22
        Me.groupBox3.TabStop = False
        Me.groupBox3.Text = "Security settings"
        '
        'btnCertificatePath
        '
        Me.btnCertificatePath.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnCertificatePath.Location = New System.Drawing.Point(368, 168)
        Me.btnCertificatePath.Name = "btnCertificatePath"
        Me.btnCertificatePath.Size = New System.Drawing.Size(24, 23)
        Me.btnCertificatePath.TabIndex = 21
        Me.btnCertificatePath.Text = "..."
        '
        'tbCertificatePath
        '
        Me.tbCertificatePath.Location = New System.Drawing.Point(112, 168)
        Me.tbCertificatePath.Name = "tbCertificatePath"
        Me.tbCertificatePath.Size = New System.Drawing.Size(248, 20)
        Me.tbCertificatePath.TabIndex = 18
        '
        'label16
        '
        Me.label16.Location = New System.Drawing.Point(8, 168)
        Me.label16.Name = "label16"
        Me.label16.Size = New System.Drawing.Size(104, 23)
        Me.label16.TabIndex = 11
        Me.label16.Text = "Client certificate:"
        '
        'cbCCC
        '
        Me.cbCCC.Checked = True
        Me.cbCCC.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cbCCC.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.cbCCC.Location = New System.Drawing.Point(8, 104)
        Me.cbCCC.Name = "cbCCC"
        Me.cbCCC.Size = New System.Drawing.Size(256, 24)
        Me.cbCCC.TabIndex = 11
        Me.cbCCC.Text = "Clear command channel"
        '
        'cbSuite
        '
        Me.cbSuite.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbSuite.Items.AddRange(New Object() {"All ciphers", "Secure only"})
        Me.cbSuite.Location = New System.Drawing.Point(112, 72)
        Me.cbSuite.Name = "cbSuite"
        Me.cbSuite.Size = New System.Drawing.Size(152, 21)
        Me.cbSuite.TabIndex = 6
        '
        'label14
        '
        Me.label14.Location = New System.Drawing.Point(8, 75)
        Me.label14.Name = "label14"
        Me.label14.Size = New System.Drawing.Size(104, 23)
        Me.label14.TabIndex = 5
        Me.label14.Text = "Allowed suites:"
        '
        'cbSSL
        '
        Me.cbSSL.Checked = True
        Me.cbSSL.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cbSSL.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.cbSSL.Location = New System.Drawing.Point(192, 40)
        Me.cbSSL.Name = "cbSSL"
        Me.cbSSL.Size = New System.Drawing.Size(64, 24)
        Me.cbSSL.TabIndex = 4
        Me.cbSSL.Text = "SSL 3.0"
        '
        'cbTLS
        '
        Me.cbTLS.Checked = True
        Me.cbTLS.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cbTLS.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.cbTLS.Location = New System.Drawing.Point(112, 40)
        Me.cbTLS.Name = "cbTLS"
        Me.cbTLS.Size = New System.Drawing.Size(72, 24)
        Me.cbTLS.TabIndex = 3
        Me.cbTLS.Text = "TLS 1.0"
        '
        'label13
        '
        Me.label13.Location = New System.Drawing.Point(8, 45)
        Me.label13.Name = "label13"
        Me.label13.Size = New System.Drawing.Size(104, 23)
        Me.label13.TabIndex = 2
        Me.label13.Text = "Allowed protocol:"
        '
        'cbSecurity
        '
        Me.cbSecurity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbSecurity.Items.AddRange(New Object() {"No security", "Explicit TLS or SSL", "Implicit TLS or SSL"})
        Me.cbSecurity.Location = New System.Drawing.Point(112, 16)
        Me.cbSecurity.Name = "cbSecurity"
        Me.cbSecurity.Size = New System.Drawing.Size(152, 21)
        Me.cbSecurity.TabIndex = 1
        '
        'label12
        '
        Me.label12.Location = New System.Drawing.Point(8, 18)
        Me.label12.Name = "label12"
        Me.label12.Size = New System.Drawing.Size(104, 23)
        Me.label12.TabIndex = 0
        Me.label12.Text = "Security:"
        '
        'label17
        '
        Me.label17.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.label17.Location = New System.Drawing.Point(10, 136)
        Me.label17.Name = "label17"
        Me.label17.Size = New System.Drawing.Size(344, 32)
        Me.label17.TabIndex = 32
        Me.label17.Text = "Fill the ""Client certificate"" field only when you want to authenticate using a cl" & _
            "ient certificate."
        '
        'openFileDialog
        '
        Me.openFileDialog.Filter = "PFX and P12 files (*.pfx;*.p12)|*.pfx;*.p12|All files (*.*)|*.*"
        '
        'label18
        '
        Me.label18.Location = New System.Drawing.Point(13, 487)
        Me.label18.Name = "label18"
        Me.label18.Size = New System.Drawing.Size(88, 23)
        Me.label18.TabIndex = 28
        Me.label18.Text = "Logging level:"
        '
        'cbLogLevel
        '
        Me.cbLogLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbLogLevel.Items.AddRange(New Object() {"Info", "Debug", "Verbose", "Errors only"})
        Me.cbLogLevel.Location = New System.Drawing.Point(120, 484)
        Me.cbLogLevel.Name = "cbLogLevel"
        Me.cbLogLevel.Size = New System.Drawing.Size(104, 21)
        Me.cbLogLevel.TabIndex = 27
        '
        'cbUseLargeBuffers
        '
        Me.cbUseLargeBuffers.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.cbUseLargeBuffers.Location = New System.Drawing.Point(235, 72)
        Me.cbUseLargeBuffers.Name = "cbUseLargeBuffers"
        Me.cbUseLargeBuffers.Size = New System.Drawing.Size(149, 24)
        Me.cbUseLargeBuffers.TabIndex = 33
        Me.cbUseLargeBuffers.Text = "Use large buffers (faster)"
        '
        'Connection
        '
        Me.AcceptButton = Me.btnConnect
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(416, 532)
        Me.Controls.Add(Me.groupBox3)
        Me.Controls.Add(Me.label18)
        Me.Controls.Add(Me.cbLogLevel)
        Me.Controls.Add(Me.sbMessage)
        Me.Controls.Add(Me.btnConnect)
        Me.Controls.Add(Me.groupBox2)
        Me.Controls.Add(Me.groupBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.KeyPreview = True
        Me.Location = New System.Drawing.Point(0, 512)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Connection"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Connection"
        Me.groupBox1.ResumeLayout(False)
        Me.groupBox1.PerformLayout()
        Me.groupBox2.ResumeLayout(False)
        Me.groupBox2.PerformLayout()
        Me.groupBox3.ResumeLayout(False)
        Me.groupBox3.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
#End Region


    ' -------------------------------------------------------------------------
    '' <summary>
    '' form confirmation by clicking 'ok' button
    '' </summary>
    '' <param name="sender"></param>
    '' <param name="e"></param>
    Private Sub btnConnect_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConnect.Click
        If ValidateForm() Then
            _ok = True
            SaveConfig()
            Me.Close()
        End If
    End Sub

    ' -------------------------------------------------------------------------
    '' <summary>
    '' proxy enable/disable click
    '' </summary>
    '' <param name="sender"></param>
    '' <param name="e"></param>
    Private Sub cbProxy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbProxy.Click
        Dim cb As System.Windows.Forms.CheckBox = CType(sender, System.Windows.Forms.CheckBox)

        tbProxy.Enabled = cb.Checked
        tbProxyPort.Enabled = cb.Checked
        cbProxy.Checked = cb.Checked
        cbProxyType.Enabled = cb.Checked
        tbProxyLogin.Enabled = cb.Checked
        tbProxyPassword.Enabled = cb.Checked
    End Sub


    ' -------------------------------------------------------------------------
    '' <summary>
    '' global key handler
    '' </summary>
    '' <param name="sender"></param>
    '' <param name="e"></param>
    Private Sub Connection_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        ' when 'enter' is pressed the form is closed
        If e.KeyChar = ControlChars.Cr Then
            If ValidateForm() Then
                _ok = True
                Me.Close()
            End If
        End If

        ' when 'esc' is pressed the form is closed
        If e.KeyChar = Convert.ToChar(Keys.Escape) Then
            Me.Close()
        End If
    End Sub

    ' -------------------------------------------------------------------------
    '' <summary>
    '' security type change 
    '' </summary>
    '' <param name="sender"></param>
    '' <param name="e"></param>        
    Private Sub cbSecurity_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbSecurity.SelectedValueChanged
        Dim cb As System.Windows.Forms.ComboBox = CType(sender, System.Windows.Forms.ComboBox)

        Dim enabled As Boolean = (cb.SelectedIndex <> 0)

        cbSuite.Enabled = enabled
        cbTLS.Enabled = enabled
        cbSSL.Enabled = enabled
        cbCCC.Enabled = enabled
        btnCertificatePath.Enabled = enabled
        tbCertificatePath.Enabled = enabled
    End Sub


    Private Sub cbTLSSSL_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbSSL.CheckedChanged, cbTLS.CheckedChanged
        Dim cb As CheckBox = CType(sender, CheckBox)
        If Not cb.Checked AndAlso cbTLS.Checked = cbSSL.Checked Then
            cb.Checked = True
        End If
    End Sub

    Private Sub btnCertificatePath_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCertificatePath.Click
        If openFileDialog.ShowDialog() = DialogResult.OK Then
            tbCertificatePath.Text = openFileDialog.FileName
        End If
    End Sub
End Class