'  
'   Rebex Sample Code License
' 
'   Copyright (c) 2009, Rebex CR s.r.o. www.rebex.net, 
'   All rights reserved.
' 
'   Permission to use, copy, modify, and/or distribute this software for any
'   purpose with or without fee is hereby granted
' 
'   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
'   EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
'   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
'   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
'   HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
'   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
'   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
'   OTHER DEALINGS IN THE SOFTWARE.
' 

Imports Rebex.Net

''' <summary>
''' Helper class to hold an FTP connection information.
''' </summary>
Public Class ConnectionInfo
    Private Const SessionName As String = "sftpConnectionInfo"
    Private _password As String
    Private _serverName As String
    Private _userName As String
    Private _port As Integer
    Private _acceptAllCertificates As Boolean = False
    Private _security As FtpSecurity = FtpSecurity.Unsecure
    ''' <summary>
    ''' The server address - either a hostname or a dotted string IP address.
    ''' </summary>
    Public Property ServerName() As String
        Get
            Return _serverName
        End Get
        Set(ByVal value As String)
            _serverName = value
        End Set
    End Property

    ''' <summary>
    ''' Port of the remote server.
    ''' </summary>
    Public Property Port() As Integer
        Get
            Return _port
        End Get
        Set(ByVal value As Integer)
            _port = value
        End Set
    End Property

    ''' <summary>
    ''' User name for logging in.
    ''' </summary>
    Public Property UserName() As String
        Get
            Return _userName
        End Get
        Set(ByVal value As String)
            _userName = value
        End Set
    End Property

    ''' <summary>
    ''' Password for logging in.
    ''' </summary>
    Public Property Password() As String
        Get
            Return _password
        End Get
        Set(ByVal value As String)
            _password = value
        End Set
    End Property

    ''' <summary>
    ''' FTP connection security.
    ''' </summary>
    Public Property Security() As FtpSecurity
        Get
            Return _security
        End Get
        Set(ByVal value As FtpSecurity)
            _security = value
        End Set
    End Property

    ''' <summary>
    ''' Accept all certificates.
    ''' </summary>
    Public Property AcceptAllCertificates() As Boolean
        Get
            Return _acceptAllCertificates
        End Get
        Set(ByVal value As Boolean)
            _acceptAllCertificates = value
        End Set
    End Property

    ''' <summary>
    ''' Retrive saved connection information from the Session.
    ''' </summary>
    ''' <returns>
    ''' Instance of the <see cref="ConnectionInfo"/>; or null if no <see cref="ConnectionInfo"/> was saved.
    ''' </returns>
    Public Shared Function Load() As ConnectionInfo
        Return TryCast(System.Web.HttpContext.Current.Session(SessionName), ConnectionInfo)
    End Function

    ''' <summary>
    ''' Save the current connection information to the Session.
    ''' </summary>
    Public Sub Save()
        System.Web.HttpContext.Current.Session(SessionName) = Me
    End Sub

    ''' <summary>
    ''' Remove connection information from the Session.
    ''' </summary>
    Public Shared Sub Clear()
        System.Web.HttpContext.Current.Session(SessionName) = Nothing
    End Sub
End Class

