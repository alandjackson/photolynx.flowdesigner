//  
//   Rebex Sample Code License
// 
//   Copyright (c) 2009, Rebex CR s.r.o. www.rebex.net, 
//   All rights reserved.
// 
//   Permission to use, copy, modify, and/or distribute this software for any
//   purpose with or without fee is hereby granted
// 
//   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//   EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//   HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
//   OTHER DEALINGS IN THE SOFTWARE.
// 

using System;
using System.Net;
using Rebex.Net;

namespace Rebex.Samples.FileList
{
	/// <summary>
	/// This sample retrieves the list of files from a FTP server.
	/// It demonstrates the usage of FtpList class.
	/// </summary>
	class FileList
	{
		[STAThread]
		static void Main(string[] args)
		{
			if (args.Length!=1)
			{
				// display syntax if wrong number of parameters or unsupported operation
				Console.WriteLine("FileList - retrieves file list from a FTP server");
				Console.WriteLine("Syntax: FileList [username:[password]@]hostname/remotepath");
				Console.WriteLine("Example: FileList ftp.rebex.net/pub/example/");
				return;
			}

			// get username and password from URI
			Uri uri = new Uri ("ftp://" + args[0]);

			// create Ftp object and connect to the server
			Ftp ftp = new Ftp();
			ftp.Connect (uri.Host, uri.Port);
			try
			{
				ftp.Secure ();
				Console.WriteLine ("Connection is secured using TLS/SSL.");
			}
			catch (TlsException ex)
			{
				Console.WriteLine ("Connection is not secure: {0}", ex.Message);
			}
			string username;
			string password;
			if (uri.UserInfo.Length == 0)
			{
				Console.Write ("Username: ");
				username = Console.ReadLine();
				Console.Write ("Password: ");
				password = Console.ReadLine();
				if (password.Length == 0)
					password = null;
			}
			else
			{
				string[] userInfo = uri.UserInfo.Split(':');
				username = userInfo[0];
				if (userInfo.Length > 1)
					password = userInfo[1];
				else
				{
					Console.Write ("Password: ");
					password = Console.ReadLine();
				}
			}

			// login with username and password
			ftp.Login (username, password);

			ftp.ChangeDirectory (uri.AbsolutePath);
			FtpList list = ftp.GetList ();

			for (int i=0; i<list.Count; i++)
			{
				FtpItem item = list[i];
				if (item.IsSymlink)
					Console.Write ("s ");
				else if (item.IsDirectory)
					Console.Write ("d ");
				else
					Console.Write ("- ");

				Console.Write (item.Modified.ToString("u").Substring(0,16));
				Console.Write (item.Size.ToString().PadLeft(10,' '));
				Console.Write (" {0}", item.Name);
				if (item.IsSymlink)
					Console.Write (" -> " + item.SymlinkPath);
				Console.WriteLine ();
			}

			// disconnect from the server
			ftp.Disconnect();
		}
	}
}
