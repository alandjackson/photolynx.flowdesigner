'  
'   Rebex Sample Code License
' 
'   Copyright (c) 2009, Rebex CR s.r.o. www.rebex.net, 
'   All rights reserved.
' 
'   Permission to use, copy, modify, and/or distribute this software for any
'   purpose with or without fee is hereby granted
' 
'   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
'   EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
'   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
'   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
'   HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
'   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
'   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
'   OTHER DEALINGS IN THE SOFTWARE.
' 

Imports System
Imports System.IO
Imports Rebex.Net

Module GetPut

    ' This sample is a simple utility to upload/download a file to/from
    ' a FTP server using the Ftp class.
    Sub Main()
        Dim args() As String = Environment.GetCommandLineArgs()

        Dim method As String = Nothing
        If args.Length = 6 Then
            Select Case args(1).ToLower()
                Case "get"
                    method = "get"
                Case "put"
                    method = "put"
            End Select
        End If

        If method Is Nothing Then
            ' display syntax if wrong number of parameters or unsupported operation
            Console.WriteLine("GetPut - uploads or downloads a file from a FTP server")
            Console.WriteLine("Syntax: GetPut get|put unsecure|secure|implicit hostname remotepath localpath ")
            Console.WriteLine("Example: GetPut get secure ftp.rebex.net /pub/example/readme.txt readme.txt")
            Return
        End If

        Dim security As FtpSecurity = DirectCast(System.Enum.Parse(GetType(FtpSecurity), args(2), True), FtpSecurity)
        Dim hostname As String = args(3)
        Dim remotePath As String = args(4)
        Dim localPath As String = args(5)

        ' set SSL parameters to accept all server certificates...
        ' do not do this in production code, server certificates should
        ' be verified - use CertificateVerifier.Default instead
        Dim par As TlsParameters = New TlsParameters
        par.CommonName = hostname
        par.CertificateVerifier = CertificateVerifier.AcceptAll

        ' create Ftp object and connect to the server
        Dim ftp As New Ftp
        ftp.Connect(hostname, Ftp.DefaultPort, par, security)

        ' ask for username and password and login
        Console.Write("Username: ")
        Dim username As String = Console.ReadLine()
        Console.Write("Password: ")
        Dim password As String = Console.ReadLine()
        ftp.Login(username, password)

        ' transfer the file
        Dim bytes As Long
        If method = "get" Then
            bytes = ftp.GetFile(remotePath, localPath)
        Else
            bytes = ftp.PutFile(localPath, remotePath)
        End If
        Console.WriteLine("Transferred {0} bytes.", bytes)

        ' disconnect from the server
        ftp.Disconnect()
    End Sub

End Module
