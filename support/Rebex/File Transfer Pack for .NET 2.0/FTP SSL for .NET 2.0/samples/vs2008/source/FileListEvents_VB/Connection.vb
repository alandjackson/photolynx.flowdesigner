'  
'   Rebex Sample Code License
' 
'   Copyright (c) 2009, Rebex CR s.r.o. www.rebex.net, 
'   All rights reserved.
' 
'   Permission to use, copy, modify, and/or distribute this software for any
'   purpose with or without fee is hereby granted
' 
'   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
'   EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
'   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
'   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
'   HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
'   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
'   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
'   OTHER DEALINGS IN THE SOFTWARE.
' 


Imports System
Imports System.Drawing
Imports System.Collections
Imports System.ComponentModel
Imports System.Windows.Forms



'/ <summary>
'/ Summary description for Connection.
'/ </summary>

Public Class Connection
    Inherits System.Windows.Forms.Form
    '
    Private cbAcceptAll As System.Windows.Forms.CheckBox
    Private cbSecure As System.Windows.Forms.CheckBox
    Private lblPort As System.Windows.Forms.Label
    Private lblUsername As System.Windows.Forms.Label
    Private lblHostname As System.Windows.Forms.Label
    Private btnCancel As System.Windows.Forms.Button
    Private WithEvents btnConnect As System.Windows.Forms.Button
    Private lblPassword As System.Windows.Forms.Label
    Private tbPassword As System.Windows.Forms.TextBox
    Private tbUsername As System.Windows.Forms.TextBox
    Private tbHostname As System.Windows.Forms.TextBox
    Private tbPort As System.Windows.Forms.TextBox
    '/ <summary>
    '/ Required designer variable.
    '/ </summary>
    Private components As System.ComponentModel.Container = Nothing


    Public ReadOnly Property Hostname() As String
        Get
            Return tbHostname.Text
        End Get
    End Property


    Public ReadOnly Property Port() As Integer
        Get
            Dim temp As Integer

            Try
                temp = Int32.Parse(tbPort.Text)
            Catch
                temp = Rebex.Net.Ftp.DefaultPort
            End Try

            Return temp
        End Get
    End Property


    Public ReadOnly Property Username() As String
        Get
            Return tbUsername.Text
        End Get
    End Property


    Public ReadOnly Property Password() As String
        Get
            Return tbPassword.Text
        End Get
    End Property


    Public ReadOnly Property Secure() As Boolean
        Get
            Return cbSecure.Checked
        End Get
    End Property


    Public ReadOnly Property AcceptAll() As Boolean
        Get
            Return cbAcceptAll.Checked
        End Get
    End Property

    Public Sub New()
        '
        ' Required for Windows Form Designer support
        '
        InitializeComponent()
    End Sub 'New

    '
    ' TODO: Add any constructor code after InitializeComponent call
    '

    '/ <summary>
    '/ Clean up any resources being used.
    '/ </summary>
    Protected Overloads Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub 'Dispose

#Region "Windows Form Designer generated code"

    '/ <summary>
    '/ Required method for Designer support - do not modify
    '/ the contents of this method with the code editor.
    '/ </summary>
    Private Sub InitializeComponent()
        Me.cbAcceptAll = New System.Windows.Forms.CheckBox
        Me.cbSecure = New System.Windows.Forms.CheckBox
        Me.lblPort = New System.Windows.Forms.Label
        Me.lblUsername = New System.Windows.Forms.Label
        Me.lblHostname = New System.Windows.Forms.Label
        Me.tbPassword = New System.Windows.Forms.TextBox
        Me.tbUsername = New System.Windows.Forms.TextBox
        Me.tbHostname = New System.Windows.Forms.TextBox
        Me.tbPort = New System.Windows.Forms.TextBox
        Me.btnCancel = New System.Windows.Forms.Button
        Me.btnConnect = New System.Windows.Forms.Button
        Me.lblPassword = New System.Windows.Forms.Label
        Me.SuspendLayout()
        ' 
        ' cbAcceptAll
        ' 
        Me.cbAcceptAll.Location = New System.Drawing.Point(80, 56)
        Me.cbAcceptAll.Name = "cbAcceptAll"
        Me.cbAcceptAll.Size = New System.Drawing.Size(248, 32)
        Me.cbAcceptAll.TabIndex = 9
        Me.cbAcceptAll.Text = "Do not validate any server certificates - this is insecure but useful for testing" + "."
        ' 
        ' cbSecure
        ' 
        Me.cbSecure.Location = New System.Drawing.Point(8, 56)
        Me.cbSecure.Name = "cbSecure"
        Me.cbSecure.Size = New System.Drawing.Size(88, 32)
        Me.cbSecure.TabIndex = 8
        Me.cbSecure.Text = "TLS/SSL"
        ' 
        ' lblPort
        ' 
        Me.lblPort.Location = New System.Drawing.Point(240, 8)
        Me.lblPort.Name = "lblPort"
        Me.lblPort.Size = New System.Drawing.Size(32, 24)
        Me.lblPort.TabIndex = 2
        Me.lblPort.Text = "Port:"
        Me.lblPort.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        ' 
        ' lblUsername
        ' 
        Me.lblUsername.Location = New System.Drawing.Point(8, 32)
        Me.lblUsername.Name = "lblUsername"
        Me.lblUsername.Size = New System.Drawing.Size(72, 24)
        Me.lblUsername.TabIndex = 4
        Me.lblUsername.Text = "Username:"
        Me.lblUsername.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        ' 
        ' lblHostname
        ' 
        Me.lblHostname.Location = New System.Drawing.Point(8, 8)
        Me.lblHostname.Name = "lblHostname"
        Me.lblHostname.Size = New System.Drawing.Size(72, 24)
        Me.lblHostname.TabIndex = 0
        Me.lblHostname.Text = "Hostname:"
        Me.lblHostname.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        ' 
        ' tbPassword
        ' 
        Me.tbPassword.Location = New System.Drawing.Point(240, 32)
        Me.tbPassword.Name = "tbPassword"
        Me.tbPassword.PasswordChar = "*"c
        Me.tbPassword.Size = New System.Drawing.Size(88, 20)
        Me.tbPassword.TabIndex = 7
        Me.tbPassword.Text = ""
        ' 
        ' tbUsername
        ' 
        Me.tbUsername.Location = New System.Drawing.Point(80, 32)
        Me.tbUsername.Name = "tbUsername"
        Me.tbUsername.Size = New System.Drawing.Size(88, 20)
        Me.tbUsername.TabIndex = 5
        Me.tbUsername.Text = ""
        ' 
        ' tbHostname
        ' 
        Me.tbHostname.Location = New System.Drawing.Point(80, 8)
        Me.tbHostname.Name = "tbHostname"
        Me.tbHostname.Size = New System.Drawing.Size(152, 20)
        Me.tbHostname.TabIndex = 1
        Me.tbHostname.Text = ""
        ' 
        ' tbPort
        ' 
        Me.tbPort.Location = New System.Drawing.Point(272, 8)
        Me.tbPort.Name = "tbPort"
        Me.tbPort.Size = New System.Drawing.Size(56, 20)
        Me.tbPort.TabIndex = 3
        Me.tbPort.Text = "21"
        ' 
        ' btnCancel
        ' 
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnCancel.Location = New System.Drawing.Point(256, 88)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.TabIndex = 11
        Me.btnCancel.Text = "Ca&ncel"
        ' 
        ' btnConnect
        ' 
        Me.btnConnect.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnConnect.Location = New System.Drawing.Point(176, 88)
        Me.btnConnect.Name = "btnConnect"
        Me.btnConnect.TabIndex = 10
        Me.btnConnect.Text = "&Connect"
        ' 
        ' lblPassword
        ' 
        Me.lblPassword.Location = New System.Drawing.Point(176, 32)
        Me.lblPassword.Name = "lblPassword"
        Me.lblPassword.Size = New System.Drawing.Size(64, 24)
        Me.lblPassword.TabIndex = 6
        Me.lblPassword.Text = "Password:"
        Me.lblPassword.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        ' 
        ' Connection
        ' 
        Me.AcceptButton = Me.btnConnect
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(336, 118)
        Me.Controls.Add(lblPassword)
        Me.Controls.Add(btnConnect)
        Me.Controls.Add(btnCancel)
        Me.Controls.Add(cbAcceptAll)
        Me.Controls.Add(cbSecure)
        Me.Controls.Add(lblPort)
        Me.Controls.Add(lblUsername)
        Me.Controls.Add(lblHostname)
        Me.Controls.Add(tbPassword)
        Me.Controls.Add(tbUsername)
        Me.Controls.Add(tbHostname)
        Me.Controls.Add(tbPort)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Connection"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Connection"
        Me.ResumeLayout(False)
    End Sub 'InitializeComponent 
#End Region


    Private Sub btnConnect_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConnect.Click
        Me.DialogResult = DialogResult.OK
    End Sub 'btnConnect_Click
End Class 'Connection
