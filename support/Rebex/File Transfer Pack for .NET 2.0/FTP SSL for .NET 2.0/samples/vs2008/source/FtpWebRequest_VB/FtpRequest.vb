'  
'   Rebex Sample Code License
' 
'   Copyright (c) 2009, Rebex CR s.r.o. www.rebex.net, 
'   All rights reserved.
' 
'   Permission to use, copy, modify, and/or distribute this software for any
'   purpose with or without fee is hereby granted
' 
'   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
'   EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
'   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
'   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
'   HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
'   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
'   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
'   OTHER DEALINGS IN THE SOFTWARE.
' 

Imports System
Imports System.IO
Imports System.Net
Imports Rebex.Net

Module FtpRequest

    ' This sample demonstrates using FtpWebRequest class to download
    ' file from a FTP, HTTP or HTTPS server.
    Sub Main()
        Dim args() As String = Environment.GetCommandLineArgs()
        If args.Length < 3 Then
            ' display syntax if wrong number of parameters
            Console.WriteLine("FtpWebRequest - download file from a Uri.")
            Console.WriteLine("Syntax: FtpWebRequest uri localpath")
            Console.WriteLine("Example: FtpWebRequest ftp://ftp.rebex.net/pub/example/readme.txt readme.txt [username [password]]")
            Return
        End If

        ' register the component for the FTP prefix
        WebRequest.RegisterPrefix("ftp://", Rebex.Net.FtpWebRequest.Creator)

        ' create web request for the given URI
        Dim request As WebRequest = WebRequest.Create(args(1))

        ' support for user name and password from arguments
        If args.Length > 3 Then
            ' local variables
            Dim userName As String = ""
            Dim password As String = ""

            Select Case args.Length
                Case 4
                    userName = args(3)          ' get the user name
                Case 5
                    userName = args(3)          ' get the user name
                    password = args(4)          ' get the user name
            End Select

            If password.Trim() = "" Then
                Console.Write("Password: ")
                password = Console.ReadLine()   ' get the user name
            End If
            ' support for authentification
            request.Credentials = New NetworkCredential(userName, password)
        End If

        ' get and read web response
        Dim response As WebResponse = request.GetResponse()
        Dim stream As Stream = response.GetResponseStream()

        Dim local As Stream = File.Create(args(2))

        Dim buffer(1023) As Byte
        Dim n As Integer

        Console.WriteLine("Downloading...")

        Do
            n = stream.Read(buffer, 0, buffer.Length)
            local.Write(buffer, 0, n)
        Loop While n > 0

        ' close both streams
        stream.Close()
        local.Close()

        Console.WriteLine("Download finished.")
    End Sub

End Module
