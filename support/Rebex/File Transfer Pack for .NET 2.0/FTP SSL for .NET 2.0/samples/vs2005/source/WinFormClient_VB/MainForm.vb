'  
'   Rebex Sample Code License
' 
'   Copyright (c) 2009, Rebex CR s.r.o. www.rebex.net, 
'   All rights reserved.
' 
'   Permission to use, copy, modify, and/or distribute this software for any
'   purpose with or without fee is hereby granted
' 
'   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
'   EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
'   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
'   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
'   HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
'   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
'   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
'   OTHER DEALINGS IN THE SOFTWARE.
' 

Imports System
Imports System.Drawing
Imports System.Collections
Imports System.ComponentModel
Imports System.Windows.Forms
Imports System.Xml
Imports System.Resources
Imports System.IO
Imports System.Text
Imports System.Diagnostics
Imports Rebex.Net
Imports Rebex.Security.Certificates
Imports System.Text.RegularExpressions

'' <summary>
'' very simple ftp client
'' </summary>
Public Class MainForm
    Inherits System.Windows.Forms.Form

    Friend WithEvents btnErrorWarning As System.Windows.Forms.Button
    Private WithEvents gbTransfer As System.Windows.Forms.GroupBox
    Private components As System.ComponentModel.Container = Nothing
    Private tbLog As System.Windows.Forms.RichTextBox
    Private WithEvents listServer As System.Windows.Forms.ListView
    Private columnName As System.Windows.Forms.ColumnHeader
    Private columnSize As System.Windows.Forms.ColumnHeader
    Private lblServer As System.Windows.Forms.Label
    Private WithEvents listLocal As System.Windows.Forms.ListView
    Private lblLocal As System.Windows.Forms.Label
    Private WithEvents lblServerDirectory As System.Windows.Forms.Label
    Private columnHeader1 As System.Windows.Forms.ColumnHeader
    Private columnHeader2 As System.Windows.Forms.ColumnHeader
    Private WithEvents lblLocalDirectory As System.Windows.Forms.Label
    Private pbTransfer As System.Windows.Forms.ProgressBar
    Private lblProgress As System.Windows.Forms.Label
    Private lblBatchTransferProgress As System.Windows.Forms.Label
    Private columnDate As System.Windows.Forms.ColumnHeader
    Private columnHeader3 As System.Windows.Forms.ColumnHeader
    Private WithEvents btnStop As System.Windows.Forms.Button
    Private WithEvents btnDelete As System.Windows.Forms.Button
    Private WithEvents btnUpload As System.Windows.Forms.Button
    Private WithEvents btnDownload As System.Windows.Forms.Button
    Private rbBin As System.Windows.Forms.RadioButton
    Private rbAsc As System.Windows.Forms.RadioButton
    Private WithEvents btnRefresh As System.Windows.Forms.Button
    Private WithEvents btnMakeDir As System.Windows.Forms.Button
    Private WithEvents cbDrives As System.Windows.Forms.ComboBox
    Private WithEvents btnConnect As System.Windows.Forms.Button
    Private gbControl As System.Windows.Forms.GroupBox
    Private headerLeft As System.Windows.Forms.PictureBox
    Private WithEvents headerRight As System.Windows.Forms.PictureBox
    Private headerMiddle As System.Windows.Forms.Panel
    Private WithEvents trackBar As System.Windows.Forms.TrackBar
    Private lblSpeed As System.Windows.Forms.Label
#If (Not DOTNET10 And Not DOTNET11) Then
    Friend WithEvents cbModeZ As System.Windows.Forms.CheckBox
#End If
    Private pbSecure As System.Windows.Forms.PictureBox
    Private cbST As System.Windows.Forms.CheckBox

    Private _ftp As Ftp                             ' FTP session
    Private listActive As ListView                  ' active list (server x local)
    Private _serverDirs As Integer                  ' number of dirs on server side
    Private _localDirs As Integer                   ' number of dirs on local side
    Private _selectedDrive As Integer = -1          ' selected drive (A:, C:, ...) in combobox
    Private _localPath As String                    ' local path
    Private _isWorking As Boolean                   ' determines whether any operation is running
    Private _showTransferProgess As Boolean         ' determines whether to show transfer progress
    Private _totalFilesTransferred As Integer       ' total files transferred last time
    Private _totalBytesTransferred As Long          ' total bytes transferred last time
    Private _currentFileLength As Long              ' current transferring file length
    Private _currentBytesTransferred As Long        ' bytes transferred of the current transferring file
    Private _lastTransferProgressTime As DateTime   ' last TransferProgress event call time
    Private _transferTime As DateTime               ' transfer launch-time
    Private _cccMode As Boolean                     ' clear command channel enabled?
    Private _problemForm As ProblemDetectedForm     ' informs the user about problems while transferring data

    ' constansts
    Private Const SYMLINK As String = "--->"        ' symlink tag
    Private Const URL As String = "http://www.rebex.net/ftp-ssl.net/" ' url

    Private _exception As String

    Public Overloads Sub WriteToLog(ByVal x As Exception)
        ShowErrorWarning(x.ToString())

        ' FtpExceptions are automatilcally logged by component logger.
        ' No need to display them again.
        ' We don't wont duplicate entries for such exceptions.
        If TypeOf x Is FtpException Then Return

        WriteToLog("* " + x.ToString(), RichTextBoxLogWriter.COLORERROR)
    End Sub

    Private Sub ShowErrorWarning(ByVal message As String)
        _exception = message
        btnErrorWarning.Visible = True
    End Sub

    Public Overloads Sub WriteToLog(ByVal message As String, ByVal color As Color)
        tbLog.Focus()
        tbLog.SelectionColor = color
        tbLog.AppendText((message + ControlChars.Cr + ControlChars.Lf))
    End Sub

    '' <summary>
    '' get filename from path (xxxxx/xxxx/yyy -> yyy)
    '' </summary>
    '' <param name="path">ftp path</param>
    '' <returns>filename</returns>
    Private Shared Function GetFilenameFromPath(ByVal path As String) As String
        Dim idx As Integer = path.LastIndexOf("/")

        If idx <> -1 AndAlso idx + 1 <> path.Length Then
            path = path.Substring((idx + 1))
        End If

        Return path
    End Function 'GetFilenameFromPath

    '' <summary>
    '' file vs. directory detection
    '' </summary>
    '' <param name="dt">filename</param>
    '' <returns>true (is file), false (is directory)</returns>
    Private Shared Function IsFile(ByVal filename As String) As Boolean
        filename = GetFilenameFromPath(filename)
        Dim r As New Regex("(.*\..*)")
        Dim m As Match = r.Match(filename)
        Return m.Success
    End Function 'IsFile

    Private Shared Function IsDirectory(ByVal filename As String) As Boolean
        Return Not IsFile(filename)
    End Function 'IsDirectory

    '' <summary>
    '' format datetime to readable form in the panels
    '' </summary>
    '' <param name="dt">datetime</param>
    '' <returns>formatted string</returns>
    Private Shared Function FormatFileTime(ByVal dt As DateTime) As String
        Return dt.ToString("yyyy-MM-dd HH:mm")
    End Function 'FormatFileTime

    ''<summary>
    ''Wrapper for Invoke method that doesn't throw an exception after the object has been
    ''disposed while the calling method was running in a background thread.
    ''</summary>
    ''<param name="method"></param>
    ''<param name="args"></param>
    Private Sub SafeInvoke(ByVal method As [Delegate], ByVal args() As Object)
        Try
            If Not IsDisposed Then Invoke(method, args)
        Catch x As ObjectDisposedException
        End Try
    End Sub

    '' <summary>
    '' event displaying ftp state
    '' </summary>
    Private Sub StateChanged(ByVal sender As Object, ByVal e As FtpStateChangedEventArgs)
        Select Case e.NewState
            Case FtpState.Disconnected, FtpState.Disposed
                pbSecure.Visible = False
                lblBatchTransferProgress.Text = ""
                lblProgress.Text = "Disconnected"
                btnConnect.Text = "Connect..."
                listServer.Items.Clear()
            Case FtpState.Ready
                lblProgress.Text = "Ready"
        End Select
    End Sub

    '' <summary>
    '' event displaying transfer progress
    '' </summary>
    Private Sub TransferProgress(ByVal sender As Object, ByVal e As FtpTransferProgressEventArgs)
        If Not _showTransferProgess Then Return

        If e.BytesSinceLastEvent > 0 Then
            _currentBytesTransferred += e.BytesSinceLastEvent

            ' only update progress bar and byte counter once per second
            Dim now As DateTime = DateTime.Now
            Dim ts As TimeSpan = now.Subtract(_lastTransferProgressTime)
            If (ts.TotalSeconds < 0.5) Then Exit Sub
            _lastTransferProgressTime = now

            lblProgress.Text = e.BytesTransferred.ToString() + " bytes"

            Dim percentage As Integer = pbTransfer.Maximum
            If _currentFileLength > 0 Then
                Dim index As Decimal = CDec(_currentBytesTransferred) / CDec(_currentFileLength)
                If index < 1 Then percentage = CInt(Fix(index * pbTransfer.Maximum))
            End If

            If percentage <> pbTransfer.Value Then pbTransfer.Value = percentage
        End If
    End Sub

    '' <summary>
    '' handles the batch transfer progress event
    '' </summary>
    Private Sub BatchTransferProgress(ByVal sender As Object, ByVal e As FtpBatchTransferProgressEventArgs)
        Dim strBatchInfo As String = String.Format("({0} / {1} file{2})    ", _
            e.FilesProcessed, e.FilesTotal, IIf(e.FilesProcessed > 1, "s", ""))

        _showTransferProgess = False
        Select Case e.Operation
            Case FtpBatchTransferOperation.HierarchyRetrievalStarted
                strBatchInfo = "Retrieving hierarchy..."
            Case FtpBatchTransferOperation.HierarchyRetrievalFailed
                strBatchInfo = "Retrieve hierarchy failed."
            Case FtpBatchTransferOperation.HierarchyRetrieved
                strBatchInfo = String.Format("Hierarchy retrieved ({0} byte{1} in {2} file{3}).", _
                    e.BytesTotal, IIf(e.BytesTotal > 1, "s", ""), _
                    e.FilesTotal, IIf(e.FilesTotal > 1, "s", ""))
            Case FtpBatchTransferOperation.DirectoryProcessingStarted
                strBatchInfo += "Processing directory..."
            Case FtpBatchTransferOperation.DirectoryProcessingFailed
                strBatchInfo += "Directory processing failed."
            Case FtpBatchTransferOperation.DirectorySkipped
                strBatchInfo += "Directory skipped."
            Case FtpBatchTransferOperation.DirectoryCreated
                strBatchInfo += "Directory created."
            Case FtpBatchTransferOperation.FileProcessingStarted
                strBatchInfo += "Processing file..."
            Case FtpBatchTransferOperation.FileTransferStarting
                strBatchInfo += "Transferring file..."
                pbTransfer.Value = 0
                _showTransferProgess = True
                _currentBytesTransferred = 0
                _currentFileLength = e.CurrentFileLength
            Case FtpBatchTransferOperation.FileProcessingFailed
                strBatchInfo += "File processing failed."
            Case FtpBatchTransferOperation.FileSkipped
                strBatchInfo += "File skipped."
            Case FtpBatchTransferOperation.FileTransferred
                strBatchInfo += "File transferred."
                _totalFilesTransferred += 1
                _totalBytesTransferred += _currentBytesTransferred
        End Select

        lblBatchTransferProgress.Text = strBatchInfo
    End Sub

    '' <summary>
    '' handles the batch transfer problem detected event
    '' </summary>
    Private Sub BatchTransferProblemDetected(ByVal sender As Object, ByVal e As FtpBatchTransferProblemDetectedEventArgs)
        _problemForm.ShowModal(Me, e)
    End Sub

    Private Sub StateChangedProxy(ByVal sender As Object, ByVal e As FtpStateChangedEventArgs)
        SafeInvoke(New FtpStateChangedEventHandler(AddressOf Me.StateChanged), New Object() {sender, e})
    End Sub

    Private Sub TransferProgressProxy(ByVal sender As Object, ByVal e As FtpTransferProgressEventArgs)
        SafeInvoke(New FtpTransferProgressEventHandler(AddressOf Me.TransferProgress), New Object() {sender, e})
    End Sub

    Private Sub BatchTransferProgressProxy(ByVal sender As Object, ByVal e As FtpBatchTransferProgressEventArgs)
        SafeInvoke(New FtpBatchTransferProgressEventHandler(AddressOf Me.BatchTransferProgress), New Object() {sender, e})
    End Sub

    Private Sub BatchTransferProblemDetectedProxy(ByVal sender As Object, ByVal e As FtpBatchTransferProblemDetectedEventArgs)
        SafeInvoke(New FtpBatchTransferProblemDetectedEventHandler(AddressOf Me.BatchTransferProblemDetected), New Object() {sender, e})
    End Sub

    '' <summary>
    '' make local list + drives combo box
    '' </summary>
    Public Sub MakeLocalList()
        Dim dirs As Integer = 0
        Dim files As Integer = 0
        Dim size As Long = 0
        Dim c As Integer
        ' make a logical drives list [first run only]
        If cbDrives.Items.Count <= 0 Then

            Dim drives As String() = Directory.GetLogicalDrives()

            Dim lowerDrive As String = "c:\"

            ' find "lower" acceptable drive
            For c = drives.Length - 1 To 0 Step -1
                If drives(c).ToLower().CompareTo("c:\") >= 0 Then
                    lowerDrive = drives(c).ToLower()
                End If
            Next c

            ' feed the drives' list and select one
            For c = 0 To drives.Length - 1
                Dim drive As String = drives(c)

                cbDrives.Items.Add(drive)

                If _selectedDrive = -1 AndAlso drive.ToLower().CompareTo(lowerDrive) = 0 Then
                    cbDrives.SelectedIndex = c
                    _selectedDrive = c
                End If
            Next c

            If cbDrives.SelectedIndex <= 0 Then
                cbDrives.SelectedIndex = 0
            End If
        End If
        ' create the list
        listLocal.Items.Clear()

        ' directory up
        If Not (_localPath Is Nothing) AndAlso _localPath.Length > 3 Then
            listLocal.Items.Add(New ListViewItem("..", 0))
            dirs += 1
        Else
            _localPath = cbDrives.SelectedItem.ToString()
        End If

        Dim currentDirectory As New DirectoryInfo(_localPath)
        Dim list As DirectoryInfo() = currentDirectory.GetDirectories()

        For c = 0 To list.Length - 1
            Dim row(2) As String
            row(0) = list(c).Name
            row(1) = ""

            ' time [on read-only devices LWT is not available for dirs?]
            Try
                row(2) = FormatFileTime(list(c).LastWriteTime)
            Catch
                row(2) = ""
            End Try

            listLocal.Items.Add(New ListViewItem(row, 1 + c))
            dirs += 1
        Next c

        _localDirs = dirs

        ' files
        Dim list2 As FileInfo() = currentDirectory.GetFiles()

        For c = 0 To list2.Length - 1
            Dim row(2) As String
            row(0) = list2(c).Name
            row(1) = list2(c).Length.ToString()
            row(2) = FormatFileTime(list2(c).LastWriteTime)

            listLocal.Items.Add(New ListViewItem(row, 1 + dirs + files))
            size += list2(c).Length
            files += 1
        Next c

        ' stats
        lblLocal.Text = dirs.ToString() + " dir" + IIf(dirs > 1, "s", "") + " " + files.ToString() + " file" + IIf(files > 1, "s", "") + " " + System.Convert.ToInt32(size / 1024).ToString() + " K"

        ' working directory
        lblLocalDirectory.Text = currentDirectory.FullName
        _localPath = currentDirectory.FullName
    End Sub

    '' <summary>
    '' make ftp list
    '' </summary>
    Public Sub MakeFtpList()
        Dim dirs As Integer = 0
        Dim files As Integer = 0
        Dim size As Long = 0

        listServer.Items.Clear()

        ' not connected?
        If _ftp Is Nothing Then
            lblServerDirectory.Text = ""
            lblServer.Text = ""
            _serverDirs = 0
            Return
        End If

        ' secure transfers
        If cbST.Enabled AndAlso Not _cccMode Then _ftp.SecureTransfers = cbST.Checked

        ' make the list
        Dim list As FtpList = Nothing

        Try
            _isWorking = True
            btnStop.Visible = True
            Dim ar As IAsyncResult = _ftp.BeginGetList(Nothing, Nothing)
            While Not ar.IsCompleted
                Application.DoEvents()
                System.Threading.Thread.Sleep(1)
            End While
            list = _ftp.EndGetList(ar)
            list.Sort()
        Catch ex As Exception
            WriteToLog(ex)
            listServer.Items.Add(New ListViewItem("..", 0))
            Return
        Finally
            btnStop.Visible = False
            _isWorking = False
        End Try

        ' directories
        listServer.Items.Add(New ListViewItem("..", 0))
        dirs += 1

        Dim c As Integer
        For c = 0 To list.Count - 1
            ' symlink
            If list(c).IsSymlink AndAlso IsDirectory(list(c).SymlinkPath) Then
                Dim row(2) As String
                row(0) = list(c).Name
                row(1) = SYMLINK
                row(2) = FormatFileTime(list(c).Modified)

                listServer.Items.Add(New ListViewItem(row, 1 + dirs))
                dirs += 1
            End If

            ' normal directory
            If list(c).IsDirectory Then
                Dim row(2) As String
                row(0) = list(c).Name
                row(1) = Nothing
                row(2) = FormatFileTime(list(c).Modified)

                listServer.Items.Add(New ListViewItem(row, 1 + dirs))
                dirs += 1
            End If
        Next c

        ' files
        For c = 0 To list.Count - 1
            ' symlink
            If list(c).IsSymlink AndAlso IsFile(list(c).SymlinkPath) Then
                Dim row(2) As String
                row(0) = list(c).Name
                row(1) = SYMLINK
                row(2) = FormatFileTime(list(c).Modified)
                listServer.Items.Add(New ListViewItem(row, 1 + dirs + files))
                files += 1
            End If

            ' normal file
            If list(c).IsFile Then
                Dim row(2) As String
                row(0) = list(c).Name
                row(1) = list(c).Size.ToString()
                row(2) = FormatFileTime(list(c).Modified)
                listServer.Items.Add(New ListViewItem(row, 1 + dirs + files))
                size += list(c).Size
                files += 1
            End If
        Next c

        ' stats
        lblServer.Text = dirs.ToString() + " dir" + IIf(dirs > 1, "s", "") + " " + files.ToString() + " file" + IIf(files > 1, "s", "") + " " + System.Convert.ToInt32(size / 1024).ToString() + " K"

        ' working directory
        lblServerDirectory.Text = _ftp.GetCurrentDirectory()

        _serverDirs = dirs
    End Sub

    '' <summary>
    '' constructor
    '' </summary>
    Public Sub New()
        MyBase.New()
        InitializeComponent()

#If (Not DOTNET10 And Not DOTNET11) Then
        CheckForIllegalCrossThreadCalls = True
        Application.EnableVisualStyles()
#End If

        Width = 640
        SetRightSizeValues()

        rbBin.Checked = True
        trackBar.Enabled = False
        btnErrorWarning.Visible = False
        lblServer.Text = ""
        listLocal.Focus()
        btnConnect.Select()

        MakeLocalList()
        _problemForm = New ProblemDetectedForm
    End Sub

    '' <summary>
    '' clean up any resources being used
    '' </summary>
    Protected Overloads Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

#Region "Windows Form Designer generated code"

    '/ <summary>
    '/ Required method for Designer support - do not modify
    '/ the contents of this method with the code editor.
    '/ </summary>
    Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(MainForm))
        Me.listServer = New System.Windows.Forms.ListView
        Me.columnName = New System.Windows.Forms.ColumnHeader
        Me.columnSize = New System.Windows.Forms.ColumnHeader
        Me.columnDate = New System.Windows.Forms.ColumnHeader
        Me.listLocal = New System.Windows.Forms.ListView
        Me.columnHeader1 = New System.Windows.Forms.ColumnHeader
        Me.columnHeader2 = New System.Windows.Forms.ColumnHeader
        Me.columnHeader3 = New System.Windows.Forms.ColumnHeader
        Me.tbLog = New System.Windows.Forms.RichTextBox
        Me.lblServer = New System.Windows.Forms.Label
        Me.lblServerDirectory = New System.Windows.Forms.Label
        Me.lblLocal = New System.Windows.Forms.Label
        Me.lblLocalDirectory = New System.Windows.Forms.Label
        Me.pbTransfer = New System.Windows.Forms.ProgressBar
        Me.lblProgress = New System.Windows.Forms.Label
        Me.lblBatchTransferProgress = New System.Windows.Forms.Label
        Me.btnStop = New System.Windows.Forms.Button
        Me.btnDelete = New System.Windows.Forms.Button
        Me.btnUpload = New System.Windows.Forms.Button
        Me.btnDownload = New System.Windows.Forms.Button
        Me.rbBin = New System.Windows.Forms.RadioButton
        Me.rbAsc = New System.Windows.Forms.RadioButton
        Me.btnRefresh = New System.Windows.Forms.Button
        Me.btnMakeDir = New System.Windows.Forms.Button
        Me.cbDrives = New System.Windows.Forms.ComboBox
        Me.btnConnect = New System.Windows.Forms.Button
        Me.gbControl = New System.Windows.Forms.GroupBox
        Me.gbTransfer = New System.Windows.Forms.GroupBox
        Me.headerLeft = New System.Windows.Forms.PictureBox
        Me.headerMiddle = New System.Windows.Forms.Panel
        Me.headerRight = New System.Windows.Forms.PictureBox
        Me.btnErrorWarning = New System.Windows.Forms.Button
        Me.trackBar = New System.Windows.Forms.TrackBar
        Me.lblSpeed = New System.Windows.Forms.Label
#If (Not DOTNET10 And Not DOTNET11) Then
        Me.cbModeZ = New System.Windows.Forms.CheckBox
#End If
        Me.cbST = New System.Windows.Forms.CheckBox
        Me.pbSecure = New System.Windows.Forms.PictureBox
        Me.gbControl.SuspendLayout()
        Me.gbTransfer.SuspendLayout()
        Me.headerMiddle.SuspendLayout()
        CType(Me.trackBar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'listServer
        '
        Me.listServer.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.listServer.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.columnName, Me.columnSize, Me.columnDate})
        Me.listServer.FullRowSelect = True
        Me.listServer.Location = New System.Drawing.Point(8, 80)
        Me.listServer.MultiSelect = False
        Me.listServer.Name = "listServer"
        Me.listServer.Size = New System.Drawing.Size(264, 303)
        Me.listServer.TabIndex = 2
        Me.listServer.View = System.Windows.Forms.View.Details
        '
        'columnName
        '
        Me.columnName.Text = "Name"
        Me.columnName.Width = 120
        '
        'columnSize
        '
        Me.columnSize.Text = "Size"
        Me.columnSize.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'columnDate
        '
        Me.columnDate.Text = "Date"
        Me.columnDate.Width = 120
        '
        'listLocal
        '
        Me.listLocal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.listLocal.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.columnHeader1, Me.columnHeader2, Me.columnHeader3})
        Me.listLocal.FullRowSelect = True
        Me.listLocal.Location = New System.Drawing.Point(396, 80)
        Me.listLocal.MultiSelect = False
        Me.listLocal.Name = "listLocal"
        Me.listLocal.Size = New System.Drawing.Size(256, 338)
        Me.listLocal.TabIndex = 25
        Me.listLocal.View = System.Windows.Forms.View.Details
        '
        'columnHeader1
        '
        Me.columnHeader1.Text = "Name"
        Me.columnHeader1.Width = 120
        '
        'columnHeader2
        '
        Me.columnHeader2.Text = "Size"
        Me.columnHeader2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'columnHeader3
        '
        Me.columnHeader3.Text = "Date"
        Me.columnHeader3.Width = 120
        '
        'tbLog
        '
        Me.tbLog.AccessibleRole = System.Windows.Forms.AccessibleRole.None
        Me.tbLog.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tbLog.BackColor = System.Drawing.SystemColors.Control
        Me.tbLog.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.tbLog.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbLog.Location = New System.Drawing.Point(0, 442)
        Me.tbLog.MaxLength = 1000000
        Me.tbLog.Name = "tbLog"
        Me.tbLog.ReadOnly = True
        Me.tbLog.Size = New System.Drawing.Size(664, 94)
        Me.tbLog.TabIndex = 8
        Me.tbLog.Text = ""
        Me.tbLog.WordWrap = False
        '
        'lblServer
        '
        Me.lblServer.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lblServer.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblServer.Location = New System.Drawing.Point(8, 390)
        Me.lblServer.Name = "lblServer"
        Me.lblServer.Size = New System.Drawing.Size(256, 13)
        Me.lblServer.TabIndex = 9
        Me.lblServer.Text = "12345"
        '
        'lblServerDirectory
        '
        Me.lblServerDirectory.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.lblServerDirectory.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.lblServerDirectory.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblServerDirectory.ForeColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.lblServerDirectory.Location = New System.Drawing.Point(8, 64)
        Me.lblServerDirectory.Name = "lblServerDirectory"
        Me.lblServerDirectory.Size = New System.Drawing.Size(264, 16)
        Me.lblServerDirectory.TabIndex = 1
        Me.lblServerDirectory.Text = "/root"
        Me.lblServerDirectory.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblLocal
        '
        Me.lblLocal.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lblLocal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLocal.Location = New System.Drawing.Point(396, 420)
        Me.lblLocal.Name = "lblLocal"
        Me.lblLocal.Size = New System.Drawing.Size(256, 13)
        Me.lblLocal.TabIndex = 12
        Me.lblLocal.Text = "12345"
        '
        'lblLocalDirectory
        '
        Me.lblLocalDirectory.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.lblLocalDirectory.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.lblLocalDirectory.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLocalDirectory.ForeColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.lblLocalDirectory.Location = New System.Drawing.Point(400, 64)
        Me.lblLocalDirectory.Name = "lblLocalDirectory"
        Me.lblLocalDirectory.Size = New System.Drawing.Size(256, 16)
        Me.lblLocalDirectory.TabIndex = 23
        Me.lblLocalDirectory.Text = "/root"
        Me.lblLocalDirectory.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pbTransfer
        '
        Me.pbTransfer.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pbTransfer.Location = New System.Drawing.Point(0, 537)
        Me.pbTransfer.Name = "pbTransfer"
        Me.pbTransfer.Size = New System.Drawing.Size(272, 20)
        Me.pbTransfer.Step = 1
        Me.pbTransfer.TabIndex = 20
        '
        'lblProgress
        '
        Me.lblProgress.BackColor = System.Drawing.Color.Transparent
        Me.lblProgress.Location = New System.Drawing.Point(280, 537)
        Me.lblProgress.Name = "lblProgress"
        Me.lblProgress.Size = New System.Drawing.Size(94, 20)
        Me.lblProgress.TabIndex = 24
        Me.lblProgress.Text = "Welcome!"
        Me.lblProgress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblBatchTransferProgress
        '
        Me.lblBatchTransferProgress.BackColor = System.Drawing.Color.Transparent
        Me.lblBatchTransferProgress.Location = New System.Drawing.Point(282, 537)
        Me.lblBatchTransferProgress.Name = "lblBatchTransferProgress"
        Me.lblBatchTransferProgress.Size = New System.Drawing.Size(264, 20)
        Me.lblBatchTransferProgress.TabIndex = 24
        Me.lblBatchTransferProgress.Text = ""
        Me.lblBatchTransferProgress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnStop
        '
        Me.btnStop.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnStop.Location = New System.Drawing.Point(16, 221)
        Me.btnStop.Name = "btnStop"
        Me.btnStop.TabIndex = 17
        Me.btnStop.Text = "Abort"
        Me.btnStop.Visible = False
        '
        'btnDelete
        '
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnDelete.Location = New System.Drawing.Point(16, 125)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.TabIndex = 11
        Me.btnDelete.Text = "Delete"
        '
        'btnUpload
        '
        Me.btnUpload.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnUpload.Location = New System.Drawing.Point(16, 194)
        Me.btnUpload.Name = "btnUpload"
        Me.btnUpload.TabIndex = 15
        Me.btnUpload.Text = "< Upload"
        '
        'btnDownload
        '
        Me.btnDownload.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnDownload.Location = New System.Drawing.Point(16, 167)
        Me.btnDownload.Name = "btnDownload"
        Me.btnDownload.TabIndex = 13
        Me.btnDownload.Text = "Download >"
        '
        'rbBin
        '
        Me.rbBin.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.rbBin.Location = New System.Drawing.Point(16, 16)
        Me.rbBin.Name = "rbBin"
        Me.rbBin.Size = New System.Drawing.Size(65, 20)
        Me.rbBin.TabIndex = 19
        Me.rbBin.Text = "Binary"
        '
        'rbAsc
        '
        Me.rbAsc.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.rbAsc.Location = New System.Drawing.Point(16, 37)
        Me.rbAsc.Name = "rbAsc"
        Me.rbAsc.Size = New System.Drawing.Size(68, 20)
        Me.rbAsc.TabIndex = 21
        Me.rbAsc.Text = "ASCII"
        '
        'btnRefresh
        '
        Me.btnRefresh.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnRefresh.Location = New System.Drawing.Point(16, 71)
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.TabIndex = 5
        Me.btnRefresh.Text = "Refresh"
        '
        'btnMakeDir
        '
        Me.btnMakeDir.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnMakeDir.Location = New System.Drawing.Point(16, 98)
        Me.btnMakeDir.Name = "btnMakeDir"
        Me.btnMakeDir.TabIndex = 7
        Me.btnMakeDir.Text = "Create folder"
        '
        'cbDrives
        '
        Me.cbDrives.ItemHeight = 13
        Me.cbDrives.Location = New System.Drawing.Point(17, 40)
        Me.cbDrives.Name = "cbDrives"
        Me.cbDrives.Size = New System.Drawing.Size(73, 21)
        Me.cbDrives.TabIndex = 4
        '
        'btnConnect
        '
        Me.btnConnect.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnConnect.Location = New System.Drawing.Point(16, 16)
        Me.btnConnect.Name = "btnConnect"
        Me.btnConnect.TabIndex = 3
        Me.btnConnect.Text = "Connect..."
        '
        'gbControl
        '
        Me.gbControl.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.gbControl.Controls.Add(Me.btnStop)
        Me.gbControl.Controls.Add(Me.btnDelete)
        Me.gbControl.Controls.Add(Me.btnUpload)
        Me.gbControl.Controls.Add(Me.btnDownload)
        Me.gbControl.Controls.Add(Me.btnRefresh)
        Me.gbControl.Controls.Add(Me.btnMakeDir)
        Me.gbControl.Controls.Add(Me.cbDrives)
        Me.gbControl.Controls.Add(Me.btnConnect)
        Me.gbControl.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.gbControl.Location = New System.Drawing.Point(284, 64)
        Me.gbControl.Name = "gbControl"
        Me.gbControl.Size = New System.Drawing.Size(106, 250)
        Me.gbControl.TabIndex = 25
        Me.gbControl.TabStop = False
        Me.gbControl.Text = "Controls"
        '
        'gbTransfer
        '
        Me.gbTransfer.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.gbTransfer.Controls.Add(Me.rbBin)
        Me.gbTransfer.Controls.Add(Me.rbAsc)
#If (Not DOTNET10 And Not DOTNET11) Then
        Me.gbTransfer.Controls.Add(Me.cbModeZ)
#End If
        Me.gbTransfer.Controls.Add(Me.cbST)
#If (Not DOTNET10 And Not DOTNET11) Then
        Me.gbTransfer.Size = New System.Drawing.Size(106, 106)
#Else
        Me.gbTransfer.Size = New System.Drawing.Size(106, 82)
#End If
        Me.gbTransfer.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.gbTransfer.Location = New System.Drawing.Point(284, 321)
        Me.gbTransfer.Name = "gbTransfer"
        Me.gbTransfer.TabIndex = 29
        Me.gbTransfer.TabStop = False
        Me.gbTransfer.Text = "Transfer"
#If (Not DOTNET10 And Not DOTNET11) Then
        '
        'cbModeZ
        '
        Me.cbModeZ.Location = New System.Drawing.Point(16, 56)
        Me.cbModeZ.Name = "cbModeZ"
        Me.cbModeZ.Size = New System.Drawing.Size(68, 24)
        Me.cbModeZ.TabIndex = 29
        Me.cbModeZ.Text = "Mode Z"
#End If
        '
        'cbST
        '
#If (Not DOTNET10 And Not DOTNET11) Then
        Me.cbST.Location = New System.Drawing.Point(16, 77)
#Else
        Me.cbST.Location = New System.Drawing.Point(16, 56)
#End If
        Me.cbST.Name = "cbST"
        Me.cbST.Size = New System.Drawing.Size(68, 24)
        Me.cbST.TabIndex = 30
        Me.cbST.Text = "Secure"
        '
        'pbSecure
        '
        Me.pbSecure.Image = CType(resources.GetObject("pbSecure.Image"), System.Drawing.Image)
        Me.pbSecure.Location = New System.Drawing.Point(24, 16)
        Me.pbSecure.Name = "pbSecure"
        Me.pbSecure.Size = New System.Drawing.Size(32, 32)
        Me.pbSecure.TabIndex = 29
        Me.pbSecure.TabStop = False
        Me.pbSecure.Visible = False
        '
        'headerMiddle
        '
        Me.headerMiddle.BackColor = System.Drawing.Color.White
        Me.headerMiddle.BackgroundImage = CType(resources.GetObject("headerMiddle.BackgroundImage"), System.Drawing.Image)
        Me.headerMiddle.Controls.Add(Me.pbSecure)
        Me.headerMiddle.Location = New System.Drawing.Point(472, 0)
        Me.headerMiddle.Name = "headerMiddle"
        Me.headerMiddle.Size = New System.Drawing.Size(64, 59)
        Me.headerMiddle.TabIndex = 27
        '
        'headerLeft
        '
        Me.headerLeft.BackColor = System.Drawing.Color.White
        Me.headerLeft.Image = CType(resources.GetObject("headerLeft.Image"), System.Drawing.Image)
        Me.headerLeft.Location = New System.Drawing.Point(0, 0)
        Me.headerLeft.Name = "headerLeft"
        Me.headerLeft.Size = New System.Drawing.Size(472, 59)
        Me.headerLeft.TabIndex = 26
        Me.headerLeft.TabStop = False
        '
        'headerRight
        '
        Me.headerRight.BackColor = System.Drawing.Color.White
        Me.headerRight.Cursor = System.Windows.Forms.Cursors.Hand
        Me.headerRight.Image = CType(resources.GetObject("headerRight.Image"), System.Drawing.Image)
        Me.headerRight.Location = New System.Drawing.Point(536, 0)
        Me.headerRight.Name = "headerRight"
        Me.headerRight.Size = New System.Drawing.Size(136, 59)
        Me.headerRight.TabIndex = 28
        Me.headerRight.TabStop = False
        '
        'btnErrorWarning
        '
        Me.btnErrorWarning.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnErrorWarning.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnErrorWarning.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.btnErrorWarning.Location = New System.Drawing.Point(643, 535)
        Me.btnErrorWarning.Name = "btnErrorWarning"
        Me.btnErrorWarning.Size = New System.Drawing.Size(21, 25)
        Me.btnErrorWarning.TabIndex = 29
        Me.btnErrorWarning.Text = "!"
        '
        'trackBar
        '
        Me.trackBar.LargeChange = 10
        Me.trackBar.Location = New System.Drawing.Point(2, 405)
        Me.trackBar.Maximum = 100
        Me.trackBar.Minimum = 1
        Me.trackBar.Name = "trackBar"
        Me.trackBar.Size = New System.Drawing.Size(282, 45)
        Me.trackBar.SmallChange = 5
        Me.trackBar.TabIndex = 30
        Me.trackBar.TickFrequency = 5
        Me.trackBar.Value = 100
        '
        'lblSpeed
        '
        Me.lblSpeed.Location = New System.Drawing.Point(185, 390)
        Me.lblSpeed.Name = "lblSpeed"
        Me.lblSpeed.Size = New System.Drawing.Size(90, 13)
        Me.lblSpeed.TabIndex = 0
        Me.lblSpeed.Text = "Speed: Unlimited"
        '
        'MainForm
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(664, 560)
        Me.Controls.Add(Me.lblSpeed)
        Me.Controls.Add(Me.btnErrorWarning)
        Me.Controls.Add(Me.lblServerDirectory)
        Me.Controls.Add(Me.headerRight)
        Me.Controls.Add(Me.headerMiddle)
        Me.Controls.Add(Me.headerLeft)
        Me.Controls.Add(Me.gbControl)
        Me.Controls.Add(Me.lblProgress)
        Me.Controls.Add(Me.lblBatchTransferProgress)
        Me.Controls.Add(Me.pbTransfer)
        Me.Controls.Add(Me.lblLocalDirectory)
        Me.Controls.Add(Me.lblLocal)
        Me.Controls.Add(Me.lblServer)
        Me.Controls.Add(Me.tbLog)
        Me.Controls.Add(Me.listLocal)
        Me.Controls.Add(Me.listServer)
        Me.Controls.Add(Me.trackBar)
        Me.Controls.Add(Me.gbTransfer)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MinimumSize = New System.Drawing.Size(664, 590)
        Me.Name = "MainForm"
        Me.Text = "Simple WinForm FTP Client"
        Me.gbControl.ResumeLayout(False)
        Me.gbTransfer.ResumeLayout(False)
        Me.headerMiddle.ResumeLayout(False)
        CType(Me.trackBar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub 'InitializeComponent 
#End Region

    '' <summary>
    '' win form client starts here
    '' </summary>
    <STAThread()> _
    Shared Sub Main()
        Application.Run(New MainForm)
    End Sub

    '' <summary>
    '' connect/disconnect ftp server
    '' </summary>
    Private Sub btnConnect_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConnect.Click
        If Not (_ftp Is Nothing) Then
            If _isWorking Then Return

            ' disconnect
            trackBar.Enabled = False
            _ftp.Disconnect()
            _ftp.Dispose()
            _ftp = Nothing
            btnConnect.Text = "Connect..."
            MakeFtpList()   ' clearing
            Return
        End If

        ' connection dialog
        Dim con As New Connection

        ' show connection dialog
        con.ShowDialog()

        ' not confirmed
        If Not con.OK Then Return

        ' create ftp object
        _ftp = New Ftp

        ' assign the logger for logging CommandSent, ResponseRead and TlsDebug events (and exceptions thrown by Ftp class)
        _ftp.LogWriter = New RichTextBoxLogWriter(tbLog, tbLog.MaxLength, con.LogLevel)

        ' set event handlers
        AddHandler _ftp.TransferProgress, AddressOf TransferProgressProxy
        AddHandler _ftp.StateChanged, AddressOf StateChangedProxy
        AddHandler _ftp.BatchTransferProgress, AddressOf BatchTransferProgressProxy
        AddHandler _ftp.BatchTransferProblemDetected, AddressOf BatchTransferProblemDetectedProxy

        Try
            _ftp.Passive = con.Passive

            If con.UseLargeBuffers Then
                _ftp.Options = _ftp.Options Or FtpOptions.UseLargeBuffers
            End If

            _ftp.Timeout = 30000

            ' proxy
            If con.ProxyEnabled Then
                If con.ProxyLogin.Length > 0 Then
                    If con.ProxyPassword.Length = 0 Then
                        con.ProxyPassword = Nothing
                    End If
                    _ftp.Proxy = New FtpProxy(CType(con.ProxyType, FtpProxyType), con.Proxy, con.ProxyPort, New System.Net.NetworkCredential(con.ProxyLogin, con.ProxyPassword))
                Else
                    _ftp.Proxy = New FtpProxy(CType(con.ProxyType, FtpProxyType), con.Proxy, con.ProxyPort)
                End If
            End If

            Dim p As TlsParameters = Nothing
            If con.SecurityType <> FtpSecurity.Unsecure Then
                p = New TlsParameters
                p.CertificateVerifier = New Verifier
                p.AllowedSuites = con.AllowedSuite
                p.CommonName = con.Host
                p.Version = con.Protocol

                If con.CertificatePath.Trim() <> String.Empty Then
                    Dim pp As PassphraseDialog = New PassphraseDialog
                    If pp.ShowDialog() = DialogResult.OK Then
                        Dim chain As CertificateChain = CertificateChain.LoadPfx(con.CertificatePath, pp.Passphrase)
                        p.CertificateRequestHandler = CertificateRequestHandler.CreateRequestHandler(chain)
                    Else
                        Throw New ApplicationException("Passphrase wasn't entered.")
                    End If
                Else
                    p.CertificateRequestHandler = New RequestHandler
                End If

                _ftp.SecureTransfers = cbST.Checked
            End If

            ' connect

            btnStop.Visible = True
            _isWorking = True

            Try
                Dim ar As IAsyncResult = _ftp.BeginConnect(con.Host, con.Port, p, con.SecurityType, Nothing, Nothing)
                While Not ar.IsCompleted
                    Application.DoEvents()
                    System.Threading.Thread.Sleep(1)
                End While
                _ftp.EndConnect(ar)
            Finally
                btnStop.Visible = False
                _isWorking = False
            End Try

            If con.SecurityType = FtpSecurity.Unsecure Then
                cbST.Checked = False
                cbST.Enabled = False
                pbSecure.Visible = False
            Else
                cbST.Checked = True
                cbST.Enabled = True
                pbSecure.Visible = True
            End If

            ' login
            If Not _ftp.IsAuthenticated Then
                _ftp.Login(con.Login, con.Password)
            End If

#If (Not DOTNET10 And Not DOTNET11) Then
            If cbModeZ.Checked Then
                _ftp.TransferMode = FtpTransferMode.Zlib
            End If
#End If

            ' ccc 
            If con.ClearCommandChannel And con.SecurityType <> FtpSecurity.Unsecure Then
                Try
                    _ftp.SecureTransfers = True
                    _ftp.ClearCommandChannel()
                    cbST.Checked = True
                    cbST.Enabled = False
                    _cccMode = True
                Catch ex As Exception
                    WriteToLog(ex)
                End Try
            End If

            trackBar.Enabled = True
            btnErrorWarning.Visible = False
            btnConnect.Text = "Disconnect"
            MakeFtpList()
        Catch ex As Exception
            WriteToLog(ex)
            If Not _ftp Is Nothing Then
                _ftp.Dispose()
                _ftp = Nothing
            End If
            Return
        Finally
            _isWorking = False
        End Try
    End Sub

    '' <summary>
    '' disconnect on exit
    '' </summary>
    Private Sub MainForm_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        If Not (_ftp Is Nothing) Then
            _ftp.Dispose()
        End If
    End Sub

    '' <summary>
    '' CWD or GET file command on server side
    '' </summary>
    Private Sub ListServerClick()
        Try
            If listServer.Items.Count < 1 Then Return

            If listServer.SelectedItems.Count < 1 Then
                listServer.Items(0).Selected = True
            End If

            listServer.Focus()

            ' ftp state test
            If _isWorking Then Return

            Dim index As Integer = listServer.SelectedIndices(0)
            Dim item As ListViewItem = listServer.SelectedItems(0)

            If index >= _serverDirs Then
                ' GET file
                DownloadFiles(item.Text, _localPath)
            Else
                ' CWD
                Dim directory As String
                If index = 0 Then
                    directory = ".."
                Else
                    directory = item.Text
                End If

                _ftp.ChangeDirectory(directory)

                MakeFtpList()
            End If
        Catch ex As Exception
            WriteToLog(ex)
        End Try
    End Sub

    Private Overloads Sub listServer_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles listServer.KeyPress
        If e.KeyChar = ControlChars.Cr Then
            ListServerClick()
            listServer.Focus()
        End If
    End Sub

    Private Overloads Sub listServer_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles listServer.DoubleClick
        ListServerClick()
        listServer.Focus()
    End Sub

    '' <summary>
    '' change drive letter
    '' </summary>
    Private Sub cbDrives_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbDrives.SelectedIndexChanged
        Try
            _localPath = cbDrives.SelectedItem.ToString()
            MakeLocalList()
        Catch ex As Exception
            WriteToLog(ex)
            listLocal.Items.Clear()
            lblLocalDirectory.Text = (lblLocal.Text = Nothing)
        End Try
    End Sub

    '' <summary>
    '' LCD or PUT command on local side
    '' </summary>
    Private Sub ListLocalClick()
        Try
            If listLocal.Items.Count < 1 Then Return

            If listLocal.SelectedItems.Count < 1 Then
                listLocal.Items(0).Selected = True
            End If

            listLocal.Focus()

            Dim index As Integer = listLocal.SelectedIndices(0)
            Dim item As ListViewItem = listLocal.SelectedItems(0)

            If index >= _localDirs Then
                ' upload file
                UploadFiles(item.Text, _localPath)
            Else
                ' local change directory
                _localPath = Path.Combine(_localPath, item.Text)
                MakeLocalList()
            End If
        Catch ex As Exception
            WriteToLog(ex)
        End Try
    End Sub

    Private Overloads Sub listLocal_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles listLocal.KeyPress ', listLocal.DoubleClick
        If e.KeyChar = ControlChars.Cr Then
            ListLocalClick()
            If listLocal.Items.Count > 0 Then
                listLocal.Focus()
            End If
        End If
    End Sub

    Private Overloads Sub listLocal_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles listLocal.DoubleClick 'listLocal.KeyPress, 
        ListLocalClick()
        listLocal.Focus()
    End Sub

    '' <summary>
    '' make dir
    '' </summary>
    Private Sub btnMakeDir_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMakeDir.Click
        Try
            Dim dir As New MakeDir

            ' FTP: MKDIR
            If listActive Is listServer Then
                If _isWorking Then Return

                dir.ShowDialog()
                If Not (dir.Directory Is Nothing) Then
                    _ftp.CreateDirectory(dir.Directory)
                    MakeFtpList()
                End If
            Else
                ' LOCAL: MKDIR
                dir.ShowDialog()

                If Not (dir.Directory Is Nothing) Then
                    Directory.CreateDirectory(Path.Combine(_localPath, dir.Directory))
                    MakeLocalList()
                End If
            End If
        Catch ex As Exception
            WriteToLog(ex)
        End Try
    End Sub

    '' <summary>
    '' refresh listing
    '' </summary>
    Private Sub btnRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        Try
            If listActive Is listServer Then
                If _isWorking Then Return

                MakeFtpList()
            Else
                MakeLocalList()
            End If
        Catch ex As Exception
            WriteToLog(ex)
        End Try
    End Sub

    '' <summary>
    '' download file or directory tree
    '' </summary>
    '' <param name="fileName">remote file name</param>
    '' <param name="localPath">local path</param>
    Private Sub DownloadFiles(ByVal fileName As String, ByVal localPath As String)
        ' ftp state test
        If _isWorking Then Return

        ' secure transfers
        If cbST.Enabled AndAlso Not _cccMode Then _ftp.SecureTransfers = cbST.Checked

        ' set asc/bin transfers
        If rbBin.Checked Then
            _ftp.TransferType = FtpTransferType.Binary
        Else
            _ftp.TransferType = FtpTransferType.Ascii
        End If

        ' transfer
        Try
            _isWorking = True
            btnStop.Visible = True
            _problemForm.Initialize()
            _totalFilesTransferred = 0
            _totalBytesTransferred = 0
            _transferTime = DateTime.Now
            _lastTransferProgressTime = _transferTime

            _ftp.BeginGetFiles(fileName, localPath, _
                FtpBatchTransferOptions.Recursive, FtpActionOnExistingFiles.ThrowException, _
                New AsyncCallback(AddressOf DownloadCallback), Nothing)
        Catch ex As Exception
            btnStop.Visible = False
            _isWorking = False
            WriteToLog(ex)
        End Try
    End Sub

    '' <summary>
    '' upload file or directory tree
    '' </summary>
    '' <param name="fileName">remote file name</param>
    '' <param name="localPath">local path</param>
    Private Sub UploadFiles(ByVal fileName As String, ByVal localPath As String)
        localPath = Path.Combine(localPath, fileName)

        ' ftp state test
        If _isWorking Then Return

        ' secure transfers
        If cbST.Enabled AndAlso Not _cccMode Then _ftp.SecureTransfers = cbST.Checked

        ' set asc/bin transfers
        If rbBin.Checked Then
            _ftp.TransferType = FtpTransferType.Binary
        Else
            _ftp.TransferType = FtpTransferType.Ascii
        End If

        ' transfer
        Try
            _isWorking = True
            btnStop.Visible = True
            _problemForm.Initialize()
            _totalFilesTransferred = 0
            _totalBytesTransferred = 0
            _transferTime = DateTime.Now
            _lastTransferProgressTime = _transferTime

            _ftp.BeginPutFiles(localPath, ".", _
                 FtpBatchTransferOptions.Recursive, FtpActionOnExistingFiles.ThrowException, _
                New AsyncCallback(AddressOf UploadCallback), Nothing)
        Catch ex As Exception
            btnStop.Visible = False
            _isWorking = False
            WriteToLog(ex)
        End Try
    End Sub

    '' <summary>
    '' callback for download finished
    '' </summary>
    Public Sub DownloadCallback(ByVal asyncResult As IAsyncResult)
        Try
            _ftp.EndGetFiles(asyncResult)
            SafeInvoke(New TransferFinishedDelegate(AddressOf TransferFinished), New Object() {Nothing, False})
        Catch ex As Exception
            SafeInvoke(New TransferFinishedDelegate(AddressOf TransferFinished), New Object() {ex, False})
        End Try
    End Sub

    '' <summary>
    '' callback for upload finished
    '' </summary>
    Public Sub UploadCallback(ByVal asyncResult As IAsyncResult)
        Try
            _ftp.EndPutFiles(asyncResult)
            SafeInvoke(New TransferFinishedDelegate(AddressOf TransferFinished), New Object() {Nothing, True})
        Catch ex As Exception
            SafeInvoke(New TransferFinishedDelegate(AddressOf TransferFinished), New Object() {ex, True})
        End Try
    End Sub

    Public Delegate Sub TransferFinishedDelegate(ByVal err As Exception, ByVal refreshFtp As Boolean)

    Private Sub TransferFinished(ByVal err As Exception, ByVal refreshFtp As Boolean)
        If Not err Is Nothing Then
            WriteToLog(err)

            Dim ex As FtpException = CType(err, FtpException)
            If (Not ex Is Nothing) AndAlso ex.Status = FtpExceptionStatus.OperationAborted Then
                _totalBytesTransferred += ex.Transferred
            End If
        End If

        ShowTransferStatus()

        _isWorking = False
        pbTransfer.Value = 0
        btnStop.Visible = False
        _showTransferProgess = False
        lblBatchTransferProgress.Text = ""

        Try
            If refreshFtp Then
                MakeFtpList()
            Else
                MakeLocalList()
            End If
        Catch ex As Exception
            WriteToLog(ex)
        End Try
    End Sub

    '' <summary>
    '' show transfer status: files, bytes, time, speed
    '' </summary>
    Private Sub ShowTransferStatus()
        ' unknown bytes rtansferred
        If _totalBytesTransferred = 0 Then Return

        ' files and bytes transferred
        Dim outstring As String = String.Format("{0} file{1} ({2} byte{3}) transferred in", _
            _totalFilesTransferred, IIf(_totalFilesTransferred > 1, "s", ""), _
            _totalBytesTransferred, IIf(_totalBytesTransferred > 1, "s", ""))

        ' time spent
        Dim ts As TimeSpan = DateTime.Now.Subtract(_transferTime)

        ' speed
        If ts.TotalSeconds > 1 Then
            outstring += IIf(ts.Days > 0, " " + ts.Days.ToString() + " day" + IIf(ts.Days > 1, "s", Nothing), Nothing)
            outstring += IIf(ts.Hours > 0, " " + ts.Hours.ToString() + " hour" + IIf(ts.Hours > 1, "s", Nothing), Nothing)
            outstring += IIf(ts.Minutes > 0, " " + ts.Minutes.ToString() + " min" + IIf(ts.Minutes > 1, "s", Nothing), Nothing)
            outstring += IIf(ts.Seconds > 0, " " + ts.Seconds.ToString() + " sec" + IIf(ts.Seconds > 1, "s", Nothing), Nothing)
        Else
            outstring += " in " + ts.TotalSeconds.ToString() + " sec"
        End If

        Dim speed As Double = _totalBytesTransferred / ts.TotalSeconds
        If speed < 1 Then
            outstring += String.Format(" at {0:F3} B/s", speed)
        ElseIf speed < 1024 Then
            outstring += String.Format(" at {0:F0} B/s", speed)
        Else
            outstring += String.Format(" at {0:F0} KB/s", speed / 1024)
        End If

        WriteToLog("> " + outstring, RichTextBoxLogWriter.COLORCOMMAND)
    End Sub

    '' <summary>
    '' handle download button click
    '' </summary>
    Private Sub btnDownload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownload.Click
        Try
            If Not listActive Is listServer Then Return

            If _isWorking Then Return

            If listServer.SelectedItems.Count > 0 Then
                DownloadFiles(listServer.SelectedItems(0).Text, _localPath)
            End If
        Catch ex As Exception
            WriteToLog(ex)
        End Try
    End Sub

    '' <summary>
    '' handle upload button click
    '' </summary>
    Private Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        Try
            If listActive Is listServer Then Return

            If _isWorking Then Return

            If listLocal.SelectedItems.Count > 0 Then
                UploadFiles(listLocal.SelectedItems(0).Text, _localPath)
            End If
        Catch ex As Exception
            WriteToLog(ex)
        End Try
    End Sub

    '' <summary>
    '' handle delete button click
    '' </summary>
    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If listActive Is listServer Then
                If _isWorking Then Return

                If listServer.SelectedItems.Count > 0 Then
                    Dim index As Integer = listServer.SelectedIndices(0)
                    Dim item As ListViewItem = listServer.SelectedItems(0)

                    If index >= _serverDirs Then
                        Dim dialog As New DeleteDialog(DeleteDialog.DialogPrompt.DeleteFile)
                        dialog.Path = item.Text
                        dialog.ShowDialog()

                        If dialog.OK Then
                            _ftp.DeleteFile(item.Text)
                            MakeFtpList()
                        End If
                    Else
                        Dim dialog As New DeleteDialog(DeleteDialog.DialogPrompt.DeleteDirectory)
                        dialog.Path = item.Text
                        dialog.ShowDialog()

                        If dialog.OK Then
                            _ftp.RemoveDirectory(item.Text)
                            MakeFtpList()
                        End If
                    End If
                End If
            Else
                If listLocal.SelectedItems.Count > 0 Then
                    Dim index As Integer = listLocal.SelectedIndices(0)
                    Dim item As ListViewItem = listLocal.SelectedItems(0)
                    Dim localPath As String = Path.Combine(_localPath, item.Text)

                    If index >= _localDirs Then
                        Dim dialog As New DeleteDialog(DeleteDialog.DialogPrompt.DeleteFile)
                        dialog.Path = item.Text
                        dialog.ShowDialog()

                        If dialog.OK Then
                            File.Delete(localPath)
                            MakeLocalList()
                        End If
                    Else
                        Dim dialog As New DeleteDialog(DeleteDialog.DialogPrompt.DeleteDirectory)
                        dialog.path = item.Text
                        dialog.ShowDialog()

                        If dialog.OK Then
                            Directory.Delete(localPath)
                            MakeLocalList()
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            WriteToLog(ex)
        End Try
    End Sub

    '' <summary>
    '' activate panel on focus event
    '' </summary>
    Private Sub listServer_Enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles listServer.Enter, lblServerDirectory.Click
        listActive = listServer
        lblServerDirectory.BackColor = SystemColors.ActiveCaption
        lblServerDirectory.ForeColor = SystemColors.ActiveCaptionText
        lblLocalDirectory.BackColor = SystemColors.InactiveCaption
        lblLocalDirectory.ForeColor = SystemColors.InactiveCaptionText
    End Sub

    '' <summary>
    '' activate panel on focus event
    '' </summary>
    Private Sub listLocal_Enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles listLocal.Enter, lblLocalDirectory.Click
        listActive = listLocal
        lblServerDirectory.BackColor = SystemColors.InactiveCaption
        lblServerDirectory.ForeColor = SystemColors.InactiveCaptionText
        lblLocalDirectory.BackColor = SystemColors.ActiveCaption
        lblLocalDirectory.ForeColor = SystemColors.ActiveCaptionText
    End Sub

    '' <summary>
    '' "smart" resizing
    '' </summary>
    Private Sub SetRightSizeValues()
        Dim borderWidth As Integer = 10
        Dim borderControl As Integer = 10

        ' width of panels
        Dim listWidth As Integer = (Me.Size.Width - gbControl.Size.Width - 2 * borderControl - 2 * borderWidth) / 2
        listLocal.Width = listWidth
        listServer.Width = listWidth
        listLocal.Location = New Point(borderWidth + listWidth + borderControl + gbControl.Size.Width + borderControl, listServer.Location.Y)

        ' status info for panels
        lblServer.Location = New Point(listServer.Location.X, listServer.Location.Y + listServer.Size.Height + 5)
        lblLocal.Location = New Point(listLocal.Location.X, listLocal.Location.Y + listLocal.Size.Height + 5)
        lblSpeed.Location = New Point(listServer.Location.X + listServer.Width - lblSpeed.Width, listServer.Location.Y + listServer.Size.Height + 5)
        trackBar.Location = New Point(listServer.Location.X, lblSpeed.Location.Y + lblSpeed.Height + 5)
        trackBar.Size = New Size(listServer.Width, trackBar.Height)

        ' local directory
        lblLocalDirectory.Location = New Point(listLocal.Location.X, lblLocalDirectory.Location.Y)
        lblLocalDirectory.Width = listLocal.Size.Width

        ' server directory
        lblServerDirectory.Location = New Point(listServer.Location.X, lblServerDirectory.Location.Y)
        lblServerDirectory.Width = listServer.Size.Width

        ' state
        lblProgress.Location = New Point(pbTransfer.Location.X + pbTransfer.Width + 8, pbTransfer.Location.Y)
        lblBatchTransferProgress.Location = New Point(lblProgress.Location.X + lblProgress.Width + 2, lblProgress.Location.Y)

        ' header
        headerRight.Location = New Point(Me.ClientSize.Width - headerRight.Width, headerRight.Location.Y)
        headerMiddle.Width = headerRight.Location.X - headerMiddle.Location.X

        ' position of secure icon
        pbSecure.Location = New Point(headerMiddle.Width - 32, pbSecure.Top)
    End Sub

    Private Sub MainForm_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Resize
        If WindowState <> FormWindowState.Minimized Then
            SetRightSizeValues()
        End If
    End Sub

    '' <summary>
    '' stop transfer when something is being transferred
    '' </summary>
    Private Sub btnStop_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnStop.Click
        _ftp.Abort()
    End Sub

    '' <summary>
    '' go to the url
    '' </summary>
    Private Sub headerRight_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles headerRight.Click
        System.Diagnostics.Process.Start(URL)
    End Sub

    '' <summary>
    '' space bar for changing panels server <-> local
    '' </summary>
    Private Sub MainForm_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Space) Then
            If listActive Is listLocal Then
                listServer.Focus()
            Else
                listLocal.Focus()
            End If
        End If
    End Sub

    Private Sub btnErrorWarning_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnErrorWarning.Click
        MessageBox.Show(_exception, "Last occured error", MessageBoxButtons.OK, MessageBoxIcon.Error)
    End Sub

    Private Sub trackBar_Scroll(ByVal sender As Object, ByVal e As System.EventArgs) Handles trackBar.Scroll
        If trackBar.Value = 100 Then
            lblSpeed.Text = "Speed: Unlimited"
            _ftp.MaxDownloadSpeed = 0
            _ftp.MaxUploadSpeed = 0
        Else
            lblSpeed.Text = String.Format("Speed: {0} kB/s", trackBar.Value)
            _ftp.MaxDownloadSpeed = trackBar.Value
            _ftp.MaxUploadSpeed = trackBar.Value
        End If
    End Sub

#If (Not DOTNET10 And Not DOTNET11) Then
    Private Sub cbModeZ_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbModeZ.CheckedChanged
        Dim client As Ftp = _ftp
        If client Is Nothing Then Return

        If cbModeZ.Checked Then
            client.TransferMode = FtpTransferMode.Zlib
        Else
            client.TransferMode = FtpTransferMode.Stream
        End If
    End Sub
#End If
End Class