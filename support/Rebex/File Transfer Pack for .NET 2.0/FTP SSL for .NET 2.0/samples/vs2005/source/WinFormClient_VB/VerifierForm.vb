'  
'   Rebex Sample Code License
' 
'   Copyright (c) 2009, Rebex CR s.r.o. www.rebex.net, 
'   All rights reserved.
' 
'   Permission to use, copy, modify, and/or distribute this software for any
'   purpose with or without fee is hereby granted
' 
'   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
'   EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
'   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
'   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
'   HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
'   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
'   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
'   OTHER DEALINGS IN THE SOFTWARE.
' 

Imports System
Imports System.Drawing
Imports System.Collections
Imports System.ComponentModel
Imports System.Windows.Forms

' -------------------------------------------------------------------------
'' <summary>
'' Summary description for Verifier.
'' </summary>
Public Class VerifierForm
    Inherits System.Windows.Forms.Form

    Private _accepted As Boolean = False
    Private _addIssuerCertificateAuthothorityToTrustedCaStore As Boolean = False

    Public WriteOnly Property Hostname() As String
        Set(ByVal Value As String)
            lblHostname.Text = Value
        End Set
    End Property

    Public WriteOnly Property Subject() As String
        Set(ByVal Value As String)
            lblSubject.Text = Value
        End Set
    End Property

    Public WriteOnly Property Issuer() As String
        Set(ByVal Value As String)
            lblIssuer.Text = Value
        End Set
    End Property

    Public WriteOnly Property ValidFrom() As String
        Set(ByVal Value As String)
            lblValidFrom.Text = Value
        End Set
    End Property

    Public WriteOnly Property ValidTo() As String
        Set(ByVal Value As String)
            lblValidTo.Text = Value
        End Set
    End Property

    Public WriteOnly Property Problem() As String
        Set(ByVal Value As String)
            lblProblem.Text = Value
        End Set
    End Property

    Public ReadOnly Property Accepted() As Boolean
        Get
            Return _accepted
        End Get
    End Property

    Public ReadOnly Property AddIssuerCertificateAuthothorityToTrustedCaStore() As Boolean
        Get
            Return _addIssuerCertificateAuthothorityToTrustedCaStore
        End Get
    End Property

    Public Property ShowAddIssuerToTrustedCaStoreButton() As Boolean
        Get
            Return btnOkAndTrust.Visible
        End Get
        Set(ByVal Value As Boolean)
            btnOkAndTrust.Visible = Value
        End Set
    End Property

#Region "Windows Form Designer generated code"

    '' <summary>
    '' Required designer variable.
    '' </summary>
    Private components As System.ComponentModel.Container = Nothing

    '' <summary>
    '' Required method for Designer support - do not modify
    '' the contents of this method with the code editor.
    '' </summary>
    Friend WithEvents panel1 As System.Windows.Forms.Panel
    Friend WithEvents label5 As System.Windows.Forms.Label
    Friend WithEvents label4 As System.Windows.Forms.Label
    Friend WithEvents label3 As System.Windows.Forms.Label
    Friend WithEvents label2 As System.Windows.Forms.Label
    Friend WithEvents label1 As System.Windows.Forms.Label
    Friend WithEvents panel2 As System.Windows.Forms.Panel
    Friend WithEvents lblProblem As System.Windows.Forms.Label
    Friend WithEvents lblHostname As System.Windows.Forms.Label
    Friend WithEvents lblSubject As System.Windows.Forms.Label
    Friend WithEvents lblIssuer As System.Windows.Forms.Label
    Friend WithEvents lblValidFrom As System.Windows.Forms.Label
    Friend WithEvents lblValidTo As System.Windows.Forms.Label
    Friend WithEvents btnReject As System.Windows.Forms.Button
    Friend WithEvents btnAccept As System.Windows.Forms.Button
    Friend WithEvents label6 As System.Windows.Forms.Label
    Friend WithEvents label8 As System.Windows.Forms.Label
    Friend WithEvents btnOkAndTrust As System.Windows.Forms.Button
    Private Sub InitializeComponent()
        Me.btnReject = New System.Windows.Forms.Button
        Me.btnAccept = New System.Windows.Forms.Button
        Me.panel1 = New System.Windows.Forms.Panel
        Me.label6 = New System.Windows.Forms.Label
        Me.lblValidTo = New System.Windows.Forms.Label
        Me.lblValidFrom = New System.Windows.Forms.Label
        Me.lblIssuer = New System.Windows.Forms.Label
        Me.lblSubject = New System.Windows.Forms.Label
        Me.lblProblem = New System.Windows.Forms.Label
        Me.panel2 = New System.Windows.Forms.Panel
        Me.label5 = New System.Windows.Forms.Label
        Me.label4 = New System.Windows.Forms.Label
        Me.label3 = New System.Windows.Forms.Label
        Me.label2 = New System.Windows.Forms.Label
        Me.label1 = New System.Windows.Forms.Label
        Me.btnOkAndTrust = New System.Windows.Forms.Button
        Me.lblHostname = New System.Windows.Forms.Label
        Me.label8 = New System.Windows.Forms.Label
        Me.panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnReject
        '
        Me.btnReject.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnReject.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnReject.Location = New System.Drawing.Point(256, 312)
        Me.btnReject.Name = "btnReject"
        Me.btnReject.Size = New System.Drawing.Size(72, 23)
        Me.btnReject.TabIndex = 2
        Me.btnReject.Text = "Reject"
        '
        'btnAccept
        '
        Me.btnAccept.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAccept.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnAccept.Location = New System.Drawing.Point(336, 312)
        Me.btnAccept.Name = "btnAccept"
        Me.btnAccept.Size = New System.Drawing.Size(72, 23)
        Me.btnAccept.TabIndex = 1
        Me.btnAccept.Text = "Accept"
        '
        'panel1
        '
        Me.panel1.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.panel1.Controls.Add(Me.lblHostname)
        Me.panel1.Controls.Add(Me.label8)
        Me.panel1.Controls.Add(Me.label6)
        Me.panel1.Controls.Add(Me.lblValidTo)
        Me.panel1.Controls.Add(Me.lblValidFrom)
        Me.panel1.Controls.Add(Me.lblIssuer)
        Me.panel1.Controls.Add(Me.lblSubject)
        Me.panel1.Controls.Add(Me.lblProblem)
        Me.panel1.Controls.Add(Me.panel2)
        Me.panel1.Controls.Add(Me.label5)
        Me.panel1.Controls.Add(Me.label4)
        Me.panel1.Controls.Add(Me.label3)
        Me.panel1.Controls.Add(Me.label2)
        Me.panel1.Location = New System.Drawing.Point(8, 8)
        Me.panel1.Name = "panel1"
        Me.panel1.Size = New System.Drawing.Size(400, 296)
        Me.panel1.TabIndex = 3
        '
        'label6
        '
        Me.label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.label6.Location = New System.Drawing.Point(8, 8)
        Me.label6.Name = "label6"
        Me.label6.Size = New System.Drawing.Size(216, 23)
        Me.label6.TabIndex = 11
        Me.label6.Text = "Certificate details:"
        '
        'lblValidTo
        '
        Me.lblValidTo.Location = New System.Drawing.Point(80, 176)
        Me.lblValidTo.Name = "lblValidTo"
        Me.lblValidTo.Size = New System.Drawing.Size(224, 23)
        Me.lblValidTo.TabIndex = 10
        '
        'lblValidFrom
        '
        Me.lblValidFrom.Location = New System.Drawing.Point(80, 152)
        Me.lblValidFrom.Name = "lblValidFrom"
        Me.lblValidFrom.Size = New System.Drawing.Size(224, 23)
        Me.lblValidFrom.TabIndex = 9
        '
        'lblIssuer
        '
        Me.lblIssuer.Location = New System.Drawing.Point(80, 104)
        Me.lblIssuer.Name = "lblIssuer"
        Me.lblIssuer.Size = New System.Drawing.Size(312, 40)
        Me.lblIssuer.TabIndex = 8
        '
        'lblSubject
        '
        Me.lblSubject.Location = New System.Drawing.Point(80, 56)
        Me.lblSubject.Name = "lblSubject"
        Me.lblSubject.Size = New System.Drawing.Size(312, 40)
        Me.lblSubject.TabIndex = 7
        '
        'lblProblem
        '
        Me.lblProblem.ForeColor = System.Drawing.Color.Red
        Me.lblProblem.Location = New System.Drawing.Point(8, 216)
        Me.lblProblem.Name = "lblProblem"
        Me.lblProblem.Size = New System.Drawing.Size(384, 72)
        Me.lblProblem.TabIndex = 6
        '
        'panel2
        '
        Me.panel2.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.panel2.Location = New System.Drawing.Point(8, 200)
        Me.panel2.Name = "panel2"
        Me.panel2.Size = New System.Drawing.Size(384, 3)
        Me.panel2.TabIndex = 5
        '
        'label5
        '
        Me.label5.Location = New System.Drawing.Point(8, 176)
        Me.label5.Name = "label5"
        Me.label5.Size = New System.Drawing.Size(72, 23)
        Me.label5.TabIndex = 4
        Me.label5.Text = "Valid to:"
        '
        'label4
        '
        Me.label4.Location = New System.Drawing.Point(8, 152)
        Me.label4.Name = "label4"
        Me.label4.Size = New System.Drawing.Size(72, 23)
        Me.label4.TabIndex = 3
        Me.label4.Text = "Valid from:"
        '
        'label3
        '
        Me.label3.Location = New System.Drawing.Point(8, 104)
        Me.label3.Name = "label3"
        Me.label3.Size = New System.Drawing.Size(72, 23)
        Me.label3.TabIndex = 2
        Me.label3.Text = "Issuer:"
        '
        'label2
        '
        Me.label2.Location = New System.Drawing.Point(8, 56)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(72, 23)
        Me.label2.TabIndex = 1
        Me.label2.Text = "Subject:"
        '
        'label1
        '
        Me.label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.label1.Location = New System.Drawing.Point(8, 8)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(256, 23)
        Me.label1.TabIndex = 0
        Me.label1.Text = "CERTIFICATE INFORMATION:"
        '
        'btnOkAndTrust
        '
        Me.btnOkAndTrust.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOkAndTrust.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnOkAndTrust.Location = New System.Drawing.Point(16, 312)
        Me.btnOkAndTrust.Name = "btnOkAndTrust"
        Me.btnOkAndTrust.Size = New System.Drawing.Size(232, 23)
        Me.btnOkAndTrust.TabIndex = 6
		Me.btnOkAndTrust.Text = "OK && Always &Trust This Authority"
        Me.btnOkAndTrust.Visible = False
        '
        'lblHostname
        '
        Me.lblHostname.Location = New System.Drawing.Point(80, 32)
        Me.lblHostname.Name = "lblHostname"
        Me.lblHostname.Size = New System.Drawing.Size(312, 24)
        Me.lblHostname.TabIndex = 15
        '
        'label8
        '
        Me.label8.Location = New System.Drawing.Point(8, 32)
        Me.label8.Name = "label8"
        Me.label8.Size = New System.Drawing.Size(72, 23)
        Me.label8.TabIndex = 14
        Me.label8.Text = "Hostname:"
        '
        'VerifierForm
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(418, 344)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnOkAndTrust)
        Me.Controls.Add(Me.panel1)
        Me.Controls.Add(Me.btnAccept)
        Me.Controls.Add(Me.btnReject)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.MaximizeBox = False
        Me.Name = "VerifierForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Certificate"
        Me.panel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    ' -------------------------------------------------------------------------
    '' <summary>
    '' constructor
    '' </summary>
    Public Sub New()
        InitializeComponent()
    End Sub

    ' -------------------------------------------------------------------------
    '' <summary>
    '' Clean up any resources being used.
    '' </summary>
    Protected Overloads Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

#End Region


    Private Sub btnAccept_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAccept.Click
        _accepted = True
        Me.Close()
    End Sub


    Private Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click
        Me.Close()
    End Sub

    Private Sub btnOkAndTrust_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOkAndTrust.Click
        _accepted = True
        _addIssuerCertificateAuthothorityToTrustedCaStore = True

        Me.Close()
    End Sub
End Class
