//  
//   Rebex Sample Code License
// 
//   Copyright (c) 2009, Rebex CR s.r.o. www.rebex.net, 
//   All rights reserved.
// 
//   Permission to use, copy, modify, and/or distribute this software for any
//   purpose with or without fee is hereby granted
// 
//   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//   EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//   HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
//   OTHER DEALINGS IN THE SOFTWARE.
// 

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.IO;
using System.Xml;

using Rebex.Net;

namespace Rebex.Samples.WinFormClient
{
	/// <summary>
	/// connection dialog
	/// </summary>
	public class Connection : System.Windows.Forms.Form
	{
		private System.ComponentModel.Container components = null;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.CheckBox cbPassive;
		private System.Windows.Forms.TextBox tbPassword;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox tbLogin;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox tbPort;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox tbHost;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.ComboBox cbProxyType;
		private System.Windows.Forms.TextBox tbProxyPort;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.CheckBox cbProxy;
		private System.Windows.Forms.TextBox tbProxy;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Button btnConnect;
		private System.Windows.Forms.TextBox tbProxyLogin;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.TextBox tbProxyPassword;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.StatusBar sbMessage;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.ComboBox cbLogLevel;
		private CheckBox cbUseLargeBuffers;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.ComboBox cbSecurity;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.CheckBox cbTLS;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.ComboBox cbSuite;
		private System.Windows.Forms.CheckBox cbSSL;
		private System.Windows.Forms.CheckBox cbCCC;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.TextBox tbCertificatePath;
		private System.Windows.Forms.Button btnCertificatePath;
		private System.Windows.Forms.OpenFileDialog openFileDialog;
		private System.Windows.Forms.Label label17;

		// CONSTANTS

		public string ANONYMOUSLOGIN = "anonymous";
		public string ANONYMOUSPASSWORD = "guest@127.0.0.1";
		public FtpProxyType[] PROXYWHENSECURE = new FtpProxyType[] { FtpProxyType.Socks4, FtpProxyType.Socks4a, FtpProxyType.Socks5, FtpProxyType.HttpConnect };

		public static readonly string ConfigFile = Path.Combine(Environment.GetFolderPath(System.Environment.SpecialFolder.LocalApplicationData), @"Rebex\FTP SSL\WinFormClient.xml");

		public bool _ok = false;    // form was confirmed

		public bool OK
		{
			get { return _ok; }
		}

		// host name

		public string Host
		{
			get { return tbHost.Text; }
			set { tbHost.Text = value; }
		}

		// port of the host

		public int Port
		{
			get
			{
				int temp;

				try
				{
					temp = Int32.Parse(tbPort.Text);
				}
				catch
				{
					return 0;    // def. value if error occures
				}

				return temp;
			}
			set { tbPort.Text = value.ToString(); }
		}

		// proxy server

		public string Proxy
		{
			set { tbProxy.Text = value; }
			get { return tbProxy.Text; }
		}

		// proxy login name

		public string ProxyLogin
		{
			set { tbProxyLogin.Text = value; }
			get { return tbProxyLogin.Text; }
		}

		// proxy password

		public string ProxyPassword
		{
			set { tbProxyPassword.Text = value; }
			get { return tbProxyPassword.Text; }
		}

		// is proxy enabled?

		public bool ProxyEnabled
		{
			set
			{
				if (value)
				{
					tbProxy.Enabled = true;
					tbProxyPort.Enabled = true;
					cbProxy.Checked = true;
					cbProxyType.Enabled = true;
					tbProxyLogin.Enabled = true;
					tbProxyPassword.Enabled = true;
				}
				else
				{
					tbProxy.Enabled = false;
					tbProxyPort.Enabled = false;
					cbProxy.Checked = false;
					cbProxyType.Enabled = false;
					tbProxyLogin.Enabled = false;
					tbProxyPassword.Enabled = false;
				}
			}

			get { return cbProxy.Checked; }
		}

		// proxy port

		public int ProxyPort
		{
			set { tbProxyPort.Text = value.ToString(); }
			get
			{
				int temp;

				try
				{
					temp = Int32.Parse(tbProxyPort.Text);
				}
				catch
				{
					return 0;    // def. value if error occures
				}

				return temp;
			}
		}

		// login name

		public string Login
		{
			get
			{
				if (!(tbLogin.Text != null && tbLogin.Text.Length > 0))
				{
					return ANONYMOUSLOGIN;
				}
				else
				{
					return tbLogin.Text;
				}
			}

			set { tbLogin.Text = value; }
		}

		// password

		public string Password
		{
			get
			{
				if (!(tbLogin.Text != null && tbLogin.Text.Length > 0))
				{
					return ANONYMOUSPASSWORD;
				}
				else
				{
					return tbPassword.Text;
				}
			}

			set { tbPassword.Text = value; }
		}

		// passive mode

		public bool Passive
		{
			get
			{
				return cbPassive.Checked;
			}
			set
			{
				cbPassive.Checked = value;
			}
		}

		// use large buffers

		public bool UseLargeBuffers
		{
			get
			{
				return cbUseLargeBuffers.Checked;
			}
			set
			{
				cbUseLargeBuffers.Checked = value;
			}
		}

		// proxy type

		public FtpProxyType ProxyType
		{
			get
			{
				switch (cbProxyType.SelectedIndex)
				{
					default:
						return FtpProxyType.Socks4;
					case 1:
						return FtpProxyType.Socks4a;
					case 2:
						return FtpProxyType.Socks5;
					case 3:
						return FtpProxyType.HttpConnect;
					case 4:
						return FtpProxyType.FtpSite;
					case 5:
						return FtpProxyType.FtpUser;
					case 6:
						return FtpProxyType.FtpOpen;
				}
			}
			set
			{
				switch (value)
				{
					default:
						cbProxyType.SelectedIndex = 0; break;
					case FtpProxyType.Socks4a:
						cbProxyType.SelectedIndex = 1; break;
					case FtpProxyType.Socks5:
						cbProxyType.SelectedIndex = 2; break;
					case FtpProxyType.HttpConnect:
						cbProxyType.SelectedIndex = 3; break;
					case FtpProxyType.FtpSite:
						cbProxyType.SelectedIndex = 4; break;
					case FtpProxyType.FtpUser:
						cbProxyType.SelectedIndex = 5; break;
					case FtpProxyType.FtpOpen:
						cbProxyType.SelectedIndex = 6; break;
				}
			}
		}
		// security type

		public FtpSecurity SecurityType
		{
			get
			{
				switch (cbSecurity.SelectedIndex)
				{
					default:
						return FtpSecurity.Unsecure;
					case 1:
						return FtpSecurity.Explicit;
					case 2:
						return FtpSecurity.Implicit;
				}
			}
			set
			{
				switch (value)
				{
					default:
						cbSecurity.SelectedIndex = 0;
						break;
					case FtpSecurity.Explicit:
						cbSecurity.SelectedIndex = 1;
						break;
					case FtpSecurity.Implicit:
						cbSecurity.SelectedIndex = 2;
						break;
				}
			}
		}

		// certificate path

		public string CertificatePath
		{
			get
			{
				return tbCertificatePath.Text;
			}
			set
			{
				tbCertificatePath.Text = value;
			}
		}

		// allowed suite		

		public TlsCipherSuite AllowedSuite
		{
			get
			{
				if (cbSuite.SelectedIndex == 0)
					return TlsCipherSuite.All;
				else
					return TlsCipherSuite.Secure;
			}
			set
			{
				if (value == TlsCipherSuite.All)
					cbSuite.SelectedIndex = 0;
				else
					cbSuite.SelectedIndex = 1;
			}
		}

		// protocol

		public TlsVersion Protocol
		{
			get
			{
				TlsVersion value = 0;
				if (cbTLS.Checked)
					value |= TlsVersion.TLS10;
				if (cbSSL.Checked)
					value |= TlsVersion.SSL30;

				return value;
			}
			set
			{
				cbTLS.Checked = ((value & TlsVersion.TLS10) != 0);
				cbSSL.Checked = ((value & TlsVersion.SSL30) != 0);
			}
		}

		// clear command channel

		public bool ClearCommandChannel
		{
			set { cbCCC.Checked = value; }
			get { return cbCCC.Checked; }
		}


		// log level

		public Rebex.LogLevel LogLevel
		{
			get
			{
				switch (cbLogLevel.SelectedIndex)
				{
					default:
						return Rebex.LogLevel.Info;
					case 1:
						return Rebex.LogLevel.Debug;
					case 2:
						return Rebex.LogLevel.Verbose;
					case 3:
						return Rebex.LogLevel.Error;
				}
			}
			set
			{
				switch (value)
				{
					default:
						cbLogLevel.SelectedIndex = 0; break;
					case Rebex.LogLevel.Debug:
						cbLogLevel.SelectedIndex = 1; break;
					case Rebex.LogLevel.Verbose:
						cbLogLevel.SelectedIndex = 2; break;
					case Rebex.LogLevel.Error:
						cbLogLevel.SelectedIndex = 3; break;
				}
			}
		}

		// -------------------------------------------------------------------------
		/// <summary>
		/// validity check
		/// </summary>
		/// <returns>true (everything is ok)</returns>
		public bool ValidateForm()
		{
			// ftp host

			if (tbHost.Text == null || tbHost.Text.Length <= 0)
			{
				sbMessage.Text = "ftp host is a required field";
				return false;
			}

			// ftp port

			if (tbPort.Text == null || tbPort.Text.Length <= 0)
			{
				sbMessage.Text = "ftp port is a required field";
				return false;
			}

			// ftp port [decimal]

			bool badFtpPort = false;

			try
			{
				uint n = UInt32.Parse(tbPort.Text);
				if (n == 0) badFtpPort = true;
			}
			catch
			{
				badFtpPort = true;
			}

			if (badFtpPort)
			{
				sbMessage.Text = "ftp port must be > 0";
				return false;
			}

			// ----------------
			// proxy check-outs
			// ----------------

			if (cbProxy.Checked)
			{
				// proxy host

				if (tbProxy.Text == null || tbProxy.Text.Length <= 0)
				{
					sbMessage.Text = "proxy host is a required field when proxy is enabled";
					return false;
				}

				// proxy port

				if (tbProxyPort.Text == null || tbProxyPort.Text.Length <= 0)
				{
					sbMessage.Text = "proxy port is a required field when proxy is enabled";
					return false;
				}

				// proxy port [decimal]

				bool badProxyPort = false;

				try
				{
					uint n = UInt32.Parse(tbProxyPort.Text);
					if (n == 0) badProxyPort = true;
				}
				catch
				{
					badProxyPort = true;
				}

				if (badProxyPort)
				{
					sbMessage.Text = "proxy port must be > 0";
					return false;
				}
				// when secure - check proxy type

				if (SecurityType != 0)
				{
					bool ok = false;

					for (int i = 0; i < PROXYWHENSECURE.Length; i++)
					{
						if (PROXYWHENSECURE[i] == ProxyType) ok = true;
					}

					if (!ok)
					{
						MessageBox.Show("choose SOCKS4 or HTTP Connect when security is on", "proxy error", MessageBoxButtons.OK, MessageBoxIcon.Error);
						return false;
					}
				}
			}

			return true;
		}

		// -------------------------------------------------------------------------
		public Connection()
		{
			InitializeComponent();

			LoadConfig();
		}

		/// <summary>
		/// load a config file and set appropritate values for site
		/// </summary>
		private void LoadConfig()
		{
			// set default values for site

			this.Host = null;
			this.Port = Ftp.DefaultPort;
			this.Login = "";
			this.Password = "";
			this.Passive = true;
			this.Proxy = null;
			this.ProxyEnabled = false;
			this.ProxyPort = 3128;
			this.ProxyLogin = null;
			this.ProxyPassword = null;
			this.ProxyType = FtpProxyType.Socks4;
			this.LogLevel = Rebex.LogLevel.Info;
			this.ClearCommandChannel = false;
			this.SecurityType = FtpSecurity.Unsecure;
			this.AllowedSuite = TlsCipherSuite.All;

			// read config file

			XmlDocument xml = new XmlDocument();
			if (!File.Exists(ConfigFile))
				return;

			xml.Load(ConfigFile);

			this.Proxy = null;

			XmlElement config = xml["configuration"];
			foreach (XmlNode key in config.ChildNodes)
			{
				string item = null;
				string name = null;
				if (key.Attributes["value"] != null)
					item = key.Attributes["value"].Value;
				if (key.Attributes["name"] != null)
					name = key.Attributes["name"].Value;
				if (key["value"] != null)
					item = key["value"].InnerText;
				if (key["name"] != null)
					name = key["name"].InnerText;

				if (name == null)
					continue;
				if (item == null)
					item = "";

				switch (name)
				{
					case "host": this.Host = item; break;
					case "login": this.Login = item; break;
					case "port":
					{
						try
						{
							this.Port = Int32.Parse(item);
						}
						catch
						{
							this.Port = Ftp.DefaultPort; // default
						}

						break;
					}
					case "password": this.Password = item; break;
					case "passive": this.Passive = (item.ToLower() == "false" ? false : true); break;
					case "largebuffers": this.UseLargeBuffers = (item.ToLower() == "false" ? false : true); break;
					case "proxy": this.Proxy = item; break;
					case "proxylogin": this.ProxyLogin = item; break;
					case "proxypassword": this.ProxyPassword = item; break;
					case "proxyenabled": this.ProxyEnabled = (item.ToLower() == "false" ? false : true); break;
					case "clearcommandchannel": this.ClearCommandChannel = (item.ToLower() == "false" ? false : true); break;
					case "certificatepath": this.CertificatePath = item; break;
					case "proxyport":
					{
						try
						{
							this.ProxyPort = Int32.Parse(item);
						}
						catch
						{
							this.ProxyPort = 3128; // default
						}

						break;
					}
					case "proxytype":
					{
						try
						{
							this.ProxyType = (FtpProxyType)Enum.Parse(typeof(FtpProxyType), item);
						}
						catch
						{
							this.ProxyType = FtpProxyType.Socks4; // default
						}

						break;
					}
					case "loglevel":
						try
						{
							this.LogLevel = (Rebex.LogLevel)Enum.Parse(typeof(Rebex.LogLevel), item);
						}
						catch
						{
							this.LogLevel = Rebex.LogLevel.Info; // default
						}

						break;
					case "securitytype":
					{
						try
						{
							this.SecurityType = (FtpSecurity)Enum.Parse(typeof(FtpSecurity), item);
						}
						catch
						{
							this.SecurityType = FtpSecurity.Unsecure;
						}

						break;
					}
					case "allowedsuite":
					{
						try
						{
							this.AllowedSuite = (TlsCipherSuite)Enum.Parse(typeof(TlsCipherSuite), item);
						}
						catch
						{
							this.AllowedSuite = TlsCipherSuite.All;
						}

						break;
					}
					case "protocol":
					{
						try
						{
							this.Protocol = (TlsVersion)Enum.Parse(typeof(TlsVersion), item, true);
						}
						catch
						{
							this.Protocol = TlsVersion.SSL30 | TlsVersion.TLS10; // default
						}

						break;
					}
				}
			}
		}

		private static void AddKey(XmlDocument xml, XmlElement config, string name, string val)
		{
			XmlElement key = xml.CreateElement("key");
			config.AppendChild(key);
			XmlAttribute atrName = xml.CreateAttribute("name");
			atrName.Value = name;
			XmlAttribute atrVal = xml.CreateAttribute("value");
			atrVal.Value = val;
			key.Attributes.Append(atrName);
			key.Attributes.Append(atrVal);
		}

		/// <summary>
		/// save site values into config file
		/// </summary>
		private void SaveConfig()
		{
			XmlDocument xml = new XmlDocument();
			XmlElement config = xml.CreateElement("configuration");
			xml.AppendChild(config);

			AddKey(xml, config, "host", this.Host);
			AddKey(xml, config, "login", this.Login);
			AddKey(xml, config, "port", this.Port.ToString());
			AddKey(xml, config, "password", this.Password);
			AddKey(xml, config, "passive", this.Passive.ToString());
			AddKey(xml, config, "largebuffers", this.UseLargeBuffers.ToString());
			AddKey(xml, config, "proxyenabled", this.ProxyEnabled.ToString());
			AddKey(xml, config, "proxy", this.Proxy);
			AddKey(xml, config, "proxyport", this.ProxyPort.ToString());
			AddKey(xml, config, "proxytype", this.ProxyType.ToString());
			AddKey(xml, config, "proxylogin", this.ProxyLogin);
			AddKey(xml, config, "proxypassword", this.ProxyPassword);
			AddKey(xml, config, "loglevel", this.LogLevel.ToString());
			AddKey(xml, config, "securitytype", this.SecurityType.ToString());
			AddKey(xml, config, "allowedsuite", this.AllowedSuite.ToString());
			AddKey(xml, config, "protocol", this.Protocol.ToString());
			AddKey(xml, config, "clearcommandchannel", this.ClearCommandChannel.ToString());
			AddKey(xml, config, "certificatepath", this.CertificatePath);

			string configPath = Path.GetDirectoryName(ConfigFile);
			if (!Directory.Exists(configPath))
				Directory.CreateDirectory(configPath);
			xml.Save(ConfigFile);
		}

		// -------------------------------------------------------------------------
		/// <summary>
		/// clean up any resources being used
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}

			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.cbUseLargeBuffers = new System.Windows.Forms.CheckBox();
			this.cbPassive = new System.Windows.Forms.CheckBox();
			this.tbPassword = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.tbLogin = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.tbPort = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.tbHost = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.label10 = new System.Windows.Forms.Label();
			this.tbProxyPassword = new System.Windows.Forms.TextBox();
			this.label9 = new System.Windows.Forms.Label();
			this.tbProxyLogin = new System.Windows.Forms.TextBox();
			this.label8 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.cbProxyType = new System.Windows.Forms.ComboBox();
			this.tbProxyPort = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.cbProxy = new System.Windows.Forms.CheckBox();
			this.tbProxy = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.btnConnect = new System.Windows.Forms.Button();
			this.sbMessage = new System.Windows.Forms.StatusBar();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.btnCertificatePath = new System.Windows.Forms.Button();
			this.tbCertificatePath = new System.Windows.Forms.TextBox();
			this.label16 = new System.Windows.Forms.Label();
			this.cbCCC = new System.Windows.Forms.CheckBox();
			this.cbSuite = new System.Windows.Forms.ComboBox();
			this.label14 = new System.Windows.Forms.Label();
			this.cbSSL = new System.Windows.Forms.CheckBox();
			this.cbTLS = new System.Windows.Forms.CheckBox();
			this.label13 = new System.Windows.Forms.Label();
			this.cbSecurity = new System.Windows.Forms.ComboBox();
			this.label12 = new System.Windows.Forms.Label();
			this.label17 = new System.Windows.Forms.Label();
			this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
			this.label18 = new System.Windows.Forms.Label();
			this.cbLogLevel = new System.Windows.Forms.ComboBox();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.groupBox3.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.cbUseLargeBuffers);
			this.groupBox1.Controls.Add(this.cbPassive);
			this.groupBox1.Controls.Add(this.tbPassword);
			this.groupBox1.Controls.Add(this.label4);
			this.groupBox1.Controls.Add(this.tbLogin);
			this.groupBox1.Controls.Add(this.label3);
			this.groupBox1.Controls.Add(this.tbPort);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.tbHost);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Controls.Add(this.label11);
			this.groupBox1.Location = new System.Drawing.Point(8, 8);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(400, 132);
			this.groupBox1.TabIndex = 18;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "FTP settings";
			// 
			// cbUseLargeBuffers
			// 
			this.cbUseLargeBuffers.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.cbUseLargeBuffers.Location = new System.Drawing.Point(235, 72);
			this.cbUseLargeBuffers.Name = "cbUseLargeBuffers";
			this.cbUseLargeBuffers.Size = new System.Drawing.Size(149, 24);
			this.cbUseLargeBuffers.TabIndex = 32;
			this.cbUseLargeBuffers.Text = "Use large buffers (faster)";
			// 
			// cbPassive
			// 
			this.cbPassive.Checked = true;
			this.cbPassive.CheckState = System.Windows.Forms.CheckState.Checked;
			this.cbPassive.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.cbPassive.Location = new System.Drawing.Point(235, 48);
			this.cbPassive.Name = "cbPassive";
			this.cbPassive.Size = new System.Drawing.Size(149, 24);
			this.cbPassive.TabIndex = 18;
			this.cbPassive.Text = "Passive mode";
			// 
			// tbPassword
			// 
			this.tbPassword.Location = new System.Drawing.Point(112, 72);
			this.tbPassword.Name = "tbPassword";
			this.tbPassword.PasswordChar = '*';
			this.tbPassword.Size = new System.Drawing.Size(100, 20);
			this.tbPassword.TabIndex = 17;
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(8, 75);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(100, 23);
			this.label4.TabIndex = 16;
			this.label4.Text = "Password:";
			// 
			// tbLogin
			// 
			this.tbLogin.Location = new System.Drawing.Point(112, 48);
			this.tbLogin.Name = "tbLogin";
			this.tbLogin.Size = new System.Drawing.Size(100, 20);
			this.tbLogin.TabIndex = 15;
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(8, 51);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(100, 23);
			this.label3.TabIndex = 14;
			this.label3.Text = "Login name:";
			// 
			// tbPort
			// 
			this.tbPort.Location = new System.Drawing.Point(296, 24);
			this.tbPort.Name = "tbPort";
			this.tbPort.Size = new System.Drawing.Size(64, 20);
			this.tbPort.TabIndex = 13;
			this.tbPort.Text = "21";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(232, 27);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(56, 23);
			this.label2.TabIndex = 12;
			this.label2.Text = "Port:";
			// 
			// tbHost
			// 
			this.tbHost.Location = new System.Drawing.Point(112, 24);
			this.tbHost.Name = "tbHost";
			this.tbHost.Size = new System.Drawing.Size(100, 20);
			this.tbHost.TabIndex = 11;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 27);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(100, 23);
			this.label1.TabIndex = 10;
			this.label1.Text = "Host name:";
			// 
			// label11
			// 
			this.label11.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.label11.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
			this.label11.Location = new System.Drawing.Point(114, 99);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(270, 30);
			this.label11.TabIndex = 31;
			this.label11.Text = "Leave login/password empty for anonymous login.";
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.label10);
			this.groupBox2.Controls.Add(this.tbProxyPassword);
			this.groupBox2.Controls.Add(this.label9);
			this.groupBox2.Controls.Add(this.tbProxyLogin);
			this.groupBox2.Controls.Add(this.label8);
			this.groupBox2.Controls.Add(this.label7);
			this.groupBox2.Controls.Add(this.cbProxyType);
			this.groupBox2.Controls.Add(this.tbProxyPort);
			this.groupBox2.Controls.Add(this.label6);
			this.groupBox2.Controls.Add(this.cbProxy);
			this.groupBox2.Controls.Add(this.tbProxy);
			this.groupBox2.Controls.Add(this.label5);
			this.groupBox2.Location = new System.Drawing.Point(8, 347);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(400, 128);
			this.groupBox2.TabIndex = 19;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Proxy settings";
			// 
			// label10
			// 
			this.label10.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.label10.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
			this.label10.Location = new System.Drawing.Point(232, 72);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(160, 48);
			this.label10.TabIndex = 30;
			this.label10.Text = "Leave login/password fields empty when not needed.";
			// 
			// tbProxyPassword
			// 
			this.tbProxyPassword.Location = new System.Drawing.Point(112, 96);
			this.tbProxyPassword.Name = "tbProxyPassword";
			this.tbProxyPassword.PasswordChar = '*';
			this.tbProxyPassword.Size = new System.Drawing.Size(104, 20);
			this.tbProxyPassword.TabIndex = 29;
			// 
			// label9
			// 
			this.label9.Location = new System.Drawing.Point(8, 99);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(100, 23);
			this.label9.TabIndex = 28;
			this.label9.Text = "Password:";
			// 
			// tbProxyLogin
			// 
			this.tbProxyLogin.Location = new System.Drawing.Point(112, 72);
			this.tbProxyLogin.Name = "tbProxyLogin";
			this.tbProxyLogin.Size = new System.Drawing.Size(104, 20);
			this.tbProxyLogin.TabIndex = 27;
			// 
			// label8
			// 
			this.label8.Location = new System.Drawing.Point(8, 75);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(100, 23);
			this.label8.TabIndex = 26;
			this.label8.Text = "Login name:";
			// 
			// label7
			// 
			this.label7.Location = new System.Drawing.Point(8, 51);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(88, 23);
			this.label7.TabIndex = 25;
			this.label7.Text = "Proxy type:";
			// 
			// cbProxyType
			// 
			this.cbProxyType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbProxyType.Items.AddRange(new object[] {
            "Socks4",
            "Socks4a",
            "Socks5",
            "HttpConnect",
            "FtpSite",
            "FtpUser",
            "FtpOpen"});
			this.cbProxyType.Location = new System.Drawing.Point(112, 48);
			this.cbProxyType.Name = "cbProxyType";
			this.cbProxyType.Size = new System.Drawing.Size(104, 21);
			this.cbProxyType.TabIndex = 24;
			// 
			// tbProxyPort
			// 
			this.tbProxyPort.Location = new System.Drawing.Point(296, 24);
			this.tbProxyPort.Name = "tbProxyPort";
			this.tbProxyPort.Size = new System.Drawing.Size(64, 20);
			this.tbProxyPort.TabIndex = 23;
			this.tbProxyPort.Text = "21";
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(232, 27);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(56, 23);
			this.label6.TabIndex = 22;
			this.label6.Text = "Port:";
			// 
			// cbProxy
			// 
			this.cbProxy.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.cbProxy.Location = new System.Drawing.Point(296, 48);
			this.cbProxy.Name = "cbProxy";
			this.cbProxy.Size = new System.Drawing.Size(88, 24);
			this.cbProxy.TabIndex = 21;
			this.cbProxy.Text = "Use proxy";
			this.cbProxy.Click += new System.EventHandler(this.cbProxy_Click);
			// 
			// tbProxy
			// 
			this.tbProxy.Location = new System.Drawing.Point(112, 24);
			this.tbProxy.Name = "tbProxy";
			this.tbProxy.Size = new System.Drawing.Size(104, 20);
			this.tbProxy.TabIndex = 20;
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(8, 27);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(100, 23);
			this.label5.TabIndex = 19;
			this.label5.Text = "Proxy:";
			// 
			// btnConnect
			// 
			this.btnConnect.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnConnect.Location = new System.Drawing.Point(316, 483);
			this.btnConnect.Name = "btnConnect";
			this.btnConnect.Size = new System.Drawing.Size(88, 23);
			this.btnConnect.TabIndex = 20;
			this.btnConnect.Text = "Connect";
			this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
			// 
			// sbMessage
			// 
			this.sbMessage.Location = new System.Drawing.Point(0, 512);
			this.sbMessage.Name = "sbMessage";
			this.sbMessage.Size = new System.Drawing.Size(416, 19);
			this.sbMessage.TabIndex = 21;
			// 
			// groupBox3
			// 
			this.groupBox3.Controls.Add(this.btnCertificatePath);
			this.groupBox3.Controls.Add(this.tbCertificatePath);
			this.groupBox3.Controls.Add(this.label16);
			this.groupBox3.Controls.Add(this.cbCCC);
			this.groupBox3.Controls.Add(this.cbSuite);
			this.groupBox3.Controls.Add(this.label14);
			this.groupBox3.Controls.Add(this.cbSSL);
			this.groupBox3.Controls.Add(this.cbTLS);
			this.groupBox3.Controls.Add(this.label13);
			this.groupBox3.Controls.Add(this.cbSecurity);
			this.groupBox3.Controls.Add(this.label12);
			this.groupBox3.Controls.Add(this.label17);
			this.groupBox3.Location = new System.Drawing.Point(8, 141);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(400, 200);
			this.groupBox3.TabIndex = 22;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Security settings";
			// 
			// btnCertificatePath
			// 
			this.btnCertificatePath.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnCertificatePath.Location = new System.Drawing.Point(368, 168);
			this.btnCertificatePath.Name = "btnCertificatePath";
			this.btnCertificatePath.Size = new System.Drawing.Size(24, 23);
			this.btnCertificatePath.TabIndex = 21;
			this.btnCertificatePath.Text = "...";
			this.btnCertificatePath.Click += new System.EventHandler(this.btnCertificatePath_Click);
			// 
			// tbCertificatePath
			// 
			this.tbCertificatePath.Location = new System.Drawing.Point(112, 168);
			this.tbCertificatePath.Name = "tbCertificatePath";
			this.tbCertificatePath.Size = new System.Drawing.Size(248, 20);
			this.tbCertificatePath.TabIndex = 18;
			// 
			// label16
			// 
			this.label16.Location = new System.Drawing.Point(8, 168);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(104, 23);
			this.label16.TabIndex = 11;
			this.label16.Text = "Client certificate:";
			// 
			// cbCCC
			// 
			this.cbCCC.Checked = true;
			this.cbCCC.CheckState = System.Windows.Forms.CheckState.Checked;
			this.cbCCC.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.cbCCC.Location = new System.Drawing.Point(8, 104);
			this.cbCCC.Name = "cbCCC";
			this.cbCCC.Size = new System.Drawing.Size(256, 24);
			this.cbCCC.TabIndex = 10;
			this.cbCCC.Text = "Clear command channel";
			// 
			// cbSuite
			// 
			this.cbSuite.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbSuite.Items.AddRange(new object[] {
            "All ciphers",
            "Secure only"});
			this.cbSuite.Location = new System.Drawing.Point(112, 72);
			this.cbSuite.Name = "cbSuite";
			this.cbSuite.Size = new System.Drawing.Size(152, 21);
			this.cbSuite.TabIndex = 6;
			// 
			// label14
			// 
			this.label14.Location = new System.Drawing.Point(8, 75);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(104, 23);
			this.label14.TabIndex = 5;
			this.label14.Text = "Allowed suites:";
			// 
			// cbSSL
			// 
			this.cbSSL.Checked = true;
			this.cbSSL.CheckState = System.Windows.Forms.CheckState.Checked;
			this.cbSSL.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.cbSSL.Location = new System.Drawing.Point(192, 40);
			this.cbSSL.Name = "cbSSL";
			this.cbSSL.Size = new System.Drawing.Size(64, 24);
			this.cbSSL.TabIndex = 4;
			this.cbSSL.Text = "SSL 3.0";
			this.cbSSL.CheckedChanged += new System.EventHandler(this.cbTLSSSL_CheckedChanged);
			// 
			// cbTLS
			// 
			this.cbTLS.Checked = true;
			this.cbTLS.CheckState = System.Windows.Forms.CheckState.Checked;
			this.cbTLS.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.cbTLS.Location = new System.Drawing.Point(112, 40);
			this.cbTLS.Name = "cbTLS";
			this.cbTLS.Size = new System.Drawing.Size(72, 24);
			this.cbTLS.TabIndex = 3;
			this.cbTLS.Text = "TLS 1.0";
			this.cbTLS.CheckedChanged += new System.EventHandler(this.cbTLSSSL_CheckedChanged);
			// 
			// label13
			// 
			this.label13.Location = new System.Drawing.Point(8, 45);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(104, 23);
			this.label13.TabIndex = 2;
			this.label13.Text = "Allowed protocol:";
			// 
			// cbSecurity
			// 
			this.cbSecurity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbSecurity.Items.AddRange(new object[] {
            "No security",
            "Explicit TLS or SSL",
            "Implicit TLS or SSL"});
			this.cbSecurity.Location = new System.Drawing.Point(112, 16);
			this.cbSecurity.Name = "cbSecurity";
			this.cbSecurity.Size = new System.Drawing.Size(152, 21);
			this.cbSecurity.TabIndex = 1;
			this.cbSecurity.SelectedValueChanged += new System.EventHandler(this.cbSecurity_SelectedValueChanged);
			// 
			// label12
			// 
			this.label12.Location = new System.Drawing.Point(8, 18);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(104, 23);
			this.label12.TabIndex = 0;
			this.label12.Text = "Security:";
			// 
			// label17
			// 
			this.label17.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
			this.label17.Location = new System.Drawing.Point(10, 136);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(344, 32);
			this.label17.TabIndex = 32;
			this.label17.Text = "Fill the \"Client certificate\" field only when you want to authenticate using a cl" +
				"ient certificate.";
			// 
			// openFileDialog
			// 
			this.openFileDialog.Filter = "PFX and P12 files (*.pfx;*.p12)|*.pfx;*.p12|All files (*.*)|*.*";
			// 
			// label18
			// 
			this.label18.Location = new System.Drawing.Point(13, 487);
			this.label18.Name = "label18";
			this.label18.Size = new System.Drawing.Size(88, 23);
			this.label18.TabIndex = 26;
			this.label18.Text = "Logging level:";
			// 
			// cbLogLevel
			// 
			this.cbLogLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbLogLevel.Items.AddRange(new object[] {
            "Info",
            "Debug",
            "Verbose",
            "Errors only"});
			this.cbLogLevel.Location = new System.Drawing.Point(120, 484);
			this.cbLogLevel.Name = "cbLogLevel";
			this.cbLogLevel.Size = new System.Drawing.Size(104, 21);
			this.cbLogLevel.TabIndex = 25;
			// 
			// Connection
			// 
			this.AcceptButton = this.btnConnect;
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(416, 532);
			this.Controls.Add(this.groupBox3);
			this.Controls.Add(this.label18);
			this.Controls.Add(this.cbLogLevel);
			this.Controls.Add(this.sbMessage);
			this.Controls.Add(this.btnConnect);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.KeyPreview = true;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "Connection";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Connection";
			this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Connection_KeyPress);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.groupBox3.ResumeLayout(false);
			this.groupBox3.PerformLayout();
			this.ResumeLayout(false);

		}
		#endregion

		// -------------------------------------------------------------------------
		/// <summary>
		/// form confirmation by clicking 'ok' button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnConnect_Click(object sender, System.EventArgs e)
		{
			if (ValidateForm())
			{
				_ok = true;
				SaveConfig();
				this.Close();
			}
		}

		// -------------------------------------------------------------------------
		/// <summary>
		/// proxy enable/disable click
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void cbProxy_Click(object sender, System.EventArgs e)
		{
			System.Windows.Forms.CheckBox cb = (System.Windows.Forms.CheckBox)sender;

			tbProxy.Enabled = cb.Checked;
			tbProxyPort.Enabled = cb.Checked;
			cbProxy.Checked = cb.Checked;
			cbProxyType.Enabled = cb.Checked;
			tbProxyLogin.Enabled = cb.Checked;
			tbProxyPassword.Enabled = cb.Checked;
		}

		// -------------------------------------------------------------------------
		/// <summary>
		/// global key handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Connection_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			// when 'enter' is pressed the form is closed

			if (e.KeyChar == '\r')
			{
				if (ValidateForm())
				{
					_ok = true;
					this.Close();
				}
			}

			// when 'esc' is pressed the form is closed

			if ((int)(e.KeyChar) == Convert.ToChar(Keys.Escape))
			{
				this.Close();
			}
		}
		// -------------------------------------------------------------------------
		/// <summary>
		/// security type change 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>		
		private void cbSecurity_SelectedValueChanged(object sender, System.EventArgs e)
		{
			System.Windows.Forms.ComboBox cb = (System.Windows.Forms.ComboBox)sender;

			bool enabled = (cb.SelectedIndex != 0);
			cbSuite.Enabled = enabled;
			cbTLS.Enabled = enabled;
			cbSSL.Enabled = enabled;
			cbCCC.Enabled = enabled;
			tbCertificatePath.Enabled = enabled;
			btnCertificatePath.Enabled = enabled;
		}

		private void cbTLSSSL_CheckedChanged(object sender, System.EventArgs e)
		{
			CheckBox cb = (CheckBox)sender;
			if (!cb.Checked && (cbTLS.Checked == cbSSL.Checked))
				cb.Checked = true;
		}

		private void btnCertificatePath_Click(object sender, System.EventArgs e)
		{
			if (openFileDialog.ShowDialog() == DialogResult.OK)
			{
				tbCertificatePath.Text = openFileDialog.FileName;
			}
		}
	}
}