//  
//   Rebex Sample Code License
// 
//   Copyright (c) 2009, Rebex CR s.r.o. www.rebex.net, 
//   All rights reserved.
// 
//   Permission to use, copy, modify, and/or distribute this software for any
//   purpose with or without fee is hereby granted
// 
//   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//   EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//   HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
//   OTHER DEALINGS IN THE SOFTWARE.
// 

using System;
using System.IO;
using Rebex.Net;

namespace Rebex.Samples.GetPut
{
	/// <summary>
	/// This sample is a simple utility to upload/download a file to/from
	/// a FTP server using the Ftp class.
	/// </summary>
	class GetPut
	{

		[STAThread]
		static void Main(string[] args)
		{
			string method = null;
			if (args.Length == 5)
			{
				switch (args[0].ToLower())
				{
					case "get": method = "get"; break;
					case "put": method = "put"; break;
				}
			}

			if (method == null)
			{
				// display syntax if wrong number of parameters or unsupported operation
				Console.WriteLine ("GetPut - uploads or downloads a file from a FTP server");
				Console.WriteLine ("Syntax: GetPut get|put unsecure|secure|implicit hostname remotepath localpath ");
				Console.WriteLine ("Example: GetPut get secure ftp.rebex.net /pub/example/readme.txt readme.txt");
				return;
			}
			FtpSecurity security = (FtpSecurity) Enum.Parse (typeof(FtpSecurity), args[1], true);
			string hostname = args[2];
			string remotePath = args[3];
			string localPath = args[4];

			// set SSL parameters to accept all server certificates...
			// do not do this in production code, server certificates should
			// be verified - use CertificateVerifier.Default instead
			TlsParameters par = new TlsParameters ();
			par.CommonName = hostname;
			par.CertificateVerifier = CertificateVerifier.AcceptAll;
			// create Ftp object and connect to the server
			Ftp ftp = new Ftp();
			ftp.Connect (hostname, Ftp.DefaultPort, par, security);
			// ask for username and password and login
			Console.Write ("Username: ");
			string username = Console.ReadLine();
			Console.Write ("Password: ");
			string password = Console.ReadLine();
			ftp.Login (username, password);

			// transfer the file
			long bytes;
			if (method == "get")
				bytes = ftp.GetFile (remotePath, localPath);
			else
				bytes = ftp.PutFile (localPath, remotePath);

			Console.WriteLine ("Transferred {0} bytes.", bytes);

			// disconnect from the server
			ftp.Disconnect();
		}
	}
}
