//  
//   Rebex Sample Code License
// 
//   Copyright (c) 2009, Rebex CR s.r.o. www.rebex.net, 
//   All rights reserved.
// 
//   Permission to use, copy, modify, and/or distribute this software for any
//   purpose with or without fee is hereby granted
// 
//   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//   EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//   HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
//   OTHER DEALINGS IN THE SOFTWARE.
// 

using System;
using System.IO;
using System.Threading;
using Rebex.Net;

namespace Rebex.Samples.GetPutAsync
{
	/// <summary>
	/// This sample is a simple utility to upload/download a file to/from
	/// a FTP server using asynchronous methods of the Ftp class.
	/// </summary>
	class GetPutAsync
	{
		[STAThread]
		static void Main (string[] args)
		{
			string method = null;
			if (args.Length == 5)
			{
				switch (args[0].ToLower())
				{
					case "get": method = "get"; break;
					case "put": method = "put"; break;
				}
			}

			if (method == null)
			{
				// display syntax if wrong number of parameters or unsupported operation
				Console.WriteLine ("GetPutAsync - uploads or downloads a file from a FTP server");
				Console.WriteLine ("Syntax: GetPutAsync get|put unsecure|secure|implicit hostname remotepath localpath");
				Console.WriteLine ("Example: GetPutAsync get unsecure ftp.microsoft.com /developr/readme.txt readme.txt");
				return;
			}
			FtpSecurity security = (FtpSecurity) Enum.Parse (typeof(FtpSecurity), args[1], true);
			string hostname = args[2];
			string remotePath = args[3];
			string localPath = args[4];

			// create Ftp object and set event handlers
			Ftp ftp = new Ftp();
			ftp.TransferProgress += new FtpTransferProgressEventHandler (TransferProgress);
			ftp.ResponseRead += new FtpResponseReadEventHandler (ResponseRead);
			ftp.CommandSent += new FtpCommandSentEventHandler (CommandSent);

			try
			{
				// set SSL parameters to accept all server certificates...
				// do not do this in production code, server certificates should
				// be verified - use CertificateVerifier.Default instead
				TlsParameters par = new TlsParameters ();
				par.CommonName = hostname;
				par.CertificateVerifier = CertificateVerifier.AcceptAll;

				// connect to the server
				ftp.Connect (hostname, Ftp.DefaultPort, par, security);
				// ask for username and password and login
				Console.Write ("Username: ");
				string username = Console.ReadLine();
				Console.Write ("Password: ");
				string password = Console.ReadLine();
				ftp.Login (username, password);
				Console.WriteLine ();

				// begin asynchronous transfer
				IAsyncResult asyncResult;
				if (method == "get")
					asyncResult = ftp.BeginGetFile (remotePath, localPath, new AsyncCallback(MyCallback), null);
				else
					asyncResult = ftp.BeginPutFile (localPath, remotePath, new AsyncCallback(MyCallback), null);

				// do something else here...

				// wait for the transfer to end
				asyncResult.AsyncWaitHandle.WaitOne();

				// get the result
				long bytes;
				if (method == "get")
					bytes = ftp.EndGetFile (asyncResult);
				else
					bytes = ftp.EndPutFile (asyncResult);

				// disconnect from the server
				ftp.Disconnect();
			}
			catch (FtpException e)
			{
				// output error description to the console
				Console.WriteLine ("'{0}' error: {1}", e.Status, e);
			}
			finally
			{
				// release all resources
				ftp.Dispose();
			}
		}

		public static void MyCallback (IAsyncResult asyncResult)
		{
			Console.WriteLine ("\nTransfer finished.");
		}

		public static void TransferProgress (object sender, FtpTransferProgressEventArgs e)
		{
			if (e.State != FtpTransferState.None)
				Console.WriteLine ("\rTransferred {0} bytes...", e.BytesTransferred);
		}

		public static void ResponseRead (object sender, FtpResponseReadEventArgs e)
		{
			Console.WriteLine (e.Response);
		}

		public static void CommandSent (object sender, FtpCommandSentEventArgs e)
		{
			Console.WriteLine ("-> " + e.Command);
		}

	}
}
