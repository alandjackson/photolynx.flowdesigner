//  
//   Rebex Sample Code License
// 
//   Copyright (c) 2009, Rebex CR s.r.o. www.rebex.net, 
//   All rights reserved.
// 
//   Permission to use, copy, modify, and/or distribute this software for any
//   purpose with or without fee is hereby granted
// 
//   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//   EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//   HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
//   OTHER DEALINGS IN THE SOFTWARE.
// 

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Rebex.Samples.FileListEvents
{
	/// <summary>
	/// Summary description for Connection.
	/// </summary>
	public class Connection : System.Windows.Forms.Form
	{
		private System.Windows.Forms.CheckBox cbAcceptAll;
		private System.Windows.Forms.CheckBox cbSecure;
		private System.Windows.Forms.Label lblPort;
		private System.Windows.Forms.Label lblUsername;
		private System.Windows.Forms.Label lblHostname;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnConnect;
		private System.Windows.Forms.Label lblPassword;
		private System.Windows.Forms.TextBox tbPassword;
		private System.Windows.Forms.TextBox tbUsername;
		private System.Windows.Forms.TextBox tbHostname;
		private System.Windows.Forms.TextBox tbPort;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public string Hostname 
		{
			get 
			{
				return tbHostname.Text;
			}
		}

		public int Port 
		{
			get 
			{
				try 
				{
					 return Int32.Parse(tbPort.Text);
				} 
				catch 
				{
					return Rebex.Net.Ftp.DefaultPort;
				}
			}
		}

		public string Username 
		{
			get 
			{
				return tbUsername.Text;
			}
		}

		public string Password 
		{
			get 
			{
				return tbPassword.Text;
			}
		}

		public bool Secure
		{
			get 
			{
				return cbSecure.Checked;
			}
		}

		public bool AcceptAll 
		{
			get 
			{
				return cbAcceptAll.Checked;
			}
		}

		public Connection()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cbAcceptAll = new System.Windows.Forms.CheckBox();
			this.cbSecure = new System.Windows.Forms.CheckBox();
			this.lblPort = new System.Windows.Forms.Label();
			this.lblUsername = new System.Windows.Forms.Label();
			this.lblHostname = new System.Windows.Forms.Label();
			this.tbPassword = new System.Windows.Forms.TextBox();
			this.tbUsername = new System.Windows.Forms.TextBox();
			this.tbHostname = new System.Windows.Forms.TextBox();
			this.tbPort = new System.Windows.Forms.TextBox();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnConnect = new System.Windows.Forms.Button();
			this.lblPassword = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// cbAcceptAll
			// 
			this.cbAcceptAll.Location = new System.Drawing.Point(80, 56);
			this.cbAcceptAll.Name = "cbAcceptAll";
			this.cbAcceptAll.Size = new System.Drawing.Size(248, 32);
			this.cbAcceptAll.TabIndex = 9;
			this.cbAcceptAll.Text = "Do not validate any server certificates - this is insecure but useful for testing" +
				".";
			// 
			// cbSecure
			// 
			this.cbSecure.Location = new System.Drawing.Point(8, 56);
			this.cbSecure.Name = "cbSecure";
			this.cbSecure.Size = new System.Drawing.Size(88, 32);
			this.cbSecure.TabIndex = 8;
			this.cbSecure.Text = "TLS/SSL";
			// 
			// lblPort
			// 
			this.lblPort.Location = new System.Drawing.Point(240, 8);
			this.lblPort.Name = "lblPort";
			this.lblPort.Size = new System.Drawing.Size(32, 24);
			this.lblPort.TabIndex = 2;
			this.lblPort.Text = "Port:";
			this.lblPort.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblUsername
			// 
			this.lblUsername.Location = new System.Drawing.Point(8, 32);
			this.lblUsername.Name = "lblUsername";
			this.lblUsername.Size = new System.Drawing.Size(72, 24);
			this.lblUsername.TabIndex = 4;
			this.lblUsername.Text = "Username:";
			this.lblUsername.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblHostname
			// 
			this.lblHostname.Location = new System.Drawing.Point(8, 8);
			this.lblHostname.Name = "lblHostname";
			this.lblHostname.Size = new System.Drawing.Size(72, 24);
			this.lblHostname.TabIndex = 0;
			this.lblHostname.Text = "Hostname:";
			this.lblHostname.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// tbPassword
			// 
			this.tbPassword.Location = new System.Drawing.Point(240, 32);
			this.tbPassword.Name = "tbPassword";
			this.tbPassword.PasswordChar = '*';
			this.tbPassword.Size = new System.Drawing.Size(88, 20);
			this.tbPassword.TabIndex = 7;
			this.tbPassword.Text = "";
			// 
			// tbUsername
			// 
			this.tbUsername.Location = new System.Drawing.Point(80, 32);
			this.tbUsername.Name = "tbUsername";
			this.tbUsername.Size = new System.Drawing.Size(88, 20);
			this.tbUsername.TabIndex = 5;
			this.tbUsername.Text = "";
			// 
			// tbHostname
			// 
			this.tbHostname.Location = new System.Drawing.Point(80, 8);
			this.tbHostname.Name = "tbHostname";
			this.tbHostname.Size = new System.Drawing.Size(152, 20);
			this.tbHostname.TabIndex = 1;
			this.tbHostname.Text = "";
			// 
			// tbPort
			// 
			this.tbPort.Location = new System.Drawing.Point(272, 8);
			this.tbPort.Name = "tbPort";
			this.tbPort.Size = new System.Drawing.Size(56, 20);
			this.tbPort.TabIndex = 3;
			this.tbPort.Text = "21";
			// 
			// btnCancel
			// 
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnCancel.Location = new System.Drawing.Point(256, 88);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.TabIndex = 11;
			this.btnCancel.Text = "Ca&ncel";
			// 
			// btnConnect
			// 
			this.btnConnect.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnConnect.Location = new System.Drawing.Point(176, 88);
			this.btnConnect.Name = "btnConnect";
			this.btnConnect.TabIndex = 10;
			this.btnConnect.Text = "&Connect";
			this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
			// 
			// lblPassword
			// 
			this.lblPassword.Location = new System.Drawing.Point(176, 32);
			this.lblPassword.Name = "lblPassword";
			this.lblPassword.Size = new System.Drawing.Size(64, 24);
			this.lblPassword.TabIndex = 6;
			this.lblPassword.Text = "Password:";
			this.lblPassword.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Connection
			// 
			this.AcceptButton = this.btnConnect;
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.CancelButton = this.btnCancel;
			this.ClientSize = new System.Drawing.Size(336, 118);
			this.Controls.Add(this.lblPassword);
			this.Controls.Add(this.btnConnect);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.cbAcceptAll);
			this.Controls.Add(this.cbSecure);
			this.Controls.Add(this.lblPort);
			this.Controls.Add(this.lblUsername);
			this.Controls.Add(this.lblHostname);
			this.Controls.Add(this.tbPassword);
			this.Controls.Add(this.tbUsername);
			this.Controls.Add(this.tbHostname);
			this.Controls.Add(this.tbPort);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "Connection";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Connection";
			this.ResumeLayout(false);

		}
		#endregion

		private void btnConnect_Click(object sender, System.EventArgs e)
		{
			this.DialogResult = DialogResult.OK;
		}
	}
}
