//  
//   Rebex Sample Code License
// 
//   Copyright (c) 2009, Rebex CR s.r.o. www.rebex.net, 
//   All rights reserved.
// 
//   Permission to use, copy, modify, and/or distribute this software for any
//   purpose with or without fee is hereby granted
// 
//   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//   EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//   HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
//   OTHER DEALINGS IN THE SOFTWARE.
// 

namespace Rebex.Samples.Code
{
	/// <summary>
	/// Miscellaneous constants.
	/// </summary>
	public static class Constants
	{
		/// <summary>
		/// Directory delimiter.
		/// </summary>
		public const char PathDelimiter = '/';
	}

	/// <summary>
	/// Page URL parameters.
	/// </summary>
	public static class UrlParameter
	{
		/// <summary>
		/// URL parameter for current directory.
		/// </summary>
		public const string CurrentFolder = "currentDir";

		/// <summary>
		/// URL parameter for command type.
		/// </summary>
		public const string Command = "command";

		/// <summary>
		/// First URL parameter for command argument value.
		/// </summary>
		public const string UrlCommandArgument1 = "cmdArg1";

		/// <summary>
		/// Second URL parameter for command argument value.
		/// </summary>
		public const string UrlCommandArgument2 = "cmdArg2";

		/// <summary>
		/// URL parameter for return URL when logging in.
		/// </summary>
		public const string ReturnUrl = "returnUrl";
	}

	/// <summary>
	/// Values for <see cref="UrlParameter.Command"/> web page argument.
	/// </summary>
	public static class UrlParameterCommand
	{
		/// <summary>
		/// Lists current directory.
		/// </summary>
		/// <remarks>
		/// Expects no argument. Lists <see cref="UrlParameter.CurrentFolder"/> directory.
		/// </remarks>
		public const string List = "list";

		/// <summary>
		/// Downloads a file.
		/// </summary>
		/// <remarks>
		/// Expects <see cref="UrlParameter.UrlCommandArgument1"/> as an absolute path to the file.
		/// </remarks>
		public const string Download = "download";

		/// <summary>
		/// Deletes a file.
		/// </summary>
		/// <remarks>
		/// Expects <see cref="UrlParameter.UrlCommandArgument1"/> as an absolute path to the file.
		/// </remarks>
		public const string DeleteFile = "deleteFile";

		/// <summary>
		/// Creates a new directory.
		/// </summary>
		/// <remarks>
		/// Expects <see cref="UrlParameter.UrlCommandArgument1"/> as a name of the new directory.
		/// </remarks>
		public const string CreateDirectory = "createDirectory";

		/// <summary>
		/// Deletes a directory.
		/// </summary>
		/// <remarks>
		/// Expects <see cref="UrlParameter.UrlCommandArgument1"/> as an absolute path to the directory.
		/// </remarks>
		public const string DeleteDirectory = "deleteDirectory";

		/// <summary>
		/// Renames file or directory.
		/// </summary>
		/// <remarks>
		/// Expects <see cref="UrlParameter.UrlCommandArgument1"/> as a name of the original file.
		/// Expects <see cref="UrlParameter.UrlCommandArgument2"/> as a new file name.
		/// </remarks>
		public const string Rename = "rename";

		/// <summary>
		/// Resolves a symlink.
		/// </summary>
		/// <remarks>
		/// Expects <see cref="UrlParameter.UrlCommandArgument1"/> as an absolute path to the symlink.
		/// </remarks>
		public const string Symlink = "symlink";

		/// <summary>
		/// Clears saved connection information.
		/// </summary>
		/// <remarks>
		/// Expects no argument.
		/// </remarks>
		public const string LogOut = "logout";
	}

	/// <summary>
	/// Determines the image to show.
	/// </summary>
	public enum IconType
	{
		File,
		Folder,
		Symlink,
		Rename,
		Delete,
		DirectoryUp,
		AddFolder,
		UploadFile,
		Refresh,
	}
}