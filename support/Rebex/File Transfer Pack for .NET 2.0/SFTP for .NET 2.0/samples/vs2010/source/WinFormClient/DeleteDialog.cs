//  
//   Rebex Sample Code License
// 
//   Copyright (c) 2009, Rebex CR s.r.o. www.rebex.net, 
//   All rights reserved.
// 
//   Permission to use, copy, modify, and/or distribute this software for any
//   purpose with or without fee is hereby granted
// 
//   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//   EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//   HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
//   OTHER DEALINGS IN THE SOFTWARE.
// 

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Rebex.Samples
{
	/// <summary>
	/// remove (delete) file dialog
	/// </summary>
	public class DeleteDialog : System.Windows.Forms.Form
	{
		public enum DialogPrompt
		{
			DeleteFile,
			DeleteDirectory,
			DeleteNonEmptyDirectory,
		}

		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.Label lblMsg;
		private System.ComponentModel.Container components = null;

		private string _msgFormat;	// message format
		private string _strPath;	// file path
		private bool _bOK = false;  // true if form was confirmed; false otherwise

		public string Path
		{
			get { return _strPath; }
			set
			{
				_strPath = value;
				if (_strPath != null) 
					lblMsg.Text = string.Format(_msgFormat, _strPath);
			}
		}

		public bool OK
		{
			get { return _bOK; }
		}

		// -------------------------------------------------------------------------
		public DeleteDialog(DialogPrompt prompt)
		{
			InitializeComponent();

			switch (prompt)
			{			 
				case DialogPrompt.DeleteFile: _msgFormat = "Delete file '{0}'?"; break;
				case DialogPrompt.DeleteDirectory: _msgFormat = "Delete directory '{0}'?"; break;
				case DialogPrompt.DeleteNonEmptyDirectory: _msgFormat = "Directory '{0}' is not empty. Delete anyway?"; break;
				default: throw new ArgumentException("Invalid DialogPrompt value.", "prompt");
			}
		}

		// -------------------------------------------------------------------------
		/// <summary>
		/// clean up any resources being used
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnOK = new System.Windows.Forms.Button();
			this.lblMsg = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// btnCancel
			// 
			this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnCancel.Location = new System.Drawing.Point(264, 40);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.TabIndex = 5;
			this.btnCancel.Text = "Cancel";
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// btnOK
			// 
			this.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnOK.Location = new System.Drawing.Point(184, 40);
			this.btnOK.Name = "btnOK";
			this.btnOK.TabIndex = 4;
			this.btnOK.Text = "OK";
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			// 
			// lblMsg
			// 
			this.lblMsg.Location = new System.Drawing.Point(16, 8);
			this.lblMsg.Name = "lblMsg";
			this.lblMsg.Size = new System.Drawing.Size(320, 23);
			this.lblMsg.TabIndex = 3;
			// 
			// DeleteDialog
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(346, 72);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.btnCancel,
																		  this.btnOK,
																		  this.lblMsg});
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.KeyPreview = true;
			this.Name = "DeleteDialog";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Delete prompt dialog";
			this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.DeleteDialog_KeyPress);
			this.ResumeLayout(false);

		}
		#endregion

		// -------------------------------------------------------------------------
		/// <summary>
		/// form confirmation click
		/// </summary>
		private void btnOK_Click(object sender, System.EventArgs e)
		{
			_bOK = true;
			this.Close();
		}

		// -------------------------------------------------------------------------
		/// <summary>
		/// form cancellation click
		/// </summary>
		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			_bOK = false;
			this.Close();
		}

		// -------------------------------------------------------------------------
		/// <summary>
		/// global key handler
		/// </summary>
		private void DeleteDialog_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			// when 'enter' is pressed the form is closed

			if (e.KeyChar == '\r')
			{
				_bOK = true;
				this.Close();
			}

			// when 'esc' is pressed the form is closed

			if ((int)(e.KeyChar) == Convert.ToChar(Keys.Escape))
			{
				_bOK = false;
				this.Close();
			}
		}
	}
}
