'  
'   Rebex Sample Code License
' 
'   Copyright (c) 2009, Rebex CR s.r.o. www.rebex.net, 
'   All rights reserved.
' 
'   Permission to use, copy, modify, and/or distribute this software for any
'   purpose with or without fee is hereby granted
' 
'   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
'   EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
'   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
'   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
'   HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
'   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
'   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
'   OTHER DEALINGS IN THE SOFTWARE.
' 

Imports System
Imports System.Drawing
Imports System.Collections
Imports System.ComponentModel
Imports System.Windows.Forms

'' <summary>
'' Summary description for AboutForm.
'' </summary>
Public Class AboutForm
    Inherits System.Windows.Forms.Form

    Public Sub New()
        ' Required for Windows Form Designer support
        InitializeComponent()
    End Sub

#Region "Windows Form Designer generated code"

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    Private label1 As System.Windows.Forms.Label
    Private WithEvents btnOk As System.Windows.Forms.Button
    Private label2 As System.Windows.Forms.Label
    Private label3 As System.Windows.Forms.Label
    Private label4 As System.Windows.Forms.Label

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.label1 = New System.Windows.Forms.Label
        Me.btnOk = New System.Windows.Forms.Button
        Me.label2 = New System.Windows.Forms.Label
        Me.label3 = New System.Windows.Forms.Label
        Me.label4 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        ' 
        ' label1
        ' 
        Me.label1.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 13.25, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(238, System.Byte))
        Me.label1.Location = New System.Drawing.Point(8, 8)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(280, 32)
        Me.label1.TabIndex = 0
        Me.label1.Text = "Rebex Key Generator"
        Me.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        ' 
        ' btnOk
        ' 
        Me.btnOk.Anchor = CType(System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right, System.Windows.Forms.AnchorStyles)
        Me.btnOk.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnOk.Location = New System.Drawing.Point(206, 114)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.TabIndex = 1
        Me.btnOk.Text = "&OK"
        ' 
        ' label2
        ' 
        Me.label2.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.label2.Location = New System.Drawing.Point(8, 40)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(280, 23)
        Me.label2.TabIndex = 2
        Me.label2.Text = "Rebex SFTP for .NET Sample Application"
        ' 
        ' label3
        ' 
        Me.label3.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.label3.Location = New System.Drawing.Point(8, 64)
        Me.label3.Name = "label3"
        Me.label3.Size = New System.Drawing.Size(280, 23)
        Me.label3.TabIndex = 3
        Me.label3.Text = "http://www.rebex.net"
        ' 
        ' label4
        ' 
        Me.label4.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.label4.Location = New System.Drawing.Point(8, 88)
        Me.label4.Name = "label4"
        Me.label4.Size = New System.Drawing.Size(280, 23)
        Me.label4.TabIndex = 4
        Me.label4.Text = "Developed by Rebex"
        ' 
        ' AboutForm
        ' 
        Me.AcceptButton = Me.btnOk
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.CancelButton = Me.btnOk
        Me.ClientSize = New System.Drawing.Size(290, 144)
        Me.Controls.Add(label4)
        Me.Controls.Add(label3)
        Me.Controls.Add(label2)
        Me.Controls.Add(btnOk)
        Me.Controls.Add(label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "AboutForm"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "About"
        Me.ResumeLayout(False)
    End Sub
#End Region


    Private Sub btnOk_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOk.Click
        Me.Close()
    End Sub 'btnOk_Click
End Class 'AboutForm
