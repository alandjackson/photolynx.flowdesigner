//  
//   Rebex Sample Code License
// 
//   Copyright (c) 2009, Rebex CR s.r.o. www.rebex.net, 
//   All rights reserved.
// 
//   Permission to use, copy, modify, and/or distribute this software for any
//   purpose with or without fee is hereby granted
// 
//   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//   EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//   HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
//   OTHER DEALINGS IN THE SOFTWARE.
// 

using System;
using System.Net;
using Rebex.Net;

namespace Rebex.Samples.FileList
{
	/// <summary>
	/// This sample retrieves the list of files from a SFTP server.
	/// It demonstrates the usage of SftpList class.
	/// </summary>
	class FileList
	{
		[STAThread]
		static void Main(string[] args)
		{
			if (args.Length < 1)
			{
				// display syntax if wrong number of parameters or unsupported operation
				Console.WriteLine("FileList - retrieves file list from a SFTP server");
				Console.WriteLine("Syntax: FileList hostname remotepath");
				Console.WriteLine("Example: FileList sftp.example.com directory/");
				return;
			}

            // get server and path
			string server = args[0];
			string path = null;

			if (args.Length == 2) 
			{
				path = args[1];
			}

			// create Sftp object and connect to the server
			Sftp ftp = new Sftp();
			ftp.Connect(server, Sftp.DefaultPort);

			// get username and password
			string username;
			string password;
			Console.Write("Username: ");
			username = Console.ReadLine();
			Console.Write("Password: ");
			password = Console.ReadLine();
			if (password.Length == 0)
				password = null;

			// login with username and password
			ftp.Login(username, password);

			if (path != null) 
			{
				ftp.ChangeDirectory(path);
			}
			SftpItemCollection list = ftp.GetList ();

			for (int i = 0; i < list.Count; i++)
			{
				SftpItem item = list[i];
				if (item.IsSymlink)
					Console.Write("s ");
				else if (item.IsDirectory)
					Console.Write("d ");
				else
					Console.Write("- ");

				Console.Write(item.Modified.ToString("u").Substring(0,16));
				Console.Write(item.Size.ToString().PadLeft(10,' '));
				Console.Write(" {0}", item.Name);
				if (item.IsSymlink)
					Console.Write(" (symlink)");
				Console.WriteLine();
			}

			// disconnect from the server
			ftp.Disconnect();
		}
	}
}
