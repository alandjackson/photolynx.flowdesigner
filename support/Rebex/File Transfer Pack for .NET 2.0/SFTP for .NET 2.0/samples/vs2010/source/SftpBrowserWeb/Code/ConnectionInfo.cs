//  
//   Rebex Sample Code License
// 
//   Copyright (c) 2009, Rebex CR s.r.o. www.rebex.net, 
//   All rights reserved.
// 
//   Permission to use, copy, modify, and/or distribute this software for any
//   purpose with or without fee is hereby granted
// 
//   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//   EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//   HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
//   OTHER DEALINGS IN THE SOFTWARE.
// 

using Rebex.Net;

namespace Rebex.Samples.Code
{
	/// <summary>
	/// Helper class to hold an SFTP connection information.
	/// </summary>
	public class ConnectionInfo
	{
		private const string SessionName = "sftpConnectionInfo";

		private string _password;
		private string _serverName;
		private string _userName;
		private int _port;

		/// <summary>
		/// The server address - either a hostname or a dotted string IP address.
		/// </summary>
		public string ServerName
		{
			get { return _serverName; }
			set { _serverName = value; }
		}

		/// <summary>
		/// Port of the remote server.
		/// </summary>
		public int Port
		{
			get { return _port; }
			set { _port = value; }
		}

		/// <summary>
		/// User name for logging in.
		/// </summary>
		public string UserName
		{
			get { return _userName; }
			set { _userName = value; }
		}

		/// <summary>
		/// Password for logging in.
		/// </summary>
		public string Password
		{
			get { return _password; }
			set { _password = value; }
		}

		/// <summary>
		/// Retrive saved connection information from the Session.
		/// </summary>
		/// <returns>
		/// Instance of the <see cref="ConnectionInfo"/>; or null if no <see cref="ConnectionInfo"/> was saved.
		/// </returns>
		public static ConnectionInfo Load()
		{
			return System.Web.HttpContext.Current.Session[SessionName] as ConnectionInfo;
		}

		/// <summary>
		/// Save the current connection information to the Session.
		/// </summary>
		public void Save()
		{
			System.Web.HttpContext.Current.Session[SessionName] = this;
		}

		/// <summary>
		/// Remove connection information from the Session.
		/// </summary>
		public static void Clear()
		{
			System.Web.HttpContext.Current.Session[SessionName] = null;
		}
	}
}