'  
'   Rebex Sample Code License
' 
'   Copyright (c) 2009, Rebex CR s.r.o. www.rebex.net, 
'   All rights reserved.
' 
'   Permission to use, copy, modify, and/or distribute this software for any
'   purpose with or without fee is hereby granted
' 
'   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
'   EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
'   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
'   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
'   HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
'   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
'   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
'   OTHER DEALINGS IN THE SOFTWARE.
' 

Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Text
Imports System.Windows.Forms
Imports Rebex.Net

Namespace Rebex.Samples.WinFormClient
    ''' <summary>
    ''' Dialog for getting user response to AuthenticationRequest event.
    ''' </summary>
    Partial Public Class AuthenticationRequestDialog
        Inherits Form
        Private ReadOnly _sshAuthenticationRequestEventArgs As SshAuthenticationRequestEventArgs

        ''' <summary>
        ''' Creates a new instance of AuthenticationRequestDialog used for providing responses to AuthenticationRequest event.
        ''' </summary>
        ''' <param name="sshAuthenticationRequestEventArgs"></param>
        Public Sub New(ByVal sshAuthenticationRequestEventArgs As SshAuthenticationRequestEventArgs)
            If sshAuthenticationRequestEventArgs Is Nothing Then
                Throw New ArgumentNullException("sshAuthenticationRequestEventArgs")
            End If

            InitializeComponent()

            _sshAuthenticationRequestEventArgs = sshAuthenticationRequestEventArgs
            InitializeDialog()
        End Sub

        ''' <summary>
        ''' Initializes dialog with data from SshAuthenticationRequestEventArgs.
        ''' </summary>
        Private Sub InitializeDialog()
            ' dialog header
            Me.Text = String.Format("Authentication request: {0}", _sshAuthenticationRequestEventArgs.Name)

            ' instructions
            Me.LabelInstructions.Text = _sshAuthenticationRequestEventArgs.Instructions

            ' prompts and textboxes for answers
            For Each item As SshAuthenticationRequestItem In _sshAuthenticationRequestEventArgs.Items
                AddLabel(PromptPanel, item.Prompt)
                AddTextBox(PromptPanel, item)
            Next

            ' resize the dialog according to the prompt count
            Me.Height += PromptPanel.DisplayRectangle.Height - PromptPanel.ClientRectangle.Height
        End Sub

        ''' <summary>
        ''' Adds Label control to the bottom of the TableLayoutPanel.
        ''' </summary>
        Private Sub AddLabel(ByVal panel As TableLayoutPanel, ByVal labelText As String)
            Dim label As New Label()
            label.Dock = DockStyle.Fill
            label.Text = labelText
            label.TextAlign = ContentAlignment.MiddleLeft

            panel.Controls.Add(label, 0, panel.RowCount)
            panel.RowCount += 1
        End Sub

        ''' <summary>
        ''' Adds TextBox control to the bottom of the TableLayoutPanel. The TextBox is data-bound to SshAuthenticationRequestItem.Response property.
        ''' </summary>
        Private Sub AddTextBox(ByVal panel As TableLayoutPanel, ByVal requestItem As SshAuthenticationRequestItem)
            Dim textbox As New TextBox()
            textbox.UseSystemPasswordChar = requestItem.IsSecret
            textbox.Dock = DockStyle.Fill
            textbox.DataBindings.Add("Text", requestItem, "Response")

            panel.Controls.Add(textbox, 0, panel.RowCount)
            panel.RowCount += 1
        End Sub

    End Class
End Namespace
