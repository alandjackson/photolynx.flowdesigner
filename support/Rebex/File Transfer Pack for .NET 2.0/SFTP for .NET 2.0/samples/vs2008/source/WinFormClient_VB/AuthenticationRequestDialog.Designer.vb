'  
'   Rebex Sample Code License
' 
'   Copyright (c) 2009, Rebex CR s.r.o. www.rebex.net, 
'   All rights reserved.
' 
'   Permission to use, copy, modify, and/or distribute this software for any
'   purpose with or without fee is hereby granted
' 
'   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
'   EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
'   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
'   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
'   HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
'   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
'   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
'   OTHER DEALINGS IN THE SOFTWARE.
' 

Namespace Rebex.Samples.WinFormClient
	Partial Class AuthenticationRequestDialog
		''' <summary>
		''' Required designer variable.
		''' </summary>
		Private components As System.ComponentModel.IContainer = Nothing

		''' <summary>
		''' Clean up any resources being used.
		''' </summary>
		''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
			If disposing AndAlso (components IsNot Nothing) Then
				components.Dispose()
			End If
			MyBase.Dispose(disposing)
		End Sub

		#Region "Windows Form Designer generated code"

		''' <summary>
		''' Required method for Designer support - do not modify
		''' the contents of this method with the code editor.
		''' </summary>
		Private Sub InitializeComponent()
            Me.ButtonCancel = New System.Windows.Forms.Button
            Me.ButtonOk = New System.Windows.Forms.Button
            Me.LabelInstructions = New System.Windows.Forms.Label
            Me.PromptPanel = New System.Windows.Forms.TableLayoutPanel
            Me.SuspendLayout()
            '
            'ButtonCancel
            '
            Me.ButtonCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.ButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.ButtonCancel.Location = New System.Drawing.Point(287, 71)
            Me.ButtonCancel.Name = "ButtonCancel"
            Me.ButtonCancel.Size = New System.Drawing.Size(75, 23)
            Me.ButtonCancel.TabIndex = 0
            Me.ButtonCancel.Text = "&Cancel"
            Me.ButtonCancel.UseVisualStyleBackColor = True
            '
            'ButtonOk
            '
            Me.ButtonOk.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.ButtonOk.DialogResult = System.Windows.Forms.DialogResult.OK
            Me.ButtonOk.Location = New System.Drawing.Point(206, 71)
            Me.ButtonOk.Name = "ButtonOk"
            Me.ButtonOk.Size = New System.Drawing.Size(75, 23)
            Me.ButtonOk.TabIndex = 0
            Me.ButtonOk.Text = "&OK"
            Me.ButtonOk.UseVisualStyleBackColor = True
            '
            'LabelInstructions
            '
            Me.LabelInstructions.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LabelInstructions.Location = New System.Drawing.Point(13, 13)
            Me.LabelInstructions.Name = "LabelInstructions"
            Me.LabelInstructions.Size = New System.Drawing.Size(349, 13)
            Me.LabelInstructions.TabIndex = 1
            Me.LabelInstructions.Text = "Label instructions"
            '
            'PromptPanel
            '
            Me.PromptPanel.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                        Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.PromptPanel.AutoScroll = True
            Me.PromptPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
            Me.PromptPanel.ColumnCount = 1
            Me.PromptPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
            Me.PromptPanel.Location = New System.Drawing.Point(16, 42)
            Me.PromptPanel.Name = "PromptPanel"
            Me.PromptPanel.RowCount = 1
            Me.PromptPanel.RowStyles.Add(New System.Windows.Forms.RowStyle)
            Me.PromptPanel.Size = New System.Drawing.Size(346, 23)
            Me.PromptPanel.TabIndex = 2
            '
            'AuthenticationRequestDialog
            '
            Me.AcceptButton = Me.ButtonOk
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.CancelButton = Me.ButtonCancel
            Me.ClientSize = New System.Drawing.Size(374, 106)
            Me.Controls.Add(Me.PromptPanel)
            Me.Controls.Add(Me.LabelInstructions)
            Me.Controls.Add(Me.ButtonOk)
            Me.Controls.Add(Me.ButtonCancel)
            Me.MaximizeBox = False
            Me.MinimizeBox = False
            Me.Name = "AuthenticationRequestDialog"
            Me.Text = "AuthenticationRequestDialog"
            Me.ResumeLayout(False)

        End Sub

		#End Region

		Private ButtonCancel As System.Windows.Forms.Button
		Private ButtonOk As System.Windows.Forms.Button
		Private LabelInstructions As System.Windows.Forms.Label
		Private PromptPanel As System.Windows.Forms.TableLayoutPanel
	End Class
End Namespace
