'  
'   Rebex Sample Code License
' 
'   Copyright (c) 2009, Rebex CR s.r.o. www.rebex.net, 
'   All rights reserved.
' 
'   Permission to use, copy, modify, and/or distribute this software for any
'   purpose with or without fee is hereby granted
' 
'   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
'   EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
'   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
'   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
'   HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
'   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
'   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
'   OTHER DEALINGS IN THE SOFTWARE.
' 

Imports System
Imports System.Net
Imports Rebex.Net

Module FileList

    ' This sample retrieves the list of files from a SFTP server.
    ' It demonstrates the usage of SftpList class.
    Sub Main()
        Dim args() As String = Environment.GetCommandLineArgs()
        If args.Length < 2 Then
            ' display syntax if wrong number of parameters or unsupported operation
            Console.WriteLine("FileList - retrieves file list from a SFTP server")
            Console.WriteLine("Syntax: FileList hostname remotepath")
            Console.WriteLine("Example: FileList sftp.example.com directory/")
            Return
        End If

        ' get server and path
        Dim server As String = args(1)
        Dim remotePath As String = Nothing

        If (args.Length = 3) Then
            remotePath = args(2)
        End If

        ' create Sftp object and connect to the server
        Dim ftp As New Sftp
        ftp.Connect(server, Sftp.DefaultPort)

        ' get username and password
        Dim username As String
        Dim password As String
        Console.Write("Username: ")
        username = Console.ReadLine()
        Console.Write("Password: ")
        password = Console.ReadLine()
        If password.Length = 0 Then
            password = Nothing
        End If

        ' login with username and password
        ftp.Login(username, password)

        If Not (remotePath Is Nothing) Then
            ftp.ChangeDirectory(remotePath)
        End If
        Dim list As SftpItemCollection = ftp.GetList()

        Dim i As Integer
        For i = 0 To list.Count - 1
            Dim item As SftpItem = list(i)
            If item.IsSymlink Then
                Console.Write("s ")
            ElseIf item.IsDirectory Then
                Console.Write("d ")
            Else
                Console.Write("- ")
            End If
            Console.Write(item.Modified.ToString("u").Substring(0, 16))
            Console.Write(item.Size.ToString().PadLeft(10, " "c))
            Console.Write(" {0}", item.Name)
            If item.IsSymlink Then
                Console.Write(" (symlink)")
            End If
            Console.WriteLine()
        Next i

        ' disconnect from the server
        ftp.Disconnect()
    End Sub

End Module
