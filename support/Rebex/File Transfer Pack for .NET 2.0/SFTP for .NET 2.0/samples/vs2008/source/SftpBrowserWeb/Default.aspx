<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" Codebehind="Default.aspx.cs"
    Inherits="Rebex.Samples._Default" %>

<%@ Import Namespace="Rebex.Samples.Code" %>
<%@ Import Namespace="Rebex.Net" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script type="text/javascript">
		// Show the delete confirmation message and redirect to new URL if confirmed
		function ConfirmDelete(url, itemName)
		{
			if (confirm("Realy delete '" + itemName + "'?"))
				window.location = url;
		}
		
		// Show a prompt message for entering new directory name and redirect to new URL if name was entered
		function ConfirmCreateDirectory(url)
		{
			var name = prompt("Enter new directory name", "Directory");
						
			if (name == null)
				return;
			
			window.location = url + '&<%= UrlParameter.UrlCommandArgument1 %>=' + escape(name);
		}
	    
	    // Show a prompt message for entering new name and redirect to new URL if new name was entered
		function ConfirmRename(url, itemName)
		{
			var name = prompt("Enter new name", itemName);
						
			if (name == null)
				return;
				
			window.location = url + '&<%= UrlParameter.UrlCommandArgument2 %>=' + escape(name);
		}
    </script>

    <%-- "Logout" link --%>
    <a class="cmdUp cmdLogout" href="Login.aspx?<%= UrlParameter.Command %>=<%= UrlParameterCommand.LogOut %>">
        Logout</a>
    <%-- show the current directory --%>
    <h1>
        <asp:Literal ID="ltPath" runat="server" /></h1>
    <%-- SFTP commands toolbar --%>
    <div>
        <ul class="toolbar">
            <%-- refresh page --%>
            <li runat="server" id="liRefresh">
                <asp:Panel CssClass="cmdUp" ID="pnlRefresh" runat="server">
                    <img src="/Images/<%= GetIcon(IconType.Refresh) %>" alt="Refresh" />
                    <asp:HyperLink runat="server" ID="hlRefresh" Text="Refresh" />
                </asp:Panel>
            </li>
            <%-- change directory one level up --%>
            <li runat="server" id="liCmdUp">
                <asp:Panel CssClass="cmdUp" ID="pnlCmdUp" runat="server">
                    <img src="/Images/<%= GetIcon(IconType.DirectoryUp) %>" alt="Directory up" />
                    <asp:HyperLink runat="server" ID="hlCmdUp" Text="Directory up" />
                </asp:Panel>
            </li>
            <%-- create a directory --%>
            <li>
                <asp:Panel CssClass="cmdUp" ID="pnlCreateDirectory" runat="server">
                    <img src="/Images/<%= GetIcon(IconType.AddFolder) %>" alt="Create a new directory" />
                    <a href="#" onclick="javascript:ConfirmCreateDirectory('Default.aspx?<%= UrlParameter.Command %>=<%= UrlParameterCommand.CreateDirectory %>&<%= UrlParameter.CurrentFolder %>=<%= Uri.EscapeDataString(CurrentDirectory) %>')">
                        Create directory</a>
                </asp:Panel>
            </li>
            <%-- upload file --%>
            <li>
                <asp:Panel CssClass="cmdUp" ID="pnlUploadFile" runat="server">
                    <img src="/Images/<%= GetIcon(IconType.UploadFile) %>" alt="Upload file" />
                    Upload file:
                    <asp:FileUpload ID="fileToUpload" runat="server" />
                    <asp:Button runat="server" ID="btnUploadFile" OnClick="UploadFile" Text="Upload" />
                </asp:Panel>
            </li>
        </ul>
    </div>
    <%-- error and info messages --%>
    <div class="clear">
        <asp:Label ID="lblError" runat="server" ForeColor="red" Font-Bold="true" EnableViewState="false" />
        <asp:Label ID="lblSuccess" runat="server" ForeColor="green" Font-Bold="true" EnableViewState="false" />
    </div>
    <%-- show list of SFTP server folders and files --%>
    <div>
        <asp:GridView ID="gwList" runat="server" CssClass="list" BorderStyle="None" BorderWidth="0"
            BorderColor="whitesmoke" RowStyle-BorderColor="white" AlternatingRowStyle-BackColor="whitesmoke"
            AutoGenerateColumns="false">
            <HeaderStyle CssClass="listHeader" />
            <Columns>
                <asp:TemplateField HeaderText="Name" ItemStyle-CssClass="tdName">
                    <ItemTemplate>
                        <%-- "delete" command --%>
                        <a href="#" onclick="javascript:ConfirmDelete('<%# GetDeleteUrl(Container.DataItem as SftpItem) %>','<%# Eval("Name") %>');">
                            <img src="/Images/<%# GetIcon(IconType.Delete) %>" alt="Delete" /></a>
                        <%-- "rename" command --%>
                        <a href="#" onclick="javascript:ConfirmRename('<%# GetRenameUrl(Container.DataItem as SftpItem) %>','<%# Eval("Name") %>');">
                            <img src="/Images/<%# GetIcon(IconType.Rename) %>" alt="Rename" /></a>
                        <%-- item icon: directory/file/link --%>
                        <a href="<%# GetUrl(Container.DataItem as SftpItem) %>">
                            <img src="/Images/<%# GetIcon(Container.DataItem as SftpItem) %>" alt="File type (file/folder)" /><%# Eval("Name") %></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Size" HeaderStyle-CssClass="thSize" ItemStyle-CssClass="tdSize"
                    ItemStyle-Wrap="false">
                    <ItemTemplate>
                        <%# GetSize(Container.DataItem as SftpItem)%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Modified" HeaderText="Modified" ItemStyle-Wrap="false" />
            </Columns>
            <EmptyDataTemplate>
                No files or directories
            </EmptyDataTemplate>
        </asp:GridView>
    </div>
</asp:Content>
