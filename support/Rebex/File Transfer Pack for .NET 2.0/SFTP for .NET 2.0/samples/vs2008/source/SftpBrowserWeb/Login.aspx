<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" Codebehind="Login.aspx.cs"
    Inherits="Rebex.Samples.Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table border="0" id="loginForm">
        <tr>
            <th>
                Server:</th>
            <td>
                <asp:TextBox ID="txtServerName" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <th>
                User name:</th>
            <td>
                <asp:TextBox ID="txtUserName" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <th>
                Password:</th>
            <td>
                <asp:TextBox ID="txtPassword" runat="server" TextMode="Password"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <th>
                Port:</th>
            <td>
                <asp:TextBox ID="txtPort" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="right">
                <asp:Button ID="btnConnect" OnClick="Connect" runat="server" Text="Connect" />
            </td>
        </tr>        
    </table>
    <asp:Label ID="lblError" runat="server" ForeColor="red"></asp:Label>
    <asp:ValidationSummary DisplayMode="SingleParagraph" runat="server" ID="validationSummary"
        EnableClientScript="true" />
    <asp:RegularExpressionValidator ID="revPort" runat="server" Display="None" ControlToValidate="txtPort"
        ErrorMessage="Enter only numeric characters." ValidationExpression="^[0-9]*$"
        SetFocusOnError="true" />
    <asp:RequiredFieldValidator ID="rfvPort" runat="server" Display="None" ControlToValidate="txtPort"
        ErrorMessage="Port must be filled" SetFocusOnError="true" />
</asp:Content>
