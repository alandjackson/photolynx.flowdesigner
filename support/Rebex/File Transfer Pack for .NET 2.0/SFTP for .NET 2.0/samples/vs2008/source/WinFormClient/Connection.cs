//  
//   Rebex Sample Code License
// 
//   Copyright (c) 2009, Rebex CR s.r.o. www.rebex.net, 
//   All rights reserved.
// 
//   Permission to use, copy, modify, and/or distribute this software for any
//   purpose with or without fee is hereby granted
// 
//   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//   EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//   HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
//   OTHER DEALINGS IN THE SOFTWARE.
// 

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.IO;
using System.Xml;

using Rebex.Net;

namespace Rebex.Samples.WinFormClient
{
	/// <summary>
	/// connection dialog
	/// </summary>
	public class Connection : System.Windows.Forms.Form
	{
		private System.ComponentModel.Container components = null;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.TextBox tbPassword;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox tbLogin;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox tbPort;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox tbHost;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.ComboBox cbProxyType;
		private System.Windows.Forms.TextBox tbProxyPort;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.CheckBox cbProxy;
		private System.Windows.Forms.TextBox tbProxy;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Button btnConnect;
		private System.Windows.Forms.TextBox tbProxyLogin;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.TextBox tbProxyPassword;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.StatusBar sbMessage;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.TextBox tbPrivateKey;
		private System.Windows.Forms.Button btnPrivateKey;
		private System.Windows.Forms.OpenFileDialog openFileDialog;
		private System.Windows.Forms.ComboBox cbLogLevel;
		private System.Windows.Forms.Label label12;

		// CONSTANTS
		public static readonly string ConfigFile = Path.Combine(Environment.GetFolderPath(System.Environment.SpecialFolder.LocalApplicationData), @"Rebex\SFTP\WinFormClient.xml");
		private System.Windows.Forms.Label label13;
		private CheckBox cbUseLargeBuffers;

		public bool _ok = false;    // form was confirmed

		public bool OK
		{
			get { return _ok; }
		}

		// host name

		public string Host
		{
			get { return tbHost.Text; }
			set { tbHost.Text = value; }
		}

		// port of the host

		public int Port
		{
			get
			{
				int temp;

				try
				{
					temp = Int32.Parse(tbPort.Text);
				}

				catch
				{
					return 0;    // def. value if error occures
				}

				return temp;
			}
			set { tbPort.Text = value.ToString(); }
		}

		// Use large buffers

		public bool UseLargeBuffers
		{
			get { return cbUseLargeBuffers.Checked; }
			set { cbUseLargeBuffers.Checked = value; }
		}

		// proxy server

		public string Proxy
		{
			set { tbProxy.Text = value; }
			get { return tbProxy.Text; }
		}

		// proxy login name

		public string ProxyLogin
		{
			set { tbProxyLogin.Text = value; }
			get { return tbProxyLogin.Text; }
		}

		// proxy password

		public string ProxyPassword
		{
			set { tbProxyPassword.Text = value; }
			get { return tbProxyPassword.Text; }
		}

		// is proxy enabled?

		public bool ProxyEnabled
		{
			set
			{
				if (value)
				{
					tbProxy.Enabled = true;
					tbProxyPort.Enabled = true;
					cbProxy.Checked = true;
					cbProxyType.Enabled = true;
					tbProxyLogin.Enabled = true;
					tbProxyPassword.Enabled = true;
				}
				else
				{
					tbProxy.Enabled = false;
					tbProxyPort.Enabled = false;
					cbProxy.Checked = false;
					cbProxyType.Enabled = false;
					tbProxyLogin.Enabled = false;
					tbProxyPassword.Enabled = false;
				}
			}

			get { return cbProxy.Checked; }
		}

		// proxy port

		public int ProxyPort
		{
			set { tbProxyPort.Text = value.ToString(); }
			get
			{
				int temp;

				try
				{
					temp = Int32.Parse(tbProxyPort.Text);
				}

				catch
				{
					return 0;    // def. value if error occures
				}

				return temp;
			}
		}

		// login name

		public string Login
		{
			get { return tbLogin.Text; }
			set { tbLogin.Text = value; }
		}

		// password

		public string Password
		{
			get { return tbPassword.Text; }
			set { tbPassword.Text = value; }
		}

		// private key

		public string PrivateKey
		{
			get { return tbPrivateKey.Text; }
			set { tbPrivateKey.Text = value; }
		}

		// proxy type

		public ProxyType ProxyType
		{
			get
			{
				switch (cbProxyType.SelectedIndex)
				{
					default:
						return ProxyType.Socks4;
					case 1:
						return ProxyType.Socks4a;
					case 2:
						return ProxyType.Socks5;
					case 3:
						return ProxyType.HttpConnect;
				}
			}
			set
			{
				switch (value)
				{
					default:
						cbProxyType.SelectedIndex = 0; break;
					case ProxyType.Socks4a:
						cbProxyType.SelectedIndex = 1; break;
					case ProxyType.Socks5:
						cbProxyType.SelectedIndex = 2; break;
					case ProxyType.HttpConnect:
						cbProxyType.SelectedIndex = 3; break;
				}
			}
		}

		// log level

		public Rebex.LogLevel LogLevel
		{
			get
			{
				switch (cbLogLevel.SelectedIndex)
				{
					default:
						return Rebex.LogLevel.Info;
					case 1:
						return Rebex.LogLevel.Debug;
					case 2:
						return Rebex.LogLevel.Verbose;
					case 3:
						return Rebex.LogLevel.Error;
				}
			}
			set
			{
				switch (value)
				{
					default:
						cbLogLevel.SelectedIndex = 0; break;
					case Rebex.LogLevel.Debug:
						cbLogLevel.SelectedIndex = 1; break;
					case Rebex.LogLevel.Verbose:
						cbLogLevel.SelectedIndex = 2; break;
					case Rebex.LogLevel.Error:
						cbLogLevel.SelectedIndex = 3; break;
				}
			}
		}

		// -------------------------------------------------------------------------
		/// <summary>
		/// validity check
		/// </summary>
		/// <returns>true (everything is ok)</returns>
		public bool ValidateForm()
		{
			// sftp host

			if (tbHost.Text == null || tbHost.Text.Length <= 0)
			{
				sbMessage.Text = "sftp host is a required field";
				return false;
			}

			// sftp port

			if (tbPort.Text == null || tbPort.Text.Length <= 0)
			{
				sbMessage.Text = "sftp port is a required field";
				return false;
			}

			// sftp port [decimal]

			bool badSftpPort = false;

			try
			{
				uint n = UInt32.Parse(tbPort.Text);
				if (n == 0) badSftpPort = true;
			}

			catch
			{
				badSftpPort = true;
			}

			if (badSftpPort)
			{
				sbMessage.Text = "sftp port must be > 0";
				return false;
			}

			// sftp login

			if (tbLogin.Text == null || tbLogin.Text.Length <= 0)
			{
				sbMessage.Text = "sftp login is a required field";
				return false;
			}

			// sftp password

			if ((tbPassword.Text == null || tbPassword.Text.Length <= 0) && tbPrivateKey.Text.Trim() == "")
			{
				sbMessage.Text = "sftp password is a required field";
				return false;
			}

			// ----------------
			// proxy check-outs
			// ----------------

			if (cbProxy.Checked)
			{
				// proxy host

				if (tbProxy.Text == null || tbProxy.Text.Length <= 0)
				{
					sbMessage.Text = "proxy host is a required field when proxy is enabled";
					return false;
				}

				// proxy port

				if (tbProxyPort.Text == null || tbProxyPort.Text.Length <= 0)
				{
					sbMessage.Text = "proxy port is a required field when proxy is enabled";
					return false;
				}

				// proxy port [decimal]

				bool badProxyPort = false;

				try
				{
					uint n = UInt32.Parse(tbProxyPort.Text);
					if (n == 0) badProxyPort = true;
				}

				catch
				{
					badProxyPort = true;
				}

				if (badProxyPort)
				{
					sbMessage.Text = "proxy port must be > 0";
					return false;
				}
			}

			return true;
		}

		// -------------------------------------------------------------------------
		public Connection()
		{
			InitializeComponent();

			LoadConfig();

			btnConnect.Select();
		}

		/// <summary>
		/// load a config file and set appropritate values for site
		/// </summary>
		private void LoadConfig()
		{
			// set default values for site

			this.Host = null;
			this.Port = 22;
			this.Login = "";
			this.Password = "";
			this.Proxy = null;
			this.ProxyEnabled = false;
			this.ProxyPort = 3128;
			this.ProxyLogin = null;
			this.ProxyPassword = null;
			this.ProxyType = ProxyType.Socks4;
			this.LogLevel = Rebex.LogLevel.Info;

			// read config file

			XmlDocument xml = new XmlDocument();
			if (!File.Exists(ConfigFile))
				return;

			xml.Load(ConfigFile);

			this.Proxy = null;

			XmlElement config = xml["configuration"];
			foreach (XmlNode key in config.ChildNodes)
			{
				string item = null;
				string name = null;
				if (key.Attributes["value"] != null)
					item = key.Attributes["value"].Value;
				if (key.Attributes["name"] != null)
					name = key.Attributes["name"].Value;
				if (key["value"] != null)
					item = key["value"].InnerText;
				if (key["name"] != null)
					name = key["name"].InnerText;

				if (name == null)
					continue;
				if (item == null)
					item = "";

				switch (name)
				{
					case "host": this.Host = item; break;
					case "login": this.Login = item; break;
					case "port":
						try
						{
							this.Port = Int32.Parse(item);
						}
						catch
						{
							this.Port = 22; // default
						}
						break;
					case "password": this.Password = item; break;
					case "largebuffers": this.UseLargeBuffers = (item.ToLower() == "false" ? false : true); break;
					case "proxy": this.Proxy = item; break;
					case "proxylogin": this.ProxyLogin = item; break;
					case "proxypassword": this.ProxyPassword = item; break;
					case "proxyenabled": this.ProxyEnabled = (item.ToLower() == "false" ? false : true); break;
					case "proxyport":
						try
						{
							this.ProxyPort = Int32.Parse(item);
						}
						catch
						{
							this.ProxyPort = 3128; // default
						}
						break;
					case "proxytype":
						try
						{
							this.ProxyType = (ProxyType)Enum.Parse(typeof(ProxyType), item);
						}
						catch
						{
							this.ProxyType = ProxyType.Socks4; // default
						}
						break;
					case "loglevel":
						try
						{
							this.LogLevel = (Rebex.LogLevel)Enum.Parse(typeof(Rebex.LogLevel), item);
						}
						catch
						{
							this.LogLevel = Rebex.LogLevel.Info; // default
						}
						break;
					case "privatekey": this.PrivateKey = item; break;
				}
			}
		}

		private static void AddKey(XmlDocument xml, XmlElement config, string name, string val)
		{
			XmlElement key = xml.CreateElement("key");
			config.AppendChild(key);
			XmlAttribute atrName = xml.CreateAttribute("name");
			atrName.Value = name;
			XmlAttribute atrVal = xml.CreateAttribute("value");
			atrVal.Value = val;
			key.Attributes.Append(atrName);
			key.Attributes.Append(atrVal);
		}

		/// <summary>
		/// save site values into config file
		/// </summary>
		private void SaveConfig()
		{
			XmlDocument xml = new XmlDocument();
			XmlElement config = xml.CreateElement("configuration");
			xml.AppendChild(config);

			AddKey(xml, config, "host", this.Host);
			AddKey(xml, config, "login", this.Login);
			AddKey(xml, config, "port", this.Port.ToString());
			AddKey(xml, config, "password", this.Password);
			AddKey(xml, config, "largebuffers", this.UseLargeBuffers.ToString());
			AddKey(xml, config, "proxyenabled", this.ProxyEnabled.ToString());
			AddKey(xml, config, "proxy", this.Proxy);
			AddKey(xml, config, "proxyport", this.ProxyPort.ToString());
			AddKey(xml, config, "proxytype", this.ProxyType.ToString());
			AddKey(xml, config, "proxylogin", this.ProxyLogin);
			AddKey(xml, config, "proxypassword", this.ProxyPassword);
			AddKey(xml, config, "privatekey", this.PrivateKey);
			AddKey(xml, config, "loglevel", this.LogLevel.ToString());

			string configPath = Path.GetDirectoryName(ConfigFile);
			if (!Directory.Exists(configPath))
				Directory.CreateDirectory(configPath);
			xml.Save(ConfigFile);
		}

		// -------------------------------------------------------------------------
		/// <summary>
		/// clean up any resources being used
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}

			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.cbUseLargeBuffers = new System.Windows.Forms.CheckBox();
			this.label13 = new System.Windows.Forms.Label();
			this.btnPrivateKey = new System.Windows.Forms.Button();
			this.tbPrivateKey = new System.Windows.Forms.TextBox();
			this.label11 = new System.Windows.Forms.Label();
			this.tbPassword = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.tbLogin = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.tbPort = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.tbHost = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.label10 = new System.Windows.Forms.Label();
			this.tbProxyPassword = new System.Windows.Forms.TextBox();
			this.label9 = new System.Windows.Forms.Label();
			this.tbProxyLogin = new System.Windows.Forms.TextBox();
			this.label8 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.cbProxyType = new System.Windows.Forms.ComboBox();
			this.tbProxyPort = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.cbProxy = new System.Windows.Forms.CheckBox();
			this.tbProxy = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.btnConnect = new System.Windows.Forms.Button();
			this.sbMessage = new System.Windows.Forms.StatusBar();
			this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
			this.cbLogLevel = new System.Windows.Forms.ComboBox();
			this.label12 = new System.Windows.Forms.Label();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.cbUseLargeBuffers);
			this.groupBox1.Controls.Add(this.label13);
			this.groupBox1.Controls.Add(this.btnPrivateKey);
			this.groupBox1.Controls.Add(this.tbPrivateKey);
			this.groupBox1.Controls.Add(this.label11);
			this.groupBox1.Controls.Add(this.tbPassword);
			this.groupBox1.Controls.Add(this.label4);
			this.groupBox1.Controls.Add(this.tbLogin);
			this.groupBox1.Controls.Add(this.label3);
			this.groupBox1.Controls.Add(this.tbPort);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.tbHost);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Location = new System.Drawing.Point(8, 8);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(400, 154);
			this.groupBox1.TabIndex = 18;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "SFTP settings";
			// 
			// cbUseLargeBuffers
			// 
			this.cbUseLargeBuffers.AutoSize = true;
			this.cbUseLargeBuffers.Location = new System.Drawing.Point(11, 74);
			this.cbUseLargeBuffers.Name = "cbUseLargeBuffers";
			this.cbUseLargeBuffers.Size = new System.Drawing.Size(141, 17);
			this.cbUseLargeBuffers.TabIndex = 32;
			this.cbUseLargeBuffers.Text = "Use large buffers (faster)";
			this.cbUseLargeBuffers.UseVisualStyleBackColor = true;
			// 
			// label13
			// 
			this.label13.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
			this.label13.Location = new System.Drawing.Point(8, 94);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(344, 32);
			this.label13.TabIndex = 31;
			this.label13.Text = "Fill the \"Private key\" field only when you want to authenticate using private key" +
				".";
			// 
			// btnPrivateKey
			// 
			this.btnPrivateKey.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnPrivateKey.Location = new System.Drawing.Point(365, 126);
			this.btnPrivateKey.Name = "btnPrivateKey";
			this.btnPrivateKey.Size = new System.Drawing.Size(22, 20);
			this.btnPrivateKey.TabIndex = 6;
			this.btnPrivateKey.Text = "...";
			this.btnPrivateKey.Click += new System.EventHandler(this.btnPrivateKey_Click);
			// 
			// tbPrivateKey
			// 
			this.tbPrivateKey.Location = new System.Drawing.Point(104, 126);
			this.tbPrivateKey.Name = "tbPrivateKey";
			this.tbPrivateKey.Size = new System.Drawing.Size(258, 20);
			this.tbPrivateKey.TabIndex = 5;
			// 
			// label11
			// 
			this.label11.Location = new System.Drawing.Point(8, 126);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(100, 23);
			this.label11.TabIndex = 18;
			this.label11.Text = "Private key:";
			// 
			// tbPassword
			// 
			this.tbPassword.Location = new System.Drawing.Point(288, 48);
			this.tbPassword.Name = "tbPassword";
			this.tbPassword.PasswordChar = '*';
			this.tbPassword.Size = new System.Drawing.Size(100, 20);
			this.tbPassword.TabIndex = 4;
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(224, 48);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(64, 23);
			this.label4.TabIndex = 16;
			this.label4.Text = "Password:";
			// 
			// tbLogin
			// 
			this.tbLogin.Location = new System.Drawing.Point(104, 48);
			this.tbLogin.Name = "tbLogin";
			this.tbLogin.Size = new System.Drawing.Size(100, 20);
			this.tbLogin.TabIndex = 3;
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(8, 48);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(96, 23);
			this.label3.TabIndex = 14;
			this.label3.Text = "Login name:";
			// 
			// tbPort
			// 
			this.tbPort.Location = new System.Drawing.Point(288, 24);
			this.tbPort.Name = "tbPort";
			this.tbPort.Size = new System.Drawing.Size(64, 20);
			this.tbPort.TabIndex = 2;
			this.tbPort.Text = "22";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(224, 24);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(64, 23);
			this.label2.TabIndex = 12;
			this.label2.Text = "Port:";
			// 
			// tbHost
			// 
			this.tbHost.Location = new System.Drawing.Point(104, 24);
			this.tbHost.Name = "tbHost";
			this.tbHost.Size = new System.Drawing.Size(100, 20);
			this.tbHost.TabIndex = 1;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 24);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(96, 23);
			this.label1.TabIndex = 10;
			this.label1.Text = "Host name:";
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.label10);
			this.groupBox2.Controls.Add(this.tbProxyPassword);
			this.groupBox2.Controls.Add(this.label9);
			this.groupBox2.Controls.Add(this.tbProxyLogin);
			this.groupBox2.Controls.Add(this.label8);
			this.groupBox2.Controls.Add(this.label7);
			this.groupBox2.Controls.Add(this.cbProxyType);
			this.groupBox2.Controls.Add(this.tbProxyPort);
			this.groupBox2.Controls.Add(this.label6);
			this.groupBox2.Controls.Add(this.cbProxy);
			this.groupBox2.Controls.Add(this.tbProxy);
			this.groupBox2.Controls.Add(this.label5);
			this.groupBox2.Location = new System.Drawing.Point(8, 168);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(400, 128);
			this.groupBox2.TabIndex = 19;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Proxy settings";
			// 
			// label10
			// 
			this.label10.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.label10.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
			this.label10.Location = new System.Drawing.Point(224, 72);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(160, 48);
			this.label10.TabIndex = 30;
			this.label10.Text = "Leave login/password fields empty when not needed.";
			// 
			// tbProxyPassword
			// 
			this.tbProxyPassword.Location = new System.Drawing.Point(104, 96);
			this.tbProxyPassword.Name = "tbProxyPassword";
			this.tbProxyPassword.PasswordChar = '*';
			this.tbProxyPassword.Size = new System.Drawing.Size(104, 20);
			this.tbProxyPassword.TabIndex = 24;
			// 
			// label9
			// 
			this.label9.Location = new System.Drawing.Point(16, 96);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(88, 23);
			this.label9.TabIndex = 28;
			this.label9.Text = "Password:";
			// 
			// tbProxyLogin
			// 
			this.tbProxyLogin.Location = new System.Drawing.Point(104, 72);
			this.tbProxyLogin.Name = "tbProxyLogin";
			this.tbProxyLogin.Size = new System.Drawing.Size(104, 20);
			this.tbProxyLogin.TabIndex = 23;
			// 
			// label8
			// 
			this.label8.Location = new System.Drawing.Point(16, 72);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(88, 23);
			this.label8.TabIndex = 26;
			this.label8.Text = "Login name:";
			// 
			// label7
			// 
			this.label7.Location = new System.Drawing.Point(16, 48);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(88, 23);
			this.label7.TabIndex = 25;
			this.label7.Text = "Proxy type:";
			// 
			// cbProxyType
			// 
			this.cbProxyType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbProxyType.Items.AddRange(new object[] {
            "Socks4",
            "Socks4a",
            "Socks5",
            "HttpConnect"});
			this.cbProxyType.Location = new System.Drawing.Point(104, 48);
			this.cbProxyType.Name = "cbProxyType";
			this.cbProxyType.Size = new System.Drawing.Size(104, 21);
			this.cbProxyType.TabIndex = 22;
			// 
			// tbProxyPort
			// 
			this.tbProxyPort.Location = new System.Drawing.Point(288, 24);
			this.tbProxyPort.Name = "tbProxyPort";
			this.tbProxyPort.Size = new System.Drawing.Size(64, 20);
			this.tbProxyPort.TabIndex = 21;
			this.tbProxyPort.Text = "22";
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(224, 24);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(56, 23);
			this.label6.TabIndex = 22;
			this.label6.Text = "Port:";
			// 
			// cbProxy
			// 
			this.cbProxy.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.cbProxy.Location = new System.Drawing.Point(288, 48);
			this.cbProxy.Name = "cbProxy";
			this.cbProxy.Size = new System.Drawing.Size(88, 24);
			this.cbProxy.TabIndex = 9;
			this.cbProxy.Text = "Use proxy";
			this.cbProxy.Click += new System.EventHandler(this.cbProxy_Click);
			// 
			// tbProxy
			// 
			this.tbProxy.Location = new System.Drawing.Point(104, 24);
			this.tbProxy.Name = "tbProxy";
			this.tbProxy.Size = new System.Drawing.Size(104, 20);
			this.tbProxy.TabIndex = 20;
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(16, 24);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(88, 23);
			this.label5.TabIndex = 19;
			this.label5.Text = "Proxy:";
			// 
			// btnConnect
			// 
			this.btnConnect.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnConnect.Location = new System.Drawing.Point(312, 299);
			this.btnConnect.Name = "btnConnect";
			this.btnConnect.Size = new System.Drawing.Size(88, 23);
			this.btnConnect.TabIndex = 8;
			this.btnConnect.Text = "Connect";
			this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
			// 
			// sbMessage
			// 
			this.sbMessage.Location = new System.Drawing.Point(0, 326);
			this.sbMessage.Name = "sbMessage";
			this.sbMessage.Size = new System.Drawing.Size(418, 22);
			this.sbMessage.TabIndex = 22;
			// 
			// cbLogLevel
			// 
			this.cbLogLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbLogLevel.Items.AddRange(new object[] {
            "Info",
            "Debug",
            "Verbose",
            "Errors only"});
			this.cbLogLevel.Location = new System.Drawing.Point(104, 299);
			this.cbLogLevel.Name = "cbLogLevel";
			this.cbLogLevel.Size = new System.Drawing.Size(104, 21);
			this.cbLogLevel.TabIndex = 7;
			// 
			// label12
			// 
			this.label12.Location = new System.Drawing.Point(16, 299);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(88, 23);
			this.label12.TabIndex = 24;
			this.label12.Text = "Logging level";
			// 
			// Connection
			// 
			this.AcceptButton = this.btnConnect;
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(418, 348);
			this.Controls.Add(this.label12);
			this.Controls.Add(this.cbLogLevel);
			this.Controls.Add(this.sbMessage);
			this.Controls.Add(this.btnConnect);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.KeyPreview = true;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "Connection";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Connection";
			this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Connection_KeyPress);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.ResumeLayout(false);

		}
		#endregion

		// -------------------------------------------------------------------------
		/// <summary>
		/// form confirmation by clicking 'ok' button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnConnect_Click(object sender, System.EventArgs e)
		{
			if (ValidateForm())
			{
				_ok = true;
				SaveConfig();
				this.Close();
			}
		}

		// -------------------------------------------------------------------------
		/// <summary>
		/// proxy enable/disable click
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void cbProxy_Click(object sender, System.EventArgs e)
		{
			System.Windows.Forms.CheckBox cb = (System.Windows.Forms.CheckBox)sender;

			if (cb.Checked)
			{
				tbProxy.Enabled = true;
				tbProxyPort.Enabled = true;
				cbProxy.Checked = true;
				cbProxyType.Enabled = true;
				tbProxyLogin.Enabled = true;
				tbProxyPassword.Enabled = true;
			}
			else
			{
				tbProxy.Enabled = false;
				tbProxyPort.Enabled = false;
				cbProxy.Checked = false;
				cbProxyType.Enabled = false;
				tbProxyLogin.Enabled = false;
				tbProxyPassword.Enabled = false;
			}
		}

		// -------------------------------------------------------------------------
		/// <summary>
		/// global key handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Connection_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			// when 'enter' is pressed the form is closed

			if (e.KeyChar == '\r')
			{
				if (ValidateForm())
				{
					_ok = true;
					this.Close();
				}
			}

			// when 'esc' is pressed the form is closed

			if ((int)(e.KeyChar) == Convert.ToChar(Keys.Escape))
			{
				this.Close();
			}
		}

		private void btnPrivateKey_Click(object sender, System.EventArgs e)
		{
			if (openFileDialog.ShowDialog() == DialogResult.OK)
			{
				tbPrivateKey.Text = openFileDialog.FileName;
			}
		}
	}
}