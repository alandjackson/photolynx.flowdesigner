'  
'   Rebex Sample Code License
' 
'   Copyright (c) 2009, Rebex CR s.r.o. www.rebex.net, 
'   All rights reserved.
' 
'   Permission to use, copy, modify, and/or distribute this software for any
'   purpose with or without fee is hereby granted
' 
'   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
'   EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
'   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
'   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
'   HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
'   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
'   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
'   OTHER DEALINGS IN THE SOFTWARE.
' 


Imports System
Imports System.Drawing
Imports System.Collections
Imports System.ComponentModel
Imports System.Windows.Forms

' -------------------------------------------------------------------------
'' <summary>
'' remove (delete) file dialog
'' </summary>
Public Class DeleteDialog
    Inherits System.Windows.Forms.Form

    Public Enum DialogPrompt
        DeleteFile
        DeleteDirectory
        DeleteNonEmptyDirectory
    End Enum

    Private WithEvents btnCancel As System.Windows.Forms.Button
    Private WithEvents btnOK As System.Windows.Forms.Button
    Private lblMsg As System.Windows.Forms.Label
    Private components As System.ComponentModel.Container = Nothing

    Private _msgFormat As String ' message format
    Private _strPath As String ' file path
    Private _bOK As Boolean ' true if form was confirmed; false otherwise

    Public Property Path() As String
        Get
            Return _strPath
        End Get
        Set(ByVal Value As String)
            _strPath = Value
            If Not (_strPath Is Nothing) Then
                lblMsg.Text = String.Format(_msgFormat, _strPath)
            End If
        End Set
    End Property


    Public ReadOnly Property OK() As Boolean
        Get
            Return _bOK
        End Get
    End Property

    ' -------------------------------------------------------------------------
    Public Sub New(ByVal prompt As DialogPrompt)
        InitializeComponent()

        Select Case prompt
            Case DialogPrompt.DeleteFile : _msgFormat = "Delete file '{0}'?"
            Case DialogPrompt.DeleteDirectory : _msgFormat = "Delete directory '{0}'?"
            Case DialogPrompt.DeleteNonEmptyDirectory : _msgFormat = "Directory '{0}' is not empty. Delete anyway?"
            Case Else : Throw New ArgumentException("Invalid DialogPrompt value.", "prompt")
        End Select
    End Sub

#Region "Windows Form Designer generated code"
    ' -------------------------------------------------------------------------
    '' <summary>
    '' Required method for Designer support - do not modify
    '' the contents of this method with the code editor.
    '' </summary>
    Private Sub InitializeComponent()
        Me.btnCancel = New System.Windows.Forms.Button
        Me.btnOK = New System.Windows.Forms.Button
        Me.lblMsg = New System.Windows.Forms.Label
        Me.SuspendLayout()
        ' 
        ' btnCancel
        ' 
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnCancel.Location = New System.Drawing.Point(264, 40)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.TabIndex = 5
        Me.btnCancel.Text = "Cancel"
        ' 
        ' btnOK
        ' 
        Me.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnOK.Location = New System.Drawing.Point(184, 40)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.TabIndex = 4
        Me.btnOK.Text = "OK"
        ' 
        ' lblMsg
        ' 
        Me.lblMsg.Location = New System.Drawing.Point(16, 8)
        Me.lblMsg.Name = "lblMsg"
        Me.lblMsg.Size = New System.Drawing.Size(320, 23)
        Me.lblMsg.TabIndex = 3
        ' 
        ' RemoveFile
        ' 
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(346, 72)
        Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.btnCancel, Me.btnOK, Me.lblMsg})
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.KeyPreview = True
        Me.Name = "RemoveFile"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Delete file"
        Me.ResumeLayout(False)
    End Sub

    ' -------------------------------------------------------------------------
    '' <summary>
    '' clean up any resources being used
    '' </summary>
    Protected Overloads Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub
#End Region


    ' -------------------------------------------------------------------------
    '' <summary>
    '' form confirmation click
    '' </summary>
    '' <param name="sender"></param>
    '' <param name="e"></param>
    Private Sub btnOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOK.Click
        _bOK = True
        Me.Close()
    End Sub


    ' -------------------------------------------------------------------------
    '' <summary>
    '' form cancellation click
    '' </summary>
    '' <param name="sender"></param>
    '' <param name="e"></param>
    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        _bOK = False
        Me.Close()
    End Sub

    ' -------------------------------------------------------------------------
    '' <summary>
    '' global key handler
    '' </summary>
    '' <param name="sender"></param>
    '' <param name="e"></param>
    Private Sub RemoveFile_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        ' when 'enter' is pressed the form is closed
        If e.KeyChar = ControlChars.Cr Then
            _bOK = True
            Me.Close()
        End If

        ' when 'esc' is pressed the form is closed
        If e.KeyChar = Convert.ToChar(Keys.Escape) Then
            Me.Close()
        End If
    End Sub
End Class