'  
'   Rebex Sample Code License
' 
'   Copyright (c) 2009, Rebex CR s.r.o. www.rebex.net, 
'   All rights reserved.
' 
'   Permission to use, copy, modify, and/or distribute this software for any
'   purpose with or without fee is hereby granted
' 
'   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
'   EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
'   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
'   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
'   HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
'   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
'   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
'   OTHER DEALINGS IN THE SOFTWARE.
' 


Imports System
Imports System.Drawing
Imports System.Collections
Imports System.ComponentModel
Imports System.Windows.Forms

' -------------------------------------------------------------------------
'' <summary>
'' make dir dialog
'' </summary>
Public Class MakeDir
    Inherits System.Windows.Forms.Form
    Private tbDir As System.Windows.Forms.TextBox
    Private WithEvents btnOK As System.Windows.Forms.Button
    Private WithEvents btnCancel As System.Windows.Forms.Button
    Private components As System.ComponentModel.Container = Nothing

    Private m_strDirectory As String = Nothing ' directory name

    Public Property Directory() As String
        Get
            Return m_strDirectory
        End Get
        Set(ByVal Value As String)
            m_strDirectory = Value
        End Set
    End Property

#Region "Windows Form Designer generated code"
    ' -------------------------------------------------------------------------
    '' <summary>
    '' Required method for Designer support - do not modify
    '' the contents of this method with the code editor.
    '' </summary>
    Private Sub InitializeComponent()
        Me.tbDir = New System.Windows.Forms.TextBox()
        Me.btnOK = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        ' 
        ' tbDir
        ' 
        Me.tbDir.Location = New System.Drawing.Point(16, 16)
        Me.tbDir.Name = "tbDir"
        Me.tbDir.Size = New System.Drawing.Size(176, 20)
        Me.tbDir.TabIndex = 0
        Me.tbDir.Text = ""
        ' 
        ' btnOK
        ' 
        Me.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnOK.Location = New System.Drawing.Point(200, 16)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.TabIndex = 1
        Me.btnOK.Text = "OK"
        ' 
        ' btnCancel
        ' 
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnCancel.Location = New System.Drawing.Point(280, 16)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.TabIndex = 2
        Me.btnCancel.Text = "Cancel"
        ' 
        ' MakeDir
        ' 
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(368, 56)
        Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.btnCancel, Me.btnOK, Me.tbDir})
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.KeyPreview = True
        Me.Name = "MakeDir"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Create folder"
        Me.ResumeLayout(False)
    End Sub
    ' -------------------------------------------------------------------------
    '' <summary>
    '' constructor
    '' </summary>
    Public Sub New()
        InitializeComponent()
    End Sub
    ' -------------------------------------------------------------------------
    '' <summary>
    '' clean up any resources being used
    '' </summary>
    Protected Overloads Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub
#End Region


    ' -------------------------------------------------------------------------
    '' <summary>
    '' form confirmation by clicking 'ok' button
    '' </summary>
    '' <param name="sender"></param>
    '' <param name="e"></param>
    Private Sub btnOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOK.Click
        m_strDirectory = tbDir.Text
        Me.Close()
    End Sub


    ' -------------------------------------------------------------------------
    '' <summary>
    '' form cancellation
    '' </summary>
    '' <param name="sender"></param>
    '' <param name="e"></param>
    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub


    ' -------------------------------------------------------------------------
    '' <summary>
    '' global key handler
    '' </summary>
    '' <param name="sender"></param>
    '' <param name="e"></param>
    Private Sub MakeDir_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        ' when 'enter' is pressed the form is cinfirmed
        If e.KeyChar = ControlChars.Cr Then
            m_strDirectory = tbDir.Text
            Me.Close()
        End If

        ' when 'esc' is pressed the form is closed
        If e.KeyChar = Convert.ToChar(Keys.Escape) Then
            Me.Close()
        End If
    End Sub
End Class