'  
'   Rebex Sample Code License
' 
'   Copyright (c) 2009, Rebex CR s.r.o. www.rebex.net, 
'   All rights reserved.
' 
'   Permission to use, copy, modify, and/or distribute this software for any
'   purpose with or without fee is hereby granted
' 
'   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
'   EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
'   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
'   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
'   HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
'   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
'   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
'   OTHER DEALINGS IN THE SOFTWARE.
' 


Imports System
Imports System.Drawing
Imports System.Collections
Imports System.ComponentModel
Imports System.Windows.Forms
Imports System.Xml
Imports System.IO
Imports Rebex.Net
Imports Rebex

' -------------------------------------------------------------------------
'' <summary>
'' connection dialog
'' </summary>
Public Class Connection
    Inherits System.Windows.Forms.Form
    Private components As System.ComponentModel.Container = Nothing
    Private groupBox1 As System.Windows.Forms.GroupBox
    Private tbPassword As System.Windows.Forms.TextBox
    Private label4 As System.Windows.Forms.Label
    Private tbLogin As System.Windows.Forms.TextBox
    Private label3 As System.Windows.Forms.Label
    Private tbPort As System.Windows.Forms.TextBox
    Private label2 As System.Windows.Forms.Label
    Private tbHost As System.Windows.Forms.TextBox
    Private label1 As System.Windows.Forms.Label
    Private groupBox2 As System.Windows.Forms.GroupBox
    Private label7 As System.Windows.Forms.Label
    Private cbProxyType As System.Windows.Forms.ComboBox
    Private tbProxyPort As System.Windows.Forms.TextBox
    Private label6 As System.Windows.Forms.Label
    Private WithEvents cbProxy As System.Windows.Forms.CheckBox
    Private tbProxy As System.Windows.Forms.TextBox
    Private label5 As System.Windows.Forms.Label
    Private WithEvents btnConnect As System.Windows.Forms.Button
    Private tbProxyLogin As System.Windows.Forms.TextBox
    Private label8 As System.Windows.Forms.Label
    Private tbProxyPassword As System.Windows.Forms.TextBox
    Private label9 As System.Windows.Forms.Label
    Private label10 As System.Windows.Forms.Label
    Private sbMessage As System.Windows.Forms.StatusBar

    ' CONSTANTS
    Public Shared ReadOnly ConfigFile As String = Path.Combine(Environment.GetFolderPath(System.Environment.SpecialFolder.LocalApplicationData), "Rebex\SFTP\WinFormClient.xml")
    Friend WithEvents cbUseLargeBuffers As System.Windows.Forms.CheckBox

    Public _ok As Boolean = False ' form was confirmed

    Public ReadOnly Property OK() As Boolean
        Get
            Return _ok
        End Get
    End Property

    ' host name   
    Public Property Host() As String
        Get
            Return tbHost.Text
        End Get
        Set(ByVal Value As String)
            tbHost.Text = Value
        End Set
    End Property

    ' port of the host
    Public Property Port() As Integer
        Get
            Dim temp As Integer

            Try
                temp = Int32.Parse(tbPort.Text)

            Catch
                Return 0 ' def. value if error occures
            End Try

            Return temp
        End Get
        Set(ByVal Value As Integer)
            tbPort.Text = Value.ToString()
        End Set
    End Property

    ' use large buffers
    Public Property UseLargeBuffers() As Boolean
        Get
            Return cbUseLargeBuffers.Checked
        End Get
        Set(ByVal Value As Boolean)
            cbUseLargeBuffers.Checked = Value
        End Set
    End Property


    ' proxy server
    Public Property Proxy() As String
        Get
            Return tbProxy.Text
        End Get
        Set(ByVal Value As String)
            tbProxy.Text = Value
        End Set
    End Property

    ' proxy login name   
    Public Property ProxyLogin() As String
        Get
            Return tbProxyLogin.Text
        End Get
        Set(ByVal Value As String)
            tbProxyLogin.Text = Value
        End Set
    End Property

    ' proxy password
    Public Property ProxyPassword() As String
        Get
            Return tbProxyPassword.Text
        End Get
        Set(ByVal Value As String)
            tbProxyPassword.Text = Value
        End Set
    End Property

    ' is proxy enabled?   
    Public Property ProxyEnabled() As Boolean

        Get
            Return cbProxy.Checked
        End Get
        Set(ByVal Value As Boolean)
            If Value Then
                tbProxy.Enabled = True
                tbProxyPort.Enabled = True
                cbProxy.Checked = True
                cbProxyType.Enabled = True
                tbProxyLogin.Enabled = True
                tbProxyPassword.Enabled = True
            Else
                tbProxy.Enabled = False
                tbProxyPort.Enabled = False
                cbProxy.Checked = False
                cbProxyType.Enabled = False
                tbProxyLogin.Enabled = False
                tbProxyPassword.Enabled = False
            End If
        End Set
    End Property

    ' proxy port
    Public Property ProxyPort() As Integer
        Get
            Dim temp As Integer

            Try
                temp = Int32.Parse(tbProxyPort.Text)

            Catch
                Return 0 ' def. value if error occures
            End Try

            Return temp
        End Get
        Set(ByVal Value As Integer)
            tbProxyPort.Text = Value.ToString()
        End Set
    End Property

    ' login name
    Public Property Login() As String
        Get
            Return tbLogin.Text
        End Get

        Set(ByVal Value As String)
            tbLogin.Text = Value
        End Set
    End Property

    ' password
    Public Property Password() As String
        Get
            Return tbPassword.Text
        End Get

        Set(ByVal Value As String)
            tbPassword.Text = Value
        End Set
    End Property

    ' private key
    Public Property PrivateKey() As String
        Get
            Return tbPrivateKey.Text
        End Get
        Set(ByVal Value As String)
            tbPrivateKey.Text = Value
        End Set
    End Property

    ' proxy type
    Public Property ProxyType() As ProxyType
        Get
            Select Case cbProxyType.SelectedIndex
                Case 1
                    Return ProxyType.Socks4a
                Case 2
                    Return ProxyType.Socks5
                Case 3
                    Return ProxyType.HttpConnect
                Case Else
                    Return ProxyType.Socks4
            End Select
        End Get
        Set(ByVal Value As ProxyType)
            Select Case Value
                Case ProxyType.Socks4a
                    cbProxyType.SelectedIndex = 1
                Case ProxyType.Socks5
                    cbProxyType.SelectedIndex = 2
                Case ProxyType.HttpConnect
                    cbProxyType.SelectedIndex = 3
                Case Else
                    cbProxyType.SelectedIndex = 0
            End Select
        End Set
    End Property

    ' log level
    Public Property LogLevel() As LogLevel
        Get
            Select Case cbLogLevel.SelectedIndex
                Case 1
                    Return LogLevel.Debug
                Case 2
                    Return LogLevel.Verbose
                Case 3
                    Return LogLevel.Error
                Case Else
                    Return LogLevel.Info
            End Select
        End Get
        Set(ByVal Value As LogLevel)
            Select Case Value
                Case LogLevel.Debug
                    cbLogLevel.SelectedIndex = 1
                Case LogLevel.Verbose
                    cbLogLevel.SelectedIndex = 2
                Case LogLevel.Error
                    cbLogLevel.SelectedIndex = 3
                Case Else
                    cbLogLevel.SelectedIndex = 0
            End Select
        End Set
    End Property


    ' -------------------------------------------------------------------------
    '' <summary>
    '' validity check
    '' </summary>
    '' <returns>true (everything is ok)</returns>
    Public Function ValidateForm() As Boolean
        ' sftp host
        If tbHost.Text Is Nothing OrElse tbHost.Text.Length <= 0 Then
            sbMessage.Text = "sftp host is a required field"
            Return False
        End If

        ' sftp port
        If tbPort.Text Is Nothing OrElse tbPort.Text.Length <= 0 Then
            sbMessage.Text = "sftp port is a required field"
            Return False
        End If

        ' sftp port [decimal]
        Dim badSftpPort As Boolean = False

        Try
            Dim n As Integer
            n = Integer.Parse(tbPort.Text)
            If n = 0 Then
                badSftpPort = True
            End If

        Catch
            badSftpPort = True
        End Try

        If badSftpPort Then
            sbMessage.Text = "sftp port must be > 0"
            Return False
        End If

        ' sftp login
        If tbLogin.Text Is Nothing OrElse tbLogin.Text.Length <= 0 Then
            sbMessage.Text = "sftp login is a required field"
            Return False
        End If

        ' sftp password
        If (tbPassword.Text Is Nothing OrElse tbPassword.Text.Length <= 0) AndAlso tbPrivateKey.Text.Trim() = "" Then
            sbMessage.Text = "sftp password is a required field"
            Return False
        End If

        ' ----------------
        ' proxy check-outs
        ' ----------------
        If cbProxy.Checked Then
            ' proxy host
            If tbProxy.Text Is Nothing OrElse tbProxy.Text.Length <= 0 Then
                sbMessage.Text = "proxy host is a required field when proxy is enabled"
                Return False
            End If

            ' proxy port
            If tbProxyPort.Text Is Nothing OrElse tbProxyPort.Text.Length <= 0 Then
                sbMessage.Text = "proxy port is a required field when proxy is enabled"
                Return False
            End If

            ' proxy port [decimal]
            Dim badProxyPort As Boolean = False

            Try
                Dim n As Integer = Integer.Parse(tbPort.Text)
                If n = 0 Then
                    badProxyPort = True
                End If

            Catch
                badProxyPort = True
            End Try

            If badProxyPort Then
                sbMessage.Text = "proxy port must be > 0"
                Return False
            End If
        End If

        Return True
    End Function

    ' -------------------------------------------------------------------------
    '' <summary>
    '' load a config file and set appropritate values for site
    '' </summary>
    Private Sub LoadConfig()
        ' set default values for site
        Me.Host = Nothing
        Me.Port = 22
        Me.Login = ""
        Me.Password = ""
        Me.Proxy = Nothing
        Me.ProxyEnabled = False
        Me.ProxyPort = 3128
        Me.ProxyLogin = Nothing
        Me.ProxyPassword = Nothing
        Me.ProxyType = ProxyType.Socks4
        Me.LogLevel = LogLevel.Info

        ' read config file

        Dim xml As New XmlDocument
        If Not File.Exists(ConfigFile) Then
            Exit Sub
        End If

        xml.Load(ConfigFile)

        Me.Proxy = Nothing

        Dim config As XmlElement = xml("configuration")
        Dim key As XmlNode
        For Each key In config.ChildNodes
            Dim item As String = Nothing
            Dim name As String = Nothing
            If Not key.Attributes("value") Is Nothing Then item = key.Attributes("value").Value
            If Not key.Attributes("name") Is Nothing Then name = key.Attributes("name").Value
            If Not key("value") Is Nothing Then item = key("value").InnerText
            If Not key("name") Is Nothing Then name = key("name").InnerText

            If Not name Is Nothing Then
                If item Is Nothing Then item = ""

                Select Case name
                    Case "host"
                        Me.Host = item
                    Case "login"
                        Me.Login = item
                    Case "port"
                        Try
                            Me.Port = Int32.Parse(item)
                        Catch
                            Me.Port = 22                            ' default
                        End Try
                    Case "password"
                        Me.Password = item
                    Case "proxy"
                        Me.Proxy = item
                    Case "largebuffers"
                        Me.UseLargeBuffers = IIf(item.ToLower() = "false", False, True)
                    Case "proxylogin"
                        Me.ProxyLogin = item
                    Case "proxypassword"
                        Me.ProxyPassword = item
                    Case "proxyenabled"
                        Me.ProxyEnabled = IIf(item.ToLower() = "false", False, True)
                    Case "proxyport"
                        Try
                            Me.ProxyPort = Int32.Parse(item)
                        Catch
                            Me.ProxyPort = 3128                            ' default
                        End Try
                    Case "proxytype"
                        Try
                            Me.ProxyType = CType([Enum].Parse(GetType(ProxyType), item), ProxyType)
                        Catch
                            Me.ProxyType = ProxyType.Socks4                            ' default
                        End Try
                    Case "loglevel"
                        Try
                            Me.LogLevel = CType([Enum].Parse(GetType(LogLevel), item), LogLevel)
                        Catch
                            Me.LogLevel = LogLevel.Info                             ' default
                        End Try
                    Case "privatekey"
                        Me.PrivateKey = item
                End Select
            End If
        Next key
    End Sub


    Private Shared Sub AddKey(ByVal xml As XmlDocument, ByVal config As XmlElement, ByVal name As String, ByVal val As String)
        Dim key As XmlElement = xml.CreateElement("key")
        config.AppendChild(key)
        Dim atrName As XmlAttribute = xml.CreateAttribute("name")
        atrName.Value = name
        Dim atrVal As XmlAttribute = xml.CreateAttribute("value")
        atrVal.Value = val
        key.Attributes.Append(atrName)
        key.Attributes.Append(atrVal)
    End Sub

    ' -------------------------------------------------------------------------
    '' <summary>
    '' save site values into config file
    '' </summary>
    Private Sub SaveConfig()
        Dim xml As XmlDocument = New XmlDocument
        Dim config As XmlElement = xml.CreateElement("configuration")
        xml.AppendChild(config)

        AddKey(xml, config, "host", Me.Host)
        AddKey(xml, config, "login", Me.Login)
        AddKey(xml, config, "port", Me.Port.ToString())
        AddKey(xml, config, "password", Me.Password)
        AddKey(xml, config, "largebuffers", Me.UseLargeBuffers.ToString())
        AddKey(xml, config, "proxyenabled", Me.ProxyEnabled.ToString())
        AddKey(xml, config, "proxy", Me.Proxy)
        AddKey(xml, config, "proxyport", Me.ProxyPort.ToString())
        AddKey(xml, config, "proxytype", Me.ProxyType.ToString())
        AddKey(xml, config, "proxylogin", Me.ProxyLogin)
        AddKey(xml, config, "proxypassword", Me.ProxyPassword)
        AddKey(xml, config, "privatekey", Me.PrivateKey)
        AddKey(xml, config, "loglevel", Me.LogLevel.ToString())

        Dim configPath As String = Path.GetDirectoryName(ConfigFile)
        If Not Directory.Exists(configPath) Then
            Directory.CreateDirectory(configPath)
        End If
        xml.Save(ConfigFile)
    End Sub


#Region "Windows Form Designer generated code"

    ' -------------------------------------------------------------------------
    '' <summary>
    '' constructor
    '' </summary>
    Public Sub New()
        InitializeComponent()

        LoadConfig()
    End Sub

    ' -------------------------------------------------------------------------
    '' <summary>
    '' clean up any resources being used
    '' </summary>
    Protected Overloads Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If

        MyBase.Dispose(disposing)
    End Sub

    ' -------------------------------------------------------------------------
    '' <summary>
    '' Required method for Designer support - do not modify
    '' the contents of this method with the code editor.
    '' </summary>
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents tbPrivateKey As System.Windows.Forms.TextBox
    Friend WithEvents btnPrivateKey As System.Windows.Forms.Button
    Friend WithEvents OpenFileDialog As System.Windows.Forms.OpenFileDialog
    Friend WithEvents label12 As System.Windows.Forms.Label
    Friend WithEvents cbLogLevel As System.Windows.Forms.ComboBox
    Friend WithEvents label13 As System.Windows.Forms.Label
    Private Sub InitializeComponent()
        Me.groupBox1 = New System.Windows.Forms.GroupBox
        Me.cbUseLargeBuffers = New System.Windows.Forms.CheckBox
        Me.label13 = New System.Windows.Forms.Label
        Me.btnPrivateKey = New System.Windows.Forms.Button
        Me.tbPrivateKey = New System.Windows.Forms.TextBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.tbPassword = New System.Windows.Forms.TextBox
        Me.label4 = New System.Windows.Forms.Label
        Me.tbLogin = New System.Windows.Forms.TextBox
        Me.label3 = New System.Windows.Forms.Label
        Me.tbPort = New System.Windows.Forms.TextBox
        Me.label2 = New System.Windows.Forms.Label
        Me.tbHost = New System.Windows.Forms.TextBox
        Me.label1 = New System.Windows.Forms.Label
        Me.groupBox2 = New System.Windows.Forms.GroupBox
        Me.label10 = New System.Windows.Forms.Label
        Me.tbProxyPassword = New System.Windows.Forms.TextBox
        Me.label9 = New System.Windows.Forms.Label
        Me.tbProxyLogin = New System.Windows.Forms.TextBox
        Me.label8 = New System.Windows.Forms.Label
        Me.label7 = New System.Windows.Forms.Label
        Me.cbProxyType = New System.Windows.Forms.ComboBox
        Me.tbProxyPort = New System.Windows.Forms.TextBox
        Me.label6 = New System.Windows.Forms.Label
        Me.cbProxy = New System.Windows.Forms.CheckBox
        Me.tbProxy = New System.Windows.Forms.TextBox
        Me.label5 = New System.Windows.Forms.Label
        Me.btnConnect = New System.Windows.Forms.Button
        Me.sbMessage = New System.Windows.Forms.StatusBar
        Me.OpenFileDialog = New System.Windows.Forms.OpenFileDialog
        Me.label12 = New System.Windows.Forms.Label
        Me.cbLogLevel = New System.Windows.Forms.ComboBox
        Me.groupBox1.SuspendLayout()
        Me.groupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'groupBox1
        '
        Me.groupBox1.Controls.Add(Me.cbUseLargeBuffers)
        Me.groupBox1.Controls.Add(Me.label13)
        Me.groupBox1.Controls.Add(Me.btnPrivateKey)
        Me.groupBox1.Controls.Add(Me.tbPrivateKey)
        Me.groupBox1.Controls.Add(Me.Label11)
        Me.groupBox1.Controls.Add(Me.tbPassword)
        Me.groupBox1.Controls.Add(Me.label4)
        Me.groupBox1.Controls.Add(Me.tbLogin)
        Me.groupBox1.Controls.Add(Me.label3)
        Me.groupBox1.Controls.Add(Me.tbPort)
        Me.groupBox1.Controls.Add(Me.label2)
        Me.groupBox1.Controls.Add(Me.tbHost)
        Me.groupBox1.Controls.Add(Me.label1)
        Me.groupBox1.Location = New System.Drawing.Point(8, 8)
        Me.groupBox1.Name = "groupBox1"
        Me.groupBox1.Size = New System.Drawing.Size(400, 155)
        Me.groupBox1.TabIndex = 18
        Me.groupBox1.TabStop = False
        Me.groupBox1.Text = "SFTP settings"
        '
        'cbUseLargeBuffers
        '
        Me.cbUseLargeBuffers.AutoSize = True
        Me.cbUseLargeBuffers.Location = New System.Drawing.Point(11, 74)
        Me.cbUseLargeBuffers.Name = "cbUseLargeBuffers"
        Me.cbUseLargeBuffers.Size = New System.Drawing.Size(141, 17)
        Me.cbUseLargeBuffers.TabIndex = 33
        Me.cbUseLargeBuffers.Text = "Use large buffers (faster)"
        Me.cbUseLargeBuffers.UseVisualStyleBackColor = True
        '
        'label13
        '
        Me.label13.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.label13.Location = New System.Drawing.Point(8, 94)
        Me.label13.Name = "label13"
        Me.label13.Size = New System.Drawing.Size(344, 32)
        Me.label13.TabIndex = 32
        Me.label13.Text = "Fill the ""Private key"" field only when you want to authenticate using private key" & _
            "."
        '
        'btnPrivateKey
        '
        Me.btnPrivateKey.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnPrivateKey.Location = New System.Drawing.Point(365, 126)
        Me.btnPrivateKey.Name = "btnPrivateKey"
        Me.btnPrivateKey.Size = New System.Drawing.Size(22, 20)
        Me.btnPrivateKey.TabIndex = 20
        Me.btnPrivateKey.Text = "..."
        '
        'tbPrivateKey
        '
        Me.tbPrivateKey.Location = New System.Drawing.Point(104, 126)
        Me.tbPrivateKey.Name = "tbPrivateKey"
        Me.tbPrivateKey.Size = New System.Drawing.Size(258, 20)
        Me.tbPrivateKey.TabIndex = 19
        '
        'Label11
        '
        Me.Label11.Location = New System.Drawing.Point(8, 126)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(96, 23)
        Me.Label11.TabIndex = 18
        Me.Label11.Text = "Private key:"
        '
        'tbPassword
        '
        Me.tbPassword.Location = New System.Drawing.Point(288, 48)
        Me.tbPassword.Name = "tbPassword"
        Me.tbPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.tbPassword.Size = New System.Drawing.Size(100, 20)
        Me.tbPassword.TabIndex = 17
        '
        'label4
        '
        Me.label4.Location = New System.Drawing.Point(224, 48)
        Me.label4.Name = "label4"
        Me.label4.Size = New System.Drawing.Size(56, 23)
        Me.label4.TabIndex = 16
        Me.label4.Text = "Password:"
        '
        'tbLogin
        '
        Me.tbLogin.Location = New System.Drawing.Point(104, 48)
        Me.tbLogin.Name = "tbLogin"
        Me.tbLogin.Size = New System.Drawing.Size(100, 20)
        Me.tbLogin.TabIndex = 15
        '
        'label3
        '
        Me.label3.Location = New System.Drawing.Point(8, 48)
        Me.label3.Name = "label3"
        Me.label3.Size = New System.Drawing.Size(96, 23)
        Me.label3.TabIndex = 14
        Me.label3.Text = "Login name:"
        '
        'tbPort
        '
        Me.tbPort.Location = New System.Drawing.Point(288, 24)
        Me.tbPort.Name = "tbPort"
        Me.tbPort.Size = New System.Drawing.Size(64, 20)
        Me.tbPort.TabIndex = 13
        Me.tbPort.Text = "22"
        '
        'label2
        '
        Me.label2.Location = New System.Drawing.Point(224, 24)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(56, 23)
        Me.label2.TabIndex = 12
        Me.label2.Text = "Port:"
        '
        'tbHost
        '
        Me.tbHost.Location = New System.Drawing.Point(104, 24)
        Me.tbHost.Name = "tbHost"
        Me.tbHost.Size = New System.Drawing.Size(100, 20)
        Me.tbHost.TabIndex = 11
        '
        'label1
        '
        Me.label1.Location = New System.Drawing.Point(8, 24)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(96, 23)
        Me.label1.TabIndex = 10
        Me.label1.Text = "Host name:"
        '
        'groupBox2
        '
        Me.groupBox2.Controls.Add(Me.label10)
        Me.groupBox2.Controls.Add(Me.tbProxyPassword)
        Me.groupBox2.Controls.Add(Me.label9)
        Me.groupBox2.Controls.Add(Me.tbProxyLogin)
        Me.groupBox2.Controls.Add(Me.label8)
        Me.groupBox2.Controls.Add(Me.label7)
        Me.groupBox2.Controls.Add(Me.cbProxyType)
        Me.groupBox2.Controls.Add(Me.tbProxyPort)
        Me.groupBox2.Controls.Add(Me.label6)
        Me.groupBox2.Controls.Add(Me.cbProxy)
        Me.groupBox2.Controls.Add(Me.tbProxy)
        Me.groupBox2.Controls.Add(Me.label5)
        Me.groupBox2.Location = New System.Drawing.Point(8, 169)
        Me.groupBox2.Name = "groupBox2"
        Me.groupBox2.Size = New System.Drawing.Size(400, 128)
        Me.groupBox2.TabIndex = 19
        Me.groupBox2.TabStop = False
        Me.groupBox2.Text = "Proxy settings"
        '
        'label10
        '
        Me.label10.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.label10.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.label10.Location = New System.Drawing.Point(224, 72)
        Me.label10.Name = "label10"
        Me.label10.Size = New System.Drawing.Size(160, 48)
        Me.label10.TabIndex = 30
        Me.label10.Text = "Leave login/password fields empty when not needed."
        '
        'tbProxyPassword
        '
        Me.tbProxyPassword.Location = New System.Drawing.Point(104, 96)
        Me.tbProxyPassword.Name = "tbProxyPassword"
        Me.tbProxyPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.tbProxyPassword.Size = New System.Drawing.Size(104, 20)
        Me.tbProxyPassword.TabIndex = 29
        '
        'label9
        '
        Me.label9.Location = New System.Drawing.Point(16, 96)
        Me.label9.Name = "label9"
        Me.label9.Size = New System.Drawing.Size(88, 23)
        Me.label9.TabIndex = 28
        Me.label9.Text = "Password:"
        '
        'tbProxyLogin
        '
        Me.tbProxyLogin.Location = New System.Drawing.Point(104, 72)
        Me.tbProxyLogin.Name = "tbProxyLogin"
        Me.tbProxyLogin.Size = New System.Drawing.Size(104, 20)
        Me.tbProxyLogin.TabIndex = 27
        '
        'label8
        '
        Me.label8.Location = New System.Drawing.Point(16, 72)
        Me.label8.Name = "label8"
        Me.label8.Size = New System.Drawing.Size(88, 23)
        Me.label8.TabIndex = 26
        Me.label8.Text = "Login name:"
        '
        'label7
        '
        Me.label7.Location = New System.Drawing.Point(16, 48)
        Me.label7.Name = "label7"
        Me.label7.Size = New System.Drawing.Size(88, 23)
        Me.label7.TabIndex = 25
        Me.label7.Text = "Proxy type:"
        '
        'cbProxyType
        '
        Me.cbProxyType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbProxyType.Items.AddRange(New Object() {"Socks4", "Socks4a", "Socks5", "HttpConnect"})
        Me.cbProxyType.Location = New System.Drawing.Point(104, 48)
        Me.cbProxyType.Name = "cbProxyType"
        Me.cbProxyType.Size = New System.Drawing.Size(104, 21)
        Me.cbProxyType.TabIndex = 24
        '
        'tbProxyPort
        '
        Me.tbProxyPort.Location = New System.Drawing.Point(288, 24)
        Me.tbProxyPort.Name = "tbProxyPort"
        Me.tbProxyPort.Size = New System.Drawing.Size(64, 20)
        Me.tbProxyPort.TabIndex = 23
        Me.tbProxyPort.Text = "22"
        '
        'label6
        '
        Me.label6.Location = New System.Drawing.Point(224, 24)
        Me.label6.Name = "label6"
        Me.label6.Size = New System.Drawing.Size(56, 23)
        Me.label6.TabIndex = 22
        Me.label6.Text = "Port:"
        '
        'cbProxy
        '
        Me.cbProxy.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.cbProxy.Location = New System.Drawing.Point(288, 48)
        Me.cbProxy.Name = "cbProxy"
        Me.cbProxy.Size = New System.Drawing.Size(88, 24)
        Me.cbProxy.TabIndex = 22
        Me.cbProxy.Text = "Use proxy"
        '
        'tbProxy
        '
        Me.tbProxy.Location = New System.Drawing.Point(104, 24)
        Me.tbProxy.Name = "tbProxy"
        Me.tbProxy.Size = New System.Drawing.Size(104, 20)
        Me.tbProxy.TabIndex = 20
        '
        'label5
        '
        Me.label5.Location = New System.Drawing.Point(16, 24)
        Me.label5.Name = "label5"
        Me.label5.Size = New System.Drawing.Size(88, 23)
        Me.label5.TabIndex = 19
        Me.label5.Text = "Proxy:"
        '
        'btnConnect
        '
        Me.btnConnect.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnConnect.Location = New System.Drawing.Point(312, 300)
        Me.btnConnect.Name = "btnConnect"
        Me.btnConnect.Size = New System.Drawing.Size(88, 23)
        Me.btnConnect.TabIndex = 20
        Me.btnConnect.Text = "Connect"
        '
        'sbMessage
        '
        Me.sbMessage.Location = New System.Drawing.Point(0, 330)
        Me.sbMessage.Name = "sbMessage"
        Me.sbMessage.Size = New System.Drawing.Size(418, 22)
        Me.sbMessage.TabIndex = 22
        '
        'label12
        '
        Me.label12.Location = New System.Drawing.Point(16, 300)
        Me.label12.Name = "label12"
        Me.label12.Size = New System.Drawing.Size(88, 23)
        Me.label12.TabIndex = 26
        Me.label12.Text = "Logging level"
        '
        'cbLogLevel
        '
        Me.cbLogLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbLogLevel.Items.AddRange(New Object() {"Info", "Debug", "Verbose", "Errors only"})
        Me.cbLogLevel.Location = New System.Drawing.Point(104, 300)
        Me.cbLogLevel.Name = "cbLogLevel"
        Me.cbLogLevel.Size = New System.Drawing.Size(104, 21)
        Me.cbLogLevel.TabIndex = 25
        '
        'Connection
        '
        Me.AcceptButton = Me.btnConnect
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(418, 352)
        Me.Controls.Add(Me.label12)
        Me.Controls.Add(Me.cbLogLevel)
        Me.Controls.Add(Me.sbMessage)
        Me.Controls.Add(Me.btnConnect)
        Me.Controls.Add(Me.groupBox2)
        Me.Controls.Add(Me.groupBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Connection"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Connection"
        Me.groupBox1.ResumeLayout(False)
        Me.groupBox1.PerformLayout()
        Me.groupBox2.ResumeLayout(False)
        Me.groupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
#End Region


    ' -------------------------------------------------------------------------
    '' <summary>
    '' form confirmation by clicking 'ok' button
    '' </summary>
    '' <param name="sender"></param>
    '' <param name="e"></param>
    Private Sub btnConnect_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConnect.Click
        If ValidateForm() Then
            _ok = True
            SaveConfig()
            Me.Close()
        End If
    End Sub

    ' -------------------------------------------------------------------------
    '' <summary>
    '' proxy enable/disable click
    '' </summary>
    '' <param name="sender"></param>
    '' <param name="e"></param>
    Private Sub cbProxy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbProxy.Click
        Dim cb As System.Windows.Forms.CheckBox = CType(sender, System.Windows.Forms.CheckBox)

        If cb.Checked Then
            tbProxy.Enabled = True
            tbProxyPort.Enabled = True
            cbProxy.Checked = True
            cbProxyType.Enabled = True
            tbProxyLogin.Enabled = True
            tbProxyPassword.Enabled = True
        Else
            tbProxy.Enabled = False
            tbProxyPort.Enabled = False
            cbProxy.Checked = False
            cbProxyType.Enabled = False
            tbProxyLogin.Enabled = False
            tbProxyPassword.Enabled = False
        End If
    End Sub


    ' -------------------------------------------------------------------------
    '' <summary>
    '' global key handler
    '' </summary>
    '' <param name="sender"></param>
    '' <param name="e"></param>
    Private Sub Connection_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        ' when 'enter' is pressed the form is closed
        If e.KeyChar = ControlChars.Cr Then
            If ValidateForm() Then
                _ok = True
                Me.Close()
            End If
        End If

        ' when 'esc' is pressed the form is closed
        If e.KeyChar = Convert.ToChar(Keys.Escape) Then
            Me.Close()
        End If
    End Sub

    Private Sub btnPrivateKey_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrivateKey.Click
        If OpenFileDialog.ShowDialog() = DialogResult.OK Then
            tbPrivateKey.Text = OpenFileDialog.FileName
        End If
    End Sub
End Class