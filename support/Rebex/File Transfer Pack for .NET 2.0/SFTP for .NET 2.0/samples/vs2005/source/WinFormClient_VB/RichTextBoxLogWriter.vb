'  
'   Rebex Sample Code License
' 
'   Copyright (c) 2009, Rebex CR s.r.o. www.rebex.net, 
'   All rights reserved.
' 
'   Permission to use, copy, modify, and/or distribute this software for any
'   purpose with or without fee is hereby granted
' 
'   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
'   EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
'   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
'   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
'   HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
'   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
'   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
'   OTHER DEALINGS IN THE SOFTWARE.
' 

Imports System
Imports System.Drawing
Imports System.Windows.Forms
Imports Rebex


''' <summary>
''' Implementation of Rebex.ILogWriter which logs to specified RichTextBox.
''' </summary>
Public Class RichTextBoxLogWriter
    Inherits LogWriterBase

    ' colors
    Friend Shared COLORRESPONSE As Color = Color.Black    ' SFTP response color
    Friend Shared COLORCOMMAND As Color = Color.DarkGreen ' SFTP command color
    Friend Shared COLORERROR As Color = Color.Red         ' color of error messages
    Friend Shared COLORINFO As Color = Color.Blue         ' info color
    Friend Shared COLORSSH As Color = Color.BlueViolet    ' color of SSH comunication
    Friend Shared COLORTLS As Color = Color.BlueViolet    ' color of TLS comunication

    Private _textbox As RichTextBox
    Private _maxCharsCount As Integer

    Public Sub New(ByVal textbox As RichTextBox, ByVal maxCharsCount As Integer, ByVal level As LogLevel)
        _textbox = textbox
        _maxCharsCount = maxCharsCount
        Me.Level = level
    End Sub    'New

    Public Overloads Overrides Sub Write(ByVal level As LogLevel, ByVal objectType As Type, ByVal objectId As Integer, ByVal area As String, ByVal message As String)
        If level < Me.Level Then Return

        Dim color As Color = COLORINFO

        If level >= LogLevel.Error Then
            color = COLORERROR
        Else
            Select Case area.ToUpper()
                Case "COMMAND"
                    color = COLORCOMMAND
                Case "RESPONSE"
                    color = COLORRESPONSE
                Case "SSH"
                    color = COLORSSH
                Case "TLS"
                    color = COLORTLS
            End Select
        End If

        message = String.Format("{0:HH:mm:ss.fff} {1} {2}: {3}{4}", _
         DateTime.Now, level.ToString(), area, message, ControlChars.NewLine)

        Try
            If Not _textbox.IsDisposed Then
                _textbox.Invoke(New WriteLogHandler(AddressOf WriteLog), New Object() {message, color})
            End If
        Catch x As ObjectDisposedException
        End Try
    End Sub    'Write

    Delegate Sub WriteLogHandler(ByVal message As String, ByVal color As Color)

    Private Sub WriteLog(ByVal message As String, ByVal color As Color)
        EnsureTextSpace(message.Length)

        _textbox.Focus()
        _textbox.SelectionColor = color
        _textbox.AppendText(message)
    End Sub    'WriteLog

    Private Sub EnsureTextSpace(ByVal length As Integer)
        If _textbox.TextLength + length < _maxCharsCount Then
            Return
        End If

        Dim spaceLeft As Integer = _maxCharsCount - length

        If spaceLeft <= 0 Then
            _textbox.Clear()
            Return
        End If

        Dim plainText As String = _textbox.Text

        ' find the end of line
        Dim start As Integer = plainText.IndexOf(ControlChars.Lf, plainText.Length - spaceLeft)
        If start >= 0 AndAlso start + 1 < plainText.Length Then
            _textbox.SelectionStart = 0
            _textbox.SelectionLength = start + 1

            ' setting the SelectedText property is available only when ReadOnly = false
            Dim ro As Boolean = _textbox.ReadOnly
            _textbox.ReadOnly = False
            _textbox.SelectedText = ""
            _textbox.ReadOnly = ro

            _textbox.SelectionStart = _textbox.TextLength
            _textbox.SelectionLength = 0
        Else
            _textbox.Clear()
        End If
    End Sub    'EnsureTextSpace

End Class 'TextBoxLogger
