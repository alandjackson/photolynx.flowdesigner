//  
//   Rebex Sample Code License
// 
//   Copyright (c) 2009, Rebex CR s.r.o. www.rebex.net, 
//   All rights reserved.
// 
//   Permission to use, copy, modify, and/or distribute this software for any
//   purpose with or without fee is hereby granted
// 
//   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//   EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//   HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
//   OTHER DEALINGS IN THE SOFTWARE.
// 

using System;
using System.IO;
using Rebex.Net;

namespace Rebex.Samples.GetPut
{
	/// <summary>
	/// This sample is a simple utility to upload/download a file to/from
	/// a SFTP server using the Sftp class.
	/// </summary>
	class GetPut
	{
		[STAThread]
		static void Main(string[] args)
		{
			string method = null;
			if (args.Length == 4)
			{
				switch (args[0].ToLower())
				{
					case "get": method = "get"; break;
					case "put": method = "put"; break;
				}
			}

			if (method == null)
			{
				// display syntax if wrong number of parameters or unsupported operation
				Console.WriteLine ("GetPut - uploads or downloads a file from a SFTP server");
				Console.WriteLine ("Syntax: GetPut get|put hostname remotepath localpath");
				Console.WriteLine ("Example: GetPut get sftp.example.com /directory/file.txt file.txt");
				return;
			}
			string hostname = args[1];
			string remotePath = args[2];
			string localPath = args[3];
			// create Sftp object and connect to the server
			Sftp ftp = new Sftp();
			ftp.Connect (hostname);
			// ask for username and password and login
			Console.Write ("Username: ");
			string username = Console.ReadLine();
			Console.Write ("Password: ");
			string password = Console.ReadLine();
			ftp.Login (username, password);

			// transfer the file
			long bytes;
			if (method == "get")
				bytes = ftp.GetFile (remotePath, localPath);
			else
				bytes = ftp.PutFile (localPath, remotePath);

			Console.WriteLine ("Transfered {0} bytes.", bytes);

			// disconnect from the server
			ftp.Disconnect();
		}
	}
}
