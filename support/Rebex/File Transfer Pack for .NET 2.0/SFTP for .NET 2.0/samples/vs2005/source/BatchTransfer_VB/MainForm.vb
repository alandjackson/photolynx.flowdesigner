'  
'   Rebex Sample Code License
' 
'   Copyright (c) 2009, Rebex CR s.r.o. www.rebex.net, 
'   All rights reserved.
' 
'   Permission to use, copy, modify, and/or distribute this software for any
'   purpose with or without fee is hereby granted
' 
'   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
'   EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
'   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
'   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
'   HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
'   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
'   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
'   OTHER DEALINGS IN THE SOFTWARE.
' 

Imports System
Imports System.IO
Imports System.Drawing
Imports System.Collections
Imports System.ComponentModel
Imports System.Windows.Forms
Imports System.Data
Imports System.Threading
#If DOTNET10 Then
Imports System.Windows.Forms.Design
#End If
Imports Rebex.Net


''' <summary>
''' Main application form.
''' </summary>
Public Class MainForm
    Inherits System.Windows.Forms.Form
    ' Path to configuration file
    Public Shared ReadOnly ConfigFilePath As String = Path.Combine(Environment.GetFolderPath(System.Environment.SpecialFolder.LocalApplicationData), "Rebex\SFTP\BatchTransfer.xml")

    ' Application configuration
    Private ReadOnly _config As Configuration

    ' Length of the current transferring file
    Private _currentFileLength As Long

    Private _sftp As Sftp
    Private _openFolder As FolderBrowserDialog

#Region "Windows Form Designer generated code"
    Public Sub New()
#If (Not DOTNET10 And Not DOTNET11) Then
        CheckForIllegalCrossThreadCalls = True
#End If
        InitializeComponent()

        _config = New Configuration(ConfigFilePath)
        _openFolder = New FolderBrowserDialog
        _problemForm = New ProblemDetectedForm
        lblFilesCount.Text = String.Empty

        LoadSettings()
    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents label5 As System.Windows.Forms.Label
    Friend WithEvents label6 As System.Windows.Forms.Label
    Friend WithEvents tbPassword As System.Windows.Forms.TextBox
    Friend WithEvents label4 As System.Windows.Forms.Label
    Friend WithEvents tbUserName As System.Windows.Forms.TextBox
    Friend WithEvents label3 As System.Windows.Forms.Label
    Friend WithEvents tbPort As System.Windows.Forms.TextBox
    Friend WithEvents label2 As System.Windows.Forms.Label
    Friend WithEvents tbHostname As System.Windows.Forms.TextBox
    Friend WithEvents label1 As System.Windows.Forms.Label
    Friend WithEvents tbLocalPath As System.Windows.Forms.TextBox
    Friend WithEvents tbRemotePath As System.Windows.Forms.TextBox
    Friend WithEvents label7 As System.Windows.Forms.Label
    Friend WithEvents lblFilesCount As System.Windows.Forms.Label
    Friend WithEvents pbFile As System.Windows.Forms.ProgressBar
    Friend WithEvents pbTotal As System.Windows.Forms.ProgressBar
    Friend WithEvents btnUpload As System.Windows.Forms.Button
    Friend WithEvents btnDownload As System.Windows.Forms.Button
    Friend WithEvents btnOpen As System.Windows.Forms.Button
    Friend WithEvents label8 As System.Windows.Forms.Label
    Friend WithEvents lblBytesTotal As System.Windows.Forms.Label
    Friend WithEvents statusBar As System.Windows.Forms.StatusBar
    Friend WithEvents groupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents panel1 As System.Windows.Forms.Panel
    Friend WithEvents label9 As System.Windows.Forms.Label
    Friend WithEvents label10 As System.Windows.Forms.Label
    Friend WithEvents lblFrom As System.Windows.Forms.Label
    Friend WithEvents lblTo As System.Windows.Forms.Label
    Friend WithEvents lblToPath As System.Windows.Forms.Label
    Friend WithEvents lblFromPath As System.Windows.Forms.Label
    Friend WithEvents btnAbort As System.Windows.Forms.Button
    Friend WithEvents _problemForm As ProblemDetectedForm
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.tbLocalPath = New System.Windows.Forms.TextBox
        Me.label5 = New System.Windows.Forms.Label
        Me.tbRemotePath = New System.Windows.Forms.TextBox
        Me.label6 = New System.Windows.Forms.Label
        Me.tbPassword = New System.Windows.Forms.TextBox
        Me.label4 = New System.Windows.Forms.Label
        Me.tbUserName = New System.Windows.Forms.TextBox
        Me.label3 = New System.Windows.Forms.Label
        Me.tbPort = New System.Windows.Forms.TextBox
        Me.label2 = New System.Windows.Forms.Label
        Me.tbHostname = New System.Windows.Forms.TextBox
        Me.label1 = New System.Windows.Forms.Label
        Me.btnOpen = New System.Windows.Forms.Button
        Me.label7 = New System.Windows.Forms.Label
        Me.lblFilesCount = New System.Windows.Forms.Label
        Me.lblFrom = New System.Windows.Forms.Label
        Me.lblTo = New System.Windows.Forms.Label
        Me.pbFile = New System.Windows.Forms.ProgressBar
        Me.pbTotal = New System.Windows.Forms.ProgressBar
        Me.btnUpload = New System.Windows.Forms.Button
        Me.btnDownload = New System.Windows.Forms.Button
        Me.label8 = New System.Windows.Forms.Label
        Me.lblBytesTotal = New System.Windows.Forms.Label
        Me.statusBar = New System.Windows.Forms.StatusBar
        Me.groupBox1 = New System.Windows.Forms.GroupBox
        Me.panel1 = New System.Windows.Forms.Panel
        Me.label10 = New System.Windows.Forms.Label
        Me.label9 = New System.Windows.Forms.Label
        Me.lblToPath = New System.Windows.Forms.Label
        Me.lblFromPath = New System.Windows.Forms.Label
        Me.btnAbort = New System.Windows.Forms.Button
        Me.groupBox1.SuspendLayout()
        Me.panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'tbLocalPath
        '
        Me.tbLocalPath.Location = New System.Drawing.Point(104, 72)
        Me.tbLocalPath.Name = "tbLocalPath"
        Me.tbLocalPath.Size = New System.Drawing.Size(272, 20)
        Me.tbLocalPath.TabIndex = 5
        Me.tbLocalPath.Text = ""
        '
        'label5
        '
        Me.label5.Location = New System.Drawing.Point(8, 72)
        Me.label5.Name = "label5"
        Me.label5.Size = New System.Drawing.Size(96, 20)
        Me.label5.TabIndex = 21
        Me.label5.Text = "Local path:"
        Me.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tbRemotePath
        '
        Me.tbRemotePath.Location = New System.Drawing.Point(104, 96)
        Me.tbRemotePath.Name = "tbRemotePath"
        Me.tbRemotePath.Size = New System.Drawing.Size(352, 20)
        Me.tbRemotePath.TabIndex = 7
        Me.tbRemotePath.Text = ""
        '
        'label6
        '
        Me.label6.Location = New System.Drawing.Point(8, 96)
        Me.label6.Name = "label6"
        Me.label6.Size = New System.Drawing.Size(96, 20)
        Me.label6.TabIndex = 23
        Me.label6.Text = "Remote path:"
        Me.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tbPassword
        '
        Me.tbPassword.Location = New System.Drawing.Point(344, 48)
        Me.tbPassword.Name = "tbPassword"
        Me.tbPassword.PasswordChar = Microsoft.VisualBasic.ChrW(42)
        Me.tbPassword.Size = New System.Drawing.Size(112, 20)
        Me.tbPassword.TabIndex = 4
        Me.tbPassword.Text = ""
        '
        'label4
        '
        Me.label4.Location = New System.Drawing.Point(280, 48)
        Me.label4.Name = "label4"
        Me.label4.Size = New System.Drawing.Size(64, 20)
        Me.label4.TabIndex = 31
        Me.label4.Text = "Password:"
        Me.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tbUserName
        '
        Me.tbUserName.Location = New System.Drawing.Point(104, 48)
        Me.tbUserName.Name = "tbUserName"
        Me.tbUserName.Size = New System.Drawing.Size(168, 20)
        Me.tbUserName.TabIndex = 3
        Me.tbUserName.Text = ""
        '
        'label3
        '
        Me.label3.Location = New System.Drawing.Point(8, 48)
        Me.label3.Name = "label3"
        Me.label3.Size = New System.Drawing.Size(96, 20)
        Me.label3.TabIndex = 30
        Me.label3.Text = "Login name:"
        Me.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tbPort
        '
        Me.tbPort.Location = New System.Drawing.Point(344, 24)
        Me.tbPort.Name = "tbPort"
        Me.tbPort.Size = New System.Drawing.Size(48, 20)
        Me.tbPort.TabIndex = 2
        Me.tbPort.Text = "22"
        '
        'label2
        '
        Me.label2.Location = New System.Drawing.Point(280, 24)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(64, 20)
        Me.label2.TabIndex = 29
        Me.label2.Text = "Port:"
        Me.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tbHostname
        '
        Me.tbHostname.Location = New System.Drawing.Point(104, 24)
        Me.tbHostname.Name = "tbHostname"
        Me.tbHostname.Size = New System.Drawing.Size(168, 20)
        Me.tbHostname.TabIndex = 1
        Me.tbHostname.Text = ""
        '
        'label1
        '
        Me.label1.Location = New System.Drawing.Point(8, 24)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(96, 20)
        Me.label1.TabIndex = 28
        Me.label1.Text = "Host name:"
        Me.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnOpen
        '
        Me.btnOpen.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnOpen.Location = New System.Drawing.Point(384, 72)
        Me.btnOpen.Name = "btnOpen"
        Me.btnOpen.Size = New System.Drawing.Size(72, 20)
        Me.btnOpen.TabIndex = 6
        Me.btnOpen.Text = "&Browse..."
        '
        'label7
        '
        Me.label7.Location = New System.Drawing.Point(8, 280)
        Me.label7.Name = "label7"
        Me.label7.Size = New System.Drawing.Size(120, 24)
        Me.label7.TabIndex = 33
        Me.label7.Text = "Files processed / total:"
        '
        'lblFilesCount
        '
        Me.lblFilesCount.Location = New System.Drawing.Point(128, 280)
        Me.lblFilesCount.Name = "lblFilesCount"
        Me.lblFilesCount.Size = New System.Drawing.Size(128, 24)
        Me.lblFilesCount.TabIndex = 34
        Me.lblFilesCount.Text = "{0} / {1}"
        '
        'lblFrom
        '
        Me.lblFrom.Location = New System.Drawing.Point(8, 224)
        Me.lblFrom.Name = "lblFrom"
        Me.lblFrom.Size = New System.Drawing.Size(48, 24)
        Me.lblFrom.TabIndex = 36
        Me.lblFrom.Text = "From:"
        '
        'lblTo
        '
        Me.lblTo.Location = New System.Drawing.Point(8, 248)
        Me.lblTo.Name = "lblTo"
        Me.lblTo.Size = New System.Drawing.Size(48, 24)
        Me.lblTo.TabIndex = 37
        Me.lblTo.Text = "To:"
        '
        'pbFile
        '
        Me.pbFile.Location = New System.Drawing.Point(8, 304)
        Me.pbFile.Name = "pbFile"
        Me.pbFile.Size = New System.Drawing.Size(464, 23)
        Me.pbFile.TabIndex = 38
        '
        'pbTotal
        '
        Me.pbTotal.Location = New System.Drawing.Point(8, 336)
        Me.pbTotal.Name = "pbTotal"
        Me.pbTotal.Size = New System.Drawing.Size(464, 23)
        Me.pbTotal.TabIndex = 39
        '
        'btnUpload
        '
        Me.btnUpload.Location = New System.Drawing.Point(312, 192)
        Me.btnUpload.Name = "btnUpload"
        Me.btnUpload.TabIndex = 9
        Me.btnUpload.Text = "&Upload"
        '
        'btnDownload
        '
        Me.btnDownload.Location = New System.Drawing.Point(232, 192)
        Me.btnDownload.Name = "btnDownload"
        Me.btnDownload.TabIndex = 8
        Me.btnDownload.Text = "&Download"
        '
        'label8
        '
        Me.label8.Location = New System.Drawing.Point(272, 280)
        Me.label8.Name = "label8"
        Me.label8.Size = New System.Drawing.Size(80, 24)
        Me.label8.TabIndex = 42
        Me.label8.Text = "Bytes total:"
        '
        'lblBytesTotal
        '
        Me.lblBytesTotal.Location = New System.Drawing.Point(360, 280)
        Me.lblBytesTotal.Name = "lblBytesTotal"
        Me.lblBytesTotal.Size = New System.Drawing.Size(112, 24)
        Me.lblBytesTotal.TabIndex = 43
        Me.lblBytesTotal.Text = "0"
        '
        'statusBar
        '
        Me.statusBar.Location = New System.Drawing.Point(0, 374)
        Me.statusBar.Name = "statusBar"
        Me.statusBar.Size = New System.Drawing.Size(480, 22)
        Me.statusBar.SizingGrip = False
        Me.statusBar.TabIndex = 44
        Me.statusBar.Text = "Welcome!"
        '
        'groupBox1
        '
        Me.groupBox1.Controls.Add(Me.tbPort)
        Me.groupBox1.Controls.Add(Me.tbPassword)
        Me.groupBox1.Controls.Add(Me.label6)
        Me.groupBox1.Controls.Add(Me.label2)
        Me.groupBox1.Controls.Add(Me.tbRemotePath)
        Me.groupBox1.Controls.Add(Me.tbHostname)
        Me.groupBox1.Controls.Add(Me.label5)
        Me.groupBox1.Controls.Add(Me.tbLocalPath)
        Me.groupBox1.Controls.Add(Me.btnOpen)
        Me.groupBox1.Controls.Add(Me.label1)
        Me.groupBox1.Controls.Add(Me.label4)
        Me.groupBox1.Controls.Add(Me.tbUserName)
        Me.groupBox1.Controls.Add(Me.label3)
        Me.groupBox1.Location = New System.Drawing.Point(4, 60)
        Me.groupBox1.Name = "groupBox1"
        Me.groupBox1.Size = New System.Drawing.Size(468, 128)
        Me.groupBox1.TabIndex = 45
        Me.groupBox1.TabStop = False
        Me.groupBox1.Text = "Settings"
        '
        'panel1
        '
        Me.panel1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.panel1.BackColor = System.Drawing.Color.White
        Me.panel1.Controls.Add(Me.label10)
        Me.panel1.Controls.Add(Me.label9)
        Me.panel1.Location = New System.Drawing.Point(0, 0)
        Me.panel1.Name = "panel1"
        Me.panel1.Size = New System.Drawing.Size(480, 56)
        Me.panel1.TabIndex = 46
        '
        'label10
        '
        Me.label10.Location = New System.Drawing.Point(16, 26)
        Me.label10.Name = "label10"
        Me.label10.Size = New System.Drawing.Size(248, 18)
        Me.label10.TabIndex = 48
        Me.label10.Text = "Uploads or downloads multiple files."
        '
        'label9
        '
        Me.label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.label9.Location = New System.Drawing.Point(8, 8)
        Me.label9.Name = "label9"
        Me.label9.Size = New System.Drawing.Size(248, 18)
        Me.label9.TabIndex = 47
        Me.label9.Text = "Batch Transfer Utility"
        '
        'lblToPath
        '
        Me.lblToPath.Location = New System.Drawing.Point(56, 248)
        Me.lblToPath.Name = "lblToPath"
        Me.lblToPath.Size = New System.Drawing.Size(416, 24)
        Me.lblToPath.TabIndex = 48
        '
        'lblFromPath
        '
        Me.lblFromPath.Location = New System.Drawing.Point(56, 224)
        Me.lblFromPath.Name = "lblFromPath"
        Me.lblFromPath.Size = New System.Drawing.Size(416, 24)
        Me.lblFromPath.TabIndex = 47
        '
        'btnAbort
        '
        Me.btnAbort.Enabled = False
        Me.btnAbort.Location = New System.Drawing.Point(392, 192)
        Me.btnAbort.Name = "btnAbort"
        Me.btnAbort.TabIndex = 10
        Me.btnAbort.Text = "&Abort"
        '
        'MainForm
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(480, 396)
        Me.Controls.Add(Me.btnAbort)
        Me.Controls.Add(Me.lblToPath)
        Me.Controls.Add(Me.lblFromPath)
        Me.Controls.Add(Me.panel1)
        Me.Controls.Add(Me.groupBox1)
        Me.Controls.Add(Me.statusBar)
        Me.Controls.Add(Me.lblBytesTotal)
        Me.Controls.Add(Me.label8)
        Me.Controls.Add(Me.btnDownload)
        Me.Controls.Add(Me.btnUpload)
        Me.Controls.Add(Me.pbTotal)
        Me.Controls.Add(Me.pbFile)
        Me.Controls.Add(Me.lblTo)
        Me.Controls.Add(Me.lblFrom)
        Me.Controls.Add(Me.lblFilesCount)
        Me.Controls.Add(Me.label7)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "MainForm"
        Me.Text = "Rebex SFTP Batch Transfer Sample"
        Me.groupBox1.ResumeLayout(False)
        Me.panel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
#End Region

    ''' <summary>
    ''' Displays an error message.
    ''' </summary>
    ''' <param name="ex">Exception to be shown.</param>
    Private Sub ShowError(ByVal ex As Exception)
        MessageBox.Show(Me, String.Format("{0}" & Chr(13) & Chr(10) & Chr(13) & Chr(10) & "Exception details: {1}", ex.Message, ex), "Error", MessageBoxButtons.OK, MessageBoxIcon.[Error])
    End Sub

    ''' <summary>
    ''' Displays an error message.
    ''' </summary>
    ''' <param name="message">Message to be shown.</param>
    Private Sub ShowError(ByVal message As String)
        MessageBox.Show(Me, message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
    End Sub

    ''' <summary>
    ''' Displays a message.
    ''' </summary>
    ''' <param name="message">Message to be shown.</param>
    Private Sub ShowMessage(ByVal message As String)
        MessageBox.Show(Me, message, "Info", MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub

    ''' <summary>
    ''' Enable/disable buttons and other controls to match the desired state
    ''' </summary>
    ''' <param name="transferring">True if transferring state is to be set; false if no transfering state is to be set.</param>
    Private Sub SetState(ByVal transferring As Boolean)
        If transferring Then
            ' enable/disable buttons
            btnDownload.Enabled = False
            btnUpload.Enabled = False
            btnAbort.Enabled = True

            ' disable connection properties
            tbHostname.Enabled = False
            tbPort.Enabled = False
            tbUserName.Enabled = False
            tbPassword.Enabled = False

            ' initialize the problem-reporting form
            _problemForm.Initialize()
        Else
            ' clear paths
            lblFromPath.Text = ""
            lblToPath.Text = ""

            ' enable / disable buttons
            btnDownload.Enabled = True
            btnUpload.Enabled = True
            btnAbort.Enabled = False

            ' enable connection properties
            tbHostname.Enabled = True
            tbPort.Enabled = True
            tbUserName.Enabled = True
            tbPassword.Enabled = True
        End If
    End Sub

    ''' <summary>
    ''' Save configuration settings.
    ''' </summary>
    Private Sub SaveSettings()
        _config.SetValue("hostname", tbHostname.Text)
        _config.SetValue("port", tbPort.Text)
        _config.SetValue("userName", tbUserName.Text)
        _config.SetValue("password", tbPassword.Text)
        _config.SetValue("localPath", tbLocalPath.Text)
        _config.SetValue("remotePath", tbRemotePath.Text)
        _config.Save()
    End Sub

    ''' <summary>
    ''' Load configuration settings.
    ''' </summary>
    Private Sub LoadSettings()
        tbHostname.Text = _config.GetString("hostname")
        tbPort.Text = _config.GetInt32("port", Sftp.DefaultPort).ToString()
        tbUserName.Text = _config.GetString("userName")
        tbPassword.Text = _config.GetString("password")
        tbLocalPath.Text = _config.GetString("localPath")
        tbRemotePath.Text = _config.GetString("remotePath")
    End Sub

    ''' <summary>
    ''' Connect to the SFTP server.
    ''' </summary>
    ''' <returns>True if successfully connected, false otherwise.</returns>
    Private Function Connect() As Boolean
        Dim port As Integer

        ' check the hostname
        If tbHostname.Text.Length = 0 Then
            ShowError("Hostname is required.")
            Return False
        End If

        ' parse the port
        If tbPort.Text.Length = 0 Then
            port = Sftp.DefaultPort
        Else
#If (Not DOTNET10 And Not DOTNET11) Then
            Integer.TryParse(tbPort.Text, port)
#Else
                Try
                    port = Integer.Parse(tbPort.Text)
                Catch
                    port = 0
                End Try
#End If

            ' check the port
            If port <= 0 Then
                ShowError("Port must be a positive number.")
                Return False
            End If
        End If

        Try
            Cursor = Cursors.WaitCursor

            statusBar.Text = "Connecting..."

            ' connect and login
            _sftp = New Sftp
            _sftp.Connect(tbHostname.Text, port)
            _sftp.Login(tbUserName.Text, tbPassword.Text)
            AddHandler _sftp.BatchTransferProgress, AddressOf _sftp_BatchTransferProgress
            AddHandler _sftp.BatchTransferProblemDetected, AddressOf _sftp_BatchTransferProblemDetected
            AddHandler _sftp.TransferProgress, AddressOf _sftp_TransferProgress

            ' register batch transfer events

            ' register single file progress event

            Return True
        Catch x As Exception
            statusBar.Text = ""
            ShowError(x)
            Return False
        Finally
            Cursor = Cursors.Arrow
        End Try

    End Function

    ''' <summary>
    ''' Disconnect from the SFTP server.
    ''' </summary>
    Private Sub Disconnect()
        _sftp.Disconnect()
        _sftp.Dispose()
        _sftp = Nothing
    End Sub

    ''' <summary>
    ''' Download files.
    ''' </summary>
    ''' <param name="remotePath">Source file or directory remote path.</param>
    ''' <param name="localPath">Target directory local path.</param>
    Private Sub DownloadFiles(ByVal remotePath As String, ByVal localPath As String)
        SaveSettings()

        ' check the local path
        If localPath Is Nothing OrElse localPath.Length = 0 Then
            ShowError("Local path cannot be empty.")
            Return
        End If

        ' connect to the SFTP server
        If Not Connect() Then
            Return
        End If

        Try
            SetState(True)

            ' download files
            _sftp.GetFiles(remotePath, localPath, SftpBatchTransferOptions.Recursive)

            Dim message As String = "Download finished successfully."
            statusBar.Text = message
            ShowMessage(message)

        Catch ex As Exception
            If TypeOf ex Is SftpException AndAlso CType(ex, SftpException).Status = SftpExceptionStatus.OperationAborted Then
                ShowMessage("Operation aborted.")
            Else
                ShowError(ex)
            End If
        Finally
            Disconnect()
            SetState(False)
        End Try
    End Sub

    ''' <summary>
    ''' Handles the Download button click event.
    ''' </summary>
    Private Sub btnDownload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDownload.Click
        DownloadFiles(tbRemotePath.Text, tbLocalPath.Text)
    End Sub

    ''' <summary>
    ''' Upload files.
    ''' </summary>
    ''' <param name="localPath">Source file or directory local path.</param>
    ''' <param name="remotePath">Target directory remote path.</param>
    Private Sub UploadFiles(ByVal localPath As String, ByVal remotePath As String)
        SaveSettings()

        ' check the local path
        If localPath Is Nothing OrElse localPath.Length = 0 Then
            ShowError("Local path cannot be empty.")
            Return
        End If

        ' connect to the SFTP server
        If Not Connect() Then
            Return
        End If

        Try
            SetState(True)

            ' upload files
            _sftp.PutFiles(localPath, remotePath, SftpBatchTransferOptions.Recursive)

            Dim message As String = "Upload finished successfully."
            statusBar.Text = message
            ShowMessage(message)
        Catch ex As Exception
            If TypeOf ex Is SftpException AndAlso CType(ex, SftpException).Status = SftpExceptionStatus.OperationAborted Then
                ShowMessage("Operation aborted.")
            Else
                ShowError(ex)
            End If
        Finally
            Disconnect()
            SetState(False)
        End Try
    End Sub

    ''' <summary>
    ''' Handles the Upload button click event.
    ''' </summary>
    Private Sub btnUpload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        UploadFiles(tbLocalPath.Text, tbRemotePath.Text)
    End Sub

    ''' <summary>
    ''' Handles the Open button click event.
    ''' </summary>
    Private Sub btnOpen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOpen.Click
        Dim res As DialogResult = _openFolder.ShowDialog(Me)

        If res <> DialogResult.OK Then
            Return
        End If

        tbLocalPath.Text = _openFolder.SelectedPath
    End Sub

    ''' <summary>
    ''' Handles the Abort button click event.
    ''' </summary>
    Private Sub btnAbort_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAbort.Click
        _sftp.AbortTransfer()

    End Sub

    ''' <summary>
    ''' Handles the batch transfer progress event.
    ''' </summary>
    Private Sub _sftp_BatchTransferProgress(ByVal sender As Object, ByVal e As SftpBatchTransferProgressEventArgs)
        pbTotal.Value = e.ProcessedPercentage
        lblFilesCount.Text = String.Format("{0} / {1}", e.FilesProcessed, e.FilesTotal)
        If e.State = SftpTransferState.Uploading Then
            lblFromPath.Text = e.LocalPath
            lblToPath.Text = e.RemotePath
        Else
            lblFromPath.Text = e.RemotePath
            lblToPath.Text = e.LocalPath
        End If

        Select Case e.Operation
            Case SftpBatchTransferOperation.HierarchyRetrievalStarted
                statusBar.Text = "Retrieving hierarchy..."
                Exit Select
            Case SftpBatchTransferOperation.HierarchyRetrievalFailed
                statusBar.Text = "Retrieve hierarchy failed."
                Exit Select
            Case SftpBatchTransferOperation.HierarchyRetrieved
                ' set the bytes total info
                lblBytesTotal.Text = e.BytesTotal.ToString()
                statusBar.Text = "Hierarchy retrieved."
                Exit Select
            Case SftpBatchTransferOperation.DirectoryProcessingStarted
                statusBar.Text = "Processing directory."
                Exit Select
            Case SftpBatchTransferOperation.DirectoryProcessingFailed
                statusBar.Text = "Directory processing failed."
                Exit Select
            Case SftpBatchTransferOperation.DirectorySkipped
                statusBar.Text = "Directory skipped."
                Exit Select
            Case SftpBatchTransferOperation.DirectoryCreated
                statusBar.Text = "Directory created."
                Exit Select
            Case SftpBatchTransferOperation.FileProcessingStarted
                statusBar.Text = "Processing file..."
                Exit Select
            Case SftpBatchTransferOperation.FileTransferStarting
                pbFile.Value = 0
                _currentFileLength = e.CurrentFileLength
                statusBar.Text = "Transferring file..."
                Exit Select
            Case SftpBatchTransferOperation.FileProcessingFailed
                statusBar.Text = "File processing failed."
                Exit Select
            Case SftpBatchTransferOperation.FileSkipped
                statusBar.Text = "File skipped."
                Exit Select
            Case SftpBatchTransferOperation.FileTransferred
                statusBar.Text = "File transferred."
                Exit Select
        End Select

        ' process any application events to prevent the windown from freezing
        Application.DoEvents()
    End Sub

    ''' <summary>
    ''' Handles the batch transfer problem detected event.
    ''' </summary>
    Private Sub _sftp_BatchTransferProblemDetected(ByVal sender As Object, ByVal e As SftpBatchTransferProblemDetectedEventArgs)
        _problemForm.ShowModal(Me, e)

        ' process any application events to prevent the windown from freezing
        Application.DoEvents()
    End Sub

    ''' <summary>
    ''' Handles the single file transfer progress event.
    ''' </summary>
    Private Sub _sftp_TransferProgress(ByVal sender As Object, ByVal e As SftpTransferProgressEventArgs)
        If _currentFileLength > 0 Then
            Dim percentage As Integer = CInt(pbFile.Maximum * CDec(e.BytesTransferred) / CDec(_currentFileLength))

            ' handle the file length changes during transfer
            If percentage > pbFile.Maximum Then percentage = pbFile.Maximum

            If percentage <> pbFile.Value Then pbFile.Value = percentage
        End If

        ' process any application events to prevent the windown from freezing
        Application.DoEvents()
    End Sub

End Class

#If DOTNET10 Then

	#Region "Folder Browser for .NET Framework 1.0"

	''' <summary>
	''' Support folder dialog in Net 1.0
	''' </summary>
	Public Class FolderBrowserDialog
		Inherits System.Windows.Forms.Design.FolderNameEditor
		Private folderDialog As FolderBrowser

		''' <summary>
		''' Constructor - create folder dialog class.
		''' </summary>
		Public Sub New()
			folderDialog = New FolderBrowser()
			' if no changes to defaults, skip using this function 
			Initialize()
		End Sub

		''' <summary>
		''' Get selected folder, the same property as in .NET 1.1 .
		''' FolderBrowserDialog.
		''' </summary>
		Public ReadOnly Property SelectedPath() As String
			Get
				Return folderDialog.DirectoryPath
			End Get
		End Property

		''' <summary>
		''' Show folder dialog.
		''' </summary>
		''' <returns>Ok or Cancel</returns>
		Public Function ShowDialog(ByVal control As Control) As DialogResult
			Return folderDialog.ShowDialog(control)
		End Function

		''' <summary>
		''' Initialize folder dialog component and 
		''' make initialozation changes.
		''' </summary>
		Protected Sub Initialize()
			MyBase.InitializeDialog(folderDialog)
		End Sub
	End Class

	#End Region

#End If


