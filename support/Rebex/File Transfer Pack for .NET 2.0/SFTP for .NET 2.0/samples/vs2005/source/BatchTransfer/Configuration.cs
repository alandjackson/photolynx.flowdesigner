//  
//   Rebex Sample Code License
// 
//   Copyright (c) 2009, Rebex CR s.r.o. www.rebex.net, 
//   All rights reserved.
// 
//   Permission to use, copy, modify, and/or distribute this software for any
//   purpose with or without fee is hereby granted
// 
//   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//   EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//   HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
//   OTHER DEALINGS IN THE SOFTWARE.
// 

using System;
using System.Text;
using System.Collections;
using System.Xml;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Rebex.Samples
{
	/// <summary>
	/// Class for easier access to configuration values.
	/// </summary>
	public class Configuration
	{
		/// <summary>
		/// Gets a Boolean value from configuration.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="defaultValue">Default value.</param>
		/// <returns>A Boolean value from configuration.</returns>
		public bool GetBoolean(string key, bool defaultValue)
		{
			try
			{
				string val = GetString(key);
				if (val == null)
					return defaultValue;
				return Convert.ToBoolean(val);
			}
			catch
			{
				return defaultValue;
			}
		}

		/// <summary>
		/// Gets an Int32 value from configuration.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="defaultValue">Default value.</param>
		/// <returns>An Int32 value from configuration.</returns>
		public int GetInt32(string key, int defaultValue)
		{
			try
			{
				string val = GetString(key);
				if (val == null)
					return defaultValue;
				return Convert.ToInt32(val);
			}
			catch
			{
				return defaultValue;
			}
		}

		/// <summary>
		/// Gets an Int32 value from configuration.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns>An Int32 value from configuration.</returns>
		public int GetInt32(string key)
		{
			return (int)GetValue(key, typeof(int));
		}

		/// <summary>
		/// Gets a string value from the configuration.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="defaultValue">The default value.</param>
		/// <returns>A string value from the configuration.</returns>
		public string GetString(string key, string defaultValue)
		{
			object o = GetValue(key, typeof(string));

			if (o == null)
				return defaultValue;
			else
				return o.ToString();
		}

		/// <summary>
		/// Gets a string value from the configuration.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns>A string value from the configuration.</returns>
		public string GetString(string key)
		{
			return GetString(key, null);
		}

		/// <summary>
		/// Gets an object value from the configuration.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="type">The type.</param>
		/// <returns>An object value from the configuration.</returns>
		public object GetValue(string key, Type type)
		{
			if (key == null)
				throw new ArgumentNullException();

			if (_table == null)
				Load();

			if (type.IsEnum)
			{
				if (_table[key] == null)
					return Enum.Parse(type, "0");
				return Enum.Parse(type, _table[key].ToString(), true);
			}

			if (_table[key] == null)
				return null;

			if (IsSerializable(type))
			{
				string value = _table[key].ToString();
				using (MemoryStream stream = new MemoryStream(Convert.FromBase64String(value)))
				{
					try { return Formatter.Deserialize(stream); }
					catch { return null; }
				}
			}
			else
			{
				return Convert.ChangeType(_table[key], type);
			}
		}

		/// <summary>
		/// Sets the configuration value.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="val">The value.</param>
		public void SetValue(string key, object val)
		{
			if (key == null)
				throw new ArgumentNullException();

			if (_table == null)
				Load();

			if (val != null && val.GetType().IsEnum)
				val = Convert.ChangeType(val, Enum.GetUnderlyingType(val.GetType()));

			if (_table.ContainsKey(key))
				_table[key] = val;
			else
				_table.Add(key, val);
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Configuration"/> class.
		/// </summary>
		/// <param name="filename">The filename where configuration is stored.</param>
		public Configuration(string filename)
		{
			_filename = filename;
		}

		/// <summary>
		/// Saves the configuration values to file.
		/// </summary>
		public void Save()
		{
			if (_table == null)
				Load();

			XmlDocument xml = new XmlDocument();
			XmlElement config = xml.CreateElement("configuration");
			xml.AppendChild(config);

			if (_table.Count == 0)
				return;

			IDictionaryEnumerator e = _table.GetEnumerator();

			while (e.MoveNext())
			{
				XmlElement key = xml.CreateElement("key");
				config.AppendChild(key);
				XmlAttribute atrName = xml.CreateAttribute("name");
				atrName.Value = e.Key.ToString();
				XmlAttribute atrVal = xml.CreateAttribute("value");
				atrVal.Value = ValueToString(e.Value);
				key.Attributes.Append(atrName);
				key.Attributes.Append(atrVal);
			}

			string configPath = Path.GetDirectoryName(_filename);
			if (!Directory.Exists(configPath))
				Directory.CreateDirectory(configPath);
			xml.Save(_filename);
		}

		#region Private attributes

		/// <summary>
		/// Filename for I/O operations
		/// </summary>
		private string _filename = null;

		/// <summary>
		/// Hashtable for inmemory config values storage
		/// </summary>
		private Hashtable _table = null;

		private BinaryFormatter _formatter = null;
		private BinaryFormatter Formatter
		{
			get
			{
				if (_formatter == null)
					_formatter = new BinaryFormatter();
				return _formatter;
			}
		}

		#endregion

		#region  Private methods
		/// <summary>
		/// Gets a value indicationg whether the type is serializable or not.
		/// </summary>
		/// <param name="type">System.Type object.</param>
		/// <returns></returns>
		private bool IsSerializable(Type type)
		{
			// strings and value types are consider as not serializable for this purpose
			return !type.IsValueType &&
				type != typeof(string) &&
				(type.Attributes & System.Reflection.TypeAttributes.Serializable) > 0;
		}

		/// <summary>
		/// Gets a string representation of the value.
		/// </summary>
		/// <remarks>If value is serializable object, it returns Base64 string.</remarks>
		/// <param name="value">Value to be represent as a string.</param>
		/// <returns></returns>
		private string ValueToString(object value)
		{
			if (IsSerializable(value.GetType()))
			{
				using (MemoryStream stream = new MemoryStream())
				{
					Formatter.Serialize(stream, value);
					return Convert.ToBase64String(stream.GetBuffer(), 0, (int)stream.Length);
				}
			}
			return value.ToString();
		}

		/// <summary>
		/// Loads the configuration from the file.
		/// </summary>
		private void Load()
		{
			XmlDocument xml = new XmlDocument();

			if (_table == null)
				_table = new Hashtable();

			if (!File.Exists(_filename))
				return;

			if (_table != null)
				_table.Clear();

			xml.Load(_filename);

			XmlElement config = xml["configuration"];

			foreach (XmlNode key in config.ChildNodes)
			{
				string item = null;
				string name = null;

				if (key.Attributes["value"] != null)
					item = key.Attributes["value"].Value;
				if (key.Attributes["name"] != null)
					name = key.Attributes["name"].Value;
				if (key["value"] != null)
					item = key["value"].InnerText;
				if (key["name"] != null)
					name = key["name"].InnerText;

				if (name == null)
					continue;

				if (item == null)
					item = "";

				_table.Add(name, item);
			}
		}
		#endregion
	}
}
