//  
//   Rebex Sample Code License
// 
//   Copyright (c) 2009, Rebex CR s.r.o. www.rebex.net, 
//   All rights reserved.
// 
//   Permission to use, copy, modify, and/or distribute this software for any
//   purpose with or without fee is hereby granted
// 
//   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//   EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//   HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
//   OTHER DEALINGS IN THE SOFTWARE.
// 

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Resources;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Threading;

using Rebex;
using Rebex.Net;
using Rebex.Samples;

namespace Rebex.Samples.WinFormClient
{
	/// <summary>
	/// Simple SFTP client.
	/// </summary>
	public class MainForm : System.Windows.Forms.Form
	{
		private System.ComponentModel.Container components = null;
		private System.Windows.Forms.RichTextBox tbLog;
		private System.Windows.Forms.ListView listServer;
		private System.Windows.Forms.ColumnHeader name;
		private System.Windows.Forms.ColumnHeader size;
		private System.Windows.Forms.Label lblServer;
		private System.Windows.Forms.ListView listLocal;
		private System.Windows.Forms.Label lblLocal;
		private System.Windows.Forms.Label lblServerDirectory;
		private System.Windows.Forms.ColumnHeader columnHeader1;
		private System.Windows.Forms.ColumnHeader columnHeader2;
		private System.Windows.Forms.Label lblLocalDirectory;
		private System.Windows.Forms.ProgressBar pbTransfer;
		private System.Windows.Forms.Label lblProgress;
		private System.Windows.Forms.Label lblBatchTransferProgress;
		private System.Windows.Forms.ColumnHeader date;
		private System.Windows.Forms.ColumnHeader columnHeader3;
		private System.Windows.Forms.Button btnStop;
		private System.Windows.Forms.Button btnDelete;
		private System.Windows.Forms.Button btnUpload;
		private System.Windows.Forms.Button btnDownload;
		private System.Windows.Forms.Button btnRefresh;
		private System.Windows.Forms.Button btnMakeDir;
		private System.Windows.Forms.ComboBox cbDrives;
		private System.Windows.Forms.Button btnConnect;
		private System.Windows.Forms.GroupBox gbControl;
		private System.Windows.Forms.PictureBox headerLeft;
		private System.Windows.Forms.Panel headerMiddle;
		private System.Windows.Forms.PictureBox headerRight;

		private Sftp _sftp;                         // SFTP session
		private ListView listActive;                // active list (server x local)
		private int _serverDirs;                    // number of dirs on server side
		private int _localDirs;                     // number of dirs on local side
		private int _selectedDrive = -1;            // selected drive (A:, C:, ...) in combobox
		private string _localPath;                  // local path
		private bool _isWorking;                    // determines whether any operation is running
		private int _totalFilesTransferred;         // total files transferred last time
		private long _totalBytesTransferred;        // total bytes transferred last time
		private long _currentFileLength;            // current transferring file length
		private long _currentBytesTransferred;      // bytes transferred of the current transferring file
		private DateTime _lastTransferProgressTime; // last TransferProgress event call time
		private DateTime _transferTime;             // transfer launch-time
		private ProblemDetectedForm _problemForm;   // informs the user about problems while transferring data

		// constansts    
		private const string SYMLINK = "--->";
		private TrackBar trackBar;
		private Label lblSpeed;      // symlink tag
		private const string URL = "http://www.rebex.net/sftp.net/"; // url

		public void WriteToLog(Exception x)
		{
			// SftpExceptions are automatilcally logged by component logger.
			// No need to display them again.
			// We don't wont duplicate entries for such exceptions.
			if (x is SftpException)
				return;

			WriteToLog("* " + x.ToString(), RichTextBoxLogWriter.COLORERROR);
		}

		public void WriteToLog(string message, Color color)
		{
			tbLog.Focus();
			tbLog.SelectionColor = color;
			tbLog.AppendText(message + "\r\n");
		}

		/// <summary>
		/// get filename from path (xxxxx/xxxx/yyy -> yyy)
		/// </summary>
		/// <param name="path">sftp path</param>
		/// <returns>filename</returns>
		private static string GetFilenameFromPath(string path)
		{
			int idx = path.LastIndexOf("/");

			if (idx != -1 && idx + 1 != path.Length)
			{
				path = path.Substring(idx + 1);
			}

			return path;
		}

		/// <summary>
		/// format datetime to readable form in the panels
		/// </summary>
		/// <param name="dt">datetime</param>
		/// <returns>formatted string</returns>
		private static string FormatFileTime(DateTime dt)
		{
			return dt.ToString("yyyy-MM-dd HH:mm");
		}

		/// <summary>
		/// Wrapper for Invoke method that doesn't throw an exception after the object has been
		/// disposed while the calling method was running in a background thread.
		/// </summary>
		/// <param name="method"></param>
		/// <param name="args"></param>
		private void SafeInvoke(Delegate method, params object[] args)
		{
			try
			{
				if (!IsDisposed)
					Invoke(method, args);
			}
			catch (ObjectDisposedException)
			{
			}
		}

		/// <summary>
		/// event displaying sftp state
		/// </summary>
		public void StateChanged(object sender, SftpStateChangedEventArgs e)
		{
			switch (e.NewState)
			{
				case SftpState.Disconnected:
				case SftpState.Disposed:
					lblBatchTransferProgress.Text = "";
					lblProgress.Text = "Disconnected";
					btnConnect.Text = "Connect";
					listServer.Items.Clear();
					break;
				case SftpState.Ready:
					lblProgress.Text = "Ready";
					break;
			}
		}

		/// <summary>
		/// event displaying transfer progress
		/// </summary>
		private void TransferProgress(object sender, SftpTransferProgressEventArgs e)
		{
			if (e.BytesSinceLastEvent > 0)
			{
				_currentBytesTransferred += e.BytesSinceLastEvent;

				// only update progress bar and byte counter once per second
				DateTime now = DateTime.Now;
				TimeSpan ts = now - _lastTransferProgressTime;
				if (ts.TotalSeconds < 0.5)
					return;
				_lastTransferProgressTime = now;

				lblProgress.Text = e.BytesTransferred + " bytes";

				int percentage = pbTransfer.Maximum;
				if (_currentFileLength > 0)
				{
					decimal index = (decimal)_currentBytesTransferred / (decimal)_currentFileLength;
					if (index < 1)
						percentage = (int)(index * pbTransfer.Maximum);
				}
				
				if (pbTransfer.Value != percentage)
					pbTransfer.Value = percentage;
			}
		}

		/// <summary>
		/// handles the batch transfer progress event
		/// </summary>
		private void BatchTransferProgress(object sender, SftpBatchTransferProgressEventArgs e)
		{
			string strBatchInfo = string.Format("({0} / {1} file{2} processed)    ", 
				e.FilesProcessed, e.FilesTotal, (e.FilesProcessed > 1 ? "s" : null));

			switch (e.Operation)
			{
				case SftpBatchTransferOperation.HierarchyRetrievalStarted:
					strBatchInfo = "Retrieving hierarchy...";
					break;
				case SftpBatchTransferOperation.HierarchyRetrievalFailed: 
					strBatchInfo = "Retrieve hierarchy failed.";
					break;
				case SftpBatchTransferOperation.HierarchyRetrieved:
					strBatchInfo = string.Format("Hierarchy retrieved ({0} byte{1} in {2} file{3}).", 
						e.BytesTotal, (e.BytesTotal > 1 ? "s" : null),
						e.FilesTotal, (e.FilesTotal > 1 ? "s" : null));
					break;
				case SftpBatchTransferOperation.DirectoryProcessingStarted: 
					strBatchInfo += "Processing directory...";
					break;
				case SftpBatchTransferOperation.DirectoryProcessingFailed: 
					strBatchInfo += "Directory processing failed.";
					break;
				case SftpBatchTransferOperation.DirectorySkipped: 
					strBatchInfo += "Directory skipped.";
					break;
				case SftpBatchTransferOperation.DirectoryCreated: 
					strBatchInfo += "Directory created.";
					break;
				case SftpBatchTransferOperation.FileProcessingStarted: 
					strBatchInfo += "Processing file...";
					break;
				case SftpBatchTransferOperation.FileTransferStarting:
					strBatchInfo += "Transferring file...";
					pbTransfer.Value = 0;
					_currentBytesTransferred = 0;
					_currentFileLength = e.CurrentFileLength;
					break;
				case SftpBatchTransferOperation.FileProcessingFailed: 
					strBatchInfo += "File processing failed.";
					break;
				case SftpBatchTransferOperation.FileSkipped:
					strBatchInfo += "File skipped.";
					break;
				case SftpBatchTransferOperation.FileTransferred: 
					strBatchInfo += "File transferred.";
					_totalFilesTransferred++;
					_totalBytesTransferred += _currentBytesTransferred;
					break;
			}

			lblBatchTransferProgress.Text = strBatchInfo;
		}

		/// <summary>
		/// Handles the batch transfer problem detected event
		/// </summary>
		private void BatchTransferProblemDetected(object sender, SftpBatchTransferProblemDetectedEventArgs e)
		{
			_problemForm.ShowModal(this, e);
		}

		private void StateChangedProxy(object sender, SftpStateChangedEventArgs e)
		{
			SafeInvoke(new SftpStateChangedEventHandler(StateChanged), new object[] { sender, e });
		}

		private void TransferProgressProxy(object sender, SftpTransferProgressEventArgs e)
		{
			SafeInvoke(new SftpTransferProgressEventHandler(TransferProgress), new object[] { sender, e });
		}

		private void BatchTransferProgressProxy(object sender, SftpBatchTransferProgressEventArgs e)
		{
			SafeInvoke(new SftpBatchTransferProgressEventHandler(BatchTransferProgress), new object[] { sender, e });
		}

		private void BatchTransferProblemDetectedProxy(object sender, SftpBatchTransferProblemDetectedEventArgs e)
		{
			SafeInvoke(new SftpBatchTransferProblemDetectedEventHandler(BatchTransferProblemDetected), new object[] { sender, e });
		}

		private void AuthenticationRequestProxy(object sender, SshAuthenticationRequestEventArgs e)
		{
			SafeInvoke(new SshAuthenticationRequestEventHandler(AuthenticationRequest), new object[] { sender, e });
		}

		/// <summary>
		/// make local list + drives combo box
		/// </summary>
		public void MakeLocalList()
		{
			int dirs = 0;
			int files = 0;
			long size = 0;

			// make a logical drives list [first run only]

			if (cbDrives.Items.Count <= 0)
			{
				string[] drives = Directory.GetLogicalDrives();
				string lowerDrive = @"c:\";

				// find "lower" acceptable drive

				for (int c = drives.Length - 1; c >= 0; c--)
				{
					if (drives[c].ToLower().CompareTo(@"c:\") >= 0) lowerDrive = drives[c].ToLower();
				}

				// feed the drives' list and select one

				for (int c = 0; c < drives.Length; c++)
				{
					string drive = drives[c];

					cbDrives.Items.Add(drive);

					if (_selectedDrive == -1 && drive.ToLower().CompareTo(lowerDrive) == 0)
					{
						cbDrives.SelectedIndex = c;
						_selectedDrive = c;
					}
				}

				if (cbDrives.SelectedIndex <= 0) cbDrives.SelectedIndex = 0;
			}

			// create the list
			listLocal.Items.Clear();

			// directory up
			if (_localPath != null && _localPath.Length > 3)
			{
				listLocal.Items.Add(new ListViewItem("..", 0));
				dirs++;
			}
			else
			{
				_localPath = cbDrives.SelectedItem.ToString();
			}

			DirectoryInfo directory = new DirectoryInfo(_localPath);
			DirectoryInfo[] list = directory.GetDirectories();

			for (int c = 0; c < list.Length; c++)
			{
				string[] row = new string[3];
				row[0] = list[c].Name;
				row[1] = "";

				// time [on read-only devices LWT is not available for dirs?]

				try
				{
					row[2] = FormatFileTime(list[c].LastWriteTime);
				}
				catch
				{
					row[2] = "";
				}

				listLocal.Items.Add(new ListViewItem(row, 1 + c));
				dirs++;
			}

			_localDirs = dirs;

			// files

			FileInfo[] list2 = directory.GetFiles();

			for (int c = 0; c < list2.Length; c++)
			{
				string[] row = new string[3];
				row[0] = list2[c].Name;
				row[1] = list2[c].Length.ToString();
				row[2] = FormatFileTime(list2[c].LastWriteTime);

				listLocal.Items.Add(new ListViewItem(row, 1 + dirs + files));
				size += list2[c].Length;
				files++;
			}

			// stats

			lblLocal.Text = dirs + " dir" + (dirs > 1 ? "s" : "") + " " +
				files + " file" + (files > 1 ? "s" : "") + " " +
				size / 1024 + " K";

			// working directory

			lblLocalDirectory.Text = directory.FullName;
			_localPath = directory.FullName;
		}

		/// <summary>
		/// make sftp list
		/// </summary>
		public void MakeSftpList()
		{
			int dirs = 0;
			int files = 0;
			long size = 0;

			listServer.Items.Clear();

			// not connected?

			if (_sftp == null)
			{
				lblServerDirectory.Text = "";
				lblServer.Text = "";
				_serverDirs = 0;
				return;
			}

			// make the list

			SftpItemCollection list = null;

			try
			{
				_isWorking = true;
				btnStop.Visible = true;
				IAsyncResult ar = _sftp.BeginGetList(null, null, null);
				while (!ar.IsCompleted)
				{
					Application.DoEvents();
					System.Threading.Thread.Sleep(1);
				}
				list = _sftp.EndGetList(ar);
				list.Sort();
			}
			catch (Exception ex)
			{
				WriteToLog(ex);
				listServer.Items.Add(new ListViewItem("..", 0));
				return;
			}
			finally
			{
				btnStop.Visible = false;
				_isWorking = false;
			}

			// directories

			listServer.Items.Add(new ListViewItem("..", 0));
			dirs++;

			for (int c = 0; c < list.Count; c++)
			{
				// normal directory

				if (list[c].IsDirectory)
				{
					string[] row = new string[3];
					row[0] = list[c].Name;
					row[1] = null;
					row[2] = FormatFileTime(list[c].Modified);

					listServer.Items.Add(new ListViewItem(row, 1 + dirs));
					dirs++;
				}
			}

			// symlinks

			for (int c = 0; c < list.Count; c++)
			{
				// symlink

				if (list[c].IsSymlink)
				{
					string[] row = new string[3];
					row[0] = list[c].Name;
					row[1] = SYMLINK;
					row[2] = FormatFileTime(list[c].Modified);

					listServer.Items.Add(new ListViewItem(row, 1 + dirs + files));
					files++;
				}
			}

			// files

			for (int c = 0; c < list.Count; c++)
			{
				// normal file

				if (list[c].IsFile)
				{
					string[] row = new string[3];
					row[0] = list[c].Name;
					row[1] = list[c].Size.ToString();
					row[2] = FormatFileTime(list[c].Modified);
					listServer.Items.Add(new ListViewItem(row, 1 + dirs + files));
					size += list[c].Size;
					files++;
				}
			}

			// stats

			lblServer.Text = dirs + " dir" + (dirs > 1 ? "s" : "") + " " +
				files + " file" + (files > 1 ? "s" : "") + " " +
				size / 1024 + " K";

			// working directory

			lblServerDirectory.Text = _sftp.GetCurrentDirectory();

			_serverDirs = dirs;
		}

		/// <summary>
		/// constructor
		/// </summary>
		public MainForm()
		{
#if (!DOTNET10 && !DOTNET11)
			Application.EnableVisualStyles();
			CheckForIllegalCrossThreadCalls = true;
#endif
			InitializeComponent();

			Width = 640;
			SetRightSizeValues();

			trackBar.Enabled = false;
			lblServer.Text = "";
			listLocal.Focus();
			btnConnect.Select();
			
			MakeLocalList();
			_problemForm = new ProblemDetectedForm();
		}

		/// <summary>
		/// clean up any resources being used
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
			this.listServer = new System.Windows.Forms.ListView();
			this.name = new System.Windows.Forms.ColumnHeader();
			this.size = new System.Windows.Forms.ColumnHeader();
			this.date = new System.Windows.Forms.ColumnHeader();
			this.listLocal = new System.Windows.Forms.ListView();
			this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
			this.tbLog = new System.Windows.Forms.RichTextBox();
			this.lblServer = new System.Windows.Forms.Label();
			this.lblServerDirectory = new System.Windows.Forms.Label();
			this.lblLocal = new System.Windows.Forms.Label();
			this.lblLocalDirectory = new System.Windows.Forms.Label();
			this.pbTransfer = new System.Windows.Forms.ProgressBar();
			this.lblProgress = new System.Windows.Forms.Label();
			this.btnStop = new System.Windows.Forms.Button();
			this.btnDelete = new System.Windows.Forms.Button();
			this.btnUpload = new System.Windows.Forms.Button();
			this.btnDownload = new System.Windows.Forms.Button();
			this.btnRefresh = new System.Windows.Forms.Button();
			this.btnMakeDir = new System.Windows.Forms.Button();
			this.cbDrives = new System.Windows.Forms.ComboBox();
			this.btnConnect = new System.Windows.Forms.Button();
			this.gbControl = new System.Windows.Forms.GroupBox();
			this.headerLeft = new System.Windows.Forms.PictureBox();
			this.headerMiddle = new System.Windows.Forms.Panel();
			this.headerRight = new System.Windows.Forms.PictureBox();
			this.lblBatchTransferProgress = new System.Windows.Forms.Label();
			this.trackBar = new System.Windows.Forms.TrackBar();
			this.lblSpeed = new System.Windows.Forms.Label();
			this.gbControl.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.headerLeft)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.headerRight)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.trackBar)).BeginInit();
			this.SuspendLayout();
			// 
			// listServer
			// 
			this.listServer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)));
			this.listServer.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.name,
            this.size,
            this.date});
			this.listServer.FullRowSelect = true;
			this.listServer.Location = new System.Drawing.Point(12, 80);
			this.listServer.MultiSelect = false;
			this.listServer.Name = "listServer";
			this.listServer.Size = new System.Drawing.Size(264, 310);
			this.listServer.TabIndex = 2;
			this.listServer.UseCompatibleStateImageBehavior = false;
			this.listServer.View = System.Windows.Forms.View.Details;
			this.listServer.DoubleClick += new System.EventHandler(this.listServer_DoubleClick);
			this.listServer.Enter += new System.EventHandler(this.listServer_Enter);
			this.listServer.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.listServer_KeyPress);
			// 
			// name
			// 
			this.name.Text = "Name";
			this.name.Width = 120;
			// 
			// size
			// 
			this.size.Text = "Size";
			this.size.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// date
			// 
			this.date.Text = "Date";
			this.date.Width = 120;
			// 
			// listLocal
			// 
			this.listLocal.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.listLocal.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
			this.listLocal.FullRowSelect = true;
			this.listLocal.Location = new System.Drawing.Point(391, 80);
			this.listLocal.MultiSelect = false;
			this.listLocal.Name = "listLocal";
			this.listLocal.Size = new System.Drawing.Size(256, 342);
			this.listLocal.TabIndex = 25;
			this.listLocal.UseCompatibleStateImageBehavior = false;
			this.listLocal.View = System.Windows.Forms.View.Details;
			this.listLocal.DoubleClick += new System.EventHandler(this.listLocal_DoubleClick);
			this.listLocal.Enter += new System.EventHandler(this.listLocal_Enter);
			this.listLocal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.listLocal_KeyPress);
			// 
			// columnHeader1
			// 
			this.columnHeader1.Text = "Name";
			this.columnHeader1.Width = 120;
			// 
			// columnHeader2
			// 
			this.columnHeader2.Text = "Size";
			this.columnHeader2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// columnHeader3
			// 
			this.columnHeader3.Text = "Date";
			this.columnHeader3.Width = 120;
			// 
			// tbLog
			// 
			this.tbLog.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
			this.tbLog.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.tbLog.BackColor = System.Drawing.SystemColors.Control;
			this.tbLog.Cursor = System.Windows.Forms.Cursors.IBeam;
			this.tbLog.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.tbLog.Location = new System.Drawing.Point(0, 447);
			this.tbLog.MaxLength = 1000000;
			this.tbLog.Name = "tbLog";
			this.tbLog.ReadOnly = true;
			this.tbLog.Size = new System.Drawing.Size(659, 94);
			this.tbLog.TabIndex = 8;
			this.tbLog.Text = "";
			this.tbLog.WordWrap = false;
			// 
			// lblServer
			// 
			this.lblServer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.lblServer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblServer.Location = new System.Drawing.Point(12, 393);
			this.lblServer.Name = "lblServer";
			this.lblServer.Size = new System.Drawing.Size(153, 13);
			this.lblServer.TabIndex = 9;
			this.lblServer.Text = "12345";
			// 
			// lblServerDirectory
			// 
			this.lblServerDirectory.BackColor = System.Drawing.SystemColors.InactiveCaption;
			this.lblServerDirectory.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.lblServerDirectory.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblServerDirectory.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.lblServerDirectory.Location = new System.Drawing.Point(12, 64);
			this.lblServerDirectory.Name = "lblServerDirectory";
			this.lblServerDirectory.Size = new System.Drawing.Size(264, 16);
			this.lblServerDirectory.TabIndex = 1;
			this.lblServerDirectory.Text = "/root";
			this.lblServerDirectory.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.lblServerDirectory.Click += new System.EventHandler(this.listServer_Enter);
			// 
			// lblLocal
			// 
			this.lblLocal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.lblLocal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblLocal.Location = new System.Drawing.Point(391, 425);
			this.lblLocal.Name = "lblLocal";
			this.lblLocal.Size = new System.Drawing.Size(256, 19);
			this.lblLocal.TabIndex = 12;
			this.lblLocal.Text = "12345";
			// 
			// lblLocalDirectory
			// 
			this.lblLocalDirectory.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.lblLocalDirectory.BackColor = System.Drawing.SystemColors.InactiveCaption;
			this.lblLocalDirectory.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.lblLocalDirectory.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblLocalDirectory.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.lblLocalDirectory.Location = new System.Drawing.Point(391, 64);
			this.lblLocalDirectory.Name = "lblLocalDirectory";
			this.lblLocalDirectory.Size = new System.Drawing.Size(256, 16);
			this.lblLocalDirectory.TabIndex = 23;
			this.lblLocalDirectory.Text = "/root";
			this.lblLocalDirectory.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.lblLocalDirectory.Click += new System.EventHandler(this.listLocal_Enter);
			// 
			// pbTransfer
			// 
			this.pbTransfer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.pbTransfer.Location = new System.Drawing.Point(0, 541);
			this.pbTransfer.Name = "pbTransfer";
			this.pbTransfer.Size = new System.Drawing.Size(266, 20);
			this.pbTransfer.Step = 1;
			this.pbTransfer.TabIndex = 20;
			// 
			// lblProgress
			// 
			this.lblProgress.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.lblProgress.BackColor = System.Drawing.Color.Transparent;
			this.lblProgress.Location = new System.Drawing.Point(280, 544);
			this.lblProgress.Name = "lblProgress";
			this.lblProgress.Size = new System.Drawing.Size(94, 20);
			this.lblProgress.TabIndex = 24;
			this.lblProgress.Text = "Welcome!";
			this.lblProgress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// btnStop
			// 
			this.btnStop.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnStop.Location = new System.Drawing.Point(16, 232);
			this.btnStop.Name = "btnStop";
			this.btnStop.Size = new System.Drawing.Size(75, 23);
			this.btnStop.TabIndex = 17;
			this.btnStop.Text = "Abort";
			this.btnStop.Visible = false;
			this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
			// 
			// btnDelete
			// 
			this.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnDelete.Location = new System.Drawing.Point(16, 132);
			this.btnDelete.Name = "btnDelete";
			this.btnDelete.Size = new System.Drawing.Size(75, 23);
			this.btnDelete.TabIndex = 11;
			this.btnDelete.Text = "Delete";
			this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
			// 
			// btnUpload
			// 
			this.btnUpload.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnUpload.Location = new System.Drawing.Point(16, 208);
			this.btnUpload.Name = "btnUpload";
			this.btnUpload.Size = new System.Drawing.Size(75, 23);
			this.btnUpload.TabIndex = 15;
			this.btnUpload.Text = "< Upload";
			this.btnUpload.Click += new System.EventHandler(this.btnUpload_Click);
			// 
			// btnDownload
			// 
			this.btnDownload.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnDownload.Location = new System.Drawing.Point(16, 184);
			this.btnDownload.Name = "btnDownload";
			this.btnDownload.Size = new System.Drawing.Size(75, 23);
			this.btnDownload.TabIndex = 13;
			this.btnDownload.Text = "Download >";
			this.btnDownload.Click += new System.EventHandler(this.btnDownload_Click);
			// 
			// btnRefresh
			// 
			this.btnRefresh.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnRefresh.Location = new System.Drawing.Point(16, 72);
			this.btnRefresh.Name = "btnRefresh";
			this.btnRefresh.Size = new System.Drawing.Size(75, 23);
			this.btnRefresh.TabIndex = 5;
			this.btnRefresh.Text = "Refresh";
			this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
			// 
			// btnMakeDir
			// 
			this.btnMakeDir.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnMakeDir.Location = new System.Drawing.Point(17, 102);
			this.btnMakeDir.Name = "btnMakeDir";
			this.btnMakeDir.Size = new System.Drawing.Size(75, 23);
			this.btnMakeDir.TabIndex = 7;
			this.btnMakeDir.Text = "Create folder";
			this.btnMakeDir.Click += new System.EventHandler(this.btnMakeDir_Click);
			// 
			// cbDrives
			// 
			this.cbDrives.ItemHeight = 13;
			this.cbDrives.Location = new System.Drawing.Point(17, 40);
			this.cbDrives.Name = "cbDrives";
			this.cbDrives.Size = new System.Drawing.Size(73, 21);
			this.cbDrives.TabIndex = 4;
			this.cbDrives.SelectedIndexChanged += new System.EventHandler(this.cbDrives_SelectedIndexChanged);
			// 
			// btnConnect
			// 
			this.btnConnect.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnConnect.Location = new System.Drawing.Point(16, 16);
			this.btnConnect.Name = "btnConnect";
			this.btnConnect.Size = new System.Drawing.Size(75, 23);
			this.btnConnect.TabIndex = 3;
			this.btnConnect.Text = "Connect...";
			this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
			// 
			// gbControl
			// 
			this.gbControl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
			this.gbControl.Controls.Add(this.btnStop);
			this.gbControl.Controls.Add(this.btnDelete);
			this.gbControl.Controls.Add(this.btnUpload);
			this.gbControl.Controls.Add(this.btnDownload);
			this.gbControl.Controls.Add(this.btnRefresh);
			this.gbControl.Controls.Add(this.btnMakeDir);
			this.gbControl.Controls.Add(this.cbDrives);
			this.gbControl.Controls.Add(this.btnConnect);
			this.gbControl.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.gbControl.Location = new System.Drawing.Point(281, 64);
			this.gbControl.Name = "gbControl";
			this.gbControl.Size = new System.Drawing.Size(106, 358);
			this.gbControl.TabIndex = 25;
			this.gbControl.TabStop = false;
			this.gbControl.Text = "Controls";
			// 
			// headerLeft
			// 
			this.headerLeft.BackColor = System.Drawing.Color.White;
			this.headerLeft.Image = ((System.Drawing.Image)(resources.GetObject("headerLeft.Image")));
			this.headerLeft.Location = new System.Drawing.Point(0, 0);
			this.headerLeft.Name = "headerLeft";
			this.headerLeft.Size = new System.Drawing.Size(472, 59);
			this.headerLeft.TabIndex = 26;
			this.headerLeft.TabStop = false;
			// 
			// headerMiddle
			// 
			this.headerMiddle.BackColor = System.Drawing.Color.White;
			this.headerMiddle.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("headerMiddle.BackgroundImage")));
			this.headerMiddle.Location = new System.Drawing.Point(472, 0);
			this.headerMiddle.Name = "headerMiddle";
			this.headerMiddle.Size = new System.Drawing.Size(64, 59);
			this.headerMiddle.TabIndex = 27;
			// 
			// headerRight
			// 
			this.headerRight.BackColor = System.Drawing.Color.White;
			this.headerRight.Cursor = System.Windows.Forms.Cursors.Hand;
			this.headerRight.Image = ((System.Drawing.Image)(resources.GetObject("headerRight.Image")));
			this.headerRight.Location = new System.Drawing.Point(536, 0);
			this.headerRight.Name = "headerRight";
			this.headerRight.Size = new System.Drawing.Size(136, 59);
			this.headerRight.TabIndex = 28;
			this.headerRight.TabStop = false;
			this.headerRight.Click += new System.EventHandler(this.headerRight_Click);
			// 
			// lblBatchTransferProgress
			// 
			this.lblBatchTransferProgress.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.lblBatchTransferProgress.BackColor = System.Drawing.Color.Transparent;
			this.lblBatchTransferProgress.Location = new System.Drawing.Point(374, 544);
			this.lblBatchTransferProgress.Name = "lblBatchTransferProgress";
			this.lblBatchTransferProgress.Size = new System.Drawing.Size(264, 20);
			this.lblBatchTransferProgress.TabIndex = 29;
			this.lblBatchTransferProgress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// trackBar
			// 
			this.trackBar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.trackBar.AutoSize = false;
			this.trackBar.Location = new System.Drawing.Point(11, 409);
			this.trackBar.Maximum = 100;
			this.trackBar.Minimum = 1;
			this.trackBar.Name = "trackBar";
			this.trackBar.Size = new System.Drawing.Size(265, 32);
			this.trackBar.TabIndex = 30;
			this.trackBar.TickFrequency = 5;
			this.trackBar.Value = 100;
			this.trackBar.Scroll += new System.EventHandler(this.trackBar_Scroll);
			// 
			// lblSpeed
			// 
			this.lblSpeed.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.lblSpeed.Location = new System.Drawing.Point(171, 393);
			this.lblSpeed.Name = "lblSpeed";
			this.lblSpeed.Size = new System.Drawing.Size(105, 13);
			this.lblSpeed.TabIndex = 31;
			this.lblSpeed.Text = "Speed: Unlimited";
			// 
			// MainForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(659, 564);
			this.Controls.Add(this.lblSpeed);
			this.Controls.Add(this.trackBar);
			this.Controls.Add(this.lblBatchTransferProgress);
			this.Controls.Add(this.lblServerDirectory);
			this.Controls.Add(this.headerRight);
			this.Controls.Add(this.headerMiddle);
			this.Controls.Add(this.headerLeft);
			this.Controls.Add(this.gbControl);
			this.Controls.Add(this.lblProgress);
			this.Controls.Add(this.pbTransfer);
			this.Controls.Add(this.lblLocalDirectory);
			this.Controls.Add(this.lblLocal);
			this.Controls.Add(this.lblServer);
			this.Controls.Add(this.tbLog);
			this.Controls.Add(this.listLocal);
			this.Controls.Add(this.listServer);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.KeyPreview = true;
			this.MinimumSize = new System.Drawing.Size(664, 590);
			this.Name = "MainForm";
			this.Text = "Simple WinForm SFTP Client";
			this.Closing += new System.ComponentModel.CancelEventHandler(this.MainForm_Closing);
			this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MainForm_KeyPress);
			this.Resize += new System.EventHandler(this.MainForm_Resize);
			this.gbControl.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.headerLeft)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.headerRight)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.trackBar)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// win form client starts here
		/// </summary>
		[STAThread]
		static void Main()
		{
			Application.EnableVisualStyles();
			Application.Run(new MainForm());
		}

		/// <summary>
		/// connect/disconnect sftp server
		/// </summary>
		private void btnConnect_Click(object sender, System.EventArgs e)
		{
			if (_sftp != null)
			{
				if (_isWorking)
					return;

				// disconnect

				trackBar.Enabled = false;
				_sftp.Disconnect();
				_sftp.Dispose();
				_sftp = null;
				btnConnect.Text = "Connect";
				MakeSftpList(); // clearing
				return;
			}

			// connection dialog

			WinFormClient.Connection con = new WinFormClient.Connection();

			// show connection dialog

			con.ShowDialog();

			// not confirmed

			if (!con.OK)
				return;

			// create sftp object

			_sftp = new Sftp();

			// assign the logger for logging CommandSent and ResponseRead events (and exceptions thrown by Sftp class)

			_sftp.LogWriter = new RichTextBoxLogWriter(tbLog, tbLog.MaxLength, con.LogLevel);

			if (con.UseLargeBuffers)
			{
				_sftp.Options |= SftpOptions.UseLargeBuffers;
			}

			// set event handlers

			_sftp.TransferProgress += new SftpTransferProgressEventHandler(TransferProgressProxy);
			_sftp.StateChanged += new SftpStateChangedEventHandler(StateChangedProxy);
			_sftp.BatchTransferProgress += new SftpBatchTransferProgressEventHandler(BatchTransferProgressProxy);
			_sftp.BatchTransferProblemDetected += new SftpBatchTransferProblemDetectedEventHandler(BatchTransferProblemDetectedProxy);
			_sftp.AuthenticationRequest += new SshAuthenticationRequestEventHandler(AuthenticationRequestProxy);

			// connect

			try
			{
				_sftp.Timeout = 30000;

				// proxy

				if (con.ProxyEnabled)
				{
					if (con.ProxyLogin.Length > 0)
					{
						if (con.ProxyPassword.Length == 0)
							con.ProxyPassword = null;

						_sftp.Proxy = new Proxy(con.ProxyType, ProxyAuthentication.Basic, con.Proxy, con.ProxyPort, new System.Net.NetworkCredential(con.ProxyLogin, con.ProxyPassword));
					}
					else
					{
						_sftp.Proxy = new Proxy(con.ProxyType, con.Proxy, con.ProxyPort);
					}
				}

				try
				{
					btnConnect.Visible = true;
					_isWorking = true;

					IAsyncResult ar = _sftp.BeginConnect(con.Host, con.Port, null, null, null);
					while (!ar.IsCompleted)
					{
						Application.DoEvents();
						Thread.Sleep(1);
					}
					_sftp.EndConnect(ar);
				}
				finally
				{
					btnStop.Visible = false;
					_isWorking = false;
				}

				// login

				trackBar.Enabled = true;

				if (con.PrivateKey != "")
				{
					PassphraseDialog pp = new PassphraseDialog();
					if (pp.ShowDialog() == DialogResult.OK)
					{
						SshPrivateKey privateKey = new SshPrivateKey(con.PrivateKey, pp.Passphrase);
						
						if (con.Password == "")
						{
							_sftp.Login(con.Login, null, privateKey);
						}
						else
						{
							_sftp.Login(con.Login, con.Password, privateKey);
						}
					}
					else
					{
						throw new Exception("Passphrase wasn't entered.");
					}
				}
				else
				{
					_sftp.Login(con.Login, con.Password);
				}

				btnConnect.Text = "Disconnect";
				MakeSftpList();
			}
			catch (Exception ex)
			{
				WriteToLog(ex);
				_sftp.Dispose();
				_sftp = null;
				return;
			}
			finally
			{
				_isWorking = false;
			}
		}

		private void AuthenticationRequest(object sender, SshAuthenticationRequestEventArgs e)
		{
			// get responses from user
			AuthenticationRequestDialog dialog = new AuthenticationRequestDialog(e);
			DialogResult result = dialog.ShowDialog();

			// process result
			if (result != DialogResult.OK)
				e.Cancel = true;
		}

		/// <summary>
		/// disconnect on exit
		/// </summary>
		private void MainForm_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (_sftp != null)
				_sftp.Dispose();
		}

		/// <summary>
		/// CWD or GET file command on server side
		/// </summary>
		private void ListServerClick(bool filesOnly)
		{
			try
			{
				if (listServer.Items.Count < 1)
					return;

				if (listServer.SelectedItems.Count < 1)
					listServer.Items[0].Selected = true;

				listServer.Focus();

				// sftp state test

				if (_isWorking)
					return;

				int index = listServer.SelectedIndices[0];
				ListViewItem item = listServer.SelectedItems[0];

				// parse listitem

				bool file = (index >= _serverDirs);
				string path = item.Text;

				// symlink click
				if (item.SubItems.Count > 1 && item.SubItems[1].Text == SYMLINK)
				{
					path = _sftp.ResolveSymlink(path);
					SftpItem info = _sftp.GetInfo(path);
					switch (info.Type)
					{
						case SftpItemType.Directory:
							file = false;
							break;
						case SftpItemType.RegularFile:
							file = true;
							break;
						default:
							WriteToLog("* unsupported symlink type", RichTextBoxLogWriter.COLORERROR);
							return;
					}
				}

				if (file)
				{
					// GET file

					DownloadFiles(path, _localPath);
				}
				else
				{
					string directory;
					if (index == 0)
						directory = "..";
					else
						directory = path;
						
					// CWD

					_sftp.ChangeDirectory(directory);
					MakeSftpList();
				}
			}
			catch (Exception ex)
			{
				WriteToLog(ex);
			}
		}

		private void listServer_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			if (e.KeyChar == '\r')
			{
				ListServerClick(false);
				listServer.Focus();
			}
		}

		private void listServer_DoubleClick(object sender, System.EventArgs e)
		{
			ListServerClick(false);
			listServer.Focus();
		}

		/// <summary>
		/// change drive letter
		/// </summary>
		private void cbDrives_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			try
			{
				_localPath = cbDrives.SelectedItem.ToString();
				MakeLocalList();
			}
			catch (Exception ex)
			{
				WriteToLog(ex);
				listLocal.Items.Clear();
				lblLocalDirectory.Text = lblLocal.Text = null;
			}
		}

		/// <summary>
		/// LCD or PUT command on local side
		/// </summary>
		private void ListLocalClick()
		{
			try
			{
				if (listLocal.Items.Count < 1) 
					return;

				if (listLocal.SelectedItems.Count < 1) 
					listLocal.Items[0].Selected = true;

				listLocal.Focus();

				int index = listLocal.SelectedIndices[0];
				ListViewItem item = listLocal.SelectedItems[0];

				if (index >= _localDirs)
				{
					// upload file

					UploadFiles(item.Text, _localPath);
				}
				else
				{
					// local change directory

					_localPath = Path.Combine(_localPath, item.Text);
					MakeLocalList();
				}
			}
			catch (Exception ex)
			{
				WriteToLog(ex);
			}
		}

		private void listLocal_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			if (e.KeyChar == '\r')
			{
				ListLocalClick();
				if (listLocal.Items.Count > 0)
				{
					listLocal.Focus();
				}
			}
		}

		private void listLocal_DoubleClick(object sender, System.EventArgs e)
		{
			ListLocalClick();
			listLocal.Focus();
		}

		/// <summary>
		/// make dir
		/// </summary>
		private void btnMakeDir_Click(object sender, System.EventArgs e)
		{
			try
			{
				MakeDir dir = new MakeDir();

				// SFTP: MKDIR

				if (listActive == listServer)
				{
					if (_isWorking)
						return;

					dir.ShowDialog();
					if (dir.Directory != null)
					{
						_sftp.CreateDirectory(dir.Directory);
						MakeSftpList();
					}
				}
				else
				{
					// LOCAL: MKDIR

					dir.ShowDialog();

					if (dir.Directory != null)
					{
						Directory.CreateDirectory(Path.Combine(_localPath, dir.Directory));
						MakeLocalList();
					}
				}
			}
			catch (Exception ex)
			{
				WriteToLog(ex);
			}
		}

		/// <summary>
		/// refresh listing
		/// </summary>
		private void btnRefresh_Click(object sender, System.EventArgs e)
		{
			try
			{
				if (listActive == listServer)
				{
					if (_isWorking)
						return;

					MakeSftpList();
				}
				else
				{
					MakeLocalList();
				}
			}
			catch (Exception ex)
			{
				WriteToLog(ex);
			}
		}

		/// <summary>
		/// download file or directory tree
		/// </summary>
		/// <param name="fileName">remote file name</param>
		/// <param name="localPath">local path</param>
		private void DownloadFiles(string fileName, string localPath)
		{
			// sftp state test

			if (_isWorking)
				return;

			// transfer

			try
			{
				_isWorking = true;
				btnStop.Visible = true;
				_problemForm.Initialize();
				_totalFilesTransferred = 0;
				_totalBytesTransferred = 0;
				_transferTime = DateTime.Now;
				_lastTransferProgressTime = _transferTime;

				_sftp.BeginGetFiles(fileName, localPath, 
					SftpBatchTransferOptions.Recursive, SftpActionOnExistingFiles.ThrowException, 
					new AsyncCallback(DownloadCallback), null);
			}
			catch (Exception ex)
			{
				btnStop.Visible = false;
				_isWorking = false;
				WriteToLog(ex);
			}
		}

		/// <summary>
		/// upload file or directory tree
		/// </summary>
		/// <param name="fileName">local file name</param>
		/// <param name="localPath">local path</param>
		private void UploadFiles(string fileName, string localPath)
		{
			localPath = Path.Combine(localPath, fileName);

			// sftp state test

			if (_isWorking)
				return;

			// transfer

			try
			{
				_isWorking = true;
				btnStop.Visible = true;
				_problemForm.Initialize();
				_totalFilesTransferred = 0;
				_totalBytesTransferred = 0;
				_transferTime = DateTime.Now;
				_lastTransferProgressTime = _transferTime;

				_sftp.BeginPutFiles(localPath, ".", 
					SftpBatchTransferOptions.Recursive, SftpActionOnExistingFiles.ThrowException, 
					new AsyncCallback(UploadCallback), null);
			}
			catch (Exception ex)
			{
				btnStop.Visible = false;
				_isWorking = false;
				WriteToLog(ex);
			}
		}

		/// <summary>
		/// callback for download finished
		/// </summary>
		public void DownloadCallback(IAsyncResult asyncResult)
		{
			try
			{
				_sftp.EndGetFiles(asyncResult);
				SafeInvoke(new TransferFinishedDelegate(TransferFinished), new object[] { null, false });
			}
			catch (Exception ex)
			{
				SafeInvoke(new TransferFinishedDelegate(TransferFinished), new object[] { ex, false });
			}
		}

		/// <summary>
		/// callback for upload finished
		/// </summary>
		public void UploadCallback(IAsyncResult asyncResult)
		{
			try
			{
				_sftp.EndPutFiles(asyncResult);
				SafeInvoke(new TransferFinishedDelegate(TransferFinished), new object[] { null, true });
			}
			catch (Exception ex)
			{
				SafeInvoke(new TransferFinishedDelegate(TransferFinished), new object[] { ex, true });
			}
		}

		private delegate void TransferFinishedDelegate(Exception error, bool refreshSftp);

		private void TransferFinished(Exception error, bool refreshSftp)
		{
			if (error != null)
			{
				WriteToLog(error);

				SftpException ex = error as SftpException;
				if (ex != null && ex.Status == SftpExceptionStatus.OperationAborted)
					_totalBytesTransferred += ex.Transferred;
			}

			ShowTransferStatus();

			try
			{
				if (refreshSftp)
					MakeSftpList();
				else
					MakeLocalList();
			}
			catch (Exception ex)
			{
				WriteToLog(ex);
			}

			_isWorking = false;
			pbTransfer.Value = 0;
			btnStop.Visible = false;
			lblBatchTransferProgress.Text = "";
		}

		/// <summary>
		/// show transfer status: files, bytes, time, speed
		/// </summary>
		private void ShowTransferStatus()
		{
			// unknown bytes transferred
			if (_totalBytesTransferred == 0)
				return;

			// files and bytes transferred
			string outstring = string.Format("{0} file{1} ({2} byte{3}) transferred in",
				_totalFilesTransferred, (_totalFilesTransferred > 1 ? "s" : null),
				_totalBytesTransferred, (_totalBytesTransferred > 1 ? "s" : null));

			// time spent
			TimeSpan ts = DateTime.Now - _transferTime;

			// speed
			if (ts.TotalSeconds > 1)
			{
				outstring += (ts.Days > 0 ? " " + ts.Days + " day" + (ts.Days > 1 ? "s" : null) : null);
				outstring += (ts.Hours > 0 ? " " + ts.Hours + " hour" + (ts.Hours > 1 ? "s" : null) : null);
				outstring += (ts.Minutes > 0 ? " " + ts.Minutes + " min" + (ts.Minutes > 1 ? "s" : null) : null);
				outstring += (ts.Seconds > 0 ? " " + ts.Seconds + " sec" + (ts.Seconds > 1 ? "s" : null) : null);
			}
			else
			{
				outstring += " " + ts.TotalSeconds + " sec";
			}

			double speed = _totalBytesTransferred / ts.TotalSeconds;
			if (speed < 1)
				outstring += string.Format(" at {0:F3} B/s", speed);
			else if (speed < 1024)
				outstring += string.Format(" at {0:F0} B/s", speed);
			else
				outstring += string.Format(" at {0:F0} KB/s", speed / 1024);


			WriteToLog("> " + outstring, RichTextBoxLogWriter.COLORCOMMAND);
		}

		/// <summary>
		/// handle download button click
		/// </summary>
		private void btnDownload_Click(object sender, System.EventArgs e)
		{
			try
			{
				if (listActive != listServer)
					return;

				if (_isWorking)
					return;

				if (listServer.SelectedItems.Count > 0)
					DownloadFiles(listServer.SelectedItems[0].Text, _localPath);
			}
			catch (Exception ex)
			{
				WriteToLog(ex);
			}
		}

		/// <summary>
		/// handle upload button click
		/// </summary>
		private void btnUpload_Click(object sender, System.EventArgs e)
		{
			try
			{
				if (listActive == listServer)
					return;

				if (_isWorking)
					return;

				if (listLocal.SelectedItems.Count > 0)
					UploadFiles(listLocal.SelectedItems[0].Text, _localPath);
			}
			catch (Exception ex)
			{
				WriteToLog(ex);
			}
		}

		/// <summary>
		/// handle delete button click
		/// </summary>
		private void btnDelete_Click(object sender, System.EventArgs e)
		{
			try
			{
				if (listActive == listServer)
				{
					if (_isWorking)
						return;

					if (listServer.SelectedItems.Count > 0)
					{
						int index = listServer.SelectedIndices[0];
						ListViewItem item = listServer.SelectedItems[0];

						if (index >= _serverDirs)
						{
							DeleteDialog dialog = new DeleteDialog(DeleteDialog.DialogPrompt.DeleteFile);
							dialog.Path = item.Text;
							dialog.ShowDialog();

							if (dialog.OK)
							{
								_sftp.DeleteFile(item.Text);
								MakeSftpList();
							}
						}
						else
						{
							DeleteDialog dialog = new DeleteDialog(DeleteDialog.DialogPrompt.DeleteDirectory);
							dialog.Path = item.Text;
							dialog.ShowDialog();

							if (dialog.OK)
							{
								_sftp.RemoveDirectory(item.Text);
								MakeSftpList();
							}
						}
					}
				}
				else
				{
					if (listLocal.SelectedItems.Count > 0)
					{
						int index = listLocal.SelectedIndices[0];
						ListViewItem item = listLocal.SelectedItems[0];
						string path = Path.Combine(_localPath, item.Text);

						if (index >= _localDirs)
						{
							DeleteDialog dialog = new DeleteDialog(DeleteDialog.DialogPrompt.DeleteFile);
							dialog.Path = item.Text;
							dialog.ShowDialog();

							if (dialog.OK)
							{
								File.Delete(path);
								MakeLocalList();
							}
						}
						else
						{
							DeleteDialog dialog = new DeleteDialog(DeleteDialog.DialogPrompt.DeleteDirectory);
							dialog.Path = item.Text;
							dialog.ShowDialog();

							if (dialog.OK)
							{
								Directory.Delete(path, false);
								MakeLocalList();
							}
						}
					}
				}
			}
			catch (Exception ex)
			{
				WriteToLog(ex);
			}
		}

		/// <summary>
		/// activate panel on focus event
		/// </summary>
		private void listServer_Enter(object sender, System.EventArgs e)
		{
			listActive = listServer;
			lblServerDirectory.BackColor = SystemColors.ActiveCaption;
			lblServerDirectory.ForeColor = SystemColors.ActiveCaptionText;
			lblLocalDirectory.BackColor = SystemColors.InactiveCaption;
			lblLocalDirectory.ForeColor = SystemColors.InactiveCaptionText;
		}

		/// <summary>
		/// activate panel on focus event
		/// </summary>
		private void listLocal_Enter(object sender, System.EventArgs e)
		{
			listActive = listLocal;
			lblServerDirectory.BackColor = SystemColors.InactiveCaption;
			lblServerDirectory.ForeColor = SystemColors.InactiveCaptionText;
			lblLocalDirectory.BackColor = SystemColors.ActiveCaption;
			lblLocalDirectory.ForeColor = SystemColors.ActiveCaptionText;
		}

		/// <summary>
		/// "smart" resizing
		/// </summary>
		private void SetRightSizeValues()
		{
			int borderWidth = 10;
			int borderControl = 10;

			// width of panels

			int listWidth = (this.Size.Width - gbControl.Size.Width - 2 * borderControl - 2 * borderWidth) / 2;
			listLocal.Width = listWidth;
			listServer.Width = listWidth;
			listLocal.Location = new Point(borderWidth + listWidth + borderControl + gbControl.Size.Width + borderControl, listServer.Location.Y);

			// status info for panels

			lblServer.Location = new Point(listServer.Location.X, listServer.Location.Y + listServer.Size.Height + 5);
			lblLocal.Location = new Point(listLocal.Location.X, listLocal.Location.Y + listLocal.Size.Height + 5);
			lblSpeed.Location = new Point(listServer.Location.X + listServer.Width - lblSpeed.Width, listServer.Location.Y + listServer.Size.Height + 5);
			trackBar.Location = new Point(listServer.Location.X, lblSpeed.Location.Y + lblSpeed.Height + 5);
			trackBar.Size = new Size(listServer.Width, trackBar.Height);

			// local directory

			lblLocalDirectory.Location = new Point(listLocal.Location.X, lblLocalDirectory.Location.Y);
			lblLocalDirectory.Width = listLocal.Size.Width;

			// server directory

			lblServerDirectory.Location = new Point(listServer.Location.X, lblServerDirectory.Location.Y);
			lblServerDirectory.Width = listServer.Size.Width;

			// state

			lblProgress.Location = new Point(pbTransfer.Location.X + pbTransfer.Width + 8, pbTransfer.Location.Y);
			lblBatchTransferProgress.Location = 
				new Point(lblProgress.Location.X + lblProgress.Width + 2, lblProgress.Location.Y);

			// header

			headerRight.Location = new Point(this.ClientSize.Width - headerRight.Width, headerRight.Location.Y);
			headerMiddle.Width = headerRight.Location.X - headerMiddle.Location.X;
		}

		private void MainForm_Resize(object sender, System.EventArgs e)
		{
			if (WindowState != FormWindowState.Minimized)
				SetRightSizeValues();
		}

		/// <summary>
		/// stop transfer when something is being transferred
		/// </summary>
		private void btnStop_Click(object sender, System.EventArgs e)
		{
			_sftp.AbortTransfer();
		}

		/// <summary>
		/// go to the url
		/// </summary>
		private void headerRight_Click(object sender, System.EventArgs e)
		{
			System.Diagnostics.Process.Start(URL);
		}

		/// <summary>
		/// space bar for changing panels server <-> local
		/// </summary>
		private void MainForm_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			if (e.KeyChar == Convert.ToChar(Keys.Space))
			{
				if (listActive == listLocal)
					listServer.Focus();
				else
					listLocal.Focus();
			}
		}

		private void trackBar_Scroll(object sender, EventArgs e)
		{
			if (trackBar.Value == 100)
			{
				lblSpeed.Text = "Speed: Unlimited";
				_sftp.MaxDownloadSpeed = 0;
				_sftp.MaxUploadSpeed = 0;
			}
			else
			{
				lblSpeed.Text = string.Format("Speed: {0} kB/s", trackBar.Value);
				_sftp.MaxDownloadSpeed = trackBar.Value;
				_sftp.MaxUploadSpeed = trackBar.Value;
			}
		}
	}
}
