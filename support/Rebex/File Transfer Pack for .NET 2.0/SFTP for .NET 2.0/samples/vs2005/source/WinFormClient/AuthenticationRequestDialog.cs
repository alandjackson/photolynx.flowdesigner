//  
//   Rebex Sample Code License
// 
//   Copyright (c) 2009, Rebex CR s.r.o. www.rebex.net, 
//   All rights reserved.
// 
//   Permission to use, copy, modify, and/or distribute this software for any
//   purpose with or without fee is hereby granted
// 
//   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//   EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//   HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
//   OTHER DEALINGS IN THE SOFTWARE.
// 

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Rebex.Net;

namespace Rebex.Samples.WinFormClient
{
	/// <summary>
	/// Dialog for getting user response to AuthenticationRequest event.
	/// </summary>
	public partial class AuthenticationRequestDialog : Form
	{
		private readonly SshAuthenticationRequestEventArgs _sshAuthenticationRequestEventArgs;

		/// <summary>
		/// Creates a new instance of AuthenticationRequestDialog used for providing responses to AuthenticationRequest event.
		/// </summary>
		/// <param name="sshAuthenticationRequestEventArgs"></param>
		public AuthenticationRequestDialog(SshAuthenticationRequestEventArgs sshAuthenticationRequestEventArgs)
		{
			if (sshAuthenticationRequestEventArgs == null)
				throw new ArgumentNullException("sshAuthenticationRequestEventArgs");

			InitializeComponent();

			_sshAuthenticationRequestEventArgs = sshAuthenticationRequestEventArgs;
			InitializeDialog();
		}

		/// <summary>
		/// Initializes dialog with data from SshAuthenticationRequestEventArgs.
		/// </summary>
		private void InitializeDialog()
		{
			// dialog header
			this.Text = string.Format("Authentication request: {0}", _sshAuthenticationRequestEventArgs.Name);

			// instructions
			this.LabelInstructions.Text = _sshAuthenticationRequestEventArgs.Instructions;

			// prompts and textboxes for answers
			foreach (SshAuthenticationRequestItem item in  _sshAuthenticationRequestEventArgs.Items)
			{
				AddLabel(PromptPanel, item.Prompt);
				AddTextBox(PromptPanel, item);
			}

			// resize the dialog according to the prompt count
			this.Height += PromptPanel.DisplayRectangle.Height - PromptPanel.ClientRectangle.Height;
		}

		/// <summary>
		/// Adds Label control to the bottom of the TableLayoutPanel.
		/// </summary>
		private void AddLabel(TableLayoutPanel panel, string labelText)
		{
			Label label = new Label();
			label.Dock = DockStyle.Fill;
			label.Text = labelText;
			label.TextAlign = ContentAlignment.MiddleLeft;
			
			panel.Controls.Add(label, 0, panel.RowCount);
			panel.RowCount++;
		}

		/// <summary>
		/// Adds TextBox control to the bottom of the TableLayoutPanel. The TextBox is data-bound to SshAuthenticationRequestItem.Response property.
		/// </summary>
		private void AddTextBox(TableLayoutPanel panel, SshAuthenticationRequestItem requestItem)
		{
			TextBox textbox = new TextBox();
			textbox.UseSystemPasswordChar = requestItem.IsSecret;
			textbox.Dock = DockStyle.Fill;
			textbox.DataBindings.Add("Text", requestItem, "Response");

			panel.Controls.Add(textbox, 0, panel.RowCount);
			panel.RowCount++;
		}
		
	}
}