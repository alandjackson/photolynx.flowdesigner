﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tamir.SharpSsh
{
	public interface IFtpNotificationProgressInfo
	{
		bool IsCanceled { get; set; }
	}
}
