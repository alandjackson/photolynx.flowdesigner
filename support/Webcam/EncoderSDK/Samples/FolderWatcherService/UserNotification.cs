﻿//-----------------------------------------------------------------------
// <copyright file="UserNotification.cs" company="Microsoft Corporation">
//     Copyright (c) Microsoft Corporation
// </copyright>
//-----------------------------------------------------------------------

namespace EncoderWatcher
{
    using System;
    using System.IO;
    using System.Net.Mail;

    /// <summary>
    /// functions handling user notification (email)
    /// </summary>
    public static class UserNotification
    {              
        /// <summary>
        /// send notification informing user that an encode job is starting
        /// </summary>
        /// <param name="sourcePath">file being encoded</param>
        /// <param name="information">extra information to send in email (attachment)</param>
        public static void EncodingStarting(string sourcePath, string information)
        {
            UserNotification.SendEmailMessage("Encoding Starting: " + sourcePath, information, null, false);            
        }
        
        /// <summary>
        /// notify user that job finished encoding (or errored)
        /// </summary>
        /// <param name="sourcePath">file encoded</param>
        /// <param name="logFile">log file for encoding</param>ls
        /// <param name="success">encode succeeded</param>
        public static void EncodingFinished(string sourcePath, string logFile, bool success)
        {
            UserNotification.SendEmailMessage(
                                              (success ? "Encoding Finished:" : "Encoding FAILED:") + sourcePath,
                                              (success ? "Success." : "FAILED:") + " Log File Attached.",
                                              logFile, 
                                              !success);            
        }

        /// <summary>
        /// send an email notification
        /// </summary>
        /// <param name="subject">subject line text</param>
        /// <param name="body">body text of email message (can be HTML)</param>
        /// <param name="attachmentPath">path for an attachment</param>
        /// <param name="highImportance">message is important</param>
        private static void SendEmailMessage(string subject, string body, string attachmentPath, bool highImportance)
        {
            // if all email items not set up correctly, dont send email
            if (String.IsNullOrEmpty(EncoderFolderWatcherService.WatcherSettings.Default.senderEmail) ||
                String.IsNullOrEmpty(EncoderFolderWatcherService.WatcherSettings.Default.recipientEmail) ||
                String.IsNullOrEmpty(EncoderFolderWatcherService.WatcherSettings.Default.smtpClientAddress))
            {
                return;
            }

            try
            {
                using (MailMessage message = new MailMessage(EncoderFolderWatcherService.WatcherSettings.Default.senderEmail, EncoderFolderWatcherService.WatcherSettings.Default.recipientEmail))
                {
                    message.Subject = subject;
                    message.Body = body;
                    message.IsBodyHtml = true;
                    message.Priority = highImportance ? MailPriority.High : MailPriority.Normal;

                    if (!String.IsNullOrEmpty(attachmentPath) && File.Exists(attachmentPath))
                    {
                        Attachment attachment = new Attachment(attachmentPath);
                        message.Attachments.Add(attachment);
                    }

                    using (SmtpClient client = new SmtpClient(EncoderFolderWatcherService.WatcherSettings.Default.smtpClientAddress))
                    {
                        if (EncoderFolderWatcherService.WatcherSettings.Default.smtpUseDefaultCredentials.Equals("true"))
                        {
                            client.UseDefaultCredentials = true;
                        }
                        else
                        {
                            client.UseDefaultCredentials = false;
                            client.Credentials = new System.Net.NetworkCredential(
                                EncoderFolderWatcherService.WatcherSettings.Default.smtpCredentialUser,
                                EncoderFolderWatcherService.WatcherSettings.Default.smtpCredentialPassword);
                        }
                        if (EncoderFolderWatcherService.WatcherSettings.Default.smtpEnableSsl.Equals("true"))
                        {
                            client.EnableSsl = true;
                        }
                        else
                        {
                            client.EnableSsl = false;
                        }
                        client.Send(message);
                    }
                }
            }
            catch (Exception exception)
            {
                Watcher.WatcherEventLog.WriteEntry(exception.Message, System.Diagnostics.EventLogEntryType.Error);
            }
        }
    }
}
