﻿//-----------------------------------------------------------------------
// <copyright file="Utilities.cs" company="Microsoft Corporation">
//     Copyright (c) Microsoft Corporation
// </copyright>
//-----------------------------------------------------------------------

namespace EncoderWatcher
{
    using System;
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;
    using System.Threading;

    /// <summary>
    /// various utility functions
    /// </summary>
    public static class Utilities
    {
        /// <summary>
        /// try, try, try... to move this file. tries for a predefined period, in case a very large file was copying, or times out
        /// note: blocks thread
        /// </summary>
        /// <param name="sourcePath">full path to source file</param>
        /// <param name="destinationPath">full path to destination file</param>
        /// <param name="logFile">log file to write to</param>
        /// <returns>true if file moved OK</returns>
        public static bool ResilientMoveFile(string sourcePath, string destinationPath, TraceListener logFile)
        {
            if (logFile != null)
            {
                logFile.WriteLine("Moving file '" + sourcePath + "' to '" + destinationPath + "'");
            }

            // it may have been created, but might not be finished copying. need to wait until we can have accessto move it
            bool haveMoved = false;
            int tryCount = 0;
            DateTime timeStart = DateTime.Now;
            while (!haveMoved)
            {
                // try to get access for predefined period
                if (DateTime.Now - timeStart > TimeSpan.FromSeconds(EncoderFolderWatcherService.WatcherSettings.Default.retryFileCopyDurationSeconds))
                {
                    throw new WatcherException("  Failed to get access to '" + sourcePath + "' within established time period -- abandoning");
                }

                // try renaming file. if we dont have access we will get the IOException
                try
                {
                    if (logFile != null)
                    {
                        logFile.WriteLine("  Trying to get access to '" + sourcePath + "'");
                    }

                    File.Move(sourcePath, destinationPath);
                    haveMoved = true;

                    if (logFile != null)
                    {
                        logFile.WriteLine("  Moved file '" + sourcePath + "' to '" + destinationPath + "'");
                    }                    
                }
                catch (FileNotFoundException)
                {
                    // source file moved underneath us (someone else claimed?)
                    if (logFile != null)
                    {
                        logFile.WriteLine("  file '" + sourcePath + "' moved from underneath us. This machine should not do the encode");
                    }

                    return false;
                }
                catch (IOException)
                {
                    // did not have access. Wait a little                    
                    if (logFile != null)
                    {
                        logFile.WriteLine("  Could not get access to '" + sourcePath + "' ... sleeping 1 second before retrying (try " + (++tryCount) + ")");
                    }

                    Thread.Sleep(1000);
                }                
            }

            return true;
        }
        
        /// <summary>
        /// tried to rename a file, if the destination file already exists it adds a number decoration to it and logs that fact
        /// so no encoding goes missing or is overwritten.
        /// </summary>
        /// <param name="sourcePath">source path</param>
        /// <param name="destinationPath">desired desintation path</param>
        /// <param name="logFile">log file to write to</param>
        /// <returns>try if file moved (and renamed if already existed)</returns>
        public static bool RenameFileMove(string sourcePath, ref string destinationPath, TraceListener logFile)
        {
            string destinationDirectory = Path.GetDirectoryName(destinationPath);
            string destinationFilenameNoExt = Path.GetFileNameWithoutExtension(destinationPath);
            string destinationFilenameExt = Path.GetExtension(destinationPath);
            int nameTrys = 0;

            while (File.Exists(destinationPath))
            {
                destinationPath = Path.Combine(destinationDirectory, destinationFilenameNoExt + "." + (++nameTrys) + destinationFilenameExt);
            }

            return Utilities.ResilientMoveFile(sourcePath, destinationPath, logFile);
        }
        
        /// <summary>
        /// extend TimeSpan to with a ToString()
        /// </summary>
        /// <param name="timeSpan">timespan to print</param>
        /// <returns>string of timespan</returns>
        public static string ToString(this TimeSpan timeSpan)
        {
            return String.Format(CultureInfo.CurrentCulture, "{0:00}.{1:00}:{2:00}:{3:00}.{4:0}", timeSpan.Days, timeSpan.Hours, timeSpan.Minutes, timeSpan.Seconds, timeSpan.Milliseconds);
        }
        
        /// <summary>
        /// return a formatted date time in a filename friendly manner
        /// </summary>
        /// <param name="dateTime">time to represent</param>
        /// <returns>filename friendly string version of time</returns>
        public static string FilenameFriendlyDateTime(DateTime dateTime)
        {
            return String.Format(CultureInfo.InvariantCulture, "{0:u}", dateTime).Replace(":", ";"); 
        }
    }
}
