﻿//-----------------------------------------------------------------------
// <copyright file="SourceFileManager.cs" company="Microsoft Corporation">
//     Copyright (c) Microsoft Corporation
// </copyright>
//-----------------------------------------------------------------------

namespace EncoderWatcher
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;
    using System.Threading;
    using System.Reflection;

    /// <summary>
    /// manages source files detected in the Source directory
    /// </summary>
    public class SourceFileManager
    {
        /// <summary>
        /// ignore files with these extensions
        /// </summary>
        private static string[] ignoreList = new string[] { ".db", ".ini" };

        /// <summary>
        /// list of source files to encode
        /// </summary>
        private Queue<SourceFile> sourceFiles = new Queue<SourceFile>();
        
        /// <summary>
        /// directory manager
        /// </summary>
        private DirectoryManager directoryManager;       
        
        /// <summary>
        /// Initializes a new instance of the SourceFileManager class
        /// </summary>
        /// <param name="directoryManager">directory manager</param>
        public SourceFileManager(DirectoryManager directoryManager)
        {
            this.directoryManager = directoryManager;   
        }

        /// <summary>
        /// Encode started
        /// </summary>
        public event EventHandler<EventArgs> EncodeStarted;

        /// <summary>
        /// Encode ended
        /// </summary>
        public event EventHandler<EventArgs> EncodeEnded;

        /// <summary>
        /// Encode errored
        /// </summary>
        public event EventHandler<EventArgs> EncodeError;                
        
        /// <summary>
        /// add a single source file to encode
        /// </summary>
        /// <param name="file">file to add</param>
        public void AddFile(SourceFile file)
        {
            // excluded file?
            if (ExcludeFile(file.FilePath))
            {
                return;
            }

            lock (this.sourceFiles)
            {
                // already in queue?
                foreach (SourceFile fileCheck in this.sourceFiles)
                {
                    if (file.FilePath == fileCheck.FilePath)
                    {
                        return;
                    }
                }

                this.sourceFiles.Enqueue(file);
            }

            ThreadPool.QueueUserWorkItem(this.EncodeJob);
        }

        /// <summary>
        /// exclude file list. Files we shouldnt even consider encoding
        /// </summary>
        /// <param name="filePath">filepath to check</param>
        /// <returns>true means file should be excluded</returns>
        private static bool ExcludeFile(string filePath)
        {
            string extensionTest = Path.GetExtension(filePath);
            foreach (string extensionIgnore in ignoreList)
            {
                if (String.Compare(extensionIgnore, extensionTest, true, CultureInfo.InvariantCulture) == 0)
                {
                    return true;
                }
            }

            return false;
        }
        
        /// <summary>
        /// encode a job
        /// </summary>
        /// <param name="thisObject">object calling</param>
        private void EncodeJob(object thisObject)
        {
            SourceFile sourceFile;

            lock (this.sourceFiles)
            {
                sourceFile = this.sourceFiles.Dequeue();
            }

            Assembly assemblyThis = Assembly.GetAssembly(this.GetType());
            AppDomainSetup info = new AppDomainSetup();
            AppDomain appDomainEncode = AppDomain.CreateDomain(sourceFile.FilePath);
            try
            {
                EncodeJob encodeJob = appDomainEncode.CreateInstanceAndUnwrap(assemblyThis.FullName, typeof(EncodeJob).FullName) as EncodeJob;

                encodeJob.Initialise(this.directoryManager, sourceFile);
                encodeJob.EncodeStarted += new EventHandler<EventArgs>(this.EncodeJob_EncodeStarted);
                encodeJob.EncodeError += new EventHandler<EventArgs>(this.EncodeJob_EncodeError);
                encodeJob.EncodeEnded += new EventHandler<EventArgs>(this.EncodeJob_EncodeEnded);
                encodeJob.Execute();
                encodeJob.EncodeStarted -= new EventHandler<EventArgs>(this.EncodeJob_EncodeStarted);
                encodeJob.EncodeError -= new EventHandler<EventArgs>(this.EncodeJob_EncodeError);
                encodeJob.EncodeEnded -= new EventHandler<EventArgs>(this.EncodeJob_EncodeEnded);
            }
            catch (Exception)
            {
                // we need the service to stay alive regardless of any exceptions in the Encode AppDomain
            }
            finally
            {
                AppDomain.Unload(appDomainEncode);
            }
        }
        
        /// <summary>
        /// bubble up EncodeStarted event
        /// </summary>
        /// <param name="sender">encode job</param>
        /// <param name="e">event arguments</param>
        private void EncodeJob_EncodeStarted(object sender, EventArgs e)
        {
            if (this.EncodeStarted != null)
            {
                this.EncodeStarted(sender, e);
            }
        }     
        
        /// <summary>
        /// bubble up EncodeEnded event
        /// </summary>
        /// <param name="sender">encode job</param>
        /// <param name="e">event arguments</param>
        private void EncodeJob_EncodeEnded(object sender, EventArgs e)
        {
            if (this.EncodeEnded != null)
            {
                this.EncodeEnded(sender, e);
            }
        }
        
        /// <summary>
        /// bubble up EncodeError event
        /// </summary>
        /// <param name="sender">encode job</param>
        /// <param name="e">event arguments</param>
        private void EncodeJob_EncodeError(object sender, EventArgs e)
        {
            if (this.EncodeError != null)
            {
                this.EncodeError(sender, e);
            }
        }
    }
}
