﻿//-----------------------------------------------------------------------
// <copyright file="Program.cs" company="Microsoft Corporation">
//     Copyright (c) Microsoft Corporation
// </copyright>
// <summary>
// Encoder Folder Watcher Service
//
// This sample is a service which monitors a folder and encodes videos that are dropped into it.
// This service requires a login account on install which is used for email and folder access.
// To set which directory the folder watcher service monitors - change the configuration file (EncoderFolderWatcherService.exe.config)
// entry for watcherRootDirectory. This can either be a local folder or a folder share (make sure this service has read/write access)
// If this is a folder share you can use multiple watcher services on different machines all watching the same folder share
// so that the workload gets distributed.
// When the watcher starts, it ensures the required directory hierarchy is established. ie.
//     Source    :   directory is watched for arriving source media files
//     Logs      :   output logs for the encodings
//     Working   :   working directory while encoding
//     Output    :   final output directory after encoding
//     Presets   :   encoding parameter files needed for each encode
// When a source media file is dropped into "Source", a watcher will claim it. Move it to the working directory and encode
// it once for every preset in the Presets folder. The logs will be written to the Log directory and the final output files
// will appear in the Output directory under a folder with the same name as the preset used.
// For email notification of job status (fail/succeed etc) update the configuration file with a senderEmail, recipientEmail and smtpClientAddress.
// Additional email settings including smtpPort, smtpUseDefaultCredentials, smtpCredentialUser, smtpCredentialPassword and smtpEnableSsl. 
// For housekeeping purposes the watcher cleans up old encodes/logs/sources etc that have been left in the Watch folders after timeToKeepOldFilesSeconds
// also settable in the configuration file, but should probably be in the order of days to weeks. The other two config settings are related
// to the amount of time between checking for newly dropped media files and the amount of time between updating progress to the log
// files.
// 
// Instructions for installing and running
//     run the following command line to install the service (you must be an Admin to do this)
//         %ProgramFiles%\Microsoft Expression\Encoder\4.0\EncoderFolderWatcherService.exe -install
//     fix up the file %ProgramFiles%\Microsoft Expression\Encoder\4.0\EncoderFolderWatcherService.exe.config per your requirements
//     Use Expression Encoder to make some encoding presets and drop them into the Presets directory under the watched folder
//     start the service using the following command line
//         %ProgramFiles%\Microsoft Expression\Encoder\4.0\EncoderFolderWatcherService.exe -start
//
// TODO: resiliency - network unplugged mid encode?
// </summary>
//-----------------------------------------------------------------------

namespace EncoderFolderWatcherService
{
    using System;
    using System.Collections;
    using System.Globalization;
    using System.IO;
    using System.Runtime.Serialization.Formatters.Binary;
    using System.ServiceProcess;

    /// <summary>
    /// main entry point
    /// </summary>
    public static class Program
    {               
        /// <summary>
        /// The main entry point for the application. display help if they try to use an unrecognized
        /// command
        /// </summary>              
        /// <param name="arguments">command line argh</param>
        public static void Main(string[] arguments)         
        {
            try
            {
                if (arguments.Length > 0)
                {
                    switch (arguments[0].ToLower(CultureInfo.InvariantCulture))
                    {
                        case "-install":
                            Install();
                            return;

                        case "-uninstall":
                            Uninstall();
                            return;

                        case "-start":
                            Start();
                            return;

                        case "-stop":
                            Stop();
                            return;

                        case "-status":
                            Status();
                            return;

                        default:
                            {
                                Console.WriteLine();
                                Console.WriteLine(WatcherServiceInstaller.ServiceName + ", Version " + typeof(FolderWatcherService).Assembly.GetName().Version.ToString());
                                Console.WriteLine();
                                Console.WriteLine("Usage: EncoderFolderWatcherService.exe <command>");
                                Console.WriteLine();
                                Console.WriteLine("Commands:");
                                Console.WriteLine("  -install      Install the service.");
                                Console.WriteLine("  -uninstall    Uninstall the service.");
                                Console.WriteLine("  -start        Start the service.");
                                Console.WriteLine("  -stop         Stop the service.");
                                Console.WriteLine("  -status       Give the current status.");
                            }

                            return;
                    }
                }

                ServiceBase[] servicesToRun;
                servicesToRun = new ServiceBase[] 
                { 
                    new FolderWatcherService() 
                };

                ServiceBase.Run(servicesToRun);
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
        }
        
        /// <summary>
        /// install the service
        /// </summary>
        private static void Install()
        {
            IDictionary savedState = new Hashtable();
            using (WatcherServiceInstaller installer = new WatcherServiceInstaller())
            {
                installer.Install(savedState);

                if (savedState.Keys.Count != 0)
                {
                    string savedStateFileName = Path.ChangeExtension(typeof(FolderWatcherService).Module.FullyQualifiedName, ".install");
                    Console.WriteLine("Writing " + savedStateFileName + ".");
                    using (FileStream fileStream = new FileStream(savedStateFileName, FileMode.Create, FileAccess.Write))
                    {
                        new BinaryFormatter().Serialize(fileStream, savedState);
                    }
                }
            }
        }
        
        /// <summary>
        /// uninstall the service
        /// </summary>
        private static void Uninstall()
        {
            IDictionary savedState = null;

            string savedStateFileName = Path.ChangeExtension(typeof(FolderWatcherService).Module.FullyQualifiedName, ".install");
            if (File.Exists(savedStateFileName))
            {
                using (FileStream fileStream = new FileStream(savedStateFileName, FileMode.Open, FileAccess.Read))
                {
                    savedState = (IDictionary)new BinaryFormatter().Deserialize(fileStream);
                }

                File.Delete(savedStateFileName);
            }

            using (WatcherServiceInstaller installer = new WatcherServiceInstaller())
            {
                installer.Uninstall(savedState);
            }
        }
        
        /// <summary>
        /// start the service
        /// </summary>
        private static void Start()
        {
            using (ServiceController controller = new ServiceController(WatcherServiceInstaller.ServiceName))
            {
                if (controller.Status == ServiceControllerStatus.Running)
                {
                    Console.WriteLine("Already running.");
                }
                else
                {
                    controller.Start();
                    Console.WriteLine("Running.");
                }
            }
        }        
        
        /// <summary>
        /// stop the service if running
        /// </summary>
        private static void Stop()
        {
            using (ServiceController controller = new ServiceController(WatcherServiceInstaller.ServiceName))
            {
                if (controller.Status == ServiceControllerStatus.Stopped)
                {
                    Console.WriteLine("Already stopped.");
                }
                else
                {
                    controller.Stop();
                    Console.WriteLine("Stopped.");
                }
            }
        }
        
        /// <summary>
        /// show status of the service on the command line
        /// </summary>
        private static void Status()
        {
            using (ServiceController controller = new ServiceController(WatcherServiceInstaller.ServiceName))
            {
                switch (controller.Status)
                {
                    case ServiceControllerStatus.ContinuePending:
                        Console.WriteLine("Continue Pending.");
                        break;

                    case ServiceControllerStatus.Paused:
                        Console.WriteLine("Paused.");
                        break;

                    case ServiceControllerStatus.PausePending:
                        Console.WriteLine("Pause Pending.");
                        break;

                    case ServiceControllerStatus.Running:
                        Console.WriteLine("Running.");
                        break;

                    case ServiceControllerStatus.StartPending:
                        Console.WriteLine("Start Pending.");
                        break;

                    case ServiceControllerStatus.Stopped:
                        Console.WriteLine("Stopped.");
                        break;

                    case ServiceControllerStatus.StopPending:
                        Console.WriteLine("Stop Pending.");
                        break;
                }
            }
        }
    }
}
