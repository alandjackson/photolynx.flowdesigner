﻿//-----------------------------------------------------------------------
// <copyright file="Watcher.cs" company="Microsoft Corporation">
//     Copyright (c) Microsoft Corporation
// </copyright>
//-----------------------------------------------------------------------

namespace EncoderWatcher
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;
    using System.Threading;
    using System.Text.RegularExpressions;

    /// <summary>
    /// the main watcher class
    /// </summary>
    public class Watcher : IDisposable
    {                
        /// <summary>
        /// manages the directories required
        /// </summary>
        private DirectoryManager directoryManager;
        
        /// <summary>
        /// thread that does the watching
        /// </summary>
        private Thread watchThread;
        
        /// <summary>
        /// autoresetevent used to terminate watching thread
        /// </summary>
        private ManualResetEvent terminateEvent = new ManualResetEvent(false);
        
        /// <summary>
        /// manager for job encoding
        /// </summary>
        private SourceFileManager sourceFileManager;
        
        /// <summary>
        /// currently active encodings
        /// </summary>
        private List<EncodeJob> activeEncodes = new List<EncodeJob>();
        
        /// <summary>
        /// dispose been called?
        /// </summary>
        private bool disposed;

        /// <summary>
        /// Initializes a new instance of the Watcher class
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2122:DoNotIndirectlyExposeMethodsWithLinkDemands", Justification = "Process not exposed publically")]
        public Watcher()
        {
            Process.GetCurrentProcess().PriorityClass = ProcessPriorityClass.Idle;         
        }     
        
        /// <summary>
        /// Finalizes an instance of the Watcher class
        /// </summary>
        ~Watcher()
        {
            this.Dispose(false);
        }

        /// <summary>
        /// Encode started
        /// </summary>
        public event EventHandler<EventArgs> EncodeStarted;

        /// <summary>
        /// Encode ended
        /// </summary>
        public event EventHandler<EventArgs> EncodeEnded;

        /// <summary>
        /// Encode errored
        /// </summary>
        public event EventHandler<EventArgs> EncodeError;

        /// <summary>
        /// Gets Event Log for this watcher
        /// </summary>
        public static EventLog WatcherEventLog
        {
            get;
            private set;
        }                    
        
        /// <summary>
        /// implement IDispose
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }       
        
        /// <summary>
        /// start watching
        /// </summary>
        public void Start()
        {
            InitEventLogging();
            WatcherEventLog.WriteEntry("Encoder Folder Watcher Service started", EventLogEntryType.SuccessAudit);
            this.watchThread = new Thread(new ThreadStart(this.StartWatchThread));
            this.watchThread.Start();
        }
        
        /// <summary>
        /// stop watching
        /// </summary> 
        public void Stop()
        {
            WatcherEventLog.WriteEntry("Encoder Folder Watcher Service stopped", EventLogEntryType.SuccessAudit);
            this.terminateEvent.Set();
            this.watchThread.Join();
        }
        
        /// <summary>
        /// watcher-wide logging occurs in system event log under "Application"
        /// per job logging gets written to the watch folders "Log" directory
        /// </summary>
        private static void InitEventLogging()
        {
            if (!EventLog.SourceExists(EncoderFolderWatcherService.WatcherServiceInstaller.ServiceName))
            {
                EventSourceCreationData creationData = new EventSourceCreationData(EncoderFolderWatcherService.WatcherServiceInstaller.ServiceName, "Application");
                EventLog.CreateEventSource(creationData);
            }

            WatcherEventLog = new EventLog();
            WatcherEventLog.Source = EncoderFolderWatcherService.WatcherServiceInstaller.ServiceName;
        }
        
        /// <summary>
        /// waits until the watch folder exists, polls every 10 secs - WLB need to do some work here, what if network goes down.
        /// </summary>
        /// <returns>true if not being forced to stop</returns>
        private bool EnsureWatchFolder()
        {
            // we need the watch folder created before we can proceed...
            bool loggedError = false, forceStop = false;
            while (!Directory.Exists(EncoderFolderWatcherService.WatcherSettings.Default.watcherRootDirectory) && !forceStop)
            {
                if (!loggedError)
                {
                    WatcherEventLog.WriteEntry("Watch folder " + EncoderFolderWatcherService.WatcherSettings.Default.watcherRootDirectory + " does not exist - waiting for it to be created...", EventLogEntryType.Error);
                }

                loggedError = true;
                forceStop = this.terminateEvent.WaitOne(10 * 1000);
            }

            if (loggedError && Directory.Exists(EncoderFolderWatcherService.WatcherSettings.Default.watcherRootDirectory))
            {
                WatcherEventLog.WriteEntry("Found Watch folder " + EncoderFolderWatcherService.WatcherSettings.Default.watcherRootDirectory, EventLogEntryType.SuccessAudit);
            }

            return !forceStop;
        }
        
        /// <summary>
        /// initialize watcher
        /// </summary>
        /// <returns>true if watcher initialized</returns>
        private bool InitWatcher()
        {
            // we need the watch folder created before we can proceed...
            if (!this.EnsureWatchFolder())
            {
                return false;
            }

            this.directoryManager = new DirectoryManager(EncoderFolderWatcherService.WatcherSettings.Default.watcherRootDirectory);
            this.sourceFileManager = new SourceFileManager(this.directoryManager);

            this.sourceFileManager.EncodeStarted += new System.EventHandler<EventArgs>(this.SourceFileManager_EncodeStarted);
            this.sourceFileManager.EncodeEnded += new System.EventHandler<EventArgs>(this.SourceFileManager_EncodeEnded);
            this.sourceFileManager.EncodeError += new System.EventHandler<EventArgs>(this.SourceFileManager_EncodeError);

            return true;
        }

        /// <summary>
        /// check to see if we own any of the files in the "Working" directory, this can happen if we experienced a failure
        /// during encode and were restarted. If we find any then copy them back into the source directory (using their original names)
        /// so we can reencode them.
        /// </summary>
        private void RestoreUnfinishedEncodes()
        {
            Regex workingFilenameRegEx = new Regex(this.directoryManager.WorkingDirectory.Replace(@"\", @"\\") +
                                                    "\\\\(?<FILENAME>.+).Source." + Environment.MachineName +
                                                    ".([0-9\\s,\\-;]+).(?<EXTENSION>.+)", RegexOptions.Compiled | RegexOptions.IgnoreCase);
            DirectoryInfo workingFolder = new DirectoryInfo(this.directoryManager.WorkingDirectory);
            foreach (FileInfo fileInfo in workingFolder.GetFiles())
            {
                MatchCollection matches = workingFilenameRegEx.Matches(fileInfo.FullName);
                if (matches.Count > 0)
                {
                    try
                    {
                        string originalSourceName = Path.Combine(this.directoryManager.SourceDirectory, matches[0].Groups["FILENAME"].ToString() + matches[0].Groups["EXTENSION"].ToString());
                        Utilities.RenameFileMove(fileInfo.FullName, ref originalSourceName, null);
                    }
                    catch (UnauthorizedAccessException)
                    {
                    }
                }
            }            
        }

        /// <summary>
        /// There may be items already in the Source directory (they wont signal the watcher, since they prexisted it) so enqueue these
        /// </summary>
        private void EnqueueItemsInSourceDirectory()
        {
            DirectoryInfo sourceFolder = new DirectoryInfo(this.directoryManager.SourceDirectory);
            foreach (FileInfo fileInfo in sourceFolder.GetFiles())
            {
                try
                {
                    this.sourceFileManager.AddFile(new SourceFile(fileInfo.FullName));
                }
                catch (UnauthorizedAccessException)
                {
                }
            }
        }
        
        /// <summary>
        /// private thread that does the watching
        /// </summary>
        private void StartWatchThread()
        {
            if (!this.InitWatcher())
            {
                return;
            }

            Thread.Sleep(5000);

            // if we own any items in the Working directory we had failed and been restarted.
            RestoreUnfinishedEncodes();

            // enqueue all files that currently live in the Source directory.
            EnqueueItemsInSourceDirectory();

            // force a directory tidy now.
            this.directoryManager.Tidy();
            
            // watch for changes in the source & preset directories...
            using (FileSystemWatcher sourceDirectoryWatcher = new FileSystemWatcher(this.directoryManager.SourceDirectory))
            using (FileSystemWatcher presetDirectoryWatcher = new FileSystemWatcher(this.directoryManager.PresetsDirectory))
            {
                sourceDirectoryWatcher.EnableRaisingEvents = true;
                sourceDirectoryWatcher.IncludeSubdirectories = false;
                sourceDirectoryWatcher.Created += new FileSystemEventHandler(this.SourceDirectory_FileCreated);
                presetDirectoryWatcher.EnableRaisingEvents = true;
                presetDirectoryWatcher.IncludeSubdirectories = false;
                presetDirectoryWatcher.Created += new FileSystemEventHandler(this.PresetDirectoryWatcher_FileCreated);
              
                // wait until Stop called. (do housekeeping every hour)
                while (!this.terminateEvent.WaitOne(60 * 60 * 1000))
                {
                    this.directoryManager.Tidy();
                }

                // cancel currently active encodes
                lock (this.activeEncodes)
                {
                    foreach (EncodeJob job in this.activeEncodes)
                    {
                        job.Stop = true;
                    }
                }
            }
        }

        /// <summary>
        /// preset has been created in the preset folder, we need to take another look at the source directory
        /// since this might be the first preset
        /// </summary>
        /// <param name="sender">source object</param>
        /// <param name="args">file system event args</param>
        void PresetDirectoryWatcher_FileCreated(object sender, FileSystemEventArgs e)
        {
            EnqueueItemsInSourceDirectory();
        }       
        
        /// <summary>
        /// file has been created in the watch folder
        /// </summary>
        /// <param name="sender">source object</param>
        /// <param name="args">file system event args</param>
        private void SourceDirectory_FileCreated(object sender, FileSystemEventArgs args)
        {
            this.sourceFileManager.AddFile(new SourceFile(args.FullPath));
        }

        /// <summary>
        /// bubble up EncodeStarted event
        /// </summary>        
        /// <param name="sender">encode job</param>
        /// <param name="e">event arguments</param>
        private void SourceFileManager_EncodeStarted(object sender, EventArgs e)
        {
            if (this.EncodeStarted != null)
            {
                this.EncodeStarted(sender, e);
            }

            lock (this.activeEncodes)
            {
                this.activeEncodes.Add(sender as EncodeJob);
            }
        }

        /// <summary>
        /// bubble up EncodeError event
        /// </summary>
        /// <param name="sender">encode job</param>
        /// <param name="e">event arguments</param>
        private void SourceFileManager_EncodeError(object sender, EventArgs e)
        {
            if (this.EncodeError != null)
            {
                this.EncodeError(sender, e);
            }

            lock (this.activeEncodes)
            {
                this.activeEncodes.Remove(sender as EncodeJob);
            }
        }

        /// <summary>
        /// bubble up EncodeEnded event
        /// </summary>
        /// <param name="sender">encode job</param>
        /// <param name="e">event arguments</param>
        private void SourceFileManager_EncodeEnded(object sender, EventArgs e)
        {
            if (this.EncodeEnded != null)
            {
                this.EncodeEnded(sender, e);
            }

            lock (this.activeEncodes)
            {
                this.activeEncodes.Remove(sender as EncodeJob);
            }
        }

        /// <summary>
        /// dispose of this class
        /// </summary>
        /// <param name="disposing">called from Dispose</param>
        private void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    this.terminateEvent.Dispose();
                    if (WatcherEventLog != null)
                    {
                        WatcherEventLog.Close();
                        WatcherEventLog = null;
                    }
                }

                this.disposed = true;
            }
        }
    }
}
