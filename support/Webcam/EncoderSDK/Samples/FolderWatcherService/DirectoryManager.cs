﻿//-----------------------------------------------------------------------
// <copyright file="DirectoryManager.cs" company="Microsoft Corporation">
//     Copyright (c) Microsoft Corporation
// </copyright>
//-----------------------------------------------------------------------

namespace EncoderWatcher
{
    using System;
    using System.Diagnostics;
    using System.IO;
   
    /// <summary>
    /// top level watch directory - when set, creates the required subdirectories
    ///     Source    :   directory is watched for arriving source media files
    ///     Logs      :   output logs for the encodings
    ///     Working   :   working directory while encoding
    ///     Output    :   final output directory after encoding
    ///     Presets   :   encoding parameter files needed for each encode
    /// </summary>
    public class DirectoryManager// : IDisposable
    {
        /// <summary>
        /// directory we are watching
        /// </summary>
        private string watchDirectory;

        /// <summary>
        /// Initializes a new instance of the DirectoryManager class
        /// </summary>
        /// <param name="watchDirectory">directory to watch</param>
        public DirectoryManager(string watchDirectory)
        {
            this.WatchDirectory = watchDirectory;
        }
        
        /// <summary>
        /// Gets or sets the watch directory
        /// </summary>
        public string WatchDirectory
        {
            get
            {
                if (String.IsNullOrEmpty(this.watchDirectory))
                {
                    throw new InvalidOperationException("WatchDirectory is not set");
                }

                return this.watchDirectory;
            }

            set
            {
                if (!String.IsNullOrEmpty(value))
                {
                    this.watchDirectory = value;
                    Directory.CreateDirectory(this.watchDirectory);
                    this.EnsureRequiredSubdirectories();
                }
            }
        }
        
        /// <summary>
        /// Gets the name of the source watch directory
        /// </summary>
        public string SourceDirectory
        {
            get
            {
                return Path.Combine(this.WatchDirectory, "Source");
            }
        }
        
        /// <summary>
        /// Gets the name of the log directory
        /// </summary>
        public string LogDirectory
        {
            get
            {
                return Path.Combine(this.WatchDirectory, "Logs");
            }
        }
        
        /// <summary>
        /// Gets the name of the working directory
        /// </summary>
        public string WorkingDirectory
        {
            get
            {
                return Path.Combine(this.WatchDirectory, "Working");
            }
        }
        
        /// <summary>
        /// Gets the name of the output directory
        /// </summary>
        public string OutputDirectory
        {
            get
            {
                return Path.Combine(this.WatchDirectory, "Output");
            }
        }
        
        /// <summary>
        /// Gets the name of the presets directory
        /// </summary>
        public string PresetsDirectory
        {
            get
            {
                return Path.Combine(this.WatchDirectory, "Presets");
            }
        }

        /// <summary>
        /// list of preset files in the presets directory
        /// </summary>
        public string[] PresetFiles
        {
            get
            {
                return Directory.GetFiles(this.PresetsDirectory, "*.xml");
            }
        }
                
        /// <summary>
        /// tidy up old files in the watcher directories (anything older than Settings.TimeToKeepOldFilesSeconds)
        /// </summary>
        public void Tidy()
        {
            TidyDirectory(this.WorkingDirectory);
            TidyDirectory(this.LogDirectory);
            TidyDirectory(this.OutputDirectory);
        }
        
        /// <summary>
        /// tidy up old files in a particular directory
        /// </summary>
        /// <param name="directory">directory to tidy up</param>
        private static void TidyDirectory(string directory)
        {
            foreach (string directoryChild in Directory.GetDirectories(directory))
            {
                TidyDirectory(directoryChild);
            }

            foreach (string filePath in Directory.GetFiles(directory))
            {
                try
                {
                    FileInfo fileInfo = new FileInfo(filePath);
                    if ((DateTime.Now.ToUniversalTime() - fileInfo.LastWriteTimeUtc) > TimeSpan.FromSeconds(EncoderFolderWatcherService.WatcherSettings.Default.timeToKeepOldFilesSeconds))
                    {
                        // file has been sitting untouched for "timeToKeepOldFilesSeconds" - clean it up
                        File.SetAttributes(filePath, File.GetAttributes(filePath) & ~FileAttributes.ReadOnly);
                        File.Delete(filePath);
                    }
                }
                catch (Exception ex)
                {
                    Watcher.WatcherEventLog.WriteEntry("Failed to tidy up file " + filePath + " Exception: " + ex, EventLogEntryType.Error);
                }
            }
        }

        /// <summary>
        /// we need the following directories under the main watching directory, create them if needed
        /// </summary>
        private void EnsureRequiredSubdirectories()
        {
            Directory.CreateDirectory(this.SourceDirectory);
            Directory.CreateDirectory(this.LogDirectory);
            Directory.CreateDirectory(this.WorkingDirectory);
            Directory.CreateDirectory(this.OutputDirectory);
            Directory.CreateDirectory(this.PresetsDirectory);
        }
    }
}
