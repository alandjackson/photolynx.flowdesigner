﻿//-----------------------------------------------------------------------
// <copyright file="SourceFile.cs" company="Microsoft Corporation">
//     Copyright (c) Microsoft Corporation
// </copyright>
//-----------------------------------------------------------------------

namespace EncoderWatcher
{
    /// <summary>
    /// represents a single source file
    /// </summary>
    public class SourceFile
    {       
        /// <summary>
        /// Initializes a new instance of the SourceFile class
        /// </summary>
        /// <param name="filepath">filepath of source</param>
        public SourceFile(string filepath)
        {
            this.FilePath = filepath;
        }

        /// <summary>
        /// Gets source filepath
        /// </summary>
        public string FilePath 
        { 
            get; 
            private set; 
        }
    }
}
