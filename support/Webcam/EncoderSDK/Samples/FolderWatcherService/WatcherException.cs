﻿//-----------------------------------------------------------------------
// <copyright file="WatcherException.cs" company="Microsoft Corporation">
//     Copyright (c) Microsoft Corporation
// </copyright>
//-----------------------------------------------------------------------

namespace EncoderWatcher 
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// watcher specific exception
    /// </summary>
    [Serializable]
    public class WatcherException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the WatcherException class
        /// </summary>
        public WatcherException() 
        { 
        }

        /// <summary>
        /// Initializes a new instance of the WatcherException class
        /// </summary>
        /// <param name="message">exception detail</param>
        public WatcherException(string message) 
            : base(message) 
        { 
        }

        /// <summary>
        /// Initializes a new instance of the WatcherException class
        /// </summary>
        /// <param name="message">exception detail</param>
        /// <param name="inner">exception to wrap</param>
        public WatcherException(string message, Exception inner) 
            : base(message, inner) 
        { 
        }

        /// <summary>
        /// Initializes a new instance of the WatcherException class
        /// </summary>
        /// <param name="info">serialization information</param>
        /// <param name="context">streaming context</param>
        protected WatcherException(SerializationInfo info, StreamingContext context)
            : base(info, context) 
        { 
        }
    }
}
