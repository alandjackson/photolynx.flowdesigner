﻿//-----------------------------------------------------------------------
// <copyright file="EncodeJob.cs" company="Microsoft Corporation">
//     Copyright (c) Microsoft Corporation
// </copyright>
//-----------------------------------------------------------------------

namespace EncoderWatcher
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;
    using Microsoft.Expression.Encoder;
    using System.Reflection;

    /// <summary>
    /// represents a single encoding job (may be encoding to multiple presets)
    /// </summary>
    [Serializable]
    public class EncodeJob
    {             
        /// <summary>
        /// directory manager
        /// </summary>
        private DirectoryManager directoryManager;        
        
        /// <summary>
        /// time encode started
        /// </summary>
        private DateTime timeStarted = DateTime.Now;
        
        /// <summary>
        /// time we last wrote progress to log, cuts down on amount of updates
        /// </summary>
        private DateTime timeLastLogProgressWrite = DateTime.Now;

        /// <summary>
        /// source path in the working directory
        /// </summary>
        private string workingSourcePath;
        
        /// <summary>
        /// Initializes a new instance of the EncodeJob class
        /// </summary>
        /// <param name="directoryManager">directory manager</param>
        /// <param name="sourceFile">source file to encode</param>
        public EncodeJob()
        {
        }

        public void Initialise(DirectoryManager directoryManager, SourceFile sourceFile)
        {
            this.directoryManager = directoryManager;
            SourceFile = sourceFile;
            string dateTime = Utilities.FilenameFriendlyDateTime(this.timeStarted);
            string filename = Path.GetFileName(SourceFile.FilePath);
            this.workingSourcePath = Path.Combine(this.directoryManager.WorkingDirectory, Path.GetFileNameWithoutExtension(filename) + ".Source." + Environment.MachineName + "." + dateTime + Path.GetExtension(filename));
        }

        /// <summary>
        /// Encode started
        /// </summary>
        public event EventHandler<EventArgs> EncodeStarted;

        /// <summary>
        /// Encode ended
        /// </summary>
        public event EventHandler<EventArgs> EncodeEnded;

        /// <summary>
        /// Encode errored
        /// </summary>
        public event EventHandler<EventArgs> EncodeError;        
        
        /// <summary>
        /// Gets or sets a value indicating whether to stop encoding
        /// </summary>
        public bool Stop 
        { 
            get; 
            set; 
        }

        /// <summary>
        /// Gets underlying encoder job
        /// </summary>
        public Job EncoderJob 
        { 
            get; 
            private set; 
        }
        
        /// <summary>
        /// Gets output log for this job
        /// </summary>
        public TraceListener JobLog
        {
            get;
            private set;            
        }

        /// <summary>
        /// Gets source file for encoding
        /// </summary>
        public SourceFile SourceFile 
        { 
            get; 
            private set; 
        }        
        
        /// <summary>
        /// do the real work - the file copy and encoding. logs progress
        /// </summary>
        public void Execute()
        {
            // We will do one encode per preset, but dont even attempt until there is a valid preset
            string[] presetFiles = this.directoryManager.PresetFiles;
            if (presetFiles.Length == 0)
            {
                throw new WatcherException("There are no presets to encode to. Add presets to the 'Presets' watch directory!");
            }

            // we try to steal the file, watcher services on other machines looking at this watch share may be 
            // trying to claim this also - if this returns true, *we* got it.
            if (Utilities.ResilientMoveFile(SourceFile.FilePath, this.workingSourcePath, null))
            {
                Watcher.WatcherEventLog.WriteEntry("Begin Processing file " + SourceFile.FilePath + " - beginning encoding.", EventLogEntryType.Information);
                string filename = Path.GetFileName(SourceFile.FilePath);
                string logFilename = Path.Combine(this.directoryManager.LogDirectory, Path.GetFileNameWithoutExtension(filename) + "." + Utilities.FilenameFriendlyDateTime(this.timeStarted) + "." + Environment.MachineName + ".log");
                string finalLogFilename = logFilename;

                using (FileStream logFileStream = new FileStream(logFilename, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.Read))                
                {
                    this.JobLog = new FlushingTextWriterTraceListener(logFileStream, false);
                    this.JobLog.WriteLine("Encoding on machine: " + Environment.MachineName);
                    this.JobLog.WriteLine("Found source file: '" + SourceFile.FilePath + "'");
                    bool success = true;

                    try
                    {
                        foreach (string presetFile in presetFiles)
                        {
                            success &= this.EncodeFile(this.workingSourcePath, this.directoryManager.WorkingDirectory, Path.GetFileNameWithoutExtension(filename), presetFile, this.JobLog);
                        }
                    }
                    catch (Exception ex)
                    {
                        this.JobLog.WriteLine(ex.ToString() + "\r\n" + this.workingSourcePath);
                        Watcher.WatcherEventLog.WriteEntry("Failure to encode " + this.workingSourcePath + " check log for that file", EventLogEntryType.Error);
                        success = false;
                    }
                    finally
                    {
                        try
                        {
                            File.Delete(this.workingSourcePath);
                        }
                        catch (Exception ex)
                        {
                            this.JobLog.WriteLine(String.Format(CultureInfo.CurrentCulture, "Problem deleting working source file {0} - {1}", this.workingSourcePath, ex));
                        }

                        this.JobLog.WriteLine(String.Format(CultureInfo.CurrentCulture, "\r\nCompleted in {0}", (DateTime.Now - this.timeStarted)));
                        Watcher.WatcherEventLog.WriteEntry("Finished processing job for " + this.workingSourcePath + (success ? " SUCCEEDED" : " FAILED") + " LogFile=" + logFilename, success ? EventLogEntryType.SuccessAudit : EventLogEntryType.Error);

                        if (!success)
                        {
                            finalLogFilename = Path.Combine(Path.GetDirectoryName(logFilename), Path.GetFileNameWithoutExtension(logFilename)) + ".FAILED" + Path.GetExtension(logFilename);
                        }

                        this.JobLog.Close();
                        if (!String.Equals(logFilename, finalLogFilename, StringComparison.OrdinalIgnoreCase))
                        {
                            Utilities.RenameFileMove(logFilename, ref finalLogFilename, null);
                        }

                        UserNotification.EncodingFinished(filename, finalLogFilename, success);
                    }
                }
            }
        }

        /// <summary>
        /// dump useful encoding information into log
        /// </summary>
        /// <param name="mediaItem">item to be encoded</param>
        /// <param name="strCR">carriage return characters to use</param>
        /// <returns>detailed information about mediaitem</returns>
        private static string EncodeInformation(MediaItem mediaItem, string strCR)
        {
            return "Encode job Details" + strCR +
                    "\tSource File" + strCR +
                    "\t\tName: " + mediaItem.MainMediaFile.FileName + strCR +
                    "\t\tDuration: " + mediaItem.MainMediaFile.Duration + strCR +
                    "\tOutput File" + strCR +
                    "\t\tOutput Filename: " + mediaItem.ActualOutputFileFullPath + strCR +
                    "\t\tEstimated File Size: " + mediaItem.OutputFileEstimatedSize + strCR +
                    "\t\tFormat: " + mediaItem.OutputFormat + strCR +
                    "\t\tFrame Rate: " + mediaItem.OutputFrameRate + strCR +
                    "\t\tAspect Ratio: " + mediaItem.OutputAspectRatio + strCR +
                    "\t\tTotal Bitrate: " + mediaItem.TotalBitrate + strCR +
                    "\t\tVideo Size: " + mediaItem.VideoSize + strCR;
        }

        /// <summary>
        /// encode file to preset
        /// </summary>
        /// <param name="sourcePath">filepath to encode</param>
        /// <param name="workingDirectory">working directory</param>
        /// <param name="originalFilenameNoExt">original filename without extension</param>
        /// <param name="presetPath">path to preset to use for this encoding</param>
        /// <param name="logFile">logging file</param>
        /// <returns>true if successful</returns>
        private bool EncodeFile(string sourcePath, string workingDirectory, string originalFilenameNoExt, string presetPath, TraceListener logFile)
        {
            bool success = true;
            string presetOutputDirectory = Path.Combine(this.directoryManager.OutputDirectory, Path.GetFileNameWithoutExtension(presetPath));
            this.EncoderJob = new Job();            

            try
            {
                logFile.WriteLine("\r\n\r\n------------------------------------------------------------\r\n");
                logFile.WriteLine("Preset: " + Path.GetFileNameWithoutExtension(presetPath));
                logFile.WriteLine("Source: " + sourcePath);
                
                Directory.CreateDirectory(presetOutputDirectory);

                // encode
                Preset preset = Preset.FromFile(presetPath);

                MediaItem mediaItem = new MediaItem(sourcePath);
                mediaItem.ApplyPreset(preset);
                mediaItem.OutputFileName = "{Original file name}.Target.{Default extension}";

                this.EncoderJob.MediaItems.Add(mediaItem);

                this.EncoderJob.OutputDirectory = workingDirectory;
                this.EncoderJob.CreateSubfolder = false;
                this.EncoderJob.EncodeProgress += new EventHandler<EncodeProgressEventArgs>(this.Job_EncodeProgress);
                this.EncoderJob.EncodeCompleted += new EventHandler<EncodeCompletedEventArgs>(this.Job_EncodeCompleted);

                UserNotification.EncodingStarting(sourcePath + "->" + mediaItem.ActualOutputFileFullPath, EncodeInformation(mediaItem, "<br>"));
                this.JobLog.WriteLine(EncodeInformation(mediaItem, "\r\n"));

                if (this.EncodeStarted != null)
                {
                    this.EncodeStarted(this, null);
                }

                this.EncoderJob.Encode();

                string outputSourcePath = Path.Combine(workingDirectory, mediaItem.ActualOutputFileName);
                string outputTargetPath = Path.Combine(presetOutputDirectory, originalFilenameNoExt + Path.GetExtension(mediaItem.ActualOutputFileName));

                Utilities.RenameFileMove(outputSourcePath, ref outputTargetPath, logFile);
                logFile.WriteLine("\r\n\r\nSuccess: Created Output file '" + outputTargetPath + "'");

                if (this.EncodeEnded != null)
                {
                    this.EncodeEnded(this, null);
                }
            }
            catch (Exception ex)
            {
                logFile.WriteLine("\r\n\r\nException: " + ex.Message);
                success = false;

                if (this.EncodeError != null)
                {
                    this.EncodeError(this, null);
                }
            }

            return success;
        }
        
        /// <summary>
        /// encode finished.
        /// </summary>
        /// <param name="sender">Encode job</param>
        /// <param name="e">encode complete event arguments</param>
        private void Job_EncodeCompleted(object sender, EncodeCompletedEventArgs e)
        {
            if (this.JobLog != null)
            {
                this.JobLog.Write("Progress:100.00% - Complete\r\n");
            }

            this.timeLastLogProgressWrite = DateTime.Now;      
        }
        
        /// <summary>
        /// update log with encoding status
        /// </summary>
        /// <param name="sender">the Expression.Encoder.Job doing this encoding</param>
        /// <param name="args">progress information</param>
        private void Job_EncodeProgress(object sender, EncodeProgressEventArgs args)
        {
            // only write progress to log at a max frequency of once every so many seconds to reduce log size
            if (DateTime.Now - this.timeLastLogProgressWrite > TimeSpan.FromSeconds(EncoderFolderWatcherService.WatcherSettings.Default.progressUpdateIntervalSeconds))
            {
                if (this.JobLog != null)
                {
                    if (args.TotalPasses > 1)
                    {
                        this.JobLog.Write(String.Format(CultureInfo.CurrentCulture, "Progress:{0:#.##}% Pass:{1}/{2}\r\n", args.Progress, args.CurrentPass, args.TotalPasses));
                    }
                    else
                    {
                        this.JobLog.Write(String.Format(CultureInfo.CurrentCulture, "Progress:{0:#.##}%\r\n", args.Progress));
                    }
                }

                this.timeLastLogProgressWrite = DateTime.Now;
            }

            args.Cancel = this.Stop;
            if (this.Stop)
            {
                this.JobLog.Write("Encode stopped.");
            }            
        }       
    }
}
