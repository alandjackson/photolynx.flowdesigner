﻿//-----------------------------------------------------------------------
// <copyright file="EncoderFolderWatcherService.cs" company="Microsoft Corporation">
//     Copyright (c) Microsoft Corporation
// </copyright>
//-----------------------------------------------------------------------

namespace EncoderFolderWatcherService
{
    using System.ServiceProcess;

    /// <summary>
    /// implements the folder watcher as a service
    /// </summary>
    public class FolderWatcherService : ServiceBase
    {
        /// <summary>
        /// watcher instance that service uses
        /// </summary>
        private EncoderWatcher.Watcher watcher;

        /// <summary>
        /// Initializes a new instance of the FolderWatcherService class
        /// </summary>
        public FolderWatcherService()
        {            
        }

        /// <summary>
        /// start service
        /// </summary>
        /// <param name="args">arguments to pass to service (ignored)</param>
        protected override void OnStart(string[] args)
        {
            this.watcher = new EncoderWatcher.Watcher();
            if (this.watcher != null)
            {
                this.watcher.Start();
            }
        }

        /// <summary>
        /// stop service and cleanup
        /// </summary>
        protected override void OnStop()
        {
            if (this.watcher != null)
            {
                this.watcher.Stop();
                this.watcher.Dispose();
            }
        }
    }
}
