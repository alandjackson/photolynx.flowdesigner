﻿//-----------------------------------------------------------------------
// <copyright file="WatcherServiceInstaller.cs" company="Microsoft Corporation">
//     Copyright (c) Microsoft Corporation
// </copyright>
//-----------------------------------------------------------------------

namespace EncoderFolderWatcherService
{
    using System.ComponentModel;
    using System.Configuration.Install;
    using System.ServiceProcess;

    /// <summary>
    /// installer class for the watcher service
    /// </summary>
    [RunInstaller(true)]
    public class WatcherServiceInstaller : Installer
    {
        /// <summary>
        /// service name
        /// </summary>
        public const string ServiceName = "Microsoft Expression Encoder 4 Folder Watcher";

        /// <summary>
        /// installer service
        /// </summary>
        private ServiceInstaller serviceInstaller = new ServiceInstaller();

        /// <summary>
        /// service process installer
        /// </summary>
        private ServiceProcessInstaller processInstaller = new ServiceProcessInstaller();

        /// <summary>
        /// Initializes a new instance of the WatcherServiceInstaller class
        /// </summary>
        public WatcherServiceInstaller()
        {            
            // Service will run under a user specified account 
            this.processInstaller.Account = ServiceAccount.User;   

            this.serviceInstaller.StartType = ServiceStartMode.Automatic;
            this.serviceInstaller.ServiceName = ServiceName;
            this.serviceInstaller.DisplayName = this.serviceInstaller.ServiceName;
            this.serviceInstaller.Description = "Encodes videos placed into a folder that is watched";

            this.serviceInstaller.AfterInstall += new InstallEventHandler(this.ServiceInstaller_AfterInstall);

            this.Context = new InstallContext();
            this.Context.Parameters["assemblypath"] = typeof(WatcherServiceInstaller).Module.FullyQualifiedName;

            Installers.Add(this.serviceInstaller);
            Installers.Add(this.processInstaller);
        }

        /// <summary>
        /// operations after install is complete
        /// </summary>
        /// <param name="sender">service installer</param>
        /// <param name="e">Event Arguments</param>
        private void ServiceInstaller_AfterInstall(object sender, InstallEventArgs e)
        {
            // Allow "DesktopInteraction" - needed? only if want to show UI (for debugging, asserts - etc)
           
            // RegistryKey key = Registry.LocalMachine.OpenSubKey(@"SYSTEM\CurrentControlSet\Services\" + serviceInstaller.ServiceName, true);
            // if (key != null && key.GetValue("Type") != null)
            // {
            //     key.SetValue("Type", ((int)key.GetValue("Type") | 256));
            // }
        }
    }
}
