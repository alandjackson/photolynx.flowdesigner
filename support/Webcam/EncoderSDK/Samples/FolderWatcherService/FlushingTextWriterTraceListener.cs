﻿//-----------------------------------------------------------------------
// <copyright file="FlushingTextWriterTraceListener.cs" company="Microsoft Corporation">
//     Copyright (c) Microsoft Corporation
// </copyright>
//-----------------------------------------------------------------------

namespace EncoderWatcher
{
    using System;
    using System.Diagnostics;
    using System.IO;

    /// <summary>
    /// exactly the same as a textwriter but flushes the output on each write so output log stays
    /// very current, slower - but current.
    /// </summary>
    public class FlushingTextWriterTraceListener : TextWriterTraceListener
    {
        /// <summary>
        /// show timecode on logs?
        /// </summary>
        private bool showTime;

        /// <summary>
        /// Initializes a new instance of the FlushingTextWriterTraceListener class
        /// </summary>
        /// <param name="stream">file to write to</param>
        /// <param name="showTime">prefix logs with timecode</param>
        public FlushingTextWriterTraceListener(Stream stream, bool showTime)
            : base(stream)
        {
           this.showTime = showTime;
        }

        /// <summary>
        /// write line and carriage treutn to log and flush
        /// </summary>
        /// <param name="message">string to write to log</param>
        public override void WriteLine(string message)
        {
            base.WriteLine(this.Prefix() + message);
            Flush();
        }

        /// <summary>
        /// write string to log and flush
        /// </summary>
        /// <param name="message">string to write to log</param>
        /// <param name="category">category for log</param>
        public override void Write(string message, string category)
        {
            base.Write(this.Prefix() + message, category);
            Flush();
        }

        /// <summary>
        /// write object to log and flush
        /// </summary>
        /// <param name="o">object to write</param>
        /// <param name="category">category for log</param>
        public override void Write(object o, string category)
        {
            base.Write(this.Prefix() + o, category);
            Flush();
        }

        /// <summary>
        /// write object and carriage return to log and flush
        /// </summary>
        /// <param name="o">object to write to log</param>
        /// <param name="category">category for log</param>
        public override void WriteLine(object o, string category)
        {
            base.WriteLine(this.Prefix() + o, category);
            Flush();
        }

        /// <summary>
        /// write string and carriage return to log and flush
        /// </summary>
        /// <param name="message">string to write to log</param>
        /// <param name="category">category for log</param>
        public override void WriteLine(string message, string category)
        {
            base.WriteLine(this.Prefix() + message, category);
            Flush();
        }

        /// <summary>
        /// prepend time if required
        /// </summary>
        /// <returns>text to prepend</returns>
        private string Prefix()
        {
            if (this.showTime)
            {
                return DateTime.Now + "\t";
            }

            return String.Empty;
        }
    }
}
