﻿using System;
using System.Threading;
using System.Windows;
using Microsoft.Expression.Encoder;
using Microsoft.Win32;
using System.Windows.Threading;

namespace WPFEncode
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        public WPFEncoder encoder;

        /// <summary>
        /// Initializes a new instance of the Window1 class
        /// </summary>
        public Window1()
        {
            encoder = new WPFEncoder();
            DataContext = encoder;
            InitializeComponent();
        }

        /// <summary>
        /// Called when the user clicks to the browse button to select a filename.
        /// </summary>
        /// <param name="sender">The sender of the event.</param>
        /// <param name="e">The event arguments.</param>
        private void OnBrowseClick(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            Nullable<bool> result = dialog.ShowDialog();
            if (result == true)
            {
                textBoxFileName.Text = dialog.FileName;
            }
        }

        /// <summary>
        /// Called when the user clicks the encode button.
        /// </summary>
        /// <param name="sender">The sender of the event.</param>
        /// <param name="e">The event arguments.</param>
        private void OnEncodeClick(object sender, RoutedEventArgs e)
        {
            encoder.Start();
        }

        /// <summary>
        /// Called when the close button is pressed. If we currently have a file being encoded,
        /// then we prompt the user before encoding.
        /// </summary>
        /// <param name="sender">The sender of the event.</param>
        /// <param name="e">The event arguments.</param>
        private void OnClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!encoder.IsIdle)
            {
                // We're still encoding, so warn the user before leaving
                MessageBoxResult result = MessageBox.Show("The file is still encoding. Are you sure you want to exit?", "WPF Encode", MessageBoxButton.YesNo);
                if (result == MessageBoxResult.Yes)
                {
                    // If the user still wants to exit then cancel the encode
                    // and wait for the encode thread to finish.
                    encoder.Stop();
                }
                else
                {
                    // The user didn't want to exit so cancel the close.
                    e.Cancel = true;
                }
            }
        }
    }
}
