﻿using System;
using System.ComponentModel;
using System.Threading;
using System.Windows;
using Microsoft.Expression.Encoder;
using Microsoft.Win32;
using System.Windows.Threading;

namespace WPFEncode
{
    public class WPFEncoder : INotifyPropertyChanged
    {
        /// <summary>
        /// Public event if any property is changed
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Indicates if the encode should be cancelled.
        /// </summary>
        private bool cancelEncode;

        /// <summary>
        /// Seperate Encode Thread.
        /// </summary>
        private Thread thread;

        /// <summary>
        /// Indicates if we are busy or not.
        /// </summary>
        private bool isIdle;
        public bool IsIdle
        {
            get { return isIdle; }
            set { isIdle = value; onPropChanged("IsIdle"); }
        }

        /// <summary>
        /// The filename of the file we wish to encode.
        /// </summary>
        private string fileName;
        public string FileName
        {
            get { return fileName; }
            set { fileName = value; onPropChanged("FileName"); }
        }

        /// <summary>
        /// Gets the current status of the encode.
        /// </summary>
        private string status;
        public string Status
        {
            get { return status; }
            set { status = value; onPropChanged("Status"); }
        }

        /// <summary>
        /// Gets the current encode progress
        /// </summary>
        private double progress;
        public double Progress
        {
            get { return progress; }
            set { progress = value; onPropChanged("Progress"); }
        }

        internal WPFEncoder()
        {
            isIdle = true;
        }

        /// <summary>
        /// Starts encoding in seperate thread
        /// </summary>
        internal void Start()
        {
            // Sets Status of encode
            Status = "Analyzing";
            IsIdle = false;

            // Verifies file path is entered for encode.
            if(string.IsNullOrWhiteSpace(fileName))
            {
                Status = "No file entered.";
                IsIdle = true;
                return;
            }

            // Creates encode thread and starts it
            thread = new Thread(new ThreadStart(EncodeThread));
            thread.Start();
        }

        /// <summary>
        /// Stops the encode in progress and closes thread.
        /// </summary>
        internal void Stop()
        {
            cancelEncode = true;
            thread.Join();
        }

        private void EncodeThread()
        {
            // Create the media item and validates it.
            MediaItem mediaItem;
            try
            {
                mediaItem = new MediaItem(FileName);
            }
            catch (InvalidMediaFileException exp)
            {
                Status = exp.Message;
                IsIdle = true;
                return;
            }

            // Create the job, add the media item and encode.
            using (Job job = new Job())
            {
                job.MediaItems.Add(mediaItem);
                job.OutputDirectory = @"C:\output";

                job.EncodeProgress += new EventHandler<EncodeProgressEventArgs>(OnProgress);

                job.Encode();
            }
            Status = "Finished.";
            IsIdle = true;
        }

        /// <summary>
        /// Called when encoding progress occurs.
        /// </summary>
        /// <param name="sender">The sender of the event.</param>
        /// <param name="e">The event arguments.</param>
        private void OnProgress(object sender, EncodeProgressEventArgs e)
        {
            // If we should cancel then indcate that.
            if (cancelEncode)
            {
                e.Cancel = true;
                IsIdle = true;
                return;
            }

            Progress = e.Progress;
            Status = string.Format("{0:F1}%", e.Progress);
        }

        /// <summary>
        /// Updates status of variable on update.
        /// </summary>
        /// <param name="propName"></param>
        private void onPropChanged(string propName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }
    }
}
