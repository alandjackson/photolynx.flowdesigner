﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Expression.Encoder;
using System.IO;

namespace PerformanceHarness
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Reflection;
    using System.Text;
    using System.Diagnostics;
    using Microsoft.Expression.Encoder;

    public static class Program
    {
        private static string input;
        private static DateTime timeStartFirstPass;
        private static DateTime timeStartSecondPass;
        private static DateTime timeEndFirstPass;
        private static DateTime timeEndSecondPass;
        private static bool twoPass = false;
        private static bool on2ndPass = false;
        private static int firstPassSampleCount = 0;
        private static int secondPassSampleCount = 0;
        private static double currentProgress = 0;
        private static int lastSampleProgress = 0;
        private static PerformanceCounter cpuCounter = new PerformanceCounter();
        private static double firstPassCpuLoad;
        private static double secondPassCpuLoad;
        private static int cpuStreamCount = 0;
        private static int gpuStreamCount = 0;
        private static bool verbose = false;
        private static int maxGPUStreamCount = 0;
        private static bool useGPU;
        private static bool isDir;

        private static int Main(string[] args)
        {
            cpuCounter.CategoryName = "Processor";
            cpuCounter.CounterName = "% Processor Time";
            cpuCounter.InstanceName = "_Total";

            if (args.Length < 1 || args.Length > 4)
            {
                return ShowError("Invalid number of parameters");
            }

            // Parses input and returns if invalid parameters
            int parse = ParseInput(args);
            if (parse != 0)
                return parse;

            System.Threading.Thread.Sleep(TimeSpan.FromSeconds(5));

            // Logging general information
            LogGeneralInfo();

            bool success = true;
            if (isDir)
            {
                string[] jobs = Directory.GetFiles(input, "*.xej");
                foreach (string job in jobs)
                {
                    if (!EncodeJob(job, useGPU))
                    {
                        success = false;
                        Console.WriteLine("Failed to encode job: {0}.", job);
                    }
                }
            }
            else
            {
                success = EncodeJob(input, useGPU);
            }

            return success ? 0 : 1;
        }

        /// <summary>
        /// Parses the user input and sets settings flags.
        /// </summary>
        private static int ParseInput(string[] args)
        {
            // Parsing the option arguments
            foreach (string arg in args)
            {
                string[] lowerArg = arg.ToLower().Split('=');
                switch (lowerArg[0])
                {
                    case "/usegpu":
                        useGPU = true;
                        break;
                    case "/verbose":
                        verbose = true;
                        break;
                    case "/maxgpustream":
                        if (lowerArg.Length == 2)
                        {
                            int count;
                            if (int.TryParse(lowerArg[1], out count))
                                maxGPUStreamCount = count;
                            else
                                return ShowError("Invalid MaxGPUStream argument.");
                        }
                        else
                            return ShowError("Invalid Parameter");
                        break;
                    default:
                        if (Directory.Exists(lowerArg[0]))
                        {
                            input = lowerArg[0];
                            isDir = true;
                        }
                        else if (File.Exists(lowerArg[0]) && Path.GetExtension(lowerArg[0]) == ".xej")
                            input = lowerArg[0];
                        else
                            return ShowError("Invalid File or Directory");
                        break;
                }
            }
            if (string.IsNullOrWhiteSpace(input))
                return ShowError("No valid File or Directory entered.");

            return 0;
        }

        /// <summary>
        /// Logs out general information, like date/time, Encoder version and hardware information.
        /// </summary>
        private static void LogGeneralInfo()
        {
            // Run Date
            Console.WriteLine("Run Date/Time: {0}", DateTime.Now.ToString());

            // Encoder Version
            string path = Assembly.GetAssembly(typeof(Job)).CodeBase;
            FileVersionInfo myFileVersionInfo = FileVersionInfo.GetVersionInfo(path.Replace("file:///", ""));
            Console.WriteLine("Encoder Version: " + myFileVersionInfo.FileVersion);

            // Machine Name
            Console.WriteLine("Machine Name: {0}", Environment.MachineName);

            // OS
            Console.Write("OS: {0}", Environment.OSVersion.ToString());

            // 32 or 64-bit?
            if (Directory.Exists(Environment.GetEnvironmentVariable("SystemDrive") + "\\Program Files (x86)"))
                Console.WriteLine(" (64-bit)");
            else
                Console.WriteLine(" (32-bit)");

            // Number of Logical Cores
            Console.WriteLine("Logical Cores: {0}", Environment.ProcessorCount);

            // Names of the Encode Devices
            string gpuString = ": None";
            if (H264EncodeDevices.Devices.Count > 0)
            {
                gpuString = ": ";

                for (int i = 0; i < H264EncodeDevices.Devices.Count; i++)
                {
                    gpuString += string.Format("{0}\t", H264EncodeDevices.Devices[i].Name);
                }
            }
            Console.WriteLine("GPU Device(s){0}", gpuString);
        }

        private static void CleanUpCounters()
        {
            twoPass = false;
            on2ndPass = false;
            firstPassSampleCount = 0;
            secondPassSampleCount = 0;
            currentProgress = 0;
            lastSampleProgress = 0;
            firstPassCpuLoad = 0;
            secondPassCpuLoad = 0;
            cpuStreamCount = 0;
            gpuStreamCount = 0;
        }

        /// <summary>
        /// Encodes the job and profiles the performance
        /// </summary>
        /// <param name="input">Input job file</param>
        /// <param name="usingGpu">true if GPU encoding should be enabled</param>
        /// <returns>true if success</returns>
        private static bool EncodeJob(string input, bool usingGpu)
        {
            CleanUpCounters();

            Job job = null;
            try { job = Job.LoadJob(input); }
            catch (InvalidJobException) { return false; }
            catch (InvalidMediaFileException) { return false; }
            catch (FeatureNotAvailableException) { return false; }

            Console.Write(Environment.NewLine + "Encoding Job " + Path.GetFileName(input));

            // Set up the progress callback function
            job.EncodeProgress += new EventHandler<EncodeProgressEventArgs>(OnProgress);

            Console.WriteLine(usingGpu ? " using GPU ..." : " using CPU ...");
            try
            {
                H264EncodeDevices.EnableGpuEncoding = usingGpu;
                H264EncodeDevices.MaxNumberGPUStream = maxGPUStreamCount > 0 ? maxGPUStreamCount : H264EncodeDevices.MaxNumberGPUStream;
            }
            catch (FeatureNotAvailableException) { }

            timeStartFirstPass = DateTime.Now;

            try
            {
                job.Encode();
            }
            catch (Exception e)
            {
                string secString = twoPass && on2ndPass ? " [2nd pass]" : "";
                Console.WriteLine("ERROR: Exception was thrown at {0:F1}% {1}", currentProgress, secString);
                Console.WriteLine(e.ToString() + Environment.NewLine);
                return false;
            }

            if (twoPass)
            {
                timeEndSecondPass = DateTime.Now;
            }
            else
            {
                timeEndFirstPass = DateTime.Now;
            }

            // Reporting results
            TimeSpan firstPassEncodeTime = timeEndFirstPass - timeStartFirstPass;
            TimeSpan secondPassEncodeTime = timeEndSecondPass - timeStartSecondPass;

            double frameRate = ((AudioVideoFile)(job.MediaItems[0].Sources[0].MediaFile)).VideoStreams[0].FrameRate;
            double duration = job.MediaItems[0].Sources[0].Clips[0].EndTime.TotalSeconds;
            double fpsFirstPass = frameRate * duration / firstPassEncodeTime.TotalSeconds;
            double fpsSecondPass = frameRate * duration / secondPassEncodeTime.TotalSeconds;
            double fpsTotal = frameRate * duration / (firstPassEncodeTime.TotalSeconds + secondPassEncodeTime.TotalSeconds);

            string fpsString = "";
            if (frameRate > 0.1)
                fpsString = " ({1:F2} fps)";

            Console.WriteLine();
            if (twoPass)
            {
                Console.WriteLine("1st Pass Encoding time: {0}" + fpsString, firstPassEncodeTime, fpsFirstPass);
                Console.WriteLine("2nd Pass Encoding time: {0}" + fpsString, secondPassEncodeTime, fpsSecondPass);
            }

            Console.WriteLine("Total Encoding time: {0}" + fpsString, firstPassEncodeTime + secondPassEncodeTime, fpsTotal);

            if (twoPass)
            {
                Console.WriteLine("1st Pass Average CPU Load: {0:F1}%", firstPassCpuLoad / firstPassSampleCount);
                Console.Write("2nd Pass Average CPU Load: {0:F1}%", secondPassCpuLoad / secondPassSampleCount);
            }
            else
            {
                Console.Write("Average CPU Load: {0:F1}%", firstPassCpuLoad / firstPassSampleCount);
            }

            if (usingGpu)
            {
                Console.Write("  GPU Streams: {0}/{1}", gpuStreamCount, gpuStreamCount + cpuStreamCount);
            }

            Console.Write(Environment.NewLine);

            job.Dispose();

            return true;
        }

        static void OnProgress(object sender, EncodeProgressEventArgs e)
        {
            twoPass = e.TotalPasses == 2;
            currentProgress = e.Progress;

            if (cpuStreamCount == 0 && e.StreamEncodeTypes.Count > 0)
            {
                cpuStreamCount = gpuStreamCount = 0;
                bool ready = true;
                foreach (StreamEncodeType type in e.StreamEncodeTypes)
                {
                    switch (type)
                    {
                        case StreamEncodeType.Software:
                            cpuStreamCount++;
                            break;
                        case StreamEncodeType.Hardware:
                            gpuStreamCount++;
                            break;
                        default:
                            ready = false;
                            break;
                    }
                    if (!ready)
                        cpuStreamCount = 0;
                }
            }

            if (twoPass && !on2ndPass && e.CurrentPass == e.TotalPasses)
            {
                on2ndPass = true;
                timeEndFirstPass = timeStartSecondPass = DateTime.Now;
            }

            if (lastSampleProgress != (int)e.Progress)
            {
                lastSampleProgress = (int)e.Progress;
                double current = cpuCounter.NextValue();
                if (verbose)
                {
                    Console.Write("\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b");
                    Console.Write("Pass {0}/{1} - {2:F1}%  CPU Load: {3:F1}%", e.CurrentPass, e.TotalPasses, e.Progress, current);
                }

                if (on2ndPass)
                {
                    secondPassSampleCount++;
                    secondPassCpuLoad += current;
                }
                else
                {
                    firstPassSampleCount++;
                    firstPassCpuLoad += current;
                }
            }
        }

        private static int ShowError(string error)
        {
            Console.WriteLine("ERROR: " + error + "\n");
            WriteUsage();
            return 1;
        }

        private static void WriteUsage()
        {
            Console.WriteLine("Usage: PerfHarness.exe /UseGPU /MaxGPUStream=<Y> <job file or directory>");
            Console.WriteLine(" ");
            Console.WriteLine("    /UseGPU: Enables GPU encoding (Cuda or Intel QSV)");
            Console.WriteLine("    /MaxGPUStream=5: Max count of streams to be encoded by the GPU (default=5)");
            Console.WriteLine("    <job file or directory> - .xej file to encode [REQUIRED]");
        }
    }
}

