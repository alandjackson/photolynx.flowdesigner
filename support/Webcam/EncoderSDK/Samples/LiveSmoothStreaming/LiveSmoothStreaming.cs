﻿using System;
using System.ComponentModel;
using System.IO;
using Microsoft.Expression.Encoder;
using Microsoft.Expression.Encoder.Live;

namespace LiveSmoothStreaming
{
    public class Streamer : INotifyPropertyChanged
    {
        // The types of Encoding available
        internal enum Output { Archive, Publish };

        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Value for whether streaming is occuring
        /// </summary>
        private bool busy;
        public bool Busy
        {
            get { return busy; }
            set { busy = value; onPropChanged("Busy"); }
        }

        // Variables to hold file and path information
        private string sourcePath;
        public string SourcePath
        {
            get { return sourcePath; }
            set { sourcePath = value; onPropChanged("SourcePath"); }
        }

        private string destinationPath;
        public string DestinationPath
        {
            get { return destinationPath; }
            set { destinationPath = value; onPropChanged("DestinationPath"); }
        }

        // Determines if we are streaming
        private EncodeStatus status;
        public EncodeStatus Status
        {
            get { return status; }
            set { status = value; onPropChanged("Status"); }
        }
        
        // Job for encoding
        private LiveJob job;

        // Type of encoding
        internal Output publishType;

        internal Streamer()
        {
            publishType = Output.Archive;
            Status = EncodeStatus.Hold;
        }

        internal bool StartStream()
        {
            Busy = true;
            // Instantiates a new job for encoding
            job = new LiveJob();

            // Verifies all information is entered
            if (string.IsNullOrWhiteSpace(sourcePath) || string.IsNullOrWhiteSpace(destinationPath))
                return false;

            job.Status += new EventHandler<EncodeStatusEventArgs>(StreamStatus);

            LiveFileSource fileSource;
            try
            {
                // Sets file to active source and checks if it is valid
                fileSource = job.AddFileSource(sourcePath);
            }
            catch (InvalidMediaFileException)
            {
                return false;
            }

            // Sets to loop media for streaming
            fileSource.PlaybackMode = FileSourcePlaybackMode.Loop;

            // Makes this file the active source. Multiple files can be added 
            // and cued to move to each other at their ends
            job.ActivateSource(fileSource);

            // Finds and applys a smooth streaming preset
            job.ApplyPreset(LivePresets.VC1IISSmoothStreaming720pWidescreen);

            // Sets up variable for fomat data
            switch (publishType)
            {
                case Output.Archive:
                    // Verifies destination path exists and if not creates it
                    try
                    {
                        if (!Directory.Exists(destinationPath))
                            Directory.CreateDirectory(destinationPath);
                    }
                    catch(IOException)
                    {
                        return false;
                    }

                    FileArchivePublishFormat archiveFormat = new FileArchivePublishFormat();

                    // Gets the location of the old extention and removes it
                    string filename = Path.GetFileNameWithoutExtension(sourcePath);

                    // Sets the archive path and file name
                    archiveFormat.OutputFileName = Path.Combine(destinationPath, filename + ".ismv");
                    job.PublishFormats.Add(archiveFormat);
                    break;

                case Output.Publish:
                    // Setups streaming of media to publishing point
                    PushBroadcastPublishFormat publishFormat = new PushBroadcastPublishFormat();
                    try
                    {
                        // checks the path for a valid publishing point
                        publishFormat.PublishingPoint = new Uri(destinationPath);
                    }
                    catch (UriFormatException)
                    {
                        return false;
                    }

                    // Adds the publishing format to the job
                    job.PublishFormats.Add(publishFormat);
                    break;
                default:
                    return false;
            }
            job.StartEncoding();

            return true;
        }

        internal void StopStream()
        {
            job.StopEncoding();
            Busy = false;
        }

        private void StreamStatus(object obj, EncodeStatusEventArgs e)
        {
            Status = e.Status;
            if (e.Status == EncodeStatus.EncodingError || e.Status == EncodeStatus.PublishingPointError)
            {
                Busy = false;
            }
        }

        internal void Dispose()
        {
            if(job!=null)
                job.Dispose();
        }

        void onPropChanged(string propName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }
    }
}
