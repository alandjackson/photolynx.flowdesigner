﻿using System;
using System.Windows;
using System.Windows.Forms;
using Microsoft.Expression.Encoder.Live;
using StringTable = LiveSmoothStreaming.Properties.Resources;

namespace LiveSmoothStreaming
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public Streamer streamer;

        /// <summary>
        /// Initializes a new instance of the MainWindow class
        /// </summary>
        public MainWindow()
        {
            streamer = new Streamer();
            DataContext = streamer;
            InitializeComponent();
        }

        /// <summary>
        /// Browse to select a filename 
        /// </summary>
        /// <param name="sender">The sender of the event.</param>
        /// <param name="e">The event arguments.</param>
        private void FileBrowse(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Title = StringTable.SelectMedia;
            DialogResult result = dialog.ShowDialog();

            if (result == System.Windows.Forms.DialogResult.OK)
            {
                FileName.Text = dialog.FileName;
                streamer.SourcePath = dialog.FileName;
            }
        }

        /// <summary>
        /// Browse to select a destination folder 
        /// </summary>
        /// <param name="sender">The sender of the event.</param>
        /// <param name="e">The event arguments.</param>
        private void FolderBrowse(object sender, RoutedEventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            DialogResult result = dialog.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                PublishPath.Text = dialog.SelectedPath;
                streamer.DestinationPath = dialog.SelectedPath;
            }
        }

        /// <summary>
        /// Changes the Publishing Format and associated properties
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PublishFormat_DropDownClosed(object sender, EventArgs e)
        {
            switch (PublishFormat.SelectedIndex)
            {
                case 0:
                    streamer.publishType = Streamer.Output.Archive;
                    PathLabel.Content = "Folder:";
                    DestBrowse.Visibility = Visibility.Visible;
                    break;
                case 1:
                    streamer.publishType = Streamer.Output.Publish;
                    PathLabel.Content = "Publish:";
                    DestBrowse.Visibility = Visibility.Hidden;
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Called when the user checks the stream button
        /// </summary>
        /// <param name="sender">The sender of the event.</param>
        /// <param name="e">The event arguments.</param>
        private void StartStream(object sender, RoutedEventArgs e)
        {
            if (!streamer.StartStream())
            {
                streamer.Busy = false;
                MessageBoxResult result = System.Windows.MessageBox.Show(StringTable.invalidPaths, "Error");
            }
            else
                Button_3.Content = "Stop Streaming";
        }

        /// <summary>
        /// Called when the user unchecks Stream Button
        /// </summary>
        /// <param name="sender">The sender of the event.</param>
        /// <param name="e">The event arguments.</param>
        private void StopStream(object sender, RoutedEventArgs e)
        {
            if (streamer.Busy)
            {
                // Stops the thread and changes the button message
                streamer.StopStream();
                Button_3.Content = "Start Streaming";
            }
        }

        /// <summary>
        /// Called when the close button is pressed. If we currently have a file being encoded,
        /// then we prompt the user before encoding.
        /// </summary>
        /// <param name="sender">The sender of the event.</param>
        /// <param name="e">The event arguments.</param>
        private void OnClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // Checks if we are streaming and if so prompts to terminate
            if (streamer.Busy)
            {
                // We're still encoding, so warn the user before leaving
                MessageBoxResult result = System.Windows.MessageBox.Show(StringTable.streaming, StringTable.appName, MessageBoxButton.YesNo);
                if (result == MessageBoxResult.No)
                {
                    // The user didn't want to exit so cancel the close.
                    e.Cancel = true;
                    return;
                }
                streamer.StopStream();
            }
            streamer.Dispose();
        }
    }
}
