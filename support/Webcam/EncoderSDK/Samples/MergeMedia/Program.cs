﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Expression.Encoder;
using Microsoft.Expression.Encoder.Profiles;


namespace MergeMedia
{
    // This sample uses the multi-source functionality to merge multiple files
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length < 2)
            {
                // Use the command-line arguments to build the list of media files to merge
                Console.WriteLine("Usage: MergeMedia <MediaFile> <MediaFile> ...");
                return;
            }

            // Create a job and the media item for the video we wish
            // to encode.
            Job job = new Job();
            MediaItem mediaItem = new MediaItem(args[0]);

            // Setting the Profiles
            mediaItem.OutputFormat = new WindowsMediaOutputFormat();

            // Use source video profile if available
            if (mediaItem.SourceVideoProfile != null)
            {
                mediaItem.OutputFormat.VideoProfile = mediaItem.SourceVideoProfile;
            }
            else
            {
                mediaItem.OutputFormat.VideoProfile = new AdvancedVC1VideoProfile()
                {
                    Size = mediaItem.MainMediaFile.VideoStreams[0].VideoSize,
                    Bitrate = new ConstantBitrate(1000)
                };

            }

            // Use source audio profile if available
            if (mediaItem.SourceAudioProfile != null)
            {
                mediaItem.OutputFormat.AudioProfile = mediaItem.SourceAudioProfile;
            }
            else
            {
                mediaItem.OutputFormat.AudioProfile = new WmaAudioProfile();
            }

            // Add all the sources
            job.MediaItems.Add(mediaItem);
            for (int i = 1; i < args.Length; i++)
            {
                mediaItem.Sources.Add(new Source(args[i]));
            }

            // Set up the progress callback function
            job.EncodeProgress
                += new EventHandler<EncodeProgressEventArgs>(OnProgress);

            // Set the output directory and encode.
            job.OutputDirectory = @"C:\output";

            // Finally encode
            job.Encode();
        }

        static void OnProgress(object sender, EncodeProgressEventArgs e)
        {
            Console.WriteLine(e.Progress);
        }
    }
}
