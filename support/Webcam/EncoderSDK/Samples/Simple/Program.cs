﻿using System;
using Microsoft.Expression.Encoder;

namespace Simple
{
    class Program
    {
        static void Main(string[] args)
        {
            // Checks to see if there are the proper number of arguments
            if (args.Length != 1)
            {
                // Use the command-line arguments to build the list of media files to encode
                Console.WriteLine("Usage: Simple <MediaFile>");
                return;
            }

            MediaItem mediaItem;
            try
            {
                // sets file name to media item
                mediaItem = new MediaItem(args[0]);
            }
            catch(InvalidMediaFileException exp)
            {
                // Media file was invalid and it returns an error msg
                Console.WriteLine(exp.ToString());
                return;
            }

            // verifies encoding of file
            Console.WriteLine("\nEncoding: {0}", args[0].ToString());

            // Create a job and the media item for the video we wish
            // to encode.
            Job job = new Job();
            job.MediaItems.Add(mediaItem);

            // Set up the progress callback function
            job.EncodeProgress
                += new EventHandler<EncodeProgressEventArgs>(OnProgress);

            // Set the output directory and encode.
            job.OutputDirectory = @"C:\output";

            // encodes job
            job.Encode();
            job.Dispose();
        }

        static void OnProgress(object sender, EncodeProgressEventArgs e)
        {
            Console.Write("\b\b\b\b\b\b\b\b");
            Console.Write("{0:F2}%", e.Progress);
        }
    }
}
