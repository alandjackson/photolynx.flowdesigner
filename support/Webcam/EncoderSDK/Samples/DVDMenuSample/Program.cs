﻿using System;
using System.Collections;
using System.Drawing;
using System.IO;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Linq;
using Microsoft.Expression.Encoder;

namespace DVDMenuSample
{
    class Program
    {
        // Sets the number of clips we will extract
        const int NUM_Clips = 4;

        // Scale for the media overlay
        const int OVERLAY_SCALE = 4;

        // Sets path for the xaml title overlay for editing and adding to file
        static string TITLE_FILE = Path.Combine(Environment.CurrentDirectory, "DVDMenuSample\\Overlay.xaml");

        static void Main(string[] args)
        {   
            // Squares the number of clips for the total number of segments to 
            // split the video into
            int numSegments = NUM_Clips << 2;
            
            // Iverse of the number of film segments to reduce the number of divisions
            double invSegments = 1.0 / numSegments;

            // Checks the arguments and advises the user of the usage
            if (args.Length != 1)
            {
                Console.WriteLine("Usage: DVDMenuSample <MediaItem>");
                return;
            }

            // stores file name for easy access
            string file = args[0];

            // Attempts to create the media file and gives an Error if it fails
            MediaItem mItem;
            try
            {
                mItem = new MediaItem(file);
            }
            catch (InvalidMediaFileException)
            {
                Console.WriteLine("Error: Invalid File or Path");
                return;
            }

            // Sets up the title overlay
            TitleOverlay(mItem);

            // Creates a temporary file for clips to save loading each pass through the loop
            MediaFile mFile = MediaFile.Create(file);

            // Sets the clip duration based on the file duration and number of segments
            double clipDuration = mFile.Duration.TotalSeconds * invSegments;

            // First clip already starts at 0, set first clip end based on clip duration
            mItem.Sources[0].Clips[0].EndTime = TimeSpan.FromSeconds(clipDuration);
            for (int i = 1; i < NUM_Clips; ++i)
            {
                // Loops through and adds clips at different points through the media file
                mItem.Sources.Add(new Source(mFile));
                mItem.Sources[i].Clips[0].StartTime = TimeSpan.FromSeconds(clipDuration * NUM_Clips * i);
                mItem.Sources[i].Clips[0].EndTime = TimeSpan.FromSeconds(clipDuration + clipDuration * NUM_Clips * i);
            }

            // Creates job for encoding media
            Job job = new Job();

            // Add the media item and set the output
            job.MediaItems.Add(mItem);
            job.OutputDirectory = @"c:\output";

            // A progress event to keep track of the encode
            job.EncodeProgress += new EventHandler<EncodeProgressEventArgs>(OnProgress);

            Console.WriteLine("Encoding Pass 1 of 2");
            job.Encode();

            // Saves the file name of the first encoded file
            string firstEncode = mItem.ActualOutputFileFullPath;

            // Sets the media item to the newly encoded file
            mItem = new MediaItem(firstEncode);

            // Sets up a video overlay of the original media item
            MediaOverlay(mItem, file);

            // Adds the newly revamped media item to the job and reencodes
            job.MediaItems.Add(mItem);
            Console.WriteLine("\nEncoding Pass 2 of 2");
            job.Encode();

            // Removes the first encoded file and cleans up the job
            File.Delete(firstEncode);
            job.Dispose();
        }

        private static void TitleOverlay(MediaItem mItem)
        {
            // Gets the media items name for the title overlay
            string title = mItem.Metadata[MetadataNames.Title];

            // Adds the Source File Name if no Title in Metadata
            if (title == null)
            {
                title = mItem.SourceFileName;
                // Removes path for source file name
                title.Remove(0, title.LastIndexOf("\\"));
            }

            // Opens xml document and loads in our title overlay
            XmlDocument doc = new XmlDocument();
            doc.Load(TITLE_FILE);
                      
            XmlNode node = doc.LastChild;
            while (node.HasChildNodes)
            {
                // Locates the node containing the label
                node = node.LastChild;
            }

            // Creates the title attribute and adds it to the xaml before saving
            XmlAttribute atribute = doc.CreateAttribute("Content");
            atribute.Value = title;
            node.Attributes.Append(atribute);

            // Sets the overlay for the title
            mItem.OverlayFileName = TITLE_FILE;

            // Sets the overlay size to be full video sized
            Rectangle overlayRect = new Rectangle(0, 0, mItem.VideoSize.Width, mItem.VideoSize.Height);
            mItem.OverlayRect = overlayRect;
        }

        private static void MediaOverlay(MediaItem mItem, string overlayFile)
        {
            // Adds the original file as an overlay
            mItem.OverlayFileName = overlayFile;

            // Plays the overlay once
            mItem.OverlayLoop = false;

            // Sets the size and location of the new overlay based on the overlay scale
            Rectangle overlayRect = new Rectangle();
            overlayRect.Size = new Size(mItem.VideoSize.Width / OVERLAY_SCALE, mItem.VideoSize.Height / OVERLAY_SCALE);
            overlayRect.Location = new Point(overlayRect.Size.Width / 2, overlayRect.Size.Height / 2);
            mItem.OverlayRect = overlayRect;
        }

        /// <summary>
        /// Gives a running status of the encode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void OnProgress(object sender, EncodeProgressEventArgs e)
        {
            Console.Write("\b\b\b\b\b{0:F1}%", e.Progress);
        }
    }
}
