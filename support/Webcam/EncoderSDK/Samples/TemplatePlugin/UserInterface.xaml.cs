﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TemplatePlugin
{
    /// <summary>
    /// Interaction logic for UserInterface.xaml
    /// </summary>
    public partial class UserInterface : UserControl
    {
        /// <summary>
        /// Plugin host
        /// </summary>
        private Microsoft.Expression.Encoder.Plugins.IPluginHost m_host;

        /// <summary>
        /// Constructor taking the plugin host
        /// </summary>
        /// <param name="host">Plugin host</param>
        public UserInterface(Microsoft.Expression.Encoder.Plugins.IPluginHost host)
        {
            m_host = host;
            InitializeComponent();
        }

        /// <summary>
        /// AddCommand button has been clicked
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="args">Event arguments</param>
        private void AddCommand(object sender, RoutedEventArgs args)
        {
            using (m_host.CreateUndoGroup("Add a custom script command"))
            {
                // Checks if there are media items. If not it ignores button press.
                if (m_host.CurrentJob.MediaItems.Count > 0)
                {
                    m_host.CurrentSelection[0].ScriptCommands.Add(
                        new Microsoft.Expression.Encoder.ScriptCommand(m_host.CurrentPlayHeadPosition, "Custom Script Command", commandName.Text));
                }
            }
        }
    }
}
