﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Microsoft.Expression.Encoder;
using Microsoft.Expression.Encoder.Devices;
using Microsoft.Expression.Encoder.Live;

namespace LiveScreenCaptureSample
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        // Data class that holds all the Encoder data
        public Data data;

        // Flag for if we are capturing or not
        private bool isCapturing;

        // Flags for resizing of capture rect
        private bool leftX, rightX;
        private bool topY, bottomY;

        // What the cursor is before we change it.
        private Cursor defaultCursor;

        public MainWindow()
        {
            InitializeComponent();

            // Sets default capture window dimensions
            int x = 300, y = 300;
            int w = 200, h = 200;

            // Set rectangle
            Canvas.SetLeft(CaptureRect, x);
            Canvas.SetTop(CaptureRect, y);
            CaptureRect.Width = w;
            CaptureRect.Height = h;

            data = new Data();
            
            DataContext = data;
            defaultCursor = this.Cursor;
        }

        private void AudioDevicesComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // Sets the audio source index based on the selection
            data.SetSource(AudioDevicesComboBox.SelectedIndex);
        }

        private void CaptureButton_Click(object sender, RoutedEventArgs e)
        {
            if (isCapturing)
            {
                isCapturing = false;
                CaptureRect.Visibility = Visibility.Visible;
                CaptureButton.Content = "Capture";
                data.Job.StopEncoding();
            }
            else
            {
                isCapturing = true; 
                CaptureRect.Visibility = Visibility.Collapsed;
                CaptureButton.Content = "Stop";
                data.Start((int)Canvas.GetLeft(CaptureRect), (int)Canvas.GetTop(CaptureRect), (int)CaptureRect.Width, (int)CaptureRect.Height);
            }
        }

        private void ExitButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void CaptureRect_MouseEnter(object sender, MouseEventArgs e)
        {
            // Gets the mouse position
            Point p = Mouse.GetPosition(this);

            // Resets the resize flags
            leftX = false; rightX = false;
            topY = false; bottomY = false;

            // Gives some wiggle room for the mouse
            double wiggle = 5.0;

            // gets the current dims of the capture rects
            double recLeft = Canvas.GetLeft(CaptureRect);
            double recUp = Canvas.GetTop(CaptureRect);
            double recRight = recLeft + CaptureRect.Width;
            double recBottom = recUp + CaptureRect.Height;

            // Checks if the mouse is close to an edge and flags it if so
            if (p.X <= recRight + wiggle && p.X >= recRight - wiggle)
                rightX = true;
            if (p.Y <= recBottom + wiggle && p.Y >= recBottom - wiggle)
                bottomY = true;

            if (p.X <= recLeft + wiggle && p.X >= recLeft - wiggle)
                leftX = true;
            if (p.Y <= recUp + wiggle && p.Y >= recUp - wiggle)
                topY = true;

            // Sets the cursor based on the flags thrown
            if (rightX || leftX)
                Cursor = Cursors.SizeWE;
            else if (topY || bottomY)
                Cursor = Cursors.SizeNS;

            if ((leftX && topY) || (rightX && bottomY))
                Cursor = Cursors.SizeNWSE;
            else if ((rightX && topY) || (leftX && bottomY))
                Cursor = Cursors.SizeNESW;
        }

        // Resets the cursor
        private void CaptureRect_MouseLeave(object sender, MouseEventArgs e)
        {
            this.Cursor = defaultCursor;
        }

        private void CaptureRect_MouseMove(object sender, MouseEventArgs e)
        {
            if (Mouse.LeftButton == MouseButtonState.Pressed)
            {
                // We are pressing the mouse so, move the rectangle.
                (sender as FrameworkElement).CaptureMouse();
                Point p = Mouse.GetPosition(this);

                if (topY)
                {
                    // We want to resize the top, not move the rect.
                    double temp = Canvas.GetTop(CaptureRect);
                    Canvas.SetTop(CaptureRect, p.Y);
                    // We have to resize the height to adjust for the change in the top of the rect.
                    CaptureRect.Height += temp - p.Y;
                }
                else if (bottomY)
                    CaptureRect.Height = p.Y - Canvas.GetTop(CaptureRect);

                if (leftX)
                {
                    // We want to resize the top, not move the rect.
                    double temp = Canvas.GetLeft(CaptureRect);
                    Canvas.SetLeft(CaptureRect, p.X);
                    // We have to resize the width to adjust for the change in the left side of the rect.
                    CaptureRect.Width += temp - p.X;
                }
                else if (rightX)
                    CaptureRect.Width = p.X - Canvas.GetLeft(CaptureRect);

                e.Handled = true;
            }
            else
            {
                // no longer holding mouse down so release the mouse
                (sender as FrameworkElement).ReleaseMouseCapture();
            }
        }

        private void ControlGrid_MouseMove(object sender, MouseEventArgs e)
        {
            // move the control grid
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                (sender as FrameworkElement).CaptureMouse();
                Point p = Mouse.GetPosition(ControlGrid);
                Thickness m = ControlGrid.Margin;
                m.Top += p.Y;
                m.Left += p.X;
                ControlGrid.Margin = m;
            }
            else
            {
                (sender as FrameworkElement).ReleaseMouseCapture();
            }
        }

        private void ControlGrid_MouseEnter(object sender, MouseEventArgs e)
        {
            Cursor = Cursors.SizeAll;
        }

        private void ControlGrid_MouseLeave(object sender, MouseEventArgs e)
        {
            Cursor = defaultCursor;
        }       
    }

    // Data class for encoder properties
    public class Data : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public LiveJob Job;
        public LiveDeviceSource Source;
        
        private ObservableCollection<EncoderDevice> m_AudioDevices;
        public ObservableCollection<EncoderDevice> AudioDevices
        {
            get { return m_AudioDevices; }
            set { m_AudioDevices = value; onPropChanged("AudioDevices"); }
        }

        private string OutputDirectory;

        public Data() 
        {
            // Initializes Job and collection objects
            Job = new LiveJob();
            AudioDevices = new ObservableCollection<EncoderDevice>();

            // Set the output directory for the job
            OutputDirectory = string.Format("{0}\\Expression\\Expression Encoder\\Live Output\\", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments));

            // Setup the the live job parameters
            SetupJob();
        }

        private void SetupJob()
        {
            // Sets the audio devices to the collection. Adds a null at the beginning for user to select no device
            AudioDevices = new ObservableCollection<EncoderDevice>(EncoderDevices.FindDevices(EncoderDeviceType.Audio));
            AudioDevices.Insert(0, null);

            // Gets all the video devices and sets the screen source.
            EncoderDevice Video = null;
            Collection<EncoderDevice> videoSources = EncoderDevices.FindDevices(EncoderDeviceType.Video);
            foreach (EncoderDevice dev in videoSources)
                if (dev.Name.Contains("Screen Capture Source"))
                    Video = dev;

            // Creats the source
            Source = Job.AddDeviceSource(Video, null);
            
            // Activates sources and sets preset to job
            Job.ActivateSource(Source);
            Job.ApplyPreset(LivePresets.VC1256kDSL16x9);
        }

        public void Start(int x, int y, int width, int height)
        {
            // Set capture rectangle
            Source.ScreenCaptureSourceProperties = new ScreenCaptureSourceProperties()
            {
                Left = x,
                Top = y,
                Width = width,
                Height = height,
            };

            // Set output file name to custom name based on timestamp
            string fileName = string.Format("capture_{0}.wmv", DateTime.Now);

            // Remove invalid characters
            fileName = fileName.Replace('/', '-');
            fileName = fileName.Replace(':', '-');

            // Clear any previous outputs
            Job.PublishFormats.Clear();

            // Add publish format and start encode
            Job.PublishFormats.Add(new FileArchivePublishFormat() { OutputFileName = string.Format("{0}{1}", OutputDirectory, fileName) });
            Job.StartEncoding();
        }

        public void SetSource(int index)
        {
            // Sets audio source based on the index selected
            Job.DeviceSources[0].AudioDevice = index < 0 ? null : AudioDevices[index];
        }

        private void onPropChanged(string propName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }
    }
}
