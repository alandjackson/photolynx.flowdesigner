﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Expression.Encoder;
using System.IO;

namespace CommandEncoder
{
    class Program
    {
        static void Main(string[] args)
        {
            // Checks for number of arguments and advises of usages
            if (args.Length < 1)
            {
                Console.WriteLine("Usage: CommandEncoder -j[d] <JobItem> [Destination]");
                Console.WriteLine("Usage: CommandEncoder -m[p][d] <MediaItem> [Preset] [Destination]");
                return;
            }

            // converts string to lower case
            args[0] = args[0].ToLower();
            switch (args[0])
            {
                case "-j":
                    LoadJob(args[1], null);
                    break;
                
                case "-jd":
                    if (args.Length == 3 && Directory.Exists(args[2]))
                        LoadJob(args[1], args[2]);
                    else
                        Console.WriteLine("Invalid Number of Arguments for Command -jd");
                    break;

                case "-m":
                    LoadMedia(args[1], null, null);
                    break;

                case "-mp":
                    if(args.Length == 3)
                        LoadMedia(args[1], args[2], null);
                    else
                        Console.WriteLine("Invalid Number of Arguments for Command -mp");
                    break;

                case "-md":
                    if (args.Length == 3 && Directory.Exists(args[2]))
                        LoadMedia(args[1], null, args[2]);
                    else
                        Console.WriteLine("Invalid Number of Arguments for Command -md");
                    break;

                case "-mpd":
                    if (args.Length == 4 && Directory.Exists(args[3]))
                        LoadMedia(args[1], args[2], args[3]);
                    else
                        Console.WriteLine("Invalid Number of Arguments for Command -mpd");
                    break;

                default:
                    Console.WriteLine("Invalid Entry");
                    break;
            }
        }

        /// <summary>
        /// Verifies the job and loads it
        /// </summary>
        /// <param name="JobName"></param>
        /// <param name="Destination"></param>
        private static void LoadJob(string JobName, string Destination)
        {
            Job job;
            try
            {
                job = Job.LoadJob(JobName);
            }
            catch (InvalidJobException)
            {
                Console.WriteLine("Error: Invalid Job or Path");
                return;
            }

            Encode(job, null, Destination);
        }

        /// <summary>
        /// Verfies the media file, creates the job and adds the media file to the job
        /// </summary>
        /// <param name="MediaFile"></param>
        /// <param name="PresetName"></param>
        /// <param name="Destination"></param>
        private static void LoadMedia(string MediaFile, string PresetName, string Destination)
        {
            MediaItem mItem;
            try
            {
                mItem = new MediaItem(MediaFile);
            }
            catch (InvalidMediaFileException)
            {
                Console.WriteLine("Error: Invalid Media File or Path");
                return;
            }

            Job job = new Job();
            job.MediaItems.Add(mItem);

            Encode(job, PresetName, Destination);
        }

        /// <summary>
        /// Encodes the job using any passed presets and custom destinations
        /// </summary>
        /// <param name="job"></param>
        /// <param name="PresetName"></param>
        /// <param name="Destination"></param>
        private static void Encode(Job job, string PresetName, string Destination)
        {
            // Sets the destination if defined and uses a default if not
            if (Destination != null)
                job.OutputDirectory = Destination;
            else
                job.OutputDirectory = @"c:\output";

            // Set up the progress callback function
            job.EncodeProgress += new EventHandler<EncodeProgressEventArgs>(OnProgress);

            //Sets preset if it was set and encodes job
            try
            {
                if (PresetName != null)
                    job.ApplyPreset(Preset.FindPreset(PresetName));

                job.Encode();
            }
            catch (EncodeErrorException exp)
            {
                Console.WriteLine("Encoding Error: " + exp.ToString());
                return;
            }
            catch (PresetNotFoundException exp)
            {
                Console.WriteLine("Invalid Preset: " + exp.ToString());
                return;
            }
        }

        private static void OnProgress(object sender, EncodeProgressEventArgs e)
        {
            Console.Write("\b\b\b\b\b\b\b\b{0:F1}%", e.Progress);
        }
    }
}
