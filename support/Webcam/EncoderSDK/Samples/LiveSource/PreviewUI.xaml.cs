﻿using System.Runtime.InteropServices;
using System.Windows;
using Microsoft.Expression.Encoder.Live;

namespace LiveSourceSample
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class PreviewUI : Window
    {
        LiveSource source;

        public PreviewUI()
        {
            InitializeComponent();
            source = new LiveSource();

            if (source.error)
            {
                // Gives error message as no audio and/or video devices found
                MessageBoxResult result = MessageBox.Show("No Video/Audio capture devices detected.", "Live Source Sample", MessageBoxButton.OK, MessageBoxImage.Stop);
                this.Close();
                return;
            }

            // sets preview window to winform panel hosted by xaml window
            source.deviceSource.PreviewWindow = new PreviewWindow(new HandleRef(panel1, panel1.Handle));
        }

        /// <summary>
        /// Called when the close button is pressed. If we currently have a file being encoded,
        /// then we prompt the user before encoding.
        /// </summary>
        /// <param name="sender">The sender of the event.</param>
        /// <param name="e">The event arguments.</param>
        private void OnClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if ((bool)EncodeButton.IsChecked)
            {
                // We're still encoding, so warn the user before leaving
                MessageBoxResult result = MessageBox.Show("The file is still encoding. Are you sure you want to exit?", "WPF Encode", MessageBoxButton.YesNo);
                if (result == MessageBoxResult.Yes)
                {
                    // If the user still wants to exit then cancel the encode
                    // and wait for the encode thread to finish.
                    source.Stop();
                }
                else
                {
                    // The user didn't want to exit so cancel the close.
                    e.Cancel = true;
                    return;
                }
            }
            source.Dispose();
        }

        private void EncodeButton_Checked(object sender, RoutedEventArgs e)
        {
            // Starts encoding
            source.Start();
        }

        private void EncodeButton_Unchecked(object sender, RoutedEventArgs e)
        {
            // Stops encoding
            source.Stop();
        }
    }
}
