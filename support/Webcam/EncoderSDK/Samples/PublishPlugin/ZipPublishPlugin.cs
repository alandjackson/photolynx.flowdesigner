﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Expression.Encoder.Plugins.Publishing;
using System.Xml;
using ZipPublishExample;

namespace MakeZip
{
    /// <summary>
    /// Main plugin class
    /// Sets up the UI, invokes the zipping operation and persists the plugin settings
    /// </summary>
    [EncoderPlugin("Zip Plugin from scratch", "Enables output to be zipped")]
    public class ZipPublishPlugin : PublishPlugin
    {
        /// <summary>
        /// Data binding object for the target directory
        /// </summary>
        private PublishData m_data;

        /// <summary>
        /// Was the operation cancelled?
        /// </summary>
        private bool m_cancelled;

        /// <summary>
        /// Basic constructor
        /// </summary>
        public ZipPublishPlugin()
        {
            m_data = new PublishData();
        }

        /// <summary>
        /// Gets a value indicating whether the publish has been canceled
        /// </summary>
        internal bool IsCancelled
        {
            get
            {
                return m_cancelled;
            }
        }

        /// <summary>
        /// Creates the UI
        /// </summary>
        /// <returns>Returns the UI object</returns>
        public override object CreateStandardSettingsEditor()
        {
            return new SimpleUI(m_data);
        }

        /// <summary>
        /// Sets the advanced settings
        /// </summary>
        /// <returns>Returns the advanced settings object</returns>
        public override object CreateAdvancedSettingsEditor()
        {
            return null;
        }

        /// <summary>
        /// Publish plugin call: zips all the files
        /// </summary>
        /// <param name="rootPath">Path of the files to be zipped</param>
        /// <param name="filesToPublish">Filename list of the files to be zipped</param>
        public override void PerformPublish(string rootPath, string[] filesToPublish)
        {
            m_cancelled = false;

            string path = System.IO.Path.Combine(m_data.TargetDirectory, System.IO.Path.GetFileName(rootPath) + ".zip");

            CreateZip.CreateZipFile(this, filesToPublish, path);
        }

        /// <summary>
        /// Loads the plugin settings
        /// </summary>
        /// <param name="reader">Xml reader object to load the settings from</param>
        public override void LoadJobSettings(XmlReader reader)
        {
            // Must nest our settings within a root element
            reader.ReadStartElement("ZipSample");
            m_data.TargetDirectory = reader.ReadElementString("TargetDirectory");
            reader.ReadEndElement();
        }

        /// <summary>
        /// Persists the plugin settings
        /// </summary>
        /// <param name="writer">Xml writer object to save the settings to</param>
        public override void SaveJobSettings(XmlWriter writer)
        {
            writer.WriteStartElement("ZipSample");
            writer.WriteElementString("TargetDirectory", m_data.TargetDirectory);
            writer.WriteEndElement();
        }

        /// <summary>
        /// Progress callback method for the Zip Code
        /// </summary>
        /// <param name="description">Description label</param>
        /// <param name="progress">Progress (in percent)</param>
        internal void CallOnProgress(string description, double progress)
        {
            OnProgress(description, progress);
        }

        /// <summary>
        /// Publish plugin call: cancels the publish
        /// </summary>
        protected override void CancelPublish()
        {
            m_cancelled = true;
        }
    }
}
