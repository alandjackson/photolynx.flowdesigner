﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Packaging;
using System.Text;
using System.Reflection;
using Microsoft.Expression.Encoder;
using System.Globalization;
using MakeZip;

namespace ZipPublishExample
{
    /// <summary>
    /// Class that creates a zip file from a set of files
    /// </summary>
    internal class CreateZip
    {
        /// <summary>
        /// Cached publish plug-in object
        /// </summary>
        private static ZipPublishPlugin m_plugin;

        /// <summary>
        /// Create a zip file from a set of files
        /// </summary>
        /// <param name="publish">The publish object which contains the progress and cancel</param>
        /// <param name="files">The list of files to add to the zip</param>
        /// <param name="zipFilename">The filename of the zip file to create</param>
        internal static void CreateZipFile(ZipPublishPlugin publish, string[] files, string zipFilename)
        {
            m_plugin = publish;
            List<string> fullFileNames = new List<string>();
            List<string> escapedFileNames = new List<string>();
            CreateFileNameList(files, fullFileNames, escapedFileNames);

            int fileCount = fullFileNames.Count;
            using (Package p = ZipPackage.Open(zipFilename, FileMode.Create))
            {
                for (int i = 0; i < fullFileNames.Count; i++)
                {
                    string uri = escapedFileNames[i];
                    PackagePart part = p.CreatePart(new Uri("/" + uri, UriKind.Relative), "", CompressionOption.Maximum);

                    // this is where you'd write your data
                    using (Stream outputStream = part.GetStream(FileMode.OpenOrCreate, FileAccess.Write))
                    {
                        using (FileStream inputStream = File.OpenRead(fullFileNames[i]))
                        {
                            EscapeFileNamesAndCopyStream(fullFileNames, escapedFileNames, fullFileNames[i], outputStream);
                        }
                    }

                    if (publish != null && m_plugin.IsCancelled)
                    {
                        break;
                    }
                }
            }

            if (m_plugin.IsCancelled)
            {
                File.Delete(zipFilename);
            }
        }

        /// <summary>
        /// Copies fullFileName's content into outputStream
        /// </summary>
        /// <param name="fullFileNames">Filename list</param>
        /// <param name="escapedFileNames">List of fixup filenames</param>
        /// <param name="fullFileName">Filename of the data to be copied</param>
        /// <param name="outputStream">Stream to copy the file data into</param>
        private static void EscapeFileNamesAndCopyStream(
            List<string> fullFileNames,
            List<string> escapedFileNames,
            string fullFileName,
            Stream outputStream)
        {
            string extension = Path.GetExtension(fullFileName);
            if (String.Compare(extension, ".JS", true /*ignoreCase*/, CultureInfo.InvariantCulture) == 0 ||
                String.Compare(extension, ".XAML", true /*ignoreCase*/, CultureInfo.InvariantCulture) == 0)
            {
                ReadFixupTextFile(fullFileNames, escapedFileNames, fullFileName, outputStream);
            }
            else
            {
                ReadAndWriteFile(fullFileName, outputStream);
            }
        }

        /// <summary>
        /// Directly copies the file into the outputStream
        /// </summary>
        /// <param name="fullFileName">Filename to copy the data from</param>
        /// <param name="outputStream">Stream to copy the file data into</param>
        private static void ReadAndWriteFile(string fullFileName, Stream outputStream)
        {
            using (FileStream inputStream = File.OpenRead(fullFileName))
            {
                CopyStream(inputStream, outputStream, 1024 * 200);
            }
        }

        /// <summary>
        /// Replaces the filenames usage within "fullFileName"
        /// with their fixup versions in all the script files and copies
        /// the data in the stream
        /// </summary>
        /// <param name="fullFileNames">Filename list</param>
        /// <param name="escapedFileNames">List of fixup filenames</param>
        /// <param name="fullFileName">Filename of the data to be copied</param>
        /// <param name="outputStream">Stream to copy the file data into</param>
        private static void ReadFixupTextFile(
            List<string> fullFileNames,
            List<string> escapedFileNames,
            string fullFileName,
            Stream outputStream)
        {
            // Check if we need to replace any of the filenames before copying the stream
            string fileContents = File.ReadAllText(fullFileName);
            for (int fileIndex = 0; fileIndex < fullFileNames.Count; fileIndex++)
            {
                fileContents = fileContents.Replace(Path.GetFileName(fullFileNames[fileIndex]), escapedFileNames[fileIndex]);
            }

            ASCIIEncoding asciiEncoding = new ASCIIEncoding();
            outputStream.Write(asciiEncoding.GetBytes(fileContents), 0, fileContents.Length);
        }

        /// <summary>
        /// Copies the filename list as well as keeping a fixedup version of the filenames
        /// </summary>
        /// <param name="files">Original filename list</param>
        /// <param name="fileNames">Destination filename list</param>
        /// <param name="escapedFileNames">Fixedup filename list</param>
        private static void CreateFileNameList(string[] files, List<string> fileNames, List<string> escapedFileNames)
        {
            string fileName;
            foreach (string strFile in files)
            {
                // OPF doesn't like non-legal characters in the filenames
                fileNames.Add(strFile);
                fileName = Path.GetFileName(strFile).Replace(" ", "_");
                fileName = Path.GetFileName(fileName).Replace(";", "_");
                fileName = Path.GetFileName(fileName).Replace("@", "_");
                fileName = Path.GetFileName(fileName).Replace("&", "_");
                fileName = Path.GetFileName(fileName).Replace("=", "_");
                fileName = Path.GetFileName(fileName).Replace("+", "_");
                fileName = Path.GetFileName(fileName).Replace("$", "_");
                escapedFileNames.Add(fileName);
            }

            return;
        }

        /// <summary>
        /// Copies bufferSize bytes from the input to the output stream
        /// </summary>
        /// <param name="inputStream">Input stream</param>
        /// <param name="outputStream">Output stream</param>
        /// <param name="bufferSize">Number of bytes to copy</param>
        private static void CopyStream(Stream inputStream, Stream outputStream, int bufferSize)
        {
            byte[] buffer = new byte[bufferSize];
            long totalWritten = 0;
            long total = inputStream.Length;
            int n;
            while ((n = inputStream.Read(buffer, 0, buffer.Length)) > 0)
            {
                if (m_plugin.IsCancelled)
                {
                    return;
                }

                m_plugin.CallOnProgress("Zipping..", Convert.ToDouble((Convert.ToDecimal(totalWritten) / Convert.ToDecimal(total)) * 100));

                outputStream.Write(buffer, 0, n);
                totalWritten += n;
            }
        }
    }
}
