﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MakeZip
{
    /// <summary>
    /// Interaction logic for SimpleUI.xaml
    /// </summary>
    public partial class SimpleUI : UserControl
    {
        /// <summary>
        /// Object for data binding of the target directory
        /// </summary>
        private PublishData m_data;

        /// <summary>
        /// Constructor taking the data object
        /// </summary>
        /// <param name="data">Data object</param>
        public SimpleUI(PublishData data)
        {
            m_data = data;
            InitializeComponent();
            this.DataContext = m_data;
        }

        /// <summary>
        /// Reset button has been clicked
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">Event arguments</param>
        private void Reset_Click(object sender, RoutedEventArgs e)
        {
            m_data.ResetSettings();
        }

        /// <summary>
        /// Browse button has been clicked 
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">Event arguments</param>
        private void Browse_Click(object sender, RoutedEventArgs e)
        {
            Shell32.Shell picker = new Shell32.Shell();
            Shell32.Folder folder = picker.BrowseForFolder(0, "Pick Folder", 0, 0);

            if (folder != null)
            {
                Shell32.FolderItem fi = (folder as Shell32.Folder3).Self;
                m_data.TargetDirectory = fi.Path;
            }
        }
    }
}
