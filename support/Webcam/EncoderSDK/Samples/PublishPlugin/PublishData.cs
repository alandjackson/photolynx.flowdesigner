﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;

namespace MakeZip
{
    /// <summary>
    /// Data class for data binding of the target directory
    /// </summary>
    public class PublishData : INotifyPropertyChanged
    {
        /// <summary>
        /// Directory where the zip file will be saved 
        /// </summary>
        private string m_targetDirectory = "";

        /// <summary>
        /// Basic constructor
        /// </summary>
        public PublishData()
        {
            ResetSettings();
        }

        /// <summary>
        /// Event fired when property changes
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets or sets the directory where the zip file will be saved
        /// </summary>
        public string TargetDirectory
        {
            get
            {
                return m_targetDirectory;
            }

            set
            {
                m_targetDirectory = value;
                OnPropChanged("TargetDirectory");
            }
        }

        /// <summary>
        /// Reset the setting to defaults
        /// </summary>
        public void ResetSettings()
        {
            TargetDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
        }

        /// <summary>
        /// Called when the property changed
        /// </summary>
        /// <param name="propName">Name of the property</param>
        internal void OnPropChanged(string propName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }
        }
    }
}
