namespace EncoderCmdlet
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Management.Automation;
    using System.Reflection;
    using Microsoft.Expression.Encoder;

    /// <summary>
    /// Cmdlet to encode one or more video files
    /// </summary>
    [Cmdlet("Convert", "Media", SupportsShouldProcess = true)]
    public class ConvertMedia : PSCmdlet
    {
        #region member variables
        /// <summary>
        /// member variable holding list of files to batch encode
        /// </summary>
        private List<string> inputBatch = new List<string>();
        #endregion

        #region Parameters
        /// <summary>
        /// Parameter that extracts FullName from the input stream which is the path property on FileInfo
        /// </summary>
        [Alias("FullName")]
        [Parameter(ParameterSetName = "Objects", Mandatory = false, ValueFromPipelineByPropertyName = true)]
        [ValidateNotNullOrEmpty]
        public string Input
        {
            get;
            set;
        }

        /// <summary>
        /// Parameter representing the output directory where the encoded files will go
        /// </summary>
        [Parameter(ParameterSetName = "Objects", Mandatory = false, ValueFromPipelineByPropertyName = true, HelpMessage = "Output diretory for the job")]
        [ValidateNotNullOrEmpty]
        public string Output
        {
            get;
            set;
        }

        /// <summary>
        /// Parameter setting the Title metadata tag
        /// </summary>
        [Parameter(ParameterSetName = "Objects", Mandatory = false, ValueFromPipelineByPropertyName = true, HelpMessage = "Set Title metadata")]
        [ValidateNotNullOrEmpty]
        public string Title
        {
            get;
            set;
        }

        /// <summary>
        /// Parameter setting the Description metadata tag
        /// </summary>
        [Parameter(ParameterSetName = "Objects", Mandatory = false, ValueFromPipelineByPropertyName = true, HelpMessage = "Set Description metadata")]
        [ValidateNotNullOrEmpty]
        public string Description
        {
            get;
            set;
        }

        /// <summary>
        /// Parameter representing the full path to an Expression Encoder job file to be loaded
        /// </summary>
        [Parameter(ParameterSetName = "JobFile", Mandatory = false, ValueFromPipelineByPropertyName = true, HelpMessage = "Path to an Expression Encoder job file")]
        [ValidateNotNullOrEmpty]
        public string JobFile
        {
            get;
            set;
        }

        /// <summary>
        /// Parameter representing the full path or the name of a preset to be applied on the job
        /// </summary>
        [Parameter(ParameterSetName = "Objects", Mandatory = false, ValueFromPipelineByPropertyName = true, HelpMessage = "Path to an Expression Encoder preset file or preset name")]
        [ValidateNotNullOrEmpty]
        public string Preset
        {
            get;
            set;
        }
        #endregion


        #region job configuration
        /// <summary>
        /// Configure metadata on job
        /// </summary>
        /// <param name="job">job instance</param>
        void ConfigureMetadata(Microsoft.Expression.Encoder.Job job)
        {
            foreach (MediaItem item in job.MediaItems)
            {
                if (!string.IsNullOrEmpty(this.Title))
                {
                    item.Metadata["Title"] = this.Title;
                }

                if (!string.IsNullOrEmpty(this.Description))
                {
                    item.Metadata["Description"] = this.Title;
                }
            }
        }

        /// <summary>
        /// Configure output directory on a job instance.
        /// </summary>
        /// <param name="job">job instance</param>
        void ConfigureOutputDirectory(Microsoft.Expression.Encoder.Job job)
        {
            string outDir = string.Empty;

            if (!String.IsNullOrEmpty(this.Output))
            {
                outDir = this.Output;
            }
            else
            {
                outDir = CurrentProviderLocation(string.Empty).Path;
            }

            job.CreateSubfolder = false;
            job.OutputDirectory = outDir;
        }

        /// <summary>
        /// Configure preset on job.
        /// </summary>
        /// <param name="job">job instance</param>
        void ConfigurePreset(Microsoft.Expression.Encoder.Job job)
        {

            // Apply the preset if one was provided
            if (this.Preset != null)
            {
                if (File.Exists(this.Preset))
                {
                    job.ApplyPreset(this.Preset);
                }
                else
                {
                    List<Preset> presetList = new List<Preset>(Microsoft.Expression.Encoder.Preset.SystemPresets);
                    for (int i = 0; i < presetList.Count; ++i)
                    {
                        if (presetList[i].Name.ToUpperInvariant() == this.Preset.ToUpperInvariant())
                        {
                            job.ApplyPreset(presetList[i]);
                            break;
                        }
                    }
                }
            }
        }
        #endregion

        /// <summary>
        /// Override BeginProcessing to initialize session variables
        /// </summary>
        protected override void BeginProcessing()
        {
            base.BeginProcessing();
        }

        /// <summary>
        /// Override EndProcessing.  Process any items that have been batched
        /// </summary>
        protected override void EndProcessing()
        {
            base.EndProcessing();
            base.WriteProgress(new ProgressRecord(0,"Complete","Complete"){RecordType=ProgressRecordType.Completed});
        }

        /// <summary>
        /// Main entrypoint for cmdlet.
        /// </summary>
        protected override void ProcessRecord()
        {
            if (ParameterSetName == "JobFile")
            {
                WriteVerbose(string.Format(CultureInfo.CurrentCulture, "Beginning job file {0}", this.JobFile));
                this.EncodeJob(this.JobFile);
            }
            else if (ParameterSetName == "Objects" && (this.Input != null && this.Input.Length > 0))
            {
                    this.EncodeItem(this.Input);
            }
            else
            {
                WriteWarning("No inputs");
            }
        }

        /// <summary>
        /// Encode a job based on a job file
        /// </summary>
        /// <param name="jobPath">path to job file</param>
        void EncodeJob(string jobPath)
        {
            using (Microsoft.Expression.Encoder.Job job = Microsoft.Expression.Encoder.Job.LoadJob(jobPath))
            {
                job.EncodeProgress += new EventHandler<Microsoft.Expression.Encoder.EncodeProgressEventArgs>(this.EncodeProgress);

                job.Encode();
            }
        }

        /// <summary>
        /// Encode item 
        /// </summary>
        /// <param name="inputPath">array of paths to input files</param>
        void EncodeItem(string inputPath)
        {
            Microsoft.Expression.Encoder.Job job = null;
           
            try
            {
                WriteVerbose(string.Format(CultureInfo.CurrentCulture, "Beginning {0}", inputPath));

                job = new Microsoft.Expression.Encoder.Job();

                this.ConfigureOutputDirectory(job);

                Microsoft.Expression.Encoder.MediaItem item = new Microsoft.Expression.Encoder.MediaItem(inputPath);

                // If the input and output directory are the same, append "_1" to the output file
                if (String.Compare(Path.GetFullPath(Path.GetDirectoryName(inputPath)).TrimEnd('\\'),
                    Path.GetFullPath(job.ActualOutputDirectory).TrimEnd('\\'), StringComparison.InvariantCultureIgnoreCase) == 0)
                {
                    item.OutputFileName = string.Format(CultureInfo.CurrentCulture, "{{Original file name}}_1.{{Default extension}}");
                }

                job.MediaItems.Add(item);

                this.ConfigureMetadata(job);

                this.ConfigurePreset(job);

                job.EncodeProgress += new EventHandler<Microsoft.Expression.Encoder.EncodeProgressEventArgs>(this.EncodeProgress);

                job.Encode();

                foreach (MediaItem mediaItem in job.MediaItems)
                {
                    FileInfo fi = new FileInfo(mediaItem.ActualOutputFileFullPath);
                    WriteObject(fi);
                }
            }
            catch (Exception e) // because we wan't to continue to the next item
            {
                this.WriteError(new ErrorRecord(e, string.Empty, ErrorCategory.NotSpecified, null));
            }
            finally
            {
                job.Dispose();
            }
        }

        #region event handlers
        /// <summary>
        /// Event handler for job progress
        /// </summary>
        /// <param name="sender">object instance that raised event</param>
        /// <param name="e">event arguments</param>
        void EncodeProgress(object sender, Microsoft.Expression.Encoder.EncodeProgressEventArgs e)
        {
            if (this.Stopping)
            {
                e.Cancel = true;
            }
            else
            {
                string progress = string.Format(
                    CultureInfo.CurrentCulture,
                    "Encoding {0} {1} % Pass {2} of {3}",
                    e.CurrentItem.ActualOutputFileName,
                    Convert.ToInt32(e.Progress).ToString(CultureInfo.CurrentUICulture),
                    e.CurrentPass,
                    e.TotalPasses);

                ProgressRecord record = new ProgressRecord(0, "Encoding", progress);
                record.PercentComplete = Convert.ToInt32(e.Progress);
                WriteProgress(record);
            }
        }
        #endregion
    }
}
