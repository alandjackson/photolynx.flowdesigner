namespace EncoderCmdlet
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Management.Automation;
    using System.Text;

    /// <summary>
    /// Snapin containing the Expression Encoder Cmdlets
    /// </summary>
    [RunInstaller(true)]
    public class EncoderCmdletSnapIn : PSSnapIn
    {
        /// <summary>
        /// Name of the SnapIn
        /// </summary>
        public override string Name
        {
            get { return "Expression Encoder Powershell module"; }
        }

        /// <summary>
        /// Vendor for the snapin
        /// </summary>
        public override string Vendor
        {
            get { return "Microsoft"; }
        }

        /// <summary>
        /// Vendor resource.
        /// </summary>
        public override string VendorResource
        {
            get { return "EncoderCmdlet, Microsoft"; }
        }

        /// <summary>
        /// Description of snapin
        /// </summary>
        public override string Description
        {
            get { return "Registers the CmdLets and Providers in this assembly"; }
        }

        /// <summary>
        /// Description resource
        /// </summary>
        public override string DescriptionResource
        {
            get { return "EncoderCmdlet,Registers the CmdLets and Providers in this assembly"; }
        }
    }
}
