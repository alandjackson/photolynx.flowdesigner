#Note: Access to .NET40 within PowerShell is required. More information is available here: # https://connect.microsoft.com/PowerShell/feedback/details/525435/net-4-0-assemblies-and-powershell-v2?wa=wsignin1.0

#Import the expressionencoder module
import-module expressionencoder

#simple encode single file using default preset
sl $home\desktop
Convert-Media -Input 'C:\users\Public\Videos\Sample Videos\Bear.wmv'

#encode single file using a file preset
Convert-Media -Input 'C:\users\Public\Videos\Sample Videos\Bear.wmv' -PresetFile 'C:\test\Preset.xml'

#encode single file using a system preset
Convert-Media -Input 'C:\users\Public\Videos\Sample Videos\Bear.wmv' -Preset 'VC-1 Zune HD'

#encode single file with metadata specifying output directory
Convert-Media -Input 'C:\users\Public\Videos\Sample Videos\Bear.wmv' -Title "Cool" -Description "Long video" -Output $home\desktop

#encode a jobfile saved by the Expression Encoder GUI
Convert-Media -jobfile "C:\users\Public\joboverlay.xej"

