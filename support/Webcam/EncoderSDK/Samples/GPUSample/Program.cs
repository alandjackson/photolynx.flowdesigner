﻿using System;
using System.IO;

namespace GPUSample
{
    class Program
    {
        private static bool m_Ready;
        private static GPUEncoder encoder = new GPUEncoder();

        static void Main(string[] args)
        {
            // Check input and give usage if asking for help or no arguments
            if (args.Length == 0 || args[0].ToLowerInvariant().Contains("?") || args[0].ToLowerInvariant().Contains("help"))
            {
                Usage();
                return;
            }
            Console.Read();

            // Parse Input
            ParseInput(args);

            // Ready flag is tripped. Begin Encoding
            if (m_Ready)
            {
                encoder.Encode();
            }
            else
            {
                Console.WriteLine("No media items added. Nothing to encode.");
            }
        }

        private static void ParseInput(string[] args)
        {
            for (int i = 0; i < args.Length; ++i)
            {
                switch (args[i].ToLowerInvariant())
                {
                    // Switch on live Mode
                    case "/live":
                        encoder.IsLive = true;
                        break;
                    // Add files from directory 
                    case "/folder":
                        if (i < args.Length - 1)
                        {
                            ++i;
                            string s = args[i];
                            if (Directory.Exists(s))
                            {
                                string[] files = Directory.GetFiles(s);
                                foreach (string file in files)
                                {
                                    encoder.Files.Add(file);
                                }
                                m_Ready = true;
                            }
                            else
                                Console.WriteLine("Directory does not exist. {0} \nSkipping.", s);
                        }
                        else
                            Console.WriteLine("Not enough arguments please check usage");
                        break;
                    // Add file for encoding
                    case "/file":
                        if (i < args.Length - 1)
                        {
                            ++i;
                            string s = args[i];
                            if (File.Exists(s))
                            {
                                encoder.Files.Add(s);
                                m_Ready = true;
                            }
                            else
                                Console.WriteLine("File does not exist. {0} \nSkipping.", s);
                        }
                        else
                            Console.WriteLine("Not enough arguments please check usage");
                        break;
                    // Add duration for encoding live source
                    case "/time": 
                        if (i < args.Length - 1)
                        {
                            ++i;
                            int time;
                            if (Int32.TryParse(args[i], out time))
                                encoder.AddTime(time);
                            else
                                Console.WriteLine("Invalid time parameter. Must be whole number in seconds. {0} \nSkipping.", args[i]);
                        }
                        else
                            Console.WriteLine("Not enough arguments please check usage");
                        break;
                    // Enable Cuda GPU and set the number of GPU Streams
                    case "/cuda":
                        encoder.UseCuda = true;
                        if (i < args.Length - 1)
                        {
                            ++i;
                            if (!Int32.TryParse(args[i], out encoder.CudaStreams))
                            {
                                Console.WriteLine("Invalid GPU stream parameter. {0} \nUsing default.", args[i]);
                            }
                            else
                            {
                                if (encoder.CudaStreams < 0 || encoder.CudaStreams > 16)
                                {
                                    Console.WriteLine("Invalid GPU stream parameter. {0} \nUsing default.", args[i]);
                                    encoder.CudaStreams = 0;
                                }
                            }
                        }
                        break;
                    // Enable Intel GPU and set the number of GPU Streams
                    case "/intel":
                        encoder.UseIntel = true;
                        if (i < args.Length - 1)
                        {
                            ++i;
                            if (!Int32.TryParse(args[i], out encoder.IntelStreams))
                                Console.WriteLine("Invalid GPU stream parameter. {0} \nUsing default.", args[i]);
                        }
                        else
                        {
                            if (encoder.IntelStreams < 0 || encoder.IntelStreams > 16)
                            {
                                Console.WriteLine("Invalid GPU stream parameter. {0} \nUsing default.", args[i]);
                                encoder.IntelStreams = 0;
                            }
                        }
                        break;
                    // Add preset name or keywords for encode type
                    case "/preset":
                        if (i < args.Length - 1)
                        {
                            ++i;
                            encoder.PresetKey = args[i];
                        }
                        else
                            Console.WriteLine("Not enough arguments please check usage");
                        break;
                    // Set output name or directory
                    case "/output":
                        if (i < args.Length - 1)
                        {
                            ++i;
                            string s = args[i];
                            if (Directory.Exists(s))
                                encoder.OutDir = s;
                            else
                                encoder.OutName = s;
                        }
                        else
                            Console.WriteLine("Not enough arguments please check usage");
                        break;
                    default:
                        Console.WriteLine("Invalid parameter. {0} \nSkipping.", args[i]);
                        break;
                }
            }
        }

        private static void Usage()
        {
            Console.WriteLine("GPUSample was created to show how to enable GPU encoding.");
            Console.WriteLine("This sample can accept multiple media files and can encode them in realtime.");
            Console.WriteLine("\nArguments-------------------------------------------------------------------");
            Console.WriteLine("/live  Indicates that encoding is in real time.");
            Console.WriteLine("/time WholeNumber  The time to before switching to next live source.");
            Console.WriteLine("/folder String  The path to a directory containing media to encode.");
            Console.WriteLine("/file String  Sets the full path of a media item, multiple sources can be used.");
            Console.WriteLine("/intel WholeNumber  Sets Intel GPU encoding and the number of GPU streams.");
            Console.WriteLine("/cuda WholeNumber  Sets NVidia GPU encoding and the number of GPU streams.");
            Console.WriteLine("/preset String  Sets the preset for encoding with part of the name or keywords");
            Console.WriteLine("/output String  The file name or the output directory to encode to.");
            Console.WriteLine("\nDefaults--------------------------------------------------------------------");
            Console.WriteLine("Time is 10 seconds. Intel streams is 8. Cuda streams is 2. Output is in My Docs with original file name.");
            Console.WriteLine("\nExample---------------------------------------------------------------------");
            Console.WriteLine("GPUSample.exe /Live /Time 30 /file C:/MyVideo/video.wmv /file C:/MyVideo/video2.wmv /cuda 3 /preset \"VC-1 420p\" /output Encoded");
        }
    }
}
