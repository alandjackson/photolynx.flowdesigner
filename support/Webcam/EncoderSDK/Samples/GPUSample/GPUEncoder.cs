﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.Expression.Encoder;
using Live = Microsoft.Expression.Encoder.Live;

namespace GPUSample
{
    class GPUEncoder
    {
        public int CudaStreams;
        public int IntelStreams;
        public bool UseCuda;
        public bool UseIntel;
        public bool IsLive;
        public string OutDir;
        public string OutName;
        public string PresetKey;
        public List<string> Files;

        private bool m_Error;
        private Preset m_Preset;
        private TimeSpan m_TimeSpan;

        public GPUEncoder()
        {
            CudaStreams = 5;
            Files = new List<string>();
            m_TimeSpan = TimeSpan.FromSeconds(10);
            OutDir = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
        }

        private void EnableGPU()
        {
            // Check to see if there are any GPU encoding devices available
            if (Settings.EncoderSku == EncoderSku.Pro)
            {
                // Enable GPU encoding and set the number of streams to use.
                H264EncodeDevices.EnableGpuEncoding = true;
                H264EncodeDevices.MaxNumberIntelHDGraphicsStream = IntelStreams != 0 ? IntelStreams : H264EncodeDevices.MaxNumberIntelHDGraphicsStream;
                H264EncodeDevices.MaxNumberGPUStream = CudaStreams != 0 ? CudaStreams : H264EncodeDevices.MaxNumberGPUStream;
                foreach (H264EncodeDevice device in H264EncodeDevices.Devices)
                {
                    switch (device.DeviceType)
                    {
                        case H264DeviceType.Cuda:
                            device.Enabled = UseCuda;
                            break;
                        case H264DeviceType.IntelHDGraphics:
                            device.Enabled = UseIntel;
                            break;
                    }
                }   
            }
        }

        public bool AddPreset()
        {
            // Split the string into parts
            string[] parts = PresetKey.Split(' ');
            foreach (Preset p in Preset.SystemPresets)
            {
                // Verify preset mode matches live mode
                if (IsLive == p.IsLivePreset)
                {
                    bool match = true;
                    // Iterate through all string parts
                    foreach (string s in parts)
                    {
                        // String part not found so we do not have a match. Go to next preset.
                        if (!p.Name.Contains(s))
                        {
                            match = false;
                            break;
                        }
                    }
                    // Found a match and return.
                    if (match)
                    {
                        m_Preset = p;
                        return true;
                    }
                }
            }

            return false;
        }

        public void AddTime(int time)
        {
            m_TimeSpan = TimeSpan.FromSeconds(time);
        }

        public void Encode()
        {
            // Try to enable GPU encoding if it is set
            if (UseCuda || UseIntel)
                EnableGPU();

            if (!string.IsNullOrWhiteSpace(PresetKey))
            {
                if (!AddPreset())
                    Console.WriteLine("Could not locate preset containing {0}, will use defaults.", PresetKey);
            }

            // Start encoding based on live flag
            if (IsLive)
                LiveEncode();
            else
                OffLineEncode();
        }

        public void LiveEncode()
        {
            // Setup job for live encoding
            using (Live.LiveJob job = new Live.LiveJob())
            {
                // Create a list for all file sources
                List<Live.LiveFileSource> sources = new List<Live.LiveFileSource>();
                foreach (string file in Files)
                {
                    try
                    {
                        // Try to create file source based
                        sources.Add(job.AddFileSource(file));
                    }
                    catch (InvalidMediaFileException)
                    {
                        Console.WriteLine("Couldn't load media file: {0}", file);
                    }

                    // No sources to encode so give error and quit
                    if (sources.Count <= 0)
                    {
                        Console.WriteLine("No media files loaded. Cannot continue");
                        return;
                    }

                    // Activate first source and add status event handler
                    job.ActivateSource(sources[0]);
                    job.Status += new EventHandler<Live.EncodeStatusEventArgs>(Status);

                    // Set preset and output extension
                    string extension = ".wmv";
                    if (m_Preset != null)
                    {
                        job.ApplyPreset(m_Preset);
                        if (m_Preset.IsSmoothStreaming)
                            extension = ".ismv";
                    }

                    // Set output name and start encoding
                    if (string.IsNullOrWhiteSpace(OutName))
                    {
                        DateTime now = DateTime.Now;
                        OutName = string.Format("Live stream {0}-{1}-{2} {3}", now.Month, now.Day, now.Year, now.Hour, now.Minute);
                    }
                    job.PublishFormats.Add(new Live.FileArchivePublishFormat(Path.Combine(OutDir, OutName + extension)));
                    job.StartEncoding();

                    // Loop that changes source based on time and exits if error.
                    DateTime endTime = DateTime.Now + TimeSpan.FromSeconds(m_TimeSpan.TotalSeconds * sources.Count);
                    bool done = false;
                    DateTime nextItem = DateTime.Now + m_TimeSpan;
                    int mediaIndex = 0;
                    while (!m_Error && !done)
                    {
                        // Sleeps then checks timing status
                        System.Threading.Thread.Sleep(TimeSpan.FromSeconds(1));

                        // Check to see that we don't need to switch to next source
                        if (DateTime.Now >= nextItem)
                        {
                            ++mediaIndex;
                            if (mediaIndex < sources.Count)
                            {
                                job.ActivateSource(sources[mediaIndex]);
                            }
                            else
                            {
                                done = true;
                            }
                            nextItem = DateTime.Now + m_TimeSpan;
                            Console.Write("\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b");
                            TimeSpan progress = endTime - DateTime.Now;
                            Console.Write("{0:F2} seconds left.", progress.TotalSeconds);
                        }
                    }
                    job.StopEncoding();
                }
            }
        }

        public void OffLineEncode()
        {
            using (Job job = new Job())
            {
                foreach (string file in Files)
                {
                    try
                    {
                        // Try to set each file as a media source
                        job.MediaItems.Add(new MediaItem(file));
                    }
                    catch (InvalidMediaFileException)
                    {
                        Console.WriteLine("Couldn't load media file: {0}", file);
                    }
                }

                // No sources added. Give error and exit.
                if (job.MediaItems.Count <= 0)
                {
                    Console.WriteLine("No media files loaded. Cannot continue");
                    return;
                }

                // Apply preset if there
                if (m_Preset != null)
                    job.ApplyPreset(m_Preset);

                // Set output directory
                job.OutputDirectory = OutDir;

                // Set output file name
                for (int i = 0; i < job.MediaItems.Count; ++i)
                {
                    if (!string.IsNullOrWhiteSpace(OutName))
                    {
                        string tempName = string.Format("{0}_{1}.{2}", OutName, i, "{Default extension}");
                        job.MediaItems[i].OutputFileName = tempName;
                    }
                    else
                    {
                        job.MediaItems[i].OutputFileName = "{Original file name}.{Default extension}";
                    }
                }

                try
                {
                    job.Encode();
                }
                catch (EncodeErrorException ex)
                {
                    Console.WriteLine("Encoding Error: {0}", ex.Message);
                    Console.WriteLine("Cannot continue.");
                }
            }
        }

        private void Progress(object obj, EncodeProgressEventArgs e)
        {
            Console.Write("\b\b\b\b\b\b\b\b\b\b");
            Console.Write("{0:F2}%", e.Progress);
        }

        private void Status(object obj, Live.EncodeStatusEventArgs e)
        {
            if (e.Message != null)
            {
                // check for error and if given throw flag to stop encoding
                string lowerMsg = e.Message.ToLowerInvariant();
                if (lowerMsg.Contains("error"))
                {
                    Console.WriteLine("Encoding Error: {0}", e.Message);
                    Console.WriteLine("Cannot continue.");
                    m_Error = true;
                }
            }
        }
    }
}
