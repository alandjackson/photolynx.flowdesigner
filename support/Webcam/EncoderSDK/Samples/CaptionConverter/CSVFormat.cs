﻿using System;
using System.ComponentModel.Composition;
using System.Globalization;
using System.IO;
using System.Text;
using Microsoft.Expression.Encoder;

namespace CsvConverter
{
    [Export("CsvConverter", typeof(IDfxpConverterContract))]
    public class CsvFormat : IDfxpConverterContract
    {
        /// <summary>
        /// Expose a reference to ourself so that MEF can enumerate us.
        /// </summary>
        [Export]
        public IDfxpConverterContract CaptionExporter
        {
            get
            {
                return this;
            }
        }
        #region Stock DFXP formatted output
        public const string DfxpPreamble =
             @"<?xml version='1.0' encoding='UTF-8'?>
               <tt xmlns='http://www.w3.org/2006/10/ttaf1' xmlns:tt='http://www.w3.org/2006/10/ttaf1' xmlns:ttm='http://www.w3.org/2006/10/ttaf1#metadata' xmlns:tts='http://www.w3.org/2006/10/ttaf1#styling' xmlns:ttp='http://www.w3.org/ns/ttml#parameter' xml:lang='{0}'>
               <head>
                <metadata>
                 <ttm:title></ttm:title>
                 <ttm:desc></ttm:desc>
                 <ttm:copyright></ttm:copyright>
                </metadata>
                <styling>
                 <style xml:id='backgroundStyle'
                  tts:fontFamily='proportionalSansSerif'
                  tts:fontSize='1.5c'
                  tts:textAlign='center'
                  tts:origin='0% 90%'
                  tts:extent='100% 8%'
                  tts:backgroundColor='rgba(0,0,0,0)'
                  tts:displayAlign='center'/>
                  <style xml:id='speakerStyle'
                  style='backgroundStyle'
                  tts:color='white'
                  tts:textOutline='0px 1px'
                  tts:backgroundColor='transparent'/>
                </styling>
                <layout>
                 <region xml:id='speaker' style='speakerStyle' tts:zIndex='1'/>
                 <region xml:id='background' style='backgroundStyle' tts:zIndex='0'/>
                </layout>
                </head>
                 <body>
                  <div>";
        public const string DfxpEntry=
             @" <p region='speaker' begin='{0}' end='{1}'>{2}</p>";
        public const string DxfpEnd =
             @"   </div>
               </body>
             </tt>";
        #endregion
        /// <summary>
        /// extensions supported by this converter
        /// </summary>
        public string[] Extensions
        {
            get
            {
                return new string[] { ".csv" };
            }
        }
        /// <summary>
        /// CSV to DFXP conversion
        /// </summary>
        /// <param name="sourceFile">source CSV file</param>
        /// <param name="destinationDfxpFile">destination DFXP file to write</param>
        /// <returns>true if converted ok, false if we cannot convert (may try another converter further down the chain)</returns>        
        public bool Convert(string sourceFile, string destinationDfxpFile, CultureInfo culture)
        {
            using (TextReader csvReader = File.OpenText(sourceFile))
            using (TextWriter dfxpFileWriter = File.CreateText(destinationDfxpFile))
            {
                dfxpFileWriter.Write(String.Format(CultureInfo.InvariantCulture, CsvFormat.DfxpPreamble, culture));

                string line = "";
                while ((line = csvReader.ReadLine())!=null)
                {
                    string[] parts = line.Split(',');
                    dfxpFileWriter.Write(String.Format(CultureInfo.InvariantCulture, CsvFormat.DfxpEntry, parts[0] + "ms", parts[1] + "ms", parts[2]));
                }

                dfxpFileWriter.Write(CsvFormat.DxfpEnd);
            }

            return true; // converted successfully ...
        }      

    }
}
