﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Expression.Encoder;

namespace ThumbsFromMarkers
{
    class Program
    {
        static void Main(string[] args)
        {
            // Checks the input arguments and verifies them
            if (args.Length != 2)
            {
                Console.WriteLine("Usage: Markers <MediaItem> <Number of Markers>");
                return;
            }

            // Holds the number of markers specified
            uint numMakers;
            
            // creates a media item and tries to add the file specified
            MediaItem mItem;
            try
            {
                mItem = new MediaItem(args[0]);
                numMakers = Convert.ToUInt32(args[1]);
            }
            catch (InvalidMediaFileException)
            {
                Console.WriteLine("Error: Invalid Media File or Path");
                Console.WriteLine("Usage: Markers <MediaItem> <Number of Markers>");
                return;
            }
            catch (FormatException)
            {
                Console.WriteLine("Error: Invalid Number, Must Be An Integer");
                Console.WriteLine("Usage: Markers <MediaItem> <Number of Markers>");
                return;
            }

            // Gets the length of the media file in seconds
            double timeSpan = mItem.FileDuration.TotalSeconds;

            // Divides the time span by the number of markers
            // Note: Adds one as beginning of file is one marker
            timeSpan /= (numMakers+1);

            // Creates a marker and thumbnail for each time iteration
            // and adds them to the media item
            for (int i = 0; i < numMakers; ++i)
            {
                Marker tmp = new Marker();

                // Can generate a key frame at each marker if desired
                tmp.GenerateKeyFrame = false;

                // Grabs a thumbnail from each marker
                tmp.GenerateThumbnail = true;

                // Adds one to account for beginning of file
                tmp.Time = TimeSpan.FromSeconds(timeSpan*(i+1));
                mItem.Markers.Add(tmp);
            }

            // Creates a job for encoding
            using(Job job = new Job())
            {
                // Adds the media item and the ouput path
                job.MediaItems.Add(mItem);
                job.OutputDirectory = @"c:\output";
                job.EncodeProgress += new EventHandler<EncodeProgressEventArgs>(OnProgress);
                job.Encode();
            }
        }

        private static void OnProgress(object sender, EncodeProgressEventArgs e)
        {
            Console.Write("\b\b\b\b\b\b\b");
            Console.Write("{0:F2}%", e.Progress);
        }
    }
}
