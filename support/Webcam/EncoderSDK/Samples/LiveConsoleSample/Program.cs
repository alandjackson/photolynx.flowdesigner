﻿/// Live Console Sample
/// Copyright Microsoft Corporation

namespace LiveConsoleSample
{
    using System;
    using System.IO;
    using System.Threading;
    using Microsoft.Expression.Encoder;
    using Microsoft.Expression.Encoder.Live;

    public class Program
    {
        /// <summary>
        /// Holds the number of retries if encoding encounters an error
        /// </summary>
        private static int NumberOfRetries = 10;

        /// <summary>
        /// Holds the duration to encode
        /// </summary>
        private static double DurationInMinutes = 5;
                
        /// <summary>
        /// The LiveJob used for encoding.
        /// </summary>
        private static LiveJob CurrentJob;

        /// <summary>
        /// The current number of attempts made at encoding.
        /// </summary>
        private static int CurrentTry;

        /// <summary>
        /// A event that indicates whether the encode is completed, either from timeout or exceeding errors.
        /// </summary>
        private static ManualResetEvent Finished;

        /// <summary>
        /// Entry Point
        /// </summary>
        /// <param name="args">Array of strings passed in from the command line.</param>
        public static void Main(string[] args)
        {
            // We have no arguments and will show usage and quit
            if (args.Length < 1)
            {
                Console.WriteLine("This sample will encode a simple live job with no source switching for a set duration. The default time is 5 minutes.");
                Console.WriteLine("If there is an encoding error, this sample will attempt to re-encode. The default is 10 times");
                Console.WriteLine("Usage:LiveConsoleSample.exe </Retry> n </Duration> n [LiveJob File]");
                Console.WriteLine("Example: LiveConsoleSample.exe c:\\jobs\\livejob.xej");
                Console.WriteLine("Example: LiveConsoleSample.exe /retry 5 c:\\jobs\\livejob.xej");
                Console.WriteLine("Example: LiveConsoleSample.exe /duration 1.5 c:\\jobs\\livejob.xej");
                return;
            }

            // Parses the input and verifies we have a job.
            if (!ParseInput(args))
            {
                Console.WriteLine("Invalid or missing job file.");
                return;
            }

            EncodeJob(true);

            Finished = new ManualResetEvent(false);
            Finished.WaitOne(TimeSpan.FromMinutes(DurationInMinutes));
           
            CurrentJob.Dispose();
        }

        /// <summary>
        /// Parse the intput strings into the proper settings.
        /// </summary>
        /// <param name="args">Array of input strings.</param>
        /// <returns>If the parsing was successful.</returns>
        private static bool ParseInput(string[] args)
        {
            // flag that indicates having parsed in the minimal requirments
            bool succeeded = false;

            // iterates through the input and stores the valid data.
            for (int i = 0; i < args.Length; ++i)
            {
                string temp = args[i].ToLowerInvariant();
                switch (temp)
                {
                    case "/retry":
                        {
                            if (int.TryParse(args[i + 1], out NumberOfRetries))
                            {
                                ++i;
                            }
                            else
                            {
                                Console.WriteLine("Invalid argument used for the number of retries. Using {0}.", NumberOfRetries);
                            }
                        }
                        break;
                    case "/duration":
                        {
                            if (double.TryParse(args[i + 1], out DurationInMinutes))
                            {
                                ++i;
                            }
                            else
                            {
                                Console.WriteLine("Invalid argument used for the duration. Using {0}.", DurationInMinutes);
                            }
                        }
                        break;
                    default:
                        {
                            if (File.Exists(args[i]))
                            {
                                succeeded = OpenJob(args[i]);
                            }
                        }
                        break;
                }
            }

            return succeeded;
        }

        /// <summary>
        /// This function attempts to open the job file and verifies that it has all the settings necessary to encode.
        /// </summary>
        /// <param name="job">LiveJob that will be loaded.</param>
        /// <returns>If the job opens successfully.</returns>
        private static bool OpenJob(string filename)
        {
            // Initializes the LiveJob object.
            CurrentJob = new LiveJob();

            // Loads the job.
            try
            {
                CurrentJob.Load(filename);
            }
            catch (XmlErrorException ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }

            // Checks to see that a job has all the components needed to encode.
            bool isJobValid = true;
            if (CurrentJob.OutputFormat.AudioProfile == null && CurrentJob.OutputFormat.VideoProfile == null)
            {
                Console.WriteLine("Job has no profile to encode to. Fix job and try again.");
                isJobValid = false;
            }

            if (CurrentJob.DeviceSources.Count < 1 && CurrentJob.FileSources.Count < 1)
            {
                Console.WriteLine("Job has no sources. Fix job and try again.");
                isJobValid = false;
            }

            if (CurrentJob.PublishFormats.Count < 1)
            {
                Console.WriteLine("Job has no publish type. Fix job and try again.");
                isJobValid = false;
            }

            return isJobValid;
        }

        /// <summary>
        ///  Sets up callbacks and encode timer, then starts encode.
        /// </summary>
        /// <param name="firstTime">Indicates if this is the first encode attempt.</param>
        private static void EncodeJob(bool firstTime)
        {
            if (firstTime)
            {
                // Sets up call back to handle encoding errors and starts encode
                CurrentJob.Status += new EventHandler<EncodeStatusEventArgs>(Status);
            }
            
            try 
            {
                CurrentJob.StartEncoding();
            }
            catch (EncodeErrorException ex)
            {
                Console.WriteLine(ex.Message);
                if (Finished != null)
                    Finished.Set();
            }
        }

        /// <summary>
        /// Callback method for the encode status
        /// </summary>
        /// <param name="obj">Object passing in the event.</param>
        /// <param name="e">The Encode parameters being passed in.</param>
        private static void Status(object obj, EncodeStatusEventArgs e)
        {
            // We have an error. Stop encoding and check number of tries.
            // If under the set number then retry encode.
            if (e.Status == EncodeStatus.EncodingError)
            {
                CurrentJob.StopEncoding();
                if (CurrentTry < NumberOfRetries)
                {
                    ++CurrentTry;
                    EncodeJob(false);
                }
                else
                {
                    Finished.Set();
                }
            }
        }
    }
}
