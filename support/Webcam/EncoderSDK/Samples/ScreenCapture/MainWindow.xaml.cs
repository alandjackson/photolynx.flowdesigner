﻿using System.Drawing;
using System.Threading;
using System.Windows;
using System.Windows.Forms;
using Microsoft.Expression.Encoder.ScreenCapture;

namespace ScreenCapture
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Screen capture job
        /// </summary>
        ScreenCaptureJob job;

        public MainWindow()
        {
            job = new ScreenCaptureJob();
            InitializeComponent();
        }

        /// <summary>
        /// Checks if we are still capturing and if check if they want to cancel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnClose(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // checks if the job is running and if so prompts to continue
            if (job.Status == RecordStatus.Running)
            {
                MessageBoxResult result = System.Windows.MessageBox.Show("Capturing in Progress. Are You Sure You Want To Quit?", "Capturing", MessageBoxButton.YesNo);
                if (result == MessageBoxResult.No)
                {
                    e.Cancel = true;
                    return;
                }
            }
            job.Stop();
            job.Dispose();
            
        }

        /// <summary>
        /// Starts capturing job and thread
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RecButton_Checked(object sender, RoutedEventArgs e)
        {
            System.Drawing.Size monitorSize = SystemInformation.PrimaryMonitorSize;
            Rectangle capRect = new Rectangle(0, 0, monitorSize.Width, monitorSize.Height);

            job.CaptureRectangle = capRect;
            job.OutputPath = @"C:\output\ScreenCap";
            job.Start();
        }

        /// <summary>
        /// Stops capturing job and thread
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RecButton_UnChecked(object sender, RoutedEventArgs e)
        {
            job.Stop();
        }
    }
}
