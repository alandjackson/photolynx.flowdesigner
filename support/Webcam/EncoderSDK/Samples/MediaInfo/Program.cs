﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Microsoft.Expression.Encoder;

namespace MediaInfo
{
    /// <summary>
    /// MediaItem Sample class
    /// This console app sample demonstrates how to use the MediaItem class to
    /// extract media and metadata information from a media file given as an input.
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Main function
        /// </summary>
        /// <param name="args">Command-line arguments</param>
        public static void Main(string[] args)
        {
            if (args.Length != 1)
            {
                // There should be only one argument, which is the media filename
                Console.WriteLine("Usage: MediaInfo <MediaFile>");
                return;
            }

            try
            {
                MediaItem mediaItem = new MediaItem(args[0]);

                // Get the file type (audio/video)
                bool hasAudio = (mediaItem.OriginalFileType & FileType.Audio) == FileType.Audio;
                bool hasVideo = (mediaItem.OriginalFileType & FileType.Video) == FileType.Video;

                string info = "Type: ";
                if (hasAudio)
                {
                    info += hasVideo ? "Audio/Video\n" : "Audio Only\n";
                }
                else
                {
                    info += hasVideo ? "Video Only\n" : "Unknown\n";
                }

                // If any, gather video information
                if ((mediaItem.OriginalFileType & FileType.Video) == FileType.Video)
                {
                    info += "Video Information:\n";

                    // Frame size
                    info += string.Format("\tFrame Size: {0} x {1}\n", mediaItem.OriginalVideoSize.Width, mediaItem.OriginalVideoSize.Height);
                    
                    // Frame rate
                    double frameRate = mediaItem.OriginalFrameRate;
                    if (mediaItem.OriginalInterlaced)
                    {
                        // Doubling the frame rate in the interlaced case
                        frameRate *= 2.0;
                    }

                    info += string.Format("\tFrame Rate: {0:F2}{1}\n", frameRate, mediaItem.OriginalInterlaced ? "i" : "p");
                    
                    // Viewing aspect ratio
                    info += string.Format("\tAspect Ratio: {0}x{1}\n", mediaItem.OriginalAspectRatio.Width, mediaItem.OriginalAspectRatio.Height);

                    // Duration
                    info += string.Format("\tDuration: {0}\n", mediaItem.FileDuration.ToString());
                }

                // If any, gather marker information
                int markerIndex = 0;
                foreach (Marker markerData in mediaItem.Markers)
                {
                    markerIndex++;
                    info += string.Format("Marker \"{0}\" ({1}/{2}): {3}\n", markerData.Value, markerIndex, mediaItem.Markers.Count, markerData.Time.ToString());
                }

                // If any, gather script information
                int scriptIndex = 0;
                if (mediaItem.ScriptCommands != null)
                {
                    foreach (ScriptCommand scriptData in mediaItem.ScriptCommands)
                    {
                        scriptIndex++;
                        info += string.Format("Script ({1}/{2}) Type:\"{0}\" Command:\"{3}\" Time:{4}\n", scriptData.Type, scriptIndex, mediaItem.ScriptCommands.Count, scriptData.Command, scriptData.Time.ToString());
                    }
                }

                // If any, gather metadata information
                int metadataIndex = 0;
                foreach (KeyValuePair<string, string> pair in mediaItem.Metadata)
                {
                    metadataIndex++;
                    info += string.Format("Metadata \"{0}\" ({1}/{2}): \"" + pair.Value + "\"\n", pair.Key, metadataIndex, mediaItem.Metadata.Count);
                }

                // Write the information gathered to the console
                Console.Write(info);
            }
            catch (InvalidMediaFileException)
            {
                // This exception signifies that the file isn't a media file or that it's not supported by Expression Encoder
                Console.WriteLine("Error: Unsupported or invalid media file");
            }
        }
    }
}
