﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using Microsoft.Expression.Encoder;

namespace ThumbnailGenerator
{
    class Program
    {
        private static ImageFormat m_Format = ImageFormat.Bmp;
        private static string m_Ext = "bmp";

        private static int m_NumberThumbs = 10;
        private static double m_Offset = 0;
        private static string[] m_SourceFiles;

        static void Main(string[] args)
        {
            // No arguments passed. Writing usage and closing
            if (args.Length == 0)
            {
                Usage();
                return;
            }
            
            // Parses Input
            ParseInput(args);

            // Iterates through all files specified
            for (int c = 0; c < m_SourceFiles.Length; ++c)
            {
                // Creates AudioVideoFile to get thumbnails from
                string sourceFile = m_SourceFiles[c];
                AudioVideoFile avFile = null;
                try
                {
                    avFile = new AudioVideoFile(sourceFile);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    return;
                }

                // Verifies offset works with file duration
                double tempOffset = m_Offset;
                if (tempOffset >= avFile.Duration.TotalSeconds)
                {
                    Console.WriteLine("Offset {0} greater than duration. Not using offset.", m_Offset);
                    tempOffset = 0;
                }
                // Gets the time for each thumbnail.
                double thumbtime = (avFile.Duration.TotalSeconds - tempOffset) / (double)m_NumberThumbs;

                // Create ThumbnailGenerator object to get thumbs from AudioVideoFile
                Microsoft.Expression.Encoder.ThumbnailGenerator generator = avFile.CreateThumbnailGenerator(avFile.VideoStreams[0].VideoSize);

                // Sets directory for thumbnail generation
                string bitmapDir = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);

                Console.WriteLine("File {0} of {1}", c+1, m_SourceFiles.Length);

                // Gets the number of thumbnails desired
                for (int i = 0; i < m_NumberThumbs; ++i)
                {
                    // Writes progress
                    Console.Write("\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b");
                    Console.Write("{0} of {1}", i + 1, m_NumberThumbs);

                    // Gets image based on timing
                    Bitmap image = generator.CreateThumbnail(TimeSpan.FromSeconds((double)i * thumbtime + m_Offset));

                    // sets thumbnail name unique and saves
                    string bitmapName = string.Format("Thumb-{0}.{1}", i + 1, m_Ext);
                    image.Save(Path.Combine(bitmapDir, bitmapName), m_Format);

                    image.Dispose();
                }
                generator.Dispose();
            }
        }

        private static void ParseInput(string[] args)
        {
            for (int i = 0; i < args.Length; ++i)
            {
                switch (args[i].ToLowerInvariant())
                {
                    case "/count":
                        ++i;
                        if (!int.TryParse(args[i], out m_NumberThumbs))
                        {
                            Console.WriteLine("Invalid value {0}. Getting {1} thumbnails.", args[i], m_NumberThumbs);
                        }
                        break;
                    case "/offset":
                        ++i;
                        if (!double.TryParse(args[i], out m_Offset))
                        {
                            Console.WriteLine("Invalid value {0}. Not using offset.", args[i]);
                        }
                        break;
                    case "/format":
                        ++i;
                        m_Ext = args[i].ToLowerInvariant();
                        switch (m_Ext)
                        {
                            case "gif":
                                m_Format = ImageFormat.Gif;
                                break;
                            case "jpg":
                                m_Format = ImageFormat.Jpeg;
                                break;
                            case "png":
                                m_Format = ImageFormat.Png;
                                break;
                            case "tif":
                                m_Format = ImageFormat.Tiff;
                                break;
                            default:
                                m_Format = ImageFormat.Bmp;
                                m_Ext = "bmp";
                                break;
                        }
                        break;
                    default:
                        if(Directory.Exists(args[i]))
                        {
                            m_SourceFiles = Directory.GetFiles(args[i]);
                        }
                        else if (File.Exists(args[i]))
                        {
                            m_SourceFiles = new string[] { args[i] };
                        }
                        else
                        {
                            Console.WriteLine("Invalid file to generate thumbs from. Cannot continue.");
                            return;
                        }
                        break;
                }
            }
        }

        private static void Usage()
        {
            Console.WriteLine("Usage: </count> </offset> </format> [SourceFile][SourceDirectory]\r\nOffset is in seconds and cannot exceed file duration.");
            Console.WriteLine("Example: ThumbnailGenerator.exe c:\\video\\video.wmv");
            Console.WriteLine("Example: ThumbnailGenerator.exe c:\\video");
            Console.WriteLine("Example: ThumbnailGenerator.exe /count 5 c:\\video\\video.wmv");
            Console.WriteLine("Example: ThumbnailGenerator.exe /offset 1.5 c:\\video\\video.wmv");
            Console.WriteLine("Example: ThumbnailGenerator.exe /format jpg c:\\video\\video.wmv");
        }
    }
}
