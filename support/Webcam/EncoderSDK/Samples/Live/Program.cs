﻿using System;
using Microsoft.Expression.Encoder;
using Microsoft.Expression.Encoder.Live;

namespace Live
{
    class Program
    {
        /// <summary>
        /// This sample demonstrates boadcasting a simple file continuously looped.
        /// </summary>
        /// <param name="args">input parameters, the name of the file we are going to broadcast</param>
        static void Main(string[] args)
        {
            // We should have only 1 string in the argument array, and
            // it should be the name of the file we are going to encode
            if (args.Length != 1)
            {
                Console.WriteLine("Usage: live <file_name>");
                return;
            }

            string fileToEncode = args[0];

            // Create a new LiveJob to begin broadcasting this file. Make sure
            // to dispose the LiveJob when you are finished with it.
            using (LiveJob job = new LiveJob())
            {
                // Gets the status of the job and advises if there is an error
                job.Status += new EventHandler<EncodeStatusEventArgs>(StatusCheck);

                // Create a new file source from the file name we were passed in
                LiveFileSource fileSource;
                try
                {
                    fileSource = job.AddFileSource(fileToEncode);
                }
                catch (InvalidMediaFileException exp)
                {
                    Console.WriteLine(exp.ToString());
                    return;
                }

                // Set this source to Loop when finished
                fileSource.PlaybackMode = FileSourcePlaybackMode.Loop;

                // Adds an encoding preset to the job
                job.ApplyPreset(LivePresets.VC1512kDSL4x3);

                // Make this source the active one
                job.ActivateSource(fileSource);

                // Create a new windows media broadcast output format so we
                // can broadcast this encoding on the current machine.
                // We are going to use the default audio and video profiles
                // that are created on this output format.
                PullBroadcastPublishFormat outputFormat = new PullBroadcastPublishFormat();

                // Let's broadcast on the local machine on port 8080
                outputFormat.BroadcastPort = 8080;

                // Set the output format on the job
                job.PublishFormats.Add(outputFormat);

                // Start encoding
                Console.WriteLine("\nPress 'x' to stop encoding...");
                job.StartEncoding();

                // Let's listen for a keypress or error message to know when to stop encoding
                while (Console.ReadKey(true).Key != ConsoleKey.X) ;

                // Stop our encoding
                Console.WriteLine("Encoding stopped.");
                job.StopEncoding();
            }
        }

        private static void StatusCheck(object obj, EncodeStatusEventArgs e)
        {
            Console.Write("\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b");
            Console.Write("Status: {0}", e.Status);
        }
    }
}
