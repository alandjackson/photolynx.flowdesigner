﻿Imports System.Drawing
Imports System.Threading
Imports System.Windows
Imports System.Windows.Forms
Imports Microsoft.Expression.Encoder.ScreenCapture

Namespace ScreenCapture
    ''' <summary>
    ''' Interaction logic for MainWindow.xaml
    ''' </summary>
    Partial Public Class MainWindow
        Inherits Window
        ''' <summary>
        ''' Screen capture job
        ''' </summary>
        Private job As ScreenCaptureJob

        Public Sub New()
            job = New ScreenCaptureJob()
            InitializeComponent()
        End Sub

        ''' <summary>
        ''' Checks if we are still capturing and if check if they want to cancel
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        Private Sub OnClose(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
            ' checks if the job is running and if so prompts to continue
            If job.Status = RecordStatus.Running Then
                Dim result As MessageBoxResult = System.Windows.MessageBox.Show("Capturing in Progress. Are You Sure You Want To Quit?", "Capturing", MessageBoxButton.YesNo)
                If result = MessageBoxResult.No Then
                    e.Cancel = True
                    Exit Sub
                End If
            End If
            job.[Stop]()

            job.Dispose()
        End Sub

        ''' <summary>
        ''' Starts capturing job and thread
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        Private Sub RecButton_Checked(ByVal sender As Object, ByVal e As RoutedEventArgs)
            Dim monitorSize As System.Drawing.Size = SystemInformation.PrimaryMonitorSize
            Dim capRect As New Rectangle(0, 0, monitorSize.Width, monitorSize.Height)

            job.CaptureRectangle = capRect
            job.OutputPath = "C:\output\ScreenCap"
            job.Start()
        End Sub

        ''' <summary>
        ''' Stops capturing job and thread
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        Private Sub RecButton_UnChecked(ByVal sender As Object, ByVal e As RoutedEventArgs)
            job.[Stop]()
        End Sub
    End Class
End Namespace