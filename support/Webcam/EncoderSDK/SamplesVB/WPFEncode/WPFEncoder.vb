﻿Imports System.ComponentModel
Imports System.Threading
Imports System.Windows
Imports Microsoft.Expression.Encoder
Imports Microsoft.Win32
Imports System.Windows.Threading

Namespace WPFEncode
    Public Class WPFEncoder
        Implements INotifyPropertyChanged
        ''' <summary>
        ''' Public event if any property is changed
        ''' </summary>
        Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

        ''' <summary>
        ''' Indicates if the encode should be cancelled.
        ''' </summary>
        Private cancelEncode As Boolean

        ''' <summary>
        ''' Seperate Encode Thread.
        ''' </summary>
        Private thread As Thread

        ''' <summary>
        ''' Indicates if we are busy or not.
        ''' </summary>
        Private m_isIdle As Boolean
        Public Property IsIdle() As Boolean
            Get
                Return m_isIdle
            End Get
            Set(ByVal value As Boolean)
                m_isIdle = value
                onPropChanged("IsIdle")
            End Set
        End Property

        ''' <summary>
        ''' The filename of the file we wish to encode.
        ''' </summary>
        Private m_fileName As String
        Public Property FileName() As String
            Get
                Return m_fileName
            End Get
            Set(ByVal value As String)
                m_fileName = value
                onPropChanged("FileName")
            End Set
        End Property

        ''' <summary>
        ''' Gets the current status of the encode.
        ''' </summary>
        Private m_status As String
        Public Property Status() As String
            Get
                Return m_status
            End Get
            Set(ByVal value As String)
                m_status = value
                onPropChanged("Status")
            End Set
        End Property

        ''' <summary>
        ''' Gets the current encode progress
        ''' </summary>
        Private m_progress As Double
        Public Property Progress() As Double
            Get
                Return m_progress
            End Get
            Set(ByVal value As Double)
                m_progress = value
                onPropChanged("Progress")
            End Set
        End Property

        Friend Sub New()
            m_isIdle = True
        End Sub

        ''' <summary>
        ''' Starts encoding in seperate thread
        ''' </summary>
        Friend Sub Start()
            ' Sets Status of encode
            Status = "Analyzing"
            IsIdle = False

            ' Verifies file path is entered for encode.
            If String.IsNullOrWhiteSpace(m_fileName) Then
                Status = "No file entered."
                IsIdle = True
                Return
            End If

            ' Creates encode thread and starts it
            thread = New Thread(New ThreadStart(AddressOf EncodeThread))
            thread.Start()
        End Sub

        ''' <summary>
        ''' Stops the encode in progress and closes thread.
        ''' </summary>
        Friend Sub [Stop]()
            cancelEncode = True
            thread.Join()
        End Sub

        Private Sub EncodeThread()
            ' Create the media item and validates it.
            Dim mediaItem As MediaItem
            Try
                mediaItem = New MediaItem(FileName)
            Catch exp As InvalidMediaFileException
                Status = exp.Message
                IsIdle = True
                Return
            End Try

            ' Create the job, add the media item and encode.
            Using job As New Job()
                job.MediaItems.Add(mediaItem)
                job.OutputDirectory = "C:\output"

                AddHandler job.EncodeProgress, AddressOf OnProgress

                job.Encode()
            End Using
            Status = "Finished."
            IsIdle = True
        End Sub

        ''' <summary>
        ''' Called when encoding progress occurs.
        ''' </summary>
        ''' <param name="sender">The sender of the event.</param>
        ''' <param name="e">The event arguments.</param>
        Private Sub OnProgress(ByVal sender As Object, ByVal e As EncodeProgressEventArgs)
            ' If we should cancel then indcate that.
            If cancelEncode Then
                e.Cancel = True
                IsIdle = True
                Return
            End If

            Progress = e.Progress
            Status = String.Format("{0:F1}%", e.Progress)
        End Sub

        ''' <summary>
        ''' Updates status of variable on update.
        ''' </summary>
        ''' <param name="propName"></param>
        Private Sub onPropChanged(ByVal propName As String)
            RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(propName))
        End Sub
    End Class
End Namespace
