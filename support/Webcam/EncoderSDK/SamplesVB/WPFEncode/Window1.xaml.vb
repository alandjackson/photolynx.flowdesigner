﻿Imports System.Threading
Imports System.Windows
Imports Microsoft.Expression.Encoder
Imports Microsoft.Win32
Imports System.Windows.Threading

Namespace WPFEncode
    ''' <summary>
    ''' Interaction logic for Window1.xaml
    ''' </summary>
    Partial Public Class Window1
        Inherits Window
        Public encoder As WPFEncoder

        ''' <summary>
        ''' Initializes a new instance of the Window1 class
        ''' </summary>
        Public Sub New()
            encoder = New WPFEncoder()
            DataContext = encoder
            InitializeComponent()
        End Sub

        ''' <summary>
        ''' Called when the user clicks to the browse button to select a filename.
        ''' </summary>
        ''' <param name="sender">The sender of the event.</param>
        ''' <param name="e">The event arguments.</param>
        Private Sub OnBrowseClick(ByVal sender As Object, ByVal e As RoutedEventArgs)
            Dim dialog As New OpenFileDialog()
            Dim result As Nullable(Of Boolean) = dialog.ShowDialog()
            If result = True Then
                textBoxFileName.Text = dialog.FileName
            End If
        End Sub

        ''' <summary>
        ''' Called when the user clicks the encode button.
        ''' </summary>
        ''' <param name="sender">The sender of the event.</param>
        ''' <param name="e">The event arguments.</param>
        Private Sub OnEncodeClick(ByVal sender As Object, ByVal e As RoutedEventArgs)
            encoder.Start()
        End Sub

        ''' <summary>
        ''' Called when the close button is pressed. If we currently have a file being encoded,
        ''' then we prompt the user before encoding.
        ''' </summary>
        ''' <param name="sender">The sender of the event.</param>
        ''' <param name="e">The event arguments.</param>
        Private Overloads Sub OnMyClosing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
            If Not encoder.IsIdle Then
                ' We're still encoding, so warn the user before leaving
                Dim result As MessageBoxResult = MessageBox.Show("The file is still encoding. Are you sure you want to exit?", "WPF Encode", MessageBoxButton.YesNo)
                If result = MessageBoxResult.Yes Then
                    ' If the user still wants to exit then cancel the encode
                    ' and wait for the encode thread to finish.
                    encoder.[Stop]()
                Else
                    ' The user didn't want to exit so cancel the close.
                    e.Cancel = True
                End If
            End If
        End Sub
    End Class
End Namespace
