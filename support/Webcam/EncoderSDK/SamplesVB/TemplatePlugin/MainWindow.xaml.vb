﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Windows
Imports System.Windows.Controls
Imports System.Windows.Data
Imports System.Windows.Documents
Imports System.Windows.Input
Imports System.Windows.Media
Imports System.Windows.Media.Imaging
Imports System.Windows.Navigation
Imports System.Windows.Shapes

Namespace TemplatePlugin
    ''' <summary>
    ''' Interaction logic for UserInterface.xaml
    ''' </summary>
    Partial Public Class UserInterface
        Inherits UserControl
        ''' <summary>
        ''' Plugin host
        ''' </summary>
        Private m_host As Microsoft.Expression.Encoder.Plugins.IPluginHost

        ''' <summary>
        ''' Constructor taking the plugin host
        ''' </summary>
        ''' <param name="host">Plugin host</param>
        Public Sub New(ByVal host As Microsoft.Expression.Encoder.Plugins.IPluginHost)
            m_host = host
            InitializeComponent()
        End Sub

        ''' <summary>
        ''' AddCommand button has been clicked
        ''' </summary>
        ''' <param name="sender">Sender object</param>
        ''' <param name="args">Event arguments</param>
        Private Sub AddCommand(ByVal sender As Object, ByVal args As RoutedEventArgs)
            Using m_host.CreateUndoGroup("Add a custom script command")
                ' Checks if there are media items. If not it ignores button press.
                If m_host.CurrentJob.MediaItems.Count > 0 Then
                    m_host.CurrentSelection(0).ScriptCommands.Add(New Microsoft.Expression.Encoder.ScriptCommand(m_host.CurrentPlayHeadPosition, "Custom Script Command", commandName.Text))
                End If
            End Using
        End Sub
    End Class
End Namespace