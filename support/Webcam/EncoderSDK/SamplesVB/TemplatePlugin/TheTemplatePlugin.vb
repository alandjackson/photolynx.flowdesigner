﻿Imports System
Imports System.Collections.Generic
Imports System.Text

Namespace TemplatePlugin
    ''' <summary>
    ''' Main Plugin class
    ''' This plugin enables customization of template settings as well as providing a way to interact with 
    ''' the job via the IPluginHost interface. 
    ''' 
    ''' To Setup:
    ''' 1. The template to be customized via the Plugin needs an extra line in
    ''' its "Default.html" that looks like this:
    ''' <!-- <$@ Options Plugin="{Plugin namespace}.{Plugin class}, {Plugin DLL filename}"$> -->
    ''' For example, the extra line in the template provided in this example (ModifiedClassic) is
    ''' <!-- <$@ Options Plugin="TemplatePlugin.TheTemplatePlugin, templateplugin.dll"$> -->
    ''' 
    ''' 2. The UserControl tag in the XAML needs this extra line:
    ''' xmlns:ns="clr-namespace:Microsoft.Expression.Encoder.Plugins.Templates;assembly=Microsoft.Expression.Encoder"
    ''' 
    ''' To Run:
    ''' 1. Copy the modified template and the Plugin DLL in the Encoder 2 "Templates" directory
    ''' 2. Run the application and select the modified template
    ''' </summary>
    Public Class TheTemplatePlugin
        Inherits Microsoft.Expression.Encoder.Plugins.Templates.TemplatePlugin
        ''' <summary>
        ''' Returns the user control used in the template parameter control
        ''' </summary>
        ''' <returns>The UI to be used</returns>
        Public Overloads Overrides Function CreateUserInterface() As Object
            Dim ui As New UserInterface(PublishingHost)
            Return DirectCast(ui, Object)
        End Function
    End Class
End Namespace