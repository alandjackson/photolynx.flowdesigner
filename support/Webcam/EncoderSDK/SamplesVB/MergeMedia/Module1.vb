﻿Imports System
Imports System.Collections.Generic
Imports System.Text
Imports Microsoft.Expression.Encoder
Imports Microsoft.Expression.Encoder.Profiles


Namespace MergeMedia
    ' This sample uses the multi-source functionality to merge multiple files
    Class Program
        Shared Sub Main(ByVal args As String())
            If args.Length < 2 Then
                ' Use the command-line arguments to build the list of media files to merge
                Console.WriteLine("Usage: MergeMedia <MediaFile> <MediaFile> ...")
                Exit Sub
            End If

            ' Create a job and the media item for the video we wish
            ' to encode.
            Dim job As New Job()
            Dim mediaItem As New MediaItem(args(0))

            ' Setting the Profiles
            mediaItem.OutputFormat = New WindowsMediaOutputFormat()

            ' Use source video profile if available
            If mediaItem.SourceVideoProfile IsNot Nothing Then
                mediaItem.OutputFormat.VideoProfile = mediaItem.SourceVideoProfile
            Else

                mediaItem.OutputFormat.VideoProfile = New AdvancedVC1VideoProfile()
            End If

            ' Use source audio profile if available
            If mediaItem.SourceAudioProfile IsNot Nothing Then
                mediaItem.OutputFormat.AudioProfile = mediaItem.SourceAudioProfile
            Else
                mediaItem.OutputFormat.AudioProfile = New WmaAudioProfile()
            End If

            ' Add all the sources
            job.MediaItems.Add(mediaItem)
            For i As Integer = 1 To args.Length - 1
                mediaItem.Sources.Add(New Source(args(i)))
            Next

            ' Set up the progress callback function
            AddHandler job.EncodeProgress, AddressOf OnProgress

            ' Set the output directory and encode.
            job.OutputDirectory = "C:\output"

            ' Finally encode
            job.Encode()
        End Sub

        Private Shared Sub OnProgress(ByVal sender As Object, ByVal e As EncodeProgressEventArgs)
            Console.WriteLine(e.Progress)
        End Sub
    End Class
End Namespace