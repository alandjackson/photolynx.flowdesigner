﻿Imports System
Imports System.Collections
Imports System.Drawing
Imports System.IO
Imports System.Xml
Imports System.Xml.XPath
Imports System.Xml.Linq
Imports Microsoft.Expression.Encoder

Namespace DVDMenuSample
    Class Program
        ' Sets the number of clips we will extract
        Const NUM_Clips As Integer = 4

        ' Scale for the media overlay
        Const OVERLAY_SCALE As Integer = 4

        ' Sets path for the xaml title overlay for editing and adding to file
        Shared TITLE_FILE As String = Environment.CurrentDirectory.Replace("bin\Debug", "Overlay.xaml")

        Shared Sub Main(ByVal args As String())
            ' Squares the number of clips for the total number of segments to 
            ' split the video into
            Dim numSegments As Integer = NUM_Clips << 2

            ' Iverse of the number of film segments to reduce the number of divisions
            Dim invSegments As Double = 1.0R / numSegments

            ' Checks the arguments and advises the user of the usage
            If args.Length <> 1 Then
                'return;
                Console.WriteLine("Usage: DVDMenuSample <MediaItem>")
            End If

            Dim file__1 As String = "C:\output\simpsons.mov"

            ' Attempts to create the media file and gives an Error if it fails
            Dim mItem As MediaItem
            Try
                mItem = New MediaItem(file__1)
            Catch generatedExceptionName As InvalidMediaFileException
                Console.WriteLine("Error: Invalid File or Path")
                Exit Sub
            End Try

            ' Sets up the title overlay
            TitleOverlay(mItem)

            ' Creates a temporary file for clips to save loading each pass through the loop
            Dim mFile As MediaFile = MediaFile.Create(file__1)

            ' Sets the clip duration based on the file duration and number of segments
            Dim clipDuration As Double = mFile.Duration.TotalSeconds * invSegments

            ' First clip already starts at 0, set first clip end based on clip duration
            mItem.Sources(0).Clips(0).EndTime = TimeSpan.FromSeconds(clipDuration)
            For i As Integer = 1 To NUM_Clips - 1
                ' Loops through and adds clips at different points through the media file
                mItem.Sources.Add(New Source(mFile))
                mItem.Sources(i).Clips(0).StartTime = TimeSpan.FromSeconds(clipDuration * NUM_Clips * i)
                mItem.Sources(i).Clips(0).EndTime = TimeSpan.FromSeconds(clipDuration + clipDuration * NUM_Clips * i)
            Next

            ' Creates job for encoding media
            Dim job As New Job()

            ' Add the media item and set the output
            job.MediaItems.Add(mItem)
            job.OutputDirectory = "c:\output"

            ' A progress event to keep track of the encode
            AddHandler job.EncodeProgress, AddressOf OnProgress

            Console.WriteLine("Encoding Pass 1 of 2")
            job.Encode()

            ' Saves the file name of the first encoded file
            Dim firstEncode As String = mItem.ActualOutputFileFullPath

            ' Sets the media item to the newly encoded file
            mItem = New MediaItem(firstEncode)

            ' Sets up a video overlay of the original media item
            MediaOverlay(mItem, file__1)

            ' Adds the newly revamped media item to the job and reencodes
            job.MediaItems.Add(mItem)
            Console.WriteLine(vbLf & "Encoding Pass 2 of 2")
            job.Encode()

            ' Removes the first encoded file and cleans up the job
            File.Delete(firstEncode)
            job.Dispose()
        End Sub

        Private Shared Sub TitleOverlay(ByVal mItem As MediaItem)
            ' Gets the media items name for the title overlay
            Dim title As String = mItem.Metadata(MetadataNames.Title)

            ' Adds the Source File Name if no Title in Metadata
            If title Is Nothing Then
                title = mItem.SourceFileName
                ' Removes path for source file name
                title.Remove(0, title.LastIndexOf("\"))
            End If

            ' Opens xml document and loads in our title overlay
            Dim doc As New XmlDocument()
            doc.Load(TITLE_FILE)

            Dim node As XmlNode = doc.LastChild
            While node.HasChildNodes
                ' Locates the node containing the label
                node = node.LastChild
            End While

            ' Creates the title attribute and adds it to the xaml before saving
            Dim atribute As XmlAttribute = doc.CreateAttribute("Content")
            atribute.Value = title
            node.Attributes.Append(atribute)
            doc.Save(TITLE_FILE)

            ' Sets the overlay for the title
            mItem.OverlayFileName = TITLE_FILE

            ' Sets the overlay size to be full video sized
            Dim overlayRect As New Rectangle(0, 0, mItem.VideoSize.Width, mItem.VideoSize.Height)
            mItem.OverlayRect = overlayRect
        End Sub

        Private Shared Sub MediaOverlay(ByVal mItem As MediaItem, ByVal overlayFile As String)
            ' Adds the original file as an overlay
            mItem.OverlayFileName = overlayFile

            ' Plays the overlay once
            mItem.OverlayLoop = False

            ' Sets the size and location of the new overlay based on the overlay scale
            Dim overlayRect As New Rectangle()
            overlayRect.Size = New Size(mItem.VideoSize.Width / OVERLAY_SCALE, mItem.VideoSize.Height / OVERLAY_SCALE)
            overlayRect.Location = New Point(overlayRect.Size.Width / 2, overlayRect.Size.Height / 2)
            mItem.OverlayRect = overlayRect
        End Sub

        ''' <summary>
        ''' Gives a running status of the encode
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        Private Shared Sub OnProgress(ByVal sender As Object, ByVal e As EncodeProgressEventArgs)
            Console.Write(vbBack & vbBack & vbBack & vbBack & vbBack & "{0:F1}%", e.Progress)
        End Sub
    End Class
End Namespace