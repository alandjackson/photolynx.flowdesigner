﻿Imports System
Imports System.Collections.Generic
Imports System.Text
Imports Microsoft.Expression.Encoder

Namespace Simple
    Class Program
        Shared Sub Main(ByVal args As String())
            ' Checks to see if there are the proper number of arguments
            If args.Length <> 1 Then
                ' Use the command-line arguments to build the list of media files to encode
                Console.WriteLine("Usage: Simple <MediaFile>")
                Exit Sub
            End If

            Dim mediaItem As MediaItem
            Try
                ' sets file name to media item
                mediaItem = New MediaItem(args(0))
            Catch exp As InvalidMediaFileException
                ' Media file was invalid and it returns an error msg
                Console.WriteLine(exp.ToString())
                Exit Sub
            End Try

            ' verifies encoding of file
            Console.WriteLine(vbLf & "Encoding: {0}", args(0).ToString())

            ' Create a job and the media item for the video we wish
            ' to encode.
            Dim job As New Job()
            job.MediaItems.Add(mediaItem)

            ' Set up the progress callback function
            AddHandler job.EncodeProgress, AddressOf OnProgress

            ' Set the output directory and encode.
            job.OutputDirectory = "C:\output"

            ' encodes job
            job.Encode()
        End Sub

        Private Shared Sub OnProgress(ByVal sender As Object, ByVal e As EncodeProgressEventArgs)
            Console.Write(vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack)
            Console.Write("{0:F2}%", e.Progress)
        End Sub
    End Class
End Namespace