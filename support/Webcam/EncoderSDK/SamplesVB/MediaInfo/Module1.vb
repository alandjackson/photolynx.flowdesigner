﻿Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.IO
Imports Microsoft.Expression.Encoder

Namespace MediaInfo
    ''' <summary>
    ''' MediaItem Sample class
    ''' This console app sample demonstrates how to use the MediaItem class to
    ''' extract media and metadata information from a media file given as an input.
    ''' </summary>
    Public Class Program
        ''' <summary>
        ''' Main function
        ''' </summary>
        ''' <param name="args">Command-line arguments</param>
        Public Shared Sub Main(ByVal args As String())
            If args.Length <> 1 Then
                ' There should be only one argument, which is the media filename
                Console.WriteLine("Usage: MediaInfo <MediaFile>")
                Return
            End If

            Try
                Dim mediaItem As New MediaItem(args(0))

                ' Get the file type (audio/video)
                Dim hasAudio As Boolean = (mediaItem.OriginalFileType And FileType.Audio) = FileType.Audio
                Dim hasVideo As Boolean = (mediaItem.OriginalFileType And FileType.Video) = FileType.Video

                Dim info As String = "Type: "
                If hasAudio Then
                    info += IIf(hasVideo, "Audio/Video" & Chr(10) & "", "Audio Only" & Chr(10) & "")
                Else
                    info += IIf(hasVideo, "Video Only" & Chr(10) & "", "Unknown" & Chr(10) & "")
                End If

                ' If any, gather video information
                If (mediaItem.OriginalFileType And FileType.Video) = FileType.Video Then
                    info += "Video Information:" & Chr(10) & ""

                    ' Frame size
                    info += String.Format("" & Chr(9) & "Frame Size: {0} x {1}" & Chr(10) & "", mediaItem.OriginalVideoSize.Width, mediaItem.OriginalVideoSize.Height)

                    ' Frame rate
                    Dim frameRate As Double = mediaItem.OriginalFrameRate
                    If mediaItem.OriginalInterlaced Then
                        ' Doubling the frame rate in the interlaced case
                        frameRate *= 2
                    End If

                    info += String.Format("" & Chr(9) & "Frame Rate: {0:F2}{1}" & Chr(10) & "", frameRate, IIf(mediaItem.OriginalInterlaced, "i", "p"))

                    ' Viewing aspect ratio
                    info += String.Format("" & Chr(9) & "Aspect Ratio: {0}x{1}" & Chr(10) & "", mediaItem.OriginalAspectRatio.Width, mediaItem.OriginalAspectRatio.Height)

                    ' Duration
                    info += String.Format("" & Chr(9) & "Duration: {0}" & Chr(10) & "", mediaItem.FileDuration.ToString())
                End If

                ' If any, gather marker information
                Dim markerIndex As Integer = 0
                For Each markerData As Marker In mediaItem.Markers
                    System.Math.Max(System.Threading.Interlocked.Increment(markerIndex), markerIndex - 1)
                    info += String.Format("Marker ""{0}"" ({1}/{2}): {3}" & Chr(10) & "", markerData.Value, markerIndex, mediaItem.Markers.Count, markerData.Time.ToString())
                Next

                ' If any, gather script information
                Dim scriptIndex As Integer = 0
                If mediaItem.ScriptCommands.Count > 0 Then
                    For Each scriptData As ScriptCommand In mediaItem.ScriptCommands
                        System.Math.Max(System.Threading.Interlocked.Increment(scriptIndex), scriptIndex - 1)
                        info += String.Format("Script ({1}/{2}) Type:""{0}"" Command:""{3}"" Time:{4}" & Chr(10) & "", scriptData.Type, scriptIndex, mediaItem.ScriptCommands.Count, scriptData.Command, scriptData.Time.ToString())
                    Next
                End If

                ' If any, gather metadata information
                Dim metadataIndex As Integer = 0
                For Each pair As KeyValuePair(Of String, String) In mediaItem.Metadata
                    System.Math.Max(System.Threading.Interlocked.Increment(metadataIndex), metadataIndex - 1)
                    info += String.Format("Metadata ""{0}"" ({1}/{2}): """ + pair.Value + """" & Chr(10) & "", pair.Key, metadataIndex, mediaItem.Metadata.Count)
                Next

                ' Write the information gathered to the console
                Console.Write(info)
            Catch generatedExceptionName As InvalidMediaFileException
                ' This exception signifies that the file isn't a media file or that it's not supported by Expression Encoder
                Console.WriteLine("Error: Unsupported or invalid media file")
            End Try
        End Sub
    End Class
End Namespace
