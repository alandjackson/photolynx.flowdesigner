﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports Microsoft.Expression.Encoder
Imports System.IO

Module CommandEncoder
    Sub Main(ByVal args As String())
        ' Checks the number of arguments and advises of usages
        If args.Length < 1 Then
            Console.WriteLine("Usage: CommandEncoder -j[d] <JobItem> [Destination]")
            Console.WriteLine("Usage: CommandEncoder -m[p][d] <MediaItem> [Preset] [Destination]")
            Exit Sub
        End If

        ' converts string to lower case
        args(0) = args(0).ToLower()
        Select Case args(0)
            Case ("-j")
                LoadJob(args(1), Nothing)
                Exit Select

            Case ("-jd")
                If args.Length = 3 AndAlso Directory.Exists(args(2)) Then
                    LoadJob(args(1), args(2))
                Else
                    Console.WriteLine("Invalid Number of Arguments for Command -jd")
                End If
                Exit Select

            Case ("-m")
                LoadMedia(args(1), Nothing, Nothing)
                Exit Select

            Case ("-mp")
                If args.Length = 3 Then
                    LoadMedia(args(1), args(2), Nothing)
                Else
                    Console.WriteLine("Invalid Number of Arguments for Command -mp")
                End If
                Exit Select

            Case ("-md")
                If args.Length = 3 AndAlso Directory.Exists(args(2)) Then
                    LoadMedia(args(1), Nothing, args(2))
                Else
                    Console.WriteLine("Invalid Number of Arguments for Command -md")
                End If
                Exit Select

            Case ("-mpd")
                If args.Length = 4 AndAlso Directory.Exists(args(3)) Then
                    LoadMedia(args(1), args(2), args(3))
                Else
                    Console.WriteLine("Invalid Number of Arguments for Command -mpd")
                End If
                Exit Select
            Case Else

                Console.WriteLine("Invalid Entry")
                Exit Select
        End Select
    End Sub

    ''' <summary>
    ''' Verifies the job and loads it
    ''' </summary>
    ''' <param name="JobName"></param>
    ''' <param name="Destination"></param>
    Private Sub LoadJob(ByVal JobName As String, ByVal Destination As String)
        Dim job__1 As Job
        Try
            job__1 = Job.LoadJob(JobName)
        Catch generatedExceptionName As InvalidJobException
            Console.WriteLine("Error: Invalid Job or Path")
            Exit Sub
        End Try

        Encode(job__1, Nothing, Destination)
    End Sub

    ''' <summary>
    ''' Verfies the media file, creates the job and adds the media file to the job
    ''' </summary>
    ''' <param name="MediaFile"></param>
    ''' <param name="PresetName"></param>
    ''' <param name="Destination"></param>
    Private Sub LoadMedia(ByVal MediaFile As String, ByVal PresetName As String, ByVal Destination As String)
        Dim mItem As MediaItem
        Try
            mItem = New MediaItem(MediaFile)
        Catch generatedExceptionName As InvalidMediaFileException
            Console.WriteLine("Error: Invalid Media File or Path")
            Exit Sub
        End Try

        Dim job As New Job()
        job.MediaItems.Add(mItem)

        Encode(job, PresetName, Destination)
    End Sub

    ''' <summary>
    ''' Encodes the job using any passed presets and custom destinations
    ''' </summary>
    ''' <param name="job"></param>
    ''' <param name="PresetName"></param>
    ''' <param name="Destination"></param>
    Private Sub Encode(ByVal job As Job, ByVal PresetName As String, ByVal Destination As String)
        ' Sets the destination if defined and uses a default if not
        If Destination IsNot Nothing Then
            job.OutputDirectory = Destination
        Else
            job.OutputDirectory = "c:\output"
        End If

        ' Set up the progress callback function
        AddHandler job.EncodeProgress, AddressOf OnProgress

        'Sets preset if it was set and encodes job
        Try
            If PresetName IsNot Nothing Then
                job.ApplyPreset(Preset.FindPreset(PresetName))
            End If

            job.Encode()
        Catch exp As EncodeErrorException
            Console.WriteLine("Encoding Error: " & exp.ToString())
            Exit Sub
        Catch exp As PresetNotFoundException
            Console.WriteLine("Invalid Preset: " & exp.ToString())
            Exit Sub
        End Try
    End Sub

    Private Sub OnProgress(ByVal sender As Object, ByVal e As EncodeProgressEventArgs)
        Console.Write(vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & "{0:F1}%", e.Progress)
    End Sub
End Module