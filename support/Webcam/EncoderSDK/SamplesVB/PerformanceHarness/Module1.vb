﻿Imports System.Collections.Generic
Imports System.IO
Imports System.Reflection
Imports System.Text
Imports System.Diagnostics
Imports Microsoft.Expression.Encoder
Imports System.Linq

Namespace PerformanceHarness

    Public NotInheritable Class Program
        Private Sub New()
        End Sub
        Private Shared input As String
        Private Shared timeStartFirstPass As DateTime
        Private Shared timeStartSecondPass As DateTime
        Private Shared timeEndFirstPass As DateTime
        Private Shared timeEndSecondPass As DateTime
        Private Shared twoPass As Boolean = False
        Private Shared on2ndPass As Boolean = False
        Private Shared firstPassSampleCount As Integer = 0
        Private Shared secondPassSampleCount As Integer = 0
        Private Shared currentProgress As Double = 0
        Private Shared lastSampleProgress As Integer = 0
        Private Shared cpuCounter As New PerformanceCounter()
        Private Shared firstPassCpuLoad As Double
        Private Shared secondPassCpuLoad As Double
        Private Shared cpuStreamCount As Integer = 0
        Private Shared gpuStreamCount As Integer = 0
        Private Shared verbose As Boolean = False
        Private Shared maxGPUStreamCount As Integer = 0
        Private Shared useGPU As Boolean
        Private Shared isDir As Boolean

        Public Shared Sub Main(ByVal args As String())
            cpuCounter.CategoryName = "Processor"
            cpuCounter.CounterName = "% Processor Time"
            cpuCounter.InstanceName = "_Total"

            If args.Length < 1 OrElse args.Length > 4 Then
                ShowError("Invalid number of parameters")
                Return
            End If

            ' Parses input and returns if invalid parameters
            Dim parse As Integer = ParseInput(args)
            If parse <> 0 Then
                Return
            End If

            System.Threading.Thread.Sleep(TimeSpan.FromSeconds(5))

            ' Logging general information
            LogGeneralInfo()

            Dim success As Boolean = True
            If isDir Then
                Dim jobs As String() = Directory.GetFiles(input, "*.xej")
                For Each job As String In jobs
                    If Not EncodeJob(job, useGPU) Then
                        success = False
                        Console.WriteLine("Failed to encode job: {0}.", job)
                    End If
                Next
            Else
                success = EncodeJob(input, useGPU)
            End If

            Return
        End Sub

        ''' <summary>
        ''' Parses the user input and sets settings flags.
        ''' </summary>
        Private Shared Function ParseInput(ByVal args As String()) As Integer
            ' Parsing the option arguments
            For Each arg As String In args
                Dim lowerArg As String() = arg.ToLower().Split("="c)
                Select Case lowerArg(0)
                    Case "/usegpu"
                        useGPU = True
                        Exit Select
                    Case "/verbose"
                        verbose = True
                        Exit Select
                    Case "/maxgpustream"
                        If lowerArg.Length = 2 Then
                            Dim count As Integer
                            If Integer.TryParse(lowerArg(1), count) Then
                                maxGPUStreamCount = count
                            Else
                                Return ShowError("Invalid MaxGPUStream argument.")
                            End If
                        Else
                            Return ShowError("Invalid Parameter")
                        End If
                        Exit Select
                    Case Else
                        If Directory.Exists(lowerArg(0)) Then
                            input = lowerArg(0)
                            isDir = True
                        ElseIf File.Exists(lowerArg(0)) AndAlso Path.GetExtension(lowerArg(0)) = ".xej" Then
                            input = lowerArg(0)
                        Else
                            Return ShowError("Invalid File or Directory")
                        End If
                        Exit Select
                End Select
            Next
            If String.IsNullOrWhiteSpace(input) Then
                Return ShowError("No valid File or Directory entered.")
            End If

            Return 0
        End Function

        ''' <summary>
        ''' Logs out general information, like date/time, Encoder version and hardware information.
        ''' </summary>
        Private Shared Sub LogGeneralInfo()
            ' Run Date
            Console.WriteLine("Run Date/Time: {0}", DateTime.Now.ToString())

            ' Encoder Version
            Dim path As String = Assembly.GetAssembly(GetType(Job)).CodeBase
            Dim myFileVersionInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(path.Replace("file:///", ""))
            Console.WriteLine("Encoder Version: " & myFileVersionInfo.FileVersion)

            ' Machine Name
            Console.WriteLine("Machine Name: {0}", Environment.MachineName)

            ' OS
            Console.Write("OS: {0}", Environment.OSVersion.ToString())

            ' 32 or 64-bit?
            If Directory.Exists(Environment.GetEnvironmentVariable("SystemDrive") & "\Program Files (x86)") Then
                Console.WriteLine(" (64-bit)")
            Else
                Console.WriteLine(" (32-bit)")
            End If

            ' Number of Logical Cores
            Console.WriteLine("Logical Cores: {0}", Environment.ProcessorCount)

            ' Names of the Encode Devices
            Dim gpuString As String = ": None"
            If H264EncodeDevices.Devices.Count > 0 Then
                gpuString = ": "

                For i As Integer = 0 To H264EncodeDevices.Devices.Count - 1
                    gpuString += String.Format("{0}" & vbTab, H264EncodeDevices.Devices(i).Name)
                Next
            End If
            Console.WriteLine("GPU Device(s){0}", gpuString)
        End Sub

        Private Shared Sub CleanUpCounters()
            twoPass = False
            on2ndPass = False
            firstPassSampleCount = 0
            secondPassSampleCount = 0
            currentProgress = 0
            lastSampleProgress = 0
            firstPassCpuLoad = 0
            secondPassCpuLoad = 0
            cpuStreamCount = 0
            gpuStreamCount = 0
        End Sub

        ''' <summary>
        ''' Encodes the job and profiles the performance
        ''' </summary>
        ''' <param name="input">Input job file</param>
        ''' <param name="usingGpu">true if GPU encoding should be enabled</param>
        ''' <returns>true if success</returns>
        Private Shared Function EncodeJob(ByVal input As String, ByVal usingGpu As Boolean) As Boolean
            CleanUpCounters()

            Dim job__1 As Job = Nothing
            Try
                job__1 = Job.LoadJob(input)
            Catch generatedExceptionName As InvalidJobException
                Return False
            Catch generatedExceptionName As InvalidMediaFileException
                Return False
            Catch generatedExceptionName As FeatureNotAvailableException
                Return False
            End Try

            Console.Write(Environment.NewLine & "Encoding Job " & Path.GetFileName(input))

            ' Set up the progress callback function
            AddHandler job__1.EncodeProgress, AddressOf OnProgress

            Console.WriteLine(If(usingGpu, " using GPU ...", " using CPU ..."))
            Try
                H264EncodeDevices.EnableGpuEncoding = usingGpu
                H264EncodeDevices.MaxNumberGPUStream = If(maxGPUStreamCount > 0, maxGPUStreamCount, H264EncodeDevices.MaxNumberGPUStream)
            Catch generatedExceptionName As FeatureNotAvailableException
            End Try

            timeStartFirstPass = DateTime.Now

            Try
                job__1.Encode()
            Catch e As Exception
                Dim secString As String = If(twoPass AndAlso on2ndPass, " [2nd pass]", "")
                Console.WriteLine("ERROR: Exception was thrown at {0:F1}% {1}", currentProgress, secString)
                Console.WriteLine(e.ToString() & Environment.NewLine)
                Return False
            End Try

            If twoPass Then
                timeEndSecondPass = DateTime.Now
            Else
                timeEndFirstPass = DateTime.Now
            End If

            ' Reporting results
            Dim firstPassEncodeTime As TimeSpan = timeEndFirstPass - timeStartFirstPass
            Dim secondPassEncodeTime As TimeSpan = timeEndSecondPass - timeStartSecondPass

            Dim frameRate As Double = DirectCast(job__1.MediaItems(0).Sources(0).MediaFile, AudioVideoFile).VideoStreams(0).FrameRate
            Dim duration As Double = job__1.MediaItems(0).Sources(0).Clips(0).EndTime.TotalSeconds
            Dim fpsFirstPass As Double = frameRate * duration / firstPassEncodeTime.TotalSeconds
            Dim fpsSecondPass As Double = frameRate * duration / secondPassEncodeTime.TotalSeconds
            Dim fpsTotal As Double = frameRate * duration / (firstPassEncodeTime.TotalSeconds + secondPassEncodeTime.TotalSeconds)

            Dim fpsString As String = ""
            If frameRate > 0.1 Then
                fpsString = " ({1:F2} fps)"
            End If

            Console.WriteLine()
            If twoPass Then
                Console.WriteLine("1st Pass Encoding time: {0}" & fpsString, firstPassEncodeTime, fpsFirstPass)
                Console.WriteLine("2nd Pass Encoding time: {0}" & fpsString, secondPassEncodeTime, fpsSecondPass)
            End If

            Console.WriteLine("Total Encoding time: {0}" & fpsString, firstPassEncodeTime + secondPassEncodeTime, fpsTotal)

            If twoPass Then
                Console.WriteLine("1st Pass Average CPU Load: {0:F1}%", firstPassCpuLoad / firstPassSampleCount)
                Console.Write("2nd Pass Average CPU Load: {0:F1}%", secondPassCpuLoad / secondPassSampleCount)
            Else
                Console.Write("Average CPU Load: {0:F1}%", firstPassCpuLoad / firstPassSampleCount)
            End If

            If usingGpu Then
                Console.Write("  GPU Streams: {0}/{1}", gpuStreamCount, gpuStreamCount + cpuStreamCount)
            End If

            Console.Write(Environment.NewLine)

            job__1.Dispose()

            Return True
        End Function

        Private Shared Sub OnProgress(ByVal sender As Object, ByVal e As EncodeProgressEventArgs)
            twoPass = e.TotalPasses = 2
            currentProgress = e.Progress

            If cpuStreamCount = 0 AndAlso e.StreamEncodeTypes.Count > 0 Then
                cpuStreamCount = InlineAssignHelper(gpuStreamCount, 0)
                Dim ready As Boolean = True
                For Each type As StreamEncodeType In e.StreamEncodeTypes
                    Select Case type
                        Case StreamEncodeType.Software
                            cpuStreamCount += 1
                            Exit Select
                        Case StreamEncodeType.Hardware
                            gpuStreamCount += 1
                            Exit Select
                        Case Else
                            ready = False
                            Exit Select
                    End Select
                    If Not ready Then
                        cpuStreamCount = 0
                    End If
                Next
            End If

            If twoPass AndAlso Not on2ndPass AndAlso e.CurrentPass = e.TotalPasses Then
                on2ndPass = True
                timeEndFirstPass = InlineAssignHelper(timeStartSecondPass, DateTime.Now)
            End If

            If lastSampleProgress <> CInt(e.Progress) Then
                lastSampleProgress = CInt(e.Progress)
                Dim current As Double = cpuCounter.NextValue()
                If verbose Then
                    Console.Write(vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack)
                    Console.Write("Pass {0}/{1} - {2:F1}%  CPU Load: {3:F1}%", e.CurrentPass, e.TotalPasses, e.Progress, current)
                End If

                If on2ndPass Then
                    secondPassSampleCount += 1
                    secondPassCpuLoad += current
                Else
                    firstPassSampleCount += 1
                    firstPassCpuLoad += current
                End If
            End If
        End Sub

        Private Shared Function ShowError(ByVal [error] As String) As Integer
            Console.WriteLine("ERROR: " & [error] & vbLf)
            WriteUsage()
            Return 1
        End Function

        Private Shared Sub WriteUsage()
            Console.WriteLine("Usage: PerfHarness.exe /UseGPU /MaxGPUStream=<Y> <job file or directory>")
            Console.WriteLine(" ")
            Console.WriteLine("    /UseGPU: Enables GPU encoding (Cuda or Intel QSV)")
            Console.WriteLine("    /MaxGPUStream=5: Max count of streams to be encoded by the GPU (default=5)")
            Console.WriteLine("    <job file or directory> - .xej file to encode [REQUIRED]")
        End Sub
        Private Shared Function InlineAssignHelper(Of T)(ByRef target As T, ByVal value As T) As T
            target = value
            Return value
        End Function

    End Class
End Namespace

