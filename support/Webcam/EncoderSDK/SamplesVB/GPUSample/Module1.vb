﻿Imports System.IO

Namespace GPUSample
    Class Program
        Private Shared m_Ready As Boolean
        Private Shared encoder As New GPUEncoder()

        Public Shared Sub Main(ByVal args As String())
            ' Check input and give usage if asking for help or no arguments
            If args.Length = 0 OrElse args(0).ToLowerInvariant().Contains("?") OrElse args(0).ToLowerInvariant().Contains("help") Then
                Usage()
                Return
            End If
            Console.Read()

            ' Parse Input
            ParseInput(args)

            ' Ready flag is tripped. Begin Encoding
            If m_Ready Then
                encoder.Encode()
            Else
                Console.WriteLine("No media items added. Nothing to encode.")
            End If
        End Sub

        Private Shared Sub ParseInput(ByVal args As String())
            For i As Integer = 0 To args.Length - 1
                Select Case args(i).ToLowerInvariant()
                    ' Switch on live Mode
                    Case "/live"
                        encoder.IsLive = True
                        Exit Select
                        ' Add files from directory 
                    Case "/folder"
                        If i < args.Length - 1 Then
                            i += 1
                            Dim s As String = args(i)
                            If Directory.Exists(s) Then
                                Dim files As String() = Directory.GetFiles(s)
                                For Each file__1 As String In files
                                    encoder.Files.Add(file__1)
                                Next
                                m_Ready = True
                            Else
                                Console.WriteLine("Directory does not exist. {0} " & vbLf & "Skipping.", s)
                            End If
                        Else
                            Console.WriteLine("Not enough arguments please check usage")
                        End If
                        Exit Select
                        ' Add file for encoding
                    Case "/file"
                        If i < args.Length - 1 Then
                            i += 1
                            Dim s As String = args(i)
                            If File.Exists(s) Then
                                encoder.Files.Add(s)
                                m_Ready = True
                            Else
                                Console.WriteLine("File does not exist. {0} " & vbLf & "Skipping.", s)
                            End If
                        Else
                            Console.WriteLine("Not enough arguments please check usage")
                        End If
                        Exit Select
                        ' Add duration for encoding live source
                    Case "/time"
                        If i < args.Length - 1 Then
                            i += 1
                            Dim time As Integer
                            If Int32.TryParse(args(i), time) Then
                                encoder.AddTime(time)
                            Else
                                Console.WriteLine("Invalid time parameter. Must be whole number in seconds. {0} " & vbLf & "Skipping.", args(i))
                            End If
                        Else
                            Console.WriteLine("Not enough arguments please check usage")
                        End If
                        Exit Select
                        ' Enable GPU and set the number of GPU Streams
                    Case "/gpu"
                        encoder.UseGPU = True
                        If i < args.Length - 1 Then
                            i += 1
                            If Not Int32.TryParse(args(i), encoder.GPUStreams) Then
                                Console.WriteLine("Invalid GPU stream parameter. {0} " & vbLf & "Skipping.", args(i))
                            End If
                        End If
                        Exit Select
                        ' Add preset name or keywords for encode type
                    Case "/preset"
                        If i < args.Length - 1 Then
                            i += 1
                            encoder.PresetKey = args(i)
                        Else
                            Console.WriteLine("Not enough arguments please check usage")
                        End If
                        Exit Select
                        ' Set output name or directory
                    Case "/output"
                        If i < args.Length - 1 Then
                            i += 1
                            Dim s As String = args(i)
                            If Directory.Exists(s) Then
                                encoder.OutDir = s
                            Else
                                encoder.OutName = s
                            End If
                        Else
                            Console.WriteLine("Not enough arguments please check usage")
                        End If
                        Exit Select
                    Case Else
                        Console.WriteLine("Invalid parameter. {0} " & vbLf & "Skipping.", args(i))
                        Exit Select
                End Select
            Next
        End Sub

        Private Shared Sub Usage()
            Console.WriteLine("GPUSample was created to show how to enable GPU encoding.")
            Console.WriteLine("This sample can accept multiple media files and can encode them in realtime.")
            Console.WriteLine(vbLf & "Arguments-------------------------------------------------------------------")
            Console.WriteLine("/live  Indicates that encoding is in real time.")
            Console.WriteLine("/time WholeNumber  The time to before switching to next live source.")
            Console.WriteLine("/folder String  The path to a directory containing media to encode.")
            Console.WriteLine("/file String  Sets the full path of a media item, multiple sources can be used.")
            Console.WriteLine("/gpu WholeNumber  Sets GPU encoding and the number of GPU streams.")
            Console.WriteLine("/preset String  Sets the preset for encoding with part of the name or keywords")
            Console.WriteLine("/output String  The file name or the output directory to encode to.")
            Console.WriteLine(vbLf & "Defaults--------------------------------------------------------------------")
            Console.WriteLine("Time is 10 seconds. GPU streams is 5. Output is in My Docs with original file name.")
            Console.WriteLine(vbLf & "Example---------------------------------------------------------------------")
            Console.WriteLine("GPUSample.exe /Live /Time 30 /file C:/MyVideo/video.wmv /file C:/MyVideo/video2.wmv /GPU 3 /preset ""VC-1 420p"" /output Encoded")
        End Sub
    End Class
End Namespace