﻿Imports System.Collections.Generic
Imports System.IO
Imports Microsoft.Expression.Encoder
Imports Live = Microsoft.Expression.Encoder.Live

Namespace GPUSample
    Class GPUEncoder
        Public GPUStreams As Integer
        Public UseGPU As Boolean
        Public IsLive As Boolean
        Public OutDir As String
        Public OutName As String
        Public PresetKey As String
        Public Files As List(Of String)

        Private m_Error As Boolean
        Private m_Preset As Preset
        Private m_TimeSpan As TimeSpan

        Public Sub New()
            GPUStreams = 5
            Files = New List(Of String)()
            m_TimeSpan = TimeSpan.FromSeconds(10)
            OutDir = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
        End Sub

        Private Sub EnableGPU()
            ' Check to see if there are any GPU encoding devices available
            If H264EncodeDevices.Devices.Count > 0 Then
                ' Enable GPU encoding and set the number of streams to use.
                H264EncodeDevices.EnableGpuEncoding = True
                If GPUStreams > 0 AndAlso GPUStreams <= 16 Then
                    H264EncodeDevices.MaxNumberGPUStream = GPUStreams
                End If
            End If
        End Sub

        Public Function AddPreset() As Boolean
            ' Split the string into parts
            Dim parts As String() = PresetKey.Split(" "c)
            For Each p As Preset In Preset.SystemPresets
                ' Verify preset mode matches live mode
                If IsLive = p.IsLivePreset Then
                    Dim match As Boolean = True
                    ' Iterate through all string parts
                    For Each s As String In parts
                        ' String part not found so we do not have a match. Go to next preset.
                        If Not p.Name.Contains(s) Then
                            match = False
                            Exit For
                        End If
                    Next
                    ' Found a match and return.
                    If match Then
                        m_Preset = p
                        Return True
                    End If
                End If
            Next

            Return False
        End Function

        Public Sub AddTime(ByVal time As Integer)
            m_TimeSpan = TimeSpan.FromSeconds(time)
        End Sub

        Public Sub Encode()
            ' Try to enable GPU encoding if it is set
            If UseGPU Then
                EnableGPU()
            End If

            If Not String.IsNullOrWhiteSpace(PresetKey) Then
                If Not AddPreset() Then
                    Console.WriteLine("Could not locate preset containing {0}, will use defaults.", PresetKey)
                End If
            End If

            ' Start encoding based on live flag
            If IsLive Then
                LiveEncode()
            Else
                OffLineEncode()
            End If
        End Sub

        Public Sub LiveEncode()
            ' Setup job for live encoding
            Using job As New Live.LiveJob()
                ' Create a list for all file sources
                Dim sources As New List(Of Live.LiveFileSource)()
                For Each file As String In Files
                    Try
                        ' Try to create file source based
                        sources.Add(job.AddFileSource(file))
                    Catch generatedExceptionName As InvalidMediaFileException
                        Console.WriteLine("Couldn't load media file: {0}", file)
                    End Try

                    ' No sources to encode so give error and quit
                    If sources.Count <= 0 Then
                        Console.WriteLine("No media files loaded. Cannot continue")
                        Return
                    End If

                    ' Activate first source and add status event handler
                    job.ActivateSource(sources(0))
                    AddHandler job.Status, AddressOf Status

                    ' Set preset and output extension
                    Dim extension As String = ".wmv"
                    If m_Preset IsNot Nothing Then
                        job.ApplyPreset(m_Preset)
                        If m_Preset.IsSmoothStreaming Then
                            extension = ".ismv"
                        End If
                    End If

                    ' Set output name and start encoding
                    If String.IsNullOrWhiteSpace(OutName) Then
                        Dim now As DateTime = DateTime.Now
                        OutName = String.Format("Live stream {0}-{1}-{2} {3}", now.Month, now.Day, now.Year, now.Hour, now.Minute)
                    End If
                    job.PublishFormats.Add(New Live.FileArchivePublishFormat(Path.Combine(OutDir, OutName & extension)))
                    job.StartEncoding()

                    ' Loop that changes source based on time and exits if error.
                    Dim endTime As DateTime = DateTime.Now + TimeSpan.FromSeconds(m_TimeSpan.TotalSeconds * sources.Count)
                    Dim done As Boolean = False
                    Dim nextItem As DateTime = DateTime.Now + m_TimeSpan
                    Dim mediaIndex As Integer = 0
                    While Not m_Error AndAlso Not done
                        ' Sleeps then checks timing status
                        System.Threading.Thread.Sleep(TimeSpan.FromSeconds(1))

                        ' Check to see that we don't need to switch to next source
                        If DateTime.Now >= nextItem Then
                            mediaIndex += 1
                            If mediaIndex < sources.Count Then
                                job.ActivateSource(sources(mediaIndex))
                            Else
                                done = True
                            End If
                            nextItem = DateTime.Now + m_TimeSpan
                            Console.Write(vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack)
                            Dim progress As TimeSpan = endTime - DateTime.Now
                            Console.Write("{0:F2} seconds left.", progress.TotalSeconds)
                        End If
                    End While
                    job.StopEncoding()
                Next
            End Using
        End Sub

        Public Sub OffLineEncode()
            Using job As New Job()
                For Each file As String In Files
                    Try
                        ' Try to set each file as a media source
                        job.MediaItems.Add(New MediaItem(file))
                    Catch generatedExceptionName As InvalidMediaFileException
                        Console.WriteLine("Couldn't load media file: {0}", file)
                    End Try
                Next

                ' No sources added. Give error and exit.
                If job.MediaItems.Count <= 0 Then
                    Console.WriteLine("No media files loaded. Cannot continue")
                    Return
                End If

                ' Apply preset if there
                If m_Preset IsNot Nothing Then
                    job.ApplyPreset(m_Preset)
                End If

                ' Set output directory
                job.OutputDirectory = OutDir

                ' Set output file name
                For i As Integer = 0 To job.MediaItems.Count - 1
                    If Not String.IsNullOrWhiteSpace(OutName) Then
                        Dim tempName As String = String.Format("{0}_{1}.{2}", OutName, i, "{Default extension}")
                        job.MediaItems(i).OutputFileName = tempName
                    Else
                        job.MediaItems(i).OutputFileName = "{Original file name}.{Default extension}"
                    End If
                Next

                Try
                    job.Encode()
                Catch ex As EncodeErrorException
                    Console.WriteLine("Encoding Error: {0}", ex.Message)
                    Console.WriteLine("Cannot continue.")
                End Try
            End Using
        End Sub

        Private Sub Progress(ByVal obj As Object, ByVal e As EncodeProgressEventArgs)
            Console.Write(vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack)
            Console.Write("{0:F2}%", e.Progress)
        End Sub

        Private Sub Status(ByVal obj As Object, ByVal e As Live.EncodeStatusEventArgs)
            If e.Message IsNot Nothing Then
                ' check for error and if given throw flag to stop encoding
                Dim lowerMsg As String = e.Message.ToLowerInvariant()
                If lowerMsg.Contains("error") Then
                    Console.WriteLine("Encoding Error: {0}", e.Message)
                    Console.WriteLine("Cannot continue.")
                    m_Error = True
                End If
            End If
        End Sub
    End Class
End Namespace