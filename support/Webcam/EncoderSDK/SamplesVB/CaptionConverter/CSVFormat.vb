﻿Imports System.ComponentModel.Composition
Imports System.Globalization
Imports System.IO
Imports System.Text
Imports Microsoft.Expression.Encoder

Namespace CsvConverter
    <Export("CsvConverter", GetType(IDfxpConverterContract))>
    Public Class CsvFormat
        Implements IDfxpConverterContract

        ''' <summary>
        ''' Expose a reference to ourself so that MEF can enumerate us.
        ''' </summary>
        <Export()>
        Public ReadOnly Property CaptionExporter() As IDfxpConverterContract
            Get
                Return Me
            End Get
        End Property

#Region "Stock DFXP formatted output"
        Public Const DfxpPreamble As String = "<?xml version='1.0' encoding='UTF-8'?>" & vbCr & vbLf & "<tt xmlns='http://www.w3.org/2006/10/ttaf1' xmlns:tt='http://www.w3.org/2006/10/ttaf1' xmlns:ttm='http://www.w3.org/2006/10/ttaf1#metadata' xmlns:tts='http://www.w3.org/2006/10/ttaf1#styling' xmlns:ttp='http://www.w3.org/ns/ttml#parameter' xml:lang='{0}'>" & vbCr & vbLf & "<head>" & vbCr & vbLf & "<metadata>" & vbCr & vbLf & "<ttm:title></ttm:title>" & vbCr & vbLf & "<ttm:desc></ttm:desc>" & vbCr & vbLf & "<ttm:copyright></ttm:copyright>" & vbCr & vbLf & "</metadata>" & vbCr & vbLf & "<styling>" & vbCr & vbLf & "<style xml:id='backgroundStyle'" & vbCr & vbLf & "tts:fontFamily='proportionalSansSerif'" & vbCr & vbLf & "tts:fontSize='1.5c'" & vbCr & vbLf & "tts:textAlign='center'" & vbCr & vbLf & "tts:origin='0% 90%'" & vbCr & vbLf & "tts:extent='100% 8%'" & vbCr & vbLf & "tts:backgroundColor='rgba(0,0,0,0)'" & vbCr & vbLf & "tts:displayAlign='center'/>" & vbCr & vbLf & "<style xml:id='speakerStyle'" & vbCr & vbLf & "style='backgroundStyle'" & vbCr & vbLf & "tts:color='white'" & vbCr & vbLf & "tts:textOutline='0px 1px'" & vbCr & vbLf & "tts:backgroundColor='transparent'/>" & vbCr & vbLf & "</styling>" & vbCr & vbLf & "<layout>" & vbCr & vbLf & "<region xml:id='speaker' style='speakerStyle' tts:zIndex='1'/>" & vbCr & vbLf & "<region xml:id='background' style='backgroundStyle' tts:zIndex='0'/>" & vbCr & vbLf & "</layout>" & vbCr & vbLf & "</head>" & vbCr & vbLf & "<body>" & vbCr & vbLf & "<div>"
        Public Const DfxpEntry As String = "<p region='speaker' begin='{0}' end='{1}'>{2}</p>"
        Public Const DxfpEnd As String = "</div>" & vbCr & vbLf & "</body>" & vbCr & vbLf & "</tt>"
#End Region

        ''' <summary>
        ''' extensions supported by this converter
        ''' </summary>
        Public ReadOnly Property Extensions As String() Implements IDfxpConverterContract.Extensions
            Get
                Return New String() {".csv"}
            End Get
        End Property

        ''' <summary>
        ''' CSV to DFXP conversion
        ''' </summary>
        ''' <param name="sourceFile">source CSV file</param>
        ''' <param name="destinationDfxpFile">destination DFXP file to write</param>
        ''' <returns>true if converted ok, false if we cannot convert (may try another converter further down the chain)</returns>        
        Public Function Convert(ByVal sourceFile As String, ByVal destinationDfxpFile As String, ByVal culture As CultureInfo) As Boolean Implements IDfxpConverterContract.Convert
            Using csvReader As TextReader = File.OpenText(sourceFile)
                Using dfxpFileWriter As TextWriter = File.CreateText(destinationDfxpFile)
                    dfxpFileWriter.Write([String].Format(CultureInfo.InvariantCulture, CsvFormat.DfxpPreamble, culture))

                    Dim line As String = ""
                    While (InlineAssignHelper(line, csvReader.ReadLine())) IsNot Nothing
                        Dim parts As String() = line.Split(","c)
                        dfxpFileWriter.Write([String].Format(CultureInfo.InvariantCulture, CsvFormat.DfxpEntry, parts(0) & "ms", parts(1) & "ms", parts(2)))
                    End While

                    dfxpFileWriter.Write(CsvFormat.DxfpEnd)
                End Using
            End Using

            Return True
            ' converted successfully ...
        End Function

        Private Shared Function InlineAssignHelper(Of T)(ByRef target As T, ByVal value As T) As T
            target = value
            Return value
        End Function

    End Class
End Namespace