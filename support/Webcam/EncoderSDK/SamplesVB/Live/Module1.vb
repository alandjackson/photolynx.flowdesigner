﻿Imports Microsoft.Expression.Encoder
Imports Microsoft.Expression.Encoder.Live

Module Module1
    ''' <summary>
    ''' This sample demonstrates boadcasting a simple file continuously looped.
    ''' </summary>
    ''' <param name="args">input parameters, the name of the file we are going to broadcast</param>
    Sub Main(ByVal args As String())
        ' We should have only 1 string in the argument array, and
        ' it should be the name of the file we are going to encode
        If args.Length <> 1 Then
            Console.WriteLine("Usage: live <file_name>")
            Return
        End If

        Dim fileToEncode As String = args(0)

        ' Create a new LiveJob to begin broadcasting this file. Make sure
        ' to dispose the LiveJob when you are finished with it.
        Using job As New LiveJob()
            ' Gets the status of the job and advises if there is an error
            AddHandler job.Status, AddressOf StatusCheck

            ' Create a new file source from the file name we were passed in
            Dim fileSource As LiveFileSource
            Try
                fileSource = job.AddFileSource(fileToEncode)
            Catch exp As InvalidMediaFileException
                Console.WriteLine(exp.ToString())
                Return
            End Try

            ' Set this source to Loop when finished
            fileSource.PlaybackMode = FileSourcePlaybackMode.[Loop]

            ' Adds an encoding preset to the job
            job.ApplyPreset(LivePresets.VC1512kDSL4x3)

            ' Make this source the active one
            job.ActivateSource(fileSource)

            ' Create a new windows media broadcast output format so we
            ' can broadcast this encoding on the current machine.
            ' We are going to use the default audio and video profiles
            ' that are created on this output format.
            Dim outputFormat As New PullBroadcastPublishFormat()

            ' Let's broadcast on the local machine on port 8080
            outputFormat.BroadcastPort = 8080

            ' Set the output format on the job
            job.PublishFormats.Add(outputFormat)

            ' Start encoding
            Console.WriteLine(vbLf & "Press 'x' to stop encoding...")
            job.StartEncoding()

            ' Let's listen for a keypress or error message to know when to stop encoding
            While Console.ReadKey(True).Key <> ConsoleKey.X


            End While

            ' Stop our encoding
            Console.WriteLine("Encoding stopped.")
            job.StopEncoding()
        End Using
    End Sub

    Private Sub StatusCheck(ByVal obj As Object, ByVal e As EncodeStatusEventArgs)
        Console.Write(vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack)
        Console.Write("Status: {0}", e.Status)
    End Sub
End Module
