﻿Imports System
Imports System.Collections.Generic
Imports System.Text
Imports Microsoft.Expression.Encoder.Plugins.Publishing
Imports System.Xml
Imports PublishPlugin.ZipPublishExample

Namespace MakeZip
    ''' <summary>
    ''' Main plugin class
    ''' Sets up the UI, invokes the zipping operation and persists the plugin settings
    ''' </summary>
    <EncoderPlugin("Zip Plugin from scratch", "Enables output to be zipped")> _
    Public Class ZipPublishPlugin
        Inherits Microsoft.Expression.Encoder.Plugins.Publishing.PublishPlugin
        ''' <summary>
        ''' Data binding object for the target directory
        ''' </summary>
        Private m_data As PublishData

        ''' <summary>
        ''' Was the operation cancelled?
        ''' </summary>
        Private m_cancelled As Boolean

        ''' <summary>
        ''' Basic constructor
        ''' </summary>
        Public Sub New()
            m_data = New PublishData()
        End Sub

        ''' <summary>
        ''' Gets a value indicating whether the publish has been canceled
        ''' </summary>
        Friend ReadOnly Property IsCancelled() As Boolean
            Get
                Return m_cancelled
            End Get
        End Property

        ''' <summary>
        ''' Creates the UI
        ''' </summary>
        ''' <returns>Returns the UI object</returns>
        Public Overloads Overrides Function CreateStandardSettingsEditor() As Object
            Return New SimpleUI(m_data)
        End Function

        ''' <summary>
        ''' Sets the advanced settings
        ''' </summary>
        ''' <returns>Returns the advanced settings object</returns>
        Public Overloads Overrides Function CreateAdvancedSettingsEditor() As Object
            Return Nothing
        End Function

        ''' <summary>
        ''' Publish plugin call: zips all the files
        ''' </summary>
        ''' <param name="rootPath">Path of the files to be zipped</param>
        ''' <param name="filesToPublish">Filename list of the files to be zipped</param>
        Public Overloads Overrides Sub PerformPublish(ByVal rootPath As String, ByVal filesToPublish As String())
            m_cancelled = False

            Dim path As String = System.IO.Path.Combine(m_data.TargetDirectory, System.IO.Path.GetFileName(rootPath) & ".zip")

            CreateZip.CreateZipFile(Me, filesToPublish, path)
        End Sub

        ''' <summary>
        ''' Loads the plugin settings
        ''' </summary>
        ''' <param name="reader">Xml reader object to load the settings from</param>
        Public Overloads Sub LoadJobSettings(ByVal reader As XmlReader)
            ' Must nest our settings within a root element
            reader.ReadStartElement("ZipSample")
            m_data.TargetDirectory = reader.ReadElementString("TargetDirectory")
            reader.ReadEndElement()
        End Sub

        ''' <summary>
        ''' Persists the plugin settings
        ''' </summary>
        ''' <param name="writer">Xml writer object to save the settings to</param>
        Public Overloads Sub SaveJobSettings(ByVal writer As XmlWriter)
            writer.WriteStartElement("ZipSample")
            writer.WriteElementString("TargetDirectory", m_data.TargetDirectory)
            writer.WriteEndElement()
        End Sub

        ''' <summary>
        ''' Progress callback method for the Zip Code
        ''' </summary>
        ''' <param name="description">Description label</param>
        ''' <param name="progress">Progress (in percent)</param>
        Friend Sub CallOnProgress(ByVal description As String, ByVal progress As Double)
            OnProgress(description, progress)
        End Sub

        ''' <summary>
        ''' Publish plugin call: cancels the publish
        ''' </summary>
        Protected Overloads Sub CancelPublish()
            m_cancelled = True
        End Sub
    End Class
End Namespace