﻿Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Windows
Imports System.Windows.Controls
Imports System.Windows.Data
Imports System.Windows.Documents
Imports System.Windows.Input
Imports System.Windows.Media
Imports System.Windows.Media.Imaging
Imports System.Windows.Navigation
Imports System.Windows.Shapes

Namespace MakeZip
    ''' <summary>
    ''' Interaction logic for SimpleUI.xaml
    ''' </summary>
    Partial Public Class SimpleUI
        Inherits UserControl
        ''' <summary>
        ''' Object for data binding of the target directory
        ''' </summary>
        Private m_data As PublishData

        ''' <summary>
        ''' Constructor taking the data object
        ''' </summary>
        ''' <param name="data">Data object</param>
        Public Sub New(ByVal data As PublishData)
            m_data = data
            InitializeComponent()
            Me.DataContext = m_data
        End Sub

        ''' <summary>
        ''' Reset button has been clicked
        ''' </summary>
        ''' <param name="sender">Sender object</param>
        ''' <param name="e">Event arguments</param>
        Private Sub Reset_Click(ByVal sender As Object, ByVal e As RoutedEventArgs)
            m_data.ResetSettings()
        End Sub

        ''' <summary>
        ''' Browse button has been clicked 
        ''' </summary>
        ''' <param name="sender">Sender object</param>
        ''' <param name="e">Event arguments</param>
        Private Sub Browse_Click(ByVal sender As Object, ByVal e As RoutedEventArgs)
            Dim picker As New Shell32.Shell()
            Dim folder As Shell32.Folder = picker.BrowseForFolder(0, "Pick Folder", 0, 0)

            If folder IsNot Nothing Then
                Dim fi As Shell32.FolderItem = TryCast(folder, Shell32.Folder3).Self
                m_data.TargetDirectory = fi.Path
            End If
        End Sub
    End Class
End Namespace