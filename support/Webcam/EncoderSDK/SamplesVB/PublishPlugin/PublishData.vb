﻿Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.ComponentModel

Namespace MakeZip
    ''' <summary>
    ''' Data class for data binding of the target directory
    ''' </summary>
    Public Class PublishData
        Implements INotifyPropertyChanged
        ''' <summary>
        ''' Directory where the zip file will be saved 
        ''' </summary>
        Private m_targetDirectory As String = ""

        ''' <summary>
        ''' Basic constructor
        ''' </summary>
        Public Sub New()
            ResetSettings()
        End Sub

        ''' <summary>
        ''' Event fired when property changes
        ''' </summary>
        Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

        ''' <summary>
        ''' Gets or sets the directory where the zip file will be saved
        ''' </summary>
        Public Property TargetDirectory() As String
            Get
                Return m_targetDirectory
            End Get

            Set(ByVal value As String)
                m_targetDirectory = value
                OnPropChanged("TargetDirectory")
            End Set
        End Property

        ''' <summary>
        ''' Reset the setting to defaults
        ''' </summary>
        Public Sub ResetSettings()
            TargetDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop)
        End Sub

        ''' <summary>
        ''' Called when the property changed
        ''' </summary>
        ''' <param name="propName">Name of the property</param>
        Friend Sub OnPropChanged(ByVal propName As String)
            RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(propName))
        End Sub
    End Class
End Namespace