﻿Imports System.Runtime.InteropServices
Imports System.Windows
Imports Microsoft.Expression.Encoder.Live

Namespace LiveSourceSample
    ''' <summary>
    ''' Interaction logic for MainWindow.xaml
    ''' </summary>
    Partial Public Class PreviewUI
        Inherits Window
        Private source As LiveSource

        Public Sub New()
            InitializeComponent()
            source = New LiveSource()

            If source.[error] Then
                ' Gives error message as no audio and/or video devices found
                Dim result As MessageBoxResult = MessageBox.Show("No Video/Audio capture devices detected.", "Live Source Sample", MessageBoxButton.OK, MessageBoxImage.[Stop])
                Me.Close()
                Return
            End If

            ' sets preview window to winform panel hosted by xaml window
            source.deviceSource.PreviewWindow = New PreviewWindow(New HandleRef(panel1, panel1.Handle))
        End Sub

        ''' <summary>
        ''' Called when the close button is pressed. If we currently have a file being encoded,
        ''' then we prompt the user before encoding.
        ''' </summary>
        ''' <param name="sender">The sender of the event.</param>
        ''' <param name="e">The event arguments.</param>
        Private Overloads Sub OnClosing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
            If CBool(EncodeButton.IsChecked) Then
                ' We're still encoding, so warn the user before leaving
                Dim result As MessageBoxResult = MessageBox.Show("The file is still encoding. Are you sure you want to exit?", "WPF Encode", MessageBoxButton.YesNo)
                If result = MessageBoxResult.Yes Then
                    ' If the user still wants to exit then cancel the encode
                    ' and wait for the encode thread to finish.
                    source.[Stop]()
                Else
                    ' The user didn't want to exit so cancel the close.
                    e.Cancel = True
                    Return
                End If
            End If
            source.Dispose()
        End Sub

        Private Sub EncodeButton_Checked(ByVal sender As Object, ByVal e As RoutedEventArgs)
            ' Starts encoding
            source.Start()
        End Sub

        Private Sub EncodeButton_Unchecked(ByVal sender As Object, ByVal e As RoutedEventArgs)
            ' Stops encoding
            source.[Stop]()
        End Sub
    End Class
End Namespace
