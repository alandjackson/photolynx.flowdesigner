﻿Imports System.IO
Imports System.Runtime.InteropServices
Imports System.Collections.ObjectModel
Imports System.Windows
Imports Microsoft.Expression.Encoder
Imports Microsoft.Expression.Encoder.Devices
Imports Microsoft.Expression.Encoder.Live

Namespace LiveSourceSample
    Friend Class LiveSource
        ''' <summary>
        ''' Creates job for capture of live source
        ''' </summary>
        Private job As LiveJob

        ''' <summary>
        ''' Device for live source
        ''' </summary>
        Friend deviceSource As LiveDeviceSource

        Friend [error] As Boolean

        Friend Sub New()
            ' Starts new job for preview window
            job = New LiveJob()

            ' Aquires audio and video devices
            Dim devices As Collection(Of EncoderDevice) = EncoderDevices.FindDevices(EncoderDeviceType.Video)
            Dim video As EncoderDevice = If(devices.Count > 0, devices(0), Nothing)
            For i As Integer = 1 To devices.Count - 1
                devices(i).Dispose()
            Next
            devices.Clear()

            devices = EncoderDevices.FindDevices(EncoderDeviceType.Audio)
            Dim audio As EncoderDevice = If(devices.Count > 0, devices(0), Nothing)
            For i As Integer = 1 To devices.Count - 1
                devices(i).Dispose()
            Next
            devices.Clear()

            ' Checks for a/v devices
            If video IsNot Nothing AndAlso audio IsNot Nothing Then
                ' Create a new device source. We use the first audio and video devices on the system
                deviceSource = job.AddDeviceSource(video, audio)

                ' Make this source the active one
                job.ActivateSource(deviceSource)
            Else
                [error] = True
            End If
        End Sub


        Friend Sub Start()
            ' Sets up publishing format for file archival type
            Dim fileOut As New FileArchivePublishFormat()
            job.ApplyPreset(LivePresets.VC1512kDSL16x9)

            ' Gets timestamp and edits it for filename
            Dim timeStamp As String = DateTime.Now.ToString()
            timeStamp = timeStamp.Replace("/", "-")
            timeStamp = timeStamp.Replace(":", ".")

            ' Sets file path and name
            Dim path__1 As String = "C:\output\"
            Dim filename As String = "Capture" & timeStamp & ".wmv"
            If Not Directory.Exists(path__1) Then
                Directory.CreateDirectory(path__1)
            End If

            fileOut.OutputFileName = Path.Combine(path__1, filename)

            ' Adds the format to the job. You can add additional formats as well such as
            ' Publishing streams or broadcasting from a port
            job.PublishFormats.Add(fileOut)

            ' Start encoding
            job.StartEncoding()
        End Sub

        Friend Sub [Stop]()
            job.StopEncoding()
        End Sub

        Friend Sub Dispose()
            ' closes devices for preview and disposes of job
            If deviceSource IsNot Nothing Then
                job.RemoveDeviceSource(deviceSource)
                deviceSource.Dispose()
            End If

            ' disposes of job
            If job IsNot Nothing Then
                job.Dispose()
            End If
        End Sub
    End Class
End Namespace
