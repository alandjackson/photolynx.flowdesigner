﻿' Live Console Sample
' Copyright Microsoft Corporation

Imports System.IO
Imports System.Threading
Imports Microsoft.Expression.Encoder
Imports Microsoft.Expression.Encoder.Live
Namespace LiveConsoleSample

    Public Class Program
        ''' <summary>
        ''' Holds the number of retries if encoding encounters an error
        ''' </summary>
        Private Shared NumberOfRetries As Integer = 10

        ''' <summary>
        ''' Holds the duration to encode
        ''' </summary>
        Private Shared DurationInMinutes As Double = 5

        ''' <summary>
        ''' The LiveJob used for encoding.
        ''' </summary>
        Private Shared CurrentJob As LiveJob

        ''' <summary>
        ''' The current number of attempts made at encoding.
        ''' </summary>
        Private Shared CurrentTry As Integer

        ''' <summary>
        ''' A event that indicates whether the encode is completed, either from timeout or exceeding errors.
        ''' </summary>
        Private Shared Finished As ManualResetEvent

        ''' <summary>
        ''' Entry Point
        ''' </summary>
        ''' <param name="args">Array of strings passed in from the command line.</param>
        Public Shared Sub Main(ByVal args As String())
            ' We have no arguments and will show usage and quit
            If args.Length < 1 Then
                Console.WriteLine("This sample will encode a simple live job with no source switching for a set duration. The default time is 5 minutes.")
                Console.WriteLine("If there is an encoding error, this sample will attempt to re-encode. The default is 10 times")
                Console.WriteLine("Usage:LiveConsoleSample.exe </Retry> n </Duration> n [LiveJob File]")
                Console.WriteLine("Example: LiveConsoleSample.exe c:\jobs\livejob.xej")
                Console.WriteLine("Example: LiveConsoleSample.exe /retry 5 c:\jobs\livejob.xej")
                Console.WriteLine("Example: LiveConsoleSample.exe /duration 1.5 c:\jobs\livejob.xej")
                Return
            End If

            ' Parses the input and verifies we have a job.
            If Not ParseInput(args) Then
                Console.WriteLine("Invalid or missing job file.")
                Return
            End If

            EncodeJob(True)

            Finished = New ManualResetEvent(False)
            Finished.WaitOne(TimeSpan.FromMinutes(DurationInMinutes))

            CurrentJob.Dispose()
        End Sub

        ''' <summary>
        ''' Parse the intput strings into the proper settings.
        ''' </summary>
        ''' <param name="args">Array of input strings.</param>
        ''' <returns>If the parsing was successful.</returns>
        Private Shared Function ParseInput(ByVal args As String()) As Boolean
            ' flag that indicates having parsed in the minimal requirments
            Dim succeeded As Boolean = False

            ' iterates through the input and stores the valid data.
            For i As Integer = 0 To args.Length - 1
                Dim temp As String = args(i).ToLowerInvariant()
                Select Case temp
                    Case "/retry"
                        If True Then
                            If Integer.TryParse(args(i + 1), NumberOfRetries) Then
                                i += 1
                            Else
                                Console.WriteLine("Invalid argument used for the number of retries. Using {0}.", NumberOfRetries)
                            End If
                        End If
                        Exit Select
                    Case "/duration"
                        If True Then
                            If Double.TryParse(args(i + 1), DurationInMinutes) Then
                                i += 1
                            Else
                                Console.WriteLine("Invalid argument used for the duration. Using {0}.", DurationInMinutes)
                            End If
                        End If
                        Exit Select
                    Case Else
                        If True Then
                            If File.Exists(args(i)) Then
                                succeeded = OpenJob(args(i))
                            End If
                        End If
                        Exit Select
                End Select
            Next

            Return succeeded
        End Function

        ''' <summary>
        ''' This function attempts to open the job file and verifies that it has all the settings necessary to encode.
        ''' </summary>
        ''' <param name="filename">The jobs filename that will be loaded.</param>
        ''' <returns>If the job opens successfully.</returns>
        Private Shared Function OpenJob(ByVal filename As String) As Boolean
            ' Initializes the LiveJob object.
            CurrentJob = New LiveJob()

            ' Loads the job.
            Try
                CurrentJob.Load(filename)
            Catch ex As XmlErrorException
                Console.WriteLine(ex.Message)
                Return False
            End Try

            ' Checks to see that a job has all the components needed to encode.
            Dim isJobValid As Boolean = True
            If CurrentJob.OutputFormat.AudioProfile Is Nothing AndAlso CurrentJob.OutputFormat.VideoProfile Is Nothing Then
                Console.WriteLine("Job has no profile to encode to. Fix job and try again.")
                isJobValid = False
            End If

            If CurrentJob.DeviceSources.Count < 1 AndAlso CurrentJob.FileSources.Count < 1 Then
                Console.WriteLine("Job has no sources. Fix job and try again.")
                isJobValid = False
            End If

            If CurrentJob.PublishFormats.Count < 1 Then
                Console.WriteLine("Job has no publish type. Fix job and try again.")
                isJobValid = False
            End If

            Return isJobValid
        End Function

        ''' <summary>
        '''  Sets up callbacks and encode timer, then starts encode.
        ''' </summary>
        ''' <param name="firstTime">Indicates if this is the first encode attempt.</param>
        Private Shared Sub EncodeJob(ByVal firstTime As Boolean)
            If firstTime Then
                ' Sets up call back to handle encoding errors and starts encode
                AddHandler CurrentJob.Status, AddressOf Status
            End If

            Try
                CurrentJob.StartEncoding()
            Catch ex As EncodeErrorException
                Console.WriteLine(ex.Message)
                If Finished IsNot Nothing Then
                    Finished.[Set]()
                End If
            End Try
        End Sub

        ''' <summary>
        ''' Callback method for the encode status
        ''' </summary>
        ''' <param name="obj">Object passing in the event.</param>
        ''' <param name="e">The Encode parameters being passed in.</param>
        Private Shared Sub Status(ByVal obj As Object, ByVal e As EncodeStatusEventArgs)
            ' We have an error. Stop encoding and check number of tries.
            ' If under the set number then retry encode.
            If e.Status = EncodeStatus.EncodingError Then
                CurrentJob.StopEncoding()
                If CurrentTry < NumberOfRetries Then
                    CurrentTry += 1
                    EncodeJob(False)
                Else
                    Finished.[Set]()
                End If
            End If
        End Sub
    End Class
End Namespace
