﻿Imports System.Windows
Imports System.Windows.Forms
Imports Microsoft.Expression.Encoder.Live
Imports StringTable = LiveSmoothStreaming.My.Resources

Namespace LiveSmoothStreaming
    ''' <summary>
    ''' Interaction logic for MainWindow.xaml
    ''' </summary>
    Partial Public Class MainWindow
        Inherits Window
        Public streamer As Streamer

        ''' <summary>
        ''' Initializes a new instance of the MainWindow class
        ''' </summary>
        Public Sub New()
            streamer = New Streamer()
            DataContext = streamer
            InitializeComponent()
        End Sub

        ''' <summary>
        ''' Browse to select a filename 
        ''' </summary>
        ''' <param name="sender">The sender of the event.</param>
        ''' <param name="e">The event arguments.</param>
        Private Sub FileBrowse(ByVal sender As Object, ByVal e As RoutedEventArgs)
            Dim dialog As New OpenFileDialog()
            dialog.Title = StringTable.SelectMedia
            Dim result As DialogResult = dialog.ShowDialog()

            If result = System.Windows.Forms.DialogResult.OK Then
                FileName.Text = dialog.FileName
            End If
        End Sub

        ''' <summary>
        ''' Browse to select a destination folder 
        ''' </summary>
        ''' <param name="sender">The sender of the event.</param>
        ''' <param name="e">The event arguments.</param>
        Private Sub FolderBrowse(ByVal sender As Object, ByVal e As RoutedEventArgs)
            Dim dialog As New FolderBrowserDialog()
            Dim result As DialogResult = dialog.ShowDialog()
            If result = System.Windows.Forms.DialogResult.OK Then
                PublishPath.Text = dialog.SelectedPath
            End If
        End Sub

        ''' <summary>
        ''' Changes the Publishing Format and associated properties
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        Private Sub PublishFormat_DropDownClosed(ByVal sender As Object, ByVal e As EventArgs)
            Select Case PublishFormat.SelectedIndex
                Case 0
                    streamer.publishType = Streamer.Output.Archive
                    PathLabel.Content = "Folder:"
                    DestBrowse.Visibility = Visibility.Visible
                    Exit Select
                Case 1
                    streamer.publishType = Streamer.Output.Publish
                    PathLabel.Content = "Publish:"
                    DestBrowse.Visibility = Visibility.Hidden
                    Exit Select
                Case Else
                    Exit Select
            End Select
        End Sub

        ''' <summary>
        ''' Called when the user checks the stream button
        ''' </summary>
        ''' <param name="sender">The sender of the event.</param>
        ''' <param name="e">The event arguments.</param>
        Private Sub StartStream(ByVal sender As Object, ByVal e As RoutedEventArgs)
            If Not streamer.StartStream() Then
                streamer.Busy = False
                Dim result As MessageBoxResult = System.Windows.MessageBox.Show(StringTable.invalidPaths, "Error")
            Else
                Button_3.Content = "Stop Streaming"
            End If
        End Sub

        ''' <summary>
        ''' Called when the user unchecks Stream Button
        ''' </summary>
        ''' <param name="sender">The sender of the event.</param>
        ''' <param name="e">The event arguments.</param>
        Private Sub StopStream(ByVal sender As Object, ByVal e As RoutedEventArgs)
            If streamer.Busy Then
                ' Stops the thread and changes the button message
                streamer.StopStream()
                Button_3.Content = "Start Streaming"
            End If
        End Sub

        ''' <summary>
        ''' Called when the close button is pressed. If we currently have a file being encoded,
        ''' then we prompt the user before encoding.
        ''' </summary>
        ''' <param name="sender">The sender of the event.</param>
        ''' <param name="e">The event arguments.</param>
        Private Overloads Sub OnClosing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
            ' Checks if we are streaming and if so prompts to terminate
            If streamer.Busy Then
                ' We're still encoding, so warn the user before leaving
                Dim result As MessageBoxResult = System.Windows.MessageBox.Show(StringTable.streaming, StringTable.appName, MessageBoxButton.YesNo)
                If result = MessageBoxResult.No Then
                    ' The user didn't want to exit so cancel the close.
                    e.Cancel = True
                    Return
                End If
                streamer.StopStream()
            End If
            streamer.Dispose()
        End Sub
    End Class
End Namespace
