﻿Imports System.ComponentModel
Imports System.IO
Imports Microsoft.Expression.Encoder
Imports Microsoft.Expression.Encoder.Live

Namespace LiveSmoothStreaming
    Public Class Streamer
        Implements INotifyPropertyChanged
        ' The types of Encoding available
        Friend Enum Output
            Archive
            Publish
        End Enum

        Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

        ''' <summary>
        ''' Value for whether streaming is occuring
        ''' </summary>
        Private m_busy As Boolean
        Public Property Busy() As Boolean
            Get
                Return m_busy
            End Get
            Set(ByVal value As Boolean)
                m_busy = value
                onPropChanged("Busy")
            End Set
        End Property

        ' Variables to hold file and path information
        Private m_sourcePath As String
        Public Property SourcePath() As String
            Get
                Return m_sourcePath
            End Get
            Set(ByVal value As String)
                m_sourcePath = value
                onPropChanged("SourcePath")
            End Set
        End Property

        Private m_destinationPath As String
        Public Property DestinationPath() As String
            Get
                Return m_destinationPath
            End Get
            Set(ByVal value As String)
                m_destinationPath = value
                onPropChanged("DestinationPath")
            End Set
        End Property

        ' Determines if we are streaming
        Private m_status As EncodeStatus
        Public Property Status() As EncodeStatus
            Get
                Return m_status
            End Get
            Set(ByVal value As EncodeStatus)
                m_status = value
                onPropChanged("Status")
            End Set
        End Property

        ' Job for encoding
        Private job As LiveJob

        ' Type of encoding
        Friend publishType As Output

        Friend Sub New()
            publishType = Output.Archive
            Status = EncodeStatus.Hold
        End Sub

        Friend Function StartStream() As Boolean
            Busy = True
            ' Instantiates a new job for encoding
            job = New LiveJob()
            ' Adds event handler for job status
            AddHandler job.Status, AddressOf StreamStatus

            ' Verifies all information is entered
            If String.IsNullOrWhiteSpace(m_sourcePath) OrElse String.IsNullOrWhiteSpace(m_destinationPath) Then
                Return False
            End If

            Dim fileSource As LiveFileSource
            Try
                ' Sets file to active source and checks if it is valid
                fileSource = job.AddFileSource(m_sourcePath)
            Catch generatedExceptionName As InvalidMediaFileException
                Return False
            End Try

            ' Sets to loop media for streaming
            fileSource.PlaybackMode = FileSourcePlaybackMode.[Loop]

            ' Makes this file the active source. Multiple files can be added 
            ' and cued to move to each other at their ends
            job.ActivateSource(fileSource)

            ' Finds and applys a smooth streaming preset
            job.ApplyPreset(LivePresets.VC1IISSmoothStreaming720pWidescreen)

            ' Sets up variable for fomat data
            Select Case publishType
                Case Output.Archive
                    ' Verifies destination path exists and if not creates it
                    Try
                        If Not Directory.Exists(m_destinationPath) Then
                            Directory.CreateDirectory(m_destinationPath)
                        End If
                    Catch generatedExceptionName As IOException
                        Return False
                    End Try

                    Dim archiveFormat As New FileArchivePublishFormat()

                    ' Gets the location of the old extention and removes it
                    Dim filename As String = Path.GetFileNameWithoutExtension(m_sourcePath)

                    ' Sets the archive path and file name
                    archiveFormat.OutputFileName = Path.Combine(m_destinationPath, filename & ".ismv")
                    job.PublishFormats.Add(archiveFormat)
                    Exit Select

                Case Output.Publish
                    ' Setups streaming of media to publishing point
                    Dim publishFormat As New PushBroadcastPublishFormat()
                    Try
                        ' checks the path for a valid publishing point
                        publishFormat.PublishingPoint = New Uri(m_destinationPath)
                    Catch generatedExceptionName As UriFormatException
                        Return False
                    End Try

                    ' Adds the publishing format to the job
                    job.PublishFormats.Add(publishFormat)
                    Exit Select
                Case Else
                    Return False
            End Select
            job.StartEncoding()

            Return True
        End Function

        Friend Sub StopStream()
            job.StopEncoding()
            Busy = False
        End Sub

        Private Sub StreamStatus(ByVal obj As Object, ByVal e As EncodeStatusEventArgs)
            Status = e.Status
            If e.Status = EncodeStatus.EncodingError OrElse e.Status = EncodeStatus.PublishingPointError Then
                Busy = False
            End If
        End Sub

        Friend Sub Dispose()
            If job IsNot Nothing Then
                job.Dispose()
            End If

        End Sub

        Private Sub onPropChanged(ByVal propName As String)
            RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(propName))
        End Sub
    End Class
End Namespace
