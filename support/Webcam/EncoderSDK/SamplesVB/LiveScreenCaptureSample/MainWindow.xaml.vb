﻿Imports System.Collections.ObjectModel
Imports System.ComponentModel
Imports System.Windows
Imports System.Windows.Controls
Imports System.Windows.Input
Imports Microsoft.Expression.Encoder
Imports Microsoft.Expression.Encoder.Devices
Imports Microsoft.Expression.Encoder.Live

Namespace LiveScreenCaptureSample
    ''' <summary>
    ''' Interaction logic for MainWindow.xaml
    ''' </summary>
    Partial Public Class MainWindow
        Inherits Window
        ' Data class that holds all the Encoder data
        Public data As Data

        ' Flag for if we are capturing or not
        Private isCapturing As Boolean

        ' Flags for resizing of capture rect
        Private leftX As Boolean, rightX As Boolean
        Private topY As Boolean, bottomY As Boolean

        ' What the cursor is before we change it.
        Private defaultCursor As Cursor

        Public Sub New()
            InitializeComponent()

            ' Sets default capture window dimensions
            Dim x As Integer = 300, y As Integer = 300
            Dim w As Integer = 200, h As Integer = 200

            ' Set rectangle
            Canvas.SetLeft(CaptureRect, x)
            Canvas.SetTop(CaptureRect, y)
            CaptureRect.Width = w
            CaptureRect.Height = h

            data = New Data()

            DataContext = data
            defaultCursor = Me.Cursor
        End Sub

        Private Sub AudioDevicesComboBox_SelectionChanged(ByVal sender As Object, ByVal e As SelectionChangedEventArgs)
            ' Sets the audio source index based on the selection
            data.SetSource(AudioDevicesComboBox.SelectedIndex)
        End Sub

        Private Sub CaptureButton_Click(ByVal sender As Object, ByVal e As RoutedEventArgs)
            If isCapturing Then
                isCapturing = False
                CaptureRect.Visibility = Visibility.Visible
                CaptureButton.Content = "Capture"
                data.Job.StopEncoding()
            Else
                isCapturing = True
                CaptureRect.Visibility = Visibility.Collapsed
                CaptureButton.Content = "Stop"
                data.Start(CInt(Canvas.GetLeft(CaptureRect)), CInt(Canvas.GetTop(CaptureRect)), CInt(CaptureRect.Width), CInt(CaptureRect.Height))
            End If
        End Sub

        Private Sub ExitButton_Click(ByVal sender As Object, ByVal e As RoutedEventArgs)
            Me.Close()
        End Sub

        Private Sub CaptureRect_MouseEnter(ByVal sender As Object, ByVal e As MouseEventArgs)
            ' Gets the mouse position
            Dim p As Point = Mouse.GetPosition(Me)

            ' Resets the resize flags
            leftX = False
            rightX = False
            topY = False
            bottomY = False

            ' Gives some wiggle room for the mouse
            Dim wiggle As Double = 5.0

            ' gets the current dims of the capture rects
            Dim recLeft As Double = Canvas.GetLeft(CaptureRect)
            Dim recUp As Double = Canvas.GetTop(CaptureRect)
            Dim recRight As Double = recLeft + CaptureRect.Width
            Dim recBottom As Double = recUp + CaptureRect.Height

            ' Checks if the mouse is close to an edge and flags it if so
            If p.X <= recRight + wiggle AndAlso p.X >= recRight - wiggle Then
                rightX = True
            End If
            If p.Y <= recBottom + wiggle AndAlso p.Y >= recBottom - wiggle Then
                bottomY = True
            End If

            If p.X <= recLeft + wiggle AndAlso p.X >= recLeft - wiggle Then
                leftX = True
            End If
            If p.Y <= recUp + wiggle AndAlso p.Y >= recUp - wiggle Then
                topY = True
            End If

            ' Sets the cursor based on the flags thrown
            If rightX OrElse leftX Then
                Cursor = Cursors.SizeWE
            ElseIf topY OrElse bottomY Then
                Cursor = Cursors.SizeNS
            End If

            If (leftX AndAlso topY) OrElse (rightX AndAlso bottomY) Then
                Cursor = Cursors.SizeNWSE
            ElseIf (rightX AndAlso topY) OrElse (leftX AndAlso bottomY) Then
                Cursor = Cursors.SizeNESW
            End If
        End Sub

        ' Resets the cursor
        Private Sub CaptureRect_MouseLeave(ByVal sender As Object, ByVal e As MouseEventArgs)
            Me.Cursor = defaultCursor
        End Sub

        Private Sub CaptureRect_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs)
            If Mouse.LeftButton = MouseButtonState.Pressed Then
                ' We are pressing the mouse so, move the rectangle.
                TryCast(sender, FrameworkElement).CaptureMouse()
                Dim p As Point = Mouse.GetPosition(Me)

                If topY Then
                    ' We want to resize the top, not move the rect.
                    Dim temp As Double = Canvas.GetTop(CaptureRect)
                    Canvas.SetTop(CaptureRect, p.Y)
                    ' We have to resize the height to adjust for the change in the top of the rect.
                    CaptureRect.Height += temp - p.Y
                ElseIf bottomY Then
                    CaptureRect.Height = p.Y - Canvas.GetTop(CaptureRect)
                End If

                If leftX Then
                    ' We want to resize the top, not move the rect.
                    Dim temp As Double = Canvas.GetLeft(CaptureRect)
                    Canvas.SetLeft(CaptureRect, p.X)
                    ' We have to resize the width to adjust for the change in the left side of the rect.
                    CaptureRect.Width += temp - p.X
                ElseIf rightX Then
                    CaptureRect.Width = p.X - Canvas.GetLeft(CaptureRect)
                End If

                e.Handled = True
            Else
                ' no longer holding mouse down so release the mouse
                TryCast(sender, FrameworkElement).ReleaseMouseCapture()
            End If
        End Sub

        Private Sub ControlGrid_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs)
            ' move the control grid
            If e.LeftButton = MouseButtonState.Pressed Then
                TryCast(sender, FrameworkElement).CaptureMouse()
                Dim p As Point = Mouse.GetPosition(ControlGrid)
                Dim m As Thickness = ControlGrid.Margin
                m.Top += p.Y
                m.Left += p.X
                ControlGrid.Margin = m
            Else
                TryCast(sender, FrameworkElement).ReleaseMouseCapture()
            End If
        End Sub

        Private Sub ControlGrid_MouseEnter(ByVal sender As Object, ByVal e As MouseEventArgs)
            Cursor = Cursors.SizeAll
        End Sub

        Private Sub ControlGrid_MouseLeave(ByVal sender As Object, ByVal e As MouseEventArgs)
            Cursor = defaultCursor
        End Sub
    End Class

    ' Data class for encoder properties
    Public Class Data
        Implements INotifyPropertyChanged
        Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

        Public Job As LiveJob
        Public Source As LiveDeviceSource

        Private m_AudioDevices As ObservableCollection(Of EncoderDevice)
        Public Property AudioDevices() As ObservableCollection(Of EncoderDevice)
            Get
                Return m_AudioDevices
            End Get
            Set(ByVal value As ObservableCollection(Of EncoderDevice))
                m_AudioDevices = value
                onPropChanged("AudioDevices")
            End Set
        End Property

        Private OutputDirectory As String

        Public Sub New()
            ' Initializes Job and collection objects
            Job = New LiveJob()
            AudioDevices = New ObservableCollection(Of EncoderDevice)()

            ' Set the output directory for the job
            OutputDirectory = String.Format("{0}\Expression\Expression Encoder\Live Output\", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments))

            ' Setup the the live job parameters
            SetupJob()
        End Sub

        Private Sub SetupJob()
            ' Sets the audio devices to the collection. Adds a null at the beginning for user to select no device
            AudioDevices = New ObservableCollection(Of EncoderDevice)(EncoderDevices.FindDevices(EncoderDeviceType.Audio))
            AudioDevices.Insert(0, Nothing)

            ' Gets all the video devices and sets the screen source.
            Dim Video As EncoderDevice = Nothing
            Dim videoSources As Collection(Of EncoderDevice) = EncoderDevices.FindDevices(EncoderDeviceType.Video)
            For Each dev As EncoderDevice In videoSources
                If dev.Name.Contains("Screen Capture Source") Then
                    Video = dev
                End If
            Next

            ' Creats the source
            Source = Job.AddDeviceSource(Video, Nothing)

            ' Activates sources and sets preset to job
            Job.ActivateSource(Source)
            Job.ApplyPreset(LivePresets.VC1256kDSL16x9)
        End Sub

        Public Sub Start(ByVal x As Integer, ByVal y As Integer, ByVal width As Integer, ByVal height As Integer)
            ' Set capture rectangle
            Source.ScreenCaptureSourceProperties = New ScreenCaptureSourceProperties() With { _
             .Left = x, _
             .Top = y, _
             .Width = width, _
             .Height = height _
            }

            ' Set output file name to custom name based on timestamp
            Dim fileName As String = String.Format("capture_{0}.wmv", DateTime.Now)

            ' Remove invalid characters
            fileName = fileName.Replace("/"c, "-"c)
            fileName = fileName.Replace(":"c, "-"c)

            ' Clear any previous outputs
            Job.PublishFormats.Clear()

            ' Add publish format and start encode
            Job.PublishFormats.Add(New FileArchivePublishFormat() With {.OutputFileName = String.Format("{0}{1}", OutputDirectory, fileName)})
            Job.StartEncoding()
        End Sub

        Public Sub SetSource(ByVal index As Integer)
            ' Sets audio source based on the index selected
            Job.DeviceSources(0).AudioDevice = If(index < 0, Nothing, AudioDevices(index))
        End Sub

        Private Sub onPropChanged(ByVal propName As String)
            RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(propName))
        End Sub
    End Class
End Namespace