﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports Microsoft.Expression.Encoder

Namespace ThumbsFromMarkers
    Class Program
        Shared Sub Main(ByVal args As String())
            ' Checks the input arguments and verifies them
            If args.Length <> 2 Then
                Console.WriteLine("Usage: Markers <MediaItem> <Number of Markers>")
                Return
            End If

            ' Holds the number of markers specified
            Dim numMakers As UInteger

            ' creates a media item and tries to add the file specified
            Dim mItem As MediaItem
            Try
                mItem = New MediaItem(args(0))
                numMakers = Convert.ToUInt32(args(1))
            Catch generatedExceptionName As InvalidMediaFileException
                Console.WriteLine("Error: Invalid Media File or Path")
                Console.WriteLine("Usage: Markers <MediaItem> <Number of Markers>")
                Return
            Catch generatedExceptionName As FormatException
                Console.WriteLine("Error: Invalid Number, Must Be An Integer")
                Console.WriteLine("Usage: Markers <MediaItem> <Number of Markers>")
                Return
            End Try

            ' Gets the length of the media file in seconds
            Dim timeSpan__1 As Double = mItem.FileDuration.TotalSeconds

            ' Divides the time span by the number of markers
            ' Note: Adds one as beginning of file is one marker
            timeSpan__1 /= (numMakers + 1)

            ' Creates a marker and thumbnail for each time iteration
            ' and adds them to the media item
            For i As Integer = 0 To numMakers - 1
                Dim tmp As New Marker()

                ' Can generate a key frame at each marker if desired
                tmp.GenerateKeyFrame = False

                ' Grabs a thumbnail from each marker
                tmp.GenerateThumbnail = True

                ' Adds one to account for beginning of file
                tmp.Time = TimeSpan.FromSeconds(timeSpan__1 * (i + 1))
                mItem.Markers.Add(tmp)
            Next

            ' Creates a job for encoding
            Using job As New Job()
                ' Adds the media item and the ouput path
                job.MediaItems.Add(mItem)
                job.OutputDirectory = "c:\output"
                AddHandler job.EncodeProgress, AddressOf OnProgress
                job.Encode()
            End Using
        End Sub

        Private Shared Sub OnProgress(ByVal sender As Object, ByVal e As EncodeProgressEventArgs)
            Console.Write(vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack)
            Console.Write("{0:F2}%", e.Progress)
        End Sub
    End Class
End Namespace
