﻿Imports System.Drawing
Imports System.Drawing.Imaging
Imports System.IO
Imports Microsoft.Expression.Encoder

Namespace ThumbnailGenerator
    Class Program
        Private Shared m_Format As ImageFormat = ImageFormat.Bmp
        Private Shared m_Ext As String = "bmp"

        Private Shared m_NumberThumbs As Integer = 10
        Private Shared m_Offset As Double = 0
        Private Shared m_SourceFiles As String()

        Public Shared Sub Main(ByVal args As String())
            ' No arguments passed. Writing usage and closing
            If args.Length = 0 Then
                Usage()
                Return
            End If

            ' Parses Input
            ParseInput(args)

            ' Iterates through all files specified
            For c As Integer = 0 To m_SourceFiles.Length - 1
                ' Creates AudioVideoFile to get thumbnails from
                Dim sourceFile As String = m_SourceFiles(c)
                Dim avFile As AudioVideoFile = Nothing
                Try
                    avFile = New AudioVideoFile(sourceFile)
                Catch ex As Exception
                    Console.WriteLine(ex.Message)
                    Return
                End Try

                ' Verifies offset works with file duration
                Dim tempOffset As Double = m_Offset
                If tempOffset >= avFile.Duration.TotalSeconds Then
                    Console.WriteLine("Offset {0} greater than duration. Not using offset.", m_Offset)
                    tempOffset = 0
                End If
                ' Gets the time for each thumbnail.
                Dim thumbtime As Double = (avFile.Duration.TotalSeconds - tempOffset) / CDbl(m_NumberThumbs)

                ' Create ThumbnailGenerator object to get thumbs from AudioVideoFile
                Dim generator As Microsoft.Expression.Encoder.ThumbnailGenerator = avFile.CreateThumbnailGenerator(avFile.VideoStreams(0).VideoSize)

                ' Sets directory for thumbnail generation
                Dim bitmapDir As String = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures)

                Console.WriteLine("File {0} of {1}", c + 1, m_SourceFiles.Length)

                ' Gets the number of thumbnails desired
                For i As Integer = 0 To m_NumberThumbs - 1
                    ' Writes progress
                    Console.Write(vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack & vbBack)
                    Console.Write("{0} of {1}", i + 1, m_NumberThumbs)

                    ' Gets image based on timing
                    Dim image As Bitmap = generator.CreateThumbnail(TimeSpan.FromSeconds(CDbl(i) * thumbtime + m_Offset))

                    ' sets thumbnail name unique and saves
                    Dim bitmapName As String = String.Format("Thumb-{0}.{1}", i + 1, m_Ext)
                    image.Save(Path.Combine(bitmapDir, bitmapName), m_Format)

                    image.Dispose()
                Next
                generator.Dispose()
            Next
        End Sub

        Private Shared Sub ParseInput(ByVal args As String())
            For i As Integer = 0 To args.Length - 1
                Select Case args(i).ToLowerInvariant()
                    Case "/count"
                        i += 1
                        If Not Integer.TryParse(args(i), m_NumberThumbs) Then
                            Console.WriteLine("Invalid value {0}. Getting {1} thumbnails.", args(i), m_NumberThumbs)
                        End If
                        Exit Select
                    Case "/offset"
                        i += 1
                        If Not Double.TryParse(args(i), m_Offset) Then
                            Console.WriteLine("Invalid value {0}. Not using offset.", args(i))
                        End If
                        Exit Select
                    Case "/format"
                        i += 1
                        m_Ext = args(i).ToLowerInvariant()
                        Select Case m_Ext
                            Case "gif"
                                m_Format = ImageFormat.Gif
                                Exit Select
                            Case "jpg"
                                m_Format = ImageFormat.Jpeg
                                Exit Select
                            Case "png"
                                m_Format = ImageFormat.Png
                                Exit Select
                            Case "tif"
                                m_Format = ImageFormat.Tiff
                                Exit Select
                            Case Else
                                m_Format = ImageFormat.Bmp
                                m_Ext = "bmp"
                                Exit Select
                        End Select
                        Exit Select
                    Case Else
                        If Directory.Exists(args(i)) Then
                            m_SourceFiles = Directory.GetFiles(args(i))
                        ElseIf File.Exists(args(i)) Then
                            m_SourceFiles = New String() {args(i)}
                        Else
                            Console.WriteLine("Invalid file to generate thumbs from. Cannot continue.")
                            Return
                        End If
                        Exit Select
                End Select
            Next
        End Sub

        Private Shared Sub Usage()
            Console.WriteLine("Usage: </count> </offset> </format> [SourceFile][SourceDirectory]" & vbCr & vbLf & "Offset is in seconds and cannot exceed file duration.")
            Console.WriteLine("Example: ThumbnailGenerator.exe c:\video\video.wmv")
            Console.WriteLine("Example: ThumbnailGenerator.exe c:\video")
            Console.WriteLine("Example: ThumbnailGenerator.exe /count 5 c:\video\video.wmv")
            Console.WriteLine("Example: ThumbnailGenerator.exe /offset 1.5 c:\video\video.wmv")
            Console.WriteLine("Example: ThumbnailGenerator.exe /format jpg c:\video\video.wmv")
        End Sub
    End Class
End Namespace
