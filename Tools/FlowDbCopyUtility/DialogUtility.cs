﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//using Microsoft.Win32;


namespace FlowDbCopyUtility
{
	public static class DialogUtility
	{

		public static string SelectFile(string title, string initialDirectory, string filter)
		{
			OpenFileDialog dialog = GetOpenFileDialog(title, initialDirectory, filter);
			dialog.Multiselect = false;

			dialog.ShowDialog();

			return dialog.FileName;
		}

		public static string[] SelectFiles(string title, string initialDirectory, string filter)
		{
			OpenFileDialog dialog = GetOpenFileDialog(title, initialDirectory, filter);
			dialog.Multiselect = true;

			dialog.ShowDialog();

			return dialog.FileNames;
		}

		private static OpenFileDialog GetOpenFileDialog(string title, string initialDirectory, string filter)
		{
			OpenFileDialog dialog = new OpenFileDialog();
			dialog.Title = title;
			dialog.InitialDirectory = initialDirectory;
			dialog.Filter = filter;

			return dialog;
		}

		public static string SelectFolder(Environment.SpecialFolder rootFolder, string description)
		{
			FolderBrowserDialog dialog = GetFolderBrowserDialog(rootFolder, description);

			dialog.ShowDialog();

			return dialog.SelectedPath;
		}


		private static FolderBrowserDialog GetFolderBrowserDialog(Environment.SpecialFolder rootFolder, string description)
		{
			FolderBrowserDialog dialog = new FolderBrowserDialog();
			dialog.RootFolder = rootFolder;
			dialog.Description = description;

			return dialog;
		}

	}
}
