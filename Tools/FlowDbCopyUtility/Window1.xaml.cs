﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;

using System.Windows.Forms;


namespace FlowDbCopyUtility
{
	/// <summary>
	/// Interaction logic for Window1.xaml
	/// </summary>
	public partial class Window1 : Window
	{
		protected static DependencyProperty SourceDbFilesDirectoryProperty =
			DependencyProperty.Register("SourceDbFilesDirectory", typeof(string), typeof(Window1));

		public string SourceDbFilesDirectory
		{
			get { return (string)this.GetValue(SourceDbFilesDirectoryProperty); }
			set { this.SetValue(SourceDbFilesDirectoryProperty, value); }
		}

		protected static DependencyProperty AppDbFilesDirectoryProperty =
			DependencyProperty.Register("AppDbFilesDirectory", typeof(string), typeof(Window1));

		public string AppDbFilesDirectory
		{
			get { return (string)this.GetValue(AppDbFilesDirectoryProperty); }
			set { this.SetValue(AppDbFilesDirectoryProperty, value); }
		}

	
		public Window1()
		{
			InitializeComponent();

			this.SourceDbFilesDirectory = Properties.Settings.Default.SourceRepositoryDirectory;
			this.AppDbFilesDirectory = 
			    Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), @"Flow\Config\DbInstall");
		}


		private void AssignSourceDirButton_Click(object sender, RoutedEventArgs e)
		{
			using(FolderBrowserDialog dialog = new FolderBrowserDialog())
			{
				DirectoryInfo dirInfo = new DirectoryInfo(this.SourceDbFilesDirectory);

				dialog.SelectedPath = (dirInfo.Exists)
					? dirInfo.FullName
					: Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

				dialog.ShowDialog();

				if (!String.IsNullOrEmpty(dialog.SelectedPath))
				{
					this.SourceDbFilesDirectory = dialog.SelectedPath;
					Properties.Settings.Default.Save();
				}
			}
		}

		private void CopyFilesButton_Click(object sender, RoutedEventArgs e)
		{
			DirectoryInfo appDirInfo = new DirectoryInfo(this.AppDbFilesDirectory);

			if (appDirInfo.Exists)
				appDirInfo.Delete(true);

			appDirInfo.Create();

			DirectoryInfo sourceDirInfo = new DirectoryInfo(this.SourceDbFilesDirectory);

			foreach (FileInfo fileInfo in sourceDirInfo.GetFiles("*.sdf"))
			{
				string destPath = Path.Combine(this.AppDbFilesDirectory, fileInfo.Name);
				fileInfo.CopyTo(destPath);
			}

			this.Close();
		}
	}
}
