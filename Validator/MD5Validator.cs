﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.Lib.Crypto;
using System.IO;

namespace MD5Validator
{
    public class MD5Validation
    {
        #region privateMembers

        ValidatorMain m_mainWindow;
        List<string> m_errorFiles;

        #endregion

        #region constructors

        public MD5Validation(ValidatorMain mainWindow)
        {
            m_mainWindow = mainWindow;
        }

        #endregion

        #region publicProperties

        public string[] ErrorList
        {
            get { return m_errorFiles.ToArray(); }
        }

        #endregion

        #region publicMethods

        public bool ValidateDirectory(string dir)
        {
            if (m_mainWindow != null)
            {
                m_mainWindow.SetWorking();
            }
            dir = @"E:\ftproot\uploads\test\test\test";

            m_errorFiles = new List<string>();
            MD5Provider md5 = new MD5Provider();

            string[] md5Files = Directory.GetFiles(dir, "*.md5");

            for( int i = 0; i < md5Files.Length; i++)
            {
                if (m_mainWindow != null)
                {
                    m_mainWindow.UpdateProgress(i / (double)md5Files.Length, String.Format("Processing {0}.", md5Files[i]));
                }
                byte[] hash = md5.HashFile( Path.Combine(dir, Path.GetFileNameWithoutExtension(md5Files[i])));
                byte[] filehash = new byte[hash.Length];
                FileStream stream = new FileStream(Path.Combine(dir,md5Files[i]), FileMode.Open, FileAccess.Read);
                stream.Read(filehash, 0, filehash.Length);
                stream.Close();

                if (!hash.SequenceEqual(filehash))
                {
                    m_errorFiles.Add(md5Files[i]);
                }
            }

            if (m_mainWindow != null)
            {
                m_mainWindow.SetCompleted();
            }

            if (m_errorFiles.Count > 0)
                return false;


            return true;
        }

        #endregion

        
    }
}
