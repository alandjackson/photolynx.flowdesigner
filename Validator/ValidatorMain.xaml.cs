﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MD5Validator
{
    /// <summary>
    /// Interaction logic for ValidatorMain.xaml
    /// </summary>
    public partial class ValidatorMain : Window
    {
        private MD5Validation m_validator;

        public ValidatorMain()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            m_validator = new MD5Validation(this);

            bool passedValidation = m_validator.ValidateDirectory(System.IO.Directory.GetCurrentDirectory());

            if (!passedValidation)
            {
                ErrorList listWindow = new ErrorList();
                listWindow.List = m_validator.ErrorList;
                listWindow.Show();
            }
        }

        public void UpdateProgress(double progress, string status)
        {
            progressBarProgress.Value = progress;
            labelStatus.Content = status;
        }

        public void SetWorking()
        {
            buttonMulti.Content = "Cancel";
        }

        public void SetCompleted()
        {
            buttonMulti.Content = "Close";
            progressBarProgress.Value = progressBarProgress.Maximum;
            labelStatus.Content = "Complete.";
        }

        private void buttonMulti_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
