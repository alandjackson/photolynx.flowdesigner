Const ForReading = 1
Const ForWriting = 2

deleteExisting = true
incrementbuildnumber = true
dobuild = true
svnCheckin = true
buildzip = true
sendEmail = true

devenv = """C:\Program Files (x86)\Microsoft Visual Studio 10.0\Common7\IDE\devenv.exe"""
wrkdir = "D:\Source\Flow"
svnexe = """C:\Program Files\SlikSvn\bin\svn.exe"""
x86Dir = wrkdir & "\Projects\Flow.Setup\x86\Release"
x64Dir = wrkdir & "\Projects\Flow.Setup\x64\Release"

pldev = "d:\Google Drive\PLDEV\LatestBuild"

solution = wrkdir & "\FlowSmall.sln"
solutionInfo =  wrkdir & "\Projects\SolutionInfo.cs"
x86Project = wrkdir & "\Projects\Flow.Setup\x86\Flow.Setup.x86.vdproj"
x64Project = wrkdir & "\Projects\Flow.Setup\x64\Flow.Setup.x64.vdproj"
svnTrunk = wrkdir

x=MsgBox("Would you like to build Flow?",4,"Auto Build")
if x = 7 Then
Wscript.Echo "Exiting Now"
WScript.Quit
End If

x=MsgBox("Would you like to send an Email at the end?",4,"Auto Build")
if x = 7 Then
sendEmail = false
End If

'1 = vbOK - OK was clicked
'2 = vbCancel - Cancel was clicked
'3 = vbAbort - Abort was clicked
'4 = vbRetry - Retry was clicked
'5 = vbIgnore - Ignore was clicked
'6 = vbYes - Yes was clicked
'7 = vbNo - No was clicked

'SVN
Set objShell = WScript.CreateObject("WScript.shell")

'delete all files in the release dir (so if the build fails, we wont package up the old stuff)
Const DeleteReadOnly = TRUE
if deleteExisting Then
    Set objFSO = CreateObject("Scripting.FileSystemObject")
    objFSO.DeleteFile(x86Dir & "\*.*"), DeleteReadOnly
    objFSO.DeleteFile(x64Dir & "\*.*"), DeleteReadOnly
    objFSO.DeleteFile(pldev & "\*.*"), DeleteReadOnly
End If


'make sure we have latest build number file
Set filesys = CreateObject("Scripting.FileSystemObject") 
If filesys.FileExists(solutionInfo) Then
   filesys.DeleteFile solutionInfo 
End If
objShell.Run svnexe &" update " & solutionInfo & " " & x86Project & " " & x64Project & " --username cheap --password cookiecrisp", 2, true
'get latest svn rev number
Set oExec = objShell.exec (svnexe & " info " & svnTrunk & " --username cheap --password cookiecrisp")
strText = oExec.StdOut.ReadAll()
'Wscript.Echo strText
revAndGarbage=Split(strText, "Revision: ")(1)
cursvnrev = Split(revAndGarbage, chr(10))(0)
'cursvnrev = 1234
'Wscript.Echo cursvnrev

'open SolutionInfo and get the current number (only thing in quotes in the entire file)
'looking for this: [assembly: AssemblyVersion("1.4.201.2744")]

Set objFSO = CreateObject("Scripting.FileSystemObject")
Set objFile = objFSO.OpenTextFile(solutionInfo, ForReading)

Do Until objFile.AtEndOfStream
    strSearchString = objFile.ReadLine
    
    If InStr(strSearchString, """") > 0 Then
        firstquote = InStr(strSearchString, "(""")
        lastquote = InStrRev(strSearchString, """)")
        currentNumber = Mid(Left(strSearchString, lastquote - 1), firstquote+2)
        'Wscript.Echo currentNumber 

    End If
Loop
objFile.Close

'increment the build number
a=Split(currentNumber, ".")
major = a(0)
minor = a(1)
build = a(2)
svnrev = a(3)

newbuild =  build
newsvnrev = cursvnrev

If incrementbuildnumber Then
newbuild =  build + 1
newsvnrev = cursvnrev + 1
End If

newNumber = major & "." & minor & "." & newbuild & "." & newsvnrev
newNumberShort = major & "." & minor & "." & newbuild

'Wscript.Echo newNumber

'update SolutionInfo file with new build number:
Set objFSO = CreateObject("Scripting.FileSystemObject")
Set objFile = objFSO.OpenTextFile(solutionInfo, ForReading)

strText = objFile.ReadAll
objFile.Close
strNewText = Replace(strText, currentNumber, newNumber)

Set objFile = objFSO.OpenTextFile(solutionInfo, ForWriting)
objFile.WriteLine strNewText
objFile.Close

'update setup projects:
'"ProductName" = "8:Flow 1.4.201 (64-bit)"
'"ProductVersion" = "8:1.4.201"
'"ProductCode" = "8:{CDD30797-48F0-4A75-8150-7324253954E7}"

Set TypeLib = CreateObject("Scriptlet.TypeLib")
newguid = TypeLib.Guid 
'Wscript.Echo newguid

newProductName = "        ""ProductName"" = ""8:Flow " & newNumberShort & """"
newProductVersion = "        ""ProductVersion"" = ""8:" & newNumberShort & """"
newProductCode = "        ""ProductCode"" = ""8:" & Trim(Left(newguid, InStr(newguid,"}"))) & """"

Set objFile = objFSO.OpenTextFile(x86Project, ForReading)

strText = objFile.ReadAll
newText =""
objFile.Close
For Each line in Split(strText,Chr(10))
    if InStr(line,"ProductName") > 0 Then
        'Wscript.Echo "found ProductName: " & line 
        newText = newText & newProductName & Chr(10)
    Elseif InStr(line,"ProductVersion") > 0 Then
        ' Wscript.Echo "found ProductVersion: " & line
         newText = newText & newProductVersion & Chr(10)
    Elseif InStr(line,"ProductCode") > 0 Then
        If InStr(line,"{") > 0 Then
            'Wscript.Echo "found ProductCode: " & line
            newText = newText & newProductCode & Chr(10)
        Else
            newText = newText & line & Chr(10)
        End If
    Else
        newText = newText & line & Chr(10)
    End If
Next

Set objFile = objFSO.OpenTextFile(x86Project, ForWriting)
objFile.WriteLine newText
objFile.Close






Set TypeLib = CreateObject("Scriptlet.TypeLib")
newguid = TypeLib.Guid 
'Wscript.Echo newguid

newProductName = "        ""ProductName"" = ""8:Flow " & newNumberShort & " (64-bit)"""
newProductVersion = "        ""ProductVersion"" = ""8:" & newNumberShort & """"
newProductCode = "        ""ProductCode"" = ""8:" & Trim(Left(newguid, InStr(newguid,"}"))) & """"
Set objFile = objFSO.OpenTextFile(x64Project, ForReading)

strText = objFile.ReadAll
newText =""
objFile.Close
For Each line in Split(strText,Chr(10))
    if InStr(line,"ProductName") > 0 Then
        'Wscript.Echo "found ProductName: " & line 
        newText = newText & newProductName & Chr(10)
    Elseif InStr(line,"ProductVersion") > 0 Then
         'Wscript.Echo "found ProductVersion: " & line
         newText = newText & newProductVersion & Chr(10)
    Elseif InStr(line,"ProductCode") > 0 Then
        If InStr(line,"{") > 0 Then
            'Wscript.Echo "found ProductCode: " & line
            newText = newText & newProductCode & Chr(10)
        Else
            newText = newText & line & Chr(10)
        End If
    Else
        newText = newText & line & Chr(10)
    End If
Next

Set objFile = objFSO.OpenTextFile(x64Project, ForWriting)
objFile.WriteLine newText
objFile.Close




'Check back in the SolutionInfo file
If svnCheckin Then
    objShell.Run svnexe &" commit " & solutionInfo & " " & x86Project & " " & x64Project & " --message ""new build number: " & newNumber & """ --username cheap --password cookiecrisp", 2, true
    'objShell.Run svnexe &" commit " & x86Project & " --message ""new build number: " & newNumber & """ --username cheap --password cookiecrisp", 2, true
    'objShell.Run svnexe &" commit " & x64Project & " --message ""new build number: " & newNumber & """ --username cheap --password cookiecrisp", 2, true
End If



'''''''''''''
'build release config
'Set objShell = WScript.CreateObject("WScript.Shell")
If dobuild Then
    objShell.Run devenv & " " & solution & " /Rebuild ""Release|Any CPU""", 2, true
End If

x86Zip = "Flow.Setup.x86_" & newNumberShort & ".zip"
x64Zip = "Flow.Setup.x64_" & newNumberShort & ".zip"
If buildzip Then
'Wscript.Echo "building Zip"
    CreateZip pldev & "\" & x64Zip, x64Dir
    CreateZip pldev & "\" & x86Zip, x86Dir
End If

'''''''''''''




if sendEmail Then
    'email_recipients = "chadheap@gmail.com"
    email_recipients = "chadheap@gmail.com;cap808@gmail.com;chad@photolynx.com;christian@photolynx.com;ajackson@photolynx.com;j.workman@photolynx.com"

    email_from = "autobuild@photolynx.com"
    email_subject0 = "New Flow Build " & newNumber
    smtp_server = "smtp.photolynx.com"

    Set objMessage = CreateObject("CDO.Message")
    objMessage.Subject = email_subject0
    objMessage.From = email_from
    objMessage.To = email_recipients
    objMessage.TextBody = Chr(10) & Chr(10) & "New Flow build is now ready: " & Chr(10) & Chr(10) & "Build Number: " & newNumber & Chr(10) & Chr(10) & "Build Date: " & Now & " (Pacific Time)" & Chr(10) & Chr(10) & "Dev link (ALL Latest Builds): https://docs.google.com/folder/d/0B9FDG4KKCMlVQzBRdldnM3lQM28/edit" & Chr(10) & Chr(10) & "Public link (Select Builds Only): https://drive.google.com/folderview?id=0B9FDG4KKCMlVLTJZTnBSTTNEX1k&usp=sharing"& Chr(10) & Chr(10)
    objMessage.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing")=2
    objMessage.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver")="smtp.gmail.com"
    objMessage.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport")=465 
    objMessage.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusername")="autobuild@photolynx.com"
    objMessage.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendpassword")="autobuild!"
    objMessage.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpusessl")=true
    objMessage.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate")=1
    objMessage.Configuration.Fields.Update
    objMessage.Send
End If
Wscript.Echo "Done"




Sub NewZip(pathToZipFile)
   'WScript.Echo "Newing up a zip file (" & pathToZipFile & ") "
   Dim fso
   Set fso = CreateObject("Scripting.FileSystemObject")
   Dim file
   Set file = fso.CreateTextFile(pathToZipFile)
 
   file.Write Chr(80) & Chr(75) & Chr(5) & Chr(6) & String(18, 0)
 
   file.Close
   Set fso = Nothing
   Set file = Nothing
 
   WScript.Sleep 500
 
End Sub
 
Sub CreateZip(pathToZipFile, dirToZip)
 
   'WScript.Echo "Creating zip  (" & pathToZipFile & ") from (" & dirToZip & ")"
 
   Dim fso
   Set fso= Wscript.CreateObject("Scripting.FileSystemObject")
 
   pathToZipFile = fso.GetAbsolutePathName(pathToZipFile)
   dirToZip = fso.GetAbsolutePathName(dirToZip)
 
   If fso.FileExists(pathToZipFile) Then
       'WScript.Echo "That zip file already exists - deleting it."
       fso.DeleteFile pathToZipFile
   End If
 
   If Not fso.FolderExists(dirToZip) Then
       'WScript.Echo "The directory to zip does not exist."
       Exit Sub
   End If
 
   NewZip pathToZipFile
 
   dim sa
   set sa = CreateObject("Shell.Application")
 
   Dim zip
   Set zip = sa.NameSpace(pathToZipFile)
 
   'WScript.Echo "opening dir  (" & dirToZip & ")"
 
   Dim d
   Set d = sa.NameSpace(dirToZip)
 
   ' Look at http://msdn.microsoft.com/en-us/library/bb787866(VS.85).aspx
   ' for more information about the CopyHere function.
   zip.CopyHere d.items, 4
 
   Do Until d.Items.Count <= zip.Items.Count
       Wscript.Sleep(200)
   Loop
 
End Sub
